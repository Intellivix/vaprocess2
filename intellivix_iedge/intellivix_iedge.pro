TEMPLATE = subdirs   # for multi projects.
CONFIG += ordered    # sequenced build.

SUBDIRS = \
# iedge
   #BCCL \
   #VACL \
   Common \
   Live555 \
   VCM \
   IVCP \
   Intellivix \
# testing
   GoogleTest \

VACL.depends = BCCL
Common.depends = BCCL VACL
Live555.depends = BCCL
VCM.depends = BCCL VACL Common Live555
IVCP.depends = BCCL
Intellivix.depends = BCCL VACL Common Live555 IVCP
GoogleTest.depends = BCCL

# genernal settings for projects. 
OTHER_FILES += env.pri

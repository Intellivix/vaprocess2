#!/bin/bash

run_id () 
{
	if [ $# -eq 0 ] 
	then
		echo " # error. no param. count: $# "
		exit 1
	fi

	ID=$1

	echo " # creating [$ID]"
	rm -rf $ID
	mkdir -p $ID/Configurations 
	cp -r Configurations/SystemConfig.xml $ID/Configurations/
	nohup ./Intellivix -i $ID -c 1>/dev/null 2>&1 &
}



echo -n "How many process [ENTER]: "
read repeat

./kill.sh

rm -rf p*

echo "## input : $repeat"

for i in $(seq 1 "$repeat");
do
	run_id p$i
done



///////////////////////////////////////
/// c++
///////////////////////////////////////

#if defined( __cplusplus )

#if __cplusplus >= 201103L

/// thread( since c++11 )
#include <thread>
#include <mutex>
#include <condition_variable>
#include <chrono>
#endif // __cplusplus >= 201103L

/// c compatibility
#include <cstdio>
#include <cstdlib>
#include <cstddef>
#include <cstdarg>
#include <cstdint>
#include <cstring>
#include <cerrno>
#include <cctype>
#include <cmath>
#include <ctime>
#include <csignal>
#include <cassert>

/// stream
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

#include <map>
#include <stack>
#include <vector>
#include <list>
#include <queue>
#include <deque>

#include <algorithm>
#include <functional>

#endif // __cplusplus

///////////////////////////////////////
/// c
///////////////////////////////////////

/// pthread
#include <pthread.h>
#include <semaphore.h>

#ifdef __WIN32
#elif __linux
#include <unistd.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#endif


///////////////////////////////////////
/// external
///////////////////////////////////////

///// plog
//#define PLOG_CAPTURE_FILE
//#include <plog/Log.h>

///// rapidxml
//#include <rapidxml/rapidxml.hpp>

///// pugixml.
//#include <pugixml.hpp>

///// SimpleWebSocket
//#include <server_ws.hpp>

///// ffmpeg
//extern "C" {
//#include <libavformat/avformat.h>
//#include <libswscale/swscale.h>
//#include <libswresample/swresample.h>
//#include <libavutil/avstring.h>
//#include <libavutil/opt.h>
//};

///// opnecv2
//#if defined( __LIB_OPENCV )
//#include <opencv2/opencv.hpp>
//#include <opencv2/core/internal.hpp>
//#ifdef __linux__
//#include <opencv2/features2d/features2d.hpp>
//#else
//#include <opencv2/nonfree/features2d.hpp>
//#endif
//#else
//#include <opencv2/core/core_c.h>
//#include <opencv2/core/core.hpp>
//#include <opencv2/core/internal.hpp>
//#include <opencv2/imgproc/imgproc_c.h>
//#include <opencv2/imgproc/imgproc.hpp>
//#endif // __LIB_OPENCV

//#if defined( __LIB_JPEG )
//extern "C" {
//#include <jpeglib.h>
//};
//#if defined( __WIN32 )
//#if defined( _DEBUG )
//#pragma comment( lib, "libjpeg_DS.lib" )
//#else
//#pragma comment( lib, "libjpeg_RS.lib" )
//#endif
//#endif // __WIN32
//#endif // __LIB_JPEG

//#if defined( __LIB_PNG )
//extern "C" {
//#include <png.h>
//}
//#if defined( __WIN32 )
//#if defined( _DEBUG )
//#pragma comment( lib, "libpng_DS.lib" )
//#pragma comment( lib, "zlib_DS.lib" )
//#else
//#pragma comment( lib, "libpng_RS.lib" )
//#pragma comment( lib, "zlib_RS.lib" )
//#endif
//#endif // __WIN32
//#endif // __LIB_PNG

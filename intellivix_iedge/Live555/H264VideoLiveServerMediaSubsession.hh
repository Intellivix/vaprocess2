/**********
This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation; either version 2.1 of the License, or (at your
option) any later version. (See <http://www.gnu.org/copyleft/lesser.html>.)

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General Public License
along with this library; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
**********/
// "liveMedia"
// Copyright (c) 1996-2015 Live Networks, Inc.  All rights reserved.
// A 'ServerMediaSubsession' object that creates new, unicast, "RTPSink"s
// on demand, from a H264 Elementary Stream video file.
// C++ header

#ifndef _H264_VIDEO_LIVE_SERVER_MEDIA_SUBSESSION_HH
#define _H264_VIDEO_LIVE_SERVER_MEDIA_SUBSESSION_HH

#ifndef _FILE_SERVER_MEDIA_SUBSESSION_HH
#include "FileServerMediaSubsession.hh"
#endif
#include "RTSPCustomServer.h"


// jun: H264VideoFileServerMediaSubsession을 복사하여 구현한 클래스이다.  
//      ByteStreamLiveSource를 객체를 생성하는 것 말고는 수정사항이 없다.  

class H264VideoLiveServerMediaSubsession: public FileServerMediaSubsession {
public:
  static H264VideoLiveServerMediaSubsession*
  createNew(UsageEnvironment& env, CRTSPStreamingChannel* streamingChannel, char const* fileName, Boolean reuseFirstSource);

  // Used to implement "getAuxSDPLine()": 
  void checkForAuxSDPLine1();
  void afterPlayingDummy1();

protected:
  H264VideoLiveServerMediaSubsession(UsageEnvironment& env,
                  CRTSPStreamingChannel* streamingChannel, 
					   char const* fileName, 
				     Boolean reuseFirstSource);
      // called only by createNew();
  virtual ~H264VideoLiveServerMediaSubsession();

  void setDoneFlag() { fDoneFlag = ~0; }

protected: // redefined virtual functions
  virtual char const* getAuxSDPLine(RTPSink* rtpSink,
				    FramedSource* inputSource);
  virtual FramedSource* createNewStreamSource(unsigned clientSessionId,
					      unsigned& estBitrate);
  virtual RTPSink* createNewRTPSink(Groupsock* rtpGroupsock,
                                    unsigned char rtpPayloadTypeIfDynamic,
				    FramedSource* inputSource);

  //virtual void deleteStream(unsigned clientSessionId, void*& streamToken);

public:

   virtual void startStream(unsigned clientSessionId, void* streamToken,
      TaskFunc* rtcpRRHandler,
      void* rtcpRRHandlerClientData,
      unsigned short& rtpSeqNum,
      unsigned& rtpTimestamp,
      ServerRequestAlternativeByteHandler* serverRequestAlternativeByteHandler,
      void* serverRequestAlternativeByteHandlerClientData);
   
   virtual void pausePlay(unsigned clientSessionId, void* streamToken);
   //virtual void seekStream(unsigned clientSessionId, void* streamToken, double& seekNPT, double streamDuration, u_int64_t& numBytes);
   // This routine is used to seek by relative (i.e., NPT) time.
   // "streamDuration", if >0.0, specifies how much data to stream, past "seekNPT".  (If <=0.0, all remaining data is streamed.)
   // "numBytes" returns the size (in bytes) of the data to be streamed, or 0 if unknown or unlimited.
   virtual void seekStream(unsigned clientSessionId, void* streamToken, char*& absStart, char*& absEnd);
   // This routine is used to seek by 'absolute' time.
   // "absStart" should be a string of the form "YYYYMMDDTHHMMSSZ" or "YYYYMMDDTHHMMSS.<frac>Z".
   // "absEnd" should be either NULL (for no end time), or a string of the same form as "absStart".
   // These strings may be modified in-place, or can be reassigned to a newly-allocated value (after delete[]ing the original).
   //virtual void nullSeekStream(unsigned clientSessionId, void* streamToken);
   // Called whenever we're handling a "PLAY" command without a specified start time.
   virtual void setStreamScale(unsigned clientSessionId, void* streamToken, float scale);
   //virtual float getCurrentNPT(void* streamToken);
   //virtual FramedSource* getStreamSource(void* streamToken);
   virtual void testScaleFactor(float& scale); // sets "scale" to the actual supported scale
   //virtual float duration() const;
   // returns 0 for an unbounded session (the default)
   // returns > 0 for a bounded session
   //virtual void getAbsoluteTimeRange(char*& absStartTime, char*& absEndTime) const;

private:
  CRTSPStreamingChannel* fStreamingChannel; 

  char* fAuxSDPLine;
  char fDoneFlag; // used when setting up "fAuxSDPLine"
  RTPSink* fDummyRTPSink; // ditto
};

#endif

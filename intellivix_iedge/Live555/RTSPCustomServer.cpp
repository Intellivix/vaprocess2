﻿#include "stdafx.h"
#include <stdio.h>
//#include <conio.h>
#include <deque>
#include <algorithm>
#include "bccl_console.h"
#include "BasicUsageEnvironment.hh"
#include "liveMedia.hh"
#include "GroupsockHelper.hh"
#include "Live555Define.h"
#include "StreamParser.hh"
#include "ByteStreamLiveSource.hh"
#include "VAMetaDataLiveSource.hh"
#include "DynamicRTSPServer.hh"
#include "RTSPCustomServer.h"

#pragma pack( 8 )

//////////////////////////////////////////////////////////////////
//
// Class : CRTSPCustomServer
//
//////////////////////////////////////////////////////////////////

//CRTSPCustomServer* g_RTSPCustermServers = nullptr;

CRTSPCustomServer::CRTSPCustomServer()
{
   m_rtspServerPortNum             = 9000;
   m_rtspServerPortNum_Alternetive = 19000;
   eventLoopWatchVariable          = 0;
   m_hTaskSchedulerEventLoopThread = 0;
   m_pParam                        = nullptr;

   rtspServer = nullptr;
   authDB     = nullptr;
   scheduler  = BasicTaskScheduler::createNew( 1000 );
   env        = BasicUsageEnvironment::createNew( *scheduler );
}

CRTSPCustomServer::~CRTSPCustomServer()
{
   Deactivate();   

   env->reclaim();
   env = nullptr;
   delete scheduler;
   scheduler = nullptr;
   if( authDB ) delete authDB;
}

void CRTSPCustomServer::Register( CRTSPStreamingChannel* pRTSPStreamingChannel )
{
   CCriticalSectionSP co( m_csRTSPStreamingChannels );

   if( FALSE == IsExist( pRTSPStreamingChannel ) ) {
      std::string streamName = pRTSPStreamingChannel->m_strStreamName;
      std::transform( streamName.begin(), streamName.end(), streamName.begin(), ::tolower );
      m_mapRTSPStremingChannels.insert( MapStreamName2StreamingChannel::value_type( std::string( streamName ), pRTSPStreamingChannel ) );

      if( pRTSPStreamingChannel->m_strStreamName_Alias1.length() ) {
         std::string streamName_alias               = pRTSPStreamingChannel->m_strStreamName_Alias1;
         pRTSPStreamingChannel->m_pRTSPCustomServer = this;
         std::transform( streamName_alias.begin(), streamName_alias.end(), streamName_alias.begin(), ::tolower );
         m_mapRTSPStremingChannels.insert( MapStreamName2StreamingChannel::value_type( std::string( streamName_alias ), pRTSPStreamingChannel ) );
      }
      if( pRTSPStreamingChannel->m_strStreamName_Alias2.length() ) {
         std::string streamName_alias               = pRTSPStreamingChannel->m_strStreamName_Alias2;
         pRTSPStreamingChannel->m_pRTSPCustomServer = this;
         std::transform( streamName_alias.begin(), streamName_alias.end(), streamName_alias.begin(), ::tolower );
         m_mapRTSPStremingChannels.insert( MapStreamName2StreamingChannel::value_type( std::string( streamName_alias ), pRTSPStreamingChannel ) );
      }
   }
}

void CRTSPCustomServer::Unregister( CRTSPStreamingChannel* pRTSPStreamingChannel )
{
   CCriticalSectionSP co( m_csRTSPStreamingChannels );

   MapStreamName2StreamingChannel::iterator iter     = m_mapRTSPStremingChannels.begin();
   MapStreamName2StreamingChannel::iterator iter_end = m_mapRTSPStremingChannels.end();
   while( iter_end != iter ) {
      CRTSPStreamingChannel* pIterRTSPClient            = iter->second;
      MapStreamName2StreamingChannel::iterator iter_del = m_mapRTSPStremingChannels.end();
      if( pIterRTSPClient == pRTSPStreamingChannel ) {
         // jun: Unlink를 하면 프로그램 종료시 다운되는 문제가 있음. -> 원인파악 필요
         //      소멸자 함수에서 rtspServer객체를 삭제하지 않은 것과 관계있음.
         //      pIterRTSPClient->UnlinkAllLiveSource (   );  주석 처리

         // By JJong 2017.11.22
         // rtspServer 객체를 삭제 시키는 코드 추가로 원복
         pIterRTSPClient->UnlinkAllLiveSource (   ); 
         
         iter_del = iter;
      }
      iter++;
      if( m_mapRTSPStremingChannels.end() != iter_del ) {
         m_mapRTSPStremingChannels.erase( iter_del );
      }
   }
}

BOOL CRTSPCustomServer::IsExist( CRTSPStreamingChannel* pRTSPStreamingChannel )
{
   BOOL bExist = FALSE;

   CCriticalSectionSP co( m_csRTSPStreamingChannels );
   MapStreamName2StreamingChannel::iterator iter     = m_mapRTSPStremingChannels.begin();
   MapStreamName2StreamingChannel::iterator iter_end = m_mapRTSPStremingChannels.end();
   while( iter_end != iter ) {
      CRTSPStreamingChannel* pIterRTSPClient = iter->second;
      if( pIterRTSPClient == pRTSPStreamingChannel ) {
         bExist = TRUE;
         break;
      }
      iter++;
   }

   return bExist;
}

CRTSPStreamingChannel* CRTSPCustomServer::Find( const std::string& strStreamName )
{
   CRTSPStreamingChannel* pFindRTSPStreamingChannel = NULL;

   CCriticalSectionSP co( m_csRTSPStreamingChannels );
   std::string _strStreamName;
   _strStreamName                                = strStreamName;
   MapStreamName2StreamingChannel::iterator iter = m_mapRTSPStremingChannels.find( _strStreamName );
   if( m_mapRTSPStremingChannels.end() != iter ) {
      pFindRTSPStreamingChannel = iter->second;
   }

   return pFindRTSPStreamingChannel;
}

void CRTSPCustomServer::AddUserRecord( char const* username, char const* password )
{
   // 스레드가 실행되어 있지 않을 경우에만 사용자를 추가하도록 함.
   if( m_hTaskSchedulerEventLoopThread ) return;
   if( NULL == authDB ) authDB = new UserAuthenticationDatabase;
   authDB->addUserRecord( username, password );
}

void CRTSPCustomServer::removeUserRecord( char const* username )
{
   if( m_hTaskSchedulerEventLoopThread ) return;
   authDB->removeUserRecord( username );
}

UINT CRTSPCustomServer::TaskSchedulerEventLoopProc()
{
   // Create the RTSP server.  Try first with the default port number (554),
   // and then with the alternative port number (8554):
   
   m_rtspServerPortNum = 9000;  // jun_u+ 
   for (int i = 0; i < 200; i++) {
      rtspServer = DynamicRTSPServer::createNew( *env, this, m_rtspServerPortNum+i, authDB );
      if( rtspServer) {
         logi( "RTSP Server Port Open [Port : %d]\n", m_rtspServerPortNum+i );
         break;
      }
   }
   if( rtspServer == NULL ) {
      *env << "Failed to create RTSP server: " << env->getResultMsg() << "\n";
      loge( "Rtsp Server Open Failed... [Port : %d]\n", m_rtspServerPortNum );
      return ( 1 );
   }

   // Also, attempt to create a HTTP server for RTSP-over-HTTP tunneling.
   // Try first with the default HTTP port (80), and then with the alternative HTTP
   // port numbers (8000 and 8080).
   /*
   if( rtspServer->setUpTunnelingOverHTTP( 80 ) || rtspServer->setUpTunnelingOverHTTP( 8000 ) || rtspServer->setUpTunnelingOverHTTP( 8080 ) ) {
      *env << "(We use port " << rtspServer->httpServerPortNum() << " for optional RTSP-over-HTTP tunneling, or for HTTP live streaming (for indexed Transport Stream files only).)\n";
   } else {
      *env << "(RTSP-over-HTTP tunneling is not available.)\n";
   }
   */

   env->taskScheduler().doEventLoop( &eventLoopWatchVariable );

   return ( DONE );
}

UINT CRTSPCustomServer::Thread_TaskSchedulerEventLoop( LPVOID pParam )
{
#ifdef SUPPORT_PTHREAD_NAME
   pthread_setname_np( pthread_self(), "RtspServer" );
#endif
   //logd ("///////////  Thread_TaskSchedulerEventLoop - Started  ///////////\n");
   CRTSPCustomServer* pRTSPCustomServer = (CRTSPCustomServer*)pParam;
   pRTSPCustomServer->TaskSchedulerEventLoopProc();
   //logd ("///////////  Thread_TaskSchedulerEventLoop - Ended  ///////////\n");
   return ( DONE );
}

void CRTSPCustomServer::Activate()
{
   // 중요: 스레드의 시작/종료는 임계영역에 의하여 보호되어야 한다.
   CCriticalSectionSP co( m_csRTSPStreamingChannels );
   if( !m_hTaskSchedulerEventLoopThread ) {
      eventLoopWatchVariable          = 0;
      CWinThread* pThread             = AfxBeginThread( Thread_TaskSchedulerEventLoop, (LPVOID)this, THREAD_PRIORITY_HIGHEST, CREATE_SUSPENDED );
      m_hTaskSchedulerEventLoopThread = pThread->m_hThread;
      pThread->ResumeThread();
   }
}

void CRTSPCustomServer::Deactivate()
{
   //logd ("///////////  CRTSPCustomServer::Deactivate - Started    ///////////\n");

   // 중요: 스레드의 시작/종료는 임계영역에 의하여 보호되어야 한다.
   CCriticalSectionSP co( m_csRTSPStreamingChannels );
   eventLoopWatchVariable = 1;
   if( m_hTaskSchedulerEventLoopThread ) {
      WaitForSingleObject( m_hTaskSchedulerEventLoopThread, INFINITE );
      m_hTaskSchedulerEventLoopThread = 0;
   }

   // By JJong 2017.11.22
   // rtspServer 객체를 delete 하기 위해서는 Medium::close() 함수 호출을 통해서
   // 내부적으로 삭제하게 해야지만 종료 시에 죽지 않는다.
   if (rtspServer)
   {
      Medium::close(rtspServer);
      rtspServer = nullptr;
   }

   //logd ("///////////  CRTSPCustomServer::Deactivate - Ended    ///////////\n");
}

//////////////////////////////////////////////////////////////////
//
// Class : CRTSPStreamingChannel
//
//////////////////////////////////////////////////////////////////

CRTSPStreamingChannel::CRTSPStreamingChannel()
{
   m_bKeyFramePushed            = FALSE;
   m_nVideoPushCount            = 0;
   m_nVAMetaDataFlag            = 0;
   m_pRTSPCustomServer          = NULL;
   m_nCRTSPStreamingChannelType = StreamingChannelLive;
   m_fScale                     = 0.0;
   m_pParam                     = NULL;
}

CRTSPStreamingChannel::~CRTSPStreamingChannel()
{
}

BOOL CRTSPStreamingChannel::PushVideoData( CRTSPVideoItem* pVideoBuff )
{
   if( NULL == pVideoBuff )
      return FALSE;
   if( FALSE == IsVideoStreamRequested() )
      return FALSE;

   if( LIVE555_VFRM_KEYFRAME & pVideoBuff->m_dwFlag )
      m_bKeyFramePushed = TRUE;
   m_nVideoPushCount++;

   // 150프레임을 관찰하였을 때 키 프레임이 없다면 키프레임이 있다고 간주한다.
   // (간혹 키프레임 없는 스트림이 존재 할 수 도 있으니까)
   if( FALSE == m_bKeyFramePushed ) {
      if( m_nVideoPushCount > 150 ) {
         m_bKeyFramePushed = TRUE;
      }
   }

   // 라이브 소스에 버퍼를 밀어넣는다.
   int nLiveSourceIdx = 0;
   m_csVideoSource.Lock();
   int nLiveSourceNum                          = (int)m_VideoSourceList.size();
   std::deque<FramedSource*>::iterator iter    = m_VideoSourceList.begin();
   std::deque<FramedSource*>::iterator iterEnd = m_VideoSourceList.end();
   while( iter != iterEnd ) {
      ByteStreamLiveSource* pLiveSource = (ByteStreamLiveSource*)*iter;
      if( pLiveSource && *iter == pLiveSource ) {
         if( nLiveSourceIdx < nLiveSourceNum - 1 ) {
            // 마지막 라이브 소스가 아니면 버퍼를 복사한 후 푸시한다.
            CRTSPVideoItem* pNewVideoBuff = new CRTSPVideoItem;
            *pNewVideoBuff                = *pVideoBuff;
            pLiveSource->fStreamingBufferQueue.Push( pNewVideoBuff );
         } else {
            // 마지막 라이브 소스는 바로 푸시한다.
            pLiveSource->fStreamingBufferQueue.Push( pVideoBuff );
         }

         // 방금 푸시한 비디오버퍼가 키프레임 인 경우 큐의 길이가 지정된 길이보다 길면 키프레임 전까지 비운다.
         if( pVideoBuff->m_dwFlag & LIVE555_VFRM_KEYFRAME ) {
            if( pLiveSource->fStreamingBufferQueue.Size() > int( pVideoBuff->m_fFrameRate ) * 0.25f + 1 ) {
               DeleteUntileKeyFrame( pLiveSource->fStreamingBufferQueue );
            }
         }
      }
      iter++;
      nLiveSourceIdx++;
   }

   m_csVideoSource.Unlock();

   return TRUE;
}

BOOL CRTSPStreamingChannel::PushVAMetaData( CRTSPVAMetaData* pRTSPVAMetaData )
{
   if( NULL == pRTSPVAMetaData )
      return FALSE;
   if( FALSE == IsVAMetaDataRequested() )
      return FALSE;

   // 라이브 소스에 버퍼를 밀어넣는다.
   m_csAMetaSource.Lock();
   int nLiveSourceNum = (int)m_MetaSourceList.size();
   std::deque<FramedSource*>::iterator iter, iterEnd;

   std::deque<FramedSource*> liveVAMetaDataSourceList_MetaDataPush;

   iter    = m_MetaSourceList.begin();
   iterEnd = m_MetaSourceList.end();
   while( iter != iterEnd ) {
      VAMetaDataLiveSource* pLiveSource = (VAMetaDataLiveSource*)*iter;
      if( pLiveSource ) {
         if( pLiveSource->fMetaDataFlag & pRTSPVAMetaData->m_VAMetaDataFlag ) {
            liveVAMetaDataSourceList_MetaDataPush.push_back( pLiveSource );
         }
      }
      iter++;
   }

   int nLiveSourceNum_MetaDataPush = (int)liveVAMetaDataSourceList_MetaDataPush.size();

   if( nLiveSourceNum_MetaDataPush > 0 ) {
      int nLiveSourceIdx = 0;
      iter               = liveVAMetaDataSourceList_MetaDataPush.begin();
      iterEnd            = liveVAMetaDataSourceList_MetaDataPush.end();
      while( iter != iterEnd ) {
         VAMetaDataLiveSource* pLiveSource = (VAMetaDataLiveSource*)*iter;
         if( pLiveSource ) {
            if( nLiveSourceIdx < nLiveSourceNum_MetaDataPush - 1 ) {
               // 마지막 라이브 소스가 아니면 버퍼를 복사한 후 푸시한다.
               CRTSPVAMetaData* pNewRTSPTextData = new CRTSPVAMetaData;
               *pNewRTSPTextData                 = *pRTSPVAMetaData;
               pLiveSource->fVAMetadataQueue.Push( pNewRTSPTextData );
            } else {
               // 마지막 라이브 소스는 바로 푸시한다.
               pLiveSource->fVAMetadataQueue.Push( pRTSPVAMetaData );
            }
         }
         iter++;
         nLiveSourceIdx++;
      }
   } else {
      delete pRTSPVAMetaData;
   }
   m_csAMetaSource.Unlock();

   if( nLiveSourceNum_MetaDataPush <= 0 )
      return FALSE;

   return TRUE;
}

BOOL CRTSPStreamingChannel::IsVideoStreamRequested()
{
   BOOL bLiveSourceExist = FALSE;
   CCriticalSectionSP co( m_csVideoSource );
   if( m_VideoSourceList.size() )
      bLiveSourceExist = TRUE;
   return bLiveSourceExist;
}

BOOL CRTSPStreamingChannel::IsVAMetaDataRequested( uint nVAMetaDataFlag )
{
   BOOL bLiveSourceExist = FALSE;
   CCriticalSectionSP co( m_csVideoSource );
   if( nVAMetaDataFlag ) {
      std::deque<FramedSource*>::iterator iter, iterEnd;
      iter    = m_MetaSourceList.begin();
      iterEnd = m_MetaSourceList.end();
      while( iter != iterEnd ) {
         VAMetaDataLiveSource* pLiveSource = (VAMetaDataLiveSource*)*iter;
         if( pLiveSource ) {
            if( LIVE555_META_SEND_WHEN_DATA_CHANGED & nVAMetaDataFlag ) {
               if( pLiveSource->fMetaDataFlag_ToBeSent & nVAMetaDataFlag ) {
                  bLiveSourceExist = TRUE;
                  break;
               }
            } else {
               if( pLiveSource->fMetaDataFlag & nVAMetaDataFlag ) {
                  bLiveSourceExist = TRUE;
                  break;
               }
            }
         }
         iter++;
      }
   } else {
      if( m_MetaSourceList.size() )
         bLiveSourceExist = TRUE;
   }

   return bLiveSourceExist;
}

void CRTSPStreamingChannel::SetVAMetaDataToBeSent( uint nVAMetaDataFlag )
{
   CCriticalSectionSP co( m_csVideoSource );
   if( nVAMetaDataFlag ) {
      std::deque<FramedSource*>::iterator iter, iterEnd;
      iter    = m_MetaSourceList.begin();
      iterEnd = m_MetaSourceList.end();
      while( iter != iterEnd ) {
         VAMetaDataLiveSource* pLiveSource = (VAMetaDataLiveSource*)*iter;
         if( pLiveSource ) {
            if( pLiveSource->fMetaDataFlag & nVAMetaDataFlag ) {
               pLiveSource->fMetaDataFlag_ToBeSent |= nVAMetaDataFlag;
            }
         }
         iter++;
      }
   }
}

void CRTSPStreamingChannel::AddVideoSource( FramedSource* pVideoSource )
{
   CCriticalSectionSP co( m_csVideoSource );
   m_VideoSourceList.push_back( pVideoSource );
}

BOOL CRTSPStreamingChannel::RemoveVideoSource( FramedSource* pVideoSource )
{
   BOOL bRemove = FALSE;
   CCriticalSectionSP co( m_csVideoSource );
   std::deque<FramedSource*>::iterator iter    = m_VideoSourceList.begin();
   std::deque<FramedSource*>::iterator iterEnd = m_VideoSourceList.end();
   while( iter != iterEnd ) {
      if( *iter == pVideoSource ) {
         m_VideoSourceList.erase( iter );
         bRemove = TRUE;
         break;
      }
      iter++;
   }

   //OnRemoveAnyLiveSource (   );

   return bRemove;
}

BOOL CRTSPStreamingChannel::IsStreamingChannelReady()
{
   return m_bKeyFramePushed;
}

void CRTSPStreamingChannel::AddVAMetaSource( FramedSource* pVAMetaSource )
{
   CCriticalSectionSP co( m_csAMetaSource );
   m_MetaSourceList.push_back( pVAMetaSource );

   UpdateVAMetadataFlag();
}

BOOL CRTSPStreamingChannel::RemoveVAMetaSource( FramedSource* pVAMetaSource )
{
   BOOL bRemove = FALSE;
   CCriticalSectionSP co( m_csAMetaSource );
   std::deque<FramedSource*>::iterator iter    = m_MetaSourceList.begin();
   std::deque<FramedSource*>::iterator iterEnd = m_MetaSourceList.end();
   while( iter != iterEnd ) {
      if( *iter == pVAMetaSource ) {
         m_MetaSourceList.erase( iter );
         bRemove = TRUE;
         break;
      }
      iter++;
   }

   //OnRemoveAnyLiveSource (   );

   return bRemove;
}

//void CRTSPStreamingChannel::OnRemoveAnyLiveSource (   )
//{
//   // 라이브 소스 객체가 모두 삭제되는 시점에 연결이 끊긴 것으로 본다.
//   m_csVideoSource.Lock (   );
//
//   if(m_strStreamName.Find("play.imp") >=0)
//   {
//      if(m_pRTSPCustomServer)
//         m_pRTSPCustomServer->OnStreamingChannelClosed (m_pRTSPCustomServer->m_pParam, this);
//   }
//   else
//   {
//      if (!m_VideoSourceList.size()) {
//         m_csAMetaSource.Lock (   );
//         if (!m_MetaSourceList.size()) {
//            if (m_pRTSPCustomServer)
//               m_pRTSPCustomServer->OnStreamingChannelClosed (m_pRTSPCustomServer->m_pParam, this);
//         }
//         m_csAMetaSource.unlock (   );
//      }
//   }
//   m_csVideoSource.unlock (   );
//}

void CRTSPStreamingChannel::UpdateVAMetadataFlag()
{
   uint nVAMetaDataFlag = 0;

   // 라이브 소스에 버퍼를 밀어넣는다.
   CCriticalSectionSP co( m_csAMetaSource );
   std::deque<FramedSource*>::iterator iter    = m_MetaSourceList.begin();
   std::deque<FramedSource*>::iterator iterEnd = m_MetaSourceList.end();
   while( iter != iterEnd ) {
      VAMetaDataLiveSource* pLiveSource = (VAMetaDataLiveSource*)*iter;
      if( pLiveSource ) {
         nVAMetaDataFlag |= pLiveSource->fMetaDataFlag;
      }
      iter++;
   }
   m_nVAMetaDataFlag = nVAMetaDataFlag;
   //logd ("m_nVAMetaDataFlag:%08x\n", m_nVAMetaDataFlag);
}

void CRTSPStreamingChannel::UnlinkAllLiveSource()
{
   {
      CCriticalSectionSP co( m_csVideoSource );
      std::deque<FramedSource*>::iterator iter    = m_VideoSourceList.begin();
      std::deque<FramedSource*>::iterator iterEnd = m_VideoSourceList.end();
      while( iter != iterEnd ) {
         ByteStreamLiveSource* pLiveSource = (ByteStreamLiveSource*)*iter;
         pLiveSource->fStreamingChannel    = NULL;
         iter++;
      }
   }

   {
      CCriticalSectionSP co( m_csAMetaSource );
      std::deque<FramedSource*>::iterator iter    = m_MetaSourceList.begin();
      std::deque<FramedSource*>::iterator iterEnd = m_MetaSourceList.end();
      while( iter != iterEnd ) {
         VAMetaDataLiveSource* pLiveSource = (VAMetaDataLiveSource*)*iter;
         pLiveSource->fStreamingChannel    = NULL;
         iter++;
      }
   }
}

#pragma pack()

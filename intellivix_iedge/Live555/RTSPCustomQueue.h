#pragma once

#include <deque>
#include "bccl_file.h"
#include "Live555Define.h"

#ifdef __WIN32
#include <winsock2.h>
#endif
using namespace BCCL;

#pragma pack (8)

//////////////////////////////////////////////////////
//
// Class : CRTSPVideoItem
//
//////////////////////////////////////////////////////

class CRTSPVideoItem : public BCCL::FileIO
{
public:
   int          m_nCodecID;
   float        m_fFrameRate;
   DWORD        m_dwFlag;
   timeval      m_PresentationTime;

public:
   CRTSPVideoItem  (   );
   ~CRTSPVideoItem (   );

   CRTSPVideoItem& operator=  (CRTSPVideoItem& from);
};

//////////////////////////////////////////////////////
//
// Class : CRTSPVAMetaData
//
//////////////////////////////////////////////////////

class CRTSPVAMetaData : public BCCL::FileIO
{
public:
   float        m_fFrameRate;
   timeval      m_PresentationTime;
   uint         m_VAMetaDataFlag;   

public:
   CRTSPVAMetaData  (   );
   ~CRTSPVAMetaData (   );

   CRTSPVAMetaData& operator=  (CRTSPVAMetaData& from);
};


class CRTSPCustomClient;

//////////////////////////////////////////////////////
//
// Class : CRTSPCustomCommand
//
//////////////////////////////////////////////////////

class CRTSPCustomCommand
{
public:
   int                          m_nCommandType;
   CRTSPCustomClient*           m_pCustomClient;

   CEvent*                      m_pevtCommandExcuted;

   friend class CRTSPCustomClient;
};


//////////////////////////////////////////////////////
//
// Class : CRTSPCustomQueue
//
//////////////////////////////////////////////////////

template<class T>
class CRTSPCustomQueue : public std::deque<T*>
{
public:
   CCriticalSection m_csQueue;

public:
   CRTSPCustomQueue  (void);
   ~CRTSPCustomQueue (void);

public:
   T* operator[] (uint i);

public:
   void  Lock   (  );
   void  Unlock (  );
   int   Size   (  );
   void  Push   (T* pItem);
   T*    Pop    (  );
   T*    Front  (  );
   void  Clear  (  );
}; 

//////////////////////////////////////////////////////
// Implementation

template<class T>
CRTSPCustomQueue<T>::CRTSPCustomQueue () 
{
}

template<class T>
CRTSPCustomQueue<T>::~CRTSPCustomQueue () 
{
   Clear ();
}

template<class T>
T* CRTSPCustomQueue<T>::operator[] (uint i) 
{
   CCriticalSectionSP co(m_csQueue);
   T* pItem = ((std::deque<T*>)(*this))[i];
   
   return pItem;
}

template<class T>
int CRTSPCustomQueue<T>::Size (  )
{
   CCriticalSectionSP co(m_csQueue);
   int nSize = (int)this->size ();   
   return nSize;
}

template<class T>
void CRTSPCustomQueue<T>::Lock (  )
{
   m_csQueue.Lock();
}

template<class T>
void CRTSPCustomQueue<T>::Unlock (  )
{
   m_csQueue.Unlock();
}

template<class T>
void CRTSPCustomQueue<T>::Push (T* pItem)
{
   CCriticalSectionSP co(m_csQueue);
   this->push_back (pItem);   
}

template<class T>
T* CRTSPCustomQueue<T>::Pop  (  )
{
   T* pItem = NULL;

   CCriticalSectionSP co(m_csQueue);
   int nSize = (int) this->size ();
   if (nSize)
   {
      pItem = this->front (  );
      this->pop_front (  );
   }   

   return pItem;
}

template<class T>
T* CRTSPCustomQueue<T>::Front (  )
{
   CCriticalSectionSP lk(m_csQueue);
   T* pItem = this->front (  );
   
   return pItem;
}

template<class T>
void CRTSPCustomQueue<T>::Clear  (  )
{
   typedef typename std::deque<T*>::iterator DIter;

   CCriticalSectionSP lk(m_csQueue);
   int nItem = (int) this->size ();
   if (nItem) {
      DIter iter = this->begin ();
      DIter iterEnd = this->end ();
      while (iter != iterEnd) {
         delete *iter;
         iter++;
      }
   }
   this->clear ();   
}


typedef CRTSPCustomQueue<CRTSPVideoItem>     CRTSPVideoQueue;
typedef CRTSPCustomQueue<CRTSPVAMetaData>    CRTSPVAMetadataQueue;
typedef CRTSPCustomQueue<CRTSPCustomCommand> CRTSPCustomCommandQueue;

void DeleteUntileKeyFrame (CRTSPVideoQueue& queue);


#pragma pack ()


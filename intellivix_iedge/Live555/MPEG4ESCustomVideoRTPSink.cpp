#include "stdafx.h"
#include "MPEG4ESCustomVideoRTPSink.h"

CMPEG4ESCustomVideoRTPSink::CMPEG4ESCustomVideoRTPSink( UsageEnvironment& env, unsigned bufferSize, IRTSPClient* pRTSPClient )
    : MediaSink( env )
{
   m_Buffer      = new uint8_t[bufferSize];
   m_nBufferSize = bufferSize;
   m_pRTSPClient = pRTSPClient;
}

CMPEG4ESCustomVideoRTPSink::~CMPEG4ESCustomVideoRTPSink( void )
{
   delete[] m_Buffer;
}

CMPEG4ESCustomVideoRTPSink* CMPEG4ESCustomVideoRTPSink::createNew( UsageEnvironment& env, unsigned bufferSize, IRTSPClient* pRTSPClient )
{
   return new CMPEG4ESCustomVideoRTPSink( env, bufferSize, pRTSPClient );
}

Boolean CMPEG4ESCustomVideoRTPSink::continuePlaying()
{
   if( fSource == 0 || m_nBufferSize == 0 || m_Buffer == 0 ) return False;
   fSource->getNextFrame( m_Buffer, m_nBufferSize, afterGettingFrame, this, onSourceClosure, this );
   return True;
}

void CMPEG4ESCustomVideoRTPSink::afterGettingFrame( void* clientData, unsigned frameSize, unsigned numTruncatedBytes, struct timeval presentationTime, unsigned durationInMicroseconds )
{
   CMPEG4ESCustomVideoRTPSink* sink = (CMPEG4ESCustomVideoRTPSink*)clientData;
   sink->afterGettingFrame1( frameSize, presentationTime );
}

void CMPEG4ESCustomVideoRTPSink::afterGettingFrame1( unsigned frameSize, struct timeval presentationTime )
{

#ifdef DEBUG
/*TCHAR szTemp[200] ={0,};
   sprintf(szTemp,"%02X %02X %02X %02X %02X "
   "%02X %02X %02X %02X %02X \r\n",
   m_Buffer[0],m_Buffer[1],m_Buffer[2],m_Buffer[3],m_Buffer[4],m_Buffer[5],
   m_Buffer[6],m_Buffer[7],m_Buffer[8],m_Buffer[9]);
   OutputDebugString(szTemp);*/
#endif
   unsigned int video_flag = 0;
   if( ( m_Buffer[2] == 1 && ( m_Buffer[3] == 0xb3 || m_Buffer[3] == 0xb0 ) ) )
      video_flag = LIVE555_VFRM_KEYFRAME; // 占쏙옙占쌜븝옙트

   if( m_pRTSPClient ) {
      m_pRTSPClient->OnVideoFrameReceived( LIVE555_VCODEC_ID_MPEG4, m_Buffer, frameSize, video_flag, presentationTime );
   }
   continuePlaying();
}

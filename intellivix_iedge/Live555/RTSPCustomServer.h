#pragma once

#include <deque>
#include <map>
#include <string>
#include "RTSPCustomQueue.h"

#pragma pack( 8 )

class FramedSource;
class UserAuthenticationDatabase;
class BasicTaskScheduler;
class UsageEnvironment;
class CRTSPStreamingChannel;
class RTSPServer;

typedef struct tag_RTSP_PlayCtrl {
   std::string strStime;
   std::string strEtime;
   std::string strReqTime;
   std::string strPlayStream;
   float nScale;
   BOOL bRate;
   //=============================================
   //0 : ALL
   //1 : Only Intra Frame
   //Other values 2000 : 2Sec Intra code send
   //=============================================
   int nIntra;
} RTSP_PlayCtrl, *pRTSP_PlayCtrl;

typedef std::map<std::string, CRTSPStreamingChannel*> MapStreamName2StreamingChannel;

//////////////////////////////////////////////////////////////////
//
// Class : CRTSPServerEvent
//
//////////////////////////////////////////////////////////////////

class CRTSPServerEvent {
public:
   LPVOID m_pParam;

public:
   virtual void OnRTSPRequest( LPVOID pParam, const std::string& strStreamName ){};
   virtual LPVOID OnRTSPPlayRequest( LPVOID pParam, const std::string& strStreamName ) { return NULL; };
   virtual void OnStreamingChannelClosed( LPVOID pParam, CRTSPStreamingChannel* pRTSPStreamingChannel ){};
   virtual void OnStreamPlay( LPVOID pParam, CRTSPStreamingChannel* pRTSPStreamingChannel ){};
   virtual void OnStreamPlayCtrl( LPVOID pParam, CRTSPStreamingChannel* pRTSPStreamingChannel, RTSP_PlayCtrl _rtspPlayCtrl ){};
};

//////////////////////////////////////////////////////////////////
//
// Class : CRTSPCustomServer
//
//////////////////////////////////////////////////////////////////

class CRTSPCustomServer : public CRTSPServerEvent {
public:
   ushort m_rtspServerPortNum; // RTSP 서버포트 번호 (554)
   ushort m_rtspServerPortNum_Alternetive; // RTSP 서버포트 대체 번호 (8554)

public:
   CRTSPCustomServer();
   ~CRTSPCustomServer();

   // CRTSPStreamingChannel 관리
   //   서버의 활성화/비활성화 관계없이 스트리밍 채널을 등록/해제할 수 있다.
public:
   void Register( CRTSPStreamingChannel* pRTSPStreamingChannel );
   void Unregister( CRTSPStreamingChannel* pRTSPStreamingChannel );
   BOOL IsExist( CRTSPStreamingChannel* pRTSPStreamingChannel );
   CRTSPStreamingChannel* Find( const std::string& strStreamName );

   // 사용자 관리
   //   사용자 관리용 함수는 Activate 호출 전에 사용해야 한다.
public:
   void AddUserRecord( char const* username, char const* password );
   void removeUserRecord( char const* username );

   // 서버 활성화/비활성화
public:
   void Activate();
   void Deactivate();

   /////////////////////////////////////////////////////////////////////////////
protected:
   CCriticalSection m_csRTSPStreamingChannels;
   MapStreamName2StreamingChannel m_mapRTSPStremingChannels;

protected:
   char eventLoopWatchVariable;
   HANDLE m_hTaskSchedulerEventLoopThread;

   RTSPServer* rtspServer;
   BasicTaskScheduler* scheduler;
   UsageEnvironment* env;
   UserAuthenticationDatabase* authDB;

protected:
   UINT TaskSchedulerEventLoopProc();
   static UINT Thread_TaskSchedulerEventLoop( LPVOID pParam );

   friend class CRTSPStreamingChannel;
};

enum CRTSPStreamingChannelType {
   StreamingChannelLive = 0,
   StreamingChannelPlay = 1
};

//////////////////////////////////////////////////////////////////
//
// Class : CRTSPStreamingChannelEvent
//
//////////////////////////////////////////////////////////////////

class CRTSPStreamingChannelEvent {
public:
   LPVOID m_pParam;

public:
   virtual void OnSetParameter( LPVOID pParam, char const* parameterString ){};
   virtual void OnGetParameter( LPVOID pParam, char const* parameterString ){};
};

//////////////////////////////////////////////////////////////////
//
// Class : CRTSPStreamingChannel
//
//////////////////////////////////////////////////////////////////

class CRTSPStreamingChannel : public CRTSPStreamingChannelEvent {
public:
   std::string m_strStreamName; // 스트림 이름 : rtsp url에 추가되는 스트림 이름
   std::string m_strStreamName_Alias1;
   std::string m_strStreamName_Alias2;

public:
   uint m_nVAMetaDataFlag;
   int m_nCRTSPStreamingChannelType;
   float m_fScale;

protected:
   CRTSPCustomServer* m_pRTSPCustomServer;

public:
   CRTSPStreamingChannel();
   ~CRTSPStreamingChannel();

public:
   BOOL PushVideoData( CRTSPVideoItem* pVideoBuff );
   BOOL PushVAMetaData( CRTSPVAMetaData* pRTSPVAMetaData );
   BOOL IsVideoStreamRequested();
   BOOL IsVAMetaDataRequested( uint nVAMetaDataFlag = 0 );
   void SetVAMetaDataToBeSent( uint nVAMetaDataFlag = 0 );
   CRTSPCustomServer* GetServer() { return m_pRTSPCustomServer; }
   //////////////////////////////////////////////////////////////////
protected:
   BOOL m_bKeyFramePushed;
   uint m_nVideoPushCount;

   CCriticalSection m_csVideoSource;
   std::deque<FramedSource*> m_VideoSourceList;

   CCriticalSection m_csAMetaSource;
   std::deque<FramedSource*> m_MetaSourceList;

protected:
   void AddVideoSource( FramedSource* pVideoSource );
   BOOL RemoveVideoSource( FramedSource* pVideoSource );
   void AddVAMetaSource( FramedSource* pVAMetaSource );
   BOOL RemoveVAMetaSource( FramedSource* pVAMetaSource );
   void OnRemoveAnyLiveSource();
   BOOL IsStreamingChannelReady();
   void UpdateVAMetadataFlag();
   void UnlinkAllLiveSource();

   friend class ByteStreamLiveSource;
   friend class VAMetaDataLiveSource;
   friend class ServerMediaSession;
   friend class CRTSPCustomServer;

   //friend ServerMediaSession* createNewLiveSMS(UsageEnvironment& env, CRTSPCustomServer* custumServer, char const* streamName);
};

#pragma pack()

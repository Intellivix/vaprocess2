#include "stdafx.h"
#include "MJPEGCustomSink.h"

CMJPEGCustomSink::CMJPEGCustomSink( UsageEnvironment& env, unsigned bufferSize, IRTSPClient* pRTSPClient )
    : MediaSink( env )
{
   m_Buffer      = new unsigned char[bufferSize];
   m_nBufferSize = bufferSize;
   m_pRTSPClient = pRTSPClient;
}

CMJPEGCustomSink::~CMJPEGCustomSink( void )
{
   delete[] m_Buffer;
}

CMJPEGCustomSink* CMJPEGCustomSink::createNew( UsageEnvironment& env, unsigned bufferSize, IRTSPClient* pRTSPClient )
{
   return new CMJPEGCustomSink( env, bufferSize, pRTSPClient );
}

Boolean CMJPEGCustomSink::continuePlaying()
{
   if( fSource == 0 || m_nBufferSize == 0 || m_Buffer == 0 ) return False;
   fSource->getNextFrame( m_Buffer, m_nBufferSize, afterGettingFrame, this, onSourceClosure, this );
   return True;
}

void CMJPEGCustomSink::afterGettingFrame1( unsigned frameSize, struct timeval presentationTime )
{
   if( frameSize == 0 ) {
      continuePlaying();
      return;
   }

   unsigned int video_flag = 0;
   video_flag              = LIVE555_VFRM_KEYFRAME;

   if( m_pRTSPClient ) {
      m_pRTSPClient->OnVideoFrameReceived( LIVE555_VCODEC_ID_MJPEG, m_Buffer, frameSize, video_flag, presentationTime );
   }

   continuePlaying();
}

void CMJPEGCustomSink::afterGettingFrame( void* clientData, unsigned frameSize, unsigned numTruncatedBytes, struct timeval presentationTime, unsigned durationInMicroseconds )
{
   CMJPEGCustomSink* sink = (CMJPEGCustomSink*)clientData;
   sink->afterGettingFrame1( frameSize, presentationTime );
}

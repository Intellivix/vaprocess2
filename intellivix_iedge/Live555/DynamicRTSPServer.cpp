/**********
This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation; either version 2.1 of the License, or (at your
option) any later version. (See <http://www.gnu.org/copyleft/lesser.html>.)

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General Public License
along with this library; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
**********/
// Copyright (c) 1996-2015, Live Networks, Inc.  All rights reserved
// A subclass of "RTSPServer" that creates "ServerMediaSession"s on demand,
// based on whether or not the specified stream name exists as a file
// Implementation

#include "stdafx.h"
#include "RTSPCommon.hh"
#include "DynamicRTSPServer.hh"
#include <liveMedia.hh>
#include <algorithm>
#include <string.h>


//-----------------------------------------------------------------------
inline std::string sleft( std::string const& source, size_t const length )
{
   if( length >= source.size() ) {
      return source;
   }
   return source.substr( 0, length );
}

inline std::string sright( std::string const& source, size_t const length )
{
   if( length >= source.size() ) {
      return source;
   }
   return source.substr( source.size() - length );
}
//-----------------------------------------------------------------------

DynamicRTSPServer*
DynamicRTSPServer::createNew( UsageEnvironment& env,
                              CRTSPCustomServer* rtspCustomServer,
                              Port ourPort,
                              UserAuthenticationDatabase* authDatabase,
                              unsigned reclamationTestSeconds )
{
   int ourSocket = setUpOurSocket( env, ourPort );
   if( ourSocket == -1 ) return NULL;

   return new DynamicRTSPServer( env, rtspCustomServer, ourSocket, ourPort, authDatabase, reclamationTestSeconds );
}

DynamicRTSPServer::DynamicRTSPServer( UsageEnvironment& env,
                                      CRTSPCustomServer* rtspCustomServer,
                                      int ourSocket,
                                      Port ourPort,
                                      UserAuthenticationDatabase* authDatabase, unsigned reclamationTestSeconds )
    : RTSPServerSupportingHTTPStreaming( env, ourSocket, ourPort, authDatabase, reclamationTestSeconds )
{
   fRTSPCustomServer = rtspCustomServer;
}

DynamicRTSPServer::~DynamicRTSPServer()
{
}

static ServerMediaSession* createNewLiveSMS( UsageEnvironment& env, CRTSPCustomServer* customServer, char const* streamName ); // jun
static ServerMediaSession* createNewPlaySMS( UsageEnvironment& env, CRTSPCustomServer* customServer, char const* streamName ); // qch1004
static ServerMediaSession* creatSDPSMS( UsageEnvironment& env, CRTSPCustomServer* customServer, char const* streamName ); // qch1004
static ServerMediaSession* createNewSMS( UsageEnvironment& env, char const* fileName, FILE* fid ); // forward

ServerMediaSession* DynamicRTSPServer::lookupServerMediaSession( char const* streamName, Boolean isFirstLookupInSession, void* _pSession )
{
   // jun : 확장자가 없으면 라이브 스트림이라고 본다.
   Boolean liveExists = false;
   Boolean PlayExists = false;
   Boolean fileExists = false;

   //const int MAX_PATH_ = 256;
   //char szUrl[MAX_PATH_] = {0, };

   ServerMediaSession* sms( NULL );
   FILE* fid( NULL );

   //URL Check
   {
      char const* extension = strrchr( streamName, '.' );
      //if (extension == NULL)
      //   liveExists = true;
      if( extension != NULL ) {
         if( strncmp( extension, ".imp", 4 ) == 0 && strstr( streamName, "media" ) != NULL )
            liveExists = true;

         if( strncmp( extension, ".imp", 4 ) == 0 && strstr( streamName, "play" ) != NULL )
            PlayExists = true;
      } else {
         if( strstr( streamName, "chuid" ) != NULL ) liveExists = true;
      }

      // First, check whether the specified "streamName" exists as a local file:
      fid        = fopen( streamName, "rb" );
      fileExists = fid != NULL;
   }

   std::string strStreamName = streamName;
   std::transform( strStreamName.begin(), strStreamName.end(), strStreamName.begin(), ::tolower );

   if( liveExists || PlayExists ) {
      if( std::string::npos == strStreamName.find( "track" ) )
         return NULL;

      //int i(0);
      //while( streamName[i] ) {
      //   if(i==0)sprintf(szUrl,"%c",putchar(tolower(streamName[++i])));
      //   else sprintf(szUrl,"%s%c",szUrl,putchar(tolower(streamName[++i])));
      //}
      //if(strstr(szUrl,"")== NULL)
      //   return NULL;
   }

   std::string strReamName( streamName );
   std::string strTrackName;
   std::string strMeta;
   std::string strParameter;

   std::transform( strReamName.begin(), strReamName.end(), strReamName.begin(), ::tolower );
   size_t nQueryCharPos = strReamName.find( "/track" );
   if( liveExists ) {
      if( nQueryCharPos != std::string::npos ) {
         int nParameterStringLen = strReamName.length() - nQueryCharPos - 1;
         strTrackName            = sright( strReamName, nParameterStringLen );
         strReamName             = sleft( strReamName, nQueryCharPos );
      }
      nQueryCharPos = strReamName.find( '?' );
      if( nQueryCharPos != std::string::npos ) {
         int nParameterStringLen = strReamName.length() - nQueryCharPos - 1;
         strMeta                 = sright( strReamName, nParameterStringLen );
         strReamName             = sleft( strReamName, nQueryCharPos );
      }
      nQueryCharPos = strMeta.find( "metadata" );
      if( nQueryCharPos != std::string::npos ) {
         int nParameterStringLen = strMeta.length() - nQueryCharPos;
         strParameter            = sright( strMeta, nParameterStringLen );
      }
   }

   if( PlayExists ) {
      if( nQueryCharPos != std::string::npos ) {
         int nParameterStringLen = strReamName.length() - nQueryCharPos - 1;
         strTrackName            = sright( strReamName, nParameterStringLen );
         strReamName             = sleft( strReamName, nQueryCharPos );
      }
      nQueryCharPos = strMeta.find( "metadata" );
      if( nQueryCharPos != std::string::npos ) {
         int nParameterStringLen = strMeta.length() - nQueryCharPos;
         strParameter            = sright( strMeta, nParameterStringLen );
      }
   }

   //ServerMediaSession Check
   sms = RTSPServer::lookupServerMediaSession( strReamName.c_str() );
   // Next, check whether we already have a "ServerMediaSession" for this file:
   Boolean smsExists = sms != NULL;

   // Handle the four possibilities for "fileExists" and "smsExists":
   if( !liveExists && !fileExists && !PlayExists ) {
      if( smsExists ) {
         // "sms" was created for a file that no longer exists. Remove it:
         removeServerMediaSession( sms );
         sms = NULL;
      }
      return NULL;
   }

   if( fileExists && smsExists ) {
      removeServerMediaSession( sms );
      sms = NULL;
   }

   ServerMediaSession* pSms = (ServerMediaSession*)_pSession;

   //Live Session
   if( liveExists && isFirstLookupInSession ) {
      // jun : Live용 sms를 생성한다.
      if( fRTSPCustomServer ) {
         // 스트림이름 스트링과 파라메터 스트링을 분리한다.
         //RTSPServer::RTSPClientSession::fOurSessionId;
         if( pSms == NULL ) {
            if( smsExists && sms ) {
               removeServerMediaSession( sms );
               sms = NULL;
            }
            sms = createNewLiveSMS( envir(), fRTSPCustomServer, streamName );
            addServerMediaSession( sms );
         }
      }
   } else if( liveExists && !isFirstLookupInSession ) {
      if( pSms ) {
         if( strTrackName.find( "track1" ) == 0 ) {
            pSms->addSubsession( H264VideoLiveServerMediaSubsession::createNew( envir(), pSms->fRTSPStreamingChannel, streamName, False ) );
         }
         if( strTrackName.find( "track2" ) == 0 ) {
            pSms->addSubsession( VAMetaDataLiveServerMediaSubsession::createNew( envir(), pSms->fRTSPStreamingChannel, strParameter ) );
         }
         sms = pSms;
      }
   }

   //Play Session
   if( PlayExists && isFirstLookupInSession ) {
      if( fRTSPCustomServer ) {
         if( pSms == NULL ) {
            if( smsExists && sms ) {
               removeServerMediaSession( sms );
               sms = NULL;
            }
            sms = createNewPlaySMS( envir(), fRTSPCustomServer, streamName );
            if( sms )
               addServerMediaSession( sms );
         }
      }
   } else if( PlayExists && !isFirstLookupInSession ) {
      if( pSms ) {
         if( strTrackName.find( "track1" ) == 0 ) {
            ServerMediaSubsession* Smsb      = H264VideoLiveServerMediaSubsession::createNew( envir(), pSms->fRTSPStreamingChannel, streamName, False );
            Smsb->fnRTSPStreamingChannelType = pSms->fRTSPStreamingChannel->m_nCRTSPStreamingChannelType;
            pSms->addSubsession( Smsb );
         }
         if( strTrackName.find( "track2" ) == 0 ) {
            ServerMediaSubsession* Smsb      = VAMetaDataLiveServerMediaSubsession::createNew( envir(), pSms->fRTSPStreamingChannel, strParameter );
            Smsb->fnRTSPStreamingChannelType = pSms->fRTSPStreamingChannel->m_nCRTSPStreamingChannelType;
            pSms->addSubsession( Smsb );
         }
         sms = pSms;
      }
   }

   return sms;
}

DynamicRTSPServer::RTSPClientConnection::~RTSPClientConnection()
{
   // Remove ourself from the server's 'client connections' hash table before we go:

   DynamicRTSPServer* fRTSPCustomServer = (DynamicRTSPServer*)&fOurServer;
   fOurServer.fClientConnections->Remove( (char const*)this );

   if( fOurSessionCookie != NULL ) {
      // We were being used for RTSP-over-HTTP tunneling. Also remove ourselves from the 'session cookie' hash table before we go:
      fOurServer.fClientConnectionsForHTTPTunneling->Remove( fOurSessionCookie );
      delete[] fOurSessionCookie;
   }
   closeSockets();
}

void DynamicRTSPServer::RTSPClientSessionEx::handleCmd_GET_PARAMETER( RTSPServer::RTSPClientConnection* ourClientConnection,
                                                                      ServerMediaSubsession* subsession, char const* fullRequestStr )
{
   // By default, we implement "GET_PARAMETER" just as a 'keep alive', and send back an empty response.
   // (If you want to handle "GET_PARAMETER" properly, you can do so by defining a subclass of "RTSPServer"
   // and "RTSPServer::RTSPClientSession", and then reimplement this virtual function in your subclass.)
   //DynamicRTSPServer::RTSPClientConnectionEx _ourClientConnection = ourClientConnection;
   RTSPClientConnectionEx* _ourClientConnection = dynamic_cast<RTSPClientConnectionEx*>( ourClientConnection );
   if( fOurServerMediaSession->fRTSPStreamingChannel && _ourClientConnection ) {
      ( fOurServerMediaSession->fRTSPStreamingChannel->OnGetParameter )( fOurServerMediaSession->fRTSPStreamingChannel->m_pParam, (char const*)_ourClientConnection->fLastCRLF );
   }
   setRTSPResponse( ourClientConnection, "200 OK", fOurSessionId, LIVEMEDIA_LIBRARY_VERSION_STRING );
}

void DynamicRTSPServer::RTSPClientSessionEx::handleCmd_SET_PARAMETER( RTSPServer::RTSPClientConnection* ourClientConnection,
                                                                      ServerMediaSubsession* subsession, char const* fullRequestStr )
{
   // By default, we implement "SET_PARAMETER" just as a 'keep alive', and send back an empty response.
   // (If you want to handle "SET_PARAMETER" properly, you can do so by defining a subclass of "RTSPServer"
   // and "RTSPServer::RTSPClientSession", and then reimplement this virtual function in your subclass.)
   RTSPClientConnectionEx* _ourClientConnection = dynamic_cast<RTSPClientConnectionEx*>( ourClientConnection );
   if( fOurServerMediaSession->fRTSPStreamingChannel && _ourClientConnection ) {
      ( fOurServerMediaSession->fRTSPStreamingChannel->OnSetParameter )( fOurServerMediaSession->fRTSPStreamingChannel->m_pParam, (char const*)_ourClientConnection->fLastCRLF );
   }
   setRTSPResponse( ourClientConnection, "200 OK", fOurSessionId );
}

void DynamicRTSPServer::RTSPClientConnection::handleCmd_DESCRIBE( char const* urlPreSuffix, char const* urlSuffix, char const* fullRequestStr )
{
   ServerMediaSession* session = NULL;
   char* sdpDescription        = NULL;
   char* rtspURL               = NULL;
   do {
      char urlTotalSuffix[RTSP_PARAM_STRING_MAX];
      if( strlen( urlPreSuffix ) + strlen( urlSuffix ) + 2 > sizeof urlTotalSuffix ) {
         handleCmd_bad();
         break;
      }
      urlTotalSuffix[0] = '\0';
      if( urlPreSuffix[0] != '\0' ) {
         strcat( urlTotalSuffix, urlPreSuffix );
         strcat( urlTotalSuffix, "/" );
      }
      strcat( urlTotalSuffix, urlSuffix );

      if( !authenticationOK( "DESCRIBE", urlTotalSuffix, fullRequestStr ) ) break;

      // We should really check that the request contains an "Accept:" #####
      // for "application/sdp", because that's what we're sending back #####

      // Begin by looking up the "ServerMediaSession" object for the specified "urlTotalSuffix":
      session = creatSDPSMS( envir(), NULL, urlTotalSuffix );
      //session = fOurServer.lookupServerMediaSession(urlTotalSuffix);
      if( session == NULL ) {
         handleCmd_notFound();
         break;
      }

      // Then, assemble a SDP description for this session:
      sdpDescription = session->generateSDPDescription();
      if( sdpDescription == NULL ) {
         // This usually means that a file name that was specified for a
         // "ServerMediaSubsession" does not exist.
         setRTSPResponse( "404 File Not Found, Or In Incorrect Format" );
         break;
      }
      unsigned sdpDescriptionSize = strlen( sdpDescription );

      // Also, generate our RTSP URL, for the "Content-Base:" header
      // (which is necessary to ensure that the correct URL gets used in subsequent "SETUP" requests).
      rtspURL = fOurServer.rtspURL( session, fClientInputSocket );

      snprintf( (char*)fResponseBuffer, sizeof fResponseBuffer,
                "RTSP/1.0 200 OK\r\nCSeq: %s\r\n"
                "%s"
                "Content-Base: %s/\r\n"
                "Content-Type: application/sdp\r\n"
                "Content-Length: %d\r\n\r\n"
                "%s",
                fCurrentCSeq,
                dateHeader(),
                rtspURL,
                sdpDescriptionSize,
                sdpDescription );

      if( session ) {
         //fOurServer.fServerMediaSessions->Remove(session->streamName());
         Medium::close( session );
         session = NULL;
      }
   } while( 0 );

   delete[] sdpDescription;
   delete[] rtspURL;
}

// Special code for handling Matroska files:
struct MatroskaDemuxCreationState {
   MatroskaFileServerDemux* demux;
   char watchVariable;
};
static void onMatroskaDemuxCreation( MatroskaFileServerDemux* newDemux, void* clientData )
{
   MatroskaDemuxCreationState* creationState = (MatroskaDemuxCreationState*)clientData;
   creationState->demux                      = newDemux;
   creationState->watchVariable              = 1;
}
// END Special code for handling Matroska files:

#define NEW_SMS( description )                                                 \
   do {                                                                        \
      char const* descStr = description                                        \
          ", streamed by the LIVE555 Media Server";                            \
      sms = ServerMediaSession::createNew( env, fileName, fileName, descStr ); \
   } while( 0 )

#define NEW_LIVE_SMS( description, streamName )                                    \
   do {                                                                            \
      char const* descStr = description                                            \
          ", streamed by the LIVE555 Media Server";                                \
      sms = ServerMediaSession::createNew( env, streamName, streamName, descStr ); \
   } while( 0 )

//#define NEW_LIVE_SMS(description) do {\
//   char const* descStr = description\
//   ", streamed by the LIVE555 Media Server";\
//   sms = ServerMediaSession::createNew(env, streamName, streamName, descStr);\
//} while(0)

#define NEW_PLAY_SMS( description, streamName )                                    \
   do {                                                                            \
      char const* descStr = description                                            \
          ", streamed by the LIVE555 Media Server";                                \
      sms = ServerMediaSession::createNew( env, streamName, streamName, descStr ); \
   } while( 0 )

//#define NEW_PLAY_SMS(description) do {\
//   char const* descStr = description\
//   ", streamed by the LIVE555 Media Server";\
//   sms = ServerMediaSession::createNew(env, streamName, streamName, descStr);\
//} while(0)

static ServerMediaSession* createNewLiveSMS( UsageEnvironment& env, CRTSPCustomServer* custumServer, char const* streamName )
{
   ServerMediaSession* sms   = NULL;
   Boolean const reuseSource = False;
   std::string strStreamName = streamName;
   std::transform( strStreamName.begin(), strStreamName.end(), strStreamName.begin(), ::tolower );
   CRTSPStreamingChannel* pFindRTSPStreamingChannel = NULL;

   // 스트림이름 스트링과 파라메터 스트링을 분리한다.
   std::string strReamName( streamName );
   std::string strRealName;
   size_t nQueryCharPos = strReamName.find( "/track" );
   std::string strTrackName;
   if( nQueryCharPos != std::string::npos ) {
      int nParameterStringLen = strReamName.length() - nQueryCharPos - 1;
      strTrackName            = sright( strReamName, nParameterStringLen );
      strRealName = strReamName = sleft( strReamName, nQueryCharPos );
   }

   nQueryCharPos = strReamName.find( '?' );
   std::string strMeta;
   if( nQueryCharPos != std::string::npos ) {
      int nParameterStringLen = strReamName.length() - nQueryCharPos - 1;
      strMeta                 = sright( strReamName, nParameterStringLen );
      strReamName             = sleft( strReamName, nQueryCharPos );
   }

   custumServer->OnRTSPRequest( custumServer->m_pParam, strReamName );
   CRTSPStreamingChannel* pRTSPStreamingChannel = custumServer->Find( strReamName );

   if( pRTSPStreamingChannel ) {
      pRTSPStreamingChannel->m_nCRTSPStreamingChannelType = StreamingChannelLive;
      //if (pRTSPStreamingChannel->IsStreamingChannelReady (   ))
      {
         // jun : 버퍼의 크기가 충분하지 않으면 영상이 깨질 수 있다.
         // 키프레임 최대 크기를 고려하여 버퍼크기를 설정해야 한다.
         OutPacketBuffer::maxSize = 800000; // allow for some possibly large H.264 frames
         NEW_LIVE_SMS( "H.264 Live Video", strRealName.c_str() );
         sms->fRTSPStreamingChannel = pRTSPStreamingChannel;

         if( strTrackName.find( "track1" ) == 0 ) {
            sms->addSubsession( H264VideoLiveServerMediaSubsession::createNew( env, pRTSPStreamingChannel, strRealName.c_str(), reuseSource ) );
         }

         if( strTrackName.find( "track2" ) == 0 ) {
            sms->addSubsession( VAMetaDataLiveServerMediaSubsession::createNew( env, pRTSPStreamingChannel, strMeta ) );
         }
      }
   }
   return sms;
}

ServerMediaSession* createNewPlaySMS( UsageEnvironment& env, CRTSPCustomServer* customServer, char const* streamName )
{
   ServerMediaSession* sms   = NULL;
   Boolean const reuseSource = False;
   std::string strStreamName = streamName;
   std::transform( strStreamName.begin(), strStreamName.end(), strStreamName.begin(), ::tolower );
   CRTSPStreamingChannel* pRTSPStreamingChannel = NULL;

   // 스트림이름 스트링과 파라메터 스트링을 분리한다.

   std::string strReamName( streamName );
   std::string strRealName;
   int nQueryCharPos = strReamName.find( "/track" );
   std::string strTrackName;
   if( nQueryCharPos != std::string::npos ) {
      int nParameterStringLen = strReamName.length() - nQueryCharPos - 1;
      strTrackName            = sright( strReamName, nParameterStringLen );
      strRealName = strReamName = sleft( strReamName, nQueryCharPos );
   }

   nQueryCharPos = strRealName.find( "metadata" );
   std::string strParameter;
   if( nQueryCharPos != std::string::npos ) {
      int nParameterStringLen = strRealName.length() - nQueryCharPos;
      strParameter            = sright( strRealName, nParameterStringLen );
   }

   pRTSPStreamingChannel = (CRTSPStreamingChannel*)customServer->OnRTSPPlayRequest( customServer->m_pParam, strReamName );

   if( pRTSPStreamingChannel ) {
      pRTSPStreamingChannel->m_nCRTSPStreamingChannelType = StreamingChannelPlay;
      //if (pRTSPStreamingChannel->IsStreamingChannelReady (   ))
      //{
      //jun : 버퍼의 크기가 충분하지 않으면 영상이 깨질 수 있다.
      //키프레임 최대 크기를 고려하여 버퍼크기를 설정해야 한다.
      OutPacketBuffer::maxSize = 800000; // allow for some possibly large H.264 frames
      NEW_PLAY_SMS( "H.264 Play Video", strReamName.c_str() );
      sms->fRTSPStreamingChannel = pRTSPStreamingChannel;
      if( strTrackName.find( "track1" ) == 0 ) {
         ServerMediaSubsession* Smsb      = H264VideoLiveServerMediaSubsession::createNew( env, pRTSPStreamingChannel, strRealName.c_str(), reuseSource );
         Smsb->fnRTSPStreamingChannelType = pRTSPStreamingChannel->m_nCRTSPStreamingChannelType;
         sms->addSubsession( Smsb );
      }

      if( strTrackName.find( "track2" ) == 0 ) {
         ServerMediaSubsession* Smsb      = VAMetaDataLiveServerMediaSubsession::createNew( env, pRTSPStreamingChannel, strParameter );
         Smsb->fnRTSPStreamingChannelType = pRTSPStreamingChannel->m_nCRTSPStreamingChannelType;
         sms->addSubsession( Smsb );
      }
      //}
   }
   return sms;
}

static ServerMediaSession* createNewSMS( UsageEnvironment& env,
                                         char const* fileName, FILE* /*fid*/ )
{
   // Use the file name extension to determine the type of "ServerMediaSession":
   char const* extension = strrchr( fileName, '.' );
   if( extension == NULL ) return NULL;

   ServerMediaSession* sms   = NULL;
   Boolean const reuseSource = False;
   if( strcmp( extension, ".aac" ) == 0 ) {
      // Assumed to be an AAC Audio (ADTS format) file:
      NEW_SMS( "AAC Audio" );
      sms->addSubsession( ADTSAudioFileServerMediaSubsession::createNew( env, fileName, reuseSource ) );
   } else if( strcmp( extension, ".amr" ) == 0 ) {
      // Assumed to be an AMR Audio file:
      NEW_SMS( "AMR Audio" );
      sms->addSubsession( AMRAudioFileServerMediaSubsession::createNew( env, fileName, reuseSource ) );
   } else if( strcmp( extension, ".ac3" ) == 0 ) {
      // Assumed to be an AC-3 Audio file:
      NEW_SMS( "AC-3 Audio" );
      sms->addSubsession( AC3AudioFileServerMediaSubsession::createNew( env, fileName, reuseSource ) );
   } else if( strcmp( extension, ".m4e" ) == 0 ) {
      // Assumed to be a MPEG-4 Video Elementary Stream file:
      NEW_SMS( "MPEG-4 Video" );
      sms->addSubsession( MPEG4VideoFileServerMediaSubsession::createNew( env, fileName, reuseSource ) );
   } else if( strcmp( extension, ".264" ) == 0 ) {
      // Assumed to be a H.264 Video Elementary Stream file:
      NEW_SMS( "H.264 Video" );
      OutPacketBuffer::maxSize = 100000; // allow for some possibly large H.264 frames
      sms->addSubsession( H264VideoFileServerMediaSubsession::createNew( env, fileName, reuseSource ) );
   } else if( strcmp( extension, ".mp3" ) == 0 ) {
      // Assumed to be a MPEG-1 or 2 Audio file:
      NEW_SMS( "MPEG-1 or 2 Audio" );
      // To stream using 'ADUs' rather than raw MP3 frames, uncomment the following:
      //#define STREAM_USING_ADUS 1
      // To also reorder ADUs before streaming, uncomment the following:
      //#define INTERLEAVE_ADUS 1
      // (For more information about ADUs and interleaving,
      //  see <http://www.live555.com/rtp-mp3/>)
      Boolean useADUs            = False;
      Interleaving* interleaving = NULL;
#ifdef STREAM_USING_ADUS
      useADUs = True;
#ifdef INTERLEAVE_ADUS
      unsigned char interleaveCycle[] = { 0, 2, 1, 3 }; // or choose your own...
      unsigned const interleaveCycleSize
          = ( sizeof interleaveCycle ) / ( sizeof( unsigned char ) );
      interleaving = new Interleaving( interleaveCycleSize, interleaveCycle );
#endif
#endif
      sms->addSubsession( MP3AudioFileServerMediaSubsession::createNew( env, fileName, reuseSource, useADUs, interleaving ) );
   } else if( strcmp( extension, ".mpg" ) == 0 ) {
      // Assumed to be a MPEG-1 or 2 Program Stream (audio+video) file:
      NEW_SMS( "MPEG-1 or 2 Program Stream" );
      MPEG1or2FileServerDemux* demux
          = MPEG1or2FileServerDemux::createNew( env, fileName, reuseSource );
      sms->addSubsession( demux->newVideoServerMediaSubsession() );
      sms->addSubsession( demux->newAudioServerMediaSubsession() );
   } else if( strcmp( extension, ".vob" ) == 0 ) {
      // Assumed to be a VOB (MPEG-2 Program Stream, with AC-3 audio) file:
      NEW_SMS( "VOB (MPEG-2 video with AC-3 audio)" );
      MPEG1or2FileServerDemux* demux
          = MPEG1or2FileServerDemux::createNew( env, fileName, reuseSource );
      sms->addSubsession( demux->newVideoServerMediaSubsession() );
      sms->addSubsession( demux->newAC3AudioServerMediaSubsession() );
   } else if( strcmp( extension, ".ts" ) == 0 ) {
      // Assumed to be a MPEG Transport Stream file:
      // Use an index file name that's the same as the TS file name, except with ".tsx":
      unsigned indexFileNameLen = strlen( fileName ) + 2; // allow for trailing "x\0"
      char* indexFileName       = new char[indexFileNameLen];
      sprintf( indexFileName, "%sx", fileName );
      NEW_SMS( "MPEG Transport Stream" );
      sms->addSubsession( MPEG2TransportFileServerMediaSubsession::createNew( env, fileName, indexFileName, reuseSource ) );
      delete[] indexFileName;
   } else if( strcmp( extension, ".wav" ) == 0 ) {
      // Assumed to be a WAV Audio file:
      NEW_SMS( "WAV Audio Stream" );
      // To convert 16-bit PCM data to 8-bit u-law, prior to streaming,
      // change the following to True:
      Boolean convertToULaw = False;
      sms->addSubsession( WAVAudioFileServerMediaSubsession::createNew( env, fileName, reuseSource, convertToULaw ) );
   } else if( strcmp( extension, ".dv" ) == 0 ) {
      // Assumed to be a DV Video file
      // First, make sure that the RTPSinks' buffers will be large enough to handle the huge size of DV frames (as big as 288000).
      OutPacketBuffer::maxSize = 300000;

      NEW_SMS( "DV Video" );
      sms->addSubsession( DVVideoFileServerMediaSubsession::createNew( env, fileName, reuseSource ) );
   } else if( strcmp( extension, ".mkv" ) == 0 || strcmp( extension, ".webm" ) == 0 ) {
      // Assumed to be a Matroska file (note that WebM ('.webm') files are also Matroska files)
      OutPacketBuffer::maxSize = 100000; // allow for some possibly large VP8 or VP9 frames
      NEW_SMS( "Matroska video+audio+(optional)subtitles" );

      // Create a Matroska file server demultiplexor for the specified file.
      // (We enter the event loop to wait for this to complete.)
      MatroskaDemuxCreationState creationState;
      creationState.watchVariable = 0;
      MatroskaFileServerDemux::createNew( env, fileName, onMatroskaDemuxCreation, &creationState );
      env.taskScheduler().doEventLoop( &creationState.watchVariable );

      ServerMediaSubsession* smss;
      while( ( smss = creationState.demux->newServerMediaSubsession() ) != NULL ) {
         sms->addSubsession( smss );
      }
   }

   return sms;
}

ServerMediaSession* creatSDPSMS( UsageEnvironment& env, CRTSPCustomServer* customServer, char const* streamName )
{
   Boolean liveExists    = false;
   Boolean PlayExists    = false;
   Boolean fileExists    = false;
   char const* extension = strrchr( streamName, '.' );
   //if (extension == NULL)
   //   liveExists = true;
   if( extension != NULL ) {
      if( strncmp( extension, ".imp", 4 ) == 0 && strstr( streamName, "media" ) != NULL )
         liveExists = true;

      if( strncmp( extension, ".imp", 4 ) == 0 && strstr( streamName, "play" ) != NULL )
         PlayExists = true;
   } else {
      if( strstr( streamName, "chuid" ) != NULL ) liveExists = true;
   }

   // First, check whether the specified "streamName" exists as a local file:
   FILE* fid  = fopen( streamName, "rb" );
   fileExists = fid != NULL;

   ServerMediaSession* sms   = NULL;
   Boolean const reuseSource = False;
   std::string strParameter;

   if( !( liveExists || PlayExists || fileExists ) ) return NULL;

   if( fileExists ) {
      sms = createNewSMS( env, streamName, fid );
   } else if( liveExists || PlayExists ) {

      std::string strReamName( streamName );

      size_t nQueryCharPos = strReamName.find( "/track" );
      std::string strTrackName;
      if( nQueryCharPos != std::string::npos ) {
         int nParameterStringLen = strReamName.length() - nQueryCharPos - 1;
         strTrackName            = sright( strReamName, nParameterStringLen );
         strReamName             = sleft( strReamName, nQueryCharPos );
      }

      nQueryCharPos = strReamName.find( "metadata" );
      std::string strParameter;
      if( nQueryCharPos != std::string::npos ) {
         int nParameterStringLen = strReamName.length() - nQueryCharPos;
         strParameter            = sright( strReamName, nParameterStringLen );
      }

      // 스트림이름 스트링과 파라메터 스트링을 분리한다.
      //OutPacketBuffer::maxSize = 800000; // allow for some possibly large H.264 frames
      NEW_PLAY_SMS( "H.264 & VAMeta", strReamName.c_str() );
      sms->addSubsession( H264VideoLiveServerMediaSubsession::createNew( env, NULL, streamName, reuseSource ) );
      if( nQueryCharPos >= 0 ) {
         sms->addSubsession( VAMetaDataLiveServerMediaSubsession::createNew( env, NULL, strParameter ) );
      }
   }
   return sms;
}

RTSPServer::RTSPClientConnection*
DynamicRTSPServer::createNewClientConnection( int clientSocket, struct sockaddr_in clientAddr )
{
   return new RTSPClientConnectionEx( *this, clientSocket, clientAddr );
}

RTSPServer::RTSPClientSession*
DynamicRTSPServer::createNewClientSession( u_int32_t sessionId )
{
   return new RTSPClientSessionEx( *this, sessionId );
}

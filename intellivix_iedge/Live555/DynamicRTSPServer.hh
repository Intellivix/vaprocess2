/**********
This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation; either version 2.1 of the License, or (at your
option) any later version. (See <http://www.gnu.org/copyleft/lesser.html>.)

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General Public License
along with this library; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
**********/
// Copyright (c) 1996-2015, Live Networks, Inc.  All rights reserved
// A subclass of "RTSPServer" that creates "ServerMediaSession"s on demand,
// based on whether or not the specified stream name exists as a file
// Header file

#ifndef _DYNAMIC_RTSP_SERVER_HH
#define _DYNAMIC_RTSP_SERVER_HH

#ifndef _RTSP_SERVER_SUPPORTING_HTTP_STREAMING_HH
#include "RTSPServerSupportingHTTPStreaming.hh"
#endif
#include "RTSPCustomServer.h"

class DynamicRTSPClientSession;

class DynamicRTSPServer: public RTSPServerSupportingHTTPStreaming {
public:
   CRTSPCustomServer* fRTSPCustomServer;
public:
  static DynamicRTSPServer* createNew(UsageEnvironment& env, 
                  CRTSPCustomServer* rtspCustomServer,
                  Port ourPort,
				      UserAuthenticationDatabase* authDatabase,
				      unsigned reclamationTestSeconds = 65);

protected:
  DynamicRTSPServer(UsageEnvironment& env, 
          CRTSPCustomServer* rtspCustomServer,
          int ourSocket, Port ourPort,
		    UserAuthenticationDatabase* authDatabase, unsigned reclamationTestSeconds);
  // called only by createNew();
  virtual ~DynamicRTSPServer();

  ////////////////////////////////////////////////////////////////////////////////////////
  class RTSPClientSessionEx : public RTSPServer::RTSPClientSession
  {
     friend class RTSPClientConnectionEx;
  public:     
     RTSPClientSessionEx(RTSPServer& ourServer, u_int32_t sessionId)
        : RTSPServer::RTSPClientSession(ourServer, sessionId)
     {}

     virtual void handleCmd_GET_PARAMETER(RTSPServer::RTSPClientConnection* ourClientConnection,
        ServerMediaSubsession* subsession, char const* fullRequestStr) override;     
     virtual void handleCmd_SET_PARAMETER(RTSPServer::RTSPClientConnection* ourClientConnection,
        ServerMediaSubsession* subsession, char const* fullRequestStr) override;
  };

  class RTSPClientConnectionEx : public RTSPServer::RTSPClientConnection
  {
     friend class RTSPClientSessionEx;
  public:
     RTSPClientConnectionEx(RTSPServer& ourServer, int clientSocket, struct sockaddr_in clientAddr) 
        : RTSPServer::RTSPClientConnection(ourServer, clientSocket, clientAddr)
     {}
  };
  ////////////////////////////////////////////////////////////////////////////////////////

protected: // redefined virtual functions
  virtual ServerMediaSession*
  lookupServerMediaSession(char const* streamName, Boolean isFirstLookupInSession = True, void* _pSms = NULL);

  // If you subclass "RTSPClientConnection", then you must also redefine this virtual function in order
  // to create new objects of your subclass:
  virtual RTSPClientConnection*
     createNewClientConnection(int clientSocket, struct sockaddr_in clientAddr);

  // If you subclass "RTSPClientSession", then you must also redefine this virtual function in order
  // to create new objects of your subclass:
  virtual RTSPClientSession*
     createNewClientSession(u_int32_t sessionId);
};

#endif

#pragma once


//////////////////////////////////////////////////////////////////
//
// Enumerations
//
//////////////////////////////////////////////////////////////////

enum LIVE555_ERROR
{
   LIVE555_ERROR_NONE              = 0,
   LIVE555_ERROR_CONNECTION_FAILED = 1,
   LIVE555_ERROR_LOGIN_FAILED      = 2,
   LIVE555_ERROR_BYE_PACKET        = 3,
};

enum LIVE555_VCODEC_ID 
{
   LIVE555_VCODEC_ID_NONE  = 255,
   LIVE555_VCODEC_ID_MPEG4 =   0,
   LIVE555_VCODEC_ID_MJPEG =   1,
   LIVE555_VCODEC_ID_H264  =   2,
};

enum LIVE555_ACODEC_ID 
{
   LIVE555_ACODEC_ID_NONE  = 255,
   LIVE555_ACODEC_ID_PCMU  = 0,
};

enum LIVE555_VFRM
{
   LIVE555_VFRM_KEYFRAME = 0x00000010,  // Key Frame, 호환성을 위해 변경하지 말 것.
};

enum LIVE555_META_DATA_FLAG
{
   LIVE555_META_EVENT_BEGUN   = 0x00000001,
   LIVE555_META_EVENT_ENDED   = 0x00000002,
   LIVE555_META_OBJECT_BEGUN  = 0x00000004,
   LIVE555_META_OBJECT_ENDED  = 0x00000008,
   LIVE555_META_FRAME_INFO    = 0x00000010,
   LIVE555_META_PLATE_BEGUN   = 0x00000020,
   LIVE555_META_PLATE_ENDED   = 0x00000040,
   LIVE555_META_FACE_BEGUN    = 0x00000080,
   LIVE555_META_FACE_ENDED    = 0x00000100,
   LIVE555_META_PTZ_DATA      = 0x00000200,
   LIVE555_META_PRESET_ID     = 0x00000400,
   LIVE555_META_EVTZONE_INFO  = 0x00000800,
   LIVE555_META_EVTZONE_COUNT = 0x00001000,
   LIVE555_META_PRESET_MAP    = 0x00002000,
   LIVE555_META_SETTING       = 0x00004000,
   LIVE555_META_LOG_INFO      = 0x00008000,
   LIVE555_META_FRAME_OBJ     = 0x00010000,
   LIVE555_META_EVT_FULL_IMG  = 0x00020000,
   LIVE555_META_OBJ_FULL_IMG  = 0X00040000,
   LIVE555_META_EVENT_IPEX	   = 0X00080000,   
   LIVE555_META_CAMERA_INFO   = 0X00100000,
};

const uint LIVE555_META_SEND_WHEN_DATA_CHANGED = LIVE555_META_EVTZONE_INFO | LIVE555_META_PRESET_MAP | LIVE555_META_CAMERA_INFO;


enum RTSPCustomClientCommandType
{
   RTSPCustomClientCommandType_AddCustomClient    = 0,
   RTSPCustomClientCommandType_DeleteCustomClient = 1,
};

//////////////////////////////////////////////////////////////////
//
// Global Functions
//
//////////////////////////////////////////////////////////////////
#include <string>

void InitLive555   (   );
void UninitLive555 (   );
uint ParseMetaDataString(std::string& metadataDescString);



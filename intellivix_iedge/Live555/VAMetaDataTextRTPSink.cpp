/**********
This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation; either version 2.1 of the License, or (at your
option) any later version. (See <http://www.gnu.org/copyleft/lesser.html>.)

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General Public License
along with this library; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
**********/
// "liveMedia"
// Copyright (c) 1996-2015 Live Networks, Inc.  All rights reserved.
// RTP sink for T.140 text (RFC 2793)
// Implementation

#include "stdafx.h"
#include "VAMetaDataTextRTPSink.hh"
#include <GroupsockHelper.hh> // for "gettimeofday()"

////////// VAMetaDataTextRTPSink implementation //////////

VAMetaDataTextRTPSink::VAMetaDataTextRTPSink(UsageEnvironment& env, Groupsock* RTPgs, unsigned char rtpPayloadFormat)
  : VAMetaDataRTPSink(env, RTPgs, rtpPayloadFormat, 10000/*mandatory RTP timestamp frequency for this payload format*/, "VADATA"),
    fOurIdleFilter(NULL), fAreInIdlePeriod(True) {
}

VAMetaDataTextRTPSink::~VAMetaDataTextRTPSink() {
  fSource = fOurIdleFilter; // hack: in case "fSource" had gotten set to NULL before we were called
  stopPlaying(); // call this now, because we won't have our 'idle filter' when the base class destructor calls it later.

  // Close our 'idle filter' as well:
  Medium::close(fOurIdleFilter);
  fSource = NULL; // for the base class destructor, which gets called next
}

VAMetaDataTextRTPSink*
VAMetaDataTextRTPSink::createNew(UsageEnvironment& env, Groupsock* RTPgs,
			    unsigned char rtpPayloadFormat) {
  return new VAMetaDataTextRTPSink(env, RTPgs, rtpPayloadFormat);
}

Boolean VAMetaDataTextRTPSink::continuePlaying() {
  // First, check whether we have an 'idle filter' set up yet. If not, create it now, and insert it in front of our existing source:
  if (fOurIdleFilter == NULL) {
    fOurIdleFilter = new VAMetaDataIdleFilter(envir(), fSource);
  } else {
    fOurIdleFilter->reassignInputSource(fSource);
  }
  fSource = fOurIdleFilter;

  // Then call the parent class's implementation:
  return MultiFramedRTPSink::continuePlaying();
}

static unsigned const rtpHeaderSize = 12;  // qch1004

void VAMetaDataTextRTPSink::doSpecialFrameHandling(unsigned /*fragmentationOffset*/,
					     unsigned char* /*frameStart*/,
					     unsigned numBytesInFrame,
					     struct timeval framePresentationTime,
					     unsigned /*numRemainingBytes*/) {
  // Set the RTP 'M' (marker) bit if we have just ended an idle period - i.e., if we were in an idle period, but just got data:
  // if (fAreInIdlePeriod && numBytesInFrame > 0) setMarkerBit();
  if(fOutBuf->overflowDataSize()==0 && fOutBuf->curPacketSize()>rtpHeaderSize) {  // qch1004
    setMarkerBit();
  }
  fAreInIdlePeriod = numBytesInFrame == 0;

  setTimestamp(framePresentationTime);
}

Boolean VAMetaDataTextRTPSink::frameCanAppearAfterPacketStart(unsigned char const* /*frameStart*/, unsigned /*numBytesInFrame*/) const {
  return False; // We don't concatenate input data; instead, send it out immediately
}


////////// VAMetaDataIdleFilter implementation //////////

VAMetaDataIdleFilter::VAMetaDataIdleFilter(UsageEnvironment& env, FramedSource* inputSource)
  : FramedFilter(env, inputSource),
    fIdleTimerTask(NULL),
    fBufferSize(OutPacketBuffer::maxSize), fNumBufferedBytes(0) {
  fBuffer = new char[fBufferSize];
}

VAMetaDataIdleFilter::~VAMetaDataIdleFilter() {
  envir().taskScheduler().unscheduleDelayedTask(fIdleTimerTask);

  delete[] fBuffer;
  detachInputSource(); // so that the subsequent ~FramedFilter() doesn't delete it
}

#define IDLE_TIMEOUT_MICROSECONDS 300000 /* 300 ms */

void VAMetaDataIdleFilter::doGetNextFrame() {
  // First, see if we have buffered data that we can deliver:
  if (fNumBufferedBytes > 0) {
    deliverFromBuffer();
    return;
  }

  // We don't have any buffered data, so ask our input source for data (unless we've already done so).
  // But also set a timer to expire if this doesn't arrive promptly:
  fIdleTimerTask = envir().taskScheduler().scheduleDelayedTask(IDLE_TIMEOUT_MICROSECONDS, handleIdleTimeout, this);
  if (fInputSource != NULL && !fInputSource->isCurrentlyAwaitingData()) {
    fInputSource->getNextFrame((unsigned char*)fBuffer, fBufferSize, afterGettingFrame, this, onSourceClosure, this);
  }
}

void VAMetaDataIdleFilter::afterGettingFrame(void* clientData, unsigned frameSize,
				       unsigned numTruncatedBytes,
				       struct timeval presentationTime,
				       unsigned durationInMicroseconds) {
  ((VAMetaDataIdleFilter*)clientData)->afterGettingFrame(frameSize, numTruncatedBytes, presentationTime, durationInMicroseconds);
}

void VAMetaDataIdleFilter::afterGettingFrame(unsigned frameSize,
				       unsigned numTruncatedBytes,
				       struct timeval presentationTime,
				       unsigned durationInMicroseconds) {
  // First, cancel any pending idle timer:
  envir().taskScheduler().unscheduleDelayedTask(fIdleTimerTask);

  // Then note the new data that we have in our buffer:
  fNumBufferedBytes = frameSize;
  fBufferedNumTruncatedBytes = numTruncatedBytes;
  fBufferedDataPresentationTime = presentationTime;
  fBufferedDataDurationInMicroseconds = durationInMicroseconds;

  // Then, attempt to deliver this data.  (If we can't deliver it now, we'll do so the next time the reader asks for data.)
  if (isCurrentlyAwaitingData()) (void)deliverFromBuffer();
}

void VAMetaDataIdleFilter::doStopGettingFrames() {
  // Cancel any pending idle timer:
  envir().taskScheduler().unscheduleDelayedTask(fIdleTimerTask);

  // And call the parent's implementation of this virtual function:
  FramedFilter::doStopGettingFrames();
}

void VAMetaDataIdleFilter::handleIdleTimeout(void* clientData) {
  ((VAMetaDataIdleFilter*)clientData)->handleIdleTimeout();
}

void VAMetaDataIdleFilter::handleIdleTimeout() {
  // No data has arrived from the upstream source within our specified 'idle period' (after data was requested from downstream).
  // Send an empty 'idle' frame to our downstream "VAMetaDataTextRTPSink".  (This will cause an empty RTP packet to get sent.)
  deliverEmptyFrame();
}

void VAMetaDataIdleFilter::deliverFromBuffer() {
  if (fNumBufferedBytes <= fMaxSize) { // common case
    fNumTruncatedBytes = fBufferedNumTruncatedBytes;
    fFrameSize = fNumBufferedBytes;
  } else {
    fNumTruncatedBytes = fBufferedNumTruncatedBytes + fNumBufferedBytes - fMaxSize;
    fFrameSize = fMaxSize;
  }  

  memmove(fTo, fBuffer, fFrameSize);
  fPresentationTime = fBufferedDataPresentationTime;
  fDurationInMicroseconds = fBufferedDataDurationInMicroseconds;

  fNumBufferedBytes = 0; // reset buffer
  
  FramedSource::afterGetting(this); // complete delivery
}

void VAMetaDataIdleFilter::deliverEmptyFrame() {
  fFrameSize = fNumTruncatedBytes = 0;
  gettimeofday(&fPresentationTime, NULL);
  FramedSource::afterGetting(this); // complete delivery
}

void VAMetaDataIdleFilter::onSourceClosure(void* clientData) {
  ((VAMetaDataIdleFilter*)clientData)->onSourceClosure();
}

void VAMetaDataIdleFilter::onSourceClosure() {
  envir().taskScheduler().unscheduleDelayedTask(fIdleTimerTask);
  fIdleTimerTask = NULL;

  FramedSource::handleClosure(this);
}

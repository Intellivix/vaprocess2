#include "stdafx.h"
#include <algorithm>
#include "Live555Define.h"

void InitRTSPClientManager (   );
void UninitRTSPClientManager (   );

//////////////////////////////////////////////////////////////////
//
// Global Functions
//
//////////////////////////////////////////////////////////////////

void InitLive555 (   )
{
   InitRTSPClientManager (   );
}

void UninitLive555 (   )
{
   UninitRTSPClientManager (   );
}

uint ParseMetaDataString(std::string& metadataDescString)
{
   uint fMetaDataFlag = 0;

   if (metadataDescString.length ()) 
   {      
      std::transform(metadataDescString.begin(), metadataDescString.end(), metadataDescString.begin(), ::tolower);
      //char *input_str = (LPSTR)(LPCTSTR)metadataDescString;
      char input_str[512];
      int len = metadataDescString.length() > 512 ? 512 : metadataDescString.length();
      _tcsncpy(input_str, metadataDescString.c_str(), 512);
      char *token_metadata_desc;
      char seps_metadata_desc[] = _T("/&");
      token_metadata_desc = _tcstok( input_str, seps_metadata_desc );
      while (token_metadata_desc) {
         char *token_param_value;
         char seps_param_value[] = _T("=");

         int param_type = 0;
         BOOL bParamMode = TRUE;
         token_param_value = _tcstok( (char*)token_metadata_desc, seps_param_value );
         while (token_param_value) {
            if (bParamMode) {
               if (0 == strcmp ("metadata", token_param_value)) {
                  param_type = 1;
               }
               bParamMode = FALSE;
            }
            else {
               if (param_type > 0) {
                  char *token_value;
                  char seps_value[] = _T(",");
                  token_value = _tcstok( (char*)token_param_value, seps_value );
                  while (token_value) {
                     if (0 == strcmp ("cam_setting", token_value)) {
                        fMetaDataFlag |= LIVE555_META_SETTING;
                     }
                     else if (0 == strcmp ("va_event", token_value)) {
                        fMetaDataFlag |= LIVE555_META_EVENT_BEGUN | LIVE555_META_EVENT_ENDED;
                     }
                    // else if (0 == strcmp ("event_ended", token_value)) {
                    //    fMetaDataFlag |= LIVE555_META_EVENT_ENDED;
                    //}
                     else if (0 == strcmp ("object_detection", token_value)) {
                        fMetaDataFlag |= LIVE555_META_OBJECT_BEGUN | LIVE555_META_OBJECT_ENDED;
                     }
                    // else if (0 == strcmp ("object_ended", token_value)) {
                    //    fMetaDataFlag |= LIVE555_META_OBJECT_ENDED;
                    //}
                     else if (0 == strcmp ("frame_info", token_value)) {
                        fMetaDataFlag |= LIVE555_META_FRAME_INFO;
                     }
                     else if (0 == strcmp ("frame_obj", token_value)) {
                        fMetaDataFlag |= LIVE555_META_FRAME_OBJ;
                     }
                     else if (0 == strcmp ("ptz_data", token_value)) {
                        fMetaDataFlag |= LIVE555_META_PTZ_DATA;
                     }
                    // else if (0 == strcmp ("plate_recognition", token_value)) {
                    //    fMetaDataFlag |= LIVE555_META_PLATE_BEGUN | LIVE555_META_PLATE_ENDED;
                    // }
                    // else if (0 == strcmp ("plate_ended", token_value)) {
                    //    fMetaDataFlag |= LIVE555_META_PLATE_ENDED;
                    //}
                     else if (0 == strcmp ("event_zone", token_value)) {
                        fMetaDataFlag |= LIVE555_META_EVTZONE_INFO;
                     }
                     else if (0 == strcmp ("event_zone_count", token_value)) {
                        fMetaDataFlag |= LIVE555_META_EVTZONE_COUNT;
                     }
                     else if (0 == strcmp("camera_info", token_value)) {
                    	   //IntelliVIX Camera configuration information(connect, VA...) Active X Uses
                        fMetaDataFlag |= LIVE555_META_CAMERA_INFO;
                     }
                     token_value = _tcstok( NULL, seps_value );
                  }
                  param_type = 0;
               }
            }
            token_param_value = _tcstok( NULL, seps_param_value );
         }
         token_metadata_desc = _tcstok( NULL, seps_metadata_desc );
      }
   }
   //logd ("fMetaDataFlag:%08x\n",    fMetaDataFlag);
   return fMetaDataFlag;
}


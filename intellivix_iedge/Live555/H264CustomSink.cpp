#include "stdafx.h"
#include "H264CustomSink.h"
#include "H264VideoRTPSource.hh"
#include "bccl_file.h"


BOOL g_bDebugH264Sink = FALSE;

const uint g_nMaxParameterSize         = 8196;
unsigned char const g_H264StartCode[4] = { 0x00, 0x00, 0x00, 0x01 };

CH264CustomSink::CH264CustomSink( UsageEnvironment& env, const char* pSPropParameterSetsStr, unsigned nBufferSize, IRTSPClient* pRTSPClient )
    : MediaSink( env )
{
   m_nWidth = m_nHeight           = 0;
   m_nBufferSize                  = nBufferSize;
   m_pParameterBuff               = new BCCL::FileIO;
   m_pWorkFrameBuff               = new BCCL::FileIO;
   BCCL::FileIO* pWorkFrameBuffer = (BCCL::FileIO*)m_pWorkFrameBuff;
   pWorkFrameBuffer->SetLength( m_nBufferSize );
   m_bInitParam                   = False;
   m_bInitStartStreamFrontRawData = False;
   m_bGetSPSData                  = False;
   m_fSPropParameterSetsStr       = pSPropParameterSetsStr;
   m_pRTSPClient                  = pRTSPClient;
   m_pFrameBuffer                 = NULL;
   m_nFrameSize                   = 0;
   m_addCnt                       = 0;
}

CH264CustomSink::~CH264CustomSink( void )
{
   BCCL::FileIO* pWorkFrameBuffer = (BCCL::FileIO*)m_pWorkFrameBuff;
   pWorkFrameBuffer->Close();
   delete pWorkFrameBuffer;
   BCCL::FileIO* pParameterBuffer = (BCCL::FileIO*)m_pParameterBuff;
   pParameterBuffer->Close();
   delete pParameterBuffer;
}

CH264CustomSink* CH264CustomSink::createNew( UsageEnvironment& env, const char* pfSPropParameterSetsStr, unsigned bufferSize, IRTSPClient* pRTSPClient )
{
   return new CH264CustomSink( env, pfSPropParameterSetsStr, bufferSize, pRTSPClient );
}

Boolean CH264CustomSink::continuePlaying()
{
   if( fSource == NULL ) return False;
   BCCL::FileIO* pWorkFrameBuffer = (BCCL::FileIO*)m_pWorkFrameBuff;
   byte* pBuff                    = pWorkFrameBuffer->GetBuffer();
   pBuff += g_nMaxParameterSize;
   int nBuffSize = m_nBufferSize - g_nMaxParameterSize;
   fSource->getNextFrame( pBuff, nBuffSize, afterGettingFrame, this, onSourceClosure, this );
   return True;
}
void CH264CustomSink::afterGettingFrame( void* pClientData, unsigned nFrameSize, unsigned nNumTruncatedBytes, struct timeval presentationTime, unsigned nDurationInMicroseconds )
{
   CH264CustomSink* sink = (CH264CustomSink*)pClientData;
   sink->afterGettingFrame( nFrameSize, presentationTime );
}

void GetVideoSizeFromSPSData( BYTE* pSPSData, uint nSPSDataLen, int& nWidth, int& nHeight );

void CH264CustomSink::afterGettingFrame( unsigned nFrameSize, struct timeval presentationTime )
{
   // RFC 6184 에 나와있는 "Network Abstraction Layer Unit Types" 내용을 인용함. (jun)
   //   +---------------+
   //   |0|1|2|3|4|5|6|7|
   //   +-+-+-+-+-+-+-+-+
   //   |F|NRI|  Type   |
   //   +---------------+

   // F    (1 bit): forbidden_zero_bit.  The H.264 specification declares a value of 1 as a syntax violation.
   // NRI  (2 bit): nal_ref_idc.  A value of 00 indicates that the content of the NAL unit is not used to reconstruct reference pictures
   //               for inter picture prediction.  Such NAL units can be discarded without risking the integrity of the reference pictures.
   //               Values greater than 00 indicate that the decoding of the NAL unit is required to maintain the integrity of the reference pictures.
   // Type (3 bit): nal_unit_type.   This component specifies the NAL unit payload type

   // nal_unit_type
   // ITU-T H.264 Advanced video coding for generic audiovisual services 문서 참조함. (jun)

   //  0: Unspecified
   //  1: Coded slice of a non-IDR picture
   //  2: Coded slice data partition A
   //  3: Coded slice data partition B
   //  4: Coded slice data partition C
   //  5: Coded slice of an IDR picture
   //  6: Supplemental enhancement information (SEI)
   //  7: Sequence parameter set
   //  8: Picture parameter set
   //  9: Access unit delimiter
   // 10: End of sequence
   // 11: End of stream
   // 12: Filler data
   // 13: Sequence parameter set extension
   // 14: Prefix NAL unit
   // 15: Subset sequence parameter set
   // 16~18: Reserved
   // 19: Coded slice of an auxiliary coded picture without partitioning
   // 20: Coded slice extension
   // 21~23: Reserved
   // 24~31: Unspecified

   enum H264_NAL_UNIT_TYPE {
      NAL_UNIT_TYPE_NON_IDR_SLICE = 1, // P-Frame
      NAL_UNIT_TYPE_DP_A_SLICE    = 2,
      NAL_UNIT_TYPE_DP_B_SLICE    = 3,
      NAL_UNIT_TYPE_DP_C_SLICE    = 4,
      NAL_UNIT_TYPE_IDR_SLICE     = 5, // I-Frame
      NAL_UNIT_TYPE_SEI           = 6,
      NAL_UNIT_TYPE_SEQ_PARAM     = 7,
      NAL_UNIT_TYPE_PIC_PARAM     = 8,
      NAL_UNIT_TYPE_ACCESS_UNIT   = 9,
      NAL_UNIT_TYPE_END_OF_SEQ    = 10,
      NAL_UNIT_TYPE_END_OF_STREAM = 11,
      NAL_UNIT_TYPE_FILLER_DATA   = 12,
      NAL_UNIT_TYPE_SEQ_EXTENSION = 13,
   };

   //BYTE& nNALHeader = m_Buffer[0];
   BCCL::FileIO* pParameterBuffer = (BCCL::FileIO*)m_pParameterBuff;
   BCCL::FileIO* pWorkFrameBuffer = (BCCL::FileIO*)m_pWorkFrameBuff;
   byte* pFrameBuffer             = pWorkFrameBuffer->GetBuffer();
   pFrameBuffer += g_nMaxParameterSize;
   BYTE& nNALHeader = pFrameBuffer[0];

   int i;

   if( g_bDebugH264Sink ) {
      // NAL Header를 이진수로 출력하기
      for( i = 7; i >= 0; i-- ) {
         BYTE nFlag = 1 << i;
         if( nFlag & nNALHeader )
            logd( "1" );
         else
            logd( "0" );
      }
      for( i = 7; i >= 0; i-- ) {
         BYTE nFlag = 1 << i;
         if( nFlag & pFrameBuffer[1] )
            logd( "1" );
         else
            logd( "0" );
      }
      logd( "  %6d\n", nFrameSize );
   }

   BYTE forbidden_zero_bit = ( nNALHeader & 0x80 ) >> 7;
   BYTE nal_ref_idc        = ( nNALHeader & 0x60 ) >> 5;
   BYTE nal_unit_type      = ( nNALHeader & 0x1F );

   if( 0 )
      if( 0 == nal_ref_idc ) {
         continuePlaying();
         return;
      }
   // 처리할 데이터가 없으면 그냥 넘김.
   if( nFrameSize == 0 ) {
      continuePlaying();
      return;
   }
   // syntax violation 이면 그냥 넘김.
   if( forbidden_zero_bit == 1 ) {
      continuePlaying();
      return;
   }

   int nWidth, nHeight;
   nWidth = nHeight = 0;

   // SPS 정보를 추가한다.  최초 연결 시 미리 LIVE555에서 얻은 정보이다.
   // 매 키프레임 시에도 SPS,PPS정보가 얻어질 수 있는데 나중에 얻어진 정보를 보다 우선 시 한다.
   if( False == m_bInitParam ) {
      unsigned numSPropRecords  = 0;
      SPropRecord* sPropRecords = parseSPropParameterSets( m_fSPropParameterSetsStr, numSPropRecords );
      if( sPropRecords ) {
         pParameterBuffer->SetLength( 0 );
         for( unsigned i = 0; i < numSPropRecords; ++i ) {
            pParameterBuffer->Write( (byte*)g_H264StartCode, 1, 4 );
            pParameterBuffer->Write( sPropRecords[i].sPropBytes, 1, sPropRecords[i].sPropLength );
            if( i == 0 ) {
               GetVideoSizeFromSPSData( sPropRecords[i].sPropBytes, sPropRecords[i].sPropLength, nWidth, nHeight );
               if( m_pRTSPClient ) {
                  if( m_nWidth != nWidth || m_nHeight != nHeight ) {
                     m_nWidth  = nWidth;
                     m_nHeight = nHeight;
                     m_pRTSPClient->OnVideoSizeChanged( m_nWidth, m_nHeight );
                  }
               }
            }
         }
         pParameterBuffer->Write( (byte*)g_H264StartCode, 1, 4 ); // 프레임 데이터가 시작되기 전에는 "시작코드"를 삽입한다.
         delete[] sPropRecords;
         m_bInitParam = True;
      }
   }

   uint nVideoFlag = 0;

   switch( nal_unit_type ) {
   case NAL_UNIT_TYPE_ACCESS_UNIT:
      continuePlaying();
      return;
   case NAL_UNIT_TYPE_SEI:
      continuePlaying();
      return;
   case NAL_UNIT_TYPE_SEQ_PARAM:
      if( g_nMaxParameterSize < nFrameSize + sizeof( g_H264StartCode ) ) {
         logd( "LIVE555 SEQ_PARAM data size is too Big!!!\n" );
         pParameterBuffer->SetLength( 0 );
         continuePlaying();
         return;
      }
      // 파라메터 정보는 키프레임 인 경우에만 수신되며 SEQ 와 PIC PARAM 데이터가 순서대로 온다.
      GetVideoSizeFromSPSData( pFrameBuffer, nFrameSize, nWidth, nHeight );
      if( m_pRTSPClient ) {
         if( m_nWidth != nWidth || m_nHeight != nHeight ) {
            m_nWidth  = nWidth;
            m_nHeight = nHeight;
            m_pRTSPClient->OnVideoSizeChanged( m_nWidth, m_nHeight );
         }
      }
      // SEQ_PARAM 데이터를 버퍼에 Write 해두기
      pParameterBuffer->SetLength( 0 );
      pParameterBuffer->Write( (byte*)g_H264StartCode, 1, 4 );
      pParameterBuffer->Write( pFrameBuffer, 1, nFrameSize );
      //if (g_bDebugH264Sink) logd ("NAL_UNIT_TYPE_SEQ_PARAM : size(%d)\n", nFrameSize);
      continuePlaying();
      m_bGetSPSData = TRUE;
      return;
   case NAL_UNIT_TYPE_PIC_PARAM:
      // 예외처리
      //if (!m_bGetSPSData) logd ("LIVE555 NAL_UNIT_TYPE_PIC_PARAM - SEQ_PARAM is Not Received!!!\n");
      m_bGetSPSData = FALSE;
      if( g_nMaxParameterSize < nFrameSize + pParameterBuffer->GetLength() + sizeof( g_H264StartCode ) ) {
         logd( "LIVE555 PIC_PARAM data size is too Big!!!\n" );
         pParameterBuffer->SetLength( 0 );
         continuePlaying();
         return;
      }
      // PIC_PARAM 데이터를 버퍼에 Write 해두기
      pParameterBuffer->Write( (byte*)g_H264StartCode, 1, 4 );
      pParameterBuffer->Write( pFrameBuffer, 1, nFrameSize );
      pParameterBuffer->Write( (byte*)g_H264StartCode, 1, 4 ); // 프레임 데이터가 시작되기 전에는 "시작코드"를 삽입한다.
      //if (g_bDebugH264Sink) logd ("LIVE555 NAL_UNIT_TYPE_PIC_PARAM : size(%d)\n", nFrameSize);
      continuePlaying();
      m_bInitParam = True;
      return;
   case NAL_UNIT_TYPE_IDR_SLICE:
      nVideoFlag = LIVE555_VFRM_KEYFRAME;
      //if (g_bDebugH264Sink) logd ("LIVE555_VFRM_KEYFRAME\n");
      break;
   }

   m_bGetSPSData = FALSE;

   int nParameterBuffLen = pParameterBuffer->GetLength();
   byte* pParameterBuff  = pParameterBuffer->GetBuffer();

   if( nParameterBuffLen > g_nMaxParameterSize ) {
      logd( "//////////////////////PBN:%d ///////////////\n", nParameterBuffLen );
      pParameterBuffer->SetLength( 0 );
      continuePlaying();
      return;
   }

   byte* pTemp( NULL );
   unsigned int uTempSize( 0 );
   if( LIVE555_VFRM_KEYFRAME & nVideoFlag && m_addCnt == 0 ) {
      byte* pParamAndFrameBuffer = pFrameBuffer - nParameterBuffLen;
      memcpy( pParamAndFrameBuffer, pParameterBuff, nParameterBuffLen );
      int nParamAndFrameBuffSize = nParameterBuffLen + nFrameSize;
      pTemp                      = new byte[nParamAndFrameBuffSize];
      memcpy( pTemp, pParamAndFrameBuffer, nParamAndFrameBuffSize );
      uTempSize = nParamAndFrameBuffSize;
   } else {
      pFrameBuffer = pFrameBuffer - sizeof( g_H264StartCode );
      memcpy( pFrameBuffer, g_H264StartCode, sizeof( g_H264StartCode ) );
      uTempSize = nFrameSize + sizeof( g_H264StartCode );
      pTemp     = new byte[uTempSize];
      memcpy( pTemp, pFrameBuffer, uTempSize );
   }

   m_addCnt++;

   if( m_pFrameBuffer == NULL ) {
      m_pFrameBuffer = new byte[uTempSize];
      memcpy( m_pFrameBuffer, pTemp, uTempSize );
      m_nFrameSize = uTempSize;
   } else {
      byte* pswp     = m_pFrameBuffer;
      m_pFrameBuffer = new byte[m_nFrameSize + uTempSize];
      memcpy( m_pFrameBuffer, pswp, m_nFrameSize );
      memcpy( m_pFrameBuffer + m_nFrameSize, pTemp, uTempSize );
      m_nFrameSize += uTempSize;
      if( pswp ) {
         delete pswp;
         pswp = NULL;
      }
   }

   if( pTemp ) {
      delete pTemp;
      pTemp = NULL;
   }

   if( fSource->bEndMarkFrame ) {
#ifdef DEBUG
      char szMsg[1024];
      sprintf( szMsg,
               _T("Sinker Packet Size : %u\n")
               _T("Frame : %u\n")
               _T("Frame add Cnt: %d\n")
               _T("Parsed 4-byte : %02x %02x %02x %02x    %02x %02x %02x\n"),
               fSource->fFrameTotSize, m_nFrameSize, m_addCnt,
               m_pFrameBuffer[0], m_pFrameBuffer[1], m_pFrameBuffer[2], m_pFrameBuffer[3], m_pFrameBuffer[4], m_pFrameBuffer[5], m_pFrameBuffer[6] );
//TRACE(szMsg);
#endif
      if( m_pRTSPClient ) {
         m_pRTSPClient->OnVideoFrameReceived( LIVE555_VCODEC_ID_H264, m_pFrameBuffer, m_nFrameSize, nVideoFlag, presentationTime );
      }
      if( m_pFrameBuffer ) {
         delete m_pFrameBuffer;
         m_pFrameBuffer = NULL;
      }
      fSource->fFrameTotSize = 0;
      m_addCnt               = 0;
      m_nFrameSize           = 0;
   }

   continuePlaying();
}

#include "BitVector.hh"

void removeEmulationBytes( BYTE* nalUnitOrig, uint numBytesInNALunit, BYTE* nalUnitCopy, uint maxSize, uint& nalUnitCopySize )
{
   ;
   nalUnitCopySize = 0;
   if( numBytesInNALunit > maxSize ) return;
   for( uint i = 0; i < numBytesInNALunit; ++i ) {
      if( i + 2 < numBytesInNALunit && nalUnitOrig[i] == 0 && nalUnitOrig[i + 1] == 0 && nalUnitOrig[i + 2] == 3 ) {
         nalUnitCopy[nalUnitCopySize++] = nalUnitOrig[i++];
         nalUnitCopy[nalUnitCopySize++] = nalUnitOrig[i++];
      } else {
         nalUnitCopy[nalUnitCopySize++] = nalUnitOrig[i];
      }
   }
}

#define SPS_MAX_SIZE 1000

void GetVideoSizeFromSPSData( BYTE* pSPSData, uint nSPSDataLen, int& nWidth, int& nHeight )
{
   // 본 함수는 H264VideoStreamFramer.cpp에 구현되어 있는
   // H264VideoStreamParser::analyze_seq_parameter_set_data 함수를 참조하여 구현하였다.
   // SPS정보 중에 영상의 너비,높이 정보를 얻는데 사용한다.

   uint num_units_in_tick;
   uint time_scale;
   uint fixed_frame_rate_flag;
   uint separate_colour_plane_flag;
   num_units_in_tick = time_scale = fixed_frame_rate_flag = 0; // default values

   // Begin by making a copy of the NAL unit data, removing any 'emulation prevention' bytes:
   BYTE sps[SPS_MAX_SIZE];
   uint spsSize;
   removeEmulationBytes( pSPSData, nSPSDataLen, sps, sizeof sps, spsSize );

   BitVector bv( sps, 0, 8 * spsSize );

   bv.skipBits( 8 ); // forbidden_zero_bit; nal_ref_idc; nal_unit_type
   uint profile_idc          = bv.getBits( 8 );
   uint constraint_setN_flag = bv.getBits( 8 ); // also "reserved_zero_2bits" at end
   uint level_idc            = bv.getBits( 8 );
   uint seq_parameter_set_id = bv.get_expGolomb();
   if( profile_idc == 100 || profile_idc == 110 || profile_idc == 122 || profile_idc == 244 || profile_idc == 44 || profile_idc == 83 || profile_idc == 86 || profile_idc == 118 || profile_idc == 128 ) {
      uint chroma_format_idc = bv.get_expGolomb();
      if( chroma_format_idc == 3 ) {
         separate_colour_plane_flag = bv.get1BitBoolean();
      }
      (void)bv.get_expGolomb(); // bit_depth_luma_minus8
      (void)bv.get_expGolomb(); // bit_depth_chroma_minus8
      bv.skipBits( 1 ); // qpprime_y_zero_transform_bypass_flag
      uint seq_scaling_matrix_present_flag = bv.get1Bit();
      if( seq_scaling_matrix_present_flag ) {
         for( int i = 0; i < ( ( chroma_format_idc != 3 ) ? 8 : 12 ); ++i ) {
            uint seq_scaling_list_present_flag = bv.get1Bit();
            if( seq_scaling_list_present_flag ) {
               uint sizeOfScalingList = i < 6 ? 16 : 64;
               uint lastScale         = 8;
               uint nextScale         = 8;
               for( uint j = 0; j < sizeOfScalingList; ++j ) {
                  if( nextScale != 0 ) {
                     uint delta_scale = bv.get_expGolomb();
                     nextScale        = ( lastScale + delta_scale + 256 ) % 256;
                  }
                  lastScale = ( nextScale == 0 ) ? lastScale : nextScale;
               }
            }
         }
      }
   }
   uint log2_max_frame_num_minus4 = bv.get_expGolomb();
   uint log2_max_frame_num        = log2_max_frame_num_minus4 + 4;
   uint pic_order_cnt_type        = bv.get_expGolomb();
   if( pic_order_cnt_type == 0 ) {
      uint log2_max_pic_order_cnt_lsb_minus4 = bv.get_expGolomb();
   } else if( pic_order_cnt_type == 1 ) {
      bv.skipBits( 1 ); // delta_pic_order_always_zero_flag
      (void)bv.get_expGolomb(); // offset_for_non_ref_pic
      (void)bv.get_expGolomb(); // offset_for_top_to_bottom_field
      uint num_ref_frames_in_pic_order_cnt_cycle = bv.get_expGolomb();
      for( uint i = 0; i < num_ref_frames_in_pic_order_cnt_cycle; ++i ) {
         (void)bv.get_expGolomb(); // offset_for_ref_frame[i]
      }
   }

   uint max_num_ref_frames                   = bv.get_expGolomb();
   uint gaps_in_frame_num_value_allowed_flag = bv.get1Bit();
   uint pic_width_in_mbs_minus1              = bv.get_expGolomb();
   uint pic_height_in_map_units_minus1       = bv.get_expGolomb();

   nWidth  = ( pic_width_in_mbs_minus1 + 1 ) * 16;
   nHeight = ( pic_height_in_map_units_minus1 + 1 ) * 16;

   uint frame_mbs_only_flag = bv.get1BitBoolean();
   if( !frame_mbs_only_flag ) {
      bv.skipBits( 1 ); // mb_adaptive_frame_field_flag
   }
   bv.skipBits( 1 ); // direct_8x8_inference_flag
   uint frame_cropping_flag = bv.get1Bit();
   if( frame_cropping_flag ) {
      uint frame_crop_left_offset   = bv.get_expGolomb();
      uint frame_crop_right_offset  = bv.get_expGolomb();
      uint frame_crop_top_offset    = bv.get_expGolomb();
      uint frame_crop_bottom_offset = bv.get_expGolomb();

      nWidth -= ( ( frame_crop_left_offset + frame_crop_right_offset ) * 2 );
      nHeight -= ( ( frame_crop_top_offset + frame_crop_bottom_offset ) * 2 * ( 2 - frame_mbs_only_flag ) );
   }
   //uint vui_parameters_present_flag = bv.get1Bit();
   //if (vui_parameters_present_flag) {
   //   analyze_vui_parameters(bv,num_units_in_tick, time_scale, fixed_frame_rate_flag);
   //}
}

﻿/**********
This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation; either version 2.1 of the License, or (at your
option) any later version. (See <http://www.gnu.org/copyleft/lesser.html>.)

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General Public License
along with this library; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
**********/
// "liveMedia"
// Copyright (c) 1996-2015 Live Networks, Inc.  All rights reserved.
// A file source that is a plain byte stream (rather than frames)
// Implementation

#include "stdafx.h"
#include "Live555Define.h"
#include "VAMetaDataLiveSource.hh"
#include "GroupsockHelper.hh"


////////// VAMetaDataLiveSource //////////
int nMetaStreamSourceCount = 0;

VAMetaDataLiveSource*
VAMetaDataLiveSource::createNew( UsageEnvironment& env,
                                 CRTSPStreamingChannel* streamingChannel, std::string& strParameter )
{
   // jun : File -> Live 로 바꾼부분이다.
   VAMetaDataLiveSource* newSource
       = new VAMetaDataLiveSource( env, streamingChannel, strParameter );

   return newSource;
}

VAMetaDataLiveSource::VAMetaDataLiveSource( UsageEnvironment& env,
                                            CRTSPStreamingChannel* streamingChannel, std::string& strParameter )
    : FramedSource( env )
    , fMetaDataFlag_ToBeSent( 0 )
    , fStreamingChannel( NULL )
{
   fMetaDataFlag = ParseMetaDataString( strParameter );
   if( streamingChannel ) {
      fMetaDataFlag_ToBeSent |= LIVE555_META_SEND_WHEN_DATA_CHANGED & fMetaDataFlag; // 최초 접속 시에 데이터를 보내기 위해
      nMetaStreamSourceCount++;
      fStreamingChannel = streamingChannel; // jun: CRTSPStreamingChannel 스트리밍 채널 객체 포인터 복사
      fFrameRate        = 1000.0f;
      fStreamingChannel->AddVAMetaSource( this );
   }
}

VAMetaDataLiveSource::~VAMetaDataLiveSource()
{
   if( fStreamingChannel ) {
      nMetaStreamSourceCount--;
      fStreamingChannel->RemoveVAMetaSource( this );

      if( nMetaStreamSourceCount == 0 ) {
         if( !fStreamingChannel->IsVideoStreamRequested() && !fStreamingChannel->IsVAMetaDataRequested() ) {
            if( fStreamingChannel->GetServer() )
               fStreamingChannel->GetServer()->OnStreamingChannelClosed( fStreamingChannel->GetServer()->m_pParam, fStreamingChannel );
            //logd( "VAMetaDataLiveSource OnStreamingChannelClosed \n" );
         }
      }
   }
}

void VAMetaDataLiveSource::doGetNextFrame()
{
   doReadFromQueue();
}

void VAMetaDataLiveSource::doStopGettingFrames()
{
   envir().taskScheduler().unscheduleDelayedTask( nextTask() );
}

void VAMetaDataLiveSource::doReadFromQueue()
{
   int addFrameSize = 0;
   fVAMetadataQueue.Lock();
   while( fVAMetadataQueue.Size() ) {
      CRTSPVAMetaData* pRTSPVAMetaData = fVAMetadataQueue.Front();
      if( pRTSPVAMetaData ) {
         int nCompBuffLength = pRTSPVAMetaData->GetLength();
         memcpy( fTo + addFrameSize, pRTSPVAMetaData->GetBuffer(), nCompBuffLength );
         addFrameSize += nCompBuffLength;
         fFrameRate        = pRTSPVAMetaData->m_fFrameRate;
         fPresentationTime = pRTSPVAMetaData->m_PresentationTime;
         fVAMetadataQueue.Pop();
         if( LIVE555_META_SEND_WHEN_DATA_CHANGED & pRTSPVAMetaData->m_VAMetaDataFlag ) {
            fMetaDataFlag_ToBeSent &= ~( LIVE555_META_SEND_WHEN_DATA_CHANGED & pRTSPVAMetaData->m_VAMetaDataFlag );
            //pRTSPVAMetaData->m_VAMetaDataFlag &= ~(LIVE555_META_SEND_WHEN_DATA_CHANGED & pRTSPVAMetaData->m_VAMetaDataFlag);
         }
         delete pRTSPVAMetaData;
      }
   }

   fVAMetadataQueue.Unlock();
   fFrameSize = addFrameSize;

   if( fFrameSize ) {
   }
   // To avoid possible infinite recursion, we need to return to the event loop to do this:
   int uSecsToDelay = int( 1000000 / ( fFrameRate ) );
   nextTask()       = envir().taskScheduler().scheduleDelayedTask( uSecsToDelay, (TaskFunc*)FramedSource::afterGetting, this );
}

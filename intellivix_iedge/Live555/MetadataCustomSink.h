#pragma once
#include "MediaSink.hh"
#include "RTSPCustomClient.h"

class CMetadataCustomSink : public MediaSink
{
public:
   void*             m_pWorkFrameBuff;               // 디코딩에 사용되는 버퍼. 파라메터 및 비디오프레임 정보를 조립하는데 사용한다.
   Boolean           m_bInitParam;
   Boolean           m_bInitStartStreamFrontRawData; // buffer에서, rawdata 앞에 start code를 삽입되었는지 식별.
   unsigned          m_nBufferSize;
   const char*       m_fSPropParameterSetsStr;       // parameter set
   IRTSPClient*      m_pRTSPClient;
   MediaSubsession*  m_pSubsession;

public:
   
public:
   static CMetadataCustomSink* createNew(UsageEnvironment& env, MediaSubsession* subsession, IRTSPClient* pRTSPClient = NULL);

protected:
   CMetadataCustomSink(UsageEnvironment& env, MediaSubsession* subsession, IRTSPClient* pRTSPClient = NULL);
   virtual ~CMetadataCustomSink(void);

protected:
   virtual Boolean continuePlaying    (   );
   static  void    afterGettingFrame  (void* pClientData, unsigned nFrameSize, unsigned nNumTruncatedBytes, struct timeval presentationTime, unsigned nDurationInMicroseconds);
   virtual void    afterGettingFrame  (unsigned nFrameSize, struct timeval presentationTime);
};


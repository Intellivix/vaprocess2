﻿/**********
This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation; either version 2.1 of the License, or (at your
option) any later version. (See <http://www.gnu.org/copyleft/lesser.html>.)

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General Public License
along with this library; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
**********/
// "liveMedia"
// Copyright (c) 1996-2015 Live Networks, Inc.  All rights reserved.
// A file source that is a plain byte stream (rather than frames)
// C++ header

#ifndef _TEXT_STREAM_LIVE_SOURCE_HH
#define _TEXT_STREAM_LIVE_SOURCE_HH

#ifndef _FRAMED_FILE_SOURCE_HH
#include "FramedSource.hh"
#endif
#include "RTSPCustomServer.h"

class VAMetaDataLiveSource: public FramedSource {
public:
  static VAMetaDataLiveSource* createNew(UsageEnvironment& env, 
                CRTSPStreamingChannel* streamingChannel, std::string& strParameter);

protected:
  VAMetaDataLiveSource(UsageEnvironment& env,
             CRTSPStreamingChannel* streamingChannel, std::string& strParameter);
	// called only by createNew()

  virtual ~VAMetaDataLiveSource();

  void doReadFromQueue();

private:
  // redefined virtual functions:
  virtual void doGetNextFrame();
  virtual void doStopGettingFrames();

public:
   uint                   fMetaDataFlag;           // 요청 상태의 메타데이터 플레그
   uint                   fMetaDataFlag_ToBeSent;  // 메타데이터를 보내야 하는 상태를 저장하는 플레그
   float                  fFrameRate;
   CRTSPVAMetadataQueue   fVAMetadataQueue;
   CRTSPStreamingChannel* fStreamingChannel;   
};

#endif

﻿#include "stdafx.h"
#include <stdio.h>
//#include <conio.h>
#include <deque>
#include "liveMedia.hh"
#include "BasicUsageEnvironment.hh"
#include "H264CustomSink.h"
#include "MPEG4ESCustomVideoRTPSink.h"
#include "MJPEGCustomSink.h"
#include "AudioCustomSink.h"
#include "MetadataCustomSink.h"
#include "bccl_console.h"
#include "RTSPCustomClient.h"
#include "GroupsockHelper.hh"
#include "Base64.hh"

// Live555 라이브러리에서 제공하는 샘플코드인 testRTSPClient.cpp 의 구현내용을 토대로 구현한 내용입니다. (jun)
// 추후 디버깅을 위해 testRTSPClient.cpp의 코드를 될 수 있는한 수정하지 않았습니다.

#pragma pack( 8 )

//////////////////////////////////////////////////////////////////
//
// Defines & Global Variables
//
//////////////////////////////////////////////////////////////////
BOOL g_bDebugLive555          = FALSE; // 콘솔창에 디버그 용 텍스트 출력을 할지의 여부
int g_nVerbosityLevel         = 0; // 디버깅용 콘솔 출력 내용을 조절함. 0:최소, 1:약간, 2:많이
unsigned g_nReceiveBufferSize = 1024 * 128; // 소켓 수신 버퍼의 크기. 특정 비디오 서버의 경우 이 크기가 낮으면 영상이 깨지는 문제가 생기기도 한다.
    // 기본값은 65536임.
unsigned g_nDummySinkReceiveBufferSize = unsigned( 1024 * 512 ); // 커스텀 싱크에서 유지하는 버퍼의 크기.

const int g_nMaxRTSPClientNum                = 256; // 라이브 555에서 동시에 수신 가능한 채널 수. 필요한 경우 늘릴 수 있음.
const int g_nMaxRTSPClientNumberPerClientMgr = 8; // 클라이언트 매니저(TaskScheduler)당 관리해야하는 채널(RTSPClient) 수.
    // 하나의 TaskScheduler에서 관리하는 채널수를 제한하는 이유는 다음과 같다.
    // 1. 하나의 태스크 스케줄러에서 UDP 연결이 16개 까지만 동작됨
    // 2. 16개의 연결에 근접할 수록 연결이 원할하지 않게됨.
int g_nRTSPClinetIDAllocationTable[g_nMaxRTSPClientNum];
CCriticalSection g_csRTSPClientIDTable;

//////////////////////////////////////////////////////////////////
//
// Global Functions
//
//////////////////////////////////////////////////////////////////
char* getLLine( char* startOfLine )
{
   // returns the start of the next line, or NULL if none
   for( char* ptr = startOfLine; *ptr != '\0'; ++ptr ) {
      if( *ptr == '\r' || *ptr == '\n' ) {
         // We found the end of the line
         *ptr++ = '\0';
         if( *ptr == '\n' ) ++ptr;
         return ptr;
      }
   }
   return NULL;
}
int GetNewRTSPClientID()
{
   int nRTSPClinetID = -1;
   CCriticalSectionSP co( g_csRTSPClientIDTable );
   int i;
   for( i = 0; i < g_nMaxRTSPClientNum; i++ ) {
      if( 0 == g_nRTSPClinetIDAllocationTable[i] ) {
         nRTSPClinetID = i;
         break;
      }
   }
   return nRTSPClinetID;
}

void AllocateRTSPClientID( int nRTSPClinetID )
{
   if( 0 <= nRTSPClinetID && nRTSPClinetID < g_nMaxRTSPClientNum ) {
      CCriticalSectionSP co( g_csRTSPClientIDTable );
      g_nRTSPClinetIDAllocationTable[nRTSPClinetID] = 1;
   }
}

void ReleaseRTSPClientID( int nRTSPClinetID )
{
   if( 0 <= nRTSPClinetID && nRTSPClinetID < g_nMaxRTSPClientNum ) {
      CCriticalSectionSP co( g_csRTSPClientIDTable );
      g_nRTSPClinetIDAllocationTable[nRTSPClinetID] = 0;
   }
}

namespace Live555 {

//////////////////////////////////////////////////////////////////
//
// Forward function definitions:
//
//////////////////////////////////////////////////////////////////

// RTSP 'response handlers':
void continueAfterDESCRIBE( RTSPClient* rtspClient, int resultCode, char* resultString );
void continueAfterSETUP( RTSPClient* rtspClient, int resultCode, char* resultString );
void continueAfterPLAY( RTSPClient* rtspClient, int resultCode, char* resultString );
void SendAuthDESCRIBE( RTSPClient* _prtspClient );

void CheckForAuthenticationFailure( RTSPClient* _prtspClient );
char* CreateAuthenticatorString( Authenticator const* authenticator, char const* cmd, char const* url );

// Other event handler functions:
void subsessionAfterPlaying( void* clientData ); // called when a stream's subsession (e.g., audio or video substream) ends
void subsessionByeHandler( void* clientData ); // called when a RTCP "BYE" is received for a subsession
void streamTimerHandler( void* clientData );
void TimeOutSink( void* clientData );
// called at the end of a stream's expected duration (if the stream has not already signaled its end using a RTCP "BYE")

// The main streaming routine (for each "rtsp://" URL):
RTSPClient* openURL( UsageEnvironment& env, char const* progName, char const* rtspURL, CRTSPCustomClient* customClient );

// Used to iterate through each stream's 'subsessions', setting up each one:
void setupNextSubsession( RTSPClient* rtspClient );

// Used to shut down and close a stream (including its "RTSPClient" object):
void shutdownStream( RTSPClient* rtspClient, int exitCode = 1 );

// A function that outputs a string that identifies each stream (for debugging output).  Modify this if you wish:
UsageEnvironment& operator<<( UsageEnvironment& env, const RTSPClient& rtspClient )
{
   return env << "[URL:\"" << rtspClient.url() << "\"]: ";
}

// A function that outputs a string that identifies each subsession (for debugging output).  Modify this if you wish:
UsageEnvironment& operator<<( UsageEnvironment& env, const MediaSubsession& subsession )
{
   return env << subsession.mediumName() << "/" << subsession.codecName();
}

void usage( UsageEnvironment& env, char const* progName )
{
   env << "Usage: " << progName << " <rtsp-url-1> ... <rtsp-url-N>\n";
   env << "\t(where each <rtsp-url-i> is a \"rtsp://\" URL)\n";
}

//////////////////////////////////////////////////////////////////
//
// Class : StreamClientState
//
//////////////////////////////////////////////////////////////////

// Define a class to hold per-stream state that we maintain throughout each stream's lifetime:

class StreamClientState {
public:
   StreamClientState();
   virtual ~StreamClientState();

public:
   MediaSubsessionIterator* iter;
   MediaSession* session;
   MediaSubsession* subsession;
   TaskToken streamTimerTask;
   double duration;
   bool streamUsingTCP; // jun 추가
};

// Implementation of "StreamClientState":

StreamClientState::StreamClientState()
    : iter( NULL )
    , session( NULL )
    , subsession( NULL )
    , streamTimerTask( NULL )
    , duration( 0.0 )
{
   streamUsingTCP = FALSE;
}

StreamClientState::~StreamClientState()
{
   delete iter;
   if( session != NULL ) {
      // We also need to delete "session", and unschedule "streamTimerTask" (if set)
      UsageEnvironment& env = session->envir(); // alias

      env.taskScheduler().unscheduleDelayedTask( streamTimerTask );
      Medium::close( session );
   }
}

//////////////////////////////////////////////////////////////////
//
// Class : ourRTSPClient
//
//////////////////////////////////////////////////////////////////

// If you're streaming just a single stream (i.e., just from a single URL, once), then you can define and use just a single
// "StreamClientState" structure, as a global variable in your application.  However, because - in this demo application - we're
// showing how to play multiple streams, concurrently, we can't do that.  Instead, we have to have a separate "StreamClientState"
// structure for each "RTSPClient".  To do this, we subclass "RTSPClient", and add a "StreamClientState" field to the subclass:

class ourRTSPClient : public RTSPClient {
public:
   static ourRTSPClient* createNew( UsageEnvironment& env, char const* rtspURL,
                                    int verbosityLevel                = 0,
                                    char const* applicationName       = NULL,
                                    portNumBits tunnelOverHTTPPortNum = 0 );

protected:
   ourRTSPClient( UsageEnvironment& env, char const* rtspURL,
                  int verbosityLevel, char const* applicationName, portNumBits tunnelOverHTTPPortNum );
   // called only by createNew();
   virtual ~ourRTSPClient();

public:
   StreamClientState scs;
   CRTSPCustomClient* m_pRTSPCustomClient; // jun : 콜백 함수 호출용으로 사용됨.
};

//////////////////////////////////////////////////////////////////
//
// Global Functions
//
//////////////////////////////////////////////////////////////////

// WARNING(yhpark): rtspClientCount is unused why?
static unsigned rtspClientCount = 0; // Counts how many streams (i.e., "RTSPClient"s) are currently in use.

RTSPClient* openURL( UsageEnvironment& env, char const* progName, char const* rtspURL, CRTSPCustomClient* customClient )
{
   env << "openURL(): ----------------------------------------------------------------------\n"
       << "\n";

   // Begin by creating a "RTSPClient" object.  Note that there is a separate "RTSPClient" object for each stream that we wish
   // to receive (even if more than stream uses the same "rtsp://" URL).
   RTSPClient* rtspClient = ourRTSPClient::createNew( env, rtspURL, g_nVerbosityLevel, progName );
   if( rtspClient == NULL ) {
      env << "Failed to create a RTSP client for URL \"" << rtspURL << "\": " << env.getResultMsg() << "\n";
      return NULL;
   }
   ourRTSPClient* pOurRTSPClient       = (ourRTSPClient*)rtspClient;
   pOurRTSPClient->m_pRTSPCustomClient = customClient;

   // WARNING(yhpark): rtspClientCount is unused why?
   ++rtspClientCount;

   // Next, send a RTSP "DESCRIBE" command, to get a SDP description for the stream.
   // Note that this command - like all RTSP commands - is sent asynchronously; we do not block, waiting for a response.
   // Instead, the following function call returns immediately, and we handle the RTSP response later, from within the event loop:
   // jun: "CSeq" sequence number를 할당받지 못하였다면 이미 rtspClient객체는 삭제된 상태이다.
   unsigned cseq = rtspClient->sendDescribeCommand( continueAfterDESCRIBE );
   if( cseq > 0 )
      return rtspClient;
   else
      return NULL;
}

// Implementation of the RTSP 'response handlers':

void continueAfterDESCRIBE( RTSPClient* rtspClient, int resultCode, char* resultString )
{
   int err_code = LIVE555_ERROR_CONNECTION_FAILED;

   UsageEnvironment& env = rtspClient->envir(); // alias
   env << "continuteAfterDESCRIBE(): ----------------------------------------------------------------------\n"
       << "\n";
   ourRTSPClient* pOurRTSPClient = (ourRTSPClient*)rtspClient;
   StreamClientState& scs        = pOurRTSPClient->scs; // alias
   if( resultCode == 0 || resultCode == 200 ) {
      do {
         if( resultCode != 0 ) {
            env << *rtspClient << "Failed to get a SDP description: " << resultString << "\n";
            env << "* Error Code: " << resultCode << "\n";
            delete[] resultString;
            if( resultCode == 401 ) err_code = LIVE555_ERROR_LOGIN_FAILED;
            break;
         }

         char* const sdpDescription = resultString;
         env << *rtspClient << "Got a SDP description:\n"
             << sdpDescription << "\n";

         // Create a media session object from this SDP description:
         scs.session = MediaSession::createNew( env, sdpDescription );
         delete[] sdpDescription; // because we don't need it anymore
         if( scs.session == NULL ) {
            env << *rtspClient << "Failed to create a MediaSession object from the SDP description: " << env.getResultMsg() << "\n";
            break;
         } else if( !scs.session->hasSubsessions() ) {
            env << *rtspClient << "This session has no media subsessions (i.e., no \"m=\" lines)\n";
            break;
         }

         // Then, create and set up our data source objects for the session.  We do this by iterating over the session's 'subsessions',
         // calling "MediaSubsession::initiate()", and then sending a RTSP "SETUP" command, on each one.
         // (Each 'subsession' will have its own data source.)
         scs.iter = new MediaSubsessionIterator( *scs.session );
         setupNextSubsession( rtspClient );
         return;
      } while( 0 );

      pOurRTSPClient->m_pRTSPCustomClient->OnErrorRaised( err_code );

      // An unrecoverable error occurred with this stream.
      shutdownStream( rtspClient );
   } else {
      switch( resultCode ) {
      case 401:
         SendAuthDESCRIBE( rtspClient );
         break;
      }
   }
}

void CheckForAuthenticationFailure( RTSPClient* _prtspClient )
{
   char* lineStart;
   while( 1 ) {
      lineStart = _prtspClient->fResponseBuffer;

      if( lineStart == NULL ) break;

      sprintf( _prtspClient->fResponseBuffer, "%s", getLLine( lineStart ) );

      if( lineStart[0] == '\0' ) break; // this is a blank line

      int n_strlen                 = strlen( lineStart ) + 1;
      char* realm                  = strDupSize( lineStart );
      char* nonce                  = strDupSize( lineStart );
      bool foundAuthenticateHeader = false;
      if( sscanf( lineStart, "WWW-Authenticate: Digest realm=\"%[^\"]\", nonce=\"%[^\"]\"", realm, n_strlen, nonce, n_strlen ) == 2 ) {
         _prtspClient->m_Authenticator.setRealmAndNonce( realm, nonce );
         foundAuthenticateHeader = true;
      } else if( sscanf( lineStart, "WWW-Authenticate: Basic realm=\"%[^\"]\"", realm, n_strlen ) == 1 ) {
         _prtspClient->m_Authenticator.setRealmAndNonce( realm, NULL ); // Basic authentication
         foundAuthenticateHeader = true;
      }
      delete[] realm;
      delete[] nonce;
      if( foundAuthenticateHeader ) break;
   }
}

char* CreateAuthenticatorString( Authenticator const* authenticator, char const* cmd, char const* url )
{
   if( authenticator != NULL && authenticator->realm() != NULL
       && authenticator->username() != NULL && authenticator->password() != NULL ) {
      // We've been provided a filled-in authenticator, so use it:
      char* authenticatorStr;
      if( authenticator->nonce() != NULL ) {
         // Digest authentication
         char* const authFmt = "Authorization: Digest username=\"%s\", realm=\"%s\", "
                               "nonce=\"%s\", uri=\"%s\", response=\"%s\"\r\n";
         char const* response = authenticator->computeDigestResponse( cmd, url );
         unsigned authBufSize = strlen( authFmt )
             + strlen( authenticator->username() ) + strlen( authenticator->realm() )
             + strlen( authenticator->nonce() ) + strlen( url ) + strlen( response );
         authenticatorStr = new char[authBufSize + 1];
         snprintf( authenticatorStr, authBufSize + 1, authFmt, authenticator->username(), authenticator->realm(), authenticator->nonce(), url, response );
         authenticator->reclaimDigestResponse( response );
      } else {
         // Basic authentication
         char* const authFmt             = "Authorization: Basic %s\r\n";
         unsigned usernamePasswordLength = strlen( authenticator->username() ) + 1 + strlen( authenticator->password() );
         char* usernamePassword          = new char[usernamePasswordLength + 1];
         snprintf( usernamePassword, usernamePasswordLength + 1, "%s:%s", authenticator->username(), authenticator->password() );

         char* response       = base64Encode( usernamePassword, usernamePasswordLength );
         unsigned authBufSize = strlen( authFmt ) + strlen( response );
         authenticatorStr     = new char[authBufSize + 1];
         snprintf( authenticatorStr, authBufSize + 1, authFmt, response );
         delete[] response;
         delete[] usernamePassword;
      }
      return authenticatorStr;
   }
   return strDup( "" );
}

void SendAuthDESCRIBE( RTSPClient* _prtspClient )
{
   ourRTSPClient* pOurRTSPClient  = (ourRTSPClient*)_prtspClient;
   _prtspClient->m_bAuthenticator = TRUE;
   _prtspClient->m_Authenticator.setUsernameAndPassword( pOurRTSPClient->m_pRTSPCustomClient->m_strID.c_str(), pOurRTSPClient->m_pRTSPCustomClient->m_strPasswd.c_str() );

   //CheckForAuthenticationFailure(_prtspClient);
   char* szAuth = CreateAuthenticatorString( &_prtspClient->m_Authenticator, "DESCRIBE", _prtspClient->url() );
   delete szAuth;
   _prtspClient->sendDescribeCommand( continueAfterDESCRIBE, &_prtspClient->m_Authenticator );
}

// By default, we request that the server stream its data using RTP/UDP.
// If, instead, you want to request that the server stream via RTP-over-TCP, change the following to True:

void setupNextSubsession( RTSPClient* rtspClient )
{
   UsageEnvironment& env = rtspClient->envir(); // alias
   env << "setupNextSubsession(): ----------------------------------------------------------------------\n"
       << "\n";

   StreamClientState& scs           = ( (ourRTSPClient*)rtspClient )->scs; // alias
   CRTSPCustomClient* pCustomClient = ( (ourRTSPClient*)rtspClient )->m_pRTSPCustomClient;
   scs.subsession                   = scs.iter->next();
   if( scs.subsession != NULL ) {
      Boolean audio_only    = ( pCustomClient && pCustomClient->m_bAudioOnly ) ? 1 : 0;
      Boolean metadata_only = ( pCustomClient && pCustomClient->m_bMetadataOnly ) ? 1 : 0; // (mkjang-140417)
      if( //(audio_only && strcmp(scs.subsession->mediumName(), "audio")) ||
          //(metadata_only && strcmp(scs.subsession->mediumName(), "application")) ||
          !scs.subsession->initiate() ) {
         env << *rtspClient << "Failed to initiate the \"" << *scs.subsession << "\" subsession: " << env.getResultMsg() << "\n";
         setupNextSubsession( rtspClient ); // give up on this subsession; go to the next one
      } else {
         env << *rtspClient << "Initiated the \"" << *scs.subsession
             << "\" subsession (client ports " << scs.subsession->clientPortNum() << "-" << scs.subsession->clientPortNum() + 1 << ")\n";

         // jun : 일부 카메라에서 수신버퍼의 크기를 늘여주지 않으면 일부 프레임데이터가 (특히 데이터가 큰 키프레임)
         // 수신되지 않는 경우가 간혹 발생되어 디코딩이 되지 않을 수 있다.
         // 아래 코드는 Live555라이브러리의 샘플코드 중 playCommon.cpp에 구현된 내용을 옮겨 놓은 것임.
         // Then, setup the "RTPSource"s for the session:
         if( scs.subsession->rtpSource() != NULL ) {
            // Because we're saving the incoming data, rather than playing
            // it in real time, allow an especially large time threshold
            // (1 second) for reordering misordered incoming packets:
            unsigned const thresh = 1000000; // 1 second
            scs.subsession->rtpSource()->setPacketReorderingThresholdTime( thresh );

            // Set the RTP source's OS socket buffer size as appropriate - either if we were explicitly asked (using -B),
            // or if the desired FileSink buffer size happens to be larger than the current OS socket buffer size.
            // (The latter case is a heuristic, on the assumption that if the user asked for a large FileSink buffer size,
            // then the input data rate may be large enough to justify increasing the OS socket buffer size also.)
            int socketNum          = scs.subsession->rtpSource()->RTPgs()->socketNum();
            unsigned curBufferSize = getReceiveBufferSize( env, socketNum );
            if( g_nReceiveBufferSize > curBufferSize ) { // jun: 지정된 수신버퍼의 크기보다 작으면 재설정.
               unsigned newBufferSize = g_nReceiveBufferSize;
               newBufferSize          = setReceiveBufferTo( env, socketNum, newBufferSize );
            }
         }

         // Continue setting up this subsession, by sending a RTSP "SETUP" command:
         rtspClient->sendSetupCommand( *scs.subsession, continueAfterSETUP, False, scs.streamUsingTCP );
      }
      return;
   }

   // We've finished setting up all of the subsessions.  Now, send a RTSP "PLAY" command to start the streaming:
   if( scs.session->absStartTime() != NULL ) {
      // Special case: The stream is indexed by 'absolute' time, so send an appropriate "PLAY" command:
      //rtspClient->sendPlayCommand(*scs.session, continueAfterPLAY, scs.session->absStartTime(), scs.session->absEndTime());
      rtspClient->sendPlayCommand( *scs.session, continueAfterPLAY, -1.0f, -1.0f, 1.0f );
   } else {
      //scs.duration = scs.session->playEndTime() - scs.session->playStartTime();
      //rtspClient->sendPlayCommand(*scs.session, continueAfterPLAY);
      rtspClient->sendPlayCommand( *scs.session, continueAfterPLAY, 0.0f, -1.0f, 1.0f );
   }
}

void continueAfterSETUP( RTSPClient* rtspClient, int resultCode, char* resultString )
{
   UsageEnvironment& env = rtspClient->envir(); // alias
   env << "continueAfterSETUP(): ----------------------------------------------------------------------\n"
       << "\n";

   StreamClientState& scs           = ( (ourRTSPClient*)rtspClient )->scs; // alias
   CRTSPCustomClient* pCustomClient = ( (ourRTSPClient*)rtspClient )->m_pRTSPCustomClient;
   do {

      if( resultCode != 0 ) {
         env << *rtspClient << "Failed to set up the \"" << *scs.subsession << "\" subsession: " << resultString << "\n";
         break;
      }

      env << *rtspClient << "Set up the \"" << *scs.subsession
          << "\" subsession (client ports " << scs.subsession->clientPortNum() << "-" << scs.subsession->clientPortNum() + 1 << ")\n";

      // Having successfully setup the subsession, create a data sink for it, and call "startPlaying()" on it.
      // (This will prepare the data sink to receive data; the actual flow of data from the client won't start happening until later,
      // after we've sent a RTSP "PLAY" command.)

      //scs.subsession->sink = DummySink::createNew(env, *scs.subsession, rtspClient->url()); // jun : 예제는 삭제

      // jun : 사용자 비디오 싱크 생성
      if( strcmp( scs.subsession->mediumName(), "video" ) == 0 ) {
         if( strcmp( scs.subsession->codecName(), "H264" ) == 0 ) {
            // For H.264 video stream, we use a special sink that insert start_codes:
            scs.subsession->sink = CH264CustomSink::createNew( env, scs.subsession->fmtp_spropparametersets(), g_nDummySinkReceiveBufferSize, pCustomClient );
         } else if( strcmp( scs.subsession->codecName(), "MP4V-ES" ) == 0 ) {
            CMPEG4ESCustomVideoRTPSink* pSink = CMPEG4ESCustomVideoRTPSink::createNew( env, g_nDummySinkReceiveBufferSize, pCustomClient );
            scs.subsession->sink              = pSink;
         }
         // MJPEG 은 여기서 추가해야 함 (xinu_ch26)
         else if( strcmp( scs.subsession->codecName(), "JPEG" ) == 0 ) {
            // For MJPEG video stream, we use a special sink that insert start_codes:
            scs.subsession->sink = CMJPEGCustomSink::createNew( env, g_nDummySinkReceiveBufferSize, pCustomClient );
         }
         pCustomClient->m_nFrameRate = (int)scs.subsession->videoFPS();
      }
      // jun : 사용자 오디오 싱크 생성
      else if( strcmp( scs.subsession->mediumName(), "audio" ) == 0 ) {
         scs.subsession->sink = CAudioCustomSink::createNew( env, scs.subsession, pCustomClient );
      }
      // (mkjang-140417) : 메타데이터 싱크 생성
      else if( strcmp( scs.subsession->mediumName(), "application" ) == 0 ) {
         scs.subsession->sink = CMetadataCustomSink::createNew( env, scs.subsession, pCustomClient );
      }

      // perhaps use your own custom "MediaSink" subclass instead
      if( scs.subsession->sink == NULL ) {
         env << *rtspClient << "Failed to create a data sink for the \"" << *scs.subsession
             << "\" subsession: " << env.getResultMsg() << "\n";
         break;
      }

      env << *rtspClient << "Created a data sink for the \"" << *scs.subsession << "\" subsession\n";
      scs.subsession->miscPtr = rtspClient; // a hack to let subsession handle functions get the "RTSPClient" from the subsession
      scs.subsession->sink->startPlaying( *( scs.subsession->readSource() ),
                                          subsessionAfterPlaying, scs.subsession );
      // Also set a handler to be called if a RTCP "BYE" arrives for this subsession:
      if( scs.subsession->rtcpInstance() != NULL ) {
         scs.subsession->rtcpInstance()->setByeHandler( subsessionByeHandler, scs.subsession );
      }
   } while( 0 );
   delete[] resultString;

   // Set up the next subsession, if any:
   setupNextSubsession( rtspClient );
}

void continueAfterPLAY( RTSPClient* rtspClient, int resultCode, char* resultString )
{
   UsageEnvironment& env = rtspClient->envir(); // alias
   env << "continueAfterPLAY(): ----------------------------------------------------------------------\n"
       << "\n";

   StreamClientState& scs = ( (ourRTSPClient*)rtspClient )->scs; // alias
   Boolean success        = False;
   do {

      if( resultCode != 0 ) {
         env << *rtspClient << "Failed to start playing session: " << resultString << "\n";
         break;
      }

      if( rtspClient->sessionTimeoutParameter() > 0 ) {
         //unsigned const delaySlop = 2; // number of seconds extra to delay, after the stream's expected duration.  (This is optional.)
         //scs.duration += delaySlop;
         unsigned uSecsToDelay = (unsigned)( rtspClient->sessionTimeoutParameter() * 1000000 ) - 100000;
         scs.streamTimerTask   = env.taskScheduler().scheduleDelayedTask( uSecsToDelay, (TaskFunc*)TimeOutSink, rtspClient );
      }

      // Set a timer to be handled at the end of the stream's expected duration (if the stream does not already signal its end
      // using a RTCP "BYE").  This is optional.  If, instead, you want to keep the stream active - e.g., so you can later
      // 'seek' back within it and do another RTSP "PLAY" - then you can omit this code.
      // (Alternatively, if you don't want to receive the entire stream, you could set this timer for some shorter value.)
      if( scs.duration > 0 ) {
         unsigned const delaySlop = 2; // number of seconds extra to delay, after the stream's expected duration.  (This is optional.)
         scs.duration += delaySlop;
         unsigned uSecsToDelay = (unsigned)( scs.duration * 1000000 );
         scs.streamTimerTask   = env.taskScheduler().scheduleDelayedTask( uSecsToDelay, (TaskFunc*)streamTimerHandler, rtspClient );
      }

      env << *rtspClient << "Started playing session";
      if( scs.duration > 0 ) {
         env << " (for up to " << scs.duration << " seconds)";
      }
      env << "...\n";

      success = True;
   } while( 0 );
   delete[] resultString;

   if( !success ) {
      // An unrecoverable error occurred with this stream.
      shutdownStream( rtspClient );
   }
}

// Implementation of the other event handlers:

void subsessionAfterPlaying( void* clientData )
{
   MediaSubsession* subsession = (MediaSubsession*)clientData;
   RTSPClient* rtspClient      = (RTSPClient*)( subsession->miscPtr );

   UsageEnvironment& env = rtspClient->envir();
   env << "subsessionAfterPlaying(): ----------------------------------------------------------------------\n"
       << "\n";

   // Begin by closing this subsession's stream:
   Medium::close( subsession->sink );
   subsession->sink = NULL;

   // Next, check whether *all* subsessions' streams have now been closed:
   MediaSession& session = subsession->parentSession();
   MediaSubsessionIterator iter( session );
   while( ( subsession = iter.next() ) != NULL ) {
      if( subsession->sink != NULL ) return; // this subsession is still active
   }

   // All subsessions' streams have now been closed, so shutdown the client:
   shutdownStream( rtspClient );
}

void subsessionByeHandler( void* clientData )
{
   MediaSubsession* subsession   = (MediaSubsession*)clientData;
   RTSPClient* rtspClient        = (RTSPClient*)subsession->miscPtr;
   ourRTSPClient* pOurRTSPClient = (ourRTSPClient*)rtspClient;
   pOurRTSPClient->m_pRTSPCustomClient->OnErrorRaised( LIVE555_ERROR_BYE_PACKET );

   UsageEnvironment& env = rtspClient->envir(); // alias
   env << "subsessionByHandler(): ----------------------------------------------------------------------\n"
       << "\n";

   env << *rtspClient << "Received RTCP \"BYE\" on \"" << *subsession << "\" subsession\n";

   // Now act as if the subsession had closed:
   subsessionAfterPlaying( subsession );
}

void streamTimerHandler( void* clientData )
{
   ourRTSPClient* rtspClient = (ourRTSPClient*)clientData;
   StreamClientState& scs    = rtspClient->scs; // alias

   UsageEnvironment& env = rtspClient->envir();
   env << "streamTimerHandler(): ----------------------------------------------------------------------\n"
       << "\n";

   scs.streamTimerTask = NULL;

   // Shut down the stream:
   shutdownStream( rtspClient );
}

void TimeOutSink( void* clientData )
{
   ourRTSPClient* rtspClient = (ourRTSPClient*)clientData;
   StreamClientState& scs    = rtspClient->scs; // alias
   UsageEnvironment& env     = rtspClient->envir(); // alias

   rtspClient->sendGetParameterCommand( *scs.session, NULL, NULL );
   if( rtspClient->sessionTimeoutParameter() ) {
      unsigned uSecsToDelay = (unsigned)( rtspClient->sessionTimeoutParameter() * 1000000 ) - 100000;
      scs.streamTimerTask   = env.taskScheduler().scheduleDelayedTask( uSecsToDelay, (TaskFunc*)TimeOutSink, rtspClient );
   }
}

void shutdownStream( RTSPClient* rtspClient, int exitCode )
{
   UsageEnvironment& env = rtspClient->envir(); // alias
   env << "shutdownStream(): ----------------------------------------------------------------------\n"
       << "\n";

   StreamClientState& scs = ( (ourRTSPClient*)rtspClient )->scs; // alias
   // First, check whether any subsessions have still to be closed:
   if( scs.session != NULL ) {
      Boolean someSubsessionsWereActive = False;
      MediaSubsessionIterator iter( *scs.session );
      MediaSubsession* subsession;

      while( ( subsession = iter.next() ) != NULL ) {
         if( subsession->sink != NULL ) {
            Medium::close( subsession->sink );
            subsession->sink = NULL;

            if( subsession->rtcpInstance() != NULL ) {
               subsession->rtcpInstance()->setByeHandler( NULL, NULL ); // in case the server sends a RTCP "BYE" while handling "TEARDOWN"
            }

            someSubsessionsWereActive = True;
         }
      }

      if( someSubsessionsWereActive ) {
         // Send a RTSP "TEARDOWN" command, to tell the server to shutdown the stream.
         // Don't bother handling the response to the "TEARDOWN".
         rtspClient->sendTeardownCommand( *scs.session, NULL );
      }
   }

   env << *rtspClient << "Closing the stream.\n";
   Medium::close( rtspClient );
   // Note that this will also cause this stream's "StreamClientState" structure to get reclaimed.

   // jun : 샘플코드는 막음.
   //if (--rtspClientCount == 0) {
   //   // The final stream has ended, so exit the application now.
   //   // (Of course, if you're embedding this code into your own application, you might want to comment this out,
   //   // and replace it with "eventLoopWatchVariable = 1;", so that we leave the LIVE555 event loop, and continue running "main()".)
   //   exit(exitCode);
   //}
}

// Implementation of "ourRTSPClient":

ourRTSPClient* ourRTSPClient::createNew( UsageEnvironment& env, char const* rtspURL,
                                         int verbosityLevel, char const* applicationName, portNumBits tunnelOverHTTPPortNum )
{
   return new ourRTSPClient( env, rtspURL, verbosityLevel, applicationName, tunnelOverHTTPPortNum );
}

ourRTSPClient::ourRTSPClient( UsageEnvironment& env, char const* rtspURL,
                              int verbosityLevel, char const* applicationName, portNumBits tunnelOverHTTPPortNum )
    : RTSPClient( env, rtspURL, verbosityLevel, applicationName, tunnelOverHTTPPortNum, -1 )
{
   m_pRTSPCustomClient = NULL; // jun
}

ourRTSPClient::~ourRTSPClient()
{
   // jun
   if( m_pRTSPCustomClient ) {
      m_pRTSPCustomClient->m_pOurRTSPClient = NULL;
   }
}

} // namespace Live555

using namespace Live555;

//////////////////////////////////////////////////////////////////
//
// Class : CRTSPCustomClientMgr
//
//////////////////////////////////////////////////////////////////

// CRTSPCustomClientMgr 는 외부에서 사용할 필요가 없기 때문에 이곳에서 클래스 선언을 하였음.

class CRTSPCustomClientMgr {
protected:
   CCriticalSection m_csRTSPClients;
   std::deque<CRTSPCustomClient*> m_RTSPClients;
   HANDLE m_hTaskSchedulerEventLoopThread;

protected:
   char eventLoopWatchVariable;

public:
   BasicTaskScheduler* scheduler;
   UsageEnvironment* env;

public:
   CRTSPCustomClientMgr();
   ~CRTSPCustomClientMgr();

public:
   void Add( CRTSPCustomClient* pRTSPClient );
   void Remove( CRTSPCustomClient* pRTSPClient );
   BOOL CheckExistence( CRTSPCustomClient* pRTSPClient );

public:
   BOOL DoesRunningRTSPClientExist();
   void CheckEventLoopRun();
   void StartTaskSchedularEventLoopThread();
   void StopTaskSchedularEventLoopThread();

   static UINT Thread_TaskSchedulerEventLoop( LPVOID pParam );
};

////////////// Implementation of CRTSPCustomClientMgr ////////////////

CRTSPCustomClientMgr* g_RTSPClientMgr = NULL;

void UninitRTSPClientManager();

void InitRTSPClientManager()
{
   UninitRTSPClientManager();
   g_RTSPClientMgr = new CRTSPCustomClientMgr[g_nMaxRTSPClientNum / g_nMaxRTSPClientNumberPerClientMgr];
   int i;
   for( i = 0; i < g_nMaxRTSPClientNum; i++ ) {
      g_nRTSPClinetIDAllocationTable[i] = 0;
   }
}

void UninitRTSPClientManager()
{
   if( g_RTSPClientMgr ) {
      delete[] g_RTSPClientMgr;
      g_RTSPClientMgr = NULL;
   }
}

CRTSPCustomClientMgr::CRTSPCustomClientMgr()
{
   eventLoopWatchVariable          = 0;
   m_hTaskSchedulerEventLoopThread = 0;

   scheduler = BasicTaskScheduler::createNew( 1000 );
   env       = BasicUsageEnvironment::createNew( *scheduler );
}

CRTSPCustomClientMgr::~CRTSPCustomClientMgr()
{
   m_csRTSPClients.Lock();
   while( m_RTSPClients.size() ) {
      std::deque<CRTSPCustomClient*>::iterator iter     = m_RTSPClients.begin();
      std::deque<CRTSPCustomClient*>::iterator iter_end = m_RTSPClients.end();
      while( iter_end != iter ) {
         CRTSPCustomClient* pIterRTSPClient = *iter;
         pIterRTSPClient->Stop();
         break;
      }
   }
   m_csRTSPClients.Unlock();

   StopTaskSchedularEventLoopThread();
   env->reclaim();
   env = NULL;
   delete scheduler;
   scheduler = NULL;
}

void CRTSPCustomClientMgr::Add( CRTSPCustomClient* pRTSPClient )
{
   CCriticalSectionSP co( m_csRTSPClients );
   if( FALSE == CheckExistence( pRTSPClient ) ) {
      m_RTSPClients.push_back( pRTSPClient );
   }
}

void CRTSPCustomClientMgr::Remove( CRTSPCustomClient* pRTSPClient )
{
   CCriticalSectionSP co( m_csRTSPClients );

   std::deque<CRTSPCustomClient*>::iterator iter     = m_RTSPClients.begin();
   std::deque<CRTSPCustomClient*>::iterator iter_end = m_RTSPClients.end();
   while( iter_end != iter ) {
      CRTSPCustomClient* pIterRTSPClient = *iter;
      if( pIterRTSPClient == pRTSPClient ) {
         m_RTSPClients.erase( iter );
         break;
      }
      iter++;
   }
}

BOOL CRTSPCustomClientMgr::CheckExistence( CRTSPCustomClient* pRTSPClient )
{
   BOOL bExist = FALSE;

   CCriticalSectionSP co( m_csRTSPClients );
   std::deque<CRTSPCustomClient*>::iterator iter     = m_RTSPClients.begin();
   std::deque<CRTSPCustomClient*>::iterator iter_end = m_RTSPClients.end();
   while( iter_end != iter ) {
      CRTSPCustomClient* pIterRTSPClient = *iter;
      if( pIterRTSPClient == pRTSPClient ) {
         bExist = TRUE;
         break;
      }
      iter++;
   }

   return bExist;
}

BOOL CRTSPCustomClientMgr::DoesRunningRTSPClientExist()
{
   int i;

   BOOL bRunningRTSPClient = FALSE;

   CCriticalSectionSP co( m_csRTSPClients );
   int nListNum = (int)m_RTSPClients.size();
   for( i = 0; i < nListNum; i++ ) {
      bRunningRTSPClient = TRUE;
      break;
   }

   return bRunningRTSPClient;
}

void CRTSPCustomClientMgr::CheckEventLoopRun()
{
   if( DoesRunningRTSPClientExist() ) {
      StartTaskSchedularEventLoopThread();
   } else {
      // jun : 한번 시작한 작업스케줄러는 굳이 종료시키지 않는다. (논리적으로는 종료시켜도 무방함)
      //StopTaskSchedularEventLoopThread (   );
   }
}

void CRTSPCustomClientMgr::StartTaskSchedularEventLoopThread()
{
   // 중요: 스레드의 시작/종료는 임계영역에 의하여 보호되어야 한다.
   CCriticalSectionSP co( m_csRTSPClients );
   if( !m_hTaskSchedulerEventLoopThread ) {
      eventLoopWatchVariable          = 0;
      CWinThread* pThread             = AfxBeginThread( Thread_TaskSchedulerEventLoop, (LPVOID)this, THREAD_PRIORITY_HIGHEST, CREATE_SUSPENDED );
      m_hTaskSchedulerEventLoopThread = pThread->m_hThread;
      pThread->ResumeThread();
   }
}

void CRTSPCustomClientMgr::StopTaskSchedularEventLoopThread()
{
   //logd ("///////////  StopTaskSchedularEventLoopThread - Started  ///////////\n");
   // 중요: 스레드의 시작/종료는 임계영역에 의하여 보호되어야 한다.
   CCriticalSectionSP co( m_csRTSPClients );
   eventLoopWatchVariable = 1;
   if( m_hTaskSchedulerEventLoopThread ) {
      WaitForSingleObject( m_hTaskSchedulerEventLoopThread, INFINITE );
      m_hTaskSchedulerEventLoopThread = 0;
   }
   //logd ("///////////  StopTaskSchedularEventLoopThread - Ended    ///////////\n");
}

UINT CRTSPCustomClientMgr::Thread_TaskSchedulerEventLoop( LPVOID pParam )
{
#ifdef SUPPORT_PTHREAD_NAME
   char thread_name[16] = {};
   sprintf( thread_name, "RtspClient" );
   pthread_setname_np( pthread_self(), thread_name );
#endif

   //logd ("///////////  Thread_TaskSchedulerEventLoop - Started  ///////////\n");
   CRTSPCustomClientMgr* pMGR = (CRTSPCustomClientMgr*)pParam;
   pMGR->env->taskScheduler().doEventLoop( &pMGR->eventLoopWatchVariable );
   //logd ("///////////  Thread_TaskSchedulerEventLoop - Ended    ///////////\n");
   return ( DONE );
}

//////////////////////////////////////////////////////////////////
//
// Class : CRTSPCustomClient
//
//////////////////////////////////////////////////////////////////

CRTSPCustomClient::CRTSPCustomClient()
{
   m_bRunning        = FALSE;
   m_pOurRTSPClient  = NULL;
   m_bStreamUsingTCP = False;
   m_bAudioOnly      = False;
   m_bMetadataOnly   = False;
   m_pMgr            = 0;
   m_nRTSPClientID   = -1;
}

CRTSPCustomClient::~CRTSPCustomClient()
{
}

void CRTSPCustomClient::Run()
{
   CCriticalSectionSP co( m_csRTSPCustomClient );
   if( m_bRunning ) {
      return;
   }

   if( -1 == m_nRTSPClientID ) {
      CCriticalSectionSP co( g_csRTSPClientIDTable );
      m_nRTSPClientID = GetNewRTSPClientID();
      AllocateRTSPClientID( m_nRTSPClientID );
   }

   int nMgrIdx                 = m_nRTSPClientID / g_nMaxRTSPClientNumberPerClientMgr;
   int nStreamingChIdxInServer = m_nRTSPClientID % g_nMaxRTSPClientNumberPerClientMgr;

   // 클라이언트 메니저를 할당받는다.
   // 전역변수에 대한 포인터만 얻어오기 때문에 임계영역에 의해 보호 받지 않아도 됨.
   CRTSPCustomClientMgr* pMgr = (CRTSPCustomClientMgr*)m_pMgr;
   if( !pMgr ) {
      pMgr   = &g_RTSPClientMgr[nMgrIdx];
      m_pMgr = (void*)pMgr;
   }

   // 메니저에 클라이언트 추가.
   pMgr->Add( this );
   pMgr->CheckEventLoopRun();

   // ourRTSPClient 객체를 생성시킨다.
   CEvent evtCmdExcuted;
   CRTSPCustomCommand* pNewCommandItem   = new CRTSPCustomCommand;
   pNewCommandItem->m_nCommandType       = RTSPCustomClientCommandType_AddCustomClient;
   pNewCommandItem->m_pCustomClient      = this;
   pNewCommandItem->m_pevtCommandExcuted = &evtCmdExcuted;
   CRTSPCustomCommandQueue* pCmdQueue    = (CRTSPCustomCommandQueue*)pMgr->scheduler->m_pRTSPCustomClientCommandQueue;
   pCmdQueue->Push( pNewCommandItem );
   WaitForSingleObject( evtCmdExcuted, INFINITE );

   if( m_pOurRTSPClient ) {
      // 메니저의 이벤트 루프 스레드가 실행되도록 한다.
      m_bRunning = TRUE;
   } else {
      Stop();
   }
}

void CRTSPCustomClient::Stop()
{
   CCriticalSectionSP co( m_csRTSPCustomClient );

   if( !m_bRunning ) {
      CRTSPCustomClientMgr* pMgr = (CRTSPCustomClientMgr*)m_pMgr;
      if( pMgr )
         pMgr->Remove( this );
      return;
   }

   CRTSPCustomClientMgr* pMgr = (CRTSPCustomClientMgr*)m_pMgr;
   if( pMgr ) {
      // ourRTSPClient 객체를 삭제(shutdownStream)시킨다.
      if( m_pOurRTSPClient ) {
         CEvent evtCmdExcuted;
         CRTSPCustomCommand* pNewCommandItem   = new CRTSPCustomCommand;
         pNewCommandItem->m_nCommandType       = RTSPCustomClientCommandType_DeleteCustomClient;
         pNewCommandItem->m_pCustomClient      = this;
         pNewCommandItem->m_pevtCommandExcuted = &evtCmdExcuted;
         CRTSPCustomCommandQueue* pCmdQueue    = (CRTSPCustomCommandQueue*)pMgr->scheduler->m_pRTSPCustomClientCommandQueue;
         pCmdQueue->Push( pNewCommandItem );
         WaitForSingleObject( evtCmdExcuted, INFINITE );
      }

      // 메니저에서 클라이언트 제거
      pMgr->Remove( this );
      pMgr->CheckEventLoopRun();

      if( m_nRTSPClientID >= 0 ) {
         ReleaseRTSPClientID( m_nRTSPClientID );
         m_nRTSPClientID = -1;
      }

      // 메니저의 이벤트 루프 스레드가 종료되어야 하는지 체크하고 클라이언트가 없다면 종료시킨다.
      m_bRunning = FALSE;
   }
   m_bRunning = FALSE; // 위 If 문에 들어가지 경우를 대비..
}

unsigned CRTSPCustomClient::SendSetParamerCommand( const char* _szName, const char* _szparameterValue )
{
   unsigned ret = 0;

   if( m_pOurRTSPClient ) {
      StreamClientState& scs = ( (ourRTSPClient*)m_pOurRTSPClient )->scs; // alias
      if( scs.session != NULL ) {
         ret = m_pOurRTSPClient->sendSetParameterCommand( *scs.session, NULL, _szName, _szparameterValue );
      }
   }

   return ret;
}

unsigned CRTSPCustomClient::SendGetParamerCommand( const char* _szName )
{
   unsigned ret = 0;

   if( m_pOurRTSPClient ) {
      StreamClientState& scs = ( (ourRTSPClient*)m_pOurRTSPClient )->scs; // alias
      if( scs.session != NULL ) {
         ret = m_pOurRTSPClient->sendGetParameterCommand( *scs.session, NULL, _szName );
      }
   }

   return ret;
}

double CRTSPCustomClient::GetNormalPlayTime( struct timeval const& presentationTime )
{
   if( m_pOurRTSPClient ) {
      StreamClientState& scs = ( (ourRTSPClient*)m_pOurRTSPClient )->scs; // alias
      if( scs.session != NULL ) {
         // jun: play 최초 시간 얻기
         MediaSession& session       = *scs.session;
         MediaSubsession* subsession = NULL;
         MediaSubsessionIterator iter( session );
         while( ( subsession = iter.next() ) != NULL ) {
            return subsession->getNormalPlayTime( presentationTime );
         }
      }
   }

   return 0.0;
}

void CRTSPCustomClient::__OPU()
{
   CRTSPCustomClientMgr* pMgr    = (CRTSPCustomClientMgr*)m_pMgr;
   ourRTSPClient* pOurRTSPClient = (ourRTSPClient*)openURL( *pMgr->env, "IVX", m_strURL.c_str(), this );
   if( pOurRTSPClient ) {
      // ourRTSPClient 객체와 본(CRTSPCustomClient) 객체와의 연관관계를 맺는다.
      m_pOurRTSPClient                   = pOurRTSPClient;
      pOurRTSPClient->scs.streamUsingTCP = m_bStreamUsingTCP;
   } else {
      m_pOurRTSPClient = NULL;
   }
}

void CRTSPCustomClient::__SDS()
{
   if( m_pOurRTSPClient )
      shutdownStream( m_pOurRTSPClient );
}

#pragma pack()

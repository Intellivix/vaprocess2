﻿#pragma once

#include "RTSPClient.hh"
#include "BasicUsageEnvironment.hh"

#pragma pack( 8 )

//////////////////////////////////////////////////////////////////
//
// Class : IRTSPClient
//
//////////////////////////////////////////////////////////////////

class IRTSPClient {
   // 서브클래스에서 구현해야 할 함수들
public:
   virtual void OnErrorRaised( int err_code ){};
   virtual void OnAudioDataReceived( int codec_id, byte* s_buffer, int s_buf_size, timeval& timestampl ){};
   virtual void OnVideoFrameReceived( int codec_id, byte* s_buffer, int s_buf_size, int flags, timeval& timestamp ){};
   virtual void OnMetaDataReceived( byte* s_buffer, int s_buf_size, timeval& timestamp ){};
   virtual void OnVideoSizeChanged( int width, int height ){};
};

//////////////////////////////////////////////////////////////////
//
// Class : CRTSPCustomClient
//
//////////////////////////////////////////////////////////////////

class CRTSPCustomClient : public IRTSPClient {
public:
   int m_nFrameRate;
   Boolean m_bStreamUsingTCP; // TRUE:TCP, FALSE:UDP
   Boolean m_bAudioOnly; // 이 플래그가 True 이면 오디오만 수신받는다.
   Boolean m_bMetadataOnly; // True : 메타데이터만 수신
   std::string m_strURL; // 접속 URL

   std::string m_strID;
   std::string m_strPasswd;

public:
   RTSPClient* m_pOurRTSPClient;
   CCriticalSection m_csRTSPCustomClient;

protected:
   int m_nRTSPClientID;
   BOOL m_bRunning;
   void* m_pMgr;

public:
   CRTSPCustomClient();
   virtual ~CRTSPCustomClient();

public:
   // 주의 : 아래의 함수들은 이제 비동기적으로 수행될 수 있다. (2014-01-09)
   void Run();
   void Stop();
   BOOL IsRunning() { return m_bRunning; };

public:
   unsigned SendSetParamerCommand( const char* _szName, const char* _szparameterValue );
   unsigned SendGetParamerCommand( const char* _szName );
   double GetNormalPlayTime( timeval const& presentationTime );

   // 내부에서 호출하는 함수임. (사용금지)
public:
   void __OPU(); // Open URL
   void __SDS(); // Shutdown Stream
};

#pragma pack()

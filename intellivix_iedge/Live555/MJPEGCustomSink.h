#pragma once
#include "MediaSink.hh"
#include "RTSPCustomClient.h"

class CMJPEGCustomSink : public MediaSink
{
private:
	unsigned char* m_Buffer;
	unsigned m_nBufferSize;	
   IRTSPClient* m_pRTSPClient;
public:
	CMJPEGCustomSink(UsageEnvironment& env, unsigned bufferSize, IRTSPClient* pRTSPClient = NULL);
	virtual ~CMJPEGCustomSink(void);
	static CMJPEGCustomSink* createNew(UsageEnvironment& env, unsigned bufferSize, IRTSPClient* pRTSPClient = NULL);

	static void afterGettingFrame(void* clientData, unsigned frameSize,	unsigned numTruncatedBytes,	struct timeval presentationTime,unsigned durationInMicroseconds);	
	virtual void afterGettingFrame1(unsigned frameSize,	struct timeval presentationTime);

protected:
	virtual Boolean continuePlaying();
};

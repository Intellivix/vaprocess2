/**********
This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation; either version 2.1 of the License, or (at your
option) any later version. (See <http://www.gnu.org/copyleft/lesser.html>.)

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General Public License
along with this library; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
**********/
// "liveMedia"
// Copyright (c) 1996-2015 Live Networks, Inc.  All rights reserved.
// A file source that is a plain byte stream (rather than frames)
// Implementation

#include "stdafx.h"
#include "ByteStreamLiveSource.hh"
#include "GroupsockHelper.hh"

int nByteStreamSourceCount = 0;

////////// ByteStreamLiveSource //////////

ByteStreamLiveSource*
ByteStreamLiveSource::createNew( UsageEnvironment& env,
                                 CRTSPStreamingChannel* streamingChannel,
                                 char const* fileName,
                                 unsigned preferredFrameSize,
                                 unsigned playTimePerFrame )
{
   // jun : File -> Live 로 바꾼부분이다.
   ByteStreamLiveSource* newSource
       = new ByteStreamLiveSource( env, streamingChannel, fileName, preferredFrameSize, playTimePerFrame );

   return newSource;
}

ByteStreamLiveSource::ByteStreamLiveSource( UsageEnvironment& env,
                                            CRTSPStreamingChannel* streamingChannel,
                                            char const* fileName,
                                            unsigned preferredFrameSize,
                                            unsigned playTimePerFrame )
    : FramedSource( env )
    , fPreferredFrameSize( preferredFrameSize )
    , fPlayTimePerFrame( playTimePerFrame )
    , fLastPlayTime( 0 )
    , fStreamingChannel( NULL )
{

   if( streamingChannel ) {
      nByteStreamSourceCount++;
      fFirstKeyFrameAdded = False;
      fFrameRate          = 30.0f;
      fStreamingChannel   = streamingChannel; // jun: CRTSPStreamingChannel 스트리밍 채널 객체 포인터 복사
      fStreamingChannel->AddVideoSource( this );
   }
}

ByteStreamLiveSource::~ByteStreamLiveSource()
{
   if( fStreamingChannel ) {
      fStreamingChannel->RemoveVideoSource( this );

      nByteStreamSourceCount--;

      if( nByteStreamSourceCount == 0 ) {
         if( !fStreamingChannel->IsVideoStreamRequested() && !fStreamingChannel->IsVAMetaDataRequested() ) {
            if( fStreamingChannel->GetServer() )
               fStreamingChannel->GetServer()->OnStreamingChannelClosed( fStreamingChannel->GetServer()->m_pParam, fStreamingChannel );
            logd( "ByteStreamLiveSource OnStreamingChannelClosed \n" );
         }
      }
   }
}

void ByteStreamLiveSource::doGetNextFrame()
{
   doReadFromQueue();
}

void ByteStreamLiveSource::doStopGettingFrames()
{
   envir().taskScheduler().unscheduleDelayedTask( nextTask() );
}

void ByteStreamLiveSource::doReadFromQueue()
{
   // Try to read as many bytes as will fit in the buffer provided (or "fPreferredFrameSize" if less)
   if( fPreferredFrameSize > 0 && fPreferredFrameSize < fMaxSize ) {
      fMaxSize = fPreferredFrameSize;
   }

   uint addFrameSize = 0;

   fStreamingBufferQueue.Lock();
   while( fStreamingBufferQueue.Size() ) {
      CRTSPVideoItem* pStreamingBuffItem = fStreamingBufferQueue.Front();
      if( pStreamingBuffItem ) {
         fFrameRate          = pStreamingBuffItem->m_fFrameRate + 1;
         fPresentationTime   = pStreamingBuffItem->m_PresentationTime;
         int tgtCopyBuffSize = fMaxSize - addFrameSize;
         if( tgtCopyBuffSize > 0 ) {
            byte* pCompBuff     = pStreamingBuffItem->GetBuffer();
            int nCompBuffLength = pStreamingBuffItem->GetLength();
            if( nCompBuffLength >= 0 ) {
               if( nCompBuffLength <= tgtCopyBuffSize ) {
                  memcpy( fTo + addFrameSize, pCompBuff, nCompBuffLength );
                  addFrameSize += nCompBuffLength;
                  fStreamingBufferQueue.Pop();
                  delete pStreamingBuffItem;
               } else if( nCompBuffLength > tgtCopyBuffSize ) {
                  int copyBuffSize = tgtCopyBuffSize;
                  memcpy( fTo + addFrameSize, pCompBuff, copyBuffSize );
                  addFrameSize += copyBuffSize;

                  int remainBuffSize = nCompBuffLength - copyBuffSize;
                  memmove( pCompBuff, pCompBuff + copyBuffSize, remainBuffSize );
                  pStreamingBuffItem->SetLength( remainBuffSize );
               }
            }
         } else
            break;
      }
   }
   fStreamingBufferQueue.Unlock();

   fFrameSize = addFrameSize;

   // To avoid possible infinite recursion, we need to return to the event loop to do this:
   int uSecsToDelay = 1000000 / ( fFrameRate * 2 );
   nextTask()       = envir().taskScheduler().scheduleDelayedTask( uSecsToDelay, (TaskFunc*)FramedSource::afterGetting, this );
}

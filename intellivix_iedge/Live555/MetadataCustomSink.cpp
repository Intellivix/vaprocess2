#include "stdafx.h"
#include "MetadataCustomSink.h"
#include "H264VideoRTPSource.hh"
#include "bccl_file.h"

CMetadataCustomSink::CMetadataCustomSink( UsageEnvironment& env, MediaSubsession* subsession, IRTSPClient* pRTSPClient )
    : MediaSink( env )
{
   m_pSubsession                  = subsession;
   m_nBufferSize                  = 1024 * 64;
   m_pWorkFrameBuff               = new BCCL::FileIO;
   BCCL::FileIO* pWorkFrameBuffer = (BCCL::FileIO*)m_pWorkFrameBuff;
   pWorkFrameBuffer->SetLength( m_nBufferSize );
   memset( pWorkFrameBuffer->GetBuffer(), 0, m_nBufferSize );
   m_bInitParam                   = False;
   m_bInitStartStreamFrontRawData = False;
   m_pRTSPClient                  = pRTSPClient;
}

CMetadataCustomSink::~CMetadataCustomSink( void )
{
   BCCL::FileIO* pWorkFrameBuffer = (BCCL::FileIO*)m_pWorkFrameBuff;
   pWorkFrameBuffer->Close();
   delete pWorkFrameBuffer;
}

CMetadataCustomSink* CMetadataCustomSink::createNew( UsageEnvironment& env, MediaSubsession* subsession, IRTSPClient* pRTSPClient )
{
   return new CMetadataCustomSink( env, subsession, pRTSPClient );
}

Boolean CMetadataCustomSink::continuePlaying()
{
   if( fSource == NULL ) return False;
   BCCL::FileIO* pWorkFrameBuffer = (BCCL::FileIO*)m_pWorkFrameBuff;
   byte* pBuff                    = pWorkFrameBuffer->GetBuffer();
   fSource->getNextFrame( pBuff, m_nBufferSize, afterGettingFrame, this, onSourceClosure, this );
   return True;
}
void CMetadataCustomSink::afterGettingFrame( void* pClientData, unsigned nFrameSize, unsigned nNumTruncatedBytes, struct timeval presentationTime, unsigned nDurationInMicroseconds )
{
   CMetadataCustomSink* sink = (CMetadataCustomSink*)pClientData;
   sink->afterGettingFrame( nFrameSize, presentationTime );
}

void CMetadataCustomSink::afterGettingFrame( unsigned nFrameSize, struct timeval presentationTime )
{
   const char* pCodecName = m_pSubsession->codecName();
   if( m_pRTSPClient ) {
      BCCL::FileIO* pWorkFrameBuffer = (BCCL::FileIO*)m_pWorkFrameBuff;
      byte* pBuff                    = pWorkFrameBuffer->GetBuffer();
      if( nFrameSize > 0 ) {
         //pBuff[9999] = 0;
         //logd ("%s\n", pBuff);
         m_pRTSPClient->OnMetaDataReceived( pBuff, nFrameSize, presentationTime );
      }
   }
   continuePlaying();
}

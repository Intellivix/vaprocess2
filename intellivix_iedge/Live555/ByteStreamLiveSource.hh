/**********
This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation; either version 2.1 of the License, or (at your
option) any later version. (See <http://www.gnu.org/copyleft/lesser.html>.)

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General Public License
along with this library; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
**********/
// "liveMedia"
// Copyright (c) 1996-2015 Live Networks, Inc.  All rights reserved.
// A file source that is a plain byte stream (rather than frames)
// C++ header

#ifndef _BYTE_STREAM_LIVE_SOURCE_HH
#define _BYTE_STREAM_LIVE_SOURCE_HH

#ifndef _FRAMED_FILE_SOURCE_HH
#include "FramedSource.hh"
#endif
#include "RTSPCustomServer.h"

// jun : ByteStreamFileSource를 참고하여 구현한 클래스이다. 
//       라이브에 맞게 일부 코드가 수정되어 있다. 

class ByteStreamLiveSource: public FramedSource {
public:
  static ByteStreamLiveSource* createNew(UsageEnvironment& env, 
                CRTSPStreamingChannel* streamingChannel,
                char const* fileName,
					 unsigned preferredFrameSize = 0,
					 unsigned playTimePerFrame = 0);
  // "preferredFrameSize" == 0 means 'no preference'
  // "playTimePerFrame" is in microseconds

protected:
  ByteStreamLiveSource(UsageEnvironment& env,
             CRTSPStreamingChannel* streamingChannel,
				 char const* fileName,
		       unsigned preferredFrameSize,
		       unsigned playTimePerFrame);
	// called only by createNew()

  virtual ~ByteStreamLiveSource();

  void doReadFromQueue();

private:
  // redefined virtual functions:
  virtual void doGetNextFrame();
  virtual void doStopGettingFrames();

private:
  unsigned fPreferredFrameSize;
  unsigned fPlayTimePerFrame;
  unsigned fLastPlayTime;

public:
   float fFrameRate;

   unsigned               fFirstKeyFrameAdded;
   CRTSPVideoQueue        fStreamingBufferQueue;
   CRTSPStreamingChannel* fStreamingChannel;
};

#endif

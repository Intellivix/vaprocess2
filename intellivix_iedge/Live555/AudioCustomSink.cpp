#include "stdafx.h"
#include "AudioCustomSink.h"
#include "H264VideoRTPSource.hh"
#include "bccl_file.h"

CAudioCustomSink::CAudioCustomSink( UsageEnvironment& env, MediaSubsession* subsession, IRTSPClient* pRTSPClient )
    : MediaSink( env )
{
   m_pSubsession                  = subsession;
   m_nBufferSize                  = 1024 * 256;
   m_pWorkFrameBuff               = new BCCL::FileIO;
   BCCL::FileIO* pWorkFrameBuffer = (BCCL::FileIO*)m_pWorkFrameBuff;
   pWorkFrameBuffer->SetLength( m_nBufferSize );
   m_bInitParam                   = False;
   m_bInitStartStreamFrontRawData = False;
   m_pRTSPClient                  = pRTSPClient;
}

CAudioCustomSink::~CAudioCustomSink( void )
{
   BCCL::FileIO* pWorkFrameBuffer = (BCCL::FileIO*)m_pWorkFrameBuff;
   pWorkFrameBuffer->Close();
   delete pWorkFrameBuffer;
}

CAudioCustomSink* CAudioCustomSink::createNew( UsageEnvironment& env, MediaSubsession* subsession, IRTSPClient* pRTSPClient )
{
   return new CAudioCustomSink( env, subsession, pRTSPClient );
}

Boolean CAudioCustomSink::continuePlaying()
{
   if( fSource == NULL ) return False;
   BCCL::FileIO* pWorkFrameBuffer = (BCCL::FileIO*)m_pWorkFrameBuff;
   byte* pBuff                    = pWorkFrameBuffer->GetBuffer();
   fSource->getNextFrame( pBuff, m_nBufferSize, afterGettingFrame, this, onSourceClosure, this );
   return True;
}
void CAudioCustomSink::afterGettingFrame( void* pClientData, unsigned nFrameSize, unsigned nNumTruncatedBytes, struct timeval presentationTime, unsigned nDurationInMicroseconds )
{
   CAudioCustomSink* sink = (CAudioCustomSink*)pClientData;
   sink->afterGettingFrame( nFrameSize, presentationTime );
}

void CAudioCustomSink::afterGettingFrame( unsigned nFrameSize, struct timeval presentationTime )
{
   const char* pCodecName = m_pSubsession->codecName();
   if( m_pRTSPClient ) {
      BCCL::FileIO* pWorkFrameBuffer = (BCCL::FileIO*)m_pWorkFrameBuff;
      byte* pBuff                    = pWorkFrameBuffer->GetBuffer();
      m_pRTSPClient->OnAudioDataReceived( LIVE555_ACODEC_ID_PCMU, pBuff, nFrameSize, presentationTime );
   }
   continuePlaying();
}

// stdafx.cpp : source file that includes just the standard includes
// Live555Test.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"
// TODO: reference any additional headers you need in STDAFX.H
// and not in this file

CCriticalSection g_mRTSP_LOG;
void RTSP_LOG(const char* data, ...)
{
   CCriticalSectionSP cs(g_mRTSP_LOG);

   const int MAX_BUFFER_LENGTH = 512;

   char    CurrentDate[32] = { 0, };
   char    DebugLog[MAX_BUFFER_LENGTH] = { 0, };

   va_list ap;
   char    Log[MAX_BUFFER_LENGTH] = { 0, };

   va_start(ap, data);
   vsprintf(Log, data, ap);
   va_end(ap);

   time_t time_in_sec;
   time (&time_in_sec);
   tm *c_time = localtime (&time_in_sec);

   snprintf(CurrentDate, 32, ("%04d-%02d-%02d %02d:%02d:%02d"),
      c_time->tm_year + 1900,
      c_time->tm_mon + 1,
      c_time->tm_mday,
      c_time->tm_hour,
      c_time->tm_min,
      c_time->tm_sec);

   //--------------------------------------------------------------------------------
   snprintf(DebugLog, MAX_BUFFER_LENGTH, ("[%s] %s"), CurrentDate, Log);
   //--------------------------------------------------------------------------------
#ifdef __WIN32
   OutputDebugString(DebugLog);
#endif
   printf(("%s"), DebugLog);
}

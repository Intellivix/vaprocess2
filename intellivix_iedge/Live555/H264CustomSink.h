#pragma once
#include "MediaSink.hh"
#include "RTSPCustomClient.h"

class CH264CustomSink : public MediaSink
{
protected:
   int            m_nWidth, m_nHeight;
   void*          m_pParameterBuff;               // 파라메터 정보가 담기는 임시버퍼 
   void*          m_pWorkFrameBuff;               // 디코딩에 사용되는 버퍼. 파라메터 및 비디오프레임 정보를 조립하는데 사용한다.
   Boolean        m_bInitParam;;             
   Boolean        m_bInitStartStreamFrontRawData; // buffer에서, rawdata 앞에 start code를 삽입되었는지 식별.
   Boolean        m_bGetSPSData;
   unsigned       m_nBufferSize;                  
   const char*    m_fSPropParameterSetsStr;       // parameter set
   IRTSPClient*   m_pRTSPClient;

   byte*          m_pFrameBuffer;
   unsigned       m_nFrameSize;
   int            m_addCnt;


public:
   static CH264CustomSink* createNew(UsageEnvironment& env, const char* pSPropParameterSetsStr, unsigned nBufferSize, IRTSPClient* pRTSPClient = NULL);

protected:
   CH264CustomSink(UsageEnvironment& env, const char* pSPropParameterSetsStr, unsigned nBufferSize, IRTSPClient* pRTSPClient = NULL);  
   virtual ~CH264CustomSink(void);

protected:
   virtual Boolean continuePlaying (  );
   static  void    afterGettingFrame  (void* pClientData, unsigned nFrameSize, unsigned nNumTruncatedBytes, struct timeval presentationTime, unsigned nDurationInMicroseconds);
   virtual void    afterGettingFrame  (unsigned nFrameSize, struct timeval presentationTime);
};

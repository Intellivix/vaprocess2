#pragma once

#ifdef __WIN32
#undef FD_SETSIZE
#define FD_SETSIZE 1024 // jun : 접속 가능 채널 수를 늘리려면 FD_SETSIZE 값을 늘려야(64->1024)한다.
#define _CRT_SECURE_NO_WARNINGS
#define NO_WARN_MBCS_MFC_DEPRECATION
#endif

#include "bccl_console.h"

/// === RTSP ===
//#define debug_fprintf fprintf
#define debug_fprintf
#define _CRT_SECURE_NO_WARNINGS

void RTSP_LOG( const char* data, ... );

extern BOOL g_bDebugLive555;

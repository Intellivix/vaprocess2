#pragma once
#include "MediaSink.hh"
#include "RTSPCustomClient.h"




class CMPEG4ESCustomVideoRTPSink : public MediaSink
{
private:
	uint8_t* m_Buffer;
	unsigned m_nBufferSize;	
   IRTSPClient* m_pRTSPClient;
public:
	CMPEG4ESCustomVideoRTPSink(UsageEnvironment& env, unsigned bufferSize, IRTSPClient* pRTSPClient = NULL);
	virtual ~CMPEG4ESCustomVideoRTPSink(void);
	static CMPEG4ESCustomVideoRTPSink* createNew(UsageEnvironment& env, unsigned bufferSize, IRTSPClient* pRTSPClient = NULL);	
	virtual void afterGettingFrame1(unsigned frameSize,	struct timeval presentationTime);
	static void afterGettingFrame(void* clientData, unsigned frameSize,	unsigned numTruncatedBytes,	struct timeval presentationTime,unsigned durationInMicroseconds);	

protected:
	virtual Boolean continuePlaying();
};

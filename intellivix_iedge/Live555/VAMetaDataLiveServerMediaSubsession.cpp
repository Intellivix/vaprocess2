/**********
This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation; either version 2.1 of the License, or (at your
option) any later version. (See <http://www.gnu.org/copyleft/lesser.html>.)

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General Public License
along with this library; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
**********/
// "liveMedia"
// Copyright (c) 1996-2015 Live Networks, Inc.  All rights reserved.
// A 'ServerMediaSubsession' object that creates new, unicast, "RTPSink"s
// on demand, from an text (subtitle) track within a Matroska file.
// Implementation

#include "stdafx.h"
#include "VAMetaDataLiveServerMediaSubsession.hh"
#include "VAMetaDataTextRTPSink.hh"
#include "VAMetaDataLiveSource.hh"

VAMetaDataLiveServerMediaSubsession* VAMetaDataLiveServerMediaSubsession
::createNew(UsageEnvironment& env, CRTSPStreamingChannel* streamingChannel, std::string& strParameter) {
  return new VAMetaDataLiveServerMediaSubsession(env, streamingChannel,strParameter);
}

VAMetaDataLiveServerMediaSubsession
::VAMetaDataLiveServerMediaSubsession(UsageEnvironment& env, CRTSPStreamingChannel* streamingChannel, std::string& strParameter)
  : FileServerMediaSubsession(env, NULL, False)
{
   fStreamingChannel = streamingChannel;
   fParameterStr = strParameter;
   fbMediaSource = False; //qch1004
}

VAMetaDataLiveServerMediaSubsession
::~VAMetaDataLiveServerMediaSubsession() {
   int n=0;
}

float VAMetaDataLiveServerMediaSubsession::duration() const { return 1.0f; }

void VAMetaDataLiveServerMediaSubsession
::seekStreamSource(FramedSource* inputSource, double& seekNPT, double /*streamDuration*/, u_int64_t& /*numBytes*/) {
  //((MatroskaDemuxedTrack*)inputSource)->seekToTime(seekNPT);
}

FramedSource* VAMetaDataLiveServerMediaSubsession
::createNewStreamSource(unsigned clientSessionId, unsigned& estBitrate) {
  estBitrate = 48; // kbps, estimate

  VAMetaDataLiveSource* liveSource(NULL);

  liveSource = VAMetaDataLiveSource::createNew(envir(), fStreamingChannel, fParameterStr);

  fbMediaSource = True;

  return liveSource;
}

RTPSink* VAMetaDataLiveServerMediaSubsession
::createNewRTPSink(Groupsock* rtpGroupsock, unsigned char rtpPayloadTypeIfDynamic, FramedSource* /*inputSource*/) {
  return VAMetaDataTextRTPSink::createNew(envir(), rtpGroupsock, rtpPayloadTypeIfDynamic);
}

void VAMetaDataLiveServerMediaSubsession::pausePlay( unsigned clientSessionId, void* streamToken )
{
   logd("VAMetaDataLiveServerMediaSubsession::pauseStream \n");

   RTSP_PlayCtrl PlayCtrl;
   PlayCtrl.strStime = ("");
   PlayCtrl.strEtime = ("");
   PlayCtrl.strReqTime = ("");
   PlayCtrl.strPlayStream = ("");
   PlayCtrl.nScale = 0;
   PlayCtrl.bRate = FALSE;
   if(fStreamingChannel)
   {
      PlayCtrl.nScale = 0;
      fStreamingChannel->GetServer()->OnStreamPlayCtrl(fStreamingChannel->GetServer(),fStreamingChannel,PlayCtrl);
   }
}


void VAMetaDataLiveServerMediaSubsession::seekStream( unsigned clientSessionId, void* streamToken, char*& absStart, char*& absEnd )
{
   logd("H264VideoLiveServerMediaSubsession::pauseStream \n");

   StreamState* streamState = (StreamState*)streamToken;

   if (streamState != NULL) 
   {
      RTSP_PlayCtrl PlayCtrl;
      PlayCtrl.strStime = ("");
      PlayCtrl.strEtime = ("");
      PlayCtrl.strReqTime = ("");
      PlayCtrl.strPlayStream = ("");
      PlayCtrl.nScale = 0;
      PlayCtrl.bRate = FALSE;

      PlayCtrl.strStime = absStart;
      PlayCtrl.strEtime = absEnd;
      if(fStreamingChannel)
      {
         PlayCtrl.nScale = 1;
         fStreamingChannel->GetServer()->OnStreamPlayCtrl(fStreamingChannel->GetServer(),fStreamingChannel,PlayCtrl);
      }
   }
}

void VAMetaDataLiveServerMediaSubsession::setStreamScale( unsigned clientSessionId, void* streamToken, float scale )
{
   StreamState* streamState = (StreamState*)streamToken;

   if (streamState != NULL) 
   {
      RTSP_PlayCtrl PlayCtrl;
      PlayCtrl.strStime = ("");
      PlayCtrl.strEtime = ("");
      PlayCtrl.strReqTime = ("");
      PlayCtrl.strPlayStream = ("");
      PlayCtrl.nScale = 0;
      PlayCtrl.bRate = FALSE;

      PlayCtrl.nScale = scale;
      if(fStreamingChannel)
      {
         PlayCtrl.nScale = 1;
         fStreamingChannel->GetServer()->OnStreamPlayCtrl(fStreamingChannel->GetServer(),fStreamingChannel,PlayCtrl);
      }
   }
}


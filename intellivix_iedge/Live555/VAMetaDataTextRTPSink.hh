/**********
This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation; either version 2.1 of the License, or (at your
option) any later version. (See <http://www.gnu.org/copyleft/lesser.html>.)

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General Public License
along with this library; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
**********/
// "liveMedia"
// Copyright (c) 1996-2015 Live Networks, Inc.  All rights reserved.
// RTP sink for T.140 text (RFC 2793)
// C++ header

#ifndef _VA_META_DATA_TEXT_RTP_SINK_HH
#define _VA_META_DATA_TEXT_RTP_SINK_HH

#ifndef _VA_META_DATA_RTP_SINK_HH
#include "VAMetaDataRTPSink.hh"
#endif
#ifndef _FRAMED_FILTER_HH
#include "FramedFilter.hh"
#endif

class VAMetaDataIdleFilter;

class VAMetaDataTextRTPSink: public VAMetaDataRTPSink {
public:
  static VAMetaDataTextRTPSink* createNew(UsageEnvironment& env, Groupsock* RTPgs, unsigned char rtpPayloadFormat);

protected:
  VAMetaDataTextRTPSink(UsageEnvironment& env, Groupsock* RTPgs, unsigned char rtpPayloadFormat);
	// called only by createNew()

  virtual ~VAMetaDataTextRTPSink();

protected: // redefined virtual functions:
  virtual Boolean continuePlaying();
  virtual void doSpecialFrameHandling(unsigned fragmentationOffset,
                                      unsigned char* frameStart,
                                      unsigned numBytesInFrame,
                                      struct timeval framePresentationTime,
                                      unsigned numRemainingBytes);
  virtual Boolean frameCanAppearAfterPacketStart(unsigned char const* frameStart,
						 unsigned numBytesInFrame) const;

protected:
  VAMetaDataIdleFilter* fOurIdleFilter;
  Boolean fAreInIdlePeriod;
};


////////// VAMetaDataIdleFilter definition //////////

// Because the T.140 text RTP payload format specification recommends that (empty) RTP packets be sent during 'idle periods'
// when no new text is available, we implement "VAMetaDataTextRTPSink" using a separate "VAMetaDataIdleFilter" class - sitting in front
// -  that delivers, to the "VAMetaDataTextRTPSink", a continuous sequence of (possibly) empty frames.
// (Note: This class should be used only by "VAMetaDataTextRTPSink", or a subclass.)

class VAMetaDataIdleFilter: public FramedFilter {
public:
  VAMetaDataIdleFilter(UsageEnvironment& env, FramedSource* inputSource);
  virtual ~VAMetaDataIdleFilter();

private: // redefined virtual functions:
  virtual void doGetNextFrame();
  virtual void doStopGettingFrames();

private:
  static void afterGettingFrame(void* clientData, unsigned frameSize,
				unsigned numTruncatedBytes,
                                struct timeval presentationTime,
                                unsigned durationInMicroseconds);
  void afterGettingFrame(unsigned frameSize,
			 unsigned numTruncatedBytes,
			 struct timeval presentationTime,
			 unsigned durationInMicroseconds);

  static void handleIdleTimeout(void* clientData);
  void handleIdleTimeout();

  void deliverFromBuffer();
  void deliverEmptyFrame();

  static void onSourceClosure(void* clientData);
  void onSourceClosure();

private:
  TaskToken fIdleTimerTask;
  unsigned fBufferSize, fNumBufferedBytes;
  char* fBuffer;
  unsigned fBufferedNumTruncatedBytes; // a count of truncated bytes from the upstream
  struct timeval fBufferedDataPresentationTime;
  unsigned fBufferedDataDurationInMicroseconds;
};

#endif

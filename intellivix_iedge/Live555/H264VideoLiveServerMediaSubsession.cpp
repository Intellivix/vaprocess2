/**********
This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation; either version 2.1 of the License, or (at your
option) any later version. (See <http://www.gnu.org/copyleft/lesser.html>.)

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General Public License
along with this library; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
**********/
// "liveMedia"
// Copyright (c) 1996-2015 Live Networks, Inc.  All rights reserved.
// A 'ServerMediaSubsession' object that creates new, unicast, "RTPSink"s
// on demand, from a H264 video file.
// Implementation

#include "stdafx.h"
#include "H264VideoLiveServerMediaSubsession.hh"
#include "H264VideoRTPSink.hh"
#include "ByteStreamLiveSource.hh"
#include "ByteStreamFileSource.hh"
#include "H264VideoStreamFramer.hh"


H264VideoLiveServerMediaSubsession*
H264VideoLiveServerMediaSubsession::createNew( UsageEnvironment& env,
                                               CRTSPStreamingChannel* streamingChannel,
                                               char const* fileName,
                                               Boolean reuseFirstSource )
{
   return new H264VideoLiveServerMediaSubsession( env, streamingChannel, fileName, reuseFirstSource );
}

H264VideoLiveServerMediaSubsession::H264VideoLiveServerMediaSubsession( UsageEnvironment& env,
                                                                        CRTSPStreamingChannel* streamingChannel,
                                                                        char const* fileName,
                                                                        Boolean reuseFirstSource )
    : FileServerMediaSubsession( env, fileName, reuseFirstSource )
    , fStreamingChannel( streamingChannel )
    , fAuxSDPLine( NULL )
    , fDoneFlag( 0 )
    , fDummyRTPSink( NULL )
{
}

H264VideoLiveServerMediaSubsession::~H264VideoLiveServerMediaSubsession()
{
   delete[] fAuxSDPLine;
}

static void afterPlayingDummy( void* clientData )
{
   H264VideoLiveServerMediaSubsession* subsess = (H264VideoLiveServerMediaSubsession*)clientData;
   subsess->afterPlayingDummy1();
}

void H264VideoLiveServerMediaSubsession::afterPlayingDummy1()
{
   // Unschedule any pending 'checking' task:
   envir().taskScheduler().unscheduleDelayedTask( nextTask() );
   // Signal the event loop that we're done:
   setDoneFlag();
}

static void checkForAuxSDPLine( void* clientData )
{
   H264VideoLiveServerMediaSubsession* subsess = (H264VideoLiveServerMediaSubsession*)clientData;
   subsess->checkForAuxSDPLine1();
}

void H264VideoLiveServerMediaSubsession::checkForAuxSDPLine1()
{
   char const* dasl;

   if( fAuxSDPLine != NULL ) {
      // Signal the event loop that we're done:
      setDoneFlag();
   } else if( fDummyRTPSink != NULL && ( dasl = fDummyRTPSink->auxSDPLine() ) != NULL ) {
      fAuxSDPLine   = strDup( dasl );
      fDummyRTPSink = NULL;

      // Signal the event loop that we're done:
      setDoneFlag();
   } else if( !fDoneFlag ) {
      // try again after a brief delay:
      // jun: 주석처리하였음.
      //int uSecsToDelay = 100000; // 100 ms
      //nextTask() = envir().taskScheduler().scheduleDelayedTask(uSecsToDelay,(TaskFunc*)checkForAuxSDPLine, this);
   }
}

char const* H264VideoLiveServerMediaSubsession::getAuxSDPLine( RTPSink* rtpSink, FramedSource* inputSource )
{
   if( fAuxSDPLine != NULL ) return fAuxSDPLine; // it's already been set up (for a previous client)

   if( fDummyRTPSink == NULL ) { // we're not already setting it up for another, concurrent stream
      // Note: For H264 video files, the 'config' information ("profile-level-id" and "sprop-parameter-sets") isn't known
      // until we start reading the file.  This means that "rtpSink"s "auxSDPLine()" will be NULL initially,
      // and we need to start reading data from our file until this changes.
      fDummyRTPSink = rtpSink;

      // Start reading the file:
      fDummyRTPSink->startPlaying( *inputSource, afterPlayingDummy, this );

      // Check whether the sink's 'auxSDPLine()' is ready:
      checkForAuxSDPLine( this );
   }

   // jun : 이곳에서 이벤트 루프를 돌리면 재귀적 처리로 인해 다운되는 현상이 발생될 수 있다.
   //envir().taskScheduler().doEventLoop(&fDoneFlag);

   return fAuxSDPLine;
}

FramedSource* H264VideoLiveServerMediaSubsession::createNewStreamSource( unsigned /*clientSessionId*/, unsigned& estBitrate )
{
   estBitrate = 50000; // kbps, estimate
   // jun : 라이브 소스용 스트림을 만듭니다.
   // Create the video source:
   ByteStreamLiveSource* liveSource = ByteStreamLiveSource::createNew( envir(), fStreamingChannel, fFileName, 100000 );
   if( liveSource == NULL ) return NULL;
   fbMediaSource = True;
   // Create a framer for the Video Elementary Stream:
   return H264VideoStreamFramer::createNew( envir(), liveSource );
}

RTPSink* H264VideoLiveServerMediaSubsession::createNewRTPSink( Groupsock* rtpGroupsock,
                                                               unsigned char rtpPayloadTypeIfDynamic,
                                                               FramedSource* /*inputSource*/ )
{
   return H264VideoRTPSink::createNew( envir(), rtpGroupsock, rtpPayloadTypeIfDynamic );
}

//void H264VideoLiveServerMediaSubsession::deleteStream( unsigned clientSessionId, void*& streamToken )
//{
//   int nTime(0);
//
//   if (!fStreamingChannel->IsVideoStreamRequested() && !fStreamingChannel->IsVAMetaDataRequested())
//   {
//      if(fStreamingChannel->GetServer())
//      fStreamingChannel->GetServer()->OnStreamingChannelClosed(fStreamingChannel->GetServer()->m_pParam, fStreamingChannel);
//   }
//}

void H264VideoLiveServerMediaSubsession::pausePlay( unsigned clientSessionId, void* streamToken )
{
   logd( "H264VideoLiveServerMediaSubsession::pauseStream \n" );

   StreamState* streamState = (StreamState*)streamToken;

   if( streamState != NULL ) {
      RTSP_PlayCtrl PlayCtrl;
      PlayCtrl.strStime      = ( "" );
      PlayCtrl.strEtime      = ( "" );
      PlayCtrl.strReqTime    = ( "" );
      PlayCtrl.strPlayStream = ( "" );
      PlayCtrl.nScale        = 0;
      PlayCtrl.bRate         = FALSE;

      if( fStreamingChannel ) {
         fStreamingChannel->GetServer()->OnStreamPlayCtrl( fStreamingChannel->GetServer()->m_pParam, fStreamingChannel, PlayCtrl );
      }
   }
}

void H264VideoLiveServerMediaSubsession::startStream( unsigned clientSessionId, void* streamToken, TaskFunc* rtcpRRHandler, void* rtcpRRHandlerClientData, unsigned short& rtpSeqNum, unsigned& rtpTimestamp, ServerRequestAlternativeByteHandler* serverRequestAlternativeByteHandler, void* serverRequestAlternativeByteHandlerClientData )
{

   StreamState* streamState = (StreamState*)streamToken;
   Destinations* destinations
       = (Destinations*)( fDestinationsHashTable->Lookup( (char const*)clientSessionId ) );
   if( streamState != NULL ) {
      streamState->startPlaying( destinations,
                                 rtcpRRHandler, rtcpRRHandlerClientData,
                                 serverRequestAlternativeByteHandler, serverRequestAlternativeByteHandlerClientData );
      RTPSink* rtpSink = streamState->rtpSink(); // alias
      if( rtpSink != NULL ) {
         rtpSeqNum    = rtpSink->currentSeqNo();
         rtpTimestamp = rtpSink->presetNextTimestamp();
      }
   }
}

void H264VideoLiveServerMediaSubsession::seekStream( unsigned clientSessionId, void* streamToken, char*& absStart, char*& absEnd )
{
   logd( "H264VideoLiveServerMediaSubsession::pauseStream \n" );

   StreamState* streamState = (StreamState*)streamToken;

   if( streamState != NULL ) {
      RTSP_PlayCtrl PlayCtrl;
      PlayCtrl.strStime      = ( "" );
      PlayCtrl.strEtime      = ( "" );
      PlayCtrl.strReqTime    = ( "" );
      PlayCtrl.strPlayStream = ( "" );
      PlayCtrl.nScale        = 0;
      PlayCtrl.bRate         = FALSE;

      PlayCtrl.strStime = absStart;
      PlayCtrl.strEtime = absEnd;
      if( fStreamingChannel ) {
         PlayCtrl.nScale = 1;
         fStreamingChannel->GetServer()->OnStreamPlayCtrl( fStreamingChannel->GetServer()->m_pParam, fStreamingChannel, PlayCtrl );
      }
   }
}

//void H264VideoLiveServerMediaSubsession::seekStream( unsigned clientSessionId, void* streamToken, double& seekNPT, double streamDuration, u_int64_t& numBytes )
//{
//
//}

void H264VideoLiveServerMediaSubsession::setStreamScale( unsigned clientSessionId, void* streamToken, float scale )
{
   StreamState* streamState = (StreamState*)streamToken;

   if( streamState != NULL ) {
      RTSP_PlayCtrl PlayCtrl;
      PlayCtrl.strStime      = ( "" );
      PlayCtrl.strEtime      = ( "" );
      PlayCtrl.strReqTime    = ( "" );
      PlayCtrl.strPlayStream = ( "" );
      PlayCtrl.nScale        = 0;
      PlayCtrl.bRate         = FALSE;

      PlayCtrl.nScale = scale;
      if( fStreamingChannel ) {
         //PlayCtrl.nScale = scale;
         fStreamingChannel->GetServer()->OnStreamPlayCtrl( fStreamingChannel->GetServer()->m_pParam, fStreamingChannel, PlayCtrl );
      }
   }
}

void H264VideoLiveServerMediaSubsession::testScaleFactor( float& scale )
{
   RTSP_PlayCtrl PlayCtrl;
   PlayCtrl.strStime      = ( "" );
   PlayCtrl.strEtime      = ( "" );
   PlayCtrl.strReqTime    = ( "" );
   PlayCtrl.strPlayStream = ( "" );
   PlayCtrl.nScale        = 1.0;
   PlayCtrl.bRate         = FALSE;

   if( fStreamingChannel ) {
      if( fStreamingChannel->m_fScale == 0 ) {
         PlayCtrl.nScale = 1;
         fStreamingChannel->GetServer()->OnStreamPlayCtrl( fStreamingChannel->GetServer()->m_pParam, fStreamingChannel, PlayCtrl );
      }
   }
}

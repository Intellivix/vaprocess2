#include "stdafx.h"
#include <stdio.h>
//#include <conio.h>
#include <deque>
#include "RTSPCustomQueue.h"

#pragma pack (8)

//////////////////////////////////////////////////////
//
// Class : CRTSPVideoItem
//
//////////////////////////////////////////////////////

CRTSPVideoItem::CRTSPVideoItem (   )
{
   m_nCodecID   = LIVE555_VCODEC_ID_NONE;
   m_fFrameRate = 30.0f;
   m_dwFlag     = 0;
}

CRTSPVideoItem::~CRTSPVideoItem (   )
{
}

CRTSPVideoItem& CRTSPVideoItem::operator= (CRTSPVideoItem& from)
{
   m_nCodecID         = from.m_nCodecID;
   m_dwFlag           = from.m_dwFlag;
   m_fFrameRate       = from.m_fFrameRate;
   m_PresentationTime = from.m_PresentationTime;

   int nCompBuffLen = from.GetLength ();
   if (nCompBuffLen)
   {
      SetLength (nCompBuffLen);
      Write (from.GetBuffer (), 1, nCompBuffLen);
   }
   return *this;
}

//////////////////////////////////////////////////////
//
// Class : CRTSPVAMetaData
//
//////////////////////////////////////////////////////

CRTSPVAMetaData::CRTSPVAMetaData (   )
{
   m_fFrameRate = 30.0f;
   m_PresentationTime.tv_sec  = 0;
   m_PresentationTime.tv_usec = 0;
   m_VAMetaDataFlag = 0;
}

CRTSPVAMetaData::~CRTSPVAMetaData (   )
{
}

CRTSPVAMetaData& CRTSPVAMetaData::operator= (CRTSPVAMetaData& from)
{
   m_fFrameRate       = from.m_fFrameRate;
   m_PresentationTime = from.m_PresentationTime;

   int nCompBuffLen = from.GetLength ();
   if (nCompBuffLen)
   {
      SetLength (nCompBuffLen);
      Write (from.GetBuffer (), 1, nCompBuffLen);
   }
   return *this;
}

//////////////////////////////////////////////////////
//
// Global Functions
//
//////////////////////////////////////////////////////

void DeleteUntileKeyFrame (CRTSPVideoQueue& queue)
{
   int i;

   queue.Lock ();
   // 키프레임을 역순으로 찾고 찾은 다음 프레임 들은 모두 삭제한다.
   int nItem = (int) queue.size ();
   BOOL bFindKeyFrame = FALSE;
   if (nItem) {
      std::deque<CRTSPVideoItem*>::iterator iter      = queue.end ();
      std::deque<CRTSPVideoItem*>::iterator iterBegin = queue.begin ();
      int nItemIdx = nItem;
      int nFindItemIdx = -1;
      do {
         iter--; nItemIdx--;
         CRTSPVideoItem* pItem = *iter;
         if (FALSE == bFindKeyFrame) {
            if (pItem->m_dwFlag & LIVE555_VFRM_KEYFRAME) {
               bFindKeyFrame = TRUE;
               nFindItemIdx = nItemIdx;
               break;
            }
         }
      } while (iter != iterBegin);

      if (nFindItemIdx >= 0) {
         for (i = 0; i < nFindItemIdx; i++) {
            CRTSPVideoItem* pItem = queue.front (   );
            if (pItem) delete pItem;
            queue.pop_front (   );
         }
      }
   }
   queue.Unlock (   );
}

#pragma pack()

TARGET = Live555
TEMPLATE = lib
include( ../env.pri )

INCLUDEPATH += ../BCCL
DEPENDPATH += ../BCCL
PRE_TARGETDEPS += $$DESTDIR/libBCCL.a

HEADERS = \
   $$PWD/AACAudioMatroskaFileServerMediaSubsession.hh \
   $$PWD/AC3AudioFileServerMediaSubsession.hh \
   $$PWD/AC3AudioMatroskaFileServerMediaSubsession.hh \
   $$PWD/AC3AudioRTPSink.hh \
   $$PWD/AC3AudioRTPSource.hh \
   $$PWD/AC3AudioStreamFramer.hh \
   $$PWD/ADTSAudioFileServerMediaSubsession.hh \
   $$PWD/ADTSAudioFileSource.hh \
   $$PWD/AMRAudioFileServerMediaSubsession.hh \
   $$PWD/AMRAudioFileSink.hh \
   $$PWD/AMRAudioFileSource.hh \
   $$PWD/AMRAudioRTPSink.hh \
   $$PWD/AMRAudioRTPSource.hh \
   $$PWD/AMRAudioSource.hh \
   $$PWD/AudioCustomSink.h \
   $$PWD/AudioInputDevice.hh \
   $$PWD/AudioRTPSink.hh \
   $$PWD/AVIFileSink.hh \
   $$PWD/Base64.hh \
   $$PWD/BasicHashTable.hh \
   $$PWD/BasicUDPSink.hh \
   $$PWD/BasicUDPSource.hh \
   $$PWD/BasicUsageEnvironment.hh \
   $$PWD/BasicUsageEnvironment0.hh \
   $$PWD/BasicUsageEnvironment_version.hh \
   $$PWD/BitVector.hh \
   $$PWD/Boolean.hh \
   $$PWD/ByteStreamFileSource.hh \
   $$PWD/ByteStreamLiveSource.hh \
   $$PWD/ByteStreamMemoryBufferSource.hh \
   $$PWD/ByteStreamMultiFileSource.hh \
   $$PWD/DarwinInjector.hh \
   $$PWD/DelayQueue.hh \
   $$PWD/DeviceSource.hh \
   $$PWD/DigestAuthentication.hh \
   $$PWD/DVVideoFileServerMediaSubsession.hh \
   $$PWD/DVVideoRTPSink.hh \
   $$PWD/DVVideoRTPSource.hh \
   $$PWD/DVVideoStreamFramer.hh \
   $$PWD/DynamicRTSPServer.hh \
   $$PWD/EBMLNumber.hh \
   $$PWD/FileServerMediaSubsession.hh \
   $$PWD/FileSink.hh \
   $$PWD/FramedFileSource.hh \
   $$PWD/FramedFilter.hh \
   $$PWD/FramedSource.hh \
   $$PWD/GroupEId.hh \
   $$PWD/Groupsock.hh \
   $$PWD/groupsock_version.hh \
   $$PWD/GroupsockHelper.hh \
   $$PWD/GSMAudioRTPSink.hh \
   $$PWD/H261VideoRTPSource.hh \
   $$PWD/H263plusVideoFileServerMediaSubsession.hh \
   $$PWD/H263plusVideoRTPSink.hh \
   $$PWD/H263plusVideoRTPSource.hh \
   $$PWD/H263plusVideoStreamFramer.hh \
   $$PWD/H263plusVideoStreamParser.hh \
   $$PWD/H264CustomSink.h \
   $$PWD/H264VideoFileServerMediaSubsession.hh \
   $$PWD/H264VideoFileSink.hh \
   $$PWD/H264VideoLiveServerMediaSubsession.hh \
   $$PWD/H264VideoMatroskaFileServerMediaSubsession.hh \
   $$PWD/H264VideoRTPSink.hh \
   $$PWD/H264VideoRTPSource.hh \
   $$PWD/H264VideoStreamDiscreteFramer.hh \
   $$PWD/H264VideoStreamFramer.hh \
   $$PWD/HandlerSet.hh \
   $$PWD/HashTable.hh \
   $$PWD/InputFile.hh \
   $$PWD/IOHandlers.hh \
   $$PWD/JPEGVideoRTPSink.hh \
   $$PWD/JPEGVideoRTPSource.hh \
   $$PWD/JPEGVideoSource.hh \
   $$PWD/Live555Define.h \
   $$PWD/liveMedia.hh \
   $$PWD/liveMedia_version.hh \
   $$PWD/Locale.hh \
   $$PWD/MatroskaDemuxedTrack.hh \
   $$PWD/MatroskaFile.hh \
   $$PWD/MatroskaFileParser.hh \
   $$PWD/MatroskaFileServerDemux.hh \
   $$PWD/Media.hh \
   $$PWD/MediaSession.hh \
   $$PWD/MediaSink.hh \
   $$PWD/MediaSource.hh \
   $$PWD/MetadataCustomSink.h \
   $$PWD/MJPEGCustomSink.h \
   $$PWD/MP3ADU.hh \
   $$PWD/MP3ADUdescriptor.hh \
   $$PWD/MP3ADUinterleaving.hh \
   $$PWD/MP3ADURTPSink.hh \
   $$PWD/MP3ADURTPSource.hh \
   $$PWD/MP3ADUTranscoder.hh \
   $$PWD/MP3AudioFileServerMediaSubsession.hh \
   $$PWD/MP3AudioMatroskaFileServerMediaSubsession.hh \
   $$PWD/MP3FileSource.hh \
   $$PWD/MP3Internals.hh \
   $$PWD/MP3InternalsHuffman.hh \
   $$PWD/MP3StreamState.hh \
   $$PWD/MP3Transcoder.hh \
   $$PWD/MPEG1or2AudioRTPSink.hh \
   $$PWD/MPEG1or2AudioRTPSource.hh \
   $$PWD/MPEG1or2AudioStreamFramer.hh \
   $$PWD/MPEG1or2Demux.hh \
   $$PWD/MPEG1or2DemuxedElementaryStream.hh \
   $$PWD/MPEG1or2DemuxedServerMediaSubsession.hh \
   $$PWD/MPEG1or2FileServerDemux.hh \
   $$PWD/MPEG1or2VideoFileServerMediaSubsession.hh \
   $$PWD/MPEG1or2VideoRTPSink.hh \
   $$PWD/MPEG1or2VideoRTPSource.hh \
   $$PWD/MPEG1or2VideoStreamDiscreteFramer.hh \
   $$PWD/MPEG1or2VideoStreamFramer.hh \
   $$PWD/MPEG2IndexFromTransportStream.hh \
   $$PWD/MPEG2TransportFileServerMediaSubsession.hh \
   $$PWD/MPEG2TransportStreamFramer.hh \
   $$PWD/MPEG2TransportStreamFromESSource.hh \
   $$PWD/MPEG2TransportStreamFromPESSource.hh \
   $$PWD/MPEG2TransportStreamIndexFile.hh \
   $$PWD/MPEG2TransportStreamMultiplexor.hh \
   $$PWD/MPEG2TransportStreamTrickModeFilter.hh \
   $$PWD/MPEG2TransportUDPServerMediaSubsession.hh \
   $$PWD/MPEG4ESCustomVideoRTPSink.h \
   $$PWD/MPEG4ESVideoRTPSink.hh \
   $$PWD/MPEG4ESVideoRTPSource.hh \
   $$PWD/MPEG4GenericRTPSink.hh \
   $$PWD/MPEG4GenericRTPSource.hh \
   $$PWD/MPEG4LATMAudioRTPSink.hh \
   $$PWD/MPEG4LATMAudioRTPSource.hh \
   $$PWD/MPEG4VideoFileServerMediaSubsession.hh \
   $$PWD/MPEG4VideoStreamDiscreteFramer.hh \
   $$PWD/MPEG4VideoStreamFramer.hh \
   $$PWD/MPEGVideoStreamFramer.hh \
   $$PWD/MPEGVideoStreamParser.hh \
   $$PWD/MultiFramedRTPSink.hh \
   $$PWD/MultiFramedRTPSource.hh \
   $$PWD/NetAddress.hh \
   $$PWD/NetCommon.h \
   $$PWD/NetInterface.hh \
   $$PWD/OnDemandServerMediaSubsession.hh \
   $$PWD/ourMD5.hh \
   $$PWD/OutputFile.hh \
   $$PWD/PassiveServerMediaSubsession.hh \
   $$PWD/ProxyServerMediaSession.hh \
   $$PWD/QCELPAudioRTPSource.hh \
   $$PWD/QuickTimeFileSink.hh \
   $$PWD/QuickTimeGenericRTPSource.hh \
   $$PWD/RTCP.hh \
   $$PWD/rtcp_from_spec.h \
   $$PWD/RTPInterface.hh \
   $$PWD/RTPSink.hh \
   $$PWD/RTPSource.hh \
   $$PWD/RTSPClient.hh \
   $$PWD/RTSPCommon.hh \
   $$PWD/RTSPCustomClient.h \
   $$PWD/RTSPCustomQueue.h \
   $$PWD/RTSPCustomServer.h \
   $$PWD/RTSPRegisterSender.hh \
   $$PWD/RTSPServer.hh \
   $$PWD/RTSPServerSupportingHTTPStreaming.hh \
   $$PWD/ServerMediaSession.hh \
   $$PWD/SimpleRTPSink.hh \
   $$PWD/SimpleRTPSource.hh \
   $$PWD/SIPClient.hh \
   $$PWD/stdafx.h \
   $$PWD/strDup.hh \
   $$PWD/StreamParser.hh \
   $$PWD/StreamReplicator.hh \
   $$PWD/T140TextMatroskaFileServerMediaSubsession.hh \
   $$PWD/T140TextRTPSink.hh \
   $$PWD/TCPStreamSink.hh \
   $$PWD/TextRTPSink.hh \
   $$PWD/TheoraVideoRTPSink.hh \
   $$PWD/TunnelEncaps.hh \
   $$PWD/uLawAudioFilter.hh \
   $$PWD/UsageEnvironment.hh \
   $$PWD/UsageEnvironment_version.hh \
   $$PWD/VAMetaDataLiveServerMediaSubsession.hh \
   $$PWD/VAMetaDataLiveSource.hh \
   $$PWD/VAMetaDataRTPSink.hh \
   $$PWD/VAMetaDataTextRTPSink.hh \
   $$PWD/VideoRTPSink.hh \
   $$PWD/VorbisAudioMatroskaFileServerMediaSubsession.hh \
   $$PWD/VorbisAudioRTPSink.hh \
   $$PWD/VorbisAudioRTPSource.hh \
   $$PWD/VP8VideoMatroskaFileServerMediaSubsession.hh \
   $$PWD/VP8VideoRTPSink.hh \
   $$PWD/VP8VideoRTPSource.hh \
   $$PWD/WAVAudioFileServerMediaSubsession.hh \
   $$PWD/WAVAudioFileSource.hh

SOURCES = \
   $$PWD/AACAudioMatroskaFileServerMediaSubsession.cpp \
   $$PWD/AC3AudioFileServerMediaSubsession.cpp \
   $$PWD/AC3AudioMatroskaFileServerMediaSubsession.cpp \
   $$PWD/AC3AudioRTPSink.cpp \
   $$PWD/AC3AudioRTPSource.cpp \
   $$PWD/AC3AudioStreamFramer.cpp \
   $$PWD/ADTSAudioFileServerMediaSubsession.cpp \
   $$PWD/ADTSAudioFileSource.cpp \
   $$PWD/AMRAudioFileServerMediaSubsession.cpp \
   $$PWD/AMRAudioFileSink.cpp \
   $$PWD/AMRAudioFileSource.cpp \
   $$PWD/AMRAudioRTPSink.cpp \
   $$PWD/AMRAudioRTPSource.cpp \
   $$PWD/AMRAudioSource.cpp \
   $$PWD/AudioCustomSink.cpp \
   $$PWD/AudioInputDevice.cpp \
   $$PWD/AudioRTPSink.cpp \
   $$PWD/AVIFileSink.cpp \
   $$PWD/Base64.cpp \
   $$PWD/BasicHashTable.cpp \
   $$PWD/BasicTaskScheduler.cpp \
   $$PWD/BasicTaskScheduler0.cpp \
   $$PWD/BasicUDPSink.cpp \
   $$PWD/BasicUDPSource.cpp \
   $$PWD/BasicUsageEnvironment.cpp \
   $$PWD/BasicUsageEnvironment0.cpp \
   $$PWD/BitVector.cpp \
   $$PWD/ByteStreamFileSource.cpp \
   $$PWD/ByteStreamLiveSource.cpp \
   $$PWD/ByteStreamMemoryBufferSource.cpp \
   $$PWD/ByteStreamMultiFileSource.cpp \
   $$PWD/DarwinInjector.cpp \
   $$PWD/DelayQueue.cpp \
   $$PWD/DeviceSource.cpp \
   $$PWD/DigestAuthentication.cpp \
   $$PWD/DVVideoFileServerMediaSubsession.cpp \
   $$PWD/DVVideoRTPSink.cpp \
   $$PWD/DVVideoRTPSource.cpp \
   $$PWD/DVVideoStreamFramer.cpp \
   $$PWD/DynamicRTSPServer.cpp \
   $$PWD/EBMLNumber.cpp \
   $$PWD/FileServerMediaSubsession.cpp \
   $$PWD/FileSink.cpp \
   $$PWD/FramedFileSource.cpp \
   $$PWD/FramedFilter.cpp \
   $$PWD/FramedSource.cpp \
   $$PWD/GroupEId.cpp \
   $$PWD/Groupsock.cpp \
   $$PWD/GroupsockHelper.cpp \
   $$PWD/GSMAudioRTPSink.cpp \
   $$PWD/H261VideoRTPSource.cpp \
   $$PWD/H263plusVideoFileServerMediaSubsession.cpp \
   $$PWD/H263plusVideoRTPSink.cpp \
   $$PWD/H263plusVideoRTPSource.cpp \
   $$PWD/H263plusVideoStreamFramer.cpp \
   $$PWD/H263plusVideoStreamParser.cpp \
   $$PWD/H264CustomSink.cpp \
   $$PWD/H264VideoFileServerMediaSubsession.cpp \
   $$PWD/H264VideoFileSink.cpp \
   $$PWD/H264VideoLiveServerMediaSubsession.cpp \
   $$PWD/H264VideoMatroskaFileServerMediaSubsession.cpp \
   $$PWD/H264VideoRTPSink.cpp \
   $$PWD/H264VideoRTPSource.cpp \
   $$PWD/H264VideoStreamDiscreteFramer.cpp \
   $$PWD/H264VideoStreamFramer.cpp \
   $$PWD/HashTable.cpp \
   $$PWD/inet.c \
   $$PWD/InputFile.cpp \
   $$PWD/IOHandlers.cpp \
   $$PWD/JPEGVideoRTPSink.cpp \
   $$PWD/JPEGVideoRTPSource.cpp \
   $$PWD/JPEGVideoSource.cpp \
   $$PWD/Live555Define.cpp \
   $$PWD/Locale.cpp \
   $$PWD/MatroskaDemuxedTrack.cpp \
   $$PWD/MatroskaFile.cpp \
   $$PWD/MatroskaFileParser.cpp \
   $$PWD/MatroskaFileServerDemux.cpp \
   $$PWD/Media.cpp \
   $$PWD/MediaSession.cpp \
   $$PWD/MediaSink.cpp \
   $$PWD/MediaSource.cpp \
   $$PWD/MetadataCustomSink.cpp \
   $$PWD/MJPEGCustomSink.cpp \
   $$PWD/MP3ADU.cpp \
   $$PWD/MP3ADUdescriptor.cpp \
   $$PWD/MP3ADUinterleaving.cpp \
   $$PWD/MP3ADURTPSink.cpp \
   $$PWD/MP3ADURTPSource.cpp \
   $$PWD/MP3ADUTranscoder.cpp \
   $$PWD/MP3AudioFileServerMediaSubsession.cpp \
   $$PWD/MP3AudioMatroskaFileServerMediaSubsession.cpp \
   $$PWD/MP3FileSource.cpp \
   $$PWD/MP3Internals.cpp \
   $$PWD/MP3InternalsHuffman.cpp \
   $$PWD/MP3InternalsHuffmanTable.cpp \
   $$PWD/MP3StreamState.cpp \
   $$PWD/MP3Transcoder.cpp \
   $$PWD/MPEG1or2AudioRTPSink.cpp \
   $$PWD/MPEG1or2AudioRTPSource.cpp \
   $$PWD/MPEG1or2AudioStreamFramer.cpp \
   $$PWD/MPEG1or2Demux.cpp \
   $$PWD/MPEG1or2DemuxedElementaryStream.cpp \
   $$PWD/MPEG1or2DemuxedServerMediaSubsession.cpp \
   $$PWD/MPEG1or2FileServerDemux.cpp \
   $$PWD/MPEG1or2VideoFileServerMediaSubsession.cpp \
   $$PWD/MPEG1or2VideoRTPSink.cpp \
   $$PWD/MPEG1or2VideoRTPSource.cpp \
   $$PWD/MPEG1or2VideoStreamDiscreteFramer.cpp \
   $$PWD/MPEG1or2VideoStreamFramer.cpp \
   $$PWD/MPEG2IndexFromTransportStream.cpp \
   $$PWD/MPEG2TransportFileServerMediaSubsession.cpp \
   $$PWD/MPEG2TransportStreamFramer.cpp \
   $$PWD/MPEG2TransportStreamFromESSource.cpp \
   $$PWD/MPEG2TransportStreamFromPESSource.cpp \
   $$PWD/MPEG2TransportStreamIndexFile.cpp \
   $$PWD/MPEG2TransportStreamMultiplexor.cpp \
   $$PWD/MPEG2TransportStreamTrickModeFilter.cpp \
   $$PWD/MPEG2TransportUDPServerMediaSubsession.cpp \
   $$PWD/MPEG4ESCustomVideoRTPSink.cpp \
   $$PWD/MPEG4ESVideoRTPSink.cpp \
   $$PWD/MPEG4ESVideoRTPSource.cpp \
   $$PWD/MPEG4GenericRTPSink.cpp \
   $$PWD/MPEG4GenericRTPSource.cpp \
   $$PWD/MPEG4LATMAudioRTPSink.cpp \
   $$PWD/MPEG4LATMAudioRTPSource.cpp \
   $$PWD/MPEG4VideoFileServerMediaSubsession.cpp \
   $$PWD/MPEG4VideoStreamDiscreteFramer.cpp \
   $$PWD/MPEG4VideoStreamFramer.cpp \
   $$PWD/MPEGVideoStreamFramer.cpp \
   $$PWD/MPEGVideoStreamParser.cpp \
   $$PWD/MultiFramedRTPSink.cpp \
   $$PWD/MultiFramedRTPSource.cpp \
   $$PWD/NetAddress.cpp \
   $$PWD/NetInterface.cpp \
   $$PWD/OnDemandServerMediaSubsession.cpp \
   $$PWD/ourMD5.cpp \
   $$PWD/OutputFile.cpp \
   $$PWD/PassiveServerMediaSubsession.cpp \
   $$PWD/ProxyServerMediaSession.cpp \
   $$PWD/QCELPAudioRTPSource.cpp \
   $$PWD/QuickTimeFileSink.cpp \
   $$PWD/QuickTimeGenericRTPSource.cpp \
   $$PWD/RTCP.cpp \
   $$PWD/rtcp_from_spec.c \
   $$PWD/RTPInterface.cpp \
   $$PWD/RTPSink.cpp \
   $$PWD/RTPSource.cpp \
   $$PWD/RTSPClient.cpp \
   $$PWD/RTSPCommon.cpp \
   $$PWD/RTSPCustomClient.cpp \
   $$PWD/RTSPCustomQueue.cpp \
   $$PWD/RTSPCustomServer.cpp \
   $$PWD/RTSPRegisterSender.cpp \
   $$PWD/RTSPServer.cpp \
   $$PWD/RTSPServerSupportingHTTPStreaming.cpp \
   $$PWD/ServerMediaSession.cpp \
   $$PWD/SimpleRTPSink.cpp \
   $$PWD/SimpleRTPSource.cpp \
   $$PWD/SIPClient.cpp \
   $$PWD/stdafx.cpp \
   $$PWD/strDup.cpp \
   $$PWD/StreamParser.cpp \
   $$PWD/StreamReplicator.cpp \
   $$PWD/T140TextMatroskaFileServerMediaSubsession.cpp \
   $$PWD/T140TextRTPSink.cpp \
   $$PWD/TCPStreamSink.cpp \
   $$PWD/TextRTPSink.cpp \
   $$PWD/TheoraVideoRTPSink.cpp \
   $$PWD/uLawAudioFilter.cpp \
   $$PWD/UsageEnvironment.cpp \
   $$PWD/VAMetaDataLiveServerMediaSubsession.cpp \
   $$PWD/VAMetaDataLiveSource.cpp \
   $$PWD/VAMetaDataRTPSink.cpp \
   $$PWD/VAMetaDataTextRTPSink.cpp \
   $$PWD/VideoRTPSink.cpp \
   $$PWD/VorbisAudioMatroskaFileServerMediaSubsession.cpp \
   $$PWD/VorbisAudioRTPSink.cpp \
   $$PWD/VorbisAudioRTPSource.cpp \
   $$PWD/VP8VideoMatroskaFileServerMediaSubsession.cpp \
   $$PWD/VP8VideoRTPSink.cpp \
   $$PWD/VP8VideoRTPSource.cpp \
   $$PWD/WAVAudioFileServerMediaSubsession.cpp \
   $$PWD/WAVAudioFileSource.cpp

#DEFINES = 


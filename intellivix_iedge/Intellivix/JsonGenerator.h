﻿#ifndef JSONGENERATOR_H
#define JSONGENERATOR_H

#include "MetaGenerator.h"

///////////////////////////////////////////////////////////////////////////////
//
// Class: CJsonGenerator
//
///////////////////////////////////////////////////////////////////////////////

class CJsonGenerator : public CMetaGenerator {
private:
   enum {
      EVENT_STATUS_BEGUN = 1, // Start of event.
      EVENT_STATUS_ENDED = 0, // End of event.
   };

   std::string m_str;

public:
   explicit CJsonGenerator();
   virtual ~CJsonGenerator();

private:
   bool RenderEventObject( CRanderObjectInImage& render, VAResults& results );
   bool IsSaveImage();

public:
   virtual MetaGeneratorType GetType() const;
   virtual std::string GetString() const;

   virtual void InitGenerator ();
   virtual void FillEventInfo( VAResults& results );
   virtual void FillLiveFps( int type );
   virtual void FillSystemStartup();
   virtual void FillSystemShutdown( int code );
};

#endif // JSONGENERATOR_H

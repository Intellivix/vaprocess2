﻿#include "StdAfx.h"

#include "GroupsockHelper.hh"
#include "Live555Define.h"
#include "GroupsockHelper.hh"
#include "RTSPCustomServer.h"

#include "VideoSnder.h"
#include "Camera.h"

CVideoSnder::CVideoSnder()
{
   m_nVideoSendMode   = 0;
   m_bDirectStreaming = FALSE;
   m_bStopSend        = FALSE;
   m_SndBuff.SetBuffExpSize( 8192 ); // 메모리 재할당이 자주일어나지 않도록 적절한 GrowByte를 지정한다.
   m_SndBuff.SetLength( 65536 );
   m_SndBuff.SetLength( 0 );
}

CVideoSnder::~CVideoSnder()
{
}

void CVideoSnder::Initialize( int nIVXStreamIdx )
{
   m_nVideoSendMode = VideoSendMode_ByCameraSocket;
   m_nIVXStreamIdx  = nIVXStreamIdx;
   m_bStopSend      = FALSE;   
}

void CVideoSnder::Close()
{
   m_Encoder.Close();
   m_CompImgBuffQueue.Lock();
   std::deque<CCompressImageBufferItem*>::iterator iterEnd = m_CompImgBuffQueue.end();
   std::deque<CCompressImageBufferItem*>::iterator iter    = m_CompImgBuffQueue.begin();
   while( iterEnd != iter ) {
      CCompressImageBufferItem* pItem = *iter;
      delete pItem;
      iter++;
   }
   m_CompImgBuffQueue.clear();
   m_CompImgBuffQueue.Unlock();
}

// 실시간 스트림 전송 시 사용하는 함수.
BOOL CVideoSnder::SendVideo( CVideoFrame* pVideoFrame, CVideoStreamProfile* pProfile, BOOL bUseHWEncoder )
{
   int nEncodeBuffSize             = 0;
   byte* pEncodeBuffer             = NULL;
   int nVideoFrameFlag             = 0;
   VideoCodecID nCodecID           = VCODEC_ID_NONE;
   CCompressImageBufferItem* pItem = NULL;

   int nVideoWidth   = pVideoFrame->GetWidth();
   int nVideoHeight  = pVideoFrame->GetHeight();
   int nStreamWidth  = nVideoWidth;
   int nStreamHeight = nVideoHeight;
   int nBitrate      = pProfile->m_nVideoBitrate;
   float fFrameRate  = pProfile->m_fVideoFrameRate;

   timeval presentationTime;
   gettimeofday( &presentationTime, NULL );

   FILETIME ftTimestamp;
   ftTimestamp = pVideoFrame->m_ftTimestamp;
   AfxGetLocalFileTime( &ftTimestamp );

   if( m_bDirectStreaming ) // 직접 전송이면 큐에서 꺼내서 준다.
   {
      if( m_CompImgBuffQueue.Size() > (int)( fFrameRate * 2 ) ) // 전송이 느려질 경우 I 프레임 이전까지 삭제
         DeletePreviousFramesToKeyFrame();

      CIPCameraInfo* pIPCamInfo = &m_pCamera->VC->m_IPCamInfo;
      //nQuality   = pIPCamInfo->m_nCompression;
      fFrameRate = pIPCamInfo->m_fFPS_Cap;

      pItem = m_CompImgBuffQueue.Pop();
      if( pItem ) {
         nCodecID         = (VideoCodecID)pItem->m_nCodecID;
         nEncodeBuffSize  = pItem->m_nCompBuffSize;
         pEncodeBuffer    = pItem->m_CompBuff;
         nVideoFrameFlag  = pItem->m_dwCommFlag;
         presentationTime = pItem->m_PresentationTime;
      }
   } else // 인코딩하여 전송해야 할 경우이면
   {
      // 코덱 정보를 세팅
      nCodecID = (VideoCodecID)pProfile->m_nVideoCodecID;
      // FFMPEG에서 H.263으로 인코딩한 데이터를 디코딩 하지 못하여 MPEG4로 전환함.
      if( nCodecID == VCODEC_ID_H263 )
         nCodecID = VCODEC_ID_MPEG4;

      if( nCodecID != VCODEC_ID_NONE ) {
         // 영상 축소 확인
         BOOL bNeedResize = FALSE;
         if( pProfile->m_nResizeType == VideoStreamResizeType_ByReductionFactor ) {
            if( pProfile->m_fReductionFactor < 1.0f ) {
               nStreamWidth  = int( nVideoWidth * pProfile->m_fReductionFactor + 1 ) / 4 * 4;
               nStreamHeight = int( nVideoHeight * pProfile->m_fReductionFactor );
               bNeedResize   = TRUE;
            }
         } else if( pProfile->m_nResizeType == VideoStreamResizeType_UserDefined ) {
            if( ( pProfile->m_nVideoWidth != nVideoWidth ) || ( pProfile->m_nVideoHeight != nVideoHeight ) ) {
               nStreamWidth  = pProfile->m_nVideoWidth;
               nStreamHeight = pProfile->m_nVideoHeight;
               bNeedResize   = TRUE;
            }
         }

         byte* pImgBuff       = NULL;
         int nYUY2ImgBuffSize = GetYUY2ImageLength( nVideoWidth, nVideoHeight );
         if( pVideoFrame->m_ImageData.Length == nYUY2ImgBuffSize ) // 비디오와 버퍼가 정상이라면
         {
            if( bNeedResize ) // 영상을 리사이즈해야 할 경우라면 처리
            {
               int nResizeImgBuffSize = GetYUY2ImageLength( nStreamWidth, nStreamHeight );
               if( m_rsImgBuffer.Length != nResizeImgBuffSize ) {
                  m_rsImgBuffer.Create( nStreamWidth * nStreamHeight * 2 ); // 임시 버퍼
               }
               Resize_YUY2( pVideoFrame->m_ImageData, nVideoWidth, nVideoHeight, m_rsImgBuffer, nStreamWidth, nStreamHeight );
               pImgBuff = (byte*)m_rsImgBuffer;
            } else {
               pImgBuff = (byte*)pVideoFrame->m_ImageData;
            }

            // 인코딩한다.
            CAVCodecParam avOpen;
            avOpen.m_nCodecID           = nCodecID;
            avOpen.m_nWidth             = nStreamWidth;
            avOpen.m_nHeight            = nStreamHeight;
            avOpen.m_fFrameRate         = fFrameRate;
            avOpen.m_nBitrate           = nBitrate;
            avOpen.m_nPixelFormat       = VPIX_FMT_YUY2;
            avOpen.m_bUseHWAcceleration = bUseHWEncoder;
            if( DONE == m_Encoder.OpenEncoder( avOpen ) ) {
               nEncodeBuffSize = 0;
               INT nResult     = m_Encoder.Compress( pImgBuff, &pEncodeBuffer, nEncodeBuffSize, nVideoFrameFlag ); // 비디오 프레임을 인코딩한다.
            }
         }
      }
   }

   if( VCODEC_ID_NONE != nCodecID ) {
      CRTSPStreamingChannel* pRTSPStreamChannel = m_pCamera->GetRtspStreamingChannel(m_nIVXStreamIdx);
      if( pRTSPStreamChannel->IsVideoStreamRequested() ) {
         CRTSPVideoItem* pVideoBuffItem = new CRTSPVideoItem;

         pVideoBuffItem->SetLength( nEncodeBuffSize );
         pVideoBuffItem->Write( pEncodeBuffer, 1, nEncodeBuffSize );
         pVideoBuffItem->m_nCodecID         = nCodecID;
         pVideoBuffItem->m_dwFlag           = nVideoFrameFlag;
         pVideoBuffItem->m_fFrameRate       = fFrameRate;
         pVideoBuffItem->m_PresentationTime = presentationTime;

         pRTSPStreamChannel->PushVideoData( pVideoBuffItem );
      }
   }

   if( pItem ) delete pItem;

   return TRUE;
}

void CVideoSnder::DeletePreviousFramesToKeyFrame()
{
   int i;
   // 키프레임을 역순으로 찾고 찾은 다음프레임 들은 모두 삭제한다.
   m_CompImgBuffQueue.Lock();
   int nItem          = (int)m_CompImgBuffQueue.size();
   BOOL bFindKeyFrame = FALSE;
   if( nItem ) {
      std::deque<CCompressImageBufferItem*>::iterator iter      = m_CompImgBuffQueue.end();
      std::deque<CCompressImageBufferItem*>::iterator iterBegin = m_CompImgBuffQueue.begin();
      int nItemIdx                                              = nItem;
      int nFindItemIdx                                          = -1;
      do {
         iter--;
         nItemIdx--;
         CCompressImageBufferItem* pItem = *iter;
         if( FALSE == bFindKeyFrame ) {
            if( pItem->m_dwCommFlag & VFRM_KEYFRAME ) {
               bFindKeyFrame = TRUE;
               nFindItemIdx  = nItemIdx;
               break;
            }
         }
      } while( iter != iterBegin );

      if( nFindItemIdx >= 0 ) {
         for( i = 0; i < nFindItemIdx; i++ ) {
            CCompressImageBufferItem* pItem = m_CompImgBuffQueue.front();
            if( pItem ) delete pItem;
            m_CompImgBuffQueue.pop_front();
         }
      }
   }
   m_CompImgBuffQueue.Unlock();
}

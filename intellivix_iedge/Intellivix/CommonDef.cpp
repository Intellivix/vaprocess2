#include "StdAfx.h"
#include "CommonDef.h"

int GetSeupItemMask( int nSetupID )
{
   int nItemMask = 0;
   if( IVXSetupItemID_DBStorage == nSetupID )
      nItemMask = IVXSetupItemMask_DBStorage;
   else if( IVXSetupItemID_UserAccount == nSetupID )
      nItemMask = IVXSetupItemMask_UserAccount;
   else if( IVXSetupItemID_Network == nSetupID )
      nItemMask = IVXSetupItemMask_Network;
   else if( IVXSetupItemID_VideoInput == nSetupID )
      nItemMask = IVXSetupItemMask_VideoInput;
   else if( IVXSetupItemID_VideoRecoding == nSetupID )
      nItemMask = IVXSetupItemMask_VideoRecoding;
   else if( IVXSetupItemID_VideoRecodingClient == nSetupID )
      nItemMask = IVXSetupItemMask_VideoRecoding;
   else if( IVXSetupItemID_VideoColor == nSetupID )
      nItemMask = IVXSetupItemMask_VideoColor;
   else if( IVXSetupItemID_AlarmInput == nSetupID )
      nItemMask = IVXSetupItemMask_AlarmInput;
   else if( IVXSetupItemID_AuxControl == nSetupID )
      nItemMask = IVXSetupItemMask_AuxControl;
   else if( IVXSetupItemID_CameraDisplayOptions == nSetupID )
      nItemMask = IVXSetupItemMask_CameraDisplayOptions;
   else if( IVXSetupItemID_VideoStreaming == nSetupID )
      nItemMask = IVXSetupItemMask_VideoStreaming;
   else if( IVXSetupItemID_FloodLightControl == nSetupID )
      nItemMask = IVXSetupItemMask_FloodLightControl;
   else if( IVXSetupItemID_VADetection == nSetupID )
      nItemMask = IVXSetupItemMask_VADetection;
   else if( IVXSetupItemID_VACameraGeometry == nSetupID )
      nItemMask = IVXSetupItemMask_VACameraGeometry;
   else if( IVXSetupItemID_PTZAreaSetup == nSetupID )
      nItemMask = IVXSetupItemMask_VAEventDetection;
   else if( IVXSetupItemID_IVXNetSystemSetup == nSetupID )
      nItemMask = IVXSetupItemMask_IVXNetSystemSetup;
   else if( IVXSetupItemID_IVXNetCameraSetup == nSetupID )
      nItemMask = IVXSetupItemMask_IVXNetCameraSetup;
   return nItemMask;
}
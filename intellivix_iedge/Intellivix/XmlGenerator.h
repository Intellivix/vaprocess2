﻿#ifndef XmlGenerator_H
#define XmlGenerator_H

#include "MetaGenerator.h"

///////////////////////////////////////////////////////////////////////////////
//
// Class: CXmlGenerator
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPPacket;

class CXmlGenerator : public CMetaGenerator {
private:
   std::stringstream* m_p_stream = nullptr;
   std::string        m_str;

public:
   explicit CXmlGenerator();
   virtual ~CXmlGenerator();

public:
   virtual MetaGeneratorType GetType() const;
   virtual std::string GetString() const;

   virtual void FillFrameInfo( std::deque<TrackedObjectEx*>& trackedObjectList );
   virtual void FillFrmObjInfo( std::deque<TrackedObjectEx*>& trackedObjectList );
   virtual void FillObjectInfo( TrackedObjectEx* t_object );
   virtual void FillEventInfo( TrackedObjectEx* t_object, EventEx* pEvent );
   virtual void FillEventZoneInfo(BOOL bNullInfo = FALSE );
   virtual void FillEventZoneCounterInfo();

   void AddToStream( CIVCPPacket* pIvcp_packet, int packet_id );
};

#endif // XmlGenerator_H

﻿#include "IntelliVIX.h"

#include "IntelliVIXDoc.h"
#include "LocalSystem.h"
#include "SupportFunction.h"
#include "vacl.h"

#include "bccl_plog.h"
#include "TimerCheckerManager.h"

#ifdef __LIB_SIMPLEWEBSOCKET
#include "Websocket.h"
#endif // __LIB_SIMPLEWEBSOCKET

#ifdef __WIN32
#include "CommonUtil.h"
#include <conio.h>
#endif

#include "BaseDirManager.h"
#include "LGU/Logger.h"

#include <getopt.h>
#include <csignal>



/////////////////////////////////////////////////////////////////////////////
//
// Function
//
/////////////////////////////////////////////////////////////////////////////

void setLogging();
void parseInput( int argc, char* argv[] );
void printVersion();

/////////////////////////////////////////////////////////////////////////////
//
// Class : CIntelliVIXApp
//
/////////////////////////////////////////////////////////////////////////////

CIntelliVIXApp theApp;

CIntelliVIXApp* GetIntelliVIXApp()
{
   return &theApp;
}

CIntelliVIXApp::CIntelliVIXApp()
{
   m_pSupportFunc = NULL;
   m_pLocalSystem = NULL;
}

CIntelliVIXApp::~CIntelliVIXApp()
{
   SAFE_DELETE( m_pLocalSystem );
   SAFE_DELETE( m_pSupportFunc );
}

int CIntelliVIXApp::InitInstance()
{
   //LOGI << "CIntelliVIXApp::InitInstance - Start";
   
   m_pDocument = CIntelliVIXDoc::GetInstance();
   m_pDocument->SetPath();

#ifdef __SUPPORT_LICENSE_CERTIFY
   int nChannels = 0;
   m_pDocument->LicenseCertifyKey( nChannels );
   if( 0 == nChannels ) {
      logi( "License Certify Key Failed ... ChannelNum[%d]\n", nChannels );
      return FALSE;
   }
   logi( "License Certify Key  ... ChannelNum[%d]\n", nChannels );
#endif
   m_pSupportFunc = CSupportFunction::GetInstance();
   m_pSupportFunc->Initialize( "TOTALLINTELLIVIX", 410, 0 );

   InitVACL();
   InitFFMPEG();
   InitVCMLib();

   SetCreationFunction_Event( CreateEventInstance );
   SetCreationFunction_EventRule( CreateEventRuleInstance );
   SetCreationFunction_EventZone( CreateEventZoneInstance );
   SetCreationFunction_TrackedObject( CreateTrackedObjectInstance );
   SetCreationFunction_ObjectFiltering( CreateObjectFilteringInstance );

   m_pLocalSystem = new CLocalSystem();
   m_pLocalSystem->InitVCM();
   int ret_load = m_pLocalSystem->LoadCFGFile();
   if( Succeed == (IVXException)ret_load ) {
      m_pLocalSystem->Startup();
   }

#ifdef __LIB_SIMPLEWEBSOCKET
   Websocket::getInstance().Prepare( this );
   Websocket::getInstance().StartThread();
#endif // __LIB_SIMPLEWEBSOCKET
   
   //LOGI << "CIntelliVIXApp::InitInstance - End";

   return ret_load;
}

int CIntelliVIXApp::ExitInstance()
{
   int nRet = 0;

   logd( "CIntelliVIXApp::ExitInstance - Start\n" );

   if( m_pLocalSystem ) {
      if( Succeed == m_pDocument->GetExitCode() ) {
         // TODO(jongoh): 임시로 저장 하지 않도록 함
         //nRet = m_pLocalSystem->SaveCFGFile();
         //logi( "LocalSystem SaveCFGFile ...  nRet : %d\n", nRet );
      }

      nRet = m_pLocalSystem->Shutdown();
      logd( "LocalSystem ShutDown ...  Ret : %d\n", nRet );

      m_pLocalSystem->UnInitVCM();
      SAFE_DELETE( m_pLocalSystem );
      logd( "LocalSystem UnInitVCM ...  \n" );
   }

   if( m_pSupportFunc ) {
      m_pSupportFunc->FreeInstance();
      m_pSupportFunc = NULL;
   }

   int exit_code = m_pDocument->GetExitCode();
   if( m_pDocument ) {
      m_pDocument->FreeInstance();
      m_pDocument = NULL;
   }

   CloseFFMPEG();
   UninitVCMLib();
   CloseVACL();

   logd( "CIntelliVIXApp::ExitInstance Ended   code[%d]\n", exit_code );

   return nRet;
}

int CIntelliVIXApp::SetLogLevel( char level )
{

   switch (level)
   {
   case '0':
      LOGD << "Dynamic changing log leve to " << plog::severityToString( plog::none );
      plog::get()->setMaxSeverity( plog::none );
      break;
   case '1':
      LOGV << "Dynamic changing log level to " << plog::severityToString( plog::verbose );
      plog::get()->setMaxSeverity( plog::verbose );
      break;
   case '2':
      LOGD << "Dynamic changing log level to " << plog::severityToString( plog::debug );
      plog::get()->setMaxSeverity( plog::debug );
      break;
   case '3':
      LOGI << "Dynamic changing log level to " << plog::severityToString( plog::info );
      plog::get()->setMaxSeverity( plog::info );
      break;
   case '4':
      LOGW << "Dynamic changing log level to " << plog::severityToString( plog::warning );
      plog::get()->setMaxSeverity( plog::warning );
      break;
   case '5':
      LOGE << "Dynamic changing log level to " << plog::severityToString( plog::error );
      plog::get()->setMaxSeverity( plog::error );
      break;
   case '6':
      LOGF << "Dynamic changing log level to " << plog::severityToString( plog::fatal );
      plog::get()->setMaxSeverity( plog::fatal );
      break;
   default:
      LOGI << "Dynamic changing log level to " << plog::severityToString( plog::info );
      plog::get()->setMaxSeverity( plog::info );
      break;
   }

   return 0;
}

// TODO(yhpark): move WriteMemory() to common.
#ifdef __WIN32
void CIntelliVIXApp::WriteMemory()
{
   logi( _T("AVirt=%4d APhys=%4d  TVirt=%4d TPhys=%4d\n"),
         GetAvailVirtMemSizeInMB(),
         GetAvailPhysMemSizeInMB(),
         GetTotalVirtMemSizeInMB(),
         GetTotalPhysMemSizeInMB() );
}
void CIntelliVIXApp::WriteMemory2()
{
   logi( _T("AVirt=%4d APhys=%4d  TVirt=%4d TPhys=%4d\n"),
         GetAvailVirtMemSizeInMB(),
         GetAvailPhysMemSizeInMB(),
         GetTotalVirtMemSizeInMB(),
         GetTotalPhysMemSizeInMB() );
}
#endif

//-------------------------------------------------------------------------------------
#ifdef __WIN32
#define INTELLIVIX_APPLIANCE _T("IntelliVIX_Appliance")
#endif

static bool g_logging_console                     = true;
static plog::Severity g_logging_level             = plog::info;
static volatile sig_atomic_t g_main_graceful_quit = 0; // for graceful quit signal.

static void callBack_graceful_quit( int iSignal )
{
   LOGF << "Signal[" << iSignal << ":" << sys_siglist[iSignal] << "] Called. starting graceful quit.";
   g_main_graceful_quit = 1;
}

void setGracefulQuitSignal()
{
   signal( SIGTERM, callBack_graceful_quit );
   signal( SIGINT, callBack_graceful_quit );
#ifdef __linux
   signal( SIGQUIT, callBack_graceful_quit );
#endif // __linux
}

void printVersion()
{
   LOGI << "Build Date [" << __DATE__ << "] Time [" << __TIME__ << "]";
   LOGI << "PRODUCT_VERSION:" << PRODUCT_VERSION;
   LOGI << "INTELLIVIX_VERSION: " << INTELLIVIX_VERSION;
   LOGI << "INTELLIVIX_BUILD_NO: " << INTELLIVIX_BUILD_NO;
   LOGI << "INTELLIVIX_BUILD_DATE:" << INTELLIVIX_BUILD_DATE;
}

void parseInput( int argc, char* argv[] )
{
   for( int i = 0; i < argc; i++ ) {
      std::cout << "process input arg list: idx[" << i << "] => [" << argv[i] << "]\n";
   }

   enum class Result {
      Okay,
      Warning_EmptyParam,
      Warning_ShowVersionOnly,
      Error_WrongParamFormat,
      Error_InternalSetup,
      Error_CaseDefeult,
   };

   Result ret = Result::Warning_EmptyParam;

   const std::string options          = "i:l:vc";
   const struct option long_options[] = {
      // --add 는 -a와 동일한 동작을 한다.
      { "id", required_argument, nullptr, 'i' },

      // --console 은 -c 와 동일한 동작을 한다.
      { "console", no_argument, nullptr, 'c' },

      // version print only.
      { "version", no_argument, nullptr, 'v' },

      //{ "append", no_argument, nullptr, 'b' },
      //{ "delete", required_argument, nullptr, 'd' },
      //{ "create", required_argument, nullptr, 'c' },
      //{ "file", required_argument, nullptr, 'f' },

      { 0, 0, 0, 0 } // 옵션 배열은 {0,0,0,0} 센티넬에 의해 만료된다.
   };

   int input_ascii_code = 0; // return char as ascii code.

   while( true ) {

      // 옵션 인덱스를 저장하는 변수.
      int option_index = 0;

      // 옵션 파싱
      input_ascii_code = getopt_long( argc, argv, options.c_str(), long_options, &option_index );
      if( input_ascii_code == -1 ) {
         std::cout << "end parsing.\n";
         break;
      }

      std::cout << "param[" << char( input_ascii_code ) << "]\n";
      if( optarg ) {
         std::cout << " with arg[" << optarg << "]";
      }
      std::cout << "\n";

      switch( input_ascii_code ) {
      case 0:
         if( long_options[option_index].flag != nullptr ) {
            // 긴 이름 옵션을 만났을 때
            std::cout << "long_option arg's name[" << long_options[option_index].name << "] optarg[" << optarg << "]\n";
         }
         break;

      case 'i':
         // `-i`를 만났거나 `--id`를 만났다.
         std::cout << "Found id[i]: with optarg(value)[" << optarg << "]\n";
         if( optarg == nullptr ) {
            setLogging();
            LOGE << "Parameter failed : " << "--id(-i) need value, which was emptty";
            ret = Result::Error_WrongParamFormat;
            exit( Parameter_Missing );
         }

         if( BaseDirManager::Get().set_multil_proces_id( std::string( optarg ) ) ) {
            std::cout << "Set Camera Config id.\n";
            std::cout << "New Base Dir: " << BaseDirManager::Get().get_base_dir() + "\n";
            ret = Result::Okay;
         } else {
            setLogging();
            LOGE << "Parameter failed : " << "Set Camera Config id failed.";
            ret = Result::Error_InternalSetup;
            exit( Parameter_Set_Camera_Id_Failed );
         }
         break;

//      case 'c':
//         std::cout << "Found id[c]\n";
//         ret = Result::Okay;
//         std::cout << "No Console\n";
//         g_logging_console = false;
//         break;

      case 'l': {
         std::cout << "Found loglevel[l]: with optarg(value)[" << optarg << "]\n";
         if( optarg == nullptr ) {
            std::cerr << "--loglevel(-l) need value, which was empty.\n";
            exit( Parameter_Missing );
         }

         int log_level = std::atoi( optarg );
         if( log_level >= 0 && log_level <= 6 ) {
            g_logging_level = static_cast<plog::Severity>( log_level );
            std::cout << "Set log level to : " << log_level << "\n";
            break;
         }
         setLogging();
         LOGE << "Parameter failed : " << "--loglevel(-l)'s value is out of range.";
         exit( Parameter_Loglevel_Out_Of_Range );
         break;
      }
      case 'v':
         ret = Result::Warning_ShowVersionOnly;
         std::cout << "VersionOnlyFinish\n";
         setLogging();
         printVersion();
         exit( Succeed );
         break;

      default:
         setLogging();
         ret = Result::Error_CaseDefeult;
         LOGE << "Parameter failed : "<< "Parameter not supported.";
         exit( Parameter_Not_Supported );
         break;
      }
   }

   if( ret == Result::Warning_EmptyParam ) {
      setLogging();
      LOGE << "Parameter failed : " << "Empty Param...";
      // allow empty params.
      exit( Parameter_Empty );
   }

   std::cout << "input param parsed okay.\n";
}

void setLogging()
{
   if( g_logging_console ) {
#ifdef _DEBUG
      //BCCL::PlogHelper::initDisplayConsole( plog::debug );
      BCCL::PlogHelper::initDisplayAndWriteToFile( g_logging_level, BaseDirManager::Get().get_base_dir() + "/intellivix.log" );
#else
      BCCL::PlogHelper::initDisplayAndWriteToFile( plog::info, BaseDirManager::Get().get_base_dir() + "/intellivix.log" );
      //BCCL::PlogHelper::initWriteToFile( plog::debug, BaseDirManager::Get().get_base_dir() + "/intellivix.log" );
#endif // _DEBUG
   } else {
      BCCL::PlogHelper::initWriteToFile( g_logging_level, BaseDirManager::Get().get_base_dir() + "/intellivix.log" );
   }

#ifdef __UPLUS_LOGGER
   // NOTE(leejh): LGU+ Logging init.
   //std::string UPlusLog = "/ivx_lgu.log";
   LGU::Logger::initWriteToFile( g_logging_level, BaseDirManager::Get().get_base_dir() + "/LGU_Total.log" );
   LGU::Logger::SetHeaderItem( COMMAND, "START");
   LGU::Logger::SetHeaderItem( FROM_SERVICE_NAME, "VAM" );
   LGU::Logger::SetHeaderItem( RES_SERVICE_NAME, "VAP" );
#endif
}

int main( int argc, char* argv[] )
{
#ifdef _DEBUG
   Sleep_ms( 100 );
#endif

   parseInput( argc, argv );

   setGracefulQuitSignal();

   std::cout << "init Logging( plog ).\n";
   setLogging();

   LOGI << "====================== IntelliVIX Start... ======================";
   
   printVersion();
#ifdef _DEBUG
   LOGD << "=== Developing gudie for logging. start. ===";
   LOGV << "LOGV << ...; || logv( ... ); verbose";
   LOGD << "LOGD << ...; || logd( ... ); debug";
   LOGI << "LOGI << ...; || logi( ... ); info";
   LOGW << "LOGW << ...; || logw( ... ); warning";
   LOGE << "LOGE << ...; || loge( ... ); error";
   LOGF << "LOGF << ...; || logf( ... ); fatal";
   LOGD << "=== Developing gudie for logging. end. ===";
#endif // _DEBUG

   srand( static_cast<uint>( time( nullptr ) ) );

   CTimerManagerSP::Activate( CTimerManagerSP::Type_Direct );

   theApp.InitInstance();

   //LOGI << "InitInstance Done, and IntelliVIX is now loop processing...";
   //LOGW << "You can stop gracefully by Ctrl+C(SIGINT) or Ctrl+\\(SIGQUIT) singals.";

#ifdef __linux
   CSetKeyboardNonBlock key_nonblock;
   key_nonblock.Set();
#endif

   int exit_code = 0;
   while( g_main_graceful_quit == 0 ) {
#ifdef __WIN32
      if( kbhit() ) {
         char ch = getchar();
         if( 'q' == ch || 'Q' == ch )
            break;
      }
#elif __linux
      if( theApp.m_pDocument->IsExitProcess() ) {
         exit_code = theApp.m_pDocument->GetExitCode();
         LOGI << "IntelliVIX exit code catch... code[" << exit_code << "]";
         break;
      }

      int ch = getchar();
      if( ch == 'q' || ch == 'Q' ) {
         LOGF << "q : Exit requested";
         break;
      } else if( '0' <= ch && ch <= '6'  ) {
         theApp.SetLogLevel(ch);
      }

      Sleep_ms( 100 ); // it is needed for background.
#endif
   }

#ifdef __linux
   key_nonblock.Restore();
#endif

   LOGD << "IntelliVIX is now exiting...  code[" << exit_code << "]";
   theApp.ExitInstance();
   LOGI << "====================== IntelliVIX End... ======================";

   return exit_code;
}

//-------------------------------------------------------------------------------------

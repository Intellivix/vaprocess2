﻿///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2013 illisis. Inc. All rights reserved.
//
// FileName : IVCP_LiveSender.h
//
// Function: Live Stream & MetaData Sender class for IVCP 3.0.
//
///////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "GroupsockHelper.hh"
#include "IvcpRtspSender.h"
#include "Camera.h"
#include "MetaGenerator.h"
#include "AVCodec.h"
#include "RTSPCustomServer.h"

///////////////////////////////////////////////////////////////////////////////
//
// Class: CIvcpRtspSender
//
///////////////////////////////////////////////////////////////////////////////

CIvcpRtspSender::CIvcpRtspSender(CCamera* pCamera)
{
   m_pCamera                                 = pCamera;
   m_bEventZoneDataTobSended                 = FALSE;
   m_bPresetMapDataTobSended                 = FALSE;
   m_bCameraInfoDataTobSended                = FALSE;
   m_nBLCMode                                = 0;
   m_bIPCameraSettingInfoReceivedJustOneTime = FALSE;

   m_TNEncoder = std::make_shared<CAVCodec>();
}

CIvcpRtspSender::~CIvcpRtspSender()
{
}

void CIvcpRtspSender::SetEventZoneDataToBeBroadcasted()
{
   m_bEventZoneDataTobSended = TRUE;
   if( m_pCamera ) {
      int i;
      for( i = 0; i < IVX_VIDEO_STREAM_NUM; i++ ) {
         CRTSPStreamingChannel* pRTSPStreamChannel = m_pCamera->GetRtspStreamingChannel(i);
         if( pRTSPStreamChannel ) pRTSPStreamChannel->SetVAMetaDataToBeSent( LIVE555_META_EVTZONE_INFO );
      }
   }
}

void CIvcpRtspSender::SetCameraInfoDataToBeBroadcasted()
{
   m_bCameraInfoDataTobSended = TRUE;
   if( m_pCamera ) {
      int i;
      for( i = 0; i < IVX_VIDEO_STREAM_NUM; i++ ) {
         CRTSPStreamingChannel* pRTSPStreamChannel = m_pCamera->GetRtspStreamingChannel(i);
         if( pRTSPStreamChannel ) pRTSPStreamChannel->SetVAMetaDataToBeSent( LIVE555_META_CAMERA_INFO );
      }
   }
}

void CIvcpRtspSender::SetPresetMapDataToBeBroadcasted()
{
   m_bPresetMapDataTobSended = TRUE;
   if( m_pCamera ) {
      int i;
      for( i = 0; i < IVX_VIDEO_STREAM_NUM; i++ ) {
         CRTSPStreamingChannel* pRTSPStreamChannel = m_pCamera->GetRtspStreamingChannel(i);
         if( pRTSPStreamChannel ) pRTSPStreamChannel->SetVAMetaDataToBeSent( LIVE555_META_PRESET_MAP );
      }
   }
}

void CIvcpRtspSender::SendEventInfo( TrackedObjectEx* t_object, EventEx* pEvent )
{
   if( !m_pCamera->IsActivated() ) return;
   if( m_pCamera->m_nState & CamState_Deactivating ) return;
   EventRuleEx* pEventRuleEx = (EventRuleEx*)pEvent->EvtRule;
   if( !pEventRuleEx ) return;
   if( !( pEventRuleEx->AlarmType & ER_AT_XML ) ) return;

   BOOL bReqFromRTSPClient = FALSE;
   if( IsReqRTSPVAMetaData( LIVE555_META_EVENT_BEGUN | LIVE555_META_EVENT_ENDED ) ) bReqFromRTSPClient = TRUE;
   if( !bReqFromRTSPClient ) return;

   if( !( pEvent->Status & ( EV_STATUS_BEGUN | EV_STATUS_ENDED ) ) )
      return;

   MetaGeneratorSP generator = GetGenerator( MetaGeneratorType::XML, m_pCamera->System, m_pCamera );
   generator->FillEventInfo( t_object, pEvent );

   if( bReqFromRTSPClient && generator->GetString().size() ) {
      uint nVAMetaDataFlag = 0;
      if( pEvent->Status & EV_STATUS_BEGUN )
         nVAMetaDataFlag = LIVE555_META_EVENT_BEGUN;
      else if( pEvent->Status & EV_STATUS_ENDED )
         nVAMetaDataFlag = LIVE555_META_EVENT_ENDED;

      SendRTSPVAMetaData( generator.get(), nVAMetaDataFlag );
   }
}

void CIvcpRtspSender::SendObjectInfo( TrackedObjectEx* t_object )
{
   if( !m_pCamera->IsActivated() ) return;
   if( m_pCamera->m_nState & CamState_Deactivating ) return;

   BOOL bReqFromRTSPClient = FALSE;
   if( IsReqRTSPVAMetaData( IVCP30_META_OBJECT_BEGUN | IVCP30_META_OBJECT_ENDED ) ) bReqFromRTSPClient = TRUE;
   if( !bReqFromRTSPClient ) return;

   MetaGeneratorSP generator = GetGenerator( MetaGeneratorType::XML, m_pCamera->System, m_pCamera );
   generator->FillObjectInfo( t_object );

   if( bReqFromRTSPClient && generator->GetString().size() ) {
      uint nVAMetaDataFlag = 0;
      if( t_object->Status & TO_STATUS_VERIFIED_START )
         nVAMetaDataFlag = LIVE555_META_OBJECT_BEGUN;
      else if( t_object->Status & TO_STATUS_TO_BE_REMOVED )
         nVAMetaDataFlag = LIVE555_META_OBJECT_ENDED;

      SendRTSPVAMetaData( generator.get(), nVAMetaDataFlag );
   }
}

void CIvcpRtspSender::SendFrameInfo( std::deque<TrackedObjectEx*>& trackedObjectList )
{
   if( !m_pCamera->IsActivated() ) return;
   if( m_pCamera->m_nState & CamState_Deactivating ) return;

   BOOL bReqFromRTSPClient = FALSE;
   if( IsReqRTSPVAMetaData( IVCP30_META_FRAME_INFO ) ) bReqFromRTSPClient = TRUE;
   if( !bReqFromRTSPClient ) return;

   MetaGeneratorSP generator = GetGenerator( MetaGeneratorType::XML, m_pCamera->System, m_pCamera );
   generator->FillFrameInfo( trackedObjectList );

   if( bReqFromRTSPClient && generator->GetString().size() ) {
      SendRTSPVAMetaData( generator.get(), IVCP30_META_FRAME_INFO );
   }
}
void CIvcpRtspSender::SendFrmObjInfo( std::deque<TrackedObjectEx*>& trackedObjectList )
{
   if( !m_pCamera->IsActivated() ) return;
   if( m_pCamera->m_nState & CamState_Deactivating ) return;

   BOOL bReqFromRTSPClient = FALSE;
   if( IsReqRTSPVAMetaData( IVCP30_META_FRAME_OBJ ) ) bReqFromRTSPClient = TRUE;
   if( !bReqFromRTSPClient ) return;

   if( trackedObjectList.empty() )
      return;

   MetaGeneratorSP generator = GetGenerator( MetaGeneratorType::XML, m_pCamera->System, m_pCamera );
   generator->FillFrmObjInfo( trackedObjectList );

   if( bReqFromRTSPClient && generator->GetString().size() ) {
      SendRTSPVAMetaData( generator.get(), IVCP30_META_FRAME_OBJ );
   }
}

void CIvcpRtspSender::SendCameraSettingInfo()
{
   if( !m_pCamera->IsActivated() ) return;
   if( !( m_pCamera->m_nState & CamState_Activate ) ) return;
   if( m_pCamera->m_nState & ( CamState_ToBeRestarted | CamState_ToBeClosed | CamState_Deactivating | CamState_ToBeActivate | CamState_ToBeDeactivate ) ) return;

   BOOL bReqFromRTSPClient = FALSE;
   if( IsReqRTSPVAMetaData( IVCP30_META_SETTING ) ) bReqFromRTSPClient = TRUE;
   if( !bReqFromRTSPClient ) return;

   // 보내줄 구조체 정리

   m_SettingInfo.m_nCameraUID = m_pCamera->UID;
   AfxGetLocalFileTime( &m_SettingInfo.m_ftFrameTime ); // 현재 시간
   m_SettingInfo.m_nSettingMask = GetSettingMask();
   m_SettingInfo.m_nSettingFlag = GetSettingFlag() & m_SettingInfo.m_nSettingMask;
   //logd ("SendCameraSettingInfo - UID:%d   Mask:%I64x  Flag:%I64x\n", m_pCamera->UID, settingInfo.m_nSettingMask, settingInfo.m_nSettingFlag);

   //m_SettingInfo.m_nStabilNum  = m_pCamera->VC->m_nVideoStabilizationFrameNum;
   //m_SettingInfo.m_nDefogLevel = m_pCamera->VC->m_nDefogLevel;

   int nFlagAuto;
   if( FALSE == m_bIPCameraSettingInfoReceivedJustOneTime ) {
      m_pCamera->VC->GetImageProperty( 0, ImagePropertyType_AWB, m_SettingInfo.m_nWhiteBalanceMode, nFlagAuto );
      m_pCamera->VC->GetImageProperty( 0, ImagePropertyType_DayAndNight, m_SettingInfo.m_nDayNightMode, nFlagAuto );
   }

   m_bIPCameraSettingInfoReceivedJustOneTime = TRUE;

   if( bReqFromRTSPClient ) {
      // 보내줄 패킷 생성
      CIVCPPacket ivcp_packet;
      IvcpGetPacketString( ivcp_packet.m_Header.m_strMessage, IVCP_ID_Live_SettingInfo );
      m_SettingInfo.WriteToPacket( ivcp_packet );

      SendRTSPVAMetaData( &ivcp_packet, IVCP30_META_SETTING );
   }
}

void CIvcpRtspSender::SendEventZoneInfo( BOOL bNullInfo )
{
   if( !m_pCamera->IsActivated() ) return;
   if( m_pCamera->m_nState & CamState_Deactivating ) return;

   BOOL bReqFromRTSPClient = FALSE;
   if( IsReqRTSPVAMetaData( LIVE555_META_EVTZONE_INFO ) ) bReqFromRTSPClient = TRUE;
   if( !bReqFromRTSPClient ) return;

   MetaGeneratorSP generator = GetGenerator( MetaGeneratorType::XML, m_pCamera->System, m_pCamera );
   generator->FillEventZoneInfo();

   //if( bReqFromRTSPClient && generator->GetString().size() ) {
   //   SendRTSPVAMetaData( generator.get(), LIVE555_META_EVTZONE_INFO );
   //}

   m_bEventZoneDataTobSended = FALSE;
}

void CIvcpRtspSender::SendEventZoneCounterInfo()
{
   if( !m_pCamera->IsActivated() ) return;
   if( m_pCamera->m_nState & CamState_Deactivating ) return;

   BOOL bReqFromRTSPClient = FALSE;
   if( IsReqRTSPVAMetaData( LIVE555_META_EVTZONE_COUNT ) ) bReqFromRTSPClient = TRUE;
   if( !bReqFromRTSPClient ) return;

   MetaGeneratorSP generator = GetGenerator( MetaGeneratorType::XML, m_pCamera->System, m_pCamera );
   generator->FillEventZoneCounterInfo();

   //if( bReqFromRTSPClient && generator->GetString().size() ) {
   //   SendRTSPVAMetaData( generator.get(), LIVE555_META_EVTZONE_COUNT );
   //}
}

void CIvcpRtspSender::SendCameraInfo()
{
   if( !m_pCamera->IsActivated() ) return;
   if( !( m_pCamera->m_nState & CamState_Activate ) ) return;
   if( m_pCamera->m_nState & ( CamState_ToBeRestarted | CamState_ToBeClosed | CamState_Deactivating | CamState_ToBeActivate | CamState_ToBeDeactivate ) ) return;

   BOOL bReqFromRTSPClient = FALSE;
   if( IsReqRTSPVAMetaData( LIVE555_META_CAMERA_INFO ) ) bReqFromRTSPClient = TRUE;
   if( !bReqFromRTSPClient ) return;

   FileIO buff;
   CXMLIO xmlIO( &buff, XMLIO_Write );
   m_pCamera->WriteSetup( &xmlIO );

   if( bReqFromRTSPClient ) {
      SendRTSPVAMetaData( &buff, LIVE555_META_CAMERA_INFO );
   }

   m_bCameraInfoDataTobSended = FALSE;
}

void CIvcpRtspSender::EncodeImage( byte* pSrcBuff_YUY2, int nSrcWidth, int nSrcHeight, BArray1D& comp_buff )
{
   m_csTNEncoder.Lock();
   // 매우 중요: 0x0037xxxx 버그
   CAVCodecParam avOpen;
   avOpen.m_nCodecID   = MDBThumbnailCodecID;
   avOpen.m_nWidth     = nSrcWidth;
   avOpen.m_nHeight    = nSrcHeight;
   avOpen.m_fFrameRate = 1.0f;
   avOpen.m_nBitrate   = ThumbnailDataSize;
   m_TNEncoder->OpenEncoder( avOpen );
   int dwFlag = VFRM_KEYFRAME;
   int nCompBuffSize;
   byte* pCompBuff;
   int i;
   // 코덱이 h264인 경우 첫 번째 인코딩(or 디코딩)이 실패하는 것을 방지하기 위해..
   for( i = 0; i < 3; i++ ) {
      if( DONE == m_TNEncoder->Compress( pSrcBuff_YUY2, &pCompBuff, nCompBuffSize, dwFlag ) ) {
         comp_buff.Create( nCompBuffSize );
         CopyMemory( comp_buff, pCompBuff, nCompBuffSize );
         break;
      }
   }
   m_csTNEncoder.Unlock();
}

int CIvcpRtspSender::GetSettingMask()
{
   int nMask = 0;
   if( m_pCamera ) {
      BOOL bPTZEnable = FALSE;
      if( m_pCamera && m_pCamera->IsPTZCamera() ) bPTZEnable = TRUE;
      UINT64 nPTZCtrlCaps = 0;
      if( m_pCamera && m_pCamera->m_pPTZCtrl ) nPTZCtrlCaps = m_pCamera->m_pPTZCtrl->m_nCapabilities;

      if( bPTZEnable ) {
         nMask |= IVCP30_ENABLE_AUTO_PTZ;
         nMask |= IVCP30_ENABLE_AUTO_FOCUS;
         nMask |= IVCP30_ENABLE_PRESET_TOURING;
         nMask |= IVCP30_ENABLE_PTZ_LOCK;

         if( nPTZCtrlCaps & PTZCtrlCap_PTZWiperSupported ) nMask |= IVCP30_ENABLE_WIPER;
         if( nPTZCtrlCaps & PTZCtrlCap_PTZHeatingWireSupported ) nMask |= IVCP30_ENABLE_HEATER;
         if( nPTZCtrlCaps & PTZCtrlCap_PTZThermalCamSupported ) nMask |= IVCP30_ENABLE_THERMAL;
         //if (nPTZCtrlCaps & PTZCtrlCap_PTZDayAndNightModeSupported)    nMask |= IVCP30_ENABLE_BW_MODE;
         if( nPTZCtrlCaps & PTZCtrlCap_PTZOneShotAutoFocusSupported ) nMask |= IVCP30_ENABLE_ONE_SHOT_AUTO_FOCUS;

         if( nPTZCtrlCaps & PTZCtrlCap_IRLigtControlSupported ) nMask |= IVC30P_ENABLE_IR_LIGHT;
      }

      nMask |= IVCP30_ENABLE_VIDEO_ANALYTICS;
      nMask |= IVCP30_PAUSE_VIDEO_ANALYTICS;
      nMask |= IVCP30_PAUSE_VIDEO_ANALYTICS_SCHEDULE;
      nMask |= IVCP30_ENABLE_VIDEO_DEFOG;
      nMask |= IVCP30_ENABLE_VIDEO_STABILIZATION;
   }

   return nMask;
}

int CIvcpRtspSender::GetSettingFlag()
{
   int nFlag = 0;
   if( m_pCamera ) {
      if( !( m_pCamera->m_nState & CamState_Activate ) ) return nFlag;
      if( m_pCamera->m_nState & ( CamState_ToBeRestarted | CamState_ToBeClosed | CamState_Deactivating | CamState_ToBeActivate | CamState_ToBeDeactivate ) ) return nFlag;

      BOOL bPTZEnable = FALSE;
      if( m_pCamera && m_pCamera->IsPTZCamera() ) bPTZEnable = TRUE;
      UINT64 nPTZCtrlCaps = 0;
      if( m_pCamera && m_pCamera->m_pPTZCtrl ) nPTZCtrlCaps = m_pCamera->m_pPTZCtrl->m_nCapabilities;

      if( bPTZEnable ) {
         if( m_pCamera->Options & CamOpt_AutoPTZCtrlMode ) nFlag |= IVCP30_ENABLE_AUTO_PTZ;
         // Auto Focus
         if( m_pCamera->Options & CamOpt_PresetTouring ) nFlag |= IVCP30_ENABLE_PRESET_TOURING;
         if( m_pCamera->Options & CamOpt_LockPTZ ) nFlag |= IVCP30_ENABLE_PTZ_LOCK;

         // Wiper
         // Heater
         // Thermal
         // BW Mode
         // One shot auto focus
      }

      CPTZCtrl* pPTZCtrl = m_pCamera->m_pPTZCtrl;
      if( m_pCamera && pPTZCtrl ) {
         if( pPTZCtrl->m_PTZInfo.m_IRLightCtrlInfo.m_nIRMode == IRMode_Always_NightMode_IROn ) nFlag |= IVC30P_ENABLE_IR_LIGHT;
         if( pPTZCtrl->m_PTZInfo.m_bHeaterOn ) nFlag |= IVCP30_ENABLE_HEATER;
         if( pPTZCtrl->m_PTZInfo.m_bWiperOn ) nFlag |= IVCP30_ENABLE_WIPER;
      }

      if( m_pCamera->DTCOptions & CamOpt_EnableVAC ) nFlag |= IVCP30_ENABLE_VIDEO_ANALYTICS;
      if( m_pCamera->Options & CamOpt_PausingVideoAnalytics ) nFlag |= IVCP30_PAUSE_VIDEO_ANALYTICS;
      if( m_pCamera->Options & CamOpt_PausingVideoAnalyticsSchedule ) nFlag |= IVCP30_PAUSE_VIDEO_ANALYTICS_SCHEDULE;
      //if (m_pCamera->VC->GetDefog())                                 nFlag |= IVCP30_ENABLE_VIDEO_DEFOG; // (mkjang-150910)
      //if (m_pCamera->VC->m_bUseDefog)                                nFlag |= IVCP30_ENABLE_VIDEO_DEFOG;
      //if (m_pCamera->VC->m_bUseVideoStabilization)                   nFlag |= IVCP30_ENABLE_VIDEO_STABILIZATION;
      int nFlagAuto;
      if( FALSE == m_bIPCameraSettingInfoReceivedJustOneTime ) {
         m_pCamera->VC->GetImageProperty( 0, ImagePropertyType_BLC, m_nBLCMode, nFlagAuto );
      }
      if( m_nBLCMode == BLCMode_On ) nFlag |= IVCP30_ENABLE_BLC_MODE;
   }

   return nFlag;
}

BOOL CIvcpRtspSender::IsReqRTSPVAMetaData( uint nVAMetaDataFlag )
{
   BOOL bReqMetadata = FALSE;
   if( m_pCamera ) {
      int i;
      for( i = 0; i < IVX_VIDEO_STREAM_NUM; i++ ) {
         CRTSPStreamingChannel* pRTSPStreamChannel = m_pCamera->GetRtspStreamingChannel(i);
         if( pRTSPStreamChannel->IsVAMetaDataRequested( nVAMetaDataFlag ) ) {
            bReqMetadata = TRUE;
         }
      }
   }
   return bReqMetadata;
}

void CIvcpRtspSender::RemoveRTSPVAMetaDataFlag( uint nVAMetaDataFlag )
{
   if( !m_pCamera ) return;

   for( int i = 0; i < IVX_VIDEO_STREAM_NUM; i++ ) {
      CRTSPStreamingChannel* pRTSPStreamChannel = m_pCamera->GetRtspStreamingChannel(i);
      if( pRTSPStreamChannel->m_nVAMetaDataFlag & nVAMetaDataFlag ) {
         pRTSPStreamChannel->m_nVAMetaDataFlag &= ~nVAMetaDataFlag;
      }
   }
}

bool CIvcpRtspSender::SendRTSPVAMetaData( CIVCPPacket* pDataPacket, uint nVAMetaDataFlag )
{
   if( !m_pCamera ) return false;

   bool bRet = false;

   for( int i = 0; i < IVX_VIDEO_STREAM_NUM; i++ ) {
      CRTSPStreamingChannel* pRTSPStreamChannel = m_pCamera->GetRtspStreamingChannel(i);
      if( pRTSPStreamChannel->m_nVAMetaDataFlag & nVAMetaDataFlag ) {
         if( pRTSPStreamChannel->IsVAMetaDataRequested() ) {
            CRTSPVAMetaData* pRTSPVAMetaData = new CRTSPVAMetaData;

            pRTSPVAMetaData->SetLength( pDataPacket->m_Header.m_nBodyLen );
            pRTSPVAMetaData->Write( (byte*)pDataPacket->m_pBody, 1, pDataPacket->m_Header.m_nBodyLen );
            pRTSPVAMetaData->m_VAMetaDataFlag = nVAMetaDataFlag;

            if( pRTSPStreamChannel->PushVAMetaData( pRTSPVAMetaData ) )
               bRet = true;
         }

         //if( bRet && ( nVAMetaDataFlag & LIVE555_META_EVTZONE_INFO ) )
         //   logi( "RTSP SendEventZoneInfo   [Index : %d][UID : %d]\n", i, m_pCamera->UID );
      }
   }

   return bRet;
}

bool CIvcpRtspSender::SendRTSPVAMetaData( CMetaGenerator* pGenerator, uint nVAMetaDataFlag )
{
   if( !m_pCamera ) return false;

   bool bRet = false;

   for( int i = 0; i < IVX_VIDEO_STREAM_NUM; i++ ) {
      CRTSPStreamingChannel* pRTSPStreamChannel = m_pCamera->GetRtspStreamingChannel(i);
      if( pRTSPStreamChannel->m_nVAMetaDataFlag & nVAMetaDataFlag ) {
         if( pRTSPStreamChannel->IsVAMetaDataRequested() ) {
            CRTSPVAMetaData* pRTSPVAMetaData = new CRTSPVAMetaData;

            std::string data = pGenerator->GetString();
            pRTSPVAMetaData->SetLength( (int)data.size() );
            pRTSPVAMetaData->Write( (byte*)data.c_str(), 1, data.size() );
            pRTSPVAMetaData->m_VAMetaDataFlag = nVAMetaDataFlag;

            if( pRTSPStreamChannel->PushVAMetaData( pRTSPVAMetaData ) )
               bRet = true;
         }

         //if( bRet && ( nVAMetaDataFlag & LIVE555_META_EVTZONE_INFO ) )
         //   logi( "RTSP SendEventZoneInfo   [Index : %d][UID : %d]\n", i, m_pCamera->UID );
      }
   }

   return bRet;
}

bool CIvcpRtspSender::SendRTSPVAMetaData( FileIO* pFile, uint nVAMetaDataFlag )
{
   if( !m_pCamera ) return false;

   bool bRet = false;

   for( int i = 0; i < IVX_VIDEO_STREAM_NUM; i++ ) {
      CRTSPStreamingChannel* pRTSPStreamChannel = m_pCamera->GetRtspStreamingChannel(i);
      if( pRTSPStreamChannel->m_nVAMetaDataFlag & nVAMetaDataFlag ) {
         if( pRTSPStreamChannel->IsVAMetaDataRequested() ) {
            CRTSPVAMetaData* pRTSPVAMetaData = new CRTSPVAMetaData;

            pRTSPVAMetaData->SetLength( pFile->GetLength() );
            pRTSPVAMetaData->Write( (byte*)pFile->GetBuffer(), 1, pFile->GetLength() );
            pRTSPVAMetaData->m_VAMetaDataFlag = nVAMetaDataFlag;

            if( pRTSPStreamChannel->PushVAMetaData( pRTSPVAMetaData ) )

               bRet = true;
         }

         //if( bRet && ( nVAMetaDataFlag & LIVE555_META_CAMERA_INFO ) )
         //   logi( "RTSP SendCameraInfo   [Index : %d][UID : %d]\n", i, m_pCamera->UID);
      }
   }

   return bRet;
}

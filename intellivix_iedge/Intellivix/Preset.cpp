﻿#include "StdAfx.h"
#include "version.h"
#include "Preset.h"

//#pragma pack(1)

const int MAX_PRESET_GROUP_NUM = 32;
const int MAX_CYCLE_TYPE       = PresetTourCycleMode_Return;

////////////////////////////////////////////////////////////////////////////////////////////////
//
// CDwellTime
//
////////////////////////////////////////////////////////////////////////////////////////////////

CDwellTime::CDwellTime()
{
   Seconds = 30;
}

void CDwellTime::operator=( CDwellTime& d_time )
{
   Seconds = d_time.Seconds;
}

void CDwellTime::GetTime( int& min, int& sec )
{
   min = Seconds / 60;
   sec = Seconds % 60;
}

void CDwellTime::CheckParams()
{
   CDwellTime def_val;
   if( Seconds < 0 ) Seconds = def_val.Seconds;
}

void CDwellTime::SetTime( int min, int sec )
{
   Seconds = min * 60 + sec;
}

int CDwellTime::ReadFile( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "DwellTime" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Seconds", &Seconds );
      xmlNode.End();
   }
   CheckParams();
   return ( DONE );
}

int CDwellTime::WriteFile( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "DwellTime" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Seconds", &Seconds );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

////////////////////////////////////////////////////////////////////////////////////////////////
//
// CPTZStopCheck
//
////////////////////////////////////////////////////////////////////////////////////////////////

CPTZStopCheck::CPTZStopCheck()
{
   PTZMovingTime = 5.0f;
}

void CPTZStopCheck::CheckParams()
{
   if( PTZMovingTime < 0 )
      PTZMovingTime = 0;
   if( PTZMovingTime > 360 * 10000 )
      PTZMovingTime = 360 * 10000;
}

int CPTZStopCheck::ReadFile( CXMLIO* pIO )
{
   static CPTZStopCheck dv;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PTZStopCheck" ) ) {
      xmlNode.Attribute( TYPE_ID( float ), "PTZMovingTime", &PTZMovingTime, &dv.PTZMovingTime );
      xmlNode.End();
   }
   CheckParams();
   return ( DONE );
}

int CPTZStopCheck::WriteFile( CXMLIO* pIO )
{
   static CPTZStopCheck dv;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PTZStopCheck" ) ) {
      xmlNode.Attribute( TYPE_ID( float ), "PTZMovingTime", &PTZMovingTime, &dv.PTZMovingTime );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

////////////////////////////////////////////////////////////////////////////////////////////////
//
// CPTSwingSpeed
//
////////////////////////////////////////////////////////////////////////////////////////////////

CPTSwingSpeed::CPTSwingSpeed()
{
   SwingMode        = PTZSwingMode_SwingByFullSpeed;
   AngleSpeed       = 5.0f;
   ImageScrollSpeed = 0.1f;
}

void CPTSwingSpeed::CheckParams()
{
}

int CPTSwingSpeed::ReadFile( CXMLIO* pIO )
{
   static CPTSwingSpeed dv;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PTSwingSpeed" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "SwingMode", &SwingMode, &dv.SwingMode );
      xmlNode.Attribute( TYPE_ID( float ), "AngleSpeed", &AngleSpeed, &dv.AngleSpeed );
      xmlNode.Attribute( TYPE_ID( float ), "ImageScrollSpeed", &ImageScrollSpeed, &dv.ImageScrollSpeed );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CPTSwingSpeed::WriteFile( CXMLIO* pIO )
{
   static CPTSwingSpeed dv;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PTSwingSpeed" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "SwingMode", &SwingMode, &dv.SwingMode );
      xmlNode.Attribute( TYPE_ID( float ), "AngleSpeed", &AngleSpeed, &dv.AngleSpeed );
      xmlNode.Attribute( TYPE_ID( float ), "ImageScrollSpeed", &ImageScrollSpeed, &dv.ImageScrollSpeed );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

////////////////////////////////////////////////////////////////////////////////////////////////
//
// CPreset
//
////////////////////////////////////////////////////////////////////////////////////////////////

CPreset::CPreset()
{
   Name[0] = '\0';
   // 예외 처리
   Option   = 0;
   PresetNo = -1;
   VAMode   = VAE_MODE_GENERAL;
   ISize2D s2d( REF_IMAGE_WIDTH, REF_IMAGE_HEIGHT );
   Config_ObjectTracking.TrackingAreas.SetRefImageSize( s2d );
   Config_ObjectTracking.NontrackingAreas.SetRefImageSize( s2d );
   Config_ObjectTracking.NonHandoffAreas.SetRefImageSize( s2d );
   Config_ObjectTracking.DynBkgAreas.SetRefImageSize( s2d );
   Config_EventDetection.EventZones.SetRefImageSize( s2d );
   Config_RealObjectSizeEstimation.RefImgSize( REF_IMAGE_WIDTH, REF_IMAGE_HEIGHT );
}

CPreset::~CPreset()
{
}

int CPreset::operator==( CPreset& preset )
{
   FileIO buff_this;
   FileIO buff_other;
   CXMLIO xmlIO_this( &buff_this, XMLIO_Write );
   CXMLIO xmlIO_other( &buff_other, XMLIO_Write );
   WriteFile( &xmlIO_this, FALSE );
   preset.WriteFile( &xmlIO_other, FALSE );
   return !( buff_this.IsDiff( buff_other ) );
}

int CPreset::operator!=( CPreset& preset )
{
   return !( *this == preset );
}

int CPreset::CopyFrom( CPreset* preset )
{
   FileIO buffer;
   CXMLIO xmlIO_Write( &buffer, XMLIO_Write );
   if( preset->WriteFile( &xmlIO_Write ) ) return ( 1 );
   buffer.GoToStartPos();
   CXMLIO xmlIO_Read( &buffer, XMLIO_Read );
   if( ReadFile( &xmlIO_Read ) ) return ( 2 );
   return ( DONE );
}

void CPreset::CheckParams()
{
   DwellTime.CheckParams();
   PTZStopCheck.CheckParams();
}

int CPreset::ReadFile( CXMLIO* pIO, BOOL bImage )
{
   static CPreset dv;
   static CAVCodec presetImageDecoder;

   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Preset" ) ) {
      xmlNode.Attribute( TYPE_ID( short ), "PresetID", &PresetID );
      xmlNode.Attribute( TYPE_ID( char ), "Name", Name, sizeof( Name ) );
      xmlNode.Attribute( TYPE_ID( int ), "PresetNo", &PresetNo );
      xmlNode.Attribute( TYPE_ID( xint ), "Option", &Option, &dv.Option );
      // client 파노라마 동작 적용 // pan, tilt, zoom, focus저장
      xmlNode.Attribute( TYPE_ID( float ), "Pan", &PresetPos.P, &dv.PresetPos.P );
      xmlNode.Attribute( TYPE_ID( float ), "Tilt", &PresetPos.T, &dv.PresetPos.T );
      xmlNode.Attribute( TYPE_ID( float ), "Zoom", &PresetPos.Z, &dv.PresetPos.Z );
      xmlNode.Attribute( TYPE_ID( float ), "Focus", &PresetPos.F, &dv.PresetPos.F );
      DwellTime.ReadFile( pIO );
      PTZStopCheck.ReadFile( pIO );
      PTZSwingSpeed.ReadFile( pIO );
      //Config_DNImgSynSetup.ReadFile (pIO);
      ReadVACSetup( pIO );
      if( bImage ) {
         int s_length = 0;
         xmlNode.Attribute( TYPE_ID( int ), "JPGLength", &s_length ); // 이전 버전 호환성을 위해 남겨둔다.
         xmlNode.Attribute( TYPE_ID( int ), "TNLen", &s_length );
         if( s_length > 0 ) {
            EncodedImage.SetLength( s_length );
            xmlNode.Attribute( TYPE_ID( XBYTE ), "JPGData", EncodedImage.GetBuffer(), EncodedImage.GetLength() ); // 이전 버전 호환성을 위해 남겨둔다.
            xmlNode.Attribute( TYPE_ID( b64byte ), "TNData", EncodedImage.GetBuffer(), EncodedImage.GetLength() );
            if( pIO->m_nCustOpt != IVXXMLIOOpt_DoNotEnDecodeImage ) {
               CAVCodecParam avOpen;
               avOpen.m_nCodecID   = VCODEC_ID_MJPEG;
               avOpen.m_nWidth     = PresetBitmapWidth;
               avOpen.m_nHeight    = PresetBitmapHeight;
               avOpen.m_fFrameRate = 30.0f;

               presetImageDecoder.Lock();
               presetImageDecoder.OpenDecoder( avOpen );
               Image.Create( PresetBitmapWidth, PresetBitmapHeight );
               BArray1D yuy2Image( Image.Width * Image.Height * 2 );
               presetImageDecoder.Decompress( EncodedImage, EncodedImage.GetLength(), yuy2Image );
               ConvertYUY2ToBGR( yuy2Image, Image );
               presetImageDecoder.Unlock();
            }
         }
      }
      xmlNode.End();
   }

#ifndef __SUPPORT_VA_ON_PRESET_MOVING
   Option &= ~PresetOption_VideoAnalyticsOnPresetMoving;
#endif

   return ( DONE );
}

int CPreset::WriteFile( CXMLIO* pIO, BOOL bImage )
{
   static CPreset dv;
   static CAVCodec presetImageEncoder;

   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Preset" ) ) {
      xmlNode.Attribute( TYPE_ID( short ), "PresetID", &PresetID );
      xmlNode.Attribute( TYPE_ID( char ), "Name", Name, sizeof( Name ) );
      xmlNode.Attribute( TYPE_ID( int ), "PresetNo", &PresetNo );
      xmlNode.Attribute( TYPE_ID( xint ), "Option", &Option, &dv.Option );
      // client 파노라마 동작 적용 // pan, tilt, zoom, focus저장
      xmlNode.Attribute( TYPE_ID( float ), "Pan", &PresetPos.P, &dv.PresetPos.P );
      xmlNode.Attribute( TYPE_ID( float ), "Tilt", &PresetPos.T, &dv.PresetPos.T );
      xmlNode.Attribute( TYPE_ID( float ), "Zoom", &PresetPos.Z, &dv.PresetPos.Z );
      xmlNode.Attribute( TYPE_ID( float ), "Focus", &PresetPos.F, &dv.PresetPos.F );
      DwellTime.WriteFile( pIO );
      PTZStopCheck.WriteFile( pIO );
      PTZSwingSpeed.WriteFile( pIO );
      //Config_DNImgSynSetup.WriteFile (pIO);
      WriteVACSetup( pIO );
      if( bImage ) {
         int nEncodedBuffSize = Image.Width * Image.Height * 2;
         if( nEncodedBuffSize == 0 ) {
            xmlNode.Attribute( TYPE_ID( int ), "JPGLength", &nEncodedBuffSize );
         } else {
            if( EncodedImage.GetLength() <= 0 ) {
               CAVCodecParam avOpen;
               avOpen.m_nCodecID   = VCODEC_ID_MJPEG;
               avOpen.m_nWidth     = PresetBitmapWidth;
               avOpen.m_nHeight    = PresetBitmapHeight;
               avOpen.m_fFrameRate = 15.0f;
               avOpen.m_nBitrate   = 1000 * 1024;
               presetImageEncoder.Lock();
               presetImageEncoder.OpenEncoder( avOpen );
               EncodedImage.SetLength( Image.Width * Image.Height * 2 );
               BYTE* pEncodedBuff = NULL;
               int nFlags         = 0;
               BArray1D yuy2Image( Image.Width * Image.Height * 2 );
               ConvertBGRToYUY2( Image, (byte*)yuy2Image );
               presetImageEncoder.Compress( yuy2Image, &pEncodedBuff, nEncodedBuffSize, nFlags );
               CopyMemory( EncodedImage.GetBuffer(), pEncodedBuff, nEncodedBuffSize );
               EncodedImage.SetLength( nEncodedBuffSize );
               presetImageEncoder.Unlock();
            }
            nEncodedBuffSize = EncodedImage.GetLength();
            xmlNode.Attribute( TYPE_ID( int ), "TNLen", &nEncodedBuffSize );
            xmlNode.Attribute( TYPE_ID( b64byte ), "TNData", EncodedImage.GetBuffer(), nEncodedBuffSize );
         }
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CPreset::ReadVACSetup( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "VAC" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Detect_Type", &VAMode );

      Config_EventDetection.ReadFile( pIO );
      Config_SmokeDetection.ReadFile( pIO );
      Config_ForegroundDetection.ReadFile( pIO );
      Config_ObjectClassification->ReadFile( pIO );
      Config_ObjectFiltering.ReadFile( pIO );
      Config_ObjectTracking.ReadFile( pIO );
      Config_RealObjectSizeEstimation.ReadFile( pIO );
      //Config_FaceDetection.ReadFile (pIO);
      Config_HeadDetection.ReadFile( pIO );

      xmlNode.End();
   }
   return ( DONE );
}

int CPreset::WriteVACSetup( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "VAC" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Detect_Type", &VAMode );

      Config_EventDetection.WriteFile( pIO );
      Config_SmokeDetection.WriteFile( pIO );
      Config_ForegroundDetection.WriteFile( pIO );
      Config_ObjectClassification->WriteFile( pIO );
      Config_ObjectFiltering.WriteFile( pIO );
      Config_ObjectTracking.WriteFile( pIO );
      Config_RealObjectSizeEstimation.WriteFile( pIO );
      //Config_FaceDetection.WriteFile (pIO);
      Config_HeadDetection.WriteFile( pIO );

      xmlNode.End();
   }
   return ( DONE );
}

////////////////////////////////////////////////////////////////////////////////////////////////
//
// CPresetGroup
//
////////////////////////////////////////////////////////////////////////////////////////////////

CPresetGroup::CPresetGroup()
{
   strcpy( Name, "" );
   CycleType = PresetTourCycleMode_Foword;
}

CPresetGroup::~CPresetGroup()
{
   DeleteAll();
}

void CPresetGroup::AddPreset( CPreset* preset )
{
   preset->PresetID = GetNewPresetID();
   Add( preset );
}

void CPresetGroup::InsertPreset( CPreset* preset )
{
   preset->PresetID = GetNewPresetID();
   CPreset* n_item  = NULL;
   CPreset* c_item  = First();
   while( c_item ) {
      if( c_item->PresetNo > preset->PresetNo ) {
         if( n_item ) {
            if( c_item->PresetNo < n_item->PresetNo )
               n_item = c_item;
         } else
            n_item = c_item;
      }
      c_item = c_item->Next;
   }
   if( n_item )
      InsertBefore( preset, n_item );
   else
      PresetList::Add( preset );
}

void CPresetGroup::DeleteAll()
{
   CPreset* c_item = Items;
   while( c_item != NULL ) {
      CPreset* p_item = c_item;
      c_item          = c_item->Next;
      delete p_item;
   }
   NItems = 0;
   Items = LastItem = NULL;
}

int CPresetGroup::Delete( int idx )
{
   CPreset* item = GetAt( idx );
   if( !item ) return ( 1 );
   Remove( item );
   delete item;
   return ( DONE );
}

CPreset* CPresetGroup::GetAt( int idx )
{
   int i;

   CPreset* c_item = First();
   for( i = 0; c_item; i++ ) {
      if( i == idx ) {
         return c_item;
      }
      c_item = c_item->Next;
   }
   return c_item;
}

int CPresetGroup::GetIndex( CPreset* item )
{
   int i;

   CPreset* c_item = First();
   for( i = 0; c_item; i++ ) {
      if( c_item == item ) {
         return ( i );
      }
      c_item = c_item->Next;
   }
   return ( -1 );
}

short CPresetGroup::GetNewPresetID()
{
   const int ushort_max = 0xFFFF;
   int* preset_id_table = new int[ushort_max];
   memset( preset_id_table, 0, sizeof( int ) * ushort_max );

   CPreset* preset;
   preset = First();
   while( preset ) {
      preset_id_table[preset->PresetID] = TRUE;
      preset                            = preset->Next;
   }
   ushort new_preset_id = 0;
   ushort i;
   for( i = 1; i < ushort_max; i++ ) {
      if( preset_id_table[i] == FALSE ) {
         new_preset_id = i;
         break;
      }
   }
   delete preset_id_table;

   return new_preset_id;
}

void CPresetGroup::CheckParams()
{
   CPresetGroup def_val;

   if( ( CycleType < 0 ) || ( CycleType > MAX_CYCLE_TYPE ) ) CycleType = def_val.CycleType;
   CPreset* preset = First();
   while( preset ) {
      preset->CheckParams();
      preset = preset->Next;
   }
}

int CPresetGroup::ReadFile( CXMLIO* pIO, BOOL bImage )
{
   int i;

   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PresetGroup" ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "Name", Name, sizeof( Name ) );
      xmlNode.Attribute( TYPE_ID( int ), "CycleType", &CycleType );
      DeleteAll();
      int n_item;
      xmlNode.Attribute( TYPE_ID( int ), "ItemNum", &n_item );
      if( ( n_item < 0 ) || ( n_item > MAX_PRESET_NUM ) ) return ( 1 );
      for( i = 0; i < n_item; i++ ) {
         CPreset* preset = new CPreset;
         if( DONE == preset->ReadFile( pIO, bImage ) )
            Add( preset );
         else
            delete preset;
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CPresetGroup::WriteFile( CXMLIO* pIO, BOOL bImage )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PresetGroup" ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "Name", Name, sizeof( Name ) );
      xmlNode.Attribute( TYPE_ID( int ), "CycleType", &CycleType );
      int n_item = (int)GetNumItems();
      xmlNode.Attribute( TYPE_ID( int ), "ItemNum", &n_item );
      CPreset* preset = First();
      while( preset ) {
         preset->WriteFile( pIO, bImage );
         preset = preset->Next;
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

////////////////////////////////////////////////////////////////////////////////////////////////
//
// CPresetGroupList
//
////////////////////////////////////////////////////////////////////////////////////////////////

CPresetGroupList::CPresetGroupList()
{
   MinPresetNo        = 1;
   MaxPresetNo        = 255;
   SelectedGroupIndex = -1;
   TouringWaitTime    = 5;
   HomePosPresetNo    = -1;
}

CPresetGroupList::~CPresetGroupList()
{
   DeleteAll();
}

int CPresetGroupList::operator!=( CPresetGroupList& list )
{
   FileIO buff_this;
   FileIO buff_other;
   CXMLIO xmlIO_this( &buff_this, XMLIO_Write );
   CXMLIO xmlIO_other( &buff_other, XMLIO_Write );
   WriteFile( &xmlIO_this );
   list.WriteFile( &xmlIO_other );
   return buff_this.IsDiff( buff_other );
}

int CPresetGroupList::CopyFrom( CPresetGroupList& list )
{
   FileIO buffer;
   CXMLIO xmlIO_write( &buffer, XMLIO_Write );
   if( list.WriteFile( &xmlIO_write ) ) return ( 1 );
   buffer.Seek( 0, FIO_SEEK_BEGIN );
   CXMLIO xmlIO_read( &buffer, XMLIO_Read );
   if( ReadFile( &xmlIO_read ) ) return ( 2 );
   return ( DONE );
}

CPreset* CPresetGroupList::FindPreset( int group_idx, int preset_idx )
{
   int n_item = (int)GetNumItems();
   if( group_idx < n_item )
      return GetAt( group_idx )->GetAt( preset_idx );
   else
      return NULL;
}

CPreset* CPresetGroupList::FindPreset( int preset_no )
{
   CPresetGroup* preset_group;
   preset_group = First();
   while( preset_group ) {
      CPreset* preset = preset_group->First();
      while( preset ) {
         if( preset->PresetNo == preset_no ) {
            return preset;
         }
         preset = preset->Next;
      }
      preset_group = preset_group->Next;
   }
   return NULL;
}

CPresetGroup* CPresetGroupList::FindPresetGroup( int preset_no )
{
   CPresetGroup* preset_group;
   preset_group = First();
   while( preset_group ) {
      CPreset* preset = preset_group->First();
      while( preset ) {
         if( preset->PresetNo == preset_no ) {
            return preset_group;
         }
         preset = preset->Next;
      }
      preset_group = preset_group->Next;
   }
   return NULL;
}

int CPresetGroupList::DeletePreset( CPreset* preset )
{
   CPresetGroup* group = First();
   while( group ) {
      CPreset* preset_eter = group->First();
      while( preset_eter ) {
         if( preset_eter == preset ) {
            group->Remove( preset );
            delete preset;
            return ( DONE );
         }
         preset_eter = preset_eter->Next;
      }
      group = group->Next;
   }
   return ( 1 );
}

CPresetGroup* CPresetGroupList::GetAt( int idx )
{
   int i;

   CPresetGroup* preset_group = First();
   for( i = 0; preset_group; i++ ) {
      if( i == idx ) return preset_group;
      preset_group = preset_group->Next;
   }
   return ( NULL );
}

int CPresetGroupList::GetPresetIndex( CPreset* preset, int& group_idx, int& preset_idx )
{
   int i;

   CPresetGroup* preset_group = First();
   for( i = 0; preset_group; i++ ) {
      int find_preset_idx = preset_group->GetIndex( preset );
      if( find_preset_idx > -1 ) {
         group_idx  = i;
         preset_idx = find_preset_idx;
         return ( DONE );
      }
      preset_group = preset_group->Next;
   }
   return ( 1 );
}

CPresetGroup* CPresetGroupList::GetSelectedGroup()
{
   if( SelectedGroupIndex < 0 ) return NULL;
   return GetAt( SelectedGroupIndex );
}

int CPresetGroupList::RemoveAt( int idx )
{
   int i;

   CPresetGroup* preset_group = First();
   for( i = 0; preset_group; i++ ) {
      if( i == idx ) {
         Remove( preset_group );
         return ( DONE );
      }
      preset_group = preset_group->Next;
   }
   return ( 1 );
}

void CPresetGroupList::DeleteAll()
{
   CPresetGroup* c_item = Items;
   while( c_item != NULL ) {
      CPresetGroup* p_item = c_item;
      c_item               = c_item->Next;
      delete p_item;
   }
   NItems = 0;
   Items = LastItem = NULL;
}

void CPresetGroupList::SetMinMaxPresetNo( int min_preset_no, int max_preset_no )
{
   MinPresetNo = min_preset_no;
   MaxPresetNo = max_preset_no;
}

int CPresetGroupList::GetAvailablePresetNo()
{
   int i;

   UpdateUsePresetNoList();

   // 사용하지 않는 번호중에 가장 빠른 번호를 리턴.
   int bExist = FALSE;
   for( i = 0; i < MAX_PRESET_NUM; i++ ) {
      if( UsePresetNoList[i] == 0 ) {
         bExist = TRUE;
         break;
      }
   }
   if( bExist )
      return i;
   else
      return -1; //모두 사용중인 경우 (FULL)
}

void CPresetGroupList::UpdateUsePresetNoList( int curr_preset_no )
{
   int i;

   if( MinPresetNo < 0 ) {
      ASSERT( 0 );
      MinPresetNo = 0;
   }
   if( MaxPresetNo >= MAX_PRESET_NUM ) {
      MaxPresetNo = MAX_PRESET_NUM - 1;
   }
   // 모두 사용 불가로 체크
   for( i = 0; i < MAX_PRESET_NUM; i++ )
      UsePresetNoList[i] = -1;
   // 사용하지 않음을 체크 (MinPresetNo ~ MaxPresetNo)
   for( i = MinPresetNo; i < MaxPresetNo; i++ )
      UsePresetNoList[i] = 0;

   if( HomePosPresetNo >= 0 )
      UsePresetNoList[HomePosPresetNo] = 1;

   // 사용하고 있는 프리셋을 체크
   CPresetGroup* preset_group = First();
   while( preset_group ) {
      CPreset* preset = preset_group->First();
      while( preset ) {
         UsePresetNoList[preset->PresetNo] = 1;
         preset                            = preset->Next;
      }
      preset_group = preset_group->Next;
   }
   if( curr_preset_no >= 0 )
      UsePresetNoList[curr_preset_no] = 0;
}

void CPresetGroupList::CheckParams()
{
   CPresetGroupList def_val;
   if( SelectedGroupIndex > MAX_PRESET_GROUP_NUM ) {
      if( GetNumItems() )
         SelectedGroupIndex = 0;
      else
         SelectedGroupIndex = def_val.SelectedGroupIndex;
   }
   CPresetGroup* preset_group = First();
   while( preset_group ) {
      preset_group->CheckParams();
      preset_group = preset_group->Next;
   }
}

int CPresetGroupList::ReadFile( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PresetGroupList" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "SelectedGroupIndex", &SelectedGroupIndex );
      xmlNode.Attribute( TYPE_ID( int ), "TouringWaitTime", &TouringWaitTime );
      DeleteAll();
      int n_item;
      xmlNode.Attribute( TYPE_ID( int ), "ItemNum", &n_item );
      if( ( n_item < 0 ) || ( n_item > MAX_PRESET_GROUP_NUM ) ) return ( 1 );
      CPresetGroup* preset_group;
      for( int i = 0; i < n_item; i++ ) {
         preset_group = new CPresetGroup;
         if( DONE == preset_group->ReadFile( pIO ) )
            Add( preset_group );
         else
            delete preset_group;
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CPresetGroupList::WriteFile( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PresetGroupList" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "SelectedGroupIndex", &SelectedGroupIndex );
      xmlNode.Attribute( TYPE_ID( int ), "TouringWaitTime", &TouringWaitTime );
      int n_item = (int)GetNumItems();
      xmlNode.Attribute( TYPE_ID( int ), "ItemNum", &n_item );
      CPresetGroup* preset_group = First();
      while( preset_group ) {
         preset_group->WriteFile( pIO );
         preset_group = preset_group->Next;
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

//#pragma pack()

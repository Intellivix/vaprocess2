﻿#include "LocalSystem.h"
#include "Camera.h"
#include "CommonUtil.h"
#include "IntelliVIXDoc.h"
#include "SupportFunction.h"
#include "WaitForMultipleObjects.h"
#include "TimerCheckerManager.h"
#include "RestSender.h"
#include "RTSPCustomServer.h"
#include "MetaGenerator.h"
#include "LGU/Logger.h"

///////////////////////////////////////////////////////////////////////////
//
// Class : CRTSPCustomServer_IVX
//
///////////////////////////////////////////////////////////////////////////

class CRTSPCustomServer_IVX : public CRTSPCustomServer {
public:
   CRTSPCustomServer_IVX()
       : CRTSPCustomServer()
   {
   }
   virtual ~CRTSPCustomServer_IVX()
   {
   }

   virtual void OnRTSPRequest( LPVOID pParam, const std::string& strStreamName )
   {
      if( NULL == pParam ) return;
      CLocalSystem* pLocalSytem = (CLocalSystem*)pParam;
      pLocalSytem->OnRTSPRequest( strStreamName );
   }

   virtual void OnStreamingChannelClosed( LPVOID pParam, CRTSPStreamingChannel* pRTSPStreamingChannel )
   {
      if( NULL == pParam ) return;
      CLocalSystem* pLocalSytem = (CLocalSystem*)pParam;
      pLocalSytem->OnStreamingChannelClosed( pRTSPStreamingChannel );
   }
};

//////////////////////////////////////////////////////////////////////
//
// Class : CLocalSystem
//
//////////////////////////////////////////////////////////////////////

CLocalSystem::CLocalSystem()
    : m_strLocalSystemCfgFileName( "SystemConfig.xml" )
{
   SetSystemName( "iEdge" );

   CameraNum = 0;
   Status    = 0;
   IsTemp    = FALSE;

   m_nGlobalState = 0;

   m_nSaveLocalSystemSetupSaveTime = 0;
   SetupChannelNo                  = -1;
   m_RTSPServer                    = std::make_shared<CRTSPCustomServer_IVX>();
   m_pDoc                          = CIntelliVIXDoc::GetInstance();
}

CLocalSystem::~CLocalSystem()
{
}

///////////////////////////////////////////////////////////////////////////////////////////////////

BOOL CLocalSystem::IsExistCamera( int nOption )
{
   CCriticalSectionSP cs( csCameraList );

   BOOL bExist = FALSE;
   for( int i = 0; i < CameraNum; i++ ) {
      if( GetCamera( i )->Options & nOption ) {
         bExist = TRUE;
         break;
      }
   }

   return bExist;
}

void CLocalSystem::SetSystemGUID( std::string strGUID )
{
   SystemGUID = strGUID;
}

std::string CLocalSystem::GetSystemGUID()
{
   return SystemGUID;
}

CCamera* CLocalSystem::GetCamera( int nChannelNo )
{
   CCamera* pCamera = NULL;
   if( nChannelNo < 0 || nChannelNo >= CameraNum )
      return NULL;

   pCamera = Cameras[nChannelNo];
   return pCamera;
}

CCamera* CLocalSystem::GetCameraFromUID( int nUID )
{
   CCamera* pFindCamera = NULL;
   int i;
   CCamera* pCamera = NULL;
   for( i = 0; i < CameraNum; i++ ) {
      pCamera = Cameras[i];
      if( pCamera->UID == nUID ) {
         pFindCamera = pCamera;
      }
   }
   return pFindCamera;
}

CCamera* CLocalSystem::GetCameraFromGuid( const std::string& strCameraGuid )
{
   CCamera* pFindCamera = NULL;
   int i;
   CCamera* pCamera = NULL;
   for( i = 0; i < CameraNum; i++ ) {
      pCamera             = Cameras[i];
      std::string strGuid = pCamera->CameraGuid;
      if( strGuid == strCameraGuid ) {
         pFindCamera = pCamera;
      }
   }
   return pFindCamera;
}

CCamera* CLocalSystem::GetCameraFromVCID( INT nVCID )
{
   CCamera* pFindCamera = NULL;
   int i;
   CCamera* pCamera = NULL;
   for( i = 0; i < CameraNum; i++ ) {
      pCamera = Cameras[i];
      if( pCamera->VCID == nVCID ) {
         pFindCamera = pCamera;
      }
   }
   return pFindCamera;
}

int CLocalSystem::GetChannelNoFromUID( int nUID )
{
   int nChannelNo   = -1;
   CCamera* pCamera = NULL;
   CCriticalSectionSP co( csCameraList );
   for( int i = 0; i < CameraNum; i++ ) {
      pCamera = Cameras[i];
      if( pCamera->UID == nUID ) {
         nChannelNo = pCamera->ChannelNo;
         break;
      }
   }

   return nChannelNo;
}

//////////////////////////////////////////////////////////////////////
// Functions
//////////////////////////////////////////////////////////////////////

void CLocalSystem::SetSystemName( LPCSTR szName )
{
   SystemName = szName;
}

LPCSTR CLocalSystem::GetSystemName()
{
   return SystemName.c_str();
}

int CLocalSystem::WriteSetupCmn( CXMLIO* pIO, int nRWFlag, int item )
{
   // CLocalSystem 의 설정 입출력 중 로컬 또는 네트워크 전송에 공통으로 필요한 설정들을 Write한다.
   if( item & IVXSetupItemMask_VideoInput ) {
      WriteVCMSetup( pIO );
   }
   return ( DONE );
}

int CLocalSystem::ReadSetupCmn( CXMLIO* pIO, int nRWFlag, int item )
{
   // CLocalSystem 의 설정 입출력 중 로컬 또는 네트워크 전송에 공통으로 필요한 설정들을 Read한다.
   if( item & IVXSetupItemMask_VideoInput ) {
      ReadVCMSetup( pIO );
   }
   return ( DONE );
}

//pCamera의 nItem 항목 설정값을 다른 모든 채널에 적용한다.
void CLocalSystem::ApplyToAllChannels( CCamera* pCamera, int nItem )
{
   FileIO buffer;
   CXMLIO xmlWriteIO( &buffer, XMLIO_Write );
   pCamera->WriteSetup( &xmlWriteIO, nItem, 0 );
   for( int i = 0; i < CameraNum; i++ ) {
      buffer.GoToStartPos();
      CXMLIO xmlReadIO( &buffer, XMLIO_Read );
      Cameras[i]->ReadSetup( &xmlReadIO, nItem );
   }
}

int CLocalSystem::ReadVCMSetup( CXMLIO* pIO )
{
   if( FALSE == VCM.Read( pIO ) ) return ( 1 );
   // 현재의 시스템이 설정을 저장하기 위한 임시객체일 경우 이를 VCM에 설정함.
   if( IsTemp )
      VCM.SetSetupTemp( TRUE );
   return ( DONE );
}

int CLocalSystem::WriteVCMSetup( CXMLIO* pIO )
{
   if( FALSE == VCM.Write( pIO ) )
      return ( 1 );
   // 현재의 시스템이 설정을 저장하기 위한 임시객체일 경우 이를 VCM에 설정함.
   if( IsTemp )
      VCM.SetSetupTemp( TRUE );
   return ( DONE );
}

void CLocalSystem::GetCameraNames( std::vector<std::string>& array )
{
   array.clear();
   std::string name;
   for( int i = 0; i < CameraNum; i++ ) {
      name = Cameras[i]->CameraName;
      array.push_back( name );
   }
}

BOOL CLocalSystem::DeallocateCamera( int nChannelNo )
{
   return DeallocateCamera( GetCamera( nChannelNo ) );
}

BOOL CLocalSystem::DeallocateCamera( CCamera* pCamera )
{
   if( !pCamera )
      return FALSE;

   m_csSystem.Lock();
   csCameraList.Lock();
   m_pDoc->FreeUID( pCamera->UID );

   CameraIter iter = Cameras.begin();
   while( iter != Cameras.end() ) {
      if( *iter == pCamera ) {
         Cameras.erase( iter );
         break;
      }
      iter++;
   }

   if( pCamera ) delete pCamera;
   csCameraList.Unlock();
   m_csSystem.Unlock();
   return TRUE;
}

void CLocalSystem::DetermineStartStopVideoStreamingThread()
{
   if( m_nGlobalState & GlobalState_ClosingAllSystems )
      return;

   CCriticalSectionSP lk( csCameraList );

   struct StartVideoStreaming {
      BOOL Flag[IVX_VIDEO_STREAM_NUM] = {
         FALSE,
      };
   };

   int i_cam, j_stream;
   std::vector<StartVideoStreaming> vecStreamingFlag( CameraNum );

   // 각 카메라 별로 스트림 활성화 여부 판단.
   for( i_cam = 0; i_cam < CameraNum; i_cam++ ) {
      CCamera* pLocalCamera = GetCamera( i_cam );
      std::vector<int> vtAvailableStreamIndexs;
      pLocalCamera->VC->GetAvailableStreamIndex( vtAvailableStreamIndexs );
      for( j_stream = 0; j_stream < IVX_VIDEO_STREAM_NUM; j_stream++ ) {
         CRTSPStreamingChannel* pRtspCtreamingChannel = pLocalCamera->GetRtspStreamingChannel( j_stream );
         // VA meta요청이 있거나 Video를 요청 할 경우 스레드를 활성화 시킨다.
         if( pRtspCtreamingChannel->IsVAMetaDataRequested() || pRtspCtreamingChannel->IsVideoStreamRequested() ) {
            for( int ii = 0; ii < (int)vtAvailableStreamIndexs.size(); ii++ ) {
               if( vtAvailableStreamIndexs[ii] == j_stream )
                  vecStreamingFlag[i_cam].Flag[j_stream] = TRUE;
            }
         }
      }
   }

   // 현재까지 전송여부 정보로 각 카메라의 비디오 스트림 스레드 시작/종료 수행.
   for( i_cam = 0; i_cam < CameraNum; i_cam++ ) {
      CCamera* pLocalCamera = GetCamera( i_cam );
      for( j_stream = 0; j_stream < IVX_VIDEO_STREAM_NUM; j_stream++ ) {
         BOOL& bSendVideo                     = vecStreamingFlag[i_cam].Flag[j_stream];
         pLocalCamera->m_bSendVideo[j_stream] = bSendVideo;
         if( bSendVideo )
            pLocalCamera->StartVideoStreamingThread( j_stream );
         else
            pLocalCamera->StopVideoStreamingThread( j_stream );
      }
   }
}

BOOL CLocalSystem::Startup()
{
   if( Status & SysState_StartUp )
      return FALSE;

   GetIntelliVIXApp()->SetLogLevel( '0' + m_LogLevel );

   VCM.SetSetupTemp( FALSE );

   if( CRestSender::getInstance()->Activate( m_ManagerURL ) ) {
      CIntelliVIXDoc::GetInstance()->SetExitProcess( System_Status_Rest_Activate_Failed );
      return FALSE;
   }

   MetaGeneratorSP generator = GetGenerator( MetaGeneratorType::JSON, this, nullptr );
   generator->FillSystemStartup();
   CRestSender::getInstance()->SendSystemStartup( generator.get() );

   ActivateSystem();

   m_pDoc->UpdateTableUID( this );

   // 로컬시스템 스레드
   m_hThread_LocalSystemProc = std::thread( ThreadFunc_LocalSystemProc, this );

   Status |= SysState_StartUp;

   return TRUE;
}

BOOL CLocalSystem::Shutdown()
{
   if( !( Status & SysState_StartUp ) )
      return TRUE;

   m_evtExitLocalSystemProc.SetEvent();
   if( m_hThread_LocalSystemProc.joinable() )
      m_hThread_LocalSystemProc.join();
   m_evtExitLocalSystemProc.ResetEvent();

   DeactivateSystem();

   MetaGeneratorSP generator = GetGenerator( MetaGeneratorType::JSON, this, nullptr );
   generator->FillSystemShutdown( m_pDoc->GetExitCode() );
   CRestSender::getInstance()->SendSystemShutdown( generator.get() );
   CRestSender::freeInstance();

   Status &= ~SysState_StartUp;
   
#ifdef __UPLUS_LOGGER
   LGU::Logger::SetHeaderItem( COMMAND, "END");
   LGU::Logger::SetHeaderItem( FROM_SERVICE_NAME, "VAP" );
   LGU::Logger::SetHeaderItem( RES_SERVICE_NAME, "VAM" );
   LOGI_(L) << LGU::Logger::header (1);
#endif

   return TRUE;
}

void CLocalSystem::InitVCM()
{
   VCM_Desc vcmDesc;
   vcmDesc.m_strStartupDirPath   = m_pDoc->m_strStartUpDir;
   vcmDesc.m_strConfigPath       = m_pDoc->m_strConfigPath;
   vcmDesc.m_pCBImageProcess     = CCamera::CallbackImageProcess;
   vcmDesc.m_pCBDisplay          = CCamera::CallbackDisplay;
   vcmDesc.m_pCBCamEvent         = CCamera::CallbackCameraEvent;
   vcmDesc.m_pCBEncodeVideoFrame = CCamera::CallbackEncodeVideoFrame;
   vcmDesc.m_pCBVCConfigToBeLoad = CCamera::CallbackVCConfigToBeLoad;
   VCM.Initialize( vcmDesc );
   VCM.Activate();
}

void CLocalSystem::UnInitVCM()
{
   VCM.Deactivate();
   VCM.Uninitialize();
}

void CLocalSystem::VerifySafeConfigFile( const std::string& strPathName )
{
   // 백업한 파일이 있는지 체크해, 있으면 복구한다.
   std::string strBackName = strPathName;
   if( !strBackName.empty() ) {
      strBackName += "_";
      if( IsExistFile( strBackName ) ) {
         // 복구할 파일이 있으면, 복구하자.
         logw( "[%s] Config file is recovered!\n", strPathName.c_str() );

         std::string strBackName2;
         strBackName2 = strBackName;
         strBackName2 += "_";
         RemoveFileEx( strBackName2 );
         MoveFileEx( strPathName, strBackName2 ); // 깨진 파일은 백업해 놓는다.
         MoveFileEx( strBackName, strPathName ); // 기존파일은 복구한다.
      }
   }
}

void CLocalSystem::BackupOldConfigFile( const std::string& strPathName )
{
   // 이전 설정파일을 "xml_" 파일로 백업해놓는다.
   std::string strBackName = strPathName;
   if( !strBackName.empty() ) {
      if( IsExistFile( strPathName ) ) // 파일이 존재할 경우에만
      {
         strBackName += "_";
         RemoveFileEx( strBackName );
         MoveFileEx( strPathName, strBackName );
      }
   }
}

void CLocalSystem::FinishSafeConfigFile( const std::string& strPathName )
{
   // 백업한 파일을 지운다. 정상적으로 저장완료.
   std::string strBackName = strPathName;
   if( !strBackName.empty() ) {
      strBackName += "_";
      RemoveFileEx( strBackName );
   }
}

int CLocalSystem::LoadCFGFile()
{
   std::string strXmlCfgPathName = sutil::sformat( "%s/%s", m_pDoc->m_strConfigPath.c_str(), m_strLocalSystemCfgFileName.c_str() );
   if( IsExistFile( strXmlCfgPathName ) == false ) {
      LOGW << "No Config File : " << strXmlCfgPathName;
      m_pDoc->SetExitProcess( Xml_Read_Config_Not_Exist );
      return Xml_Read_Config_Not_Exist;
   }

   CCriticalSectionSP cs( m_csLocalCfgRW );
   VerifySafeConfigFile( strXmlCfgPathName );
   CXMLIO xmlIO;
   if( DONE != xmlIO.ResetFile( strXmlCfgPathName, XMLIO_Read ) ) {
      m_pDoc->SetExitProcess( Xml_Read_Config_Parsing_Failed );
      return Xml_Read_Config_Parsing_Failed;
   }

   IVXException ret = Succeed;
   try {
      ReadSetup( &xmlIO, -1, -1, 0 );
   } catch( IVXException error ) {
      ret = error;
      m_pDoc->SetExitProcess( ret );
      LOGF << "SystemConfig.xml load failed... Code[" << ret << "]";
   }

   return (int)ret;
}

int CLocalSystem::SaveCFGFile()
{
   int e_code = DONE;
   CCriticalSectionSP cs( m_csLocalCfgRW );
   // 설정 내용을 메모리에 Write 한다.

   std::string strXmlCfgPathName = sutil::sformat( "%s/%s", m_pDoc->m_strConfigPath.c_str(), m_strLocalSystemCfgFileName.c_str() );

   BackupOldConfigFile( strXmlCfgPathName );

   CXMLIO xmlIO( strXmlCfgPathName, XMLIO_Write );
   if( WriteSetup( &xmlIO, -1, -1, 0 ) ) {
      e_code = 1;
      LOGE << "Error: Cannot save the configuration file[" << m_strLocalSystemCfgFileName << "]";
   } else {
      FinishSafeConfigFile( strXmlCfgPathName );
      LOGI << "Saved the configuration file[" << m_strLocalSystemCfgFileName << "]";
   }

   return ( e_code );
}

void CLocalSystem::SetSystemSetupToBeSaved( int nDelayTime )
{
   m_nSaveLocalSystemSetupSaveTime = GetTickCount() + nDelayTime;
}

BOOL CLocalSystem::ApplySetup()
{
   ASSERT( System_Temp );
   int cs_items;
   BOOL bSystemDeactivate = FALSE;

   //시스템 설정 변경
   cs_items = GetChangedSetupItem( System_Temp );
   if( cs_items ) {
      bSystemDeactivate = CheckDeactivation( System_Temp, cs_items );
      TempToOrigin( System_Temp, cs_items );
   }

   if( bSystemDeactivate ) {
      DeactivateSystem();
   }

   //카메라 설정 변경
   for( int i = 0; i < CameraNum; i++ ) {
      if( SetupChannelNo >= 0 && SetupChannelNo != i )
         continue;
      CCamera* pCamera = Cameras[i];
      pCamera->ApplySetupLocal( bSystemDeactivate );
   }

   if( bSystemDeactivate ) {
      ActivateSystem();
   }
   return TRUE;
}

BOOL CLocalSystem::InitializeWizardSetup( int nSetupChannelNo )
{
   System_Temp = CreateTemp( nSetupChannelNo );
   if( !System_Temp )
      return FALSE;
   return TRUE;
}

BOOL CLocalSystem::FinalizeWizardSetup( BOOL bSaveChange )
{
   if( !System_Temp )
      return FALSE;

   if( bSaveChange ) {
      ApplySetup();
   }

   DeleteTempSystem();
   return TRUE;
}

CLocalSystem* CLocalSystem::CreateTemp( int nSetupChannelNo, int nExcludeSetupChannelNo )
{
   int i;

   SetupChannelNo = nSetupChannelNo;
   if( NULL == System_Temp )
      System_Temp = new CLocalSystem;
   System_Temp->IsTemp                                  = TRUE;
   System_Temp->VCM.GetVCMDesc()->m_pCBVCConfigToBeLoad = CCamera::CallbackVCConfigToBeLoad;
   FileIO buffer;
   uint32_t nCheckStart = GetTickCount();
   CXMLIO xmlWriteIO( &buffer, XMLIO_Write );
   xmlWriteIO.m_nCustOpt = IVXXMLIOOpt_DoNotEnDecodeImage;
   WriteSetup( &xmlWriteIO, nSetupChannelNo, nExcludeSetupChannelNo, 0 );
   buffer.GoToStartPos();
   CXMLIO xmlReadIO( &buffer, XMLIO_Read );
   xmlReadIO.m_nCustOpt = IVXXMLIOOpt_DoNotEnDecodeImage;

   try {
      System_Temp->ReadSetup( &xmlReadIO, nSetupChannelNo, nExcludeSetupChannelNo, 0 );

      uint32_t nElapsedTime = GetTickCount() - nCheckStart;
      //logd ("********* CreateTemp Elapsed Time = %d ***********\n", nElapsedTime);
      for( i = 0; i < CameraNum; i++ ) {
         CCamera* pOrgCamera = Cameras[i];
         CCamera* pTmpCamera = System_Temp->Cameras[i];
         pOrgCamera->LinkToTemp( pTmpCamera );
      }

   } catch( IVXException error ) {
      if( System_Temp )
         delete System_Temp;
      System_Temp = NULL;
   }

   return System_Temp;
}

void CLocalSystem::DeleteTempSystem()
{
   if( System_Temp ) {
      System_Temp->RemoveAllCameras();
      delete System_Temp;
      System_Temp = NULL;
      for( int i = 0; i < CameraNum; i++ ) {
         CCamera* pCamera   = Cameras[i];
         pCamera->TmpCamera = NULL;
      }
   }
}

int CLocalSystem::TempToOrigin( CLocalSystem* t_sys, int item )
{
   FileIO buffer;
   CXMLIO xmlWriteIO( &buffer, XMLIO_Write );

   if( t_sys->WriteSetupCmn( &xmlWriteIO, 0, item ) )
      return ( 1 );

   buffer.GoToStartPos();

   CXMLIO xmlReadIO( &buffer, XMLIO_Read );

   if( ReadSetupCmn( &xmlReadIO, 0, item ) )
      return ( 1 );

   return ( DONE );
}

int CLocalSystem::ReadSetup( CXMLIO* pIO, int nSetupChannelNo, int nExcludeSetupChannelNo, int nRWFlag ) throw( IVXException )
{
   CCriticalSectionSP cs( m_csLocalCfgRW );

   int nCameraNum = 0;
   // 로컬 시스템에만 필요한 설정들을 읽는다.
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "LocalSystem" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "SysGUIID", &SystemGUID );

      if( 0 == SystemGUID.length() )
         SystemGUID = CreateGUID();

      CXMLNode xmlNode( pIO );
      if( xmlNode.Start( "System" ) ) {
         xmlNode.Attribute( TYPE_ID( string ), "Name", &SystemName );
         xmlNode.Attribute( TYPE_ID( int ), "CamNum", &nCameraNum );

         //if (pIO->GetStorageType() == FIO_STORAGE_FILE)
         {
            ReadSetupCmn( pIO, nRWFlag );

            CCriticalSectionSP co( csCameraList );

            if( nCameraNum < 0 ) nCameraNum = 0;
            if( nCameraNum == 0 ) throw Xml_Read_Config_Camera_Number_Zero;

            if( (int)Cameras.size() != nCameraNum ) {
               RemoveAllCameras();
               for( int i = 0; i < nCameraNum; i++ ) {
                  CCamera* pCamera = new CCamera( this, i );
                  Cameras.push_back( pCamera );
               }
            }

            CameraNum = Cameras.size(); // 로딩된 실제 카메라 수

            for( int i = 0; i < CameraNum; i++ ) {
               CCamera* pCamera = Cameras[i];
               if( nSetupChannelNo >= 0 && nSetupChannelNo != i )
                  continue;
               if( nExcludeSetupChannelNo >= 0 && nExcludeSetupChannelNo == i )
                  continue;
               pCamera->ReadFile( pIO, nRWFlag );
            }
         }
         xmlNode.End();
      } else
         throw Xml_Read_Config_System_Node_Not_Exist; // "System" Node check
      if( xmlNode.Start( "VAServer" ) ) {
         xmlNode.Attribute( TYPE_ID( BOOL )  , "StandAlone"       , &m_pDoc->m_StandAlone );
         xmlNode.Attribute( TYPE_ID( int )   , "Manager2Process"  , &m_Manager2Process );
         xmlNode.Attribute( TYPE_ID( string ), "ManagerURL"       , &m_ManagerURL );
         xmlNode.Attribute( TYPE_ID( int )   , "LogLevel"         , &m_LogLevel );
         xmlNode.Attribute( TYPE_ID( int )   , "MetaType"         , &m_MetaType );
         xmlNode.Attribute( TYPE_ID( BOOL )  , "SaveMetaImageInCurrDir", &m_pDoc->m_SaveMetaInCurrDir );
         xmlNode.Attribute( TYPE_ID( string ), "SaveMetaImagePath", &m_SaveMetaImagePath );
         xmlNode.Attribute( TYPE_ID( int )   , "WhoService"       , &m_WhoService );
         xmlNode.Attribute( TYPE_ID( int )   , "ProcessHeartbeat" , &m_ProcessHeartbeat );
         xmlNode.Attribute( TYPE_ID( string ), "LogPath"     , &m_Uplus_Log_Path_ );
         xmlNode.Attribute( TYPE_ID( int )   , "LogCycle"    , &m_Uplus_Log_Cycle_ );
         xmlNode.End();
         
#ifdef __UPLUS_LOGGER
         std::string UPlus_now_filename_ = LGU::Logger::GetFirstLogFilePath(m_Uplus_Log_Cycle_, m_Uplus_Log_Path_);
         LGU::Logger::SetWriteLogPath(UPlus_now_filename_);
         LGU::Logger::SetHeaderItem( COMMAND, "START");
         LGU::Logger::SetHeaderItem( FROM_SERVICE_NAME, "VAP" );
         LGU::Logger::SetHeaderItem( RES_SERVICE_NAME, "VAM" );
         LGU::Logger::SetHeaderItem( VA_PROCESS_CALL_TIME );
         LOGI_(L) << LGU::Logger::header (1);
         LOGI << "LGUPlus Log Save File Complete\n";
#endif
      }
   } else
      throw Xml_Read_Config_LocalSystem_Node_Not_Exist; // "LocalSystem" Node check

   return ( DONE );
}

int CLocalSystem::WriteSetup( CXMLIO* pIO, int nSetupChannelNo, int nExcludeSetupChannelNo, int nRWFlag )
{
   CCriticalSectionSP cs( m_csLocalCfgRW );
   // 로컬 시스템에만 필요한 설정들을 쓴다.
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "LocalSystem" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "SysGUIID", &SystemGUID );
      if( 0 == SystemGUID.length() )
         SystemGUID = CreateGUID();

      CXMLNode xmlNode( pIO );
      if( xmlNode.Start( "System" ) ) {
         xmlNode.Attribute( TYPE_ID( string ), "Name", &SystemName );
         xmlNode.Attribute( TYPE_ID( int )   , "CamNum", &CameraNum );

         //if (pIO->GetStorageType() == FIO_STORAGE_FILE)
         {
            WriteSetupCmn( pIO, nRWFlag );

            for( int i = 0; i < CameraNum; i++ ) {
               if( nSetupChannelNo >= 0 && nSetupChannelNo != i )
                  continue;
               if( nExcludeSetupChannelNo >= 0 && nExcludeSetupChannelNo == i )
                  continue;
               Cameras[i]->WriteFile( pIO, nRWFlag );
            }
         }
         xmlNode.End();
      }

	   if( xmlNode.Start( "VAServer" ) ) {
         xmlNode.Attribute( TYPE_ID( BOOL )  , "StandAlone"       , &m_pDoc->m_StandAlone );
	      xmlNode.Attribute( TYPE_ID( int )   , "Manager2Process"  , &m_Manager2Process );
	      xmlNode.Attribute( TYPE_ID( string ), "ManagerURL"       , &m_ManagerURL );
	      xmlNode.Attribute( TYPE_ID( int )   , "LogLevel"         , &m_LogLevel );
	      xmlNode.Attribute( TYPE_ID( int )   , "MetaType"         , &m_MetaType );
         xmlNode.Attribute( TYPE_ID( BOOL )  , "SaveMetaImageInCurrDir", &m_pDoc->m_SaveMetaInCurrDir );
	      xmlNode.Attribute( TYPE_ID( string ), "SaveMetaImagePath", &m_SaveMetaImagePath );
	      xmlNode.Attribute( TYPE_ID( int )   , "WhoService"       , &m_WhoService );
	      xmlNode.Attribute( TYPE_ID( int )   , "ProcessHeartbeat" , &m_ProcessHeartbeat );
         xmlNode.Attribute( TYPE_ID( string ), "LogPath"     , &m_Uplus_Log_Path_ );
         xmlNode.Attribute( TYPE_ID( int )   , "LogCycle"    , &m_Uplus_Log_Cycle_ );
	      xmlNode.End();
	   }
   }
   return ( DONE );
}

int CLocalSystem::AddCamera()
{
   ASSERT( CameraNum == Cameras.size() );

   if( CameraNum >= DeviceChannelNum ) {
      LOGE << "Exceed CameraNum[ " << CameraNum << "] >= DeviceChannelNum[" << DeviceChannelNum << "]";
      return ( 8 );
   }

   Status |= SysState_OnCamAdd;
   int nChannelNo   = CameraNum;
   CCamera* pCamera = new CCamera( this, nChannelNo );

   BOOL bAddCamera = TRUE;

   if( !bAddCamera ) {
      delete pCamera;
      Status &= ~SysState_OnCamAdd;
      return ( 9 );
   }
   AddCamera( pCamera );
   Status &= ~SysState_OnCamAdd;

   return ( DONE );
}

int CLocalSystem::AddCamera( CCamera* pNewCamera )
{
   ASSERT( CameraNum == Cameras.size() );

   if( CameraNum >= DeviceChannelNum ) {
      LOGE << "Exceed CameraNum[" << CameraNum << "] >= DeviceChannelNum[" << DeviceChannelNum << "]";
      return ( 8 );
   }

   Status |= SysState_OnCamAdd;

   pNewCamera->CameraGuid = CreateGUID();
   pNewCamera->UID        = m_pDoc->AllocateUID();

   pNewCamera->VC->m_nState |= VCState_IPCamStreamVideoSizeToBeChecked;
   pNewCamera->Activate();

   {
      CCriticalSectionSP co( csCameraList );
      Cameras.push_back( pNewCamera );
   }

   SetSystemSetupToBeSaved( 5000 );
   Status &= ~SysState_OnCamAdd;

   return ( DONE );
}

int CLocalSystem::IsCanRemoveCamera( CCamera* pCamera )
{
   return ( DONE );
}

int CLocalSystem::RemoveCamera( CCamera* pCamera )
{
   if( !pCamera ) return ( DONE );
   int nRet = IsCanRemoveCamera( pCamera );
   if( ( DONE ) != nRet ) return nRet;

   for( int j = 0; j < IVX_VIDEO_STREAM_NUM; j++ ) {
      CRTSPStreamingChannel* pRTSPStreamingChannel = pCamera->GetRtspStreamingChannel( j );
      if( pRTSPStreamingChannel ) m_RTSPServer->Unregister( pRTSPStreamingChannel );
   }

   pCamera->Deactivate();

   RemoveCamera( pCamera->ChannelNo );

   return ( DONE );
}

BOOL CLocalSystem::RemoveCamera( int nChannelNo )
{
   DeallocateCamera( nChannelNo );

   int i;
   CCamera* pCamera;
   std::string strDefCameraName;
   // 채널 번호 및 카메라 이름을 다시 부여한다.
   for( i = 0; i < CameraNum; i++ ) {
      pCamera          = Cameras[i];
      strDefCameraName = sutil::sformat( "Camera-%d", pCamera->ChannelNo + 1 );
      if( pCamera->CameraName == strDefCameraName ) {
         pCamera->SetCameraName( i );
      }
      pCamera->ChannelNo            = i;
      pCamera->VC->m_nChannelNo_IVX = i;
   }

   CHFLAG mask = 0;
   for( i = 0; i < nChannelNo; i++ )
      mask |= ( CHFLAG( 1 ) << i );

   return TRUE;
}

int CLocalSystem::RemoveAllCameras()
{
   CameraIter iter    = Cameras.begin();
   CameraIter iterEnd = Cameras.end();
   while( iter != iterEnd ) {
      CCamera* pCamera = *iter;
      delete pCamera;
      iter++;
   }
   Cameras.clear();
   return 0;
}

//시스템을 다시 시작해야하는지 여부를 결정
int CLocalSystem::CheckDeactivation( CLocalSystem* system, int filter )
{
   if( filter & IVXSetupItemMask_VideoInput ) {
      if( VCM.CheckRestart( system->VCM ) )
         return ( 1 );
   }
   return ( DONE );
}

int CLocalSystem::GetChangedSetupItem( CLocalSystem* system, int filter )
{
   int c_item = 0;
   if( filter & IVXSetupItemMask_VideoInput ) {
      if( VCM.CheckChange( system->VCM ) ) c_item |= IVXSetupItemMask_VideoInput;
   }

   return c_item;
}

int CLocalSystem::ActivateSystem()
{
   int i;
   logd( "[LS] ActivateSystem - Start\n" );

   if( Status & SysState_SystemActivated ) return ( DONE );
   Status |= SysState_SystemActivated;

   CCamera* pCamera = NULL;
   // 카메라가 Activate 전에 DB 가 먼저 시작되어야 한다.
   for( i = 0; i < CameraNum; i++ ) {
      logd( "Camera[%d] Activate Starting.", i );
      pCamera  = Cameras[i];
      int iRet = pCamera->Activate();
      if( iRet != 0 ) {
         loge( "Camera[%d] Activate error[%d]", i, iRet );
      }
   }

   if( m_rtspServerPort > 0 ) {
      m_RTSPServer->m_pParam                        = (LPVOID)this;
      m_RTSPServer->m_rtspServerPortNum             = (ushort)m_rtspServerPort;
      m_RTSPServer->m_rtspServerPortNum_Alternetive = (ushort)m_rtspServerPort_Alternetive;

      // NOTE(yhpark): you can skip RTSP Server.
      m_RTSPServer->Activate();
   }

   logd( "[LS] ActivateSystem - Done\n" );
   return ( DONE );
}

void CLocalSystem::DeactivateSystem()
{
   size_t i;
   if( !( Status & SysState_SystemActivated ) ) return;

   DeleteTempSystem();
   logd( "[LS] Deactivate System - Start\n" );
   Status &= ~SysState_SystemActivated;

   for( i = 0; i < Cameras.size(); i++ )
      Cameras[i]->VC->SignalDeactivate();
   // 각 카메라를 Deactivate 한다.
   for( i = 0; i < Cameras.size(); i++ ) {
      Cameras[i]->Deactivate();
      std::string str = sutil::sformat( "Deactivating Camera [%d] - OK", i + 1 );
   }

   m_RTSPServer->Deactivate();

   RemoveAllCameras();

   logd( "[LS] Deactivating System - Done\n" );
}

UINT CLocalSystem::ThreadFunc_LocalSystemProc( LPVOID pParam )
{
#ifdef SUPPORT_PTHREAD_NAME
   pthread_setname_np( pthread_self(), "LocalSystem" );
#endif
   ( (CLocalSystem*)pParam )->LocalSystemProc();
   return ( 0 );
}

void CLocalSystem::SendLiveFps()
{
   MetaGeneratorSP generator = GetGenerator( MetaGeneratorType::JSON, this, nullptr );
   generator->FillLiveFps( Camera_Live_Fps );
   CRestSender::getInstance()->SendLiveFps( generator.get() );
}

void CLocalSystem::performanceMonitoring( CMMTimer* pTimer_1sec )
{
   TimerCheckerManager::Get().DebugMsg( pTimer_1sec->m_szName, pTimer_1sec->m_nPeriod );
   auto timer = TimerCheckerManager::Get().GetTimer( pTimer_1sec->m_szName );
   assert( timer );
   double real_interval = timer.get()->getCurrentDiff();

   uint total_process_count = 0;
   CCriticalSectionSP co( csCameraList );
   if( 0 == CameraNum )
      return;

   for( CCamera* pCamera : Cameras ) {
      assert( pCamera );
      total_process_count += pCamera->m_processed_count;
      pCamera->m_mutex_processed_count.lock();
      pCamera->m_processed_count = 0;
      pCamera->m_mutex_processed_count.unlock();
   }

   double measured_avg_fps_for_all_cams = total_process_count / double( CameraNum ) * 1000 / real_interval;
   //logd( "avg [ %6.3f ]", measured_avg_fps_for_all_cams );

   static uint call_count = 0;
   static std::vector<double> vec_avgs_in_sec;

   call_count++;
   vec_avgs_in_sec.push_back( measured_avg_fps_for_all_cams );

   if( call_count % 10 == 0 ) {
      double sum = std::accumulate( vec_avgs_in_sec.begin(), vec_avgs_in_sec.end(), 0.0 );
      double avg = sum / vec_avgs_in_sec.size();

      double min = *std::min_element( vec_avgs_in_sec.begin(), vec_avgs_in_sec.end() );
      double max = *std::max_element( vec_avgs_in_sec.begin(), vec_avgs_in_sec.end() );

      // Standard Deviation
      std::vector<double> diff( vec_avgs_in_sec.size() );
      std::transform( vec_avgs_in_sec.begin(), vec_avgs_in_sec.end(), diff.begin(),
                      [avg]( double x ) {
                         return x - avg;
                      } );
      double sq_sum = std::inner_product( diff.begin(), diff.end(), diff.begin(), 0.0 );
      double stdev  = std::sqrt( sq_sum / vec_avgs_in_sec.size() );

      //logi( "avg [ %6.3f ] min~max [ %6.3f ~ %6.3f ] stdev[ %6.3f ] in 10 secs.", avg, min, max, stdev );
      vec_avgs_in_sec.clear();
   }
}

void CLocalSystem::LocalSystemProc()
{
   int i;
   CMMTimer* pTimer_SetVideoSendingFlags = m_pTimerMgr->GetNewTimer( "LocalSystem SetVideoSendingFlags" );
   pTimer_SetVideoSendingFlags->SetFPS( 10 );
   pTimer_SetVideoSendingFlags->StartTimer();

   CMMTimer* pTimer_1sec = m_pTimerMgr->GetNewTimer( "LocalSystem 1sec" );
   pTimer_1sec->SetFPS( 1 );
   pTimer_1sec->StartTimer();

   CMMTimer* pTimer_100ms = m_pTimerMgr->GetNewTimer( "LocalSystem 100ms" );
   //pTimer_100ms->SetFPS( 10 );
   pTimer_100ms->SetMilliSec( 100 );
   pTimer_100ms->StartTimer();

   CWaitForMultipleObjects wmo;
   std::vector<CEventEx*> vecEvt;

   // yhpakr(WARNING): Sequence matching between push and enum.
   vecEvt.push_back( &m_evtExitLocalSystemProc );
   vecEvt.push_back( &m_evtRestartCamera );
   vecEvt.push_back( &pTimer_SetVideoSendingFlags->GetTimerEvent() );
   vecEvt.push_back( &pTimer_1sec->GetTimerEvent() );
   vecEvt.push_back( &pTimer_100ms->GetTimerEvent() );

   enum LocalSystemProcEvtType {
      Event_ExitLocalSystemProc = WAIT_OBJECT_0,
      Event_RestartCameras,
      Event_SetVideoSendingFlags,
      Event_Timer_1sec,
      Event_Timer_100ms,
   };

   wmo.Init( vecEvt );

   while( true ) {
      wmo.WaitForMultipleObjects( FALSE );
      std::vector<bool> vecSignal = wmo.GetSignal();

      if( vecSignal[Event_ExitLocalSystemProc] ) {
         logd( "LocalSystemProc Event_ExitLocalSystemProc\n" );
         wmo.ClearAllEventEx();
         break;
      }

      // 카메라 재시작 수행
      if( vecSignal[Event_RestartCameras] ) {
         if( !( m_nGlobalState & ( GlobalState_RemovingCameras | GlobalState_ClosingAllSystems ) ) && !( m_nGlobalState & ( GlobalState_RemovingSystem ) ) ) {
            //m_csSystem.lock ();
            for( i = 0; i < CameraNum; i++ ) {
               CCamera* pCamera = GetCamera( i );
               if( pCamera->m_nState & int(CamState_ToBeRestarted) ) {
                  pCamera->VC->m_nState |= VCState_IPCamStreamVideoSizeToBeChecked;
                  pCamera->Deactivate();
                  pCamera->Activate();
                  pCamera->m_nState &= ~CamState_ToBeRestarted;
               }
               if( pCamera->m_nState & int(CamState_ToBeActivate) ) {
                  pCamera->Activate();
                  pCamera->m_nState &= ~CamState_ToBeActivate;
               }
            }
            for( i = 0; i < CameraNum; i++ ) {
               CCamera* pCamera = GetCamera( i );
               if( pCamera->m_nState & int(CamState_ToBeDeactivate) ) {
                  pCamera->VC->SignalDeactivate();
#ifdef __UPLUS_LOGGER
                  LGU::Logger::SetHeaderItem( COMMAND, "START");
                  LGU::Logger::SetHeaderItem( FROM_SERVICE_NAME, "Camera" );
                  LGU::Logger::SetHeaderItem( RES_SERVICE_NAME, "VAP" );
                  LGU::Logger::SetHeaderItem( VA_PROCESS_CALL_TIME );
                  LGU::Logger::SetHeaderItem( VA_PROCESS_TERM_TIME );
                  LOGI_(L) << LGU::Logger::header (1);
#endif
               }
            }
            for( i = 0; i < CameraNum; i++ ) {
               CCamera* pCamera = GetCamera( i );
               if( pCamera->m_nState & int(CamState_ToBeDeactivate) ) {
                  pCamera->m_nState &= ~CamState_ToBeDeactivate;
                  pCamera->Deactivate();
               }
            }
            //m_csSystem.unlock ();
         }
      }

      // 비디오 전송여부 플레그 셋팅
      if( vecSignal[Event_SetVideoSendingFlags] ) {
         DetermineStartStopVideoStreamingThread();
      }

      // 1초 주기로 처리되는 작업들
      if( vecSignal[Event_Timer_1sec] ) {
         
         // yhpark. monitoring.
         performanceMonitoring( pTimer_1sec );

         static int count = 1;
         if( 0 == ( count % m_ProcessHeartbeat ) ) // Keep alive 전송
         {
            SendLiveFps();
         }

         // 로컬시스템 설정 저장
         if( ( m_nSaveLocalSystemSetupSaveTime && ( GetTickCount() > m_nSaveLocalSystemSetupSaveTime ) ) ) {
            // TODO(yhpark) :: save config later, why?
            SaveCFGFile(); // 일정 시간 경과뒤 자동저장
            __PTZ3DFile.SaveFile();
            m_nSaveLocalSystemSetupSaveTime = 0;
            logi( "Local System Setup has been saved!!\n" );
         }

         if( m_nGlobalState & GlobalState_ObjectCountersToBeReset ) {
            m_csSystem.Lock();
            for( i = 0; i < CameraNum; i++ ) {
               CCamera* pCamera = GetCamera( i );
               if( pCamera )
                  pCamera->m_nStateEx2 |= CamStateEx2_ObjectCountersTeBeReset;
            }
            m_csSystem.Unlock();

            m_nGlobalState &= ~GlobalState_ObjectCountersToBeReset;
         }

         count++;
      }

      // 100 ms 단위로 처리되는 작업들
      if( vecSignal[Event_Timer_100ms] ) {
         if( !( m_nGlobalState & GlobalState_ClosingAllSystems ) ) {
            // PTZ 이동 체크
            CCamera* pCamera;

            for( int i = 0; i < CameraNum; i++ ) {
               pCamera = GetCamera( i );
               // 각 카메라 별 ShowLiveFrame 판단
               if( pCamera->IsShowLiveFrame() ) {
                  if( !( pCamera->m_nStateEx & CamStateEx_ShowLiveFrame ) )
                     pCamera->m_nStateEx |= CamStateEx_ShowLiveFrame;
               } else {
                  if( pCamera->m_nStateEx & CamStateEx_ShowLiveFrame )
                     pCamera->m_nStateEx &= ~CamStateEx_ShowLiveFrame;
               }
               // 각 카메라 별 RequestVideoStreamToIVXVS 판단
               if( pCamera->m_nStateEx2 & CamStateEx2_RequestVideoStreamToIVXVS )
                  pCamera->m_nStateEx2 &= ~CamStateEx2_RequestVideoStreamToIVXVS;
               // 각 카메라 별 DecodingLiveVideo 판단
               if( pCamera->IsDecodingLiveVideo() ) {
                  if( !( pCamera->m_nStateEx2 & CamStateEx2_DecodingLiveVideo ) )
                     pCamera->m_nStateEx2 |= CamStateEx2_DecodingLiveVideo;
               } else {
                  if( pCamera->m_nStateEx2 & CamStateEx2_DecodingLiveVideo )
                     pCamera->m_nStateEx2 &= ~CamStateEx2_DecodingLiveVideo;
               }
            }
         }
      }
   }

   pTimer_1sec->StopTimer();
   pTimer_100ms->StopTimer();
   pTimer_SetVideoSendingFlags->StopTimer();
   m_pTimerMgr->DeleteTimer( pTimer_1sec->m_nTimerID );
   m_pTimerMgr->DeleteTimer( pTimer_100ms->m_nTimerID );
   m_pTimerMgr->DeleteTimer( pTimer_SetVideoSendingFlags->m_nTimerID );
}

////////////////////////////////////////////////////////////////////////////////////////////////

void CLocalSystem::OnRTSPRequest( const std::string& strStreamName )
{
   int i, j;
   Lock_CameraList();
   for( i = 0; i < CameraNum; i++ ) {
      CCamera* pCamera = Cameras[i];
      if( pCamera ) {
         std::vector<int> vtAvailableStreamIndexs;
         pCamera->VC->GetAvailableStreamIndex( vtAvailableStreamIndexs );
         //Live
         for( j = 0; j < IVX_VIDEO_STREAM_NUM; j++ ) {
            CRTSPStreamingChannel* pRTSPStreamingChannel = pCamera->GetRtspStreamingChannel( j );
            if( strStreamName == pRTSPStreamingChannel->m_strStreamName || strStreamName == pRTSPStreamingChannel->m_strStreamName_Alias1 || strStreamName == pRTSPStreamingChannel->m_strStreamName_Alias2 ) {
               for( int ii = 0; ii < (int)vtAvailableStreamIndexs.size(); ii++ ) {
                  if( vtAvailableStreamIndexs[ii] == j )
                     m_RTSPServer->Register( pRTSPStreamingChannel );
               }
            }
         }
      }
   }
   Unlock_CameraList();
}

void CLocalSystem::OnStreamingChannelClosed( CRTSPStreamingChannel* pRTSPStreamingChannel )
{
   if( NULL == pRTSPStreamingChannel ) return;

   logd( "RTSP Closed client  [%s]", pRTSPStreamingChannel->m_strStreamName.c_str() );

   Lock_CameraList();
   m_RTSPServer->Unregister( pRTSPStreamingChannel );
   Unlock_CameraList();
}

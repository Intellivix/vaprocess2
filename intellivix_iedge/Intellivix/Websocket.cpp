﻿#ifdef __LIB_SIMPLEWEBSOCKET

#include "Websocket.h"

// debug message shortener.
#define WHOIS std::string( connection.get()->remote_endpoint_address() + ":" + std::to_string( connection.get()->remote_endpoint_port() ) )

Websocket& Websocket::getInstance()
{
   static Websocket instance;
   return instance;
}

Websocket::Websocket()
{
   LOGD << "Creating Websocket's Singleton instance.";
}

Websocket::~Websocket()
{
   StopThread();
}

void Websocket::Prepare( CIntelliVIXApp* pIntellivix, ushort port )
{
   m_ws_server.config.port = port;

   assert( pIntellivix );
   m_pIntellivix = pIntellivix;
   assert( pIntellivix->m_pLocalSystem );
   m_pLocalSystem = pIntellivix->m_pLocalSystem;

   /// Intellivix service.
   ServiceAddCamera();
   ServiceModifyCamera();
   ServiceListCamera();
   ServiceRemoveCamera();
   ServiceMeta();

   /// examples service.
   ServiceExamEcho();
   ServiceExamEchoThrice();
   ServiceExamBroadCast();
}

void Websocket::StartThread()
{
   m_thread = std::thread( [this]() {
#ifdef SUPPORT_PTHREAD_NAME
      pthread_setname_np( pthread_self(), "Websocket" );
#endif
      LOGI << "Websocket thread starting. port : " << m_ws_server.config.port;
      m_ws_server.start();
   } );
}

void Websocket::StopThread()
{
   for( auto& a_connection : m_ws_server.get_connections() )
      a_connection->send_close( 0 );

   m_ws_server.stop();
   if( m_thread.joinable() )
      m_thread.join();
}

void Websocket::SendText( shared_ptr<WsServer::Connection> connection,
                          const std::string& message,
                          const std::function<void( const SimpleWeb::error_code& )>& callback )
{
   auto send_stream = make_shared<WsServer::SendStream>();
   *send_stream << message;
   connection->send( send_stream, callback, 129 ); // fin_rsv_opcode: 129 = one fragment & text.
}

void Websocket::SendBinary( shared_ptr<WsServer::Connection> connection,
                            const std::shared_ptr<WsServer::SendStream>& send_stream,
                            const std::function<void( const SimpleWeb::error_code& )>& callback )
{
   connection->send( send_stream, callback, 130 ); // fin_rsv_opcode: 130 = one fragment & binary.
}

void Websocket::BroadcastBinary( const std::shared_ptr<WsServer::SendStream>& send_stream,
                                 const std::function<void( const SimpleWeb::error_code& )>& callback )
{
   for( auto& a_connection : m_ws_server.get_connections() )
      SendBinary( a_connection, send_stream, callback );
}

void Websocket::BroadcastText( const string& message_to_send,
                               const std::function<void( const SimpleWeb::error_code& )>& callback )
{
   for( auto& a_connection : m_ws_server.get_connections() )
      SendText( a_connection, message_to_send, callback );
}

///
/// === Directory Service. Intellivix ===
///
void Websocket::ServiceAddCamera()
{
   std::string path = "addCamera";
   auto& endpoint   = m_ws_server.endpoint["^/" + path + "/?$"];

   endpoint.on_message = [path, this]( shared_ptr<WsServer::Connection> connection, shared_ptr<WsServer::Message> message ) {

      auto recv_message = message->string();
      LOGD << path << " received: \"" << recv_message << "\" from " << WHOIS;

      std::string reply_message;
      if( recv_message == "do" ) {

         LOGV << "valid protocol. adding camera starting.";
         int iRet = AddCamera( reply_message );
         if( iRet ) {
            reply_message = "#error: failed to addCamera. code:" + std::to_string( iRet );
            LOGE << path << reply_message;
         } else {
            LOGI << path << " done. reply message:" << reply_message;
         }
      } else {
         reply_message = "#error: invalid protocol.";
         LOGE << path << reply_message << " [" + recv_message + "]";
      }

      LOGD << path << " Sending message \"" << reply_message << "\" to " << WHOIS;
      SendText( connection, reply_message, [path]( const SimpleWeb::error_code& ec ) {
         if( ec ) {
            LOGE << path << " Error sending message. "
                 << "Error: " << ec << ", error message: " << ec.message();
         } else {
            LOGV << path << " message sent successfully.";
         }
      } );

   };
   endpoint.on_open = [path, this]( shared_ptr<WsServer::Connection> connection ) {
      LOGD << path << " Opened connection " << WHOIS;
   };
   endpoint.on_close = [path, this]( shared_ptr<WsServer::Connection> connection, int status, const string& /*reason*/ ) {
      LOGW << path << " Closed connection " << WHOIS << " with status code " << status;
   };
   endpoint.on_error = [path, this]( shared_ptr<WsServer::Connection> connection, const SimpleWeb::error_code& ec ) {
      LOGE << path << " Error in connection " << WHOIS << "Error: " << ec << ", error message: " << ec.message();
   };
}

void Websocket::ServiceModifyCamera()
{
   std::string path = "modifyCamera";
   auto& endpoint   = m_ws_server.endpoint["^/" + path + "/?$"];

   endpoint.on_message = [path, this]( shared_ptr<WsServer::Connection> connection, shared_ptr<WsServer::Message> message ) {

      auto recv_message = message->string();
      LOGD << path << " received: \"" << recv_message << "\" from " << WHOIS;

      std::string reply_message;
      if( recv_message.find( "do#" ) == 0 ) {

         std::string camID = recv_message.substr( 3 );

         LOGV << path << " valid protocol. modifyCamera camera starting. cameraID:" << camID;
         int iRet = ModifyCamera( camID, reply_message );
         if( iRet ) {
            reply_message = "#error: failed to modifyCamera code:" + std::to_string( iRet );
            LOGE << reply_message;
         } else {
            LOGI << path << " done. reply message:" << reply_message;
         }
      } else {
         reply_message = "#error: invalid protocol.";
         LOGE << path << reply_message << " [" + recv_message + "]";
      }

      LOGD << path << " Sending message \"" << reply_message << "\" to " << WHOIS;
      SendText( connection, reply_message, [path]( const SimpleWeb::error_code& ec ) {
         if( ec ) {
            LOGE << path << " Error sending message. "
                 << "Error: " << ec << ", error message: " << ec.message();
         } else {
            LOGV << path << " message sent successfully.";
         }
      } );

   };
   endpoint.on_open = [path, this]( shared_ptr<WsServer::Connection> connection ) {
      LOGD << path << " Opened connection " << WHOIS;
   };
   endpoint.on_close = [path, this]( shared_ptr<WsServer::Connection> connection, int status, const string& /*reason*/ ) {
      LOGW << path << " Closed connection " << WHOIS << " with status code " << status;
   };
   endpoint.on_error = [path, this]( shared_ptr<WsServer::Connection> connection, const SimpleWeb::error_code& ec ) {
      LOGE << path << " Error in connection " << WHOIS << "Error: " << ec << ", error message: " << ec.message();
   };
}

void Websocket::ServiceListCamera()
{
   std::string path = "listCamera";
   auto& endpoint   = m_ws_server.endpoint["^/" + path + "/?$"];

   endpoint.on_message = [path, this]( shared_ptr<WsServer::Connection> connection, shared_ptr<WsServer::Message> message ) {

      auto recivedMsg = message->string();
      LOGD << path << " received: \"" << recivedMsg << "\" from " << WHOIS;

      std::string reply_message;
      if( recivedMsg == "do" ) {

         LOGV << path << " valid protocol. listing camera starting.";
         int iRet = GetCameraList( reply_message );
         if( iRet ) {
            reply_message = "#error: failed to listCamera. code:" + std::to_string( iRet );
            LOGE << reply_message;
         } else {
            LOGI << path << " done. reply message:" << reply_message;
         }
      } else {
         reply_message = "#error: invalid protocol.";
         LOGE << path << reply_message << " [" + recivedMsg + "]";
      }

      LOGD << path << " Sending message \"" << reply_message << "\" to " << WHOIS;
      SendText( connection, reply_message, [path]( const SimpleWeb::error_code& ec ) {
         if( ec ) {
            LOGE << path << " Error sending message. "
                 << "Error: " << ec << ", error message: " << ec.message();
         } else {
            LOGV << path << " message sent successfully.";
         }
      } );

   };
   endpoint.on_open = [path, this]( shared_ptr<WsServer::Connection> connection ) {
      LOGD << path << " Opened connection " << WHOIS;
   };
   endpoint.on_close = [path, this]( shared_ptr<WsServer::Connection> connection, int status, const string& /*reason*/ ) {
      LOGW << path << " Closed connection " << WHOIS << " with status code " << status;
   };
   endpoint.on_error = [path, this]( shared_ptr<WsServer::Connection> connection, const SimpleWeb::error_code& ec ) {
      LOGE << path << " Error in connection " << WHOIS << "Error: " << ec << ", error message: " << ec.message();
   };
}

void Websocket::ServiceRemoveCamera()
{
   std::string path = "removeCamera";
   auto& endpoint   = m_ws_server.endpoint["^/" + path + "/?$"];

   endpoint.on_message = [path, this]( shared_ptr<WsServer::Connection> connection, shared_ptr<WsServer::Message> message ) {

      auto recv_message = message->string();
      LOGD << path << " received: \"" << recv_message << "\" from " << WHOIS;

      std::string reply_message;
      if( recv_message.find( "do#" ) == 0 ) {

         std::string camID = recv_message.substr( 3 );

         LOGD << path << " valid protocol. RemoveCamera camera starting. cameraID:" << camID;
         int iRet = RemoveCamera( camID, reply_message );
         if( iRet ) {
            reply_message = "#error: failed to RemoveCamera code:" + std::to_string( iRet );
            LOGE << reply_message;
         } else {
            LOGI << path << " done. reply message:" << reply_message;
         }
      } else {
         reply_message = "#error: invalid protocol.";
         LOGE << path << reply_message << " [" + recv_message + "]";
      }

      LOGD << path << " Sending message \"" << reply_message << "\" to " << WHOIS;
      SendText( connection, reply_message, [path]( const SimpleWeb::error_code& ec ) {
         if( ec ) {
            LOGE << path << " Error sending message. "
                 << "Error: " << ec << ", error message: " << ec.message();
         } else {
            LOGV << path << " message sent successfully.";
         }
      } );

   };
   endpoint.on_open = [path, this]( shared_ptr<WsServer::Connection> connection ) {
      LOGD << path << " Opened connection " << WHOIS;
   };
   endpoint.on_close = [path, this]( shared_ptr<WsServer::Connection> connection, int status, const string& /*reason*/ ) {
      LOGW << path << " Closed connection " << WHOIS << " with status code " << status;
   };
   endpoint.on_error = [path, this]( shared_ptr<WsServer::Connection> connection, const SimpleWeb::error_code& ec ) {
      LOGE << path << " Error in connection " << WHOIS << "Error: " << ec << ", error message: " << ec.message();
   };
}

void Websocket::ServiceMeta()
{
   std::string path = "meta";
   auto& endpoint   = m_ws_server.endpoint["^/" + path + "/?$"];

   endpoint.on_message = [path, this]( shared_ptr<WsServer::Connection> connection, shared_ptr<WsServer::Message> message ) {
      auto recv_message = message->string();
      LOGD << path << " received: \"" << recv_message << "\" from " << WHOIS;
   };
   endpoint.on_open = [path, this]( shared_ptr<WsServer::Connection> connection ) {
      LOGD << path << " Opened connection " << WHOIS;
   };
   endpoint.on_close = [path, this]( shared_ptr<WsServer::Connection> connection, int status, const string& /*reason*/ ) {
      LOGW << path << " Closed connection " << WHOIS << " with status code " << status;
   };
   endpoint.on_error = [path, this]( shared_ptr<WsServer::Connection> connection, const SimpleWeb::error_code& ec ) {
      LOGE << path << " Error in connection " << WHOIS << "Error: " << ec << ", error message: " << ec.message();
   };
}

///
/// Prcoessing addCamera.
///

CCamera* Websocket::GetCamera( const std::string& camID )
{
   vector<string> vecSplit_ID;
   sutil::GetSplitStringList( camID, ',', vecSplit_ID );

   if( vecSplit_ID.size() < 2 ) {
      LOGE << "failed to parsing cameraID:" << camID;
      return NULL;
   }
   CCamera* pCamera = m_pLocalSystem->GetCameraFromGuid( vecSplit_ID[0] );
   if( !pCamera ) {
      pCamera = m_pLocalSystem->GetCameraFromUID( stoi( vecSplit_ID[1] ) );
      if( !pCamera ) {
         LOGE << "failed to find cameraID:" << camID;
         return NULL;
      }
   }

   return pCamera;
}

std::string Websocket::GetCameraID( CCamera* pCamera )
{
   string strCameraID = sutil::sformat( "%s,%d,%d", pCamera->CameraGuid.c_str(), pCamera->UID, pCamera->ChannelNo );
   return strCameraID;
}

int Websocket::AddCamera( std::string& strReply )
{
   std::string camera_name             = "added_by_websocket_cam";
   VideoChannelType video_channel_type = VideoChannelType_IPCamera;
   CompanyID company_id                = CompanyID_RTSP;
   IPCameraType ip_camera_type         = IPCameraType_RTSP;


   // Hanhwa.
   std::string ip_address = "192.168.0.24";
   std::string url        = "rtsp://192.168.0.24/profile8/media.smp";
   std::string user_name  = "admin";
   std::string password   = "pass0001!";
   
   int port_num = 554;
   float fps    = 30;

   // cellinx.
   //   std::string ip_address              = "192.168.0.144";
   //   std::string url                     = "rtsp://192.168.0.144/AVStream1_3";
   //   std::string user_name               = "root";
   //   std::string password                = "pass";

   CCamera* pCamera = new CCamera( m_pLocalSystem, m_pLocalSystem->CameraNum );
   pCamera->CameraName = camera_name;

   CVideoChannelInfo& vcInfo  = pCamera->VC->GetVideoChannelInfo();
   vcInfo.m_nVideoChannelType = video_channel_type;

   // URL Info.
   // RTSP  : IP,Port,URL1,FPS,Port,URL2,FPS
   // Onvif : IP,Port,StreamNo1,FPS,Port,StreamNo2,FPS
   // sutil::GetSplitStringList(strURL, ',', vecSplit_URL);
   CIPCameraInfo& IPCamInfo_0  = pCamera->VC->m_IPCamInfoArr[0];
   IPCamInfo_0.m_nCompanyID    = company_id;
   IPCamInfo_0.m_nIPCameraType = ip_camera_type;
   IPCamInfo_0.m_strIPAddress  = ip_address;
   IPCamInfo_0.m_nPortNo       = port_num;
   IPCamInfo_0.m_strURL        = url;
   IPCamInfo_0.m_fFPS_Cap      = fps;
   IPCamInfo_0.m_strUserName   = user_name;
   IPCamInfo_0.m_strPassword   = password;

   int erCode = m_pLocalSystem->AddCamera( pCamera );
   if( erCode ) {
      SAFE_DELETE( pCamera )
      LOGE << "failed to add camera. code:" << erCode;
      return erCode;
   }

   m_pLocalSystem->SetSystemSetupToBeSaved();

   strReply = sutil::sformat( "%s,%d,%d", pCamera->CameraGuid.c_str(), pCamera->UID, pCamera->ChannelNo );

   return erCode;
}

int Websocket::ModifyCamera( std::string& camID, std::string& strReply )
{
   CCamera* pCamera = GetCamera( camID );
   if( pCamera == nullptr ) {
      strReply = "#error: failed to RemoveCamera.";
      return ( 1 );
   }

   m_pLocalSystem->InitializeWizardSetup( pCamera->ChannelNo );
   CLocalSystem* pSystemTemp = m_pLocalSystem->GetSystemTemp();
   if( !pSystemTemp ) {
      m_pLocalSystem->FinalizeWizardSetup( FALSE );
      strReply = "#error: failed to RemoveCamera.";
      return ( 2 );
   }

   CCamera* pCameraTemp = pSystemTemp->GetCameraFromGuid( pCamera->CameraGuid );
   if( !pCameraTemp ) {
      m_pLocalSystem->FinalizeWizardSetup( FALSE );
      strReply = "#error: failed to RemoveCamera.";
      return ( 3 );
   }

   // URL Info.
   // RTSP  : IP,Port,URL1,FPS,Port,URL2,FPS
   // Onvif : IP,Port,StreamNo1,FPS,Port,StreamNo2,FPS
   // sutil::GetSplitStringList(strURL, ',', vecSplit_URL);

   CompanyID company_id        = CompanyID_RTSP;
   IPCameraType ip_camera_type = IPCameraType_RTSP;
   std::string ip_address      = "192.168.0.24";
   int port_num                = 554;
   std::string url             = "rtsp://192.168.0.24/profile8/media.smp";
   float fps                   = 7;
   std::string user_name       = "admin";
   std::string password        = "pass0001!";

   CIPCameraInfo& IPCamInfo_0  = pCameraTemp->VC->m_IPCamInfoArr[0];
   IPCamInfo_0.m_strUserName   = user_name;
   IPCamInfo_0.m_strPassword   = password;
   IPCamInfo_0.m_nCompanyID    = company_id;
   IPCamInfo_0.m_nIPCameraType = ip_camera_type;
   IPCamInfo_0.m_strIPAddress  = ip_address;
   IPCamInfo_0.m_nPortNo       = port_num;
   IPCamInfo_0.m_strURL        = url;
   IPCamInfo_0.m_fFPS_Cap      = fps;
   IPCamInfo_0.m_strUserName   = user_name;
   IPCamInfo_0.m_strPassword   = password;

   CVideoChannelInfo& vcInfo = pCameraTemp->VC->GetVideoChannelInfo();
   (void)vcInfo; // unused.

   CIPCameraInfo& IPCamInfo_1 = pCameraTemp->VC->m_IPCamInfoArr[1];
   // not yet.
   (void)IPCamInfo_1;

   m_pLocalSystem->FinalizeWizardSetup( TRUE );
   m_pLocalSystem->SetSystemSetupToBeSaved();

   strReply = "done.";

   return ( DONE );
}

int Websocket::GetCameraList( std::string& reply_message )
{
   CCriticalSectionSP co( m_pLocalSystem->csCameraList );
   std::deque<CCamera*>& Cameras = m_pLocalSystem->Cameras;

   for( size_t i = 0; i < Cameras.size(); i++ ) {
      CCamera* pCamera = Cameras[i];
      if( !pCamera )
         continue;

      reply_message += GetCameraID( pCamera ) + "\n";
   }

   if( reply_message.empty() ) {
      reply_message = "empty";
   } else {
      reply_message.pop_back(); // last "\n"
   }

   return DONE;
}

int Websocket::RemoveCamera( std::string camID, std::string& replyMsg )
{
   CCamera* pCamera = GetCamera( camID );
   if( !pCamera ) {
      replyMsg = "#error: cannot find camera.";
      return 1;
   }
   int iRet = m_pLocalSystem->RemoveCamera( pCamera );
   if( iRet ) {
      replyMsg = "#error: failed to RemoveCamera.";
      return 2;
   }
   m_pLocalSystem->SetSystemSetupToBeSaved();
   replyMsg = "done.";
   return DONE;
}

///
/// === Directory Service. From Examples. ===
///
// Example 1: echo WebSocket endpoint
void Websocket::ServiceExamEcho()
{
   auto& echo      = m_ws_server.endpoint["^/echo/?$"];
   echo.on_message = [this]( shared_ptr<WsServer::Connection> connection, shared_ptr<WsServer::Message> message ) {
      auto message_str = message->string();
      LOGV << "Server: Message received: \"" << message_str << "\" from " << WHOIS;
      LOGV << "Server: Sending message \"" << message_str << "\" to " << WHOIS;
      auto send_stream = make_shared<WsServer::SendStream>();
      *send_stream << message_str;
      // connection->send is an asynchronous function
      connection->send( send_stream, []( const SimpleWeb::error_code& ec ) {
         if( ec ) {
            LOGE << "Server: Error sending message. " <<
                // See http://www.boost.org/doc/libs/1_55_0/doc/html/boost_asio/reference.html, Error Codes for error code meanings
                "Error: " << ec << ", error message: " << ec.message();
         }
      } );
   };
   echo.on_open = [this]( shared_ptr<WsServer::Connection> connection ) {
      LOGD << "Server: Opened connection " << WHOIS;
   };
   echo.on_close = [this]( shared_ptr<WsServer::Connection> connection, int status, const string& /*reason*/ ) {
      LOGW << "Server: Closed connection " << WHOIS << " with status code " << status;
   };
   echo.on_error = [this]( shared_ptr<WsServer::Connection> connection, const SimpleWeb::error_code& ec ) {
      LOGE << "Server: Error in connection " << WHOIS << ". "
           << "Error: " << ec << ", error message: " << ec.message();
   };
}

// Example 2: Echo thrice
void Websocket::ServiceExamEchoThrice()
{
   auto& echo_thrice      = m_ws_server.endpoint["^/echo_thrice/?$"];
   echo_thrice.on_message = [this]( shared_ptr<WsServer::Connection> connection, shared_ptr<WsServer::Message> message ) {
      auto send_stream = make_shared<WsServer::SendStream>();
      *send_stream << message->string();
      connection->send( send_stream, [connection, send_stream]( const SimpleWeb::error_code& ec ) {
         if( !ec )
            connection->send( send_stream ); // Sent after the first send operation is finished
      } );
      connection->send( send_stream ); // Most likely queued. Sent after the first send operation is finished.
   };
}

// Example 3: Echo to all WebSocket endpoints
void Websocket::ServiceExamBroadCast()
{
   auto& echo_all      = m_ws_server.endpoint["^/echo_all/?$"];
   echo_all.on_message = [this]( shared_ptr<WsServer::Connection> /*connection*/, shared_ptr<WsServer::Message> message ) {
      shared_ptr<WsServer::SendStream> send_stream = make_shared<WsServer::SendStream>();
      *send_stream << message->string();
      // echo_all.get_connections() can also be used to solely receive connections on this endpoint
      // same as broadcast function.
      for( auto& a_connection : m_ws_server.get_connections() )
         a_connection->send( send_stream );
   };
}

#endif // __LIB_SIMPLEWEBSOCKET

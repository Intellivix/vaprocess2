#ifdef __WIN32
#include "StdAfx.h"
#include "AuxVideoDisplayWnd.h"

#include "SinglePTZTrack.h"
#include "Camera.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


CAuxVideoDisplayWnd::CAuxVideoDisplayWnd (int x,int y,CCamera* camera)
{
   Camera = camera;
   CString title;
   title.Format ("[Cellinx]Foreground Detection Result");
   m_bDoNotUpdateAuxVideoDispWndPos = TRUE;
   // TODO(yhpark): Temporary only for just building. 
   //CImageViewWnd::Create (title,CRect(x,y,100,100),&g_AuxVideoDisplayWnd);
   m_bDoNotUpdateAuxVideoDispWndPos = FALSE;
}

void CAuxVideoDisplayWnd::OnClose (   )
{
   CCamera &camera = *Camera;
   camera.CS_AuxVideoDisplay.lock (   );
   CRect rectThisWindow;
   GetWindowRect (rectThisWindow);
   camera.AuxVideoDispWndPos (rectThisWindow.left, rectThisWindow.top);
   if (camera.m_pSPTZTracker) 
      camera.m_pSPTZTracker->SetImageView (NULL, NULL);
   camera.AuxVideoDisplayWnd = NULL;
   CImageViewWnd::OnClose (   );

   camera.CS_AuxVideoDisplay.unlock (   );
}

void CAuxVideoDisplayWnd::OnMove(int x, int y)
{
   CImageViewWnd::OnMove (x, y);

   if (FALSE == m_bDoNotUpdateAuxVideoDispWndPos) {
      CCamera &camera = *Camera;
      camera.CS_AuxVideoDisplay.lock (   );
      CRect rectThisWindow;
      GetWindowRect (rectThisWindow);
      camera.AuxVideoDispWndPos (rectThisWindow.left, rectThisWindow.top);
      camera.CS_AuxVideoDisplay.unlock (   );
   }
}

void CAuxVideoDisplayWnd::SetVideoSize (int width,int height)
{
   ImageView->SetCanvasSize (width,height);
   CRect w_rect,c_rect,d_rect;
   GetWindowRect  (&w_rect);
   GetClientRect  (&c_rect);
   ClientToScreen (&c_rect);
   d_rect.left   = w_rect.left   - c_rect.left;
   d_rect.top    = w_rect.top    - c_rect.top;
   d_rect.right  = w_rect.right  - c_rect.right;
   d_rect.bottom = w_rect.bottom - c_rect.bottom;
   c_rect.right  = c_rect.left   + width;
   c_rect.bottom = c_rect.top    + height;
   w_rect.left   = c_rect.left   + d_rect.left;
   w_rect.top    = c_rect.top    + d_rect.top;
   w_rect.right  = c_rect.right  + d_rect.right;
   w_rect.bottom = c_rect.bottom + d_rect.bottom;
   MoveWindow (&w_rect);
}
#endif // __WIN32

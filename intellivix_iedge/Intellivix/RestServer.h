﻿#ifndef RESTSERVER_H
#define RESTSERVER_H

#include "RestDefine.h"
#include <cpprest/http_msg.h>
#include <memory>
#include <vector>
#include <map>

//////////////////////////////////////////////////////////////////////////
// class CRestHandler
//////////////////////////////////////////////////////////////////////////

class CRestHandler
{
public:
   enum class Method
   {
      Get = 0,
      Post,
      Put,
      Delete,
   };

   struct ParamRequest
   {
      Method                       method;
      std::vector<string_t>        relative_paths;
      std::map<string_t, string_t> query;
      json_value                   data = json_value::object(true);
   };

   struct ParamResponse
   {
      web::http::status_code       code = web::http::status_codes::OK;
      json_value                   data = json_value::object(true);
   };

public:
   explicit CRestHandler()   {   }
   virtual ~CRestHandler()   {   }

   virtual void OnRecv(const ParamRequest& request, ParamResponse& response) = 0;
};

//////////////////////////////////////////////////////////////////////////
// class CRestServer
//////////////////////////////////////////////////////////////////////////

class CRestServer
{
public:
   struct ParamActivate
   {
      std::string    url;
      CRestHandler*  handler = nullptr;
   };

   enum class RetVal
   {
      Success = 0,
      Activated,
      Deactivated,
      RestOpenFailed,
      RestCloseFailed,
   };

private:
   class CRestServerImpl;
   std::shared_ptr<CRestServerImpl>       m_pImpl;

public:
   explicit CRestServer();
   ~CRestServer();

   RetVal Activate(const ParamActivate& param);
   RetVal Deactivate();
};

#endif // RESTSERVER_H

﻿#pragma once

#if defined( __WIN32 )
#pragma warning( disable : 4819 )
#pragma warning( disable : 4099 )
#pragma warning( disable : 4204 )

#define _CRT_SECURE_NO_WARNINGS

#define _AFX_NO_MFC_CONTROLS_IN_DIALOGS // NOTE(yhpark): [MS2015] Use MFC : Use MFC in a Static Library

#ifndef _USING_V110_SDK71_
#define _USING_V110_SDK71_ // jun : v140_xp 옵션으로 빌드시 release 모드에서 컴파일 에러 발생 문제에 대한 예외처리임
#endif
#endif // __WIN32

#include "bccl.h"
#include "VACEx.h"
#include "Common.h"
#include "VCM.h"
#include "CommonDef.h"
#include "SupportFunction.h"

#ifndef SAFE_DELETE
#define SAFE_DELETE( x ) \
   {                     \
      if( x ) delete x;  \
      x = NULL;          \
   }
#endif

#ifndef SAFE_DELETE_ARR
#define SAFE_DELETE_ARR( x ) \
   {                         \
      if( x ) delete[] x;    \
      x = NULL;              \
   }
#endif

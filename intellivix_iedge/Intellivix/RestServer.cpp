﻿#include "RestServer.h"
#include "RestUtil.h"
#include <cpprest/http_listener.h>

using namespace web::http;
using namespace web_rest_util;
using namespace utility::conversions;

using web::http::experimental::listener::http_listener;

//////////////////////////////////////////////////////////////////////////
// class CRestServer
//////////////////////////////////////////////////////////////////////////

class CRestServer::CRestServerImpl {
private:
   std::shared_ptr<http_listener> m_pListener;
   CRestHandler* m_pHandler = nullptr;

public:
   explicit CRestServerImpl( const string_t& url )
   {
#if 0
      uri_builder builer;
      builer.set_scheme(U("http"));
      builer.set_host(U("*"));
      builer.set_port(10023);
      builer.set_path(U("api"));

      uri u(builer.to_uri());
      m_pListener = std::make_shared<http_listener>(u);
#else
      m_pListener = std::make_shared<http_listener>( url );
#endif
      m_pListener->support( methods::GET, bind( &CRestServerImpl::handle_get, this, std::placeholders::_1 ) );
      m_pListener->support( methods::PUT, bind( &CRestServerImpl::handle_put, this, std::placeholders::_1 ) );
      m_pListener->support( methods::POST, bind( &CRestServerImpl::handle_post, this, std::placeholders::_1 ) );
      m_pListener->support( methods::DEL, bind( &CRestServerImpl::handle_delete, this, std::placeholders::_1 ) );
   }

   ~CRestServerImpl()
   {
   }

   pplx::task<void> open( CRestHandler* pHandler )
   {
      m_pHandler = pHandler;
      return m_pListener->open();
   }

   pplx::task<void> close()
   {
      return m_pListener->close();
   }

   void PaserRequest(const http_request& message, CRestHandler::ParamRequest& request)
   {
      string_t relative_path = web::uri::decode(message.relative_uri().path());
      request.relative_paths = web::uri::split_path(relative_path);
      request.data           = message.extract_json().get();

      if (message.request_uri().query().length())
         request.query = uri::split_query(uri::decode(message.request_uri().query()));
   }

   void handle_get(http_request message)
   {
      try
      {
         CRestHandler::ParamRequest  request;
         CRestHandler::ParamResponse response;

         request.method = CRestHandler::Method::Get;
         PaserRequest(message, request);

         if (m_pHandler)
            m_pHandler->OnRecv(request, response);

         if (response.data.is_null())
         {
            message.reply(response.code);
            return;
         }

         message.reply(response.code, response.data);
      }
      catch (const http_exception& exption)
      {
         LOGE << "Rest handler method[Get] Failed : [" << exption.what() << "]";
      }
   }

   void handle_put(http_request message)
   {
      try
      {
         CRestHandler::ParamRequest  request;
         CRestHandler::ParamResponse response;

         request.method = CRestHandler::Method::Put;
         PaserRequest(message, request);

         if (m_pHandler)
            m_pHandler->OnRecv(request, response);

         if (response.data.is_null())
         {
            message.reply(response.code);
            return;
         }

         message.reply(response.code, response.data);
      }
      catch (const http_exception& exption)
      {
         LOGE << "Rest handler method[Put] Failed : [" << exption.what() << "]";
      }
   }

   void handle_post(http_request message)
   {
      try
      {
         CRestHandler::ParamRequest  request;
         CRestHandler::ParamResponse response;

         request.method = CRestHandler::Method::Post;
         PaserRequest(message, request);

         if (m_pHandler)
            m_pHandler->OnRecv(request, response);

         if (response.data.is_null())
         {
            message.reply(response.code);
            return;
         }

         message.reply(response.code, response.data);
      }
      catch (const http_exception& exption)
      {
         LOGE << "Rest handler method[Post] Failed : [" << exption.what() << "]";
      }
   }

   void handle_delete(http_request message)
   {
      try
      {
         CRestHandler::ParamRequest  request;
         CRestHandler::ParamResponse response;

         request.method = CRestHandler::Method::Delete;
         PaserRequest(message, request);

         if (m_pHandler)
            m_pHandler->OnRecv(request, response);

         if (response.data.is_null())
         {
            message.reply(response.code);
            return;
         }

         message.reply(response.code, response.data);
      }
      catch (const http_exception& exption)
      {
         LOGE << "Rest handler method[Delete] Failed : [" << exption.what() << "]";
      }
   }
};

//////////////////////////////////////////////////////////////////////////
// class CRestServer
//////////////////////////////////////////////////////////////////////////

CRestServer::CRestServer()
{
}

CRestServer::~CRestServer()
{
}

CRestServer::RetVal CRestServer::Activate(const ParamActivate& param)
{
   RetVal ret = RetVal::Success;

   if (m_pImpl)
      return RetVal::Activated;

   try
   {      
      m_pImpl = std::make_shared<CRestServerImpl>(to_string_t(param.url));
      m_pImpl->open(param.handler).wait();
   }
   catch (const http_exception& exption)
   {
      ret = RetVal::RestOpenFailed;
      LOGE << "Rest Server Activate Failed : [" << exption.what() << "]";
   }

   return RetVal::Success;
}

CRestServer::RetVal CRestServer::Deactivate()
{
   RetVal ret = RetVal::Success;

   if (false == (bool)m_pImpl)
      return RetVal::Deactivated;

   try
   {
      m_pImpl->close().wait();
      m_pImpl.reset();
   }
   catch (const http_exception& exption)
   {
      ret = RetVal::RestCloseFailed;
      LOGE << "Rest Server Deactivate Failed : [" << exption.what() << "]";
   }

   return ret;
}

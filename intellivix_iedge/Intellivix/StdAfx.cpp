#include "StdAfx.h"

#if defined( __WIN32 )

// pugixml
#ifdef _DEBUG
#pragma comment( lib, "pugixml_DS.lib" )
#else
#pragma comment( lib, "pugixml_RS.lib" )
#endif


#if defined( __IPP_50 )
#pragma comment( lib, "ippacemerged.lib" )
#pragma comment( lib, "ippacmerged.lib" )
#pragma comment( lib, "ippalign.lib" )
#pragma comment( lib, "ippccemerged.lib" )
#pragma comment( lib, "ippccmerged.lib" )
#pragma comment( lib, "ippchemerged.lib" )
#pragma comment( lib, "ippchmerged.lib" )
#pragma comment( lib, "ippcorel.lib" )
#pragma comment( lib, "ippcvemerged.lib" )
#pragma comment( lib, "ippcvmerged.lib" )
#pragma comment( lib, "ippdcemerged.lib" )
#pragma comment( lib, "ippdcmerged.lib" )
#pragma comment( lib, "ippiemerged.lib" )
#pragma comment( lib, "ippimerged.lib" )
#pragma comment( lib, "ippjemerged.lib" )
#pragma comment( lib, "ippjmerged.lib" )
#pragma comment( lib, "ippmemerged.lib" )
#pragma comment( lib, "ippmmerged.lib" )
#pragma comment( lib, "ippscemerged.lib" )
#pragma comment( lib, "ippscmerged.lib" )
#pragma comment( lib, "ippsemerged.lib" )
#pragma comment( lib, "ippsmerged.lib" )
#pragma comment( lib, "ippsremerged.lib" )
#pragma comment( lib, "ippsrmerged.lib" )
#pragma comment( lib, "ippvcemerged.lib" )
#pragma comment( lib, "ippvcmerged.lib" )
#pragma comment( lib, "ippvmemerged.lib" )
#pragma comment( lib, "ippvmmerged.lib" )
#elif defined( __IPP_80 )
#pragma comment( lib, "ippcoremt.lib" )
#pragma comment( lib, "ippsmt.lib" )
#pragma comment( lib, "ippimt.lib" )
#pragma comment( lib, "ippcvmt.lib" )
#endif

#ifdef _DEBUG
#pragma comment( lib, "ZLib_DS.lib" )
#pragma comment( lib, "BCCL_DS.lib" )
#pragma comment( lib, "VACL_DS.lib" )
#pragma comment( lib, "COMMON_DS.lib" )
#pragma comment( lib, "VCM_DS.lib" )
#pragma comment( lib, "IVCP30_DS.lib" )
#pragma comment( lib, "Live555_DS.lib" )

#if defined( __LIB_OPENCV )
#pragma comment( lib, "libjasperd.lib" )
#pragma comment( lib, "libjpegd.lib" )
#pragma comment( lib, "libpngd.lib" )
#pragma comment( lib, "libtiffd.lib" )
#pragma comment( lib, "zlibd.lib" )
#pragma comment( lib, "IlmImfd.lib" )
#pragma comment( lib, "opencv_highgui2412d.lib" )
#endif // __LIB_OPENCV

#pragma comment( lib, "ZLib_RS.lib" )
#pragma comment( lib, "BCCL_RS.lib" )
#pragma comment( lib, "VACL_RS.lib" )
#pragma comment( lib, "COMMON_RS.lib" )
#pragma comment( lib, "VCM_RS.lib" )
#pragma comment( lib, "IVCP30_RS.lib" )
#pragma comment( lib, "Live555_RS.lib" )

#if defined( __LIB_OPENCV )
#pragma comment( lib, "libjasper.lib" )
#pragma comment( lib, "libjpeg.lib" )
#pragma comment( lib, "libpng.lib" )
#pragma comment( lib, "libtiff.lib" )
#pragma comment( lib, "zlib.lib" )
#pragma comment( lib, "IlmImf.lib" )
#pragma comment( lib, "opencv_highgui2412.lib" )

#endif // __LIB_OPENCV

#endif // Not _DEBUG

#pragma comment( lib, "avcodec.lib" )
#pragma comment( lib, "avformat.lib" )
#pragma comment( lib, "avutil.lib" )
#pragma comment( lib, "swscale.lib" )
#pragma comment( lib, "Rpcrt4.lib" ) // CreateGUID 함수 링크 시 사용

#endif // __WIN32

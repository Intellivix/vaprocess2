﻿#pragma once

#include "vacl_tgtrack.h"
#include "PTZCtrl.h"
#include "PTZVector.h"

#ifdef __WIN32
#include "Win32CL/ImageView.h"
#endif

using namespace VACL;

class ISinglePTZTracker;

////////////////////////////////////////////////////////////////////////
//
// Enumerations
//
////////////////////////////////////////////////////////////////////////

enum SPTZTrk_PTCtrlMode {
   SPTZTrk_PTCtrlMode_ContMove   = 0,
   SPTZTrk_PTCtrlMode_ABSPosMove = 1
};

enum SPTZTrk_ZoomCtrlMove {
   SPTZTrk_ZoomCtrlMove_NotMoving  = 0,
   SPTZTrk_ZoomCtrlMove_Magnifying = 1
};

// Single PTZ Track Status
enum SPTZTrkState {
   SPTZTrkState_TrackBegun           = 0x00000001,
   SPTZTrkState_TrackOngoing         = 0x00000002,
   SPTZTrkState_TrackToBeEnded       = 0x00000004,
   SPTZTrkState_ZoomToBeStarted      = 0x00000010,
   SPTZTrkState_ZoomBegun            = 0x00000020,
   SPTZTrkState_ZoomOngoing          = 0x00000040,
   SPTZTrkState_TrackObjLost         = 0x00000080,
   SPTZTrkState_TrackObjIntantlyLost = 0x00000100,
   SPTZTrkState_MoveTrkObjToCenter   = 0x00000400,
   SPTZTrkState_BKModelToBeUpdate    = 0x00000800,
};

////////////////////////////////////////////////////////////////////////
//
// class GlobalSinglePTZTrackSetup
//
////////////////////////////////////////////////////////////////////////

class GlobalSinglePTZTrackSetup {
public:
   float m_fContPTZTrkSpeedRatio;
   uint m_nZoomBeginWaitTime;
   float m_fPTZTrackObjectStoppingCheckTime;
   float m_fMinSimiarityScore;
   float m_fZoomOutLogValueWhenLostObj;
   float m_fMinTiltAngle;
   float m_fMaxTiltAngle;

public:
   GlobalSinglePTZTrackSetup();

public:
   int Read( CXMLIO* pIO );
   int Write( CXMLIO* pIO );
};

extern GlobalSinglePTZTrackSetup g_SPTZTrkSetup;

////////////////////////////////////////////////////////////////////////
//
// class SinglePTZTrackSetup
//
////////////////////////////////////////////////////////////////////////

class SinglePTZTrackSetup {
public:
   int SPTZTrkType;
   int Options;
   int PanTiltCtrlMode;
   int ZoomCtrlMode;
   float MaxTrackingTime;
   float ObjectMagnifyingRatio;
   FPoint2D ViewingPos;

public:
   SinglePTZTrackSetup();
   BOOL operator!=( SinglePTZTrackSetup& Setup );

public:
   int ReadFile( CXMLIO* pIO );
   int WriteFile( CXMLIO* pIO );
   BOOL IsTheSame( SinglePTZTrackSetup* pFrom );
};

class CSinglePTZTrack;

////////////////////////////////////////////////////////////////////////
//
// class ContPTZTrkObj
//
////////////////////////////////////////////////////////////////////////

class ContPTZTrkObj {
public:
   float m_fFrameRate;
   IBox2D m_b2TrkBox;
   IBox2D m_b2PrevTrkBox;
   FPoint2D m_p2TrkPos;
   FPoint2D m_p2PrevTrkPos;
   FPoint2D m_ObjVelocity;
   FPoint2D m_AvgObjVelocity;
   FPoint2D m_p2CurPTZVelocity;
   FPoint2D m_p2ImgCenterToObjCenter;
   FPTZVector m_CurPanTiltAngleVelocity;

   TargetTracking_NCC TargetTracker;

public:
   CPTZCtrl* m_pPTZCtrl;
   CSinglePTZTrack* m_pTracker;

public:
   ContPTZTrkObj();

public:
   void Initialize( IBox2D b2TrkBox, GImage& srcImage, CSinglePTZTrack* pTracker );
   void Close();
   float Perform( GImage& srcImage );
   FPoint2D CalcPTZVelocity();
   void GetPanTiltAngleVelocity( float& fPanAngleVel, float& fTiltAngleVel );
   FPoint2D GetGammmaCorrectionVelocity( FPoint2D p2InputVelocity, float fGammaValue );
};

////////////////////////////////////////////////////////////////////////
//
// class CSinglePTZTrack
//
////////////////////////////////////////////////////////////////////////

class CSinglePTZTrack {
   friend class ContPTZTrkObj;

public:
   SinglePTZTrackSetup m_Setup;
   CPTZCtrl* m_pPTZCtrl;
#if defined( __WIN32 )
public:
   Win32CL::CImageView* m_pImageView;
   std::recursive_mutex* m_pcsImageView;
#endif

protected:
   uint m_nSPTZTrk_PTCtrlMode;
   uint m_nState;
   uint m_nPrevState;
   uint32_t m_nPrevTime;
   uint32_t m_nCurrTime;
   uint32_t m_nTrackObjIntantlyLostTime;
   uint32_t m_nTrackStartTime;
   uint32_t m_nFindingTrackObjectStartTime;
   uint32_t m_nZoomStartTime;

   float m_fFrameRate;
   ISize2D m_s2OrgSrcImgSize;
   ISize2D m_s2SrcImgSize;
   FPoint2D m_p2CenterPos;
   FPoint2D m_InitObjVelocity;
   FPoint2D m_AvgObjVelocity;
   FPoint2D m_AvgObjVelocity_ShortTerm;
   float m_fAvgMachingScoreVelocity = 0.0f;
   float m_fPrevMachingScore        = MC_VSV;
   FPTZVector m_PrevPTZPos;
   FPTZVector m_CurrPTZPos;

protected:
   int m_nChannelNo;

   ObjectTracking* m_pObjectTracker;
   CCriticalSection* m_pcsObjectTracker;
   TrackedObjectEx* m_pCurrTrkObj;
   EventZoneEx m_EventZoneOfSPTZTrkObj;
   EventRuleEx m_EventRuleOfSPTZTrkObj;

   ContPTZTrkObj m_ContPTZTrkObj;
   CPerformanceCounter m_PCounter;

public:
   int m_nZoomMoveCount;
   BOOL m_bSPTZTrkInitialized;
   BOOL m_bSPTZTrkLostVideoAnalyics;

public:
   float m_fZoomCur;
   float m_fZoomLogOffset;
   float m_fTgtZoom;
   float m_fInitZoom;
   float m_fInitZoomBegun;

   FPTZVector m_pvPTVel;

protected:
   float m_fContPanMoveSpeedRatio;
   float m_fContTiltMoveSpeedRatio;
   float m_fRealObjSpeedFactor;
   float m_fMaxZoomLogOffsetForConvergence;
   float m_fZoomLogOffsetForSlowZoomSpeed;
   float m_fContZoomSpeed_Fast;
   float m_fContZoomSpeed_Slow;

public:
   CSinglePTZTrack();
   virtual ~CSinglePTZTrack();

public:
   void Create( CPTZCtrl* pPTZCtrl );
   void Close();

public:
   int ReadFile( CXMLIO* pIO );
   int WriteFile( CXMLIO* pIO );

public:
   void Initialize( int nChNo, GImage& sg_image, ObjectTracking& objectTracker, CCriticalSection& csObjectTracker, TrackedObjectEx* pObj, float fFrameRate );
   int Uninitialize();
   void Perform( BGRImage& cimgSrc, GImage& gimgSrc );
   IBox2D GetTrkBox();
   BOOL IsDrawTrkObj();
   uint GetState()
   {
      return m_nState;
   }
   void ModifyState( uint nRemove, uint nAdd );
   BOOL CheckStateChange( uint nPrevState, uint nCurState, uint nFlag );

protected:
   void InvalidateAllObjectAndEvent();

public:
   FPoint2D GetTrkPos();

#if defined( __WIN32 )
public:
   void SetImageView( Win32CL::CImageView* pImageView, std::recursive_mutex* pcsImageView );
#endif
protected:
   void InitPTZCamParams();
   void AddContPTZTrkLogItem();
   void DetermineZoomTracking();
   float GetTargetZoom();
   float GetZoomSpeed();
   void TargetLostCheckProc();
   //public:
   void DrawTrackState( BGRImage& cimgSrc, GImage& gimgSrc );
};

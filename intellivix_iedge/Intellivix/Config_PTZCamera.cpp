﻿#include "StdAfx.h"
#include "Config_PTZCamera.h"

///////////////////////////////////////////////////////////////////////////////
//
// Class: CConfig_PTZCamera
//
///////////////////////////////////////////////////////////////////////////////

CConfig_PTZCamera::CConfig_PTZCamera()
{
   Priority     = 1;
   ZoomFactor   = 4.0f;
   PanTiltSpeed = 32;
   PanTiltSpeed = 7;
   ZoomSpeed    = 7;
   FocusSpeed   = 1;
   ViewingPos( 0.5f, 0.5f );
   PanRotationType          = PanRotationType_ClockWise;
   TiltRotationType         = TiltRotationType_Bottom90;
   DueNorthPoleOffsetAngle  = 0.0f;
   PanAngleDisplayRangeType = PanAngleDisplayRangeType_0_P360;

   // 메인 홈위치 설정 관련
   Flag_UseMainHomePos = FALSE;
   HomePTZPos( 0.0f, 0.0f, 1.0f, 0.0f, 0.0f );
   HomePresetNo        = 127;
   WaitingTime_HomePos = 10.0f;

   // PTZ 위치 초기화
   m_bUsePTZPosInitSchedule    = FALSE;
   m_nPTZPosInitSchedulingMode = PTZPosInitScheduling_AtSpecifiedTime;
   m_nDayInterval              = 1;
   m_tmPosInitTimeFrom         = CTime( 2012, 6, 11, 1, 0, 0 );
   m_tmPosInitTimeTo           = CTime( 2012, 6, 11, 2, 0, 0 );
   UpdatePosInitRandomTimeSpan();
   m_bRandomTimeOnSpecifiedPeriod = TRUE;
   m_nInitPeriod_Time             = 1;
   m_nInitPeriod_Minute           = 0;
   m_bDoesNotUpdateVideoAtPosinit = FALSE;
   m_bRestartCameraAfterPosInit   = FALSE;
}

CConfig_PTZCamera& CConfig_PTZCamera::operator=( CConfig_PTZCamera& from )
{
   FileIO buffer;
   CXMLIO xmlIOWrite( &buffer, XMLIO_Write );
   from.WriteFile( &xmlIOWrite );
   buffer.GoToStartPos();
   CXMLIO xmlIORead( &buffer, XMLIO_Read );
   ReadFile( &xmlIORead );

   return *this;
}

void CConfig_PTZCamera::GetPTZTrackingParams( EventZoneEx* evt_zone )
{
   evt_zone->Priority   = Priority;
   evt_zone->ViewingPos = ViewingPos;
   evt_zone->ZoomFactor = ZoomFactor;
}

void CConfig_PTZCamera::SetPTZTrackingParams( EventZoneEx* evt_zone )
{
   Priority   = evt_zone->Priority;
   ViewingPos = evt_zone->ViewingPos;
   ZoomFactor = evt_zone->ZoomFactor;
}

void CConfig_PTZCamera::UpdatePosInitRandomTimeSpan()
{
   if( m_tmPosInitTimeTo >= m_tmPosInitTimeFrom ) {
      CTimeSpan tsPeriod = m_tmPosInitTimeTo - m_tmPosInitTimeFrom;
      srand( GetTickCount() );
      int nRandInt              = ( ushort( rand() ) << 15 ) + rand();
      int nTotalMinutes         = int( tsPeriod.GetTotalMinutes() + 1 );
      int nRnadMininute         = nRandInt % nTotalMinutes;
      m_tsPosInitRandomTimeSpan = CTimeSpan( 0, 0, nRnadMininute, 0 );
   }
}

BOOL CConfig_PTZCamera::ReadFile( CXMLIO* pIO )
{
   static CConfig_PTZCamera dv; // default value
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PTZCamParam" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Priority", &Priority, &dv.Priority );
      xmlNode.Attribute( TYPE_ID( float ), "ZoomFactor", &ZoomFactor, &dv.ZoomFactor );
      xmlNode.Attribute( TYPE_ID( int ), "PanTiltSpeed", &PanTiltSpeed, &dv.PanTiltSpeed );
      xmlNode.Attribute( TYPE_ID( int ), "ZoomSpeed", &ZoomSpeed, &dv.ZoomSpeed );
      xmlNode.Attribute( TYPE_ID( int ), "FocusSpeed", &FocusSpeed, &dv.FocusSpeed );
      xmlNode.Attribute( TYPE_ID( FPoint2D ), "ViewingPos", &ViewingPos, &dv.ViewingPos );
      xmlNode.Attribute( TYPE_ID( int ), "PanRotationType", &PanRotationType, &dv.PanRotationType );
      xmlNode.Attribute( TYPE_ID( int ), "TiltRotationType", &TiltRotationType, &dv.TiltRotationType );
      xmlNode.Attribute( TYPE_ID( float ), "DueNorthPoleOA", &DueNorthPoleOffsetAngle, &dv.DueNorthPoleOffsetAngle );
      xmlNode.Attribute( TYPE_ID( int ), "PanAngleDispRangeT", &PanAngleDisplayRangeType, &dv.PanAngleDisplayRangeType );
      // 메인 홈 위치
      xmlNode.Attribute( TYPE_ID( BOOL ), "UseMainHomePos", &Flag_UseMainHomePos, &dv.Flag_UseMainHomePos );
      Flag_UseMainHomePos = FALSE;
      xmlNode.Attribute( TYPE_ID( float ), "Home_Pan", &HomePTZPos.P, &dv.HomePTZPos.P );
      xmlNode.Attribute( TYPE_ID( float ), "Home_Tilt", &HomePTZPos.T, &dv.HomePTZPos.T );
      xmlNode.Attribute( TYPE_ID( float ), "Home_Zoom", &HomePTZPos.Z, &dv.HomePTZPos.Z );
      xmlNode.Attribute( TYPE_ID( float ), "Home_Focus", &HomePTZPos.F, &dv.HomePTZPos.F );
      xmlNode.Attribute( TYPE_ID( float ), "Home_Iris", &HomePTZPos.I, &dv.HomePTZPos.I );
      xmlNode.Attribute( TYPE_ID( int ), "HomePresetNo", &HomePresetNo, &dv.HomePresetNo );
      xmlNode.Attribute( TYPE_ID( float ), "WaitTime_HP", &WaitingTime_HomePos, &dv.WaitingTime_HomePos );
      // PTZ 위치 초기화
      int nPosInitRandomTimeSpanInMin = 0;
      xmlNode.Attribute( TYPE_ID( BOOL ), "UsePTZPosInitSchedule", &m_bUsePTZPosInitSchedule, &dv.m_bUsePTZPosInitSchedule );
      xmlNode.Attribute( TYPE_ID( int ), "HomePresetNo", &m_nPTZPosInitSchedulingMode, &dv.m_nPTZPosInitSchedulingMode );
      xmlNode.Attribute( TYPE_ID( CTime ), "PrevPosInitTime", &m_tmPrevPosInitTime, &dv.m_tmPrevPosInitTime );
      xmlNode.Attribute( TYPE_ID( CTime ), "PosInitTimeFrom", &m_tmPosInitTimeFrom, &dv.m_tmPosInitTimeFrom );
      xmlNode.Attribute( TYPE_ID( CTime ), "PosInitTimeTo", &m_tmPosInitTimeTo, &dv.m_tmPosInitTimeTo );
      xmlNode.Attribute( TYPE_ID( int ), "PosInitRandomTimeSpanInMin", &nPosInitRandomTimeSpanInMin );
      xmlNode.Attribute( TYPE_ID( int ), "HomePresetNo", &m_nDayInterval, &dv.m_nDayInterval );
      xmlNode.Attribute( TYPE_ID( BOOL ), "RandomTimeOnSpecifiedPeriod", &m_bRandomTimeOnSpecifiedPeriod, &dv.m_bRandomTimeOnSpecifiedPeriod );
      xmlNode.Attribute( TYPE_ID( int ), "InitPeriod_Time", &m_nInitPeriod_Time, &dv.m_nInitPeriod_Time );
      xmlNode.Attribute( TYPE_ID( int ), "InitPeriod_Min", &m_nInitPeriod_Minute, &dv.m_nInitPeriod_Minute );
      xmlNode.Attribute( TYPE_ID( BOOL ), "DoesNotUpdateVideoAtPosinit", &m_bDoesNotUpdateVideoAtPosinit, &dv.m_bDoesNotUpdateVideoAtPosinit );
      xmlNode.Attribute( TYPE_ID( BOOL ), "RestartCameraAfterPosInit", &m_bRestartCameraAfterPosInit, &dv.m_bRestartCameraAfterPosInit );
      m_tsPosInitRandomTimeSpan = CTimeSpan( 0, 0, nPosInitRandomTimeSpanInMin, 0 );
      xmlNode.End();
      return TRUE;
   }
   return FALSE;
}

BOOL CConfig_PTZCamera::WriteFile( CXMLIO* pIO )
{
   static CConfig_PTZCamera dv; // default value
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PTZCamParam" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Priority", &Priority, &dv.Priority );
      xmlNode.Attribute( TYPE_ID( float ), "ZoomFactor", &ZoomFactor, &dv.ZoomFactor );
      xmlNode.Attribute( TYPE_ID( int ), "PanTiltSpeed", &PanTiltSpeed, &dv.PanTiltSpeed );
      xmlNode.Attribute( TYPE_ID( int ), "ZoomSpeed", &ZoomSpeed, &dv.ZoomSpeed );
      xmlNode.Attribute( TYPE_ID( int ), "FocusSpeed", &FocusSpeed, &dv.FocusSpeed );
      xmlNode.Attribute( TYPE_ID( FPoint2D ), "ViewingPos", &ViewingPos, &dv.ViewingPos );
      xmlNode.Attribute( TYPE_ID( int ), "PanRotationType", &PanRotationType, &dv.PanRotationType );
      xmlNode.Attribute( TYPE_ID( int ), "TiltRotationType", &TiltRotationType, &dv.TiltRotationType );
      xmlNode.Attribute( TYPE_ID( float ), "DueNorthPoleOA", &DueNorthPoleOffsetAngle, &dv.DueNorthPoleOffsetAngle );
      xmlNode.Attribute( TYPE_ID( int ), "PanAngleDispRangeT", &PanAngleDisplayRangeType, &dv.PanAngleDisplayRangeType );

      // 메인 홈 위치
      xmlNode.Attribute( TYPE_ID( BOOL ), "UseMainHomePos", &Flag_UseMainHomePos, &dv.Flag_UseMainHomePos );
      Flag_UseMainHomePos = FALSE;
      xmlNode.Attribute( TYPE_ID( float ), "Home_Pan", &HomePTZPos.P, &dv.HomePTZPos.P );
      xmlNode.Attribute( TYPE_ID( float ), "Home_Tilt", &HomePTZPos.T, &dv.HomePTZPos.T );
      xmlNode.Attribute( TYPE_ID( float ), "Home_Zoom", &HomePTZPos.Z, &dv.HomePTZPos.Z );
      xmlNode.Attribute( TYPE_ID( float ), "Home_Focus", &HomePTZPos.F, &dv.HomePTZPos.F );
      xmlNode.Attribute( TYPE_ID( float ), "Home_Iris", &HomePTZPos.I, &dv.HomePTZPos.I );
      xmlNode.Attribute( TYPE_ID( int ), "HomePresetNo", &HomePresetNo, &dv.HomePresetNo );
      xmlNode.Attribute( TYPE_ID( float ), "WaitTime_HP", &WaitingTime_HomePos, &dv.WaitingTime_HomePos );
      // PTZ 위치 초기화
      int nPosInitRandomTimeSpanInMin = int( m_tsPosInitRandomTimeSpan.GetTotalMinutes() );
      xmlNode.Attribute( TYPE_ID( BOOL ), "UsePTZPosInitSchedule", &m_bUsePTZPosInitSchedule, &dv.m_bUsePTZPosInitSchedule );
      xmlNode.Attribute( TYPE_ID( int ), "HomePresetNo", &m_nPTZPosInitSchedulingMode, &dv.m_nPTZPosInitSchedulingMode );
      xmlNode.Attribute( TYPE_ID( CTime ), "PrevPosInitTime", &m_tmPrevPosInitTime, &dv.m_tmPrevPosInitTime );
      xmlNode.Attribute( TYPE_ID( CTime ), "PosInitTimeFrom", &m_tmPosInitTimeFrom, &dv.m_tmPosInitTimeFrom );
      xmlNode.Attribute( TYPE_ID( CTime ), "PosInitTimeTo", &m_tmPosInitTimeTo, &dv.m_tmPosInitTimeTo );
      xmlNode.Attribute( TYPE_ID( int ), "PosInitRandomTimeSpanInMin", &nPosInitRandomTimeSpanInMin );
      xmlNode.Attribute( TYPE_ID( int ), "HomePresetNo", &m_nDayInterval, &dv.m_nDayInterval );
      xmlNode.Attribute( TYPE_ID( BOOL ), "RandomTimeOnSpecifiedPeriod", &m_bRandomTimeOnSpecifiedPeriod, &dv.m_bRandomTimeOnSpecifiedPeriod );
      xmlNode.Attribute( TYPE_ID( int ), "InitPeriod_Time", &m_nInitPeriod_Time, &dv.m_nInitPeriod_Time );
      xmlNode.Attribute( TYPE_ID( int ), "InitPeriod_Min", &m_nInitPeriod_Minute, &dv.m_nInitPeriod_Minute );
      xmlNode.Attribute( TYPE_ID( BOOL ), "DoesNotUpdateVideoAtPosinit", &m_bDoesNotUpdateVideoAtPosinit, &dv.m_bDoesNotUpdateVideoAtPosinit );
      xmlNode.Attribute( TYPE_ID( BOOL ), "RestartCameraAfterPosInit", &m_bRestartCameraAfterPosInit, &dv.m_bRestartCameraAfterPosInit );
      xmlNode.End();
      return TRUE;
   }
   return FALSE;
}

BOOL CConfig_PTZCamera::operator!=( CConfig_PTZCamera from )
{
   BOOL bRet = TRUE;
   if( Flag_UseMainHomePos == from.Flag_UseMainHomePos && HomePTZPos.P == from.HomePTZPos.P && HomePTZPos.T == from.HomePTZPos.T && HomePTZPos.Z == from.HomePTZPos.Z && HomePTZPos.F == from.HomePTZPos.F && HomePTZPos.I == from.HomePTZPos.I && HomePresetNo == from.HomePresetNo && WaitingTime_HomePos == from.WaitingTime_HomePos ) {
      bRet = FALSE;
   }

   return bRet;
}

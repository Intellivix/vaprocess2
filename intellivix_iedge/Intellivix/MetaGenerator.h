﻿#ifndef METAGENERATOR_H
#define METAGENERATOR_H

#include "VACEx.h"

//////////////////////////////////////////////////////////////////////////
// Define
//////////////////////////////////////////////////////////////////////////

enum class MetaGeneratorType {
   NONE = 0,
   XML,
   JSON,
};

//////////////////////////////////////////////////////////////////////////
// class CRanderObjectInImage
//////////////////////////////////////////////////////////////////////////

class CRanderObjectInImage {
private:
   BCCL::BGRImage m_img_bgr;
   ISize2D m_va_size;

public:
   struct PARAM {
      byte* yuy2_buffer = nullptr;
      int src_image_width;
      int src_image_height;
      int va_image_width;
      int va_image_height;
   };

private:
   void DrawObjectBoundingBox( TrackedObject* t_object );

public:
   CRanderObjectInImage();
   ~CRanderObjectInImage();

   bool PreparingSourceImage( const PARAM& param );
   bool RenderVaBoxes( std::deque<TrackedObjectEx*>& trackedObjectList );
   bool SaveJPGFile( const std::string& file_name );
};

//////////////////////////////////////////////////////////////////////////
// class CMetaGenerator
//////////////////////////////////////////////////////////////////////////

class CCamera;
class CLocalSystem;

class CMetaGenerator {
protected:
   CCamera* m_pCamera           = nullptr;
   CLocalSystem* m_pLocalSystem = nullptr;

public:
   struct VaMeta {
      int nTOLType            = -1;
      EventEx* event          = nullptr;
      TrackedObjectEx* object = nullptr;
   };
   typedef std::vector<VaMeta> VAResults;

   explicit CMetaGenerator();
   virtual ~CMetaGenerator();

public:
   virtual void InitGenerator() {}
   void SetLocalSystem( CLocalSystem* pLocalSystem );
   void SetCamera( CCamera* pCamera );

   virtual MetaGeneratorType GetType() const = 0;
   virtual std::string GetString() const     = 0;

   virtual void FillFrameInfo( std::deque<TrackedObjectEx*>& trackedObjectList );
   virtual void FillFrmObjInfo( std::deque<TrackedObjectEx*>& trackedObjectList );
   virtual void FillObjectInfo( TrackedObjectEx* t_object );
   virtual void FillEventInfo( VAResults& results );
   virtual void FillEventInfo( TrackedObjectEx* t_object, EventEx* pEvent );
   virtual void FillEventZoneInfo( BOOL bNullInfo = FALSE );
   virtual void FillEventZoneCounterInfo();
   virtual void FillLiveFps( int type );
   virtual void FillSystemStartup();
   virtual void FillSystemShutdown( int code );
};

//////////////////////////////////////////////////////////////////////////
// Factory pattern function
//////////////////////////////////////////////////////////////////////////

typedef std::shared_ptr<CMetaGenerator> MetaGeneratorSP;

MetaGeneratorSP GetGenerator( const MetaGeneratorType& type, CLocalSystem* system, CCamera* camera );
MetaGeneratorSP GetGenerator( const MetaGeneratorType& type, CCamera* camera );

#endif // METAGENERATOR_H

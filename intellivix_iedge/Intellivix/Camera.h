﻿#pragma once
#include <vacl_environ.h>
#include <vacl_foregnd.h>
#include <vacl_foregnd2.h>
#include <vacl_objclass.h>
#include <vacl_realsize.h>
#include <vacl_objtrack.h>
#include <vacl_objrecog.h>

using namespace VACL;

#include "VideoSnder.h"
#include "VACEx.h"
#include "Preset.h"
#include "Config_VideoStream.h"
#include "Config_PTZCamera.h"
#include "CommonDef.h"
#include "TimerCheckerManager.h"
#include "SinglePTZTrack.h"

#ifdef __WIN32
#include "AuxVideoDisplayWnd.h"
#endif // __WIN32

//#pragma pack (1)

struct VideoFrameInfo;
class CLocalSystem;
class CCamera;
class CIntelliVIXDoc;
class CIvcpRtspSender;
class CVideoChannel;
class CRTSPCustomServer;
class CRTSPStreamingChannel;
class CPTZStatusCallback;
class CPTZCtrlCallback;

///////////////////////////////////////////////////////////////////////////////
//
// Enumerations
//
///////////////////////////////////////////////////////////////////////////////

enum SPTZTrkMode {
   SPTZTrkMode_None,
   SPTZTrkMode_SpecifyTrkObj,
   SPTZTrkMode_SelectTrkObj,
   SPTZTrkMode_EvtTriggered,
};

enum CAMERA_DESCRIPTOR {
   CD_CAMERA_ID = 0,
   CD_SYSTEM_NAME,
   CD_CAMERA_NAME,
   CD_CHANNEL_NO,
   CD_CHANNEL_NO_BRF,
   CD_LIVE_ALARM_EVENT,
   CD_STATUS,
   CD_VIDEO_RESOLUTION,
   CD_CAMERA_TYPE,
   CD_RECORD_STATE,
   CD_PLAY_STATE,
   CD_DISPLAY_NAME,
   CD_DISPLAY_FULL_NAME,
   CD_FRAME_RATE_LIVE,
   CD_PTZ_TOURING_STATE,
   CD_BITRATE,
   CD_NUM,
};

enum FX_STAGE {
   FX_READY     = 0,
   FX_SEARCHING = 1,
};

enum PTZMoveDirection {
   PTZMoveDirection_Left      = 0,
   PTZMoveDirection_Right     = 1,
   PTZMoveDirection_Up        = 2,
   PTZMoveDirection_Down      = 3,
   PTZMoveDirection_LeftUp    = 4,
   PTZMoveDirection_LeftDown  = 5,
   PTZMoveDirection_RightUp   = 6,
   PTZMoveDirection_RightDown = 7,
   PTZMoveDirection_ZoomIn    = 8,
   PTZMoveDirection_ZoomOut   = 9,
   PTZMoveDirection_FocusNear = 10,
   PTZMoveDirection_FocusFar  = 11,
};

// 일반 옵션 (int Options)
enum CamOpt {
   CamOpt_SlaveCam                                  = 0x00000002,
   CamOpt_MasterCam                                 = 0x00000004,
   CamOpt_PresetTouring                             = 0x00000008,
   CamOpt_AutoPTZCtrlMode                           = 0x00000020,
   CamOpt_AutoPTZCtrlModeAutoOn                     = 0x00000040,
   CamOpt_MutualAssistanceTrk                       = 0x00000080,
   CamOpt_PTZMoveBySpecifyingRectArea               = 0x01000000,
   CamOpt_PausingVideoAnalytics                     = 0x02000000,
   CamOpt_PausingVideoAnalyticsToBeReleased         = 0x04000000,
   CamOpt_PausingVideoAnalyticsSchedule             = 0x08000000,
   CamOpt_PausingVideoAnalyticsScheduleToBeReleased = 0x10000000,
   CamOpt_LockPTZ                                   = 0x20000000,
   CamOpt_RestartPresetTouring                      = 0x40000000,
};

// DTC 관련 옵션
enum CamOpt2 {
   CamOpt_EnableVAC = 0x00000001,
};

// UI 관련 옵션
enum CamUIOpt {
   CamUIOpt_ItemExpand = 0x00000001,
};

// 카메라의 각종 상태를 나타내는 플래그들
// CamState는 클라이언트에 전달되는 카메라 상태 값이다.
// 클라이언트에서도 알아야 할 상태값을 정의해야 한다면 이곳에 추가해야한다.
enum CamState {
   CamState_Activate                      = 0x00000001,
   CamState_SuddenSceneChange             = 0x00000002, // 영상의 갑작스러운 장면 변화가 감지된 경우.
   CamState_ToBeClosed                    = 0x00000004, // 카메라을 닫는 경우.
   CamState_ToBeRestarted                 = 0x00000008,
   CamState_InitializationFailure         = 0x00000100,
   CamState_OpenSerialPortFailed          = 0x00000200, // 시리얼 포트를 열지 못함
   CamState_ChannelUnregistered           = 0x00000400, // 등록되지 않은 채널
   CamState_OpenPVTFileFailed             = 0x00000800, // PVT 파일을 열지 못함
   CamState_OpenVCTFileFailed             = 0x00001000, // VCT 파일을 열지 못함
   CamState_OpenVideoFileFailed           = 0x00002000,
   CamState_CreatingBKModel               = 0x00004000, // 배경 모델 생성상태임을 사용자에게 알리는 용임.
   CamState_DoVideoAnalytics              = 0x00008000, // 비디오 분석 상태.
   CamState_VideoAnalyticsOnPresetMoving  = 0x00010000, // Preset 이동 시 비디오 분석
   CamState_VideoAnalyticsToBeInitialized = 0x00020000,
   CamState_CanNotFindSourceVideoChannel  = 0x00040000, // 가상 PTZ 카메라인 경우 원본 비디오 채널을 찾을 수 없는 상태
   CamState_VideoAnalyticsUnregistered    = 0x00080000, // 비디오 분석 기능이 등록되지 않은 채널
   CamState_EventAlarmOn                  = 0x00100000,

   CamState_Deactivating   = 0x00800000,
   CamState_EventExist     = 0x01000000,
   CamState_ToBeDeactivate = 0x02000000,
   CamState_ToBeActivate   = 0x04000000,

   CamState_MainVACConfigToBeLoaded = 0x40000000,
   CamState_FrameRateIsMismatched   = 0x80000000,
};

// 카메라의 각종 상태를 나타내는 확장 플래그들
// CamStateEx와 CamStateEx2는 클라이언트에 전달되지 않는 카메라의 상태 값이다.
enum CamStateEx {
   CamStateEx_OnSetup       = 0x00000004,
   CamStateEx_SendLiveVideo = 0x00000040,

   CamStateEx_Repaintable     = 0x00000100,
   CamStateEx_KeyFrameSended  = 0x00000200, // 서버시스템의 카메라에서 사용.
   CamStateEx_GettingPTZPosOk = 0x00000400,
   CamStateEx_SendVideo       = 0x00000800, // 비디오 전송 스레드
   CamStateEx_ThreadSendVideo = 0x00001000, // 비디오 전송 쓰래드 생존 여부

   CamStateEx_FlushingLiveEvents        = 0x00010000,
   CamStateEx_PTZTouringDlgOpened       = 0x00040000,
   CamStateEx_ShowLiveFrame             = 0x00100000,
   CamStateEx_SignalStopCameraMgrThread = 0x02000000, //CameraMgrProc 쓰래드 종료 플래그
   CamStateEx_ReceiveLiveVideo          = 0x04000000,
   CamStateEx_PrepareLiveVideo          = 0x08000000,
   CamStateEx_WaitingEvtQueueEmpty      = 0x40000000,
};

enum CamStateEx2 {
   CamStateEx2_PausingVideoAnalyticsBegun       = 0x00000001,
   CamStateEx2_PausingVideoAnalyticsEnded       = 0x00000002,
   CamStateEx2_RequestVideoStreamToIVXVS        = 0x00000010,
   CamStateEx2_DecodingLiveVideo                = 0x00000020,
   CamStateEx2_DefVideoAnalyticsSetupToBeLoaded = 0x00000040,
   CamStateEx2_VAModuleInitialized              = 0x00000080,
   CamStateEx2_FDAndOTInitialized               = 0x00000100,
   CamStateEx2_InitVAModuleExceptObjectTracker  = 0x00000200,
   CamStateEx2_ToBeUpdateVAAreas                = 0x00000400,
   CamStateEx2_ObjectTrackerJustInitialized     = 0x00000800,
   CamStateEx2_ObjectCountersTeBeReset          = 0x00001000,
};

// PTZ 카메라에서 필요할 만한 상태는 이곳에 정의한다.
enum PTZState {
   PTZState_PresetTour                       = 0x00000001, // PTZ 투어링 상태.
   PTZState_PresetTourPaused                 = 0x00000002, // PTZ 투어링이 일시정지 된 상태. (1.사용자에 의한 프리셋 이동, 2.알람입력, 3.사용자PTZ제어, 4.단일추적)
   PTZState_PresetMoveStarted                = 0x00000004, // 프리셋 위치로 이동이 막 시작된 상태
   PTZState_PresetMoving                     = 0x00000008, // 프리셋 위치로 이동 중
   PTZState_PTZMoving                        = 0x00000010, // PTZ/F 이동중 (PTZ/F 제어 상태 포함)
   PTZState_UserControlBegun                 = 0x00000020, // 사용자에 의해 PTZ/F 이동이 막 시작된 상태
   PTZState_UserControlEnded                 = 0x00000040, // 사용자에 의해 PTZ/F 이동이 막 중지된 상태
   PTZState_UserControlOngoing               = 0x00000080, // 사용자가 PTZ/F 제어 중
   PTZState_PTZStopping                      = 0x00000100, // 사용자가 이동 중지 명령을 내린후에 PTZ 이동이 멈추고 있는 상태
   PTZState_GotoAbsPosBegun                  = 0x00000200,
   PTZState_GotoAbsPosOngoing                = 0x00000400,
   PTZState_SPTZTrkOngoing                   = 0x00000800,
   PTZState_SPTZTrkBegun                     = 0x00001000,
   PTZState_SPTZTrkToBeEnded                 = 0x00002000,
   PTZState_SPTZTrkEnded                     = 0x00004000,
   PTZState_SpecifyingTrkObj                 = 0x00008000,
   PTZState_GotoHomePosToBeStarted           = 0x00010000,
   PTZState_PresetTourToBeRestarted          = 0x00020000,
   PTZState_PTZStoppedByUserControl          = 0x00080000,
   PTZState_PTZControlReady                  = 0x00100000,
   PTZState_RestartPresetTourFromFirstPreset = 0x00200000,
   PTZState_SPTZCamAutoTracking              = 0x00400000, // PTZ 카메라 단독으로 물체를 추적하고 있는 경우.
   PTZState_AutoPTZTrkByMasterOngoing        = 0x00800000, // 마스터카메라에의하여 PTZ자동추적을 하고 있는 상태.
   PTZState_AutoPTZTrkByMasterToBeEnded      = 0x01000000, // 위 상태가 끝나기를 대기하고 있는 상태.
   PTZState_LockObjEvtOngoing                = 0x02000000, // LockedObject에 의하여 이벤트가 발생하고 있는상태
   PTZState_SignalPTZMoveStarted             = 0x20000000,
   PTZState_SignalPTZMoveStopped             = 0x40000000,
};

const int PTZState_OnNet = 0xFFFFFFFF & ~( 0 );
// PTZState_OnNet 플레그는 서버 클라이언트 간에 전송가능한 플레그를 정의한다.

enum PTZStateEx {
   PTZStateEx_InHomePos                          = 0x00000008,
   PTZStateEx_PresetMoveBySensorInputToBeStarted = 0x00000100,
   PTZStateEx_PresetMoveBySensorInputOngoing     = 0x00000200,
   PTZStateEx_AllPTZCtrlDisabled                 = 0x00000400,
   PTZStateEx_GotoAbsPosNotSupported             = 0x00000800, // VPU->SRS->RAS로 PTZ카메라 정보를 전달하기위해 임시로 만든 상태임.
   PTZStateEx_PTZPosInitToBeStarted              = 0x00001000,
   PTZStateEx_PTZPosInitBegun                    = 0x00002000,
   PTZStateEx_PTZPosInitOngoing                  = 0x00004000,
   PTZStateEx_GoHomeOnPTZFreeze                  = 0x00020000,
   PTZStateEx_TheFirstPresetMovingToBeStarted    = 0x00040000,
};

enum PTZMoveType {
   PTZMoveType_Cont       = 0x00000001,
   PTZMoveType_Focus      = 0x00000002,
   PTZMoveType_Iris       = 0x00000004,
   PTZMoveType_GotoAbsPos = 0x00000008,
   PTZMoveType_TargetBox  = 0x00000010,
   PTZMoveType_OffsetPos  = 0x00000020,
};

enum PresetMoveType {
   PresetMoveType_Normal         = 1, // 일반모드이다. 사용자에의하여 프리셋 이동을하였거나 연동된 알람입력에 의하여 프리셋이동한 상태를 말한다.
   PresetMoveType_Touring        = 2, // 프리셋 투어링에 의하여 프리셋이 이동한 상태이다.
   PresetMoveType_PresetMoveOnly = 3, // 프리셋 이동만 수행한다. Normal 과 Touring 에서는 해당 프리셋에 연동된 VACL설정이 로드되는 반면 본 상태에서는 그렇지 않다.
};

enum PTZMouseMove {
   PTZMouseMove_Joystick           = 1,
   PTZMouseMove_ClickPosToCenter   = 2,
   PTZMouseMove_SpecifyTrackObject = 3,
};

// enum PTZCmdType에 선언된 변수와 겹치지 않기 위해 10000번 부터 시작한다.

enum IVXCmdType {
   IVXCmdType_DefogOn                           = 10000,
   IVXCmdType_DefogLevel                        = 10001,
   IVXCmdType_VideoStabilizationOn              = 10002,
   IVXCmdType_VideoStabilizationFrameNum        = 10003,
   IVXCmdType_TestIRMode                        = 10004,
   IVXCmdType_DayNightMode                      = 10005,
   IVXCmdType_WhiteBalanceMode                  = 10006,
   IVXCmdType_GotoHomePos                       = 10007,
   IVXCmdType_ForceToStartHomePosVideoAnalytics = 10008,
   IVXCmdType_IRMode                            = 10009,
};

enum VACSetupType {
   VACSetupType_NotLoaded = 0,
   VACSetupType_Default   = 1,
   VACSetupType_Main      = 2,
   VACSetupType_Preset    = 3,
};

enum GetVideoFrameMode {
   GetVideoFrameMode_Smallest = 0,
   GetVideoFrameMode_Largest  = 1,
   GetVideoFrameMode_EMap     = 2,
};

enum ForegroundDetectorType {
   ForegroundDetectorType_SG      = 0,
   ForegroundDetectorType_FAD     = 1,
   ForegroundDetectorType_HCT     = 2,
   ForegroundDetectorType_Face    = 3,
   ForegroundDetectorType_Oblique = 4,
};

enum TOLType {
   TOLType_ObjecTracking_Tracked = 0,
   TOLType_ObjecTracking_Lost    = 1,
   TOLType_EventDetection_EvtGen = 2,
   TOLType_Num                   = 3,
   TOLType_SuddenSceneChange     = 4,
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: CCamera
//
///////////////////////////////////////////////////////////////////////////////

class CCamera {
public:
   CCamera( CLocalSystem* pSystem, int nChannelNo = 0 );
   virtual ~CCamera();

   uint PrevSuddenSceneChangeDetectionTime = 0;
   bool nChecked_Camera_Damage_ = FALSE;
   int Camera_Damage_Zone_ID_   = -1 ;
   std::string Camera_Damage_Zone_Name_ = "";
   int DetectCameraTime = 0;
   
public:
   struct CheckedOverlapEvent {
      int   EventType         = 0;
      int   EventDetectTime   = 0;
      float PrevDetectTime    = 0.0f;
      BOOL  SendEventInfo     = TRUE;
      BOOL  FirstEvent        = FALSE;
   };  
   CheckedOverlapEvent Check_OverlapEvent_[ER_EVENT_CROWD_DENSITY + 1];
   int nEvent_ID_count_;// event count
   int nObject_ID_count_; // object count
   int ChannelNo; // 채널 번호
   int UID; // LocalSystem, ClientSytem에 등록되어있는 카메라를 구분하기 위한 유일한 값을 갖는 정수값.
   std::string CameraGuid;

   CIntelliVIXDoc* m_pDoc;

   CLocalSystem* System;
   std::string CameraName;
   BGRImage SnapshotImage;
   CCriticalSection m_csSnapshotImage;

   // Camera 객체 선언
public:
   CCamera* LocalCamera;
   CCamera* TmpCamera;

   // 이벤트 관리
public:
   void ClearLiveEventsAndObjectBuffer( BOOL bClearVideoAnalyticsEventOnly = FALSE );

   ////////////////////////////////////////////////////////////////////////////////////////////////////////
   // Network
   ////////////////////////////////////////////////////////////////////////////////////////////////////////

public:
   struct VideoStreamingThreadContext {
      int m_nStreamIdx;
      CCamera* m_pCamera;
   };

public:
   BOOL m_bStartVideoStreamThread[IVX_VIDEO_STREAM_NUM];
   BOOL m_bSendLiveVideo;
   uint m_nSendLiveVideoReqTime;
   BOOL m_bSendVideo[IVX_VIDEO_STREAM_NUM];
   BOOL m_bStopVideoStreamingThread[IVX_VIDEO_STREAM_NUM];
   std::thread m_hVideoStreamingThread[IVX_VIDEO_STREAM_NUM];
   CMMTimer* m_TimerVideoStreaming[IVX_VIDEO_STREAM_NUM];
   CVideoSnder m_LiveSnderMC[IVX_VIDEO_STREAM_NUM];

public:
   CCriticalSection m_csVideoStreamStartStop;
   BOOL IsStartVideoStreamingThread( int nStreamIdx );
   void StartVideoStreamingThread( int nStreamIdx );
   void StopVideoStreamingThread( int nStreamIdx );
   float GetVideoStreamingFrameRate( int nStreamIdx );
   UINT VideoStreamingProc( int nStreamIdx );
   static UINT ThreadFuction_VideoStreaming( std::shared_ptr<VideoStreamingThreadContext> pContext );

   ////////////////////////////////////////////////////////////////////////////////////////////////////////
   // Camera Setup 관련
   ////////////////////////////////////////////////////////////////////////////////////////////////////////
public:
   CCriticalSection m_csCameraSetup;

public:
   int ApplySetupLocal( BOOL bDeactivate );
   int LinkToTemp( CCamera* t_cam );
   int TempToOrigin( CCamera* t_cam, int item = SMG_SETUP_ALL );
   int OriginToTemp( CCamera* t_cam, int item = SMG_SETUP_ALL );
   int GetChangedSetupItem( CCamera* camera );
   int CheckDeactivation( CCamera* camera, int filter = SMG_SETUP_ALL );

   int ReadFile( CXMLIO* pIO, int nRWFlag ) throw( IVXException );
   int WriteFile( CXMLIO* pIO, int nRWFlag );
   int ReadSetup( CXMLIO* pIO, int item = SMG_SETUP_ALL, int nRWFlag = 0 ) throw( IVXException );
   int WriteSetup( CXMLIO* pIO, int item = SMG_SETUP_ALL, int nRWFlag = 0);
   int ReadVACSetup( CXMLIO* pIO, int item = SMG_SETUP_ALL );
   int WriteVACSetup( CXMLIO* pIO, int item = SMG_SETUP_ALL );
   int WriteDefVACSetup( CXMLIO* pIO, int item = SMG_SETUP_ALL );
   int ReadMainVACSetup( CXMLIO* pIO, int item = SMG_SETUP_ALL ) throw( IVXException );
   int WriteMainVACSetup( CXMLIO* pIO, int item = SMG_SETUP_ALL );

   ////////////////////////////////////////////////////////////////////////////////////////////////////////
   // 카메라 관리
   ////////////////////////////////////////////////////////////////////////////////////////////////////////
public:
   FILETIME CurFileTime;
   FILETIME m_ftLastDoImageProcessTime;
   FILETIME m_ftLastDoDisplayTime;
   int m_nDoImgProcPos;
   int m_nDoRecordPos;
   int m_nDoDisplayPos;

   BOOL m_bResetObjCountUsed;
   int m_nResetObjCount_PeriodTime;
   int m_nResetObjCount_LastActiveTime;

   std::vector<RESET_OBJECT_TIME> m_vecResetObjCount_DayTime;

public:
   BOOL IsVisible();
   BOOL IsDecodingLiveVideo();
   BOOL IsVideoSignalOk();
   BOOL IsActivated();

   CLocalSystem* GetSystem()
   {
      return System;
   }
   void SetVideoSize( int nVideoType, int nWidth, int nHeight );
   void SetCameraName( LPCSTR szCameraName );
   void SetCameraName( int nChannelNo );
   float GetFrameRateLive( int nVCProcCallbackFuncType );
   void GetDescString( int nDescID, std::string& str );
   std::vector<float> fpsRecVec;
   uint ImgProcCount = 0;
   uint m_nPrevImgProcCountCheckTime = 0;

   // UI 관련
public:
   void SetSystem( CLocalSystem* pSystem )
   {
      System = pSystem;
   }
   int GetFrameStateString( IVXVideoFrameInfo* pVideoFrameInfo, std::string& str );
   BOOL IsEventAlarmOn( int nVideoType ); // 실시간 또는 재생 시의 이벤트 알람 상태를 얻는다.

   int GetStreamIndexOfVideoImage( int nVideoType, int nGetVideoFrameMode );
   BOOL GetVideoImage( int nVideoType, BGRImage& d_image, int nGetVideoFrameMode = GetVideoFrameMode_Largest );
   BOOL GetYUY2VideoImage( BYTE** pYUY2Img, int& nWidth, int& nHeight );
   BOOL Snapshot( BOOL bToTmp );
   void SetLocalCamera( CCamera* pCamera )
   {
      LocalCamera = pCamera;
   }
   EventZone* FindEventZone( int zone_id );

   ////////////////////////////////////////////////////////////////////////////////////////////////////////
   // 비디오 분석 결과의 화면 출력과 전송
   ////////////////////////////////////////////////////////////////////////////////////////////////////////
public:
   CVideoFrame m_VideoFrameLive; // 실시간 영상용

public:
   void ProcessOnVCDisplayStatus();
   CVideoFrame* GetDispVideoFrame( int nVideoType );
   void ExtractVideoFrameInfo( IVXVideoFrameInfo& videoFrameInfo, int nImgWidth, int nImgHeight, BOOL bSendToClient = FALSE );
   BOOL SetVideoFrame_Live( BYTE* pYUY2Buff, int nImgWidth, int nImgHeight );
   BOOL SetTrackedObjectsOfLiveVideoFrame();

   BOOL IsShowLiveFrame();
   BOOL ShowFrame_LiveVideo( BYTE* pYUY2Buff, int nImgWidth, int nImgHeight );
   BOOL ShowFrame( BYTE* pDrawBuffer, CVideoFrame* pVideoFrame, BOOL bRepaint = FALSE );
   void UpdateObjectCounters( IVXVideoFrameInfo& s_videoFrameInfo );

   BOOL IsPanoramaCamera();
   BOOL IsVideoFileCamera();
   BOOL RestartCamera();

   ////////////////////////////////////////////////////////////////////////////////////////////////////////
   // PTZ
   ////////////////////////////////////////////////////////////////////////////////////////////////////////

public:
   int PTZMouseMoveMode;
   uint32_t m_nGotoHomePosWaitingStartTime;
   uint32_t m_nGotoHomePosWaitingTime;
   uint32_t m_nExePresetTourWaitingStartTime;
   uint32_t m_nExePresetTourWaitingTime;
   uint32_t m_nPresetTouringStartTime;
   uint32_t m_nPTZTrkModeAutoOnWaitTime;
   uint32_t m_nUserControlEndTime;
   int m_nTgtZoomPos_MouseWheel_UpdateTime;
   float m_fTgtZoomPos_MouseWheel;
   FPTZVector m_pvCurAbsPTZPos; // 현재의 절대각 위치.    CLocalSystem::m_VideoServer 변수에서 사용.
   FPTZVector m_pvUserOffsetPos;
   float m_fCurFocalLength; // 현재의 소스영상크기를 기준으로 한 FocalLength 값. CLocalSystem::m_VideoServer 변수에서 사용.

   PTZPosition PTZPos; // GetPosition된 값
   CPTZCtrl* m_pPTZCtrl;
   uint32_t HomePosStartTime;

public:
   BOOL GetAbsPtzPos( PTZPosition& pos );
   BOOL GetAbsPtzPos( FPTZVector& pos );
   BOOL GotoAbsPtzPos( PTZPosition& pos );
   BOOL GotoAbsPtzPos( FPTZVector& pos );
   BOOL ConvertAbsPtzPos( PTAbsPosConvertMode nPTAbsPosConvertMode, const FPTZVector& pvInputAbsPTPos, FPTZVector& pvOutputPTPos );

public:
   static FBox2D null_fox2d;
   static float  MovingCamCropRatio;
   CEventEx m_evtExitCameraMgrProc;
   std::thread m_hCameraMgrThread;
   static UINT ThreadFunc_CameraMgrProc( LPVOID param );
   BOOL StartCameraMgrThread();
   BOOL StopCameraMgrThread();
   void ProcessDecreaseFps();
   void CameraMgrProc();

   void SetContPTZMoveSpeed( int pt_speed, int z_speed, int f_speed );
   BOOL GetAutoPTZCtrlMode();
   void SetAutoPTZCtrlMode( BOOL bAutoPTZCtrl, BOOL bSendIVXConnection = TRUE, BOOL bSendIVXVS = TRUE );
   void ContPTZMove( FPTZVector move_vec, BOOL bUserCtrl );
   void ContPTZMove_Focus( float focus, BOOL bUserCtrl );
   void ContPTZMove_Iris( float iris, BOOL bUserCtrl );
   void ContPTZMove( FPTZVector move_vec, float focus, float iris, BOOL bUserCtrl );
   void AbsPosMove( FPoint2D center_pos, BOOL bUserCtrl );
   void AbsPosMove( FBox2D tgt_box, BOOL bUserCtrl );
   void PTZMove( int nPTZMoveType, FPTZVector pos, BOOL bUserCtrl );
   void PTZMove( int nPTZMoveType, PTZPosition pos, BOOL bUserCtrl );
   void PTZMove( int nPTZMoveType, FPTZVector move_vec, float focus, float iris, FBox2D tgt_box, FPTZVector offset_pos, BOOL bUserCtrl );
   void GotoHomePos( BOOL bForce = FALSE );
   BOOL GetPTSpeed( BOOL bSet, float& pt_speed );
   void ExecPTZCmd( int nPTZCmdType, int nParam );

   // PTZ 위치 초기화
public:
   BOOL m_bPrevPositionInitTimePassed;

   ////////////////////////////////////////////////////////////////////////////////////////////////////////
   //  PTZ Control & PTZ Touring
   ////////////////////////////////////////////////////////////////////////////////////////////////////////

public:
   int m_nPTZTourReturnDirection;
   int m_nPTZMoveType; // PTZF 움직임 종류 (PTZ or F). enum PTZF_MOVE_TYPE 참고
   int m_nPresetMoveType; // 프리셋 이동 종류 (수동, 투어링, 알람). enum PresetMoveType 참고
   int m_nPresetMoveRemainTime; // 다음 프리셋으로 이동하기 까지 남은 시간. 화면 디스플레이 용이다.
   uint32_t m_nPresetTourPausedTime; // Preset Touring 을 수행한 바로 이전 시간.
   uint32_t m_nPrvTimePT; // Preset Touring 을 수행한 바로 이전 시간.
   uint32_t m_nPresetMoveElapsed; // Preset 이동을 한 후 경과된 시간.
   int m_nPresetNoBySensorInput; // 센서 입력에 의한 프리셋 이동에서의 프리셋 번호.
   uint32_t m_nPresetMoveTimeBySenserInput;
   uint32_t m_nPresetPeriodBySensorInput;
   uint32_t m_nPresetStartTime; // 프리셋으로 이동이 시작된 시간(msec)
   uint32_t m_nLastPTZMoveTime; // PTZ 제어를 멈춘 시간. 연속 이동중 Stop 한 경우, 절대각 이동 또는 Preset Move를 한 경우임.
   uint32_t m_nPTZCtrlStopTime; // PTZ 제어를 멈춘 시간. 연속 이동중 Stop 한 경우, 절대각 이동 또는 Preset Move를 한 경우임.
   uint32_t m_nPTZMoveStopWaitTime; // PTZ 제어를 중단 한 후 PTZ 이동이 완전히 멈출 때 까지의 대기시간. 대부분 이 시간이 경과되기 전에 PTZ제어객체에서 멈추었다는 신호를 보내준다.
   float m_fDisplayZoomStepSize; // 화면 출력시만 사용되는 줌 스탭 크기
   char m_szActivePresetName[256];
   CCriticalSection m_csActivePresetName;

   CPreset* m_pCurPreset; // Preset Touring에 서 사용하는 현재 프리셋 포인터.
   CPresetGroup* m_pActivePresetGroup;
   CPreset* m_pActivePreset;
   CPresetGroupList m_PresetGroupList;
   CCriticalSection m_csActivePreset;

public:
   BOOL ApplyPTZTouringSetup( BOOL bEnablePTZTouring, BOOL bPresetChanged, CPresetGroupList& preset_groups, BOOL bDoNotDeactivate = FALSE );
   BOOL SetPreset( int nPresetNo );
   BOOL ClearPreset( int nPresetNo );
   BOOL GotoPreset( int nPresetNo, int nPresetMoveType = PresetMoveType_Normal, BOOL bForceMaxSpeed = FALSE );

   void LoadHandoffVACSetup();
   void LoadMainVACSetup( int item = SMG_SETUP_ALL );
   void LoadDefVACSetup( int item = SMG_SETUP_ALL );
   void LoadPresetVACSetup( CPreset* pPreset );

   void LoadRealtimeEventCountInfoToMainVASetup();
   void LoadRealtimeEventCountInfoToPresetVASetup();
   void LoadRealtimeEventCountInfoToMainOrPresetVASetup();
   BOOL ResetAllObjectCounters();

   void LoadEventDetectionSetupWhenPresetMovingVA();
   void SetActivePreset( BOOL bExistActivePreset, int nGroupIndex, int nPresetIndex );
   void SetActivePreset( CPreset* pPreset );
   void LoadHomePositionSetting();

   ////////////////////////////////////////////////////////////////////////////////////////////////////////

   // Video Analytics Core
   ////////////////////////////////////////////////////////////////////////////////////////////////////////
public:
   // 중요 : m_nState, m_nPTZState, m_nPTZStateEx 변수들은 매 프레임 별로 클라이언트들에게 상태가 전달된다.
   //        m_nStateEx 는 상태가 전달되지 않는다.
   int m_nState; // 각종 상태를 나타내는 플래그들의 집합.
   int m_nStateEx; // UI 관련 상태를 나타내는 플래그들의 집합. (sjm)
   int m_nStateEx2;
   int m_nPTZState;
   int m_nPTZStateEx;
   int m_nCurVACSetupType; // enum VACSetupType 참조
   BOOL m_bGetColorSrcImageOK;
   BOOL m_bGetGrayScaleSrcImage;
   BOOL m_bGetYuy2SrcImageOK; // (xinu_bk09)
   int m_nPrvState; // m_nState 의 이전 상태
   int m_nPrvPTZState;
   BOOL m_bReductionFactorChecked;
   int Options; // 각종 옵션을 나타내는 플래그들의 집합.
   int DTCOptions; // DTC ON/OFF 관련 옵션을 나타내는 플래그들의 집합.
   float FrameRate; // 입력이 카메라 비디오인 경우에는 DesiredFrameRate와 동일한 값을 갖고, 입력이 AVI 파일인 경우에는 AVI 파일의 Frame Rate와 동일한 값이 된다.
   float ReductionFactor; // VMDT 모듈로 입력되는 영상의 축소률.
   float DesiredFrameRate; // 입력이 카메라 비디오인 경우 사용자가 원하는 영상 처리 Frame Rate.
   UINT64 PrevImageProcessCount; // 이전에 처리된 프레임 수.
   BOOL m_bExistEvent;
   BOOL m_bPrevAutoPTZCtrlMode;
   BOOL m_bPrevVideoAnalytics;
   CTime m_PausingVideoAnalyticsStartTime; // 비디오 분석 잠시 멈춤 시작 시간
   uint32_t m_nPausingVideoAnalyticsAutoReleaseWaitingTime; // 비디오 분석 잠시 멈춤을 유지하는 시간
   CTime m_PausingVideoAnalyticsScheduleStartTime; // 비디오 분석 스케줄 잠시 멈춤을 시작하는 시간
   uint32_t m_nPausingVideoAnalyticsScheduleAutoReleaseWaitingTime; //
   uint32_t m_nStoppingVideoAnalyticsStartTime;
   uint32_t m_nStoppingVideoAnalyticsPeriodInMilliSec;

public:
   GImage SGImage; // 영상 분석용 그레이스케일 이미지.
   BGRImage SCImage; // 영상 분석용으로 사용되는 컬러이미지.
   GImage BMUSImage; // Image buffer for storing a background model update scheme
   BArray1D YUY2SrcImg; // 영상 분석용으로 사용되는 YUY2 포멧의 소스버퍼. SrcImgSize의 영상크기를 가진다.
   ISize2D SrcImgSize; // 영상 분석용 소스영상 크기
   ISize2D ProcImgSize; // 크기가 조정된 영상분석영상 크기. SGImage, BMUSImage, SCImage 가 이 크기를 가진다.
   FILETIME Timestamp;

public:
   CEventEx m_evtDoProcessOnVideoFrame;
   CCompressImageBufferQueue m_EncodedVideoStramQueue[MAX_IP_CAMERA_STREAM_NUM]; // IP 카메라에서 얻어지는 인코딩 비디오 프레임의 큐.

public:
   CConfig_PTZCamera Config_PTZC;
   CConfig_VideoStream Config_VS; 
   // 실제 운용시 사용되는 객체
public:
   int                              m_nLastForegroundDetectorType = -1;
   ObjectIDGeneration               ObjectIDGenerator;
   EventDetection                   EventDetector;
   ForegroundDetection*             frg_detector;
   ForegroundDetection_SGBM         ForegroundDetector_General;
#if defined(__LIB_SEETA)
   ForegroundDetection_FCD_Seeta    ForegroundDetector_Face;
   FaceLandmarkDetection_Seeta      FaceLandmarkDetector;
#endif
   ForegroundDetection_HMD_OHV_MBS  ForegroundDetector_Overhead;
   ForegroundDetection_FAD          ForegroundDetector_MovCam;

#if defined(__LIB_DARKNET)
   ForegroundDetectionClient_OBD_YOLO ForegroundDetector_Object;
#endif
   PersonAttrRecognitionClient  PersonAttrRecognizer;
   VehicleTypeRecognitionClient VehicleTypeRecognizer;

   IBox2D  CropBox;
   GImage  CropImage;

   BOOL ObjectAnalysisMode_DNN_ = TRUE;
   
   FaceDetectionConfig          Config_FaceDetection;
   HeadDetectionConfig          Config_HeadDetection;
   ObjectClassification_Common* ObjectClassifier = nullptr;
   ObjectClassification_Basic   ObjectClassifier_Basic;
   ObjectClassification_DNN     ObjectClassifier_DNN;
   //ObjectClassificationClient   ObjectClassifier_DNN;
   ObjectFilteringEx            ObjectFilter;
   ObjectTracking               ObjectTracker;
   FrontalFaceVerification      FrontalFaceVerifier;
   RealObjectSizeEstimation     RealObjectSizeEstimator;
   EventGrouping                EventGrouper;

public:
   ColorChangeDetection ColorChangeDetector;
   FlameDetection FlameDetector;
   GroupTracking GroupTracker;
   SmokeDetection SmokeDetector;

public:
   CrowdDensityEstimation_OBS CrowdDensityEstimator;
   DwellAnalysis DwellAnalyzer;
   TrafficCongestionDetection TrafficCongestionDetector;
   WaterLevelDetection WaterLevelDetector;
   ZoneColorDetection ZoneColorDetector;

   // 홈위치의 분석 설정을 저장만 하는 클래스
public:
   EventDetection                MainConfig_EventDetection;
   ForegroundDetection_SGBM      MainConfig_ForegroundDetection;
   ObjectClassification_Common   MainConfig_ObjectClassification;
   ObjectFilteringEx             MainConfig_ObjectFiltering;
   ObjectTracking                MainConfig_ObjectTracking;
   RealObjectSizeEstimation      MainConfig_RealObjectSizeEstimation;
   GroupTracking                 MainConfig_GroupTracking;
   SmokeDetection                MainConfig_SmokeDetection;
   FlameDetection                MainConfig_FlameDetection;
   HeadDetectionConfig           MainConfig_HeadDetection;
   //FaceDetectionConfig         MainConfig_FaceDetection;
   int VAMode;

#ifdef __WIN32
public:
   std::recursive_mutex CS_AuxVideoDisplay;
   IPoint2D AuxVideoDispWndPos;
   CAuxVideoDisplayWnd* AuxVideoDisplayWnd;
#endif // __WIN32

public:
   CCriticalSection CS_DoImageProcess;
   CCriticalSection CS_ForegroundDetector; // Processing 과 Display 스레드간의 충돌 방지용.
   CCriticalSection CS_ObjectTracker; // Processing 과 Display 스레드간의 충돌 방지용.
   CCriticalSection CS_EventDetector;
   CCriticalSection CS_PresetZones;

public:
   int InitSrcAndProcImgSize();
   void CorrectReductionFactor( int nWidth, int nHeight );
   ISize2D GetResizedImageSize();
   int InitVAModules();
   void UninitVAModules();
   void ResetVAModules();
   void InvalidateAllObjectsAndEvents();
   void UpdateVAAreaMaps();

#ifdef __WIN32
   void OpenAuxVideoDisplayWnd();
   void CloseAuxVideoDisplayWnd();
#endif // __WIN32

   void ProcessOnTrackedObjectsAndEvents();
   void ProcessOnTrackedObjects();
   void ProcessOnEvents();
   void ProcessOnResetObjectCounter();
   void TakeActionForEvent( int nTOLType, EventEx* event, TrackedObjectEx* t_object );

public:
   int Activate();
   int Deactivate();

   // NOTE(yhpark): There are codes that simple things, which just preparing VA's image, into complicated codes...
   BOOL DoesVideoAnalyticsOnColorImageNeeded();
   BOOL DoesColorImageNeeded();
   void GetSourceImage( BYTE* pImgBuff, int nWidth, int nHeight );
   ForegroundDetection* GetForegroundDetector(int VAMode);

   int Initialize();
   void Close();
   BOOL PausingVideoAnalytics( BOOL bPause, BOOL bAutoRelase, int nAutoRelaseWaitingTime, CLocalSystem* pServerSystem = NULL );
   BOOL PausingVideoAnalyticsSchedule( BOOL bPause, BOOL bAutoRelase, int nAutoRelaseWaitingTime, CLocalSystem* pServerSystem = NULL );
   void StoppingVideoAnalyticsForAWhile( uint nStoppingTimeInMilliSec );

   ////////////////////////////////////////////////////////////////////////////////////////////////////////
   // VCM (Video Channel Manager)
   ////////////////////////////////////////////////////////////////////////////////////////////////////////

public:
   CVideoChannel* VC;
   int VCID; // 카메라와 1:1 대응되는 비디오 채널 아이디

   ushort NoVideoSignalChannels;
   ushort ConnectingChannels;
   ushort AuthorizationFailedChannels;

   uint m_nVideoFrameFlag;

   std::shared_ptr<CPTZStatusCallback> m_PTZStatusCallback;
   std::shared_ptr<CPTZCtrlCallback> m_PTZCtrlCallback;
   std::shared_ptr<CRTSPStreamingChannel> m_RTSPStreamingChannel[IVX_VIDEO_STREAM_NUM];
   std::shared_ptr<CIvcpRtspSender> m_ivcpRtspSender;

public:
   int ReadVCSetup( CXMLIO* pIO, int nRWFlag );
   int WriteVCSetup( CXMLIO* pIO, int nRWFlag );

public:
   void OnPTZMoveStarted();
   void OnPTZMoveStopped();
   void ForceToStartHomePosVideoAnalytics();
   void SetGotoHomePosToBeStarted( int nWaitTime );
   void SetPresetTourToBeStarted( int nWaitTime );
   void SetGotoHomePosOrPresetTourToBeStarted( int nWaitTime );
   BOOL SetAllPTZMoveStopped();

public: // 멀티스트리밍 관련함수
   ISize2D GetInputStreamVideoSize( int nIVXStreamIndex );
   float GetInputStreamFrameRate( int nIVXStreamIndex );
   int GetInputStreamCodecID( int nIVXStreamIndex );
   void DetermineVCDisplayCallbackFuncState();
   void DetermineVCImgProcCallbackFuncState();
   void DetermineVCVideoFrameCallbackFuncState();

public:
   void DoImgProcDispatchPTZMsg( const VC_PARAM_STREAM& param );
   void DoImgProcPresetTouring( const VC_PARAM_STREAM& param ); // DoImageProcess 함수에서 호출
   void DoImgProcVideoAnalytics( const VC_PARAM_STREAM& param ); // DoImageProcess 함수에서 호출
   void DoImgProcSinglePTZTrack( const VC_PARAM_STREAM& param ); // DoImageProcess 함수에서 호출
   void DoImageProcess( const VC_PARAM_STREAM& param );
   BOOL OnVCCameraEvent( const VC_PARAM_CAMERA_EVENT& param );
   BOOL OnVCConfigToBeLoaded();
   BOOL OnEncodeVideoFrame( const VC_PARAM_ENCODE& param );

   // VCM에서 호출되는 콜백 함수들...
   UINT m_nDisplayLog    = 0;
   UINT m_nDisplayLogCnt = 0;

   static BOOL CallbackImageProcess( const VC_PARAM_STREAM& param );
   static BOOL CallbackDisplay( const VC_PARAM_STREAM& param );
   static BOOL CallbackDecodeVideoFrame( const VC_PARAM_STREAM& param );
   static BOOL CallbackEncodeVideoFrame( const VC_PARAM_ENCODE& param );
   static void CallbackCameraEvent( const VC_PARAM_CAMERA_EVENT& param );
   static BOOL CallbackVCConfigToBeLoad( LPVOID pParam );

   BOOL ParseRTSPRarameter( char const* szInData, std::string& strRequest );
   BOOL ParseRTSPXMLValue( const std::string& strRequest, std::string& strValue );
   BOOL OnRTSP_VideoChannelSetting( const std::string& strRequest );
   BOOL OnRTSP_CameraSettingAll( const std::string& strRequest, int nType );
   BOOL OnRTSP_VideoAnalysisSetting( const std::string& strRequest );
   BOOL OnRTSP_CameraInfo( const std::string& strRequest );
   BOOL OnRTSP_ResetEventZoneCount( const std::string& strRequest );
   BOOL OnRTSP_ResetCountSchedule( const std::string& strRequest );

   void OnRTSPSetParameter( char const* fullRequestStr );
   void OnRTSPGetParameter( char const* fullRequestStr );

   void IVCPSendFrameInfo();

public:
   BOOL IsPTZCamera();
   BOOL IsPTZCameraReady();
   BOOL IsVirtualPTZCamera();
   BOOL IsPTZMoving();
   BOOL IsPTZDoingNothing();
   void DetermineVideoAnalyticsStateOnOff(); // 비디오 분석 수행 여부 판별함수
   void ProcessOnVideoFrame();

   CRTSPStreamingChannel* GetRtspStreamingChannel( int pos );

   ////////////////////////////////////////////////////////////////////////////////////////////////////////
   // Single PTZ Tracking
   ////////////////////////////////////////////////////////////////////////////////////////////////////////
public:
   int SPTZTrkMode;
   int SPTZTrackObjectIDHighlighted;
   int SPTZTrackObjectIDToBeTracked;

   TrackedObjectEx SinglePTZTrackingObj;
   TrackedObjectEx* SinglePTZTrackingObjPtr;
   EventEx SinglePTZTrackingEvent;
   EventRuleEx PTZTrkEvtRule;

   PTZPosition PTZPosInit;
   CSinglePTZTrack* m_pSPTZTracker;

public:
   BOOL IsSinglePTZTrkSupported();
   BOOL IsSinglePTZTracking();
   BOOL IsSupportAutomousPTZCameraTracking();
   BOOL SetSPTZTrkToBeEnded();
   BOOL FindEventTriggerdTrackedObject();
   BOOL FindHandoffedTrackObject();
   BOOL IsSPTZTracking();
   
   BOOL CheckOverlapEvent(int nEventType);

public:
   //monitoring..
   uint m_processed_count = 0;
   std::recursive_mutex m_mutex_processed_count;
   // combine logger
   std::string UPlus_now_filename_ = "";
   
public:
   //-------------------------------------------------------------------------------------
   // [SGLEE:20180219MON] 마스킹 관련
   //-------------------------------------------------------------------------------------
   enum EMaskingFlag
   {
      MaskingFlag_On                      = 0x00000001,  // 마스킹 ON
      MaskingFlag_TypeMosaic              = 0x00000010,  // 모자이크 타입
      MaskingFlag_TypeBlurring            = 0x00000020,  // 블러링 타입
      MaskingFlag_HumanAreaWholeBody      = 0x00000100,  // 사람의 전신 영역 마스킹
      MaskingFlag_HumanAreaHead           = 0x00000200,  // 사람의 머리 영역 마스킹
      MaskingFlag_HumanAreaFace           = 0x00000400,  // 사람의 얼굴 영역 마스킹

      MaskingFlag_None                    = 0x00000000,
   };

   DWORD             m_dwMaskingFlags;       // 마스킹 플래그
   int               m_nMaskingMosaicSize;   // 마스킹 모자이크 사이즈
   float             m_fMaskingZoomFactor;   // 마스킹 줌 팩터
};


//#pragma pack ()

﻿#include "StdAfx.h"
#include "version.h"
#include "IntelliVIXDoc.h"
#include "LocalSystem.h"
#include "Camera.h"
#include "LicenseManager.h"
#include "BaseDirManager.h"

#if defined(__linux)
//#include <dlfcn.h>
#endif

/////////////////////////////////////////////////////////////////////////////
//
// Class : CIntelliVIXDoc
//
/////////////////////////////////////////////////////////////////////////////
CIntelliVIXDoc* CIntelliVIXDoc::m_pThis = NULL;

CIntelliVIXDoc::CIntelliVIXDoc()
    : m_vecTableUID( 512, false )
{
   m_pLicenseManager = new CLicenseManager;
}

CIntelliVIXDoc::~CIntelliVIXDoc()
{
   if( m_pLicenseManager ) {
      delete m_pLicenseManager;
      m_pLicenseManager = nullptr;
   }
}

CIntelliVIXDoc* CIntelliVIXDoc::GetInstance()
{
   if( NULL == m_pThis ) {
      m_pThis = new CIntelliVIXDoc;
      m_pThis->Initialize();
   }

   return m_pThis;
}

void CIntelliVIXDoc::FreeInstance()
{
   if( m_pThis ) {
      m_pThis->Uninitialize();
      delete m_pThis;
      m_pThis = NULL;
   }
}

void CIntelliVIXDoc::SetExitProcess( int code )
{
   if( false == m_bDoExit ) {
      m_bDoExit   = true;
      m_nExitCode = code;
   }
}

bool CIntelliVIXDoc::IsExitProcess()
{
   return m_bDoExit;
}

int CIntelliVIXDoc::SetPath()
{
   m_strStartUpDir = BaseDirManager::Get().get_base_dir() + "/";
   //LOGI << "m_strStartUpDir:" << m_strStartUpDir;

   m_strConfigPath = sutil::sformat( "%s%s", m_strStartUpDir.c_str(), "Configurations" );
   //MakeDirectory( m_strConfigPath );

   //   m_strDwellAnalysisDataPath = sutil::sformat( "%s%s", m_strStartUpDir.c_str(), "DwellAnalysisData" );
   //   MakeDirectory( m_strDwellAnalysisDataPath.c_str() );

   //   m_strObjectDetectorDataDirName = sutil::sformat( "%s%s", m_strStartUpDir.c_str(), "ObjectDetectorDataFiles" );
   //   if( false == IsExistDirectory( m_strObjectDetectorDataDirName ) ) {
   //      LOGF << "[ERROR] ObjectDetectorDataFiles directory[" << m_strObjectDetectorDataDirName.c_str() << "] is not Exist !!!";
   //      return 1;
   //   }

   return ( DONE );
}

int CIntelliVIXDoc::GetExitCode()
{
   return m_nExitCode;
}

int CIntelliVIXDoc::Initialize()
{
   m_strStartUpDir = BaseDirManager::Get().get_base_dir() + "/";
   m_strExeDir = BaseDirManager::Get().get_excutable_dir() + "/";
   //LOGI << "m_strStartUpDir:" << m_strStartUpDir;

   m_strConfigPath = sutil::sformat( "%s%s", m_strStartUpDir.c_str(), "Configurations" );
   //MakeDirectory( m_strConfigPath );

   m_strDwellAnalysisDataPath = sutil::sformat( "%s%s", m_strExeDir.c_str(), "DwellAnalysisData" );
   MakeDirectory( m_strDwellAnalysisDataPath.c_str() );

   m_strObjectDetectorDataDirName = sutil::sformat( "%s%s", m_strExeDir.c_str(), "ObjectDetectorDataFiles" );
   if( false == IsExistDirectory( m_strObjectDetectorDataDirName ) ) {
      LOGF << "[ERROR] ObjectDetectorDataFiles directory[" << m_strObjectDetectorDataDirName.c_str() << "] is not Exist !!!";
      return 1;
   }

//   m_hCaffeDLL = dlopen("/usr/local/lib/libcaffe.so", RTLD_LAZY);
//   if( nullptr == m_hCaffeDLL )
//   {
//      SetExitProcess(System_Status_Caffe_Load_Failed);
//      return 2;
//   }
   //g_LGU_CLogger_.LogWriteToFile();
   return ( DONE );
}

int CIntelliVIXDoc::Uninitialize()
{
//   if( m_hCaffeDLL )
//   {
//      dlclose(m_hCaffeDLL);
//      m_hCaffeDLL = nullptr;
//   }

   return ( DONE );
}

void* CIntelliVIXDoc::GetCaffeHandle()
{
   return m_hCaffeDLL;
}

bool CIntelliVIXDoc::IsSupportGpu()
{
   return false;
}

void CIntelliVIXDoc::UpdateTableUID( CLocalSystem* pLocalSystem )
{
   CCriticalSectionSP co( pLocalSystem->csCameraList );
   std::deque<CCamera*>& Cameras = pLocalSystem->Cameras;

   for( size_t ii = 0; ii < Cameras.size(); ii++ ) {
      CCamera* pCamera = Cameras[ii];
      if( m_vecTableUID[pCamera->UID] ) {
         logi( "EXIST UID!!! --- [%d] \n", pCamera->UID );
         pCamera->UID = AllocateUID();
         logi( "EXIST UID!!!  New Allocate UID --- [%d] \n", pCamera->UID );
      }
      m_vecTableUID[pCamera->UID] = true;
   }
}

int CIntelliVIXDoc::AllocateUID()
{
   for( size_t ii = 0; ii < m_vecTableUID.size(); ii++ ) {
      if( false == m_vecTableUID[ii] ) {
         m_vecTableUID[ii] = true;
         return ii;
      }
   }
   return ( -1 );
}

void CIntelliVIXDoc::FreeUID( int UID )
{
   m_vecTableUID[UID] = false;
}

int CIntelliVIXDoc::LicenseCertifyKey( int& nChannels )
{
   CLicenseManager::Company company = CLicenseManager::Company::NONE;
   return m_pLicenseManager->LicenseCertifyKey( company, nChannels );
}

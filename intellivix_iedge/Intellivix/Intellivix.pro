TARGET = Intellivix
TEMPLATE = app
CONFIG += qtc_runnable console
include( ../env.pri )

INCLUDEPATH += ../BCCL
INCLUDEPATH += ../VACL
INCLUDEPATH += ../Common
INCLUDEPATH += ../Live555
INCLUDEPATH += ../VCM
INCLUDEPATH += ../IVCP

DEPENDPATH += ../BCCL
DEPENDPATH += ../VACL
DEPENDPATH += ../Common
DEPENDPATH += ../Live555
DEPENDPATH += ../VCM
DEPENDPATH += ../IVCP

PRE_TARGETDEPS += $$DESTDIR/libBCCL.a
PRE_TARGETDEPS += $$DESTDIR/libVACL.a
PRE_TARGETDEPS += $$DESTDIR/libCommon.a
PRE_TARGETDEPS += $$DESTDIR/libLive555.a
PRE_TARGETDEPS += $$DESTDIR/libVCM.a
PRE_TARGETDEPS += $$DESTDIR/libIVCP.a

HEADERS = \
   $$PWD/Camera.h \
   $$PWD/CommonDef.h \
   $$PWD/Config_PTZCamera.h \
   $$PWD/Config_VideoStream.h \
   $$PWD/EventGrouping.h \
   $$PWD/IntelliVIX.h \
   $$PWD/IntelliVIXDoc.h \
   $$PWD/LicenseManager.h \
   $$PWD/LocalSystem.h \
   $$PWD/Preset.h \
   $$PWD/StdAfx.h \
   $$PWD/VideoFrame.h \
   $$PWD/VideoSnder.h \
   $$PWD/Websocket.h \
   $$PWD/SinglePTZTrack.h \
   $$PWD/IvcpRtspSender.h \
   $$PWD/AuxVideoDisplayWnd.h \  # win32cl
   $$PWD/RestDefine.h \
   $$PWD/RestUtil.h \
   #$$PWD/RestServer.h \
   #$$PWD/RestLocalSystem.h \
   $$PWD/RestSender.h \
   $$PWD/XmlGenerator.h \
   $$PWD/MetaGenerator.h \
   $$PWD/JsonGenerator.h \
   $$PWD/RtspParameter.h \

SOURCES = \
   $$PWD/Camera.cpp \
   $$PWD/CommonDef.cpp \
   $$PWD/Config_PTZCamera.cpp \
   $$PWD/Config_VideoStream.cpp \
   $$PWD/EventGrouping.cpp \
   $$PWD/IntelliVIX.cpp \
   $$PWD/IntelliVIXDoc.cpp \
   $$PWD/LicenseManager.cpp \
   $$PWD/LocalSystem.cpp \
   $$PWD/Preset.cpp \
   $$PWD/StdAfx.cpp \
   $$PWD/VideoFrame.cpp \
   $$PWD/VideoSnder.cpp \
   $$PWD/Websocket.cpp \
   $$PWD/SinglePTZTrack.cpp \
   $$PWD/IvcpRtspSender.cpp \   
   $$PWD/AuxVideoDisplayWnd.cpp \  # win32cl
   $$PWD/RestUtil.cpp \
   #$$PWD/RestServer.cpp \
   #$$PWD/RestLocalSystem.cpp \
   $$PWD/RestSender.cpp \
   $$PWD/XmlGenerator.cpp \
   $$PWD/MetaGenerator.cpp \
   $$PWD/JsonGenerator.cpp \
   $$PWD/RtspParameter.cpp \

OTHER_FILES += \
   grpc/intellivix_server.h \
   grpc/intellivix_server.cc \


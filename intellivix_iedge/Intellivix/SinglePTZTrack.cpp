﻿#include "StdAfx.h"
#include "SinglePTZTrack.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


BOOL g_bDebugSPTZTrack             = TRUE;
BOOL g_bDebugSinglePTZTracking     = FALSE;
float g_fTrackObjLostConfirmPeriod = 1500;
GlobalSinglePTZTrackSetup g_SPTZTrkSetup;

////////////////////////////////////////////////////////////////////////
//
// class GlobalSinglePTZTrackSetup
//
////////////////////////////////////////////////////////////////////////


GlobalSinglePTZTrackSetup::GlobalSinglePTZTrackSetup()
{
   m_fContPTZTrkSpeedRatio            = 0.40f;
   m_fPTZTrackObjectStoppingCheckTime = 6.0f;
   m_fMinSimiarityScore               = 0.25f;
   m_fZoomOutLogValueWhenLostObj      = 1.0f;
   m_fMinTiltAngle                    = -70.0f;
   m_fMaxTiltAngle                    = 95.0f;
}

int GlobalSinglePTZTrackSetup::Read( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "GlobalSinglePTZTrackSetup" ) ) {
      xmlNode.Attribute( TYPE_ID( float ), "PTZTrackObjectStoppingCheckTime", &m_fPTZTrackObjectStoppingCheckTime );
      xmlNode.Attribute( TYPE_ID( float ), "MinTiltAngle", &m_fMinTiltAngle );
      xmlNode.Attribute( TYPE_ID( float ), "MaxTiltAngle", &m_fMaxTiltAngle );
      xmlNode.Attribute( TYPE_ID( float ), "ContPTZTrkSpeedRatio", &m_fContPTZTrkSpeedRatio );
      xmlNode.Attribute( TYPE_ID( float ), "MinSimiarityScore", &m_fMinSimiarityScore );
      xmlNode.Attribute( TYPE_ID( float ), "ZoomOutLogValueWhenLostObj", &m_fZoomOutLogValueWhenLostObj );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int GlobalSinglePTZTrackSetup::Write( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "GlobalSinglePTZTrackSetup" ) ) {
      xmlNode.Attribute( TYPE_ID( float ), "PTZTrackObjectStoppingCheckTime", &m_fPTZTrackObjectStoppingCheckTime );
      xmlNode.Attribute( TYPE_ID( float ), "MinTiltAngle", &m_fMinTiltAngle );
      xmlNode.Attribute( TYPE_ID( float ), "MaxTiltAngle", &m_fMaxTiltAngle );
      xmlNode.Attribute( TYPE_ID( float ), "ContPTZTrkSpeedRatio", &m_fContPTZTrkSpeedRatio );
      xmlNode.Attribute( TYPE_ID( float ), "MinSimiarityScore", &m_fMinSimiarityScore );
      xmlNode.Attribute( TYPE_ID( float ), "ZoomOutLogValueWhenLostObj", &m_fZoomOutLogValueWhenLostObj );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

////////////////////////////////////////////////////////////////////////
//
// class SinglePTZTrackSetup
//
////////////////////////////////////////////////////////////////////////

SinglePTZTrackSetup::SinglePTZTrackSetup()
{
   Options               = 0;
   SPTZTrkType           = 0;
   PanTiltCtrlMode       = SPTZTrk_PTCtrlMode_ContMove;
   ZoomCtrlMode          = SPTZTrk_ZoomCtrlMove_NotMoving;
   MaxTrackingTime       = 20.0f;
   ObjectMagnifyingRatio = 0.3f;
   ViewingPos( 0.5f, 0.45f );
}

BOOL SinglePTZTrackSetup::operator!=( SinglePTZTrackSetup& Setup )
{
   BOOL bRet = TRUE;

   if( SPTZTrkType == Setup.SPTZTrkType && Options == Setup.Options && PanTiltCtrlMode == Setup.PanTiltCtrlMode && ZoomCtrlMode == Setup.ZoomCtrlMode && MaxTrackingTime == Setup.MaxTrackingTime && ObjectMagnifyingRatio == Setup.ObjectMagnifyingRatio && ViewingPos == Setup.ViewingPos ) {
      bRet = FALSE;
   }

   return bRet;
}

int SinglePTZTrackSetup::ReadFile( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "SPTZTrkSetup" ) ) {
      xmlNode.Attribute( TYPE_ID( xint ), "Options", &Options );
      xmlNode.Attribute( TYPE_ID( int ), "SPTZTrkType", &SPTZTrkType );
      xmlNode.Attribute( TYPE_ID( int ), "PanTiltCtrlMode", &PanTiltCtrlMode );
      xmlNode.Attribute( TYPE_ID( int ), "ZoomCtrlMode", &ZoomCtrlMode );
      xmlNode.Attribute( TYPE_ID( float ), "MaxTrackingTime", &MaxTrackingTime );
      MaxTrackingTime = 200;
      xmlNode.Attribute( TYPE_ID( float ), "ObjectMagnifyingRatio", &ObjectMagnifyingRatio );
      xmlNode.Attribute( TYPE_ID( FPoint2D ), "ViewingPos", &ViewingPos );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int SinglePTZTrackSetup::WriteFile( CXMLIO* pIO )

{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "SPTZTrkSetup" ) ) {
      xmlNode.Attribute( TYPE_ID( xint ), "Options", &Options );
      xmlNode.Attribute( TYPE_ID( int ), "SPTZTrkType", &SPTZTrkType );
      xmlNode.Attribute( TYPE_ID( int ), "PanTiltCtrlMode", &PanTiltCtrlMode );
      xmlNode.Attribute( TYPE_ID( int ), "ZoomCtrlMode", &ZoomCtrlMode );
      xmlNode.Attribute( TYPE_ID( float ), "MaxTrackingTime", &MaxTrackingTime );
      xmlNode.Attribute( TYPE_ID( float ), "ObjectMagnifyingRatio", &ObjectMagnifyingRatio );
      xmlNode.Attribute( TYPE_ID( FPoint2D ), "ViewingPos", &ViewingPos );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

BOOL SinglePTZTrackSetup::IsTheSame( SinglePTZTrackSetup* pFrom )
{
   FileIO fioThis, fioFrom;
   CXMLIO xmlThis( &fioThis, XMLIO_Write );
   CXMLIO xmlFrom( &fioFrom, XMLIO_Write );
   WriteFile( &xmlThis );
   pFrom->WriteFile( &xmlFrom );
   return !fioThis.IsDiff( fioFrom );
}

////////////////////////////////////////////////////////////////////////
//
// class ContPTZTrkObj
//
////////////////////////////////////////////////////////////////////////

ContPTZTrkObj::ContPTZTrkObj()
{
}

void ContPTZTrkObj::Initialize( IBox2D b2TrkBox, GImage& srcImage, CSinglePTZTrack* pTracker )
{
   m_ObjVelocity    = FPoint2D( 0.0f, 0.0f );
   m_AvgObjVelocity = m_ObjVelocity;
   m_b2TrkBox       = b2TrkBox;
   m_b2PrevTrkBox   = m_b2TrkBox;
   m_p2TrkPos       = m_b2TrkBox.GetCenterPosition();
   m_p2PrevTrkPos   = m_p2TrkPos;
   m_pTracker       = pTracker;
   m_pPTZCtrl       = m_pTracker->m_pPTZCtrl;
   m_fFrameRate     = m_pTracker->m_fFrameRate;

   TargetTracker.Close();
   if( b2TrkBox.X > 0 )
      TargetTracker.Initialize( srcImage, b2TrkBox, pTracker->m_fFrameRate );
   logd( "[TargetTracker][b2TrkBox][%dx%d][%dx%d]\n", b2TrkBox.Width, b2TrkBox.Height, b2TrkBox.X, b2TrkBox.Y );
}

void ContPTZTrkObj::Close()
{
   TargetTracker.Close();
}

float ContPTZTrkObj::Perform( GImage& srcImage )
{
   CPerformanceCounter m_PCounter;
   m_PCounter.CheckTimeStart();

   float fMatchingScore = TargetTracker.Perform( srcImage );

   double elapsed_time = m_PCounter.GetElapsedTime();
   //logd("[TargetTracker::Perform] %f\n", elapsed_time);

   FPTZVector pvCurPTZPos;
   m_pPTZCtrl->GetAbsPos( pvCurPTZPos );
   m_b2TrkBox               = TargetTracker.TargetRegion;
   m_p2TrkPos               = m_b2TrkBox.GetCenterPosition();
   m_p2PrevTrkPos           = m_b2PrevTrkBox.GetCenterPosition();
   m_b2PrevTrkBox           = m_b2TrkBox;
   m_ObjVelocity            = ( m_p2TrkPos - m_p2PrevTrkPos ) * m_fFrameRate;
   m_p2ImgCenterToObjCenter = m_p2TrkPos - m_pTracker->m_p2CenterPos;

   float avgRatio = ( 1.0f / m_fFrameRate ) * 0.5f;
   if( m_AvgObjVelocity.X == 0.0f && m_AvgObjVelocity.Y == 0.0f )
      m_AvgObjVelocity = m_ObjVelocity;
   else
      m_AvgObjVelocity = m_AvgObjVelocity * ( 1.0f - avgRatio ) + m_ObjVelocity * avgRatio;

   m_p2CurPTZVelocity = CalcPTZVelocity();

   float fCenterRange             = 0.1f;
   float fMagImgCenterToObjCenter = Mag( m_p2ImgCenterToObjCenter );
   if( !( m_pTracker->m_nState & SPTZTrkState_MoveTrkObjToCenter ) ) {
      fCenterRange = 0.25f;
      if( fMagImgCenterToObjCenter >= m_pTracker->m_p2CenterPos.Y * fCenterRange )
         m_pTracker->m_nState |= SPTZTrkState_MoveTrkObjToCenter;
   } else {
      fCenterRange = 0.10f;
      if( fMagImgCenterToObjCenter < m_pTracker->m_p2CenterPos.Y * fCenterRange )
         m_pTracker->m_nState &= ~SPTZTrkState_MoveTrkObjToCenter;
   }
   if( fMagImgCenterToObjCenter < m_pTracker->m_p2CenterPos.Y * fCenterRange )
      m_p2CurPTZVelocity *= 0.0f;
   m_p2CurPTZVelocity.X *= g_SPTZTrkSetup.m_fContPTZTrkSpeedRatio * m_pTracker->m_fContPanMoveSpeedRatio;
   m_p2CurPTZVelocity.Y *= g_SPTZTrkSetup.m_fContPTZTrkSpeedRatio * m_pTracker->m_fContTiltMoveSpeedRatio;
   m_pPTZCtrl->GetOffsetAngle( m_pTracker->m_s2SrcImgSize.Width, m_pTracker->m_s2SrcImgSize.Height, m_p2CurPTZVelocity.X, m_p2CurPTZVelocity.Y, m_CurPanTiltAngleVelocity.P, m_CurPanTiltAngleVelocity.T );

   return fMatchingScore;
}

FPoint2D ContPTZTrkObj::CalcPTZVelocity()
{
   ISize2D& s2SrcImgSize = m_pTracker->m_s2SrcImgSize;
   FPoint2D p2CenterPos  = m_pTracker->m_p2CenterPos;
   IBox2D b2TrkBox       = m_pTracker->GetTrkBox();
   float zf              = m_pTracker->m_pPTZCtrl->GetCurrZoom();
   FPoint2D p2PTZVelocity;
   // vA : Object Velocity Vector
   // vB : Center --> Obj;
   float fANorm, fBNorm;
   float fABDot;
   float fAonB_PS; // Projection Scalar A onto B
   FPoint2D vA, vB, vnA, vnB;
   static FPoint2D vZero( 0.0f, 0.0f );
   vA     = m_AvgObjVelocity;
   vB     = m_p2ImgCenterToObjCenter;
   fANorm = Mag( vA );
   fBNorm = Mag( vB );
   if( fANorm != 0.0f )
      vnA = vA / fANorm;
   else
      vnA = vZero;
   if( fBNorm != 0.0f )
      vnB = vB / fBNorm;
   else
      vnB = vZero;
   fABDot = IP( vA, vB );
   if( fBNorm != 0.0f )
      fAonB_PS = fABDot / fBNorm;
   else
      fAonB_PS = 0.0f;
   if( fBNorm < p2CenterPos.Y * 0.25f )
      vB *= 0.0f;
   else if( fBNorm > p2CenterPos.Y * 0.35f )
      vB = vnB * ( p2CenterPos.Y * 0.7f );
   BOOL bConvergeToCenter = FALSE;
   if( fAonB_PS < 0.0f )
      bConvergeToCenter = TRUE;
   float fRatio_vA = m_pTracker->m_fRealObjSpeedFactor;
   fRatio_vA       = 0.1f;
   p2PTZVelocity   = ( vA * fRatio_vA + vB * ( 1.0f - fRatio_vA ) ) * 1.8f;
   return p2PTZVelocity;
}

void ContPTZTrkObj::GetPanTiltAngleVelocity( float& fPanAngleVel, float& fTiltAngleVel )
{
   fPanAngleVel  = m_CurPanTiltAngleVelocity.P;
   fTiltAngleVel = m_CurPanTiltAngleVelocity.T;
}

FPoint2D ContPTZTrkObj::GetGammmaCorrectionVelocity( FPoint2D p2InputVelocity, float fGammaValue )
{
   FPoint2D p2OutputVelocity;

   float fXV          = fabs( p2InputVelocity.X );
   float fXM          = m_pTracker->m_p2CenterPos.X;
   p2OutputVelocity.X = pow( fXV, fGammaValue ) / pow( fXM, fGammaValue ) * fXM;
   if( p2InputVelocity.X < 0.0f )
      p2OutputVelocity.X *= -1.0f;

   float fYV          = fabs( p2InputVelocity.Y );
   float fYM          = m_pTracker->m_p2CenterPos.Y;
   p2OutputVelocity.Y = pow( fYV, fGammaValue ) / pow( fYM, fGammaValue ) * fYM;
   if( p2InputVelocity.Y < 0.0f )
      p2OutputVelocity.Y *= -1.0f;

   return p2OutputVelocity;
}

////////////////////////////////////////////////////////////////////////
//
// class CSinglePTZTrack
//
////////////////////////////////////////////////////////////////////////

CSinglePTZTrack::CSinglePTZTrack()
{
   m_nState = 0;

   m_pPTZCtrl    = NULL;
   m_nChannelNo  = -1;
   m_pCurrTrkObj = NULL;

   m_nSPTZTrk_PTCtrlMode       = SPTZTrk_PTCtrlMode_ContMove;
   m_bSPTZTrkInitialized       = FALSE;
   m_bSPTZTrkLostVideoAnalyics = FALSE;

   m_fContZoomSpeed_Fast            = 1.0f;
   m_fContZoomSpeed_Slow            = 1.0f;
   m_fZoomLogOffsetForSlowZoomSpeed = 0.55f;
   m_fContPanMoveSpeedRatio         = 1.0f;
   m_fContTiltMoveSpeedRatio        = 1.0f;
   m_fRealObjSpeedFactor            = 0.75f;
#if defined( __WIN32 )
   m_pImageView   = NULL;
   m_pcsImageView = NULL;
#endif
}

CSinglePTZTrack::~CSinglePTZTrack()
{
   Close();
}

void CSinglePTZTrack::InitPTZCamParams()
{

   int nPTZControlType               = m_pPTZCtrl->GetPTZControlTypeOfPTZCmd();
   switch( nPTZControlType ) {

   case PTZControlType_FSNetworks_FS_IR307H:
      m_fContPanMoveSpeedRatio          = 1.5f;
      m_fContTiltMoveSpeedRatio         = 1.5f;
      m_fContZoomSpeed_Fast             = 1.0f;
      m_fContZoomSpeed_Slow             = 1.0f;
      m_fZoomLogOffsetForSlowZoomSpeed  = 0.5f;
      m_fMaxZoomLogOffsetForConvergence = 0.3f;
      break;
   }
}

void CSinglePTZTrack::Create( CPTZCtrl* pPTZCtrl )
{
   m_pPTZCtrl = pPTZCtrl;
}

void CSinglePTZTrack::Close()
{
   m_ContPTZTrkObj.Close();
}

int CSinglePTZTrack::ReadFile( CXMLIO* pIO )
{
   if( m_Setup.ReadFile( pIO ) ) return ( 1 );
   return ( DONE );
}

int CSinglePTZTrack::WriteFile( CXMLIO* pIO )
{
   if( m_Setup.WriteFile( pIO ) ) return ( 1 );
   return ( DONE );
}

void CSinglePTZTrack::Initialize( int nChNo, GImage& sg_image, ObjectTracking& objectTracker, CCriticalSection& csObjectTracker, TrackedObjectEx* pObj, float fFrameRate )
{
   if( m_bSPTZTrkInitialized )
      Uninitialize();
#ifdef __DEBUG
   _DebugView_TGT = m_pImageView;
#endif
   m_nChannelNo = nChNo;
   m_fFrameRate = fFrameRate;
   ISize2D srcImgSize( sg_image.Width, sg_image.Height );

   m_pObjectTracker   = &objectTracker;
   m_pcsObjectTracker = &csObjectTracker;
   m_pCurrTrkObj      = pObj;
   m_pCurrTrkObj->GetMeanVelocity( (int)m_fFrameRate, m_InitObjVelocity );

   InvalidateAllObjectAndEvent();

   m_s2SrcImgSize( srcImgSize.Width, srcImgSize.Height );
   m_p2CenterPos( srcImgSize.Width * 0.5f, srcImgSize.Height * 0.5f );
   m_s2OrgSrcImgSize = m_pPTZCtrl->GetVideoSize();

   m_pPTZCtrl->SetVideoSize( ISize2D( srcImgSize.Width, srcImgSize.Height ) );
   m_pPTZCtrl->GetAbsPos( m_CurrPTZPos.P, m_CurrPTZPos.T, m_CurrPTZPos.Z );
   m_pPTZCtrl->SetContPTZTrackMode( FALSE );

   m_nCurrTime = GetTickCount();
   m_nPrevTime = m_nCurrTime;

   m_nState = 0;
   m_nState |= SPTZTrkState_TrackBegun | SPTZTrkState_MoveTrkObjToCenter;
   m_nPrevState = m_nState;

   InitPTZCamParams();
   m_fTgtZoom        = m_CurrPTZPos.Z;
   m_fInitZoom       = m_CurrPTZPos.Z;
   m_nTrackStartTime = GetTickCount();

   if( m_pPTZCtrl->IsRealTimeGetAbsPosSupported() )
      m_bSPTZTrkInitialized = TRUE;
   //logd("[CH:%d][SPTZ] CSinglePTZTrack::Initialize OK \n", m_nChannelNo+1);

   m_nZoomMoveCount               = 0;
   m_nTrackObjIntantlyLostTime    = 0;
   m_nFindingTrackObjectStartTime = 0;
   m_nZoomStartTime               = GetTickCount() - 10000;
}

int CSinglePTZTrack::Uninitialize()
{
   if( m_pPTZCtrl )
      m_pPTZCtrl->SetVideoSize( m_s2OrgSrcImgSize );
   if( m_bSPTZTrkInitialized ) {
      m_bSPTZTrkInitialized = FALSE;
      //logd("[CH:%d][SPTZ] CSinglePTZTrack::Uninitialize OK \n", m_nChannelNo+1);
   }
   return ( DONE );
}

IBox2D CSinglePTZTrack::GetTrkBox()
{
   return m_ContPTZTrkObj.m_b2TrkBox;
}

BOOL CSinglePTZTrack::IsDrawTrkObj()
{
   if( GetState() & SPTZTrkState_TrackOngoing )
      return TRUE;
   return FALSE;
}

void CSinglePTZTrack::ModifyState( uint nRemove, uint nAdd )
{
   m_nPrevState = m_nState;
   m_nState &= ~nRemove;
   m_nState |= nAdd;
}

BOOL CSinglePTZTrack::CheckStateChange( uint nPrevState, uint nCurState, uint nFlag )
{
   m_nPrevState = nPrevState;
   return ::CheckStateChange( nPrevState, nCurState, nFlag );
}

FPoint2D CSinglePTZTrack::GetTrkPos()
{
   IBox2D b2TrkBox = GetTrkBox();
   FPoint2D center_pos;
   FPoint2D viewing_pos = m_Setup.ViewingPos;
   center_pos.X         = b2TrkBox.X + float( b2TrkBox.Width ) * viewing_pos.X;
   center_pos.Y         = b2TrkBox.Y + float( b2TrkBox.Height ) * viewing_pos.Y;
   return center_pos;
}

void CSinglePTZTrack::Perform( BGRImage& cimgSrc, GImage& gimgSrc )
{
   if( NULL == m_pPTZCtrl )
      return;
   if( FALSE == m_bSPTZTrkInitialized ) {
      m_nState = SPTZTrkState_TrackToBeEnded;
      return;
   }

   if( m_nState & SPTZTrkState_TrackToBeEnded ) {
      if( g_bDebugSPTZTrack ) logd( "[CH:%d][SPTZ] SPTZTrkState_TrackToBeEnded \n", m_nChannelNo + 1 );
      m_nState &= ~( SPTZTrkState_TrackBegun | SPTZTrkState_TrackOngoing );
      m_pPTZCtrl->ContPTZMoveAV( FPTZVector( 0.0f, 0.0f, 0.0f ) );
      m_pPTZCtrl->m_nState &= ~PTZ_State_AutomousPTZTracking;
      if( m_pPTZCtrl->m_pSubPTZControl )
         m_pPTZCtrl->m_pSubPTZControl->m_nState &= ~PTZ_State_AutomousPTZTracking;
   }

   {
      m_nPrevTime             = m_nCurrTime;
      m_nCurrTime             = GetTickCount();
      //float fProcessingPeriod = float( m_nCurrTime - m_nPrevTime ) / 1000.0f;
      //float fFramePeriod      = 1.0f / m_fFrameRate;
      m_fZoomCur              = m_pPTZCtrl->GetCurrZoom();
   }

   ////////////////////////////////////////////////////////////////////////////////////
   // 단일 PTZ 추적 시작
   if( m_nState & SPTZTrkState_TrackBegun ) {
      m_nState &= ~SPTZTrkState_TrackBegun;
      m_nState |= SPTZTrkState_TrackOngoing;
      m_pPTZCtrl->m_nState |= PTZ_State_AutomousPTZTracking;
      if( m_pPTZCtrl->m_pSubPTZControl )
         m_pPTZCtrl->m_pSubPTZControl->m_nState |= PTZ_State_AutomousPTZTracking;
      m_fInitZoomBegun = m_fInitZoom;

      TrackedObjectEx* to = m_pCurrTrkObj;
      IBox2D tob          = ( IBox2D )( *m_pCurrTrkObj );

      float cpx = tob.X + tob.Width * 0.5f;
      float cpy = tob.Y + tob.Height * 0.5f;

      // 추적물체가 밖으로 벗어나 있으면 안쪽영역내로 설정
      if( tob.X < 0 ) tob.X = 0;
      if( tob.Y < 0 ) tob.Y = 0;
      if( tob.X + tob.Width > gimgSrc.Width ) tob.Width = gimgSrc.Width - tob.X;
      if( tob.Y + tob.Height > gimgSrc.Height ) tob.Height = gimgSrc.Height - tob.Y;

      // 물체의 종류가 사람인 경우에는 상단 부분으로 제한한다.
      if( TO_TYPE_HUMAN == to->Type ) {
         tob.Height *= 0.35f;
      }

      // 최초 검출하였을 물체크기의 80%만 취함.
      tob.Width *= 0.80f;
      if( TO_TYPE_HUMAN != to->Type )
         tob.Height *= 0.80f;
      tob.X = cpx - tob.Width * 0.7f;
      if( TO_TYPE_HUMAN != to->Type )
         tob.Y = cpy - tob.Height * 0.7f;

      // 추적물체가 기준 크기보다 큰 경우 크기를 min_tbox_size이하로 조정
      int min_tbox_size = 30;
      if( tob.Width > min_tbox_size ) {
         tob.Width = min_tbox_size;

         // 물체가 진행하던 방향이 있고 물체의 크기가 충분히 크면 물체 박스의 위치를 이동방향 최 전단에 위치시킨다.
         float cpx_  = cpx;
         float x_dir = m_InitObjVelocity.X / fabs( m_InitObjVelocity.X );
         cpx_        = cpx_ + x_dir * to->Width * 0.35f - x_dir * tob.Width * 0.5f;
         tob.X       = cpx_ - min_tbox_size * 0.5f;
      }
      if( tob.Height > min_tbox_size ) {
         tob.Height = min_tbox_size;

         // 물체가 진행하던 방향이 있고 물체의 크기가 충분히 크면 물체 박스의 위치를 이동방향 최 전단에 위치시킨다.
         float cpy_ = cpy;
         if( TO_TYPE_HUMAN != to->Type ) {
            float y_dir = m_InitObjVelocity.Y / fabs( m_InitObjVelocity.Y );
            cpy_        = cpy_ + y_dir * to->Height * 0.35f - y_dir * tob.Height * 0.5f;
         }
         tob.Y = cpy_ - min_tbox_size * 0.5f;
      }

      m_ContPTZTrkObj.Initialize( tob, gimgSrc, this );
      m_AvgObjVelocity = m_InitObjVelocity * 2;
      

      m_nFindingTrackObjectStartTime = GetTickCount();
      if( g_bDebugSPTZTrack ) logd( "[CH:%d][SPTZ] == Single PTZ tracking is begun ==\n", m_nChannelNo + 1 );
   }

   /////////////////////////////////////////////////////////////////////

   if( m_nState & SPTZTrkState_TrackOngoing ) {

      if( FALSE == ( m_nState & SPTZTrkState_TrackObjIntantlyLost ) ) {

         m_PCounter.CheckTimeStart();

         float fMatchingScore = m_ContPTZTrkObj.Perform( gimgSrc );

         double elapsed_time = m_PCounter.GetElapsedTime();
         //logd("[CH:%d][CSinglePTZTrack::Perform] %f\n", m_nChannelNo + 1, elapsed_time);

         if( MC_VSV != m_fPrevMachingScore ) {
            float fMatchingScoreVelocity = fMatchingScore - m_fPrevMachingScore;
            float factor                 = 0.5f;
            m_fAvgMachingScoreVelocity   = m_fAvgMachingScoreVelocity * ( 1.0f - factor ) + fMatchingScoreVelocity * factor;
         }

         float factor               = 0.1f;
         m_AvgObjVelocity           = m_AvgObjVelocity * ( 1.0f - factor ) + m_ContPTZTrkObj.m_ObjVelocity * factor;
         factor                     = 0.5f;
         m_AvgObjVelocity_ShortTerm = m_AvgObjVelocity_ShortTerm * ( 1.0f - factor ) + m_ContPTZTrkObj.m_ObjVelocity * factor;

         float avgObjSpeed           = Mag( m_AvgObjVelocity );
         float avgObjSpeed_ShortTerm = Mag( m_AvgObjVelocity_ShortTerm );

         float ps, ts;
         m_ContPTZTrkObj.GetPanTiltAngleVelocity( ps, ts );
         //logd( "[PTAV:%5.2f,%5.2f][MS: %5.4f] [MSV: %+5.4f] [AOS: %9.6f] [AOS_S: %9.4f]\n", ps, ts, fMatchingScore, m_fAvgMachingScoreVelocity, avgObjSpeed, avgObjSpeed_ShortTerm );

         m_fPrevMachingScore = fMatchingScore;

         m_nState &= ~SPTZTrkState_TrackToBeEnded;
         m_nState &= ~SPTZTrkState_TrackObjLost;

         
         if( m_nState & SPTZTrkState_TrackOngoing ) {
            
            if( fMatchingScore < 0.15f ) {
               m_nState |= SPTZTrkState_TrackToBeEnded;
               m_pPTZCtrl->ContPTZMoveAV( FPTZVector( 0.0f, 0.0f, 0.0f ) );
               if( g_bDebugSPTZTrack ) logw( "[CH:%d][SPTZ] SPTZ Tracking to be ended!! : Too low matching score:%f\n", m_nChannelNo + 1, fMatchingScore );
            }
            else if( m_fAvgMachingScoreVelocity < -0.40f ) {
               m_nState |= SPTZTrkState_TrackToBeEnded;
               m_nState |= SPTZTrkState_TrackObjLost;
               m_pPTZCtrl->ContPTZMoveAV( FPTZVector( 0.0f, 0.0f, 0.0f ) );
               if( g_bDebugSPTZTrack ) logw( "[CH:%d][SPTZ] SPTZ Tracking to be ended!! : Abruptly decreasing matching score:%f\n", m_nChannelNo + 1, m_fAvgMachingScoreVelocity );
            }
            
            // 속도가 너무 낮으면 물체 놓침으로 판단한다.
            float LimitVelocity = 0.25f;
            if( avgObjSpeed < LimitVelocity ) {
               if( g_bDebugSPTZTrack ) logw( "[CH:%d][SPTZ] SPTZ Tracking to be ended!! : Object Not Moving :%f\n", m_nChannelNo + 1, m_fAvgMachingScoreVelocity );
               m_nState |= SPTZTrkState_TrackToBeEnded;
               m_nState |= SPTZTrkState_TrackObjLost;
               m_pPTZCtrl->ContPTZMoveAV( FPTZVector( 0.0f, 0.0f, 0.0f ) );
            }
            // 속도가 너무 높으면 물체 놓침으로 판단한다.
            else if( avgObjSpeed_ShortTerm > 500.0f ) {
               if( g_bDebugSPTZTrack ) logw( "[CH:%d][SPTZ] SPTZ Tracking to be ended!! : Object Not Moving :%f\n", m_nChannelNo + 1, m_fAvgMachingScoreVelocity );
               m_nState |= SPTZTrkState_TrackToBeEnded;
               m_nState |= SPTZTrkState_TrackObjLost;
               m_pPTZCtrl->ContPTZMoveAV( FPTZVector( 0.0f, 0.0f, 0.0f ) );
            }     
         }
      }

      float fZoomSpeed = 0.0f;

      if( m_Setup.ZoomCtrlMode == SPTZTrk_ZoomCtrlMove_Magnifying && !( m_nState & ( SPTZTrkState_TrackObjLost | SPTZTrkState_TrackObjIntantlyLost ) ) ) {
         if( !( m_nState & ( SPTZTrkState_ZoomToBeStarted | SPTZTrkState_ZoomBegun | SPTZTrkState_ZoomOngoing ) ) )
            DetermineZoomTracking();
         if( m_nState & SPTZTrkState_ZoomToBeStarted ) {
            if( m_nZoomStartTime && ( m_nCurrTime - m_nZoomStartTime > 6000 ) ) {
               ModifyState( SPTZTrkState_ZoomToBeStarted, SPTZTrkState_ZoomBegun );
               if( g_bDebugSPTZTrack ) logd( "[CH:%d][SPTZ] Zooming to be started    CurZ:%f\n", m_nChannelNo + 1, m_fZoomCur );
            }
         }
         if( m_nState & SPTZTrkState_ZoomBegun ) {
            m_nZoomMoveCount++;
            ModifyState( SPTZTrkState_ZoomBegun, SPTZTrkState_ZoomOngoing );
            m_nZoomStartTime = m_nCurrTime;
            m_fInitZoomBegun = m_fZoomCur;
            m_fTgtZoom       = GetTargetZoom();
            m_pPTZCtrl->SetTgtAbsPos( FPTZVector( PREV_CMD, PREV_CMD, m_fTgtZoom ) );
            if( g_bDebugSPTZTrack ) logd( "[CH:%d][SPTZ] Zooming is started    CurZ:%6.2f  TgtZ:%6.2f\n", m_nChannelNo + 1, m_fZoomCur, m_fTgtZoom );
         }
         if( m_nState & SPTZTrkState_ZoomOngoing ) {
            fZoomSpeed = GetZoomSpeed();
            if( 0.0f == fZoomSpeed ) {
               ModifyState( SPTZTrkState_ZoomOngoing, 0 );
               if( g_bDebugSPTZTrack ) logd( "[CH:%d][SPTZ] Zooming is   ended     CurZ:%6.2f  TgtZ:%6.2f\n", m_nChannelNo + 1, m_fZoomCur, m_fTgtZoom );
            }
         }
      }

      TargetLostCheckProc();

      if( FALSE == ( m_nState & SPTZTrkState_TrackObjIntantlyLost ) ) {
         m_ContPTZTrkObj.GetPanTiltAngleVelocity( m_pvPTVel.P, m_pvPTVel.T );
         FPTZVector move_vec( m_pvPTVel.P, m_pvPTVel.T, fZoomSpeed );
         m_pPTZCtrl->ContPTZMoveAV( move_vec, FALSE );
      }

      if( m_nState & SPTZTrkState_TrackObjIntantlyLost ) {
         if( CheckStateChange( m_nPrevState, m_nState, uint( SPTZTrkState_TrackObjIntantlyLost ) ) ) {
            if( g_bDebugSPTZTrack ) logd( "[CH:%d][SPTZ] The tracking object is instantly lost!!\n", m_nChannelNo + 1 );
         }

         m_pPTZCtrl->ContPTZMoveAV( FPTZVector( 0.0f, 0.0f, 0.0f ) );
         ModifyState( SPTZTrkState_ZoomBegun | SPTZTrkState_ZoomOngoing, 0 );

         if( !m_nTrackObjIntantlyLostTime ) {
            m_nTrackObjIntantlyLostTime = GetTickCount();
         }

         m_nState |= SPTZTrkState_TrackToBeEnded;
      } else if( m_ContPTZTrkObj.TargetTracker.Status & TT_STATUS_TARGET_OUT_OF_RANGE ) {
         ModifyState( 0, SPTZTrkState_TrackObjIntantlyLost );
         if( g_bDebugSPTZTrack ) logd( "[CH:%d][SPTZ] TT_STATUS_TARGET_OUT_OF_RANGE\n", m_nChannelNo + 1 );
      } else if( m_ContPTZTrkObj.TargetTracker.TargetLostTime > 5.0f ) {
         ModifyState( 0, SPTZTrkState_TrackObjIntantlyLost );
         if( g_bDebugSPTZTrack ) logd( "[CH:%d][SPTZ] TargetLostTime > 5.0f\n", m_nChannelNo + 1 );
      }

      uint nMaxTrackingPeriod = uint( m_Setup.MaxTrackingTime * 1000 );
      if( m_nCurrTime - m_nTrackStartTime > nMaxTrackingPeriod ) {
         m_nState |= SPTZTrkState_TrackToBeEnded;
         m_pPTZCtrl->ContPTZMoveAV( FPTZVector( 0.0f, 0.0f, 0.0f ) );

         if( g_bDebugSPTZTrack ) logd( "[CH:%d][SPTZ] Single PTZ tracking to be ended : Time Elapsed!\n", m_nChannelNo + 1 );
      }
   }
   DrawTrackState( cimgSrc, gimgSrc );
}

void CSinglePTZTrack::InvalidateAllObjectAndEvent()
{
   m_pcsObjectTracker->Lock();

   int i;
   for( i = 0; i < 2; i++ ) {
      TrackedObjectEx* t_object;
      if( i == 0 ) t_object = (TrackedObjectEx*)m_pObjectTracker->TrackedObjects.First();
      if( i == 1 ) t_object = (TrackedObjectEx*)m_pObjectTracker->LostObjects.First();
      while( t_object ) {
         if( t_object->Status & TO_STATUS_VERIFIED ) {
            t_object->Status |= TO_STATUS_TO_BE_REMOVED;
         }
         t_object = (TrackedObjectEx*)t_object->Next;
      }
   }
   m_pcsObjectTracker->Unlock();
}

void CSinglePTZTrack::DetermineZoomTracking()
{
   float fCurZoom     = m_fZoomCur;
   float fTgtZoom     = fCurZoom;
   fTgtZoom           = GetTargetZoom();
   float fCurZoomLog  = log( fCurZoom ) * MC_LOG2_INV;
   float fTgtZoomLog  = log( fTgtZoom ) * MC_LOG2_INV;
   float fZoomLogDiff = fabs( fTgtZoomLog - fCurZoomLog );
   if( 0 == m_nZoomMoveCount && fZoomLogDiff > 0.25f ) {
      m_fTgtZoom = fTgtZoom;
      m_nState |= SPTZTrkState_ZoomToBeStarted;
   } else if( fZoomLogDiff > m_fMaxZoomLogOffsetForConvergence ) {
      m_fTgtZoom = fTgtZoom;
      m_nState |= SPTZTrkState_ZoomToBeStarted;
   }
}

float CSinglePTZTrack::GetTargetZoom()
{
   float fTgtZoom               = 1.0f;
   float fObjectMagnifyingRatio = m_Setup.ObjectMagnifyingRatio;
   float fTgtDeltaZoomX         = ( float( m_s2SrcImgSize.Height ) * fObjectMagnifyingRatio ) / float( m_ContPTZTrkObj.m_b2TrkBox.Width );
   float fTgtDeltaZoomY         = ( float( m_s2SrcImgSize.Height ) * fObjectMagnifyingRatio ) / float( m_ContPTZTrkObj.m_b2TrkBox.Height );
   float fTgtDeltaZoom          = fTgtDeltaZoomX;
   if( fTgtDeltaZoom > fTgtDeltaZoomY )
      fTgtDeltaZoom = fTgtDeltaZoomY;
   float fMinZoomFactor, fMaxZoomFactor;
   m_pPTZCtrl->GetRange_ZoomFactor( fMinZoomFactor, fMaxZoomFactor );
   fTgtZoom = m_fInitZoomBegun * fTgtDeltaZoom;
   if( fTgtZoom > fMaxZoomFactor ) fTgtZoom = fMaxZoomFactor;
   if( fTgtZoom < fMinZoomFactor ) fTgtZoom = fMinZoomFactor;
   if( fTgtZoom < m_fInitZoom ) fTgtZoom = m_fInitZoom;
   return fTgtZoom;
}

float CSinglePTZTrack::GetZoomSpeed()
{
   float fZoomSpeed = 0.0f;
   {
      float fZoomLogOffset = log( m_fTgtZoom ) * MC_LOG2_INV - log( m_fZoomCur ) * MC_LOG2_INV;
      if( fabs( fZoomLogOffset ) > m_fMaxZoomLogOffsetForConvergence * 0.9 ) {
         float fContZoomSpeed = m_fContZoomSpeed_Fast;
         if( fabs( fZoomLogOffset ) < m_fZoomLogOffsetForSlowZoomSpeed )
            fContZoomSpeed = m_fContZoomSpeed_Slow;
         if( fZoomLogOffset < 0.0f )
            fZoomSpeed = -fContZoomSpeed;
         if( fZoomLogOffset > 0.0f )
            fZoomSpeed = fContZoomSpeed;
      }
   }
   return fZoomSpeed;
}

void CSinglePTZTrack::TargetLostCheckProc()
{
   /*if (1) {
      FPTZVector& pvCurAbsPos = m_pPTZCtrl->GetAbsPos ();
      if (g_SPTZTrkSetup.m_fMinTiltAngle > pvCurAbsPos.T) {
         m_nState |= SPTZTrkState_TrackToBeEnded;
         if(g_bDebugSPTZTrack) logd ("[CH:%d][SPTZ] Toward at sky Area\n");
      }
   }*/
   
   if( m_s2SrcImgSize.Width * m_s2SrcImgSize.Height * 0.33 < m_ContPTZTrkObj.m_b2TrkBox.Width * m_ContPTZTrkObj.m_b2TrkBox.Height ) {
      if( g_bDebugSPTZTrack ) logd( "[CH:%d][SPTZ] Target size is too big\n" );
      m_nState |= SPTZTrkState_TrackToBeEnded;
   }
}
#if defined( __WIN32 )
void CSinglePTZTrack::SetImageView( Win32CL::CImageView* pImageView, std::recursive_mutex* pcsImageView )
{
   m_pImageView   = pImageView;
   m_pcsImageView = pcsImageView;
}
#endif
void CSinglePTZTrack::DrawTrackState( BGRImage& cimgSrc, GImage& gimgSrc )
{
#if defined( __WIN32 )
   // 테스트 출력
   if( m_pcsImageView ) {
      m_pcsImageView->lock();
      if( m_pImageView ) {
         CString str;
         int x, y;
         x              = 0;
         y              = 0;
         Win32CL::CImageView* iv = m_pImageView;
         {
            // 원본 이미지 및 관련 정보 출력
            {
#ifdef __DEBUG
                x = _DVX_TGT;
         y = _DVY_TGT;
#else

                iv->DrawImage( gimgSrc, x, y );
         if( m_nState & SPTZTrkState_TrackObjIntantlyLost ) {
         } else {
            // 추적 박스 그리기
            IBox2D tb = m_ContPTZTrkObj.m_b2TrkBox;
            tb.Y += y;
            iv->DrawRectangle( tb.X, tb.Y, tb.Width, tb.Height, RGB( 255, 0, 0 ) );
            //logd("[SinglePTZTrack][DrawRectangle][%dx%d / %dx%d]\n",tb.Width, tb.Height,tb.X,tb.Y);
            FPoint2D vA = m_ContPTZTrkObj.m_ObjVelocity;
            // vA : Object Velocity Vector
            iv->DrawLine(
                int( x + m_ContPTZTrkObj.m_p2TrkPos.X ),
                int( y + m_ContPTZTrkObj.m_p2TrkPos.Y ),
                int( x + m_ContPTZTrkObj.m_p2TrkPos.X + vA.X ),
                int( y + m_ContPTZTrkObj.m_p2TrkPos.Y + vA.Y ),
                RGB( 255, 50, 50 ), 2 );
            iv->DrawLine(
                int( x + m_ContPTZTrkObj.m_p2TrkPos.X ),
                int( y + m_ContPTZTrkObj.m_p2TrkPos.Y ),
                int( x + m_ContPTZTrkObj.m_p2TrkPos.X + m_InitObjVelocity.X * 8 ),
                int( y + m_ContPTZTrkObj.m_p2TrkPos.Y + m_InitObjVelocity.Y * 8 ),
                RGB( 100, 100, 255 ), 2 );
            iv->DrawLine(
                int( x + int( cimgSrc.Width * 0.5f ) ),
                int( y + int( cimgSrc.Height * 0.5f ) ),
                int( x + int( cimgSrc.Width * 0.5f ) + m_ContPTZTrkObj.m_p2CurPTZVelocity.X ),
                int( y + int( cimgSrc.Height * 0.5f ) + m_ContPTZTrkObj.m_p2CurPTZVelocity.Y ),
                RGB( 0, 255, 0 ), 2 );
         }
         y += cimgSrc.Height;
#endif
      }

#ifndef __DEBUG

      // 물체 이동관련 정보 드로잉.
      {
         int y_prev = y;
         x          = 0;
         iv->DrawFilledRectangle( x, y, cimgSrc.Width, 400, RGB( 0, 0, 0 ) );
         str.Format( "Red:   Object Velocity" );
         iv->DrawText( str, x, y, RGB( 255, 50, 50 ) );
         y += 20;
         str.Format( "Green: Current PTZ Velocity" );
         iv->DrawText( str, x, y, RGB( 0, 255, 0 ) );
         y += 20;
         str.Format( "P:%5.1f  T:%5.1f", m_pvPTVel.P, m_pvPTVel.T );
         iv->DrawText( str, x, y, RGB( 255, 255, 255 ) );
      }
#endif
   }
   m_pImageView->Invalidate( FALSE );
}
m_pcsImageView->unlock();
}
#endif
}

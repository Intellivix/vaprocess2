﻿///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2013 illisis. Inc. All rights reserved.
//
// FileName : IVCP_LiveSender.h
//
// Function: Live Stream & MetaData Sender class for IVCP 3.0.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "IVCP_Define.h"
#include "IVCP_PacketStruct.h"


class CCamera;
class EventEx;
class TrackedObjectEx;
class CRTSPStreamingChannel;
class CRTSPVAMetaData;
class CMetaGenerator;
class CAVCodec;

///////////////////////////////////////////////////////////////////////////////
//
// Class: CIvcpRtspSender
//
///////////////////////////////////////////////////////////////////////////////

class CIvcpRtspSender {
private:
   CCamera* m_pCamera;

   CCriticalSection m_csTNEncoder;
   std::shared_ptr<CAVCodec> m_TNEncoder;

   BOOL m_bEventZoneDataTobSended;
   BOOL m_bCameraInfoDataTobSended;
   BOOL m_bPresetMapDataTobSended;

public:
   CIVCPLiveSettingInfo m_SettingInfo;
   int m_nBLCMode;
   BOOL m_bIPCameraSettingInfoReceivedJustOneTime;

public:
   explicit CIvcpRtspSender( CCamera* pCamera );
   ~CIvcpRtspSender();

public:
   void SetEventZoneDataToBeBroadcasted();
   void SetPresetMapDataToBeBroadcasted();
   void SetCameraInfoDataToBeBroadcasted();

   void SendEventInfo( TrackedObjectEx* t_object, EventEx* pEvent );
   void SendObjectInfo( TrackedObjectEx* t_object );
   void SendFrameInfo( std::deque<TrackedObjectEx*>& trackedObjectList );
   void SendFrmObjInfo( std::deque<TrackedObjectEx*>& trackedObjectList );
   void SendCameraSettingInfo();
   void SendEventZoneInfo( BOOL bNullInfo = FALSE );
   void SendEventZoneCounterInfo();
   void SendCameraInfo();

private:
   void EncodeImage( byte* pSrcBuff_YUY2, int nSrcWidth, int nSrcHeight, BArray1D& comp_buff );

   int GetSettingMask();
   int GetSettingFlag();

   BOOL IsReqRTSPVAMetaData( uint nVAMetaDataFlag );
   void RemoveRTSPVAMetaDataFlag( uint nVAMetaDataFlag );

   bool SendRTSPVAMetaData( CIVCPPacket* pDataPacket, uint nVAMetaDataFlag );
   bool SendRTSPVAMetaData( CMetaGenerator* pGenerator, uint nVAMetaDataFlag );
   bool SendRTSPVAMetaData( FileIO* pFile, uint nVAMetaDataFlag );
};

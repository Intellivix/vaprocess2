﻿#include "StdAfx.h"
#include "RtspParameter.h"
#include "Camera.h"
#include "LocalSystem.h"
#include "IvcpRtspSender.h"

CRtspParameter::CRtspParameter( CCamera* pCamera )
    : m_pCamera( pCamera )
{
   ChannelNo = m_pCamera->ChannelNo;
   UID       = m_pCamera->UID;

   VC             = m_pCamera->VC;
   System         = m_pCamera->System;
   ivcpLiveSender = m_pCamera->m_ivcpRtspSender.get();
}

CRtspParameter::~CRtspParameter()
{
}

void CRtspParameter::OnRTSPSetParameter( char const* fullRequestStr )
{
   //logi("[SET_PARAM] OnRTSPSetParameter : %s \n", fullRequestStr);

   std::string strRequest;
   if( !ParseRTSPRarameter( fullRequestStr, strRequest ) )
      return;

   //------------------------------------------------------------------------
   // Param 의 헤더 분리
   size_t nPos = strRequest.find( ':' );
   if( std::string::npos == nPos )
      return;
   std::string strName = strRequest.substr( 0, nPos );

   if( "CHANNEL_SETTING" == strName )
      OnRTSP_VideoChannelSetting( strRequest );
   else if( "CHANNEL_SETTING_ALL" == strName )
      OnRTSP_CameraSettingAll( strRequest, 1 );
   else if( "ANALYSIS_SETTING" == strName )
      OnRTSP_VideoAnalysisSetting( strRequest );
   else if( "ANALYSIS_SETTING_ALL" == strName )
      OnRTSP_CameraSettingAll( strRequest, 2 );
   else if( "CHANNEL_INFO" == strName )
      OnRTSP_CameraInfo( strRequest );
   else if( "RESET_EVENT_ZONE_COUNT" == strName )
      OnRTSP_ResetEventZoneCount( strRequest );
   else if( "RESET_COUNT_SCHEDULE" == strName )
      OnRTSP_ResetCountSchedule( strRequest );
   else
      logd( "[SET_PARAM] OnRTSPSetParameter   Name[%s]\n", strName.c_str() );

   VC->SendGetParameterCommand( "Success" );
}

void CRtspParameter::OnRTSPGetParameter( char const* fullRequestStr )
{
#if 0
   logd( "[GET_PARAM] OnRTSPGetParameter : %s \n", fullRequestStr );
#endif
}

BOOL CRtspParameter::ParseRTSPRarameter( char const* szInData, std::string& strRequest )
{
   // \r\n 문자 제거
   static const std::string szRN1 = "\r\n";
   static const std::string szRN2 = "\r\n\r\n";

   strRequest = szInData;

   size_t nPos = strRequest.find( szRN2 );
   if( std::string::npos == nPos )
      return FALSE;
   strRequest.erase( nPos, szRN2.length() );

   nPos = strRequest.rfind( szRN1 );
   if( std::string::npos == nPos )
      return FALSE;
   strRequest.erase( nPos, szRN1.length() );

   return TRUE;
}

BOOL CRtspParameter::ParseRTSPXMLValue( const std::string& strRequest, std::string& strValue )
{
   static const std::string szXMLHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";

   size_t nPos = strRequest.find( szXMLHeader );
   if( std::string::npos == nPos )
      return FALSE;

   strValue = strRequest.substr( nPos, strRequest.length() - nPos );

   return TRUE;
}

BOOL CRtspParameter::OnRTSP_VideoChannelSetting( const std::string& strRequest )
{
   std::string strValue;
   if( !ParseRTSPXMLValue( strRequest, strValue ) )
      return FALSE;

   logd( "OnRTSP VideoChannelSetting Start [CameraNo : %d] [UID : %d]\n", ChannelNo, UID );
   System->InitializeWizardSetup( ChannelNo );
   CCamera* TmpCamera = m_pCamera->TmpCamera;
   if( !TmpCamera ) return FALSE;

   FileIO buff;
   buff.Attach( (byte*)( strValue.c_str() ), strValue.length() );
   CXMLIO xmlIO( &buff, XMLIO_Read );
   TmpCamera->ReadVCSetup( &xmlIO, 0 );
   buff.Detach();

   System->FinalizeWizardSetup( TRUE );
   System->SetSystemSetupToBeSaved();

   ivcpLiveSender->SetCameraInfoDataToBeBroadcasted();
   ivcpLiveSender->SetEventZoneDataToBeBroadcasted();
   logd( "OnRTSP VideoChannelSetting End [CameraNo : %d] [UID : %d]\n", ChannelNo, UID );

   return TRUE;
}

BOOL CRtspParameter::OnRTSP_CameraSettingAll( const std::string& strRequest, int nType )
{
   std::string strValue;
   if( !ParseRTSPXMLValue( strRequest, strValue ) )
      return FALSE;

   logd( "OnRTSP OnRTSP_CameraSettingAll Start [CameraNo : %d] [UID : %d]\n", ChannelNo, UID );
   System->InitializeWizardSetup();

   FileIO buff;
   buff.Attach( (byte*)( strValue.c_str() ), strValue.length() );

   System->csCameraList.Lock();
   for( int ii = 0; ii < System->CameraNum; ii++ ) {
      buff.GoToStartPos();
      CXMLIO xmlIO( &buff, XMLIO_Read );
      CCamera* pTempCamera = System->Cameras[ii]->TmpCamera;
      if( 1 == nType ) {
         pTempCamera->ReadVCSetup( &xmlIO, 0 );
      } else {
         CXMLNode xmlNode( &xmlIO );
         if( xmlNode.Start( "OBJECT_DTC" ) ) {
            xmlNode.Attribute( TYPE_ID( int ), "DTCOpt", &pTempCamera->DTCOptions );
            xmlNode.Attribute( TYPE_ID( float ), "ReductionFactor", &pTempCamera->ReductionFactor );
            xmlNode.Attribute( TYPE_ID( float ), "DesiredFPS", &pTempCamera->DesiredFrameRate );
            xmlNode.Attribute( TYPE_ID( int ), "Detect_Type", &pTempCamera->VAMode );
            xmlNode.End();
         }
         //pTempCamera->ReadMainVACSetup(&xmlIO);
      }
   }
   System->csCameraList.Unlock();

   buff.Detach();

   System->FinalizeWizardSetup( TRUE );
   System->SetSystemSetupToBeSaved();

   ivcpLiveSender->SetCameraInfoDataToBeBroadcasted();
   ivcpLiveSender->SetEventZoneDataToBeBroadcasted();
   logd( "OnRTSP OnRTSP_CameraSettingAll End [CameraNo : %d] [UID : %d]\n", ChannelNo, UID );

   return TRUE;
}

BOOL CRtspParameter::OnRTSP_VideoAnalysisSetting( const std::string& strRequest )
{
   std::string strValue;
   if( !ParseRTSPXMLValue( strRequest, strValue ) )
      return FALSE;

   logd( "OnRTSP VideoAnalysisSetting Start [CameraNo : %d] [UID : %d]\n", ChannelNo, UID );
   System->InitializeWizardSetup( ChannelNo );
   CCamera* TmpCamera = m_pCamera->TmpCamera;
   if( !TmpCamera ) return FALSE;

   FileIO buff;
   buff.Attach( (byte*)( strValue.c_str() ), strValue.length() );
   CXMLIO xmlIO( &buff, XMLIO_Read );

   CXMLNode xmlNode( &xmlIO );
   if( xmlNode.Start( "OBJECT_DTC" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "DTCOpt", &TmpCamera->DTCOptions );
      xmlNode.Attribute( TYPE_ID( float ), "ReductionFactor", &TmpCamera->ReductionFactor );
      xmlNode.Attribute( TYPE_ID( float ), "DesiredFPS", &TmpCamera->DesiredFrameRate );
      xmlNode.Attribute( TYPE_ID( int ), "Detect_Type", &TmpCamera->VAMode );
      xmlNode.End();
   }

   try {
      TmpCamera->ReadMainVACSetup( &xmlIO );
   } catch( IVXException ) {
      LOGE << "ReadMainVACSetup";
   }

   buff.Detach();

   System->FinalizeWizardSetup( TRUE );
   System->SetSystemSetupToBeSaved();

   ivcpLiveSender->SetCameraInfoDataToBeBroadcasted();
   ivcpLiveSender->SetEventZoneDataToBeBroadcasted();

   logd( "OnRTSP VideoAnalysisSetting End [CameraNo : %d] [UID : %d]\n", ChannelNo, UID );

   return TRUE;
}

BOOL CRtspParameter::OnRTSP_CameraInfo( const std::string& strRequest )
{
   logd( "OnRTSP CameraInfo [CameraNo : %d] [UID : %d]\n", ChannelNo, UID );

   ivcpLiveSender->SetCameraInfoDataToBeBroadcasted();
   ivcpLiveSender->SetEventZoneDataToBeBroadcasted();

   return TRUE;
}

BOOL CRtspParameter::OnRTSP_ResetEventZoneCount( const std::string& strRequest )
{
   logd( "OnRTSP ResetEventZoneCount [CameraNo : %d] [UID : %d]\n", ChannelNo, UID );
#if 0
   ResetAllObjectCounters();
   System->SetSystemSetupToBeSaved();
#else
   m_pCamera->m_nStateEx2 |= CamStateEx2_ObjectCountersTeBeReset;
#endif

   return TRUE;
}

BOOL CRtspParameter::OnRTSP_ResetCountSchedule( const std::string& strRequest )
{
   logd( "OnRTSP ResetCountSchedule [CameraNo : %d] [UID : %d]\n", ChannelNo, UID );

   std::string strValue;
   if( !ParseRTSPXMLValue( strRequest, strValue ) )
      return FALSE;

   FileIO buff;
   buff.Attach( (byte*)( strValue.c_str() ), strValue.length() );
   CXMLIO xmlIO( &buff, XMLIO_Read );
   CIVCPResetObjectCountPeriod IVCPSchedule;
   IVCPSchedule.ReadXML( &xmlIO );
   buff.Detach();

   m_pCamera->m_bResetObjCountUsed        = IVCPSchedule.m_bUse;
   m_pCamera->m_nResetObjCount_PeriodTime = IVCPSchedule.m_nPeriodTime;

   m_pCamera->m_vecResetObjCount_DayTime.clear();
   for( int ii = 0; ii < (int)IVCPSchedule.m_vecReset.size(); ii++ ) {
      RESET_OBJECT_TIME rdTime;
      rdTime.m_nDay  = IVCPSchedule.m_vecReset[ii].m_nDay;
      rdTime.m_nTime = IVCPSchedule.m_vecReset[ii].m_nTime;
      m_pCamera->m_vecResetObjCount_DayTime.push_back( rdTime );
   }

   System->SetSystemSetupToBeSaved();

   return TRUE;
}

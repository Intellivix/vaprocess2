﻿#pragma once

#include <vacl.h>

// #pragma pack(1)

///////////////////////////////////////////////////////////////////////////////
//
// Defines & Enumerations
//
///////////////////////////////////////////////////////////////////////////////

const int MAX_PRESET_NUM = 256;

enum PresetTourCycleMode {
   PresetTourCycleMode_Foword  = 0,
   PresetTourCycleMode_Reverse = 1,
   PresetTourCycleMode_Return  = 2
};

enum PTZSwingMode {
   PTZSwingMode_SwingByFullSpeed                = 0,
   PTZSwingMode_SwingByConstantAngleSpeed       = 1,
   PTZSwingMode_SwingByConstantImageScrollSpeed = 2,
};

enum PTZTourReturnDirection {
   PTZTourReturnDirection_Prev = 0,
   PTZTourReturnDirection_Next = 1,
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: CDwellTime
//
///////////////////////////////////////////////////////////////////////////////

class CDwellTime {
public:
   int Seconds;

public:
   CDwellTime();

public:
   void operator=( CDwellTime& d_time );
   void GetTime( int& min, int& sec );
   void SetTime( int min, int sec );

public:
   void CheckParams();
   int ReadFile( CXMLIO* pIO );
   int WriteFile( CXMLIO* pIO );
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: CPTZStopCheck
//
///////////////////////////////////////////////////////////////////////////////

class CPTZStopCheck

{
public:
   float PTZMovingTime; // 영상 처리를 이용하지 않는 경우 고정시간 동안 대기한다.

public:
   CPTZStopCheck();

public:
   void CheckParams();
   int ReadFile( CXMLIO* pIO );
   int WriteFile( CXMLIO* pIO );
};

//////////////////////////////////////////////////////////////////////////////
//
// Class: CPTSwingSpeed
//
///////////////////////////////////////////////////////////////////////////////

class CPTSwingSpeed

{
public:
   int SwingMode;
   float AngleSpeed;
   float ImageScrollSpeed;

public:
   CPTSwingSpeed();

public:
   void CheckParams();
   int ReadFile( CXMLIO* pIO );
   int WriteFile( CXMLIO* pIO );
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: CPreset
//
///////////////////////////////////////////////////////////////////////////////

enum PresetOption {
   PresetOption_EnableSinglePTZTracking      = 0x00000001,
   PresetOption_StoppingPresetMovingOnEvent  = 0x00000002,
   PresetOption_VideoAnalyticsOnPresetMoving = 0x00000004,
};

class CPreset

{
public:
   ushort PresetID; // 프리셋 아이디.
   char Name[256]; // 프리셋 이름.
   int Option; // 사용자 지정 옵션
   int PresetNo; // PTZ에 설정되는 프리셋 번호
   CDwellTime DwellTime; // 현재 프리셋의 위치에서 기다리는 시간.
   CPTZStopCheck PTZStopCheck; // PTZ카메라 이동의 멈춤을 체크하기 위한 정보
   CPTSwingSpeed PTZSwingSpeed;
   FileIO EncodedImage;
   BGRImage Image; // 각 프리셋 위치를 대표하는 이미지
   // client 파노라마 동작 적용 // pan, tilt, zoom, focus저장
   FPTZVector PresetPos;

public:
   int VAMode;

   EventDetection Config_EventDetection;
   ForegroundDetection_SGBM Config_ForegroundDetection;
   ObjectClassification_Basic *Config_ObjectClassification;
   ObjectFilteringEx Config_ObjectFiltering;
   ObjectTracking Config_ObjectTracking;
   RealObjectSizeEstimation Config_RealObjectSizeEstimation;
   GroupTracking Config_GroupTracking;
   SmokeDetection Config_SmokeDetection;
   FlameDetection Config_FlameDetection;
   //CDayNightImageSynthesisSetup Config_DNImgSynSetup;
   //FaceDetectionConfig          Config_FaceDetection;
   HeadDetectionConfig Config_HeadDetection;

public:
   CPreset *Prev, *Next;

public:
   CPreset();
   ~CPreset();

public:
   int operator==( CPreset& preset );
   int operator!=( CPreset& preset );

public:
   int CopyFrom( CPreset* preset );

public:
   void CheckParams();
   int ReadFile( CXMLIO* pIO, BOOL bImage = TRUE );
   int WriteFile( CXMLIO* pIO, BOOL bImage = TRUE );
   int ReadVACSetup( CXMLIO* pIO );
   int WriteVACSetup( CXMLIO* pIO );
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: CPresetGroup
//
///////////////////////////////////////////////////////////////////////////////

typedef LinkedList<CPreset> PresetList;

class CPresetGroup : public PresetList

{
public:
   char Name[256];
   int CycleType;

public:
   CPresetGroup *Prev, *Next;

public:
   CPresetGroup();
   virtual ~CPresetGroup();

public:
   void DeleteAll();

public:
   void AddPreset( CPreset* preset );
   void InsertPreset( CPreset* preset );
   int Delete( int idx );
   CPreset* GetAt( int idx );
   int GetIndex( CPreset* preset );

public:
   void CheckParams();
   int ReadFile( CXMLIO* pIO, BOOL bImage = TRUE );
   int WriteFile( CXMLIO* pIO, BOOL bImage = TRUE );

protected:
   short GetNewPresetID();
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: CPresetGroup
//
///////////////////////////////////////////////////////////////////////////////

class CPresetGroupList : public LinkedList<CPresetGroup>

{
public:
   int MinPresetNo;
   int MaxPresetNo;
   int SelectedGroupIndex;
   int TouringWaitTime;
   int HomePosPresetNo;
   BOOL UsePresetNoList[MAX_PRESET_NUM];

public:
   CPresetGroupList();
   virtual ~CPresetGroupList();

public:
   int operator!=( CPresetGroupList& list );

public:
   int CopyFrom( CPresetGroupList& list );
   CPreset* FindPreset( int group_idx, int preset_idx );
   CPreset* FindPreset( int preset_no );
   CPresetGroup* FindPresetGroup( int preset_no );
   int DeletePreset( CPreset* preset );
   CPresetGroup* GetAt( int idx );
   void SetMinMaxPresetNo( int min_preset_no, int max_preset_no );
   int GetAvailablePresetNo();
   void UpdateUsePresetNoList( int curr_preset_no = -1 );
   int GetPresetIndex( CPreset* preset, int& group_idx, int& preset_idx );
   CPresetGroup* GetSelectedGroup();
   int RemoveAt( int idx );
   void DeleteAll();

public:
   void CheckParams();
#ifndef __DSPBIOS
   int ReadFile( CXMLIO* pIO );
   int WriteFile( CXMLIO* pIO );
#endif
};

// #pragma pack()

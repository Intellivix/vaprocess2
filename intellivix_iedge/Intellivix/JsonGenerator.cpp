﻿#include "JsonGenerator.h"
#include "RestDefine.h"
#include "RestUtil.h"
#include "Camera.h"
#include "LocalSystem.h"
#include "IntelliVIXDoc.h"
#include "CommonUtil.h"
#include "BaseDirManager.h"
#include "LGU/Logger.h" // lgu+ log 

using namespace utility::conversions;
using namespace web_rest_util;

CJsonGenerator::CJsonGenerator()
{
}

CJsonGenerator::~CJsonGenerator()
{
}

void CJsonGenerator::InitGenerator() 
{
}

MetaGeneratorType CJsonGenerator::GetType() const
{
   return MetaGeneratorType::JSON;
}

std::string CJsonGenerator::GetString() const
{
   return m_str;
}

bool CJsonGenerator::IsSaveImage()
{
   bool ret = false;
   if( 2 == m_pLocalSystem->m_MetaType)
      ret = true;

   return ret;
}

bool CJsonGenerator::RenderEventObject( CRanderObjectInImage& render, VAResults& results )
{
   std::deque<TrackedObjectEx*> TrackedObjectList;
   for( VaMeta& item : results ) {
      EventEx* event          = item.event;
      TrackedObjectEx* object = item.object;

      if( nullptr == event )
         continue;
      if( !( event->Status & ( EV_STATUS_BEGUN | EV_STATUS_ENDED ) ) )
         continue;

      TrackedObjectList.push_back( object );
   }

   if( TrackedObjectList.empty() )
      return false;

   CRanderObjectInImage::PARAM param;
   param.src_image_width  = m_pCamera->SrcImgSize.Width;
   param.src_image_height = m_pCamera->SrcImgSize.Height;
   param.va_image_width   = m_pCamera->ProcImgSize.Width;
   param.va_image_height  = m_pCamera->ProcImgSize.Height;
   param.yuy2_buffer      = m_pCamera->YUY2SrcImg;
   render.PreparingSourceImage( param );
   render.RenderVaBoxes( TrackedObjectList );

   return true;
}

void CJsonGenerator::FillEventInfo( VAResults& results )
{
   json_value data   = json_value::object( true );
   json_value header = json_value::object( true );
   json_value body   = json_value::object( true );

   header[U( "type" )] = json_value::string( U( "event" ) );
   header[U( "uid" )]  = json_value::number( m_pCamera->UID );
   header[U( "guid" )] = as_json( m_pCamera->CameraGuid );
   
   // snapshot get time
   timeval today;
   struct tm *tm_today;
   gettimeofday(&today, NULL);
   tm_today = localtime(&today.tv_sec);
   std::string ym_string  = sutil::sformat( "%04d%02d", tm_today->tm_year + 1900, tm_today->tm_mon + 1);
   std::string ydt_string = sutil::sformat( "%04d-%02d-%02d_%02d-%02d-%02d-%03d", 
                                            tm_today->tm_year + 1900, tm_today->tm_mon + 1, tm_today->tm_mday, 
                                            tm_today->tm_hour, tm_today->tm_min, tm_today->tm_sec, today.tv_usec );
   std::string non_bar_ydt_string = sutil::sformat( "%04d%02d%02d%02d%02d%02d%03d", 
                                            tm_today->tm_year + 1900, tm_today->tm_mon + 1, tm_today->tm_mday, 
                                            tm_today->tm_hour, tm_today->tm_min, tm_today->tm_sec, today.tv_usec );
   //LOGW << "FileEventInfo Now Time Check Message :  " << ydt_string.c_str();

   bool event_begun = false;
   int index             = 0;
   json_value event_list = json_value::array( results.size() );

   for( VaMeta& item : results ) {
      json_value list_item = json_value::object( true );

      EventEx* event          = item.event;
      TrackedObjectEx* object = item.object;
      
#ifdef __UPLUS_LOGGER
      LGU::Logger::SetHeaderItem( COMMAND, "EVENT" );
      LGU::Logger::SetHeaderItem( FROM_SERVICE_NAME, "VAP" );
      LGU::Logger::SetHeaderItem( RES_SERVICE_NAME, "VAM" );
      LGU::Logger::SetHeaderItem( VA_PROCESS_TERM_TIME );
      LOGI_(L) << LGU::Logger::HeaderEventLog (0, event);
#endif
      
      if( !( event->Status & ( EV_STATUS_BEGUN | EV_STATUS_ENDED ) ) )
         continue;
      
      event_begun = (event->Status & EV_STATUS_BEGUN) ? true : false;

      if( is_exist_flag( event->Status, EV_STATUS_BEGUN ) ) {
         if( IsNullFileTime( event->Time_EventBegun ) )
            AfxGetLocalFileTime(&event->Time_EventBegun);// = m_pCamera->CurFileTime;

         list_item[U( "start" )]       = as_json( event->Time_EventBegun );
         list_item[U( "end" )]         = as_json( event->Time_EventBegun );
         list_item[U( "eventStatus" )] = json_value::number( EVENT_STATUS_BEGUN );
      } else if( is_exist_flag( event->Status, EV_STATUS_ENDED ) ) {
         list_item[U( "start" )]       = as_json( event->Time_EventBegun );
         list_item[U( "end" )]         = as_json( non_bar_ydt_string );
         list_item[U( "eventStatus" )] = json_value::number( EVENT_STATUS_ENDED );
      }

      int event_type              = event->Type;
      if (event_type == 0)       event_type = 1;

      list_item[U( "eventID" )]   = json_value::number( event->ID );
      list_item[U( "eventType" )] = json_value::number( event_type );
      {
         json_value evt_info = json_value::object( true );

         if( ER_EVENT_DWELL == event_type ) {
            evt_info[U( "dwellTime" )] = json_value::number( double(object->Time_Dwell) );
         }

         if( ER_EVENT_COLOR_CHANGE == event_type ) {
            json_value color_info                    = json_value::object( true );
            color_info[U( "centerBackgroundColor" )] = as_json( object->RgnFrgColor );
            color_info[U( "regionBackgroundColor" )] = as_json( object->RgnBkgColor );
            color_info[U( "centerForegroundColor" )] = as_json( object->CntFrgColor );
            color_info[U( "regionForegroundColor" )] = as_json( object->CntBkgColor );
            evt_info[U( "colorChangeInfo" )]         = color_info;
         }

         list_item[U( "eventInfo" )] = evt_info;
      }

      list_item[U( "objectId" )]      = json_value::number( object->ID );
      list_item[U( "objectType" )]    = json_value::number( object->Type );
      list_item[U( "objectBox" )]     = as_json( ( IBox2D )( *object ) );
      
      if(event->EvtZone)
      {
         list_item[U( "eventZoneId" )]   = json_value::number( event->EvtZone->EvtRuleID );   // jun_u+ 
         list_item[U( "eventZoneName" )] = as_json( event->EvtZone->Name );
      }

      json_value va_info = json_value::object( true );
      {
         va_info[U( "width" )]    = json_value::number( m_pCamera->ProcImgSize.Width );
         va_info[U( "height" )]   = json_value::number( m_pCamera->ProcImgSize.Height );
         va_info[U( "fps" )]      = json_value::number( double( m_pCamera->DesiredFrameRate ) );
         list_item[U( "vaInfo" )] = va_info;
      }

      event_list[index++] = list_item;
   }
   CRanderObjectInImage render;
   if( IsSaveImage() && event_begun && RenderEventObject( render, results ) ) {
      std::string image_path_for_json;
      std::string image_path;
      CIntelliVIXDoc* doc = CIntelliVIXDoc::GetInstance();
      if (doc) {
         BaseDirManager& bdm = BaseDirManager::Get();
         const std::string& pid = bdm.get_multil_proces_id();
         char buff[2048];
         
         if (doc->m_SaveMetaInCurrDir) {
            snprintf(buff, sizeof(buff), "EventImage/%s/%s",pid.c_str(), ym_string.c_str());
            image_path_for_json = buff;
            snprintf(buff, sizeof(buff), "%sEventImage/%s/%s",doc->m_strStartUpDir.c_str(), pid.c_str(), ym_string.c_str());
            image_path = buff;
            if( false == IsExistDirectory( image_path ) ) {
               snprintf(buff, sizeof(buff),"%sEventImage", doc->m_strStartUpDir.c_str());
               MakeDirectory(buff);
               snprintf(buff, sizeof(buff),"%sEventImage/%s", doc->m_strStartUpDir.c_str(), pid.c_str()); 
               MakeDirectory(buff);
               snprintf(buff, sizeof(buff),"%sEventImage/%s/%s", doc->m_strStartUpDir.c_str(), pid.c_str(), ym_string.c_str());
               MakeDirectory(buff);
            }
         }
         else  {
            snprintf(buff, sizeof(buff), "EventImage/%s/%s", pid.c_str(), ym_string.c_str());
            image_path_for_json = buff;
            snprintf(buff, sizeof(buff), "%s/EventImage/%s/%s",m_pLocalSystem->m_SaveMetaImagePath.c_str(), pid.c_str(), ym_string.c_str());
            image_path = buff;
            if( false == IsExistDirectory( image_path ) ) {
               snprintf(buff, sizeof(buff), "%s/EventImage", m_pLocalSystem->m_SaveMetaImagePath.c_str());
               MakeDirectory(buff);
               snprintf(buff, sizeof(buff), "%s/EventImage/%s",m_pLocalSystem->m_SaveMetaImagePath.c_str(), pid.c_str()); 
               MakeDirectory(buff);
               snprintf(buff, sizeof(buff), "%s/EventImage/%s/%s",m_pLocalSystem->m_SaveMetaImagePath.c_str(), pid.c_str(), ym_string.c_str());
               MakeDirectory(buff);
            }
         }
      }
   
      std::string file_name = sutil::sformat( "%s/%s.jpg", image_path.c_str(), ydt_string.c_str() );
      std::string file_name_for_json = sutil::sformat( "%s/%s.jpg", image_path_for_json.c_str(), ydt_string.c_str() );
      if( render.SaveJPGFile( file_name ) )
         body[U( "image_path" )] = as_json( file_name_for_json );
      else {
         body[U( "image_path" )] = json_value::null();
         // jun_u+ : 스냅샷 저장 로그 기록
         logw("Event Snapshot Save Error");
      }
   } else
      body[U( "image_path" )] = json_value::null();

   body[U( "list" )] = event_list;

   data[U( "header" )] = header;
   data[U( "body" )]   = body;

   m_str = to_utf8string( data.serialize() );
   
   //LOGI_( L ) << LGU::Logger::header(0);
}

void CJsonGenerator::FillLiveFps( int type )
{
   // jun_u+ : fps 전송부분
   json_value data   = json_value::object( true );
   json_value status = json_value::object( true );

   if(type > 0){
      status[U( "type" )]        = json_value::number( type );
      status[U( "temp_string" )] = json_value::string( U( "" ) );
   }

   json_value cam_list = json_value::array();
   {
      CCriticalSectionSP co( m_pLocalSystem->csCameraList );

      size_t ii = 0;
      for( CCamera* pCamera : m_pLocalSystem->Cameras ) {
         json_value cam_item = json_value::object( true );

         float fBitrate = 0;
         if( VideoChannelType_IPCamera == pCamera->VC->m_nVideoChannelType ) {
            fBitrate = pCamera->VC->GetDisplayBitrate();
         }

         cam_item[U( "cam_id" )]      = as_json( pCamera->CameraGuid );
         cam_item[U( "va_fps" )]      = json_value::number( int (pCamera->GetFrameRateLive( VCProcCallbackFuncType_ImgProc ) + 0.5f) );
         cam_item[U( "capture_fps" )] = json_value::number( int (pCamera->GetFrameRateLive( VCProcCallbackFuncType_Display ) + 0.5f) );
         cam_item[U( "bitrate" )]     = json_value::number( int( fBitrate / 1000.0f ) );

         //LOGI << "processDecreaseFPS Curr : " << GetLocalTimeString()<< "[FPS] (imgProc :" << pCamera->GetFrameRateLive( VCProcCallbackFuncType_ImgProc ) << ")(Disp :" <<pCamera->GetFrameRateLive( VCProcCallbackFuncType_Display ) << ")";

         cam_list[ii++] = cam_item;
      }
   }
   status[U( "info" )] = cam_list;

   data[U( "status" )] = status;

   m_str = to_utf8string( data.serialize() );
}

void CJsonGenerator::FillSystemStartup()
{
   json_value data   = json_value::object( true );
   json_value status = json_value::object( true );

   status[U( "type" )]        = json_value::number( System_Startup );
   status[U( "return_code" )] = json_value::number( 0 );
   status[U( "temp_string" )] = json_value::string( U( "" ) );

   data[U( "status" )] = status;

   m_str = to_utf8string( data.serialize() );
}

void CJsonGenerator::FillSystemShutdown( int code )
{
   json_value data   = json_value::object( true );
   json_value status = json_value::object( true );

   status[U( "type" )]        = json_value::number( System_Shutdown );
   status[U( "return_code" )] = json_value::number( code );
   status[U( "temp_string" )] = json_value::string( U( "" ) );

   data[U( "status" )] = status;

   m_str = to_utf8string( data.serialize() );
}


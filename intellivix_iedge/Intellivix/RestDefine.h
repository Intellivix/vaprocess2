﻿#ifndef RESTDEFINE_H
#define RESTDEFINE_H

#include <cpprest/json.h>

using utility::string_t;
using web::json::value;

typedef web::json::value  json_value;
typedef web::json::array  json_array;
typedef web::json::object json_object;

namespace web_rest_define
{

};

#endif // RESTDEFINE_H

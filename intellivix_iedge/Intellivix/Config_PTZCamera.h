﻿#pragma once

enum PTZPosInitScheduling {
   PTZPosInitScheduling_AtSpecifiedTime      = 0,
   PTZPosInitScheduling_AtSpecifiedIntervals = 1,
};

// 내부형식은 오른손 좌표계를 사용한다.
// 오른손 좌표계 인 경우 Pan은 반시계 방향일 때 값이 증가하며, Tilt는 아래방향으로 값이 증가한다.

enum PTAbsPosConvertMode {
   PTAbsPosConvertMode_Inner2Outer = 1, // 내부형식에서 외부형식으로 변환할 때
   PTAbsPosConvertMode_Outer2Inner = 2, // 외부형식에서 내부형식으로 변환할 때
};

enum PanRotationType {
   PanRotationType_ClockWise        = 0, // 시계방향 Pan각 증가
   PanRotationType_CounterClockWise = 1, // 반 시계방향 Pan각 증가
};

enum TiltRotationType {
   TiltRotationType_Bottom90 = 0, // Bottom 이 +90도 인..
   TiltRotationType_Top90    = 1, // Top    이 +90도 인..
};

enum PanAngleDisplayRangeType {
   PanAngleDisplayRangeType_0_P360    = 0, // Pan 각 범위를 0~360도로 한다.
   PanAngleDisplayRangeType_M180_P180 = 1, // Pan 각 범위를 -180~180도 한다.
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: CConfig_PTZCamera
//
///////////////////////////////////////////////////////////////////////////////

class CConfig_PTZCamera {
   // 마스터/슬래이브 추적 관련
public:
   int Priority; // 기본 우선 순위.
   float ZoomFactor; // 기본 줌 배율 값.
   FPoint2D ViewingPos; // 물체 박스에서의 위치를 나타내며 PTZ 추적시 이 위치를 기준으로 추적함.

   // PTZ 일반.
public:
   int PanTiltSpeed;
   int ZoomSpeed;
   int FocusSpeed;
   float DueNorthPoleOffsetAngle;
   int PanRotationType; // enum PanRotationType 참조
   int TiltRotationType; // enum TiltRotationType 참조
   int PanAngleDisplayRangeType; // enum PanAngleDisplayRangeType 참조

   // 메인 홈위치 설정.
public:
   int Flag_UseMainHomePos; // 메인 홈위치를 사용할지의 여부.
   FPTZVector HomePTZPos; // 메인 홈 위치 (PTZ 절대위치).
   int HomePresetNo; // 메인 홈 위치 (Preset 번호).
   float WaitingTime_HomePos; // 홈위치로 되돌아가기까지 대기하는 시간.

   // PTZ 위치 초기화
public:
   BOOL m_bUsePTZPosInitSchedule;
   int m_nPTZPosInitSchedulingMode;
   CTime m_tmPrevPosInitTime;
   CTime m_tmPosInitTimeFrom;
   CTime m_tmPosInitTimeTo;
   CTimeSpan m_tsPosInitRandomTimeSpan;
   int m_nDayInterval;
   BOOL m_bRandomTimeOnSpecifiedPeriod;
   int m_nInitPeriod_Time;
   int m_nInitPeriod_Minute;
   BOOL m_bDoesNotUpdateVideoAtPosinit;
   BOOL m_bRestartCameraAfterPosInit;

public:
   CConfig_PTZCamera();
   CConfig_PTZCamera& operator=( CConfig_PTZCamera& from );
   BOOL operator!=( CConfig_PTZCamera from );

public:
   void GetPTZTrackingParams( EventZoneEx* evt_zone );
   void SetPTZTrackingParams( EventZoneEx* evt_zone );
   void UpdatePosInitRandomTimeSpan();
   BOOL ReadFile( CXMLIO* pIO );
   BOOL WriteFile( CXMLIO* pIO );
};

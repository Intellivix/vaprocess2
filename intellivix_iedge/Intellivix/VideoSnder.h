﻿#pragma once

#include "VideoFrame.h"
#include "CompressImageBuffer.h"
#include "Config_VideoStream.h"
#include "AVCodec.h"
#include "Live555Define.h"

class CLocalSystem;
class CCamera;
typedef std::list<CLocalSystem*> CSystemList;

enum VideoSendMode {
   VideoSendMode_BySystemSocket = 1,
   VideoSendMode_ByCameraSocket = 2,
};

//////////////////////////////////////////////////////////////
//
// class CVideoSnder
//
//////////////////////////////////////////////////////////////

class CVideoSnder {
public:
   CVideoSnder();
   virtual ~CVideoSnder( void );

public:
   FileIO m_SndBuff;
   CAVCodec m_Encoder;
   int m_nSliceSize;
   BOOL m_bStopSend;
   BArray1D m_rsImgBuffer; // (xinu_cb04)

public:
   int m_nIVXStreamIdx; // IntelliVIX 에서 제공하는 스트림 인덱스
   int m_nVideoSendMode;
   BOOL m_bDirectStreaming;
   IVXVideoFrameInfo m_SendingVideoFrameInfo;
   CCamera* m_pCamera;

public:
   CCompressImageBufferQueue m_CompImgBuffQueue;

public:
   void Initialize( int nPacketID );
   void Initialize( int nPacketID, int nIVXStreamIdx );
   void Close();
   void SetStopSend() { m_bStopSend = TRUE; }
   BOOL SendVideo( CVideoFrame* pVideoFrame, CVideoStreamProfile* pVideoStreamProfie, BOOL bUseHWEncoder = FALSE ); // 라이브 스트림 전송용으로 사용된다.
   BOOL SendBuffer( int nChannelNo, FileIO& buffer );

public:
   void DeletePreviousFramesToKeyFrame();
};

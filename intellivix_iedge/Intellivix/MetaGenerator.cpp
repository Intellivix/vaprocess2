﻿#include "MetaGenerator.h"
#include "Camera.h"

//////////////////////////////////////////////////////////////////////////
//
// class CRanderObjectInImage
//
//////////////////////////////////////////////////////////////////////////

CRanderObjectInImage::CRanderObjectInImage()
{
}

CRanderObjectInImage::~CRanderObjectInImage()
{
}

bool CRanderObjectInImage::PreparingSourceImage( const PARAM& param )
{
   m_img_bgr.Create( param.src_image_width, param.src_image_height );
   BCCL::ConvertYUY2ToBGR( param.yuy2_buffer, m_img_bgr );

   m_va_size.Width  = param.va_image_width;
   m_va_size.Height = param.va_image_height;

   return true;
}

bool CRanderObjectInImage::RenderVaBoxes( std::deque<TrackedObjectEx*>& trackedObjectList )
{
   if( m_img_bgr.Width == 0 || m_img_bgr.Height == 0 ) {
      return false;
   }

   for( size_t i = 0; i < trackedObjectList.size(); i++ ) {
      TrackedObjectEx* t_object = trackedObjectList[i];
      if( t_object->Status & TO_STATUS_VERIFIED ) {
         // All object.
         DrawObjectBoundingBox( t_object );
      }
   }

   return true;
}

bool CRanderObjectInImage::SaveJPGFile( const std::string& file_name )
{
   if( DONE != m_img_bgr.WriteJPGFile( file_name.c_str() ) )
      return false;

   if( false == IsExistFile( file_name.c_str() ) )
      return false;

   return true;
}

void CRanderObjectInImage::DrawObjectBoundingBox( TrackedObject* t_object )
{
   ISize2D c_size( m_img_bgr.Width, m_img_bgr.Height );
   float scale_x = float( c_size.Width  / m_va_size.Width );
   float scale_y = float( c_size.Height / m_va_size.Height );
   IBox2D s_rgn1;
   s_rgn1.X      = floor( scale_x * t_object->X + 0.5f );
   s_rgn1.Y      = floor( scale_y * t_object->Y + 0.5f );
   s_rgn1.Width  = floor( scale_x * t_object->Width + 0.5f );
   s_rgn1.Height = floor( scale_y * t_object->Height + 0.5f );
   IBox2D s_rgn2;
   s_rgn2.X      = s_rgn1.X - 1;
   s_rgn2.Y      = s_rgn1.Y - 1;
   s_rgn2.Width  = s_rgn1.Width + 2;
   s_rgn2.Height = s_rgn1.Height + 2;
   BCCL::CropRegion( c_size, s_rgn1 );
   BCCL::CropRegion( c_size, s_rgn2 );
   BGRColor color;
   if( t_object->Events.GetNumItems() )
      color( 0, 0, 255 ); // red
   else
      color( 0, 255, 0 ); // green
   BCCL::DrawRectangle( s_rgn1, color, m_img_bgr );
   BCCL::DrawRectangle( s_rgn2, color, m_img_bgr );
}

//////////////////////////////////////////////////////////////////////////
//
// class CMetaGenerator
//
//////////////////////////////////////////////////////////////////////////

CMetaGenerator::CMetaGenerator()
{
}

CMetaGenerator::~CMetaGenerator()
{
}

void CMetaGenerator::SetLocalSystem( CLocalSystem* pLocalSystem )
{
   m_pLocalSystem = pLocalSystem;
}

void CMetaGenerator::SetCamera( CCamera* pCamera )
{
   m_pCamera = pCamera;
}

void CMetaGenerator::FillFrameInfo( std::deque<TrackedObjectEx*>& trackedObjectList )
{
}

void CMetaGenerator::FillFrmObjInfo( std::deque<TrackedObjectEx*>& trackedObjectList )
{
}

void CMetaGenerator::FillObjectInfo( TrackedObjectEx* t_object )
{
}

void CMetaGenerator::FillEventInfo( VAResults& results )
{
}

void CMetaGenerator::FillEventInfo( TrackedObjectEx* t_object, EventEx* pEvent )
{
}

void CMetaGenerator::FillEventZoneInfo( BOOL bNullInfo )
{
}

void CMetaGenerator::FillEventZoneCounterInfo()
{
}

void CMetaGenerator::FillLiveFps( int type )
{
}

void CMetaGenerator::FillSystemStartup()
{
}

void CMetaGenerator::FillSystemShutdown( int code )
{
}

//////////////////////////////////////////////////////////////////////////
//
// Factory pattern function
//
//////////////////////////////////////////////////////////////////////////

#include "XmlGenerator.h"
#include "JsonGenerator.h"

MetaGeneratorSP GetGenerator( const MetaGeneratorType& type, CLocalSystem* system, CCamera* camera )
{
   MetaGeneratorSP instance;

   switch( type ) {
   case MetaGeneratorType::XML:
      instance = std::make_shared<CXmlGenerator>();
      break;
   case MetaGeneratorType::JSON:
      instance = std::make_shared<CJsonGenerator>();
      break;
   default:
      break;
   }

   instance->SetLocalSystem( system );
   instance->SetCamera( camera );
   instance->InitGenerator ();

   return instance;
}

MetaGeneratorSP GetGenerator( const MetaGeneratorType& type, CCamera* camera )
{
   return GetGenerator( type, camera->System, camera );
}

#include "StdAfx.h"
#include <deque>
#include <vacl_event.h>
#include <vacl_objtrack.h>
#include "EventGrouping.h"

using namespace VACL;

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventGrouping
//
///////////////////////////////////////////////////////////////////////////////

EventGrouping::EventGrouping()
{
   FrameRate = 10.0f;
}

EventGrouping::~EventGrouping()
{
   DeadObjects.Delete();
   EvtGenObjects.Delete();
}

BOOL IsExistEventType( std::deque<int>& eventTypeList, int fineEventID )
{
   size_t i;
   size_t eventTypeNum = eventTypeList.size();

   for( i = 0; i < eventTypeNum; i++ ) {
      if( eventTypeList[i] == fineEventID ) {
         return TRUE;
      }
   }
   return FALSE;
}

TrackedObjectEx* EventGrouping::FindTrackedObject( int eventType )
{
   TrackedObject* t_object = EvtGenObjects.First();
   while( t_object ) {
      Event* event = t_object->Events.First();
      if( event ) {
         if( event->Type == eventType ) {
            return (TrackedObjectEx*)t_object;
         }
      }
      t_object = t_object->Next;
   }
   return NULL;
}

void EventGrouping::FinalizeEvents()
{
   TrackedObject* eg_object = (TrackedObject*)EvtGenObjects.First();
   while( eg_object != NULL ) {
      if( eg_object->Status & TO_STATUS_TO_BE_REMOVED ) {
         EventEx* eg_event = (EventEx*)eg_object->Events.First();
         while( eg_event != NULL ) {
            if( eg_event->Status & ( EV_STATUS_BEGUN | EV_STATUS_ONGOING ) ) {
               eg_event->EndTime.GetLocalTime();
               eg_event->NPT_EndTime = NormalPlayTime;
               eg_event->Status      = EV_STATUS_ENDED | EV_STATUS_TO_BE_REMOVED;
            }
            eg_event = (EventEx*)eg_object->Events.Next( eg_event );
         }
      } else {
         EventEx* eg_event = (EventEx*)eg_object->Events.First();
         while( eg_event != NULL ) {
            Event* n_event = (EventEx*)eg_object->Events.Next( eg_event );
            if( eg_event->Status & EV_STATUS_TO_BE_REMOVED ) {
               eg_object->Events.Remove( eg_event );
               delete eg_event;
            }
            eg_event = (EventEx*)n_event;
         }
      }
      eg_object = (TrackedObject*)EvtGenObjects.Next( eg_object );
   }
}

void EventGrouping::CleanTrackedObjectList()
{
   TrackedObject* t_object = EvtGenObjects.First();
   while( t_object != NULL ) {
      TrackedObject* n_object = EvtGenObjects.Next( t_object );
      if( t_object->Status & TO_STATUS_TO_BE_REMOVED ) {
         EvtGenObjects.Remove( t_object );
         if( t_object->Status & TO_STATUS_VERIFIED )
            DeadObjects.Add( t_object );
         else
            delete t_object;
      }
      t_object = n_object;
   }
   int c_thrsld = (int)( 5.0f * FrameRate );
   t_object     = DeadObjects.First();
   while( t_object != NULL ) {
      TrackedObject* n_object = DeadObjects.Next( t_object );
      if( FrameCount - t_object->FrameCount > c_thrsld ) {
         DeadObjects.Remove( t_object );
         delete t_object;
      }
      t_object = n_object;
   }
}

void EventGrouping::Initialize( ISize2D& si_size, float frm_rate )
{
   SrcImgSize = si_size;
   FrameRate  = frm_rate;
   FrameCount = 0;
}

void EventGrouping::Close()
{
   FrameCount = 0;
   DeadObjects.Delete();
   EvtGenObjects.Delete();
}

int EventGrouping::Perform( double dfNormalPlayTime, ObjectTracking* obj_tracker, EventDetection* event_detector )
{
   size_t i;

   NormalPlayTime = dfNormalPlayTime;

   TrackedObjectEx* eg_object = (TrackedObjectEx*)EvtGenObjects.First();
   while( eg_object != NULL ) {
      EventEx* eg_event = (EventEx*)eg_object->Events.First();
      while( eg_event != NULL ) {
         if( eg_event->Status & EV_STATUS_BEGUN ) {
            eg_event->Status &= ~EV_STATUS_BEGUN;
            if( !( eg_event->Status & EV_STATUS_ONGOING ) ) {
               eg_event->Status |= EV_STATUS_ONGOING;
            }
         } else if( eg_event->Status & EV_STATUS_ENDED ) {
            eg_event->Status &= ~EV_STATUS_ENDED;
         }
         eg_event = (EventEx*)eg_object->Events.Next( eg_event );
      }
      eg_object = (TrackedObjectEx*)EvtGenObjects.Next( eg_object );
   }

   CleanTrackedObjectList();
   FinalizeEvents();

   FrameCount++;

   std::deque<int> currEventTypes;

   TrackedObjectList* trackedObjects = NULL;

   for( i = 0; i < 3; i++ ) {
      if( i == 0 ) trackedObjects = &obj_tracker->TrackedObjects;
      if( i == 1 ) trackedObjects = &obj_tracker->LostObjects;
      if( i == 2 ) trackedObjects = &event_detector->EvtGenObjects;
      TrackedObjectEx* t_object   = (TrackedObjectEx*)trackedObjects->First();
      while( t_object != NULL ) {
         EventEx* event = (EventEx*)t_object->Events.First();
         while( event != NULL ) {
            int evtRuleType = event->EvtRule->EventType;
            if( i != 1 || evtRuleType == 4 ) { // evtRuleType == 4 == ER_EVENT_DISAPPEARING
               if( event->Status & ( EV_STATUS_BEGUN | EV_STATUS_ONGOING ) ) {
                  if( FALSE == IsExistEventType( currEventTypes, evtRuleType ) ) {
                     if( FALSE == IsExistEventType( PrevEventTypes, evtRuleType ) ) {
                        TrackedObjectEx* eg_object = new TrackedObjectEx;
                        eg_object->ID              = event_detector->GetNewObjectID();
                        eg_object->Status          = TO_STATUS_VALIDATED | TO_STATUS_VERIFIED;
                        eg_object->Type            = TO_TYPE_UNKNOWN;
                        ( (IBox2D)*eg_object )     = ( (IBox2D)*t_object );
                        EvtGenObjects.Add( eg_object );

                        EventRuleEx* evt_rule = new EventRuleEx;
                        //evt_rule->EventType = evtRuleType;
                        //int nIVXEvtID = -1;
                        //VACEvtIDToIVXEvtID (evtRuleType, nIVXEvtID);
                        //CString& strEventName = g_pDoc->m_aStrings.m_strEvents[nIVXEvtID];
                        //strcpy (evt_rule->Name, strEventName);

                        EventZoneEx* evt_zone = new EventZoneEx;
                        evt_zone->EvtRuleID   = evt_rule->ID;
                        evt_zone->EvtRule     = evt_rule;

                        EventRules.Add( evt_rule );
                        EventZones.Add( evt_zone );

                        EventEx* eg_event = (EventEx*)CreateInstance_Event( evt_zone, evt_rule );
                        eg_event->StartTime.GetLocalTime();
                        eg_event->NPT_StartTime = NormalPlayTime;
                        ;
                        eg_object->Events.Add( eg_event );
                        eg_event->NumObjects++;
                        logd( "OBJ:%d  Event Type (%d) - Started (%f)\n", eg_object->ID, evtRuleType, NormalPlayTime );
                     }
                     currEventTypes.push_back( evtRuleType );
                  }
               }
            }
            event = (EventEx*)t_object->Events.Next( event );
         }
         t_object = (TrackedObjectEx*)trackedObjects->Next( t_object );
      }
   }

   eg_object = (TrackedObjectEx*)EvtGenObjects.First();
   while( eg_object ) {
      int sx            = 100000;
      int sy            = 100000;
      int ex            = -1;
      int ey            = -1;
      EventEx* eg_event = (EventEx*)eg_object->Events.First();
      if( eg_event ) {
         int evtRuleType = eg_event->Type;
         for( i = 0; i < 3; i++ ) {
            if( i == 0 ) trackedObjects = &obj_tracker->TrackedObjects;
            if( i == 1 ) trackedObjects = &obj_tracker->LostObjects;
            if( i == 2 ) trackedObjects = &event_detector->EvtGenObjects;
            TrackedObjectEx* t_object   = (TrackedObjectEx*)trackedObjects->First();
            while( t_object != NULL ) {
               EventEx* event = (EventEx*)t_object->Events.First();
               while( event != NULL ) {
                  if( i != 1 || evtRuleType == 4 ) { //
                     if( event->Status & ( EV_STATUS_BEGUN | EV_STATUS_ONGOING ) ) {
                        if( event->EvtRule->EventType == evtRuleType ) {
                           int x1           = t_object->X;
                           int y1           = t_object->Y;
                           int x2           = x1 + t_object->Width;
                           int y2           = y1 + t_object->Height;
                           if( sx > x1 ) sx = x1;
                           if( sy > y1 ) sy = y1;
                           if( ex < x2 ) ex = x2;
                           if( ey < y2 ) ey = y2;
                        }
                     }
                  }
                  event = (EventEx*)t_object->Events.Next( event );
               }
               t_object = (TrackedObjectEx*)trackedObjects->Next( t_object );
            }
         }
      }
      IBox2D b_box;
      b_box( sx - 2, sy - 2, ex - sx + 4, ey - sy + 4 );
      eg_object->X      = sx - 2;
      eg_object->Y      = sy - 2;
      eg_object->Width  = ex - sx + 4;
      eg_object->Height = ey - sy + 4;

      if( !( eg_object->Status & ( TO_STATUS_TO_BE_REMOVED ) ) )
         eg_object->FrameCount = FrameCount;

      eg_object = (TrackedObjectEx*)eg_object->Next;
   }

   size_t prevEventTypeNum = PrevEventTypes.size();

   for( i = 0; i < prevEventTypeNum; i++ ) {
      int eventTypeID = PrevEventTypes[i];
      if( FALSE == IsExistEventType( currEventTypes, eventTypeID ) ) {
         TrackedObjectEx* eg_object = FindTrackedObject( eventTypeID );
         if( eg_object ) {
            EventEx* eg_event = (EventEx*)eg_object->Events.First();
            if( eg_event ) {
               eg_event->Status = EV_STATUS_ENDED | EV_STATUS_TO_BE_REMOVED;
               eg_event->EndTime.GetLocalTime();
               eg_event->NPT_EndTime = NormalPlayTime;

               logd( "OBJ:%d  Event Type (%d) - Started (%f)\n", eg_object->ID, eg_event->Type, NormalPlayTime );

            } else {
               eg_object->Status |= TO_STATUS_TO_BE_REMOVED;
            }
         }
      }
   }

   PrevEventTypes = currEventTypes;

   return ( DONE );
}

void EventGrouping::Reset()
{
   FrameCount = 0;
   DeadObjects.Delete();
   TrackedObject* eg_object = EvtGenObjects.First();
   while( eg_object != NULL ) {
      eg_object->Status |= TO_STATUS_TO_BE_REMOVED;
      eg_object = EvtGenObjects.Next( eg_object );
   }
}

void EventGrouping::UpdateEventThumbnails( byte* si_buffer, ISize2D& si_size, int pix_fmt, float cbs_ratio, ISize2D* tn_size )
{
   FPoint2D scale;
   scale.X                 = (float)si_size.Width / SrcImgSize.Width;
   scale.Y                 = (float)si_size.Height / SrcImgSize.Height;
   TrackedObject* t_object = EvtGenObjects.First();
   while( t_object != NULL ) {
      Event* event = t_object->Events.First();
      while( event != NULL ) {
         if( event->Status & EV_STATUS_BEGUN ) {
            IBox2D so_box      = t_object->Scale( scale );
            event->ThumbPixFmt = pix_fmt;
            CaptureThumbnail( si_buffer, si_size, pix_fmt, so_box, event->ThumbBuffer, event->ThumbSize, cbs_ratio, tn_size );
         }
         event = t_object->Events.Next( event );
      }
      t_object = EvtGenObjects.Next( t_object );
   }
}

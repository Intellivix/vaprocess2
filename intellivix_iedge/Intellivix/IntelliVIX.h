﻿#pragma once

class CSupportFunction;
class CLocalSystem;
class CIntelliVIXDoc;

class CIntelliVIXApp {
public:
   CIntelliVIXApp();
   ~CIntelliVIXApp();

   CSupportFunction* m_pSupportFunc;
   CLocalSystem* m_pLocalSystem;
   CIntelliVIXDoc* m_pDocument;

   int InitInstance();
   int ExitInstance();

   int SetLogLevel( char level );

   void WriteMemory();
   void WriteMemory2();
};

CIntelliVIXApp* GetIntelliVIXApp();



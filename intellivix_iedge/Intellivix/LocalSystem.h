﻿#pragma once

#include "StdAfx.h"
#include "version.h"
#include "CommonUtil.h"
#include "IntelliVIX.h"

// #include "CpuMonitoring.h" // CPU

class CIntelliVIXDoc;
class CRTSPCustomServer;
class CRTSPStreamingChannel;
class CCamera;

////////////////////////////////////////////////////////////////////
//
// Enumerations
//
////////////////////////////////////////////////////////////////////

enum SysState {
   SysState_StartUp         = 0x00000001,
   SysState_ToBeRemoved     = 0x00000010,
   SysState_OnCamAdd        = 0x00000040,
   SysState_SendLiveVideo   = 0x00000800,
   SysState_SystemActivated = 0x00002000,
};

enum GlobalState {
   GlobalState_LocalSysStarting             = 0x00000010,
   GlobalState_RemovingCameras              = 0x00001000,
   GlobalState_SystemToBeClosedWithoutCheck = 0x00040000,
   GlobalState_ClosingAllSystems            = 0x00080000,
   GlobalState_SystemToBeClosed             = 0x00200000,
   GlobalState_TobeSaveAllSetup             = 0x00400080,
   GlobalState_RemovingSystem               = 0x00800000,
   GlobalState_ObjectCountersToBeReset      = 0x01000000,
};

typedef std::deque<CCamera*>::iterator CameraIter;

///////////////////////////////////////////////////////////////////////////
//
// Class : CLocalSystem
//
///////////////////////////////////////////////////////////////////////////

class CLocalSystem {
protected:
   std::string SystemGUID;
   std::string SystemName;
   CLocalSystem* System_Temp = nullptr;

public:
   CCriticalSection m_csLocalCfgRW;
   CCriticalSection m_csSystem;
   const std::string m_strLocalSystemCfgFileName;

   uint m_nGlobalState;
   int SetupChannelNo;
   int m_rtspServerPort             = 554;
   int m_rtspServerPort_Alternetive = 8554;

   int  m_Manager2Process  = 0; // 0(d) : REST(JSon), 1: WebSocket
   int  m_LogLevel         = 3;
   int  m_MetaType         = 1; // 1(d) : Meta 전문만 요청, 2: Meta + 이미지 요청
   int  m_WhoService       = 1; // 1(d) : Intellivix, 2 : CUDO, 11 : Other 1, 12 : Other 2
   int  m_ProcessHeartbeat = 5; // 5~10(d)초
   std::string m_ManagerURL; // Manager 또는 WebSocket URL
   std::string m_SaveMetaImagePath; // Meta 이미지에 대해서 별도 저장이 필요할 경우 해당 위치 정보 전달
   
   std::string m_Uplus_Log_Path_ = "";
   int m_Uplus_Log_Cycle_ = 5;
   
   
   CIntelliVIXDoc* m_pDoc = nullptr;
   CTimerManagerSP m_pTimerMgr;

   BOOL IsTemp;
   CVideoChannelManager VCM;

   std::deque<CCamera*> Cameras;
   int CameraNum;
   CCriticalSection csCameraList; // Cameras, CameraNum 사용하기 위한 락
   int Status;

   uint32_t m_nSaveLocalSystemSetupSaveTime;

   // 로컬시스템 스레드
   std::thread m_hThread_LocalSystemProc;
   CEventEx m_evtExitLocalSystemProc;
   CEventEx m_evtRestartCamera;

private:
   static UINT ThreadFunc_LocalSystemProc( LPVOID pParam );
   void LocalSystemProc();

public:
   CLocalSystem();
   virtual ~CLocalSystem();

public:
   CLocalSystem* GetSystemTemp()
   {
      return System_Temp;
   }

   BOOL IsExistCamera( int nOption );

   void SetSystemGUID( std::string strGUID );
   std::string GetSystemGUID();

   CCamera* GetCamera( int nChannelNo );
   CCamera* GetCameraFromUID( int nUID );
   CCamera* GetCameraFromGuid( const std::string& strCameraGuid );
   CCamera* GetCameraFromVCID( INT nVCID );
   int GetChannelNoFromUID( int nUID );

   BOOL DeallocateCamera( int nChannelNo );
   BOOL DeallocateCamera( CCamera* pCamera );

   BOOL Startup();
   BOOL Shutdown();
   void SetSystemName( LPCSTR szName );
   LPCSTR GetSystemName();

public:
   BOOL ApplySetup();
   BOOL InitializeWizardSetup( int nSetupChannelNo = -1 );
   BOOL FinalizeWizardSetup( BOOL bSaveChange );
   void ApplyToAllChannels( CCamera* pCamera, int nItem );
   CLocalSystem* CreateTemp( int nSetupChannelNo = -1, int nExcludeSetupChannelNo = -1 );
   void DeleteTempSystem();
   void GetCameraNames( std::vector<std::string>& array );

   int ReadVCMSetup( CXMLIO* pIO );
   int WriteVCMSetup( CXMLIO* pIO );

public:
   void Lock_CameraList()
   {
      csCameraList.Lock();
   }
   void Unlock_CameraList()
   {
      csCameraList.Unlock();
   }

public:
   void InitVCM();
   void UnInitVCM();
   void VerifySafeConfigFile( const std::string& strPathName );

   void BackupOldConfigFile( const std::string& strPathName );
   void FinishSafeConfigFile( const std::string& strPathName );
   int LoadCFGFile();
   int SaveCFGFile();
   void SetSystemSetupToBeSaved( int nDelayTime = 15000 );
   int WriteSetupCmn( CXMLIO* pIO, int nRWFlag, int item = SMG_SETUP_ALL );
   int ReadSetupCmn( CXMLIO* pIO, int nRWFlag, int item = SMG_SETUP_ALL );
   int TempToOrigin( CLocalSystem* t_sys, int item = SMG_SETUP_ALL );
   int ReadSetup( CXMLIO* IO, int nSetupChannelNo, int nExcludeSetupChannelNo, int nRWFlag ) throw( IVXException );
   int WriteSetup( CXMLIO* IO, int nSetupChannelNo, int nExcludeSetupChannelNo, int nRWFlag );

public:
   void DetermineStartStopVideoStreamingThread();

public:
   int ActivateSystem();
   void DeactivateSystem();

   int IsCanRemoveCamera( CCamera* pCamera );

public:
   int AddCamera();
   int AddCamera( CCamera* pNewCamera );
   int RemoveCamera( CCamera* pCamera );
   int RemoveCamera( int nChannelNo );
   int RemoveAllCameras();
   int CheckDeactivation( CLocalSystem* system, int filter = SMG_SETUP_ALL );
   int GetChangedSetupItem( CLocalSystem* system, int filter = SMG_SETUP_ALL );

public:
   // RTSP Server
   std::shared_ptr<CRTSPCustomServer> m_RTSPServer;
   void OnRTSPRequest( const std::string& strStreamName );
   void OnStreamingChannelClosed( CRTSPStreamingChannel* pRTSPStreamingChannel );
   void SendLiveFps();
   void performanceMonitoring( CMMTimer* pTimer_1sec );
};

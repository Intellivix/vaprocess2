﻿////////////////////////////////////////////////////////////////
#if !defined( __COMMONDEF_H )
#define __COMMONDEF_H

////////////////////////////////////////////////////////////////////////////////////////

#include "AVCodec.h"

#define UPDATE_FTP_ADDRESS _T("127.0.0.1")
#define UPDATE_FTP_USERNAME _T("intellivix")
#define UPDATE_FTP_PASSWARD _T("illisis")
#define UPDATE_FTP_FILENAME _T("IntelliVIX Latest Version")
#define DOWNLOAD_LATEST_FILENAME _T("IntelliVIX_Update.exe")
#define CODEC_SETTING_FILE_NAME "CodecSetting.dat"
#define LOCAL_SYSTEM_BASE_PORT 2004
#define LOCAL_SYSTEM_RAS_PORT 2005
#define IVX_THUMBNAIL_WIDTH 120
#define IVX_THUMBNAIL_HEIGHT 120
#define MIN_CONNECTION_NUM 1
#define MAX_CONNECTION_NUM 8
#define MAX_SYSTEM_NAME 64
#define MAX_CAMERA_NAME 64
#define REG_VALNAME_USERS _T("Users")
#define REG_SUB_KEY _T("SOFTWARE\\illisis")
#define MAX_REG_USER 100
#define LICENSE_SERVER_HOSTNAME_DACOM "www.illisis.com"
#define LICENSE_SERVER_PAGEURL_DACOM "registration/lkg/V2/response_a.asp"
#define LICENSE_SERVER_HOSTNAME_LOCAL "192.168.0.5"
#define LICENSE_SERVER_PAGEURL_LOCAL "lkg/V2/response_a.asp"

const CSize MDBThumbnailSize( 120, 120 );
const VideoCodecID MDBThumbnailCodecID = VCODEC_ID_MJPEG;
const int ThumbnailDataSize            = 64 * 1024; // 64Kbps

// 경고[JUN] : 이벤트 종류 추가 시 유의해야 할 사항
//         1. 위 EID_NUM 값 반드시 변경.
//         2. CIntelliVIXStrings::Initialize 함수 내에서 m_strAlarms 배열생성 시 갯수 지정 반드시 변경.
//         3. 추가된 이벤트 아이디의 변경 및 삭제 금지! -> 호환성 문제 발생

// DB에서 정의한 이벤트 타입과 응용프로그램에서 정의한 EVENT 타입을 Mapping 시켜주는 함수를 정의한다.
// (CommonDef.h 의 EVENT_ID enum 형식과 DBDefine.h  의 __EvtType enum 형식을 비교해볼 것)
std::vector<int> GetSupportIVXEventIDList();

enum SetupID {
   IVXSetupItemID_DBStorage                       = 0,
   IVXSetupItemID_UserAccount                     = 1,
   IVXSetupItemID_Network                         = 2,
   IVXSetupItemID_LocalSystemBasic                = 4,
   IVXSetupItemID_LocalSystemAdvanced             = 5,
   IVXSetupItemID_ExternalSystemLink              = 6,
   IVXSetupItemID_LocalSystemCustomView           = 7,
   IVXSetupItemID_VideoInput                      = 8,
   IVXSetupItemID_VideoRecoding                   = 9,
   IVXSetupItemID_VideoRecodingClient             = 10,
   IVXSetupItemID_VideoColor                      = 11,
   IVXSetupItemID_AlarmInput                      = 12,
   IVXSetupItemID_AuxControl                      = 13,
   IVXSetupItemID_CameraDisplayOptions            = 14,
   IVXSetupItemID_VideoStreaming                  = 15,
   IVXSetupItemID_FloodLightControl               = 16,
   IVXSetupItemID_VADetection                     = 17,
   IVXSetupItemID_VACameraGeometry                = 18,
   IVXSetupItemID_PTZAreaSetup                    = 19,
   IVXSetupItemID_IVXNetSystemSetup               = 20,
   IVXSetupItemID_IVXNetCameraSetup               = 21,
   IVXSetupItemID_TimeSyncServer                  = 22,
   IVXSetupItemID_SMS                             = 23,
   SID_not_used                                   = 24,
   IVXSetupItemID_PTZCtrlOuterLink                = 25,
   IVXSetupItemID_LocalSystemPlateSetting         = 26,
   IVXSetupItemID_AlarmInputMPU                   = 27,
   IVXSetupItemID_AlarmInputET0808                = 28,
   IVXSetupItemID_FaceDetectionRecognitionSetting = 29,
   IVXSetupItemID_FaceServerSetting               = 30,
   IVXSetupItemID_BIMDCollector                   = 31,
};

enum IVXSetupItemMask {
   IVXSetupItemMask_DBStorage             = 0x00000001,
   IVXSetupItemMask_UserAccount           = 0x00000002,
   IVXSetupItemMask_Network               = 0x00000004,
   IVXSetupItemMask_LocalSystemBasic      = 0x00000010,
   IVXSetupItemMask_LocalSystemAdvanced   = 0x00000020,
   IVXSetupItemMask_ExternalSystemLink    = 0x00000040,
   IVXSetupItemMask_LocalSystemCustomView = 0x00000080,

   IVXSetupItemMask_VideoInput           = 0x00000100,
   IVXSetupItemMask_VideoRecoding        = 0x00000200,
   IVXSetupItemMask_VideoColor           = 0x00000400,
   IVXSetupItemMask_AlarmInput           = 0x00000800,
   IVXSetupItemMask_AuxControl           = 0x00001000,
   IVXSetupItemMask_CameraDisplayOptions = 0x00002000,
   IVXSetupItemMask_VideoStreaming       = 0x00004000,
   IVXSetupItemMask_FloodLightControl    = 0x00008000,

   IVXSetupItemMask_VADetection      = 0x00010000,
   IVXSetupItemMask_VACameraGeometry = 0x00020000,
   IVXSetupItemMask_VAEventDetection = 0x00040000,

   IVXSetupItemMask_IVXNetSystemSetup = 0x00200000,
   IVXSetupItemMask_IVXNetCameraSetup = 0x00400000,
   IVXSetupItemMask_TimeSyncServer    = 0X00800000,

   IVXSetupItemMask_SMS                     = 0x01000000,
   IVXSetupItemMask_LocalSystemPlateSetting = 0x04000000,
   IVXSetupItemMask_PTZCtrlOuterLink        = 0x08000000,
   IVXSetupItemMask_Panorama                = 0x10000000,

   IVXSetupItemMask_FaceDetectionRecognitionSetting = 0x20000000,
   IVXSetupItemMask_FaceServerSetting               = 0x40000000,
   IVXSetupItemMask_BIMDCollector                   = 0x80000000,
};

int GetSeupItemMask( int nSetupID );

// 주의!! : enum SETUP_ITEM_MASK에 항목을 추가하면 아래의 상수에 플래그를 반드시 추가해야한다.
//          CCsystem::AbleSetupItems 변수를 통하여 로컬시스템 또는 클라이언트 시스템에 따라서 설정가능한 항목을 지정하도록 되어있다.

const uint SMG_SETUP_ALL = ( IVXSetupItemMask_DBStorage | IVXSetupItemMask_CameraDisplayOptions | IVXSetupItemMask_VADetection | IVXSetupItemMask_VACameraGeometry | IVXSetupItemMask_AlarmInput | IVXSetupItemMask_AuxControl | IVXSetupItemMask_VAEventDetection | IVXSetupItemMask_VideoInput | IVXSetupItemMask_VideoRecoding | IVXSetupItemMask_VideoColor | IVXSetupItemMask_FloodLightControl | IVXSetupItemMask_IVXNetSystemSetup | IVXSetupItemMask_IVXNetCameraSetup | IVXSetupItemMask_VideoStreaming | IVXSetupItemMask_SMS | // TYLEE
                             IVXSetupItemMask_Panorama | IVXSetupItemMask_PTZCtrlOuterLink | 0 );

const uint SMG_CAMERA_SETUP = ( IVXSetupItemMask_VideoInput | IVXSetupItemMask_VideoRecoding | IVXSetupItemMask_VideoColor | IVXSetupItemMask_AlarmInput | IVXSetupItemMask_AuxControl | IVXSetupItemMask_CameraDisplayOptions | IVXSetupItemMask_VideoStreaming | IVXSetupItemMask_FloodLightControl | IVXSetupItemMask_IVXNetCameraSetup | IVXSetupItemMask_Panorama | IVXSetupItemMask_PTZCtrlOuterLink );

const uint SMG_SYSTEM_SETUP = ( IVXSetupItemMask_DBStorage | IVXSetupItemMask_UserAccount | IVXSetupItemMask_Network | IVXSetupItemMask_LocalSystemBasic | IVXSetupItemMask_LocalSystemAdvanced | IVXSetupItemMask_ExternalSystemLink | IVXSetupItemMask_LocalSystemCustomView | IVXSetupItemMask_IVXNetSystemSetup | IVXSetupItemMask_SMS );

const int DeviceChannelNum = 4;

enum IVXException {
   Succeed = 0,

   Parameter_Missing = 1,
   Parameter_Not_Supported,
   Parameter_Set_Camera_Id_Failed,
   Parameter_Loglevel_Out_Of_Range,
   Parameter_Empty,

   // SystemConfig.xml 읽기 실패 코드
   Xml_Read_Config_Not_Exist                    = 100,
   Xml_Read_Config_Parsing_Failed               = 101,
   Xml_Read_Config_LocalSystem_Node_Not_Exist   = 102,
   Xml_Read_Config_System_Node_Not_Exist        = 103,
   Xml_Read_Config_Camera_Number_Zero           = 104,
   Xml_Read_Config_Camera_Node_Not_Exist        = 105,
   Xml_Read_Config_Camera_URL_Empty             = 106,
   Xml_Read_Config_Camera_Guid_Empty            = 107, // jun_u+ : 현재 U+ only 사용
   Xml_Read_Config_CameraSetup_Node_Not_Exist   = 108,
   Xml_Read_Config_ObjectDtc_Node_Not_Exist     = 109,
   Xml_Read_Config_Vac_Node_Not_Exist           = 110,
   Xml_Read_Config_VideoInput_Node_Not_Exist    = 111,
   Xml_Read_Config_Vac_Parsing_Failed           = 112,
   Xml_Read_Config_VideoInput_Parsing_Failed    = 113,

   Camera_Status_No_Signal                      = 120, // 카메라 신호가 없을 시
   Camera_Status_Authorization_Failed           = 121, // 카메라 Login 실패
   Camera_Status_Rest_Send_Failed               = 122, // Rest 전송에 실패 하였을 시에
   Camera_Status_Decoded_Video_Size_Is_Bigger   = 123, // 새로　수신된　영상이　지정된　영상　보다　클때

   System_Status_Rest_Activate_Failed           = 300,
   System_Status_Caffe_Load_Failed              = 301,
};

enum StatusType {
   Camera_Live_Fps = 1, // 현재 FPS
   Camera_Va_Decrease_Fps, // VA FPS 저하 시에
   Camera_End = 99,

   System_Startup = 100,
   System_Shutdown,
   System_End = 199,
};

#endif

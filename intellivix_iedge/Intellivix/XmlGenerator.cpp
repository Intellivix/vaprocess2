﻿#include "XmlGenerator.h"
#include "SupportFunction.h"
#include "Camera.h"

///////////////////////////////////////////////////////////////////////////////
//
// Class: CXmlGenerator
//
///////////////////////////////////////////////////////////////////////////////

CXmlGenerator::CXmlGenerator()
{   
   //m_p_stream = new std::stringstream( std::stringstream::in | std::stringstream::out | std::stringstream::binary );
}

CXmlGenerator::~CXmlGenerator()
{
//    if( m_p_stream ) {
//        delete m_p_stream;
//    }
}

MetaGeneratorType CXmlGenerator::GetType() const
{
   return MetaGeneratorType::XML;
}

std::string CXmlGenerator::GetString() const
{
   //return m_p_stream->str();
   return m_str;
}

void CXmlGenerator::AddToStream( CIVCPPacket* pIvcp_packet, int packet_id )
{
   assert( pIvcp_packet );

   // IVCP 는 Header 정보가 필요하지만
   // RTSP 를 통해 전송하는 데이터는 IVCP 의 Header 정보가 필요 없다.
//   IvcpGetPacketString( pIvcp_packet->m_Header.m_strMessage, (IVCP30_PacketID)packet_id );
//   *m_p_stream << pIvcp_packet->m_Header.m_strMessage
//            << " "
//            << pIvcp_packet->m_Header.m_nOptions
//            << " "
//            << pIvcp_packet->m_Header.m_nBinaryOptionalLen
//            << " "
//            << pIvcp_packet->m_Header.m_nBodyLen
//            << "\n"
//            << pIvcp_packet->m_pBody;

   m_str.clear();
   m_str.assign(pIvcp_packet->m_pBody, pIvcp_packet->m_Header.m_nBodyLen);
   //LOGI << m_str;
}

void CXmlGenerator::FillFrameInfo( std::deque<TrackedObjectEx*>& trackedObjectList )
{
   // 보내줄 구조체 정리
   CIVCPLiveFrameInfo frameInfo;
   FILETIME ftCurTime;
   AfxGetLocalFileTime( &ftCurTime );
   frameInfo.m_cCameraGUID = m_pCamera->CameraGuid;
   frameInfo.m_nCameraUID  = m_pCamera->UID;
   //frameInfo.m_ftFrameTime  = m_pCamera->Timestamp; // 타임스탬프
   frameInfo.m_ftFrameTime  = ftCurTime;
   frameInfo.m_nObjectNum   = 0;
   frameInfo.m_nCameraState = m_pCamera->m_nState;
   frameInfo.m_nPTZState    = m_pCamera->m_nPTZState;
   frameInfo.m_nPTZStateEx  = m_pCamera->m_nPTZStateEx;
   frameInfo.m_fFrameRate   = m_pCamera->DesiredFrameRate;
   frameInfo.m_nProcWidth   = m_pCamera->ProcImgSize.Width;
   frameInfo.m_nProcHeight  = m_pCamera->ProcImgSize.Height;

   int nObjCount        = (int)trackedObjectList.size();
   frameInfo.m_pObjects = new CIVCPFrameObject[nObjCount];

   for( int i = 0; i < nObjCount; i++ ) {
      TrackedObjectEx* t_object = trackedObjectList[i];

      if( frameInfo.m_nObjectNum >= nObjCount ) break;
      if( t_object->Status & TO_STATUS_VERIFIED ) {
         CIVCPFrameObject& frameObject    = frameInfo.m_pObjects[frameInfo.m_nObjectNum];
         frameObject.m_nObjectID          = t_object->ID;
         frameObject.m_nObjectType        = t_object->Type;
         frameObject.m_nObjectStatus      = t_object->Status;
         frameObject.m_nObjectEventStatus = t_object->EventStatus;
         frameObject.m_nObjectVAEvent     = t_object->VAEventFlag;
         frameObject.m_ObjectBox.X        = t_object->X;
         frameObject.m_ObjectBox.Y        = t_object->Y;
         frameObject.m_ObjectBox.Width    = t_object->Width;
         frameObject.m_ObjectBox.Height   = t_object->Height;
         frameObject.m_fTimeDwell         = t_object->Time_Dwell;

         int nEventNum           = 0;
         Event* pEvent           = NULL;
         nEventNum               = t_object->Events.GetNumItems();
         pEvent                  = t_object->Events.First();
         frameObject.m_pEvents   = new CIVCPFrameEvent[nEventNum];
         frameObject.m_nEventNum = 0;
         while( pEvent ) {
            if( frameObject.m_nEventNum >= nEventNum ) break;
            CIVCPFrameEvent& frameEvent = frameObject.m_pEvents[frameObject.m_nEventNum];
            frameEvent.m_nEventID       = pEvent->ID;
            frameEvent.m_nEventType     = pEvent->EvtRule->EventType;
            if( frameEvent.m_nEventType == ER_EVENT_LINE_CROSSING ) frameEvent.m_nEventType = ER_EVENT_PATH_PASSING;
            frameObject.m_nEventNum++;
            pEvent = pEvent->Next;
         }

         frameInfo.m_nObjectNum++;
      }
   }

   CIVCPPacket ivcp_packet;
   frameInfo.WriteToPacket( ivcp_packet );
   AddToStream( &ivcp_packet, IVCP_ID_Live_FrameInfo );
}

void CXmlGenerator::FillFrmObjInfo( std::deque<TrackedObjectEx*>& trackedObjectList )
{
   // 보내줄 구조체 정리
   CIVCPLiveFrameObjectInfo frameInfo;
   FILETIME ftCurTime;
   AfxGetLocalFileTime( &ftCurTime );
   frameInfo.m_nCameraUID  = m_pCamera->UID;
   frameInfo.m_cCameraGUID = m_pCamera->CameraGuid;
   //frameInfo.m_ftFrameTime  = m_pCamera->Timestamp; // 타임스탬프
   frameInfo.m_ftFrameTime = ftCurTime;
   frameInfo.m_fFrameRate  = m_pCamera->DesiredFrameRate;
   frameInfo.m_szFrameSize = m_pCamera->ProcImgSize;
   frameInfo.m_nObjectNum  = 0;

   int nObjCount        = (int)trackedObjectList.size();
   frameInfo.m_pObjects = new CIVCPFrameObjectEx[nObjCount];
   int i;
   for( i = 0; i < nObjCount; i++ ) {
      TrackedObjectEx* t_object = trackedObjectList[i];

      if( frameInfo.m_nObjectNum >= nObjCount ) break;
      if( t_object->Status & TO_STATUS_VERIFIED ) {
         CIVCPFrameObjectEx& frameObject = frameInfo.m_pObjects[frameInfo.m_nObjectNum];
         frameObject.m_nObjectID         = t_object->ID;
         frameObject.m_nObjectType       = t_object->Type;
         frameObject.m_ObjectBox.X       = t_object->X;
         frameObject.m_ObjectBox.Y       = t_object->Y;
         frameObject.m_ObjectBox.Width   = t_object->Width;
         frameObject.m_ObjectBox.Height  = t_object->Height;
         frameObject.m_nObjectStatus     = t_object->Status;

         // 추가 정보
         frameObject.m_fArea         = t_object->Area;
         frameObject.m_fMajorAxisLen = t_object->MajorAxisLength;
         frameObject.m_fMinorAxisLen = t_object->MinorAxisLength;
         frameObject.m_fSpeed        = t_object->Speed;

         if( t_object->RealHeight > 0.0f ) {
            frameObject.m_bSendRealValue    = TRUE;
            frameObject.m_fRealArea         = t_object->RealArea;
            frameObject.m_fRealHeight       = t_object->RealHeight;
            frameObject.m_fRealWidth        = t_object->RealWidth;
            frameObject.m_fRealMajorAxisLen = t_object->RealMajorAxisLength;
            frameObject.m_fRealMinorAxisLen = t_object->RealMinorAxisLength;
            frameObject.m_fRealSpeed        = t_object->RealSpeed;
         }
         frameObject.m_fNorSpeed = t_object->NorSpeed;

         int nEventNum           = 0;
         Event* pEvent           = NULL;
         nEventNum               = t_object->Events.GetNumItems();
         pEvent                  = t_object->Events.First();
         frameObject.m_pEvents   = new CIVCPFrameEvent[nEventNum];
         frameObject.m_nEventNum = 0;
         while( pEvent ) {
            if( frameObject.m_nEventNum >= nEventNum ) break;
            CIVCPFrameEvent& frameEvent = frameObject.m_pEvents[frameObject.m_nEventNum];
            frameEvent.m_nEventID       = pEvent->ID;
            frameEvent.m_nEventType     = pEvent->EvtRule->EventType;
            if( frameEvent.m_nEventType == ER_EVENT_LINE_CROSSING ) frameEvent.m_nEventType = ER_EVENT_PATH_PASSING;
            frameObject.m_nEventNum++;
            pEvent = pEvent->Next;
         }
         frameInfo.m_nObjectNum++;
      }
   }

   CIVCPPacket ivcp_packet;
   frameInfo.WriteToPacket( ivcp_packet );
   AddToStream( &ivcp_packet, IVCP_ID_Live_FrmObjInfo );
}

void CXmlGenerator::FillEventInfo( TrackedObjectEx* t_object, EventEx* pEvent )
{
   EventRuleEx* pEventRuleEx = (EventRuleEx*)pEvent->EvtRule;
   if( !pEventRuleEx ) return;
   if( !( pEventRuleEx->AlarmType & ER_AT_XML ) ) return;

   if( !( pEvent->Status & ( EV_STATUS_BEGUN | EV_STATUS_ENDED ) ) )
      return;

   // XML Alarm 메시지에는 이벤트가 DB에 저장될때 할당받는 아이디(EvtID)가 필요하므로
   // 반드시 AddEventToMDB  함수호출 뒤에 구현되어야 한다.

   // 보내줄 구조체 정리
   CIVCPLiveEventInfo eventInfo;
   eventInfo.m_nCameraUID  = m_pCamera->UID;
   eventInfo.m_cCameraGUID = m_pCamera->CameraGuid;

   if( pEvent->Status & EV_STATUS_BEGUN ) {
      eventInfo.m_nEventStatus = CIVCPLiveEventInfo::EVENT_STATUS_BEGUN;
      if( IsNullFileTime( pEvent->Time_EventBegun ) ) pEvent->Time_EventBegun = m_pCamera->CurFileTime;
      eventInfo.m_ftStartTime = pEvent->Time_EventBegun;
      eventInfo.m_ftEndTime   = pEvent->Time_EventBegun;
   } else if( pEvent->Status & EV_STATUS_ENDED ) {
      eventInfo.m_nEventStatus = CIVCPLiveEventInfo::EVENT_STATUS_ENDED;
      eventInfo.m_ftStartTime  = pEvent->Time_EventBegun;
      eventInfo.m_ftEndTime    = m_pCamera->CurFileTime;
   }
   eventInfo.m_nEventID   = pEvent->ID;
   eventInfo.m_nEventType = pEvent->Type;
   if( eventInfo.m_nEventType == ER_EVENT_LINE_CROSSING ) eventInfo.m_nEventType = ER_EVENT_PATH_PASSING;
   eventInfo.m_nEventRuleID = pEvent->EvtRule->ID;
   eventInfo.m_nObjectID    = t_object->ID;
   eventInfo.m_nObjectType  = t_object->Type;
   eventInfo.m_nNumObjects  = pEvent->NumObjects;
   eventInfo.m_ObjectBox    = ( IBox2D )( *t_object );
   eventInfo.m_nEventZoneID = pEvent->EvtZone->ID;
   strcpy( eventInfo.m_strEventZoneName, pEvent->EvtZone->Name );
   eventInfo.m_nProcWidth  = m_pCamera->ProcImgSize.Width;
   eventInfo.m_nProcHeight = m_pCamera->ProcImgSize.Height;

   if( ER_EVENT_COLOR_CHANGE == eventInfo.m_nEventType ) {
      eventInfo.m_ColorChangeInfo.m_RegionForegroundColor.B = t_object->RgnFrgColor.R;
      eventInfo.m_ColorChangeInfo.m_RegionForegroundColor.G = t_object->RgnFrgColor.G;
      eventInfo.m_ColorChangeInfo.m_RegionForegroundColor.R = t_object->RgnFrgColor.B;

      eventInfo.m_ColorChangeInfo.m_RegionBackgroundColor.B = t_object->RgnBkgColor.R;
      eventInfo.m_ColorChangeInfo.m_RegionBackgroundColor.G = t_object->RgnBkgColor.G;
      eventInfo.m_ColorChangeInfo.m_RegionBackgroundColor.R = t_object->RgnBkgColor.B;

      eventInfo.m_ColorChangeInfo.m_CenterForegroundColor.B = t_object->CntFrgColor.R;
      eventInfo.m_ColorChangeInfo.m_CenterForegroundColor.G = t_object->CntFrgColor.G;
      eventInfo.m_ColorChangeInfo.m_CenterForegroundColor.R = t_object->CntFrgColor.B;

      eventInfo.m_ColorChangeInfo.m_CenterBackgroundColor.B = t_object->CntBkgColor.R;
      eventInfo.m_ColorChangeInfo.m_CenterBackgroundColor.G = t_object->CntBkgColor.G;
      eventInfo.m_ColorChangeInfo.m_CenterBackgroundColor.R = t_object->CntBkgColor.B;
   } else if( ER_EVENT_DWELL == eventInfo.m_nEventType ) {
      eventInfo.m_fDwellTime = t_object->Time_Dwell;
   }

   eventInfo.m_bEvtFullImg = FALSE;
   eventInfo.m_bThumbnail  = FALSE;

   eventInfo.CreateBodybuffer( (byte*)0, 0 ); // 섬네일 없는 버전

   CIVCPPacket ivcp_packet;
   eventInfo.WriteToPacket( ivcp_packet );
   AddToStream( &ivcp_packet, IVCP_ID_Live_EventInfo );
}

void CXmlGenerator::FillObjectInfo( TrackedObjectEx* t_object )
{
   // XML Alarm 메시지에는 Object가 DB에 저장될때 할당받는 아이디(EvtID)가 필요하므로
   // 반드시 AddObjectToMDB  함수호출 뒤에 구현되어야 한다.

   // 보내줄 구조체 정리
   CIVCPLiveObjectInfo objectInfo;

   objectInfo.m_nCameraUID  = m_pCamera->UID;
   objectInfo.m_cCameraGUID = m_pCamera->CameraGuid;

   if( t_object->Status & TO_STATUS_VERIFIED_START ) {
      objectInfo.m_nObjectStatus = CIVCPLiveEventInfo::EVENT_STATUS_BEGUN; // IVCP_EVENT_STATUS_START;
      if( IsNullFileTime( t_object->Time_ObjectBegun ) )
         t_object->Time_ObjectBegun = m_pCamera->CurFileTime;
      objectInfo.m_ftStartTime = t_object->Time_ObjectBegun;
      objectInfo.m_ftEndTime   = t_object->Time_ObjectBegun;
   } else if( t_object->Status & TO_STATUS_TO_BE_REMOVED ) {
      objectInfo.m_nObjectStatus = CIVCPLiveEventInfo::EVENT_STATUS_ENDED; //IVCP_EVENT_STATUS_END;
      objectInfo.m_ftStartTime   = t_object->Time_ObjectBegun;
      objectInfo.m_ftEndTime     = m_pCamera->CurFileTime;
   }
   objectInfo.m_nObjectID = t_object->ID;

   if( IS_SUPPORT( __SUPPORT_FACE_RECOGNITION ) ) {
      if( m_pCamera->VAMode == VAE_MODE_FACE )
         objectInfo.m_nObjectType = TO_TYPE_FACE;
      else
         objectInfo.m_nObjectType = t_object->Type;
   } else
      objectInfo.m_nObjectType = t_object->Type;

   // 평균
   objectInfo.m_fAvgArea            = t_object->AvgArea;
   objectInfo.m_fAvgWidth           = t_object->AvgWidth;
   objectInfo.m_fAvgHeight          = t_object->AvgHeight;
   objectInfo.m_fAvgAspectRatio     = t_object->AvgAspectRatio;
   objectInfo.m_fAvgAxisLengthRatio = t_object->AvgAxisLengthRatio;
   objectInfo.m_fAvgMajorAxisLength = t_object->AvgMajorAxisLength;
   objectInfo.m_fAvgMinorAxisLength = t_object->AvgMinorAxisLength;
   objectInfo.m_fAvgNorSpeed        = t_object->AvgNorSpeed;
   objectInfo.m_fAvgSpeed           = t_object->AvgSpeed;
   if( t_object->AvgRealWidth > 0.0f ) {
      objectInfo.m_bSendRealValue = TRUE;

      objectInfo.m_fAvgRealArea            = t_object->AvgRealArea;
      objectInfo.m_fAvgRealDistance        = t_object->AvgRealDistance;
      objectInfo.m_fAvgRealWidth           = t_object->AvgRealWidth;
      objectInfo.m_fAvgRealHeight          = t_object->AvgRealHeight;
      objectInfo.m_fAvgRealMajorAxisLength = t_object->AvgRealMajorAxisLength;
      objectInfo.m_fAvgRealMinorAxisLength = t_object->AvgRealMinorAxisLength;
      objectInfo.m_fAvgRealSpeed           = t_object->AvgRealSpeed;
   }
   if( t_object->MaxWidth > 0.0f ) {
      objectInfo.m_bSendMaxValue = TRUE;

      objectInfo.m_nMaxWidth           = t_object->MaxWidth;
      objectInfo.m_nMaxHeight          = t_object->MaxHeight;
      objectInfo.m_fMaxArea            = t_object->MaxArea;
      objectInfo.m_fMaxAspectRatio     = t_object->MaxAspectRatio;
      objectInfo.m_fMaxAxisLengthRatio = t_object->MaxAxisLengthRatio;
      objectInfo.m_fMaxMajorAxisLength = t_object->MaxMajorAxisLength;
      objectInfo.m_fMaxMinorAxisLength = t_object->MaxMinorAxisLength;
      objectInfo.m_fMaxNorSpeed        = t_object->MaxNorSpeed;
      objectInfo.m_fMaxSpeed           = t_object->MaxSpeed;
      if( objectInfo.m_bSendRealValue ) {
         objectInfo.m_fMaxRealDistance = t_object->MaxRealDistance;
         objectInfo.m_fMaxRealSpeed    = t_object->MaxRealSpeed;
      }
   }
   if( t_object->MinWidth > 0.0f ) {
      objectInfo.m_bSendMinValue = TRUE;

      objectInfo.m_nMinWidth           = t_object->MinWidth;
      objectInfo.m_nMinHeight          = t_object->MinHeight;
      objectInfo.m_fMinArea            = t_object->MinArea;
      objectInfo.m_fMinAspectRatio     = t_object->MinAspectRatio;
      objectInfo.m_fMinAxisLengthRatio = t_object->MinAxisLengthRatio;
      objectInfo.m_fMinMajorAxisLength = t_object->MinMajorAxisLength;
      objectInfo.m_fMinMinorAxisLength = t_object->MinMinorAxisLength;
      objectInfo.m_fMinNorSpeed        = t_object->MinNorSpeed;
      objectInfo.m_fMinSpeed           = t_object->MinSpeed;
      if( objectInfo.m_bSendRealValue ) {
         objectInfo.m_fMinRealDistance = t_object->MinRealDistance;
         objectInfo.m_fMinRealSpeed    = t_object->MinRealSpeed;
      }
   }
   // 히스토그램
   t_object->ColorHistogram.Finalize( objectInfo.m_fHistogram );
   objectInfo.m_nProcWidth  = m_pCamera->ProcImgSize.Width;
   objectInfo.m_nProcHeight = m_pCamera->ProcImgSize.Height;

   objectInfo.m_bThumbnail  = FALSE;
   objectInfo.m_bObjFullImg = FALSE;

   CIVCPPacket ivcp_packet;
   objectInfo.WriteToPacket( ivcp_packet );
   AddToStream( &ivcp_packet, IVCP_ID_Live_ObjectInfo );
}

void CXmlGenerator::FillEventZoneInfo( BOOL bNullInfo )
{
   CIVCPLiveEventZoneInfo evtZoneInfo;
   evtZoneInfo.m_cCameraGUID = m_pCamera->CameraGuid;
   evtZoneInfo.m_nCameraUID  = m_pCamera->UID; // 카메라 UID
   AfxGetLocalFileTime( &evtZoneInfo.m_ftFrameTime ); // 현재 시간

   if( bNullInfo ) // 이동중이면 NULL 정보를 보낸다.
   {
   } else {
      m_pCamera->m_csActivePreset.Lock();
      m_pCamera->CS_EventDetector.Lock();

      int nPresetID = -1; // -1: 프리셋 아님, 0이상: 프리셋
      // 현재 활성화된 EventDetector 를 알아낸다.
      EventDetection* pEventDetector = &m_pCamera->MainConfig_EventDetection;
      if( m_pCamera->m_nPTZState & PTZState_PresetTour ) {
         if( m_pCamera->m_pActivePreset ) {
            pEventDetector = &m_pCamera->m_pActivePreset->Config_EventDetection;
            nPresetID      = m_pCamera->m_pActivePreset->PresetNo;
         }
      }

      evtZoneInfo.m_nWidth  = pEventDetector->EventZones.Width;
      evtZoneInfo.m_nHeight = pEventDetector->EventZones.Height;

      // 이벤트존 크기만큼 폴리곤을 추출해낸다.
      int nZoneNum              = pEventDetector->EventZones.GetNumItems();
      evtZoneInfo.m_pEventZones = new CIVCPEventZone[nZoneNum];
      EventZoneEx* pEvtZone     = (EventZoneEx*)pEventDetector->EventZones.First();
      while( pEvtZone ) {
         if( pEvtZone->EvtRule ) {
            EventRuleEx* pEvtRule       = (EventRuleEx*)pEvtZone->EvtRule;
            CIVCPEventZone& evtZoneItem = evtZoneInfo.m_pEventZones[evtZoneInfo.m_nZoneNum];
            evtZoneItem.m_nZoneID       = pEvtZone->ID;
            evtZoneItem.m_nPresetID     = nPresetID;
            evtZoneItem.m_nPriority     = pEvtZone->Priority;
            evtZoneItem.m_nOptions      = pEvtZone->Options;
            evtZoneItem.m_nWidth        = pEvtZone->RefImgSize.Width;
            evtZoneItem.m_nHeight       = pEvtZone->RefImgSize.Height;
            strcpy( evtZoneItem.m_strName, pEvtZone->Name );
            evtZoneItem.m_ptCenter     = pEvtZone->CenterPos;
            evtZoneItem.m_nVerticesNum = pEvtZone->Vertices.Length;
            if( evtZoneItem.m_nVerticesNum > 0 ) {
               evtZoneItem.m_pVertices = new IPoint2D[evtZoneItem.m_nVerticesNum];
               for( int i = 0; i < evtZoneItem.m_nVerticesNum; i++ ) {
                  evtZoneItem.m_pVertices[i] = pEvtZone->Vertices[i];
               }
            }
            evtZoneItem.m_nRuleOptions    = pEvtRule->Options;
            evtZoneItem.m_nRuleEventType  = pEvtRule->EventType;
            evtZoneItem.m_nRuleObjectType = pEvtRule->ObjectType;

            strcpy( evtZoneItem.m_strRuleName, pEvtRule->Name );

            // 룰 설정을 복사한다.
            evtZoneItem.m_aAbandoned.m_fDetectionTime         = pEvtRule->ERC_Abandoned.DetectionTime;
            evtZoneItem.m_aCounting.m_nCount                  = pEvtRule->ERC_Counting.Count;
            evtZoneItem.m_aCounting.m_fAspectRatio            = pEvtRule->ERC_Counting.AspectRatio;
            evtZoneItem.m_aCounting.m_aMajorAxis              = pEvtRule->ERC_Counting.MajorAxis;
            evtZoneItem.m_aDirectionalMotion.m_fRange         = pEvtRule->ERC_DirectionalMotion.Range;
            evtZoneItem.m_aDirectionalMotion.m_fDirection     = pEvtRule->ERC_DirectionalMotion.Direction;
            evtZoneItem.m_aDirectionalMotion.m_fDetectionTime = pEvtRule->ERC_DirectionalMotion.DetectionTime;
            evtZoneItem.m_aFlooding.m_nDirection              = pEvtRule->ERC_WaterLevel.Direction;
            evtZoneItem.m_aFlooding.m_fDetectionTime          = pEvtRule->ERC_WaterLevel.DetectionTime;
            evtZoneItem.m_aFlooding.m_fThreshold              = pEvtRule->ERC_WaterLevel.Threshold;
            evtZoneItem.m_aFlooding.m_CrossingLine            = pEvtRule->ERC_WaterLevel.CrossingLine;
            evtZoneItem.m_aLineCrossing.m_nDirection          = pEvtRule->ERC_LineCrossing.Direction;
            evtZoneItem.m_aLineCrossing.m_CrossingLine        = pEvtRule->ERC_LineCrossing.CrossingLine;

            EventRuleConfig_PathPassing& ppFrom     = pEvtRule->ERC_PathPassing;
            evtZoneItem.m_aPathPassing.m_nDirection = ppFrom.Direction;
            evtZoneItem.m_aPathPassing.m_nItemNum   = ppFrom.ZoneTree.Children.Length;
            if( evtZoneItem.m_aPathPassing.m_nItemNum > 0 ) {
               evtZoneItem.m_aPathPassing.m_pItems = new CIVCPEventRule_PathPassingItem[evtZoneItem.m_aPathPassing.m_nItemNum];
               for( int i = 0; i < evtZoneItem.m_aPathPassing.m_nItemNum; i++ ) {
                  CIVCPEventRule_PathPassingItem& ppItem = evtZoneItem.m_aPathPassing.m_pItems[i];
                  ppItem.m_nID                           = ppFrom.ZoneTree.Children[i].ID;
                  ppItem.m_ptCenter                      = ppFrom.ZoneTree.Children[i].CenterPos;
                  ppItem.m_nVerticesNum                  = ppFrom.ZoneTree.Children[i].Vertices.Length;
                  if( ppItem.m_nVerticesNum > 0 ) {
                     ppItem.m_pVertices = new IPoint2D[ppItem.m_nVerticesNum];
                     for( int j = 0; j < ppItem.m_nVerticesNum; j++ ) {
                        ppItem.m_pVertices[j] = ppFrom.ZoneTree.Children[i].Vertices[j];
                     }
                  }
               }
            }
            evtZoneItem.m_aPathPassing.m_nArrowNum = ppFrom.Arrows.GetNumItems();
            if( evtZoneItem.m_aPathPassing.m_nArrowNum > 0 ) {
               evtZoneItem.m_aPathPassing.m_pArrows = new CIVCPEventRule_PathPassingArrow[evtZoneItem.m_aPathPassing.m_nArrowNum];
               for( int i = 0; i < evtZoneItem.m_aPathPassing.m_nArrowNum; i++ ) {
                  Arrow* pArrow                                          = ppFrom.Arrows.Get( i );
                  ILine2D il2d                                           = ( ILine2D )( *pArrow );
                  evtZoneItem.m_aPathPassing.m_pArrows[i].m_nSplitZoneID = pArrow->SplitZoneID;
                  evtZoneItem.m_aPathPassing.m_pArrows[i].m_lnArrow      = il2d;
               }
            }

            evtZoneInfo.m_nZoneNum++;
         }
         pEvtZone = (EventZoneEx*)pEvtZone->Next;
      }

      m_pCamera->CS_EventDetector.Unlock();
      m_pCamera->m_csActivePreset.Unlock();
   }

   CIVCPPacket ivcp_packet;
   evtZoneInfo.WriteToPacket( ivcp_packet );
   AddToStream( &ivcp_packet, IVCP_ID_Live_EventZoneInfo );
}

void CXmlGenerator::FillEventZoneCounterInfo()
{
   CIVCPLiveEventZoneCountInfo zoneCountInfo;
   zoneCountInfo.m_cCameraGUID = m_pCamera->CameraGuid;
   zoneCountInfo.m_nCameraUID  = m_pCamera->UID;
   AfxGetLocalFileTime( &zoneCountInfo.m_ftFrameTime );

   m_pCamera->CS_EventDetector.Lock();
   int nZoneNum                = m_pCamera->EventDetector.EventZones.GetNumItems();
   zoneCountInfo.m_pZoneCounts = new CIVCPEventZoneCount[nZoneNum];
   EventZoneEx* pEvtZone       = (EventZoneEx*)m_pCamera->EventDetector.EventZones.First();
   while( pEvtZone ) {
      if( pEvtZone->EvtRule ) {
         CIVCPEventZoneCount& zone_count = zoneCountInfo.m_pZoneCounts[zoneCountInfo.m_nZoneNum];
         zone_count.m_nZoneID            = pEvtZone->ID;
         zone_count.m_nZoneCount         = pEvtZone->EvtRule->ERC_Counting.Count;
         if( ER_EVENT_DWELL == pEvtZone->EvtRule->EventType ) {
            zone_count.m_CurDwellObjectCount   = pEvtZone->CurDwellObjectCount;
            zone_count.m_TotalDwellObjectCount = pEvtZone->TotalDwellObjectCount;
            zone_count.m_AverageDwellTime      = pEvtZone->AverageDwellTime;
         }
         zoneCountInfo.m_nZoneNum++;
      }
      pEvtZone = (EventZoneEx*)m_pCamera->EventDetector.EventZones.Next( pEvtZone );
   }
   m_pCamera->CS_EventDetector.Unlock();

   CIVCPPacket ivcp_packet;
   zoneCountInfo.WriteToPacket( ivcp_packet );
   AddToStream( &ivcp_packet, IVCP_ID_Live_EventZoneCount );
}

﻿#ifndef WEBSOCKET_H
#define WEBSOCKET_H

#ifdef __LIB_SIMPLEWEBSOCKET

// NOTE(yhpark) : use boost::asio instead of ASIO stand alone.
//#define ASIO_STANDALONE // asio don't depend on boost.
//#define USE_STANDALONE_ASIO // SimpleWebSocket don't use boost::asio

#include "server_ws.hpp"

#include "IntelliVIX.h"
#include "LocalSystem.h"

class CIntelliVIXApp;

using namespace std;

using WsServer = SimpleWeb::SocketServer<SimpleWeb::WS>;

class Websocket {
public:
   // singleton.
   static Websocket& getInstance();

   void Prepare( CIntelliVIXApp* pIntellivix, ushort port = 8088 );
   void StartThread();
   void StopThread();

   void SendText( shared_ptr<WsServer::Connection> connection,
                  const std::string& message,
                  const std::function<void( const SimpleWeb::error_code& )>& callback = nullptr );
   void SendBinary( shared_ptr<WsServer::Connection> connection,
                    const std::shared_ptr<WsServer::SendStream>& send_stream,
                    const std::function<void( const SimpleWeb::error_code& )>& callback = nullptr );
   void BroadcastBinary( const std::shared_ptr<WsServer::SendStream>& send_stream,
                         const std::function<void( const SimpleWeb::error_code& )>& callback = nullptr );
   void BroadcastText( const std::string& message_to_send,
                       const std::function<void( const SimpleWeb::error_code& )>& callback = nullptr );

protected:
   std::thread m_thread;
   WsServer m_ws_server;

   ///
   /// === Directory Service. Intellivix ===
   ///
   void ServiceAddCamera();
   void ServiceModifyCamera();
   void ServiceListCamera();
   void ServiceRemoveCamera();
   void ServiceMeta();

   ///
   /// Prcoessing Camera.
   ///
   std::string GetCameraID( CCamera* pCamera );
   CCamera* GetCamera( const std::string& strID );
   int AddCamera( std::string& strReply );
   int ModifyCamera( string& camID, std::string& strReply );
   int GetCameraList( std::string& strReply );
   int RemoveCamera( std::string camID, std::string& replyMsg );

   CIntelliVIXApp* m_pIntellivix = nullptr;
   CLocalSystem* m_pLocalSystem  = nullptr;

   ///
   /// === Directory Service. From Exmaples ===
   ///
   void ServiceExamEcho();
   void ServiceExamEchoThrice();
   void ServiceExamBroadCast();

   Websocket();
   virtual ~Websocket();
};

#endif // __LIB_SIMPLEWEBSOCKET
#endif // WEBSOCKET_H

#pragma once

#include "IVCP_PacketStruct.h"
#include "vacl_objtrack.h"
#include "vacl_event.h"
#include "vacl_water.h"
#include "EventGrouping.h"

#include "PTZVector.h"

using namespace VACL;

class CCPacket_VideoMetaData;
class CIVCP_FrameMetaData;
class EventGrouping;

////////////////////////////////////////////////////////////
//
// Enumerations
//
////////////////////////////////////////////////////////////

enum VIDEO_FRAME_DESCRIPTOR {
   VF_DESC_FRAME_TIME = 0,
};

enum FrameState {
   FrameState_BackgroundModeling          = 0x00000001,
   FrameState_SuddenSceneChangeIsDetected = 0x00000002,
   FrameState_OpenSerialPortIsFailed      = 0x00000004,
   FrameState_GoingToPreset               = 0x00000008,
   FrameState_PTZCameraMoving             = 0x00000010,
   FrameState_PTZPosInitializing          = 0x00000020,
   FrameState_FrameUpdated                = 0x00000040, // 디코딩된 영상추출모드 : 지정된 시간의 디코딩된 영상을 항상얻기 때문에 본 플레그는 항상 On이 된다.
   // Raw Data 추출모드     : 지정된 시간까지 영상을 디코딩하기 위해서 Raw(Encode) 비디오데이터 추출을 모두 완료한 경우
   FrameState_VideoAnalyticsOngoing = 0x00000080,
   FrameState_PTZGotoHomePos        = 0x00000100,
   FrameState_SinglePTZTracking     = 0x00000200,
};

enum VideoSrcType {
   VideoSrcType_Live   = 0,
   VideoSrcType_Play   = 1,
   VideoSrcType_Record = 2,
};

const int VideoSrcTypeNum = 2;

///////////////////////////////////////////////////////////////////////////////////
//
// class : IVXVideoFrameInfo
//
///////////////////////////////////////////////////////////////////////////////////

class IVXVideoFrameInfo {
public:
   int m_nFrmNo;
   int m_nChNo;
   int m_nFrmSize;
   FILETIME m_ftFrmTime;
   double m_dfNormalPlayTime;
   short m_nState; // enum FrameState 참고
   uint m_nChState; // enum CamState 참고
   uint m_nChPTZState; // enum PTZState 참고.
   uint m_nChPTZStateEx; // enum PTZStateEx 참고.
   int m_nVideoSrcType; // enum VideoSrcType 참고. 실시간/재생
   DWORD m_nVideoFrmFlag;
   int m_nWidth;
   int m_nHeight;
   int m_nBitrate;
   int m_nCodecType;
   uint m_nPresetMoveRemainTime;

public:
   CIVCPLiveEventZoneCountInfo m_EventZoneCountInfo;

public:
   int m_nTrackedObjectCount;
   std::deque<TrackedObjectEx*> m_TrackedObjects;

public:
   int m_nVideoDataPos;

public:
   CCriticalSection m_CS;

public:
   FPTZVector m_PTZPos;

public:
   BOOL m_bReadWriteTrackedObjectList;

public:
   IVXVideoFrameInfo();
   ~IVXVideoFrameInfo();

public:
   IVXVideoFrameInfo& operator=( IVXVideoFrameInfo& videoFrameInfo );

public:
   void InitMemberVariables();
   void PushTrackedObject( TrackedObjectEx* pTO );
   void DeleteAllTrackedObject();

public:
   BOOL ReadXML( CXMLIO* pIO );
   BOOL WriteXML( CXMLIO* pIO );
   void ReadFrameHeader( FileIO* pBuff );
   void WriteFrameHeader( FileIO* pBuff );
};

////////////////////////////////////////////////////////////
//
// Class : CVideoFrame
//
////////////////////////////////////////////////////////////
// 비디오 프레임의 메타데이터(추적물체 및 이벤트 정보 등)를 담는 클래스이다.

class CVideoFrame {
public:
   CVideoFrame();
   virtual ~CVideoFrame();

public:
   IVXVideoFrameInfo m_VideoFrameInfo;

public:
   int m_nVideoSrcType; // enum VideoSrcType
   int m_nUpdateCount;
   FILETIME m_ftTimestamp;

public:
   BOOL m_bImportImageData;
   BArray1D m_ImageData;
   BYTE* m_pYUY2Data; // MS: CVideoFrame 내부에서 사용하지 않는 변수임...

public:
   // 비디오 사이즈가 변경되는 경우 때문에 이전의 비디오 사이즈 저장 (mkjang-141229)
   int m_nPrevWidth;
   int m_nPrevHeight;

public:
   int GetChannelNo() { return m_VideoFrameInfo.m_nChNo; }
   int GetWidth() { return m_VideoFrameInfo.m_nWidth; }
   int GetHeight() { return m_VideoFrameInfo.m_nHeight; }
   int GetVideoFrameFlag() { return m_VideoFrameInfo.m_nVideoFrmFlag; }
   int GetFrameState() { return m_VideoFrameInfo.m_nState; }
   FILETIME& GetFrameTime() { return m_VideoFrameInfo.m_ftFrmTime; }

public:
   void LockVideoFrameInfo() { m_VideoFrameInfo.m_CS.Lock(); }
   void UnlockVideoFrameInfo() { m_VideoFrameInfo.m_CS.Unlock(); }

public:
   void Create( int nWidth, int nHeight, int nVideoType );
   void Close();
   void Copy( CVideoFrame& from );
   void CopyVideoFrameInfo( IVXVideoFrameInfo& s_videoFrameInfo, BOOL bExcludeTrackedObjects = TRUE );
   void CopyTrackedObjects( IVXVideoFrameInfo& s_videoFrameInfo );
   BOOL CopyTrackedObjects( ObjectTracking* pObjectTracker, CCriticalSection* csOT,
                            EventDetection* pEventDetector, CCriticalSection* csED,
                            EventGrouping* pEventGrouper,
                            WaterLevelDetection* pWaterLevelDetector, FPoint2D& scale );
   BOOL CopyTrackedObjects( CIVCP_FrameMetaData* pFrameMetaData );
   BOOL PushTrackedObject( TrackedObjectEx* t_object, FPoint2D& scale );
   void DeleteAllTrackedObjects();
   void SetImage( byte* pSrcImage, int width, int height, BOOL bImportImg = FALSE );
};

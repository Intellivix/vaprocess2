﻿#pragma once

//////////////////////////////////////////////////////////////////////////
//
// class CRtspParameter
//
//////////////////////////////////////////////////////////////////////////

class CCamera;
class CVideoChannel;
class CLocalSystem;
class CIvcpRtspSender;

class CRtspParameter {
private:
   int ChannelNo;
   int UID;

   CCamera* m_pCamera;
   CVideoChannel* VC;
   CLocalSystem* System;
   CIvcpRtspSender* ivcpLiveSender;

private:
   BOOL ParseRTSPRarameter( char const* szInData, std::string& strRequest );
   BOOL ParseRTSPXMLValue( const std::string& strRequest, std::string& strValue );

   BOOL OnRTSP_VideoChannelSetting( const std::string& strRequest );
   BOOL OnRTSP_CameraSettingAll( const std::string& strRequest, int nType );
   BOOL OnRTSP_VideoAnalysisSetting( const std::string& strRequest );
   BOOL OnRTSP_CameraInfo( const std::string& strRequest );
   BOOL OnRTSP_ResetEventZoneCount( const std::string& strRequest );
   BOOL OnRTSP_ResetCountSchedule( const std::string& strRequest );

public:
   explicit CRtspParameter( CCamera* pCamera );
   ~CRtspParameter();

   void OnRTSPSetParameter( char const* fullRequestStr );
   void OnRTSPGetParameter( char const* fullRequestStr );
};

/*
 *
 * Copyright 2015, Google Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "StdAfx.h"

#include <iostream>
#include <fstream>
#include <string>
#include <memory>
#include <vector>

//#ifndef __WIN32
//#include <boost/process.hpp>
//#include <boost/asio.hpp>
//using namespace boost::process;
//using namespace boost::process::initializers;
//#endif

#if defined(BOOST_WINDOWS_API)
#   include <Windows.h>
#elif defined(BOOST_POSIX_API)
#   include <sys/wait.h>
#   include <errno.h>
#endif

#include <grpc++/grpc++.h>
#include "intellivix.grpc.pb.h"
#include "intellivix_server.h"
#include "LocalSystem.h"

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;

using intellivix::Intellivix;
using intellivix::Profile;
using intellivix::ProfileListReply;
using intellivix::ProfileListRequest;
using intellivix::StreamListReply;
using intellivix::StreamListRequest;
using intellivix::StreamReply;
using intellivix::StreamRequest;
using intellivix::SysInfoRequest;
using intellivix::SysInfoReply;

using namespace std;

////////////////////////////////////////////////////////////////////////////////////////
// class gRPCCODE
////////////////////////////////////////////////////////////////////////////////////////

class gRPCCODE
{
public:
   int     m_nCode;
   string  m_strCode;

   enum
   {
      SUCCESS = 0,
      CH_ID_INVALID,
      CH_INFO_INVALID,
      CH_ADD_FAILED,
      CH_REMOVE_FAILED,
      CH_MODIFY_FAILED,
      CH_CODEC_NONE,
      CH_CONNECTION_TYPE_INVALID,
   };

   gRPCCODE(int nCode, const string& strCode) :
      m_nCode(nCode), m_strCode(strCode)
   {}
   gRPCCODE(int nCode) :
      m_nCode(nCode)
   {
      switch (nCode)
      {
      case SUCCESS:                          m_strCode = "Success";                                break;
      case CH_ID_INVALID:                    m_strCode = "Channel id invalid";                     break;
      case CH_INFO_INVALID:                  m_strCode = "Channel information invalid";            break;
      case CH_ADD_FAILED:                    m_strCode = "Channel add failed";                     break;
      case CH_REMOVE_FAILED:                 m_strCode = "Channel remove failed";                  break;
      case CH_MODIFY_FAILED:                 m_strCode = "Channel modify failed";                  break;
      case CH_CODEC_NONE:                    m_strCode = "Channel codec nothing";                  break;
      case CH_CONNECTION_TYPE_INVALID:       m_strCode = "Channel connection type invalid";        break;
      default:
         break;
      }
   }

   void ReportError()
   {
      logi("gRPC ERROR --- [Code : %d] [String : %s]\n", m_nCode, m_strCode.c_str());
   }
};

////////////////////////////////////////////////////////////////////////////////////////
// class IntellivixServiceImpl
////////////////////////////////////////////////////////////////////////////////////////

class IntellivixServiceImpl final : public Intellivix::Service
{
public:
   CLocalSystem* m_pLocalSystem;
   IntellivixServiceImpl(CLocalSystem* pLocalSystem);

private:
   CCamera* GetCamera         (const string& strID);
   string   GetCameraID       (CCamera* pCamera);
   int      GetProtocalType   (const CIPCameraInfo& info);
   string   GetProfile        (const CIPCameraInfo& info);
   int      SetCameraInfo     (const string& strURL, int nType, CIPCameraInfo& info_1, CIPCameraInfo& info_2);

public:
   Status AddStream        (ServerContext* context, const StreamRequest* request, StreamReply* reply) override;
   Status RemoveStream     (ServerContext* context, const StreamRequest* request, StreamReply* reply) override;
   Status ModifyStream     (ServerContext* context, const StreamRequest* request, StreamReply* reply) override;
   Status GetStreamList    (ServerContext* context, const StreamListRequest* request, StreamListReply* reply) override;
   Status GetProfileList   (ServerContext* context, const ProfileListRequest* request, ProfileListReply* reply) override;
   Status SysInfo          (ServerContext* context, const SysInfoRequest* request, SysInfoReply* reply) override;
};

////////////////////////////////////////////////////////////////////////////////////////

IntellivixServiceImpl::IntellivixServiceImpl(CLocalSystem* pLocalSystem) :
   m_pLocalSystem(pLocalSystem)
{

}

CCamera* IntellivixServiceImpl::GetCamera(const string& strID)
{
   vector<string> vecSplit_ID;
   sutil::GetSplitStringList(strID, ',', vecSplit_ID);

   if (vecSplit_ID.size() < 2)
      throw gRPCCODE(gRPCCODE::CH_ID_INVALID);
   CCamera* pCamera = m_pLocalSystem->GetCameraFromGuid(vecSplit_ID[0]);
   if (!pCamera)
   {
      pCamera = m_pLocalSystem->GetCameraFromUID(stoi(vecSplit_ID[1]));
      if (!pCamera)
         throw gRPCCODE(gRPCCODE::CH_ID_INVALID);
   }

   return pCamera;
}

string IntellivixServiceImpl::GetCameraID(CCamera* pCamera)
{
   string strCameraID = sutil::sformat("%s,%d,%d", pCamera->CameraGuid, pCamera->UID, pCamera->ChannelNo);
   return strCameraID;
}

int IntellivixServiceImpl::GetProtocalType(const CIPCameraInfo& info)
{
   int nRet = 0;
   switch (info.m_nIPCameraType)
   {
   case IPCameraType_RTSP:     nRet = 1;   break;
   case IPCameraType_Onvif:    nRet = 2;   break;
   default:
      throw gRPCCODE(gRPCCODE::CH_CONNECTION_TYPE_INVALID);
      break;
   }

   return nRet;
}

string IntellivixServiceImpl::GetProfile(const CIPCameraInfo& info)
{
   string strCodec;
   switch (info.m_nVideoFormat_Cap)
   {
   case VCODEC_ID_MPEG4:         strCodec = "MPEG4";           break;
   case VCODEC_ID_MJPEG:         strCodec = "MJPEG";           break;
   case VCODEC_ID_RAW:           strCodec = "RAW";             break;
   case VCODEC_ID_H264:          strCodec = "H264";            break;
   case VCODEC_ID_XVID:          strCodec = "XVID";            break;
   case VCODEC_ID_H263:          strCodec = "H263";            break;
   case VCODEC_ID_MPEG1VIDEO:    strCodec = "MPEG1VIDEO";      break;
   case VCODEC_ID_MPEG2VIDEO:    strCodec = "MPEG2VIDEO";      break;
   case VCODEC_ID_H265:          strCodec = "H265";            break;

   case VCODEC_ID_NONE:
   default:
      throw gRPCCODE(gRPCCODE::CH_CODEC_NONE);
      break;
   }

   string strProfile = sutil::sformat("%dx%d,%s,%d", info.m_nWidth, info.m_nHeight, strCodec.c_str(), (int)info.m_fFPS_Cap);

   return strProfile;
}

int IntellivixServiceImpl::SetCameraInfo(const string& strURL, int nType, CIPCameraInfo& info_1, CIPCameraInfo& info_2)
{
   vector<string> vecSplit_URL;

   // URL Info.
   // RTSP  : IP,Port,URL1,FPS,Port,URL2,FPS
   // Onvif : IP,Port,StreamNo1,FPS,Port,StreamNo2,FPS
   sutil::GetSplitStringList(strURL, ',', vecSplit_URL);

   if (vecSplit_URL.size() < 4)
      throw gRPCCODE(gRPCCODE::CH_INFO_INVALID);

   switch (nType)
   {
   case 1: // RTSP
      info_1.m_strIPAddress = vecSplit_URL[0];
      info_1.m_nPortNo      = stoi(vecSplit_URL[1]);
      info_1.m_strURL       = vecSplit_URL[2];
      info_1.m_fFPS_Cap     = stoi(vecSplit_URL[3]);
      if (7 == vecSplit_URL.size())
      {
         info_2 = info_1;
         info_2.m_nPortNo  = stoi(vecSplit_URL[4]);
         info_2.m_strURL   = vecSplit_URL[5];
         info_2.m_fFPS_Cap = stoi(vecSplit_URL[6]);
      }
      break;
#ifdef __SUPPORT_ONVIF
   case 2: // ONVIF
      info_1.m_strIPAddress   = vecSplit_URL[0];
      info_1.m_nPortNo        = 554;
      info_1.m_nPTZPortNo     = stoi(vecSplit_URL[1]);
      info_1.m_nVideoStreamNo = stoi(vecSplit_URL[2]);
      info_1.m_fFPS_Cap       = stoi(vecSplit_URL[3]);
      if (7 == vecSplit_URL.size())
      {
         info_2 = info_1;
         info_2.m_nPTZPortNo     = stoi(vecSplit_URL[4]);
         info_2.m_nVideoStreamNo = stoi(vecSplit_URL[5]);
         info_2.m_fFPS_Cap       = stoi(vecSplit_URL[6]);
      }
      break;
#endif
   default:
      throw gRPCCODE(gRPCCODE::CH_CONNECTION_TYPE_INVALID);
      return 1;
   }

   return (DONE);
}

Status IntellivixServiceImpl::AddStream(ServerContext* context, const StreamRequest* request, StreamReply* reply)
{
   logi("gRPC  --- START [%s] [URL:%s]\n", __FUNCTION__, request->stream_url().c_str());

   reply->set_error_code(0);
   reply->set_error_string("Success");

   int nChannelNo = m_pLocalSystem->CameraNum;
   CCamera* pCamera = new CCamera(m_pLocalSystem, nChannelNo);
   CVideoChannelInfo& vcInfo  = pCamera->VC->GetVideoChannelInfo();
   CIPCameraInfo& IPCamInfo_0 = pCamera->VC->m_IPCamInfoArr[0];
   CIPCameraInfo& IPCamInfo_1 = pCamera->VC->m_IPCamInfoArr[1];

   try
   {
      int nType      = request->protocol_type();
      string strName = request->stream_name();
      string strURL  = request->stream_url();

      strncpy(pCamera->CameraName, strName.c_str(), MAX_CAMERA_NAME - 1);
      vcInfo.m_nVideoChannelType = VideoChannelType_IPCamera;

      IPCamInfo_0.m_strUserName = request->userid();
      IPCamInfo_0.m_strPassword = request->passwd();

      switch (nType)
      {
      case 1: // RTSP
         IPCamInfo_0.m_nCompanyID     = CompanyID_RTSP;
         IPCamInfo_0.m_nIPCameraType  = IPCameraType_RTSP;
         break;
#ifdef __SUPPORT_ONVIF
      case 2: // ONVIF
         IPCamInfo_0.m_nCompanyID     = CompanyID_Onvif;
         IPCamInfo_0.m_nIPCameraType  = IPCameraType_Onvif;
         break;
#endif
      default:
         throw gRPCCODE(gRPCCODE::CH_CONNECTION_TYPE_INVALID);
         break;
      }

      SetCameraInfo(strURL, nType, IPCamInfo_0, IPCamInfo_1);

      if ((DONE) != m_pLocalSystem->AddCamera(pCamera))
         throw gRPCCODE(gRPCCODE::CH_ADD_FAILED);

      reply->set_stream_id(GetCameraID(pCamera));

      m_pLocalSystem->SetSystemSetupToBeSaved();
   }
   catch (gRPCCODE& code)
   {
      SAFE_DELETE(pCamera);

      reply->set_error_code(code.m_nCode);
      reply->set_error_string(code.m_strCode);
      code.ReportError();
   }

   logi("gRPC  --- END [%s]\n", __FUNCTION__);

   return Status::OK;
}

Status IntellivixServiceImpl::RemoveStream(ServerContext* context, const StreamRequest* request, StreamReply* reply)
{
   logi("gRPC  --- START [%s] [ID:%s]\n", __FUNCTION__, request->stream_id().c_str());

   reply->set_error_code(0);
   reply->set_error_string("Success");

   try
   {
      string strID = request->stream_id();
      CCamera* pCamera = GetCamera(strID);

      if ((DONE) != m_pLocalSystem->RemoveCamera(pCamera))
         throw gRPCCODE(gRPCCODE::CH_REMOVE_FAILED);

      m_pLocalSystem->SetSystemSetupToBeSaved();
   }
   catch (gRPCCODE& code)
   {
      reply->set_error_code(code.m_nCode);
      reply->set_error_string(code.m_strCode);
      code.ReportError();
   }

   logi("gRPC  --- END [%s]\n", __FUNCTION__);

   return Status::OK;
}

Status IntellivixServiceImpl::ModifyStream(ServerContext* context, const StreamRequest* request, StreamReply* reply)
{
   logi("gRPC  --- START [%s] [ID:%s]\n", __FUNCTION__, request->stream_id().c_str());

   reply->set_error_code(0);
   reply->set_error_string("Success");

   try
   {
      string strID   = request->stream_id();
      int nType      = request->protocol_type();
      string strName = request->stream_name();
      string strURL  = request->stream_url();
      vector<string> vecSplit_ID, vecSplit_URL;

      CCamera* pCamera = GetCamera(strID);

      m_pLocalSystem->InitializeWizardSetup(pCamera->ChannelNo);
      CLocalSystem* pSystemTemp = m_pLocalSystem->GetSystemTemp();
      if (!pSystemTemp)
         throw gRPCCODE(gRPCCODE::CH_MODIFY_FAILED);
      CCamera* pCameraTemp = pSystemTemp->GetCameraFromGuid(pCamera->CameraGuid);
      if (!pCameraTemp)
         throw gRPCCODE(gRPCCODE::CH_MODIFY_FAILED);

      CVideoChannelInfo& vcInfo  = pCameraTemp->VC->GetVideoChannelInfo();
      CIPCameraInfo& IPCamInfo_0 = pCameraTemp->VC->m_IPCamInfoArr[0];
      CIPCameraInfo& IPCamInfo_1 = pCameraTemp->VC->m_IPCamInfoArr[1];

      IPCamInfo_0.m_strUserName = request->userid();
      IPCamInfo_0.m_strPassword = request->passwd();

      SetCameraInfo(strURL, nType, IPCamInfo_0, IPCamInfo_1);

      m_pLocalSystem->FinalizeWizardSetup(TRUE);
      m_pLocalSystem->SetSystemSetupToBeSaved();
   }
   catch (gRPCCODE& code)
   {
      m_pLocalSystem->FinalizeWizardSetup(FALSE);
      reply->set_error_code(code.m_nCode);
      reply->set_error_string(code.m_strCode);
      code.ReportError();
   }

   logi("gRPC  --- END [%s]\n", __FUNCTION__);

   return Status::OK;
}

Status IntellivixServiceImpl::GetStreamList(ServerContext* context, const StreamListRequest* request, StreamListReply* reply)
{
   logi("gRPC  --- START [%s] [Type:%d]\n", __FUNCTION__, request->req_type());

   reply->set_error_code(0);
   reply->set_error_string("Success");

   try
   {
      StreamListReply::Stream  *pStreams = NULL;

      CCriticalSectionSP co(m_pLocalSystem->csCameraList);
      std::deque<CCamera*>& Cameras = m_pLocalSystem->Cameras;

      for (size_t ii = 0; ii < Cameras.size(); ii++)
      {
         CCamera* pCamera = Cameras[ii];
         if (!pCamera)  continue;

         CIPCameraInfo& IPCamInfo_0 = pCamera->VC->m_IPCamInfoArr[0];
         CIPCameraInfo& IPCamInfo_1 = pCamera->VC->m_IPCamInfoArr[1];
         string strURL;

         pStreams = reply->add_streams();
         pStreams->set_stream_id(GetCameraID(pCamera));
         pStreams->set_stream_name(pCamera->CameraName);
         pStreams->set_protocol_type(GetProtocalType(IPCamInfo_0));
         pStreams->set_userid(IPCamInfo_0.m_strUserName);
         pStreams->set_passwd(IPCamInfo_0.m_strPassword);

         switch (IPCamInfo_0.m_nIPCameraType)
         {
         case IPCameraType_RTSP:
            strURL = sutil::sformat("%s,%d,%s,%d", IPCamInfo_0.m_strIPAddress.c_str(), IPCamInfo_0.m_nPortNo, IPCamInfo_0.m_strURL.c_str(), (int)IPCamInfo_0.m_fFPS_Cap);
            if (IPCamInfo_1.m_bUseStream && !IPCamInfo_1.m_strIPAddress.empty() && !IPCamInfo_1.m_strURL.empty())
               strURL = sutil::sformat("%s,%d,%s,%d", strURL.c_str(), IPCamInfo_1.m_nPortNo, IPCamInfo_1.m_strURL.c_str(), (int)IPCamInfo_1.m_fFPS_Cap);
            break;
         case IPCameraType_Onvif:
            strURL = sutil::sformat("%s,%d,%d,%d", IPCamInfo_0.m_strIPAddress.c_str(), IPCamInfo_0.m_nPTZPortNo, IPCamInfo_0.m_nVideoStreamNo, (int)IPCamInfo_0.m_fFPS_Cap);
            if (IPCamInfo_1.m_bUseStream && !IPCamInfo_1.m_strIPAddress.empty() && 0 != IPCamInfo_1.m_nVideoStreamNo)
               strURL = sutil::sformat("%s,%d,%d,%d", strURL.c_str(), IPCamInfo_1.m_nPTZPortNo, IPCamInfo_1.m_nVideoStreamNo, (int)IPCamInfo_1.m_fFPS_Cap);
            break;
         default:
            throw gRPCCODE(gRPCCODE::CH_CONNECTION_TYPE_INVALID);
            break;
         }
         pStreams->set_stream_url(strURL);
      }
   }
   catch (gRPCCODE& code)
   {
      reply->set_error_code(code.m_nCode);
      reply->set_error_string(code.m_strCode);
      code.ReportError();
   }

   logi("gRPC  --- END [%s]\n", __FUNCTION__);

   return Status::OK;
}

Status IntellivixServiceImpl::GetProfileList(ServerContext* context, const ProfileListRequest* request, ProfileListReply* reply)
{
   logi("gRPC  --- START [%s] [Type:%d]\n", __FUNCTION__, request->req_type());

   reply->set_error_code(0);
   reply->set_error_string("Success");

   try
   {
      Profile  *pProfiles = NULL;

      CCriticalSectionSP co(m_pLocalSystem->csCameraList);
      std::deque<CCamera*>& Cameras = m_pLocalSystem->Cameras;

      for (size_t ii = 0; ii < Cameras.size(); ii++)
      {
         CCamera* pCamera = Cameras[ii];
         if (!pCamera)  continue;

         CIPCameraInfo& IPCamInfo_0 = pCamera->VC->m_IPCamInfoArr[0];
         CIPCameraInfo& IPCamInfo_1 = pCamera->VC->m_IPCamInfoArr[1];

         pProfiles = reply->add_profiles();
         pProfiles->set_stream_id(GetCameraID(pCamera));
         pProfiles->set_stream_name(pCamera->CameraName);
         pProfiles->set_profile_1(GetProfile(IPCamInfo_0));
         pProfiles->set_profile_2(GetProfile(IPCamInfo_1));
      }
   }
   catch (gRPCCODE& code)
   {
      reply->set_error_code(code.m_nCode);
      reply->set_error_string(code.m_strCode);
      code.ReportError();
   }

   logi("gRPC  --- END [%s]\n", __FUNCTION__);

   return Status::OK;
}

Status IntellivixServiceImpl::SysInfo(ServerContext* context, const SysInfoRequest* request, SysInfoReply* reply)
{
   logi("gRPC  --- START [%s] [Type:%d]\n", __FUNCTION__, request->req_type());

   reply->set_error_code(0);
   reply->set_error_string("Success");

   try
   {
      reply->set_maxch_count(DeviceChannelNum);
   }
   catch (gRPCCODE& code)
   {
      reply->set_error_code(code.m_nCode);
      reply->set_error_string(code.m_strCode);
      code.ReportError();
   }

   logi("gRPC  --- END [%s]\n", __FUNCTION__);

   return Status::OK;
}


////////////////////////////////////////////////////////////////////////////////////////
// RunServer
////////////////////////////////////////////////////////////////////////////////////////

std::thread    g_thread_gRPC;
CEventEx       g_evt_gRPCExit;
Server*        g_pServer;

void thread_gRPCRun(LPVOID pVoid)
{
   CLocalSystem* pLocalSystem = (CLocalSystem*)pVoid;
   RunServer(pLocalSystem);

   return;
}

int gRPC_StartThread(CLocalSystem* pLocalSystem)
{
   g_thread_gRPC = std::thread(thread_gRPCRun, pLocalSystem);
   return (DONE);
}

int gRPC_StopThread()
{
   if (g_pServer)
      g_pServer->Shutdown();

   if (g_thread_gRPC.joinable())
      g_thread_gRPC.join();

   return (DONE);
}

void RunServer(CLocalSystem* pLocalSystem)
{
  std::string server_address("0.0.0.0:50052");
  IntellivixServiceImpl service(pLocalSystem);

  ServerBuilder builder;
  // Listen on the given address without any authentication mechanism.
  builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
  // Register "service" as the instance through which we'll communicate with
  // clients. In this case it corresponds to an *synchronous* service.
  builder.RegisterService(&service);
  // Finally assemble the server.
  std::unique_ptr<Server> server(builder.BuildAndStart());
  std::cout << "Server listening on " << server_address << std::endl;

  g_pServer = server.get();

  // Wait for the server to shutdown. Note that some other thread must be
  // responsible for shutting down the server for this call to ever return.
  server->Wait();
}

#ifdef SERVER_TEST
int main(int argc, char** argv)
{
  RunServer();

  return 0;
}
#endif

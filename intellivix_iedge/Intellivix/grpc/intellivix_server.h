#ifndef intellivix_server_h__
#define intellivix_server_h__



#define MANUFACTURE_ID "illisis Inc." 

class CLocalSystem;

int  gRPC_StartThread(CLocalSystem* pLocalSystem);
int  gRPC_StopThread();
void RunServer(CLocalSystem* pLocalSystem);


#endif

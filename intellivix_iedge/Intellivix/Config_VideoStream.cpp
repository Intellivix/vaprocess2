#include "StdAfx.h"
#include "Config_VideoStream.h"

CVideoStreamProfile::CVideoStreamProfile()
{
   m_nVideoServerStreamIdx = 0;
   m_bDirectStreaming      = TRUE;
   m_strProfileName        = "Profile1";
   m_nVideoServerStreamIdx = 0;
   m_nVideoCodecID         = VCODEC_ID_H264;
   m_nVideoBitrate         = 2000 * 1024;
   m_fVideoFrameRate       = 15.0f;
   m_nResizeType           = 0;
   m_fReductionFactor      = 1.0f;
   m_nVideoWidth           = 0;
   m_nVideoHeight          = 0;
}

BOOL CVideoStreamProfile::Read( CXMLIO* pIO )
{
   static CVideoStreamProfile dv; // default value
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "VideoStreamProfile" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "ProfileName", &m_strProfileName );
      xmlNode.Attribute( TYPE_ID( BOOL ), "DirectStreaming", &m_bDirectStreaming, &dv.m_bDirectStreaming );
      xmlNode.Attribute( TYPE_ID( int ), "StreamIdx", &m_nVideoServerStreamIdx, &dv.m_nVideoServerStreamIdx );
      xmlNode.Attribute( TYPE_ID( int ), "VideoCodecID", &m_nVideoCodecID, &dv.m_nVideoCodecID );
      xmlNode.Attribute( TYPE_ID( int ), "VideoBitrate", &m_nVideoBitrate, &dv.m_nVideoBitrate );
      xmlNode.Attribute( TYPE_ID( float ), "VideoFrameRate", &m_fVideoFrameRate, &dv.m_fVideoFrameRate );
      xmlNode.Attribute( TYPE_ID( int ), "ResizeType", &m_nResizeType, &dv.m_nResizeType );
      xmlNode.Attribute( TYPE_ID( float ), "ResizeFactor", &m_fReductionFactor, &dv.m_fReductionFactor );
      xmlNode.Attribute( TYPE_ID( int ), "VideoWidth", &m_nVideoWidth, &dv.m_nVideoWidth );
      xmlNode.Attribute( TYPE_ID( int ), "VideoHeight", &m_nVideoHeight, &dv.m_nVideoHeight );
      xmlNode.End();
   }

   if( m_fVideoFrameRate <= 0.0f ) m_fVideoFrameRate = 1.0f; // 비디오 프레임비율이 0 fps인 것을 막음.
   return TRUE;
}

BOOL CVideoStreamProfile::Write( CXMLIO* pIO )
{
   static CVideoStreamProfile dv; // default value
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "VideoStreamProfile" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "ProfileName", &m_strProfileName, &dv.m_strProfileName );
      xmlNode.Attribute( TYPE_ID( BOOL ), "DirectStreaming", &m_bDirectStreaming, &dv.m_bDirectStreaming );
      xmlNode.Attribute( TYPE_ID( int ), "StreamIdx", &m_nVideoServerStreamIdx, &dv.m_nVideoServerStreamIdx );
      xmlNode.Attribute( TYPE_ID( int ), "VideoCodecID", &m_nVideoCodecID, &dv.m_nVideoCodecID );
      xmlNode.Attribute( TYPE_ID( int ), "VideoBitrate", &m_nVideoBitrate, &dv.m_nVideoBitrate );
      xmlNode.Attribute( TYPE_ID( float ), "VideoFrameRate", &m_fVideoFrameRate, &dv.m_fVideoFrameRate );
      xmlNode.Attribute( TYPE_ID( int ), "ResizeType", &m_nResizeType, &dv.m_nResizeType );
      xmlNode.Attribute( TYPE_ID( float ), "ResizeFactor", &m_fReductionFactor, &dv.m_fReductionFactor );
      xmlNode.Attribute( TYPE_ID( int ), "VideoWidth", &m_nVideoWidth, &dv.m_nVideoWidth );
      xmlNode.Attribute( TYPE_ID( int ), "VideoHeight", &m_nVideoHeight, &dv.m_nVideoHeight );
      xmlNode.End();
      return TRUE;
   }
   return FALSE;
}

void CVideoStreamProfile::QualityToBitrate( int nQuality, int& nBitRate )
{
   if( 0 <= nQuality && nQuality <= 100 ) {
      int nBitPerPixel              = 16;
      float fMaxEncodedImgSizeRatio = 1.0f;
      int nGammaQuality             = int( pow( float( nQuality ), 2.0f ) / pow( 100.0f, 2.0f ) * 100 );
      nBitRate                      = int( ( m_nVideoWidth * m_nVideoHeight * m_fVideoFrameRate * nBitPerPixel ) * fMaxEncodedImgSizeRatio * ( (nGammaQuality)*1.0f / 99.0f + 0.01f ) );
      if( nBitRate < 64 * 1024 ) {
         nBitRate = 64 * 1024;
      }
   } else {
      nBitRate = 1024 * 512;
   }
}

//////////////////////////////////////////////////////////////
//
// Class : CConfig_VideoStream
//
//////////////////////////////////////////////////////////////

CConfig_VideoStream::CConfig_VideoStream()
{
   // Profile 1
   //     --> CVideoStreamProfile 생성자에 정의된 값을 사용함.
   m_VideoStreamProfiles[0].m_strProfileName = "Default";

   // Profile 2
   m_VideoStreamProfiles[1].m_strProfileName        = "High Quality";
   m_VideoStreamProfiles[1].m_nVideoServerStreamIdx = 1;
   m_VideoStreamProfiles[1].m_nVideoBitrate         = 4000 * 1024;

   // Profile 3
   //     --> CVideoStreamProfile 생성자에 정의된 값을 사용함.

   // Profile 4
   //     --> CVideoStreamProfile 생성자에 정의된 값을 사용함.

   m_bUseHWEncoder = FALSE;
}

CVideoStreamProfile* CConfig_VideoStream::GetVideoStreamProfile( int nStreamIdx )
{
   CVideoStreamProfile* pVideoStreamProfie = NULL;
   if( 0 <= nStreamIdx && nStreamIdx < IVX_VIDEO_STREAM_NUM ) {
      pVideoStreamProfie = &m_VideoStreamProfiles[nStreamIdx];
   }
   return pVideoStreamProfie;
}

BOOL CConfig_VideoStream::Read( CXMLIO* pIO )
{
   int i;

   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "VideoStream" ) ) {
      {
         CXMLNode xmlNode( pIO );
         if( xmlNode.Start( "AVStreamProfiles" ) ) {
            int nProfileNum;
            xmlNode.Attribute( TYPE_ID( int ), "ProfileNum", &nProfileNum );
            for( i = 0; i < IVX_VIDEO_STREAM_NUM; i++ ) {
               m_VideoStreamProfiles[i].Read( pIO );
            }
            xmlNode.End();
         }
      }
      xmlNode.Attribute( TYPE_ID( BOOL ), "UseHWEncoder", &m_bUseHWEncoder );
      xmlNode.End();
      return TRUE;
   }
   return FALSE;
}

BOOL CConfig_VideoStream::Write( CXMLIO* pIO )
{
   int i;

   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "VideoStream" ) ) {
      {
         CXMLNode xmlNode( pIO );
         if( xmlNode.Start( "AVStreamProfiles" ) ) {
            int nProfileNum = IVX_VIDEO_STREAM_NUM;
            xmlNode.Attribute( TYPE_ID( int ), "ProfileNum", &nProfileNum );
            for( i = 0; i < IVX_VIDEO_STREAM_NUM; i++ ) {
               m_VideoStreamProfiles[i].Write( pIO );
            }
            xmlNode.End();
         }
      }
      xmlNode.Attribute( TYPE_ID( BOOL ), "UseHWEncoder", &m_bUseHWEncoder );
      xmlNode.End();
      return TRUE;
   }
   return FALSE;
}

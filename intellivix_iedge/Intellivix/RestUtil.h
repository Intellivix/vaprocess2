﻿#ifndef RESTUTIL_H
#define RESTUTIL_H

#include "bccl.h"
#include "RestDefine.h"
#include <vector>

namespace web_rest_util
{
   template <typename T>
   bool is_valid(const std::vector<T>& vec, int pos)
   {
      if (vec.size() > pos)
         return true;
      return false;
   }

   template <typename T>
   bool is_exist_item(const T& find, const std::vector<T>& target)
   {
      auto iter = std::find(target.begin(), target.end(), find);
      if (iter == target.end())
         return false;
      return true;
   }

   template <typename T>
   bool is_exist_flag(const T& value, const T& flag)
   {
      if (value & flag)
         return true;
      return false;
   }

   void to_lower(std::vector<string_t>& vec);
   void to_upper(std::vector<string_t>& vec);

   json_value get_response_body        (bool success, const json_value& data);
   json_value get_response_error       (const string_t& error_msg);
   json_value get_response_body_error  (const string_t& error_msg);

   json_value as_json      (const std::string& value);
   json_value as_json      (BOOL value);
   json_value as_json      (const FILETIME& ft);
   json_value as_json      (const BCCL::BGRColor& color);
   json_value as_json      (const BCCL::IBox2D& box);

   std::string as_string   (const string_t& key, const json_value& value);

   bool as_vector_int      (const string_t& key, const json_value& json, std::vector<int>& vec);
   bool as_vector_int      (const json_array& array, std::vector<int>& vec);
   bool as_vector_string_t (const string_t& key, const json_value& json, std::vector<string_t>& vec);
   bool as_vector_string_t (const json_array& array, std::vector<string_t>& vec);

   pplx::task<void> async_do_while(std::function<pplx::task<bool>(void)> func);
};

#endif // RESTUTIL_H

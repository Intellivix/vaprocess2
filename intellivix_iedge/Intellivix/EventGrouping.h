#pragma once

using namespace VACL;

class VACL::ObjectTracking;
class VACL::EventDetection;

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventGrouping
//
///////////////////////////////////////////////////////////////////////////////

class EventGrouping {
public:
   float FrameRate;
   double NormalPlayTime;
   ISize2D SrcImgSize;
   TrackedObjectList EvtGenObjects;

protected:
   int FrameCount;
   std::deque<int> PrevEventTypes;
   TrackedObjectList DeadObjects;
   EventRuleList EventRules;
   EventZoneList EventZones;

public:
   EventGrouping();
   virtual ~EventGrouping();

protected:
   TrackedObjectEx* FindTrackedObject( int eventType );
   void FinalizeEvents();
   void CleanTrackedObjectList();

public:
   void Initialize( ISize2D& si_size, float frm_rate );
   void Close();
   int Perform( double dfNormalPlayTime, ObjectTracking* obj_tracker, EventDetection* event_detector );
   void Reset();

public:
   void UpdateEventThumbnails( byte* si_buffer, ISize2D& si_size, int pix_fmt = VAE_PIXFMT_YUY2, float cbs_ratio = 1.5f, ISize2D* tn_size = NULL );
};

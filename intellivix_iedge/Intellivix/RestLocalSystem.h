﻿#ifndef RESTLOCALSYSTEM_H
#define RESTLOCALSYSTEM_H

#include "RestServer.h"

class CLocalSystem;

class CRestLocalSystem : public CRestHandler
{
private:
   std::shared_ptr<CRestServer>       m_pRestServer;
   CLocalSystem*                      m_pLocalSystem = nullptr;   

private:
   virtual void OnRecv(const ParamRequest& request, ParamResponse& response);   

public:
   explicit CRestLocalSystem(CLocalSystem* pLocalSystem);
   virtual ~CRestLocalSystem();

   int Activate(const std::string& url);
   int Deactivate();
};

#endif // RESTLOCALSYSTEM_H

﻿#ifndef INTELLIVIX_DOC_H
#define INTELLIVIX_DOC_H

#include <vector>
#include <string>

class CLocalSystem;
class CLicenseManager;

class CIntelliVIXDoc {
private:
   CIntelliVIXDoc();
   virtual ~CIntelliVIXDoc();

private:
   static CIntelliVIXDoc* m_pThis;

   void* m_hCaffeDLL                  = nullptr;
   CLicenseManager* m_pLicenseManager = nullptr;

   int m_nExitCode = 0;
   bool m_bDoExit  = false;
   std::vector<bool> m_vecTableUID;
   
public:
   bool m_SaveMetaInCurrDir = false;
   bool m_StandAlone = false;

   // Path
public:
   std::string m_strExeDir;
   std::string m_strStartUpDir;
   std::string m_strConfigPath;
   std::string m_strDwellAnalysisDataPath;
   std::string m_strObjectDetectorDataDirName;
   std::string m_std_LOG_full_path_;
   
public:
   static CIntelliVIXDoc* GetInstance();
   void FreeInstance();

   int SetPath();
      
   int Initialize();
   int Uninitialize();

   void* GetCaffeHandle();
   bool IsSupportGpu();

   void SetExitProcess( int code );
   bool IsExitProcess();
   int GetExitCode();

   void UpdateTableUID( CLocalSystem* pLocalSystem );
   int AllocateUID();
   void FreeUID( int UID );

   int LicenseCertifyKey( int& nChannels );
};

#endif // INTELLIVIX_DOC_H

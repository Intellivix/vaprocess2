﻿#include "StdAfx.h"
#include "VideoFrame.h"
#include "SupportFunction.h"
#include "EventGrouping.h"

////////////////////////////////////////////////////////////
//
// Class : IVXVideoFrameInfo
//
////////////////////////////////////////////////////////////

IVXVideoFrameInfo::IVXVideoFrameInfo()
{
   InitMemberVariables();
   m_bReadWriteTrackedObjectList = TRUE;
}

IVXVideoFrameInfo::~IVXVideoFrameInfo()
{
   DeleteAllTrackedObject();
}

IVXVideoFrameInfo& IVXVideoFrameInfo::operator=( IVXVideoFrameInfo& videoFrameInfo )
{
   FileIO buffer;
   videoFrameInfo.m_CS.Lock();
   videoFrameInfo.WriteFrameHeader( &buffer );
   buffer.GoToStartPos();
   m_CS.Lock();
   this->ReadFrameHeader( &buffer );
   m_CS.Unlock();
   videoFrameInfo.m_CS.Unlock();
   return *this;
}

void IVXVideoFrameInfo::InitMemberVariables()
{
   ZeroMemory( &m_ftFrmTime, sizeof( m_ftFrmTime ) );

   m_nFrmNo                = 0;
   m_nChNo                 = 0;
   m_nFrmSize              = 0;
   m_nState                = 0;
   m_nChState              = 0;
   m_nChPTZState           = 0;
   m_nChPTZStateEx         = 0;
   m_nVideoSrcType         = 0;
   m_nVideoFrmFlag         = 0;
   m_nWidth                = 0;
   m_nHeight               = 0;
   m_nBitrate              = 0;
   m_nCodecType            = 0;
   m_nPresetMoveRemainTime = 0;
   m_dfNormalPlayTime      = 0.0;

   m_nTrackedObjectCount = 0;

   m_nVideoDataPos = 0;
}

void IVXVideoFrameInfo::PushTrackedObject( TrackedObjectEx* pTO )
{
   CCriticalSectionSP lk( m_CS );

   m_TrackedObjects.push_back( pTO );
   m_nTrackedObjectCount++;
}

void IVXVideoFrameInfo::DeleteAllTrackedObject()
{
   int i;

   CCriticalSectionSP lk( m_CS );
   int nObjNum = m_TrackedObjects.size();

   for( i = 0; i < nObjNum; i++ ) {
      TrackedObjectEx* t_object = m_TrackedObjects[i];
      if( t_object ) {
         delete t_object;
      }
   }
   m_TrackedObjects.clear();

   m_nTrackedObjectCount = 0;
}

BOOL IVXVideoFrameInfo::ReadXML( CXMLIO* pIO )
{
   int i;

   static IVXVideoFrameInfo dv; // default value;

   CCriticalSectionSP lk( m_CS );

   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "FH" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "FN", &m_nFrmNo );
      xmlNode.Attribute( TYPE_ID( int ), "CN", &m_nChNo );
      xmlNode.Attribute( TYPE_ID( int ), "FS", &m_nFrmSize );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "FT", &m_ftFrmTime );
      xmlNode.Attribute( TYPE_ID( double ), "PT", &m_dfNormalPlayTime, &dv.m_dfNormalPlayTime );
      xmlNode.Attribute( TYPE_ID( xint ), "St", &m_nState, &dv.m_nState );
      xmlNode.Attribute( TYPE_ID( xint ), "CS", &m_nChState, &dv.m_nChState );
      xmlNode.Attribute( TYPE_ID( xint ), "PS", &m_nChPTZState, &dv.m_nChPTZState );
      xmlNode.Attribute( TYPE_ID( xint ), "PSX", &m_nChPTZStateEx, &dv.m_nChPTZStateEx );
      xmlNode.Attribute( TYPE_ID( char ), "VT", &m_nVideoSrcType, &dv.m_nVideoSrcType );
      xmlNode.Attribute( TYPE_ID( xint ), "VF", &m_nVideoFrmFlag, &dv.m_nVideoFrmFlag );
      xmlNode.Attribute( TYPE_ID( int ), "W", &m_nWidth, &dv.m_nWidth );
      xmlNode.Attribute( TYPE_ID( int ), "H", &m_nHeight, &dv.m_nHeight );
      xmlNode.Attribute( TYPE_ID( int ), "B", &m_nBitrate, &dv.m_nBitrate );
      xmlNode.Attribute( TYPE_ID( int ), "CT", &m_nCodecType );
      xmlNode.Attribute( TYPE_ID( uint ), "PR", &m_nPresetMoveRemainTime, &dv.m_nPresetMoveRemainTime );

      if( m_bReadWriteTrackedObjectList ) {
         int nTrackedObjectCount = 0;
         xmlNode.Attribute( TYPE_ID( int ), "TOC", &nTrackedObjectCount );

         DeleteAllTrackedObject();

         if( nTrackedObjectCount > 0 ) {
            for( i = 0; i < nTrackedObjectCount; i++ ) {
               TrackedObjectEx* pTrackedObject = new TrackedObjectEx;
               pTrackedObject->ReadXMLForDisplay( pIO );
               m_TrackedObjects.push_back( pTrackedObject );
            }
            m_nTrackedObjectCount = nTrackedObjectCount;
         }

         m_EventZoneCountInfo.m_bRWOnlyCountInfo = TRUE;
         m_EventZoneCountInfo.ReadXML( pIO );
      }

      xmlNode.Attribute( TYPE_ID( FPoint3D ), "PP", &m_PTZPos, &dv.m_PTZPos );

      xmlNode.End();
   }
   return ( DONE );
}

BOOL IVXVideoFrameInfo::WriteXML( CXMLIO* pIO )
{
   int i;

   static IVXVideoFrameInfo dv; // default value;

   CCriticalSectionSP lk( m_CS );

   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "FH" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "FN", &m_nFrmNo );
      xmlNode.Attribute( TYPE_ID( int ), "CN", &m_nChNo );
      xmlNode.Attribute( TYPE_ID( int ), "FS", &m_nFrmSize );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "FT", &m_ftFrmTime );
      xmlNode.Attribute( TYPE_ID( double ), "PT", &m_dfNormalPlayTime, &dv.m_dfNormalPlayTime );
      xmlNode.Attribute( TYPE_ID( xint ), "St", &m_nState, &dv.m_nState );
      xmlNode.Attribute( TYPE_ID( xint ), "CS", &m_nChState, &dv.m_nChState );
      xmlNode.Attribute( TYPE_ID( xint ), "PS", &m_nChPTZState, &dv.m_nChPTZState );
      xmlNode.Attribute( TYPE_ID( xint ), "PSX", &m_nChPTZStateEx, &dv.m_nChPTZStateEx );
      xmlNode.Attribute( TYPE_ID( char ), "VT", &m_nVideoSrcType, &dv.m_nVideoSrcType );
      xmlNode.Attribute( TYPE_ID( xint ), "VF", &m_nVideoFrmFlag, &dv.m_nVideoFrmFlag );
      xmlNode.Attribute( TYPE_ID( int ), "W", &m_nWidth, &dv.m_nWidth );
      xmlNode.Attribute( TYPE_ID( int ), "H", &m_nHeight, &dv.m_nHeight );
      xmlNode.Attribute( TYPE_ID( int ), "B", &m_nBitrate, &dv.m_nBitrate );
      xmlNode.Attribute( TYPE_ID( int ), "CT", &m_nCodecType );
      xmlNode.Attribute( TYPE_ID( uint ), "PR", &m_nPresetMoveRemainTime, &dv.m_nPresetMoveRemainTime );

      if( m_bReadWriteTrackedObjectList ) {
         int nTrackedObjectNum = (int)m_TrackedObjects.size();
         xmlNode.Attribute( TYPE_ID( int ), "TOC", &nTrackedObjectNum );

         if( nTrackedObjectNum > 0 ) {
            for( i = 0; i < m_nTrackedObjectCount; i++ ) {
               TrackedObjectEx* pTrackedObject = m_TrackedObjects[i];
               pTrackedObject->WriteXMLForDisplay( pIO );
            }
         }

         m_EventZoneCountInfo.m_bRWOnlyCountInfo = TRUE;
         m_EventZoneCountInfo.WriteXML( pIO );
      }

      xmlNode.Attribute( TYPE_ID( FPoint3D ), "PP", &m_PTZPos, &dv.m_PTZPos );

      xmlNode.End();
   }
   return ( DONE );
}

void IVXVideoFrameInfo::ReadFrameHeader( FileIO* pBuff )
{
   CCriticalSectionSP lk( m_CS );

   CXMLIO xmlIO( pBuff, XMLIO_Read );
   ReadXML( &xmlIO );
   m_nVideoDataPos = pBuff->Where();
}

void IVXVideoFrameInfo::WriteFrameHeader( FileIO* pBuff )
{
   CCriticalSectionSP lk( m_CS );

   CXMLIO xmlIO( pBuff, XMLIO_Write );
   WriteXML( &xmlIO );
   m_nVideoDataPos = pBuff->Where();
}

////////////////////////////////////////////////////////////
//
// Class : CVideoFrame
//
////////////////////////////////////////////////////////////

CVideoFrame::CVideoFrame()
{
   m_nUpdateCount     = 0;
   m_pYUY2Data        = NULL;
   m_nVideoSrcType    = VideoSrcType_Live;
   m_bImportImageData = FALSE;

   // (mkjang-141229)
   m_nPrevWidth  = 0;
   m_nPrevHeight = 0;
}

CVideoFrame::~CVideoFrame()
{
   Close();
}

void CVideoFrame::Create( int nWidth, int nHeight, int nVideoType )
{
   LockVideoFrameInfo();

   m_VideoFrameInfo.InitMemberVariables();
   m_VideoFrameInfo.m_nWidth        = nWidth;
   m_VideoFrameInfo.m_nHeight       = nHeight;
   m_VideoFrameInfo.m_nVideoSrcType = nVideoType;
   m_VideoFrameInfo.DeleteAllTrackedObject();

   UnlockVideoFrameInfo();

   m_nVideoSrcType = nVideoType;
}

void CVideoFrame::Close()
{
   if( m_bImportImageData )
      m_ImageData.Import( NULL, 0 );
}

void CVideoFrame::SetImage( byte* pSrcImage, int width, int height, BOOL bImportImg )
{
   if( width != m_VideoFrameInfo.m_nWidth || height != m_VideoFrameInfo.m_nHeight ) {
      m_VideoFrameInfo.m_nWidth  = width;
      m_VideoFrameInfo.m_nHeight = height;
   }
   if( !bImportImg ) {
      if( m_bImportImageData )
         m_ImageData.Import( NULL, 0 );
      int nYUY2BuffLen = m_VideoFrameInfo.m_nWidth * m_VideoFrameInfo.m_nHeight * 2;
      if( m_ImageData.Length != nYUY2BuffLen ) {
         m_ImageData.Create( nYUY2BuffLen );
         m_bImportImageData = FALSE;
      }
   }

   if( pSrcImage ) {
      if( bImportImg ) {
         if( (byte*)m_ImageData != pSrcImage ) {
            m_ImageData.Import( pSrcImage, width * height * 2 );
            m_bImportImageData = TRUE;
         }
      } else {
         if( m_ImageData.Length == width * height * 2 )
            memcpy( (byte*)m_ImageData, pSrcImage, m_ImageData.Length );
      }
   }
}

void CVideoFrame::Copy( CVideoFrame& from )
{
   // 이 함수는 엄밀한 의미로의 "복사"를 수행하지 않는다.
   // 예외적인 항목은 다음과 같다.
   // 1. YUY2OffscreenSurface의 Import
   // 2. m_ImageData는 복사하지 않음.

   from.LockVideoFrameInfo();
   LockVideoFrameInfo();
   Create( from.m_VideoFrameInfo.m_nWidth, from.m_VideoFrameInfo.m_nHeight, from.m_VideoFrameInfo.m_nVideoSrcType );
   m_VideoFrameInfo = from.m_VideoFrameInfo;
   m_nVideoSrcType  = from.m_nVideoSrcType;
   m_nUpdateCount   = from.m_nUpdateCount;
   m_ftTimestamp    = from.m_ftTimestamp;
   UnlockVideoFrameInfo();
   from.UnlockVideoFrameInfo();
}

void CVideoFrame::CopyVideoFrameInfo( IVXVideoFrameInfo& s_videoFrameInfo, BOOL bExcludeTrackedObjects )
{
   BOOL bReadWriteTrackedObjectList = TRUE;

   CCriticalSectionSP lk( s_videoFrameInfo.m_CS );

   LockVideoFrameInfo();

   FileIO buffer;
   s_videoFrameInfo.m_bReadWriteTrackedObjectList = !bExcludeTrackedObjects;
   m_VideoFrameInfo.m_bReadWriteTrackedObjectList = !bExcludeTrackedObjects;
   s_videoFrameInfo.WriteFrameHeader( &buffer );
   buffer.GoToStartPos();
   m_VideoFrameInfo.ReadFrameHeader( &buffer );
   s_videoFrameInfo.m_bReadWriteTrackedObjectList = TRUE;
   m_VideoFrameInfo.m_bReadWriteTrackedObjectList = TRUE;

   UnlockVideoFrameInfo();
}

void CVideoFrame::CopyTrackedObjects( IVXVideoFrameInfo& s_videoFrameInfo )
{
   int i;

   CCriticalSectionSP lk( s_videoFrameInfo.m_CS );
   LockVideoFrameInfo();

   DeleteAllTrackedObjects();

   // 추적 물체 복사
   if( s_videoFrameInfo.m_nTrackedObjectCount > 0 ) {
      for( i = 0; i < s_videoFrameInfo.m_nTrackedObjectCount; i++ ) {
         TrackedObjectEx* t_object = new TrackedObjectEx;
         *t_object                 = *( s_videoFrameInfo.m_TrackedObjects[i] );
         m_VideoFrameInfo.PushTrackedObject( t_object );
      }
   }

   // 이벤트 존 카운트 정보 복사
   int nZoneNum = s_videoFrameInfo.m_EventZoneCountInfo.m_nZoneNum;
   if( nZoneNum > 0 ) {
      m_VideoFrameInfo.m_EventZoneCountInfo.ClearZoneCounts();
      m_VideoFrameInfo.m_EventZoneCountInfo.m_nZoneNum    = nZoneNum;
      m_VideoFrameInfo.m_EventZoneCountInfo.m_pZoneCounts = new CIVCPEventZoneCount[nZoneNum];
      for( i = 0; i < nZoneNum; i++ ) {
         m_VideoFrameInfo.m_EventZoneCountInfo.m_pZoneCounts[i].m_nZoneID    = s_videoFrameInfo.m_EventZoneCountInfo.m_pZoneCounts[i].m_nZoneID;
         m_VideoFrameInfo.m_EventZoneCountInfo.m_pZoneCounts[i].m_nZoneCount = s_videoFrameInfo.m_EventZoneCountInfo.m_pZoneCounts[i].m_nZoneCount;
      }
   }

   UnlockVideoFrameInfo();

   m_nUpdateCount++;
}

BOOL CVideoFrame::CopyTrackedObjects( ObjectTracking* pObjectTracker, CCriticalSection* csOT,
                                      EventDetection* pEventDetector, CCriticalSection* csED,
                                      EventGrouping* pEventGrouper,
                                      WaterLevelDetection* pWaterLevelDetector, FPoint2D& scale )
{
   int i;

   LockVideoFrameInfo();

   DeleteAllTrackedObjects();

   // 수위 표시 객체 복사
   if( pWaterLevelDetector ) {
      csED->Lock();
      WaterLevelDetection& wl_detector = *pWaterLevelDetector;
      if( wl_detector.IsEnabled() ) {
         WaterZone* wt_zone = wl_detector.WaterZones.First();
         while( wt_zone != NULL ) {
            if( wt_zone->WaterLevel[0].X || wt_zone->WaterLevel[1].X ) {
               TrackedObjectEx* t_objDisp = new TrackedObjectEx;
               t_objDisp->Type            = TO_TYPE_UNKNOWN;
               t_objDisp->ID              = 1;
               int ex                     = (int)( wt_zone->WaterLevel[1].X * scale.X );
               int ey                     = (int)( wt_zone->WaterLevel[1].Y * scale.Y );
               t_objDisp->X               = (int)( wt_zone->WaterLevel[0].X * scale.X );
               t_objDisp->Y               = (int)( wt_zone->WaterLevel[0].Y * scale.Y );
               t_objDisp->Width           = ex - t_objDisp->X;
               t_objDisp->Height          = ey - t_objDisp->Y;

               m_VideoFrameInfo.PushTrackedObject( t_objDisp );
            }
            wt_zone = wl_detector.WaterZones.Next( wt_zone );
         }
      }
      csED->Unlock();
   }

   // 추적 물체 복사
   csOT->Lock();
   for( i = 0; i < 4; i++ ) {
      LinkedList<TrackedObject>* pTrackedObjects;
      if( i == 0 ) pTrackedObjects = &pObjectTracker->TrackedObjects;
      if( i == 1 ) {
         pTrackedObjects = &pObjectTracker->LostObjects;
         continue;
      }
      if( i == 2 ) pTrackedObjects = &pEventDetector->EvtGenObjects;
      if( i == 3 ) pTrackedObjects = &pEventGrouper->EvtGenObjects;

      TrackedObjectEx* t_object = (TrackedObjectEx*)pTrackedObjects->First();
      while( t_object != NULL ) {
         BOOL bAddObject = FALSE;
         if( t_object->Status & ( TO_STATUS_VERIFIED ) && !( t_object->Status & TO_STATUS_TO_BE_REMOVED ) )
            bAddObject = TRUE;
         if( bAddObject ) {
            TrackedObjectEx* t_objDisp = new TrackedObjectEx;
            FileIO buffer;
            CXMLIO xmlIOWrite( &buffer, XMLIO_Write );
            t_object->WriteXMLForDisplay( &xmlIOWrite );
            buffer.GoToStartPos();
            CXMLIO xmlIORead( &buffer, XMLIO_Read );
            t_objDisp->ReadXMLForDisplay( &xmlIORead );

            t_objDisp->X      = (int)( t_object->X * scale.X + 0.5f );
            t_objDisp->Y      = (int)( t_object->Y * scale.Y + 0.5f );
            t_objDisp->Width  = (int)( t_object->Width * scale.X + 0.5f );
            t_objDisp->Height = (int)( t_object->Height * scale.Y + 0.5f );
            t_objDisp->Scale  = scale;

            t_objDisp->VAEventFlag = 0;
            Event* event           = t_object->Events.First();
            while( event != NULL ) {
               if( event->Status & ( EV_STATUS_BEGUN | EV_STATUS_ONGOING ) ) {
                  t_objDisp->VAEventFlag |= 1 << event->Type;
               }
               event = t_object->Events.Next( event );
            }
            m_VideoFrameInfo.PushTrackedObject( t_objDisp );
         }
         t_object = (TrackedObjectEx*)pTrackedObjects->Next( t_object );
      }
   }
   csOT->Unlock();

   // 이벤트 존 카운트 정보 복사
   m_VideoFrameInfo.m_EventZoneCountInfo.ClearZoneCounts();
   csED->Lock();
   int nZoneNum                                        = pEventDetector->EventZones.GetNumItems();
   m_VideoFrameInfo.m_EventZoneCountInfo.m_pZoneCounts = new CIVCPEventZoneCount[nZoneNum];

   int& nZoneIndex           = m_VideoFrameInfo.m_EventZoneCountInfo.m_nZoneNum;
   nZoneIndex                = 0;
   EventRuleEx* pEventRuleEx = (EventRuleEx*)pEventDetector->EventRules.First();
   while( pEventRuleEx ) {
      // 현재 IVCP에는 각 EventZone ID와 카운트정보가 맵핑되어 있는데
      // 나중에는 EventRule ID와 카운트 정보가 맵핑되도록 변경해야 한다.
      m_VideoFrameInfo.m_EventZoneCountInfo.m_pZoneCounts[nZoneIndex].m_nZoneID    = pEventRuleEx->ID; // 이밋로 Zone ID에 Rule ID 값으로 복사한다.
      m_VideoFrameInfo.m_EventZoneCountInfo.m_pZoneCounts[nZoneIndex].m_nZoneCount = pEventRuleEx->ERC_Counting.Count;
      nZoneIndex++;
      pEventRuleEx = (EventRuleEx*)pEventRuleEx->Next;
   }
   csED->Unlock();

   UnlockVideoFrameInfo();

   return TRUE;
}

BOOL CVideoFrame::PushTrackedObject( TrackedObjectEx* t_object, FPoint2D& scale )
{
   // CVideoFrame::CopyTrackedObjects의 추적물체로 부터 복사하는 코드와 동일 해야 함.

   TrackedObjectEx* t_objDisp = new TrackedObjectEx;

   t_objDisp->Type                = t_object->Type;
   t_objDisp->ID                  = t_object->ID;
   t_objDisp->Width               = t_object->Width;
   t_objDisp->Height              = t_object->Height;
   t_objDisp->RealHeight          = t_object->RealHeight;
   t_objDisp->RealWidth           = t_object->RealWidth;
   t_objDisp->Area                = t_object->Area;
   t_objDisp->Status              = t_object->Status;
   t_objDisp->StatusEx            = t_object->StatusEx;
   t_objDisp->AspectRatio         = t_object->AspectRatio;
   t_objDisp->MajorAxisLength     = t_object->MajorAxisLength;
   t_objDisp->AxisLengthRatio     = t_object->AxisLengthRatio;
   t_objDisp->Speed               = t_object->Speed;
   t_objDisp->NorSpeed            = t_object->NorSpeed;
   t_objDisp->RealSpeed           = t_object->RealSpeed;
   t_objDisp->RealArea            = t_object->RealArea;
   t_objDisp->RealMajorAxisLength = t_object->RealMajorAxisLength;
   t_objDisp->Status              = t_object->Status;

   if( IS_SUPPORT( __SUPPORT_FISHEYE_DEWARPING ) ) {
      t_objDisp->MajorAxisAngle1 = t_object->MajorAxisAngle1;
      t_objDisp->MinorAxisLength = t_object->MinorAxisLength;
      t_objDisp->CenterPos       = t_object->CenterPos;
      t_objDisp->MajorAxis       = t_object->MajorAxis;
      t_objDisp->MinorAxis       = t_object->MinorAxis;
      t_objDisp->Scale           = scale;
   }
   int sx            = (int)( t_object->X * scale.X );
   int sy            = (int)( t_object->Y * scale.Y );
   int ex            = (int)( ( t_object->X + t_object->Width ) * scale.X );
   int ey            = (int)( ( t_object->Y + t_object->Height ) * scale.Y );
   t_objDisp->X      = sx;
   t_objDisp->Y      = sy;
   t_objDisp->Width  = ex - sx;
   t_objDisp->Height = ey - sy;

   t_objDisp->VAEventFlag = 0;
   Event* event           = t_object->Events.First();
   while( event != NULL ) {
      if( event->Status & ( EV_STATUS_BEGUN | EV_STATUS_ONGOING ) ) {
         t_objDisp->VAEventFlag |= 1 << event->Type;
      }
      event = t_object->Events.Next( event );
   }
   m_VideoFrameInfo.PushTrackedObject( t_objDisp );

   return TRUE;
}

void CVideoFrame::DeleteAllTrackedObjects()
{
   LockVideoFrameInfo();
   m_VideoFrameInfo.DeleteAllTrackedObject();
   m_VideoFrameInfo.m_nTrackedObjectCount = 0;
   UnlockVideoFrameInfo();

   m_nUpdateCount++;
}

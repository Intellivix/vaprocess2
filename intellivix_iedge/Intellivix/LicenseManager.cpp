﻿#include "LicenseManager.h"

#include "version.h"
#include "bccl_plog.h"
#include "bccl_define.h"
#include "Common.h"
#include "License/KeyDecryption.h"

#if defined( __linux )
#include <net/if.h>
#endif

int CLicenseItem::ReadWriteXML( BCCL::CXMLIO* pIO )
{
   CXMLNode xml_node( pIO );
   if( xml_node.Start( "VAEngineLicense" ) ) {
      CXMLNode xml_node_license( pIO );
      if( xml_node_license.Start( "LicenseKey" ) ) {
         xml_node_license.TextValue( TYPE_ID( string ), &strLicense );
         xml_node_license.End();
      }
      CXMLNode xml_node_serial( pIO );
      if( xml_node_serial.Start( "SerialNumber" ) ) {
         xml_node_serial.TextValue( TYPE_ID( string ), &strProductSerial );
         xml_node_serial.End();
      }
      xml_node.End();
   }
   return ( DONE );
}

int CLicenseItem::LoadXML()
{
   CXMLIO xmlio( m_license_xml_file, BCCL::XMLIO_Read );
   ReadWriteXML( &xmlio );
   return ( DONE );
}

int CLicenseItem::SaveXML()
{
   CXMLIO xmlio( m_license_xml_file, BCCL::XMLIO_Write );
   ReadWriteXML( &xmlio );
   return ( DONE );
}

std::ostream& operator<<( std::ostream& stream, const CLicenseItem& object )
{
   stream << "\n=========================================";
   stream << "\nLicense : " << object.strLicense;
   stream << "\nProduct : " << object.strProductSerial;
   stream << "\nHWKey   : " << object.strHWKey;
   stream << "\n=========================================";
   stream << "\nProduct name   : " << object.strProductName;
   stream << "\nProduct key    : " << object.strProductKey;
   stream << "\nProduct code   : " << object.strProductCode;
   stream << "\nProduct ver    : " << object.strProductVer;
   stream << "\n=========================================";
   stream << "\nResult Channel : " << object.nChannels;
   stream << "\nResult FC      : " << object.nFC;
   stream << "\n=========================================\n\n";
   return stream;
}

CLicenseManager::CLicenseManager()
{
}

CLicenseManager::~CLicenseManager()
{
}

int CLicenseManager::LicenseCertifyKey( CLicenseManager::Company company, int& out_channel )
{
   out_channel = 0;

   switch( company ) {
   case Company::NONE:
      break;
   case Company::CELLINX_FS: {

      m_licesnse_info.strProductName = "FSNETWORKS";
      m_licesnse_info.strProductKey  = "294EC3A3";
      m_licesnse_info.strProductCode = "FSN";
      m_licesnse_info.strProductVer  = "10";
      m_licesnse_info.LoadXML();

      std::string mmc_path = "/sys/devices/platform/hi_mci.1/mmc_host/mmc1";
      GetSdcardCidInfo_To_HwKey( mmc_path, m_licesnse_info.strHWKey );

      m_licesnse_info.nChannels = CertifyKey(
          m_licesnse_info.strLicense.c_str(),
          m_licesnse_info.strProductSerial.c_str(),
          m_licesnse_info.strHWKey.c_str(),
          m_licesnse_info.strProductKey.c_str(),
          m_licesnse_info.strProductCode.c_str(),
          m_licesnse_info.strProductVer.c_str(),
          m_licesnse_info.nFC );

      if( 1 == m_licesnse_info.nChannels && m_licesnse_info.nFC == FCLASS_VIDEOANALYTICS ) {
         out_channel = m_licesnse_info.nChannels;
         LOGI << "License Passed.";
         LOGD << m_licesnse_info;
         return ( DONE );
      }
      LOGE << "failed to pass license." << m_licesnse_info;
      return ( 1 );
   }
   }
   return ( 2 );
}

std::string CLicenseManager::exec( const std::string& cmd )
{
#ifdef __linux
   std::array<char, 128> buffer;
   std::string result;

   std::shared_ptr<FILE> pipe( popen( cmd.c_str(), "r" ), pclose );
   if( pipe == nullptr )
      throw std::runtime_error( "popen() failed!" );

   while( !feof( pipe.get() ) ) {
      if( fgets( buffer.data(), 128, pipe.get() ) != nullptr )
         result += buffer.data();
   }
   return result;
#elif __WIN32
   LOGF << "Not Implented for WIN.";
   return "";
#endif
}

int CLicenseManager::GetSdcardCidInfo_To_HwKey( const std::string& mmc_path, std::string& sdcard_hw_key )
{
#ifdef __linux
#ifdef __SUPPORT_LICENSE_SDCARD

   std::string stdout        = exec( "find " + mmc_path + " -name mmc1:????" );
   std::string mmc_full_path = stdout.substr( 0, stdout.size() - 1 );

   // NOTE(yhpark): Cellinx board has a bug that sometimes manfid is 0x000000. so we make fixed-prefix, '0000'.
   //stdout                    = exec( "cat " + mmc_full_path + "/manfid" ); // 0x00001b
   //std::string sdcard_manfid = stdout.substr( 4, 4 ); // 001b
   std::string sdcard_manfid = "0000";

   stdout                    = exec( "cat " + mmc_full_path + "/serial" ); // 0xfde2495d
   std::string sdcard_serial = stdout.substr( 2, 8 ); // fde2495d

   if( sdcard_manfid.size() == 4 && sdcard_serial.size() == 8 ) {
      sdcard_hw_key = sdcard_manfid + sdcard_serial;
   } else {
      LOGE << "Failed to get SDCard's CID info.\n"
           << "mmc_full_path: " << mmc_full_path << "\n"
           << "sdcard_manfid: " << sdcard_manfid << "\n"
           << "sdcard_serial: " << sdcard_serial;
   }

   LOGD << "Hardware Key in SD_CARD( manfid + serial ) = " << sdcard_hw_key;

   return ( DONE );
#endif // #ifdef __SUPPORT_LICENSE_SDCARD
#else
   return 1;
#endif // #ifdef __linux
}

int CLicenseManager::GetMACAddress( std::string& strMac )
#if defined( __WIN32 )
{
   ULONG buf_size = sizeof( IP_ADAPTER_INFO );
   BArray1D adapter_info( buf_size );
   if( GetAdaptersInfo( (IP_ADAPTER_INFO*)(byte*)adapter_info, &buf_size ) == ERROR_BUFFER_OVERFLOW ) adapter_info.Create( buf_size );
   if( GetAdaptersInfo( (IP_ADAPTER_INFO*)(byte*)adapter_info, &buf_size ) == ERROR_SUCCESS ) {
      BYTE* addr = ( (IP_ADAPTER_INFO*)(byte*)adapter_info )->Address;
      strMac     = sutil::sformat( "%02X-%02X-%02X-%02X-%02X-%02X", addr[0], addr[1], addr[2], addr[3], addr[4], addr[5] );
      return ( DONE );
   }
   return ( 1 );
}
#elif defined( __linux )
{
   struct ifreq ifr;
   struct ifconf ifc;
   char buf[1024];
   int success = 0;

   int sock = socket( AF_INET, SOCK_DGRAM, IPPROTO_IP );
   if( sock == -1 ) {
      /* handle error*/
      return 1;
   };

   ifc.ifc_len = sizeof( buf );
   ifc.ifc_buf = buf;
   if( ioctl( sock, SIOCGIFCONF, &ifc ) == -1 ) {
      /* handle error */
      return 2;
   }

   struct ifreq* it = ifc.ifc_req;
   const struct ifreq* const end = it + ( ifc.ifc_len / sizeof( struct ifreq ) );

   for( ; it != end; ++it ) {
      strcpy( ifr.ifr_name, it->ifr_name );
      if( ioctl( sock, SIOCGIFFLAGS, &ifr ) == 0 ) {
         if( !( ifr.ifr_flags & IFF_LOOPBACK ) ) { // don't count loopback
            if( ioctl( sock, SIOCGIFHWADDR, &ifr ) == 0 ) {
               success = 1;
               break;
            }
         }
      } else { /* handle error */
      }
   }

   unsigned char mac_address[6] = {
      0,
   };

   if( success ) {
      memcpy( mac_address, ifr.ifr_hwaddr.sa_data, 6 );
      strMac = sutil::sformat( "%02X-%02X-%02X-%02X-%02X-%02X",
                               mac_address[0], mac_address[1], mac_address[2], mac_address[3], mac_address[4], mac_address[5] );

      return ( DONE );
   }

   return 3;
}
#else
{
   return ( -1 );
}
#endif

﻿#pragma once

#include <iostream>
#include <fstream>
#include <vector>

#include "bccl_xml.h"

class CLicenseItem {
public:
   const std::string m_license_xml_file = "Configurations/VAEngineLicense.xml";
   int ReadWriteXML( BCCL::CXMLIO* pIO );
   int LoadXML();
   int SaveXML();
   friend std::ostream& operator<<( std::ostream& stream, const CLicenseItem& object );

   std::string strProductName;
   std::string strProductKey;
   std::string strProductCode;
   std::string strProductVer;

   std::string strLicense;
   std::string strProductSerial;
   std::string strHWKey;

   int nChannels = 0; // result
   int nFC       = 0; // result
};

class CLicenseManager {
public:
   enum Company {
      NONE,
      CELLINX_FS,
   };

   CLicenseManager();
   virtual ~CLicenseManager();

   int LicenseCertifyKey( Company company, int& out_channel );

   static std::string exec( const std::string& cmd );

private:
   int GetMACAddress( std::string& strMac );
   int GetSdcardCidInfo_To_HwKey( const std::string& mmc_path, std::string& sdcard_hw_key );

   CLicenseItem m_licesnse_info;
};

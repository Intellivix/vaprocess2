#pragma once

#ifdef __WIN32
 #include "Win32CL\ImageView.h"

class CCamera;

class CAuxVideoDisplayWnd : public Win32CL::CImageViewWnd
{
public:
   CCamera* Camera;
   BOOL m_bDoNotUpdateAuxVideoDispWndPos;

public:
   CAuxVideoDisplayWnd (int x,int y,CCamera* camera);

public:

   void SetVideoSize (int width,int height);

public:
   void OnClose (   );
   void OnMove  (int x, int y);

};
#endif // #ifdef __WIN32

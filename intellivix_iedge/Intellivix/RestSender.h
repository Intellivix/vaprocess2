﻿#ifndef RESTSENDER_H
#define RESTSENDER_H

#include <string>
#include <memory>

class CMetaGenerator;

//////////////////////////////////////////////////////////////////////////
//
// class CRestSender
//
//////////////////////////////////////////////////////////////////////////

class CRestSender {
private:
   class CRestSenderImpl;
   std::shared_ptr<CRestSenderImpl> m_pImpl;

   static CRestSender* m_instance;

private:
   CRestSender();
   ~CRestSender();

public:
   static CRestSender* getInstance();
   static void freeInstance();

   int Activate( const std::string& url );
   int Deactivate();

   int SendEventInfo( CMetaGenerator* generator );
   int SendLiveFps( CMetaGenerator* generator );
   int SendSystemStartup( CMetaGenerator* generator );
   int SendSystemShutdown( CMetaGenerator* generator );

   int SendText();
};

#endif // RESTSENDER_H

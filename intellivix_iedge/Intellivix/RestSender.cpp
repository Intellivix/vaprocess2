﻿#include "RestSender.h"
#include "RestDefine.h"
#include <cpprest/http_client.h>
#include "MetaGenerator.h"
#include "IntelliVIXDoc.h"
#include "CommonDef.h"

using namespace utility::conversions;
using namespace web;
using namespace web::http;
using namespace web::http::client;
using namespace pplx;

CCriticalSection g_csRestSender;

//////////////////////////////////////////////////////////////////////////
//
// class CRestSender
//
//////////////////////////////////////////////////////////////////////////

class CRestSender::CRestSenderImpl {
public:
   string_t m_url;
   std::shared_ptr<http_client> m_pClient;
   cancellation_token_source m_cts;
 

public:
   explicit CRestSenderImpl( const string_t& url )
   {
      m_url     = url;
      m_pClient = std::make_shared<http_client>( url );
   }

   ~CRestSenderImpl()
   {
      m_cts.cancel();
   }

   void Log( const http_response& response )
   {
      logi( "Rest code[%d] msg[\n%s]\n", response.status_code(), to_utf8string( response.to_string() ).c_str() );
   }

   void Log( const char* msg, const http_exception& exption )
   {
      loge( "rest erro[%s] msg[%s]", msg, exption.what() );
      //CIntelliVIXDoc::GetInstance()->SetExitProcess( Camera_Status_Rest_Send_Failed );
   }

   bool Send( const method& mtd, const string_t& text, bool wait = false )
   {
      if (CIntelliVIXDoc::GetInstance()->m_StandAlone)
         return false;
            
      CCriticalSectionSP co( g_csRestSender );
      
      if( m_url.empty() )
         return false;

      bool ret = true;
      try {
         task<http_response> task_response = m_pClient->request( mtd, text, m_cts.get_token() );

         std::function<void( task<http_response> task_ret )> func = [this]( task<http_response> task_ret ) {
            try {
               if( is_task_cancellation_requested() ) {
                  // TODO: Perform any necessary cleanup here...
                  // Cancel the current task.
                  cancel_current_task();
                  return true;
               }
               Log( task_ret.get() );
            } catch( const http_exception& exption ) {
               Log( "http response exception", exption );
            }
         };

         if( wait ) {
            task_response.then( func ).wait();
            task_response.get();
         } else {
            task_response.then( func );
         }

      } catch( const http_exception& exption ) {
         Log( "Rest send failed", exption );
         //ret = false;
      }

      return ret;
   }

   bool Send( const method& mtd, const string_t& text, const json_value& data, bool wait = false )
   {
      if (CIntelliVIXDoc::GetInstance()->m_StandAlone)
         return false;
      
      if( m_url.empty() )
         return false;
      
      bool ret = true;
      try {
         task<http_response> task_response = m_pClient->request( mtd, text, data, m_cts.get_token() );

         std::function<void( task<http_response> task_ret )> func = [this]( task<http_response> task_ret ) {
            try {
               if( is_task_cancellation_requested() ) {
                  // TODO: Perform any necessary cleanup here...
                  // Cancel the current task.
                  cancel_current_task();
                  return true;
               }
               Log( task_ret.get() );
            } catch( const http_exception& exption ) {
               Log( "http response exception2", exption );
            }
         };

         if( wait ) {
            task_response.then( func ).wait();
            task_response.get();
         } else {
            task_response.then( func );
         }

      } catch( const http_exception& exption ) {
         Log( "Rest send failed2", exption );
         //ret = false;
      }

      return ret;
   }
};

//////////////////////////////////////////////////////////////////////////
//
// class CRestSender
//
//////////////////////////////////////////////////////////////////////////

CRestSender* CRestSender::m_instance = nullptr;

CRestSender* CRestSender::getInstance()
{
   CCriticalSectionSP co( g_csRestSender );
   
   if( nullptr == m_instance ) {
      m_instance = new CRestSender();
   }

   return m_instance;
}

void CRestSender::freeInstance()
{
   CCriticalSectionSP co( g_csRestSender );
   
   if( m_instance ) {
      m_instance->Deactivate();
      delete m_instance;
      m_instance = nullptr;
   }
}

CRestSender::CRestSender()
{
}

CRestSender::~CRestSender()
{
}

int CRestSender::Activate( const std::string& url )
{
   CCriticalSectionSP co( g_csRestSender );
   
   if( url.empty() )
      return 1;

   Deactivate();

   m_pImpl = std::make_shared<CRestSenderImpl>( to_string_t( url ) );

   return 0;
}

int CRestSender::Deactivate()
{
   CCriticalSectionSP co( g_csRestSender );
   
   if( m_pImpl )
      m_pImpl.reset();

   return 0;
}

int CRestSender::SendEventInfo( CMetaGenerator* generator )
{
   string_t data = to_string_t( generator->GetString() );
   char buff[10000];
   snprintf(buff, sizeof(buff), "curl -X POST -H \"Content-Type: application/json\" -d \'%s\' %s", data.c_str(), m_pImpl->m_url.c_str());
   logi (buff);
   system (buff);
   
   return true;

   /*
   std::error_code code;
   json_value json = json_value::parse( data, code );
   if( m_pImpl->Send( methods::POST, U( "" ), json ) ) {
      logi( "Send Event OK : %s\n", generator->GetString().c_str() );
   }
   else {
      logw( "Send Event Failed : %s\n", generator->GetString().c_str() );
   }  
   */
   
   return 0;
}

int CRestSender::SendLiveFps( CMetaGenerator* generator )
{
   string_t data = to_string_t( generator->GetString() );
   char buff[10000];
   snprintf(buff, sizeof(buff), "curl -X POST -H \"Content-Type: application/json\" -d \'%s\' %s", data.c_str(), m_pImpl->m_url.c_str());
   logi (buff);
   system (buff);
   
   /*
   std::error_code code;
   json_value json = json_value::parse( data, code );
   if ( true == m_pImpl->Send( methods::POST, U( "" ), json ) ) {
      logi( "Send LiveFPS OK : %s\n", generator->GetString().c_str() );
   }
   else {
      logw( "Send LiveFPS Failed : %s\n", generator->GetString().c_str() );
   }
   */

   return 0;
}

int CRestSender::SendSystemStartup( CMetaGenerator* generator )
{
   string_t data = to_string_t( generator->GetString() );
   char buff[10000];
   snprintf(buff, sizeof(buff), "curl -X POST -H \"Content-Type: application/json\" -d \'%s\' %s", data.c_str(), m_pImpl->m_url.c_str());
   logi (buff);
   system (buff);
   
/*
   std::error_code code;
   json_value json = json_value::parse( data, code );
   if( true == m_pImpl->Send( methods::POST, U( "" ), json, true ) ) {
      logi( "System Startup OK[%s]\n", generator->GetString().c_str() );
      return 0;
   }
   else {
      logw( "System Startup Failed [%s]\n", generator->GetString().c_str() );
      return 1;
   }

*/
   return 0;
}

int CRestSender::SendSystemShutdown( CMetaGenerator* generator )
{
   string_t data = to_string_t( generator->GetString() );
   char buff[10000];
   snprintf(buff, sizeof(buff), "curl -X POST -H \"Content-Type: application/json\" -d \'%s\' %s", data.c_str(), m_pImpl->m_url.c_str());
   logi (buff);
   system (buff);

   /*
   std::error_code code;
   json_value json = json_value::parse( data, code );
   if( true == m_pImpl->Send( methods::POST, U( "" ), json, true ) )
      logi( "System shutdown OK[%s]\n", generator->GetString().c_str() );
   else
      logw( "System shutdown Failed[%s]\n", generator->GetString().c_str() );
   */
   return 0;
}

int CRestSender::SendText()
{
   json_value data   = json_value::object( true );
   data[U( "text" )] = json_value::string( U( "Hello world!" ) );

   if( true == m_pImpl->Send( methods::POST, U( "api/v1/status/stream" ), data, true ) )  {
      logi( "VAManager notity SendText  end \n" );
   }
   else {
      logi( "VAManager notity SendText  Failed \n" );
   }

   return 0;
}

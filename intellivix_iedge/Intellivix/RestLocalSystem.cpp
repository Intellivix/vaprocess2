﻿#include "RestLocalSystem.h"
#include "RestUtil.h"

using namespace web::http;
using namespace web_rest_util;


CRestLocalSystem::CRestLocalSystem(CLocalSystem* pLocalSystem)
{
   m_pLocalSystem = pLocalSystem;
   m_pRestServer  = std::make_shared<CRestServer>();
}

CRestLocalSystem::~CRestLocalSystem()
{

}

int CRestLocalSystem::Activate(const std::string& url)
{
   CRestServer::ParamActivate ac;

   ac.url = url;
   ac.handler = this;

   if (CRestServer::RetVal::Success != m_pRestServer->Activate(ac))
      return 1;

   return 0;
}

int CRestLocalSystem::Deactivate()
{
   m_pRestServer->Deactivate();

   return 0;
}

void CRestLocalSystem::OnRecv(const ParamRequest& request, ParamResponse& response)
{
   if (request.relative_paths.empty())
   {
      response.code = status_codes::BadRequest;
      response.data[U("res_code")] = json_value::number(400);
      response.data[U("error")]    = json_value::string(U("Relative path empty."));
      return;
   }

   std::vector<string_t> relative_paths = request.relative_paths;
   to_lower(relative_paths);

   string_t api_name = relative_paths[0];
   relative_paths.erase(relative_paths.begin());
   if (U("vixrest") != api_name)
   {
      response.code = status_codes::BadRequest;
      response.data[U("res_code")] = json_value::number(400);
      response.data[U("error")]    = json_value::string(U("Only vixrest api is supported."));
      return;
   }

   //string_t version = relative_paths[1];
   //string_t command = relative_paths[2];

   response.code = status_codes::BadRequest;
   response.data[U("res_code")] = json_value::number(400);
   response.data[U("error")]    = json_value::string(U("Not supported api."));
}



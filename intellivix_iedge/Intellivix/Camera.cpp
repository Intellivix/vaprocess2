﻿#include "StdAfx.h"
#include <vacl_ipp.h>
#include <GroupsockHelper.hh>
#include <RTSPCustomServer.h>
#include <VideoChannel.h>
#include "bccl_geomobj.h"
#include "LGU/Logger.h" // lgu+ log
#include "version.h"
#include "IntelliVIX.h"
#include "Camera.h"
#include "SupportFunction.h"
#include "LocalSystem.h"
#include "WaitForMultipleObjects.h"
#include "IntelliVIXDoc.h"
#include "MetaGenerator.h"
#include "TimerCheckerManager.h"
#include "RestSender.h"
#include "IvcpRtspSender.h"
#include "RtspParameter.h"

#ifdef __LIB_SIMPLEWEBSOCKET
#include "Websocket.h"
#endif // __LIB_SIMPLEWEBSOCKET

#ifdef __DEBUG_FGD
int _DVX_FGD, _DVY_FGD;
Win32CL::CImageView* _DebugView_FGD;
#endif

//#pragma pack (1)


//extern int g_nEvent_Count_;
//////////////////////////////////////////////////////////////////////////////
//
// Class: CIVXPTZStatusCallback
//
///////////////////////////////////////////////////////////////////////////////

class CIVXPTZStatusCallback : public CPTZStatusCallback {
private:
   CCamera* m_pCamera;

public:
   explicit CIVXPTZStatusCallback( CCamera* pCamera )
       : m_pCamera( pCamera )
   {
   }
   virtual ~CIVXPTZStatusCallback()
   {
   }

   virtual void OnPTZMoveStarted()
   {
      if( m_pCamera )
         m_pCamera->m_nPTZState |= PTZState_SignalPTZMoveStarted;
   }

   virtual void OnPTZMoveStopped()
   {
      if( m_pCamera )
         m_pCamera->m_nPTZState |= PTZState_SignalPTZMoveStopped;
   }
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: CIVXPTZCtrlCallback
//
///////////////////////////////////////////////////////////////////////////////

// CPTZCtrlCallback 클래스는 Client에서 PTZ Area와 같은 설정창에서 PTZ를 제어하기 위한 클래스이다.
// CCamera::m_PTZCtrlCallback 변수의 포인터가 CPTZCtrl::m_pPTZCtrlCallback에 할당되어 생성된다.

class CIVXPTZCtrlCallback : public CPTZCtrlCallback {
private:
   CCamera* m_pCamera;

public:
   explicit CIVXPTZCtrlCallback( CCamera* pCamera )
       : m_pCamera( pCamera )
   {
   }
   virtual ~CIVXPTZCtrlCallback()
   {
   }

   virtual void ContPTZMove( FPTZVector move_vel )
   {
      if( m_pCamera ) {
         m_pCamera->PTZMove( PTZMoveType_Cont, move_vel, TRUE );
      }
   }
   virtual int GotoAbsPos( FPTZVector pv )
   {
      if( m_pCamera ) {
         m_pCamera->PTZMove( PTZMoveType_GotoAbsPos, pv, TRUE );
      }
      return ( DONE );
   }
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: CRTSPStreamingChannel_IVX
//
///////////////////////////////////////////////////////////////////////////////

class CRTSPStreamingChannel_IVX : public CRTSPStreamingChannel {
public:
   virtual void OnSetParameter( LPVOID pParam, char const* parameterString )
   {
      if( pParam ) {
         CCamera* pCamera = (CCamera*)pParam;
         pCamera->OnRTSPSetParameter( parameterString );
      }
   }
   virtual void OnGetParameter( LPVOID pParam, char const* parameterString )
   {
      if( pParam ) {
         CCamera* pCamera = (CCamera*)pParam;
         pCamera->OnRTSPGetParameter( parameterString );
      }
   }
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: CCamera
//
///////////////////////////////////////////////////////////////////////////////
FBox2D CCamera::null_fox2d( 0.0f, 0.0f, 0.0f, 0.0f );
float CCamera::MovingCamCropRatio = 0.5f;

CCamera::CCamera( CLocalSystem* pSystem, int nChannelNo )
{
   int i;

   m_ivcpRtspSender   = std::make_shared<CIvcpRtspSender>( this );
   System             = pSystem;
   ChannelNo          = nChannelNo;
   UID                = -1;
   m_nState           = 0;
   m_nStateEx         = 0;
   m_nStateEx2        = 0;
   m_nPTZState        = 0;
   m_nPTZStateEx      = 0;
   m_nCurVACSetupType = VACSetupType_NotLoaded;
   LocalCamera        = NULL;
   TmpCamera          = NULL;

   SetCameraName( nChannelNo );

   // Image Processing 관련
   Options = CamOpt_AutoPTZCtrlMode | CamOpt_AutoPTZCtrlModeAutoOn | CamOpt_MutualAssistanceTrk | CamOpt_LockPTZ;

   m_bReductionFactorChecked = FALSE;
   DTCOptions                = 0; // CamOpt_EnableVAC;

   FrameRate        = 30.0f;
   ReductionFactor  = 1.0f;
   DesiredFrameRate = 7.0f;

#if defined( __WIN32 )
   AuxVideoDisplayWnd = NULL;
#endif

   m_nPrvState    = 0;
   m_nPrvPTZState = 0;

   m_pPTZCtrl                  = NULL;
   m_pActivePreset             = NULL;
   m_pActivePresetGroup        = NULL;
   NoVideoSignalChannels       = 0;
   ConnectingChannels          = 0;
   AuthorizationFailedChannels = 0;
   VC                          = NULL;
   PrevImageProcessCount       = -1;



   ISize2D s2d( REF_IMAGE_WIDTH, REF_IMAGE_HEIGHT );
   //ISize2D s2d( SrcImgSize.Width, SrcImgSize.Height );
   frg_detector = &ForegroundDetector_General;
   MainConfig_ObjectTracking.TrackingAreas.SetRefImageSize( s2d );
   MainConfig_ObjectTracking.NontrackingAreas.SetRefImageSize( s2d );
   MainConfig_ObjectTracking.NonHandoffAreas.SetRefImageSize( s2d );
   MainConfig_ObjectTracking.DynBkgAreas.SetRefImageSize( s2d );
   MainConfig_EventDetection.EventZones.SetRefImageSize( s2d );
   MainConfig_RealObjectSizeEstimation.RefImgSize( SrcImgSize.Width, SrcImgSize.Height );

   if( System ) {
      m_VideoFrameLive.Create( 0, 0, VideoSrcType_Live );

      CVideoChannelInfo vcInfo;
      vcInfo.m_pParam      = (LPVOID)this;
      VC                   = System->VCM.CreateVideoChannel( vcInfo );
      VC->m_nChannelNo_IVX = nChannelNo;
      VCID                 = VC->m_nVCID;
   }

   m_nPTZTrkModeAutoOnWaitTime = 60 * 5;
   m_nPresetTouringStartTime   = 0;
   PTZMouseMoveMode            = PTZMouseMove_Joystick;
   SPTZTrkMode                 = SPTZTrkMode_None;
   m_nPTZTourReturnDirection   = PTZTourReturnDirection_Next;
   m_nPTZMoveType              = -1;
   m_nPresetMoveType           = -1;
   m_nPresetMoveRemainTime     = 0;
   m_nPresetStartTime          = 0;
   m_nPTZCtrlStopTime          = 0;
   m_nPTZMoveStopWaitTime      = 7000;
   m_fDisplayZoomStepSize      = 0.0f;
   m_szActivePresetName[0]     = 0;

   CPresetGroup* pGroup = new CPresetGroup;
   sprintf( pGroup->Name, "Group-%d", 1 );
   m_PresetGroupList.Add( pGroup );
   m_PresetGroupList.SelectedGroupIndex = 0;

   m_pSPTZTracker            = new CSinglePTZTrack;
   SinglePTZTrackingObj.Next = SinglePTZTrackingObj.Prev = NULL;
   SinglePTZTrackingEvent.Next = SinglePTZTrackingEvent.Prev = NULL;
   SinglePTZTrackingObj.Status                               = TO_STATUS_VERIFIED;

   HomePosStartTime    = 0;
   m_PTZStatusCallback = std::make_shared<CIVXPTZStatusCallback>( this );
   m_PTZCtrlCallback   = std::make_shared<CIVXPTZCtrlCallback>( this );

   m_fCurFocalLength = 600.0f;

   m_pvUserOffsetPos( 0.0f, 0.0f, 1.0f );

   AfxGetLocalFileTime( &m_ftLastDoImageProcessTime );
   AfxGetLocalFileTime( &m_ftLastDoDisplayTime );

   for( i = 0; i < IVX_VIDEO_STREAM_NUM; i++ ) {
      m_bStartVideoStreamThread[i]   = FALSE;
      m_bSendVideo[i]                = FALSE;
      m_bStopVideoStreamingThread[i] = FALSE;
      m_TimerVideoStreaming[i]       = NULL;
   }

   m_bExistEvent = FALSE;

   m_bSendLiveVideo              = FALSE;
   m_bPrevPositionInitTimePassed = FALSE;

   m_bGetColorSrcImageOK   = FALSE;
   m_bGetGrayScaleSrcImage = FALSE;
   m_bGetYuy2SrcImageOK    = FALSE;

   m_nVideoFrameFlag = 0;

   m_nStoppingVideoAnalyticsStartTime        = 0;
   m_nStoppingVideoAnalyticsPeriodInMilliSec = 0;

   VAMode = VAE_MODE_GENERAL;

   for( int ii = 0; ii < IVX_VIDEO_STREAM_NUM; ii++ )
      m_RTSPStreamingChannel[ii] = std::make_shared<CRTSPStreamingChannel_IVX>();

   m_bResetObjCountUsed            = FALSE;
   m_nResetObjCount_PeriodTime     = -1;
   m_nResetObjCount_LastActiveTime = -1;

   m_dwMaskingFlags     = MaskingFlag_None;
   m_nMaskingMosaicSize = 8;
   m_fMaskingZoomFactor = 1.25f;

   nEvent_ID_count_ = 0;
   nObject_ID_count_ = 0;

   // TODO(jongoh): 참고 사항
   // 객체 분류 테스트를 위해 추가
   //Config_FaceDetection.m_nObjectAnalysisMode |= ObjectAnalysisMode_DNNObjectClassification;

   ObjectAnalysisMode_DNN_ = TRUE;

   
   // overlap event proc
   // ER_EVENT_CAMERA_DAMAGE = 60 , But don't need setting 26~60 so Camera_damage is 26
   int Max_event = ER_EVENT_CROWD_DENSITY + 1;
   for(int i=0; i <= Max_event; i++) {
      Check_OverlapEvent_[i].EventType = i;
      Check_OverlapEvent_[i].EventDetectTime = 1;
      if (i == Max_event)   {
         Check_OverlapEvent_[i].EventType = ER_EVENT_CAMERA_DAMAGE;
      }
   }
   
   
   m_pDoc = CIntelliVIXDoc::GetInstance();
}

CCamera::~CCamera()
{
   int i;
   for( i = 0; i < IVX_VIDEO_STREAM_NUM; i++ ) {
      if( m_bStartVideoStreamThread[i] )
         StopVideoStreamingThread( i );
   }
#if defined( __WIN32 )
   if( AuxVideoDisplayWnd ) delete AuxVideoDisplayWnd;
#endif
   if( System )
      System->VCM.DeleteVideoChannel( VCID );

   delete m_pSPTZTracker;

   VC = NULL;
}

BOOL CCamera::IsVisible()
{
   if( m_nDisplayLogCnt < 10 ) return TRUE;
   return FALSE;
}

BOOL CCamera::IsDecodingLiveVideo()
{
   BOOL bDecodingLiveVideo = FALSE;
   if( m_nStateEx & CamStateEx_ShowLiveFrame )
      bDecodingLiveVideo = TRUE;
   return bDecodingLiveVideo;
}

BOOL CCamera::IsVideoSignalOk()
{
   UINT64 nCamFlag       = VC->GetCameraFlag();
   BOOL bSignalOKChExist = ( BOOL )( ( ~NoVideoSignalChannels & nCamFlag ) > 0 );

   return bSignalOKChExist;
}

BOOL CCamera::IsActivated()
{
   if( !( m_nState & CamState_Activate ) || ( m_nState & ( CamState_ToBeRestarted | CamState_ToBeClosed | CamState_InitializationFailure | CamState_ToBeDeactivate | CamState_ToBeActivate ) ) ) return FALSE;
   return TRUE;
}

void CCamera::SetCameraName( LPCSTR szCameraName )
{
   CameraName = szCameraName;
}

void CCamera::SetCameraName( int nChannelNo )
{
   CameraName = sutil::sformat( "Camera-%d", nChannelNo + 1 );
}

void CCamera::SetVideoSize( int nVideoType, int nWidth, int nHeight )
{
   CVideoFrame* pVideoFrame                = GetDispVideoFrame( nVideoType );
   pVideoFrame->m_VideoFrameInfo.m_nWidth  = nWidth;
   pVideoFrame->m_VideoFrameInfo.m_nHeight = nHeight;
}

int CCamera::GetFrameStateString( IVXVideoFrameInfo* pVideoFrameInfo, std::string& str )
{
   int nFrameState = 0;
   ASSERT( pVideoFrameInfo );
   if( pVideoFrameInfo->m_nState & FrameState_BackgroundModeling ) {
      //str.Format(MLANG(IDS_CREATING_BG_MODEL));
      nFrameState = FrameState_BackgroundModeling;
   } else if( pVideoFrameInfo->m_nState & FrameState_PTZPosInitializing ) {
      //str.Format(MLANG(IDS_INIT_PTZ));
      nFrameState = FrameState_PTZPosInitializing;
   } else if( pVideoFrameInfo->m_nState & FrameState_SuddenSceneChangeIsDetected ) {
      //str.Format(MLANG(IDS_SUDDEN_SCENE_CHANGE_DETECTED));
      nFrameState = FrameState_SuddenSceneChangeIsDetected;
   } else if( pVideoFrameInfo->m_nState & FrameState_OpenSerialPortIsFailed ) {
      //str.Format (MLANG(IDS_FAILURE_OPEN_SERIAL_PORT_D), VC->m_PTZInfo.m_nCOMPortNo);
      nFrameState = FrameState_OpenSerialPortIsFailed;
   } else if( pVideoFrameInfo->m_nState & FrameState_GoingToPreset ) {
      m_csActivePresetName.Lock();
      // if (m_pActivePreset) str.Format(MLANG(IDS_MOVING_PRESET_POSITION_S), m_szActivePresetName);
      // else                 str.Format(MLANG(IDS_MOVING_PRESET_POSITION));
      m_csActivePresetName.Unlock();
      nFrameState = FrameState_GoingToPreset;
   } else if( ( pVideoFrameInfo->m_nState & FrameState_PTZGotoHomePos ) && !( pVideoFrameInfo->m_nState & FrameState_PTZCameraMoving ) ) {
      int nSec = ( m_nGotoHomePosWaitingTime - ( GetTickCount() - m_nGotoHomePosWaitingStartTime ) ) / 1000;
      //if(nSec <= 3 && nSec > 0)
      //   str.Format(MLANG(IDS_MOVING_HOME_POSITION), nSec);
      nFrameState = FrameState_PTZGotoHomePos;
   } else if( pVideoFrameInfo->m_nState & FrameState_SinglePTZTracking ) {
      str         = "Single PTZ Tracking";
      nFrameState = FrameState_SinglePTZTracking;
   }
   return ( nFrameState );
}

float CCamera::GetFrameRateLive( int nVCProcCallbackFuncType )
{
   bool bProcessing = false;
   if( ( m_nState & CamState_DoVideoAnalytics ) || ( m_nPTZState & PTZState_SPTZTrkOngoing ) )
      bProcessing = true;

   std::function<float( float, int )> round = []( float x, int dig ) {
      if( 0. == x ) return 0.;
      return floor( x * pow( 10, dig ) + 0.5 ) / pow( 10, dig );
   };

   float fps = 0;
   switch( nVCProcCallbackFuncType ) {
   case VCProcCallbackFuncType_Display:
      fps = round( VC->GetRealTimeFrameRate( VCProcCallbackFuncType_Display ), 2 );
      break;
   case VCProcCallbackFuncType_ImgProc:
      fps = bProcessing ? round( VC->GetRealTimeFrameRate( VCProcCallbackFuncType_ImgProc ), 2 ) : 0;
      if(fps < 1.0f) fps = 7.0f;
      //fps = round( VC->GetRealTimeFrameRate( VCProcCallbackFuncType_ImgProc ), 2 );
      break;
   default:
      break;
   }

   return (float)fps;
}

void CCamera::GetDescString( int nDescID, std::string& str )
{
   if( IS_SUPPORT( __SUPPORT_FACE_RECOGNITION ) || IS_SUPPORT( __SUPPORT_HEAD_COUNTING ) ) {
      if( ChannelNo < 0 )
         return;
   }

   int i;

   str = _T("");
   switch( nDescID ) {
   case CD_CAMERA_ID:
      str = sutil::sformat( "[C%d]", ChannelNo + 1 );
      break;
   case CD_SYSTEM_NAME:
      str = GetSystem()->GetSystemName();
      break;
   case CD_CAMERA_NAME:
   case CD_DISPLAY_NAME:
      str = CameraName;
      break;
   case CD_DISPLAY_FULL_NAME:
      str = sutil::sformat( "%s [C%d]", CameraName.c_str(), ChannelNo + 1 );
      break;
   case CD_CHANNEL_NO:
      str = sutil::sformat( "Camera-%d", ChannelNo + 1 );
      break;
   case CD_CHANNEL_NO_BRF:
      str = sutil::sformat( "CH%d", ChannelNo + 1 );
      break;
   case CD_FRAME_RATE_LIVE: {
      BOOL bProcessing = FALSE;
      if( ( m_nState & CamState_DoVideoAnalytics ) || ( m_nPTZState & PTZState_SPTZTrkOngoing ) )
         bProcessing = TRUE;

      int nCaptureFrameRate   = int( VC->GetRealTimeFrameRate( VCProcCallbackFuncType_Display ) + 0.5f );
      int nDisplayFrameRate   = int( VC->GetRealTimeFrameRate( VCProcCallbackFuncType_Display ) + 0.5f );
      int nImageProcFrameRate = bProcessing ? int( VC->GetRealTimeFrameRate( VCProcCallbackFuncType_ImgProc ) + 0.5f ) : 0;
      str                     = sutil::sformat( "D:%d P:%d", nDisplayFrameRate, nImageProcFrameRate );
   } break;
   case CD_STATUS: {
      if( m_nState & CamState_ChannelUnregistered )
         ; // str.Format(MLANG(IDS_VIDEO_STATUS_MSG_7), ChannelNo+1);
      else {
         if( m_nState & CamState_OpenVideoFileFailed )
            str = sutil::sformat( "Failure: Open Video File" );
         else if( m_nState & CamState_OpenPVTFileFailed )
            str = sutil::sformat( "Failure: Load Cylinder Mapping Table." );
         else if( m_nState & CamState_OpenVCTFileFailed )
            str = sutil::sformat( "Failure: Open VCT File" );
         else if( m_nState & CamState_CanNotFindSourceVideoChannel )
            str = sutil::sformat( "Cannot find source video channel." );
         else if( NoVideoSignalChannels ) { // 비디오 신호 체크
            std::string strChNum;
            int nCamNum           = VC->GetCameraNum();
            BOOL bDisplayNoSignal = TRUE;
            if( bDisplayNoSignal ) {
               str = sutil::sformat( "No Signal! " );

               if( nCamNum >= 2 ) {
                  int nCamCheckCount = 0;
                  UINT64 nCamFlag    = VC->GetCameraFlag();
                  for( i = 0; nCamCheckCount < nCamNum; i++ ) {
                     if( nCamFlag & ( (UINT64)1 << i ) ) {
                        if( NoVideoSignalChannels & ( 1 << i ) ) {
                           strChNum = sutil::sformat( "%d ", i + 1 );
                           str += strChNum;
                        }
                        nCamCheckCount++;
                     }
                  }
               }
            }
            if( AuthorizationFailedChannels ) {
               str += "\n";
               std::string strText;
               strText = sutil::sformat( "Login Failed " );
               str += strText;
               if( nCamNum >= 2 ) {
                  for( i = 0; i < nCamNum; i++ ) {
                     if( AuthorizationFailedChannels & ( 1 << i ) ) {
                        strChNum = sutil::sformat( "%d ", i + 1 );
                        str += strChNum;
                     }
                  }
               }
            } else if( ConnectingChannels ) {
               str += "\n";
               std::string strText;
               strText = sutil::sformat( "Connecting..." );
               str += strText;
               if( nCamNum >= 2 ) {
                  for( i = 0; i < nCamNum; i++ ) {
                     if( ConnectingChannels & ( 1 << i ) ) {
                        strChNum = sutil::sformat( "%d ", i + 1 );
                        str += strChNum;
                     }
                  }
               }
            }
         }
      }
   } break;
   case CD_VIDEO_RESOLUTION:
      str = sutil::sformat( "%dx%d", SrcImgSize.Width, SrcImgSize.Height ); // MS: 멀티스트림일 때와 아닐 때를 구분하여 정보를 표시해야 한다.
      break;
   case CD_CAMERA_TYPE: {
      //str = VideoChannelTypeString.GetString (VC->m_nVideoChannelType);
      //std::string strPTZCam;
      //strPTZCam = sutil::sformat("(%s)", "PTZ Camera");
      //if (IsPTZCamera (   )) str += strPTZCam;
   } break;
   case CD_PTZ_TOURING_STATE: {
      BOOL bDrawTourState = TRUE;
      if( !( m_nPTZState & PTZState_PresetTour ) )
         bDrawTourState = FALSE;
      if( m_nPTZState & PTZState_PresetTour && m_nPTZState & PTZState_PresetTourPaused )
         bDrawTourState = FALSE;
      if( IsPTZMoving() )
         bDrawTourState = FALSE;
      if( !m_pActivePreset )
         bDrawTourState = FALSE;

      if( bDrawTourState ) {
         int nPresetNum = 0;
         m_csActivePreset.Lock();
         if( m_pActivePresetGroup )
            nPresetNum = m_pActivePresetGroup->GetNumItems();
         m_csActivePreset.Unlock();
         if( nPresetNum > 1 ) {
            // 경과 시간 스트링을 얻는다.
            CDwellTime dwell_time;
            dwell_time.Seconds = m_nPresetMoveRemainTime;
            std::string strRemainTime;
            // WARNING(yhpark): if dwell_time stringfy to strRemainTime, then remove this comments.
            strRemainTime = sutil::sformat( "[%d]", dwell_time );
            LOGD << "test test : " << strRemainTime;
            str += strRemainTime;
         }
      }

      if( bDrawTourState ) {
         // 현재 프리셋의 이름을 얻는다.
         m_csActivePreset.Lock();
         if( m_pActivePreset ) {
            std::string preset_name;
            preset_name = sutil::sformat( "[%s]", m_pActivePreset->Name );
            if( !str.empty() ) str += " ";
            str += preset_name;
         }
         m_csActivePreset.Unlock();
      }
   } break;
   case CD_BITRATE: {
      std::string strDisp;
      if( VideoChannelType_IPCamera == VC->m_nVideoChannelType ) {
         float fBitrate = VC->GetDisplayBitrate();
         if( fBitrate > 0.0f ) {
            strDisp = sutil::sformat( "D:%4d Kbps", int( fBitrate / 1000.0f ) );
         }
      }
      str = strDisp;
   } break;
   default:
      ASSERT( FALSE );
      break;
   }
}

EventZone* CCamera::FindEventZone( int zone_id )
{
   CCriticalSectionSP co( CS_EventDetector );
   EventZoneEx* evt_zone = (EventZoneEx*)EventDetector.EventZones.First();
   while( evt_zone ) {
      if( evt_zone->ID == zone_id ) {
         return evt_zone;
      }
      evt_zone = (EventZoneEx*)EventDetector.EventZones.Next( evt_zone );
   }
   return NULL;
}

BOOL CCamera::ShowFrame_LiveVideo( BYTE* pDrawBuffer, int nImgWidth, int nImgHeight )
{
   CVideoFrame videoFrame;
   m_VideoFrameLive.LockVideoFrameInfo();
   videoFrame.Create( nImgWidth, nImgHeight, VideoSrcType_Live );
   videoFrame.m_VideoFrameInfo = m_VideoFrameLive.m_VideoFrameInfo;
   m_VideoFrameLive.UnlockVideoFrameInfo();
   BOOL bRet = ShowFrame( pDrawBuffer, &videoFrame );
   return bRet;
}

void CCamera::UpdateObjectCounters( IVXVideoFrameInfo& s_videoFrameInfo )
{
   int i, j;

   std::vector<CPreset*> vtAllPreset;
   // vtAllPreset 리스의 첫항목은 디폴트VACL 설정을 로드하기 위해 아래와같이 NULL 값을 푸시한다.
   vtAllPreset.push_back( NULL );

   // 현재 선택된 그룹의 모든 프리셋을 리스트에 추가한다.
   CPresetGroup* c_group = m_PresetGroupList.GetSelectedGroup();
   if( c_group && c_group->GetNumItems() > 0 ) {
      CPreset* pPreset = c_group->First();
      while( pPreset ) {
         vtAllPreset.push_back( pPreset );
         pPreset = pPreset->Next;
      }
   }

   EventDetection* pEventDetector;
   int nItemNum = vtAllPreset.size();
   for( i = 0; i < nItemNum; i++ ) {
      CPreset* pPreset = vtAllPreset[i];

      if( pPreset == NULL )
         pEventDetector = &MainConfig_EventDetection; // 기본 VACL 설정 (preset 별 vac 설정이 아닌) 인 경우.
      else
         pEventDetector = &pPreset->Config_EventDetection;

      EventRuleEx* pEventRuleEx = (EventRuleEx*)pEventDetector->EventRules.First();
      while( pEventRuleEx ) {

         int nZoneNum = s_videoFrameInfo.m_EventZoneCountInfo.m_nZoneNum;
         for( j = 0; j < nZoneNum; j++ ) {
            CIVCPEventZoneCount& zoneCountInfo = s_videoFrameInfo.m_EventZoneCountInfo.m_pZoneCounts[j];
            if( pEventRuleEx->ID == zoneCountInfo.m_nZoneID ) {
               pEventRuleEx->ERC_Counting.Count = zoneCountInfo.m_nZoneCount;
            }
         }
         pEventRuleEx = (EventRuleEx*)pEventRuleEx->Next;
      }
   }
}

BOOL CCamera::IsShowLiveFrame()
{

   BOOL bShowFrame = FALSE;
   if( IsVisible() ) bShowFrame = TRUE;

   return bShowFrame;
}

BOOL CCamera::ShowFrame( BYTE* pDrawBuffer, CVideoFrame* pVideoFrame, BOOL bRepaint )
{
   ASSERT( pVideoFrame );
   IVXVideoFrameInfo* pVideoFrameInfo = &pVideoFrame->m_VideoFrameInfo;

   int nVideoType  = pVideoFrameInfo->m_nVideoSrcType;
   BOOL bShowFrame = FALSE;

   // 실시간 영상이 화면에 출력되야하는 상황인지 체크
   if( nVideoType == VideoSrcType_Live ) {
      if( m_nStateEx & CamStateEx_ShowLiveFrame )
         bShowFrame = TRUE;
   }
   if( !bShowFrame ) return FALSE;

   int nWidth  = pVideoFrameInfo->m_nWidth;
   int nHeight = pVideoFrameInfo->m_nHeight;
   ASSERT( nWidth > 0 && nHeight > 0 );

   BOOL bVideoSizeChanged = FALSE;

   // 멀티 스트리밍 시 bVideoSizeChanged가 TRUE가 되지 않는 버그 수정 (mkjang-141229)
   if( pVideoFrame->m_nPrevWidth > 0 && pVideoFrame->m_nPrevHeight > 0 ) {
      if( nWidth != pVideoFrame->m_nPrevWidth || nHeight != pVideoFrame->m_nPrevHeight ) {
         bVideoSizeChanged = TRUE;
      }
   }
   pVideoFrame->m_nPrevWidth  = nWidth;
   pVideoFrame->m_nPrevHeight = nHeight;

   if( bVideoSizeChanged ) {
      SetVideoSize( nVideoType, nWidth, nHeight );
   }

   //비디오 데이타 세팅
   pVideoFrame->m_pYUY2Data = pDrawBuffer;

   // 실시간 영상
   if( nVideoType == VideoSrcType_Live ) {
      if( IsVisible() ) {
      }
   }
   if( nVideoType == VideoSrcType_Live ) // Live 데이터일 경우에만
   {
      BOOL bGetVideoImg = FALSE;
      // 세팅 상태일 때만 하면 될 듯?
      if( bGetVideoImg ) {
         BGRImage s_image;
         if( GetVideoImage( VideoSrcType_Live, s_image ), GetVideoFrameMode_Largest ) {
         }
      }
   }
   return TRUE;
}

int CCamera::IsEventAlarmOn( int nVideoType )
{
   if( nVideoType == VideoSrcType_Live ) {
      if( m_nState & CamState_EventAlarmOn )
         return TRUE;
   }

   return FALSE;
}

int CCamera::GetStreamIndexOfVideoImage( int nVideoType, int nGetVideoFrameMode )
{
   int nStreamIndex = -1;
   if( nVideoType == VideoSrcType_Live ) {
      if( ( m_nState & CamState_InitializationFailure ) )
         return nStreamIndex;
   }

   if( nVideoType == VideoSrcType_Live ) {
      switch( nGetVideoFrameMode ) {
      case GetVideoFrameMode_Largest:
         nStreamIndex = VC->GetStreamIdxOfTheLargestVideoSizeInDecoding();
         break;
      case GetVideoFrameMode_Smallest:
         nStreamIndex = VC->GetStreamIdxOfTheSmallestVideoSizeInDecoding();
         break;
      case GetVideoFrameMode_EMap:
         nStreamIndex = VC->GetStreamIdxOfProcCallbackFunc( VCProcCallbackFuncType_Display );
         break;
      }
   }
   return nStreamIndex;
}

BOOL CCamera::GetVideoImage( int nVideoType, BGRImage& d_image, int nGetVideoFrameMode )
{
   // GetVideoImage의 사용처
   // - 스냅샷, 이메일 전송에 사용되는 정지영상 얻기.
   // - E-Map의 비디오 이미지 업데이트용, 컬러 설정의 비디오영상 얻기 용도.

   // 멀티스트리밍에서 비디오이미지를 얻는 문제
   // 스냅샷, 이메일 -> 디코딩 되고 있는 스트림 중에 가장 최고해상도.
   // 비디오 컬러 설정 -> 적당한 해상도를 선택한다.
   // E-Map -> 우선적으로 저해상도의 스트림을 얻는다.

   if( nVideoType == VideoSrcType_Live ) {
      if( ( m_nState & CamState_InitializationFailure ) )
         return FALSE;
   }
   BYTE* pYUY2Src = NULL;
   ISize2D s2SrcImgSize;

   if( nVideoType == VideoSrcType_Live ) {
      int nStreamIdx = GetStreamIndexOfVideoImage( nVideoType, nGetVideoFrameMode );

      int nImgWidth, nImgHeight;
      VC->GetVideoBuff( nStreamIdx, &pYUY2Src, nImgWidth, nImgHeight );
      if( pYUY2Src == NULL ) {
         return FALSE;
      }
      s2SrcImgSize( nImgWidth, nImgHeight );
   } else {
      CVideoFrame* pVideoFrame = GetDispVideoFrame( nVideoType );
      pYUY2Src                 = pVideoFrame->m_pYUY2Data;
      if( pVideoFrame->GetWidth() == 0 || pVideoFrame->GetHeight() == 0 || !pYUY2Src ) return FALSE;
      s2SrcImgSize( pVideoFrame->GetWidth(), pVideoFrame->GetHeight() );
   }

   if( s2SrcImgSize.Width ) {
      d_image.Create( s2SrcImgSize.Width, s2SrcImgSize.Height );
      BCCL::ConvertYUY2ToBGR( pYUY2Src, d_image );
   } else {
      return FALSE;
   }

   return TRUE;
}

BOOL CCamera::GetYUY2VideoImage( BYTE** pYUY2Img, int& nWidth, int& nHeight )
{
   // GetYUY2VideoImage 함수의 사용처
   // 1. 섬네일을 만들때
   // 2. IVCP에서 이벤트 섬네일을 만들때
   // CCamera::GetVideoImage 함수의 내용가 대부분 겹치기때문에 추후에 통합이 필요할 듯 함.
   UINT64 nCamFlag       = VC->GetCameraFlag();
   BOOL bSignalOKChExist = ( ~NoVideoSignalChannels & nCamFlag ) > 0;
   if( ( m_nState & CamState_InitializationFailure ) || !bSignalOKChExist )
      return FALSE;

   int nStreamIdx = 0; // 메인스트림.
   BYTE* pYUY2Src = NULL;
   VC->GetVideoBuff( nStreamIdx, &pYUY2Src, nWidth, nHeight );
   if( pYUY2Src == NULL ) {
      return FALSE;
   }
   *pYUY2Img = pYUY2Src;

   return TRUE;
}

int CCamera::CheckDeactivation( CCamera* camera, int filter )
{
   if( filter & IVXSetupItemMask_VADetection ) {
      if( ( DTCOptions & CamOpt_EnableVAC ) != ( camera->DTCOptions & CamOpt_EnableVAC ) ) return ( 1 );
      if( ReductionFactor != camera->ReductionFactor ) return ( 1 );
      if( DesiredFrameRate != camera->DesiredFrameRate ) return ( 1 );
      if( VAMode != camera->VAMode ) return ( 1 );
   }

   if( filter & IVXSetupItemMask_VideoInput ) {
      if( System->VCM.CheckRestart( VC->m_nVCID ) ) return ( 1 );
      if( VC->CheckRestart( camera->VC->GetVideoChannelInfo() ) ) return ( 1 );
   }

   int FLAG_DEACTIVATE = ( CHECK_SETUP_RESULT_RESTART | CHECK_SETUP_RESULT_RESTART_ALL );

   if( filter & IVXSetupItemMask_VADetection ) {
      if( MainConfig_ForegroundDetection.CheckSetup( &camera->MainConfig_ForegroundDetection ) & FLAG_DEACTIVATE ) return ( 1 );
      if( MainConfig_ObjectTracking.CheckSetup( &camera->MainConfig_ObjectTracking ) & FLAG_DEACTIVATE ) return ( 1 );
      if( MainConfig_ObjectFiltering.CheckSetup( &camera->MainConfig_ObjectFiltering ) & FLAG_DEACTIVATE ) return ( 1 );
      if( MainConfig_HeadDetection.CheckSetup( &camera->MainConfig_HeadDetection ) & FLAG_DEACTIVATE ) return ( 1 );
   }

   if( filter & IVXSetupItemMask_VACameraGeometry ) {
      if( MainConfig_RealObjectSizeEstimation.CheckSetup( &camera->MainConfig_RealObjectSizeEstimation ) & FLAG_DEACTIVATE )
         return ( 1 );
   }

   if( filter & IVXSetupItemMask_VAEventDetection ) {
      if( MainConfig_EventDetection.CheckSetup( &camera->MainConfig_EventDetection ) & FLAG_DEACTIVATE ) return ( 1 );
      if( MainConfig_SmokeDetection.CheckSetup( &camera->MainConfig_SmokeDetection ) & FLAG_DEACTIVATE ) return ( 1 );
   }

   return ( DONE );
}

int CCamera::GetChangedSetupItem( CCamera* camera )
{
   int c_item = 0;
   FileIO orgBuff;
   FileIO tmpBuff;
   CXMLIO xmlOrg;
   CXMLIO xmlTmp;

   if( DTCOptions != camera->DTCOptions ) c_item |= IVXSetupItemMask_VADetection;
   if( ReductionFactor != camera->ReductionFactor ) c_item |= IVXSetupItemMask_VADetection;
   if( DesiredFrameRate != camera->DesiredFrameRate ) c_item |= IVXSetupItemMask_VADetection;
   if( VAMode != camera->VAMode ) c_item |= IVXSetupItemMask_VADetection;

   // VACL 모듈 변경 체크
   int FLAG_CHANGE = ( CHECK_SETUP_RESULT_CHANGED );

   if( MainConfig_ForegroundDetection.CheckSetup( &camera->MainConfig_ForegroundDetection ) & FLAG_CHANGE ) c_item |= IVXSetupItemMask_VADetection;
   if( MainConfig_ObjectTracking.CheckSetup( &camera->MainConfig_ObjectTracking ) & FLAG_CHANGE ) c_item |= IVXSetupItemMask_VADetection | IVXSetupItemMask_VAEventDetection;
   if( MainConfig_ObjectFiltering.CheckSetup( &camera->MainConfig_ObjectFiltering ) & FLAG_CHANGE ) c_item |= IVXSetupItemMask_VADetection;
   if( MainConfig_ObjectClassification.CheckSetup( &camera->MainConfig_ObjectClassification ) & FLAG_CHANGE ) c_item |= IVXSetupItemMask_VADetection;
   if( MainConfig_RealObjectSizeEstimation.CheckSetup( &camera->MainConfig_RealObjectSizeEstimation ) & FLAG_CHANGE ) c_item |= IVXSetupItemMask_VACameraGeometry;
   if( MainConfig_HeadDetection.CheckSetup( &camera->MainConfig_HeadDetection ) & FLAG_CHANGE ) c_item |= IVXSetupItemMask_VADetection;
   if( MainConfig_EventDetection.CheckSetup( &camera->MainConfig_EventDetection ) & FLAG_CHANGE ) c_item |= IVXSetupItemMask_VAEventDetection;
   if( MainConfig_SmokeDetection.CheckSetup( &camera->MainConfig_SmokeDetection ) & FLAG_CHANGE ) c_item |= IVXSetupItemMask_VAEventDetection;

   // VIDEO INPUT
   if( IsDiff( &Config_PTZC, &camera->Config_PTZC, sizeof( CConfig_PTZCamera ) ) ) c_item |= IVXSetupItemMask_VideoInput;
   if( System->VCM.CheckChange( VC->m_nVCID ) ) c_item |= IVXSetupItemMask_VideoInput;
   if( VC->CheckChange( camera->VC->GetVideoChannelInfo() ) ) c_item |= IVXSetupItemMask_VideoInput;

   // Video Streaming
   orgBuff.SetLength( 0 );
   tmpBuff.SetLength( 0 );
   xmlOrg.SetFileIO( &orgBuff, XMLIO_Write );
   xmlTmp.SetFileIO( &tmpBuff, XMLIO_Write );
   Config_VS.Write( &xmlOrg );
   camera->Config_VS.Write( &xmlTmp );
   if( orgBuff.IsDiff( tmpBuff ) ) c_item |= IVXSetupItemMask_VideoStreaming;

   return c_item;
}

int CCamera::LinkToTemp( CCamera* t_cam )
{
   TmpCamera = t_cam;
   if( TmpCamera ) {
      // 설정 창 오픈속도 향상을 위해 전체 카메라 설정로드 방식에서 사용자 선택 카메라 설정만 로드하는 방식으로 변경한 경우
      // 가상 PTZ를 위한 아래와 같은 별도의 처리가 필요하다.
      // 가상 PTZ 설정을 위한 변수를 Org -> Temp로 복사함.
      TmpCamera->VC->m_pParam            = VC->m_pParam;
      TmpCamera->VC->m_nVCID             = VC->m_nVCID;
      TmpCamera->VC->m_nVCIdx            = VC->m_nVCIdx;
      TmpCamera->VC->m_nChannelNo_IVX    = VC->m_nChannelNo_IVX;
      TmpCamera->VC->m_nVideoChannelType = VC->m_nVideoChannelType;
   }

   return ( DONE );
}

int CCamera::TempToOrigin( CCamera* t_cam, int item )
{
   FileIO buffer;
   CXMLIO xmlWriteIO( &buffer, XMLIO_Write );
   if( t_cam->WriteSetup( &xmlWriteIO, item ) ) return ( 1 );

   buffer.GoToStartPos();
   CXMLIO xmlReadIO( &buffer, XMLIO_Read );

   int ret = DONE;
   try {
      if( ReadSetup( &xmlReadIO, item ) ) return ( 1 );
   } catch( IVXException error ) {
      ret = error;
   }

   return ret;
}

int CCamera::OriginToTemp( CCamera* t_cam, int item )
{
   FileIO buffer;
   CXMLIO xmlWriteIO( &buffer, XMLIO_Write );
   if( WriteSetup( &xmlWriteIO, item ) ) return ( 1 );

   buffer.GoToStartPos();
   CXMLIO xmlReadIO( &buffer, XMLIO_Read );

   int ret = DONE;
   try {
      if( ReadSetup( &xmlReadIO, item ) ) return ( 1 );
   } catch( IVXException error ) {
      ret = error;
   }

   return ( DONE );
}

int CCamera::ApplySetupLocal( BOOL bDeactivate )
{
   if( NULL == TmpCamera ) return 0;
   BOOL deactivate = bDeactivate;

   TmpCamera->CorrectReductionFactor( SrcImgSize.Width, SrcImgSize.Height );

   int cs_items = 0;
   cs_items |= GetChangedSetupItem( TmpCamera );
   if( cs_items && !deactivate )
      deactivate = CheckDeactivation( TmpCamera, cs_items );

   if( deactivate ) VC->m_nState |= VCState_IPCamStreamVideoSizeToBeChecked;
   if( deactivate ) Deactivate();
   if( cs_items ) TempToOrigin( TmpCamera, cs_items );
   if( cs_items && !deactivate ) LoadMainVACSetup( cs_items ); // 카메라 재 시작을 할 필요가 없는 경우에는 이곳에서 설정을 로드한다.
   if( deactivate ) Activate();
   return ( DONE );
}

int CCamera::ReadFile( CXMLIO* pIO, int nRWFlag ) throw( IVXException )
{
   static CCamera dv( NULL, ChannelNo );

   CCriticalSectionSP co( m_csCameraSetup );

   // ReadFile 함수는 카메라 전체 설정을 읽는다.
   // ReadSetup 함수와 혼동할 수 있는데 ReadSetup 함수는 일반 설정 및 비디오 분석 설정과 관련된 설정만 읽는다.
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Camera" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "ChNo", &ChannelNo );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &UID );
      xmlNode.Attribute( TYPE_ID( string ), "Guid", &CameraGuid );

      //if( UID < 0 ) throw Xml_Read_Config_Camera_Uid_Empty;
      if( CameraGuid.empty() ) throw Xml_Read_Config_Camera_Guid_Empty;

      if( pIO->GetStorageType() == FIO_STORAGE_FILE ) {
         xmlNode.Attribute( TYPE_ID( CTime ), "PVAST", &m_PausingVideoAnalyticsStartTime, &dv.m_PausingVideoAnalyticsStartTime );
         xmlNode.Attribute( TYPE_ID( int ), "PVAARWT", &m_nPausingVideoAnalyticsAutoReleaseWaitingTime, &dv.m_nPausingVideoAnalyticsAutoReleaseWaitingTime );
         xmlNode.Attribute( TYPE_ID( CTime ), "PVASST", &m_PausingVideoAnalyticsScheduleStartTime, &dv.m_PausingVideoAnalyticsScheduleStartTime );
         xmlNode.Attribute( TYPE_ID( int ), "PVASARWT", &m_nPausingVideoAnalyticsScheduleAutoReleaseWaitingTime, &dv.m_nPausingVideoAnalyticsScheduleAutoReleaseWaitingTime );
      }

      xmlNode.Attribute( TYPE_ID( string ), "Name", &CameraName );

      xmlNode.Attribute( TYPE_ID( int ), "Opt", &Options, &dv.Options );
      xmlNode.Attribute( TYPE_ID( int ), "SPTZTrkMode", &SPTZTrkMode, &dv.SPTZTrkMode );
      xmlNode.Attribute( TYPE_ID( uint ), "PTZTrkModeAutoOnWaitTime", &m_nPTZTrkModeAutoOnWaitTime, &dv.m_nPTZTrkModeAutoOnWaitTime );
      xmlNode.Attribute( TYPE_ID( ISize2D ), "SrcImgSize", &SrcImgSize, &dv.SrcImgSize );
      
      m_csActivePreset.Lock();
      m_pActivePreset = NULL;
      CS_EventDetector.Lock();
      m_PresetGroupList.ReadFile( pIO );
      CS_EventDetector.Unlock();
      m_csActivePreset.Unlock();

      ReadSetup( pIO, SMG_SETUP_ALL, nRWFlag ); // read CameraSetup node.

      ProcImgSize = GetResizedImageSize(); // SrcImgSize 와 ReductionFactor로 부터 영상 크기를 미리 구함.
#ifdef __SUPPORT_PTZ_CAMERA
      m_pSPTZTracker->ReadFile( pIO );
#endif
      return ( DONE );
   } else
      throw Xml_Read_Config_Camera_Node_Not_Exist;

   return ( 1 );
}

int CCamera::WriteFile( CXMLIO* pIO, int nRWFlag )
{
   static CCamera dv( NULL, ChannelNo );

   CCriticalSectionSP co( m_csCameraSetup );

   CVideoChannelInfo* pVCInfo = System->VCM.GetVideoChannel( VCID );

   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Camera" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "ChNo", &ChannelNo );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &UID );
      xmlNode.Attribute( TYPE_ID( string ), "Guid", &CameraGuid );
      if( pIO->GetStorageType() == FIO_STORAGE_FILE ) {
         xmlNode.Attribute( TYPE_ID( CTime ), "PVAST", &m_PausingVideoAnalyticsStartTime, &dv.m_PausingVideoAnalyticsStartTime );
         xmlNode.Attribute( TYPE_ID( int ), "PVAARWT", &m_nPausingVideoAnalyticsAutoReleaseWaitingTime, &dv.m_nPausingVideoAnalyticsAutoReleaseWaitingTime );
         xmlNode.Attribute( TYPE_ID( CTime ), "PVASST", &m_PausingVideoAnalyticsScheduleStartTime, &dv.m_PausingVideoAnalyticsScheduleStartTime );
         xmlNode.Attribute( TYPE_ID( int ), "PVASARWT", &m_nPausingVideoAnalyticsScheduleAutoReleaseWaitingTime, &dv.m_nPausingVideoAnalyticsScheduleAutoReleaseWaitingTime );
      }

      xmlNode.Attribute( TYPE_ID( string ), "Name", &CameraName );

      xmlNode.Attribute( TYPE_ID( int ), "Opt", &Options, &dv.Options );
      xmlNode.Attribute( TYPE_ID( int ), "SPTZTrkMode", &SPTZTrkMode, &dv.SPTZTrkMode );
      xmlNode.Attribute( TYPE_ID( uint ), "PTZTrkModeAutoOnWaitTime", &m_nPTZTrkModeAutoOnWaitTime, &dv.m_nPTZTrkModeAutoOnWaitTime );
      xmlNode.Attribute( TYPE_ID( ISize2D ), "SrcImgSize", &SrcImgSize, &dv.SrcImgSize );

      m_csActivePreset.Lock();
      CS_EventDetector.Lock();
      m_PresetGroupList.WriteFile( pIO );
      CS_EventDetector.Unlock();
      m_csActivePreset.Unlock();

      WriteSetup( pIO, SMG_SETUP_ALL, nRWFlag );

      m_pSPTZTracker->WriteFile( pIO );

      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CCamera::ReadSetup( CXMLIO* pIO, int item, int nRWFlag ) throw( IVXException )
{
   static CCamera dv( NULL, ChannelNo );
   CCriticalSectionSP co( m_csCameraSetup );

   // 일반설정창 또는 비디오분석 설정창과 관련된 설정값들을 읽는다.
   // (카메라 전체 설정은 ReadFile로 읽음)
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "CameraSetup" ) ) {
      if( item & int(IVXSetupItemMask_CameraDisplayOptions) ) {
      }

      if( item & int(IVXSetupItemMask_VADetection) ) {
         CXMLNode xmlNode( pIO );
         if( xmlNode.Start( "OBJECT_DTC" ) ) {
            xmlNode.Attribute( TYPE_ID( int ), "DTCOpt", &DTCOptions, &dv.DTCOptions );
            xmlNode.Attribute( TYPE_ID( float ), "ReductionFactor", &ReductionFactor, &dv.ReductionFactor );
            xmlNode.Attribute( TYPE_ID( float ), "DesiredFPS", &DesiredFrameRate, &dv.DesiredFrameRate );
            xmlNode.Attribute( TYPE_ID( BOOL ), "ReductionFactorChecked", &m_bReductionFactorChecked, &dv.m_bReductionFactorChecked );
            xmlNode.Attribute( TYPE_ID( int ), "Detect_Type", &VAMode, &dv.VAMode );
            xmlNode.Attribute( TYPE_ID( BOOL ), "OC_DNN", &ObjectAnalysisMode_DNN_, &dv.ObjectAnalysisMode_DNN_ );
            
            xmlNode.End();
         } else
            throw Xml_Read_Config_ObjectDtc_Node_Not_Exist;
         if( xmlNode.Start( "ResetObjCount" ) ) {
            xmlNode.Attribute( TYPE_ID( BOOL ), "ResetObjCountUsed", &m_bResetObjCountUsed, &dv.m_bResetObjCountUsed );
            xmlNode.Attribute( TYPE_ID( int ), "ResetObjPeriodTime", &m_nResetObjCount_PeriodTime, &dv.m_nResetObjCount_PeriodTime );

            std::string strResetTime;
            xmlNode.Attribute( TYPE_ID( string ), "ResetTimeDay", &strResetTime );
            GetResetTimeToStringToVector( strResetTime, m_vecResetObjCount_DayTime );

            xmlNode.End();
         }
      }
      if( item & int(IVXSetupItemMask_VAEventDetection) ) {
      }

      ReadMainVACSetup( pIO, item );

      if( item & int(IVXSetupItemMask_VideoInput) ) {
         CXMLNode xmlNode( pIO );
         if( xmlNode.Start( "VIDEOINPUT" ) ) {
            Config_PTZC.ReadFile( pIO );

            if( DONE != ReadVCSetup( pIO, nRWFlag ) )
               throw Xml_Read_Config_VideoInput_Parsing_Failed;

            xmlNode.End();
         } else
            throw Xml_Read_Config_VideoInput_Node_Not_Exist;
      }
      if( item & int(IVXSetupItemMask_FloodLightControl) ) {
         VC->m_PTZInfo.m_IRLightCtrlInfo.Read( pIO );
         VC->m_IPCamInfo.m_PTZInfo.m_IRLightCtrlInfo.Read( pIO );
      }

      if( item & int(IVXSetupItemMask_AlarmInput) ) {
         CXMLNode xmlNode( pIO );
         if( xmlNode.Start( "ALARMINPUT" ) ) {
            xmlNode.End();
         }
      }
      if( item & int(IVXSetupItemMask_VideoStreaming) ) {
         Config_VS.Read( pIO );
      }
      // + : url , id, pw 등이 설정되어 있지 않으면 예외처리
      //if (!VC->m_IPCamInfo.m_strUserName.length() || !VC->m_IPCamInfo.m_strPassword.length())
        // throw Camera_Status_Authorization_Failed;
      if (!VC->m_IPCamInfo.m_strURL.length())
         throw Xml_Read_Config_Camera_URL_Empty;

      xmlNode.End();
   } else
      throw Xml_Read_Config_CameraSetup_Node_Not_Exist; // "CameraSetup" Node check

   return ( DONE );
}

int CCamera::WriteSetup( CXMLIO* pIO, int item, int nRWFlag )
{
   static CCamera dv( NULL, ChannelNo );
   CCriticalSectionSP co( m_csCameraSetup );

   // 일반설정창 또는 비디오분석 설정창과 관련된 설정값들을 Write한다.
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "CameraSetup" ) ) {

      if( item & int(IVXSetupItemMask_VADetection) ) {
         CXMLNode xmlNode( pIO );
         if( xmlNode.Start( "OBJECT_DTC" ) ) {
            xmlNode.Attribute( TYPE_ID( int ), "DTCOpt", &DTCOptions, &dv.DTCOptions );
            xmlNode.Attribute( TYPE_ID( float ), "ReductionFactor", &ReductionFactor, &dv.ReductionFactor );
            xmlNode.Attribute( TYPE_ID( float ), "DesiredFPS", &DesiredFrameRate, &dv.DesiredFrameRate );
            xmlNode.Attribute( TYPE_ID( BOOL ), "ReductionFactorChecked", &m_bReductionFactorChecked, &dv.m_bReductionFactorChecked );
            xmlNode.Attribute( TYPE_ID( int ), "Detect_Type", &VAMode, &dv.VAMode );
            xmlNode.Attribute( TYPE_ID( BOOL ), "OC_DNN", &ObjectAnalysisMode_DNN_, &dv.ObjectAnalysisMode_DNN_ );
            xmlNode.End();
         }
         if( xmlNode.Start( "ResetObjCount" ) ) {
            xmlNode.Attribute( TYPE_ID( BOOL ), "ResetObjCountUsed", &m_bResetObjCountUsed, &dv.m_bResetObjCountUsed );
            xmlNode.Attribute( TYPE_ID( int ), "ResetObjPeriodTime", &m_nResetObjCount_PeriodTime, &dv.m_nResetObjCount_PeriodTime );
            std::string strResetTime = GetResetTimeVectorToString( m_vecResetObjCount_DayTime );
            xmlNode.Attribute( TYPE_ID( string ), "ResetTimeDay", &strResetTime );
            xmlNode.End();
         }
      }

      WriteMainVACSetup( pIO, item );
      if( item & int(IVXSetupItemMask_VideoInput) ) {
         CXMLNode xmlNode( pIO );
         if( xmlNode.Start( "VIDEOINPUT" ) ) {
            Config_PTZC.WriteFile( pIO );
            WriteVCSetup( pIO, nRWFlag );
            xmlNode.End();
         }
      } else if( item & int(IVXSetupItemMask_FloodLightControl) ) {
         VC->m_PTZInfo.m_IRLightCtrlInfo.Write( pIO );
         VC->m_IPCamInfo.m_PTZInfo.m_IRLightCtrlInfo.Write( pIO );
      }

      if( item & int(IVXSetupItemMask_AlarmInput) ) {
         CXMLNode xmlNode( pIO );
         if( xmlNode.Start( "ALARMINPUT" ) ) {
            xmlNode.End();
         }
      }
      if( item & int(IVXSetupItemMask_VideoStreaming) ) {
         Config_VS.Write( pIO );
      }
      xmlNode.End();
      return ( DONE );
   }

   return ( 1 );
}

int CCamera::ReadVACSetup( CXMLIO* pIO, int item )
// 본 함수가 호출된 후에는 반드시 비디오 분석을 재초기화 해야한다.
{
   CCriticalSectionSP co( m_csCameraSetup );
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "VAC" ) ) {
      if( item & int(IVXSetupItemMask_VAEventDetection) ) {
         // 중요: EventDetector::ReadFile 함수를 호출하게 되면 반드시 VAC 모듈을 재시작 해야한다.
         //       가상경로 이벤트가 발생하고 있는 도중에 가상경로 통과 이벤트 설정을 다시로드하게되면
         //       소멸된 가상경로 관련 객체를 참조하게 되는 오류가 발생한다.
         CS_EventDetector.Lock();
         EventDetector.ReadFile( pIO );
         SmokeDetector.ReadFile( pIO );
         
         EventRuleEx* evt_rule = dynamic_cast<EventRuleEx*>( EventDetector.EventRules.First() );
         EventZoneEx* evt_zone = dynamic_cast<EventZoneEx*>( EventDetector.EventZones.First() );
         //hardcoding cause VAManager error :: event type converting 1 -> 0 
         // 회손이벤트 정보 저장
         while( evt_rule && evt_zone ) {
            if( evt_rule->EventType == 1 ) evt_rule->EventType = ER_EVENT_LOITERING; // 배회&침임 모두 배회 이벤트로 처리한다. 
            if( evt_rule->EventType == ER_EVENT_CAMERA_DAMAGE ) {
               nChecked_Camera_Damage_ = TRUE;
               Camera_Damage_Zone_Name_ = evt_zone->Name;
               Camera_Damage_Zone_ID_   = evt_zone->EvtRuleID;
               break;
            }
            evt_rule = dynamic_cast<EventRuleEx*>( EventDetector.EventRules.Next( evt_rule ) );
            evt_zone = dynamic_cast<EventZoneEx*>( EventDetector.EventZones.Next( evt_zone ) );
         }
         CS_EventDetector.Unlock();
      }

      if( item & int(IVXSetupItemMask_VADetection) ) {
         ForegroundDetector_General.ReadFile( pIO );
         if( ObjectClassifier ) ObjectClassifier->ReadFile( pIO );
         ObjectFilter.ReadFile( pIO );
         Config_HeadDetection.ReadFile( pIO );
      }

      if( item & int(IVXSetupItemMask_VAEventDetection) )
         ObjectTracker.ReadFile( pIO );
      if( item & int(IVXSetupItemMask_VACameraGeometry) )
         RealObjectSizeEstimator.ReadFile( pIO );

      xmlNode.End();
   }
   return ( DONE );
}

int CCamera::WriteVACSetup( CXMLIO* pIO, int item )
{
   CCriticalSectionSP co( m_csCameraSetup );
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "VAC" ) ) {
      if( item & int(IVXSetupItemMask_VAEventDetection) ) {
         CS_EventDetector.Lock();
         EventDetector.WriteFile( pIO );
         SmokeDetector.WriteFile( pIO );
         CS_EventDetector.Unlock();
      }
      if( item & int(IVXSetupItemMask_VADetection) ) {
         ForegroundDetector_General.WriteFile( pIO );
         if( ObjectClassifier ) ObjectClassifier->WriteFile( pIO );
         ObjectFilter.WriteFile( pIO );
         Config_HeadDetection.WriteFile( pIO );
      }

      if( item & int(IVXSetupItemMask_VAEventDetection) )
         ObjectTracker.WriteFile( pIO );
      if( item & int(IVXSetupItemMask_VACameraGeometry) )
         RealObjectSizeEstimator.WriteFile( pIO );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CCamera::ReadMainVACSetup( CXMLIO* pIO, int item ) throw( IVXException )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "VAC" ) ) {
      if( item & IVXSetupItemMask_VAEventDetection ) {
         MainConfig_EventDetection.ReadFile( pIO );
         MainConfig_SmokeDetection.ReadFile( pIO );
         //MainConfig_FlameDetection.ReadFile( pIO );
      }
      if( item & IVXSetupItemMask_VADetection ) {
         MainConfig_ForegroundDetection.ReadFile( pIO );
         MainConfig_ObjectClassification.ReadFile( pIO );
         MainConfig_ObjectFiltering.ReadFile( pIO );
         MainConfig_HeadDetection.ReadFile( pIO );
      }
      if( item & IVXSetupItemMask_VAEventDetection )
         MainConfig_ObjectTracking.ReadFile( pIO );

      if( item & IVXSetupItemMask_VACameraGeometry )
         MainConfig_RealObjectSizeEstimation.ReadFile( pIO );

      xmlNode.End();
   } else
      throw Xml_Read_Config_Vac_Node_Not_Exist;

   return ( DONE );
}

int CCamera::WriteMainVACSetup( CXMLIO* pIO, int item )
{
   CCriticalSectionSP co( m_csCameraSetup );
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "VAC" ) ) {
      if( item & IVXSetupItemMask_VAEventDetection ) {
         MainConfig_EventDetection.WriteFile( pIO );
         MainConfig_SmokeDetection.WriteFile( pIO );
      }
      if( item & IVXSetupItemMask_VADetection ) {
         MainConfig_ForegroundDetection.WriteFile( pIO );
         MainConfig_ObjectClassification.WriteFile( pIO );
         MainConfig_ObjectFiltering.WriteFile( pIO );
         MainConfig_HeadDetection.WriteFile( pIO );
      }
      if( item & IVXSetupItemMask_VAEventDetection )
         MainConfig_ObjectTracking.WriteFile( pIO );

      if( item & IVXSetupItemMask_VACameraGeometry )
         MainConfig_RealObjectSizeEstimation.WriteFile( pIO );

      xmlNode.End();
   }
   return ( DONE );
}

int CCamera::WriteDefVACSetup( CXMLIO* pIO, int item )
{
   EventDetection _EventDetector;

   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "VAC" ) ) {
      if( item & IVXSetupItemMask_VAEventDetection ) {
         _EventDetector.WriteFile( pIO );
         SmokeDetector.WriteFile( pIO );
      }

      if( item & IVXSetupItemMask_VADetection ) {
         MainConfig_ForegroundDetection.WriteFile( pIO );
         MainConfig_ObjectClassification.WriteFile( pIO );
         MainConfig_ObjectFiltering.WriteFile( pIO );
         MainConfig_HeadDetection.WriteFile( pIO );
      }
      if( item & IVXSetupItemMask_VAEventDetection )
         MainConfig_ObjectTracking.WriteFile( pIO );

      if( item & IVXSetupItemMask_VACameraGeometry )
         MainConfig_RealObjectSizeEstimation.WriteFile( pIO );

      xmlNode.End();
   }
   return ( DONE );
}

void CCamera::IVCPSendFrameInfo()
{
   if( ( m_nPrvState & CamState_DoVideoAnalytics ) == false ) {
      return;
   }
   std::deque<TrackedObjectEx*> allTrackedObjectList;
   std::deque<TrackedObjectEx*> WLTrackedObjectList;

   // 수위 표시 객체 복사
   if( WaterLevelDetector.IsEnabled() ) {
      TrackedObjectEx* t_objWaterLevel = NULL;
      WaterZone* wt_zone               = WaterLevelDetector.WaterZones.First();
      while( wt_zone != NULL ) {
         if( wt_zone->WaterLevel[0].X || wt_zone->WaterLevel[1].X ) {
            t_objWaterLevel         = new TrackedObjectEx;
            t_objWaterLevel->Type   = TO_TYPE_UNKNOWN;
            t_objWaterLevel->ID     = 1;
            int ex                  = wt_zone->WaterLevel[1].X;
            int ey                  = wt_zone->WaterLevel[1].Y;
            t_objWaterLevel->X      = wt_zone->WaterLevel[0].X;
            t_objWaterLevel->Y      = wt_zone->WaterLevel[0].Y;
            t_objWaterLevel->Width  = ex - t_objWaterLevel->X;
            t_objWaterLevel->Height = ey - t_objWaterLevel->Y;
            allTrackedObjectList.push_back( t_objWaterLevel );
         }
         wt_zone = WaterLevelDetector.WaterZones.Next( wt_zone );
         WLTrackedObjectList.push_back( t_objWaterLevel );
      }
   }

   if( IsSinglePTZTracking() && !( m_pSPTZTracker->GetState() & SPTZTrkState_TrackObjIntantlyLost ) ) {
      // yhpark. when SPTZTrkState_TrackObjIntantlyLost do this only.
      allTrackedObjectList.push_back( &SinglePTZTrackingObj );
      return;
   }

   std::deque<TrackedObjectList*> TrackObjectListVector;
   TrackObjectListVector.push_back( &ObjectTracker.TrackedObjects );
   TrackObjectListVector.push_back( &ObjectTracker.LostObjects );
   TrackObjectListVector.push_back( &EventDetector.EvtGenObjects );

   for( int i = 0; i < int( TrackObjectListVector.size() ); i++ ) {
      TrackedObjectList* trackedObjects = TrackObjectListVector[i];
      TrackedObjectEx* t_object         = (TrackedObjectEx*)trackedObjects->First();
      while( t_object ) {
         if( t_object->Status & ( TO_STATUS_VERIFIED ) && !( t_object->Status & TO_STATUS_TO_BE_REMOVED ) ) {
            Event* event = t_object->Events.First();
            while( event != NULL ) {
               if( event->Status & ( EV_STATUS_BEGUN | EV_STATUS_ONGOING ) ) {
                  t_object->VAEventFlag |= 1 << event->Type;
               }
               event = t_object->Events.Next( event );
            }

            allTrackedObjectList.push_back( t_object );
         }
         t_object = (TrackedObjectEx*)t_object->Next;
      }
   }

//#ifdef _DEBUG_DRAWING_VA
//   RenderVaBoxes( allTrackedObjectList );
//#endif // _DEBUG_DRAWING_VA

   m_ivcpRtspSender->SendFrameInfo( allTrackedObjectList );
   m_ivcpRtspSender->SendFrmObjInfo( allTrackedObjectList );
   m_ivcpRtspSender->SendEventZoneInfo();
   m_ivcpRtspSender->SendEventZoneCounterInfo();

   // TODO(jongoh):  할당된 객체를 delete 해줌
   auto iter = WLTrackedObjectList.begin();
   while( iter != WLTrackedObjectList.end() ) {
      delete( *iter );
      iter++;
   }
   WLTrackedObjectList.clear();

   // TODO(yhpark): ivcp packet.
   //MetaGeneratorSP generator = GetGenerator( MetaGeneratorType::XML, this );
   //generator->FillFrameInfo( allTrackedObjectList );
   //LOGD << generator->GetString();

#ifdef __LIB_SIMPLEWEBSOCKET
   auto send_stream = make_shared<WsServer::SendStream>();
   *send_stream << generator->GetString();
   Websocket::getInstance().BroadcastBinary( send_stream );
#endif // __LIB_SIMPLEWEBSOCKET
}

void CCamera::ClearLiveEventsAndObjectBuffer( BOOL bClearVideoAnalyticsEventOnly )
{
   if( FALSE == ( m_nStateEx2 & CamStateEx2_InitVAModuleExceptObjectTracker ) )
      m_VideoFrameLive.DeleteAllTrackedObjects();
}

CVideoFrame* CCamera::GetDispVideoFrame( int nVideoType )
{
   CVideoFrame* pVideoFrame;
   if( nVideoType == VideoSrcType_Live ) pVideoFrame = &m_VideoFrameLive;
   return pVideoFrame;
}

void CCamera::ExtractVideoFrameInfo( IVXVideoFrameInfo& videoFrameInfo, int nImgWidth, int nImgHeight, BOOL bSendToClient )
{
   videoFrameInfo.m_nChNo = ChannelNo;
   BCCL::Time tm;
   tm.GetLocalTime();
   BCCL::Convert( tm, videoFrameInfo.m_ftFrmTime );

   videoFrameInfo.m_dfNormalPlayTime = VC->m_dfNormalPlayTime;
   videoFrameInfo.m_nFrmNo           = int(VC->GetCaptureCount());
   videoFrameInfo.m_nWidth           = nImgWidth;
   videoFrameInfo.m_nHeight          = nImgHeight;
   videoFrameInfo.m_nState           = 0;
   if( m_nState & CamState_CreatingBKModel )
      videoFrameInfo.m_nState |= FrameState_BackgroundModeling;
   if( m_nState & CamState_SuddenSceneChange )
      videoFrameInfo.m_nState |= FrameState_SuddenSceneChangeIsDetected;
   if( m_nState & CamState_OpenSerialPortFailed )
      videoFrameInfo.m_nState |= FrameState_OpenSerialPortIsFailed;
   if( m_nPTZState & PTZState_PresetMoving )
      videoFrameInfo.m_nState |= FrameState_GoingToPreset;
   if( m_nPTZState & PTZState_PTZMoving || m_nPTZState & PTZState_PresetMoving || m_nPTZState & PTZState_SPTZTrkOngoing ) // PTZ가 이동중인가?
      videoFrameInfo.m_nState |= FrameState_PTZCameraMoving;
   if( m_nState & CamState_DoVideoAnalytics )
      videoFrameInfo.m_nState |= FrameState_PTZCameraMoving;
   if( m_nPTZStateEx & PTZStateEx_PTZPosInitOngoing )
      videoFrameInfo.m_nState |= FrameState_VideoAnalyticsOngoing;
   if( m_nPTZState & PTZState_GotoHomePosToBeStarted )
      videoFrameInfo.m_nState |= FrameState_PTZGotoHomePos;
   if( m_nPTZState & PTZState_SPTZTrkOngoing )
      videoFrameInfo.m_nState |= FrameState_SinglePTZTracking;

   videoFrameInfo.m_nChState    = m_nState;
   videoFrameInfo.m_nChPTZState = m_nPTZState;
   if( bSendToClient ) videoFrameInfo.m_nChPTZState &= PTZState_OnNet;
   videoFrameInfo.m_nChPTZStateEx         = m_nPTZStateEx;
   videoFrameInfo.m_nPresetMoveRemainTime = m_nPresetMoveRemainTime;
   videoFrameInfo.m_nVideoFrmFlag         = VFRM_ODD_EVEN_FIELD & m_nVideoFrameFlag;
   if( IsPTZCamera() && m_pPTZCtrl )
      videoFrameInfo.m_PTZPos = m_pPTZCtrl->GetAbsPos();
}

//실시간 랜더 비디오의 헤더 세팅
BOOL CCamera::SetVideoFrame_Live( BYTE* pYUY2Buff, int nImgWidth, int nImgHeight )
{
   IVXVideoFrameInfo videoFrameInfo;
   ExtractVideoFrameInfo( videoFrameInfo, nImgWidth, nImgHeight );
   m_VideoFrameLive.CopyVideoFrameInfo( videoFrameInfo );
   return TRUE;
}

//실시간 랜더 비디오의 오브젝트 정보 세팅
BOOL CCamera::SetTrackedObjectsOfLiveVideoFrame()
{
   if( !(SrcImgSize.Width > 0) )    return FALSE;
   if( !(ProcImgSize.Width > 0) )   return FALSE;
   FPoint2D scale;
   scale.X = float (SrcImgSize.Width / ProcImgSize.Width);
   scale.Y = float (SrcImgSize.Height / ProcImgSize.Height);

   m_VideoFrameLive.LockVideoFrameInfo();
   m_VideoFrameLive.DeleteAllTrackedObjects();

   m_VideoFrameLive.UnlockVideoFrameInfo();

   return TRUE;
}

BOOL CCamera::Snapshot( BOOL bToTmp )
{
   BOOL bReturn = FALSE;
   m_csSnapshotImage.Lock();
   if( bToTmp ) {
      if( TmpCamera )
         bReturn = GetVideoImage( VideoSrcType_Live, TmpCamera->SnapshotImage, GetVideoFrameMode_Largest ); // 스냅샷용
   } else
      bReturn = GetVideoImage( VideoSrcType_Live, SnapshotImage, GetVideoFrameMode_Largest ); // 스냅샷용
   m_csSnapshotImage.Unlock();
   return bReturn;
}

int CCamera::InitSrcAndProcImgSize()
{
   int nImgProcStreamIdx = VC->GetStreamIdxOfProcCallbackFunc( VCProcCallbackFuncType_ImgProc );
   ISize2D videoSize     = VC->GetVideoSize( nImgProcStreamIdx );
   CorrectReductionFactor( videoSize.Width, videoSize.Height );
   ProcImgSize = GetResizedImageSize();
   return ( DONE );
}

// width가 4의 배수가 되도록 ReductionFactor를 조정한다. (ipp 라이브러리: 4의 배수)
void CCamera::CorrectReductionFactor( int nWidth, int nHeight )
{
   // 비디오 채널이 영상의 입력을 담당하므로 영상 크기를 복사.
   SrcImgSize.Width  = nWidth;
   SrcImgSize.Height = nHeight;
   // ReductionFactor 계산.
   float fMultiple  = 4.0f;
   float fRemain    = fmod( float( SrcImgSize.Width * ReductionFactor ), fMultiple );
   float fGImgWidth = float (SrcImgSize.Width);
   if( fRemain ) {
      if( fRemain < fMultiple / 2 )
         fGImgWidth = SrcImgSize.Width * ReductionFactor - fRemain;
      else
         fGImgWidth = SrcImgSize.Width * ReductionFactor + ( fMultiple - fRemain );
      ReductionFactor = fGImgWidth / SrcImgSize.Width;
   }
}

ISize2D CCamera::GetResizedImageSize()
{
   ISize2D img_size;

   float fResizedImageWidth = ReductionFactor * SrcImgSize.Width;
   if( fabs( floor( fResizedImageWidth ) - fResizedImageWidth ) < fabs( ceil( fResizedImageWidth ) - fResizedImageWidth ) )
      fResizedImageWidth = floor( fResizedImageWidth );
   else
      fResizedImageWidth = ceil( fResizedImageWidth );
   img_size.Width  = int( fResizedImageWidth );
   img_size.Height = int( ReductionFactor * SrcImgSize.Height );
   return img_size;
}

BOOL CCamera::DoesVideoAnalyticsOnColorImageNeeded()
{
#ifdef __SUPPORT_GRAY_IMAGE_USE
#else
   if( m_nState & CamState_DoVideoAnalytics ) return TRUE; // 기본 적으로 컬러 이미지 영상 분석을 하도록 한다.
   if( SmokeDetector.IsEnabled() ) return TRUE;
   if( FlameDetector.IsEnabled() ) return TRUE;
   if( ColorChangeDetector.IsEnabled() ) return TRUE;
   if( m_nState & CamState_VideoAnalyticsOnPresetMoving ) return TRUE;
   if( VAE_MODE_FACE == VAMode ) return TRUE;
#endif

   return FALSE;
}

BOOL CCamera::DoesColorImageNeeded()
{
   if( DoesVideoAnalyticsOnColorImageNeeded() ) return TRUE;
   if( m_nPTZState & ( PTZState_SPTZTrkBegun | PTZState_SPTZTrkOngoing ) ) return TRUE;

   return FALSE;
}

void CCamera::GetSourceImage( BYTE* pImgBuff, int nWidth, int nHeight )
{
   // 초기화
   m_bGetColorSrcImageOK   = FALSE;
   m_bGetGrayScaleSrcImage = FALSE;
   m_bGetYuy2SrcImageOK    = FALSE;

   if (!pImgBuff) {
      SGImage.Delete();
      YUY2SrcImg.Delete();
      BMUSImage.Delete();
   }

   // 입력 영상이 안들어와요.
   if( !pImgBuff || !IsVideoSignalOk() ) {
      return;
   }

   // 일반 영상 분석용
   if( ( m_nState & CamState_DoVideoAnalytics ) || ( m_nState & CamState_VideoAnalyticsOnPresetMoving ) || ( m_nPTZState & ( PTZState_SPTZTrkBegun | PTZState_SPTZTrkOngoing ) ) ) {

      // 단일 추적용 yuy2 소스 버퍼 확보용
      if( ( m_nState & CamState_VideoAnalyticsOnPresetMoving ) == FALSE ) {
         int nYUY2ImgLen = nWidth * nHeight * 2;
         if( nYUY2ImgLen != YUY2SrcImg.Length )
            YUY2SrcImg.Create( nYUY2ImgLen );

         if( YUY2SrcImg.Length ) {
            CopyMemory( static_cast<byte*>(YUY2SrcImg), pImgBuff, size_t(nYUY2ImgLen) );
            m_bGetYuy2SrcImageOK = TRUE;
         }
         VC->GetImage_Timestamp( Timestamp ); // 타임스탬프 : 재전송을 위해.. 아마도..
         VC->SetDecodingFPS( DesiredFrameRate );
      }

      if( SGImage.Width != ProcImgSize.Width || SGImage.Height != ProcImgSize.Height )
         SGImage.Create( ProcImgSize.Width, ProcImgSize.Height );

      if( nWidth == ProcImgSize.Width && nHeight == ProcImgSize.Height ) {
         // 소스영상과 처리영상의 크기가 같으면 VC가 가지고 있는 영상을 그대로 사용한다.
         BCCL::ConvertYUY2ToGray( YUY2SrcImg, SGImage );
      } else {
         // 소스영상과 처리영상의 크기가 다르면 영상크기를 변환시켜 사용한다.
         GImage tmpGImage( nWidth, nHeight );

         BCCL::ConvertYUY2ToGray( YUY2SrcImg, tmpGImage );
         BCCL::Resize( tmpGImage, SGImage );
      }

      m_bGetGrayScaleSrcImage = TRUE;

      // BMUSImage == 분석용 백그라운드 이미지.
      if( BMUSImage.Width != ProcImgSize.Width || BMUSImage.Height != ProcImgSize.Height )
         BMUSImage.Create( ProcImgSize.Width, ProcImgSize.Height );
   }

   if( DoesColorImageNeeded() ) {
      if( YUY2SrcImg.Length ) {
         if( SCImage.Width != ProcImgSize.Width || SCImage.Height != ProcImgSize.Height )
            SCImage.Create( ProcImgSize.Width, ProcImgSize.Height );

         if( SrcImgSize.Width == ProcImgSize.Width && SrcImgSize.Height == ProcImgSize.Height ) {
            BCCL::ConvertYUY2ToBGR( static_cast<byte*>(YUY2SrcImg), SCImage );
         } else {
            BGRImage tmpBGRImage( SrcImgSize.Width, SrcImgSize.Height );
            BCCL::ConvertYUY2ToBGR( static_cast<byte*>(YUY2SrcImg), tmpBGRImage );
            BCCL::Resize( tmpBGRImage, SCImage );
         }
      }
      m_bGetColorSrcImageOK = TRUE;
   }
}

ForegroundDetection* CCamera::GetForegroundDetector( int VAMode )
{
   ForegroundDetection* frg_detector = NULL;
   switch( VAMode ) {
   case VAE_MODE_GENERAL:
      frg_detector = &ForegroundDetector_General;
      break;
   case VAE_MODE_FACE:
#if defined( __LIB_SEETA )
      frg_detector = &ForegroundDetector_Face;
#endif
      break;
   case VAE_MODE_OBJECT:
#if defined( __LIB_DARKNET )
      frg_detector = &ForegroundDetector_Object;
#endif
      break;
   case VAE_MODE_OVERHEAD:
      frg_detector = &ForegroundDetector_Overhead;
      break;
   case VAE_MODE_MOVCAM:
      frg_detector = &ForegroundDetector_MovCam;
      break;
   default:
      break;
   }
   return ( frg_detector );
}

int CCamera::InitVAModules()
{
   if( VC->IsVideoAnalyticsVideoServer() )
      return ( 1 );
   int VAMode = CCamera::VAMode;
   if( ( m_nState & CamState_VideoAnalyticsOnPresetMoving ) && m_nPTZState & ( PTZState_PresetMoveStarted | PTZState_PresetMoving ) )
      VAMode = VAE_MODE_MOVCAM;

   // 버전 예외처리 추가
   if( VAE_MODE_OVERHEAD == VAMode && IS_NOT_SUPPORT( __SUPPORT_HEAD_COUNTING ) ) VAMode = VAE_MODE_GENERAL;
   if( VAE_MODE_FACE == VAMode && IS_NOT_SUPPORT( __SUPPORT_FACE_RECOGNITION ) ) VAMode = VAE_MODE_GENERAL;
   // ipx-110b : DNN 버전이 아닌데 VAMode가 VAE_MODE_OBJECT 이면  위와 같이 예외 처리
   if( !m_pDoc->IsSupportGpu() && VAE_MODE_OBJECT == VAMode )
      VAMode = VAE_MODE_GENERAL;

   // ipx-110b : 차량 보행자 속성을 지원하지 않아도 아래와 같이 설정을 제외해야 함.
   if( !m_pDoc->IsSupportGpu() ) {
      Config_FaceDetection.m_nObjectAnalysisMode &= ~( ObjectAnalysisMode_PersonAttribute | ObjectAnalysisMode_VehicleAttribute );
   }

   float frm_rate  = DesiredFrameRate;
   ISize2D si_size = ProcImgSize;
   if( VAE_MODE_MOVCAM == VAMode ) {
      CropBox.X      = si_size.Width * ( ( 1.0f - MovingCamCropRatio ) * 0.5f );
      CropBox.Y      = si_size.Height * ( ( 1.0f - MovingCamCropRatio ) * 0.5f );
      CropBox.Width  = int( si_size.Width * MovingCamCropRatio ) / 4 * 4;
      CropBox.Height = int( si_size.Height * MovingCamCropRatio ) / 4 * 4;
      si_size( CropBox.Width, CropBox.Height );
      CropImage.Create( si_size.Width, si_size.Height );
   } else {
      CropImage.Delete();
   }

   static ISize2D ri_size( REF_IMAGE_WIDTH, REF_IMAGE_HEIGHT );
   if(SrcImgSize.Width){
      ri_size( SrcImgSize.Width, SrcImgSize.Height );
   }
   
   // jun_u+ : for DVT
   ForegroundDetector_General.InitBkgLearnTime = 5.0f;
   ForegroundDetector_General.BkgUpdatePeriod  = 0.2f;
   ForegroundDetector_General.EDThreshold = 65;  // jun_u+ : sensitivity up for fast objects
   int sscd = 3;
   ForegroundDetector_General.SSCDThreshold = -2.0f / 25.0f * sscd + 1.0f;
   if (ObjectFilter.MinWidth  < ProcImgSize.Height * 0.075f) ObjectFilter.MinWidth  = ProcImgSize.Height * 0.075f;
   if (ObjectFilter.MinHeight < ProcImgSize.Height * 0.075f) ObjectFilter.MinHeight = ProcImgSize.Height * 0.075f;
   

   frg_detector = GetForegroundDetector( VAMode );

   if( VAE_MODE_OBJECT == VAMode || VAE_MODE_FACE == VAMode || VAE_MODE_OVERHEAD == VAMode )
   {
      ObjectAnalysisMode_DNN_ = FALSE;
      //Config_FaceDetection.m_nObjectAnalysisMode &= ~ObjectAnalysisMode_DNNObjectClassification;
   }
   //if( Config_FaceDetection.m_nObjectAnalysisMode & ObjectAnalysisMode_DNNObjectClassification )
   //   ObjectClassifier = &ObjectClassifier_DNN;

   if (ObjectAnalysisMode_DNN_ == TRUE)
      ObjectClassifier = &ObjectClassifier_DNN;
   else
      ObjectClassifier = &ObjectClassifier_Basic;

   if( ProcImgSize.Width > 0 && ProcImgSize.Height > 0 ) {

      // 일단 기본 VA 모듈들을 모두 Enabled 상태로 만든다.
      frg_detector->Enable( TRUE );
      ObjectTracker.Enable( TRUE );
      RealObjectSizeEstimator.Enable( TRUE );
      //if( Config_FaceDetection.m_nObjectAnalysisMode & ObjectAnalysisMode_DNNObjectClassification )
      
      if(ObjectAnalysisMode_DNN_ == TRUE)
         dynamic_cast<ObjectClassification_DNN*>(ObjectClassifier)->Enable( TRUE );
      else
         dynamic_cast<ObjectClassification_Basic*>(ObjectClassifier)->Enable( TRUE );

      // 설정된 이벤트 존의 종류에 따라 특정 모듈을 enable/disable 시킨다.
      if( EventDetector.IsEventZoneAvailable( ER_EVENT_ZONE_COLOR, FALSE, TRUE ) || EventDetector.IsEventZoneAvailable( ER_EVENT_WATER_LEVEL, FALSE, TRUE ) ) {
         // 오직 "영역 색상" 또는 "수위" 이벤트 존만 지정되어 있는 경우,
         // 불필요한 VA 모듈들을 모두 disable 상태로 만든다.
         frg_detector->Enable( FALSE );
         ObjectTracker.Enable( FALSE );
         RealObjectSizeEstimator.Enable( FALSE );
         //if( Config_FaceDetection.m_nObjectAnalysisMode & ObjectAnalysisMode_DNNObjectClassification )
         
         if(ObjectAnalysisMode_DNN_ == TRUE)
            dynamic_cast<ObjectClassification_DNN*>(ObjectClassifier)->Enable( FALSE );
         else
            dynamic_cast<ObjectClassification_Basic*>(ObjectClassifier)->Enable( FALSE );
      } else if( EventDetector.IsEventZoneAvailable( ER_EVENT_CROWD_DENSITY, FALSE, TRUE ) ) {
         ObjectTracker.Enable( FALSE );
         //if( Config_FaceDetection.m_nObjectAnalysisMode & ObjectAnalysisMode_DNNObjectClassification )
         if(ObjectAnalysisMode_DNN_ == TRUE)
            dynamic_cast<ObjectClassification_DNN*>(ObjectClassifier)->Enable( FALSE );
         else
            dynamic_cast<ObjectClassification_Basic*>(ObjectClassifier)->Enable( FALSE );
      }

      // 기본 파라미터 설정을 수행한다.
      frg_detector->CopyParams1( ForegroundDetector_General );

      // 비디오 분석에 필요한 버퍼들을 생성한다.
      if( si_size.Width != BMUSImage.Width || si_size.Height != BMUSImage.Height )
         BMUSImage.Create( si_size.Width, si_size.Height );
      // 기본 VA 분석 모듈들을 초기화한다.
      if( FALSE == ( m_nStateEx2 & CamStateEx2_InitVAModuleExceptObjectTracker ) )
         BMUSImage.Clear();

      ObjectIDGenerator.Reset();
      frg_detector->Initialize( si_size, frm_rate );
      if( VAE_MODE_OVERHEAD == VAMode ) {
         CCriticalSectionSP co( CS_ForegroundDetector );
         ForegroundDetector_Overhead.SetHeadRadius( float( Config_HeadDetection.HRRadius ), ri_size );
      }

      BOOL bInitObjectTracker = FALSE;
      if( ObjectTracker.SrcImgSize.Width != si_size.Width || ObjectTracker.SrcImgSize.Height != si_size.Height )
         bInitObjectTracker = TRUE;
      else if( m_nStateEx2 & CamStateEx2_InitVAModuleExceptObjectTracker )
         bInitObjectTracker = FALSE;

      if( bInitObjectTracker ) {
         CS_ObjectTracker.Lock();
         ObjectTracker.Close();
         ObjectTracker.Initialize( si_size, frm_rate, ObjectIDGenerator );
         CS_ObjectTracker.Unlock();
         m_nStateEx2 |= CamStateEx2_ObjectTrackerJustInitialized;

         if( VAE_MODE_OBJECT == VAMode ) {
            ObjectTracker.MaxTime_Undetected = 0.5f;
            if( m_dwMaskingFlags & CCamera::MaskingFlag_On )
               ObjectTracker.MaxTime_Undetected = 4.0f;
            ObjectTracker.MinCount_Tracked = int( frm_rate * ObjectTracker.MinTime_Tracked );
         }
      }
      RealObjectSizeEstimator.Initialize( ri_size );

      //g_pDoc->m_cs_gpu_load_balance.Lock();
      //unsigned int low_usage_gpu_num = g_pDoc->m_gpu_load_balance.getLowUsageGpuNum();
      //g_pDoc->m_cs_gpu_load_balance.Unlock();

      //if( Config_FaceDetection.m_nObjectAnalysisMode & ObjectAnalysisMode_DNNObjectClassification )
      
      if(ObjectAnalysisMode_DNN_ == TRUE)
      {
         int nerror = dynamic_cast<ObjectClassification_DNN*>(ObjectClassifier)->Initialize(m_pDoc->GetCaffeHandle(), m_pDoc->m_strObjectDetectorDataDirName.c_str(), 0, -1);
        
         LOGI << "ObjectClassifier : " << nerror;
         //( (ObjectClassificationClient*)ObjectClassifier )->Initialize();
         //if( m_pDoc->ObjectClassificationServers.size() > 0 )
          //( (ObjectClassificationClient*)ObjectClassifier )->Connect( *m_pDoc->ObjectClassificationServers[0] );
      } else {
         dynamic_cast<ObjectClassification_Basic*>(ObjectClassifier)->Initialize();
      }

      CS_ObjectTracker.Lock();
      CS_EventDetector.Lock();

      EventDetector.Initialize( si_size, frm_rate, ObjectIDGenerator );

      switch( VAMode ) {
      case VAE_MODE_FACE: {
         // ObjectTracker의 일부 파라미터 값들을 재설정한다.
         ObjectTracker.MinCount_Tracked   = 2;
         ObjectTracker.MaxTime_Undetected = 3.0f;
         ObjectTracker.MinMovingDistance  = 0.0f;
// FaceDetector를 초기화한다.
#if defined( __LIB_SEETA )
         ForegroundDetection_FCD_Seeta* frg_detector2 = (ForegroundDetection_FCD_Seeta*)frg_detector;
         int e_code                                   = ( (ForegroundDetection_FCD_Seeta*)frg_detector )->InitFaceDetector( g_pDoc->m_strObjectDetectorDataDirName );
         if( e_code ) {
            logd( "[ERROR] Cannot load the data files of the module [ForegrounDetection_FCD_Seeta]. (Error Code: %d)\n", e_code );
            return ( 4 );
         }
         e_code = FaceLandmarkDetector.Initialize( g_pDoc->m_strObjectDetectorDataDirName );
         if( e_code ) {
            logd( "[ERROR] Cannot load the data files of the module [FaceLandmarkDetection_Seeta]. (Error Code: %d)\n", e_code );
            return ( 3 );
         }
#endif
      } break;
      case VAE_MODE_OBJECT: {
         // ObjectTracker의 일부 파라미터 값들을 재설정한다.
         //ObjectTracker.MinCount_Tracked   = 2;
         ObjectTracker.MaxTime_Undetected = 0.5f;
         //ObjectTracker.MinMovingDistance  = 0.0f / 240.0f;
         // ipx-110b : DNN 버전 인 경우에만 아래 코드 수행
         // ForegroundDetectionClient를 ObjectDetectionServer에 연결시킨다.
         if( IS_SUPPORT( __SUPPORT_DNN_VERSION ) ) {
#if defined( __LIB_DARKNET )
            if( g_pDoc->UseDNNbyGPU ) {
               if( g_pDoc->ObjectDetectionServers.size() > 0 )
                  ( (ForegroundDetectionClient_OBD_YOLO*)frg_detector )->Connect( *g_pDoc->ObjectDetectionServers[low_usage_gpu_num] );
            }
#endif
         }
      } break;
      case VAE_MODE_OVERHEAD: {
         // VAE모드가 변경되었다가 설정되는 경우를 대비하여 기본설정으로 되돌려야함.
         ObjectTracker.MinCount_Tracked   = 3;
         ObjectTracker.MaxTime_Undetected = 0.0f;

         ObjectClassifier->SetEnforcedObjectType( TO_TYPE_HUMAN );
         ForegroundDetection_HMD_OHV_MBS* frg_detector2 = dynamic_cast<ForegroundDetection_HMD_OHV_MBS*>(frg_detector);
         float bd_size                                  = frg_detector2->GetBodySize();
         ObjectFilter.MinArea                           = 0.5f * bd_size * bd_size;
         ObjectFilter.MinWidth                          = int( 0.5f * bd_size );
         ObjectFilter.MinHeight                         = int( 0.5f * bd_size );
      } break;
      case VAE_MODE_MOVCAM: {
      } break;
      case VAE_MODE_OVERHEAD_3DCAM: {
         ObjectClassifier->SetEnforcedObjectType( TO_TYPE_HUMAN );
      } break;
      default:
         // VAE모드가 변경되었다가 설정되는 경우를 대비하여 기본설정으로 되돌려야함.
         ObjectTracker.MinCount_Tracked   = 3;
         ObjectTracker.MaxTime_Undetected = 0.0f;
         break;
      }

      if( Config_FaceDetection.m_nObjectAnalysisMode & ObjectAnalysisMode_PersonAttribute ) {
         PersonAttrRecognizer.Initialize();
         //if( g_pDoc->PersonAttrRecognitionServers.size() > 0 )
         // PersonAttrRecognizer.Connect( *g_pDoc->PersonAttrRecognitionServers[low_usage_gpu_num] );
      }

      if( Config_FaceDetection.m_nObjectAnalysisMode & ObjectAnalysisMode_VehicleAttribute ) {
         VehicleTypeRecognizer.Initialize();
         //if( g_pDoc->VehicleTypeRecognitionServers.size() > 0 )
         // VehicleTypeRecognizer.Connect( *g_pDoc->VehicleTypeRecognitionServers[low_usage_gpu_num] );
      }

      m_nStateEx2 |= CamStateEx2_FDAndOTInitialized;

      UpdateVAAreaMaps();

      // 일단 확장 VA 모듈들을 모두 Disabled 상태로 만든다.
      ColorChangeDetector.Enable( FALSE );
      CrowdDensityEstimator.Enable( FALSE );
      DwellAnalyzer.Enable( FALSE );
      FlameDetector.Enable( FALSE );
      GroupTracker.Enable( FALSE );
      SmokeDetector.Enable( FALSE );
      TrafficCongestionDetector.Enable( FALSE );
      WaterLevelDetector.Enable( FALSE );
      ZoneColorDetector.Enable( FALSE );
      // 이벤트 존 설정 상태에 따라 필요한 확장 VA 모듈을 Enabled 상태로 만든다.
      if( VAMode == VAE_MODE_GENERAL ) {
         if( EventDetector.IsEventZoneAvailable( ER_EVENT_COLOR_CHANGE ) ) ColorChangeDetector.Enable( TRUE );
         if( EventDetector.IsEventZoneAvailable( ER_EVENT_FLAME ) ) FlameDetector.Enable( TRUE );
         if( EventDetector.IsEventZoneAvailable( ER_EVENT_GATHERING ) || EventDetector.IsEventZoneAvailable( ER_EVENT_VIOLENCE ) ) GroupTracker.Enable( TRUE );
         if( EventDetector.IsEventZoneAvailable( ER_EVENT_SMOKE ) ) SmokeDetector.Enable( TRUE );
         if( EventDetector.IsEventZoneAvailable( ER_EVENT_TRAFFIC_CONGESTION ) ) TrafficCongestionDetector.Enable( TRUE );
         if( EventDetector.IsEventZoneAvailable( ER_EVENT_WATER_LEVEL ) ) WaterLevelDetector.Enable( TRUE );
         if( EventDetector.IsEventZoneAvailable( ER_EVENT_ZONE_COLOR ) ) ZoneColorDetector.Enable( TRUE );
      }
      if( EventDetector.IsEventZoneAvailable( ER_EVENT_CROWD_DENSITY ) ) CrowdDensityEstimator.Enable( TRUE );
      if( EventDetector.IsEventZoneAvailable( ER_EVENT_DWELL ) ) DwellAnalyzer.Enable( TRUE );
      // 확장 VA 모듈들을 초기화한다. (Enabled 상태의 모듈들만 초기화를 수행함)
      std::string path_name;
      ColorChangeDetector.Initialize( si_size, frm_rate );
      CrowdDensityEstimator.Initialize( EventDetector );
      path_name = sutil::sformat( "%s/DwellAnalysis-UID%03d.xml", m_pDoc->m_strDwellAnalysisDataPath.c_str(), UID );
      DwellAnalyzer.Initialize( EventDetector, path_name.c_str() );
      FlameDetector.Initialize( si_size, frm_rate );
      GroupTracker.Initialize( si_size, frm_rate );
      SmokeDetector.Initialize( si_size, frm_rate );
      TrafficCongestionDetector.Initialize( EventDetector );
      WaterLevelDetector.Initialize( EventDetector );
      ZoneColorDetector.Initialize( EventDetector );
      CS_EventDetector.Unlock();
      CS_ObjectTracker.Unlock();

      // 이벤트 존의 설정 상태에 따라 ForegroundDetector_General의 몇몇 파라미터들의 값을 조절한다.
      CS_ForegroundDetector.Lock();
      frg_detector->InitParams( EventDetector );
      CS_ForegroundDetector.Unlock();

#ifdef __UPLUS_LOGGER
      LGU::Logger::SetHeaderItem( COMMAND, "START");
      LGU::Logger::SetHeaderItem( FROM_SERVICE_NAME, "VAP" );
      LGU::Logger::SetHeaderItem( RES_SERVICE_NAME, "VAM" );
      LGU::Logger::SetHeaderItem( VA_PROCESS_CALL_TIME );
      LOGI_(L) << LGU::Logger::header (1);
#endif
      
      m_nStateEx2 |= CamStateEx2_VAModuleInitialized;
   }

   return ( DONE );
}

void CCamera::UninitVAModules()
{ 
#ifdef __UPLUS_LOGGER
   LGU::Logger::SetHeaderItem(COMMAND, "STOP");
   LGU::Logger::SetHeaderItem(FROM_SERVICE_NAME, "VAP" );
   LGU::Logger::SetHeaderItem(RES_SERVICE_NAME, "VAM" );
   LGU::Logger::SetHeaderItem( VA_PROCESS_CALL_TIME );
   LOGI_(L) << LGU::Logger::header (1);
#endif
   
   CS_ForegroundDetector.Lock();
   ForegroundDetector_General.Close();
   CS_ForegroundDetector.Unlock();

   if( FALSE == ( m_nStateEx2 & CamStateEx2_InitVAModuleExceptObjectTracker ) ) {
      CS_ObjectTracker.Lock();
      ObjectTracker.Close();
      CS_ObjectTracker.Unlock();
   }

   ObjectClassifier_DNN.Close();
   ObjectClassifier_Basic.Close();

   CS_EventDetector.Lock();
   EventDetector.Close();

   m_nStateEx2 &= ~CamStateEx2_FDAndOTInitialized;

   ColorChangeDetector.Close();
   FlameDetector.Close();
   GroupTracker.Close();
   SmokeDetector.Close();
   DwellAnalyzer.Close();
   TrafficCongestionDetector.Close();
   WaterLevelDetector.Close();
   ZoneColorDetector.Close();
   CS_EventDetector.Unlock();
   PersonAttrRecognizer.Close();
   VehicleTypeRecognizer.Close();

   m_nStateEx2 &= ~CamStateEx2_VAModuleInitialized;
}

void CCamera::ResetVAModules()
{
   // 모든 분석모듈을 Reset 시키기 전에 추적물체에 대한 종료처리를 시킨다.
   InvalidateAllObjectsAndEvents();

   ProcessOnTrackedObjectsAndEvents();

   BOOL bClearVideoAnalyticsEventOnly = TRUE;
   ClearLiveEventsAndObjectBuffer( bClearVideoAnalyticsEventOnly );

   // 모든 분석모듈을 Reset시킨다.
   //    Reset()이 호출되면 모든 TrackedObject 객체들이 삭제되므로 TrackedObject
   //    객체를 참조하고 있는 것이 있다면 그 전에 해제해야 한다.
   CS_ForegroundDetector.Lock();
   frg_detector->Reset();
   CS_ForegroundDetector.Unlock();

   CS_ObjectTracker.Lock();
   if( FALSE == ( m_nStateEx2 & CamStateEx2_InitVAModuleExceptObjectTracker ) )
      ObjectTracker.Reset();
   ColorChangeDetector.Reset();
   FlameDetector.Reset();
   GroupTracker.Reset();
   SmokeDetector.Reset();

   // [중요] 이벤트 검출기를 1회 수행(Perform 시킨다)
   //       이벤트를 Ended 상태로 후처리(ProcessOnTrackedObjectsAndEvents)를 모두 하였기 때문에 이벤트 객체를 삭제하기 위해서이다.
   //       이 작업을 수행하지 않으면 다음번 후처리 시 다운될 수 있다.
   //       왜냐하면 EventDetection 설정이 다시 로드되면 각 물체의 이벤트 객체가 참조하고 있던 EventRule EventZone객체가 사라지기 때문이다.
   if( EventDetector.SrcImgSize.Width > 0 ) {
      EventDetector.Perform( ObjectTracker );
      EventDetector.Perform( ColorChangeDetector );
      EventDetector.Perform( FlameDetector );
      EventDetector.Perform( GroupTracker );
      EventDetector.Perform( SmokeDetector );
      EventDetector.Perform( TrafficCongestionDetector );
      EventDetector.Perform( WaterLevelDetector );
      EventDetector.Perform( ZoneColorDetector );
      if( DwellAnalyzer.IsEnabled() ) {
         DwellAnalyzer.Perform( ObjectTracker );
         EventDetector.Perform( DwellAnalyzer );
         // WARNING: DwellAnalyzer.Perform(ObjectTracker)은 반드시 EventDetector.Perform(ObjectTracker) 보다 뒤에 호출되어야 한다.
         //          DwellAnalyzer.Perform(ObjectTracker)가 EventDetector.Perform(ObjectTracker)의 실행 결과를 이용하기 때문이다.
      }
   }
   CS_ObjectTracker.Unlock();
}

void CCamera::InvalidateAllObjectsAndEvents()
{
   // Reset 함수들는 TrackedObject객체의 상태값을 TO_BE_REMOVED로 변경한다.
   // Event 객체의 상태를 END상태로 변경하려면 이 부분을 직접 구현해야한다.

   // 추적 물체
   CS_ObjectTracker.Lock();
   ObjectTracker.TrackedObjects.Move( ObjectTracker.LostObjects );
   TrackedObject* t_object = ObjectTracker.TrackedObjects.First();
   while( t_object != NULL ) {
      if( t_object->Status & ( TO_STATUS_VERIFIED ) ) {
         if( FALSE == ( m_nStateEx2 & CamStateEx2_InitVAModuleExceptObjectTracker ) )
            t_object->Status |= TO_STATUS_TO_BE_REMOVED;
         EventEx* event = dynamic_cast<EventEx*>(t_object->Events.First());
         while( event != NULL ) {
            if( event->Status & ( EV_STATUS_BEGUN | EV_STATUS_ONGOING ) )
               event->Status = EV_STATUS_ENDED;
            event = dynamic_cast<EventEx*>(t_object->Events.Next( event ));
         }
      }
      t_object = ObjectTracker.TrackedObjects.Next( t_object );
   }
   // 이벤트 검출기 - 이벤트 발생 물체들
   t_object = EventDetector.EvtGenObjects.First();
   while( t_object != NULL ) {
      if( FALSE == ( m_nStateEx2 & CamStateEx2_InitVAModuleExceptObjectTracker ) )
         t_object->Status |= TO_STATUS_TO_BE_REMOVED;
      EventEx* event = dynamic_cast<EventEx*>(t_object->Events.First());
      while( event != NULL ) {
         if( event->Status & ( EV_STATUS_BEGUN | EV_STATUS_ONGOING ) )
            event->Status = EV_STATUS_ENDED;
         event = dynamic_cast<EventEx*>(t_object->Events.Next( event ));
      }
      t_object = EventDetector.EvtGenObjects.Next( t_object );
   }

   CS_ObjectTracker.Unlock();
}

void CCamera::UpdateVAAreaMaps()
{
   if( VC->IsVideoAnalyticsVideoServer() )
      return;
   if( ProcImgSize.Width <= 0 && ProcImgSize.Height <= 0 )
      return;
   if( FALSE == ( m_nStateEx2 & CamStateEx2_FDAndOTInitialized ) )
      return;

   static ISize2D ri_size( REF_IMAGE_WIDTH, REF_IMAGE_HEIGHT );
   if(SrcImgSize.Width) {
      ri_size( SrcImgSize.Width, SrcImgSize.Height );
   }
   
   if( ( m_nStateEx2 & CamStateEx2_ToBeUpdateVAAreas ) || ( m_nStateEx2 & CamStateEx2_ObjectTrackerJustInitialized ) ) {
      CS_ObjectTracker.Lock();
      ObjectTracker.UpdateAreaMaps( ri_size );
      if( ( VACSetupType_Default == m_nCurVACSetupType ) || ( m_nState & CamState_VideoAnalyticsOnPresetMoving ) ) {
         BOOL bAdditionalAreas = FALSE;
         VC->GetTrackingAreaMap( ObjectTracker.TrkAreaMap, ObjectTracker.DynBkgAreaMap, bAdditionalAreas );
      }
      CS_ObjectTracker.Unlock();
      m_nStateEx2 &= ~CamStateEx2_ToBeUpdateVAAreas;
   }

   CS_ForegroundDetector.Lock();
   frg_detector->SetDetectionAreaMap( ObjectTracker.TrkAreaMap );
   CS_ForegroundDetector.Unlock();

   CS_EventDetector.Lock();
   
   static ISize2D s2d( REF_IMAGE_WIDTH, REF_IMAGE_HEIGHT );
   if(SrcImgSize.Width){
      s2d( SrcImgSize.Width, SrcImgSize.Height );
   }
   
   EventDetector.UpdateAreaMaps( s2d );
   CS_EventDetector.Unlock();
}

#if defined( __WIN32 )
void CCamera::OpenAuxVideoDisplayWnd()
{
   if( ProcImgSize.Width <= 0 || ProcImgSize.Height <= 0 ) return;
   CS_AuxVideoDisplay.lock();
   if( AuxVideoDisplayWnd == NULL ) AuxVideoDisplayWnd = new CAuxVideoDisplayWnd( 0, 0, this );
   AuxVideoDisplayWnd->m_bDoNotUpdateAuxVideoDispWndPos = TRUE;

   if( MonitorFromPoint( CPoint( AuxVideoDispWndPos.X, AuxVideoDispWndPos.Y ), MONITOR_DEFAULTTONULL ) == NULL ) {
      AuxVideoDispWndPos( 0, 0 );
   }
   AuxVideoDisplayWnd->SetWindowPos( &CWnd::wndTop, AuxVideoDispWndPos.X, AuxVideoDispWndPos.Y, 0, 0, SWP_NOSIZE );
   ISize2D auxVideoDispWndSize = ProcImgSize;
   if( IsSinglePTZTrkSupported() && SPTZTrkMode != SPTZTrkMode_None )
      auxVideoDispWndSize.Height = ProcImgSize.Height * 1 + 100;
#ifdef __DEBUG_FGD
   _DebugView_FGD             = AuxVideoDisplayWnd->ImageView;
   auxVideoDispWndSize.Height = ProcImgSize.Height * 15;
#endif
   AuxVideoDisplayWnd->SetVideoSize( auxVideoDispWndSize.Width, auxVideoDispWndSize.Height );
   AuxVideoDisplayWnd->ImageView->SetDrawMode( IV_DM_DIRECT );

   if( m_pSPTZTracker )
      m_pSPTZTracker->SetImageView( AuxVideoDisplayWnd->ImageView, &CS_AuxVideoDisplay );
   AuxVideoDisplayWnd->m_bDoNotUpdateAuxVideoDispWndPos = FALSE;

   CS_AuxVideoDisplay.unlock();
   logd( "[AuxVideoDisplayWnd Open !!!]\n" );
}
#endif

void CCamera::ProcessOnTrackedObjectsAndEvents()
{
   if( m_nState & int(CamState_DoVideoAnalytics) ) {
      if( m_pSPTZTracker ) {
         if( ( m_pSPTZTracker->GetState() & SPTZTrkState_TrackToBeEnded ) == false ) {
            LoadRealtimeEventCountInfoToMainOrPresetVASetup();
         }
      } else {
         LoadRealtimeEventCountInfoToMainOrPresetVASetup();
      }
   }

   if( m_nStateEx2 & CamStateEx2_ObjectCountersTeBeReset ) {
      ResetAllObjectCounters();
      m_nStateEx2 &= ~CamStateEx2_ObjectCountersTeBeReset;
      System->SetSystemSetupToBeSaved();
   }

   SetTrackedObjectsOfLiveVideoFrame();
   IVCPSendFrameInfo();
   ProcessOnTrackedObjects();
   ProcessOnEvents();
}

void CCamera::ProcessOnTrackedObjects()
{
   CCriticalSectionSP lk( CS_ObjectTracker );

   TrackedObjectEx* t_object = dynamic_cast<TrackedObjectEx*>(ObjectTracker.TrackedObjects.First());
   while( t_object != NULL ) {
      if( t_object->Status & TO_STATUS_VERIFIED ) {
         // 추적 물체 박스의 시간을 기록 (시작 시간으로 부터 경과된 시간) (mkjang-150515)
         if( t_object->TrkObjBoxElapsedTimeList.size() > 0 ) {
            int nTrkObjBoxElapsedTime = GetElapsedMilliSec( t_object->TrkObjBoxStartTime, m_ftLastDoImageProcessTime );
            t_object->TrkObjBoxElapsedTimeList.push_back( nTrkObjBoxElapsedTime );
         }

         if( t_object->Status & TO_STATUS_VERIFIED_START ) {
            // 추적 물체 박스의 시작 시간 기록 (mkjang-150515)
            if( t_object->TrkObjBoxElapsedTimeList.size() == 0 ) {
               t_object->TrkObjBoxStartTime = m_ftLastDoImageProcessTime;
               int nTrkObjBoxElapsedTime    = GetElapsedMilliSec( t_object->TrkObjBoxStartTime, m_ftLastDoImageProcessTime );
               t_object->TrkObjBoxElapsedTimeList.push_back( nTrkObjBoxElapsedTime );
            }

            m_ivcpRtspSender->SendObjectInfo( t_object );
         }
         if( t_object->Status & TO_STATUS_TO_BE_REMOVED ) {
            m_ivcpRtspSender->SendObjectInfo( t_object );
         }
      }
      t_object = dynamic_cast<TrackedObjectEx*>(ObjectTracker.TrackedObjects.Next( t_object ));
   }
}

void CCamera::ProcessOnEvents()
{
   m_bExistEvent = FALSE;
   TrackedObjectList* trackedObjects = NULL;
   CMetaGenerator::VAResults results;

   for( int i = 0; i < TOLType_Num; i++ ) {
      if( i == TOLType_ObjecTracking_Tracked ) trackedObjects = &ObjectTracker.TrackedObjects;
      if( i == TOLType_ObjecTracking_Lost ) trackedObjects = &ObjectTracker.LostObjects;
      if( i == TOLType_EventDetection_EvtGen ) trackedObjects = &EventDetector.EvtGenObjects;

      TrackedObjectEx* t_object = dynamic_cast<TrackedObjectEx*>(trackedObjects->First());
      while( t_object != NULL ) {
         EventEx* event = dynamic_cast<EventEx*>(t_object->Events.First());
         while( event != NULL ) {
            m_bExistEvent = TRUE;
            if( event->Type != ER_EVENT_CAMERA_DAMAGE ) {
              if( i != 1 || event->EvtRule->EventType == ER_EVENT_ENTERING ) {
                 if( event->Status & ( EV_STATUS_BEGUN | EV_STATUS_ENDED ) ) {
              
                    //event id counter not completed
                    if(nEvent_ID_count_ != event->ID) event->ID = event->ID + 1;
                    nEvent_ID_count_ = event->ID;
                    // object id counter
                    if (nObject_ID_count_ != t_object->ID) t_object->ID = t_object->ID + 1;
                    nObject_ID_count_ = t_object->ID;
                    
                    CMetaGenerator::VaMeta meta;
                    meta.nTOLType = i;
                    meta.event    = event;
                    meta.object   = t_object;
                    results.push_back( meta );
                    TakeActionForEvent( i, event, t_object );
                    if(event->Status & EV_STATUS_BEGUN){
                       CheckOverlapEvent(event->Type);
                    }     
                 }
              }
            }
            event = dynamic_cast<EventEx*>(t_object->Events.Next( event ));
         }
         t_object = dynamic_cast<TrackedObjectEx*>(trackedObjects->Next( t_object ));
      }
   }

   // 카메라 훼손 이벤트 전달 부분   
   // 갑작스런 장면변화 이벤트
   if(CheckStateChange( m_nPrvState, m_nState, int( CamState_SuddenSceneChange ), TRUE ) ) {
      if(nChecked_Camera_Damage_ == TRUE) {
         nEvent_ID_count_  = nEvent_ID_count_ + 1;
         nObject_ID_count_ = nObject_ID_count_ + 1;
         
         static TrackedObjectEx demageZoneTrackedObject;
         static EventEx         demageZoneEvent;
         static EventZoneEx     demageZone;
         static EventRuleEx     demageZoneRule;
         demageZoneEvent.EvtZone = &demageZone;
         demageZone.EvtRule = &demageZoneRule;
         
         CMetaGenerator::VaMeta meta_SSC;
         meta_SSC.nTOLType      = TOLType_SuddenSceneChange;
         meta_SSC.object        = &demageZoneTrackedObject;
         meta_SSC.object->ID    = nObject_ID_count_;
         meta_SSC.object->Type  = ER_EVENT_CAMERA_DAMAGE;
         meta_SSC.event         = &demageZoneEvent;
         meta_SSC.event->ID     = nEvent_ID_count_;
         meta_SSC.event->Type   = ER_EVENT_CAMERA_DAMAGE;
         AfxGetLocalFileTime (&meta_SSC.event->Time_EventBegun);
         meta_SSC.event->EvtZone->EvtRuleID = Camera_Damage_Zone_ID_;
         strcpy( meta_SSC.event->EvtZone->Name, Camera_Damage_Zone_Name_.c_str() );
         
         CheckOverlapEvent(ER_EVENT_CROWD_DENSITY + 1);
         
         logw("Suddenly Camera Changed to Scene [TOL TYPE :%d (Obj/Evt Type:%d/%d ID:%d/%d)]",
              meta_SSC.nTOLType, meta_SSC.object->Type, meta_SSC.event->Type, meta_SSC.object->ID, meta_SSC.event->ID);
         results.push_back( meta_SSC );
      }
   }
   if(CheckStateChange( m_nPrvState, m_nState, int( CamState_SuddenSceneChange ), FALSE ) ) {

   }

   if( results.size() > 0) {   
      MetaGeneratorSP generator = GetGenerator( MetaGeneratorType::JSON, this );
      generator->SetLocalSystem(dynamic_cast<CLocalSystem*>(System) );
      generator->FillEventInfo( results );
      CRestSender::getInstance()->SendEventInfo( generator.get() );
      results.clear();
   }

   if( m_nState & CamState_SuddenSceneChange ) {
      m_bExistEvent = TRUE;
   }

   if( m_nPTZState & ( PTZState_SPTZTrkBegun | PTZState_SPTZTrkOngoing | PTZState_SPTZTrkToBeEnded | PTZState_SPTZTrkEnded | PTZState_LockObjEvtOngoing | PTZState_AutoPTZTrkByMasterOngoing | PTZState_AutoPTZTrkByMasterToBeEnded ) ) {
      m_bExistEvent = TRUE;
   }

   if( m_bExistEvent )
      m_nState |= CamState_EventExist;
   else
      m_nState &= ~CamState_EventExist;
}

void CCamera::ProcessOnResetObjectCounter()
{
   if( !m_bResetObjCountUsed || !( m_nState & CamState_DoVideoAnalytics ) )
      return;

   BCCL::Time tm;
   tm.GetLocalTime();

   uint nTotalMin = uint(tm.Hour * 60 + tm.Minute);
   if( nTotalMin == uint(m_nResetObjCount_LastActiveTime) )
      return;

   uint nRemainder = nTotalMin % uint(m_nResetObjCount_PeriodTime);
   if( 10 <= m_nResetObjCount_PeriodTime && 0 == nRemainder ) {
#if 0
      ResetAllObjectCounters();
      System->SetSystemSetupToBeSaved();
#else
      m_nStateEx2 |= CamStateEx2_ObjectCountersTeBeReset;
#endif
   }

   int nCount = int(m_vecResetObjCount_DayTime.size());
   for( int ii = 0; ii < nCount; ii++ ) {
      RESET_OBJECT_TIME& rdTime = m_vecResetObjCount_DayTime[ii];
      if( rdTime.m_nDay == tm.Weekday && rdTime.m_nTime == int(nTotalMin) ) {
         m_nStateEx2 |= CamStateEx2_ObjectCountersTeBeReset;
         break;
      }
   }

   if( m_nStateEx2 & CamStateEx2_ObjectCountersTeBeReset )
      m_nResetObjCount_LastActiveTime = int(nTotalMin);
}

void CCamera::TakeActionForEvent( int nTOLType, EventEx* event, TrackedObjectEx* t_object )
{
   if( !IsPTZCamera() ) {
      BOOL bEventGrouping = FALSE;
      if( bEventGrouping ) {
         switch( nTOLType ) {
         case TOLType_EventDetection_EvtGen:
            break;
         }
      } else {
         switch( nTOLType ) {
         case TOLType_ObjecTracking_Tracked:
         case TOLType_ObjecTracking_Lost:
         case TOLType_EventDetection_EvtGen:
            break;
         }
      }

      if( event->Status & ( EV_STATUS_BEGUN | EV_STATUS_ENDED ) ) {
         m_ivcpRtspSender->SendEventInfo( t_object, event );
      }
   }
}

int CCamera::Activate()
{
   m_bReductionFactorChecked = FALSE;

   logd( "[CH:%d] Camera Activate - Start\n", ChannelNo + 1 );
   m_nDisplayLog = 0;
   // 로컬시스템에서만 호출되는 함수.
   if( m_nState & CamState_Activate )
      return ( DONE );
   //int prev_status = m_nState;
   m_nState        = 0;
   m_nPrvState     = 0;
   m_nPTZState &= ~( 0xFFFFFFFF & ~( 0 ) );
   m_nPrvPTZState &= ~( 0xFFFFFFFF & ~( 0 ) );
   m_nPTZStateEx = 0;

   if( IS_NOT_PRODUCT( __SUPPORT_VIRTUAL_PTZ_CAMERA ) ) {
      if( IsVirtualPTZCamera() )
         VC->m_nVideoChannelType = VideoChannelType_Unused;
   }
   if( IS_NOT_PRODUCT( __SUPPORT_PANORAMIC_CAMERA ) ) {
      if( IsPanoramaCamera() )
         VC->m_nVideoChannelType = VideoChannelType_Unused;
   }
   if( IS_NOT_PRODUCT( __SUPPORT_WEBCAM ) ) {
      if( VC->m_nVideoChannelType == VideoChannelType_WebCam )
         VC->m_nVideoChannelType = VideoChannelType_Unused;
   }

   int j_IVXStream;
   for( j_IVXStream = 0; j_IVXStream < IVX_VIDEO_STREAM_NUM; j_IVXStream++ ) {
      CVideoStreamProfile* pVideoStreamProfie = Config_VS.GetVideoStreamProfile( j_IVXStream );
      if( FALSE == VC->IsSupportDirectVideoStreaming( pVideoStreamProfie->m_nVideoServerStreamIdx ) ) {
         pVideoStreamProfie->m_bDirectStreaming = FALSE;
      }
   }

   if( DONE != Initialize() ) {
      LOGE << "cam failed to Initialize. Cam ch:" << ChannelNo;
      m_nState |= CamState_InitializationFailure;
   } else {
      StartCameraMgrThread();
      int proc_flags = VCThreadType_Cap | VCThreadType_IP | VCThreadType_Disp;

      DetermineVCDisplayCallbackFuncState();
      VC->Activate( proc_flags );
      VC->SetDecodingFPS( DesiredFrameRate );
      if( VC->IsSupportAudioDataTransmit() ) { // 스피커를 켜야 한다면 세팅한다.
         VC->StartAudioInOut();
      }

      m_nState |= CamState_Activate;
   }

   for( int nStreamNo = 0; nStreamNo < IVX_VIDEO_STREAM_NUM; nStreamNo++ ) {
      CRTSPStreamingChannel* pRTSPStreamingChannel  = GetRtspStreamingChannel( nStreamNo );
      pRTSPStreamingChannel->m_strStreamName        = sutil::sformat( "%s/stream%d/media.imp", CameraGuid.c_str(), nStreamNo + 1 );
      pRTSPStreamingChannel->m_strStreamName_Alias1 = sutil::sformat( "chuid%d/stream%d/media.imp", UID, nStreamNo + 1 );
      pRTSPStreamingChannel->m_strStreamName_Alias2 = sutil::sformat( "ch%d/stream%d/media.imp", ChannelNo + 1, nStreamNo + 1 );
      pRTSPStreamingChannel->m_pParam               = (LPVOID*)this;
   }

   logi( "[CH:%d] Camera Activate - Done\n", ChannelNo + 1 );
#ifdef __UPLUS_LOGGER
   LGU::Logger::SetHeaderItem( FROM_SERVICE_NAME, "VAP" );
   LGU::Logger::SetHeaderItem( RES_SERVICE_NAME, "Camera" );
   LGU::Logger::SetHeaderItem( VA_PROCESS_CALL_TIME );
#endif
   return ( DONE );
}

int CCamera::Deactivate()
{
   logi( "[CH:%d] Camera deactivating - Start\n", ChannelNo + 1 );
   if( !( m_nState & CamState_Activate ) ) return ( DONE );
   m_nState |= CamState_Deactivating;

   int i;
   for( i = 0; i < IVX_VIDEO_STREAM_NUM; i++ ) {
      if( m_bStartVideoStreamThread[i] )
         StopVideoStreamingThread( i );
   }

   VC->Deactivate();

   StopCameraMgrThread();

   // 연결이 끊겼을 때에도 아래의 큐를 비워야 할 듯.
   for( i = 0; i < MAX_IP_CAMERA_STREAM_NUM; i++ )
      m_EncodedVideoStramQueue[i].Clear();

   Close();

   ClearLiveEventsAndObjectBuffer();

   m_nState &= ~CamState_Activate;
   m_nState |= CamState_ToBeClosed;
   m_nState &= ~CamState_Deactivating;
   logi( "[CH:%d] Camera deactivating - Done\n", ChannelNo + 1 );
   m_nDisplayLogCnt = 0;

   return ( DONE );
}

#if defined( __WIN32 )
void CCamera::CloseAuxVideoDisplayWnd()
{
   AuxVideoDisplayWnd->OnClose();
   logd( "[AuxVideoDisplayWnd Close...]\n" );
}
#endif
int CCamera::Initialize()
{
   // Camera::Activate 호출 시 본 함수를 호출한다.
   m_nState &= ~CamState_ChannelUnregistered;
   m_nState &= ~CamState_VideoAnalyticsUnregistered;

#if defined( __DISTRIBUTION )
   CLicenseManager* pLicenseManager = CLicenseManager::GetInstance();

   int nChannelNo = ChannelNo;

   if( 1 ) {
      if( FALSE == RegisterChannel() )
         return ( 1 );
   } else {
      if( pLicenseManager->CheckRegistration( nChannelNo, FCLASS_MONITORING ) ) {
         m_nState |= CamState_ChannelUnregistered;
         return ( 1 );
      }
      if( pLicenseManager->CheckRegistration( nChannelNo, FCLASS_VIDEOANALYTICS ) )
         m_nState |= CamState_VideoAnalyticsUnregistered;
   }
#endif

   VC->SetPTZControlOpenMode( PTZCtrlOpenMode_Normal ); // 중요: CreateOnly 모드가 아닌 경우에도 호출을 반드시 해주어야 함.
   // 합성채널(PTZ+고정식 다수 구성)일 경우에는 임시로!! 무조건 PTZ카메라로 둔다.
   if( VC->m_IPCamInfo.m_nIPCameraType == IPCameraType_IVXVideoServer ) {
      if( VC->m_IPCamInfo.m_nVideoChNo == -1 ) {
         VC->m_IPCamInfo.m_bUsePTZ = TRUE;
         VC->m_bPTZCamera          = TRUE;
      }
   }

   if( DesiredFrameRate < 1.0f ) DesiredFrameRate = 7.0f;
   VC->m_fFPS_IP = DesiredFrameRate;

   VC->m_nChannelNo_IVX = ChannelNo;
   VC->m_szCameraName   = sutil::sformat( _T("%s"), CameraName.c_str() );
   if( VC->Initialize() != DONE ) {
      LOGE << "VC Initialize failed.";
      if( VC->m_nState & VCState_FailureOpenPVTFile )
         m_nState |= CamState_OpenPVTFileFailed;
      if( VC->m_nState & VCState_FailureOpenVCTFile )
         m_nState |= CamState_OpenVCTFileFailed;
      if( VC->m_nState & VCState_FailureOpenVideoFile )
         m_nState |= CamState_OpenVideoFileFailed;
      return ( 2 );
   }

   if( VC->m_nState & VCState_FailureOpenSerialPort )
      m_nState |= CamState_OpenSerialPortFailed;

   if( VC->m_nVideoChannelType == VideoChannelType_VideoFile && VC->m_FileCamInfo.m_bSyncVCAFpsToVideoFps ) {
      FrameRate = VC->m_fFPS_Cap;
   } else {
      FrameRate = DesiredFrameRate;
   }
   //#ifdef __SUPPORT_PTZ_CAMERA
   if( IsPTZCamera() ) { // PTZ카메라인 경우
      m_pPTZCtrl = VC->GetPTZControl();
      if( m_pPTZCtrl != NULL ) {
         m_pPTZCtrl->m_pPTZCtrlCallback   = m_PTZCtrlCallback.get();
         m_pPTZCtrl->m_pPTZStatusCallback = m_PTZStatusCallback.get();
         m_pSPTZTracker->Create( m_pPTZCtrl );
      }

      float fZoomStepSize = 0.0f;
      if( ZoomModuleType_ThermalImaging == m_pPTZCtrl->m_PTZInfo.m_nZoomModuleType ) {
         PTZControlProperty* pPTZCtrlPropoerty = GetPTZControlProperty( VC->GetPTZControlTypeOfPTZCmd() );
         if( pPTZCtrlPropoerty ) {
            if( pPTZCtrlPropoerty->m_nCapabilities & PTZCtrlCap_PTZThermalCamSupported ) {
               fZoomStepSize = pPTZCtrlPropoerty->m_fThermalCameraZoomStepSize;
            }
         }
      }
      m_fDisplayZoomStepSize = fZoomStepSize;
   }

   if( IsPTZCamera() ) {
      m_fTgtZoomPos_MouseWheel            = -1.0f;
      m_nTgtZoomPos_MouseWheel_UpdateTime = 0;
      m_nPrvTimePT                        = 0;
      m_nPresetMoveElapsed                = 0;
      m_csActivePreset.Lock();
      m_pCurPreset         = NULL;
      m_pActivePresetGroup = m_PresetGroupList.GetSelectedGroup();
      m_csActivePreset.Unlock();
      if( FALSE == ( Options & CamOpt_PresetTouring ) )
         SetGotoHomePosToBeStarted( 0 );
      // 절대각 이동을 지원하지 않는 카메라 속성 얻은 후 상태 변경.

      if( m_pPTZCtrl ) {
         if( !m_pPTZCtrl->IsSetAbsPosSupported() ) {
            m_nPTZStateEx |= PTZStateEx_GotoAbsPosNotSupported;
            // 절대각 이동을 지원하지 않으면
            PTZMouseMoveMode = PTZMouseMove_Joystick;
         }
      }
   }
   //#endif
   BOOL bPTZTouring = FALSE;
   if( IsPTZCamera() ) {
      if( Options & CamOpt_PresetTouring ) {
         bPTZTouring = TRUE;
      }
      m_nPTZState &= ~( PTZState_PTZMoving | PTZState_PresetMoving );
   }
   if( bPTZTouring )
      m_nPTZState |= PTZState_PresetTour;
   else
      m_nPTZState &= ~PTZState_PresetTour;

   m_nCurVACSetupType = VACSetupType_NotLoaded;
   if( IsPTZCamera() ) {
      // PTZ카메라 인 경우 카메라 시작 시에는 PTZ가 멈추IsPTZReady
      OnPTZMoveStopped();
   } else {
      LoadMainVACSetup();
   }

   return ( DONE );
}

void CCamera::Close()
{
   if( IsPTZCamera() ) {
      SetActivePreset( NULL );
      m_pSPTZTracker->Close();
   }

   UninitVAModules();
   logd( "[CH:%d] Flushing Live Events - OK\n", ChannelNo + 1 );
   ClearLiveEventsAndObjectBuffer();
   m_pPTZCtrl = NULL;
   logd( "[CH:%d] Stopping Sending Event - OK\n", ChannelNo + 1 );
}

UINT CCamera::ThreadFunc_CameraMgrProc( LPVOID param )
{
#ifdef SUPPORT_PTHREAD_NAME
   char thread_name[16] = {};
   sprintf( thread_name, "CameraMgr" );
   pthread_setname_np( pthread_self(), thread_name );
#endif

   CCamera* pCamera = (CCamera*)param;
   pCamera->CameraMgrProc();
   
   return ( DONE );
}

BOOL CCamera::StartCameraMgrThread()
{
   m_hCameraMgrThread = std::thread( ThreadFunc_CameraMgrProc, this );

   return TRUE;
}

BOOL CCamera::StopCameraMgrThread()
{
   m_nStateEx |= CamStateEx_SignalStopCameraMgrThread;
   m_evtExitCameraMgrProc.SetEvent();
   if( m_hCameraMgrThread.joinable() )
      m_hCameraMgrThread.join();
   //m_evtExitCameraMgrProc.ResetEvent();
   m_nStateEx &= ~CamStateEx_SignalStopCameraMgrThread;
   return TRUE;
}

void CCamera::ProcessDecreaseFps()
{
   size_t check_count = 30;

   float va_fps = GetFrameRateLive( VCProcCallbackFuncType_ImgProc );
   if( va_fps > 0.0f ) {
      fpsRecVec.push_back( va_fps );

      if( check_count < fpsRecVec.size() ) {
         int count = 0;
         for( size_t ii = 0; ii < fpsRecVec.size(); ii++ ) {
            if( fpsRecVec[ii] < DesiredFrameRate *0.7f)
               count++;
         }

         if( count > 10 ) {
            MetaGeneratorSP generator = GetGenerator( MetaGeneratorType::JSON, this );
            generator->SetLocalSystem(dynamic_cast<CLocalSystem*>(System));
            generator->FillLiveFps( Camera_Va_Decrease_Fps );
            CRestSender::getInstance()->SendLiveFps( generator.get() );
            LOGW_ (L) << "LGUPLUS SEND LIVE FPS LOG TEST";
            fpsRecVec.clear();
         }

         if( fpsRecVec.size() > 0 )
            fpsRecVec.erase( fpsRecVec.begin() );
      }
   }
}

#include <fstream>
void CCamera::CameraMgrProc()
{
#ifdef SUPPORT_PTHREAD_NAME
   char thread_name[16]{};
   sprintf( thread_name, "Cam_c%d_u%d", ChannelNo, UID );
   pthread_setname_np( pthread_self(), thread_name );
#endif

   //LOGD << "Camera Manager Proc Starting...";

   // Normal is 10ms.
   CTimerManagerSP TimerManager;
   CMMTimer* pTimer_base = TimerManager->GetNewTimer( "CameraMgrProc 10ms" );
   pTimer_base->SetFPS( 100.0f );
   pTimer_base->StartTimer();

   CWaitForMultipleObjects wmo;
   enum CameraMgrProcEvtType {
      Event_ExitCameraMgrProc,
      Event_ProcessOnVideoFrame,
      Event_Timer_base_ms,
   };
   std::vector<CEventEx*> vecEvt;
   vecEvt.push_back( &m_evtExitCameraMgrProc );
   vecEvt.push_back( &m_evtDoProcessOnVideoFrame );
   vecEvt.push_back( &pTimer_base->GetTimerEvent() );
   wmo.Init( vecEvt );
   uint base_timer_counter = 0;
   bool bProcPer1000ms     = false;
   bool bProcPer200ms      = false;
   bool bProcPer100ms      = false;
   bool bProcPer50ms       = false;

   uint nPTZTrackOffTime = 0; // 사용자가 자동 추적을 Off 한 시간.

   std::vector<float> vec_fps;

   while( true ) {
      wmo.WaitForMultipleObjects( FALSE );
      std::vector<bool> vecSignal = wmo.GetSignal();

      if( vecSignal[Event_ExitCameraMgrProc] ) {
         wmo.ClearAllEventEx();
         break;
      }

      // 아이피 카메라에서 비디오 프레임이 수신되는 프레임의 처리.
      if( vecSignal[Event_ProcessOnVideoFrame] ) {
         //LOGD << TimerCheckerManager::Get().DebugMsg( "VideoFrameEvent", 0 );
         ProcessOnVideoFrame();
      }

      // 10밀리초에 한번꼴로 수행되는 작업. baseframe = 10ms. Cellinx is 20ms.
      if( vecSignal[Event_Timer_base_ms] ) {

         // pTimer_base_counter is 10ms
         bProcPer1000ms = ( base_timer_counter % 100 == 0 ) ? true : false;
         bProcPer200ms  = ( base_timer_counter % 20 == 0 ) ? true : false;
         bProcPer100ms  = ( base_timer_counter % 10 == 0 ) ? true : false;
         bProcPer50ms   = ( base_timer_counter % 5 == 0 ) ? true : false; // 20fps

         base_timer_counter++;

         // 50밀리마다 수행되는 작업
         if( bProcPer50ms ) {
            // 멀티스트리밍 관련
            VC->SetDecodeAndConnectionPolicy( VCDecodeAndConnectionPolicy_AdaptiveDecoding );
            DetermineVCDisplayCallbackFuncState();
            DetermineVCImgProcCallbackFuncState();
            DetermineVCVideoFrameCallbackFuncState();
            
            struct tm time_tm;
            plog::util::Time time_plog;
            plog::util::ftime( &time_plog );
            plog::util::localtime_s( &time_tm, &time_plog.time );
            
            int nLogCycle = System->m_Uplus_Log_Cycle_;
            std::string LogPath = System->m_Uplus_Log_Path_.c_str();
            
            if( time_tm.tm_min % nLogCycle == 0 && time_tm.tm_sec == 0)   {
               UPlus_now_filename_ = LGU::Logger::GetCycleLogFilePath(LogPath);
               if(IsExistDirectory( UPlus_now_filename_ ) == false) { 
                 LGU::Logger::SetWriteLogPath(UPlus_now_filename_);
               }
            }
            

         } // bProcPer50ms

         if( bProcPer200ms ) {               
 
            if( IsPTZCamera() && m_pPTZCtrl ) {
               BOOL bSmoothZoomTracking = FALSE;

               if( m_nPTZState & PTZState_SPTZTrkOngoing )
                  bSmoothZoomTracking = TRUE;

               m_pPTZCtrl->SetSmoothZoomCtrlMode( bSmoothZoomTracking );
            }
            // M/S 자동 추적 자동 On 관련.
            // 마스터/슬레이브에서 자동 추적을 Off 한 후 일정시간이 지나면 자동으로 On 하도록 한다.
            if( IsPTZCamera() && m_pPTZCtrl ) {
               uint32_t nCurTime = GetTickCount();
               if( !( Options & CamOpt_AutoPTZCtrlMode ) ) // PTZ 추적 플래그가 Off되어 있는 상태
               {
                  if( !nPTZTrackOffTime ) // 자동 추적상태 플레그가 Off 한 순간.
                     nPTZTrackOffTime = nCurTime;
                  if( nPTZTrackOffTime && ( nCurTime - m_nLastPTZMoveTime ) < 1000 ) {
                     nPTZTrackOffTime = nCurTime;
                  }
                  if( m_nStateEx & CamStateEx_SignalStopCameraMgrThread )
                     break;
                  if( Options & CamOpt_AutoPTZCtrlModeAutoOn ) {
                     if( !( m_nStateEx & CamStateEx_PTZTouringDlgOpened ) ) {
                        if( nCurTime - nPTZTrackOffTime >= m_nPTZTrkModeAutoOnWaitTime * 1000 ) {
                           Options |= CamOpt_AutoPTZCtrlMode; // 자동추적옵션이 일정시간이 지난다음 자동으로 켜지는 경우.
                           SetAutoPTZCtrlMode( TRUE, TRUE, TRUE );
                        }
                     }
                  } else
                     nPTZTrackOffTime = 0;
               } else {
                  nPTZTrackOffTime = 0;
               }
            } // if (IsPTZCamera () && m_pPTZCtrl)
         } // bProcPer200ms

         if( bProcPer1000ms ) {
            m_ivcpRtspSender->SendCameraInfo();
            ProcessOnResetObjectCounter();
            ProcessDecreaseFps();
            
            // 사용자 제어중이면 마지막 PTZ제어시간 업데이트
            if( m_nPTZState & PTZState_UserControlOngoing ) {
               m_nLastPTZMoveTime = GetTickCount();
            }

         } // bProcPer1Sec
      } // Event_Timer_10ms

   } // while

   pTimer_base->StopTimer();
   TimerManager->DeleteTimer( pTimer_base->m_nTimerID );

   logd( "Camera Manager Proc Exit!!\n" );
}

void CCamera::SetContPTZMoveSpeed( int pt_speed, int z_speed, int f_speed )
{
   ASSERT( IsPTZCamera() );
   if( pt_speed != -1 )
      Config_PTZC.PanTiltSpeed = pt_speed;
   if( z_speed != -1 )
      Config_PTZC.ZoomSpeed = z_speed;
   if( f_speed != -1 )
      Config_PTZC.FocusSpeed = f_speed;
}

BOOL CCamera::GetAutoPTZCtrlMode()
{
   if( Options & CamOpt_AutoPTZCtrlMode )
      return TRUE;
   return FALSE;
}

void CCamera::SetAutoPTZCtrlMode( BOOL bAutoPTZCtrl, BOOL bSendIVXConnection, BOOL bSendIVXVS )
{
   //logd ("[CH:%d] SetAutoPTZCtrlMode (%d)\n", ChannelNo+1, bAutoPTZCtrl);
   if( IsPTZCamera() ) {
      if( bAutoPTZCtrl )
         m_nPTZStateEx &= ~PTZStateEx_AllPTZCtrlDisabled;
      else
         m_nPTZStateEx |= PTZStateEx_AllPTZCtrlDisabled;
      if( bAutoPTZCtrl )
         Options |= CamOpt_AutoPTZCtrlMode;
      else
         Options &= ~CamOpt_AutoPTZCtrlMode;

      m_nPTZStateEx &= ~( PTZStateEx_PresetMoveBySensorInputToBeStarted | PTZStateEx_PresetMoveBySensorInputOngoing );

      if( !bAutoPTZCtrl ) {
         SetSPTZTrkToBeEnded();
      }
      m_nPTZStateEx &= ~( PTZStateEx_PresetMoveBySensorInputToBeStarted | PTZStateEx_PresetMoveBySensorInputOngoing );

      // PTZ 투어링
      if( bAutoPTZCtrl && Options & CamOpt_PresetTouring ) {
         m_nPTZState |= PTZState_PresetTour;
         SetPresetTourToBeStarted( 500 );
      } else {
         m_nPTZState &= ~PTZState_PresetTour;
      }
      // 제어권을 가져가면..
      if( !bAutoPTZCtrl ) {
         if( m_pPTZCtrl ) {
            m_pPTZCtrl->SetContPTZTrackMode( FALSE );
            if( m_nPTZState & ( PTZState_AutoPTZTrkByMasterOngoing | PTZState_SPTZTrkOngoing ) )
               m_pPTZCtrl->ContPTZMoveAV( FPTZVector( 0.0f, 0.0f, 0.0f ) );
         }
      } else {
         SetGotoHomePosToBeStarted( 500 );
      }
   }
}

BOOL CCamera::PausingVideoAnalytics( BOOL bPause, BOOL bAutoRelase, int nAutoRelaseWaitingTime, CLocalSystem* pServerSystem )
{
   if( bPause ) {
      Options |= CamOpt_PausingVideoAnalytics;
      m_nStateEx2 |= CamStateEx2_PausingVideoAnalyticsBegun;
      m_PausingVideoAnalyticsStartTime               = CTime::GetCurrentTime();
      m_nPausingVideoAnalyticsAutoReleaseWaitingTime = uint32_t(nAutoRelaseWaitingTime);
      if( bAutoRelase )
         Options |= CamOpt_PausingVideoAnalyticsToBeReleased;
   } else {
      Options &= ~CamOpt_PausingVideoAnalytics;
      m_nStateEx2 |= CamStateEx2_PausingVideoAnalyticsEnded;
   }
   return TRUE;
}

BOOL CCamera::PausingVideoAnalyticsSchedule( BOOL bPause, BOOL bAutoRelase, int nAutoRelaseWaitingTime, CLocalSystem* pServerSystem )
{
   if( bPause ) {
      EventDetector.Flag_CheckSchedule = FALSE;
      Options |= CamOpt_PausingVideoAnalyticsSchedule;
      m_PausingVideoAnalyticsScheduleStartTime               = CTime::GetCurrentTime();
      m_nPausingVideoAnalyticsScheduleAutoReleaseWaitingTime = nAutoRelaseWaitingTime;
      if( bAutoRelase )
         Options |= CamOpt_PausingVideoAnalyticsScheduleToBeReleased;
   } else {
      EventDetector.Flag_CheckSchedule = TRUE;
      Options &= ~CamOpt_PausingVideoAnalyticsSchedule;
      if( !( m_nState & CamState_VideoAnalyticsToBeInitialized ) ) {
         m_nState |= CamState_VideoAnalyticsToBeInitialized;
         logi( "[CH:%d] CamState_VideoAnalyticsToBeInitialized - 일시 중지해제\n", ChannelNo + 1 );
      }
   }
   return TRUE;
}

void CCamera::StoppingVideoAnalyticsForAWhile( uint nStoppingTimeInMilliSec )
{
   // 로컬 시스템인 경우에만 사용하는 함수임.
   BOOL bResetTime = FALSE;
   if( m_nStoppingVideoAnalyticsStartTime ) {
      if( GetTickCount() - m_nStoppingVideoAnalyticsStartTime < nStoppingTimeInMilliSec ) {
         bResetTime = TRUE;
      }
   } else {
      bResetTime = TRUE;
   }
   if( bResetTime ) {
      m_nStoppingVideoAnalyticsStartTime        = GetTickCount();
      m_nStoppingVideoAnalyticsPeriodInMilliSec = nStoppingTimeInMilliSec;
   }
}

void CCamera::ProcessOnVCDisplayStatus()
{
   int no_signals                   = int(VC->GetNoVideoSignalFlag());
   int connecting_chflags           = int(VC->GetConnectingChannelFlag());
   int authorization_failed_chflags = int(VC->GetAuthorizationFailedChannelFlag());

   BOOL bUpdateDisplayStatus = FALSE;
   if( no_signals != NoVideoSignalChannels )
      bUpdateDisplayStatus = TRUE;
   if( connecting_chflags != ConnectingChannels )
      bUpdateDisplayStatus = TRUE;
   if( authorization_failed_chflags != AuthorizationFailedChannels )
      bUpdateDisplayStatus = TRUE;

   if( bUpdateDisplayStatus ) {
      NoVideoSignalChannels       = ushort(no_signals);
      ConnectingChannels          = ushort(connecting_chflags);
      AuthorizationFailedChannels = ushort(connecting_chflags);
   }
}

/////////////////////////////////////////////////////////////////////////////////
//
// PTZ TOURING
//
/////////////////////////////////////////////////////////////////////////////////

BOOL CCamera::ApplyPTZTouringSetup( BOOL bEnablePTZTouring, BOOL bPresetChanged, CPresetGroupList& preset_groups, BOOL bDoNotDeactivate )
{
   if( bPresetChanged ) {
      int nActivePresetNo = 0;
      m_csActivePreset.Lock();
      if( m_pActivePreset )
         nActivePresetNo = m_pActivePreset->PresetNo;
      m_PresetGroupList.CopyFrom( preset_groups );
      if( nActivePresetNo )
         m_pActivePreset = m_PresetGroupList.FindPreset( nActivePresetNo );
      m_pCurPreset         = NULL;
      m_pActivePresetGroup = m_PresetGroupList.GetSelectedGroup();
      m_csActivePreset.Unlock();

      m_ivcpRtspSender->SetPresetMapDataToBeBroadcasted();
   }

   if( bEnablePTZTouring )
      Options |= CamOpt_PresetTouring;
   else
      Options &= ~CamOpt_PresetTouring;
   if( bEnablePTZTouring )
      m_nPTZState |= PTZState_PresetTour;
   else
      m_nPTZState &= ~PTZState_PresetTour;

   return TRUE;
}

BOOL CCamera::GetAbsPtzPos( PTZPosition& pos )
{
   float focus;
   if( DONE != m_pPTZCtrl->GetAbsPos( pos.PanAngle, pos.TiltAngle, pos.ZoomFactor, focus ) ) {
      return FALSE;
   }
   return TRUE;
}

BOOL CCamera::GetAbsPtzPos( FPTZVector& pos )
{
   if( DONE != m_pPTZCtrl->GetAbsPos( pos.P, pos.T, pos.Z, pos.F ) ) {
      return FALSE;
   }
   return TRUE;
}

BOOL CCamera::GotoAbsPtzPos( PTZPosition& pos )
{
   FPTZVector vec( pos.PanAngle, pos.TiltAngle, pos.ZoomFactor, PREV_CMD, PREV_CMD );
   return GotoAbsPtzPos( vec );
}

BOOL CCamera::GotoAbsPtzPos( FPTZVector& pos )
{
   if( !IsPTZCamera() ) return TRUE;
   PTZMove( PTZMoveType_GotoAbsPos, pos, TRUE );
   return FALSE;
   return TRUE;
}

BOOL CCamera::ConvertAbsPtzPos( PTAbsPosConvertMode nPTAbsPosConvertMode, const FPTZVector& pvInputAbsPTPos, FPTZVector& pvOutputPTPos )
{
   if( !m_pPTZCtrl ) return FALSE;

   pvOutputPTPos.Z = pvInputAbsPTPos.Z;

   if( PTAbsPosConvertMode_Inner2Outer == nPTAbsPosConvertMode ) {
      if( m_fDisplayZoomStepSize > 0.0f ) {
         float z         = pvOutputPTPos.Z;
         pvOutputPTPos.Z = pow( 2.0f, floor( ( log( z / m_fDisplayZoomStepSize ) / log( 2.0f ) ) + 0.5f ) ) * m_fDisplayZoomStepSize;
      }
   }

   float fMaxPanAngle, fMinPanAngle;
   m_pPTZCtrl->GetRange_PanAngle( fMinPanAngle, fMaxPanAngle );

   switch( Config_PTZC.PanRotationType ) {
   case PanRotationType_ClockWise:
      pvOutputPTPos.P = fMaxPanAngle - pvInputAbsPTPos.P + fMinPanAngle;
      break;
   case PanRotationType_CounterClockWise:
      pvOutputPTPos.P = pvInputAbsPTPos.P; // 내부적으로 CCW을 사용하기 때문에 복사만 한다.
      break;
   }

   switch( Config_PTZC.TiltRotationType ) {
   case TiltRotationType_Bottom90:
      pvOutputPTPos.T = pvInputAbsPTPos.T; // 내부적으로 Bottom이 90도 이기 때문에 복사만 한다.
      break;
   case TiltRotationType_Top90:
      pvOutputPTPos.T = -pvInputAbsPTPos.T;
      break;
   }

   float& fDueNorthPoleOffsetAngle = Config_PTZC.DueNorthPoleOffsetAngle;
   switch( nPTAbsPosConvertMode ) {
   case PTAbsPosConvertMode_Inner2Outer:
      pvOutputPTPos.P -= fDueNorthPoleOffsetAngle;
      break;
   case PTAbsPosConvertMode_Outer2Inner:
      pvOutputPTPos.P += fDueNorthPoleOffsetAngle;
      break;
   }

   switch( Config_PTZC.PanAngleDisplayRangeType ) {
   case PanAngleDisplayRangeType_0_P360:
      if( pvOutputPTPos.P >= 360.0f ) pvOutputPTPos.P -= 360.0f;
      if( pvOutputPTPos.P < 0.0f ) pvOutputPTPos.P += 360.0f;
      break;
   case PanAngleDisplayRangeType_M180_P180:
      if( pvOutputPTPos.P >= 360.0f ) pvOutputPTPos.P -= 360.0f;
      if( pvOutputPTPos.P < 0.0f ) pvOutputPTPos.P += 360.0f;
      if( 180.0f < pvOutputPTPos.P && pvOutputPTPos.P <= 360.0f ) pvOutputPTPos.P -= 360.0f;
      break;
   }
   pvOutputPTPos.F = pvInputAbsPTPos.F;

   return TRUE;
}

BOOL CCamera::SetPreset( int nPresetNo )
{
   if( !IsPTZCamera() ) return TRUE;
   if( m_pPTZCtrl ) {
      if( m_pPTZCtrl->SetPreset( nPresetNo ) )
         return FALSE;
   }
   return TRUE;
}

BOOL CCamera::ClearPreset( int nPresetNo )
{
   if( !IsPTZCamera() ) return TRUE;
   if( m_pPTZCtrl && m_pPTZCtrl->ClearPreset( nPresetNo ) )
      return FALSE;
   return TRUE;
}

BOOL CCamera::GotoPreset( int nPresetNo, int nPresetMoveType, BOOL bForceMaxSpeed )
{
   ASSERT( IsPTZCamera() );
   if( !m_pPTZCtrl ) return FALSE;

   // should single ptz check
   if( m_nPTZState & PTZState_SPTZTrkOngoing || m_nPTZState & PTZState_SPTZTrkBegun || m_nPTZState & PTZState_SPTZTrkToBeEnded ) {
      m_nPTZState |= PTZState_SPTZTrkEnded;
      logd( "[CH:%d] Want PTZ Preset Move!! But Keep going Single PTZ \n", ChannelNo + 1 );
   }

   logd( "[CH:%d] Goto Preset(%d) : Begun\n", ChannelNo + 1, nPresetNo );
   m_nLastPTZMoveTime = GetTickCount();
   m_nPresetMoveType  = nPresetMoveType;
   CCriticalSectionSP co( m_csActivePreset );

   CPreset* pPreset = m_PresetGroupList.FindPreset( nPresetNo );
   if( !pPreset ) return FALSE;

   if( m_nPresetMoveType != PresetMoveType_PresetMoveOnly ) {
      if( m_nPresetMoveType == PresetMoveType_Normal ) {
         if( m_nPTZState & PTZState_PresetTour ) {
            m_nPTZState |= PTZState_PresetTourPaused; // 사용자에 의하여 프리셋이동을 하는 경우.
            m_nPresetTourPausedTime = GetTickCount();
         }
      }
      m_nPTZState |= PTZState_PresetMoveStarted;
      if( m_nState & CamState_MainVACConfigToBeLoaded )
         m_nState &= ~CamState_MainVACConfigToBeLoaded;
      if( m_nStateEx2 & CamStateEx2_DefVideoAnalyticsSetupToBeLoaded )
         m_nStateEx2 &= ~CamStateEx2_DefVideoAnalyticsSetupToBeLoaded;
      if( m_nPTZStateEx & PTZStateEx_TheFirstPresetMovingToBeStarted )
         m_nPTZStateEx &= ~PTZStateEx_TheFirstPresetMovingToBeStarted;

      SetActivePreset( pPreset );
   }

   if( bForceMaxSpeed || PTZSwingMode_SwingByFullSpeed == pPreset->PTZSwingSpeed.SwingMode ) {
      m_pPTZCtrl->GotoPreset( nPresetNo );
   } else {
      if( FALSE == m_pPTZCtrl->IsRealTimeGetAbsPosSupported() ) {
         logd( "This Camera is not support PTZ swing\nChange to full speed mode", 5000 );
         pPreset->PTZSwingSpeed.SwingMode = PTZSwingMode_SwingByFullSpeed;
         m_pPTZCtrl->GotoPreset( nPresetNo );
      } else {
         FPTZVector pvAbsPosOfPreset = m_pPTZCtrl->GetAbsPosOfPreset( pPreset->PresetNo );
         if( PTZSwingMode_SwingByConstantAngleSpeed == pPreset->PTZSwingSpeed.SwingMode ) {
            m_pPTZCtrl->GotoAbsPosByConstantAngleSpeed( pvAbsPosOfPreset, pPreset->PTZSwingSpeed.AngleSpeed );
         }
         if( PTZSwingMode_SwingByConstantImageScrollSpeed == pPreset->PTZSwingSpeed.SwingMode ) {
            m_pPTZCtrl->GotoAbsPosByConstantImageScrollSpeed( pvAbsPosOfPreset, pPreset->PTZSwingSpeed.ImageScrollSpeed );
         }
         // Preset 이동 시 영상 분석: 시작 부분
         if( pPreset->Option & PresetOption_VideoAnalyticsOnPresetMoving ) {
            m_nState |= CamState_VideoAnalyticsOnPresetMoving;
            m_nState |= CamState_VideoAnalyticsToBeInitialized;
            logd( "[CH:%d] Video Analytics On PresetMoving : Begun\n", ChannelNo + 1 );
         }
      }
      m_nPTZState &= ~PTZState_UserControlOngoing;
   }
   return TRUE;
}

void CCamera::ContPTZMove( FPTZVector move_vec, BOOL bUserCtrl )
{
   FPTZVector null_pos;
   PTZMove( PTZMoveType_Cont, move_vec, NO_CMD, NO_CMD, null_fox2d, null_pos, bUserCtrl );
}

void CCamera::ContPTZMove( FPTZVector move_vec, float focus, float iris, BOOL bUserCtrl )
{
   FPTZVector null_pos;
   PTZMove( PTZMoveType_Cont | PTZMoveType_Focus | PTZMoveType_Iris, move_vec, focus, iris, null_fox2d, null_pos, bUserCtrl );
}

void CCamera::ContPTZMove_Focus( float focus, BOOL bUserCtrl )
{
   FPTZVector null_pos;
   PTZMove( PTZMoveType_Cont, FPTZVector( NO_CMD, NO_CMD, NO_CMD ), focus, NO_CMD, null_fox2d, null_pos, bUserCtrl );
}

void CCamera::ContPTZMove_Iris( float iris, BOOL bUserCtrl )
{
   FPTZVector null_pos;
   PTZMove( PTZMoveType_Cont, FPTZVector( NO_CMD, NO_CMD, NO_CMD ), NO_CMD, iris, null_fox2d, null_pos, bUserCtrl );
}

void CCamera::AbsPosMove( FPoint2D center_pos, BOOL bUserCtrl )
{
   FBox2D tgt_box;
   tgt_box( center_pos.X, center_pos.Y, 0.0f, 0.0f );
   AbsPosMove( tgt_box, bUserCtrl );
}

void CCamera::AbsPosMove( FBox2D tgt_box, BOOL bUserCtrl )
{
   FPTZVector null_pos;
   PTZMove( PTZMoveType_TargetBox, FPTZVector( NO_CMD, NO_CMD, NO_CMD ), NO_CMD, NO_CMD, tgt_box, null_pos, bUserCtrl );
}

void CCamera::PTZMove( int nPTZMoveType, FPTZVector pos, BOOL bUserCtrl )
{
   PTZMove( nPTZMoveType, FPTZVector( NO_CMD, NO_CMD, NO_CMD ), NO_CMD, NO_CMD, null_fox2d, pos, bUserCtrl );
}

void CCamera::PTZMove( int nPTZMoveType, PTZPosition pos, BOOL bUserCtrl )
{
   PTZMove( nPTZMoveType, FPTZVector( NO_CMD, NO_CMD, NO_CMD ), NO_CMD, NO_CMD, null_fox2d, FPTZVector( pos.PanAngle, pos.TiltAngle, pos.ZoomFactor, PREV_CMD, PREV_CMD ), bUserCtrl );
}

void CCamera::PTZMove( int nPTZMoveType, FPTZVector move_vec, float focus, float iris, FBox2D tgt_box, FPTZVector pos, BOOL bUserCtrl )
{
   if( !IsActivated() ) return;
   if( !m_pPTZCtrl ) return;
   FSize2D img_size( float(m_VideoFrameLive.GetWidth()), float(m_VideoFrameLive.GetHeight()) );

   m_nLastPTZMoveTime = GetTickCount();
   m_nPTZStateEx &= ~PTZStateEx_InHomePos;
   m_nPTZMoveType = nPTZMoveType;
   m_pPTZCtrl->SetContPTZTrackMode( !bUserCtrl );
   m_pPTZCtrl->SetUserCtrlMode( bUserCtrl );
   if( nPTZMoveType & PTZMoveType_Cont ) {
      if( move_vec.P != NO_CMD )
         move_vec.P *= 90.0f;
      if( move_vec.T != NO_CMD )
         move_vec.T *= 90.0f;

      // 감마 속도 적용
      FPTZVector m_fvGammaMove;
      static float fMaxGammaSpeed       = 90.0f;
      static float fTgtPosConvergeGamma = 1.5f;
      if( move_vec.P != NO_CMD ) {
         m_fvGammaMove.P = pow( fabs( move_vec.P ), fTgtPosConvergeGamma ) / pow( fabs( fMaxGammaSpeed ), fTgtPosConvergeGamma ) * fMaxGammaSpeed;
         if( move_vec.P < 0.0f )
            move_vec.P = -m_fvGammaMove.P;
         else
            move_vec.P = m_fvGammaMove.P;
      }
      if( move_vec.T != NO_CMD ) {
         m_fvGammaMove.T = pow( fabs( move_vec.T ), fTgtPosConvergeGamma ) / pow( fabs( fMaxGammaSpeed ), fTgtPosConvergeGamma ) * fMaxGammaSpeed;
         if( move_vec.T < 0.0f )
            move_vec.T = -m_fvGammaMove.T;
         else
            move_vec.T = m_fvGammaMove.T;
      }
      move_vec.F = focus;
      m_pPTZCtrl->ContPTZMoveAV( move_vec );
   }
   if( nPTZMoveType & PTZMoveType_TargetBox ) {
      m_pPTZCtrl->OffsetPosition( tgt_box );
   }
   if( nPTZMoveType & PTZMoveType_OffsetPos ) {
      m_pPTZCtrl->OffsetPosition( pos.P, pos.T, (int)pos.Z );
   }
   if( nPTZMoveType & PTZMoveType_GotoAbsPos ) {
      m_pPTZCtrl->GotoAbsPos( pos );
   }

   // 모든 종류의 PTZ 제어에 대해서...
   if( nPTZMoveType & ( PTZMoveType_Cont | PTZMoveType_Focus | PTZMoveType_TargetBox | PTZMoveType_OffsetPos | PTZMoveType_GotoAbsPos ) ) {
      if( m_nPTZState & PTZState_PresetTour ) {
         m_nPTZState |= PTZState_PresetTourPaused; // 사용자가 연속이동,절대각 이동을 하는 경우.
         m_nPresetTourPausedTime = GetTickCount();
         SetPresetTourToBeStarted( (DWORD)m_PresetGroupList.TouringWaitTime * 1000 );
      }
      if( bUserCtrl ) {
         if( !( m_nPTZState & PTZState_UserControlOngoing ) )
            m_nPTZState |= PTZState_UserControlBegun; // 사용자 연속이동 시작
         m_nPTZState |= PTZState_UserControlOngoing; // 사용자 연속이동 시작
      }
      m_csActivePreset.Lock();
      if( m_pActivePreset ) {
         SetActivePreset( NULL );
      }
      m_csActivePreset.Unlock();
      // 매우중요: 프리셋 이동 도중에 연속이동 제어를 하는 경우 프리셋 이동을 취소한다.
      if( m_nPTZState & PTZState_PresetMoving )
         m_nPTZState &= ~PTZState_PresetMoving;
   }

   // 절대각 제어에 대해서...
   if( nPTZMoveType & ( PTZMoveType_TargetBox | PTZMoveType_OffsetPos | PTZMoveType_GotoAbsPos ) ) {
      if( !( m_nPTZState & PTZState_GotoAbsPosBegun ) ) {
         m_nPTZState |= PTZState_GotoAbsPosBegun;
         m_nPTZCtrlStopTime = GetTickCount();
      }
      m_nPTZState |= PTZState_GotoAbsPosOngoing;
      m_nPTZState |= PTZState_UserControlEnded; // 사용자 절대각 제어.
      m_nPTZState &= ~PTZState_UserControlOngoing;
      if( bUserCtrl )
         m_nUserControlEndTime = GetTickCount();
   } else {
      // 조이스틱 제어에 의한 이동이 멈추었다면.
      // 중요: 연속이동 멈춤은 반드시 아래와 같이 할 것.
      //      CPTZCtrl::IsContPTZMoveStopped 함수는 실행된 연속이동 백터값으로 체크하기 때문에 약간 늦게 값이 반영된다.
      //      move_vec 벡터는 NO_CMD 명령을 사용하기 때문에 잘못된 결과가 나타난다.
      //if (m_pPTZCtrl->IsContPTZMoveStopped () && move_vec.IsContPTZFIMoveStopped ())
      if( m_pPTZCtrl->IsContPTZFIMoveStopped() ) {
         if( m_nPTZState & PTZState_PresetMoving )
            return;
         if( m_nPTZState & PTZState_UserControlOngoing ) {
            m_nPTZCtrlStopTime = GetTickCount();
            m_nPTZState |= PTZState_UserControlEnded; // 연속이동 제어시 정지한 경우.
            if( bUserCtrl )
               m_nUserControlEndTime = GetTickCount();
         }
         m_nPTZState &= ~PTZState_UserControlOngoing;
      }
   }
}

void CCamera::GotoHomePos( BOOL bForce )
{
   if( !Config_PTZC.Flag_UseMainHomePos ) return;
   //if (!IsActivated (   )) return;
   //if (!IsPTZCamera () || !m_pPTZCtrl) return;
   //if (!m_pPTZCtrl->IsPTZReady ()) return;

   if( Config_PTZC.Flag_UseMainHomePos || bForce ) {
      SetActivePreset( NULL );
      m_nState |= CamState_MainVACConfigToBeLoaded;

      FPTZVector& hp = Config_PTZC.HomePTZPos;
      m_pPTZCtrl->SetContPTZTrackMode( FALSE );
      logd( "[CH:%d] Goto Home Pos\n", ChannelNo + 1 );
      if( m_nPTZStateEx & PTZStateEx_GotoAbsPosNotSupported ) {
         if( Config_PTZC.HomePresetNo >= 0 ) {
            m_pPTZCtrl->GotoPreset( Config_PTZC.HomePresetNo );
            logd( "   GotoPreset PresetNo:%d\n", Config_PTZC.HomePresetNo );
         }
      } else {
         BOOL bUserControl = TRUE; // 연속이동 모드로 움직이지 않게 하기위해 사용자제어모드로 지정한다.
         PTZMove( PTZMoveType_GotoAbsPos, hp, bUserControl );
      }
      m_nPTZStateEx |= PTZStateEx_InHomePos;
      HomePosStartTime               = GetTickCount();
      m_nGotoHomePosWaitingStartTime = 0;
   }
}

void CCamera::OnPTZMoveStarted()
{
   if( HomePosStartTime ) {
      if( GetTickCount() - HomePosStartTime > 4000 ) {
         if( m_nPTZStateEx & PTZStateEx_InHomePos ) {
            m_nPTZStateEx &= ~PTZStateEx_InHomePos;
         }
         HomePosStartTime = 0;
      }
   }
}

void CCamera::OnPTZMoveStopped()
{
   logd( "[CCamera::OnPTZMoveStopped]\n" );

   // 모든 PTZ 이동중 상태를 클리어하기.
   m_nPTZState &= ~( PTZState_SignalPTZMoveStopped | PTZState_PTZStopping | PTZState_GotoAbsPosOngoing | PTZState_PTZMoving );
   // 추적 중이 아닐때만 비디오 분석 초기화 요청을 한다.
   if( !( m_nPTZState & ( PTZState_SPTZTrkOngoing | PTZState_AutoPTZTrkByMasterOngoing ) ) && ( m_nState & CamState_DoVideoAnalytics ) ) {
      if( !( m_nState & CamState_VideoAnalyticsToBeInitialized ) ) {
         m_nState |= CamState_VideoAnalyticsToBeInitialized;
      }
   }

   BOOL bAutoPTZTrackingMode = FALSE;
   if( ( Options & CamOpt_AutoPTZCtrlMode ) && m_nPTZState & ( PTZState_SPTZTrkOngoing | PTZState_AutoPTZTrkByMasterOngoing ) )
      bAutoPTZTrackingMode = TRUE;

   if( m_nState & CamState_VideoAnalyticsOnPresetMoving ) {
      logd( "[CH:%d] Video Analytics On PresetMoving : Ended\n", ChannelNo + 1 );
      m_nState &= ~CamState_VideoAnalyticsOnPresetMoving;
   }

   if( !( m_nPTZState & PTZState_PresetMoving ) && !bAutoPTZTrackingMode && !( m_nPTZStateEx & PTZStateEx_InHomePos ) ) {
      m_nStateEx2 |= CamStateEx2_DefVideoAnalyticsSetupToBeLoaded;
   }

   if( Config_PTZC.Flag_UseMainHomePos && !bAutoPTZTrackingMode ) {
      m_PresetGroupList.TouringWaitTime = 0;
      Config_PTZC.WaitingTime_HomePos   = 0;
      SetPresetTourToBeStarted( (DWORD)m_PresetGroupList.TouringWaitTime * 1000 );
      SetGotoHomePosToBeStarted( int( Config_PTZC.WaitingTime_HomePos * 1000 ) );
   }
   // jun [2014-07-02]: 사용자 제어가 상태가 풀리지 않아 영상분석이 안되는 문제에 대한 조치임.
   //                   추후에 보다 근본적인 문제 해결이 필요할 듯함.
   m_nPTZState &= ~PTZState_UserControlOngoing;
}

void CCamera::ForceToStartHomePosVideoAnalytics()
{
   if( m_pPTZCtrl ) {
      if( ( Options & CamOpt_AutoPTZCtrlMode ) && ( m_nPTZState & PTZState_PresetTour ) && !( m_nPTZState & PTZState_PresetTourPaused ) ) return;
      m_nPTZStateEx |= PTZStateEx_InHomePos;
      m_nPTZState &= ~PTZState_GotoHomePosToBeStarted;
      if( !( m_nState & CamState_VideoAnalyticsToBeInitialized ) ) {
         m_nState |= CamState_VideoAnalyticsToBeInitialized;
         logi( "[CH:%d] CamState_VideoAnalyticsToBeInitialized - ForceToStartHomePosVideoAnalytics\n", ChannelNo + 1 );
      }
      m_nState |= CamState_MainVACConfigToBeLoaded;
   }
}

void CCamera::SetGotoHomePosToBeStarted( int nWaitTime )
{
   m_nGotoHomePosWaitingTime = nWaitTime;
   if( Config_PTZC.Flag_UseMainHomePos && !( m_nPTZState & ( PTZState_SPTZTrkOngoing | PTZState_AutoPTZTrkByMasterOngoing ) ) && !( m_nPTZStateEx & ( PTZStateEx_InHomePos ) ) && !( m_nPTZStateEx & ( PTZStateEx_PresetMoveBySensorInputOngoing | PTZStateEx_AllPTZCtrlDisabled ) ) && ( !( m_nPTZState & PTZState_PresetTour ) || ( m_nPTZState & PTZState_PresetTour && !( Options & CamOpt_AutoPTZCtrlMode ) ) ) ) {
      m_nPTZState |= PTZState_GotoHomePosToBeStarted;
      uint32_t nUserControlElapsedTime = GetTickCount() - m_nUserControlEndTime;
      if( nUserControlElapsedTime < nWaitTime * 1.5f ) {
         nWaitTime = nWaitTime - nUserControlElapsedTime;
         if( nWaitTime < 0 ) nWaitTime = 0;
         m_nGotoHomePosWaitingTime = nWaitTime;
      }
      m_nGotoHomePosWaitingStartTime = GetTickCount();
      logd( "[CH:%d] PTZ will move to home pos after %d sec\n", ChannelNo + 1, nWaitTime / 1000 );
   }
   //이노뎁 알람 이벤트 발생시 ptz 제어권 및 프리셋 이동
   else if( Config_PTZC.Flag_UseMainHomePos && ( m_nPTZStateEx & PTZStateEx_GoHomeOnPTZFreeze ) ) {
      m_nPTZState |= PTZState_GotoHomePosToBeStarted;
      m_nPTZStateEx &= ~PTZStateEx_GoHomeOnPTZFreeze;
      m_nGotoHomePosWaitingStartTime = GetTickCount();
      logd( "[CH:%d] PTZ will move to home pos after %d sec (when PTZFreeze state)\n", ChannelNo + 1, nWaitTime / 1000 );
   }
}

void CCamera::SetPresetTourToBeStarted( int nWaitTime )
{
   BOOL bPrevPresetTourToBeRestarted = FALSE;
   if( m_nPTZState & PTZState_PresetTourToBeRestarted )
      bPrevPresetTourToBeRestarted = TRUE;
   m_nExePresetTourWaitingTime = nWaitTime;

   if( Options & CamOpt_AutoPTZCtrlMode && !( m_nPTZState & ( PTZState_SPTZTrkOngoing | PTZState_AutoPTZTrkByMasterOngoing ) ) && m_nPTZState & PTZState_PresetTour && m_nPTZState & PTZState_PresetTourPaused && !( m_nPTZStateEx & ( PTZStateEx_PresetMoveBySensorInputOngoing | PTZStateEx_AllPTZCtrlDisabled ) ) ) {
      m_nPTZState |= PTZState_PresetTourToBeRestarted;
      m_nExePresetTourWaitingStartTime = GetTickCount();
      // Off -> On이 되는 순간만 메시지 출력
      if( !bPrevPresetTourToBeRestarted )
         logd( "[CH:%d] PTZ will restart touring after %d sec\n", ChannelNo + 1, nWaitTime / 1000 );
   }
}

void CCamera::SetGotoHomePosOrPresetTourToBeStarted( int nWaitTime )
{
   SetPresetTourToBeStarted( nWaitTime );
   SetGotoHomePosToBeStarted( nWaitTime );
}

BOOL CCamera::SetAllPTZMoveStopped()
{
   int nCheckState = PTZState_UserControlBegun | PTZState_UserControlOngoing | PTZState_UserControlEnded | PTZState_PTZStopping | PTZState_PresetMoveStarted | PTZState_PresetMoving;
   if( m_nPTZState & nCheckState ) {
      m_nPTZState &= ~nCheckState;
      return TRUE;
   }
   return FALSE;
}

void CCamera::OnRTSPSetParameter( char const* fullRequestStr )
{
   CRtspParameter RtspParameter( this );
   RtspParameter.OnRTSPSetParameter( fullRequestStr );
}

void CCamera::OnRTSPGetParameter( char const* fullRequestStr )
{
   CRtspParameter RtspParameter( this );
   RtspParameter.OnRTSPGetParameter( fullRequestStr );
}

void CCamera::DoImgProcDispatchPTZMsg( const VC_PARAM_STREAM& param )
{
   if( !m_pPTZCtrl || !IsPTZCamera() ) return;
   // 카메라 시작 시 Home Position 이동
   m_nDoImgProcPos = 105;
   if( m_nPTZState & PTZState_GotoHomePosToBeStarted ) {
      if( m_pPTZCtrl->IsPTZReady() ) {
         if( m_nPTZState & ( PTZState_SPTZTrkOngoing | PTZState_AutoPTZTrkByMasterOngoing ) )
            m_nPTZState &= ~PTZState_GotoHomePosToBeStarted;
         if( !( m_nPTZState & PTZState_PresetTour ) || ( m_nPTZState & PTZState_PresetTour && !( Options & CamOpt_AutoPTZCtrlMode ) ) ) {
            if( m_nPTZState & PTZState_PTZMoving )
               m_nGotoHomePosWaitingStartTime = GetTickCount();
            if( Config_PTZC.Flag_UseMainHomePos ) {
               uint nWaitTime = m_nGotoHomePosWaitingTime;
               if( GetTickCount() - m_nGotoHomePosWaitingStartTime > nWaitTime ) {
                  m_nPTZState &= ~PTZState_GotoHomePosToBeStarted;
                  GotoHomePos();
               }
            }
         }
      }
   }
   //----------------------------------------------------------------
   // CPTZCtrl 객체로부터 PTZ이동이 중지되었다는 신호를 받았을 때
   // 중요 : PTZState_SignalPTZMoveStarted 상태 체크보다 우선해야 한다.
   //        카메라가 멈추는과 거의 동시에 카메라 제어를 시작하게 되면
   //        Stop, Start 플레그가 동시에 켜질 수도 있다.
   if( m_nPTZState & PTZState_SignalPTZMoveStopped ) {
      m_nPTZState &= ~( PTZState_SignalPTZMoveStopped );
      OnPTZMoveStopped();
   }
   // CPTZCtrl 객체로부터 PTZ이동이 시작되었다는 신호를 받았을 때
   if( m_nPTZState & PTZState_SignalPTZMoveStarted ) {
      m_nPTZState &= ~( PTZState_SignalPTZMoveStarted );
      m_nPTZState |= PTZState_PTZMoving;
      OnPTZMoveStarted();
   }
   if( m_nPTZState & PTZState_PTZMoving ) {
      if( m_nPTZState & PTZState_PresetTour && m_nPTZState & PTZState_PresetTourPaused ) {
         m_nPresetTourPausedTime          = GetTickCount();
         m_nExePresetTourWaitingStartTime = GetTickCount();
      }
   }
   //----------------------------------------------------------------
   // PTZ 제어에 의하여 발생된 메시지(상태)에 대한 처리를 한다.
   // 사용자가 PTZ 제어를 시작하였을 때.
   if( m_nPTZState & PTZState_UserControlBegun ) {
      m_nState &= ~( CamState_CreatingBKModel );
      m_nPTZState &= ~( PTZState_UserControlBegun | PTZState_PresetMoving | PTZState_PTZStopping );
      InvalidateAllObjectsAndEvents();
      m_nPTZState |= PTZState_PTZMoving;
   }
   // 사용자가 PTZ 제어를 종료 하였을 때.
   else if( m_nPTZState & PTZState_UserControlEnded ) {
      m_nPTZState &= ~( PTZState_UserControlEnded );
      m_nPTZState |= PTZState_PTZStopping;
   } else if( m_nPTZState & PTZState_GotoAbsPosOngoing ) {
      // 절대각 이동 중 인 경우 지정된 시간(m_nPTZMoveStopWaitTime)이 지나면 절대각이동상태(PTZState_GotoAbsPosOngoing)를 Off 한다.
      if( GetTickCount() - m_nPTZCtrlStopTime >= m_nPTZMoveStopWaitTime ) {
         m_nPTZState &= ~PTZState_GotoAbsPosOngoing;
         OnPTZMoveStopped();
      }
   }
   // 사용자가 PTZ 제어를 중지한 시작한 시점부터 실제로 PTZ 카메라 이동이 멈추기 전까지.
   if( m_nPTZState & PTZState_PTZStopping ) {
      // 사용자에의하여 이동되었으면 정해진 시간동안 기다린후 해제한다.
      if( GetTickCount() - m_nPTZCtrlStopTime >= m_nPTZMoveStopWaitTime ) {
         m_nPTZState &= ~( PTZState_PTZStopping );
         OnPTZMoveStopped();
      }
   }

   // 비디오 서버,클라이언트에서 현재 절대각 및 초점거리 값을 사용한다.
   if( m_pPTZCtrl ) {
      m_pvCurAbsPTZPos  = m_pPTZCtrl->GetAbsPos();
      m_fCurFocalLength = m_pPTZCtrl->GetFocalLength();
   }

   m_nDoImgProcPos = 140;

   //--------------------------------------------------------------------------
   // 센서입력에 의한 프리셋 이동 예정인 경우.
   if( m_nPTZStateEx & PTZStateEx_PresetMoveBySensorInputToBeStarted ) {
      if( !( m_nPTZState & ( PTZState_SPTZTrkBegun | PTZState_SPTZTrkOngoing ) ) ) {
         m_nPTZStateEx &= ~PTZStateEx_PresetMoveBySensorInputToBeStarted;
         m_nPTZStateEx |= PTZStateEx_PresetMoveBySensorInputOngoing;
         if( m_nPTZState & PTZState_PresetTour ) {
            m_nPTZState |= PTZState_PresetTourPaused;
            m_nPresetTourPausedTime = GetTickCount();
         }
         m_nPresetMoveTimeBySenserInput = GetTickCount();
         GotoPreset( m_nPresetNoBySensorInput, PresetMoveType_Normal, TRUE );
         logd( "[CH:%d] Goto Preset[%d] By Alarm Input - Start\n", ChannelNo + 1, m_nPresetNoBySensorInput );
      }
   }
   if( m_nPTZStateEx & PTZStateEx_PresetMoveBySensorInputOngoing ) {
      if( GetTickCount() - m_nPresetMoveTimeBySenserInput > m_nPresetPeriodBySensorInput ) {
         m_nPTZStateEx &= ~PTZStateEx_PresetMoveBySensorInputOngoing;
         logd( "[CH:%d] Goto Preset[%d] By Alarm Input - Ended\n", ChannelNo + 1, m_nPresetNoBySensorInput );
         SetGotoHomePosOrPresetTourToBeStarted( 2000 );
         //외부 알람연동에 의해 프리셋 이동 후 제어권넘기기
      }
   }
   m_nDoImgProcPos = 160;
   //--------------------------------------------------------------------------
   // PTZ 위치 초기화
   // 1. 위치 초기화 스케줄을 체크하고 위치초기화 실행시점에 시작상태로 둔다.
   //    사용자에의하여 실행이 되었으면 모든 PTZ제어를 중단시키고 위치초기화를 바로 실행한다.
   BOOL bPTZPosInit = FALSE;
   if( Config_PTZC.m_bUsePTZPosInitSchedule ) {
      if( !( m_nPTZStateEx & ( PTZStateEx_PTZPosInitToBeStarted | PTZStateEx_PTZPosInitBegun | PTZStateEx_PTZPosInitOngoing ) ) ) {
         if( PTZPosInitScheduling_AtSpecifiedTime == Config_PTZC.m_nPTZPosInitSchedulingMode ) {
            CTime ct = CTime::GetCurrentTime();
            if( ct - Config_PTZC.m_tmPrevPosInitTime > CTimeSpan( Config_PTZC.m_nDayInterval, 0, 0, 0 ) ) {
               CTime tf = Config_PTZC.m_tmPosInitTimeFrom;
               CTime tmPosInitTime;
               if( Config_PTZC.m_bRandomTimeOnSpecifiedPeriod ) {
                  tmPosInitTime = CTime( ct.GetYear(), ct.GetMonth(), ct.GetDay(), tf.GetHour(), tf.GetMinute(), 0 );
                  tmPosInitTime += Config_PTZC.m_tsPosInitRandomTimeSpan;
               } else {
                  tmPosInitTime = CTime( ct.GetYear(), ct.GetMonth(), ct.GetDay(), tf.GetHour(), tf.GetMinute(), 0 );
               }
               BOOL bPositionInitTimePassed = ct > tmPosInitTime;
               if( !m_bPrevPositionInitTimePassed && bPositionInitTimePassed ) {
                  bPTZPosInit = TRUE;
               }
               m_bPrevPositionInitTimePassed = bPositionInitTimePassed;
            }
         } else if( PTZPosInitScheduling_AtSpecifiedIntervals == Config_PTZC.m_nPTZPosInitSchedulingMode ) {
            CTime ct = CTime::GetCurrentTime();
            if( ct - Config_PTZC.m_tmPrevPosInitTime > CTimeSpan( 0, Config_PTZC.m_nInitPeriod_Time, Config_PTZC.m_nInitPeriod_Minute, 0 ) ) {
               bPTZPosInit = TRUE;
            }
         }
      }
   }
   if( bPTZPosInit ) m_nPTZStateEx |= PTZStateEx_PTZPosInitToBeStarted;

   // 2. 다음의 PTZ 제어가 중지될 때 까지 기다린다.
   //    - M/S추적, 단일PTZ추적, Handoff, 공조감시, 프리셋 이동, 이벤트 발생
   if( m_nPTZStateEx & PTZStateEx_PTZPosInitToBeStarted ) {
      if( IsPTZDoingNothing() ) {
         m_nPTZStateEx &= ~PTZStateEx_PTZPosInitToBeStarted;
         m_nPTZStateEx |= PTZStateEx_PTZPosInitBegun;
      }
   }
   // 3. PTZ 위치 초기화 수행
   if( m_nPTZStateEx & PTZStateEx_PTZPosInitBegun ) {
      m_nPTZStateEx &= ~PTZStateEx_PTZPosInitBegun;
      m_nPTZStateEx |= PTZStateEx_PTZPosInitOngoing;
      m_pPTZCtrl->PosInit();
      if( m_nPTZState & PTZState_PresetTour )
         m_nPTZState |= PTZState_PresetTourPaused;
      logd( "[CH:%d] PTZ Position Initialization - Start\n", ChannelNo + 1 );
      m_nPTZState |= PTZState_PTZMoving;
      Config_PTZC.m_tmPrevPosInitTime = CTime::GetCurrentTime();
   }
   // 4. PTZ 위치 초기화 완료
   if( m_nPTZStateEx & PTZStateEx_PTZPosInitOngoing ) {
      if( !( m_pPTZCtrl->m_nState & PTZ_State_PosInitializing ) ) {
         m_nPTZStateEx &= ~PTZStateEx_PTZPosInitOngoing;
         m_nPTZState &= ~PTZState_PTZMoving;
         logd( "[CH:%d] PTZ Position Initialization - Ended \n", ChannelNo + 1 );
         if( m_nPTZState & PTZState_PresetTour ) {
            m_nPTZState |= PTZState_PresetTourPaused;
            m_nPresetTourPausedTime = GetTickCount();
         }
         SetGotoHomePosOrPresetTourToBeStarted( 2000 );
         if( !( m_nState & CamState_ToBeRestarted ) ) {
            if( Config_PTZC.m_bRestartCameraAfterPosInit ) {
               RestartCamera();
            }
         }
      }
   }
}

void CCamera::DoImgProcPresetTouring( const VC_PARAM_STREAM& param )
{
   if( !m_pPTZCtrl || !IsPTZCamera() ) return;

   BOOL bRestartPresetTour = FALSE;
   if( CheckStateChange( m_nPrvPTZState, m_nPTZState, int( PTZState_PresetTour ), TRUE ) ) {
      if( Options & CamOpt_RestartPresetTouring ) {
         m_nPTZState |= PTZState_RestartPresetTourFromFirstPreset;
         bRestartPresetTour = TRUE;
      }
   }

   uint32_t nCurTime = GetTickCount();
   uint nElapsedTick = 0;
   if( m_nPrvTimePT )
      nElapsedTick = nCurTime - m_nPrvTimePT;
   m_nPrvTimePT = nCurTime;

   if( !( m_nPrvPTZState & PTZState_PresetTour ) && ( m_nPTZState & PTZState_PresetTour ) )
      m_nPTZStateEx |= PTZStateEx_TheFirstPresetMovingToBeStarted;
   if( ( m_nPrvPTZState & PTZState_PresetTourPaused ) && !( m_nPTZState & PTZState_PresetTourPaused ) )
      m_nPTZStateEx |= PTZStateEx_TheFirstPresetMovingToBeStarted;

   ///////////////////////////////////////////////////////////////////
   // Preset Touring을 동작시키는 부분이다.
   // 프리셋 이동 후 경과 시간을 체크하고 시간이 경과되면 다음 프리셋으로 이동한다.
   // 이때 PTZState_PresetMoveStarted 상태가 On 된다.
   if( Options & CamOpt_AutoPTZCtrlMode && ( m_nPTZState & PTZState_PresetTour ) && m_pActivePresetGroup && VC->GetVideoSignalFlag() ) {
      m_csActivePreset.Lock();

      CPreset *next, *prev;
      if( bRestartPresetTour ) {
         if( m_pCurPreset ) {
            GotoPreset( m_pCurPreset->PresetNo, PresetMoveType_Touring, TRUE );
         }
      }
      if( m_nPTZState & PTZState_PresetTourPaused ) {
         BOOL bRestartPresetTourInPauseState = FALSE;
         // 정해진 대기시간 이후 투어링을 재시작 한다.
         if( m_nPTZState & PTZState_PresetTourToBeRestarted ) {
            uint nWaitTime = m_nExePresetTourWaitingTime;
            if( GetTickCount() - m_nExePresetTourWaitingStartTime > nWaitTime ) {
               m_nPTZState &= ~( PTZState_PresetTourToBeRestarted );
               bRestartPresetTourInPauseState = TRUE;
            }
         }
         // PTZ 추적을 하고있지 않으면 주기적으로 PTZ 재시작을 한다.
         // 투어링이 재시작되지 않는 오류에 대한 예외처리 임. 원인이 밝혀지면 삭제해야 할 코드
         static int nPTZState_SomeDoing   = 0xFFFFFFFF & ~( PTZState_PresetTour | PTZState_PresetTourPaused );
         static int nPTZStateEx_SomeDoing = 0xFFFFFFFF & ~( PTZStateEx_InHomePos | PTZStateEx_GotoAbsPosNotSupported );
         if( !( m_nPTZState & nPTZState_SomeDoing ) && !( m_nPTZStateEx & nPTZStateEx_SomeDoing ) ) {
            if( GetTickCount() - m_nPresetTourPausedTime > 60000 ) {
               m_nPresetTourPausedTime        = GetTickCount();
               bRestartPresetTourInPauseState = TRUE;
            }
         }

         if( bRestartPresetTourInPauseState ) {
            m_nPTZState &= ~PTZState_PresetTourPaused;
            if( m_pCurPreset ) {
               GotoPreset( m_pCurPreset->PresetNo, PresetMoveType_Touring, TRUE );
               m_nPresetTouringStartTime = GetTickCount();
            }
         }
         nElapsedTick = 0;
      } else if( ( m_nPTZState & PTZState_PresetMoving ) && ( m_nPresetMoveType == PresetMoveType_Touring ) ) {
         nElapsedTick = 0;
      }
      m_nPresetMoveElapsed += nElapsedTick;

      BOOL bForceMaxSpeed = FALSE;
      bForceMaxSpeed      = ( m_nPTZStateEx & PTZStateEx_TheFirstPresetMovingToBeStarted ) ? TRUE : FALSE;

      if( !m_pCurPreset || ( ( Options & CamOpt_RestartPresetTouring ) && ( m_nPTZState & PTZState_RestartPresetTourFromFirstPreset ) ) ) {
         // Preset 순회 방법에 따라 처음 시작되어야 하는 초기 프리셋을 선택한다.
         switch( m_pActivePresetGroup->CycleType ) {
         case PresetTourCycleMode_Foword:
         case PresetTourCycleMode_Return:
            m_pCurPreset = m_pActivePresetGroup->First();
            break;
         case PresetTourCycleMode_Reverse:
            m_pCurPreset = m_pActivePresetGroup->Last();
            break;
         default:
            ASSERT( FALSE );
            break;
         }
         if( m_pCurPreset ) {
            GotoPreset( m_pCurPreset->PresetNo, PresetMoveType_Touring, bForceMaxSpeed );
            m_nPresetMoveElapsed = 0;
         }
      } else {
         int nDwellTimeSec = m_pCurPreset->DwellTime.Seconds;
         // 프리셋이 1개만 설정되어있는 경우 다음프리셋으로 이동시키지 못하도록 아래와 같은 처리를 한다.
         if( m_pActivePresetGroup->GetNumItems() == 1 )
            nDwellTimeSec = 3600 * 6;
         // 프리셋 이동 남은시간 업데이트.
         m_nPresetMoveRemainTime = nDwellTimeSec - m_nPresetMoveElapsed / 1000;

         BOOL bGotoNextPreset = FALSE;
         if( m_nPresetMoveElapsed > uint( nDwellTimeSec * 1000 ) ) {
            bGotoNextPreset = TRUE;
            logd( "GotoNextPreset : m_nPreset : %d // Dwell : %d", m_pCurPreset->PresetNo, nDwellTimeSec * 1000 );
         }
         if( ( ( m_pCurPreset->Option & PresetOption_StoppingPresetMovingOnEvent ) && m_nState & CamState_EventExist ) && !( m_nPTZState & ( PTZState_UserControlBegun | PTZState_UserControlOngoing ) ) && !( m_nPTZState & PTZState_PresetTourPaused ) && ( GetTickCount() - m_nLastPTZMoveTime > (uint)m_PresetGroupList.TouringWaitTime * 1000 ) )
            bGotoNextPreset = FALSE;

         // 시간이 경과되면 다음 프리셋으로 이동.
         if( bGotoNextPreset ) {
            //순방향
            if( m_pActivePresetGroup->CycleType == PresetTourCycleMode_Foword ) {
               next = m_pCurPreset->Next;
               if( next )
                  m_pCurPreset = next;
               else
                  m_pCurPreset = m_pActivePresetGroup->First();
            }
            //역방향
            else if( m_pActivePresetGroup->CycleType == PresetTourCycleMode_Reverse ) {
               prev = m_pCurPreset->Prev;
               if( prev )
                  m_pCurPreset = prev;
               else
                  m_pCurPreset = m_pActivePresetGroup->Last();
            }
            //왕복
            else if( m_pActivePresetGroup->CycleType == PresetTourCycleMode_Return ) {
               next = m_pCurPreset->Next;
               prev = m_pCurPreset->Prev;
               if( m_nPTZTourReturnDirection == PTZTourReturnDirection_Next ) {
                  if( next )
                     m_pCurPreset = next;
                  else
                     m_pCurPreset = prev;
                  if( !next ) m_nPTZTourReturnDirection = PTZTourReturnDirection_Prev;
                  ;
               } else if( m_nPTZTourReturnDirection == PTZTourReturnDirection_Prev ) {
                  if( prev )
                     m_pCurPreset = prev;
                  else
                     m_pCurPreset = next;
                  if( !prev ) m_nPTZTourReturnDirection = PTZTourReturnDirection_Next;
               }
            }
         }
         if( bGotoNextPreset ) {
            if( m_pCurPreset ) {
               GotoPreset( m_pCurPreset->PresetNo, PresetMoveType_Touring, bForceMaxSpeed );
               m_nPresetMoveElapsed = 0;
            }
         }
      }
      m_csActivePreset.Unlock();
   }

   if( m_nPTZState & PTZState_RestartPresetTourFromFirstPreset )
      m_nPTZState &= ~PTZState_RestartPresetTourFromFirstPreset;

   //////////////////////////////////////////////////////////////////////////////////////////////////////

   // 사용자가 PTZ 제어를 시작하였을 때.
   if( m_nPTZState & ( PTZState_UserControlBegun | PTZState_UserControlEnded | PTZState_UserControlOngoing ) ) {
      // 건너 뛰기.
   }
   // 프리셋 이동 시작 시점
   else if( m_nPTZState & PTZState_PresetMoveStarted ) {
      m_nPTZState &= ~( PTZState_PresetMoveStarted | PTZState_PTZMoving | PTZState_PTZStopping );
      m_nPTZState |= ( PTZState_PresetMoving );
      m_nState &= ~CamState_CreatingBKModel;
      m_nPresetStartTime = GetTickCount();
      InvalidateAllObjectsAndEvents();
   }
   m_nDoImgProcPos = 240;
   // 사용자가 PTZ 제어를 중지한 시작한 시점부터 실제로 PTZ 카메라 이동이 멈추기 전까지.
   if( m_nPTZState & PTZState_PTZStopping ) {
      // 건너 뛰기.
   } else if( m_nPTZState & PTZState_PresetMoving ) {
      // CPreset 객체는 PTZ가 멈추었는지를 검사하기위한 CPTZStopCheck 객채를 가지고 있다.
      // CPTZStopCheck의 맴버에는 FrameChangeDetector에 필요한 파라메터를 가지고 있으므로
      CPTZStopCheck* pPTZStopChecker = NULL;
      BOOL bUsePTZStopChecker        = TRUE; // CPTZStopCheck을 사용할 것인가?
      BOOL bStopPresetMoving         = FALSE; // Preset이동중인지?
      m_csActivePreset.Lock();
      switch( m_nPresetMoveType ) {
      case PresetMoveType_Normal:
      case PresetMoveType_Touring:
         if( m_pActivePreset )
            pPTZStopChecker = &m_pActivePreset->PTZStopCheck;
         else {
            bUsePTZStopChecker = FALSE; // ActivePreset이 설정되지 않았다면 CPTZStopCheck을 사용하지 않는다.
            bStopPresetMoving  = TRUE; // ActivePreset이 설정되지 않았다면 이미 멈춰있은것으로 판단한다.  --> ????
         }
         break;
      default:
         bUsePTZStopChecker = FALSE;
         bStopPresetMoving  = TRUE;
         break;
      }
      // [프리셋 위치로 이동 중인지 체크...]
      if( bUsePTZStopChecker ) {
         pPTZStopChecker->PTZMovingTime = 0.5f;
         float ptzMovingTime            = pPTZStopChecker->PTZMovingTime;
         //if (ptzMovingTime < 3.0f) ptzMovingTime = 3.0f;
         if( GetTickCount() - m_nPresetStartTime >= ( ptzMovingTime + 1.0f ) * 1000 ) {
            if( m_pPTZCtrl && m_pPTZCtrl->IsRealTimeGetAbsPosSupported() && !m_pPTZCtrl->m_PTZInfo.m_nTxOnly ) {
               if( !m_pPTZCtrl->IsPTZFMoving() )
                  bStopPresetMoving = TRUE;
            } else
               bStopPresetMoving = TRUE; // 지정된 시간이 지났으므로 Preset이동이 멈추었다.
         }
      }
      // [프리셋 위치로 이동이 완료됨...]
      if( bStopPresetMoving ) {
         ResetVAModules();
         UninitVAModules();
         if( m_pActivePreset )
            LoadPresetVACSetup( m_pActivePreset ); // 해당 프리셋의 비디오 분석 정보를 로드한다.
         else
            LoadMainVACSetup();
         InitVAModules();
         if( m_nPTZState & PTZState_PresetTourPaused )
            SetPresetTourToBeStarted( (DWORD)m_PresetGroupList.TouringWaitTime * 1000 );
         m_nPTZState &= ~PTZState_PresetMoving;
         m_nPTZState &= ~PTZState_PTZMoving; // 사용자가 지정한 시간이 지났으면 PTZ가 멈춘것이 확실함.
         if( !( m_nState & CamState_VideoAnalyticsToBeInitialized ) ) {
            m_nState |= CamState_VideoAnalyticsToBeInitialized; // 프리셋 이동이 완료된 경우
         }
      }
      m_csActivePresetName.Lock();
      if( m_pActivePreset ) {
         strcpy( m_szActivePresetName, m_pActivePreset->Name );
      } else {
         m_szActivePresetName[0] = 0;
      }
      m_csActivePresetName.Unlock();
      m_csActivePreset.Unlock();
   }

   return;
}

void CCamera::DoImgProcVideoAnalytics( const VC_PARAM_STREAM& param )
{
   CurFileTime = VC->GetTimeStamp();

   BOOL bVideoAnalytics = ( m_nState & CamState_DoVideoAnalytics ) ? TRUE : FALSE;
   if( !param.pYUY2Buff ) bVideoAnalytics = FALSE;
   if( m_nPTZState & PTZState_SPTZTrkBegun ) bVideoAnalytics = FALSE;
   if( IS_NOT_SUPPORT( __SUPPORT_VIDEO_ANALYTICS ) )
      bVideoAnalytics = FALSE;

   if( FALSE == ( m_nStateEx2 & CamStateEx2_VAModuleInitialized ) ) {
      bVideoAnalytics = FALSE;
   }

   //BOOL bVideoanalyticsOnPresetMoving = ( m_nState & CamState_VideoAnalyticsOnPresetMoving ) ? TRUE : FALSE;
   //BOOL bProcessOnColorImage = DoesVideoAnalyticsOnColorImageNeeded();

   if( IsSinglePTZTracking() ) {
      if( SPTZTrkState_TrackObjIntantlyLost & m_pSPTZTracker->GetState() ) {
         bVideoAnalytics = TRUE;
      }
   }

   int r_code = 0;

   if( ( m_nState & CamState_VideoAnalyticsToBeInitialized ) || ( m_nState & CamState_MainVACConfigToBeLoaded ) || ( m_nStateEx2 & CamStateEx2_DefVideoAnalyticsSetupToBeLoaded ) ) {
      if( m_nState & CamState_VideoAnalyticsOnPresetMoving || IsSinglePTZTracking() ) {
         m_nStateEx2 |= CamStateEx2_InitVAModuleExceptObjectTracker;
      }
      if( FALSE == ( m_nState & CamState_VideoAnalyticsOnPresetMoving ) ) {
         ResetVAModules();
      }
      UninitVAModules();
      if( m_nState & CamState_MainVACConfigToBeLoaded ) {
         LoadMainVACSetup();
         //logd ("[CH:%d] Main Config Loaded\n", ChannelNo + 1);
      } else if( m_nStateEx2 & CamStateEx2_DefVideoAnalyticsSetupToBeLoaded ) {
         LoadDefVACSetup();
         //logd ("[CH:%d] Default Config Loaded\n", ChannelNo + 1);
      } else if( m_nState & CamState_VideoAnalyticsOnPresetMoving ) {
         LoadEventDetectionSetupWhenPresetMovingVA();
         //logd ("[CH:%d] Load PresetMoving Config\n", ChannelNo + 1);
      }
      InitVAModules();
      if( ( m_nState & CamState_MainVACConfigToBeLoaded ) || ( m_nStateEx2 & CamStateEx2_DefVideoAnalyticsSetupToBeLoaded ) )
         SetActivePreset( NULL );

      if( m_nState & CamState_VideoAnalyticsToBeInitialized ) {
         m_nState &= ~CamState_VideoAnalyticsToBeInitialized;
      }
      m_nState &= ~( CamState_SuddenSceneChange | CamState_CreatingBKModel );
      m_nState &= ~CamState_MainVACConfigToBeLoaded;
      m_nState &= ~CamState_VideoAnalyticsToBeInitialized;
      m_nStateEx2 &= ~CamStateEx2_DefVideoAnalyticsSetupToBeLoaded;
      m_nStateEx2 &= ~CamStateEx2_InitVAModuleExceptObjectTracker;
   }

   TrackedObjectEx* t_object = NULL;

   if( bVideoAnalytics && m_bGetGrayScaleSrcImage && 
       (0 == PrevSuddenSceneChangeDetectionTime || GetTickCount () - PrevSuddenSceneChangeDetectionTime > 5000)) {

      m_mutex_processed_count.lock();
      m_processed_count++;
      m_mutex_processed_count.unlock();

      if( CropImage.Width ) {
         static IPoint2D p2d( 0, 0 );
         CropImage.Copy( SGImage, CropBox,  p2d );
         CS_ObjectTracker.Lock();
         for( int i = 0; i < 2; i++ ) {
            if( 0 == i ) t_object = dynamic_cast<TrackedObjectEx*>(ObjectTracker.TrackedObjects.First());
            if( 1 == i ) t_object = dynamic_cast<TrackedObjectEx*>(ObjectTracker.LostObjects.First());
            while( t_object != NULL ) {
               t_object->X -= CropBox.X;
               t_object->Y -= CropBox.Y;
               t_object = dynamic_cast<TrackedObjectEx*>(t_object->Next);
            }
         }
         CS_ObjectTracker.Unlock();
      }

      CS_ObjectTracker.Lock();
      // EventDetector.EvtGenObjects 목록에 속한 객체들 중에 TO_STATUS_TO_BE_REMOVED 상태에 있는 객체들을 제거한다.
      ObjectTracker.CleanTrackedObjectList();
      CS_ObjectTracker.Unlock();
      // EventDetector.EvtGenObjects  목록에 속한 객체들 중에 TO_STATUS_TO_BE_REMOVED 상태에 있는 객체들을 제거한다.
      CS_EventDetector.Lock();
      EventDetector.CleanEvtGenObjectList();
      CS_EventDetector.Unlock();

      GImage* pGrayImage = &SGImage;
      if( CropImage.Width ) pGrayImage = &CropImage;
      BGRImage* pColorImage = NULL;
      if( m_bGetColorSrcImageOK && !CropImage.Width ) pColorImage = &SCImage;

      if( IsSinglePTZTracking() || ( m_nState & CamState_VideoAnalyticsOnPresetMoving ) )
         UpdateVAAreaMaps();

      CS_ForegroundDetector.Lock();
      CPerformanceCounter pc1;
      pc1.CheckTimeStart();
      ISize2D srcImgSize( param.nWidth, param.nHeight );
      int r_code       = frg_detector->Perform( *pGrayImage, pColorImage, param.pYUY2Buff, &srcImgSize, &ObjectTracker, &RealObjectSizeEstimator );
      double diff1 = double(pc1.GetElapsedTime());
      CS_ForegroundDetector.Unlock();

      static int prev_r_code = r_code;

      switch( r_code ) {
      case FGD_RCODE_ERROR: {
         logd( "[ERROR] An error occurred in ForegroundDetector!    UID [%d]\n", UID );
      } break;
      case FGD_RCODE_NORMAL: {
         m_nState &= ~CamState_CreatingBKModel;

         CS_ObjectTracker.Lock();
         // 기본 비디오 분석을 수행한다.
         ObjectTracker.Perform( *frg_detector );
#if defined( __ENABLE_CAMERA_AUTO_CALIBRATION )
         CameraCalibrator.Perform( ObjectTracker );
#endif
         if( VAMode == VAE_MODE_FACE ) {
            ObjectTracker.UpdateFaceRegions();
            CPerformanceCounter pc2;
            pc2.CheckTimeStart();
#if defined( __LIB_SEETA )
            FaceLandmarkDetector.Perform( *pGrayImage, ObjectTracker );
#endif
         }
         if( VAMode == VAE_MODE_FACE || VAMode == VAE_MODE_OBJECT ) {
            FrontalFaceVerifier.Perform( ObjectTracker );
         }
         RealObjectSizeEstimator.Perform( ObjectTracker );
         //if( Config_FaceDetection.m_nObjectAnalysisMode & ObjectAnalysisMode_DNNObjectClassification )
         if(ObjectAnalysisMode_DNN_ == TRUE)
            dynamic_cast<ObjectClassification_DNN*>(ObjectClassifier)->Perform( ObjectTracker );
         else
            dynamic_cast<ObjectClassification_Basic*>(ObjectClassifier)->Perform( ObjectTracker );
         if( Config_FaceDetection.m_nObjectAnalysisMode & ObjectAnalysisMode_PersonAttribute )
            PersonAttrRecognizer.Perform( ObjectTracker );
         if( Config_FaceDetection.m_nObjectAnalysisMode & ObjectAnalysisMode_VehicleAttribute )
            VehicleTypeRecognizer.Perform( ObjectTracker );
         ObjectFilter.Perform( ObjectTracker );
         CS_ObjectTracker.Unlock();

         // 확장 비디오 분석(전경 검출이 필요한 경우)을 수행한다.
         ColorChangeDetector.Perform( ObjectTracker );
         CrowdDensityEstimator.Perform( *frg_detector );
         FlameDetector.Perform( ObjectTracker );
         GroupTracker.Perform( ObjectTracker );
         SmokeDetector.Perform( ObjectTracker );
         TrafficCongestionDetector.Perform( ObjectTracker );
         // 배경 모델 업데이트 계획을 획득한다.
         ObjectTracker.GetBkgModelUpdateScheme( frg_detector->GetBkgModelUpdateMode(), BMUSImage );
         // [주의] 처음에 호출해야 한다.
         ColorChangeDetector.GetBkgModelUpdateScheme( BMUSImage );
         FlameDetector.GetBkgModelUpdateScheme( BMUSImage );
         SmokeDetector.GetBkgModelUpdateScheme( BMUSImage );
         EventDetector.GetBkgModelUpdateScheme( BMUSImage );
         // [주의] 마지막에 호출해야 한다.
         // 배경 모델을 업데이트한다.
         CS_ForegroundDetector.Lock();
         frg_detector->UpdateBkgModel( BMUSImage );
         CS_ForegroundDetector.Unlock();

         m_nState &= ~CamState_SuddenSceneChange;
         break;
      }
      case FGD_RCODE_INITIAL_BACKGROUND_LEARNING: // 배경 모델을 생성하고 있는 경우.
      {
         BMUSImage.Clear();
         if( prev_r_code != r_code )
            logi( "FGD_RCODE_INITIAL_BACKGROUND_LEARNING\n" );

         m_nState |= CamState_CreatingBKModel; // 메시지 출력용
         CS_ObjectTracker.Lock();
         frg_detector->UpdateBkgModel( BMUSImage );
         // 현재 FGD_RCODE_RESET을 타지 않아 강제로 처리 (mkjang-160420)
         if( VAE_MODE_FACE == VAMode )
            InvalidateAllObjectsAndEvents();
         CS_ObjectTracker.Unlock();
         break;
      }
      case FGD_RCODE_SUDDEN_SCENE_CHANGE: // 갑작스러운 장면 변화를 감지한 경우
      {
         logi( "[CH:%d] FGD_RCODE_SUDDEN_SCENE_CHANGE\n", ChannelNo + 1 );
         if( VC->IsAllSignalOK() ) {
            m_nState |= CamState_SuddenSceneChange; // 메시지 출력용
         }
         if( !( m_nState & CamState_VideoAnalyticsToBeInitialized ) ) {
            m_nState |= CamState_VideoAnalyticsToBeInitialized;
         }
         PrevSuddenSceneChangeDetectionTime = GetTickCount();      
         break;
      }
      case FGD_RCODE_RESET: // 배경 모델의 재초기화가 필요한 경우
         logi( "[CH:%d] FGD_RCODE_RESET\n", ChannelNo + 1 );
         InvalidateAllObjectsAndEvents();
         ColorChangeDetector.Reset();
         FlameDetector.Reset();
         GroupTracker.Reset();
         SmokeDetector.Reset();
         break;
      default:
         m_nState &= ~CamState_SuddenSceneChange; // 이벤트 저장용
         break;
      }
      prev_r_code = r_code;

      CS_EventDetector.Lock();
      if( !IsSinglePTZTracking() ) {
         // 확장 비디오 분석(전경 검출이 필요 없는 경우을 수행한다.
         WaterLevelDetector.Perform( SGImage, SCImage );
         ZoneColorDetector.Perform( SCImage );
         // 이벤트 검출을 수행한다.
         EventDetector.Perform( ObjectTracker );
         EventDetector.Perform( ColorChangeDetector );
         EventDetector.Perform( CrowdDensityEstimator );
         EventDetector.Perform( FlameDetector );
         EventDetector.Perform( GroupTracker );
         EventDetector.Perform( SmokeDetector );
         EventDetector.Perform( TrafficCongestionDetector );
         EventDetector.Perform( WaterLevelDetector );
         EventDetector.Perform( ZoneColorDetector );
         DwellAnalyzer.Perform( ObjectTracker );
         // [주의] DwellAnalyzer.Perform(ObjectTracker)은 반드시 EventDetector.Perform(ObjectTracker) 보다 뒤에 호출되어야 한다.
         //        DwellAnalyzer.Perform(ObjectTracker)가 EventDetector.Perform(ObjectTracker)의 실행 결과를 이용하기 때문이다.
         EventDetector.Perform( DwellAnalyzer );
      }

      if( CropImage.Width ) {
         for( int i = 0; i < 2; i++ ) {
            if( 0 == i ) t_object = dynamic_cast<TrackedObjectEx*>(ObjectTracker.TrackedObjects.First());
            if( 1 == i ) t_object = dynamic_cast<TrackedObjectEx*>(ObjectTracker.LostObjects.First());
            while( t_object != NULL ) {
               t_object->X += CropBox.X;
               t_object->Y += CropBox.Y;
               t_object = (TrackedObjectEx*)t_object->Next;
            }
         }
      }

      // 객체/이벤트 썸네일을 획득한다.
      if( YUY2SrcImg.Length ) {
         ISize2D tn_size( MDBThumbnailSize.cx, MDBThumbnailSize.cy );
         if( CropImage.Width ) {
            ObjectTracker.SrcImgSize = ProcImgSize;
            EventDetector.SrcImgSize = ProcImgSize;
         }
         ObjectTracker.UpdateObjectThumbnails( YUY2SrcImg, SrcImgSize, VAE_PIXFMT_YUY2, 1.5f, &tn_size ); // ObjectTracker.TrackedObjects 및 ObjectTracker.LostObjects에 대한 객체 썸네일 획득
         EventDetector.UpdateEventThumbnails( YUY2SrcImg, SrcImgSize, VAE_PIXFMT_YUY2, 1.5f, &tn_size ); // EventDetector.EvtGenObjects에 대한 이벤트 썸네일 획득
         EventDetector.UpdateEventThumbnails( ObjectTracker, YUY2SrcImg, SrcImgSize, VAE_PIXFMT_YUY2, 1.5f, &tn_size ); // ObjectTracker.TrackedObjects 및 ObjectTracker.LostObjects에 대한 이벤트 썸네일 획득

         if( CropImage.Width ) {
            EventDetector.SrcImgSize( CropImage.Width, CropImage.Height );
            ObjectTracker.SrcImgSize( CropImage.Width, CropImage.Height );
         }
      }
      CS_EventDetector.Unlock();

      // =============== FINISH VA =================

      // yhpark. monitoring.
      //uint va_elapsed_time = uint( time_took_for_va.GetElapsedTime() );
      //LOGD << interval_checker << " took[" << va_elapsed_time << "] seq:" << m_va_sequence++;
   }


   // 객체/이벤트 썸네일을 획득한다.
   if( bVideoAnalytics && YUY2SrcImg.Length ) {
      ObjectTracker.UpdateObjectThumbnails( YUY2SrcImg, SrcImgSize, VAE_PIXFMT_YUY2 ); // ObjectTracker.TrackedObjects 및 ObjectTracker.LostObjects에 대한 객체 썸네일 획득
      EventDetector.UpdateEventThumbnails( YUY2SrcImg, SrcImgSize, VAE_PIXFMT_YUY2 ); // EventDetector.EvtGenObjects에 대한 이벤트 썸네일 획득
      EventDetector.UpdateEventThumbnails( ObjectTracker, YUY2SrcImg, SrcImgSize, VAE_PIXFMT_YUY2, 1.5f ); // ObjectTracker.TrackedObjects 및 ObjectTracker.LostObjects에 대한 이벤트 썸네일 획득
   }

   CS_ObjectTracker.Lock();
   t_object = dynamic_cast<TrackedObjectEx*>(ObjectTracker.TrackedObjects.First());
   while( t_object != NULL ) {
      EventEx* event = dynamic_cast<EventEx*>(t_object->Events.First());
      while( event != NULL ) {
         if( event->Status & ( EV_STATUS_BEGUN ) ) {
            AfxGetLocalFileTime (&event->Time_EventBegun);
         }
         event = dynamic_cast<EventEx*>(t_object->Events.Next( event ));
      }
      t_object = dynamic_cast<TrackedObjectEx*>(t_object->Next);
   }
   CS_ObjectTracker.Unlock();

   ProcessOnTrackedObjectsAndEvents();

   if( bVideoAnalytics ) {
      if( r_code == FGD_RCODE_RESET ) {
         ResetVAModules();
         if( !( m_nState & CamState_VideoAnalyticsToBeInitialized ) ) {
            m_nState |= CamState_VideoAnalyticsToBeInitialized;
            //logi( "[CH:%d] CamState_VideoAnalyticsToBeInitialized - FGD_RCODE_RESET\n", ChannelNo + 1 );
         }
      }
   }

#if defined( __WIN32 )
   CS_AuxVideoDisplay.lock();
   if( AuxVideoDisplayWnd && Options & CamOpt_PausingVideoAnalytics ) {
      AuxVideoDisplayWnd->ImageView->Clear();
      AuxVideoDisplayWnd->ImageView->Invalidate( FALSE );
   }
   CS_AuxVideoDisplay.unlock();
#endif
}

void CCamera::DoImageProcess( const VC_PARAM_STREAM& param )
{
   CCriticalSectionSP co( CS_DoImageProcess );
   // 비디오 스트림의 속성이 변경되는 경우에는 영상 처리 카운트가 0부터 다시 시작된다.
   if( 0 == param.nCount ) {
      if( !( m_nState & CamState_VideoAnalyticsToBeInitialized ) ) {
         m_nState |= CamState_VideoAnalyticsToBeInitialized;
      }
      InitSrcAndProcImgSize();
      DetermineVideoAnalyticsStateOnOff(); // 비디오 분석 수행 여부 판별함수
   }

   m_nDoImgProcPos = 100;

   AfxGetLocalFileTime( &m_ftLastDoImageProcessTime );

   // 초기 영상 접속 시 분석영상의 크기를 결정한다.
   if( FALSE == m_bReductionFactorChecked && VC->IsAllSignalOK() ) {
      BOOL bReductionFactorHasChanged = FALSE;
      if( VC->m_nWidth >= 200 && ReductionFactor < 1.0f ) {
         ReductionFactor             = 1.0f;
         bReductionFactorHasChanged = TRUE;
      } else if( VC->m_nWidth >= 1270 && ReductionFactor > 0.5f ) {
         ReductionFactor            = 0.5f;
         bReductionFactorHasChanged = TRUE;
      } else if( VC->m_nWidth >= 1920 && ReductionFactor != 0.3f ) {
         ReductionFactor            = 0.3f;
         bReductionFactorHasChanged = TRUE;
      }
      if( bReductionFactorHasChanged ) {
         if( SGImage.Width > 0 ) {
            m_nState |= CamState_ToBeRestarted;
            dynamic_cast<CLocalSystem*>(System)->SetSystemSetupToBeSaved( 30000 );
         }
      }
      m_bReductionFactorChecked = TRUE;
   }

   if( m_nState & ( CamState_ToBeRestarted | CamState_ToBeClosed | CamState_ToBeDeactivate ) )
      return;
   if( System->m_nGlobalState & GlobalState_ClosingAllSystems )
      return;

   if( m_nStateEx2 & CamStateEx2_PausingVideoAnalyticsEnded ) {
      InitVAModules();
      m_nStateEx2 &= ~CamStateEx2_PausingVideoAnalyticsEnded;
   }

   // 중요 : 위의 VAC모듈 초기화 관련 호출 이후에 DetermineVideoAnalyticsStateOnOff 함수가 호출되어야 합니다.
   DetermineVideoAnalyticsStateOnOff(); // 아래의 IsVideoAnalyticsVideoServer 호출보다 먼저 수행되어야 하는 함수임.
   GetSourceImage( param.pYUY2Buff, param.nWidth, param.nHeight );

   //LOGI << "VA Image Source size [ " << SGImage.Width << " x " << SGImage.Height << " ]";

   BOOL bPTZCtrlReady = FALSE;
   if( VC->m_pPTZCtrl && VC->m_pPTZCtrl->IsPTZCtrlReady() ) {
      bPTZCtrlReady = TRUE;
      m_nPTZState |= PTZState_PTZControlReady;
   } else {
      m_nPTZState &= ~PTZState_PTZControlReady;
   }

   DoImgProcVideoAnalytics( param );

   if( bPTZCtrlReady ) {
      DoImgProcDispatchPTZMsg( param );
      DoImgProcPresetTouring( param );
      DoImgProcSinglePTZTrack( param );
   }

   m_nPTZState &= ~( PTZState_GotoAbsPosBegun );

   m_nPrvState = m_nState;
   if( bPTZCtrlReady ) {
      m_nPrvPTZState = m_nPTZState;
   }
   
   ImgProcCount++;
   if (0)
   if (!m_nPrevImgProcCountCheckTime || GetTickCount() - m_nPrevImgProcCountCheckTime > 999) {
      logi( "[CH:%02d] ImgProc::FrameCount=%d", ChannelNo + 1, ImgProcCount);
      ImgProcCount = 0;
      m_nPrevImgProcCountCheckTime = GetTickCount();
   }
   
}

BOOL CCamera::OnVCCameraEvent( const VC_PARAM_CAMERA_EVENT& param )
{
   // Camera Event는 이벤트에 대응되는 처리 이후 반드시 함수를 리턴해야함.
   if( VCCamEvent_PTZCtrlByServerReleased == param.nCamEvent ) {
      if( !( Options & CamOpt_AutoPTZCtrlMode ) )
         SetAutoPTZCtrlMode( TRUE, TRUE, TRUE );
      return TRUE;
   }
   if( VCCamEvent_PTZCtrlByServer == param.nCamEvent ) {
      if( Options & CamOpt_AutoPTZCtrlMode )
         SetAutoPTZCtrlMode( FALSE, TRUE, TRUE );
      return TRUE;
   }

   switch( param.nCamEvent ) {
   case VCCamEvent_VideoSizeIsChanged_Bigger:
   {
      LOGE << "VCCamEvent_VideoSizeIsChanged_Bigger";
      m_pDoc->SetExitProcess( Camera_Status_Decoded_Video_Size_Is_Bigger );
      break;
   }
   case VCCamEvent_VideoSizeIsChanged:
   {
      //CCamera_IP* ipcamera = VC->GetIPCameraByStreamIdx(0);
      //if (ipcamera) {
      //   if ((VC->m_IPCamInfo.m_nWidth  < param.PrevImgSize.Width) ||
      //       (VC->m_IPCamInfo.m_nHeight < param.PrevImgSize.Height)) {
      //      m_pDoc->SetExitProcess( Camera_Status_Decoded_Video_Size_Is_Bigger );
      //   }
      //}
      break;
   }
   case VCCamEvent_AuthorizationFailed:
      m_pDoc->SetExitProcess( Camera_Status_Authorization_Failed );
      break;
   case VCCamEvent_VideoSignalNotOK:
      m_pDoc->SetExitProcess( Camera_Status_No_Signal );
      break;
   default:
      break;
   }

   NoVideoSignalChannels       = ushort(VC->GetNoVideoSignalFlag());
   ConnectingChannels          = ushort(VC->GetConnectingChannelFlag());
   AuthorizationFailedChannels = ushort(VC->GetAuthorizationFailedChannelFlag());

   return TRUE;
}

BOOL CCamera::OnVCConfigToBeLoaded()
{
   if( TmpCamera ) {
      OriginToTemp( TmpCamera, IVXSetupItemMask_VideoInput );
   }
   return 0;
}

BOOL CCamera::CallbackImageProcess( const VC_PARAM_STREAM& param )
{
   CCamera* pCamera = (CCamera*)param.pParam;
   pCamera->DoImageProcess( param );
   return TRUE;
}

BOOL CCamera::CallbackDisplay( const VC_PARAM_STREAM& param )
{
   CCamera* pCamera = static_cast<CCamera*>(param.pParam);
   if( 1000 < GetTickCount() - pCamera->m_nDisplayLog && pCamera->m_nDisplayLogCnt < 5 ) {
      //logd( "Display [Ch = %d] [UID = %d] [%d x %d]\n", pCamera->ChannelNo, pCamera->UID, param.nWidth, param.nHeight );
      pCamera->m_nDisplayLog = GetTickCount();
      pCamera->m_nDisplayLogCnt++;
      
      //LOGI_( L ) << LGU::Logger::header( 0 ) ;
   }

   return TRUE;
}

void CCamera::CallbackCameraEvent( const VC_PARAM_CAMERA_EVENT& param )
{
   CCamera* pCamera = static_cast<CCamera*>(param.pParam);
   pCamera->OnVCCameraEvent( param );
}

BOOL CCamera::CallbackVCConfigToBeLoad( LPVOID pParam )
{
   CCamera* pCamera = static_cast<CCamera*>(pParam);
   return pCamera->OnVCConfigToBeLoaded();
}

BOOL CCamera::OnEncodeVideoFrame( const VC_PARAM_ENCODE& param )
{
   if( !( m_nState & CamState_Activate ) ) return FALSE;
   if( m_nState & CamState_Deactivating ) return FALSE;
   if( m_nState & CamState_ToBeClosed ) return FALSE;

   m_nVideoFrameFlag = param.dwFlag;

   // [2013-11-01] jun : 콜백함수내에서 직접처리하는 경우 문제가 발생할 소지가 있어 Queue 처리를 하였다.
   CCompressImageBufferItem* pNewItem = new CCompressImageBufferItem;
   pNewItem->m_CompBuff.Create( param.nCompBuffSize );
   pNewItem->m_nCompBuffSize    = param.nCompBuffSize;
   pNewItem->m_dwCommFlag       = param.dwFlag;
   pNewItem->m_nCodecID         = param.nCodecID;
   pNewItem->m_PresentationTime = param.PresentationTime;
   CopyMemory( pNewItem->m_CompBuff, param.pCompBuff, param.nCompBuffSize );

   ASSERT( 0 <= param.nStreamIdx && param.nStreamIdx < MAX_IP_CAMERA_STREAM_NUM );

   m_EncodedVideoStramQueue[param.nStreamIdx].Push( pNewItem );
   m_evtDoProcessOnVideoFrame.SetEvent();

   return TRUE;
}

BOOL CCamera::CallbackEncodeVideoFrame( const VC_PARAM_ENCODE& param )
{
   CCamera* pCamera = static_cast<CCamera*>(param.pParam);
   pCamera->OnEncodeVideoFrame( param );

   return TRUE;
}

BOOL CCamera::CallbackDecodeVideoFrame( const VC_PARAM_STREAM& param )
{
   CCamera* pCamera = static_cast<CCamera*>(param.pParam);

   return TRUE;
}

int CCamera::ReadVCSetup( CXMLIO* pIO, int nRWFlag )
{
   if( FALSE == VC->Read( pIO, nRWFlag ) )
      return 1;
   // 매우 중요: 현재 비디오 채널 아이디를 읽은 값으로 변경.
   //            비디오 채널(CVideoChanel) 객체는 CCamera 생성자에서 생성되며 이 때 VCID가 정해진다.
   //            그런데 설정을 로드하면 CVideoChannel의 m_nVCID가 변경되기 때문에 설정을 로드한 후에는 VCID를 갱신해야한다.
   VCID = VC->m_nVCID;
   return DONE;
}

int CCamera::WriteVCSetup( CXMLIO* pIO, int nRWFlag )
{
   VC->Write( pIO, nRWFlag );
   return DONE;
}

/////////////////////////////////////////////////////////////////////////////////////////
// 실시간 비디오 전송
/////////////////////////////////////////////////////////////////////////////////////////

BOOL CCamera::IsStartVideoStreamingThread( int nStreamIdx )
{
   if( m_bStartVideoStreamThread[nStreamIdx] )
      return TRUE;
   return FALSE;
}

void CCamera::StartVideoStreamingThread( int nStreamIdx )
{
   if( !( m_nState & CamState_Activate ) ) return;
   if( m_nState & CamState_Deactivating ) return;
   if( m_nState & CamState_ToBeClosed ) return;
   if( VC && ( VideoChannelType_Unused == VC->m_nVideoChannelType ) )
      return;
   // 이 함수는 두개 이상의 스레드에서 동시에 접근 할 수 있기 때문에 임계영역 처리를 하였음.
   CCriticalSectionSP co( m_csVideoStreamStartStop );

   if( m_bStartVideoStreamThread[nStreamIdx] ) return;

   if( !m_TimerVideoStreaming[nStreamIdx] )
      m_TimerVideoStreaming[nStreamIdx] = System->m_pTimerMgr->GetNewTimer( "CameraMgrProc StartVideoStreamingThread" );
   m_LiveSnderMC[nStreamIdx].m_pCamera = this;
   m_LiveSnderMC[nStreamIdx].Initialize( nStreamIdx );

   std::shared_ptr<VideoStreamingThreadContext> pContext = std::make_shared<VideoStreamingThreadContext>();
   pContext->m_nStreamIdx                                = nStreamIdx;
   pContext->m_pCamera                                   = this;
   m_hVideoStreamingThread[nStreamIdx]                   = std::thread( ThreadFuction_VideoStreaming, pContext );
   m_bStartVideoStreamThread[nStreamIdx]                 = TRUE;
}

void CCamera::StopVideoStreamingThread( int nStreamIdx )
{
   // 이 함수는 두개 이상의 스레드에서 동시에 접근 할 수 있기 때문에 임계영역 처리를 하였음.
   CCriticalSectionSP co( m_csVideoStreamStartStop );

   if( !m_bStartVideoStreamThread[nStreamIdx] ) return;

   m_bStopVideoStreamingThread[nStreamIdx] = TRUE;
   m_LiveSnderMC[nStreamIdx].SetStopSend();
   if( m_TimerVideoStreaming[nStreamIdx] )
      m_TimerVideoStreaming[nStreamIdx]->GetTimerEvent().SetEvent();
   if( m_hVideoStreamingThread[nStreamIdx].joinable() )
      m_hVideoStreamingThread[nStreamIdx].join();
   m_bStopVideoStreamingThread[nStreamIdx] = FALSE;

   m_LiveSnderMC[nStreamIdx].Close();
   if( m_TimerVideoStreaming[nStreamIdx] ) {
      System->m_pTimerMgr->DeleteTimer( m_TimerVideoStreaming[nStreamIdx]->m_nTimerID );
      m_TimerVideoStreaming[nStreamIdx] = NULL;
   }
   m_bStartVideoStreamThread[nStreamIdx] = FALSE;
}

float CCamera::GetVideoStreamingFrameRate( int nStreamingIdx )
{
   float fFrameRate = 0.0F;

   CVideoStreamProfile* pVideoStreamProfile = Config_VS.GetVideoStreamProfile( nStreamingIdx );
   if( pVideoStreamProfile ) {
      fFrameRate = VC->m_fFPS_Cap;
      if( VC->IsSupportDirectVideoStreaming() ) {
         if( FALSE == pVideoStreamProfile->m_bDirectStreaming )
            fFrameRate = pVideoStreamProfile->m_fVideoFrameRate;
         else
            fFrameRate = VC->GetVideoFrameRate( pVideoStreamProfile->m_nVideoServerStreamIdx );
      }
   }
   return ( fFrameRate );
}

UINT CCamera::VideoStreamingProc( int nStreamIdx )
{
   //logd ("[CH:%d] VideoStreamingProc(%d) - Start\n", ChannelNo+1, nStreamIdx);
   // 각 스트림 별로 비디오 전송여부를 체크하는 기능은 CLocalSystem::DetermineStartStopVideoStreamingThread 함수에서 수행된다.

   int i;

   CVideoStreamProfile* pVideoStreamProfile = Config_VS.GetVideoStreamProfile( nStreamIdx );
   CMMTimer* pTimer                         = m_TimerVideoStreaming[nStreamIdx];
   CVideoSnder* pVideoSender                = &m_LiveSnderMC[nStreamIdx];
   pTimer->StartTimer();
   pTimer->SetFPS( float(pVideoStreamProfile->m_fVideoFrameRate) );

   for( i = 0;; i++ ) {
      pVideoStreamProfile = Config_VS.GetVideoStreamProfile( nStreamIdx );
      ASSERT( pVideoStreamProfile );

      if( VC->IsSupportDirectVideoStreaming( pVideoStreamProfile->m_nVideoServerStreamIdx ) && pVideoStreamProfile->m_bDirectStreaming )
         pVideoSender->m_bDirectStreaming = TRUE;
      else
         pVideoSender->m_bDirectStreaming = FALSE;

      if( m_bStopVideoStreamingThread[nStreamIdx] )
         break;

      if( m_bSendVideo[nStreamIdx] ) // m_bSendVideo 값은 CLocalSystem::DetermineStartStopVideoStreamingThread 함수에서 결정
      {
         FILETIME ftTimestamp;
         VC->GetImage_Timestamp( ftTimestamp ); // 타임스탬프

         BYTE* pYUY2Buff;
         int nVideoServerStreamIndex = 0;
         int nImgWidth = 0, nImgHeight = 0;

         if( VideoChannelType_IPCamera == VC->m_nVideoChannelType ) {
            nVideoServerStreamIndex = pVideoStreamProfile->m_nVideoServerStreamIdx;
            //int nIPCameraType       = VC->m_IPCamInfoArr[nVideoServerStreamIndex].m_nIPCameraType;
         }
         VC->GetVideoBuff( pVideoStreamProfile->m_nVideoServerStreamIdx, &pYUY2Buff, nImgWidth, nImgHeight );
         if( !nImgWidth ) {
            std::vector<int> vtAvailableStreamIdxs;
            VC->GetAvailableStreamIndex( vtAvailableStreamIdxs );
            if( vtAvailableStreamIdxs.size() )
               nVideoServerStreamIndex = vtAvailableStreamIdxs[0];
            pVideoStreamProfile->m_nVideoServerStreamIdx = nVideoServerStreamIndex;
         }

         CVideoFrame videoFrame;
         videoFrame.Create( nImgWidth, nImgHeight, VideoSrcType_Live );
         ExtractVideoFrameInfo( videoFrame.m_VideoFrameInfo, nImgWidth, nImgHeight, TRUE );

         if( pYUY2Buff ) {
            videoFrame.SetImage( pYUY2Buff, videoFrame.GetWidth(), videoFrame.GetHeight(), TRUE );
            videoFrame.m_ftTimestamp = ftTimestamp; // 타임스탬프
            videoFrame.CopyTrackedObjects( m_VideoFrameLive.m_VideoFrameInfo );

            // MS: 각 스트림별 비디오 신호 체크를 해야 한다.
            if( videoFrame.GetWidth() > 0 && IsVideoSignalOk() ) {
               pVideoSender->SendVideo( &videoFrame, pVideoStreamProfile, Config_VS.m_bUseHWEncoder );
            }
         }
      }

      float fFrameRate = pVideoStreamProfile->m_fVideoFrameRate;
      if( pVideoSender->m_bDirectStreaming )
         fFrameRate = 60.0f;
      pTimer->SetFPS( fFrameRate );
      pTimer->Wait();
   }
   pTimer->StopTimer();
   pVideoSender->Close();

   //logd ("[CH:%d] VideoStreamingProc(%d) - Ended\n", ChannelNo+1, nStreamIdx);
   return ( DONE );
}

UINT CCamera::ThreadFuction_VideoStreaming( std::shared_ptr<VideoStreamingThreadContext> pContext )
{
   if( pContext ) {
      CCamera* pCamera = (CCamera*)pContext->m_pCamera;
      if( pCamera ) {
         pCamera->VideoStreamingProc( pContext->m_nStreamIdx );
      }
   }
   return ( DONE );
}

void CCamera::LoadHandoffVACSetup()
// 본 함수가 호출된 후에는 반드시 비디오분석 모듈을 초기화 시켜야 한다.
{
   // 현재 VACL 클래스를 디폴트 VACL 설정값으로 동작시킨다.
   // 단 헨드오프 추적과 관련된 설정만을 복사한다.
   // 기본 VAC설정의 이벤트 존 정보등은 복사해서는 안된다.
   EventDetection ed;
   ObjectClassification_Common oc;
   ObjectFilteringEx of;
   ObjectTracking ot;
   RealObjectSizeEstimation ros;

   FileIO io_vac;
   CXMLIO xmlWriteIO( &io_vac, XMLIO_Write );

   CXMLNode xmlNode( &xmlWriteIO );
   if( xmlNode.Start( "VAC" ) ) {
      ed.WriteFile( &xmlWriteIO );
      MainConfig_ForegroundDetection.WriteFile( &xmlWriteIO );
      //oc.WriteFile(&xmlWriteIO);
      of.WriteFile( &xmlWriteIO );
      ot.WriteFile( &xmlWriteIO );
      ros.WriteFile( &xmlWriteIO );
      xmlNode.End();
   }

   io_vac.GoToStartPos();
   CXMLIO xmlReadIO( &io_vac, XMLIO_Read );
   ReadVACSetup( &xmlReadIO );
   ObjectTracker.MinMovingDistance = MainConfig_ObjectTracking.MinMovingDistance;
   ObjectTracker.MinTime_Tracked   = MainConfig_ObjectTracking.MinTime_Tracked;

   m_nStateEx2 |= CamStateEx2_ToBeUpdateVAAreas;
}

void CCamera::LoadDefVACSetup( int item )
{
   //logi( "[CH:%d] LoadDefVACSetup\n", ChannelNo + 1 );

   FileIO io_vac;
   CXMLIO xmlWriteIO( &io_vac, XMLIO_Write );
   CCriticalSectionSP co( m_csCameraSetup );
   WriteDefVACSetup( &xmlWriteIO, item ); // jun : 그동안 Item 인자가 빠져있었음.
   io_vac.GoToStartPos();
   CXMLIO xmlReadIO( &io_vac, XMLIO_Read );
   ReadVACSetup( &xmlReadIO, item );
   m_nCurVACSetupType = VACSetupType_Default;

   m_nStateEx2 |= CamStateEx2_ToBeUpdateVAAreas;
}

void CCamera::LoadMainVACSetup( int item )
// 현재 VACL 클래스를 디폴트 VACL 설정값으로 동작시킨다.
// 본 함수가 호출된 후에는 반드시 비디오분석 모듈을 초기화 시켜야 한다.
{
   //logi( "[CH:%d] LoadMainVACSetup", ChannelNo + 1 );

   CCriticalSectionSP co( m_csCameraSetup );
   FileIO io_vac;
   CXMLIO xmlWriteIO( &io_vac, XMLIO_Write );
   WriteMainVACSetup( &xmlWriteIO, item );
   io_vac.GoToStartPos();
   CXMLIO xmlReadIO( &io_vac, XMLIO_Read );

   ReadVACSetup( &xmlReadIO, item );
   m_nCurVACSetupType = VACSetupType_Main;

   m_nStateEx2 |= CamStateEx2_ToBeUpdateVAAreas;
}

void CCamera::LoadPresetVACSetup( CPreset* pPreset )
// 현재 VACL 클래스를 특정 프리셋의 VACL 설정값으로 동작시킨다.
// 본 함수가 호출된 후에는 반드시 비디오분석 모듈을 초기화 시켜야 한다.
{
   //logi( "[CH:%d] LoadPresetVACSetup PresetID:%d\n", ChannelNo + 1, pPreset->PresetID );

   CCriticalSectionSP co( m_csCameraSetup );
   FileIO io_vac;
   CXMLIO xmlWriteIO( &io_vac, XMLIO_Write );
   pPreset->WriteVACSetup( &xmlWriteIO );
   io_vac.GoToStartPos();
   CXMLIO xmlReadIO( &io_vac, XMLIO_Read );
   ReadVACSetup( &xmlReadIO );
   m_nCurVACSetupType = VACSetupType_Preset;

   m_nStateEx2 |= CamStateEx2_ToBeUpdateVAAreas;
}

void CCamera::LoadRealtimeEventCountInfoToMainVASetup()
{
   if( VACSetupType_Main == m_nCurVACSetupType ) {
      CCriticalSectionSP co( m_csCameraSetup );
      CS_EventDetector.Lock();
      FileIO io_vac;
      CXMLIO xmlWriteIO( &io_vac, XMLIO_Write );
      WriteEventCountInfo( &xmlWriteIO, &EventDetector );
      io_vac.GoToStartPos();
      CXMLIO xmlReadIO( &io_vac, XMLIO_Read );
      ReadEventCountInfo( &xmlReadIO, &MainConfig_EventDetection );
      CS_EventDetector.Unlock();
   }
}

void CCamera::LoadRealtimeEventCountInfoToPresetVASetup()
{
   if( VACSetupType_Preset == m_nCurVACSetupType ) {
      CS_EventDetector.Lock();
      CCriticalSectionSP co( m_csCameraSetup );
      FileIO io_vac;
      CXMLIO xmlWriteIO( &io_vac, XMLIO_Write );
      WriteEventCountInfo( &xmlWriteIO, &EventDetector );
      io_vac.GoToStartPos();
      CXMLIO xmlReadIO( &io_vac, XMLIO_Read );
      if( NULL != m_pActivePreset )
         ReadEventCountInfo( &xmlReadIO, &m_pActivePreset->Config_EventDetection );
      CS_EventDetector.Unlock();
   }
}

void CCamera::LoadRealtimeEventCountInfoToMainOrPresetVASetup()
{
   if( VACSetupType_Main == m_nCurVACSetupType ) {
      LoadRealtimeEventCountInfoToMainVASetup();
   } else if( VACSetupType_Preset == m_nCurVACSetupType ) {
      LoadRealtimeEventCountInfoToPresetVASetup();
   }
}

void CCamera::LoadEventDetectionSetupWhenPresetMovingVA()
{
   CS_EventDetector.Lock();

   EventDetector.ClearEventRuleList();
   EventDetector.ClearEventZoneList();

   EventZone* evt_zone = CreateInstance_EventZone();
   evt_zone->Create( 4 );
   evt_zone->Vertices[0]( 0, 0 );

   if(SrcImgSize.Width) {
      evt_zone->Vertices[1]( SrcImgSize.Width - 1, 0 );
      evt_zone->Vertices[2]( SrcImgSize.Width - 1, SrcImgSize.Height - 1 );
      evt_zone->Vertices[3]( 0, SrcImgSize.Height - 1 );
   }
   else {
      evt_zone->Vertices[1]( REF_IMAGE_WIDTH - 1, 0 );
      evt_zone->Vertices[2]( REF_IMAGE_WIDTH - 1, REF_IMAGE_HEIGHT - 1 );
      evt_zone->Vertices[3]( 0, REF_IMAGE_HEIGHT - 1 );
   }

   EventDetector.EventZones.Add( evt_zone );
   EventRule* evt_rule = CreateInstance_EventRule();
   evt_rule->Options |= ER_OPTION_ENABLE_COUNTER;
   evt_rule->EventType = ER_EVENT_LOITERING;
   EventDetector.EventRules.Add( evt_rule );

   evt_zone->EvtRule = evt_rule;

   CS_EventDetector.Unlock();
}

void CCamera::SetActivePreset( BOOL bExistActivePreset, int nGroupIndex, int nPresetIndex )
{
   m_csActivePreset.Lock();
   if( bExistActivePreset )
      m_pActivePreset = m_PresetGroupList.FindPreset( nGroupIndex, nPresetIndex );
   else
      m_pActivePreset = NULL;
   m_PresetGroupList.SelectedGroupIndex = nGroupIndex;
   if( m_pActivePreset )
      m_pActivePresetGroup = m_PresetGroupList.FindPresetGroup( m_pActivePreset->PresetNo );
   else
      m_pActivePresetGroup = NULL;

   m_csActivePresetName.Lock();
   if( m_pActivePreset ) {
      strcpy( m_szActivePresetName, m_pActivePreset->Name );
   } else {
      m_szActivePresetName[0] = 0;
   }
   m_csActivePresetName.Unlock();

   m_csActivePreset.Unlock();
}

void CCamera::SetActivePreset( CPreset* pPreset )
{
   m_csActivePreset.Lock();
   m_pActivePreset = pPreset;
   m_csActivePreset.Unlock();
}

BOOL CCamera::IsPanoramaCamera()
{
   ASSERT( VC );
   if( VC->m_nVideoChannelType == VideoChannelType_Panorama )
      return TRUE;

   return FALSE;
}

BOOL CCamera::IsVideoFileCamera()
{
   ASSERT( VC );

   if( VC->m_nVideoChannelType == VideoChannelType_VideoFile )
      return TRUE;

   return FALSE;
}

BOOL CCamera::GetPTSpeed( BOOL bSetOn, float& pt_speed )
{
   if( !IsPTZCamera() || !m_pPTZCtrl ) return FALSE;
   // 중요: 본 함수에서 연결체크는 안함. --> SRS-IVXNetLib와 관련있음
   //if(!m_pPTZCtrl->IsPTZConnected (   )) return FALSE;
   if( bSetOn )
      pt_speed = ceil( float( Config_PTZC.PanTiltSpeed ) / 5.0f ) / 90.0f;
   else
      pt_speed = float( Config_PTZC.PanTiltSpeed ) / 90.0f;

   return TRUE;
}

BOOL CCamera::RestartCamera()
{
   logd( "[CH:%d] Restart Camera ...\n", ChannelNo + 1 );

#ifdef __UPLUS_LOGGER
   LGU::Logger::SetHeaderItem( COMMAND, "RESTART");
   LGU::Logger::SetHeaderItem( FROM_SERVICE_NAME, "VAP" );
   LGU::Logger::SetHeaderItem( RES_SERVICE_NAME, "VAM" );
   LGU::Logger::SetHeaderItem( VA_PROCESS_CALL_TIME );
   LOGI_(L) << LGU::Logger::header (1);
#endif
   
   m_nStateEx2 |= CamStateEx2_VAModuleInitialized;
   m_nState |= CamState_ToBeRestarted;
   System->m_evtRestartCamera.SetEvent();

#ifdef __SUPPORT_FISHEYE_CAMERA
   if( VC->IsFisheyeCamera() )
      VC->m_bFisheyeInit = FALSE;
#endif

   return TRUE;
}

BOOL CCamera::ResetAllObjectCounters()
{
   std::vector<CPreset*> vtAllPreset;
   // vtAllPreset 리스의 첫항목은 디폴트VACL 설정을 로드하기 위해 아래와같이 NULL 값을 푸시한다.
   vtAllPreset.push_back( NULL );

   // 현재 선택된 그룹의 모든 프리셋을 리스트에 추가한다.
   CPresetGroup* c_group = m_PresetGroupList.GetSelectedGroup();
   if( c_group && c_group->GetNumItems() > 0 ) {
      CPreset* pPreset = c_group->First();
      while( pPreset ) {
         vtAllPreset.push_back( pPreset );
         pPreset = pPreset->Next;
      }
   }

   EventDetector.ResetObjectCounters();

   EventDetection* pEventDetector;
   int nItemNum = vtAllPreset.size();
   for( int i = 0; i < nItemNum; i++ ) {
      CPreset* pPreset = vtAllPreset[i];

      if( pPreset == NULL )
         pEventDetector = &MainConfig_EventDetection; // 기본 VACL 설정 (preset 별 vac 설정이 아닌) 인 경우.
      else
         pEventDetector = &pPreset->Config_EventDetection;

      EventRuleEx* pEventRuleEx = (EventRuleEx*)pEventDetector->EventRules.First();
      while( pEventRuleEx ) {
         pEventRuleEx->ERC_Counting.Count = 0;
         pEventRuleEx                     = (EventRuleEx*)pEventRuleEx->Next;
      }
   }

   DwellAnalyzer.ResetZoneCounters();

   return TRUE;
}

void CCamera::ExecPTZCmd( int nPTZCmdType, int nParam )
{
   if( !IsActivated() ) return;
   switch( nPTZCmdType ) {
   case PTZCmdType_MenuOpen:
      if( m_pPTZCtrl ) m_pPTZCtrl->MenuOpen();
      break;
   case PTZCmdType_MenuEnter:
      if( m_pPTZCtrl ) m_pPTZCtrl->MenuEnter();
      break;
   case PTZCmdType_MenuCancel:
      if( m_pPTZCtrl ) m_pPTZCtrl->MenuCancel();
      break;
   case PTZCmdType_PosInit:
      if( m_pPTZCtrl )
         m_nPTZStateEx |= PTZStateEx_PTZPosInitBegun;
      break;
   case PTZCmdType_BLCOn:
      if( m_pPTZCtrl ) {
         m_pPTZCtrl->BLCOn( nParam );
      }
      StoppingVideoAnalyticsForAWhile( 2000 );
      break;
   case PTZCmdType_SetBLCLevel:
      if( m_pPTZCtrl ) {
         m_pPTZCtrl->SetBLCLevel( nParam );
      }
      StoppingVideoAnalyticsForAWhile( 2000 );
      break;
   case PTZCmdType_SetDZoomMode:
      if( m_pPTZCtrl )
         m_pPTZCtrl->SetDZoomMode( nParam );
      break;
   case PTZCmdType_SetWiper:
      if( m_pPTZCtrl ) {
         m_pPTZCtrl->SetWiper( nParam );
         StoppingVideoAnalyticsForAWhile( 2000 );
      }
      break;
   case PTZCmdType_SetHeatingWire:
      if( m_pPTZCtrl )
         m_pPTZCtrl->SetHeatingWire( nParam );
      break;
   case PTZCmdType_SetDayAndNightMode:
      if( m_pPTZCtrl ) {
         m_pPTZCtrl->SetDayAndNightMode( nParam );
         StoppingVideoAnalyticsForAWhile( 2000 );
      }
      break;
   case PTZCmdType_OneShotAutoFocus:
      if( m_pPTZCtrl ) {
         m_pPTZCtrl->OneShotAutoFocus();
         StoppingVideoAnalyticsForAWhile( 8000 );
      }
      break;
   case PTZCmdType_SetAutoFocusOn:
      if( m_pPTZCtrl ) {
         m_pPTZCtrl->SetAutoFocusOn( nParam );
         StoppingVideoAnalyticsForAWhile( 6000 );
      }
      break;
   case PTZCmdType_ThermalCameraOn:
      if( m_pPTZCtrl ) {
         m_pPTZCtrl->ThermalCameraOn( nParam );
      }
      break;
   case PTZCmdType_SetFan:
      if( m_pPTZCtrl )
         m_pPTZCtrl->SetFan( nParam );
      break;
   case PTZCmdType_SetZoomControlMode:
      if( m_pPTZCtrl )
         m_pPTZCtrl->SetActiveZoomCtrl();
      break;
   case PTZCmdType_SetIRMode:
      if( m_pPTZCtrl ) {
         m_pPTZCtrl->m_PTZInfo.m_IRLightCtrlInfo.m_nIRMode = nParam;
         VC->m_PTZInfo.m_IRLightCtrlInfo.m_nIRMode         = nParam;
         StoppingVideoAnalyticsForAWhile( 2000 );
      }
      break;
   case IVXCmdType_DefogOn:
#ifdef __SUPPORT_IMAGE_ENHANCEMENT
      //VC->m_bUseDefog = nParam;
      VC->SetDefog( nParam );
      StoppingVideoAnalyticsForAWhile( 5000 );
#endif
      break;
   case IVXCmdType_DefogLevel:
#ifdef __SUPPORT_IMAGE_ENHANCEMENT
      //VC->m_nDefogLevel = nParam;
      VC->SetDefogLevel( nParam );
      StoppingVideoAnalyticsForAWhile( 3000 );
#endif
      break;
   case IVXCmdType_VideoStabilizationOn:
#ifdef __SUPPORT_IMAGE_ENHANCEMENT
      //VC->m_bUseVideoStabilization = nParam;
      VC->SetVideoStabilization( nParam );
      StoppingVideoAnalyticsForAWhile( 4000 );
#endif
      break;
   case IVXCmdType_VideoStabilizationFrameNum:
#ifdef __SUPPORT_IMAGE_ENHANCEMENT
      VC->m_nVideoStabilizationFrameNum = nParam;
      StoppingVideoAnalyticsForAWhile( 3000 );
#endif
      break;
   case IVXCmdType_TestIRMode: {
      switch( nParam ) {
      case PTZIRCtrlMode_Auto:
         if( m_pPTZCtrl ) m_pPTZCtrl->SetIRMode( nParam );
         break;
      case PTZIRCtrlMode_DayMode:
         if( m_pPTZCtrl )
            m_pPTZCtrl->SetIRMode( nParam );
         break;
      case PTZIRCtrlMode_NightModeAndIROff:
         if( m_pPTZCtrl ) {
            m_pPTZCtrl->SetIRMode( nParam );
            m_pPTZCtrl->SetIRLight( 0 );
         }
         break;
      case PTZIRCtrlMode_NightModeAndIROn:
         if( m_pPTZCtrl ) {
            m_pPTZCtrl->SetIRMode( nParam );
            m_pPTZCtrl->SetIRLight( 99 );
         }
         break;
      }
   } break;
   case IVXCmdType_DayNightMode:
      VC->SetImageProperty( 0, ImagePropertyType_DayAndNight, nParam );
      StoppingVideoAnalyticsForAWhile( 2000 );
      break;
   case IVXCmdType_WhiteBalanceMode:
      VC->SetImageProperty( 0, ImagePropertyType_AWB, nParam );
      StoppingVideoAnalyticsForAWhile( 2000 );
      break;
   case IVXCmdType_GotoHomePos:
      GotoHomePos( TRUE );
      break;
   case IVXCmdType_ForceToStartHomePosVideoAnalytics:
      ForceToStartHomePosVideoAnalytics();
      break;
   }
}

BOOL CCamera::IsPTZCamera()
{
   BOOL bPTZCamera = TRUE;
   if( VC ) bPTZCamera = VC->m_bPTZCamera;
   return bPTZCamera;
}

BOOL CCamera::IsPTZCameraReady()
{
   if( IsPTZCamera() ) {
      if( m_pPTZCtrl && m_nPTZState & PTZState_PTZControlReady ) {
         return TRUE;
      }
   }
   return FALSE;
}

BOOL CCamera::IsVirtualPTZCamera()
{
   BOOL bVirtualPTZCamera = FALSE;
   if( VC && VC->m_nVideoChannelType == VideoChannelType_VirtualPTZ )
      bVirtualPTZCamera = TRUE;

   return bVirtualPTZCamera;
}

BOOL CCamera::IsPTZMoving()
{
   BOOL bPTZMoving = FALSE;
   if( IsPTZCamera() ) {
      if( m_nPTZState & ( PTZState_PTZMoving | PTZState_GotoAbsPosOngoing | PTZState_PTZStopping | PTZState_SPTZTrkOngoing | PTZState_PresetMoveStarted | PTZState_PresetMoving ) )
         bPTZMoving = TRUE;
   }
   return bPTZMoving;
}

BOOL CCamera::IsPTZDoingNothing()
{
   BOOL bSomeDoing = FALSE;
   if( m_nPTZState & ( PTZState_PresetMoveStarted | PTZState_PresetMoving | PTZState_PTZMoving | PTZState_UserControlBegun | PTZState_UserControlOngoing | PTZState_UserControlEnded | PTZState_PTZStopping | PTZState_GotoAbsPosBegun | PTZState_GotoAbsPosOngoing | PTZState_SPTZTrkOngoing | PTZState_SPTZTrkBegun | PTZState_SPTZTrkToBeEnded | PTZState_SPTZTrkEnded | PTZState_SPTZCamAutoTracking | PTZState_AutoPTZTrkByMasterOngoing | PTZState_AutoPTZTrkByMasterToBeEnded | PTZState_LockObjEvtOngoing ) )
      bSomeDoing = TRUE;
   if( m_nPTZStateEx & ( PTZStateEx_PresetMoveBySensorInputToBeStarted | PTZStateEx_PresetMoveBySensorInputOngoing ) )
      bSomeDoing = TRUE;
   return !bSomeDoing;
}

void CCamera::DetermineVideoAnalyticsStateOnOff()
{
   // 비디오 분석 수행 여부 판별함수

   BOOL bVideoAnalytics = TRUE;
   if( FALSE == ( m_nState & CamState_VideoAnalyticsUnregistered ) ) {
      if( !( DTCOptions & CamOpt_EnableVAC ) )
         bVideoAnalytics = FALSE;
      if( Options & CamOpt_PausingVideoAnalytics ) {
         bVideoAnalytics = FALSE;
         if( Options & CamOpt_PausingVideoAnalyticsToBeReleased ) {
            CTimeSpan ts( 0, 0, 0, m_nPausingVideoAnalyticsAutoReleaseWaitingTime );
            if( CTime::GetCurrentTime() - m_PausingVideoAnalyticsStartTime > ts ) {
               Options &= ~CamOpt_PausingVideoAnalyticsToBeReleased;
               Options &= ~CamOpt_PausingVideoAnalytics;
               PausingVideoAnalytics( FALSE, 0, 0, NULL );
            }
         }
      }
      if( Options & CamOpt_PausingVideoAnalyticsSchedule ) {
         if( Options & CamOpt_PausingVideoAnalyticsScheduleToBeReleased ) {
            CTimeSpan ts( 0, 0, 0, m_nPausingVideoAnalyticsScheduleAutoReleaseWaitingTime );
            if( CTime::GetCurrentTime() - m_PausingVideoAnalyticsScheduleStartTime > ts ) {
               Options &= ~CamOpt_PausingVideoAnalyticsScheduleToBeReleased;
               Options &= ~CamOpt_PausingVideoAnalyticsSchedule;
               PausingVideoAnalyticsSchedule( FALSE, 0, 0, NULL );
            }
         }
      }

      if( FALSE == IsVideoSignalOk() )
         bVideoAnalytics = FALSE;

      if( TRUE == bVideoAnalytics ) {
         // 단일 PTZ 추적 옵션이 켜저 있는 프리셋이라면 비디오 분석을 한다.
         BOOL bActivePresetSPTZTrk = FALSE;
         m_csActivePreset.Lock();
         if( m_pActivePreset && m_pActivePreset->Option & PresetOption_EnableSinglePTZTracking ) {
            bActivePresetSPTZTrk = TRUE;
         }
         m_csActivePreset.Unlock();

         // 비디오 분석이
         if( IsPTZCamera() ) {
            if( IsPTZMoving() ) {
               if( FALSE == ( m_nState & int(CamState_VideoAnalyticsOnPresetMoving) ) )
                  bVideoAnalytics = FALSE;
            }
            if( m_nPTZState & PTZState_GotoHomePosToBeStarted ) {
               bVideoAnalytics = FALSE;
            }
            if( FALSE == IsPTZCameraReady() )
               bVideoAnalytics = FALSE;
         }
      }
   } else {
      bVideoAnalytics = FALSE;
   }

   if( m_nStoppingVideoAnalyticsStartTime ) {
      if( GetTickCount() - m_nStoppingVideoAnalyticsStartTime < m_nStoppingVideoAnalyticsPeriodInMilliSec ) {
         bVideoAnalytics = FALSE;
      } else {
         m_nStoppingVideoAnalyticsStartTime        = 0;
         m_nStoppingVideoAnalyticsPeriodInMilliSec = 0;
         if( !( m_nState & int(CamState_VideoAnalyticsToBeInitialized) ) ) {
            m_nState |= CamState_VideoAnalyticsToBeInitialized;
         }
      }
   }

   if( bVideoAnalytics )
      m_nState |= CamState_DoVideoAnalytics;
   else
      m_nState &= ~CamState_DoVideoAnalytics;

   if( m_bPrevVideoAnalytics && !bVideoAnalytics ) {
      if( !( m_nPTZState & ( PTZState_SPTZTrkOngoing | PTZState_AutoPTZTrkByMasterOngoing ) ) ) {
         if( !( m_nState & int(CamState_VideoAnalyticsToBeInitialized) ) ) {
            m_nState |= CamState_VideoAnalyticsToBeInitialized;
         }
      }
   }
   m_bPrevVideoAnalytics = bVideoAnalytics;
}

void CCamera::LoadHomePositionSetting()
{
   SetActivePreset( NULL );
   m_nState |= CamState_MainVACConfigToBeLoaded;
}

void CCamera::ProcessOnVideoFrame()
{
   // 비디오 스트림 전달&처리 순서
   // 1. CVideoChannel::CallbackReceiveEncodeFrame
   // 2. CVideoChannel::OnReceiveEncodeFrame
   // 3. CCamera::CallbackEncodeVideoFrame
   // 4. CCamera::OnEncodeVideoFrame
   //    - m_EncodedVideoStramQueue[i_VCStream] 에 비디오서버 스트림 인덱스 별로 비디오 스트림을 저장
   // 5. CCamera::ProcessOnVideoFrame
   //    - 비디오 스트림 큐에 항목이 추가될 때마다 카메라 메니저 스레드에서 ProcessOnVideoFrame 함수를 호출.
   //    - m_EncodedVideoStramQueue[i_VCStream]에서 비디오 스트림을 꺼내서 CVideoSnder::m_CompImgBuffQueue변수에 스트림을 전달 또는 복사함.
   // 6. CCamera::VideoStreamingProc
   //    - 각 IVX스트림별로 바이패스 또는 트랜스 코딩 스트리밍이 수행되는 프로시져 임.
   // 7. CVideoSnder::SendVideo
   //    - CVideoSnder::m_CompImgBuffQueue에 저장되어있는 큐를 꺼네어 클라이언트 별로 전송함.

   while( 1 ) {
      BOOL bExistVideoFrame = FALSE;
      int i_VCStream;
      for( i_VCStream = 0; i_VCStream < MAX_IP_CAMERA_STREAM_NUM; i_VCStream++ ) {
         CCompressImageBufferItem* pItem = m_EncodedVideoStramQueue[i_VCStream].Pop();
         if( pItem ) {
            bExistVideoFrame  = TRUE;
            BOOL bProcessItem = TRUE;
            if( !( m_nState & CamState_Activate ) ) bProcessItem = FALSE;
            if( m_nState & CamState_Deactivating ) bProcessItem = FALSE;
            if( m_nState & CamState_ToBeClosed ) bProcessItem = FALSE;

            if( bProcessItem ) {
               int& nCodecID      = pItem->m_nCodecID;
               BYTE* pCompBuff    = (byte*)pItem->m_CompBuff;
               int& nCompBuffSize = pItem->m_nCompBuffSize;
               DWORD& dwFlag      = pItem->m_dwCommFlag;

               // 일부 비디오서버(Bosch)의 경우 비디오신호가 없음을 나타내는 비디오스트림이
               // 전송되는 경우가 있다. 비디오 신호가 없다고 판단되면 이러한 데이터는 전송하지 않는다.
               // if (VC->GetVideoSignalFlag (   )) --> 우선 인코드된 데이터가 있으면 무조건 보낸다.
               {
                  int nTargetLiveSendNum = 0;
                  int j_IVXStream;
                  for( j_IVXStream = 0; j_IVXStream < IVX_VIDEO_STREAM_NUM; j_IVXStream++ ) {
                     CVideoStreamProfile* pProfileInfo = Config_VS.GetVideoStreamProfile( j_IVXStream );
                     if( VC->IsSupportDirectVideoStreaming( i_VCStream ) && pProfileInfo->m_bDirectStreaming && m_bSendVideo[j_IVXStream] && pProfileInfo->m_nVideoServerStreamIdx == i_VCStream ) {
                        nTargetLiveSendNum++;
                     }
                  }

                  for( j_IVXStream = 0; j_IVXStream < IVX_VIDEO_STREAM_NUM; j_IVXStream++ ) {
                     CVideoStreamProfile* pProfileInfo = Config_VS.GetVideoStreamProfile( j_IVXStream );
                     if( VC->IsSupportDirectVideoStreaming( i_VCStream ) && pProfileInfo->m_bDirectStreaming && m_bSendVideo[j_IVXStream] ) {
                        if( pProfileInfo->m_nVideoServerStreamIdx == i_VCStream ) {
                           if( nTargetLiveSendNum > 1 ) {
                              CCompressImageBufferItem* pCopyItem = new CCompressImageBufferItem;
                              pCopyItem->m_nCodecID               = pItem->m_nCodecID;
                              pCopyItem->m_CompBuff               = pItem->m_CompBuff;
                              pCopyItem->m_nCompBuffSize          = pItem->m_nCompBuffSize;
                              pCopyItem->m_dwCommFlag             = pItem->m_dwCommFlag;
                              pCopyItem->m_dwFlag                 = pItem->m_dwFlag;
                              pCopyItem->m_dwType                 = pItem->m_dwType;
                              nTargetLiveSendNum++;
                              m_LiveSnderMC[j_IVXStream].m_CompImgBuffQueue.Push( pCopyItem );
                           } else {
                              if( pItem ) {
                                 m_LiveSnderMC[j_IVXStream].m_CompImgBuffQueue.Push( pItem );
                                 pItem = NULL;
                              }
                           }
                        }
                     }
                  }
               }
            }
            if( pItem ) {
               delete pItem;
               pItem = NULL;
            }
         }
      }
      if( !bExistVideoFrame )
         break;
      if( m_nState & ( CamState_Deactivating | CamState_ToBeClosed ) )
         break;
      if( !( m_nState & CamState_Activate ) )
         break;
   }
}

ISize2D CCamera::GetInputStreamVideoSize( int nIVXStreamIndex )
{
   ISize2D InputStreamVideoSize;
   CVideoStreamProfile* pProfile = &Config_VS.m_VideoStreamProfiles[nIVXStreamIndex];
   if( pProfile->m_bDirectStreaming && VC->IsSupportDirectVideoStreaming( pProfile->m_nVideoServerStreamIdx ) ) {
      // 직접 전송(전달,Bypass)모드는 비디오크기를 그대로 전달함.
      InputStreamVideoSize = VC->GetVideoSize( pProfile->m_nVideoServerStreamIdx );
   } else {
      ISize2D videoSize = VC->GetVideoSize( pProfile->m_nVideoServerStreamIdx );
      switch( pProfile->m_nResizeType ) {
      case VideoStreamResizeType_ByReductionFactor: // 영상
         InputStreamVideoSize.Width  = int( videoSize.Width * pProfile->m_fReductionFactor + 1 ) / 4 * 4;
         InputStreamVideoSize.Height = int( videoSize.Height * pProfile->m_fReductionFactor );
         break;
      case VideoStreamResizeType_UserDefined:
         InputStreamVideoSize.Width  = pProfile->m_nVideoWidth;
         InputStreamVideoSize.Height = pProfile->m_nVideoHeight;
         break;
      default:
         InputStreamVideoSize = videoSize;
         break;
      }
   }
   return InputStreamVideoSize;
}

float CCamera::GetInputStreamFrameRate( int nIVXStreamIndex )
{
   float fFrameRate                        = 30.0f;
   CVideoStreamProfile* pVideoStreamProfie = Config_VS.GetVideoStreamProfile( nIVXStreamIndex );
   if( pVideoStreamProfie ) {
      fFrameRate = pVideoStreamProfie->m_fVideoFrameRate;
      if( pVideoStreamProfie->m_bDirectStreaming && VC->IsSupportDirectVideoStreaming( pVideoStreamProfie->m_nVideoServerStreamIdx ) ) {
         if( VideoChannelType_IPCamera == VC->m_nVideoChannelType ) {
            fFrameRate = VC->m_IPCamInfoArr[nIVXStreamIndex].m_fFPS_Cap;
         } else {
            fFrameRate = VC->m_fFPS_Cap;
         }
      }
   }
   return fFrameRate;
}

int CCamera::GetInputStreamCodecID( int nIVXStreamIndex )
{
   int nCodecID                            = VCODEC_ID_H264;
   CVideoStreamProfile* pVideoStreamProfie = Config_VS.GetVideoStreamProfile( nIVXStreamIndex );
   if( pVideoStreamProfie ) {
      nCodecID = pVideoStreamProfie->m_nVideoCodecID;
      if( pVideoStreamProfie->m_bDirectStreaming && VC->IsSupportDirectVideoStreaming( pVideoStreamProfie->m_nVideoServerStreamIdx ) ) {
         if( VideoChannelType_IPCamera == VC->m_nVideoChannelType ) {
            nCodecID = VC->m_IPCamInfoArr[pVideoStreamProfie->m_nVideoServerStreamIdx].m_nVideoFormat_Cap;
         }
      }
   }
   return nCodecID;
}

void CCamera::DetermineVCDisplayCallbackFuncState()
{
   BOOL bUseVCDisplayCallbackFunc = ( m_nStateEx2 & CamStateEx2_DecodingLiveVideo ) ? TRUE : FALSE;
   VC->SetUseOfProcCallbackFunc( VCProcCallbackFuncType_Display, bUseVCDisplayCallbackFunc );
}

void CCamera::DetermineVCImgProcCallbackFuncState()
{
   BOOL bUseVCImgProcCallbackFunc = FALSE;
   if( m_nState & CamState_DoVideoAnalytics ) {
      bUseVCImgProcCallbackFunc = TRUE;
   }
   if( m_nPTZState & ( PTZState_PresetTour | PTZState_SPTZTrkBegun | PTZState_SPTZTrkOngoing | PTZState_SPTZTrkEnded | PTZState_SPTZTrkToBeEnded ) ) {
      bUseVCImgProcCallbackFunc = TRUE;
   }
   if( m_nPTZState & ( PTZState_PresetTour ) ) {
      bUseVCImgProcCallbackFunc = TRUE;
   }

   if( Options & CamOpt_PresetTouring ) {
      bUseVCImgProcCallbackFunc = TRUE;
   }

   if( VideoChannelType_VideoFile == VC->m_nVideoChannelType ) {
      if( VC->m_FileCamInfo.m_bSyncVCAFpsToVideoFps )
         bUseVCImgProcCallbackFunc = FALSE;
   }

   VC->SetUseOfProcCallbackFunc( VCProcCallbackFuncType_ImgProc, bUseVCImgProcCallbackFunc );
   VC->SetStreamIdxOfProcCallbackFunc( VCProcCallbackFuncType_ImgProc, 0 );

   if( FALSE == bUseVCImgProcCallbackFunc ) {
      int nImgProcStreamIndex = VC->GetStreamIdxOfProcCallbackFunc( VCProcCallbackFuncType_ImgProc );
      VC_PARAM_STREAM input;
      input.pParam     = this;
      input.nVCState   = VC->m_nState;
      input.nCount     = INT_MAX;
      input.nStreamIdx = nImgProcStreamIndex;
      input.nCamState  = 0;
      input.pYUY2Buff  = NULL;
      input.nWidth     = 0;
      input.nHeight    = 0;
      DoImageProcess( input );
   }
}

void CCamera::DetermineVCVideoFrameCallbackFuncState()
{
   BOOL bUseStreamIndex[MAX_IP_CAMERA_STREAM_NUM];
   BOOL bDecodeStreamIndex[MAX_IP_CAMERA_STREAM_NUM];
   ZeroMemory( bUseStreamIndex, sizeof( bUseStreamIndex ) );
   ZeroMemory( bDecodeStreamIndex, sizeof( bDecodeStreamIndex ) );

   int j_IVXStream;
   for( j_IVXStream = 0; j_IVXStream < IVX_VIDEO_STREAM_NUM; j_IVXStream++ ) {
      if( m_bSendVideo[j_IVXStream] ) {
         CVideoStreamProfile* pProfileInfo = Config_VS.GetVideoStreamProfile( j_IVXStream );
         if( VC->IsSupportDirectVideoStreaming( pProfileInfo->m_nVideoServerStreamIdx ) ) {
            if( FALSE == pProfileInfo->m_bDirectStreaming ) {
               bDecodeStreamIndex[pProfileInfo->m_nVideoServerStreamIdx] = TRUE;
            } else {
               bUseStreamIndex[pProfileInfo->m_nVideoServerStreamIdx] = TRUE;
            }
         } else {
            bDecodeStreamIndex[pProfileInfo->m_nVideoServerStreamIdx] = TRUE;
         }
      }
   }

   int i;
   for( i = 0; i < MAX_IP_CAMERA_STREAM_NUM; i++ ) {
      VC->SetUseOfStreamIdxInEncodeCallBackFunc( i, bUseStreamIndex[i] );
      VC->SetUseOfStreamIdxInDecodeCallBackFunc( i, bDecodeStreamIndex[i] );
   }
}

BOOL CCamera::IsSinglePTZTrkSupported()
{
   if( m_pPTZCtrl ) {
      return m_pPTZCtrl->IsSinglePTZTrkSupported();
   }
   return ( FALSE );
}

BOOL CCamera::IsSinglePTZTracking()
{
   if( m_nPTZState & ( PTZState_SPTZTrkOngoing | PTZState_SPTZTrkOngoing | PTZState_SPTZTrkOngoing ) ) {
      return ( TRUE );
   }
   return ( FALSE );
}

BOOL CCamera::IsSupportAutomousPTZCameraTracking()
{
   BOOL bSupportAutomousPTZCameraTracking = TRUE;

   //if (IS_SUPPORT(__SUPPORT_AUTONOMOUS_PTZ_CAMERA_TRACKING)) {
   //   bSupportAutomousPTZCameraTracking = TRUE;
   //}
   //else {
   //   if (IS_PRODUCT(__PRODUCT_G200)) {
   //      if (SICode_2011_ChosunDynastyPalace == g_nSICode) {
   //         if (VideoChannelType_IPCamera == VC->m_nVideoChannelType) {
   //            if (VC->m_IPCamInfo.m_nIPCameraType == IPCameraType_Bosch_VIPX1)
   //               bSupportAutomousPTZCameraTracking = TRUE;
   //         }
   //      }
   //   }
   //}
   return bSupportAutomousPTZCameraTracking;
}

void CCamera::DoImgProcSinglePTZTrack( const VC_PARAM_STREAM& param )
{
   //LOGD << TimerCheckerManager::Get().DebugMsg( "SinglePTZTrack", (int)( 1000.0 / DesiredFrameRate ) );

   if( !m_pPTZCtrl || !IsPTZCamera() ) return;
   if( !VC->m_pPTZCtrl->IsPTZReady() ) return;

   if( ( Options & CamOpt_AutoPTZCtrlMode ) && !( m_nPTZState & PTZState_SPTZTrkOngoing ) ) {
      if( FindHandoffedTrackObject() ) {
         m_nPTZState |= PTZState_SPTZTrkBegun;
      }
   }

   if( SPTZTrkMode == SPTZTrkMode_EvtTriggered ) {
      if( Options & CamOpt_AutoPTZCtrlMode ) {
         if( !( m_nPTZState & ( PTZState_SPTZTrkOngoing | PTZState_PresetTourToBeRestarted | PTZState_GotoHomePosToBeStarted ) ) ) {
            if( FindEventTriggerdTrackedObject() )
               m_nPTZState |= PTZState_SPTZTrkBegun;
         }
      }
   }

   if( !( DTCOptions & CamOpt_EnableVAC ) && m_nPTZState & PTZState_SPTZTrkBegun ) {
      m_nPTZState &= ~PTZState_SPTZTrkBegun;
   }

   if( Options & CamOpt_PausingVideoAnalytics ) {
      if( m_nPTZState & PTZState_SPTZTrkBegun )
         m_nPTZState &= ~PTZState_SPTZTrkBegun;
      if( m_nPTZState & PTZState_SPTZTrkOngoing )
         m_nPTZState |= PTZState_SPTZTrkToBeEnded;
   }

   if( !IsSupportAutomousPTZCameraTracking() )
      m_nPTZState &= ~( PTZState_SPTZTrkBegun | PTZState_SPTZTrkOngoing );

   if( m_nPTZState & PTZState_SPTZTrkBegun ) {
      //if (m_bGetColorSrcImageOK && m_bGetGrayScaleSrcImage) {
      m_nPTZState &= ~( PTZState_SPTZTrkBegun );
      m_nPTZState &= ~( PTZState_AutoPTZTrkByMasterOngoing | PTZState_AutoPTZTrkByMasterToBeEnded );

      if( m_nPTZState & PTZState_PresetTour ) {
         m_nPTZState |= ( PTZState_PresetTourPaused );
         m_nPresetTourPausedTime = GetTickCount();
         //  }
         SetActivePreset( NULL );

         VC->m_fFPS_IP = 15.0f;

         CS_ObjectTracker.Lock();
         m_pSPTZTracker->Initialize( ChannelNo, SGImage, ObjectTracker, CS_ObjectTracker, SinglePTZTrackingObjPtr, VC->m_fFPS_IP );
         m_pSPTZTracker->Perform( SCImage, SGImage );

         m_nPTZState |= ( PTZState_SPTZTrkOngoing );
         CS_ObjectTracker.Unlock();

         logw( "[I will work SendEventInfo After I work DoImgProc]\n" );
         m_ivcpRtspSender->SendEventInfo( &SinglePTZTrackingObj, &SinglePTZTrackingEvent );

         m_nState |= CamState_VideoAnalyticsToBeInitialized;
      }
   }

   if( m_nPTZState & PTZState_SPTZTrkOngoing ) {
      if( m_bGetGrayScaleSrcImage ) {
         m_pSPTZTracker->Perform( SCImage, SGImage );
         if( m_pSPTZTracker->GetState() & SPTZTrkState_TrackToBeEnded ) {
            SetSPTZTrkToBeEnded();
         }
         IBox2D tb = m_pSPTZTracker->GetTrkBox();
         SinglePTZTrackingObj( tb.X, tb.Y, tb.Width, tb.Height );
         m_nPTZStateEx &= ~PTZStateEx_InHomePos;
      }
   }
   if( m_nPTZState & PTZState_SPTZTrkToBeEnded ) {
      m_nPTZState &= ~( PTZState_SPTZTrkToBeEnded );
      m_nPTZState |= PTZState_SPTZTrkEnded;
   }
   if( m_nPTZState & PTZState_SPTZTrkEnded ) {
      m_pPTZCtrl->ContPTZMoveAV( FPTZVector( 0.0f, 0.0f, 0.0f ) );
      m_nPTZState &= ~( PTZState_SPTZTrkOngoing | PTZState_SPTZTrkEnded );
      m_nPTZState &= ~( PTZState_SignalPTZMoveStopped | PTZState_PTZStopping | PTZState_GotoAbsPosOngoing | PTZState_PTZMoving );
      m_nPTZState &= ~( PTZState_AutoPTZTrkByMasterOngoing | PTZState_AutoPTZTrkByMasterToBeEnded );

      if( !m_pPTZCtrl->IsContPTZMoveStopped() )
         m_nPTZState |= ( PTZState_UserControlEnded | PTZState_PTZMoving );
      m_nPTZCtrlStopTime = GetTickCount();

      m_pSPTZTracker->Uninitialize();
      SinglePTZTrackingEvent.Status = EV_STATUS_ENDED;
      //SetGotoHomePosOrPresetTourToBeStarte(0);
      SetPresetTourToBeStarted( 500 );
      logd( "SPTZTrackObjLost : SetGotoHomePosOrPresetTourToBeStarted" );
      VC->m_fFPS_IP = DesiredFrameRate;
   }
}

BOOL CCamera::SetSPTZTrkToBeEnded()
{
   if( m_nPTZState & ( PTZState_SPTZTrkBegun | PTZState_SPTZTrkOngoing ) ) {
      m_nPTZState |= PTZState_SPTZTrkToBeEnded;
      return TRUE;
   }
   return FALSE;
}

BOOL CCamera::FindEventTriggerdTrackedObject()
{
   int bFindObj            = FALSE;
   SinglePTZTrackingObjPtr = NULL;
   FPoint2D fp2Scale( float( REF_IMAGE_WIDTH ) / ProcImgSize.Width, float( REF_IMAGE_HEIGHT ) / ProcImgSize.Height );
   if(SrcImgSize.Width){
      fp2Scale( float( SrcImgSize.Width ) / ProcImgSize.Width, float( SrcImgSize.Height ) / ProcImgSize.Height );
   }
   
   TrackedObjectEx* t_object = (TrackedObjectEx*)ObjectTracker.TrackedObjects.First();
   while( t_object != NULL ) {
      if( t_object->Status & ( TO_STATUS_VERIFIED ) ) {
         FPoint2D center_pos = t_object->GetCenterPosition();
         center_pos.X *= fp2Scale.X;
         center_pos.Y *= fp2Scale.Y;
         BOOL bTrkObjInNTrkArea = FALSE;
         if( FALSE == bTrkObjInNTrkArea ) {
            bFindObj = TRUE;
            if( NULL == SinglePTZTrackingObjPtr ) {
               SinglePTZTrackingObj    = *t_object;
               SinglePTZTrackingObjPtr = t_object;
            } else {
               if( SinglePTZTrackingObjPtr->Area < t_object->Area ) {
                  SinglePTZTrackingObj    = *t_object;
                  SinglePTZTrackingObjPtr = t_object;
               }
            }
            break;
         }
      }
      if( bFindObj ) break;
      t_object = (TrackedObjectEx*)ObjectTracker.TrackedObjects.Next( t_object );
   }
   return bFindObj;
}

BOOL CCamera::FindHandoffedTrackObject()
{
   int bFindObj = FALSE;
   FPoint2D fp2Scale( float( REF_IMAGE_WIDTH ) / ProcImgSize.Width, float( REF_IMAGE_HEIGHT ) / ProcImgSize.Height );
   if(SrcImgSize.Width){
      fp2Scale( float( SrcImgSize.Width ) / ProcImgSize.Width, float( SrcImgSize.Height ) / ProcImgSize.Height );
   }
   
   TrackedObjectEx* t_object = (TrackedObjectEx*)ObjectTracker.TrackedObjects.First();
   while( t_object != NULL ) {
      if( t_object->Status & ( TO_STATUS_VERIFIED ) ) {
         if( t_object->Time_Tracked > 0.2f ) {
            FPoint2D center_pos = t_object->GetCenterPosition();
            center_pos.X *= fp2Scale.X;
            center_pos.Y *= fp2Scale.Y;
            BOOL bTrkObjInNTrkArea = FALSE;
         }
      }
      if( bFindObj ) break;
      t_object = (TrackedObjectEx*)ObjectTracker.TrackedObjects.Next( t_object );
   }
   return bFindObj;
}

BOOL CCamera::IsSPTZTracking()
{
   if( m_nPTZState & PTZState_SPTZTrkOngoing )
      return TRUE;
   return FALSE;
}

BOOL CCamera::CheckOverlapEvent(int nEventType)
{
   //static BOOL bFitstTime = FALSE;
   //BOOL bOverlapEvent = TRUE;
   //clock_t current_tick = clock();
   //float CurrentSec = current_tick / CLOCKS_PER_SEC;
   
   
   DWORD ticks = GetTickCount();
   float seconds = (ticks/1000) % 60;
   
   float OverlapEventTime = seconds - Check_OverlapEvent_[nEventType].PrevDetectTime;
   if (Check_OverlapEvent_[nEventType].FirstEvent == false && OverlapEventTime >= Check_OverlapEvent_[nEventType].EventDetectTime) {
      //bOverlapEvent = FALSE;
      Check_OverlapEvent_[nEventType].SendEventInfo = TRUE;
      Check_OverlapEvent_[nEventType].FirstEvent = TRUE;
      Check_OverlapEvent_[nEventType].PrevDetectTime = seconds;
   }
   else{
      Check_OverlapEvent_[nEventType].SendEventInfo = FALSE;
   }
   
   return TRUE;
}
CRTSPStreamingChannel* CCamera::GetRtspStreamingChannel( int pos )
{
   return m_RTSPStreamingChannel[pos].get();
}

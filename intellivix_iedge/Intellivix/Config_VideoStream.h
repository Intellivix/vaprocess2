#pragma once

const int IVX_VIDEO_STREAM_NUM = 4;

enum VideoStreamResizeType {
   VideoStreamResizeType_OrignalSize       = 0,
   VideoStreamResizeType_ByReductionFactor = 1,
   VideoStreamResizeType_UserDefined       = 2,
};

//////////////////////////////////////////////////////////////
//
// Class : CVideoStreamProfile
//
//////////////////////////////////////////////////////////////

class CVideoStreamProfile {
public:
   std::string m_strProfileName;
   BOOL m_bDirectStreaming;
   int m_nVideoServerStreamIdx; // 비디오 서버의 스트림인덱스. IntelliVIX 스트림 인덱스와 혼돈하지 말것.
   int m_nVideoCodecID;
   int m_nVideoBitrate;
   float m_fVideoFrameRate;
   int m_nResizeType; // enum VideoStreamResizeType 참조
   float m_fReductionFactor;
   int m_nVideoWidth;
   int m_nVideoHeight;

public:
   CVideoStreamProfile();
   BOOL Read( CXMLIO* pIO );
   BOOL Write( CXMLIO* pIO );

   void QualityToBitrate( int nQuality, int& nBitRate );
};

//////////////////////////////////////////////////////////////
//
// Class : CConfig_VideoStream
//
//////////////////////////////////////////////////////////////

class CConfig_VideoStream {
public:
   CVideoStreamProfile m_VideoStreamProfiles[IVX_VIDEO_STREAM_NUM];
   BOOL m_bUseHWEncoder;

public:
   CConfig_VideoStream();

public:
   CVideoStreamProfile* GetVideoStreamProfile( int nStreamIdx );

public:
   BOOL Read( CXMLIO* pIO );
   BOOL Write( CXMLIO* pIO );
};

///////////////////////////////////////////////////////////////////////////////

enum DisplayStreamSelectionMode {
   DisplayStreamSelectionMode_FixedByMainStream       = 0,
   DisplayStreamSelectionMode_AdaptiveStreamSelection = 1,
};

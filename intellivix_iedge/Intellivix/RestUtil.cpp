﻿#include "stdafx.h"
#include "RestUtil.h"
#include "CommonUtil.h"
#include "bccl_time.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace utility::conversions;

namespace web_rest_util
{
   //////////////////////////////////////////////////////////////////////////
   // util api
   //////////////////////////////////////////////////////////////////////////

   void to_lower(std::vector<string_t>& vec)
   {
      auto iter = vec.begin();
      while (iter != vec.end())
      {
         string_t& str = *iter;
         std::transform(str.begin(), str.end(), str.begin(), ::tolower);
         iter++;
      }
   }

   void to_upper(std::vector<string_t>& vec)
   {
      auto iter = vec.begin();
      while (iter != vec.end())
      {
         string_t& str = *iter;
         std::transform(str.begin(), str.end(), str.begin(), ::toupper);
         iter++;
      }
   }

   //////////////////////////////////////////////////////////////////////////

   json_value get_response_body(bool success, const json_value& data)
   {
      json_value body = json_value::object(true);

      body[U("success")] = json_value::boolean(success);
      body[U("data")] = data;

      return body;
   }

   json_value get_response_error(const string_t& error_msg)
   {
      json_value body = json_value::object(true);
      json_value error = json_value::object(true);

      error[U("error")] = json_value::string(error_msg);

      body[U("success")] = json_value::boolean(false);
      body[U("data")] = error;

      return body;
   }

   json_value get_response_body_error(const string_t& error_msg)
   {
      json_value body = json_value::object(true);

      body[U("success")] = json_value::boolean(false);
      body[U("error")] = json_value::string(error_msg);

      return body;
   }

   //////////////////////////////////////////////////////////////////////////

   json_value as_json(const std::string& value)
   {
      if (value.empty())   return json_value::null();

      return json_value::string(to_string_t(value));
   }

   json_value as_json(BOOL value)
   {
      return json_value::boolean(0 != value);
   }

   json_value as_json(const FILETIME& ft)
   {
      char szStr[256] = { 0, };

      FILETIME   ft2 = ft;

      BCCL::Time time;
      BCCL::Convert(ft2, time);
      std::string value = sutil::sformat("%04d_%02d%02d_%02d%02d%02d_%03d", time.Year, time.Month, time.Day, time.Hour, time.Minute, time.Second, time.Millisecond);

      if (value.empty())   return json_value::null();

      return as_json(value);
   }

   json_value as_json(const BCCL::BGRColor& color)
   {
      json_value json_color = json_value::array(3);
      json_color[0] = color.R;
      json_color[1] = color.G;
      json_color[2] = color.B;

      return json_color;
   }

   json_value as_json(const BCCL::IBox2D& box)
   {
      json_value json_box = json_value::array(4);
      json_box[0] = box.X;
      json_box[1] = box.Y;
      json_box[2] = box.Width;
      json_box[3] = box.Height;

      return json_box;
   }

   std::string as_string(const string_t& key, const json_value& value)
   {
      return value.at(key).as_string();
   }

   //////////////////////////////////////////////////////////////////////////

   bool for_each_array(const json_array& array, std::function<void(const json_value&)> func)
   {
      auto iter = array.begin();
      while (iter != array.end())
      {
         json_value value = *iter;
         func(value);
         iter++;
      }

      return true;
   }

   bool as_vector_int(const string_t& key, const json_value& json, std::vector<int>& vec)
   {
      if (json.is_null())
         return false;

      if (json.at(key).is_null())
         return false;

      json_array array = json.at(key).as_array();

      return as_vector_int(array, vec);
   }

   bool as_vector_int(const json_array& array, std::vector<int>& vec)
   {
      for_each_array(array, [&vec](const json_value& value)
      {
         if (value.is_integer())
            vec.push_back(value.as_integer());
      });

      return true;
   }

   bool as_vector_string_t(const string_t& key, const json_value& json, std::vector<string_t>& vec)
   {
      if (json.is_null())
         return false;

      if (json.at(key).is_null())
         return false;

      json_array array = json.at(key).as_array();

      return as_vector_string_t(array, vec);
   }

   bool as_vector_string_t(const json_array& array, std::vector<string_t>& vec)
   {
      for_each_array(array, [&vec](const json_value& value)
      {
         if (value.is_string())
            vec.push_back(value.as_string());
      });

      return true;
   }

   //////////////////////////////////////////////////////////////////////////

   pplx::task<bool> _do_while_iteration(std::function<pplx::task<bool>(void)> func)
   {
      pplx::task_completion_event<bool> ev;

      func().then([=](bool task_completed)
      {
         ev.set(task_completed);
      });

      return pplx::create_task(ev);
   }

   pplx::task<bool> _do_while_impl(std::function<pplx::task<bool>(void)> func)
   {
      return _do_while_iteration(func).then([=](bool continue_next_iteration) -> pplx::task<bool>
      {
         return ((continue_next_iteration == true) ? _do_while_impl(func) : pplx::task_from_result(false));
      });
   }

   /*
   // async_do_while function example

   auto t = pplx::create_task([&]() {
      async_do_while([&]()
      {
         return client.receive().then([=](pplx::task<websocket_incoming_message> in_msg_task)
         {
            websocket_incoming_message in_msg = in_msg_task.get();

            return in_msg.extract_string().then([=](pplx::task<std::string> str_tsk)
            {
               std::string msg_str = str_tsk.get();
               std::cout << "Recv : " << msg_str << std::endl;
            });

         }).then([](pplx::task<void> end_task)
         {
            bool ret = end_task.is_done();

            try
            {
               end_task.get();
               return true;
            }
            catch (websocket_exception code)
            {
               //
               // Do error handling or eat the exception as needed.
               //
               std::cout << "exception : " << code.what() << " " << code.error_code() << std::endl;
            }
            catch (...)
            {
               //
               // Do error handling or eat the exception as needed.
               //
               std::cout << "exception" << std::endl;
            }
            // We are here means we encountered some exception.
            // Return false to break the asynchronous loop.
            return false;
         });
      });
   }).wait();
   */

   pplx::task<void> async_do_while(std::function<pplx::task<bool>(void)> func)
   {
      return _do_while_impl(func).then([](bool) {});
   }
};

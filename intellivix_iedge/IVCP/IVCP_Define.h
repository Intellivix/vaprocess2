﻿///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2007 illisis. Inc. All rights reserved.
//
// FileName: IVCP_Define.h
//
// Function: Defines for IVCP 3.0.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#ifdef WIN32
#define __WIN32
#define __AFXMFC
#else
#define __LINUX
#endif

#if defined(__WIN32)
#ifdef __AFXMFC
#include <afxwin.h>
#include <afxmt.h>
#include <afxtempl.h>
#include <afxsock.h>
#else // WIN32 API
#include "IVCP_Define_Win32.h"
#endif
#elif defined(__LINUX)
#include "IVCP_Define_Linux.h"
#endif

///////////////////////////////////////////////////////////////////////////////
// Compiler

#if defined(__WIN32)
   #if defined(__VC6)
      #define __VC_VER   6   // Microsoft Visual C++ 6
   #elif defined(__VC2003)
      #define __VC_VER   7   // Microsoft Visual C++ 2003
   #elif defined(__VC2005)
      #define __VC_VER   8   // Microsoft Visual C++ 2005
   #elif defined(__VC2008)
      #define __VC_VER   9   // Microsoft Visual C++ 2008
   #elif defined(__VC2010)
      #define __VC_VER  10   // Microsoft Visual C++ 2010
   #endif
#endif

///////////////////////////////////////////////////////////////////////////////
// Type Defines

typedef unsigned int   uint;
typedef unsigned char  byte;
typedef unsigned long  ulong;
typedef unsigned short ushort;

///////////////////////////////////////////////////////////////////////////////
// Defines
#define ICVP_IPADDR_LEN    64
#define ICVP_USERID_LEN    32
#define ICVP_PASSWD_LEN    32
#define ICVP_NAI_LEN       80
#define ICVP_KEYCODE_LEN   96

#define IVCP_SYSTEM_BASE_PORT  4204
#define IVCP_MMS_BASE_PORT     6204
#define IVCP_FACE_BASE_PORT    5204

///////////////////////////////////////////////////////////////////////////////
//
// Log
//
///////////////////////////////////////////////////////////////////////////////

typedef enum _ivcpLogPriority {
    IVCP_LOG_NONE = 0,
    IVCP_LOG_LOW,
    IVCP_LOG_MED,
    IVCP_LOG_HIGH,
    IVCP_LOG_INFO,
    IVCP_LOG_WARN,
    IVCP_LOG_ERR,
    IVCP_LOG_FATAL,
} ivcpLogPriority;

typedef enum _ivcpPlayCtrl {
	IVCP_PLAY_STOP = 0,
	IVCP_PLAY_START,
	IVCP_PLAY_PAUSE,
	IVCP_PLAY_CONTINUE,	
} ivcpPlayCtrl;


///////////////////////////////////////////////////////////////////////////////
//
// FILETIME 관련 함수
//
///////////////////////////////////////////////////////////////////////////////

void GetCurrFileTime  (FILETIME& ftTime);
int  GetDeltaFileTime (FILETIME& ftEnd, FILETIME& ftStart);

TARGET = IVCP
TEMPLATE = lib
include( ../env.pri )

INCLUDEPATH += ../BCCL
DEPENDPATH += ../BCCL
PRE_TARGETDEPS += $$DESTDIR/libBCCL.a

HEADERS += \
    cGlobals.h \
    IVCP30_Define.h \
    IVCP_Define.h \
    IVCP_Define_Linux.h \
    IVCP_Define_Win32.h \
    IVCP_Packet.h \
    IVCP_PacketStruct.h

SOURCES += \
    IVCP_Define.cpp \
    IVCP_Packet.cpp \
    IVCP_PacketStruct.cpp


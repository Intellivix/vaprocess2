﻿///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2013 illisis. Inc. All rights reserved.
//
// FileName: IVCP_Define.cpp
//
// Function: Defines for IVCP 3.0.
//
///////////////////////////////////////////////////////////////////////////////

#include "IVCP_Define.h"

///////////////////////////////////////////////////////////////////////////////
//
// Log
//
///////////////////////////////////////////////////////////////////////////////

int IVCP_LOG(int nLevel, LPCSTR format, ...)
{
   return 0;
}

///////////////////////////////////////////////////////////////////////////////
//
// FILETIME 관련 함수
//
///////////////////////////////////////////////////////////////////////////////

void GetCurrFileTime (FILETIME& ftTime)
{
#ifdef __WIN32
   SYSTEMTIME stTime;
   GetLocalTime (&stTime);
   SystemTimeToFileTime (&stTime, &ftTime);
#else
   //time_t rawtime;
   //struct tm *ptm;
   //time ( &rawtime );
   // ptm = localtime ( &rawtime );

   //struct timespec timeCurrent;
   //::clock_gettime(CLOCK_REALTIME, &timeCurrent);
   //ftTime.dwHighDateTime = timeCurrent.tv_sec; 
   //ftTime.dwLowDateTime = timeCurrent.tv_usec; 

   struct timeval currTime;
   gettimeofday(&currTime, NULL);
   ftTime.dwHighDateTime = currTime.tv_sec;
   ftTime.dwLowDateTime = currTime.tv_usec;
#endif
}

int GetDeltaFileTime (FILETIME& ftEnd, FILETIME& ftStart)
{
#ifdef __WIN32 // 윈도우는 64bit 숫자
   __int64* pStartTime = (__int64*)&ftStart;
   __int64* pEndTime   = (__int64*)&ftEnd;
   double pDeltaTime   = (*pEndTime) - (*pStartTime);
   int nMSec = (pDeltaTime / 10000); // millisecond
#else
   struct timeval time0, time1, time_diff;
   time0.tv_sec  = ftStart.dwHighDateTime;
   time0.tv_usec = ftStart.dwLowDateTime;
   time1.tv_sec  = ftEnd.dwHighDateTime;
   time1.tv_usec = ftEnd.dwLowDateTime;
   //timersub(&time1, &time0, &time_diff);
   time_diff.tv_sec  = time1.tv_sec - time0.tv_sec; 
   time_diff.tv_usec = time1.tv_usec - time0.tv_usec; 
   if( time_diff.tv_usec < 0 ) {
      time_diff.tv_sec--;
      time_diff.tv_usec += 1000000;
   }
   int nMSec = (time_diff.tv_sec * 1000) + (time_diff.tv_usec / 1000);
#endif
   return nMSec;
}

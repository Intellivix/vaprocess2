﻿///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2013 illisis. Inc. All rights reserved.
//
// FileName : IVCP_Defines_linux.h
//
// Function: Defines for ...
//
///////////////////////////////////////////////////////////////////////////////

// Win32 version

#pragma once

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <winsock.h>

// CRT stuff:
#include <assert.h>
#include <stdio.h>

// STL stuff:
#include <vector>
#include <list>
#include <string>
#include <map>
#include <algorithm>


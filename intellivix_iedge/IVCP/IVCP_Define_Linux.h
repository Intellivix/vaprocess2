﻿///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2013 illisis. Inc. All rights reserved.
//
// FileName : IVCP_Defines_linux.h
//
// Function : Defines for ...
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "cGlobals.h"

//typedef cCriticalSection CCriticalSection;
//typedef cEvent CEvent;

// Socket
//typedef int SOCKET;
#define INVALID_SOCKET               (SOCKET)(~0)
//#define INADDR_NONE                  0xffffffff
#define SD_BOTH                      (2)
#define closesocket(x)               (shutdown(x, SD_BOTH), close(x))
#define ioctlsocket                  ioctl
#define SOCKET_ERROR                 (-1)
#define WSAEWOULDBLOCK               EWOULDBLOCK
#define WSAEISCONN                   EISCONN
//#define AF_INET                      2               // internetwork: UDP, TCP, etc.
//#define INADDR_ANY                   (ULONG)0x00000000

typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr* LPSOCKADDR;
#if 0
typedef struct _in_addr_x {
        union {
                struct { UCHAR s_b1,s_b2,s_b3,s_b4; } S_un_b;
                struct { USHORT s_w1,s_w2; } S_un_w;
                ULONG S_addr;
			} S_un;
		#define s_addr_x  S_un.S_addr /* can be used for most tcp & ip code */
		#define s_host_x  S_un.S_un_b.s_b2    // host on imp
		#define s_net_x   S_un.S_un_b.s_b1    // network
		#define s_imp_x   S_un.S_un_w.s_w2    // imp
		#define s_impno_x S_un.S_un_b.s_b4    // imp #
		#define s_lh_x    S_un.S_un_b.s_b3    // logical host
		} in_addr_x;
#endif
// ~Socket

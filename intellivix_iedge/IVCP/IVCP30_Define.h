﻿///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2013 illisis. Inc. All rights reserved.
//
// FileName: IVCP_RemoteStruct.h
//
// Function: Data structure for IVCP 3.0.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

extern "C"
{

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// IVCP 3.0 Interface Structures
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const float PTZ_NO_CMD   = (float)1E-20;

///////////////////////////////////////////////////////////////////////////////
//
// Enumerations
//
///////////////////////////////////////////////////////////////////////////////

// 연결 상태

enum IVCP_ConnectionStatus
{
   IVCP_NET_CONNECTED  = 0x00000001,   // 네트워크 연결 여부
   IVCP_CONN_STARTUP   = 0x00000002,   // 로긴 과정이 완료되어 정상 동작중
};

// 메타데이터 플래그

enum IVCP_MetadataFlag
{
   IVCP_META_EVENT_BEGUN      = 0x00000001,
   IVCP_META_EVENT_ENDED      = 0x00000002,
   IVCP_META_OBJECT_BEGUN     = 0x00000004,
   IVCP_META_OBJECT_ENDED     = 0x00000008,
   IVCP_META_FRAME_INFO       = 0x00000010,
   IVCP_META_PLATE_BEGUN      = 0x00000020,
   IVCP_META_PLATE_ENDED      = 0x00000040,
   IVCP_META_FACE_BEGUN       = 0x00000080,
   IVCP_META_FACE_ENDED       = 0x00000100,
   IVCP_META_PTZ_DATA         = 0x00000200,
   IVCP_META_PRESET_ID        = 0x00000400,
   IVCP_META_EVTZONE_INFO     = 0x00000800,
   IVCP_META_EVTZONE_COUNT    = 0x00001000,
   IVCP_META_PRESET_MAP       = 0x00002000,
   IVCP_META_SETTING          = 0x00004000,
   IVCP_META_LOG_INFO         = 0x00008000,
   IVCP_META_FRAME_OBJ        = 0x00010000,
   IVCP_META_EVT_FULL_IMG     = 0X00020000, // (mkjang-140509)
   IVCP_META_OBJ_FULL_IMG     = 0X00040000, // (mkjang-140509)
   IVCP_META_EVENT_IPEX	      = 0X00080000,
   IVCP_META_CMD_DOOR         = 0X00100000,
   IVCP_META_HEATMAP          = 0X00200000, 
   IVCP_META_STATISTICS_INFO  = 0X00400000, 
};

// 비디오 코덱

enum IVCP_VideoCodeType
{
   IVCP_VCODEC_NONE     = 255,
   IVCP_VCODEC_MPEG4    =   0,
   IVCP_VCODEC_MJPEG    =   1,
   IVCP_VCODEC_RAW      =   2,
   IVCP_VCODEC_H264     =   3,
};

// 검색 요청 종류

enum IVCP_SearchType
{
   IVCP_SEARCH_EVENT    = 0,
   IVCP_SEARCH_OBJECT   = 1,
   IVCP_SEARCH_FRAME    = 2,
   IVCP_SEARCH_PLATE    = 3,
   IVCP_SEARCH_FACE     = 4,
   IVCP_SEARCH_LOG      = 5,
};

///////////////////////////////////////////////////////////////////////////////
// 일반 요청 응답 디파인

#define IVCP_CAM_LIVEMETA_RES    16    // int 형 결과값 (0: 성공, other: 실패)
#define IVCP_CAM_LIVEVIDEO_RES   18    // 
#define IVCP_CAM_PLAYSTART_RES   20    // 
#define IVCP_CAM_PLAYSTOP_RES    22    // 
#define IVCP_CAM_PLAYCONTROL_RES 24    // 
#define IVCP_CAM_SETTING_RES     28    // 
#define IVCP_CAM_COMMAND_RES     30    // 
#define IVCP_CAM_TALKAUDIO_RES   32    // 

#define IVCP_PTZ_CONTMOVE_RES    37    // 
#define IVCP_PTZ_ABSMOVE_RES     39    // 
#define IVCP_PTZ_BOXMOVE_RES     41    // 
#define IVCP_PTZ_GETABSPOS_RES   43    // CIvcpPTZInfoData 구조체 넘김

#define IVCP_SRH_FRAME_RES       73    // 
#define IVCP_SRH_EVENT_RES       75    // 
#define IVCP_SRH_OBJECT_RES      77    // 
#define IVCP_SRH_PLATE_RES       79    // 
#define IVCP_SRH_FACE_RES        81    // 
#define IVCP_SRH_LOG_RES         83    // 
#define IVCP_SRH_STOP_RES        85    // 

///////////////////////////////////////////////////////////////////////////////

// 이벤트/객체/번호판 메타 상태

enum IVCP_EventStatus
{
   IVCP_EVENT_BEGUN  = 0, // Start of event.
   IVCP_EVENT_ENDED  = 1, // End of event.
};

// 이벤트 종류

enum IVCP_EventType 
{   
   IVCP_EVENT_LOITERING           = 0,
   IVCP_EVENT_PATH_PASSING        = 1,
   IVCP_EVENT_DIRECTIONAL_MOTION  = 2,
   IVCP_EVENT_APPEARING           = 3,
   IVCP_EVENT_DISAPPEARING        = 4,
   IVCP_EVENT_STOPPING            = 5,
   IVCP_EVENT_ABANDONED           = 6,
   IVCP_EVENT_LINE_CROSSING       = 7,
   IVCP_EVENT_SMOKE               = 8,
   IVCP_EVENT_FLAME               = 9,
   IVCP_EVENT_FALLING             = 10,
   IVCP_EVENT_GROUPING            = 11,
   IVCP_EVENT_FIGHTING            = 12,
   IVCP_EVENT_UNSAFE_CROSSING     = 13,
   IVCP_EVENT_CAR_ACCIDENT        = 14,
   IVCP_EVENT_DISPERSION          = 15,
   IVCP_EVENT_TRAFFIC_CONGESTION  = 16,
   IVCP_EVENT_COLOR_CHANGE        = 17,
   IVCP_EVENT_CAR_PARKING         = 18,
   IVCP_EVENT_REMOVED             = 19,
   IVCP_EVENT_WATER_LEVEL         = 20,
   IVCP_EVENT_ZONE_COLOR          = 21,
   IVCP_EVENT_DWELL               = 22,
   IVCP_EVENT_DWELL_OVERCROWDED   = 23,
   IVCP_EVENT_DWELL_OVERWAITING   = 24,
};

// 객체 종류

enum IVCP_ObjectType 
{   
   IVCP_OBJECT_HUMAN                = 0x00000001,
   IVCP_OBJECT_VEHICLE              = 0x00000002,
   IVCP_OBJECT_UNKNOWN              = 0x00000004,
   IVCP_OBJECT_FACE                 = 0x00000008,
   IVCP_OBJECT_FLAME                = 0x00000010,
   IVCP_OBJECT_SMOKE                = 0x00000020,
   IVCP_OBJECT_WATER_LEVEL          = 0x00000040,
};

///////////////////////////////////////////////////////////////////////////////

// 카메라 특성 값

enum IVCP_CameraInfoParam
{
   IVCP_CAM_PTZ_CAMERA        = 0x00000001,  // PTZ 카메라
   IVCP_CAM_USE_MOBILE        = 0x00000002,  // 모바일용으로 권장하는 카메라
   IVCP_CAM_USE_AUDIO_OUT     = 0x00000004,  // 오디오 아웃 지원
   IVCP_CAM_USE_AUDIO_IN      = 0x00000008,  // 오디오 인 지원
};

// 카메라 설정 값

#ifndef IVCP_CAMERA_SETTING_FLAG
#define IVCP_CAMERA_SETTING_FLAG

enum IVCP_CameraSettingFlag
{
   IVCP30_ENABLE_AUTO_PTZ                = 0x0000000000000001, // 자동 추적 기능 (PTZ)
   IVCP30_ENABLE_AUTO_FOCUS              = 0x0000000000000002, // 자동 초점 기능 (Focus)
   IVCP30_ENABLE_PRESET_TOURING          = 0x0000000000000004, // 프리셋 투어링 기능
   IVCP30_ENABLE_VIDEO_ANALYTICS         = 0x0000000000000008, // 영상분석 On/Off
   IVCP30_PAUSE_VIDEO_ANALYTICS          = 0x0000000000000010, // 영상분석 일시중지 On/Off
   IVCP30_PAUSE_VIDEO_ANALYTICS_SCHEDULE = 0x0000000000000020, // 영상분석 스케쥴 일시중지 On/Off
   IVCP30_ENABLE_VIDEO_DEFOG             = 0x0000000000000040, // 영상 안개 개선 기능 On/Off
   IVCP30_ENABLE_VIDEO_STABILIZATION     = 0x0000000000000080, // 영상 안정화 기능 On/Off
   IVCP30_START_HOME_POS_VIDEO_ANALYTICS = 0X0000000000000100, // 홈 위치 비디오 분석 시작 (홈 위치와 상관없이)

   IVCP30_ENABLE_WIPER                   = 0x0000000000001000,
   IVCP30_ENABLE_HEATER                  = 0x0000000000002000,
   IVCP30_ENABLE_BLC_MODE                = 0x0000000000004000,
   IVCP30_ENABLE_ONE_SHOT_AUTO_FOCUS     = 0x0000000000008000,
   IVCP30_ENABLE_THERMAL                 = 0x0000000000010000,
   IVCP30_ENABLE_FAN                     = 0x0000000000020000,
   IVCP30_ENABLE_PTZ_LOCK                = 0x0000000000040000,
   
   IVCP30_ENABLE_PLATE_INSTANT           = 0x0000000000100000, // 번호판 인식 (고정)
   IVCP30_ENABLE_PLATE_LOCKEDOBJ         = 0x0000000000200000, // 번호판 인식 (마스터/슬래이브)
   IVCP30_ENABLE_FACE_INSTANT            = 0x0000000000400000, // 얼굴 감지 (고정)
   IVCP30_ENABLE_FACE_LOCKEDOBJ          = 0x0000000000800000, // 얼굴 감지 (마스터/슬래이브)};
};

#endif

// 카메라 설정 값 (아직 설계중)
#ifndef IVCP_CAMERA_COMMAND_FLAG
#define IVCP_CAMERA_COMMAND_FLAG

enum IVCP_CameraCommandFlag
{
   IVCP30_CAMERA_RESTART                   = 1,  // 카메라 재시작
   IVCP30_PTZ_POSITION_INIT                = 2,  // PTZ 위치 초기화
   IVCP30_PTZ_GOTO_HOME_POSITION           = 3,  // PTZ 홈 위치 이동
   IVCP30_SET_DEFOG_LEVEL                  = 4,  // 안개 개선 레벨
   IVCP30_SET_STABILIZATION_LEVEL          = 5,  // 영상 안정화 레벨
   IVCP30_SENSOR_INPUT                     = 6,  // 센서 입력
   IVCP30_GOTO_PRESET_ID                   = 7,  // 프리셋 이동
   IVCP30_GOTO_PRESET_TOURING              = 8,  // 프리셋 투어링 시작
   IVCP30_SET_IR_MODE                      = 9,  // IR 제어 - 0:Off, 1:On, 2:Schedule
   IVCP30_SET_DAY_NIGHT_MODE               = 10, // Day Night Mode - 0:Day, 1:Night, 2:Auto
   IVCP30_SET_AUTO_WHITE_BALANCE           = 11, // Auto White Balance - 0:Auto, 1:Manual, 2:Indoor, 3:Outdoor
};

#endif
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Metadata Info & Video Data Structures
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIvcpCameraInfo
//
///////////////////////////////////////////////////////////////////////////////

#define MAX_VSTREAMS                3
#define MAX_ASTREAMS                1

typedef struct
{
   // 카메라 기본 정보
   int   m_nCameraUID;                 // 카메라 고유 번호 (UID)
   char  m_strCameraGuid[128];         // 카메라 고유 값 (GUID)
   int   m_nCameraNo;                  // 로컬 채널 번호
   char  m_strCameraName[128];         // 카메라 이름
   int   m_nCameraType;                // 카메라 종류 (아날로그/IP카메라/파일)
   int   m_nCameraParam;               // 카메라 특성 (PTZ 카메라 여부 등)
   int   m_nWidth;                     // 원본 영상 너비
   int   m_nHeight;                    // 원본 영상 높이
   float m_fFrameRate;                 // 원본 프레임 레이트
   int   m_nVStreamNum;                // 비디오 스트리밍 개수
   int   m_nAStreamNum;                // 오디오 스트리밍 개수

   // 비디오 스트리밍 정보
   int   m_nVideoCodec[MAX_VSTREAMS];  // 압축 코덱
   int   m_nBitrate    [MAX_VSTREAMS]; // 비트레이트
   int   m_nVideoWidth[MAX_VSTREAMS];  // 영상 너비
   int   m_nVideoHeight[MAX_VSTREAMS]; // 영상 높이
   float m_fFrmRate[MAX_VSTREAMS];     // 영상 전송 프레임 레이트

   // 오디오 스트리밍 정보
   int   m_nAudioCodec[MAX_ASTREAMS];    // 오디오 코덱
   int   m_nChannels[MAX_ASTREAMS];      // 채널수
   int   m_nSamplingRate[MAX_ASTREAMS];  // 영상 너비
   int   m_nBitsPerSample[MAX_ASTREAMS]; // 영상 높이
}
CIvcpCameraInfo;

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIvcpLiveFrameInfo
//
///////////////////////////////////////////////////////////////////////////////

#define MAX_FRAME_OBJECT_NUM              100

typedef struct 
{
   int  m_nObjectID;                   // 객체 ID
   int  m_nObjectType;                 // 객체 종류 (사람, 자동차, 미확인)
   RECT m_ObjectBox;                   // 객체 좌표 (Proc 기준)
   int  m_nFlagLocked;                 // 추적중인지 여부
   int  m_nEventFlag;                  // 이벤트 발생 여부
} 
CIvcpFrameObject;

typedef struct 
{
   // 카메라 정보
   int      m_nCameraUID;            // 카메라 UID
   FILETIME m_ftFrameTime;           // 프레임 출력 시간
   int      m_nCameraState;          // 카메라 상태 정보
   int      m_nPTZState;             // PTZ 관련 정보
   int      m_nPTZStateEx;
   float    m_fFrameRate;            // 화면 프레임 레이트
   int      m_nProcWidth;            // 영상 분석 크기
   int      m_nProcHeight;
   // 객체 정보
   int              m_nObjectNum;                    // 현재 객체 개수
   CIvcpFrameObject m_Objects[MAX_FRAME_OBJECT_NUM]; // 프레임 객체 정보
} 
CIvcpFrameInfo;

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIvcpLiveFrameInfo
//
///////////////////////////////////////////////////////////////////////////////

#define MAX_FRAME_OBJECT_NUM              100
#define MAX_FRAME_EVENT_NUM               5

typedef struct 
{
   int   m_nObjectID;                   // 객체 ID
   int   m_nObjectType;                 // 객체 종류 (사람, 자동차, 미확인)
   RECT  m_ObjectBox;                   // 객체 좌표 (Proc 기준)
   int   m_nObjectStatus;               // 객체 상태 (추적 여부)
   float m_fArea;
   float m_fWidth;
   float m_fHeight;
   float m_fMajorAxisLen;
   float m_fMinorAxisLen;
   float m_fSpeed;
   float m_fRealArea;
   float m_fRealWidth;
   float m_fRealHeight;
   float m_fRealMajorAxisLen;
   float m_fRealMinorAxisLen;
   float m_fRealSpeed;
   float m_fNorSpeed;
   int   m_nEventNum;
   int   m_nEventIDs[MAX_FRAME_EVENT_NUM];
   int   m_nEventTypes[MAX_FRAME_EVENT_NUM];
} 
CIvcpFrameObjectEx;

typedef struct 
{
   // 카메라 정보
   int      m_nCameraUID;            // 카메라 UID
   char     m_strCameraGuid[128];      // 카메라 Guid
   FILETIME m_ftFrameTime;           // 프레임 출력 시간
   float    m_fFrameRate;            // 화면 프레임 레이트
   int      m_nProcWidth;            // 영상 분석 크기
   int      m_nProcHeight;
   // 객체 정보
   int              m_nObjectNum;                    // 현재 객체 개수
   CIvcpFrameObjectEx m_Objects[MAX_FRAME_OBJECT_NUM]; // 프레임 객체 정보
} 
CIvcpFrameObjectInfo;

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIvcpEventInfo
//
///////////////////////////////////////////////////////////////////////////////

typedef struct
{
   int        m_nCameraUID;            // 카메라 UID
   FILETIME   m_ftStartTime;           // 이벤트 시작 시간
   FILETIME   m_ftEndTime;             // 이벤트 종료 시간
   int        m_nEventStatus;          // 이벤트 상태 (0: 시작, 1: 종료)
   int        m_nEventID;              // 이벤트 ID (시작-종료을 매칭시킬때 사용)
   int        m_nEventType;            // 이벤트 종류
   int        m_nEventRuleID;          // 이벤트 룰 ID
   int        m_nObjectID;             // 객체 ID (출력,저장용)
   int        m_nObjectType;           // 객체 종류 (사람, 차량, 미확인)
   RECT       m_ObjectBox;             // 객체 위치 (아래 Proc 좌표 기준)
   int        m_nEventZoneID;          // 발생한 이벤트 존 ID
   char       m_strEventZoneName[128]; // 발생한 이벤트 존 이름

   int        m_nProcWidth;            // 분석 영상 크기
   int        m_nProcHeight;
   BOOL       m_bThumbnail;            // 섬네일 첨부 여부
   int        m_nThumbCodecID;         // 섬네일 코덱 ID
   int        m_nThumbWidth;           // 섬네일 크기
   int        m_nThumbHeight;
   int        m_nThumbLen;             // 섬네일 데이터 크기
   BYTE      *m_pThumbBuffer;          // 섬네일 버퍼

   // (mkjang-140509)
   BOOL       m_bEvtFullImg;           // 이벤트 전체 이미지 첨부 여부
   int        m_nEvtFullImgWidth;      // 이벤트 전체 이미지 크기
   int        m_nEvtFullImgHeight;
   int        m_nEvtFullImgLen;        // 이벤트 전체 이미지 데이터 크기   
   BYTE      *m_pEvtFullImgBuffer;     // 이벤트 전체 이미지 버퍼
}
CIvcpEventInfo;

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIvcpObjectInfo
//
///////////////////////////////////////////////////////////////////////////////

typedef struct
{
   int        m_nCameraUID;            // 카메라 UID
   FILETIME   m_ftStartTime;           // 객체 나타난 시간
   FILETIME   m_ftEndTime;             // 객체 사라진 시간
   int        m_nObjectID;             // 객체 ID (시작-종료을 매칭시킬때 사용)
   int        m_nObjectType;           // 객체 종류
   int        m_nObjectStatus;         // 상태 (0: 시작, 1: 종료)
   RECT       m_ObjectBox;             // 객체 위치 (아래 Proc 좌표 기준)
   // Average
   float      m_fAvgArea;
   float      m_fAvgWidth;
   float      m_fAvgHeight;
   float      m_fAvgAspectRatio;
   float      m_fAvgAxisLengthRatio;
   float      m_fAvgMajorAxisLength;
   float      m_fAvgMinorAxisLength;
   float      m_fAvgNorSpeed;
   float      m_fAvgSpeed;
   float      m_fAvgRealArea;
   float      m_fAvgRealDistance;
   float      m_fAvgRealWidth;
   float      m_fAvgRealHeight;
   float      m_fAvgRealMajorAxisLength;
   float      m_fAvgRealMinorAxisLength;
   float      m_fAvgRealSpeed;
   // Max
   int        m_nMaxWidth;
   int        m_nMaxHeight;
   float      m_fMaxArea;
   float      m_fMaxAspectRatio;
   float      m_fMaxAxisLengthRatio;
   float      m_fMaxMajorAxisLength;
   float      m_fMaxMinorAxisLength;
   float      m_fMaxNorSpeed;
   float      m_fMaxSpeed;
   float      m_fMaxRealDistance;
   float      m_fMaxRealSpeed;
   // Min
   int        m_nMinWidth;
   int        m_nMinHeight;
   float      m_fMinArea;
   float      m_fMinAspectRatio;
   float      m_fMinAxisLengthRatio;
   float      m_fMinMajorAxisLength;
   float      m_fMinMinorAxisLength;
   float      m_fMinNorSpeed;
   float      m_fMinSpeed;
   float      m_fMinRealDistance;
   float      m_fMinRealSpeed;
   // Color Histogram
   float      m_fHistogram[10];        // 색상 히스토그램 (대표색상 10가지 값)

   int        m_nProcWidth;            // 분석 영상 크기
   int        m_nProcHeight;
   BOOL       m_bThumbnail;            // 섬네일 첨부 여부
   int        m_nThumbCodecID;         // 섬네일 코덱 ID
   int        m_nThumbWidth;           // 섬네일 크기
   int        m_nThumbHeight;
   int        m_nThumbLen;             // 섬네일 데이터 크기
   BYTE      *m_pThumbBuffer;          // 섬네일 버퍼

   // (mkjang-140509)
   BOOL       m_bObjFullImg;           // 이벤트 전체 이미지 첨부 여부
   int        m_nObjFullImgWidth;      // 이벤트 전체 이미지 크기
   int        m_nObjFullImgHeight;
   int        m_nObjFullImgLen;        // 이벤트 전체 이미지 데이터 크기   
   BYTE      *m_pObjFullImgBuffer;     // 이벤트 전체 이미지 버퍼
}
CIvcpObjectInfo;

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIvcpPlateInfo
//
///////////////////////////////////////////////////////////////////////////////

typedef struct 
{
   int        m_nCameraUID;         // 카메라 UID
   FILETIME   m_ftStartTime;        // 시작 시간
   FILETIME   m_ftEndTime;          // 종료 시간
   int        m_nStatus;            // 상태 (0: 시작, 1: 종료)
   char       m_strPlateName[20];   // 번호판 문자열
   int        m_nPlateID;           // ID (시작-종료을 매칭시킬때 사용)
   int        m_nPlateType;         // 번호판 종류
   RECT       m_PlateBox;           // 객체 위치 (아래 Proc 좌표 기준)
   int        m_nProcWidth;         // 분석 영상 크기
   int        m_nProcHeight;
   BOOL       m_bThumbnail;         // 섬네일 첨부 여부
   BOOL       m_bPicture;           // 이미지 첨부 여부 (JPG)

   int        m_nThumbCodecID;      // 섬네일 코덱 ID
   int        m_nThumbWidth;        // 섬네일 크기
   int        m_nThumbHeight;
   int        m_nMainThumbLen;      // 섬네일 데이터 (전체 이미지)
   int        m_nSubThumbLen;       // 섬네일 데이터 (번호판 이미지)
   int        m_nPictureLen;        // 이미지 데이터 (원본 이미지)

   BYTE      *m_pMainThumbnail;     // 섬네일 버퍼
   BYTE      *m_pSubThumbnail;      // 
   BYTE      *m_pPictureBuffer;     // 이미지 버퍼
}
CIvcpPlateInfo;

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIvcpFaceInfo
//
///////////////////////////////////////////////////////////////////////////////

typedef struct 
{
   int        m_nCameraUID;         // 카메라 UID
   FILETIME   m_ftStartTime;        // 시작 시간
   FILETIME   m_ftEndTime;          // 종료 시간
   int        m_nFaceID;            // ID
   char       m_strName[20];        // 사람 이름
   int        m_nAge;               // 나이
   int        m_nSex;               // 성별 
   char       m_strPhoneNum[20];    // 전화번호
   float      m_fRecognRate;        // 인식률
   BOOL       m_bThumbnail;         // 섬네일 첨부 여부
   BOOL       m_bPicture;           // 이미지 첨부 여부 (JPG)

   int        m_nThumbCodecID;      // 섬네일 코덱 ID
   int        m_nThumbWidth;        // 섬네일 크기
   int        m_nThumbHeight;
   int        m_nThumbLen;          // 섬네일 데이터 크기
   int        m_nPictureLen;        // 이미지 데이터 크기
   
   BYTE      *m_pThumbBuffer;       // 섬네일 버퍼
   BYTE      *m_pPictureBuffer;     // 이미지 버퍼
}
CIvcpFaceInfo;

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIvcpPTZInfoData
//
///////////////////////////////////////////////////////////////////////////////

typedef struct
{
   int      m_nCameraUID;     // 카메라 UID
   FILETIME m_ftFrameTime;    // 메타 발생 시간
   float    m_fPanAngle;      // Pan 각도 : 반시계 방향이 증가하는 방향이다. 단위(degree, 0~360도)
   float    m_fTiltAngle;     // Tilt 각도 : 아래로 향하는 방향이 증가하는 방향임. 단위(degree, 0~360도)
   float    m_fZoomFactor;    // Zoom 값 : 현재 줌 위치를 나타냄. 18배줌 카메라이면 1~18사이의 값을 가짐.
   float    m_fFOV;           // FOV (Field Of View) : Pan 축으로의 화각을 나타냄. 단위(degree, 0~360도)
}
CIvcpPTZInfoData;

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIvcpPresetIdInfo
//
///////////////////////////////////////////////////////////////////////////////

typedef struct
{
   int      m_nCameraUID;        // 카메라 UID
   FILETIME m_ftFrameTime;       // 메타 발생 시간
   int      m_nCurPresetID;      // 현재 프리셋 ID
}
CIvcpPresetIdInfo;

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIvcpEventZoneInfo
//
///////////////////////////////////////////////////////////////////////////////

#define MAX_EVTZONE_NUM               20
#define MAX_ZONE_POINT_NUM            20

typedef struct
{
   float m_fDetectionTime;
}
CIvcpEventZoneDuration;

typedef struct
{
   float m_fDetectionTime;
   float m_fDirection;
   float m_fRange;
}
CIvcpEventZoneDirectionalMotion;

typedef struct
{
   int   m_nDirection;
   float m_fDetectionTime;
   float m_fPassingTime;
   int   m_nAreaNum;
   int   m_nAreaID[MAX_EVTZONE_NUM];
   POINT m_ptAreaCenter[MAX_EVTZONE_NUM];
   int   m_nAreaVerticeNum[MAX_EVTZONE_NUM];
   POINT m_AreaVertices[MAX_EVTZONE_NUM][MAX_ZONE_POINT_NUM];
   int   m_nArrowNum;
   int   m_nArrowZoneID;
   POINT m_ptArrowStart[MAX_EVTZONE_NUM];
   POINT m_ptArrowEnd[MAX_EVTZONE_NUM];
}
CIvcpEventZonePathPassing;

///////////////////////////////////////

typedef struct
{
   int         m_nZoneID;           // 존 ID
   int         m_nPresetID;         // 프리셋 ID
   int         m_nPriority;         // 우선순위 (높을수록 높음)
   int         m_nOptions;        
   int         m_nWidth;            // 기준 화면 크기
   int         m_nHeight;
   char        m_strName[128];      // 존 이름
   POINT       m_ptCenter;          // 존 중심점
   int         m_nVerticesNum;       // 점 개수
   POINT       m_Vertices[MAX_ZONE_POINT_NUM];  // 점들
   // 룰 정보
   int         m_nRuleOptions;      // 룰 옵션
   int         m_nRuleEventType;    // 룰 이벤트 종류
   int         m_nRuleObjectType;   // 룰 객체 종류
   char        m_strRuleName[128];  // 룰 이름
   LPVOID      m_pExtraInfo;        // 추가 정보
}
CIvcpEventZone;

///////////////////////////////////////////////////////////////////////////////

typedef struct
{
   int            m_nCameraUID;           // 카메라 UID
   FILETIME       m_ftFrameTime;          // 메타 발생 시간
   int            m_nWidth;               // 기준 화면 크기
   int            m_nHeight;

   int            m_nZoneNum;                // 이벤트 존 개수 
   CIvcpEventZone m_Zones[MAX_EVTZONE_NUM];  // 이벤트 존 정보
}
CIvcpEventZoneInfo;

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIvcpEventZoneCountInfo
//
///////////////////////////////////////////////////////////////////////////////

typedef struct
{
   int      m_nCameraUID;        // 카메라 UID
   FILETIME m_ftFrameTime;       // 메타 발생 시간
   int      m_nZoneNum;          // 카운터가 발생한 이벤트 존 개수
   int      m_nZoneID[MAX_EVTZONE_NUM];      // 이벤트 존 ID
   int      m_nZoneCount[MAX_EVTZONE_NUM];   // 이벤트 존 카운터
}
CIvcpEventZoneCountInfo;

//////////////////////////////////////////////////////////////////////////////
//
// (META) CIvcpPresetMapInfo
//
///////////////////////////////////////////////////////////////////////////////

#define MAX_PRESET_NUM  100
#define MAX_GROUP_NUM   100

typedef struct
{
   int      m_nID;           // 프리셋 ID
   char     m_strName[256];  // 프리셋 이름
}
CIvcpPresetItem;

typedef struct
{
   int      m_nID;
   char     m_strName[256];         // 그룹 이름
   int      m_nCycleType;
   int      m_nPresetNum;           // 프리셋 개수
   CIvcpPresetItem *m_pPresetItems; // 프리셋 정보
}
CIvcpPresetGroup;

typedef struct
{
   int      m_nCameraUID;            // 카메라 UID
   FILETIME m_ftFrameTime;           // 메타 발생 시간
   int      m_nGroupNum;             // 그룹 개수
   CIvcpPresetGroup *m_pGroupItems;  // 그룹 정보
}
CIvcpPresetMapInfo;

//////////////////////////////////////////////////////////////////////////////
//
// (META) CIvcpSettingInfo
//
///////////////////////////////////////////////////////////////////////////////

typedef struct
{
   int      m_nCameraUID;        // 카메라 UID
   FILETIME m_ftFrameTime;       // 메타 발생 시간
   UINT64   m_nSettingMask;      // 세팅 마스크
   UINT64   m_nSettingFlag;      // 세팅 플래그
}
CIvcpSettingInfo;

//////////////////////////////////////////////////////////////////////////////
//
// (DATA) CIvcpVideoStreamData
//
///////////////////////////////////////////////////////////////////////////////

typedef struct
{
   int      m_nCameraUID;        // 카메라 UID
   int      m_nCodecID;
   int      m_nWidth;            // 이미지 크기
   int      m_nHeight;
   float    m_fFrameRate;        // 전송 프레임 속도
   int      m_nFrameFlag;        // 프레임 정보 (0:P-frame, 1:I-Frame)
   FILETIME m_ftFrameTime;       // 프레임 타임스탬프
   FILETIME m_ftPlayTime;        // 재생 시간 - 재생에서만 사용 (플레이어 싱크용)
   int      m_nBuffLen;          // 데이터 크기

   BYTE    *m_pBuffer;           // 데이터 포인터
}
CIvcpVideoStreamData;

//////////////////////////////////////////////////////////////////////////////
//
// (DATA) CIvcpAudioStreamData
//
///////////////////////////////////////////////////////////////////////////////

typedef struct
{
   int      m_nCameraUID;        // 카메라 UID
   int      m_nCodecID;
   int      m_nChannel;
   int      m_nBitPerSamples;
   int      m_nSamplingRate;
   FILETIME m_ftFrameTime;       // 프레임 타임스탬프
   FILETIME m_ftPlayTime;        // 재생 시간 (현재는 사용안함)
   int      m_nBuffLen;

   BYTE    *m_pBuffer;           // 데이터 포인터
}
CIvcpAudioStreamData;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Request Structures
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIvcpSearchEventReq
//
///////////////////////////////////////////////////////////////////////////////

#define MAX_EVTZONE_ID_NUM            20

typedef struct
{
   FILETIME m_ftStart;           // 검색 시작 기간
   FILETIME m_ftEnd;             // 검색 종료 기간
   int      m_nEvtFlag;          // 이벤트 플래그
   int      m_nObjFlag;          // 객체 플래그
   int      m_nEvtZoneNum;       // 검색할 이벤트 존 개수
   int      m_EvtZoneIDs[MAX_EVTZONE_ID_NUM]; // 검색할 이벤트 존 정보
   BOOL     m_bThumbnail;        // 섬네일 전송 여부
}
CIvcpSearchEventReq;

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIvcpSearchObjectReq
//
///////////////////////////////////////////////////////////////////////////////

#define SEARCH_SELECT_COLOR_MAX        6  // 대표 색상 최대 선택 수

typedef struct
{
   int         m_nColorType;           // 대표 색상(0) or 히스토그램(1)
   int         m_nColorMode;           // 조건 AND, OR
   int         m_nColorSensibility;    // 색상 매칭 민감도

   int         m_nColorNum;                         // 대표 색상 개수
   COLORREF    m_crColor[SEARCH_SELECT_COLOR_MAX];  // 대표 색상
   int         m_nPercent[SEARCH_SELECT_COLOR_MAX]; // 점유률
   
   int         m_nHistogramLen;
   float       m_fHistogram[10];        // 색상 히스토그램 (대표색상 10가지 값)
}
CIvcpSearchColorFilter;

enum IVCP_SearchObjectFilterOption
{
   IVCP_FO_FILTER_ENABLE     = 0x00000001, // 객체 필터 사용 여부 (not used)
   IVCP_FO_REAL_SIZE_VALUES  = 0x00000002, // 실측치를 사용할지 여부 (미터 or 픽셀)
   IVCP_FO_WIDTH             = 0x00000004, // 너비
   IVCP_FO_HEIGHT            = 0x00000008, // 높이
   IVCP_FO_AREA              = 0x00000010, // 면적
   IVCP_FO_ASPECT_RATIO      = 0x00000020, // 종횡비
   IVCP_FO_AXIS_LENGTH_RATIO = 0x00000040, // 축 비율
   IVCP_FO_MAJOR_AXIS_LENGTH = 0x00000080, // 장축 길이
   IVCP_FO_MINOR_AXIS_LENGTH = 0x00000100, // 단축 길이
   IVCP_FO_NORMALIZED_SPEED  = 0x00000200, // 정규화 속도
   IVCP_FO_SPEED             = 0x00000400, // 속도
   IVCP_FO_REAL_DISTANCE     = 0x00000800,
};

typedef struct
{
public:
   int   m_nOptions;
   float m_fMinWidth;
   float m_fMaxWidth;
   float m_fMinHeight;
   float m_fMaxHeight;
   float m_fMinArea;
   float m_fMaxArea;
   float m_fMinAspectRatio;
   float m_fMaxAspectRatio;
   float m_fMinAxisLengthRatio;
   float m_fMaxAxisLengthRatio;
   float m_fMinMajorAxisLength;
   float m_fMaxMajorAxisLength;
   float m_fMinMinorAxisLength;
   float m_fMaxMinorAxisLength;
   float m_fMinNorSpeed;
   float m_fMaxNorSpeed;
   float m_fMinSpeed;
   float m_fMaxSpeed;
   float m_fMinRealArea;
   float m_fMaxRealArea;
   float m_fMinRealDistance;
   float m_fMaxRealDistance;
   float m_fMinRealWidth;
   float m_fMaxRealWidth;
   float m_fMinRealHeight;
   float m_fMaxRealHeight;
   float m_fMinRealMajorAxisLength;
   float m_fMaxRealMajorAxisLength;
   float m_fMinRealMinorAxisLength;
   float m_fMaxRealMinorAxisLength;
   float m_fMinRealSpeed;
   float m_fMaxRealSpeed;
}
CIvcpSearchObjectFilter;

///////////////////////////////////////////////////////////////////////////////

/*
enum IVCP_SearchObjectCondition
{
   SEARCH_USE_COLOR     = 0x00000001,
   SEARCH_USE_OBJTYPE   = 0x00000002,
   SEARCH_USE_OBJFILTER = 0x00000004,
};
*/

typedef struct
{
   FILETIME m_ftStart;        // 검색 시작 기간
   FILETIME m_ftEnd;          // 검색 종료 기간
   int      m_nObjID;         // 객체 아이디 (mkjang-140926)
   int      m_nCondFlag;      // 검색 조건에 대한 플래그

   int                     m_nObjFlag;      // 객체 종류
   CIvcpSearchColorFilter  m_ColorFilter;   // 색상 필터
   CIvcpSearchObjectFilter m_ObjectFilter;  // 객체 필터
   BOOL     m_bThumbnail;     // 섬네일 전송 여부
}
CIvcpSearchObjectReq;

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIvcpSearchFrameReq
//
///////////////////////////////////////////////////////////////////////////////

typedef struct
{
public:
   FILETIME m_ftStart;              // 검색 시작 기간
   FILETIME m_ftEnd;                // 검색 종료 기간
}
CIvcpSearchFrameReq;

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIvcpSearchPlateReq
//
///////////////////////////////////////////////////////////////////////////////

typedef struct
{
public:
   FILETIME m_ftStart;              // 검색 시작 기간
   FILETIME m_ftEnd;                // 검색 종료 기간
   char     m_strPlateNumber[32];   // 검색 번호판 
   BOOL     m_bUseColor;            // 색상 검색 사용
   COLORREF m_crColor;              // 검색 색상
   BOOL     m_bThumbnail;           // 섬네일 전송 여부
}
CIvcpSearchPlateReq;

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIvcpSearchFaceReq
//
///////////////////////////////////////////////////////////////////////////////

typedef struct
{
   FILETIME m_ftStart;           // 검색 시작 기간
   FILETIME m_ftEnd;             // 검색 종료 기간
   char     m_strName[64];       // 사람 이름
   BOOL     m_bThumbnail;        // 섬네일 전송 여부
}
CIvcpSearchFaceReq;

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIvcpSearchLogReq
//
///////////////////////////////////////////////////////////////////////////////

typedef struct
{
   FILETIME m_ftStart;           // 검색 시작 기간
   FILETIME m_ftEnd;             // 검색 종료 기간
   char     m_strKeyword[128];   // 검색 키워드
   int      m_nOption;           // 검색 옵션 플래그
   int      m_nEvtFlag;          // 이벤트 플래그
   int      m_nObjFlag;          // 객체 플래그
   BOOL     m_bThumbnail;        // 섬네일 전송 여부
}
CIvcpSearchLogReq;

//////////////////////////////////////////////////////////////////////////////
//
// (DATA) CIvcpTalkAudioData
//
///////////////////////////////////////////////////////////////////////////////

typedef struct
{
   int      m_nCameraUID;        // 카메라 UID
   int      m_nTalkID;           // 오디오방송 ID
   int      m_nCodecID;
   int      m_nChannel;
   int      m_nBitPerSamples;
   int      m_nSamplingRate;

   int      m_nBuffLen;
   BYTE    *m_pBuffer;           // 데이터 포인터
}
CIvcpTalkAudioData;

//////////////////////////////////////////////////////////////////////////////
//
// (DATA) CIvcpSnapShotData
//
///////////////////////////////////////////////////////////////////////////////

typedef struct
{
   int      m_nCameraUID;        // 카메라 UID
   int      m_nCodecID;
   int      m_nChannel;
   int      m_nWidth;
   int      m_nHeight;

   int      m_nBuffLen;
   BYTE    *m_pBuffer;           // 데이터 포인터
}
CIvcpSnapShotData;
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Event Handler Structures
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// CIvcpSystemEventHandler
//
///////////////////////////////////////////////////////////////////////////////

typedef struct
{
   void (*OnConnectionOK)       (const char *strSystemName, int nCameraNum, CIvcpCameraInfo *pCamList, LPVOID pParam);
   void (*OnReconnected)        (const char *strSystemName, int nCameraNum, CIvcpCameraInfo *pCamList, LPVOID pParam);
   void (*OnConnectionFail)     (int nError, const char *strError, LPVOID pParam);
   void (*OnDisconnected)       (LPVOID pParam);
   void (*OnResponse)           (int nReqID, LPVOID pResult, LPVOID pParam);
   void (*OnChanged)            (int nType, int nParam, const char *strParam, LPVOID pParam);

   void (*OnMonitorInfo)        (int nType, LPVOID pInfo, LPVOID pParam);

   void (*OnSearchProgress)     (int nStep, int nParam, LPVOID pParam);
   void (*OnSearchMetadataInfo) (int nType, LPVOID pInfo, LPVOID pParam);

   void (*OnTalkStatus)         (int nStatus, LPVOID pParam);
   void (*OnTalkAudioData)      (CIvcpTalkAudioData *pData, LPVOID pParam);
}
CIvcpSystemEventHandler;

///////////////////////////////////////////////////////////////////////////////
//
// CIvcpChannelEventHandler
//
///////////////////////////////////////////////////////////////////////////////

typedef struct
{
   void (*OnConnectionOK)       (CIvcpCameraInfo *pCameraInfo, LPVOID pParam);
   void (*OnReconnected)        (CIvcpCameraInfo *pCameraInfo, LPVOID pParam);
   void (*OnConnectionFail)     (int nError, const char *strError, LPVOID pParam);
   void (*OnDisconnected)       (LPVOID pParam);
   void (*OnResponse)           (int nReqID, LPVOID pResult, LPVOID pParam);
   void (*OnChanged)            (int nType, int nParam, const char *strParam, LPVOID pParam);

   void (*OnLiveMetadataInfo)   (int nType, LPVOID pInfo, LPVOID pParam);
   void (*OnLiveVideoData)      (CIvcpVideoStreamData *pData, LPVOID pParam);
   void (*OnLiveAudioData)      (CIvcpAudioStreamData *pData, LPVOID pParam);
   void (*OnLiveStatus)         (int nType, int nParam, LPVOID pParam);   

   void (*OnPlayMetadataInfo)   (int nType, LPVOID pInfo, LPVOID pParam);
   void (*OnPlayVideoData)      (CIvcpVideoStreamData *pData, LPVOID pParam);
   void (*OnPlayAudioData)      (CIvcpAudioStreamData *pData, LPVOID pParam);
   void (*OnPlayProgress)       (int nStep, int nParam, LPVOID pParam);   

   void (*OnSearchProgress)     (int nStep, int nParam, LPVOID pParam);
   void (*OnSearchMetadataInfo) (int nType, LPVOID pInfo, LPVOID pParam);

   void (*OnTalkStatus)         (int nStatus, LPVOID pParam);
   void (*OnTalkAudioData)      (CIvcpTalkAudioData *pData, LPVOID pParam);
   void (*OnSnapShotData)      (CIvcpSnapShotData *pData, LPVOID pParam);
}
CIvcpChannelEventHandler;

};

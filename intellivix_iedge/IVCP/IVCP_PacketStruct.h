﻿///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2013 illisis. Inc. All rights reserved.
//
// FileName: IVCP_PacketStruct.h
//
// Function: Data structure using C2XML for IVCP 3.0.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "IVCP_Define.h"
#include "IVCP_Packet.h"

#include "bccl.h"    // for CXMLIO

#define __IVCP_SUPPORT_FACE_RECOGNITION
//#define __IVCP_SUPPORT_EVENT_IPEX	//[jmlee 2015 02 04] 비상벨 IPEX 연동 추가
#define __IVCP_SUPPORT_MMS
#ifdef __IVCP_SUPPORT_MMS
#include <list>
#include <vector>
#endif

#ifndef OUT
#define OUT
#endif

///////////////////////////////////////////////////////////////////////////////
//
//  IVCP 3.0
//
///////////////////////////////////////////////////////////////////////////////

typedef UINT64   CHFLAG;   // 채널 별 녹화정보를 담는 타입

///////////////////////////////////////////////////////////////////////////////
//
// Enumerations (PacketType)
//
///////////////////////////////////////////////////////////////////////////////

enum IVCP30_PacketType
{
   IVCP_TYPE_UNKNOWN = 0,
   IVCP_TYPE_CONN,      // 연결
   IVCP_TYPE_SYSTEM,    // 시스템
   IVCP_TYPE_CAMERA,    // 카메라
   IVCP_TYPE_PTZ,       // PTZ 제어
   IVCP_TYPE_LIVE,      // 실시간 데이터
   IVCP_TYPE_PLAY,      // 재생 데이터
   IVCP_TYPE_SEARCH,    // 검색 관련
   IVCP_TYPE_EXPORT,    // 내보내기 관련
   IVCP_TYPE_FACE,      // 얼굴 인식 데이터
   IVCP_TYPE_MMS,       // 통합관리서버(MMS) 접속게이트웨이
   IVCP_TYPE_IPEX,      // [jmlee 2015 02 04] 비상벨 연동 IPEX 추가
   IVCP_TYPE_AC,        // AC 출입 연동
};

enum IVCP30_BodyType
{
   IVCP_TYPE_XML = 0,   // XML 텍스트
   IVCP_TYPE_TXT,       // Text 형태
   IVCP_TYPE_BIN,       // Binary 형태
   IVCP_TYPE_XMZ,       // XML 압축 (with Zip)
};

///////////////////////////////////////////////////////////////////////////////
//
// Enumerations (PacketID)
//
///////////////////////////////////////////////////////////////////////////////

enum IVCP30_PacketID
{
   IVCP_ID_Unknown = 0,
   IVCP_ID_Conn_OptionsReq = 1,           // 시스템의 종류,버전과 카메라 개수를 읽어옵니다.
   IVCP_ID_Conn_OptionsRes,
   IVCP_ID_Conn_LoginReq,                 // 로그인을 요청합니다. 옵션으로 시스템 정보를 요청합니다.
   IVCP_ID_Conn_LoginRes,
   IVCP_ID_Conn_LogoutReq,                // 로그아웃을 요청합니다.
   IVCP_ID_Conn_LogoutRes,
   IVCP_ID_Conn_CameraListReq,            // 카메라 목록 정보를 요청합니다.
   IVCP_ID_Conn_CameraListRes,
   IVCP_ID_Conn_StartUpReq,               // 비디오/메타데이터 전송을 전체 시작한다.
   IVCP_ID_Conn_StartUpRes,
   IVCP_ID_Conn_ShutDownReq,              // 비디오/메타데이터 전송을 전체 중지한다.
   IVCP_ID_Conn_ShutDownRes,
   IVCP_ID_Conn_SnapshotReq,              // 초기 접속시 카메라 스냅 화면 요청
   IVCP_ID_Conn_SnapshotRes,

   IVCP_ID_Camera_LiveMetadataReq,        // 카메라 메타데이터 전송을 요청합니다.
   IVCP_ID_Camera_LiveMetadataRes,
   IVCP_ID_Camera_LiveVideoReq,           // 카메라 비디오 전송을 요청합니다.
   IVCP_ID_Camera_LiveVideoRes,


   IVCP_ID_Camera_PlayStreamStartReq,     // 재생 영상 전송을 요청합니다.
   IVCP_ID_Camera_PlayStreamStartRes,
   IVCP_ID_Camera_PlayStreamStopReq,      // 재생 영상 전송을 중지합니다.
   IVCP_ID_Camera_PlayStreamStopRes,
   IVCP_ID_Camera_PlayStreamControlReq,   // 재생 영상 전송을 제어합니다.
   IVCP_ID_Camera_PlayStreamControlRes,
   IVCP_ID_Camera_PlayDataStreamReq,	  //플레이 데이타 스트림요청 모든 프레임을 최고 배속으로 전송
   IVCP_ID_Camera_PlayDataStreamRes,
   IVCP_ID_Camera_PlayDataCtrlReq,		  //플레이 데이타 스트림 컨트롤 일시정지, 재시작, 정지 등 컨트롤 세팅
   IVCP_ID_Camera_PlayDataCtrlRes,
   IVCP_ID_Camera_CarPlateRecognitionReq, //번호판 인식 요청 함수
   IVCP_ID_Camera_CarPlateRecognitionRes,

   IVCP_ID_Camera_SnapshotReq,            // 카메라 스냅 화면 요청
   IVCP_ID_Camera_SnapshotRes,
   IVCP_ID_Camera_SettingReq,             // 카메라 관련 세팅을 조절합니다.
   IVCP_ID_Camera_SettingRes,
   IVCP_ID_Camera_CommandReq,             // 카메라 관련 명령을 요청합니다.
   IVCP_ID_Camera_CommandRes,
   IVCP_ID_Camera_TalkAudioReq,           // 오디오 방송을 설정합니다.
   IVCP_ID_Camera_TalkAudioRes,
   IVCP_ID_Camera_TalkStatus,             // 오디오 방송 상태 전달
   IVCP_ID_Camera_SendAudioData,          // 오디오 방송 업로드 데이터
   IVCP_ID_Camera_RecvAudioData,          // 오디오 방송 다운로드 데이터


   IVCP_ID_Ptz_ContMoveReq,               // PTZ 연속 이동 제어
   IVCP_ID_Ptz_ContMoveRes,
   IVCP_ID_Ptz_AbsMoveReq,                // PTZ 절대각 제어
   IVCP_ID_Ptz_AbsMoveRes,
   IVCP_ID_Ptz_BoxMoveReq,                // PTZ 박스 이동 제어 (화면에서의 포인트)
   IVCP_ID_Ptz_BoxMoveRes,
   IVCP_ID_Ptz_GetAbsPosReq,              // PTZ 절대각 읽기
   IVCP_ID_Ptz_GetAbsPosRes,


   IVCP_ID_Live_FrameInfo,                // 프레임 메타데이터 (실시간)
   IVCP_ID_Live_EventInfo,                // 이벤트 메타데이터
   IVCP_ID_Live_ObjectInfo,               // 객체 메타데이터
   IVCP_ID_Live_PlateInfo,                // 번호판 메타데이터
   IVCP_ID_Live_FaceInfo,                 // 얼굴 메타데이터
   IVCP_ID_Live_LogInfo,                  // 로그 정보
   IVCP_ID_Live_PtzDataInfo,              // PTZ 정보 메타데이터
   IVCP_ID_Live_PresetIdInfo,             // PresetID 메타데이터
   IVCP_ID_Live_EventZoneInfo,            // 이벤트존 메타데이터
   IVCP_ID_Live_EventZoneCount,           // 이벤트존 카운트 메타데이터
   IVCP_ID_Live_PresetMapInfo,            // 프리셋 맵 정보
   IVCP_ID_Live_SettingInfo,              // 카메라 설정 정보
   IVCP_ID_Live_VideoData,                // 영상 수신 데이터
   IVCP_ID_Live_AudioData,                // 오디오 수신 데이터
   IVCP_ID_Live_FrmObjInfo,               // 프레임 객제 메터데이터 (SDS 지원용 - 빅데이터용)
   IVCP_ID_Live_Status,                   // 실시간 상태 정보
   IVCP_ID_Live_ACInfo,                   // 실시간 AC 제어
   IVCP_ID_Live_HeatMap,                  // 통계 데이터 HeatMap
   IVCP_ID_Live_StatisticsInfo,           // 통계 데이터 이벤트 정보


   IVCP_ID_Play_FrameInfo,
   IVCP_ID_Play_EventInfo,
   IVCP_ID_Play_ObjectInfo,
   IVCP_ID_Play_PlateInfo,
   IVCP_ID_Play_FaceInfo,
   IVCP_ID_Play_PtzDataInfo,
   IVCP_ID_Play_PresetIdInfo,
   IVCP_ID_Play_EventZoneInfo,
   IVCP_ID_Play_EventZoneCount,
   IVCP_ID_Play_VideoData,
   IVCP_ID_Play_AudioData,
   IVCP_ID_Play_Progress,


   IVCP_ID_Search_FrameReq,               // 프레임 검색을 요청합니다.
   IVCP_ID_Search_FrameRes,
   IVCP_ID_Search_EventReq,               // 이벤트 검색을 요청합니다.
   IVCP_ID_Search_EventRes,
   IVCP_ID_Search_ObjectReq,              // 객체 검색을 요청합니다.
   IVCP_ID_Search_ObjectRes,
   IVCP_ID_Search_PlateReq,               // 번호판 검색을 요청합니다.
   IVCP_ID_Search_PlateRes,
   IVCP_ID_Search_FaceReq,                // 얼굴 검색을 요청합니다.
   IVCP_ID_Search_FaceRes,
   IVCP_ID_Search_LogReq,                 // 로그 검색을 요청합니다.
   IVCP_ID_Search_LogRes,
   IVCP_ID_Search_StopReq,                // 검색을 중지합니다. (통합)
   IVCP_ID_Search_StopRes,


   IVCP_ID_Search_Progress,               // 검색 진행 과정
   IVCP_ID_Search_FrameInfo,              // 프레임 검색 결과
   IVCP_ID_Search_EventInfo,              // 이벤트 검색 결과
   IVCP_ID_Search_ObjectInfo,             // 객체 검색 결과
   IVCP_ID_Search_PlateInfo,              // 번호판 검색 결과
   IVCP_ID_Search_FaceInfo,               // 얼굴 검색 결과
   IVCP_ID_Search_LogInfo,                // 로그 검색 결과


   IVCP_ID_System_SettingReq,
   IVCP_ID_System_SettingRes,
   IVCP_ID_System_CommandReq,
   IVCP_ID_System_CommandRes,
   IVCP_ID_System_MonitorReq,
   IVCP_ID_System_MonitorRes,
   IVCP_ID_System_MonitorInfo,            //
   IVCP_ID_System_AudioInOutReq,          // 오디오 방송을 설정합니다.
   IVCP_ID_System_AudioInOutRes,
   IVCP_ID_System_SendAudioData,
   IVCP_ID_System_RecvAudioData,

   IVCP_ID_Camera_ExportStartReq,
   IVCP_ID_Camera_ExportStartRes,
   IVCP_ID_Camera_ExportStopReq,
   IVCP_ID_Camera_ExportStopRes,

   IVCP_ID_Export_Progress,
   IVCP_ID_Export_FrameInfo,
   IVCP_ID_Export_EventInfo,
   IVCP_ID_Export_ObjectInfo,
   IVCP_ID_Export_PlateInfo,
   IVCP_ID_Export_FaceInfo,
   IVCP_ID_Export_PtzDataInfo,
   IVCP_ID_Export_PresetIdInfo,
   IVCP_ID_Export_EventZoneInfo,
   IVCP_ID_Export_EventZoneCount,
   IVCP_ID_Export_VideoData,
   IVCP_ID_Export_AudioData,

   IVCP_ID_Data_FaceUserData,			 // 얼굴인식 사용자 데이터
   IVCP_ID_Data_FaceImageData,       // 얼굴인식 이미지 데이터
   IVCP_ID_Data_FaceMatchData,       // 얼굴인식 매칭   데이터
   IVCP_ID_Data_FaceSearchData,      // 사진 기반 검색용 데이터


   IVCP_ID_MMS_LoginRes,                              // 로그인요청 응답 (그룹권한정보포함)  RAS ->MMS
   IVCP_ID_MMS_UserAccount_Notify,                    // 사용자계정정보 전송                 RAS
   IVCP_ID_MMS_SystemStatusInfo_Req,                  // 시스템 상태정보 (CPU,메모리 등) 요청 MMSC
   IVCP_ID_MMS_SystemStatusInfo_Res,                  // 시스템 상태정보 (CPU,메모리 등) 응답 MMSC
   IVCP_ID_MMS_NVRServer_ConnectInfo_Req,             // 저장/분배서버 접속정보 요청          MMSC
   IVCP_ID_MMS_NVRServer_ConnectInfo_Res,             // 저장/분배서버 접속정보 응답          MMSC
   IVCP_ID_MMS_NVRServer_ConfigFile_Transfer_Req,     // 저장/분배서버 Configuration 파일 전송 요청 MMSC
   IVCP_ID_MMS_NVRServer_ConfigFile_Transfer_Res,     // 저장/분배서버 Configuration 파일 전송 응답 MMSC
   //일방적 통보
   IVCP_ID_MMS_NVRServer_GracefulClose_Notify,        // 저장/분배서버 종료 알림
   IVCP_ID_MMS_NVRServer_FailOver_Notify,             // 저장/분배서버 페일오버 알림
   IVCP_ID_MMS_KeepAlive_Notify,                      // Keep Alive
   IVCP_ID_MMS_MMS_Failover_Conn_Info_Notify,         // 통합관리서버 페일오버 접속정보 알림
   IVCP_ID_MMS_MMS_FailOver_Notify,                   // 통합관리서버 자체 페일오버 알림
   //20150226,qch1004
   //채널 구성 정보 변경 알림  (중계서버->저장/분배/분석)
   IVCP_ID_VRS_Connect_Information,
   IVCP_ID_VRS_Channel_Channge_nty,

   IVCP_ID_Event_IPEX_Req,										// [jmlee 2015 02 04] 비상벨 연동 IPEX 추가
   IVCP_ID_Event_IPEX_Res,										// [jmlee 2015 02 04] 비상벨 연동 IPEX 추가

   IVCP_EVENT_WINDDLG_STATUS,
   IVCP_HOTLIST_INFO,
   IVCP_ID_Event_AC,                               // [jmlee] AC 출입 연동 추가
   IVCP_ID_DoorInfo_AC,

   // 지능형VMS 관련
   IVCP_ID_VMS_Conn_CameraListSetReq,              // 카메라리스트Set목록 정보 요청
   IVCP_ID_VMS_Conn_CameraListSetRes,              // 카메라리스트Set목록 정보 응답

   
};

///////////////////////////////////////////////////////////////////////////////

enum IVCP30_MetadataFlag
{
   IVCP30_META_EVENT_BEGUN       = 0x00000001,
   IVCP30_META_EVENT_ENDED       = 0x00000002,
   IVCP30_META_OBJECT_BEGUN      = 0x00000004,
   IVCP30_META_OBJECT_ENDED      = 0x00000008,
   IVCP30_META_FRAME_INFO        = 0x00000010,
   IVCP30_META_PLATE_INFO        = 0x00000020,
   IVCP30_META_FACE_UPDATE       = 0x00000040,
   IVCP30_META_FACE_BEGUN        = 0x00000080,
   IVCP30_META_FACE_ENDED        = 0x00000100,
   IVCP30_META_PTZ_DATA          = 0x00000200,
   IVCP30_META_PRESET_ID         = 0x00000400,
   IVCP30_META_EVTZONE_INFO      = 0x00000800,
   IVCP30_META_EVTZONE_COUNT     = 0x00001000,
   IVCP30_META_PRESET_MAP        = 0x00002000,
   IVCP30_META_SETTING           = 0x00004000,
   IVCP30_META_LOG_INFO          = 0x00008000,
   IVCP30_META_FRAME_OBJ         = 0x00010000,
   IVCP30_META_EVT_FULL_IMG      = 0x00020000, // (mkjang-140509)
   IVCP30_META_OBJ_FULL_IMG      = 0X00040000, // (mkjang-140509)
   IVCP30_META_EVENT_IPEX	     = 0X00080000, // [jmlee 2015 02 04] 비상벨 연동(IPEX) 추가
   IVCP30_META_AC_CTRL           = 0X00100000, // [jmlee] AC 출입 연동
   IVCP30_META_HEATMAP           = 0X00200000,
   IVCP30_META_STATISTICS_INFO   = 0X00400000,
   IVCP30_META_EVENT_SNAPSHOT    = 0X00800000, 
};

// 카메라 특성 값

enum IVCP30_CameraInfoParam
{
   IVCP30_CAM_PTZ_CAMERA        = 0x00000001,  // PTZ 카메라
   IVCP30_CAM_USE_MOBILE        = 0x00000002,  // 모바일용으로 권장하는 카메라
   IVCP30_CAM_USE_AUDIO_OUT     = 0x00000004,  // 오디오 아웃 지원
   IVCP30_CAM_USE_AUDIO_IN      = 0x00000008,  // 오디오 인 지원
};

// 카메라 설정 값

#ifndef IVCP_CAMERA_SETTING_FLAG
#define IVCP_CAMERA_SETTING_FLAG

enum IVCP30_CameraSettingFlag
{
   IVCP30_ENABLE_AUTO_PTZ                = 0x0000000000000001, // 자동 추적 기능 (PTZ)
   IVCP30_ENABLE_AUTO_FOCUS              = 0x0000000000000002, // 자동 초점 기능 (Focus)
   IVCP30_ENABLE_PRESET_TOURING          = 0x0000000000000004, // 프리셋 투어링 기능
   IVCP30_ENABLE_VIDEO_ANALYTICS         = 0x0000000000000008, // 영상분석 On/Off
   IVCP30_PAUSE_VIDEO_ANALYTICS          = 0x0000000000000010, // 영상분석 일시중지 On/Off
   IVCP30_PAUSE_VIDEO_ANALYTICS_SCHEDULE = 0x0000000000000020, // 영상분석 스케쥴 일시중지 On/Off
   IVCP30_ENABLE_VIDEO_DEFOG             = 0x0000000000000040, // 영상 안개 개선 기능 On/Off
   IVCP30_ENABLE_VIDEO_STABILIZATION     = 0x0000000000000080, // 영상 안정화 기능 On/Off
   IVCP30_START_HOME_POS_VIDEO_ANALYTICS = 0X0000000000000100, // 홈 위치 비디오 분석 시작 (홈 위치와 상관없이)
   IVCP30_GOTO_HOME_POSITION             = 0X0000000000000200,

   IVCP30_ENABLE_WIPER                   = 0x0000000000001000,
   IVCP30_ENABLE_HEATER                  = 0x0000000000002000,
   IVCP30_ENABLE_BLC_MODE                = 0x0000000000004000,
   IVCP30_ENABLE_ONE_SHOT_AUTO_FOCUS     = 0x0000000000008000,
   IVCP30_ENABLE_THERMAL                 = 0x0000000000010000,
   IVCP30_ENABLE_FAN                     = 0x0000000000020000,
   IVCP30_ENABLE_PTZ_LOCK                = 0x0000000000040000,
   IVCP30_ENABLE_WDR_MODE                = 0X0000000000080000, // (mkjang-140618)

   IVCP30_ENABLE_PLATE_INSTANT           = 0x0000000000100000, // 번호판 인식 (고정)
   IVCP30_ENABLE_PLATE_LOCKEDOBJ         = 0x0000000000200000, // 번호판 인식 (마스터/슬래이브)
   IVCP30_ENABLE_FACE_INSTANT            = 0x0000000000400000, // 얼굴 감지 (고정)
   IVCP30_ENABLE_FACE_LOCKEDOBJ          = 0x0000000000800000, // 얼굴 감지 (마스터/슬래이브)
   IVC30P_ENABLE_IR_LIGHT                = 0x0000000001000000,
};

#endif

// 카메라 설정 값
#ifndef IVCP_CAMERA_COMMAND_FLAG
#define IVCP_CAMERA_COMMAND_FLAG

enum IVCP30_CameraCommandFlag
{
   IVCP30_CAMERA_RESTART                   = 1,  // 카메라 재시작
   IVCP30_PTZ_POSITION_INIT                = 2,  // PTZ 위치 초기화
   IVCP30_PTZ_GOTO_HOME_POSITION           = 3,  // PTZ 홈 위치 이동
   IVCP30_SET_DEFOG_LEVEL                  = 4,  // 안개 개선 레벨
   IVCP30_SET_STABILIZATION_LEVEL          = 5,  // 영상 안정화 레벨
   IVCP30_SENSOR_INPUT                     = 6,  // 센서 입력
   IVCP30_GOTO_PRESET                      = 7,  // 프리셋 이동
   IVCP30_GOTO_PRESET_TOURING              = 8,  // 프리셋 투어링 시작
   IVCP30_SET_IR_MODE                      = 9,  // IR 제어 - 0:Off, 1:On, 2:Schedule
   IVCP30_SET_DAY_NIGHT_MODE               = 10, // Day Night Mode - 0:Day, 1:Night, 2:Auto
   IVCP30_SET_AUTO_WHITE_BALANCE           = 11, // Auto White Balance - 0:Auto, 1:Manual, 2:Indoor, 3:Outdoor
   IVCP30_SET_DNR_LEVEL                    = 12, // Digital Noise Reduction - 0 : off, Level : 1~5
};

#endif

#define GUID_LENGTH    (55)     // GUID 형식: EA5A8CE0-60A9-4846-B7F9-5210D44CFBFE
#define IPADDR_LENGTH  (15)

enum IVCP30_PlateAlarmPriorty
{
   CRITICAL_PRIORTY   = 0,
   MAJOR_PRIORTY,
   NOMAL_PRIORTY,
   MINOR_PRIORTY,
   NON_PRIORTY,  
};

enum IVCP_Face_Type
{
  FACE_TYPE_VERIFICATION_A	= 1,
  FACE_TYPE_VERIFICATION_M,
  FACE_TYPE_RECOGNITION,
  FACE_TYPE_ANALYSIS,
};



///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// Functions (PacketType, PacketID)
//
///////////////////////////////////////////////////////////////////////////////

const char *IvcpGetPacketTypeString (int nPacketID);
const char* IvcpGetPacketDescDString (int nPacketID);
void IvcpGetPacketString (char *strMessage, int nPacketID, int nBodyType = IVCP_TYPE_XML);

BOOL IvcpIsBinaryPacket (const char *strMessage);
int IvcpGetPacketType (const char *strMessage);
int IvcpGetPacketIDforReq (const char *strMessage);
int IvcpGetPacketIDforRes (const char *strMessage);

void ivcpGetFileTimeFromString (FILETIME &ftTime, const char *strTime);
void ivcpGetStringFromFileTime (std::string &strTime, FILETIME &ftTime);

// TYLEE: 2015-04-29
void ivcpGetGUIDFromString (char *pstring, char *pGUID);
void ivcpGetGUIDFromString (char *pstring, std::string *pGUID);
int  ivcpSetGUIDToString   (const std::string& szGUID, char *pstring);

///////////////////////////////////////////////////////////////////////////////
//
//  CIVCPPacketExt
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPPacketExt
{
public:
   int m_nPacketID;

public:
   virtual int ReadXML         (CXMLIO* pIO) { return (DONE); }
   virtual int WriteXML        (CXMLIO* pIO) { return (DONE); }
   virtual int ReadPacketBody  (FileIO& buff);
   virtual int WritePacketBody (FileIO& buff);

public:
   virtual int ReadFromPacket (CIVCPPacket& aPacket);
   virtual int WriteToPacket  (CIVCPPacket& aPacket);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnOptionReq : 서버 기본 정보를 읽어온다.
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPConnOptionsReq : public CIVCPPacketExt
{
public:
   float   m_fNetworkVer;

public:
   CIVCPConnOptionsReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnOptionsRes : 서버 기본 정보
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPConnOptionsRes : public CIVCPPacketExt
{
public:
   char            m_strDeviceName[128];  // G400 V4.0
   int             m_nDeviceVer;          // 1332
   float           m_fNetworkVer;         // IVCP 3.0
   char            m_strSystemName[128];  // 시스템 이름
   int             m_nCameraNum;          // 카메라 개수
   int             m_nSystemUID;          // 시스템 UID
   char            m_strSystemGUID[64];   // 시스템 GUID


public:
   CIVCPConnOptionsRes ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnLoginReq : 로그인을 요청한다.
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPConnLoginReq : public CIVCPPacketExt
{
public:
   int     m_nLoginType;         // 인증방식 (자동, 로컬, 인증)
   char    m_strUserID[128];     // 사용자 이름
   char    m_strPasswd[128];     // 암호
   char    m_strDeviceName[128]; // 디바이스 이름

public:
   enum IVCP_LoginType
   {
      Auto   = 0,
      Local  = 1,
      Server = 2,
   };

public:
   CIVCPConnLoginReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnLoginRes : 로그인 응답
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPConnLoginRes : public CIVCPPacketExt
{
public:
   int m_nLoginResult;

public:
   enum IVCP_LoginResult
   {
      LoginSuccess      = 0,
      InvalidAccount    = 1,
      IncorrectPasswd   = 2,
      AlreadyLogin      = 3,
      NotSupported      = 4,
   };

public:
   CIVCPConnLoginRes ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnLogoutReq : 로그아웃을 요청한다.
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPConnLogoutReq : public CIVCPPacketExt
{
public:
   int m_nLogoutType;

public:
   CIVCPConnLogoutReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnLogoutRes : 로그아웃 응답
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPConnLogoutRes : public CIVCPPacketExt
{
public:
   int m_nLogoutResult;

public:
   enum IVCP_Error
   {
      LogoutSuccess  = 0,
      ForcedLogout   = 1,
   };

public:
   CIVCPConnLogoutRes ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnCameraListReq : 카메라 목록 정보 요청
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPConnCameraListReq : public CIVCPPacketExt
{
public:
   CIVCPConnCameraListReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};



///////////////////////////////////////////////////////////////////////////////
//
// class CIVCPCameraInfo
//
///////////////////////////////////////////////////////////////////////////////

#define MAX_VSTREAMS                3
#define MAX_ASTREAMS                1

typedef struct tagIVCPCameraInfo
{
   // 카메라 기본 정보
   int m_nCameraUID;                   // 카메라 고유 번호 (UID)
   char m_strGuid[128];                // 카메라 Guid (고유 키값)
   int m_nCameraNo;                    // 로컬 채널 번호
   char m_strCameraName[128];          // 카메라 이름
   int m_nCameraType;                  // 카메라 종류 (아날로그/IP카메라/파일)
   int m_nCameraParam;                 // 카메라 특성 (PTZ 카메라 여부 등)
   int m_nWidth;                       // 원본 영상 너비
   int m_nHeight;                      // 원본 영상 높이
   float m_fFrameRate;                 // 원본 프레임 레이트
   int m_nVStreamNum;                  // 비디오 스트리밍 갯수
   int m_nAStreamNum;                  // 오디오 스트리밍 갯수

   // 비디오 스트리밍 정보
   int m_nVideoCodec [MAX_VSTREAMS];   // 압축 코덱
   int m_nBitrate    [MAX_VSTREAMS];   // 비트레이트
   int m_nVideoWidth [MAX_VSTREAMS];   // 영상 너비
   int m_nVideoHeight[MAX_VSTREAMS];   // 영상 높이
   float m_fFrmRate  [MAX_VSTREAMS];   // 영상 전송 프레임 레이트
   int m_nSendType   [MAX_VSTREAMS];   // 전송 타입 (이벤트-적응형, 일반)

   // 오디오 스트리밍 정보
   int m_nAudioCodec[MAX_ASTREAMS];    // 오디오 코덱
   int m_nChannels[MAX_ASTREAMS];      // 채널수
   int m_nSamplingRate[MAX_ASTREAMS];  // 영상 너비
   int m_nBitsPerSample[MAX_ASTREAMS]; // 영상 높이

} IVCPCameraInfo;

class CIVCPCameraInfo
{
public:
   // 카메라 기본 정보
   int   m_nCameraUID;                 // 카메라 고유 번호 (UID)
   char  m_strGuid[128];               // 카메라 Guid (고유 키값)
   int   m_nCameraNo;                  // 로컬 채널 번호
   char  m_strCameraName[128];         // 카메라 이름
   int   m_nCameraType;                // 카메라 종류 (아날로그/IP카메라/파일)
   int   m_nCameraParam;               // 카메라 특성 (PTZ 카메라 여부 등)
   int   m_nWidth;                     // 원본 영상 너비
   int   m_nHeight;                    // 원본 영상 높이
   float m_fFrameRate;                 // 원본 프레임 레이트
   int   m_nVStreamNum;                // 비디오 스트리밍 갯수
   int   m_nAStreamNum;                // 오디오 스트리밍 갯수

   // 비디오 스트리밍 정보
   int   m_nVideoCodec [MAX_VSTREAMS]; // 압축 코덱
   int   m_nBitrate    [MAX_VSTREAMS]; // 비트레이트
   int   m_nVideoWidth [MAX_VSTREAMS]; // 영상 너비
   int   m_nVideoHeight[MAX_VSTREAMS]; // 영상 높이
   float m_fFrmRate    [MAX_VSTREAMS]; // 영상 전송 프레임 레이트

   // 오디오 스트리밍 정보
   int m_nAudioCodec   [MAX_ASTREAMS]; // 오디오 코덱
   int m_nChannels     [MAX_ASTREAMS]; // 채널수
   int m_nSamplingRate [MAX_ASTREAMS]; // 영상 너비
   int m_nBitsPerSample[MAX_ASTREAMS]; // 영상 높이

public:
   CIVCPCameraInfo ();
   //CIVCPCameraInfo& operator= (CIVCPCameraInfo& from);

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);

public:
   void ParseVideoStreamDesc (int nIdx, const std::string &strDesc);
   void ParseAudioStreamDesc (int nIdx, const std::string &strDesc);
   void MakeVideoStreamDesc (int nIdx, std::string &strDesc);
   void MakeAudioStreamDesc (int nIdx, std::string &strDesc);

public:
   void GetCameraInfo(IVCPCameraInfo &CameraInfo);
   void SetCameraInfo(IVCPCameraInfo *pCameraInfo);
};

typedef std::deque<CIVCPCameraInfo*> IVCPCameraInfoList;

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnCameraListRes : 카메라 목록 정보 응답
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPConnCameraListRes : public CIVCPPacketExt
{
public:
   // 카메라 정보
   int                m_nStart;     // 읽어온 카메라 시작 (0: 전부, 1-based index)
   int                m_nCount;     // 읽어온 카메라 개수
   IVCPCameraInfoList m_CameraList;

public:
   CIVCPConnCameraListRes ();
   virtual ~CIVCPConnCameraListRes ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);

public:
   void ClearCameras ();
};

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnStartUpReq : 비디오/메타데이터 전송을 전체 시작한다.
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPConnStartUpReq : public CIVCPPacketExt
{
public:
   int  m_nStreamFlag;
   int  m_nMetaFlag;
   int  m_nOptions;

public:
   CIVCPConnStartUpReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnStartUpRes
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPConnStartUpRes : public CIVCPPacketExt
{
public:
   int m_nResult;

public:
   CIVCPConnStartUpRes ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnShutDownReq : 비디오/메타데이터 전송을 전체 중지한다.
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPConnShutDownReq : public CIVCPPacketExt
{
public:
   int  m_nStreamFlag;
   int  m_nMetaFlag;
   int  m_nOptions;

public:
   CIVCPConnShutDownReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnShutDownRes
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPConnShutDownRes : public CIVCPPacketExt
{
public:
   int m_nResult;

public:
   CIVCPConnShutDownRes ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnSnapshotReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPConnSnapshotReq : public CIVCPPacketExt
{
public:
   CHFLAG   m_nChannelFlag;
   int      m_nResizeFlag;
   int      m_nWidth;
   int      m_nHeight;

public:
   CIVCPConnSnapshotReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnSnapshotRes - 카메라 개수대로 날라옴
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPConnSnapshotRes : public CIVCPPacketExt
{
public:
   std::string  m_cCameraGUID;
   int      m_nCameraUID;
   int      m_nCodecID;
   int      m_nWidth;
   int      m_nHeight;

public:
   int      m_nXMLDataLen;
   int      m_nBodyLen;
   char*    m_pBodyBuffer; // 전달용 버퍼

public:
   CIVCPConnSnapshotRes ();
   ~CIVCPConnSnapshotRes ();

   byte *GetSnapshotBuffer() { return (byte*)(m_pBodyBuffer + m_nXMLDataLen); }
   int GetSnapshotLen() { return (m_nBodyLen - m_nXMLDataLen); }

public:
   virtual int ReadXML        (CXMLIO* pIO);
   virtual int WriteXML       (CXMLIO* pIO);
   virtual int ReadFromPacket (CIVCPPacket& aPacket);
   virtual int WriteToPacket  (CIVCPPacket& aPacket);

   void ParseBodyBuffer ();
   void CreateBodybuffer (byte *pEncodedBuff, int nEncodedLen);
   void DeleteBodyBuffer ();
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPSystemResultRes
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPSystemResultRes : public CIVCPPacketExt
{
public:
   int  m_nResponseID;  // 응답 ID
   int  m_nResult;      // 결과값

public:
   CIVCPSystemResultRes ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraResultRes
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPCameraResultRes : public CIVCPPacketExt
{
public:
   std::string  m_cCameraGUID;
   int  m_nCameraUID;   // 카메라 UID
   int  m_nResponseID;  // 응답 ID
   int  m_nResult;      // 결과값

public:
   CIVCPCameraResultRes ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraLiveMetadataReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPCameraLiveMetadataReq : public CIVCPPacketExt
{
public:
   std::string  m_cCameraGUID;
   int  m_nCameraUID;      // 카메라 UID
   int  m_nResponseID;     // 응답 ID
   int  m_nMetaFlag;       // 전송 여부
   int  m_nOptionFlag;     // 썸메일 전송 여부

public:
   CIVCPCameraLiveMetadataReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraLiveVideoReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPCameraLiveVideoReq : public CIVCPPacketExt
{
public:
   std::string  m_cCameraGUID;
   int  m_nCameraUID;      // 카메라 UID
   int  m_nResponseID;     // 응답 ID
   int  m_nStreamFlag;     // 스트리밍 인덱스 (우측부터 0 채널: 1 << StreamIndex)

public:
   CIVCPCameraLiveVideoReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraPlayStreamStartReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPCameraPlayStreamStartReq : public CIVCPPacketExt
{
public:
   std::string  m_cCameraGUID;
   int      m_nCameraUID;     // 카메라 UID
   int      m_nResponseID;    // 응답 ID
   int      m_nStreamID;      // 스트림 콜백 ID
   int      m_nStreamFlag;    // 전송받을 스트림 플래그
   int      m_nMetaFlag;      // 전송받을 메타 플래그
   FILETIME m_ftStart;        // 재생 시작 시간
   FILETIME m_ftEnd;          // 재생 종료 시간
   BOOL     m_bReverse;       // 재생 방향
   float    m_fPlaySpeed;     // 재생 속도 (1/8 ~ 32)
   int      m_nPlayMode;      // 이벤트 재생 여부 (현재는 미사용)

   BOOL     m_bTranscode;     // 트랜스코딩 여부
   int      m_nCodecID;       // 코덱 ID
   int      m_nResizeOption;  // 축소 방법 (0:비율, 1:너비기준크기, 2:고정크기)
   float    m_fResizeRate;    // 축소 비율
   int      m_nWidth;         // 크기
   int      m_nHeight;        // 높이
   int      m_nBitrate;       // 비트레이트

public:
   CIVCPCameraPlayStreamStartReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraPlayStreamStopReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPCameraPlayStreamStopReq : public CIVCPPacketExt
{
public:
   std::string  m_cCameraGUID;
   int      m_nCameraUID;     // 카메라 UID
   int      m_nResponseID;    // 응답 ID
   int      m_nStreamID;      // 스트림 콜백 ID

public:
   CIVCPCameraPlayStreamStopReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraPlayStreamControlReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPCameraPlayStreamControlReq : public CIVCPPacketExt
{
public:
   std::string  m_cCameraGUID;
   int      m_nCameraUID;     // 카메라 UID
   int      m_nResponseID;    // 응답 ID
   int      m_nStreamID;      // 스트림 콜백 ID
   int      m_nCtrlFlag;      // 플래그 (1: 플레이시간, 2:재생속도. 4:재생방향)
   FILETIME m_ftPlayTime;     // 현재 재생 시간
   float    m_fPlaySpeed;     // 재생 속도 (1/8 ~ 32)
   BOOL     m_bReverse;       // 재생 방향

public:
   CIVCPCameraPlayStreamControlReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraPlayDataStartReq
//
///////////////////////////////////////////////////////////////////////////////
class CIVCPCameraPlayDataStartReq : public CIVCPPacketExt
{
public:
   std::string  m_cCameraGUID;
   int      m_nCameraUID;     // 카메라 UID
   int      m_nResponseID;    // 응답 ID
   int      m_nStreamID;      // 스트림 콜백 ID
    int      m_nMetaFlag;      // 전송받을 메타 플래그
   FILETIME m_fStartTime;     // 현재 재생 시간
   FILETIME m_fEndTime;	   // 현재 재생 시간
public:
   CIVCPCameraPlayDataStartReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraPlayDataCtrl
//
///////////////////////////////////////////////////////////////////////////////
class CIVCPCameraPlayDataCtrlReq : public CIVCPPacketExt
{
public:
   std::string  m_cCameraGUID;
   int      m_nCameraUID;     // 카메라 UID
   int      m_nResponseID;    // 응답 ID
   int      m_nStreamID;      // 스트림 콜백 ID
    int      m_nCtrlType;      // 전송받을 메타 플래그

public:
   CIVCPCameraPlayDataCtrlReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnCarPlateRecognition	번호판 분석 요청 응답
//
///////////////////////////////////////////////////////////////////////////////
class CIVCPConnCarPlateRecognitionReq : public CIVCPPacketExt
{
public:
   int   m_nResponseID;				// 응답 ID
   char	m_strFileID[MAX_PATH];		//파일 관리 아이디
   int	m_nFileType;				//0:JPEG    1: AVI
   char	m_strFileName[MAX_PATH];
   INT64	m_nFileSize;
   char	m_strURL[MAX_PATH];

public:
    CIVCPConnCarPlateRecognitionReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraSnapshotReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPCameraSnapshotReq : public CIVCPPacketExt
{
public:
   std::string  m_cCameraGUID;
   int      m_nCameraUID;
   int      m_nResponseID;     // 응답 ID
   int      m_nResizeFlag;
   int      m_nWidth;
   int      m_nHeight;
   char     m_strFilePath[128];
public:
   CIVCPCameraSnapshotReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraSnapshotRes
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPCameraSnapshotRes : public CIVCPPacketExt
{
public:
   std::string  m_cCameraGUID;
   int      m_nCameraUID;
   int      m_nResponseID;     // 응답 ID
   int      m_nCodecID;
   int      m_nWidth;
   int      m_nHeight;
   int      m_nSnapLen;
   byte    *m_bSnapShotImg;
public:
   int      m_nXMLDataLen;
   int      m_nBodyLen;
   char*    m_pBodyBuffer; // 전달용 버퍼

public:
   CIVCPCameraSnapshotRes ();
   ~CIVCPCameraSnapshotRes ();

   byte *GetSnapshotBuffer() { return (byte*)(m_pBodyBuffer + m_nXMLDataLen); }
   int GetSnapshotLen() { return (m_nBodyLen - m_nXMLDataLen); }

public:
   virtual int ReadXML        (CXMLIO* pIO);
   virtual int WriteXML       (CXMLIO* pIO);
   virtual int ReadFromPacket (CIVCPPacket& aPacket);
   virtual int WriteToPacket  (CIVCPPacket& aPacket);

   void ParseBodyBuffer ();
   void CreateBodybuffer (byte *pEncodedBuff, int nEncodedLen);
   void DeleteBodyBuffer ();
};

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraSettingReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPCameraSettingReq : public CIVCPPacketExt
{
public:
   std::string  m_cCameraGUID;
   int      m_nCameraUID;
   int      m_nResponseID;     // 응답 ID
   UINT64   m_nSettingMask;
   UINT64   m_nSettingFlag;

public:
   CIVCPCameraSettingReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraCommandReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPCameraCommandReq : public CIVCPPacketExt
{
public:
   std::string  m_cCameraGUID;
   int      m_nCameraUID;
   int      m_nResponseID;     // 응답 ID
   int      m_nCommand;
   int      m_nParam1;
   int      m_nParam2;

public:
   CIVCPCameraCommandReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};


///////////////////////////////////////////////////////////////////////////////
//
// (PTZ) CIVCPCameraPtzContMoveReq
//
///////////////////////////////////////////////////////////////////////////////

const float IVCP_PTZ_NO_CMD   = (float)1E-20;

class CIVCPCameraPtzContMoveReq : public CIVCPPacketExt
{
public:
   std::string  m_cCameraGUID;
   int   m_nCameraUID;  // 카메라 UID
   int   m_nResponseID;     // 응답 ID
   float m_fPan;
   float m_fTilt;
   float m_fZoom;
   float m_fFocus;

public:
   CIVCPCameraPtzContMoveReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (PTZ) CIVCPCameraPtzAbsMoveReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPCameraPtzAbsMoveReq : public CIVCPPacketExt
{
public:
   std::string  m_cCameraGUID;
   int   m_nCameraUID;  // 카메라 UID
   int   m_nResponseID;     // 응답 ID
   float m_fPan;
   float m_fTilt;
   float m_fZoom;
   float m_fFocus;

public:
   CIVCPCameraPtzAbsMoveReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (PTZ) CIVCPCameraPtzBoxMoveReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPCameraPtzBoxMoveReq : public CIVCPPacketExt
{
public:
   std::string  m_cCameraGUID;
   int   m_nCameraUID;  // 카메라 UID
   int   m_nResponseID;     // 응답 ID
   float m_fLeft;
   float m_fTop;
   float m_fWidth;
   float m_fHeight;

public:
   CIVCPCameraPtzBoxMoveReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (PTZ) CIVCPCameraPtzGetAbsPosReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPCameraPtzGetAbsPosReq : public CIVCPPacketExt
{
public:
   std::string  m_cCameraGUID;
   int   m_nCameraUID;  // 카메라 UID
   int   m_nResponseID; // 응답 ID

public:
   CIVCPCameraPtzGetAbsPosReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (PTZ) CIVCPCameraPtzGetAbsPosRes
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPCameraPtzGetAbsPosRes : public CIVCPPacketExt
{
public:
   std::string  m_cCameraGUID;
   int      m_nCameraUID;  // 카메라 UID
   int      m_nResponseID; // 응답 ID
   FILETIME m_ftFrameTime; // 메타 발생 시간
   float    m_fPanAngle;   // Pan 각도 : 반시계 방향이 증가하는 방향이다. 단위(degree, 0~360도)
   float    m_fTiltAngle;  // Tilt 각도 : 아래로 향하는 방향이 증가하는 방향임. 단위(degree, 0~360도)
   float    m_fZoomFactor; // Zoom 값 : 현재 줌 위치를 나타냄. 18배줌 카메라이면 1~18사이의 값을 가짐.
   float    m_fFOV;        // FOV (Field Of View) : Pan 축으로의 화각을 나타냄. 단위(degree, 0~360도)

public:
   CIVCPCameraPtzGetAbsPosRes ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// CIVCPFrameEvent

class CIVCPFrameEvent
{
public:
   int m_nEventID;
   int m_nEventType;

public:
   CIVCPFrameEvent ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

/////////////////////////////////////////////////////////////////////
// CIVCPFrameObject

class CIVCPFrameObject
{
public:
   int              m_nObjectID;
   int              m_nObjectStatus;
   int              m_nObjectType;
   int              m_nObjectEventStatus;
   uint             m_nObjectVAEvent;
   IBox2D           m_ObjectBox;
   float            m_fTimeDwell;
   int              m_nEventNum;
   CIVCPFrameEvent* m_pEvents;

public:
   CIVCPFrameObject ();
   ~CIVCPFrameObject ();

   void ClearEvents();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

/////////////////////////////////////////////////////////////////////
// CIVCPLiveFrameInfo

class CIVCPLiveFrameInfo : public CIVCPPacketExt
{
public:
   std::string       m_cCameraGUID;
   int               m_nCameraUID;
   FILETIME          m_ftFrameTime;
   int               m_nObjectNum;
   CIVCPFrameObject* m_pObjects;

public:
   int        m_nCameraState;
   int        m_nPTZState;
   int        m_nPTZStateEx;
   float      m_fFrameRate;
   int        m_nProcWidth;
   int        m_nProcHeight;

public:
   CIVCPLiveFrameInfo (   );
   ~CIVCPLiveFrameInfo (   );

   void ClearObjects ();

public:
   virtual int ReadXML        (CXMLIO* pIO);
   virtual int WriteXML       (CXMLIO* pIO);
   virtual int WriteToPacket  (CIVCPPacket& aPacket);
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLiveFrameObectInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPFrameObjectEx
{
public:
   int              m_nObjectID;
   int              m_nObjectType;
   IBox2D           m_ObjectBox;
   int              m_nObjectStatus;
   float            m_fArea;
   float            m_fWidth;
   float            m_fHeight;
   float            m_fMajorAxisLen;
   float            m_fMinorAxisLen;
   float            m_fSpeed;
   float            m_fRealArea;
   float            m_fRealWidth;
   float            m_fRealHeight;
   float            m_fRealMajorAxisLen;
   float            m_fRealMinorAxisLen;
   float            m_fRealSpeed;
   float            m_fNorSpeed;
   int              m_nEventNum;
   CIVCPFrameEvent* m_pEvents;

public:
   BOOL m_bSendRealValue;

public:
   CIVCPFrameObjectEx ();
   ~CIVCPFrameObjectEx ();

   void ClearEvents();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

/////////////////////////////////////////////////////////////////////
class CIVCPLiveFrameObjectInfo : public CIVCPPacketExt
{
public:
   std::string  m_cCameraGUID;
   int        m_nCameraUID;
   FILETIME   m_ftFrameTime;
   float      m_fFrameRate;
   ISize2D    m_szFrameSize;

   int        m_nObjectNum;
   CIVCPFrameObjectEx* m_pObjects;

public:
   CIVCPLiveFrameObjectInfo (   );
   ~CIVCPLiveFrameObjectInfo (   );

   void ClearObjects ();

public:
   virtual int ReadXML        (CXMLIO* pIO);
   virtual int WriteXML       (CXMLIO* pIO);
   virtual int WriteToPacket  (CIVCPPacket& aPacket);
};


///////////////////////////////////////////////////////////////////////////////

class CIVCPColorChangeInfo
{
public:
   BGRColor m_CenterBackgroundColor;
   BGRColor m_CenterForegroundColor;
   BGRColor m_RegionBackgroundColor;
   BGRColor m_RegionForegroundColor;

public:
   CIVCPColorChangeInfo (   );
   ~CIVCPColorChangeInfo (   );
public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLiveEventInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPLiveEventInfo : public CIVCPPacketExt
{
public:
   std::string  m_cCameraGUID;
   int        m_nCameraUID;       // 카메라 UID
   FILETIME   m_ftStartTime;      // 이벤트 시작 시간
   FILETIME   m_ftEndTime;        // 이벤트 종료 시간
   float      m_fDwellTime; 
   int        m_nEventStatus;     // 이벤트 상태 (시작, 종료)
   int        m_nEventID;         // 이벤트 ID (시작-종료을 매칭시킬때 사용)
   int        m_nEventType;       // 이벤트 종류
   int        m_nEventRuleID;     // 이벤트 룰 ID
   int        m_nObjectID;        // 객체 ID (출력,저장용)
   int        m_nObjectType;      // 객체 종류 (사람, 차량, 미확인)
   int        m_nNumObjects;      // 이벤트를 발생 시킨 물체 수 -> 물체 카운트 설정의 면적 정보를 기준으로 추정되는 물체 수
   IBox2D     m_ObjectBox;        // 객체 위치 (아래 Proc 좌표 기준)
   int        m_nEventZoneID;     // 발생한 이벤트 존 ID
   char       m_strEventZoneName[128]; // 발생한 이벤트 존 이름

   int        m_nProcWidth;       // 분석 영상 크기
   int        m_nProcHeight;
   int        m_nThumbCodecID;    // 썸네일 코덱 ID
   int        m_nThumbWidth;      // 썸네일 이미지 크기
   int        m_nThumbHeight;
   BOOL       m_bThumbnail;       // 썸네일 첨부 여부

   // (mkjang-140509)
   int        m_nEvtFullImgWidth; // 이벤트 전체 이미지 크기
   int        m_nEvtFullImgHeight;
   BOOL       m_bEvtFullImg;      // 이벤트 전체 이미지 첨부 여부

   int        m_nThumbnailLen;
   int        m_nEvtFullImgLen;

   CIVCPColorChangeInfo m_ColorChangeInfo;

   int        m_nXMLDataLen;
   int        m_nBodyLen;
   char*      m_pBodyBuffer; // 전달용 버퍼

public:
   enum IVCP_LiveEventInfoStatus
   {
      EVENT_STATUS_BEGUN  =  0, // Start of event.
      EVENT_STATUS_ENDED  =  1, // End of event.
   };

public:
   CIVCPLiveEventInfo ();
   virtual ~CIVCPLiveEventInfo ();

   byte *GetThumbnailBuffer() { return (byte*)(m_pBodyBuffer + m_nXMLDataLen); }
   int GetThumbnailLen() { return m_nThumbnailLen; }

   // (mkjang-140509)
   byte *GetEvtFullImgBuffer() { return (byte*)(m_pBodyBuffer + m_nXMLDataLen + m_nThumbnailLen); }
   int GetEvtFullImgLen() { return m_nEvtFullImgLen; }

public:
   virtual int ReadXML        (CXMLIO* pIO);
   virtual int WriteXML       (CXMLIO* pIO);
   virtual int ReadFromPacket (CIVCPPacket& aPacket);
   virtual int WriteToPacket  (CIVCPPacket& aPacket);

   void ParseBodyBuffer ();
   void CreateBodybuffer (byte *pEncodedBuff, int nEncodedLen);
   void CreateBodybuffer (byte *pThumbnailBuff, int nThumbnailLen, byte *pEvtFullImgBuff, int nEvtFullImgLen);
   void DeleteBodyBuffer ();
};


class CIVCPLiveEventInfoSceneChanged : public CIVCPLiveEventInfo
{
public:
   CIVCPLiveEventInfoSceneChanged();
   virtual ~CIVCPLiveEventInfoSceneChanged();

   virtual int WriteXML       (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPHeatMap
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPHeatMap : public CIVCPPacketExt
{
public:         
   std::string  m_cCameraGUID;
   int        m_nCameraUID;
   char       m_szCameraName[100];

   FILETIME   m_ftStartTime;
   FILETIME   m_ftEndTime;

   int        m_nHeatMapType;
   int        m_nHeatMapWidth;
   int        m_nHeatMapHeight;      
   int        m_nHeatMapMaxValue;
   int        m_nHeatMapMinValue;
   UIArray2D  m_OrgHeatMapData;
   
   BOOL       m_bIncludeHeatMapImg;
   int        m_nHeatMapImgCodecID;
   int        m_nHeatMapImgDataLen;   
   char       m_szColorMapType[32];

   int        m_nXMLDataLen;
   int        m_nBodyLen;
   char*      m_pBodyBuffer;      // 전달용 버퍼

public:
   CIVCPHeatMap();
   virtual ~CIVCPHeatMap();

   virtual int ReadXML        (CXMLIO* pIO);
   virtual int WriteXML       (CXMLIO* pIO);
   virtual int ReadFromPacket (CIVCPPacket& aPacket);
   virtual int WriteToPacket  (CIVCPPacket& aPacket);

   byte* GetHeatMapBuffer  ()   { return (byte*)(m_pBodyBuffer + m_nXMLDataLen); }
   int   GetHeatMapImgLen  ()   { return m_nHeatMapImgDataLen; }

   void ParseBodyBuffer  ();
   void CreateBodyBuffer (byte *pEncodedBuff, int nEncodedLen);   
   void DeleteBodyBuffer ();
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPStatistics
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPStatisticsItemInfo
{
public:
   int   m_nZID;
   char  m_szZName[128];
   float m_avgDwellTime;
   int   m_nCount;

public:
   CIVCPStatisticsItemInfo ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};


class CIVCPStatistics : public CIVCPPacketExt
{
public:
   std::string m_cCameraGUID;
   int        m_nCameraUID;
   char       m_szCameraName[100];

   FILETIME   m_ftStartTime;
   FILETIME   m_ftEndTime;

   std::vector<CIVCPStatisticsItemInfo>   m_vecItem;

public:
   CIVCPStatistics ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLiveObjectInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPLiveObjectInfo : public CIVCPPacketExt
{
public:
   std::string m_cCameraGUID;
   int        m_nCameraUID;      // 카메라 UID
   FILETIME   m_ftStartTime;
   FILETIME   m_ftEndTime;
   int        m_nObjectStatus;
   int        m_nObjectID;
   int        m_nObjectType;
   IBox2D     m_ObjectBox;


   BOOL m_bSendRealValue;
   BOOL m_bSendMaxValue;
   BOOL m_bSendMinValue;

   // Average
   float      m_fAvgArea;
   float      m_fAvgWidth;
   float      m_fAvgHeight;
   float      m_fAvgAspectRatio;
   float      m_fAvgAxisLengthRatio;
   float      m_fAvgMajorAxisLength;
   float      m_fAvgMinorAxisLength;
   float      m_fAvgNorSpeed;
   float      m_fAvgSpeed;
   float      m_fAvgRealArea;
   float      m_fAvgRealDistance;
   float      m_fAvgRealWidth;
   float      m_fAvgRealHeight;
   float      m_fAvgRealMajorAxisLength;
   float      m_fAvgRealMinorAxisLength;
   float      m_fAvgRealSpeed;
   // Max
   int        m_nMaxWidth;
   int        m_nMaxHeight;
   float      m_fMaxArea;
   float      m_fMaxAspectRatio;
   float      m_fMaxAxisLengthRatio;
   float      m_fMaxMajorAxisLength;
   float      m_fMaxMinorAxisLength;
   float      m_fMaxNorSpeed;
   float      m_fMaxSpeed;
   float      m_fMaxRealDistance;
   float      m_fMaxRealSpeed;
   // Min
   int        m_nMinWidth;
   int        m_nMinHeight;
   float      m_fMinArea;
   float      m_fMinAspectRatio;
   float      m_fMinAxisLengthRatio;
   float      m_fMinMajorAxisLength;
   float      m_fMinMinorAxisLength;
   float      m_fMinNorSpeed;
   float      m_fMinSpeed;
   float      m_fMinRealDistance;
   float      m_fMinRealSpeed;
   // Color Histogram
   float      m_fHistogram[10];

   int        m_nProcWidth;       // 분석 영상 크기
   int        m_nProcHeight;
   int        m_nThumbCodecID;    // 썸네일 코덱 ID
   int        m_nThumbWidth;      // 썸네일 이미지 크기
   int        m_nThumbHeight;
   BOOL       m_bThumbnail;       // 썸네일 첨부 여부

   // (mkjang-140509)
   int        m_nObjFullImgWidth; // 이벤트 전체 이미지 크기
   int        m_nObjFullImgHeight;
   BOOL       m_bObjFullImg;      // 이벤트 전체 이미지 첨부 여부

public:
   // (mkjang-140509)
   int        m_nThumbnailLen;
   int        m_nObjFullImgLen;

   int        m_nXMLDataLen;
   int        m_nBodyLen;
   char*      m_pBodyBuffer; // 전달용 버퍼

public:
   CIVCPLiveObjectInfo ();
   ~CIVCPLiveObjectInfo ();

   byte *GetThumbnailBuffer() { return (byte*)(m_pBodyBuffer + m_nXMLDataLen); }
   int GetThumbnailLen() { return m_nThumbnailLen; }

   // (mkjang-140509)
   byte *GetObjFullImgBuffer() { return (byte*)(m_pBodyBuffer + m_nXMLDataLen + m_nThumbnailLen); }
   int GetObjFullImgLen() { return m_nObjFullImgLen; }

public:
   virtual int ReadXML        (CXMLIO* pIO);
   virtual int WriteXML       (CXMLIO* pIO);
   virtual int ReadFromPacket (CIVCPPacket& aPacket);
   virtual int WriteToPacket  (CIVCPPacket& aPacket);

   void ParseBodyBuffer ();
   void CreateBodybuffer (byte *pEncodedBuff, int nEncodedLen);
   void CreateBodybuffer (byte *pThumbnailBuff, int nThumbnailLen, byte *pObjFullImgBuff, int nObjFullImgLen);
   void DeleteBodyBuffer ();
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLiveLogInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPLiveLogInfo : public CIVCPPacketExt      // not defined
{
public:
   int      m_nCameraUID;
   char     m_cCameraGUID[GUID_LENGTH+1];   // TYLEE: 2015-04-27
};

///////////////////////////////////////////////////////////////////////////////
//
// (LIVE) CIVCPLivePTZInfoData
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPLivePTZDataInfo : public CIVCPPacketExt
{
public:
   char     m_cCameraGUID[GUID_LENGTH+1];   // TYLEE: 2015-04-27
   int      m_nCameraUID;  // 카메라 UID
   FILETIME m_ftFrameTime; // 메타 발생 시간
   float    m_fPanAngle;   // Pan 각도 : 반시계 방향이 증가하는 방향이다. 단위(degree, 0~360도)
   float    m_fTiltAngle;  // Tilt 각도 : 아래로 향하는 방향이 증가하는 방향임. 단위(degree, 0~360도)
   float    m_fZoomFactor; // Zoom 값 : 현재 줌 위치를 나타냄. 18배줌 카메라이면 1~18사이의 값을 가짐.
   float    m_fFOV;        // FOV (Field Of View) : Pan 축으로의 화각을 나타냄. 단위(degree, 0~360도)

public:
   CIVCPLivePTZDataInfo ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLivePresetIdInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPLivePresetIdInfo : public CIVCPPacketExt
{
public:
   char     m_cCameraGUID[GUID_LENGTH+1];   // TYLEE: 2015-04-27
   int      m_nCameraUID;
   FILETIME m_ftFrameTime;
   int      m_nCurPresetID;

public:
   CIVCPLivePresetIdInfo ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (LIVE) CIVCPLiveEventZoneInfo
//
///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// CIVCPPlayEventRule

class CIVCPEventRule_DetectionTime
{
public:
   float m_fDetectionTime;

public:
   CIVCPEventRule_DetectionTime ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};
class CIVCPEventRule_Counting
{
public:
   int     m_nCount;
   float   m_fAspectRatio;
   ILine2D m_aMajorAxis;

public:
   CIVCPEventRule_Counting ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};
class CIVCPEventRule_DirectionalMotion
{
public:
   float m_fDetectionTime;
   float m_fDirection;
   float m_fRange;

public:
   CIVCPEventRule_DirectionalMotion ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};
class CIVCPEventRule_PathPassingItem
{
public:
   int         m_nID;
   IPoint2D    m_ptCenter;
   int         m_nVerticesNum;
   IPoint2D   *m_pVertices;

public:
   CIVCPEventRule_PathPassingItem ();
   ~CIVCPEventRule_PathPassingItem ();

   void ClearVertices ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};
class CIVCPEventRule_PathPassingArrow
{
public:
   int         m_nSplitZoneID;
   ILine2D     m_lnArrow;

public:
   CIVCPEventRule_PathPassingArrow ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};
class CIVCPEventRule_PathPassing
{
public:
   int   m_nDirection;
   float m_fDetectionTime;
   float m_fPassingTime;
   int   m_nItemNum;
   CIVCPEventRule_PathPassingItem *m_pItems;
   int   m_nArrowNum;
   CIVCPEventRule_PathPassingArrow *m_pArrows;

public:
   CIVCPEventRule_PathPassing ();
   ~CIVCPEventRule_PathPassing ();

   void ClearItems ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};
class CIVCPEventRule_Flooding
{
public:
   int     m_nDirection;
   float   m_fDetectionTime;
   float   m_fThreshold;
   ILine2D m_CrossingLine;

public:
   CIVCPEventRule_Flooding ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

class CIVCPEventRule_LineCrossing
{
public:   
   int     m_nDirection;
   ILine2D m_CrossingLine;

public:
   CIVCPEventRule_LineCrossing ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

/////////////////////////////////////////////////////////////////////
// CIVCPEventZone

class CIVCPEventZone
{
public:
   int         m_nZoneID;
   int         m_nPresetID;
   int         m_nPriority;
   int         m_nOptions;
   int         m_nWidth;
   int         m_nHeight;
   char        m_strName[128];
   IPoint2D    m_ptCenter;
   int         m_nVerticesNum;
   IPoint2D   *m_pVertices;

   // 룰 정보
   int         m_nRuleOptions;
   int         m_nRuleEventType;
   int         m_nRuleObjectType;
   char        m_strRuleName[128];

   CIVCPEventRule_DetectionTime     m_aAbandoned;
   CIVCPEventRule_Counting          m_aCounting;
   CIVCPEventRule_DirectionalMotion m_aDirectionalMotion;
   CIVCPEventRule_PathPassing       m_aPathPassing;
   CIVCPEventRule_Flooding          m_aFlooding;
   CIVCPEventRule_LineCrossing      m_aLineCrossing;

public:
   CIVCPEventZone ();
   ~CIVCPEventZone ();

   void ClearVertices ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

class CIVCPLiveEventZoneInfo : public CIVCPPacketExt
{
public:
   std::string m_cCameraGUID;
   int      m_nCameraUID;
   FILETIME m_ftFrameTime;
   int      m_nWidth;
   int      m_nHeight;

   int      m_nZoneNum;
   CIVCPEventZone *m_pEventZones;

public:
   CIVCPLiveEventZoneInfo ();
   ~CIVCPLiveEventZoneInfo ();

   void ClearEventZones ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLiveEventZoneCountInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPEventZoneCount
{
public:
   int         m_nZoneID;
   int         m_nZoneCount;

   int         m_CurDwellObjectCount;
   int         m_TotalDwellObjectCount;
   float       m_AverageDwellTime;

public:
   CIVCPEventZoneCount ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////

class CIVCPLiveEventZoneCountInfo : public CIVCPPacketExt
{
public:
   std::string    m_cCameraGUID;
   int            m_nCameraUID;
   FILETIME       m_ftFrameTime;
   int            m_nZoneNum;
   CIVCPEventZoneCount *m_pZoneCounts;

public:
   BOOL     m_bRWOnlyCountInfo;

public:
   CIVCPLiveEventZoneCountInfo ();
   ~CIVCPLiveEventZoneCountInfo ();

   void ClearZoneCounts ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
   virtual int WriteToPacket  (CIVCPPacket& aPacket);
};

//////////////////////////////////////////////////////////////////////////////
//
// CIVCPResetObjectCountPeriod
//
///////////////////////////////////////////////////////////////////////////////
typedef struct _tagResetTime
{
   int      m_nDay;                    // 특정 날짜마다 Reset   ==> 0 : Sunday ~ 6 : Saturday
   int      m_nTime;                   // (단위 : Minute)
} RESET_OBJECT_TIME;

std::string GetResetTimeVectorToString(const std::vector<RESET_OBJECT_TIME>& vecReset);
void GetResetTimeToStringToVector(const std::string& strResetTime, std::vector<RESET_OBJECT_TIME>& vecReset);

class CIVCPResetObjectCountPeriod
{
public:
   int            m_nCameraUID;           // 카메라 고유 번호 (UID)
   std::string    m_strGuid;              // 카메라 Guid (고유 키값)   

   BOOL           m_bUse;
   int            m_nPeriodTime;          // 일정 시간 마다 Reset (단위 : Minute)

   std::vector<RESET_OBJECT_TIME>    m_vecReset;

public:
   CIVCPResetObjectCountPeriod();
   ~CIVCPResetObjectCountPeriod();

public:
   void PushResetTime         (int nDay, int nTime);
   virtual int ReadXML        (CXMLIO* pIO);
   virtual int WriteXML       (CXMLIO* pIO);
};

//////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLivePresetMapInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPPresetItemInfo
{
public:
   int     m_nID;
   char    m_strName[256];

public:
   CIVCPPresetItemInfo ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

class CIVCPPresetGroupInfo
{
public:
   int     m_nID;
   char    m_strName[256];
   int     m_nCycleType;
   int     m_nPresetNum;
   CIVCPPresetItemInfo *m_pPresetItems;

public:
   CIVCPPresetGroupInfo ();
   ~CIVCPPresetGroupInfo ();

   void ClearPresetItems ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////

class CIVCPLivePresetMapInfo : public CIVCPPacketExt
{
public:
   std::string  m_cCameraGUID;
   int      m_nCameraUID;
   FILETIME m_ftFrameTime;
   int      m_nGroupNum;
   CIVCPPresetGroupInfo *m_pGroupItems;

public:
   CIVCPLivePresetMapInfo ();
   ~CIVCPLivePresetMapInfo ();

   void ClearGroupItems ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

//////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLiveSettingInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPLiveSettingInfo : public CIVCPPacketExt
{
public:
   std::string  m_cCameraGUID;
   int      m_nCameraUID;
   FILETIME m_ftFrameTime;
   UINT64   m_nSettingMask;      // 세팅 마스크
   UINT64   m_nSettingFlag;      // 세팅 플래그
   int      m_nStabilNum;
   int      m_nDefogLevel;
   int      m_nWhiteBalanceMode;
   int      m_nDayNightMode;

public:
   CIVCPLiveSettingInfo ();
   ~CIVCPLiveSettingInfo ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
   virtual int WriteToPacket  (CIVCPPacket& aPacket);
};

//////////////////////////////////////////////////////////////////////////////
//
// (INFO) CIVCPLiveStatus
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPLiveStatus : public CIVCPPacketExt
{
public:
   std::string  m_cCameraGUID;
   int      m_nCameraUID;  // 카메라 UID
   FILETIME m_ftFrameTime; // 현재 시간
   int      m_nType;       // 상태 타입
   int      m_nParam;      // 상태 정보

public:
   CIVCPLiveStatus ();
   ~CIVCPLiveStatus ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPPlayFrameMetaInfo
//
///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// CIVCPPlayFrameObject

class CIVCPPlayFrameObject
{
public:
   int              m_nObjectID;
   int              m_nObjectType;
   int              m_nEventFlag;
   int              m_nFlagLocked;
   IBox2D           m_ObjectBox;

public:
   CIVCPPlayFrameObject ();
   ~CIVCPPlayFrameObject ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

/////////////////////////////////////////////////////////////////////
//
// CIVCPPlayFrameInfo
//
/////////////////////////////////////////////////////////////////////

class CIVCPPlayFrameInfo : public CIVCPPacketExt
{
public:
   std::string  m_cCameraGUID;
   int        m_nCameraUID;
   int        m_nStreamID;
   FILETIME   m_ftFrameTime;
   int        m_nObjectNum;
   CIVCPPlayFrameObject* m_pObjects;

public:
   int        m_nCameraState;
   int        m_nPTZState;
   int        m_nPTZStateEx;
   float      m_fFrameRate;
   int        m_nProcWidth;
   int        m_nProcHeight;

public:
   CIVCPPlayFrameInfo (   );
   ~CIVCPPlayFrameInfo (   );

   void ClearObjects ();

public:
   virtual int ReadXML        (CXMLIO* pIO);
   virtual int WriteXML       (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPPlayEventInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPPlayEventInfo : public CIVCPLiveEventInfo // not defined
{
public:
   int      m_nStreamID;
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPPlayEventInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPPlayObjectInfo : public CIVCPLiveObjectInfo   // not defined
{
public:
   int      m_nStreamID;
};

///////////////////////////////////////////////////////////////////////////////
//
// (LIVE) CIVCPPlayPTZDataInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPPlayPTZDataInfo : public CIVCPPacketExt
{
public:
   std::string  m_cCameraGUID;
   int      m_nCameraUID;
   int      m_nStreamID;
   FILETIME m_ftFrameTime;
   float    m_fPanAngle;   // Pan 각도 : 반시계 방향이 증가하는 방향이다. 단위(degree, 0~360도)
   float    m_fTiltAngle;  // Tilt 각도 : 아래로 향하는 방향이 증가하는 방향임. 단위(degree, 0~360도)
   float    m_fZoomFactor; // Zoom 값 : 현재 줌 위치를 나타냄. 18배줌 카메라이면 1~18사이의 값을 가짐.
   float    m_fFOV;        // FOV (Field Of View) : Pan 축으로의 화각을 나타냄. 단위(degree, 0~360도)

public:
   CIVCPPlayPTZDataInfo ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPPlayPresetIdInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPPlayPresetIdInfo : public CIVCPPacketExt
{
public:
   std::string  m_cCameraGUID;
   int      m_nCameraUID;
   int      m_nStreamID;
   FILETIME m_ftFrameTime;
   int      m_nCurPresetID;

public:
   CIVCPPlayPresetIdInfo ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPPlayEventZoneInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPPlayEventZoneInfo : public CIVCPPacketExt
{
public:
   std::string  m_cCameraGUID;
   int      m_nCameraUID;
   int      m_nStreamID;
   FILETIME m_ftFrameTime;
   int      m_nWidth;
   int      m_nHeight;

   int      m_nZoneNum;
   CIVCPEventZone *m_pEventZones;

public:
   CIVCPPlayEventZoneInfo ();
   ~CIVCPPlayEventZoneInfo ();

   void ClearEventZones ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (PLAY) CIVCPPlayEventZoneCountInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPPlayEventZoneCountInfo : public CIVCPPacketExt
{
public:
   std::string  m_cCameraGUID;
   int      m_nCameraUID;
   int      m_nStreamID;
   FILETIME m_ftFrameTime;
   int      m_nZoneNum;
   CIVCPEventZoneCount *m_pZoneCounts;

public:
   CIVCPPlayEventZoneCountInfo ();
   ~CIVCPPlayEventZoneCountInfo ();

   void ClearZoneCounts ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (DATA) CIVCPPlayProgress
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPPlayProgress : public CIVCPPacketExt
{
public:
   std::string  m_cCameraGUID;
   int      m_nCameraUID;
   int      m_nStreamID;
   int      m_nStep;
   int      m_nParam;
   FILETIME m_ftTime;
   FILETIME m_ftPlayTime;

public:
   enum IVCP_PlayProgressStep
   {
      WAITING   = 1,
      READY     = 2,
      BEGIN     = 3,
      END       = 4,
   };

public:
   CIVCPPlayProgress ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchFremeReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPSearchFrameReq : public CIVCPPacketExt
{
public:
   int      m_nSearchID;
   CHFLAG   m_nChannelFlag;
   FILETIME m_ftStart;
   FILETIME m_ftEnd;

public:
   CIVCPSearchFrameReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchEventReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPSearchEventReq : public CIVCPPacketExt
{
public:
   int      m_nSearchID;
   CHFLAG   m_nChannelFlag;
   FILETIME m_ftStart;
   FILETIME m_ftEnd;
   int      m_nEvtFlag;
   int      m_nObjFlag;
   int      m_nEvtZoneNum;
   int     *m_pEvtZoneIDs;

   BOOL     m_bThumbnail;  // 섬네일 전송 여부

public:
   CIVCPSearchEventReq ();
   ~CIVCPSearchEventReq ();

   void ClearEvtZoneIDs();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchObjectReq
//
///////////////////////////////////////////////////////////////////////////////

#define SEARCH_SELECT_COLOR_MAX        6  // 대표 색상 최대 선택 수

class CIVCPSearchColorFilter
{
public:
   int         m_nColorType;          // 대표 색상 or 히스토그램
   int         m_nColorMode;          // 조건 AND, OR
   int         m_nColorSensibility;   // 색상 매칭 민감도

   int         m_nColorNum;                         // 대표 색상 개수
   COLORREF    m_crColor[SEARCH_SELECT_COLOR_MAX];  // 대표 색상
   int         m_nPercent[SEARCH_SELECT_COLOR_MAX]; // 점유률

   int         m_nWidth, m_nHeight, m_nDepth;
   int         m_nHistogramLen;  // 히스토그램 데이터 길이
   void*       m_pHistogramData; // 히스토그램 버퍼

public:
   CIVCPSearchColorFilter();
   ~CIVCPSearchColorFilter();

   void SetHistogramData(void *pBuffer, int nLen = 0, int nWidth = 0, int nHeight = 0, int nDepth = 0);
   BOOL GetHistogramData(void **pBuffer, int &nLen, int &nWidth, int &nHeight, int &nDepth);

   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

class CIVCPSearchObjectFilter
{
public:
   int   m_nOptions;
   float m_fMinWidth;
   float m_fMaxWidth;
   float m_fMinHeight;
   float m_fMaxHeight;
   float m_fMinArea;
   float m_fMaxArea;
   float m_fMinAspectRatio;
   float m_fMaxAspectRatio;
   float m_fMinAxisLengthRatio;
   float m_fMaxAxisLengthRatio;
   float m_fMinMajorAxisLength;
   float m_fMaxMajorAxisLength;
   float m_fMinMinorAxisLength;
   float m_fMaxMinorAxisLength;
   float m_fMinNorSpeed;
   float m_fMaxNorSpeed;
   float m_fMinSpeed;
   float m_fMaxSpeed;
   float m_fMinRealArea;
   float m_fMaxRealArea;
   float m_fMinRealDistance;
   float m_fMaxRealDistance;
   float m_fMinRealWidth;
   float m_fMaxRealWidth;
   float m_fMinRealHeight;
   float m_fMaxRealHeight;
   float m_fMinRealMajorAxisLength;
   float m_fMaxRealMajorAxisLength;
   float m_fMinRealMinorAxisLength;
   float m_fMaxRealMinorAxisLength;
   float m_fMinRealSpeed;
   float m_fMaxRealSpeed;

public:
   enum IVCP_SearchObjectFilterOption
   {
      FILTER_ENABLE         = 0x00000001,
      USE_REAL_SIZE_VALUES  = 0x00000002,
      USE_WIDTH             = 0x00000004,
      USE_HEIGHT            = 0x00000008,
      USE_AREA              = 0x00000010,
      USE_ASPECT_RATIO      = 0x00000020,
      USE_AXIS_LENGTH_RATIO = 0x00000040,
      USE_MAJOR_AXIS_LENGTH = 0x00000080,
      USE_MINOR_AXIS_LENGTH = 0x00000100,
      USE_NORMALIZED_SPEED  = 0x00000200,
      USE_SPEED             = 0x00000400,
      USE_REAL_DISTANCE     = 0x00000800,
   };

public:
   CIVCPSearchObjectFilter();

   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////

class CIVCPSearchObjectReq : public CIVCPPacketExt
{
public:
   int      m_nSearchID;
   int      m_nObjID; // (mkjang-140926)
   CHFLAG   m_nChannelFlag;
   FILETIME m_ftStart;
   FILETIME m_ftEnd;
   int      m_nCondFlag;   // 검색 조건에 대한 플래그

   int                     m_nObjFlag;      // 객체 종류
   CIVCPSearchColorFilter  m_ColorFilter;   // 색상 필터
   CIVCPSearchObjectFilter m_ObjectFilter;  // 객체 필터

   BOOL     m_bThumbnail;  // 섬네일 전송 여부

public:
   enum IVCP_SearchObjectCondition
   {
      USE_COLOR     = 0x00000001,
      USE_OBJTYPE   = 0x00000002,
      USE_OBJFILTER = 0x00000004,
   };

public:
   CIVCPSearchObjectReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchPlateReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPSearchPlateReq : public CIVCPPacketExt
{
public:
   int      m_nSearchID;
   CHFLAG   m_nChannelFlag;
   FILETIME m_ftStart;
   FILETIME m_ftEnd;
   char     m_strPlateNumber[32];
   BOOL     m_bUseColor;            // 색상 검색 사용
   COLORREF m_crColor;              // 검색 색상

   BOOL     m_bThumbnail;        // 섬네일 전송 여부

public:
   CIVCPSearchPlateReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchFaceReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPSearchFaceReq : public CIVCPPacketExt
{
public:
   int      m_nSearchID;
   CHFLAG   m_nChannelFlag;
   FILETIME m_ftStart;
   FILETIME m_ftEnd;
   char     m_strName[64];
   BOOL     m_bThumbnail;        // 섬네일 전송 여부

public:
   CIVCPSearchFaceReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchLogReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPSearchLogReq : public CIVCPPacketExt
{
public:
   int      m_nSearchID;
   CHFLAG   m_nChannelFlag;
   FILETIME m_ftStart;
   FILETIME m_ftEnd;
   char     m_strKeyword[128];
   int      m_nOption;           // 검색 옵션 플래그
   int      m_nEvtFlag;          // 이벤트 플래그
   int      m_nObjFlag;          // 객체 플래그
   BOOL     m_bThumbnail;        // 섬네일 전송 여부

public:
   CIVCPSearchLogReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchStopReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPSearchStopReq : public CIVCPPacketExt
{
public:
   int      m_nSearchID;

public:
   CIVCPSearchStopReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchCommonRes
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPSearchResultRes : public CIVCPPacketExt
{
public:
   int      m_nSearchID;
   int      m_nResult;

public:
   CIVCPSearchResultRes ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchProgress
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPSearchProgress : public CIVCPPacketExt
{
public:
   int      m_nSearchID;
   int      m_nStep;
   int      m_nParam;

public:
   enum IVCP_SearchProgressStep
   {
      BEGIN     = 0,
      END       = 1,
      NOT_FOUND = 2,
   };

public:
   CIVCPSearchProgress ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPSearchFrameInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPSearchFrameInfo : public CIVCPPlayFrameInfo  // not defined
{
public:
   int      m_nSearchID;
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPSearchEventInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPSearchEventInfo : public CIVCPLiveEventInfo  // same
{
public:
   int      m_nSearchID;
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPSearchObjectInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPSearchObjectInfo : public CIVCPLiveObjectInfo // same
{
public:
   int      m_nSearchID;
};


///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPSearchFaceInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPSearchLogInfo : public CIVCPLiveLogInfo  // not defined
{
public:
   int      m_nSearchID;
};

///////////////////////////////////////////////////////////////////////////////
//
// (CAME) CIVCPSystemSettingReq
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// (CAME) CIVCPSystemCammandReq
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// (CAME) CIVCPSystemAudioInOutReq
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// (DATA) CIVCPSystemAudioDataReq
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// (DATA) CIVCPSystemAudioData
//
///////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////
// 사용자 정보
struct _UserDataInfo
{
   int  m_nFaceID;
   int  m_Index;
   char m_szUserID[100];
   char m_szUserName[100];
   BCCL::Time m_Time;
   int  m_iAge, m_iGender;
   //int  m_iGlasses;
   char m_strBeloging_ID[30];
   char m_strPosition_ID[30];
   char m_strTEL[20];
   char m_strHP[20];
   int  m_iBlacklist_chk;

   int m_iWidth, m_iHeight; 
   char m_szImageFormat[10];

   float m_fRate;
   char m_szRequestType[3];
   BOOL m_bImage_Attach;

   byte* ImageBuffer;
   byte* SubImagebuffer;
   byte* SubImagebuffer_Svr;

   int iImageSize, iSubImageSize, iSubImageSize_Svr;

   char m_szVrifi_ID[50];
   int  m_iChannel_no;
   int  m_iEvtStatus;  // jmlee 2014 08 21 추가
   int  m_iEvt_ID;		// jmlee 2014 08 21 추가
   int  m_iFrameIndex; // jmlee 2014 08 21 추가
   int  m_iObjectID;		// jmlee 2014 10 08 추가
   int  m_iEvtZoneID;	// jmlee 2014 10 08 추가
   char m_szEvtZonName[32]; // jmlee 2014 10 08 추가
};

/***********************************************************************
[jmlee] 서버에서 사진 기반 검색 관련 구조체
***********************************************************************/
struct _ImagesInfoForFFV_Req  // 서버에 얼굴 이미지 버퍼를 전송 - 서버로 부터 FFV값을 추출하기 위한 데이터
{
   char m_szRequestType[3];
   char m_szID[100];
   char m_szRequest_Date[16];
   int  m_iTN_Max_Length, m_iTotal_Length;
   int  m_iFaceNum, m_iSystemID, m_iChannelNo, m_iObjectID;
   char m_szHWID[12*3];
};

struct _ImagesInfoForMatch_Req
{
   char m_szRequestType[3];
   char m_szID[100];
   char m_szRequest_Date[16];
   int  m_iTN_WH_Size, m_iTN_Length; // 섬네일 이미지 길이

   int  m_iImage_Max_Length/*블럭당 이미지 길이*/, m_iFFV_Max_Length/*블럭당 FFV 길이*/;
   int  m_iFaceNum, m_iSystemID, m_iChannelNo, m_iObjectID;
};

// 사용자 정보
struct _RecvFFVInfo
{
   char m_szID[100];
   byte* DataBuffer;
   int iDataSize;
};

// 서버로 부터 수신 받은 검색 결과 정보
struct _RecvSearchInfo
{
   byte* m_pDataBuffer;
   int m_iDataSize;
   int m_iCount;
};


#ifdef __IVCP_SUPPORT_MMS

///////////////////////////////////////////////////////////////////////////////
//
// CIVCPMMSLoginRes : 로그인 응답 (통합관리서버)
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPMMSLoginRes : public CIVCPConnLoginRes//public CIVCPPacketExt
{
public:
   int m_nGroupLevel;   // 그룹권한

public:
   CIVCPMMSLoginRes ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// CIVCPMMSFailoverConnInfoNotify : 통합관리서버 페일오버 접속정보 알림
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPMMSFailoverConnInfoNotify : public CIVCPConnLoginRes
{
public:
   CIVCPMMSFailoverConnInfoNotify();

public:
   std::string  m_szMMSPrimaryAddr;     // 기본 통합관리서버 주소
   std::string  m_szMMSStandbyAddr;     // 예비 통합관리서버 주소

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);

};

///////////////////////////////////////////////////////////////////////////////
//
// CIVCPUserAccount_Notify : 사용자 계정 정보 전달
//
///////////////////////////////////////////////////////////////////////////////

typedef struct tagUserAccountInfo
{
   std::string  m_szUserID;
   std::string  m_szPassword;
   std::string  m_szDescription;
   CHAR     m_cLevel;
   CHAR     m_cUserType;
} USER_ACCOUNT_INFO;


class CIVCPUserAccount_Notify : public CIVCPPacketExt
{
public:
   CIVCPUserAccount_Notify ();

   void  AddUserAccountInfo(USER_ACCOUNT_INFO *pInfo);
   void  SetUserAccountInfoList(std::list <USER_ACCOUNT_INFO> *pInfoList);
   void  Clear();
   void  GetUserInfoList(OUT std::list <USER_ACCOUNT_INFO> *pInfoList);
   std::list <USER_ACCOUNT_INFO> *GetUserInfoList() { return &m_listUserAccountInfo;}

private:
   std::list <USER_ACCOUNT_INFO>  m_listUserAccountInfo;

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// CIVCPSystemStatusInfo_Req : 시스템 상태 정보 요청
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPSystemStatusInfo_Req : public CIVCPPacketExt
{
public:
   BOOL m_bStart;    // 상태정보 전송 시작/중지

public:
   CIVCPSystemStatusInfo_Req ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};


///////////////////////////////////////////////////////////////////////////////
//
// CIVCPSystemStatusInfo_Res : 시스템 상태 정보 응답
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPSystemStatusInfo_Res : public CIVCPPacketExt
{
public:
   BYTE  m_cCPUUsage;      // CPU 사용률 (%)
   UINT  m_nMemoryUsage;   // 메모리 사용량 (MB)
   UINT  m_nMemorySize;    // 메모리 총크기 (MB)
   UINT  m_nHDDUsage;      // HDD 사용량 (MB)
   UINT  m_nHDDSize;       // HDD 총크기 (MB)

public:
   CIVCPSystemStatusInfo_Res ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};


///////////////////////////////////////////////////////////////////////////////
//
// CIVCPNVRServer_ConnectInfo_Req : 저장/분배서버 접속정보 요청
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPNVRServer_ConnectInfo_Req : public CIVCPPacketExt
{
public:
   CIVCPNVRServer_ConnectInfo_Req();
};



typedef struct tagNVRServerInfo
{
   std::string  m_szNVRServerName;
   std::string  m_szIPAddr;
   INT      m_nPort;
   std::string  m_szDescription;
} NVRSERVER_INFO;

///////////////////////////////////////////////////////////////////////////////
//
// CIVCPNVRServer_ConnectInfo_Res : 저장/분배서버 접속정보 응답
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPNVRServer_ConnectInfo_Res : public CIVCPPacketExt
{
public:
   CIVCPNVRServer_ConnectInfo_Res();

   void  AddConnectInfo(NVRSERVER_INFO *pInfo);
   void  Clear();
   std::list <NVRSERVER_INFO> *GetConnectInfo() { return &m_listNVRServerInfo;}

private:
   std::list <NVRSERVER_INFO>  m_listNVRServerInfo;

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);

};

/////////////////////////////////////////////////////////////////////////////////////////
//
// IVCP_ID_MMS_NVRServer_ConfigFile_Transfer_Req : 저장/분배서버 설정 파일 전송 요청
//
/////////////////////////////////////////////////////////////////////////////////////////

class CIVCPNVRServer_ConfigFile_Transfer_Req : public CIVCPPacketExt
{
public:
   CIVCPNVRServer_ConfigFile_Transfer_Req();
};

/////////////////////////////////////////////////////////////////////////////////////////
//
// IVCP_ID_MMS_NVRServer_ConfigFile_Transfer_Res : 저장/분배서버 설정 전송 응답 (XML 파일전송)
//
/////////////////////////////////////////////////////////////////////////////////////////

class CIVCPNVRServer_ConfigFile_Transfer_Res : public CIVCPPacketExt
{
public:
   CIVCPNVRServer_ConfigFile_Transfer_Res();
   ~CIVCPNVRServer_ConfigFile_Transfer_Res();

   INT      m_nTotalFileCount;
   INT      m_nCurFileIndex;
   std::string  m_szCurFileName;
   std::string  m_szCurFileContent;

public:
   INT      m_nXMLDataLen;
   INT      m_nBodyLen;
   char*    m_pBodyBuffer; // 전달용 버퍼
   BYTE*    m_pConfigFileBuffer;

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
   virtual int ReadFromPacket (CIVCPPacket& aPacket);
   virtual int WriteToPacket  (CIVCPPacket& aPacket);

   void ParseBodyBuffer ();
   void ParseConfigFileBuffer();
   void CreateBodybuffer (byte *pEncodedBuff, int nEncodedLen);
   void DeleteBodyBuffer ();
};


///////////////////////////////////////////////////////////////////////////////
//
// CIVCPClosePacketNotify : 시스템 종료 메시지 전달
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPGracefulCloseNotify : public CIVCPPacketExt
{

public:
   CIVCPGracefulCloseNotify ();
};


typedef struct tagNVRServerFailOverInfo
{
   std::string  m_szNVRServerName;
   std::string  m_szIPAddr_Primary;  // 주서버 IP
   std::string  m_szIPAddr_Standby;  // 예비서버 IP
   INT          m_nPort;
   std::string  m_szDescription;
} NVRSERVER_FAILOVER_INFO;

///////////////////////////////////////////////////////////////////////////////
//
// CIVCPNVRServer_FailOver_Notify : 저장/분배서버 페일오버 알림
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPNVRServer_FailOver_Notify : public CIVCPPacketExt
{
public:
   CIVCPNVRServer_FailOver_Notify();

   void  SetFailOverInfo(NVRSERVER_FAILOVER_INFO *pInfo);
   void  GetFailOverInfo(OUT NVRSERVER_FAILOVER_INFO *pInfo);

private:
   NVRSERVER_FAILOVER_INFO m_NVRServerFailOverInfo;

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// CIVCPMMS_FailOver_Notify : 통합관리서버 페일오버 알림
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPMMS_FailOver_Notify : public CIVCPPacketExt
{
public:
   CIVCPMMS_FailOver_Notify();
};

///////////////////////////////////////////////////////////////////////////////
//
// CIVCPVMS_KeepAliveNotify : VMS Keep Alive 패킷
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPVMS_KeepAlive_Notify : public CIVCPPacketExt
{
public:
   CIVCPVMS_KeepAlive_Notify();

   char  m_szGuid[GUID_LENGTH+1];

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

class CIVCConnectInfo : public CIVCPPacketExt
{
public:
   CIVCConnectInfo();

public:
   char	m_szAddr[16];		      //파일 관리 아이디
   int	m_nPort;				      //0:JPEG    1: AVI
   char	m_szId[31];
   char	m_szPasswd[31];   
   char  m_szGuid[GUID_LENGTH+1];

   // 예비-중계서버 접속정보
   char	m_szStandbyVRS_Addr[16];
   int	m_nStandbyVRS_Port;
   char	m_szStandbyVRS_Id[31];
   char	m_szStandbyVRS_Passwd[31];
   char  m_szStandbyVRS_Guid[GUID_LENGTH+1];
   char	m_szSystemGuid[GUID_LENGTH+1];
public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

class CIVCPXMLData : public CIVCPPacketExt
{
public:
   CIVCPXMLData();
   ~CIVCPXMLData();

public:
   char*    m_pBodyBuffer; // 전달용 버퍼

public:
   void CreateBodybuffer (byte *pEncodedBuff, int nEncodedLen);
   void DeleteBodyBuffer ();
};


class CVCWPLiveVideoStreamData : public CIVCPPacketExt
{
public:
   std::string  m_cCameraGUID;
   int      m_nCameraUID;
   int      m_nStreamIdx;
   int      m_nCodecID;
   int      m_nWidth;
   int      m_nHeight;
   float    m_fFrameRate;
   int      m_nFrameFlag;
   FILETIME m_ftFrameTime;
   int      m_nDataSize;

public:
   CVCWPLiveVideoStreamData ();
   ~CVCWPLiveVideoStreamData (){}

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};


class CVCWPConnCameraListRes : public CIVCPConnCameraListRes
{
public:
   CVCWPConnCameraListRes();
   ~CVCWPConnCameraListRes() {};

   // TYLEE: iVMS, 해당 카메라의 IVX서버 GUID
   char  m_cSystemGUID[GUID_LENGTH+1];
   char  m_cSystemIPAddr[IPADDR_LENGTH+1];

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);

};


class CIVCPCameraInfoList : CIVCPPacketExt
{
public:
   CIVCPCameraInfoList();
   ~CIVCPCameraInfoList();

   std::list<VOID *> m_list;
   char  m_cSystemGUID[GUID_LENGTH+1];
   char  m_cSystemIPAddr[IPADDR_LENGTH+1];
   INT   m_nCameraCount;
   //std::list<VOID *> m_listCameraInfo;
   //std::vector<INT>  vt;
   
public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
   void  Clear();

};


class CIVCPConnCameraListSetReq : public CIVCPPacketExt
{
public:
   int m_nStart;  // 읽어올 카메라 인덱스 (0: 전부)
   int m_nCount;  // 읽어올 카메라 개수

public:
   CIVCPConnCameraListSetReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};


class CVCWPConnCameraListSetRes : public CIVCPPacketExt
{
public:
   CVCWPConnCameraListSetRes();
   ~CVCWPConnCameraListSetRes();

   INT   m_nListSetCount;

   //std::list<CIVCPCameraInfoList>   m_listCameraInfoSet;
   std::list<VOID *>   m_listCameraInfoSet;

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
   
   void Clear();

};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPEventIPEXInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPEventIPEXInfo : public CIVCPPacketExt
{
public:
   std::string  m_cCameraGUID;
   int        m_nCameraUID;        // 카메라 UID
   char       m_szCameraName[100]; // 카메라 Name
   char       m_szEventType[50];   // 이벤트 종류
   char       m_szObjectType[20];  // 객체 종류 (사람, 차량, 미확인)
   char       m_szTelNo[100];      // 담당자 전화 번호

   int        m_nXMLDataLen;
   int        m_nBodyLen;
   char*      m_pBodyBuffer; // 전달용 버퍼

public:
   CIVCPEventIPEXInfo ();
   ~CIVCPEventIPEXInfo ();

public:
   virtual int ReadXML        (CXMLIO* pIO);
   virtual int WriteXML       (CXMLIO* pIO);
   virtual int ReadFromPacket (CIVCPPacket& aPacket);
   virtual int WriteToPacket  (CIVCPPacket& aPacket);

   void ParseBodyBuffer ();
   void CreateBodybuffer (byte *pEncodedBuff, int nEncodedLen);
   void DeleteBodyBuffer ();
};

class CIVCPCarPlateAlarmInfo
{
public:
   int      m_nAlarmId;
   char     m_szAlarmName[32];
public:
   CIVCPCarPlateAlarmInfo(){}
   ~CIVCPCarPlateAlarmInfo(){}
public:
   virtual int ReadXML        (CXMLIO* pIO);
   virtual int WriteXML       (CXMLIO* pIO);
};

typedef std::deque<CIVCPCarPlateAlarmInfo*> IVCPBlAlarmListInfo;

class CIVCPCarPlateHotListInfo
{
public:
   int      m_nHotInfoId;       //리스트ID
   std::string m_szCarPlateNo;     //차량번호
   std::string m_szCarOwner;       //소유주이름
   std::string m_szRegLocate;      //등록지
   std::string m_szCarModel;       //차량모델 종류
   std::string m_szCarColor;       //차량색상
   std::string	m_szCompetentDep;   //주무부처(관할)   
   std::string m_szDescription;	  //특이사항
   int	   m_nAlarmId;         //알람 종류
   int      m_nAlarmPriorty;        //알람 우선순위
public:
   CIVCPCarPlateHotListInfo();   
   ~CIVCPCarPlateHotListInfo(){}
public:
   virtual int ReadXML        (CXMLIO* pIO);
   virtual int WriteXML       (CXMLIO* pIO);
};

typedef std::deque<CIVCPCarPlateHotListInfo*> IVCPHotListInfo;


class CIVCPAlarmListInfo
{
public:
   int      m_nAlarmCnt;
   IVCPBlAlarmListInfo  m_AlarmList;
public:
   CIVCPAlarmListInfo();
   ~CIVCPAlarmListInfo();
public:
   virtual int ReadXML        (CXMLIO* pIO);
   virtual int WriteXML       (CXMLIO* pIO);
   void ClearInfo();
};

class CIVCPHotListListInfo
{
public:
   int      m_nHListCnt;
   IVCPHotListInfo      m_HotList;
public:
   CIVCPHotListListInfo();
   ~CIVCPHotListListInfo();
public:
   virtual int ReadXML        (CXMLIO* pIO);
   virtual int WriteXML       (CXMLIO* pIO);
   void ClearInfo();
};


class CIVCPCarPlateHotList : public CIVCPPacketExt
{
public:
   CIVCPAlarmListInfo   m_AlarmList;
   CIVCPHotListListInfo m_HotList;
public:
   virtual int ReadXML        (CXMLIO* pIO);
   virtual int WriteXML       (CXMLIO* pIO);
};
#endif   //__IVCP_SUPPORT_MMS

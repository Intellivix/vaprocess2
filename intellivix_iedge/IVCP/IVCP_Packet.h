﻿///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2013 illisis. Inc. All rights reserved.
//
// FileName: IVCP_Packet.h
//
// Function: CIVCPPacket class for IVCP 3.0.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "IVCP_Define.h"

class CIVCPPacket {
   ////////////////////////////////////////////////////////////////////
   // CIVCPPacketHeader Class

   class CIVCPPacketHeader {
   public:
      CIVCPPacketHeader();

   public:
      char m_strMessage[20]; // Communication message of IVCP.
      int m_nOptions; // Packet Options
      int m_nBinaryOptionalLen; // Binary length of IVCP.
      int m_nBodyLen; // The whole Body length of IVCP.
   };

   ////////////////////////////////////////////////////////////////////

public:
   CIVCPPacketHeader m_Header; // Packet block header of IVCP.
   char* m_pBody; // Packet block body of IVCP.

public:
   CIVCPPacket();
   CIVCPPacket( const char* szMessage );
   ~CIVCPPacket();

public:
   CIVCPPacket& operator=( CIVCPPacket& from );

public:
   BOOL ParseHeader( const char* resString, int nLen );
   BOOL AllocBody();
   void FreeBody();
};

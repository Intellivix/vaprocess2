﻿///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2013 illisis. Inc. All rights reserved.
//
// FileName: IVCP_PacketStruct.cpp
//
// Function: Data structure using C2XML for IVCP 3.0.
//
///////////////////////////////////////////////////////////////////////////////

#include "IVCP_PacketStruct.h"

#define SAFE_FREE( c ) \
   if( c != NULL ) {   \
      free( c );       \
      c = NULL;        \
   }

using namespace BCCL;

///////////////////////////////////////////////////////////////////////////////
//
// string 관련 함수
//
///////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <sstream>

void GetSplitStringList( const std::string& strIn, char ch, std::vector<std::string>& vecOut )
{
   std::istringstream iss( strIn );
   std::string token;
   while( std::getline( iss, token, ch ) ) {
      vecOut.push_back( token );
   }
}
#if 0
template<typename ... Args>
std::string sformat(const std::string& format, Args ... args)
{
   size_t size = snprintf(nullptr, 0, format.c_str(), args ...) + 1; // Extra space for '\0'
   std::unique_ptr<char[]> buf(new char[size]);
   snprintf(buf.get(), size, format.c_str(), args ...);
   return std::string(buf.get(), buf.get() + size - 1); // We don't want the '\0' inside
}
#else
std::string sformat( const char* format, ... )
{
   const int MAX_BUFFER_LENGTH = 512;

   va_list ap;
   char szMsg[MAX_BUFFER_LENGTH] = {
      0,
   };

   va_start( ap, format );
   vsprintf( szMsg, format, ap );
   va_end( ap );

   return std::string( szMsg );
}
#endif

   ///////////////////////////////////////////////////////////////////////////////
   //
   // Defines
   //
   ///////////////////////////////////////////////////////////////////////////////

#define ER_EVENT_LOITERING 0
#define ER_EVENT_PATH_PASSING 1
#define ER_EVENT_DIRECTIONAL_MOTION 2
#define ER_EVENT_APPEARING 3
#define ER_EVENT_DISAPPEARING 4
#define ER_EVENT_STOPPING 5
#define ER_EVENT_ABANDONED 6
#define ER_EVENT_LINE_CROSSING 7
#define ER_EVENT_SMOKE 8
#define ER_EVENT_FLAME 9
#define ER_EVENT_FALLING 10
#define ER_EVENT_GROUPING 11
#define ER_EVENT_FIGHTING 12
#define ER_EVENT_UNSAFE_CROSSING 13
#define ER_EVENT_CAR_ACCIDENT 14
#define ER_EVENT_DISPERSION 15
#define ER_EVENT_TRAFFIC_CONGESTION 16
#define ER_EVENT_COLOR_CHANGE 17
#define ER_EVENT_CAR_PARKING 18
#define ER_EVENT_REMOVED 19
#define ER_EVENT_WATER_LEVEL 20
#define ER_EVENT_ZONE_COLOR 21
#define ER_EVENT_DWELL 22
#define ER_EVENT_DWELL_OVERCROWDED 23
#define ER_EVENT_DWELL_OVERWAITING 24

///////////////////////////////////////////////////////////////////////////////
//
// Global Functions
//
///////////////////////////////////////////////////////////////////////////////

typedef struct _IvcpPacktIDString {
   const char* prefix;
   const char* desc;
} IvcpPacktIDString;

static IvcpPacktIDString s_IvcpPacktIDString[] = {
   "U",
   "U", // IVCP_ID_Unknown
   "N",
   "OPTION", // IVCP_ID_Conn_OptionsReq
   "N",
   "OPTION", // IVCP_ID_Conn_OptionsRes
   "N",
   "LOGIN", // IVCP_ID_Conn_LoginReq
   "N",
   "LOGIN", // IVCP_ID_Conn_LoginRes
   "N",
   "LOGOUT", // IVCP_ID_Conn_LogoutReq
   "N",
   "LOGOUT", // IVCP_ID_Conn_LogoutRes
   "N",
   "CAMLIST", // IVCP_ID_Conn_CameraListReq
   "N",
   "CAMLIST", // IVCP_ID_Conn_CameraListRes
   "N",
   "STARTUP", // IVCP_ID_Conn_StartUpReq
   "N",
   "STARTUP", // IVCP_ID_Conn_StartUpRes
   "N",
   "SHUTDOWN", // IVCP_ID_Conn_ShutDownReq
   "N",
   "SHUTDOWN", // IVCP_ID_Conn_ShutDownRes
   "N",
   "SNAPSHOT", // IVCP_ID_Conn_SnapshotReq,              // 초기 접속시 카메라 스냅 화면 요청
   "N",
   "SNAPSHOT", // IVCP_ID_Conn_SnapshotRes,

   "C",
   "LIVEMETA", // IVCP_ID_Camera_LiveMetaDataReq,
   "C",
   "LIVEMETA", // IVCP_ID_Camera_LiveMetaDataRes,
   "C",
   "LIVEVIDEO", // IVCP_ID_Camera_LiveVideoReq,
   "C",
   "LIVEVIDEO", // IVCP_ID_Camera_LiveVideoRes,

   "C",
   "PLAYSTART", // IVCP_ID_Camera_PlayStartReq,
   "C",
   "PLAYSTART", // IVCP_ID_Camera_PlayStartRes,
   "C",
   "PLAYSTOP", // IVCP_ID_Camera_PlayStopReq,
   "C",
   "PLAYSTOP", // IVCP_ID_Camera_PlayStopRes,
   "C",
   "PLAYCTRL", // IVCP_ID_Camera_PlayControlReq,
   "C",
   "PLAYCTRL", // IVCP_ID_Camera_PlayControlRes,
   "C",
   "PLAYDATA", // IVCP_ID_Camera_PlayDataStreamReq,
   "C",
   "PLAYDATA", // IVCP_ID_Camera_PlayDataStreamReq,
   "C",
   "PLAYDATACTRL", // IVCP_ID_Camera_PlayControlReq,
   "C",
   "PLAYDATACTRL", // IVCP_ID_Camera_PlayControlRes,
   "C",
   "CARPLATE", // IVCP_ID_Camera_CarPlateRecognitionReq,
   "C",
   "CARPLATE", // IVCP_ID_Camera_CarPlateRecognitionRes,

   "C",
   "SNAPSHOT", // IVCP_ID_Camera_SnapshotReq
   "C",
   "SNAPSHOT", // IVCP_ID_Camera_SnapshotRes,
   "C",
   "SETTING", // IVCP_ID_Camera_SettingReq
   "C",
   "SETTING", // IVCP_ID_Camera_SettingRes,
   "C",
   "COMMAND", // IVCP_ID_Camera_CommandReq,
   "C",
   "COMMAND", // IVCP_ID_Camera_CommandRes,
   "C",
   "TALKAUDIO", // IVCP_ID_Camera_TalkAudioReq,
   "C",
   "TALKAUDIO", // IVCP_ID_Camera_TalkAudioRes,
   "C",
   "TALKSTATUS", // IVCP_ID_Camera_TalkStatus,
   "C",
   "AUDIODATA", // IVCP_ID_Camera_SendAudioData,
   "C",
   "AUDIODATA", // IVCP_ID_Camera_RecvAudioData,

   "Z",
   "CONTMOV", // IVCP_ID_Ptz_ContMoveReq,
   "Z",
   "CONTMOV", // IVCP_ID_Ptz_ContMoveRes,
   "Z",
   "ABSMOV", // IVCP_ID_Ptz_AbsMoveReq,
   "Z",
   "ABSMOV", // IVCP_ID_Ptz_AbsMoveRes,
   "Z",
   "BOXMOV", // IVCP_ID_Ptz_BoxMoveReq,
   "Z",
   "BOXMOV", // IVCP_ID_Ptz_BoxMoveRes,
   "Z",
   "GETABSPOS", // IVCP_ID_Ptz_GetAbsPosReq,
   "Z",
   "GETABSPOS", // IVCP_ID_Ptz_GetAbsPosRes,

   "L",
   "FRAMEINFO", // IVCP_ID_Live_FrameInfo,
   "L",
   "EVENTINFO", // IVCP_ID_Live_EventInfo,
   "L",
   "OBJECTINFO", // IVCP_ID_Live_ObjectInfo,
   "L",
   "PLATEINFO", // IVCP_ID_Live_PlateInfo,
   "L",
   "FACEINFO", // IVCP_ID_Live_FaceInfo,
   "L",
   "LOGINFO", // IVCP_ID_Live_LogInfo,
   "L",
   "PTZDATAINFO", // IVCP_ID_Live_PtzDataInfo,
   "L",
   "PRESETIDINFO", // IVCP_ID_Live_PresetIdInfo,
   "L",
   "EVTZONEINFO", // IVCP_ID_Live_EventZoneInfo,
   "L",
   "EVTZONECNT", // IVCP_ID_Live_EventZoneCount,
   "L",
   "PRESETMAPINFO", // IVCP_ID_Live_PresetMapInfo,
   "L",
   "SETTINGINFO", // IVCP_ID_Live_SettingInfo,
   "L",
   "VIDEO", // IVCP_ID_Live_VideoData,
   "L",
   "AUDIO", // IVCP_ID_Live_AudioData,
   "L",
   "FRAMEOBJ", // IVCP_ID_Live_FrmObjInfo
   "L",
   "STATUS", // IVCP_ID_Live_Status
   "L",
   "ACINFO", // IVCP_ID_Live_ACInfo
   "L",
   "HEATMAP", // IVCP_ID_Live_HeatMap
   "L",
   "STATISTICSINFO", // IVCP_ID_Live_StatisticsInfo

   "P",
   "FRAMEINFO", // IVCP_ID_Play_FrameInfo,
   "P",
   "EVENTINFO", // IVCP_ID_Play_EventInfo,
   "P",
   "OBJECTINFO", // IVCP_ID_Play_ObjectInfo,
   "P",
   "PLATEINFO", // IVCP_ID_Play_PlateInfo,
   "P",
   "FACEINFO", // IVCP_ID_Play_FaceInfo,
   "P",
   "PTZDATAINFO", // IVCP_ID_Play_PtzDataInfo,
   "P",
   "PRESETIDINFO", // IVCP_ID_Play_PresetIdInfo,
   "P",
   "EVTZONEINFO", // IVCP_ID_Play_EventZoneInfo,
   "P",
   "EVTZONECNT", // IVCP_ID_Play_EventZoneCount,
   "P",
   "VIDEO", // IVCP_ID_Play_VideoData,
   "P",
   "AUDIO", // IVCP_ID_Play_AudioData,
   "P",
   "PROGRESS", // IVCP_ID_Play_Progress,

   "F",
   "FRAME", // IVCP_ID_Search_FrameReq
   "F",
   "FRAME", // IVCP_ID_Search_FrameRes
   "F",
   "EVENT", // IVCP_ID_Search_EventReq
   "F",
   "EVENT", // IVCP_ID_Search_EventRes
   "F",
   "OBJECT", // IVCP_ID_Search_ObjectReq
   "F",
   "OBJECT", // IVCP_ID_Search_ObjectRes
   "F",
   "PLATE", // IVCP_ID_Search_PlateReq
   "F",
   "PLATE", // IVCP_ID_Search_PlateRes
   "F",
   "FACE", // IVCP_ID_Search_FaceReq
   "F",
   "FACE", // IVCP_ID_Search_FaceRes
   "F",
   "LOG", // IVCP_ID_Search_LogReq
   "F",
   "LOG", // IVCP_ID_Search_LogRes
   "F",
   "STOP", // IVCP_ID_Search_StopReq (통합)
   "F",
   "STOP", // IVCP_ID_Search_StopRes

   "F",
   "PROGRESS", // IVCP_ID_Search_Progress (검색 진행 정보)
   "F",
   "FRAMEINFO", // IVCP_ID_Search_FreameInfo
   "F",
   "EVENTINFO", // IVCP_ID_Search_EventInfo
   "F",
   "OBJECTINFO", // IVCP_ID_Search_ObjectInfo
   "F",
   "PLATEINFO", // IVCP_ID_Search_PlateInfo
   "F",
   "FACEINFO", // IVCP_ID_Search_FaceInfo
   "F",
   "LOGINFO", // IVCP_ID_Search_LogInfo

   "S",
   "SETTING", // IVCP_ID_System_SettingReq
   "S",
   "SETTING", // IVCP_ID_System_SettingRes,
   "S",
   "COMMAND", // IVCP_ID_System_CommandReq,
   "S",
   "COMMAND", // IVCP_ID_System_CommandRes,
   "S",
   "MONITOR", // IVCP_ID_System_MonitorReq,
   "S",
   "MONITOR", // IVCP_ID_System_MonitorRes,
   "S",
   "MONITORINFO", // IVCP_ID_System_MonitorInfo,
   "S",
   "AUDIOINOUT", // IVCP_ID_System_AudioInOutReq,
   "S",
   "AUDIOINOUT", // IVCP_ID_System_AudioInOutRes,
   "S",
   "AUDIODATA", // IVCP_ID_System_SendAudioData,
   "S",
   "AUDIODATA", // IVCP_ID_System_RecvAudioData,

   "C",
   "EXPORTSTART", // IVCP_ID_Camera_ExportStartReq,
   "C",
   "EXPORTSTART", // IVCP_ID_Camera_ExportStartRes,
   "C",
   "EXPORTSTOP", // IVCP_ID_Camera_ExportStopReq,
   "C",
   "EXPORTSTOP", // IVCP_ID_Camera_ExportStopRes,

   "E",
   "PROGRESS", // IVCP_ID_Export_Progress,
   "E",
   "FRAMEINFO", // IVCP_ID_Export_FrameInfo,
   "E",
   "EVENTINFO", // IVCP_ID_Export_EventInfo,
   "E",
   "OBJECTINFO", // IVCP_ID_Export_ObjectInfo,
   "E",
   "PLATEINFO", // IVCP_ID_Export_PlateInfo,
   "E",
   "FACEINFO", // IVCP_ID_Export_FaceInfo,
   "E",
   "PTZDATAINFO", // IVCP_ID_Export_PtzDataInfo,
   "E",
   "PRESETIDINFO", // IVCP_ID_Export_PresetIdInfo,
   "E",
   "EVTZONEINFO", // IVCP_ID_Export_EventZoneInfo,
   "E",
   "EVTZONECNT", // IVCP_ID_Export_EventZoneCount,
   "E",
   "VIDEO", // IVCP_ID_Export_VideoData,
   "E",
   "AUDIO", // IVCP_ID_Export_AudioData,

   "R",
   "FACEUSER", // IVCP_ID_Data_FaceUserData,
   "R",
   "FACEIMAGE", // IVCP_ID_Data_FaceImageData,
   "R",
   "FACEMATCH", // IVCP_ID_Data_FaceMatchData,
   "R",
   "FACESEARCH", // IVCP_ID_Data_FaceSearchData

   "M",
   "MMSLogInRes", // IVCP_ID_MMS_LoginRes
   "M",
   "UserAccountNotify", // IVCP_ID_MMS_UserAccountNotify
   "M",
   "SysStatusInfoReq", // IVCP_ID_MMS_SystemStatusInfo_Req
   "M",
   "SysStatusInfoRes", // IVCP_ID_MMS_SystemStatusInfo_Res
   "M",
   "NVRSvrConnInfoReq", // IVCP_ID_MMS_NVRServer_ConnectInfo_Req
   "M",
   "NVRSvrConnInfoRes", // IVCP_ID_MMS_NVRServer_ConnectInfo_Res
   "M",
   "NVRSvrCfgTransReq", // IVCP_ID_MMS_NVRServer_ConfigFile_Transfer_Req
   "M",
   "NVRSvrCfgTransRes", // IVCP_ID_MMS_NVRServer_ConfigFile_Transfer_Res
   "M",
   "NVRSvrGraceClose", // IVCP_ID_MMS_NVRServer_GracefulClose_Notify
   "M",
   "NVRFailOverNotify", // IVCP_ID_MMS_NVRServer_FailOver_Notify
   "M",
   "KeepAliveNotify", //
   "M",
   "MMSFailoverConnInfoNotify", // IVCP_ID_MMS_MMS_Failover_Conn_Info_Notify
   "M",
   "MMSFailoverNotify", // IVCP_ID_MMS_MMS_FailOver_Notify
   "M",
   "VRSConInfo", // VRS 접속 정보 전달 IVCP_ID_VRS_Connect_Information
   "M",
   "VRSEditNotify", // 채널 정보 변경 통지 IVCP_ID_VRS_Channel_Channge_nty

   //  비상벨 IPEX 연동
   "I",
   "EventIPEX", // IVCP_ID_Event_IPEX_Req
   "I",
   "EventIPEX", // IVCP_ID_Event_IPEX_Res
   "L",
   "EVEVNT_WIND", // IVCP_Window_Open/Close State
   "M",
   "HOTLIST", // IVCP_ID_REPORT_HOT_LIST,

   //  AC 출입 연동 추가
   "A",
   "EVENT_AC",
   "A",
   "DOORINFO_AC",
};

const char* IvcpGetPacketIDString( int nPacketID )
{
   return s_IvcpPacktIDString[nPacketID].desc;
}

const char* IvcpGetPacketDescString( int nPacketID )
{
   return s_IvcpPacktIDString[nPacketID].prefix;
}

void IvcpGetPacketString( char* strMessage, int nPacketID, int nBodyType )
{
   switch( nBodyType ) {
   case IVCP_TYPE_XML: // XML 텍스트
      sprintf( strMessage, "%s_%s", s_IvcpPacktIDString[nPacketID].prefix, s_IvcpPacktIDString[nPacketID].desc );
      break;
   case IVCP_TYPE_TXT: // Text 형태
      sprintf( strMessage, "%s.%s", s_IvcpPacktIDString[nPacketID].prefix, s_IvcpPacktIDString[nPacketID].desc );
      break;
   case IVCP_TYPE_BIN: // Binary 형태
      sprintf( strMessage, "%s-%s", s_IvcpPacktIDString[nPacketID].prefix, s_IvcpPacktIDString[nPacketID].desc );
      break;
   case IVCP_TYPE_XMZ: // XML 압축 (with Zip)
      sprintf( strMessage, "%s=%s", s_IvcpPacktIDString[nPacketID].prefix, s_IvcpPacktIDString[nPacketID].desc );
      break;
   }
}

BOOL IvcpIsBinaryPacket( const char* strMessage )
{
   if( strMessage[2] == '.' ) return TRUE;
   return FALSE;
}

int IvcpGetPacketType( const char* strMessage )
{
   switch( *strMessage ) {
   case 'N': // Connection
      return IVCP_TYPE_CONN;
      break;
   case 'S': // System
      return IVCP_TYPE_SYSTEM;
      break;
   case 'C': // Camera
      return IVCP_TYPE_CAMERA;
      break;
   case 'Z': // PTZ 제어
      return IVCP_TYPE_PTZ;
      break;
   case 'L': // 실시간 데이터
      return IVCP_TYPE_LIVE;
      break;
   case 'P': // 재생 데이터
      return IVCP_TYPE_PLAY;
      break;
   case 'F': // 검색 데이터
      return IVCP_TYPE_SEARCH;
      break;
   case 'E': // 내보내기 데이터
      return IVCP_TYPE_EXPORT;
      break;
   case 'R':
      return IVCP_TYPE_FACE;
      break;
   case 'M': // 통합관리서버(MMS)
      return IVCP_TYPE_MMS;
      break;
   case 'I':
      return IVCP_TYPE_IPEX;
      break;
   case 'A': // 출입 이벤트
      return IVCP_TYPE_AC;
      break;
   }
   return ( IVCP_TYPE_UNKNOWN );
}

int IvcpGetPacketIDforReq( const char* strMessage ) // Request 만 비교한다.
{
   int nPacketType = IvcpGetPacketType( strMessage );
   char* strString = (char*)strMessage + 2; // "C_STRING"
   switch( nPacketType ) {
   case IVCP_TYPE_CONN: // 연결
      if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Conn_OptionsReq].desc ) == 0 )
         return IVCP_ID_Conn_OptionsReq;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Conn_LoginReq].desc ) == 0 )
         return IVCP_ID_Conn_LoginReq;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Conn_LogoutReq].desc ) == 0 )
         return IVCP_ID_Conn_LogoutReq;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Conn_CameraListReq].desc ) == 0 )
         return IVCP_ID_Conn_CameraListReq;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Conn_StartUpReq].desc ) == 0 )
         return IVCP_ID_Conn_StartUpReq;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Conn_SnapshotReq].desc ) == 0 )
         return IVCP_ID_Conn_SnapshotReq;
      break;
   case IVCP_TYPE_SYSTEM: // 시스템
      if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_System_SettingReq].desc ) == 0 )
         return IVCP_ID_System_SettingReq;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_System_CommandReq].desc ) == 0 )
         return IVCP_ID_System_CommandReq;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_System_MonitorReq].desc ) == 0 )
         return IVCP_ID_System_MonitorReq;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_System_AudioInOutReq].desc ) == 0 )
         return IVCP_ID_System_AudioInOutReq;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_System_SendAudioData].desc ) == 0 )
         return IVCP_ID_System_SendAudioData;
      break;
   case IVCP_TYPE_CAMERA: // 카메라
      if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Camera_LiveMetadataReq].desc ) == 0 )
         return IVCP_ID_Camera_LiveMetadataReq;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Camera_LiveVideoReq].desc ) == 0 )
         return IVCP_ID_Camera_LiveVideoReq;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Camera_PlayStreamStartReq].desc ) == 0 )
         return IVCP_ID_Camera_PlayStreamStartReq;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Camera_PlayStreamStopReq].desc ) == 0 )
         return IVCP_ID_Camera_PlayStreamStopReq;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Camera_PlayStreamControlReq].desc ) == 0 )
         return IVCP_ID_Camera_PlayStreamControlReq;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Camera_SnapshotReq].desc ) == 0 )
         return IVCP_ID_Camera_SnapshotReq;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Camera_SettingReq].desc ) == 0 )
         return IVCP_ID_Camera_SettingReq;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Camera_CommandReq].desc ) == 0 )
         return IVCP_ID_Camera_CommandReq;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Camera_TalkAudioReq].desc ) == 0 )
         return IVCP_ID_Camera_TalkAudioReq;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Camera_SendAudioData].desc ) == 0 )
         return IVCP_ID_Camera_SendAudioData;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Camera_ExportStartReq].desc ) == 0 )
         return IVCP_ID_Camera_ExportStartReq;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Camera_ExportStopReq].desc ) == 0 )
         return IVCP_ID_Camera_ExportStopReq;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Camera_PlayDataStreamReq].desc ) == 0 )
         return IVCP_ID_Camera_PlayDataStreamReq;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Camera_PlayDataCtrlReq].desc ) == 0 )
         return IVCP_ID_Camera_PlayDataCtrlReq;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Camera_CarPlateRecognitionReq].desc ) == 0 )
         return IVCP_ID_Camera_CarPlateRecognitionReq;
      break;
   case IVCP_TYPE_PTZ: // PTZ 제어
      if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Ptz_ContMoveReq].desc ) == 0 )
         return IVCP_ID_Ptz_ContMoveReq;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Ptz_AbsMoveReq].desc ) == 0 )
         return IVCP_ID_Ptz_AbsMoveReq;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Ptz_BoxMoveReq].desc ) == 0 )
         return IVCP_ID_Ptz_BoxMoveReq;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Ptz_GetAbsPosReq].desc ) == 0 )
         return IVCP_ID_Ptz_GetAbsPosReq;
      break;
   case IVCP_TYPE_SEARCH: // 검색 관련
      if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Search_EventReq].desc ) == 0 )
         return IVCP_ID_Search_EventReq;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Search_FrameReq].desc ) == 0 )
         return IVCP_ID_Search_FrameReq;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Search_ObjectReq].desc ) == 0 )
         return IVCP_ID_Search_ObjectReq;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Search_PlateReq].desc ) == 0 )
         return IVCP_ID_Search_PlateReq;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Search_FaceReq].desc ) == 0 )
         return IVCP_ID_Search_FaceReq;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Search_LogReq].desc ) == 0 )
         return IVCP_ID_Search_LogReq;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Search_StopReq].desc ) == 0 )
         return IVCP_ID_Search_StopReq;
      break;
   case IVCP_TYPE_FACE: // 얼굴인식 데이터
      if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Data_FaceUserData].desc ) == 0 )
         return IVCP_ID_Data_FaceUserData;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Data_FaceImageData].desc ) == 0 )
         return IVCP_ID_Data_FaceImageData;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Data_FaceMatchData].desc ) == 0 )
         return IVCP_ID_Data_FaceMatchData;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Data_FaceSearchData].desc ) == 0 )
         return IVCP_ID_Data_FaceSearchData;
      break;
   case IVCP_TYPE_MMS: // 통합관리서버 (MMS)
      if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_MMS_LoginRes].desc ) == 0 )
         return IVCP_ID_MMS_LoginRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_MMS_UserAccount_Notify].desc ) == 0 )
         return IVCP_ID_MMS_UserAccount_Notify;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_MMS_SystemStatusInfo_Req].desc ) == 0 )
         return IVCP_ID_MMS_SystemStatusInfo_Req;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_MMS_SystemStatusInfo_Res].desc ) == 0 )
         return IVCP_ID_MMS_SystemStatusInfo_Res;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_MMS_NVRServer_ConnectInfo_Req].desc ) == 0 )
         return IVCP_ID_MMS_NVRServer_ConnectInfo_Req;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_MMS_NVRServer_ConfigFile_Transfer_Req].desc ) == 0 )
         return IVCP_ID_MMS_NVRServer_ConfigFile_Transfer_Req;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_MMS_NVRServer_ConfigFile_Transfer_Res].desc ) == 0 )
         return IVCP_ID_MMS_NVRServer_ConfigFile_Transfer_Res;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_MMS_NVRServer_GracefulClose_Notify].desc ) == 0 )
         return IVCP_ID_MMS_NVRServer_GracefulClose_Notify;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_MMS_NVRServer_FailOver_Notify].desc ) == 0 )
         return IVCP_ID_MMS_NVRServer_FailOver_Notify;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_MMS_KeepAlive_Notify].desc ) == 0 )
         return IVCP_ID_MMS_KeepAlive_Notify;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_VRS_Connect_Information].desc ) == 0 )
         return IVCP_ID_VRS_Connect_Information;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_VRS_Channel_Channge_nty].desc ) == 0 )
         return IVCP_ID_VRS_Channel_Channge_nty;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_HOTLIST_INFO].desc ) == 0 )
         return IVCP_HOTLIST_INFO;
      break;

   case IVCP_TYPE_IPEX: // [jmlee 2015 02 04] 비상벨 IPEX 연동 추가
      if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Event_IPEX_Req].desc ) == 0 )
         return IVCP_ID_Event_IPEX_Req;
      break;

   case IVCP_TYPE_AC: // AC 연동 추가
      if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Event_AC].desc ) == 0 )
         return IVCP_ID_Event_AC;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_DoorInfo_AC].desc ) == 0 )
         return IVCP_ID_DoorInfo_AC;
      break;
   }
   return ( IVCP_ID_Unknown );
}

int IvcpGetPacketIDforRes( const char* strMessage ) // Response 만 비교한다.
{
   int nPacketType = IvcpGetPacketType( strMessage );
   char* strString = (char*)strMessage + 2; // "C_STRING"
   switch( nPacketType ) {
   case IVCP_TYPE_CONN: // 연결
      if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Conn_OptionsRes].desc ) == 0 )
         return IVCP_ID_Conn_OptionsRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Conn_LoginRes].desc ) == 0 )
         return IVCP_ID_Conn_LoginRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Conn_LogoutRes].desc ) == 0 )
         return IVCP_ID_Conn_LogoutRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Conn_CameraListRes].desc ) == 0 )
         return IVCP_ID_Conn_CameraListRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Conn_StartUpRes].desc ) == 0 )
         return IVCP_ID_Conn_StartUpRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Conn_SnapshotRes].desc ) == 0 )
         return IVCP_ID_Conn_SnapshotRes;
      break;
   case IVCP_TYPE_SYSTEM: // 시스템
      if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_System_SettingRes].desc ) == 0 )
         return IVCP_ID_System_SettingRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_System_CommandRes].desc ) == 0 )
         return IVCP_ID_System_CommandRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_System_MonitorRes].desc ) == 0 )
         return IVCP_ID_System_MonitorRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_System_MonitorInfo].desc ) == 0 )
         return IVCP_ID_System_MonitorInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_System_AudioInOutRes].desc ) == 0 )
         return IVCP_ID_System_AudioInOutRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_System_RecvAudioData].desc ) == 0 )
         return IVCP_ID_System_RecvAudioData;
      break;
   case IVCP_TYPE_CAMERA: // 카메라
      if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Camera_LiveMetadataRes].desc ) == 0 )
         return IVCP_ID_Camera_LiveMetadataRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Camera_LiveVideoRes].desc ) == 0 )
         return IVCP_ID_Camera_LiveVideoRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Camera_PlayStreamStartRes].desc ) == 0 )
         return IVCP_ID_Camera_PlayStreamStartRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Camera_PlayStreamStopRes].desc ) == 0 )
         return IVCP_ID_Camera_PlayStreamStopRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Camera_PlayStreamControlRes].desc ) == 0 )
         return IVCP_ID_Camera_PlayStreamControlRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Camera_SnapshotRes].desc ) == 0 )
         return IVCP_ID_Camera_SnapshotRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Camera_SettingRes].desc ) == 0 )
         return IVCP_ID_Camera_SettingRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Camera_CommandRes].desc ) == 0 )
         return IVCP_ID_Camera_CommandRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Camera_TalkAudioRes].desc ) == 0 )
         return IVCP_ID_Camera_TalkAudioRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Camera_TalkStatus].desc ) == 0 )
         return IVCP_ID_Camera_TalkStatus;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Camera_RecvAudioData].desc ) == 0 )
         return IVCP_ID_Camera_RecvAudioData;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Camera_ExportStartRes].desc ) == 0 )
         return IVCP_ID_Camera_ExportStartRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Camera_ExportStopRes].desc ) == 0 )
         return IVCP_ID_Camera_ExportStopRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Camera_PlayDataStreamRes].desc ) == 0 )
         return IVCP_ID_Camera_PlayDataStreamRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Camera_PlayDataCtrlRes].desc ) == 0 )
         return IVCP_ID_Camera_PlayDataCtrlRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Camera_CarPlateRecognitionRes].desc ) == 0 )
         return IVCP_ID_Camera_CarPlateRecognitionRes;
      break;
   case IVCP_TYPE_PTZ: // PTZ 제어
      if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Ptz_ContMoveRes].desc ) == 0 )
         return IVCP_ID_Ptz_ContMoveRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Ptz_AbsMoveRes].desc ) == 0 )
         return IVCP_ID_Ptz_AbsMoveRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Ptz_BoxMoveRes].desc ) == 0 )
         return IVCP_ID_Ptz_BoxMoveRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Ptz_GetAbsPosRes].desc ) == 0 )
         return IVCP_ID_Ptz_GetAbsPosRes;
      break;
   case IVCP_TYPE_LIVE: // 실시간 데이터
      if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Live_FrameInfo].desc ) == 0 )
         return IVCP_ID_Live_FrameInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Live_FrmObjInfo].desc ) == 0 )
         return IVCP_ID_Live_FrmObjInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Live_EventInfo].desc ) == 0 )
         return IVCP_ID_Live_EventInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Live_ObjectInfo].desc ) == 0 )
         return IVCP_ID_Live_ObjectInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Live_PlateInfo].desc ) == 0 )
         return IVCP_ID_Live_PlateInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Live_FaceInfo].desc ) == 0 )
         return IVCP_ID_Live_FaceInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Live_PtzDataInfo].desc ) == 0 )
         return IVCP_ID_Live_PtzDataInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Live_PresetIdInfo].desc ) == 0 )
         return IVCP_ID_Live_PresetIdInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Live_EventZoneInfo].desc ) == 0 )
         return IVCP_ID_Live_EventZoneInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Live_EventZoneCount].desc ) == 0 )
         return IVCP_ID_Live_EventZoneCount;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Live_PresetMapInfo].desc ) == 0 )
         return IVCP_ID_Live_PresetMapInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Live_SettingInfo].desc ) == 0 )
         return IVCP_ID_Live_SettingInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Live_VideoData].desc ) == 0 )
         return IVCP_ID_Live_VideoData;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Live_AudioData].desc ) == 0 )
         return IVCP_ID_Live_AudioData;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Live_Status].desc ) == 0 )
         return IVCP_ID_Live_Status;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Live_ACInfo].desc ) == 0 )
         return IVCP_ID_Live_ACInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Live_HeatMap].desc ) == 0 )
         return IVCP_ID_Live_HeatMap;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Live_StatisticsInfo].desc ) == 0 )
         return IVCP_ID_Live_StatisticsInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_EVENT_WINDDLG_STATUS].desc ) == 0 )
         return IVCP_EVENT_WINDDLG_STATUS;
      break;
   case IVCP_TYPE_PLAY: // 재생 데이터
      if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Play_FrameInfo].desc ) == 0 )
         return IVCP_ID_Play_FrameInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Play_EventInfo].desc ) == 0 )
         return IVCP_ID_Play_EventInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Play_ObjectInfo].desc ) == 0 )
         return IVCP_ID_Play_ObjectInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Play_PlateInfo].desc ) == 0 )
         return IVCP_ID_Play_PlateInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Play_FaceInfo].desc ) == 0 )
         return IVCP_ID_Play_FaceInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Play_PtzDataInfo].desc ) == 0 )
         return IVCP_ID_Play_PtzDataInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Play_PresetIdInfo].desc ) == 0 )
         return IVCP_ID_Play_PresetIdInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Play_EventZoneInfo].desc ) == 0 )
         return IVCP_ID_Play_EventZoneInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Play_EventZoneCount].desc ) == 0 )
         return IVCP_ID_Play_EventZoneCount;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Play_VideoData].desc ) == 0 )
         return IVCP_ID_Play_VideoData;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Play_AudioData].desc ) == 0 )
         return IVCP_ID_Play_AudioData;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Play_Progress].desc ) == 0 )
         return IVCP_ID_Play_Progress;
      break;
   case IVCP_TYPE_SEARCH: // 검색 관련
      if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Search_Progress].desc ) == 0 )
         return IVCP_ID_Search_Progress;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Search_FrameInfo].desc ) == 0 )
         return IVCP_ID_Search_FrameInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Search_EventInfo].desc ) == 0 )
         return IVCP_ID_Search_EventInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Search_ObjectInfo].desc ) == 0 )
         return IVCP_ID_Search_ObjectInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Search_PlateInfo].desc ) == 0 )
         return IVCP_ID_Search_PlateInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Search_FaceInfo].desc ) == 0 )
         return IVCP_ID_Search_FaceInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Search_LogInfo].desc ) == 0 )
         return IVCP_ID_Search_LogInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Search_FrameRes].desc ) == 0 )
         return IVCP_ID_Search_FrameRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Search_EventRes].desc ) == 0 )
         return IVCP_ID_Search_EventRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Search_ObjectRes].desc ) == 0 )
         return IVCP_ID_Search_ObjectRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Search_PlateRes].desc ) == 0 )
         return IVCP_ID_Search_PlateRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Search_FaceRes].desc ) == 0 )
         return IVCP_ID_Search_FaceRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Search_LogRes].desc ) == 0 )
         return IVCP_ID_Search_LogRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Search_StopRes].desc ) == 0 )
         return IVCP_ID_Search_StopRes;
      break;
   case IVCP_TYPE_EXPORT: // 내보내기 관련
      if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Export_FrameInfo].desc ) == 0 )
         return IVCP_ID_Export_FrameInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Export_EventInfo].desc ) == 0 )
         return IVCP_ID_Export_EventInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Export_ObjectInfo].desc ) == 0 )
         return IVCP_ID_Export_ObjectInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Export_PlateInfo].desc ) == 0 )
         return IVCP_ID_Export_PlateInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Export_FaceInfo].desc ) == 0 )
         return IVCP_ID_Export_FaceInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Export_PtzDataInfo].desc ) == 0 )
         return IVCP_ID_Export_PtzDataInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Export_PresetIdInfo].desc ) == 0 )
         return IVCP_ID_Export_PresetIdInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Export_EventZoneInfo].desc ) == 0 )
         return IVCP_ID_Export_EventZoneInfo;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Export_EventZoneCount].desc ) == 0 )
         return IVCP_ID_Export_EventZoneCount;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Export_VideoData].desc ) == 0 )
         return IVCP_ID_Export_VideoData;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Export_AudioData].desc ) == 0 )
         return IVCP_ID_Export_AudioData;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Export_Progress].desc ) == 0 )
         return IVCP_ID_Export_Progress;
      break;
   case IVCP_TYPE_FACE: // 얼굴인식 데이터
      if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Data_FaceUserData].desc ) == 0 )
         return IVCP_ID_Data_FaceUserData;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Data_FaceImageData].desc ) == 0 )
         return IVCP_ID_Data_FaceImageData;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Data_FaceMatchData].desc ) == 0 )
         return IVCP_ID_Data_FaceMatchData;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Data_FaceSearchData].desc ) == 0 )
         return IVCP_ID_Data_FaceSearchData;
      break;
   case IVCP_TYPE_MMS: // 통합관리서버 (MMS)
      if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_MMS_LoginRes].desc ) == 0 )
         return IVCP_ID_MMS_LoginRes;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_MMS_UserAccount_Notify].desc ) == 0 )
         return IVCP_ID_MMS_UserAccount_Notify;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_MMS_SystemStatusInfo_Req].desc ) == 0 )
         return IVCP_ID_MMS_SystemStatusInfo_Req;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_MMS_SystemStatusInfo_Res].desc ) == 0 )
         return IVCP_ID_MMS_SystemStatusInfo_Res;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_MMS_NVRServer_ConnectInfo_Req].desc ) == 0 )
         return IVCP_ID_MMS_NVRServer_ConnectInfo_Req;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_MMS_NVRServer_ConnectInfo_Res].desc ) == 0 )
         return IVCP_ID_MMS_NVRServer_ConnectInfo_Res;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_MMS_NVRServer_ConfigFile_Transfer_Res].desc ) == 0 )
         return IVCP_ID_MMS_NVRServer_ConfigFile_Transfer_Res;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_MMS_NVRServer_GracefulClose_Notify].desc ) == 0 )
         return IVCP_ID_MMS_NVRServer_GracefulClose_Notify;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_MMS_NVRServer_FailOver_Notify].desc ) == 0 )
         return IVCP_ID_MMS_NVRServer_FailOver_Notify;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_MMS_KeepAlive_Notify].desc ) == 0 )
         return IVCP_ID_MMS_KeepAlive_Notify;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_VRS_Connect_Information].desc ) == 0 )
         return IVCP_ID_VRS_Connect_Information;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_VRS_Channel_Channge_nty].desc ) == 0 )
         return IVCP_ID_VRS_Channel_Channge_nty;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_HOTLIST_INFO].desc ) == 0 )
         return IVCP_HOTLIST_INFO;
      break;
   case IVCP_TYPE_IPEX: // [jmlee 2015 02 04] 비상벨 IPEX 연동 추가
      if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Event_IPEX_Res].desc ) == 0 )
         return IVCP_ID_Event_IPEX_Res;
      break;

   case IVCP_TYPE_AC: // [jmlee] AC 출입 연동 추가
      if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_Event_AC].desc ) == 0 )
         return IVCP_ID_Event_AC;
      else if( strcmp( strString, s_IvcpPacktIDString[IVCP_ID_DoorInfo_AC].desc ) == 0 )
         return IVCP_ID_DoorInfo_AC;
      break;
   }
   return ( IVCP_ID_Unknown );
}

void ivcpGetFileTimeFromString( FILETIME& ftTime, const char* strTime )
{
   int nLen = strlen( strTime );
   if( nLen < 17 ) return;

   //                      01234567890123456
   // Time String Format : 20050105101010123
   char szYear[8];
   char szMonth[4];
   char szDay[4];
   char szHour[4];
   char szMin[4];
   char szSec[4];
   char szMSec[4];
   strncpy( szYear, strTime, 4 );
   szYear[4] = 0;
   strncpy( szMonth, strTime + 4, 2 );
   szMonth[2] = 0;
   strncpy( szDay, strTime + 6, 2 );
   szDay[2] = 0;
   strncpy( szHour, strTime + 8, 2 );
   szHour[2] = 0;
   strncpy( szMin, strTime + 10, 2 );
   szMin[2] = 0;
   strncpy( szSec, strTime + 12, 2 );
   szSec[2] = 0;
   strncpy( szMSec, strTime + 14, 3 );
   szMSec[3] = 0;

#ifdef __WIN32
   SYSTEMTIME st;
   st.wYear         = atoi( szYear );
   st.wMonth        = atoi( szMonth );
   st.wDay          = atoi( szDay );
   st.wHour         = atoi( szHour );
   st.wMinute       = atoi( szMin );
   st.wSecond       = atoi( szSec );
   st.wMilliseconds = atoi( szMSec );

   SystemTimeToFileTime( &st, &ftTime );
#else
   int nYear         = atoi( szYear );
   int nMonth        = atoi( szMonth );
   int nDay          = atoi( szDay );
   int nHour         = atoi( szHour );
   int nMinute       = atoi( szMin );
   int nSecond       = atoi( szSec );
   int nMilliseconds = atoi( szMSec );

   time_t rawtime;
   struct tm* timeinfo;
   time( &rawtime );
   timeinfo          = localtime( &rawtime );
   timeinfo->tm_year = nYear - 1900;
   timeinfo->tm_mon  = nMonth - 1;
   timeinfo->tm_mday = nDay;
   timeinfo->tm_hour = nHour;
   timeinfo->tm_min  = nMinute;
   timeinfo->tm_sec  = nSecond;

   ftTime.dwHighDateTime = (unsigned long)mktime( timeinfo );
   ftTime.dwLowDateTime  = ( nMilliseconds * 1000 );
#endif
}

void ivcpGetStringFromFileTime( std::string& strTime, FILETIME& ftTime )
{
#ifdef __WIN32
   SYSTEMTIME st;
   FileTimeToSystemTime( &ftTime, &st );
   strTime = sformat( "%04d%02d%02d_%02d%02d%02d_%03d", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds );
#else
   struct tm* ptm;
   time_t rawtime = ftTime.dwHighDateTime;
   DWORD nMSec    = ( ftTime.dwLowDateTime / 1000 ); //000);
   //ptm = gmtime (&rawtime); // UTC time
   ptm     = localtime( &rawtime ); // LocalTime
   strTime = sformat( "%04d%02d%02d_%02d%02d%02d_%03d", ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday, ptm->tm_hour, ptm->tm_min, ptm->tm_sec, nMSec );
#endif
}

// TYLEE: 2015-04-29
// {{ GUID - Begin
void ivcpGetGUIDFromString( char* pstring, char* pGUID )
{
   if( pGUID ) {
      if( pGUID ) {
         strcpy( pGUID, pstring );
      }
   }
}

void ivcpGetGUIDFromString( char* pstring, std::string* pGUID )
{
   if( pGUID ) {
      ivcpGetGUIDFromString( pstring, pGUID );
   }
}

int ivcpSetGUIDToString( const std::string& szGUID, char* pstring )
{
   int nLength = 0;
   if( pstring ) {
      nLength = GUID_LENGTH;
      sprintf( pstring, "%s", szGUID.c_str() );
   }

   return nLength;
}
// }} GUID - End

///////////////////////////////////////////////////////////////////////////////
//
// Class: CIVCPPacketExt
//
///////////////////////////////////////////////////////////////////////////////

int CIVCPPacketExt::ReadPacketBody( FileIO& buff )
{
   CXMLIO xmlIO( &buff, XMLIO_Read );
   if( DONE != ReadXML( &xmlIO ) ) return ( 1 );
   return ( DONE );
}

int CIVCPPacketExt::WritePacketBody( FileIO& buff )
{
   CXMLIO xmlIO( &buff, XMLIO_Write );
   if( DONE != WriteXML( &xmlIO ) ) return ( 1 );
   return ( DONE );
}

int CIVCPPacketExt::ReadFromPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      FileIO buffBody;
      buffBody.Attach( (byte*)aPacket.m_pBody, aPacket.m_Header.m_nBodyLen );
      if( DONE != ReadPacketBody( buffBody ) ) {
         buffBody.Detach();
         return ( 2 );
      }
      buffBody.Detach();
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPPacketExt::WriteToPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      aPacket.FreeBody();
   }
   FileIO buffBody;
   if( DONE == WritePacketBody( buffBody ) ) {
      aPacket.m_Header.m_nBodyLen = buffBody.GetLength();
      aPacket.m_pBody             = (char*)buffBody.Detach();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnOptionsReq : 서버 기본 정보
//
///////////////////////////////////////////////////////////////////////////////

CIVCPConnOptionsReq::CIVCPConnOptionsReq()
{
   m_nPacketID   = IVCP_ID_Conn_OptionsReq;
   m_fNetworkVer = 3.0;
}
int CIVCPConnOptionsReq::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Options" ) ) {
      xmlNode.Attribute( TYPE_ID( float ), "NetVer", &m_fNetworkVer );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPConnOptionsReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Options" ) ) {
      xmlNode.Attribute( TYPE_ID( float ), "NetVer", &m_fNetworkVer );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnOptionsRes : 서버 기본 정보
//
///////////////////////////////////////////////////////////////////////////////

CIVCPConnOptionsRes::CIVCPConnOptionsRes()
{
   m_nPacketID   = IVCP_ID_Conn_OptionsRes;
   m_nDeviceVer  = 0;
   m_fNetworkVer = 3.0;
   m_nCameraNum  = 0;
}
int CIVCPConnOptionsRes::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Options" ) ) {
      //TRACE(_T("[CIVCPConnOptionsRes::ReadXML] <1> m_strSystemGUID[%s]\n"), m_strSystemGUID);
      xmlNode.Attribute( TYPE_ID( char ), "DevName", m_strDeviceName, sizeof( m_strDeviceName ) );
      xmlNode.Attribute( TYPE_ID( int ), "DevVer", &m_nDeviceVer );
      xmlNode.Attribute( TYPE_ID( float ), "NetVer", &m_fNetworkVer );
      xmlNode.Attribute( TYPE_ID( char ), "SysName", m_strSystemName, sizeof( m_strSystemName ) );
      xmlNode.Attribute( TYPE_ID( int ), "ChNum", &m_nCameraNum );
      xmlNode.Attribute( TYPE_ID( int ), "SystemUID", &m_nSystemUID );
      xmlNode.Attribute( TYPE_ID( char ), "SystemGUID", m_strSystemGUID, sizeof( m_strSystemGUID ) );
      //TRACE(_T("[CIVCPConnOptionsRes::ReadXML] <2> m_strSystemGUID[%s]\n"), m_strSystemGUID);
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPConnOptionsRes::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Options" ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "DevName", m_strDeviceName, sizeof( m_strDeviceName ) );
      xmlNode.Attribute( TYPE_ID( int ), "DevVer", &m_nDeviceVer );
      xmlNode.Attribute( TYPE_ID( float ), "NetVer", &m_fNetworkVer );
      xmlNode.Attribute( TYPE_ID( char ), "SysName", m_strSystemName, sizeof( m_strSystemName ) );
      xmlNode.Attribute( TYPE_ID( int ), "ChNum", &m_nCameraNum );
      xmlNode.Attribute( TYPE_ID( int ), "SystemUID", &m_nSystemUID );
      xmlNode.Attribute( TYPE_ID( char ), "SystemGUID", m_strSystemGUID, sizeof( m_strSystemGUID ) );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnLoginReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPConnLoginReq::CIVCPConnLoginReq()
{
   m_nPacketID  = IVCP_ID_Conn_LoginReq;
   m_nLoginType = 0;
}
int CIVCPConnLoginReq::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Login" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Type", &m_nLoginType );
      xmlNode.Attribute( TYPE_ID( char ), "ID", &m_strUserID, sizeof( m_strUserID ) );
      xmlNode.Attribute( TYPE_ID( char ), "PW", &m_strPasswd, sizeof( m_strPasswd ) );
      xmlNode.Attribute( TYPE_ID( char ), "DevName", &m_strDeviceName, sizeof( m_strDeviceName ) );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPConnLoginReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Login" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Type", &m_nLoginType );
      xmlNode.Attribute( TYPE_ID( char ), "ID", &m_strUserID, sizeof( m_strUserID ) );
      xmlNode.Attribute( TYPE_ID( char ), "PW", &m_strPasswd, sizeof( m_strPasswd ) );
      xmlNode.Attribute( TYPE_ID( char ), "DevName", &m_strDeviceName, sizeof( m_strDeviceName ) );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnLoginRes
//
///////////////////////////////////////////////////////////////////////////////

CIVCPConnLoginRes::CIVCPConnLoginRes()
{
   m_nPacketID    = IVCP_ID_Conn_LoginRes;
   m_nLoginResult = 0;
}
int CIVCPConnLoginRes::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Login" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Result", &m_nLoginResult );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPConnLoginRes::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Login" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Result", &m_nLoginResult );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnLogoutReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPConnLogoutReq::CIVCPConnLogoutReq()
{
   m_nPacketID   = IVCP_ID_Conn_LogoutReq;
   m_nLogoutType = 0;
}
int CIVCPConnLogoutReq::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Logout" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Type", &m_nLogoutType );
      xmlNode.End();
      return ( DONE );
   }

   return ( 1 );
}
int CIVCPConnLogoutReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Logout" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Type", &m_nLogoutType );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnLogoutRes
//
///////////////////////////////////////////////////////////////////////////////

CIVCPConnLogoutRes::CIVCPConnLogoutRes()
{
   m_nPacketID     = IVCP_ID_Conn_LogoutRes;
   m_nLogoutResult = 0;
}
int CIVCPConnLogoutRes::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Logout" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Result", &m_nLogoutResult );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPConnLogoutRes::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Logout" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Result", &m_nLogoutResult );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnCameraListReq : 카메라 목록 정보를 얻어온다.
//
///////////////////////////////////////////////////////////////////////////////

CIVCPConnCameraListReq::CIVCPConnCameraListReq()
{
   m_nPacketID = IVCP_ID_Conn_CameraListReq;
}
int CIVCPConnCameraListReq::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "CameraList" ) ) {
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPConnCameraListReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "CameraList" ) ) {
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// class CIVCPCameraInfo
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraInfo::CIVCPCameraInfo()
{
   m_nCameraUID       = -1;
   m_strGuid[0]       = '\0';
   m_nCameraNo        = 0;
   m_strCameraName[0] = '\0';
   m_nCameraType      = 0;
   m_nCameraParam     = 0;
   m_nWidth           = 0;
   m_nHeight          = 0;
   m_fFrameRate       = 0.0f;
   m_nVStreamNum      = 0;
   m_nAStreamNum      = 0;
   for( int i = 0; i < MAX_VSTREAMS; i++ ) {
      m_nVideoCodec[i]  = 0;
      m_nBitrate[i]     = 0;
      m_nVideoWidth[i]  = 0;
      m_nVideoHeight[i] = 0;
      m_fFrmRate[i]     = 0.0f;
   }
   for( int i = 0; i < MAX_ASTREAMS; i++ ) {
      m_nAudioCodec[i]    = 0;
      m_nChannels[i]      = 0;
      m_nSamplingRate[i]  = 0;
      m_nBitsPerSample[i] = 0;
   }
}
int CIVCPCameraInfo::ReadXML( CXMLIO* pIO )
{
   int i;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "CamInfo" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( char ), "Guid", &m_strGuid, sizeof( m_strGuid ) );
      xmlNode.Attribute( TYPE_ID( int ), "No", &m_nCameraNo );
      xmlNode.Attribute( TYPE_ID( char ), "Name", &m_strCameraName, sizeof( m_strCameraName ) );
      xmlNode.Attribute( TYPE_ID( int ), "Type", &m_nCameraType );
      xmlNode.Attribute( TYPE_ID( int ), "Param", &m_nCameraParam );
      xmlNode.Attribute( TYPE_ID( int ), "VW", &m_nWidth );
      xmlNode.Attribute( TYPE_ID( int ), "VH", &m_nHeight );
      xmlNode.Attribute( TYPE_ID( float ), "FR", &m_fFrameRate );
      xmlNode.Attribute( TYPE_ID( int ), "VNum", &m_nVStreamNum );
      xmlNode.Attribute( TYPE_ID( int ), "ANum", &m_nAStreamNum );

      std::string strStreamIndex;
      std::string strStreamDesc;
      if( m_nVStreamNum > MAX_VSTREAMS ) m_nVStreamNum = MAX_VSTREAMS; // 제한
      for( i = 0; i < m_nVStreamNum; i++ ) {
         strStreamIndex = sformat( "VDesc%d", i + 1 );
         xmlNode.Attribute( TYPE_ID( string ), strStreamIndex.c_str(), &strStreamDesc );
         ParseVideoStreamDesc( i, strStreamDesc );
      }
      if( m_nAStreamNum > MAX_ASTREAMS ) m_nAStreamNum = MAX_ASTREAMS; // 제한
      for( i = 0; i < m_nAStreamNum; i++ ) {
         strStreamIndex = sformat( "ADesc%d", i + 1 );
         xmlNode.Attribute( TYPE_ID( string ), strStreamIndex.c_str(), &strStreamDesc );
         ParseAudioStreamDesc( i, strStreamDesc );
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPCameraInfo::WriteXML( CXMLIO* pIO )
{
   int i;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "CamInfo" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( char ), "Guid", &m_strGuid, sizeof( m_strGuid ) );
      xmlNode.Attribute( TYPE_ID( int ), "No", &m_nCameraNo );
      xmlNode.Attribute( TYPE_ID( char ), "Name", &m_strCameraName, sizeof( m_strCameraName ) );
      xmlNode.Attribute( TYPE_ID( int ), "Type", &m_nCameraType );
      xmlNode.Attribute( TYPE_ID( int ), "Param", &m_nCameraParam );
      xmlNode.Attribute( TYPE_ID( int ), "VW", &m_nWidth );
      xmlNode.Attribute( TYPE_ID( int ), "VH", &m_nHeight );
      xmlNode.Attribute( TYPE_ID( float ), "FR", &m_fFrameRate );
      xmlNode.Attribute( TYPE_ID( int ), "VNum", &m_nVStreamNum );
      xmlNode.Attribute( TYPE_ID( int ), "ANum", &m_nAStreamNum );

      std::string strStreamIndex;
      std::string strStreamDesc;
      for( i = 0; i < m_nVStreamNum; i++ ) {
         MakeVideoStreamDesc( i, strStreamDesc );
         strStreamIndex = sformat( "VDesc%d", i + 1 );
         xmlNode.Attribute( TYPE_ID( string ), strStreamIndex.c_str(), &strStreamDesc );
      }
      for( i = 0; i < m_nAStreamNum; i++ ) {
         MakeAudioStreamDesc( i, strStreamDesc );
         strStreamIndex = sformat( "ADesc%d", i + 1 );
         xmlNode.Attribute( TYPE_ID( string ), strStreamIndex.c_str(), &strStreamDesc );
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
void CIVCPCameraInfo::ParseVideoStreamDesc( int nIdx, const std::string& strDesc )
{
   char resString[200];
   strcpy( resString, strDesc.c_str() );
   int nCount = 0;
   int i      = 0;
   int nLen   = strlen( resString );
   while( ( i < nLen ) && ( nCount < 6 ) ) {
      while( ( i < nLen ) && ( resString[i] == ' ' ) )
         i++;
      switch( nCount ) {
      case 0: // Codec
         m_nVideoCodec[nIdx] = atoi( &resString[i] );
         break;
      case 1: // Bitrate
         m_nBitrate[nIdx] = atoi( &resString[i] );
         break;
      case 2: // Width
         m_nVideoWidth[nIdx] = atoi( &resString[i] );
         break;
      case 3: // Height
         m_nVideoHeight[nIdx] = atoi( &resString[i] );
         break;
      case 4: // FrameRate
         m_fFrmRate[nIdx] = (float)atof( &resString[i] );
         break;
      case 5: // SendType
         break;
      }
      nCount++;
      while( ( i < nLen ) && ( resString[i] != ' ' ) )
         i++;
      if( resString[i] != ' ' )
         break;
   }
}
void CIVCPCameraInfo::ParseAudioStreamDesc( int nIdx, const std::string& strDesc )
{
   char resString[200];
   strcpy( resString, strDesc.c_str() );
   int nCount = 0;
   int i      = 0;
   int nLen   = strlen( resString );
   while( ( i < nLen ) && ( nCount < 4 ) ) {
      while( ( i < nLen ) && ( resString[i] == ' ' ) )
         i++;
      switch( nCount ) {
      case 0: // Codec
         m_nAudioCodec[nIdx] = atoi( &resString[i] );
         break;
      case 1: // Channel
         m_nChannels[nIdx] = atoi( &resString[i] );
         break;
      case 2: // SamplingRate
         m_nSamplingRate[nIdx] = atoi( &resString[i] );
         break;
      case 3: // BitsPerSample
         m_nBitsPerSample[nIdx] = atoi( &resString[i] );
         break;
      }
      nCount++;
      while( ( i < nLen ) && ( resString[i] != ' ' ) )
         i++;
      if( resString[i] != ' ' )
         break;
   }
}
void CIVCPCameraInfo::MakeVideoStreamDesc( int nIdx, std::string& strDesc )
{
   // "0 50 320 240 7.0 0.5" (Codec Bitrate Width Height FrameRate ReductionFactor)
   int nSendType = 0;
   strDesc       = sformat( "%d %d %d %d %f %d", m_nVideoCodec[nIdx], m_nBitrate[nIdx], m_nVideoWidth[nIdx], m_nVideoHeight[nIdx], m_fFrmRate[nIdx], nSendType );
}
void CIVCPCameraInfo::MakeAudioStreamDesc( int nIdx, std::string& strDesc )
{
   // "0 2 44100 16" (Codec Channel SamplingRate BitsPerSample)
   strDesc = sformat( "%d %d %d %d", m_nAudioCodec[nIdx], m_nChannels[nIdx], m_nSamplingRate[nIdx], m_nBitsPerSample[nIdx] );
}

void CIVCPCameraInfo::GetCameraInfo( IVCPCameraInfo& CameraInfo )
{
   CameraInfo.m_nCameraUID = m_nCameraUID;
   strcpy( CameraInfo.m_strGuid, m_strGuid );
   CameraInfo.m_nCameraNo = m_nCameraNo;
   strcpy( CameraInfo.m_strCameraName, m_strCameraName );
   CameraInfo.m_nCameraType  = m_nCameraType;
   CameraInfo.m_nCameraParam = m_nCameraParam;
   CameraInfo.m_nWidth       = m_nWidth;
   CameraInfo.m_nHeight      = m_nHeight;
   CameraInfo.m_fFrameRate   = m_fFrameRate;
   CameraInfo.m_nVStreamNum  = m_nVStreamNum;
   CameraInfo.m_nAStreamNum  = m_nAStreamNum;
   memcpy( CameraInfo.m_nVideoCodec, m_nVideoCodec, sizeof( m_nVideoCodec ) );
   memcpy( CameraInfo.m_nBitrate, m_nBitrate, sizeof( m_nBitrate ) );
   memcpy( CameraInfo.m_nVideoWidth, m_nVideoWidth, sizeof( m_nVideoWidth ) );
   memcpy( CameraInfo.m_nVideoHeight, m_nVideoHeight, sizeof( m_nVideoHeight ) );
   memcpy( CameraInfo.m_fFrmRate, m_fFrmRate, sizeof( m_fFrmRate ) );
   memcpy( CameraInfo.m_nAudioCodec, m_nAudioCodec, sizeof( m_nAudioCodec ) );
   memcpy( CameraInfo.m_nChannels, m_nChannels, sizeof( m_nChannels ) );
   memcpy( CameraInfo.m_nSamplingRate, m_nSamplingRate, sizeof( m_nSamplingRate ) );
   memcpy( CameraInfo.m_nBitsPerSample, m_nBitsPerSample, sizeof( m_nBitsPerSample ) );
}

void CIVCPCameraInfo::SetCameraInfo( IVCPCameraInfo* pCameraInfo )
{
   m_nCameraUID = pCameraInfo->m_nCameraUID;
   strcpy( m_strGuid, pCameraInfo->m_strGuid );
   m_nCameraNo = pCameraInfo->m_nCameraNo;
   strcpy( m_strCameraName, pCameraInfo->m_strCameraName );
   m_nCameraType  = pCameraInfo->m_nCameraType;
   m_nCameraParam = pCameraInfo->m_nCameraParam;
   m_nWidth       = pCameraInfo->m_nWidth;
   m_nHeight      = pCameraInfo->m_nHeight;
   m_fFrameRate   = pCameraInfo->m_fFrameRate;
   m_nVStreamNum  = pCameraInfo->m_nVStreamNum;
   m_nAStreamNum  = pCameraInfo->m_nAStreamNum;
   memcpy( m_nVideoCodec, pCameraInfo->m_nVideoCodec, sizeof( m_nVideoCodec ) );
   memcpy( m_nBitrate, pCameraInfo->m_nBitrate, sizeof( m_nBitrate ) );
   memcpy( m_nVideoWidth, pCameraInfo->m_nVideoWidth, sizeof( m_nVideoWidth ) );
   memcpy( m_nVideoHeight, pCameraInfo->m_nVideoHeight, sizeof( m_nVideoHeight ) );
   memcpy( m_fFrmRate, pCameraInfo->m_fFrmRate, sizeof( m_fFrmRate ) );
   memcpy( m_nAudioCodec, pCameraInfo->m_nAudioCodec, sizeof( m_nAudioCodec ) );
   memcpy( m_nChannels, pCameraInfo->m_nChannels, sizeof( m_nChannels ) );
   memcpy( m_nSamplingRate, pCameraInfo->m_nSamplingRate, sizeof( m_nSamplingRate ) );
   memcpy( m_nBitsPerSample, pCameraInfo->m_nBitsPerSample, sizeof( m_nBitsPerSample ) );
}

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnSystemInfoResponse
//
///////////////////////////////////////////////////////////////////////////////

CIVCPConnCameraListRes::CIVCPConnCameraListRes()
{
   m_nPacketID = IVCP_ID_Conn_CameraListRes;
   m_nStart    = 0; // 읽어온 카메라 인덱스 (0: 전부)
   m_nCount    = 0; // 읽어온 카메라 개수
}
CIVCPConnCameraListRes::~CIVCPConnCameraListRes()
{
   ClearCameras();
}
int CIVCPConnCameraListRes::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "CameraList" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Start", &m_nStart );
      xmlNode.Attribute( TYPE_ID( int ), "Count", &m_nCount );
      ClearCameras();
      for( int i = 0; i < m_nCount; i++ ) {
         CIVCPCameraInfo* pCameraInfo = new CIVCPCameraInfo();
         pCameraInfo->ReadXML( pIO );
         m_CameraList.push_back( pCameraInfo );
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPConnCameraListRes::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "CameraList" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Start", &m_nStart );
      xmlNode.Attribute( TYPE_ID( int ), "Count", &m_nCount );
      for( int i = 0; i < m_nCount; i++ ) {
         m_CameraList[i]->WriteXML( pIO );
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
void CIVCPConnCameraListRes::ClearCameras()
{
   int nCameraNum = m_CameraList.size();
   for( int i = 0; i < nCameraNum; i++ ) {
      CIVCPCameraInfo* pCameraInfo = m_CameraList[i];
      if( pCameraInfo ) delete pCameraInfo;
   }
   m_CameraList.clear();
}

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnStartUpReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPConnStartUpReq::CIVCPConnStartUpReq()
{
   m_nPacketID   = IVCP_ID_Conn_StartUpReq;
   m_nStreamFlag = 0;
   m_nMetaFlag   = 0;
   m_nOptions    = 0;
}
int CIVCPConnStartUpReq::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "StartUp" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "StreamFlag", &m_nStreamFlag );
      xmlNode.Attribute( TYPE_ID( int ), "MetaFlag", &m_nMetaFlag );
      xmlNode.Attribute( TYPE_ID( int ), "Options", &m_nOptions );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPConnStartUpReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "StartUp" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "StreamFlag", &m_nStreamFlag );
      xmlNode.Attribute( TYPE_ID( int ), "MetaFlag", &m_nMetaFlag );
      xmlNode.Attribute( TYPE_ID( int ), "Options", &m_nOptions );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnStartUpRes
//
///////////////////////////////////////////////////////////////////////////////

CIVCPConnStartUpRes::CIVCPConnStartUpRes()
{
   m_nPacketID = IVCP_ID_Conn_StartUpRes;
   m_nResult   = 0;
}
int CIVCPConnStartUpRes::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "StartUp" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Result", &m_nResult );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPConnStartUpRes::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "StartUp" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Result", &m_nResult );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnShutDownReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPConnShutDownReq::CIVCPConnShutDownReq()
{
   m_nPacketID   = IVCP_ID_Conn_ShutDownReq;
   m_nStreamFlag = 0;
   m_nMetaFlag   = 0;
   m_nOptions    = 0;
}
int CIVCPConnShutDownReq::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "ShutDown" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "StreamFlag", &m_nStreamFlag );
      xmlNode.Attribute( TYPE_ID( int ), "MetaFlag", &m_nMetaFlag );
      xmlNode.Attribute( TYPE_ID( int ), "Options", &m_nOptions );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPConnShutDownReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "ShutDown" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "StreamFlag", &m_nStreamFlag );
      xmlNode.Attribute( TYPE_ID( int ), "MetaFlag", &m_nMetaFlag );
      xmlNode.Attribute( TYPE_ID( int ), "Options", &m_nOptions );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnShutDownRes
//
///////////////////////////////////////////////////////////////////////////////

CIVCPConnShutDownRes::CIVCPConnShutDownRes()
{
   m_nPacketID = IVCP_ID_Conn_ShutDownRes;
   m_nResult   = 0;
}
int CIVCPConnShutDownRes::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "ShutDown" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Result", &m_nResult );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPConnShutDownRes::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "ShutDown" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Result", &m_nResult );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnSnapshotReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPConnSnapshotReq::CIVCPConnSnapshotReq()
{
   m_nPacketID    = IVCP_ID_Conn_SnapshotReq;
   m_nChannelFlag = 0;
   m_nResizeFlag  = 0;
   m_nWidth       = 0;
   m_nHeight      = 0;
}
int CIVCPConnSnapshotReq::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Snapshot" ) ) {
      xmlNode.Attribute( TYPE_ID( UINT64 ), "ChFlag", &m_nChannelFlag );
      xmlNode.Attribute( TYPE_ID( int ), "ResizeFlag", &m_nResizeFlag );
      xmlNode.Attribute( TYPE_ID( int ), "Width", &m_nWidth );
      xmlNode.Attribute( TYPE_ID( int ), "Height", &m_nHeight );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPConnSnapshotReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Snapshot" ) ) {
      xmlNode.Attribute( TYPE_ID( UINT64 ), "ChFlag", &m_nChannelFlag );
      xmlNode.Attribute( TYPE_ID( int ), "ResizeFlag", &m_nResizeFlag );
      xmlNode.Attribute( TYPE_ID( int ), "Width", &m_nWidth );
      xmlNode.Attribute( TYPE_ID( int ), "Height", &m_nHeight );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnSnapshotRes
//
///////////////////////////////////////////////////////////////////////////////

CIVCPConnSnapshotRes::CIVCPConnSnapshotRes()
{
   m_nPacketID  = IVCP_ID_Conn_SnapshotRes;
   m_nCameraUID = -1;
   m_nCodecID   = 0;
   m_nWidth     = 0;
   m_nHeight    = 0;
}
CIVCPConnSnapshotRes::~CIVCPConnSnapshotRes()
{
   if( m_pBodyBuffer ) free( m_pBodyBuffer );
}
int CIVCPConnSnapshotRes::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Snapshot" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "CodecID", &m_nCodecID );
      xmlNode.Attribute( TYPE_ID( int ), "Width", &m_nWidth );
      xmlNode.Attribute( TYPE_ID( int ), "Height", &m_nHeight );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPConnSnapshotRes::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Snapshot" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "CodecID", &m_nCodecID );
      xmlNode.Attribute( TYPE_ID( int ), "Width", &m_nWidth );
      xmlNode.Attribute( TYPE_ID( int ), "Height", &m_nHeight );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
void CIVCPConnSnapshotRes::ParseBodyBuffer()
{
   FileIO buff;
   buff.Attach( (byte*)m_pBodyBuffer, m_nXMLDataLen );
   CXMLIO xmlIO( &buff, XMLIO_Read );
   if( DONE != ReadXML( &xmlIO ) ) {
      buff.Detach();
      return;
   }
}
void CIVCPConnSnapshotRes::CreateBodybuffer( byte* pEncodedBuff, int nEncodedLen )
{
   if( m_pBodyBuffer ) delete m_pBodyBuffer;
   m_pBodyBuffer = NULL;
   m_nXMLDataLen = 0;
   m_nBodyLen    = 0;

   FileIO buff;
   CXMLIO xmlIO( &buff, XMLIO_Write );
   if( WriteXML( &xmlIO ) ) return;

   m_nXMLDataLen = buff.GetLength();
   ;
   m_nBodyLen = m_nXMLDataLen + nEncodedLen;

   // 먼저 버퍼를 만든다.
   m_pBodyBuffer = (char*)malloc( m_nXMLDataLen + nEncodedLen );
   if( m_pBodyBuffer ) {
      memcpy( m_pBodyBuffer, (byte*)buff, m_nXMLDataLen );
      memcpy( m_pBodyBuffer + m_nXMLDataLen, pEncodedBuff, nEncodedLen );
   }
}
int CIVCPConnSnapshotRes::ReadFromPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      m_nCameraUID    = aPacket.m_Header.m_nOptions;
      m_nXMLDataLen   = aPacket.m_Header.m_nBinaryOptionalLen;
      m_nBodyLen      = aPacket.m_Header.m_nBodyLen;
      m_pBodyBuffer   = aPacket.m_pBody;
      aPacket.m_pBody = NULL;

      ParseBodyBuffer(); // 바로 파싱한다.
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPConnSnapshotRes::WriteToPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      aPacket.FreeBody();
   }
   if( m_pBodyBuffer ) {
      aPacket.m_Header.m_nOptions    = m_nCameraUID;
      aPacket.m_Header.m_nBinaryOptionalLen = m_nXMLDataLen;
      aPacket.m_Header.m_nBodyLen    = m_nBodyLen;
      aPacket.m_pBody                = m_pBodyBuffer;
      m_pBodyBuffer                  = NULL;
      return ( DONE );
   }
   return ( 1 );
}

void CIVCPConnSnapshotRes::DeleteBodyBuffer()
{
   SAFE_FREE( m_pBodyBuffer );
}

CIVCPConnCarPlateRecognitionReq::CIVCPConnCarPlateRecognitionReq()
{
   m_nPacketID   = IVCP_ID_Camera_CarPlateRecognitionReq;
   m_nResponseID = 0;
   ZeroMemory( m_strFileID, sizeof( char[MAX_PATH] ) );
   m_nFileType = 0;
   ZeroMemory( m_strFileName, sizeof( char[MAX_PATH] ) );
   m_nFileSize = 0;
}

int CIVCPConnCarPlateRecognitionReq::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "CarPlate" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "RequestID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( char ), "FileID", m_strFileID, sizeof( m_strFileID ) );
      xmlNode.Attribute( TYPE_ID( int ), "FileType", &m_nFileType );
      xmlNode.Attribute( TYPE_ID( char ), "FileName", m_strFileName, sizeof( m_strFileName ) );
      xmlNode.Attribute( TYPE_ID( INT64 ), "FileSize", &m_nFileSize );
      xmlNode.Attribute( TYPE_ID( char ), "URL", m_strURL, sizeof( m_strURL ) );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPConnCarPlateRecognitionReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "CarPlate" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "RequestID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( char ), "FileID", m_strFileID, sizeof( m_strFileID ) );
      xmlNode.Attribute( TYPE_ID( int ), "FileType", &m_nFileType );
      xmlNode.Attribute( TYPE_ID( char ), "FileName", m_strFileName, sizeof( m_strFileName ) );
      xmlNode.Attribute( TYPE_ID( INT64 ), "FileSize", &m_nFileSize );
      xmlNode.Attribute( TYPE_ID( char ), "URL", m_strURL, sizeof( m_strURL ) );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

#ifdef __NOT_USE__

CIVCPConnCarPlateRecognitionRes::CIVCPConnCarPlateRecognitionRes()
{
   m_nPacketID  = IVCP_ID_Camera_CarPlateRecognitionRes;
   m_nFileID    = 0;
   m_nStatus    = 0;
   m_nResultCnt = 0;
}

int CIVCPConnCarPlateRecognitionRes::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "CarPlate" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "FileID", &m_nFileID );
      xmlNode.Attribute( TYPE_ID( int ), "Status", &m_nStatus );
      xmlNode.Attribute( TYPE_ID( int ), "TotalFrameNum", &m_nResultCnt );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPConnCarPlateRecognitionRes::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "CarPlate" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "FileID", &m_nFileID );
      xmlNode.Attribute( TYPE_ID( int ), "Status", &m_nStatus );
      xmlNode.Attribute( TYPE_ID( int ), "TotalFrameNum", &m_nResultCnt );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPSystemResultRes
//
///////////////////////////////////////////////////////////////////////////////

CIVCPSystemResultRes::CIVCPSystemResultRes()
{
   m_nPacketID   = 0;
   m_nResponseID = 0;
   m_nResult     = 0;
}
int CIVCPSystemResultRes::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "SystemResult" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "ReqID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( int ), "Result", &m_nResult );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPSystemResultRes::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "SystemResult" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "ResID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( int ), "Result", &m_nResult );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraResultRes
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraResultRes::CIVCPCameraResultRes()
{
   m_nPacketID   = 0;
   m_nCameraUID  = -1;
   m_nResponseID = 0;
   m_nResult     = 0;
}
int CIVCPCameraResultRes::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "CameraResult" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "ResID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( int ), "Result", &m_nResult );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPCameraResultRes::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "CameraResult" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "ResID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( int ), "Result", &m_nResult );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraLiveMetadataReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraLiveMetadataReq::CIVCPCameraLiveMetadataReq()
{
   m_nPacketID   = IVCP_ID_Camera_LiveMetadataReq;
   m_nCameraUID  = -1;
   m_nMetaFlag   = 0;
   m_nOptionFlag = 0;
}
int CIVCPCameraLiveMetadataReq::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "LiveMeta" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "ResID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( int ), "Flag", &m_nMetaFlag );
      xmlNode.Attribute( TYPE_ID( int ), "Option", &m_nOptionFlag );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPCameraLiveMetadataReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "LiveMeta" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "ResID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( int ), "Flag", &m_nMetaFlag );
      xmlNode.Attribute( TYPE_ID( int ), "Option", &m_nOptionFlag );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraLiveVideoReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraLiveVideoReq::CIVCPCameraLiveVideoReq()
{
   m_nPacketID   = IVCP_ID_Camera_LiveVideoReq;
   m_nCameraUID  = -1;
   m_nStreamFlag = 0;
}
int CIVCPCameraLiveVideoReq::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "LiveVideo" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "ResID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( int ), "Flag", &m_nStreamFlag );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPCameraLiveVideoReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "LiveVideo" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "ResID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( int ), "Flag", &m_nStreamFlag );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraPlayStreamStartReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraPlayStreamStartReq::CIVCPCameraPlayStreamStartReq()
{
   m_nPacketID   = IVCP_ID_Camera_PlayStreamStartReq;
   m_nCameraUID  = -1; // 카메라 UID
   m_nResponseID = 0;
   m_nStreamID   = 0; // 스트림 콜백 ID
   m_nStreamFlag = 0; // 전송받을 스트림 플래그
   m_nMetaFlag   = 0; // 전송받을 메타 플래그
   m_bReverse    = FALSE; // 재생 방향
   m_fPlaySpeed  = 1.0f; // 재생 속도 (1/8 ~ 32)
   m_nPlayMode   = 0;

   m_bTranscode    = FALSE; // 트랜스코딩 여부
   m_nCodecID      = 0; // 코덱 ID
   m_nResizeOption = 0; // 축소 방법 (0:비율, 1:너비기준크기, 2:고정크기)
   m_fResizeRate   = 1.0; // 축소 비율
   m_nWidth        = 0; // 크기
   m_nHeight       = 0; // 높이
   m_nBitrate      = 1000 * 1024; // 비트레이트
}
int CIVCPCameraPlayStreamStartReq::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PlayStart" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "ResID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( int ), "StreamID", &m_nStreamID );
      xmlNode.Attribute( TYPE_ID( int ), "StreamFlag", &m_nStreamFlag );
      xmlNode.Attribute( TYPE_ID( int ), "MetaFlag", &m_nMetaFlag );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "Start", &m_ftStart );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "End", &m_ftEnd );
      xmlNode.Attribute( TYPE_ID( BOOL ), "Reverse", &m_bReverse );
      xmlNode.Attribute( TYPE_ID( float ), "Speed", &m_fPlaySpeed );
      xmlNode.Attribute( TYPE_ID( int ), "Mode", &m_nPlayMode );

      xmlNode.Attribute( TYPE_ID( BOOL ), "TransCode", &m_bTranscode );
      xmlNode.Attribute( TYPE_ID( int ), "CodecID", &m_nCodecID );
      xmlNode.Attribute( TYPE_ID( int ), "ResizeOpt", &m_nResizeOption );
      xmlNode.Attribute( TYPE_ID( float ), "ResizeRete", &m_fResizeRate );
      xmlNode.Attribute( TYPE_ID( int ), "Width", &m_nWidth );
      xmlNode.Attribute( TYPE_ID( int ), "Height", &m_nHeight );
      xmlNode.Attribute( TYPE_ID( int ), "Bitrate", &m_nBitrate );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPCameraPlayStreamStartReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PlayStart" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "ResID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( int ), "StreamID", &m_nStreamID );
      xmlNode.Attribute( TYPE_ID( int ), "StreamFlag", &m_nStreamFlag );
      xmlNode.Attribute( TYPE_ID( int ), "MetaFlag", &m_nMetaFlag );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "Start", &m_ftStart );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "End", &m_ftEnd );
      xmlNode.Attribute( TYPE_ID( BOOL ), "Reverse", &m_bReverse );
      xmlNode.Attribute( TYPE_ID( float ), "Speed", &m_fPlaySpeed );
      xmlNode.Attribute( TYPE_ID( int ), "Mode", &m_nPlayMode );

      xmlNode.Attribute( TYPE_ID( BOOL ), "TransCode", &m_bTranscode );
      xmlNode.Attribute( TYPE_ID( int ), "CodecID", &m_nCodecID );
      xmlNode.Attribute( TYPE_ID( int ), "ResizeOpt", &m_nResizeOption );
      xmlNode.Attribute( TYPE_ID( float ), "ResizeRete", &m_fResizeRate );
      xmlNode.Attribute( TYPE_ID( int ), "Width", &m_nWidth );
      xmlNode.Attribute( TYPE_ID( int ), "Height", &m_nHeight );
      xmlNode.Attribute( TYPE_ID( int ), "Bitrate", &m_nBitrate );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraPlayStreamStopReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraPlayStreamStopReq::CIVCPCameraPlayStreamStopReq()
{
   m_nPacketID  = IVCP_ID_Camera_PlayStreamStopReq;
   m_nCameraUID = -1;
   m_nStreamID  = 0;
}
int CIVCPCameraPlayStreamStopReq::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PlayStop" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "ResID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( int ), "StreamID", &m_nStreamID );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPCameraPlayStreamStopReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PlayStop" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "ResID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( int ), "StreamID", &m_nStreamID );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraPlayStreamControlReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraPlayStreamControlReq::CIVCPCameraPlayStreamControlReq()
{
   m_nPacketID  = IVCP_ID_Camera_PlayStreamControlReq;
   m_nCameraUID = -1;
   m_nStreamID  = 0;
   m_fPlaySpeed = 1.0f;
   m_bReverse   = FALSE;
}
int CIVCPCameraPlayStreamControlReq::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PlayControl" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "ResID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( int ), "StreamID", &m_nStreamID );
      xmlNode.Attribute( TYPE_ID( int ), "CtrlFlag", &m_nCtrlFlag );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "PlayTime", &m_ftPlayTime );
      xmlNode.Attribute( TYPE_ID( float ), "Speed", &m_fPlaySpeed );
      xmlNode.Attribute( TYPE_ID( BOOL ), "Reverse", &m_bReverse );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPCameraPlayStreamControlReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PlayControl" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "ResID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( int ), "StreamID", &m_nStreamID );
      xmlNode.Attribute( TYPE_ID( int ), "CtrlFlag", &m_nCtrlFlag );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "PlayTime", &m_ftPlayTime );
      xmlNode.Attribute( TYPE_ID( float ), "Speed", &m_fPlaySpeed );
      xmlNode.Attribute( TYPE_ID( BOOL ), "Reverse", &m_bReverse );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraPlayDataStartReq
//
///////////////////////////////////////////////////////////////////////////////
CIVCPCameraPlayDataStartReq::CIVCPCameraPlayDataStartReq()
{
   m_nPacketID   = IVCP_ID_Camera_PlayDataStreamReq;
   m_nCameraUID  = -1;
   m_nResponseID = 0;
   m_nStreamID   = 0;
   m_nMetaFlag   = 0;
   ZeroMemory( &m_fStartTime, sizeof( FILETIME ) );
   ZeroMemory( &m_fEndTime, sizeof( FILETIME ) );
}

int CIVCPCameraPlayDataStartReq::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PlayDataStart" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "CameraUID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "RequestID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( int ), "StreamID", &m_nStreamID );
      xmlNode.Attribute( TYPE_ID( int ), "MetaFlag", &m_nMetaFlag );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "StartTime", &m_fStartTime );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "EndTime", &m_fEndTime );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPCameraPlayDataStartReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PlayDataStart" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "CameraUID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "RequestID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( int ), "StreamID", &m_nStreamID );
      xmlNode.Attribute( TYPE_ID( int ), "MetaFlag", &m_nMetaFlag );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "StartTime", &m_fStartTime );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "EndTime", &m_fEndTime );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraPlayDataCtrl
//
///////////////////////////////////////////////////////////////////////////////
CIVCPCameraPlayDataCtrlReq::CIVCPCameraPlayDataCtrlReq()
{
   m_nPacketID   = IVCP_ID_Camera_PlayDataCtrlReq;
   m_nCameraUID  = -1;
   m_nResponseID = 0;
   m_nCtrlType   = 0;
}

int CIVCPCameraPlayDataCtrlReq::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PlayDataControl" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "CameraUID ", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "RequestID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( int ), "StreamID", &m_nStreamID );
      xmlNode.Attribute( TYPE_ID( int ), "CtrlType", &m_nCtrlType );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPCameraPlayDataCtrlReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PlayDataControl" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "CameraUID ", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "RequestID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( int ), "StreamID", &m_nStreamID );
      xmlNode.Attribute( TYPE_ID( int ), "CtrlType", &m_nCtrlType );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraSnapshotReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraSnapshotReq::CIVCPCameraSnapshotReq()
{
   m_nPacketID   = IVCP_ID_Camera_SnapshotReq;
   m_nCameraUID  = -1;
   m_nResizeFlag = 0;
   m_nWidth      = 0;
   m_nHeight     = 0;
}
int CIVCPCameraSnapshotReq::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Snapshot" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "ResID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( int ), "ResizeFlag", &m_nResizeFlag );
      xmlNode.Attribute( TYPE_ID( int ), "Width", &m_nWidth );
      xmlNode.Attribute( TYPE_ID( int ), "Height", &m_nHeight );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPCameraSnapshotReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Snapshot" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "ResID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( int ), "ResizeFlag", &m_nResizeFlag );
      xmlNode.Attribute( TYPE_ID( int ), "Width", &m_nWidth );
      xmlNode.Attribute( TYPE_ID( int ), "Height", &m_nHeight );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraSnapshotRes
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraSnapshotRes::CIVCPCameraSnapshotRes()
{
   m_nPacketID   = IVCP_ID_Camera_SnapshotRes;
   m_nCameraUID  = -1;
   m_nCodecID    = 0;
   m_nWidth      = 0;
   m_nHeight     = 0;
   m_pBodyBuffer = NULL;
}
CIVCPCameraSnapshotRes::~CIVCPCameraSnapshotRes()
{
   if( m_pBodyBuffer ) free( m_pBodyBuffer );
}
int CIVCPCameraSnapshotRes::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Snapshot" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "ResID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( int ), "CodecID", &m_nCodecID );
      xmlNode.Attribute( TYPE_ID( int ), "Width", &m_nWidth );
      xmlNode.Attribute( TYPE_ID( int ), "Height", &m_nHeight );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPCameraSnapshotRes::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Snapshot" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "ResID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( int ), "CodecID", &m_nCodecID );
      xmlNode.Attribute( TYPE_ID( int ), "Width", &m_nWidth );
      xmlNode.Attribute( TYPE_ID( int ), "Height", &m_nHeight );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
void CIVCPCameraSnapshotRes::ParseBodyBuffer()
{
   FileIO buff;
   buff.Attach( (byte*)m_pBodyBuffer, m_nXMLDataLen );
   CXMLIO xmlIO( &buff, XMLIO_Read );
   if( DONE != ReadXML( &xmlIO ) ) {
      buff.Detach();
      return;
   }
}

void CIVCPCameraSnapshotRes::CreateBodybuffer( byte* pEncodedBuff, int nEncodedLen )
{
   if( m_pBodyBuffer ) delete m_pBodyBuffer;
   m_pBodyBuffer = NULL;
   m_nXMLDataLen = 0;
   m_nBodyLen    = 0;
   m_nSnapLen    = nEncodedLen;
   FileIO buff;
   CXMLIO xmlIO( &buff, XMLIO_Write );
   if( WriteXML( &xmlIO ) ) return;

   m_nXMLDataLen = buff.GetLength();
   ;
   m_nBodyLen = m_nXMLDataLen + nEncodedLen;

   // 먼저 버퍼를 만든다.
   m_pBodyBuffer = (char*)malloc( m_nXMLDataLen ) + nEncodedLen;
   if( m_pBodyBuffer ) {
      memcpy( m_pBodyBuffer, (byte*)buff, m_nXMLDataLen );
      memcpy( m_pBodyBuffer + m_nXMLDataLen, pEncodedBuff, nEncodedLen );
   }
}
int CIVCPCameraSnapshotRes::ReadFromPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      m_nCameraUID    = aPacket.m_Header.m_nOptions;
      m_nXMLDataLen   = aPacket.m_Header.m_nBinaryOptionalLen;
      m_nBodyLen      = aPacket.m_Header.m_nBodyLen;
      m_pBodyBuffer   = aPacket.m_pBody;
      aPacket.m_pBody = NULL;

      ParseBodyBuffer(); // 바로 파싱한다.
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPCameraSnapshotRes::WriteToPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      aPacket.FreeBody();
   }
   if( m_pBodyBuffer ) {
      aPacket.m_Header.m_nOptions    = m_nCameraUID;
      aPacket.m_Header.m_nBinaryOptionalLen = m_nXMLDataLen;
      aPacket.m_Header.m_nBodyLen    = m_nBodyLen;
      aPacket.m_pBody                = m_pBodyBuffer;
      m_pBodyBuffer                  = NULL;
      return ( DONE );
   }
   return ( 1 );
}

void CIVCPCameraSnapshotRes::DeleteBodyBuffer()
{
   SAFE_FREE( m_pBodyBuffer );
}

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraSettingReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraSettingReq::CIVCPCameraSettingReq()
{
   m_nPacketID    = IVCP_ID_Camera_SettingReq;
   m_nCameraUID   = -1;
   m_nSettingMask = 0;
   m_nSettingFlag = 0;
}
int CIVCPCameraSettingReq::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Setting" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "ResID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( xint64 ), "Mask", &m_nSettingMask );
      xmlNode.Attribute( TYPE_ID( xint64 ), "Flag", &m_nSettingFlag );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPCameraSettingReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Setting" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "ResID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( xint64 ), "Mask", &m_nSettingMask );
      xmlNode.Attribute( TYPE_ID( xint64 ), "Flag", &m_nSettingFlag );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraCommandReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraCommandReq::CIVCPCameraCommandReq()
{
   m_nPacketID  = IVCP_ID_Camera_CommandReq;
   m_nCameraUID = -1;
   m_nCommand   = 0;
   m_nParam1    = 0;
   m_nParam2    = 0;
}
int CIVCPCameraCommandReq::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Command" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "ResID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( int ), "Command", &m_nCommand );
      xmlNode.Attribute( TYPE_ID( int ), "Param1", &m_nParam1 );
      xmlNode.Attribute( TYPE_ID( int ), "Param2", &m_nParam2 );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPCameraCommandReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Command" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "ResID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( int ), "Command", &m_nCommand );
      xmlNode.Attribute( TYPE_ID( int ), "Param1", &m_nParam1 );
      xmlNode.Attribute( TYPE_ID( int ), "Param2", &m_nParam2 );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (PTZ) CIVCPCameraPtzContMoveReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraPtzContMoveReq::CIVCPCameraPtzContMoveReq()
{
   m_nPacketID  = IVCP_ID_Ptz_ContMoveReq;
   m_nCameraUID = -1;
   m_fPan       = IVCP_PTZ_NO_CMD;
   m_fTilt      = IVCP_PTZ_NO_CMD;
   m_fZoom      = IVCP_PTZ_NO_CMD;
   m_fFocus     = IVCP_PTZ_NO_CMD;
}
int CIVCPCameraPtzContMoveReq::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PtzContMove" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "ResID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( float ), "Pan", &m_fPan );
      xmlNode.Attribute( TYPE_ID( float ), "Tilt", &m_fTilt );
      xmlNode.Attribute( TYPE_ID( float ), "Zoom", &m_fZoom );
      xmlNode.Attribute( TYPE_ID( float ), "Focus", &m_fFocus );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPCameraPtzContMoveReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PtzContMove" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "ResID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( float ), "Pan", &m_fPan );
      xmlNode.Attribute( TYPE_ID( float ), "Tilt", &m_fTilt );
      xmlNode.Attribute( TYPE_ID( float ), "Zoom", &m_fZoom );
      xmlNode.Attribute( TYPE_ID( float ), "Focus", &m_fFocus );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (PTZ) CIVCPCameraPtzAbsMoveReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraPtzAbsMoveReq::CIVCPCameraPtzAbsMoveReq()
{
   m_nPacketID  = IVCP_ID_Ptz_AbsMoveReq;
   m_nCameraUID = -1;
   m_fPan       = IVCP_PTZ_NO_CMD;
   m_fTilt      = IVCP_PTZ_NO_CMD;
   m_fZoom      = IVCP_PTZ_NO_CMD;
   m_fFocus     = IVCP_PTZ_NO_CMD;
}
int CIVCPCameraPtzAbsMoveReq::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PtzAbsMove" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "ResID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( float ), "Pan", &m_fPan, NULL, "%.2f" );
      xmlNode.Attribute( TYPE_ID( float ), "Tilt", &m_fTilt, NULL, "%.2f" );
      xmlNode.Attribute( TYPE_ID( float ), "Zoom", &m_fZoom, NULL, "%.2f" );
      xmlNode.Attribute( TYPE_ID( float ), "Focus", &m_fFocus, NULL, "%.2f" );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPCameraPtzAbsMoveReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PtzAbsMove" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "ResID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( float ), "Pan", &m_fPan, NULL, "%.2f" );
      xmlNode.Attribute( TYPE_ID( float ), "Tilt", &m_fTilt, NULL, "%.2f" );
      xmlNode.Attribute( TYPE_ID( float ), "Zoom", &m_fZoom, NULL, "%.2f" );
      xmlNode.Attribute( TYPE_ID( float ), "Focus", &m_fFocus, NULL, "%.2f" );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (PTZ) CIVCPCameraPtzBoxMoveReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraPtzBoxMoveReq::CIVCPCameraPtzBoxMoveReq()
{
   m_nCameraUID = -1;
   m_fLeft      = 0.0f;
   m_fTop       = 0.0f;
   m_fWidth     = 0.0f;
   m_fHeight    = 0.0f;
}
int CIVCPCameraPtzBoxMoveReq::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PtzBoxMove" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "ResID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( float ), "Left", &m_fLeft );
      xmlNode.Attribute( TYPE_ID( float ), "Top", &m_fTop );
      xmlNode.Attribute( TYPE_ID( float ), "Width", &m_fWidth );
      xmlNode.Attribute( TYPE_ID( float ), "Height", &m_fHeight );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPCameraPtzBoxMoveReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PtzBoxMove" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "ResID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( float ), "Left", &m_fLeft );
      xmlNode.Attribute( TYPE_ID( float ), "Top", &m_fTop );
      xmlNode.Attribute( TYPE_ID( float ), "Width", &m_fWidth );
      xmlNode.Attribute( TYPE_ID( float ), "Height", &m_fHeight );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (PTZ) CIVCPCameraPtzGetAbsPosReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraPtzGetAbsPosReq::CIVCPCameraPtzGetAbsPosReq()
{
   m_nPacketID   = IVCP_ID_Ptz_GetAbsPosReq;
   m_nCameraUID  = -1;
   m_nResponseID = 0;
}
int CIVCPCameraPtzGetAbsPosReq::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PtzGetAbsPos" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "ResID", &m_nResponseID );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPCameraPtzGetAbsPosReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PtzGetAbsPos" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "ResID", &m_nResponseID );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (PTZ) CIVCPCameraPtzGetAbsPosRes
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraPtzGetAbsPosRes::CIVCPCameraPtzGetAbsPosRes()
{
   m_nPacketID   = IVCP_ID_Ptz_GetAbsPosRes;
   m_nCameraUID  = -1;
   m_nResponseID = 0;
   m_fPanAngle   = 0.0f; // Pan 각도 : 반시계 방향이 증가하는 방향이다. 단위(degree, 0~360도)
   m_fTiltAngle  = 0.0f; // Tilt 각도 : 아래로 향하는 방향이 증가하는 방향임. 단위(degree, 0~360도)
   m_fZoomFactor = 1.0f; // Zoom 값 : 현재 줌 위치를 나타냄. 18배줌 카메라이면 1~18사이의 값을 가짐.
   m_fFOV        = 60.0f; // FOV (Field Of View) : Pan 축으로의 화각을 나타냄. 단위(degree, 0~360도)
}
int CIVCPCameraPtzGetAbsPosRes::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PtzAbsPos" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "ResID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "Time", &m_ftFrameTime );
      xmlNode.Attribute( TYPE_ID( float ), "Pan", &m_fPanAngle );
      xmlNode.Attribute( TYPE_ID( float ), "Tilt", &m_fTiltAngle );
      xmlNode.Attribute( TYPE_ID( float ), "Zoom", &m_fZoomFactor );
      xmlNode.Attribute( TYPE_ID( float ), "FOV", &m_fFOV );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPCameraPtzGetAbsPosRes::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PtzAbsPos" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "ResID", &m_nResponseID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "Time", &m_ftFrameTime );
      xmlNode.Attribute( TYPE_ID( float ), "Pan", &m_fPanAngle );
      xmlNode.Attribute( TYPE_ID( float ), "Tilt", &m_fTiltAngle );
      xmlNode.Attribute( TYPE_ID( float ), "Zoom", &m_fZoomFactor );
      xmlNode.Attribute( TYPE_ID( float ), "FOV", &m_fFOV );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLiveFrameMetaInfo
//
///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// CIVCPFrameEvent

CIVCPFrameEvent::CIVCPFrameEvent()
{
   m_nEventID   = 0;
   m_nEventType = 0;
}
int CIVCPFrameEvent::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Event" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "ID", &m_nEventID );
      xmlNode.Attribute( TYPE_ID( int ), "Type", &m_nEventType );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPFrameEvent::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Event" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "ID", &m_nEventID );
      xmlNode.Attribute( TYPE_ID( int ), "Type", &m_nEventType );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

/////////////////////////////////////////////////////////////////////
// CIVCPFrameObject

CIVCPFrameObject::CIVCPFrameObject()
{
   m_nObjectID          = 0;
   m_nObjectStatus      = 0;
   m_nObjectType        = 0;
   m_nObjectVAEvent     = 0;
   m_nObjectEventStatus = 0;
   m_fTimeDwell         = 0;
   m_pEvents            = NULL;
   m_nEventNum          = 0;
}
CIVCPFrameObject::~CIVCPFrameObject()
{
   ClearEvents();
}
int CIVCPFrameObject::ReadXML( CXMLIO* pIO )
{
   int nEventNum = 0;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Object" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "ID", &m_nObjectID );
      xmlNode.Attribute( TYPE_ID( xint ), "Status", &m_nObjectStatus );
      xmlNode.Attribute( TYPE_ID( xint ), "Type", &m_nObjectType );
      xmlNode.Attribute( TYPE_ID( xint ), "VAEvent", &m_nObjectVAEvent );
      xmlNode.Attribute( TYPE_ID( xint ), "EventStatus", &m_nObjectEventStatus );
      xmlNode.Attribute( TYPE_ID( IBox2D ), "Box", &m_ObjectBox );
      xmlNode.Attribute( TYPE_ID( float ), "TimeDwell", &m_fTimeDwell );
      xmlNode.Attribute( TYPE_ID( int ), "EvtNum", &nEventNum );
      ClearEvents();
      m_nEventNum = 0;
      if( nEventNum > 0 ) {
         m_pEvents = new CIVCPFrameEvent[nEventNum];
         for( m_nEventNum = 0; m_nEventNum < nEventNum; m_nEventNum++ ) {
            if( m_pEvents[m_nEventNum].ReadXML( pIO ) != DONE )
               break;
         }
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPFrameObject::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Object" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "ID", &m_nObjectID );
      xmlNode.Attribute( TYPE_ID( xint ), "Status", &m_nObjectStatus );
      xmlNode.Attribute( TYPE_ID( xint ), "Type", &m_nObjectType );
      xmlNode.Attribute( TYPE_ID( xint ), "VAEvent", &m_nObjectVAEvent );
      xmlNode.Attribute( TYPE_ID( xint ), "EventStatus", &m_nObjectEventStatus );
      xmlNode.Attribute( TYPE_ID( IBox2D ), "Box", &m_ObjectBox );
      xmlNode.Attribute( TYPE_ID( float ), "TimeDwell", &m_fTimeDwell );
      xmlNode.Attribute( TYPE_ID( int ), "EvtNum", &m_nEventNum );
      if( m_pEvents ) {
         for( int i = 0; i < m_nEventNum; i++ ) {
            m_pEvents[i].WriteXML( pIO );
         }
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
void CIVCPFrameObject::ClearEvents()
{
   if( m_pEvents ) {
      delete[] m_pEvents;
      m_pEvents = NULL;
   }
}

/////////////////////////////////////////////////////////////////////
// CIVCPLiveFrameInfo

CIVCPLiveFrameInfo::CIVCPLiveFrameInfo()
{
   m_nPacketID  = IVCP_ID_Live_FrameInfo;
   m_nCameraUID = -1;
   m_nObjectNum = 0;
   m_pObjects   = NULL;
}

CIVCPLiveFrameInfo::~CIVCPLiveFrameInfo()
{
   ClearObjects();
}

int CIVCPLiveFrameInfo::ReadXML( CXMLIO* pIO )
{
   int nObjectNum = 0;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Frame" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "FrmTime", &m_ftFrameTime );
      xmlNode.Attribute( TYPE_ID( int ), "ObjNum", &nObjectNum );

      xmlNode.Attribute( TYPE_ID( xint ), "CamState", &m_nCameraState );
      xmlNode.Attribute( TYPE_ID( xint ), "PTZState", &m_nPTZState );
      xmlNode.Attribute( TYPE_ID( xint ), "PTZStateEx", &m_nPTZStateEx );
      xmlNode.Attribute( TYPE_ID( float ), "FrameRate", &m_fFrameRate );
      xmlNode.Attribute( TYPE_ID( int ), "Width", &m_nProcWidth );
      xmlNode.Attribute( TYPE_ID( int ), "Height", &m_nProcHeight );

      ClearObjects();

      if( nObjectNum > 0 ) {
         m_pObjects = new CIVCPFrameObject[nObjectNum];
         for( m_nObjectNum = 0; m_nObjectNum < nObjectNum; m_nObjectNum++ ) {
            if( m_pObjects[m_nObjectNum].ReadXML( pIO ) != DONE )
               break;
         }
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPLiveFrameInfo::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Frame" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "FrmTime", &m_ftFrameTime );
      xmlNode.Attribute( TYPE_ID( int ), "ObjNum", &m_nObjectNum );

      xmlNode.Attribute( TYPE_ID( xint ), "CamState", &m_nCameraState );
      xmlNode.Attribute( TYPE_ID( xint ), "PTZState", &m_nPTZState );
      xmlNode.Attribute( TYPE_ID( xint ), "PTZStateEx", &m_nPTZStateEx );
      xmlNode.Attribute( TYPE_ID( float ), "FrameRate", &m_fFrameRate );
      xmlNode.Attribute( TYPE_ID( int ), "Width", &m_nProcWidth );
      xmlNode.Attribute( TYPE_ID( int ), "Height", &m_nProcHeight );

      if( m_pObjects ) {
         for( int i = 0; i < m_nObjectNum; i++ ) {
            m_pObjects[i].WriteXML( pIO );
         }
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
void CIVCPLiveFrameInfo::ClearObjects()
{
   if( m_pObjects ) {
      delete[] m_pObjects;
      m_pObjects = NULL;
   }

   m_nObjectNum = 0;
}

int CIVCPLiveFrameInfo::WriteToPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      aPacket.FreeBody();
   }
   FileIO buffBody;
   if( DONE == WritePacketBody( buffBody ) ) {
      aPacket.m_Header.m_nOptions = m_nCameraUID;
      aPacket.m_Header.m_nBodyLen = buffBody.GetLength();
      aPacket.m_pBody             = (char*)buffBody.Detach();

      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLiveFrameMetaInfo
//
///////////////////////////////////////////////////////////////////////////////

CIVCPFrameObjectEx::CIVCPFrameObjectEx()
{
   m_nObjectID   = 0;
   m_nObjectType = 0;
   m_pEvents     = NULL;
   m_nEventNum   = 0;
}
CIVCPFrameObjectEx::~CIVCPFrameObjectEx()
{
   ClearEvents();
}
int CIVCPFrameObjectEx::ReadXML( CXMLIO* pIO )
{
   int nEventNum = 0;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Object" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "ID", &m_nObjectID );
      xmlNode.Attribute( TYPE_ID( xint ), "Type", &m_nObjectType );
      xmlNode.Attribute( TYPE_ID( IBox2D ), "Box", &m_ObjectBox );
      xmlNode.Attribute( TYPE_ID( int ), "Status", &m_nObjectStatus );
      xmlNode.Attribute( TYPE_ID( float ), "Area", &m_fArea );
      xmlNode.Attribute( TYPE_ID( float ), "MajorAxisLen", &m_fMajorAxisLen );
      xmlNode.Attribute( TYPE_ID( float ), "MinorAxisLen", &m_fMinorAxisLen );
      xmlNode.Attribute( TYPE_ID( float ), "Speed", &m_fSpeed );
      xmlNode.Attribute( TYPE_ID( float ), "NorSpeed", &m_fNorSpeed );
      {
         xmlNode.Attribute( TYPE_ID( float ), "RealArea", &m_fRealArea );
         xmlNode.Attribute( TYPE_ID( float ), "RealWidth", &m_fRealWidth );
         xmlNode.Attribute( TYPE_ID( float ), "RealHeight", &m_fRealHeight );
         xmlNode.Attribute( TYPE_ID( float ), "RealMajorAxisLen", &m_fRealMajorAxisLen );
         xmlNode.Attribute( TYPE_ID( float ), "RealMinorAxisLen", &m_fRealMinorAxisLen );
         xmlNode.Attribute( TYPE_ID( float ), "RealSpeed", &m_fRealSpeed );
      }
      xmlNode.Attribute( TYPE_ID( int ), "EvtNum", &nEventNum );
      ClearEvents();
      m_nEventNum = 0;
      if( nEventNum > 0 ) {
         m_pEvents = new CIVCPFrameEvent[nEventNum];
         for( m_nEventNum = 0; m_nEventNum < nEventNum; m_nEventNum++ ) {
            if( m_pEvents[m_nEventNum].ReadXML( pIO ) != DONE )
               break;
         }
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPFrameObjectEx::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Object" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "ID", &m_nObjectID );
      xmlNode.Attribute( TYPE_ID( xint ), "Type", &m_nObjectType );
      xmlNode.Attribute( TYPE_ID( IBox2D ), "Box", &m_ObjectBox );
      xmlNode.Attribute( TYPE_ID( int ), "Status", &m_nObjectStatus );
      xmlNode.Attribute( TYPE_ID( float ), "Area", &m_fArea, NULL, "%.0f" );
      xmlNode.Attribute( TYPE_ID( float ), "MajorAxisLen", &m_fMajorAxisLen, NULL, "%.1f" );
      xmlNode.Attribute( TYPE_ID( float ), "MinorAxisLen", &m_fMinorAxisLen, NULL, "%.1f" );
      xmlNode.Attribute( TYPE_ID( float ), "Speed", &m_fSpeed, NULL, "%.1f" );
      xmlNode.Attribute( TYPE_ID( float ), "NorSpeed", &m_fNorSpeed, NULL, "%.3f" );
      if( m_bSendRealValue ) {
         xmlNode.Attribute( TYPE_ID( float ), "RealArea", &m_fRealArea, NULL, "%.1f" );
         xmlNode.Attribute( TYPE_ID( float ), "RealWidth", &m_fRealWidth, NULL, "%.2f" );
         xmlNode.Attribute( TYPE_ID( float ), "RealHeight", &m_fRealHeight, NULL, "%.2f" );
         xmlNode.Attribute( TYPE_ID( float ), "RealMajorAxisLen", &m_fRealMajorAxisLen, NULL, "%.2f" );
         xmlNode.Attribute( TYPE_ID( float ), "RealMinorAxisLen", &m_fRealMinorAxisLen, NULL, "%.2f" );
         xmlNode.Attribute( TYPE_ID( float ), "RealSpeed", &m_fRealSpeed, NULL, "%.2f" );
      }
      xmlNode.Attribute( TYPE_ID( int ), "EvtNum", &m_nEventNum );
      if( m_pEvents ) {
         for( int i = 0; i < m_nEventNum; i++ ) {
            m_pEvents[i].WriteXML( pIO );
         }
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

void CIVCPFrameObjectEx::ClearEvents()
{
   if( m_pEvents ) {
      delete[] m_pEvents;
      m_pEvents = NULL;
   }
}

/////////////////////////////////////////////////////////////////////

CIVCPLiveFrameObjectInfo::CIVCPLiveFrameObjectInfo()
{
   m_nPacketID  = IVCP_ID_Live_FrmObjInfo;
   m_nCameraUID = -1;
   m_nObjectNum = 0;
   m_pObjects   = NULL;
}

CIVCPLiveFrameObjectInfo::~CIVCPLiveFrameObjectInfo()
{
   ClearObjects();
}

int CIVCPLiveFrameObjectInfo::ReadXML( CXMLIO* pIO )
{
   int nObjectNum = 0;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "FrameObj" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "FrameTime", &m_ftFrameTime );
      xmlNode.Attribute( TYPE_ID( float ), "FrameRate", &m_fFrameRate );
      xmlNode.Attribute( TYPE_ID( ISize2D ), "FrameSize", &m_szFrameSize );
      xmlNode.Attribute( TYPE_ID( int ), "ObjectNum", &nObjectNum );

      ClearObjects();
      m_nObjectNum = 0;
      if( nObjectNum > 0 ) {
         m_pObjects = new CIVCPFrameObjectEx[nObjectNum];
         for( m_nObjectNum = 0; m_nObjectNum < nObjectNum; m_nObjectNum++ ) {
            if( m_pObjects[m_nObjectNum].ReadXML( pIO ) != DONE )
               break;
         }
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPLiveFrameObjectInfo::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "FrameObj" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "FrameTime", &m_ftFrameTime );
      xmlNode.Attribute( TYPE_ID( float ), "FrameRate", &m_fFrameRate );
      xmlNode.Attribute( TYPE_ID( ISize2D ), "FrameSize", &m_szFrameSize );
      xmlNode.Attribute( TYPE_ID( int ), "ObjectNum", &m_nObjectNum );

      if( m_pObjects ) {
         for( int i = 0; i < m_nObjectNum; i++ ) {
            m_pObjects[i].WriteXML( pIO );
         }
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
void CIVCPLiveFrameObjectInfo::ClearObjects()
{
   if( m_pObjects ) {
      delete[] m_pObjects;
      m_pObjects = NULL;
   }
}

int CIVCPLiveFrameObjectInfo::WriteToPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      aPacket.FreeBody();
   }
   FileIO buffBody;
   if( DONE == WritePacketBody( buffBody ) ) {
      aPacket.m_Header.m_nOptions = m_nCameraUID;
      aPacket.m_Header.m_nBodyLen = buffBody.GetLength();
      aPacket.m_pBody             = (char*)buffBody.Detach();
      return ( DONE );
   }
   return ( 1 );
}

/////////////////////////////////////////////////////////////////////

CIVCPColorChangeInfo::CIVCPColorChangeInfo()
{
}

CIVCPColorChangeInfo::~CIVCPColorChangeInfo()
{
}

int CIVCPColorChangeInfo::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "ColorChangeInfo" ) ) {
      xmlNode.Attribute( TYPE_ID( BGRColor ), "CntBkgColor", &m_CenterBackgroundColor );
      xmlNode.Attribute( TYPE_ID( BGRColor ), "RgnBkgColor", &m_RegionBackgroundColor );
      xmlNode.Attribute( TYPE_ID( BGRColor ), "CntFrgColor", &m_CenterForegroundColor );
      xmlNode.Attribute( TYPE_ID( BGRColor ), "RgnFrgColor", &m_RegionForegroundColor );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPColorChangeInfo::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "ColorChangeInfo" ) ) {
      xmlNode.Attribute( TYPE_ID( BGRColor ), "CntBkgColor", &m_CenterBackgroundColor );
      xmlNode.Attribute( TYPE_ID( BGRColor ), "RgnBkgColor", &m_RegionBackgroundColor );
      xmlNode.Attribute( TYPE_ID( BGRColor ), "CntFrgColor", &m_CenterForegroundColor );
      xmlNode.Attribute( TYPE_ID( BGRColor ), "RgnFrgColor", &m_RegionForegroundColor );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLiveEventInfo
//
///////////////////////////////////////////////////////////////////////////////

CIVCPLiveEventInfo::CIVCPLiveEventInfo()
{
   m_nPacketID         = IVCP_ID_Live_EventInfo;
   m_nCameraUID        = -1;
   m_nEventStatus      = 0;
   m_nEventID          = 0;
   m_nEventType        = 0;
   m_fDwellTime        = 0.0f;
   m_nEventRuleID      = 0;
   m_nObjectID         = 0;
   m_nObjectType       = 0;
   m_nNumObjects       = 1;
   m_nEventZoneID      = 0;
   m_nProcWidth        = 0;
   m_nProcHeight       = 0;
   m_nThumbCodecID     = 0;
   m_nThumbWidth       = 0;
   m_nThumbHeight      = 0;
   m_bThumbnail        = FALSE;
   m_nEvtFullImgWidth  = 0;
   m_nEvtFullImgHeight = 0;
   m_bEvtFullImg       = FALSE;
   m_nThumbnailLen     = 0;
   m_nEvtFullImgLen    = 0;
   m_pBodyBuffer       = NULL;
   m_nXMLDataLen       = 0;
}

CIVCPLiveEventInfo::~CIVCPLiveEventInfo()
{
   if( m_pBodyBuffer ) free( m_pBodyBuffer );
}

int CIVCPLiveEventInfo::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Event" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "Start", &m_ftStartTime );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "End", &m_ftEndTime );
      xmlNode.Attribute( TYPE_ID( float ), "DwellTime", &m_fDwellTime );
      xmlNode.Attribute( TYPE_ID( int ), "EvtStatus", &m_nEventStatus );
      xmlNode.Attribute( TYPE_ID( int ), "EvtID", &m_nEventID );
      xmlNode.Attribute( TYPE_ID( int ), "EvtType", &m_nEventType );
      xmlNode.Attribute( TYPE_ID( int ), "EvtRuleID", &m_nEventRuleID );
      xmlNode.Attribute( TYPE_ID( int ), "ObjID", &m_nObjectID );
      xmlNode.Attribute( TYPE_ID( xint ), "ObjType", &m_nObjectType );
      xmlNode.Attribute( TYPE_ID( int ), "NumObjects", &m_nNumObjects );
      xmlNode.Attribute( TYPE_ID( IBox2D ), "ObjBox", &m_ObjectBox );
      xmlNode.Attribute( TYPE_ID( int ), "EZID", &m_nEventZoneID );
      xmlNode.Attribute( TYPE_ID( char ), "EZName", &m_strEventZoneName, sizeof( m_strEventZoneName ) );
      xmlNode.Attribute( TYPE_ID( int ), "Width", &m_nProcWidth );
      xmlNode.Attribute( TYPE_ID( int ), "Height", &m_nProcHeight );
      xmlNode.Attribute( TYPE_ID( int ), "TCodecID", &m_nThumbCodecID );
      xmlNode.Attribute( TYPE_ID( int ), "TWidth", &m_nThumbWidth );
      xmlNode.Attribute( TYPE_ID( int ), "THeight", &m_nThumbHeight );
      xmlNode.Attribute( TYPE_ID( BOOL ), "Thumbnail", &m_bThumbnail );
      xmlNode.Attribute( TYPE_ID( int ), "ThumbnailLen", &m_nThumbnailLen );
      // (mkjang-140509)
      xmlNode.Attribute( TYPE_ID( int ), "EvtFullImgWidth", &m_nEvtFullImgWidth );
      xmlNode.Attribute( TYPE_ID( int ), "EvtFullImgHeight", &m_nEvtFullImgHeight );
      xmlNode.Attribute( TYPE_ID( BOOL ), "EvtFullImg", &m_bEvtFullImg );
      xmlNode.Attribute( TYPE_ID( int ), "EvtFullImgLen", &m_nEvtFullImgLen );
      if( ER_EVENT_COLOR_CHANGE == m_nEventType ) {
         m_ColorChangeInfo.ReadXML( pIO );
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPLiveEventInfo::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Event" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "Start", &m_ftStartTime );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "End", &m_ftEndTime );
      if( ER_EVENT_DWELL == m_nEventType )
         xmlNode.Attribute( TYPE_ID( float ), "DwellTime", &m_fDwellTime );
      xmlNode.Attribute( TYPE_ID( int ), "EvtStatus", &m_nEventStatus );
      xmlNode.Attribute( TYPE_ID( int ), "EvtID", &m_nEventID );
      xmlNode.Attribute( TYPE_ID( int ), "EvtType", &m_nEventType );
      xmlNode.Attribute( TYPE_ID( int ), "EvtRuleID", &m_nEventRuleID );
      xmlNode.Attribute( TYPE_ID( int ), "ObjID", &m_nObjectID );
      xmlNode.Attribute( TYPE_ID( xint ), "ObjType", &m_nObjectType );
      xmlNode.Attribute( TYPE_ID( int ), "NumObjects", &m_nNumObjects );
      xmlNode.Attribute( TYPE_ID( IBox2D ), "ObjBox", &m_ObjectBox );
      xmlNode.Attribute( TYPE_ID( int ), "EZID", &m_nEventZoneID );
      xmlNode.Attribute( TYPE_ID( char ), "EZName", &m_strEventZoneName, sizeof( m_strEventZoneName ) );
      xmlNode.Attribute( TYPE_ID( int ), "Width", &m_nProcWidth );
      xmlNode.Attribute( TYPE_ID( int ), "Height", &m_nProcHeight );
      xmlNode.Attribute( TYPE_ID( int ), "TCodecID", &m_nThumbCodecID );
      xmlNode.Attribute( TYPE_ID( int ), "TWidth", &m_nThumbWidth );
      xmlNode.Attribute( TYPE_ID( int ), "THeight", &m_nThumbHeight );
      xmlNode.Attribute( TYPE_ID( BOOL ), "Thumbnail", &m_bThumbnail );
      xmlNode.Attribute( TYPE_ID( int ), "ThumbnailLen", &m_nThumbnailLen );
      // (mkjang-140509)
      xmlNode.Attribute( TYPE_ID( int ), "EvtFullImgWidth", &m_nEvtFullImgWidth );
      xmlNode.Attribute( TYPE_ID( int ), "EvtFullImgHeight", &m_nEvtFullImgHeight );
      xmlNode.Attribute( TYPE_ID( BOOL ), "EvtFullImg", &m_bEvtFullImg );
      xmlNode.Attribute( TYPE_ID( int ), "EvtFullImgLen", &m_nEvtFullImgLen );
      if( ER_EVENT_COLOR_CHANGE == m_nEventType ) {
         m_ColorChangeInfo.WriteXML( pIO );
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
void CIVCPLiveEventInfo::ParseBodyBuffer()
{
   FileIO buff;
   buff.Attach( (byte*)m_pBodyBuffer, m_nXMLDataLen );
   CXMLIO xmlIO( &buff, XMLIO_Read );
   if( DONE != ReadXML( &xmlIO ) ) {
      buff.Detach();
      return;
   }
}
void CIVCPLiveEventInfo::CreateBodybuffer( byte* pEncodedBuff, int nEncodedLen )
{
   if( m_pBodyBuffer ) delete m_pBodyBuffer;
   m_pBodyBuffer = NULL;
   m_nXMLDataLen = 0;
   m_nBodyLen    = 0;

   m_nThumbnailLen = nEncodedLen;

   FileIO buff;
   CXMLIO xmlIO( &buff, XMLIO_Write );
   if( WriteXML( &xmlIO ) ) return;

   m_nXMLDataLen = buff.GetLength();
   m_nBodyLen    = m_nXMLDataLen + nEncodedLen;

   // 먼저 버퍼를 만든다.
   m_pBodyBuffer = (char*)malloc( m_nXMLDataLen + nEncodedLen );
   if( m_pBodyBuffer ) {
      memcpy( m_pBodyBuffer, (byte*)buff, m_nXMLDataLen );
      memcpy( m_pBodyBuffer + m_nXMLDataLen, pEncodedBuff, nEncodedLen );
   }
}

// (mkjang-140509)
void CIVCPLiveEventInfo::CreateBodybuffer( byte* pThumbnailBuff, int nThumbnailLen, byte* pEvtFullImgBuff, int nEvtFullImgLen )
{
   if( m_pBodyBuffer ) delete m_pBodyBuffer;
   m_pBodyBuffer = NULL;
   m_nXMLDataLen = 0;
   m_nBodyLen    = 0;

   m_nThumbnailLen  = nThumbnailLen;
   m_nEvtFullImgLen = nEvtFullImgLen;

   FileIO buff;
   CXMLIO xmlIO( &buff, XMLIO_Write );
   if( WriteXML( &xmlIO ) ) return;

   m_nXMLDataLen = buff.GetLength();
   m_nBodyLen    = m_nXMLDataLen + m_nThumbnailLen + m_nEvtFullImgLen;

   // 먼저 버퍼를 만든다.
   m_pBodyBuffer = (char*)malloc( m_nXMLDataLen + nThumbnailLen + nEvtFullImgLen );
   if( m_pBodyBuffer ) {
      memcpy( m_pBodyBuffer, (byte*)buff, m_nXMLDataLen );
      memcpy( m_pBodyBuffer + m_nXMLDataLen, pThumbnailBuff, m_nThumbnailLen );
      memcpy( m_pBodyBuffer + m_nXMLDataLen + m_nThumbnailLen, pEvtFullImgBuff, m_nEvtFullImgLen );
   }
}

int CIVCPLiveEventInfo::ReadFromPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      m_nCameraUID    = aPacket.m_Header.m_nOptions;
      m_nXMLDataLen   = aPacket.m_Header.m_nBinaryOptionalLen;
      m_nBodyLen      = aPacket.m_Header.m_nBodyLen;
      m_pBodyBuffer   = aPacket.m_pBody;
      aPacket.m_pBody = NULL;

      ParseBodyBuffer(); // 바로 파싱한다.
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPLiveEventInfo::WriteToPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      aPacket.FreeBody();
   }
   if( m_pBodyBuffer ) {
      aPacket.m_Header.m_nOptions    = m_nCameraUID;
      aPacket.m_Header.m_nBinaryOptionalLen = m_nXMLDataLen;
      aPacket.m_Header.m_nBodyLen    = m_nBodyLen;
      aPacket.m_pBody                = (char*)malloc( m_nBodyLen );
      memcpy( aPacket.m_pBody, m_pBodyBuffer, m_nBodyLen );
      return ( DONE );
   }
   return ( 1 );
}

void CIVCPLiveEventInfo::DeleteBodyBuffer()
{
   SAFE_FREE( m_pBodyBuffer );
}

CIVCPLiveEventInfoSceneChanged::CIVCPLiveEventInfoSceneChanged()
    : CIVCPLiveEventInfo()
{
}

CIVCPLiveEventInfoSceneChanged::~CIVCPLiveEventInfoSceneChanged()
{
}

int CIVCPLiveEventInfoSceneChanged::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Event" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "EvtStatus", &m_nEventStatus );
      xmlNode.Attribute( TYPE_ID( int ), "EvtType", &m_nEventType );
      xmlNode.Attribute( TYPE_ID( int ), "Width", &m_nProcWidth );
      xmlNode.Attribute( TYPE_ID( int ), "Height", &m_nProcHeight );
      xmlNode.Attribute( TYPE_ID( int ), "TCodecID", &m_nThumbCodecID );
      xmlNode.Attribute( TYPE_ID( int ), "TWidth", &m_nThumbWidth );
      xmlNode.Attribute( TYPE_ID( int ), "THeight", &m_nThumbHeight );
      xmlNode.Attribute( TYPE_ID( BOOL ), "Thumbnail", &m_bThumbnail );
      xmlNode.Attribute( TYPE_ID( int ), "ThumbnailLen", &m_nThumbnailLen );
      xmlNode.End();

      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPHeatMap
//
///////////////////////////////////////////////////////////////////////////////

CIVCPHeatMap::CIVCPHeatMap()
{
   m_nPacketID   = IVCP_ID_Live_HeatMap;
   m_nCameraUID  = -1;
   m_pBodyBuffer = NULL;

   m_nHeatMapWidth    = 0;
   m_nHeatMapHeight   = 0;
   m_nHeatMapMaxValue = -1;
   m_nHeatMapMinValue = -1;
   m_nXMLDataLen      = 0;
}
CIVCPHeatMap::~CIVCPHeatMap()
{
   if( m_pBodyBuffer ) free( m_pBodyBuffer );
}

int CIVCPHeatMap::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "HeatMap" ) ) {
      xmlNode.Attribute( TYPE_ID( FILETIME ), "StartTime", &m_ftStartTime );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "EndTime", &m_ftEndTime );
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( char ), "CameraName", m_szCameraName, sizeof( m_szCameraName ) );

      xmlNode.Attribute( TYPE_ID( int ), "HeatMapType", &m_nHeatMapType );
      xmlNode.Attribute( TYPE_ID( int ), "HeatMapWidth", &m_nHeatMapWidth );
      xmlNode.Attribute( TYPE_ID( int ), "HeatMapHeight", &m_nHeatMapHeight );
      xmlNode.Attribute( TYPE_ID( int ), "MaxValue", &m_nHeatMapMaxValue );
      xmlNode.Attribute( TYPE_ID( int ), "MinValue", &m_nHeatMapMinValue );
      m_OrgHeatMapData.Create( m_nHeatMapWidth, m_nHeatMapHeight );
      xmlNode.Attribute( TYPE_ID( uint ), "OrgHeatMapData", m_OrgHeatMapData, m_OrgHeatMapData.Width * m_OrgHeatMapData.Height );
      xmlNode.Attribute( TYPE_ID( BOOL ), "IncludeHeatMapImg", &m_bIncludeHeatMapImg );

      if( m_bIncludeHeatMapImg ) {
         CXMLNode xmlNode2( pIO );
         if( xmlNode2.Start( "HeatMapImg" ) ) {
            xmlNode.Attribute( TYPE_ID( int ), "HeatMapImgCodecID", &m_nHeatMapImgCodecID );
            xmlNode.Attribute( TYPE_ID( int ), "HeatMapImgDataLen", &m_nHeatMapImgDataLen );
            xmlNode.Attribute( TYPE_ID( char ), "ColorMapType", m_szColorMapType, sizeof( m_szColorMapType ) );
            xmlNode2.End();
         }
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPHeatMap::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "HeatMap" ) ) {
      xmlNode.Attribute( TYPE_ID( FILETIME ), "StartTime", &m_ftStartTime );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "EndTime", &m_ftEndTime );
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( char ), "CameraName", m_szCameraName, sizeof( m_szCameraName ) );

      xmlNode.Attribute( TYPE_ID( int ), "HeatMapType", &m_nHeatMapType );
      xmlNode.Attribute( TYPE_ID( int ), "HeatMapWidth", &m_nHeatMapWidth );
      xmlNode.Attribute( TYPE_ID( int ), "HeatMapHeight", &m_nHeatMapHeight );
      xmlNode.Attribute( TYPE_ID( int ), "MaxValue", &m_nHeatMapMaxValue );
      xmlNode.Attribute( TYPE_ID( int ), "MinValue", &m_nHeatMapMinValue );
      xmlNode.Attribute( TYPE_ID( uint ), "OrgHeatMapData", m_OrgHeatMapData, m_OrgHeatMapData.Width * m_OrgHeatMapData.Height );
      xmlNode.Attribute( TYPE_ID( BOOL ), "IncludeHeatMapImg", &m_bIncludeHeatMapImg );

      if( m_bIncludeHeatMapImg ) {
         CXMLNode xmlNode2( pIO );
         if( xmlNode2.Start( "HeatMapImg" ) ) {
            xmlNode.Attribute( TYPE_ID( int ), "HeatMapImgCodecID", &m_nHeatMapImgCodecID );
            xmlNode.Attribute( TYPE_ID( int ), "HeatMapImgDataLen", &m_nHeatMapImgDataLen );
            xmlNode.Attribute( TYPE_ID( char ), "ColorMapType", m_szColorMapType, sizeof( m_szColorMapType ) );
            xmlNode2.End();
         }
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

void CIVCPHeatMap::ParseBodyBuffer()
{
   FileIO buff;
   buff.Attach( (byte*)m_pBodyBuffer, m_nXMLDataLen );
   CXMLIO xmlIO( &buff, XMLIO_Read );
   if( DONE != ReadXML( &xmlIO ) ) {
      buff.Detach();
      return;
   }

   //BGRImage img;
   //img.ReadJPGFile((byte*)(m_pBodyBuffer + m_nDataLen), m_nHeatMapImgDataLen);
   //img.WriteJPGFile("c:\\222.jpg", 100);
}

void CIVCPHeatMap::CreateBodyBuffer( byte* pEncodedBuff, int nEncodedLen )
{
   if( m_pBodyBuffer ) delete m_pBodyBuffer;
   m_pBodyBuffer = NULL;
   m_nXMLDataLen = 0;
   m_nBodyLen    = 0;

   m_nHeatMapImgDataLen = nEncodedLen;

   FileIO buff;
   CXMLIO xmlIO( &buff, XMLIO_Write );
   if( WriteXML( &xmlIO ) ) return;

   m_nXMLDataLen = buff.GetLength();
   m_nBodyLen    = m_nXMLDataLen + nEncodedLen;

   // 먼저 버퍼를 만든다.
   m_pBodyBuffer = (char*)malloc( m_nBodyLen );
   if( m_pBodyBuffer ) {
      memcpy( m_pBodyBuffer, (byte*)buff, m_nXMLDataLen );
      memcpy( m_pBodyBuffer + m_nXMLDataLen, pEncodedBuff, nEncodedLen );
   }
}

int CIVCPHeatMap::ReadFromPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      m_nCameraUID    = aPacket.m_Header.m_nOptions;
      m_nXMLDataLen   = aPacket.m_Header.m_nBinaryOptionalLen;
      m_nBodyLen      = aPacket.m_Header.m_nBodyLen;
      m_pBodyBuffer   = aPacket.m_pBody;
      aPacket.m_pBody = NULL;

      ParseBodyBuffer(); // 바로 파싱한다.
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPHeatMap::WriteToPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      aPacket.FreeBody();
   }
   if( m_pBodyBuffer ) {
      aPacket.m_Header.m_nOptions    = m_nCameraUID;
      aPacket.m_Header.m_nBinaryOptionalLen = m_nXMLDataLen;
      aPacket.m_Header.m_nBodyLen    = m_nBodyLen;
      aPacket.m_pBody                = (char*)malloc( m_nBodyLen );
      memcpy( aPacket.m_pBody, m_pBodyBuffer, m_nBodyLen );
      return ( DONE );
   }
   return ( 1 );
}

void CIVCPHeatMap::DeleteBodyBuffer()
{
   SAFE_FREE( m_pBodyBuffer );
}

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPStatistics
//
///////////////////////////////////////////////////////////////////////////////

CIVCPStatisticsItemInfo::CIVCPStatisticsItemInfo()
{
}

int CIVCPStatisticsItemInfo::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "EvtZone" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "ZID", &m_nZID );
      xmlNode.Attribute( TYPE_ID( char ), "ZName", m_szZName, sizeof( m_szZName ) );
      xmlNode.Attribute( TYPE_ID( float ), "AvgDwellTime", &m_avgDwellTime );
      xmlNode.Attribute( TYPE_ID( int ), "Count", &m_nCount );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPStatisticsItemInfo::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "EvtZone" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "ZID", &m_nZID );
      xmlNode.Attribute( TYPE_ID( char ), "ZName", m_szZName, sizeof( m_szZName ) );
      xmlNode.Attribute( TYPE_ID( float ), "AvgDwellTime", &m_avgDwellTime );
      xmlNode.Attribute( TYPE_ID( int ), "Count", &m_nCount );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

//////////////////////////////////////////////////////////////////////////

CIVCPStatistics::CIVCPStatistics()
{
}

int CIVCPStatistics::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Statistics" ) ) {
      xmlNode.Attribute( TYPE_ID( FILETIME ), "StartTime", &m_ftStartTime );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "EndTime", &m_ftEndTime );
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( char ), "CameraName", m_szCameraName, sizeof( m_szCameraName ) );

      int nCount = 0;
      xmlNode.Attribute( TYPE_ID( int ), "ZoneNum", &nCount );
      for( int ii = 0; ii < nCount; ii++ ) {
         CIVCPStatisticsItemInfo item;
         item.ReadXML( pIO );
         m_vecItem.push_back( item );
      }

      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPStatistics::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Statistics" ) ) {
      xmlNode.Attribute( TYPE_ID( FILETIME ), "StartTime", &m_ftStartTime );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "EndTime", &m_ftEndTime );
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( char ), "CameraName", m_szCameraName, sizeof( m_szCameraName ) );

      int nCount = m_vecItem.size();
      xmlNode.Attribute( TYPE_ID( int ), "ZoneNum", &nCount );
      for( int ii = 0; ii < nCount; ii++ ) {
         CIVCPStatisticsItemInfo& item = m_vecItem[ii];
         item.WriteXML( pIO );
      }

      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (NETA) CIVCPLiveObjectInfo
//
///////////////////////////////////////////////////////////////////////////////

CIVCPLiveObjectInfo::CIVCPLiveObjectInfo()
{
   m_nPacketID         = IVCP_ID_Live_ObjectInfo;
   m_nProcWidth        = 0;
   m_nProcHeight       = 0;
   m_bThumbnail        = FALSE;
   m_nThumbCodecID     = 0;
   m_nThumbWidth       = 0;
   m_nThumbHeight      = 0;
   m_nObjFullImgWidth  = 0;
   m_nObjFullImgHeight = 0;
   m_bObjFullImg       = FALSE;
   m_nThumbnailLen     = 0;
   m_nObjFullImgLen    = 0;
   m_pBodyBuffer       = NULL;
   m_nXMLDataLen       = 0;
}

CIVCPLiveObjectInfo::~CIVCPLiveObjectInfo()
{
   if( m_pBodyBuffer ) free( m_pBodyBuffer );
}

int CIVCPLiveObjectInfo::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Object" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "StartTime", &m_ftStartTime );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "EndTime", &m_ftEndTime );
      xmlNode.Attribute( TYPE_ID( int ), "ObjID", &m_nObjectID );
      xmlNode.Attribute( TYPE_ID( xint ), "ObjType", &m_nObjectType );
      xmlNode.Attribute( TYPE_ID( int ), "ObjStatus", &m_nObjectStatus );
      // Average
      xmlNode.Attribute( TYPE_ID( float ), "AvgArea", &m_fAvgArea, NULL, "%.0f" );
      xmlNode.Attribute( TYPE_ID( float ), "AvgWidth", &m_fAvgWidth, NULL, "%.1f" );
      xmlNode.Attribute( TYPE_ID( float ), "AvgHeight", &m_fAvgHeight, NULL, "%.1f" );
      xmlNode.Attribute( TYPE_ID( float ), "AvgAspectRatio", &m_fAvgAspectRatio, NULL, "%.3f" );
      xmlNode.Attribute( TYPE_ID( float ), "AvgAxisLengthRatio", &m_fAvgAxisLengthRatio, NULL, "%.3f" );
      xmlNode.Attribute( TYPE_ID( float ), "AvgMajorAxisLength", &m_fAvgMajorAxisLength, NULL, "%.1f" );
      xmlNode.Attribute( TYPE_ID( float ), "AvgMinorAxisLength", &m_fAvgMinorAxisLength, NULL, "%.1f" );
      xmlNode.Attribute( TYPE_ID( float ), "AvgNorSpeed", &m_fAvgNorSpeed, NULL, "%.2f" );
      xmlNode.Attribute( TYPE_ID( float ), "AvgSpeed", &m_fAvgSpeed, NULL, "%.1f" );
      if( 1 ) {
         xmlNode.Attribute( TYPE_ID( float ), "AvgRealArea", &m_fAvgRealArea, NULL, "%.1f" );
         xmlNode.Attribute( TYPE_ID( float ), "AvgRealDistance", &m_fAvgRealDistance, NULL, "%.2f" );
         xmlNode.Attribute( TYPE_ID( float ), "AvgRealWidth", &m_fAvgRealWidth, NULL, "%.2f" );
         xmlNode.Attribute( TYPE_ID( float ), "AvgRealHeight", &m_fAvgRealHeight, NULL, "%.2f" );
         xmlNode.Attribute( TYPE_ID( float ), "AvgRealMajorAxisLength", &m_fAvgRealMajorAxisLength, NULL, "%.2f" );
         xmlNode.Attribute( TYPE_ID( float ), "AvgRealMinorAxisLength", &m_fAvgRealMinorAxisLength, NULL, "%.2f" );
         xmlNode.Attribute( TYPE_ID( float ), "AvgRealSpeed", &m_fAvgRealSpeed, NULL, "%.2f" );
      }
      if( 1 ) {
         xmlNode.Attribute( TYPE_ID( int ), "MaxWidth", &m_nMaxWidth );
         xmlNode.Attribute( TYPE_ID( int ), "MaxHeight", &m_nMaxHeight );
         xmlNode.Attribute( TYPE_ID( float ), "MaxArea", &m_fMaxArea, NULL, "%.1f" );
         xmlNode.Attribute( TYPE_ID( float ), "MaxAspectRatio", &m_fMaxAspectRatio, NULL, "%.3f" );
         xmlNode.Attribute( TYPE_ID( float ), "MaxAxisLengthRatio", &m_fMaxAxisLengthRatio, NULL, "%.3f" );
         xmlNode.Attribute( TYPE_ID( float ), "MaxMajorAxisLength", &m_fMaxMajorAxisLength, NULL, "%.1f" );
         xmlNode.Attribute( TYPE_ID( float ), "MaxMinorAxisLength", &m_fMaxMinorAxisLength, NULL, "%.1f" );
         xmlNode.Attribute( TYPE_ID( float ), "MaxNorSpeed", &m_fMaxNorSpeed, NULL, "%.2f" );
         xmlNode.Attribute( TYPE_ID( float ), "MaxSpeed", &m_fMaxSpeed, NULL, "%.1f" );
         if( 1 ) {
            xmlNode.Attribute( TYPE_ID( float ), "MaxRealDistance", &m_fMaxRealDistance, NULL, "%.2f" );
            xmlNode.Attribute( TYPE_ID( float ), "MaxRealSpeed", &m_fMaxRealSpeed, NULL, "%.1f" );
         }
      }
      if( 1 ) {
         xmlNode.Attribute( TYPE_ID( int ), "MinWidth", &m_nMinWidth );
         xmlNode.Attribute( TYPE_ID( int ), "MinHeight", &m_nMinHeight );
         xmlNode.Attribute( TYPE_ID( float ), "MinArea", &m_fMinArea, NULL, "%.1f" );
         xmlNode.Attribute( TYPE_ID( float ), "MinAspectRatio", &m_fMinAspectRatio, NULL, "%.3f" );
         xmlNode.Attribute( TYPE_ID( float ), "MaxAspectRatio", &m_fMaxAspectRatio, NULL, "%.3f" );
         xmlNode.Attribute( TYPE_ID( float ), "MinAxisLengthRatio", &m_fMinAxisLengthRatio, NULL, "%.3f" );
         xmlNode.Attribute( TYPE_ID( float ), "MinMajorAxisLength", &m_fMinMajorAxisLength, NULL, "%.1f" );
         xmlNode.Attribute( TYPE_ID( float ), "MinMinorAxisLength", &m_fMinMinorAxisLength, NULL, "%.1f" );
         xmlNode.Attribute( TYPE_ID( float ), "MinNorSpeed", &m_fMinNorSpeed, NULL, "%.2f" );
         xmlNode.Attribute( TYPE_ID( float ), "MinSpeed", &m_fMinSpeed, NULL, "%.1f" );
         if( 1 ) {
            xmlNode.Attribute( TYPE_ID( float ), "MinRealDistance", &m_fMinRealDistance, NULL, "%.2f" );
            xmlNode.Attribute( TYPE_ID( float ), "MinRealSpeed", &m_fMinRealSpeed, NULL, "%.1f" );
         }
      }

      xmlNode.Attribute( TYPE_ID( float ), "Histogram", m_fHistogram, 10 );

      xmlNode.Attribute( TYPE_ID( int ), "Width", &m_nProcWidth );
      xmlNode.Attribute( TYPE_ID( int ), "Height", &m_nProcHeight );
      xmlNode.Attribute( TYPE_ID( int ), "TCodecID", &m_nThumbCodecID );
      xmlNode.Attribute( TYPE_ID( int ), "TWidth", &m_nThumbWidth );
      xmlNode.Attribute( TYPE_ID( int ), "THeight", &m_nThumbHeight );
      xmlNode.Attribute( TYPE_ID( BOOL ), "Thumbnail", &m_bThumbnail );
      xmlNode.Attribute( TYPE_ID( int ), "ThumbnailLen", &m_nThumbnailLen );

      xmlNode.Attribute( TYPE_ID( int ), "ObjFullImgWidth", &m_nObjFullImgWidth );
      xmlNode.Attribute( TYPE_ID( int ), "ObjFullImgHeight", &m_nObjFullImgHeight );
      xmlNode.Attribute( TYPE_ID( BOOL ), "ObjFullImg", &m_bObjFullImg );
      xmlNode.Attribute( TYPE_ID( int ), "ObjFullImgLen", &m_nObjFullImgLen );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPLiveObjectInfo::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Object" ) ) {
      // Average
      xmlNode.Attribute( TYPE_ID( float ), "AvgArea", &m_fAvgArea, NULL, "%.0f" );
      xmlNode.Attribute( TYPE_ID( float ), "AvgWidth", &m_fAvgWidth, NULL, "%.1f" );
      xmlNode.Attribute( TYPE_ID( float ), "AvgHeight", &m_fAvgHeight, NULL, "%.1f" );
      xmlNode.Attribute( TYPE_ID( float ), "AvgAspectRatio", &m_fAvgAspectRatio, NULL, "%.3f" );
      xmlNode.Attribute( TYPE_ID( float ), "AvgAxisLengthRatio", &m_fAvgAxisLengthRatio, NULL, "%.3f" );
      xmlNode.Attribute( TYPE_ID( float ), "AvgMajorAxisLength", &m_fAvgMajorAxisLength, NULL, "%.1f" );
      xmlNode.Attribute( TYPE_ID( float ), "AvgMinorAxisLength", &m_fAvgMinorAxisLength, NULL, "%.1f" );
      xmlNode.Attribute( TYPE_ID( float ), "AvgNorSpeed", &m_fAvgNorSpeed, NULL, "%.2f" );
      xmlNode.Attribute( TYPE_ID( float ), "AvgSpeed", &m_fAvgSpeed, NULL, "%.1f" );
      if( 1 ) {
         xmlNode.Attribute( TYPE_ID( float ), "AvgRealArea", &m_fAvgRealArea, NULL, "%.1f" );
         xmlNode.Attribute( TYPE_ID( float ), "AvgRealDistance", &m_fAvgRealDistance, NULL, "%.2f" );
         xmlNode.Attribute( TYPE_ID( float ), "AvgRealWidth", &m_fAvgRealWidth, NULL, "%.2f" );
         xmlNode.Attribute( TYPE_ID( float ), "AvgRealHeight", &m_fAvgRealHeight, NULL, "%.2f" );
         xmlNode.Attribute( TYPE_ID( float ), "AvgRealMajorAxisLength", &m_fAvgRealMajorAxisLength, NULL, "%.2f" );
         xmlNode.Attribute( TYPE_ID( float ), "AvgRealMinorAxisLength", &m_fAvgRealMinorAxisLength, NULL, "%.2f" );
         xmlNode.Attribute( TYPE_ID( float ), "AvgRealSpeed", &m_fAvgRealSpeed, NULL, "%.2f" );
      }
      if( 1 ) {
         xmlNode.Attribute( TYPE_ID( int ), "MaxWidth", &m_nMaxWidth );
         xmlNode.Attribute( TYPE_ID( int ), "MaxHeight", &m_nMaxHeight );
         xmlNode.Attribute( TYPE_ID( float ), "MaxArea", &m_fMaxArea, NULL, "%.1f" );
         xmlNode.Attribute( TYPE_ID( float ), "MaxAspectRatio", &m_fMaxAspectRatio, NULL, "%.3f" );
         xmlNode.Attribute( TYPE_ID( float ), "MaxAxisLengthRatio", &m_fMaxAxisLengthRatio, NULL, "%.3f" );
         xmlNode.Attribute( TYPE_ID( float ), "MaxMajorAxisLength", &m_fMaxMajorAxisLength, NULL, "%.1f" );
         xmlNode.Attribute( TYPE_ID( float ), "MaxMinorAxisLength", &m_fMaxMinorAxisLength, NULL, "%.1f" );
         xmlNode.Attribute( TYPE_ID( float ), "MaxNorSpeed", &m_fMaxNorSpeed, NULL, "%.2f" );
         xmlNode.Attribute( TYPE_ID( float ), "MaxSpeed", &m_fMaxSpeed, NULL, "%.1f" );
         if( 1 ) {
            xmlNode.Attribute( TYPE_ID( float ), "MaxRealDistance", &m_fMaxRealDistance, NULL, "%.2f" );
            xmlNode.Attribute( TYPE_ID( float ), "MaxRealSpeed", &m_fMaxRealSpeed, NULL, "%.1f" );
         }
      }
      if( 1 ) {
         xmlNode.Attribute( TYPE_ID( int ), "MinWidth", &m_nMinWidth );
         xmlNode.Attribute( TYPE_ID( int ), "MinHeight", &m_nMinHeight );
         xmlNode.Attribute( TYPE_ID( float ), "MinArea", &m_fMinArea, NULL, "%.1f" );
         xmlNode.Attribute( TYPE_ID( float ), "MinAspectRatio", &m_fMinAspectRatio, NULL, "%.3f" );
         xmlNode.Attribute( TYPE_ID( float ), "MaxAspectRatio", &m_fMaxAspectRatio, NULL, "%.3f" );
         xmlNode.Attribute( TYPE_ID( float ), "MinAxisLengthRatio", &m_fMinAxisLengthRatio, NULL, "%.3f" );
         xmlNode.Attribute( TYPE_ID( float ), "MinMajorAxisLength", &m_fMinMajorAxisLength, NULL, "%.1f" );
         xmlNode.Attribute( TYPE_ID( float ), "MinMinorAxisLength", &m_fMinMinorAxisLength, NULL, "%.1f" );
         xmlNode.Attribute( TYPE_ID( float ), "MinNorSpeed", &m_fMinNorSpeed, NULL, "%.2f" );
         xmlNode.Attribute( TYPE_ID( float ), "MinSpeed", &m_fMinSpeed, NULL, "%.1f" );
         if( 1 ) {
            xmlNode.Attribute( TYPE_ID( float ), "MinRealDistance", &m_fMinRealDistance, NULL, "%.2f" );
            xmlNode.Attribute( TYPE_ID( float ), "MinRealSpeed", &m_fMinRealSpeed, NULL, "%.1f" );
         }
      }

      xmlNode.Attribute( TYPE_ID( float ), "Histogram", m_fHistogram, NULL, "%.3f", 10 );

      xmlNode.Attribute( TYPE_ID( int ), "Width", &m_nProcWidth );
      xmlNode.Attribute( TYPE_ID( int ), "Height", &m_nProcHeight );
      xmlNode.Attribute( TYPE_ID( int ), "TCodecID", &m_nThumbCodecID );
      xmlNode.Attribute( TYPE_ID( int ), "TWidth", &m_nThumbWidth );
      xmlNode.Attribute( TYPE_ID( int ), "THeight", &m_nThumbHeight );
      xmlNode.Attribute( TYPE_ID( BOOL ), "Thumbnail", &m_bThumbnail );
      xmlNode.Attribute( TYPE_ID( int ), "ThumbnailLen", &m_nThumbnailLen );

      xmlNode.Attribute( TYPE_ID( int ), "ObjFullImgWidth", &m_nObjFullImgWidth );
      xmlNode.Attribute( TYPE_ID( int ), "ObjFullImgHeight", &m_nObjFullImgHeight );
      xmlNode.Attribute( TYPE_ID( BOOL ), "ObjFullImg", &m_bObjFullImg );
      xmlNode.Attribute( TYPE_ID( int ), "ObjFullImgLen", &m_nObjFullImgLen );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

void CIVCPLiveObjectInfo::ParseBodyBuffer()
{
   FileIO buff;
   buff.Attach( (byte*)m_pBodyBuffer, m_nXMLDataLen );
   CXMLIO xmlIO( &buff, XMLIO_Read );
   if( DONE != ReadXML( &xmlIO ) ) {
      buff.Detach();
      return;
   }
}

void CIVCPLiveObjectInfo::CreateBodybuffer( byte* pEncodedBuff, int nEncodedLen )
{
   if( m_pBodyBuffer ) delete m_pBodyBuffer;
   m_pBodyBuffer = NULL;
   m_nXMLDataLen = 0;
   m_nBodyLen    = 0;

   m_nThumbnailLen = nEncodedLen;

   FileIO buff;
   CXMLIO xmlIO( &buff, XMLIO_Write );
   if( WriteXML( &xmlIO ) ) return;

   m_nXMLDataLen = buff.GetLength();
   ;
   m_nBodyLen = m_nXMLDataLen + nEncodedLen;

   // 먼저 버퍼를 만든다.
   m_pBodyBuffer = (char*)malloc( m_nXMLDataLen + nEncodedLen );
   if( m_pBodyBuffer ) {
      memcpy( m_pBodyBuffer, (byte*)buff, m_nXMLDataLen );
      memcpy( m_pBodyBuffer + m_nXMLDataLen, pEncodedBuff, nEncodedLen );
   }
}

// (mkjang-140509)
void CIVCPLiveObjectInfo::CreateBodybuffer( byte* pThumbnailBuff, int nThumbnailLen, byte* pObjFullImgBuff, int nObjFullImgLen )
{
   if( m_pBodyBuffer ) delete m_pBodyBuffer;
   m_pBodyBuffer = NULL;
   m_nXMLDataLen = 0;
   m_nBodyLen    = 0;

   m_nThumbnailLen  = nThumbnailLen;
   m_nObjFullImgLen = nObjFullImgLen;

   FileIO buff;
   CXMLIO xmlIO( &buff, XMLIO_Write );
   if( WriteXML( &xmlIO ) ) return;

   m_nXMLDataLen = buff.GetLength();
   ;
   m_nBodyLen = m_nXMLDataLen + m_nThumbnailLen + m_nObjFullImgLen;

   // 먼저 버퍼를 만든다.
   m_pBodyBuffer = (char*)malloc( m_nXMLDataLen + nThumbnailLen + nObjFullImgLen );
   if( m_pBodyBuffer ) {
      memcpy( m_pBodyBuffer, (byte*)buff, m_nXMLDataLen );
      memcpy( m_pBodyBuffer + m_nXMLDataLen, pThumbnailBuff, m_nThumbnailLen );
      memcpy( m_pBodyBuffer + m_nXMLDataLen + m_nThumbnailLen, pObjFullImgBuff, m_nObjFullImgLen );
   }
}

int CIVCPLiveObjectInfo::ReadFromPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      m_nCameraUID    = aPacket.m_Header.m_nOptions;
      m_nXMLDataLen   = aPacket.m_Header.m_nBinaryOptionalLen;
      m_nBodyLen      = aPacket.m_Header.m_nBodyLen;
      m_pBodyBuffer   = aPacket.m_pBody;
      aPacket.m_pBody = NULL;

      ParseBodyBuffer(); // 바로 파싱한다.
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPLiveObjectInfo::WriteToPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      aPacket.FreeBody();
   }
   if( m_pBodyBuffer ) {
      aPacket.m_Header.m_nOptions    = m_nCameraUID;
      aPacket.m_Header.m_nBinaryOptionalLen = m_nXMLDataLen;
      aPacket.m_Header.m_nBodyLen    = m_nBodyLen;
      aPacket.m_pBody                = (char*)malloc( m_nBodyLen );
      memcpy( aPacket.m_pBody, m_pBodyBuffer, m_nBodyLen );
      return ( DONE );
   }
   return ( 1 );
}

void CIVCPLiveObjectInfo::DeleteBodyBuffer()
{
   SAFE_FREE( m_pBodyBuffer );
}

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLivePTZDataInfo
//
///////////////////////////////////////////////////////////////////////////////

CIVCPLivePTZDataInfo::CIVCPLivePTZDataInfo()
{
   m_nPacketID   = IVCP_ID_Live_PtzDataInfo;
   m_nCameraUID  = -1;
   m_fTiltAngle  = 0.0f; // Pan 각도 : 반시계 방향이 증가하는 방향이다. 단위(degree, 0~360도)
   m_fTiltAngle  = 0.0f; // Tilt 각도 : 아래로 향하는 방향이 증가하는 방향임. 단위(degree, 0~360도)
   m_fZoomFactor = 1.0f; // Zoom 값 : 현재 줌 위치를 나타냄. 18배줌 카메라이면 1~18사이의 값을 가짐.
   m_fFOV        = 60.0f; // FOV (Field Of View) : Pan 축으로의 화각을 나타냄. 단위(degree, 0~360도)
}
int CIVCPLivePTZDataInfo::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PtzData" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "Time", &m_ftFrameTime );
      xmlNode.Attribute( TYPE_ID( float ), "Pan", &m_fPanAngle );
      xmlNode.Attribute( TYPE_ID( float ), "Tilt", &m_fTiltAngle );
      xmlNode.Attribute( TYPE_ID( float ), "Zoom", &m_fZoomFactor );
      xmlNode.Attribute( TYPE_ID( float ), "FOV", &m_fFOV );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPLivePTZDataInfo::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PtzData" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "Time", &m_ftFrameTime );
      xmlNode.Attribute( TYPE_ID( float ), "Pan", &m_fPanAngle );
      xmlNode.Attribute( TYPE_ID( float ), "Tilt", &m_fTiltAngle );
      xmlNode.Attribute( TYPE_ID( float ), "Zoom", &m_fZoomFactor );
      xmlNode.Attribute( TYPE_ID( float ), "FOV", &m_fFOV );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLivePresetIdInfo
//
///////////////////////////////////////////////////////////////////////////////

CIVCPLivePresetIdInfo::CIVCPLivePresetIdInfo()
{
   m_nPacketID    = IVCP_ID_Live_PresetIdInfo;
   m_nCameraUID   = -1;
   m_nCurPresetID = 0;
}
int CIVCPLivePresetIdInfo::ReadXML( CXMLIO* pIO )
{
   int nObjectNum = 0;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PresetID" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "FrmTime", &m_ftFrameTime );
      xmlNode.Attribute( TYPE_ID( int ), "PresetID", &m_nCurPresetID );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPLivePresetIdInfo::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PresetID" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "FrmTime", &m_ftFrameTime );
      xmlNode.Attribute( TYPE_ID( int ), "PresetID", &m_nCurPresetID );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLiveEventZoneInfo
//
///////////////////////////////////////////////////////////////////////////////

CIVCPEventRule_DetectionTime::CIVCPEventRule_DetectionTime()
{
   m_fDetectionTime = 0.0f;
}
int CIVCPEventRule_DetectionTime::ReadXML( CXMLIO* pIO )
{
   int nObjectNum = 0;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "RuleTime" ) ) {
      xmlNode.Attribute( TYPE_ID( float ), "DectectionTime", &m_fDetectionTime );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPEventRule_DetectionTime::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "RuleTime" ) ) {
      xmlNode.Attribute( TYPE_ID( float ), "DectectTime", &m_fDetectionTime );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

CIVCPEventRule_Counting::CIVCPEventRule_Counting()
{
   m_nCount       = 0;
   m_fAspectRatio = 0.0f;
}
int CIVCPEventRule_Counting::ReadXML( CXMLIO* pIO )
{
   int nObjectNum = 0;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "RuleCounting" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Count", &m_nCount );
      xmlNode.Attribute( TYPE_ID( float ), "AspectRatio", &m_fAspectRatio );
      xmlNode.Attribute( TYPE_ID( ILine2D ), "MajorAxis", &m_aMajorAxis );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPEventRule_Counting::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "RuleCounting" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Count", &m_nCount );
      xmlNode.Attribute( TYPE_ID( float ), "AspectRatio", &m_fAspectRatio );
      xmlNode.Attribute( TYPE_ID( ILine2D ), "MajorAxis", &m_aMajorAxis );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

CIVCPEventRule_DirectionalMotion::CIVCPEventRule_DirectionalMotion()
{
   m_fDetectionTime = 0.0f;
   m_fDirection     = 0.0f;
   m_fRange         = 0.0f;
}
int CIVCPEventRule_DirectionalMotion::ReadXML( CXMLIO* pIO )
{
   int nObjectNum = 0;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "RuleDirMotion" ) ) {
      xmlNode.Attribute( TYPE_ID( float ), "DectectTime", &m_fDetectionTime );
      xmlNode.Attribute( TYPE_ID( float ), "Direction", &m_fDirection );
      xmlNode.Attribute( TYPE_ID( float ), "Range", &m_fRange );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPEventRule_DirectionalMotion::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "RuleDirMotion" ) ) {
      xmlNode.Attribute( TYPE_ID( float ), "DectectTime", &m_fDetectionTime );
      xmlNode.Attribute( TYPE_ID( float ), "Direction", &m_fDirection );
      xmlNode.Attribute( TYPE_ID( float ), "Range", &m_fRange );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

CIVCPEventRule_PathPassingItem::CIVCPEventRule_PathPassingItem()
{
   m_nID          = 0;
   m_nVerticesNum = 0;
   m_pVertices    = NULL;
}
CIVCPEventRule_PathPassingItem::~CIVCPEventRule_PathPassingItem()
{
   ClearVertices();
}
int CIVCPEventRule_PathPassingItem::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PathPassing" ) ) {
      xmlNode.Attribute( TYPE_ID( float ), "ID", &m_nID );
      xmlNode.Attribute( TYPE_ID( IPoint2D ), "CenterPt", &m_ptCenter );
      xmlNode.Attribute( TYPE_ID( int ), "VerticesNum", &m_nVerticesNum );
      ClearVertices();
      if( m_nVerticesNum > 0 ) {
         m_pVertices = new IPoint2D[m_nVerticesNum];
         xmlNode.Attribute( TYPE_ID( IPoint2D ), "Vertices", m_pVertices, m_nVerticesNum );
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPEventRule_PathPassingItem::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PathPassing" ) ) {
      xmlNode.Attribute( TYPE_ID( float ), "ID", &m_nID );
      xmlNode.Attribute( TYPE_ID( IPoint2D ), "CenterPt", &m_ptCenter );
      xmlNode.Attribute( TYPE_ID( int ), "VerticesNum", &m_nVerticesNum );
      if( m_nVerticesNum > 0 ) {
         xmlNode.Attribute( TYPE_ID( IPoint2D ), "Vertices", m_pVertices, m_nVerticesNum );
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
void CIVCPEventRule_PathPassingItem::ClearVertices()
{
   if( m_pVertices ) {
      delete[] m_pVertices;
      m_pVertices = NULL;
   }
}

CIVCPEventRule_PathPassingArrow::CIVCPEventRule_PathPassingArrow()
{
   m_nSplitZoneID = 0;
}
int CIVCPEventRule_PathPassingArrow::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Arrow" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "SplitID", &m_nSplitZoneID );
      xmlNode.Attribute( TYPE_ID( ILine2D ), "Arrow", &m_lnArrow );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPEventRule_PathPassingArrow::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Arrow" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "SplitID", &m_nSplitZoneID );
      xmlNode.Attribute( TYPE_ID( ILine2D ), "Arrow", &m_lnArrow );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

CIVCPEventRule_PathPassing::CIVCPEventRule_PathPassing()
{
   m_nDirection     = 0;
   m_fDetectionTime = 0.0f;
   m_fPassingTime   = 0.0f;
   m_nItemNum       = 0;
   m_pItems         = NULL;
   m_nArrowNum      = 0;
   m_pArrows        = NULL;
}
CIVCPEventRule_PathPassing::~CIVCPEventRule_PathPassing()
{
   ClearItems();
}
int CIVCPEventRule_PathPassing::ReadXML( CXMLIO* pIO )
{
   int nItemNum  = 0;
   int nArrowNum = 0;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "RulePassPathing" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Direction", &m_nDirection );
      xmlNode.Attribute( TYPE_ID( float ), "DectectTime", &m_fDetectionTime, NULL, "%.1f" );
      xmlNode.Attribute( TYPE_ID( float ), "PassingTime", &m_fPassingTime, NULL, "%.1f" );
      xmlNode.Attribute( TYPE_ID( int ), "ItemNum", &nItemNum );
      xmlNode.Attribute( TYPE_ID( int ), "ArrowNum", &nArrowNum );
      ClearItems();
      m_nItemNum = 0;
      if( nItemNum > 0 ) {
         m_pItems = new CIVCPEventRule_PathPassingItem[nItemNum];
         for( m_nItemNum = 0; m_nItemNum < nItemNum; m_nItemNum++ ) {
            if( m_pItems[m_nItemNum].ReadXML( pIO ) != DONE )
               break;
         }
      }
      m_nArrowNum = 0;
      if( nArrowNum > 0 ) {
         m_pArrows = new CIVCPEventRule_PathPassingArrow[nArrowNum];
         for( m_nArrowNum = 0; m_nArrowNum < nArrowNum; m_nArrowNum++ ) {
            if( m_pArrows[m_nArrowNum].ReadXML( pIO ) != DONE )
               break;
         }
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPEventRule_PathPassing::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "RulePassPathing" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Direction", &m_nDirection );
      xmlNode.Attribute( TYPE_ID( float ), "DectectTime", &m_fDetectionTime );
      xmlNode.Attribute( TYPE_ID( float ), "PassingTime", &m_fPassingTime );
      xmlNode.Attribute( TYPE_ID( int ), "ItemNum", &m_nItemNum );
      xmlNode.Attribute( TYPE_ID( int ), "ArrowNum", &m_nArrowNum );
      if( m_pItems ) {
         for( int i = 0; i < m_nItemNum; i++ ) {
            m_pItems[i].WriteXML( pIO );
         }
      }
      if( m_pArrows ) {
         for( int i = 0; i < m_nArrowNum; i++ ) {
            m_pArrows[i].WriteXML( pIO );
         }
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
void CIVCPEventRule_PathPassing::ClearItems()
{
   if( m_pItems ) {
      delete[] m_pItems;
      m_pItems = NULL;
   }
   if( m_pArrows ) {
      delete[] m_pArrows;
      m_pArrows = NULL;
   }
}

CIVCPEventRule_Flooding::CIVCPEventRule_Flooding()
{
   m_nDirection     = 0;
   m_fDetectionTime = 0.0f;
   m_fThreshold     = 0.0f;
   m_CrossingLine[0]( -1, -1 );
   m_CrossingLine[1]( -1, -1 );
}
int CIVCPEventRule_Flooding::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "RuleFlooding" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Direction", &m_nDirection );
      xmlNode.Attribute( TYPE_ID( float ), "DetectionTime", &m_fDetectionTime );
      xmlNode.Attribute( TYPE_ID( float ), "Threshold", &m_fThreshold );
      xmlNode.Attribute( TYPE_ID( ILine2D ), "Range", &m_CrossingLine );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPEventRule_Flooding::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "RuleFlooding" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Direction", &m_nDirection );
      xmlNode.Attribute( TYPE_ID( float ), "DetectionTime", &m_fDetectionTime );
      xmlNode.Attribute( TYPE_ID( float ), "Threshold", &m_fThreshold );
      xmlNode.Attribute( TYPE_ID( ILine2D ), "Range", &m_CrossingLine );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

CIVCPEventRule_LineCrossing::CIVCPEventRule_LineCrossing()
{
   m_nDirection = 0;
   m_CrossingLine[0]( -1, -1 );
   m_CrossingLine[1]( -1, -1 );
}
int CIVCPEventRule_LineCrossing::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "RuleLineCrossing" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Direction", &m_nDirection );
      xmlNode.Attribute( TYPE_ID( ILine2D ), "Range", &m_CrossingLine );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPEventRule_LineCrossing::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "RuleLineCrossing" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Direction", &m_nDirection );
      xmlNode.Attribute( TYPE_ID( ILine2D ), "Range", &m_CrossingLine );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
/////////////////////////////////////////////////////////////////////

CIVCPEventZone::CIVCPEventZone()
{
   m_nZoneID      = 0;
   m_nPresetID    = 0;
   m_nPriority    = 0;
   m_nOptions     = 0;
   m_nVerticesNum = 0;
   m_pVertices    = NULL;

   m_nRuleOptions    = 0;
   m_nRuleEventType  = 0;
   m_nRuleObjectType = 0;
}
CIVCPEventZone::~CIVCPEventZone()
{
   ClearVertices();
}
void CIVCPEventZone::ClearVertices()
{
   if( m_pVertices ) {
      delete[] m_pVertices;
      m_pVertices = NULL;
   }
}
int CIVCPEventZone::ReadXML( CXMLIO* pIO )
{
   int nObjectNum = 0;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "EventZone" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "ZoneID", &m_nZoneID );
      xmlNode.Attribute( TYPE_ID( int ), "PresetID", &m_nPresetID );
      xmlNode.Attribute( TYPE_ID( int ), "Priority", &m_nPriority );
      xmlNode.Attribute( TYPE_ID( int ), "Options", &m_nOptions );
      xmlNode.Attribute( TYPE_ID( int ), "Width", &m_nWidth );
      xmlNode.Attribute( TYPE_ID( int ), "Height", &m_nHeight );
      xmlNode.Attribute( TYPE_ID( char ), "ZoneName", &m_strName, sizeof( m_strName ) );
      xmlNode.Attribute( TYPE_ID( IPoint2D ), "CenterPt", &m_ptCenter );
      xmlNode.Attribute( TYPE_ID( int ), "VerticesNum", &m_nVerticesNum );
      ClearVertices();
      if( m_nVerticesNum > 0 ) {
         m_pVertices = new IPoint2D[m_nVerticesNum];
         xmlNode.Attribute( TYPE_ID( IPoint2D ), "Vertices", m_pVertices, m_nVerticesNum );
      }
      xmlNode.Attribute( TYPE_ID( int ), "RuleOptions", &m_nRuleOptions );
      xmlNode.Attribute( TYPE_ID( int ), "RuleEvtType", &m_nRuleEventType );
      xmlNode.Attribute( TYPE_ID( int ), "RuleObjType", &m_nRuleObjectType );
      xmlNode.Attribute( TYPE_ID( char ), "RuleName", &m_strRuleName, sizeof( m_strRuleName ) );

      m_aAbandoned.ReadXML( pIO );
      m_aCounting.ReadXML( pIO );
      m_aDirectionalMotion.ReadXML( pIO );
      m_aPathPassing.ReadXML( pIO );
      m_aLineCrossing.ReadXML( pIO );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPEventZone::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "EventZone" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "ZoneID", &m_nZoneID );
      xmlNode.Attribute( TYPE_ID( int ), "PresetID", &m_nPresetID );
      xmlNode.Attribute( TYPE_ID( int ), "Priority", &m_nPriority );
      xmlNode.Attribute( TYPE_ID( int ), "Options", &m_nOptions );
      xmlNode.Attribute( TYPE_ID( int ), "Width", &m_nWidth );
      xmlNode.Attribute( TYPE_ID( int ), "Height", &m_nHeight );
      xmlNode.Attribute( TYPE_ID( char ), "ZoneName", &m_strName, sizeof( m_strName ) );
      xmlNode.Attribute( TYPE_ID( IPoint2D ), "CenterPt", &m_ptCenter );
      xmlNode.Attribute( TYPE_ID( int ), "VerticesNum", &m_nVerticesNum );
      if( m_nVerticesNum > 0 ) {
         xmlNode.Attribute( TYPE_ID( IPoint2D ), "Vertices", m_pVertices, m_nVerticesNum );
      }
      xmlNode.Attribute( TYPE_ID( int ), "RuleOptions", &m_nRuleOptions );
      xmlNode.Attribute( TYPE_ID( int ), "RuleEvtType", &m_nRuleEventType );
      xmlNode.Attribute( TYPE_ID( int ), "RuleObjType", &m_nRuleObjectType );
      xmlNode.Attribute( TYPE_ID( char ), "RuleName", &m_strRuleName, sizeof( m_strRuleName ) );

      m_aAbandoned.WriteXML( pIO );
      m_aCounting.WriteXML( pIO );
      m_aDirectionalMotion.WriteXML( pIO );
      m_aPathPassing.WriteXML( pIO );
      m_aLineCrossing.ReadXML( pIO );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

/////////////////////////////////////////////////////////////////////

CIVCPLiveEventZoneInfo::CIVCPLiveEventZoneInfo()
{
   m_nPacketID   = IVCP_ID_Live_EventZoneInfo;
   m_nCameraUID  = -1;
   m_nZoneNum    = 0;
   m_pEventZones = NULL;
}
CIVCPLiveEventZoneInfo::~CIVCPLiveEventZoneInfo()
{
   ClearEventZones();
}
void CIVCPLiveEventZoneInfo::ClearEventZones()
{
   if( m_pEventZones ) {
      delete[] m_pEventZones;
      m_pEventZones = NULL;
   }
}
int CIVCPLiveEventZoneInfo::ReadXML( CXMLIO* pIO )
{
   int nZoneNum = 0;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "EventZoneInfo" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "FrmTime", &m_ftFrameTime );
      xmlNode.Attribute( TYPE_ID( int ), "Width", &m_nWidth );
      xmlNode.Attribute( TYPE_ID( int ), "Height", &m_nHeight );
      xmlNode.Attribute( TYPE_ID( int ), "ZoneNum", &nZoneNum );
      ClearEventZones();
      m_nZoneNum = 0;
      if( nZoneNum > 0 ) {
         m_pEventZones = new CIVCPEventZone[nZoneNum];
         for( m_nZoneNum = 0; m_nZoneNum < nZoneNum; m_nZoneNum++ ) {
            if( m_pEventZones[m_nZoneNum].ReadXML( pIO ) != DONE )
               break;
         }
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPLiveEventZoneInfo::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "EventZoneInfo" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "FrmTime", &m_ftFrameTime );
      xmlNode.Attribute( TYPE_ID( int ), "Width", &m_nWidth );
      xmlNode.Attribute( TYPE_ID( int ), "Height", &m_nHeight );
      xmlNode.Attribute( TYPE_ID( int ), "ZoneNum", &m_nZoneNum );
      if( m_pEventZones ) {
         for( int i = 0; i < m_nZoneNum; i++ ) {
            m_pEventZones[i].WriteXML( pIO );
         }
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLiveEventZoneCountInfo
//
///////////////////////////////////////////////////////////////////////////////

CIVCPEventZoneCount::CIVCPEventZoneCount()
{
   m_nZoneID    = 0;
   m_nZoneCount = 0;

   m_CurDwellObjectCount   = 0;
   m_TotalDwellObjectCount = 0;
   m_AverageDwellTime      = 0;
}
int CIVCPEventZoneCount::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Item" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "ZID", &m_nZoneID );
      xmlNode.Attribute( TYPE_ID( int ), "Count", &m_nZoneCount );

      xmlNode.Attribute( TYPE_ID( int ), "CurDwellObjectCount", &m_CurDwellObjectCount );
      xmlNode.Attribute( TYPE_ID( int ), "TotalDwellObjectCount", &m_TotalDwellObjectCount );
      xmlNode.Attribute( TYPE_ID( float ), "AverageDwellTime", &m_AverageDwellTime );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPEventZoneCount::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Item" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "ZID", &m_nZoneID );
      xmlNode.Attribute( TYPE_ID( int ), "Count", &m_nZoneCount );

      xmlNode.Attribute( TYPE_ID( int ), "CurDwellObjectCount", &m_CurDwellObjectCount );
      xmlNode.Attribute( TYPE_ID( int ), "TotalDwellObjectCount", &m_TotalDwellObjectCount );
      xmlNode.Attribute( TYPE_ID( float ), "AverageDwellTime", &m_AverageDwellTime );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////

CIVCPLiveEventZoneCountInfo::CIVCPLiveEventZoneCountInfo()
{
   m_nPacketID   = IVCP_ID_Live_EventZoneCount;
   m_nCameraUID  = -1;
   m_nZoneNum    = 0;
   m_pZoneCounts = NULL;

   m_bRWOnlyCountInfo = FALSE;
}

CIVCPLiveEventZoneCountInfo::~CIVCPLiveEventZoneCountInfo()
{
   ClearZoneCounts();
}
int CIVCPLiveEventZoneCountInfo::ReadXML( CXMLIO* pIO )
{
   int nZoneNum = 0;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "ZoneCnt" ) ) {
      if( FALSE == m_bRWOnlyCountInfo ) {
         xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
         xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
         xmlNode.Attribute( TYPE_ID( FILETIME ), "FrmTime", &m_ftFrameTime );
      }
      xmlNode.Attribute( TYPE_ID( int ), "ZoneNum", &nZoneNum );

      ClearZoneCounts();

      if( nZoneNum > 0 ) {
         m_pZoneCounts = new CIVCPEventZoneCount[nZoneNum];
         for( m_nZoneNum = 0; m_nZoneNum < nZoneNum; m_nZoneNum++ ) {
            if( m_pZoneCounts[m_nZoneNum].ReadXML( pIO ) != DONE )
               break;
         }
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPLiveEventZoneCountInfo::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "ZoneCnt" ) ) {
      if( FALSE == m_bRWOnlyCountInfo ) {
         xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
         xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
         xmlNode.Attribute( TYPE_ID( FILETIME ), "FrmTime", &m_ftFrameTime );
      }
      xmlNode.Attribute( TYPE_ID( int ), "ZoneNum", &m_nZoneNum );

      if( m_pZoneCounts ) {
         for( int i = 0; i < m_nZoneNum; i++ ) {
            m_pZoneCounts[i].WriteXML( pIO );
         }
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
void CIVCPLiveEventZoneCountInfo::ClearZoneCounts()
{
   if( m_pZoneCounts ) {
      delete[] m_pZoneCounts;
      m_pZoneCounts = NULL;
   }
   m_nZoneNum = 0;
}

int CIVCPLiveEventZoneCountInfo::WriteToPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      aPacket.FreeBody();
   }
   FileIO buffBody;
   if( DONE == WritePacketBody( buffBody ) ) {
      aPacket.m_Header.m_nOptions = m_nCameraUID;
      aPacket.m_Header.m_nBodyLen = buffBody.GetLength();
      aPacket.m_pBody             = (char*)buffBody.Detach();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// CIVCPResetObjectCountPeriod
//
///////////////////////////////////////////////////////////////////////////////

std::string GetResetTimeVectorToString( const std::vector<RESET_OBJECT_TIME>& vecReset )
{
   std::string strResetTime;
   for( int ii = 0; ii < (int)vecReset.size(); ii++ ) {
      const RESET_OBJECT_TIME& stTime = vecReset[ii];
      std::string strTemp             = sformat( "%d:%d,", stTime.m_nDay, stTime.m_nTime );
      strResetTime += strTemp;
   }
   if( vecReset.size() )
      strResetTime.erase( strResetTime.length() - 1, 1 );

   return strResetTime;
}

void GetResetTimeToStringToVector( const std::string& strResetTime, std::vector<RESET_OBJECT_TIME>& vecReset )
{
   std::vector<std::string> vecSplit;
   GetSplitStringList( strResetTime, ',', vecSplit );
   for( int ii = 0; ii < (int)vecSplit.size(); ii++ ) {
      std::string& strTemp = vecSplit[ii];
      size_t pos           = strTemp.find( ':' );
      if( std::string::npos != pos ) {
         RESET_OBJECT_TIME stTime;
         stTime.m_nDay  = atoi( strTemp.substr( 0, pos++ ).c_str() );
         stTime.m_nTime = atoi( strTemp.substr( pos, strTemp.length() - pos ).c_str() );
         vecReset.push_back( stTime );
      }
   }
}

CIVCPResetObjectCountPeriod::CIVCPResetObjectCountPeriod()
{
   m_nCameraUID  = -1;
   m_nPeriodTime = -1;
   m_bUse        = FALSE;
}

CIVCPResetObjectCountPeriod::~CIVCPResetObjectCountPeriod()
{
   m_vecReset.clear();
}

void CIVCPResetObjectCountPeriod::PushResetTime( int nDay, int nTime )
{
   BOOL bPush = TRUE;
   for( int ii = 0; ii < (int)m_vecReset.size(); ii++ ) {
      if( m_vecReset[ii].m_nDay == nDay && m_vecReset[ii].m_nTime == nTime ) {
         bPush = FALSE;
         break;
      }
   }
   if( !bPush ) return;

   RESET_OBJECT_TIME stTime;
   stTime.m_nDay  = nDay;
   stTime.m_nTime = nTime;
   m_vecReset.push_back( stTime );
}

int CIVCPResetObjectCountPeriod::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "ResetObjectCntPeriod" ) ) {
      std::string strResetTime;
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_strGuid );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( BOOL ), "Use", &m_bUse );
      xmlNode.Attribute( TYPE_ID( int ), "PeriodTime", &m_nPeriodTime );
      xmlNode.Attribute( TYPE_ID( string ), "ResetTimeDay", &strResetTime );
      GetResetTimeToStringToVector( strResetTime, m_vecReset );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPResetObjectCountPeriod::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "ResetObjectCntPeriod" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_strGuid );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( BOOL ), "Use", &m_bUse );
      xmlNode.Attribute( TYPE_ID( int ), "PeriodTime", &m_nPeriodTime );
      std::string strResetTime = GetResetTimeVectorToString( m_vecReset );
      xmlNode.Attribute( TYPE_ID( string ), "ResetTimeDay", &strResetTime );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLiveEventZoneCountInfo
//
///////////////////////////////////////////////////////////////////////////////

CIVCPPresetItemInfo::CIVCPPresetItemInfo()
{
   m_nID        = 0;
   m_strName[0] = '\0';
}

int CIVCPPresetItemInfo::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Item" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "ID", &m_nID );
      xmlNode.Attribute( TYPE_ID( char ), "Name", &m_strName, sizeof( m_strName ) );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPPresetItemInfo::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Item" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "ID", &m_nID );
      xmlNode.Attribute( TYPE_ID( char ), "Name", &m_strName, sizeof( m_strName ) );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

CIVCPPresetGroupInfo::CIVCPPresetGroupInfo()
{
   m_nID          = 0;
   m_strName[0]   = '\0';
   m_nPresetNum   = 0;
   m_pPresetItems = NULL;
}
CIVCPPresetGroupInfo::~CIVCPPresetGroupInfo()
{
   ClearPresetItems();
}
int CIVCPPresetGroupInfo::ReadXML( CXMLIO* pIO )
{
   int nItemNum = 0;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Group" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "ID", &m_nID );
      xmlNode.Attribute( TYPE_ID( char ), "Name", &m_strName, sizeof( m_strName ) );
      xmlNode.Attribute( TYPE_ID( int ), "ItemNum", &nItemNum );

      ClearPresetItems();
      m_nPresetNum = 0;
      if( nItemNum > 0 ) {
         m_pPresetItems = new CIVCPPresetItemInfo[nItemNum];
         for( m_nPresetNum = 0; m_nPresetNum < nItemNum; m_nPresetNum++ ) {
            if( m_pPresetItems[m_nPresetNum].ReadXML( pIO ) != DONE )
               break;
         }
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPPresetGroupInfo::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Group" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "ID", &m_nID );
      xmlNode.Attribute( TYPE_ID( char ), "Name", &m_strName, sizeof( m_strName ) );
      xmlNode.Attribute( TYPE_ID( int ), "ItemNum", &m_nPresetNum );

      if( m_pPresetItems ) {
         for( int i = 0; i < m_nPresetNum; i++ ) {
            m_pPresetItems[i].WriteXML( pIO );
         }
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
void CIVCPPresetGroupInfo::ClearPresetItems()
{
   if( m_pPresetItems ) {
      delete[] m_pPresetItems;
      m_pPresetItems = NULL;
   }
}

///////////////////////////////////////////////////////////

CIVCPLivePresetMapInfo::CIVCPLivePresetMapInfo()
{
   m_nPacketID   = IVCP_ID_Live_PresetMapInfo;
   m_nCameraUID  = -1;
   m_nGroupNum   = 0;
   m_pGroupItems = NULL;
}
CIVCPLivePresetMapInfo::~CIVCPLivePresetMapInfo()
{
   ClearGroupItems();
}
int CIVCPLivePresetMapInfo::ReadXML( CXMLIO* pIO )
{
   int nGroupNum = 0;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PresetMap" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "FrmTime", &m_ftFrameTime );
      xmlNode.Attribute( TYPE_ID( int ), "GroupNum", &m_nGroupNum );
      ClearGroupItems();
      m_nGroupNum = 0;
      if( nGroupNum > 0 ) {
         m_pGroupItems = new CIVCPPresetGroupInfo[nGroupNum];
         for( m_nGroupNum = 0; m_nGroupNum < nGroupNum; m_nGroupNum++ ) {
            if( m_pGroupItems[m_nGroupNum].ReadXML( pIO ) != DONE )
               break;
         }
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPLivePresetMapInfo::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PresetMap" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "FrmTime", &m_ftFrameTime );
      xmlNode.Attribute( TYPE_ID( int ), "GroupNum", &m_nGroupNum );
      if( m_pGroupItems ) {
         for( int i = 0; i < m_nGroupNum; i++ ) {
            m_pGroupItems[i].WriteXML( pIO );
         }
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
void CIVCPLivePresetMapInfo::ClearGroupItems()
{
   if( m_pGroupItems ) {
      delete[] m_pGroupItems;
      m_pGroupItems = NULL;
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLiveSettingInfo
//
///////////////////////////////////////////////////////////////////////////////

CIVCPLiveSettingInfo::CIVCPLiveSettingInfo()
{
   m_nPacketID         = IVCP_ID_Live_SettingInfo;
   m_nCameraUID        = -1;
   m_nSettingMask      = 0;
   m_nSettingFlag      = 0;
   m_nWhiteBalanceMode = 0;
   m_nDayNightMode     = 0;
}
CIVCPLiveSettingInfo::~CIVCPLiveSettingInfo()
{
}
int CIVCPLiveSettingInfo::ReadXML( CXMLIO* pIO )
{
   int nPresetNum = 0;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Setting" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "FrmTime", &m_ftFrameTime );
      xmlNode.Attribute( TYPE_ID( xint64 ), "Mask", &m_nSettingMask );
      xmlNode.Attribute( TYPE_ID( xint64 ), "Flag", &m_nSettingFlag );
      xmlNode.Attribute( TYPE_ID( int ), "Stabil", &m_nStabilNum );
      xmlNode.Attribute( TYPE_ID( int ), "Defog", &m_nDefogLevel );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPLiveSettingInfo::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Setting" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "FrmTime", &m_ftFrameTime );
      xmlNode.Attribute( TYPE_ID( xint64 ), "Mask", &m_nSettingMask );
      xmlNode.Attribute( TYPE_ID( xint64 ), "Flag", &m_nSettingFlag );
      xmlNode.Attribute( TYPE_ID( int ), "Stabil", &m_nStabilNum );
      xmlNode.Attribute( TYPE_ID( int ), "Defog", &m_nDefogLevel );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPLiveSettingInfo::WriteToPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      aPacket.FreeBody();
   }
   FileIO buffBody;
   if( DONE == WritePacketBody( buffBody ) ) {
      aPacket.m_Header.m_nOptions = m_nCameraUID;
      aPacket.m_Header.m_nBodyLen = buffBody.GetLength();
      aPacket.m_pBody             = (char*)buffBody.Detach();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (INFO) CIVCPLiveStatus
//
///////////////////////////////////////////////////////////////////////////////

CIVCPLiveStatus::CIVCPLiveStatus()
{
   m_nPacketID  = IVCP_ID_Live_Status;
   m_nCameraUID = -1;
   m_nType      = 0;
   m_nParam     = 0;
}
CIVCPLiveStatus::~CIVCPLiveStatus()
{
}
int CIVCPLiveStatus::ReadXML( CXMLIO* pIO )
{
   int nPresetNum = 0;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "LiveStatus" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "FrmTime", &m_ftFrameTime );
      xmlNode.Attribute( TYPE_ID( int ), "Type", &m_nType );
      xmlNode.Attribute( TYPE_ID( int ), "Param", &m_nParam );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPLiveStatus::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "LiveStatus" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "FrmTime", &m_ftFrameTime );
      xmlNode.Attribute( TYPE_ID( int ), "Type", &m_nType );
      xmlNode.Attribute( TYPE_ID( int ), "Param", &m_nParam );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPPlayFrameObject
//
///////////////////////////////////////////////////////////////////////////////

CIVCPPlayFrameObject::CIVCPPlayFrameObject()
{
   m_nObjectID   = 0;
   m_nObjectType = 0;
   m_nEventFlag  = 0;
   m_nFlagLocked = 0;
}
CIVCPPlayFrameObject::~CIVCPPlayFrameObject()
{
}
int CIVCPPlayFrameObject::ReadXML( CXMLIO* pIO )
{
   int nEventNum = 0;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Object" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "ID", &m_nObjectID );
      xmlNode.Attribute( TYPE_ID( xint ), "Type", &m_nObjectType );
      xmlNode.Attribute( TYPE_ID( int ), "Event", &m_nEventFlag );
      xmlNode.Attribute( TYPE_ID( int ), "Locked", &m_nFlagLocked );
      xmlNode.Attribute( TYPE_ID( IBox2D ), "Box", &m_ObjectBox );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPPlayFrameObject::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Object" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "ID", &m_nObjectID );
      xmlNode.Attribute( TYPE_ID( xint ), "Type", &m_nObjectType );
      xmlNode.Attribute( TYPE_ID( int ), "Event", &m_nEventFlag );
      xmlNode.Attribute( TYPE_ID( int ), "Locked", &m_nFlagLocked );
      xmlNode.Attribute( TYPE_ID( IBox2D ), "Box", &m_ObjectBox );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

/////////////////////////////////////////////////////////////////////
// CIVCPLiveFrameInfo

CIVCPPlayFrameInfo::CIVCPPlayFrameInfo()
{
   m_nPacketID  = IVCP_ID_Play_FrameInfo;
   m_nCameraUID = -1;
   m_nStreamID  = 0;
   m_nObjectNum = 0;
   m_pObjects   = NULL;
}
CIVCPPlayFrameInfo::~CIVCPPlayFrameInfo()
{
   ClearObjects();
}
int CIVCPPlayFrameInfo::ReadXML( CXMLIO* pIO )
{
   int nObjectNum = 0;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Frame" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "StreamID", &m_nStreamID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "FrmTime", &m_ftFrameTime );
      xmlNode.Attribute( TYPE_ID( int ), "ObjNum", &nObjectNum );

      xmlNode.Attribute( TYPE_ID( xint ), "CamState", &m_nCameraState );
      xmlNode.Attribute( TYPE_ID( xint ), "PTZState", &m_nPTZState );
      xmlNode.Attribute( TYPE_ID( xint ), "PTZStateEx", &m_nPTZStateEx );
      xmlNode.Attribute( TYPE_ID( float ), "FrameRate", &m_fFrameRate );
      xmlNode.Attribute( TYPE_ID( int ), "Width", &m_nProcWidth );
      xmlNode.Attribute( TYPE_ID( int ), "Height", &m_nProcHeight );

      ClearObjects();
      m_nObjectNum = 0;
      if( nObjectNum > 0 ) {
         m_pObjects = new CIVCPPlayFrameObject[nObjectNum];
         for( m_nObjectNum = 0; m_nObjectNum < nObjectNum; m_nObjectNum++ ) {
            if( m_pObjects[m_nObjectNum].ReadXML( pIO ) != DONE )
               break;
         }
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPPlayFrameInfo::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Frame" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "StreamID", &m_nStreamID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "FrmTime", &m_ftFrameTime );
      xmlNode.Attribute( TYPE_ID( int ), "ObjNum", &m_nObjectNum );

      xmlNode.Attribute( TYPE_ID( xint ), "CamState", &m_nCameraState );
      xmlNode.Attribute( TYPE_ID( xint ), "PTZState", &m_nPTZState );
      xmlNode.Attribute( TYPE_ID( xint ), "PTZStateEx", &m_nPTZStateEx );
      xmlNode.Attribute( TYPE_ID( float ), "FrameRate", &m_fFrameRate );
      xmlNode.Attribute( TYPE_ID( int ), "Width", &m_nProcWidth );
      xmlNode.Attribute( TYPE_ID( int ), "Height", &m_nProcHeight );

      if( m_pObjects ) {
         for( int i = 0; i < m_nObjectNum; i++ ) {
            m_pObjects[i].WriteXML( pIO );
         }
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
void CIVCPPlayFrameInfo::ClearObjects()
{
   if( m_pObjects ) {
      delete[] m_pObjects;
      m_pObjects = NULL;
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPPlayPTZDataInfo
//
///////////////////////////////////////////////////////////////////////////////

CIVCPPlayPTZDataInfo::CIVCPPlayPTZDataInfo()
{
   m_nPacketID   = IVCP_ID_Play_PtzDataInfo;
   m_nCameraUID  = -1;
   m_fTiltAngle  = 0.0f; // Pan 각도 : 반시계 방향이 증가하는 방향이다. 단위(degree, 0~360도)
   m_fTiltAngle  = 0.0f; // Tilt 각도 : 아래로 향하는 방향이 증가하는 방향임. 단위(degree, 0~360도)
   m_fZoomFactor = 1.0f; // Zoom 값 : 현재 줌 위치를 나타냄. 18배줌 카메라이면 1~18사이의 값을 가짐.
   m_fFOV        = 60.0f; // FOV (Field Of View) : Pan 축으로의 화각을 나타냄. 단위(degree, 0~360도)
}
int CIVCPPlayPTZDataInfo::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PtzData" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "StreamID", &m_nStreamID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "Time", &m_ftFrameTime );
      xmlNode.Attribute( TYPE_ID( float ), "Pan", &m_fPanAngle );
      xmlNode.Attribute( TYPE_ID( float ), "Tilt", &m_fTiltAngle );
      xmlNode.Attribute( TYPE_ID( float ), "Zoom", &m_fZoomFactor );
      xmlNode.Attribute( TYPE_ID( float ), "FOV", &m_fFOV );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPPlayPTZDataInfo::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PtzData" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "StreamID", &m_nStreamID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "Time", &m_ftFrameTime );
      xmlNode.Attribute( TYPE_ID( float ), "Pan", &m_fPanAngle );
      xmlNode.Attribute( TYPE_ID( float ), "Tilt", &m_fTiltAngle );
      xmlNode.Attribute( TYPE_ID( float ), "Zoom", &m_fZoomFactor );
      xmlNode.Attribute( TYPE_ID( float ), "FOV", &m_fFOV );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (PLAY) CIVCPPlayPresetIdInfo
//
///////////////////////////////////////////////////////////////////////////////

CIVCPPlayPresetIdInfo::CIVCPPlayPresetIdInfo()
{
   m_nPacketID    = IVCP_ID_Play_PresetIdInfo;
   m_nCameraUID   = -1;
   m_nStreamID    = 0;
   m_nCurPresetID = 0;
}
int CIVCPPlayPresetIdInfo::ReadXML( CXMLIO* pIO )
{
   int nObjectNum = 0;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PresetID" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "StreamID", &m_nStreamID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "FrmTime", &m_ftFrameTime );
      xmlNode.Attribute( TYPE_ID( int ), "PresetID", &m_nCurPresetID );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPPlayPresetIdInfo::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PresetID" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "StreamID", &m_nStreamID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "FrmTime", &m_ftFrameTime );
      xmlNode.Attribute( TYPE_ID( int ), "PresetID", &m_nCurPresetID );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (PLAY) CIVCPPlayEventZoneInfo
//
///////////////////////////////////////////////////////////////////////////////

CIVCPPlayEventZoneInfo::CIVCPPlayEventZoneInfo()
{
   m_nPacketID   = IVCP_ID_Play_EventZoneInfo;
   m_nCameraUID  = -1;
   m_nStreamID   = 0;
   m_nZoneNum    = 0;
   m_pEventZones = NULL;
}
CIVCPPlayEventZoneInfo::~CIVCPPlayEventZoneInfo()
{
   ClearEventZones();
}
void CIVCPPlayEventZoneInfo::ClearEventZones()
{
   if( m_pEventZones ) {
      delete[] m_pEventZones;
      m_pEventZones = NULL;
   }
}
int CIVCPPlayEventZoneInfo::ReadXML( CXMLIO* pIO )
{
   int nZoneNum = 0;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "EventZoneInfo" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "StreamID", &m_nStreamID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "FrmTime", &m_ftFrameTime );
      xmlNode.Attribute( TYPE_ID( int ), "Width", &m_nWidth );
      xmlNode.Attribute( TYPE_ID( int ), "Height", &m_nHeight );
      xmlNode.Attribute( TYPE_ID( int ), "ZoneNum", &nZoneNum );
      ClearEventZones();
      m_nZoneNum = 0;
      if( nZoneNum > 0 ) {
         m_pEventZones = new CIVCPEventZone[nZoneNum];
         for( m_nZoneNum = 0; m_nZoneNum < nZoneNum; m_nZoneNum++ ) {
            if( m_pEventZones[m_nZoneNum].ReadXML( pIO ) != DONE )
               break;
         }
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPPlayEventZoneInfo::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "EventZoneInfo" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "StreamID", &m_nStreamID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "FrmTime", &m_ftFrameTime );
      xmlNode.Attribute( TYPE_ID( int ), "Width", &m_nWidth );
      xmlNode.Attribute( TYPE_ID( int ), "Height", &m_nHeight );
      xmlNode.Attribute( TYPE_ID( int ), "ZoneNum", &m_nZoneNum );
      if( m_pEventZones ) {
         for( int i = 0; i < m_nZoneNum; i++ ) {
            m_pEventZones[i].WriteXML( pIO );
         }
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (PLAY) CIVCPPlayEventZoneCountInfo
//
///////////////////////////////////////////////////////////////////////////////

CIVCPPlayEventZoneCountInfo::CIVCPPlayEventZoneCountInfo()
{
   m_nPacketID   = IVCP_ID_Play_EventZoneCount;
   m_nCameraUID  = -1;
   m_nZoneNum    = 0;
   m_pZoneCounts = NULL;
}
CIVCPPlayEventZoneCountInfo::~CIVCPPlayEventZoneCountInfo()
{
   ClearZoneCounts();
}
int CIVCPPlayEventZoneCountInfo::ReadXML( CXMLIO* pIO )
{
   int nZoneNum = 0;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "ZoneCnt" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "StreamID", &m_nStreamID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "FrmTime", &m_ftFrameTime );
      xmlNode.Attribute( TYPE_ID( int ), "ZoneNum", &nZoneNum );

      ClearZoneCounts();
      m_nZoneNum = 0;
      if( nZoneNum > 0 ) {
         m_pZoneCounts = new CIVCPEventZoneCount[nZoneNum];
         for( m_nZoneNum = 0; m_nZoneNum < nZoneNum; m_nZoneNum++ ) {
            if( m_pZoneCounts[m_nZoneNum].ReadXML( pIO ) != DONE )
               break;
         }
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPPlayEventZoneCountInfo::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "ZoneCnt" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "StreamID", &m_nStreamID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "FrmTime", &m_ftFrameTime );
      xmlNode.Attribute( TYPE_ID( int ), "ZoneNum", &m_nZoneNum );

      if( m_pZoneCounts ) {
         for( int i = 0; i < m_nZoneNum; i++ ) {
            m_pZoneCounts[i].WriteXML( pIO );
         }
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
void CIVCPPlayEventZoneCountInfo::ClearZoneCounts()
{
   if( m_pZoneCounts ) {
      delete[] m_pZoneCounts;
      m_pZoneCounts = NULL;
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// (PLAY) CIVCPPlayProgress
//
///////////////////////////////////////////////////////////////////////////////

CIVCPPlayProgress::CIVCPPlayProgress()
{
   m_nPacketID  = IVCP_ID_Play_Progress;
   m_nCameraUID = -1;
   m_nStreamID  = 0;
   m_nStep      = 0;
   m_nParam     = 0;
}
int CIVCPPlayProgress::ReadXML( CXMLIO* pIO )
{
   int nEvtZoneNum = 0;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Progress" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "SID", &m_nStreamID );
      xmlNode.Attribute( TYPE_ID( int ), "Step", &m_nStep );
      xmlNode.Attribute( TYPE_ID( int ), "Param", &m_nParam );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPPlayProgress::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Progress" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "UID", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "SID", &m_nStreamID );
      xmlNode.Attribute( TYPE_ID( int ), "Step", &m_nStep );
      xmlNode.Attribute( TYPE_ID( int ), "Param", &m_nParam );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchPlateReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPSearchFrameReq::CIVCPSearchFrameReq()
{
   m_nPacketID    = IVCP_ID_Search_FrameReq;
   m_nSearchID    = 0;
   m_nChannelFlag = 0;
}
int CIVCPSearchFrameReq::ReadXML( CXMLIO* pIO )
{
   int nEvtZoneNum = 0;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "SearchFrame" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "SrchID", &m_nSearchID );
      xmlNode.Attribute( TYPE_ID( UINT64 ), "ChFlag", &m_nChannelFlag );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "Start", &m_ftStart );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "End", &m_ftEnd );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPSearchFrameReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "SearchFrame" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "SrchID", &m_nSearchID );
      xmlNode.Attribute( TYPE_ID( UINT64 ), "ChFlag", &m_nChannelFlag );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "Start", &m_ftStart );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "End", &m_ftEnd );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchEventReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPSearchEventReq::CIVCPSearchEventReq()
{
   m_nPacketID    = IVCP_ID_Search_EventReq;
   m_nSearchID    = 0;
   m_nChannelFlag = 0;
   m_nEvtFlag     = 0;
   m_nObjFlag     = 0;
   m_nEvtZoneNum  = 0;
   m_pEvtZoneIDs  = NULL;
   m_bThumbnail   = FALSE;
}
CIVCPSearchEventReq::~CIVCPSearchEventReq()
{
   ClearEvtZoneIDs();
}
int CIVCPSearchEventReq::ReadXML( CXMLIO* pIO )
{
   int nEvtZoneNum = 0;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "SearchEvent" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "SrchID", &m_nSearchID );
      xmlNode.Attribute( TYPE_ID( UINT64 ), "ChFlag", &m_nChannelFlag );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "Start", &m_ftStart );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "End", &m_ftEnd );
      xmlNode.Attribute( TYPE_ID( int ), "EvtFlag", &m_nEvtFlag );
      xmlNode.Attribute( TYPE_ID( int ), "ObjFlag", &m_nObjFlag );
      xmlNode.Attribute( TYPE_ID( int ), "EZNum", &m_nEvtZoneNum );
      ClearEvtZoneIDs();
      if( m_nEvtZoneNum > 0 ) {
         m_pEvtZoneIDs = new int[m_nEvtZoneNum];
         xmlNode.Attribute( TYPE_ID( int ), "EZIDs", &m_pEvtZoneIDs, m_nEvtZoneNum );
      }
      xmlNode.Attribute( TYPE_ID( BOOL ), "Thumbnail", &m_bThumbnail );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPSearchEventReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "SearchEvent" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "SrchID", &m_nSearchID );
      xmlNode.Attribute( TYPE_ID( UINT64 ), "ChFlag", &m_nChannelFlag );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "Start", &m_ftStart );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "End", &m_ftEnd );
      xmlNode.Attribute( TYPE_ID( int ), "EvtFlag", &m_nEvtFlag );
      xmlNode.Attribute( TYPE_ID( int ), "ObjFlag", &m_nObjFlag );
      xmlNode.Attribute( TYPE_ID( int ), "EZNum", &m_nEvtZoneNum );
      if( m_nEvtZoneNum > 0 ) {
         xmlNode.Attribute( TYPE_ID( int ), "EZIDs", &m_pEvtZoneIDs, m_nEvtZoneNum );
      }
      xmlNode.Attribute( TYPE_ID( BOOL ), "Thumbnail", &m_bThumbnail );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
void CIVCPSearchEventReq::ClearEvtZoneIDs()
{
   if( m_pEvtZoneIDs ) {
      delete[] m_pEvtZoneIDs;
      m_pEvtZoneIDs = NULL;
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchObjectReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPSearchColorFilter::CIVCPSearchColorFilter()
{
   m_nColorType        = 0;
   m_nColorMode        = 0;
   m_nColorSensibility = 0;
   m_nColorNum         = 0;
   for( int i = 0; i < SEARCH_SELECT_COLOR_MAX; i++ ) {
      m_crColor[i]  = 0; // 대표 색상
      m_nPercent[i] = 0; // 점유률
   }
   m_nWidth = m_nHeight = m_nDepth = 0;
   m_nHistogramLen                 = 0;
   m_pHistogramData                = NULL;
}
CIVCPSearchColorFilter::~CIVCPSearchColorFilter()
{
   if( m_pHistogramData ) delete( (byte*)m_pHistogramData );
}
void CIVCPSearchColorFilter::SetHistogramData( void* pBuffer, int nLen, int nWidth, int nHeight, int nDepth )
{
   // 먼저 삭제한다.
   if( m_pHistogramData ) delete( (byte*)m_pHistogramData );
   m_pHistogramData = NULL;
   m_nHistogramLen  = 0;
   m_nWidth = m_nHeight = m_nDepth = 0;

   if( pBuffer == NULL ) return;

   // 히스토그램을 복사한다.
   if( ( nLen > 0 ) && ( pBuffer != NULL ) ) {
      m_nWidth  = nWidth;
      m_nHeight = nHeight;
      m_nDepth  = nDepth;

      m_nHistogramLen  = nLen;
      m_pHistogramData = new byte[m_nHistogramLen];
      memcpy( m_pHistogramData, pBuffer, m_nHistogramLen );
   }
}
BOOL CIVCPSearchColorFilter::GetHistogramData( void** pBuffer, int& nLen, int& nWidth, int& nHeight, int& nDepth )
{
   if( m_nHistogramLen == 0 ) return FALSE;
   //if ((m_nWidth == 0) || (m_nHeight == 0) || (m_nDepth == 0)) return FALSE;

   nWidth  = m_nWidth;
   nHeight = m_nHeight;
   nDepth  = m_nDepth;

   nLen     = m_nHistogramLen;
   *pBuffer = ( (void*)m_pHistogramData );
   return TRUE;
}
int CIVCPSearchColorFilter::ReadXML( CXMLIO* pIO )
{
   std::string strKey;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "ColorFilter" ) ) {
      xmlNode.Attribute( TYPE_ID( uint ), "ColorType", &m_nColorType );
      xmlNode.Attribute( TYPE_ID( uint ), "ColorMode", &m_nColorMode );
      xmlNode.Attribute( TYPE_ID( uint ), "Sensibility", &m_nColorSensibility );
      xmlNode.Attribute( TYPE_ID( uint ), "ColorNum", &m_nColorNum );
      for( int i = 0; i < m_nColorNum; i++ ) {
         strKey = sformat( "CR%02d", i );
         xmlNode.Attribute( TYPE_ID( uint ), strKey.c_str(), &m_crColor[i] );
         strKey = sformat( "PE%02d", i );
         xmlNode.Attribute( TYPE_ID( uint ), strKey.c_str(), &m_nPercent[i] );
      }
      xmlNode.Attribute( TYPE_ID( int ), "HistoLen", &m_nHistogramLen );
      if( m_nHistogramLen > 0 ) {
         xmlNode.Attribute( TYPE_ID( ushort ), "Width", &m_nWidth );
         xmlNode.Attribute( TYPE_ID( ushort ), "Height", &m_nHeight );
         xmlNode.Attribute( TYPE_ID( ushort ), "Depth", &m_nDepth );
         if( m_pHistogramData ) delete( (byte*)m_pHistogramData );
         m_pHistogramData = new byte[m_nHistogramLen];
         xmlNode.Attribute( TYPE_ID( XBYTE ), "HistoData", m_pHistogramData, m_nHistogramLen );
      }
      xmlNode.End();
   } else
      return FALSE;
   return TRUE;
}
int CIVCPSearchColorFilter::WriteXML( CXMLIO* pIO )
{
   std::string strKey;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "ColorFilter" ) ) {
      xmlNode.Attribute( TYPE_ID( uint ), "ColorType", &m_nColorType );
      xmlNode.Attribute( TYPE_ID( uint ), "ColorMode", &m_nColorMode );
      xmlNode.Attribute( TYPE_ID( uint ), "Sensibility", &m_nColorSensibility );
      xmlNode.Attribute( TYPE_ID( uint ), "ColorNum", &m_nColorNum );
      for( int i = 0; i < m_nColorNum; i++ ) {
         strKey = sformat( "CR%02d", i );
         xmlNode.Attribute( TYPE_ID( uint ), strKey.c_str(), &m_crColor[i] );
         strKey = sformat( "PE%02d", i );
         xmlNode.Attribute( TYPE_ID( uint ), strKey.c_str(), &m_nPercent[i] );
      }
      xmlNode.Attribute( TYPE_ID( int ), "HistoLen", &m_nHistogramLen );
      if( m_nHistogramLen > 0 ) {
         xmlNode.Attribute( TYPE_ID( ushort ), "Width", &m_nWidth );
         xmlNode.Attribute( TYPE_ID( ushort ), "Height", &m_nHeight );
         xmlNode.Attribute( TYPE_ID( ushort ), "Depth", &m_nDepth );
         xmlNode.Attribute( TYPE_ID( XBYTE ), "HistoData", m_pHistogramData, m_nHistogramLen );
      }
      xmlNode.End();
   } else
      return FALSE;
   return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

CIVCPSearchObjectFilter::CIVCPSearchObjectFilter()
{
   m_nOptions = FILTER_ENABLE | USE_WIDTH | USE_HEIGHT | USE_AREA | USE_ASPECT_RATIO | USE_AXIS_LENGTH_RATIO | USE_MAJOR_AXIS_LENGTH | USE_MINOR_AXIS_LENGTH | USE_NORMALIZED_SPEED | USE_SPEED | USE_REAL_DISTANCE;

   m_fMinWidth               = 0.0f;
   m_fMaxWidth               = 1000.0f;
   m_fMinHeight              = 0.0f;
   m_fMaxHeight              = 1000.0f;
   m_fMinArea                = 0.0f;
   m_fMaxArea                = 1000000.0f;
   m_fMinAspectRatio         = 0.1f;
   m_fMaxAspectRatio         = 10.0f;
   m_fMinAxisLengthRatio     = 1.0f;
   m_fMaxAxisLengthRatio     = 10.0f;
   m_fMinMajorAxisLength     = 0.0f;
   m_fMaxMajorAxisLength     = 1000.0f;
   m_fMinMinorAxisLength     = 0.0f;
   m_fMaxMinorAxisLength     = 1000.0f;
   m_fMinNorSpeed            = 0.0f;
   m_fMaxNorSpeed            = 1000.0f;
   m_fMinSpeed               = 0.0f;
   m_fMaxSpeed               = 1000.0f;
   m_fMinRealArea            = 0.0f;
   m_fMaxRealArea            = 1000000.0f;
   m_fMinRealDistance        = 0.0f;
   m_fMaxRealDistance        = 10000.0f;
   m_fMinRealWidth           = 0.0f;
   m_fMaxRealWidth           = 1000.0f;
   m_fMinRealHeight          = 0.0f;
   m_fMaxRealHeight          = 1000.0f;
   m_fMinRealMajorAxisLength = 0.0f;
   m_fMaxRealMajorAxisLength = 1000.0f;
   m_fMinRealMinorAxisLength = 0.0f;
   m_fMaxRealMinorAxisLength = 1000.0f;
   m_fMinRealSpeed           = 0.0f;
   m_fMaxRealSpeed           = 1000.0f;
}
int CIVCPSearchObjectFilter::ReadXML( CXMLIO* pIO )
{
   static CIVCPSearchObjectFilter dv; // defalut value (기본값과 같으면 xml로 부터 읽거나 저장하지 않음)

   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "ObjectFilter" ) ) {
      xmlNode.Attribute( TYPE_ID( xint ), "Options", &m_nOptions, &dv.m_nOptions );
      xmlNode.Attribute( TYPE_ID( float ), "MinWidth", &m_fMinWidth, &dv.m_fMinWidth );
      xmlNode.Attribute( TYPE_ID( float ), "MaxWidth", &m_fMaxWidth, &dv.m_fMaxWidth );
      xmlNode.Attribute( TYPE_ID( float ), "MinHeight", &m_fMinHeight, &dv.m_fMinHeight );
      xmlNode.Attribute( TYPE_ID( float ), "MaxHeight", &m_fMaxHeight, &dv.m_fMaxHeight );
      xmlNode.Attribute( TYPE_ID( float ), "MinArea", &m_fMinArea, &dv.m_fMinArea );
      xmlNode.Attribute( TYPE_ID( float ), "MaxArea", &m_fMaxArea, &dv.m_fMaxArea );
      xmlNode.Attribute( TYPE_ID( float ), "MinAspectRatio", &m_fMinAspectRatio, &dv.m_fMinAspectRatio );
      xmlNode.Attribute( TYPE_ID( float ), "MaxAspectRatio", &m_fMaxAspectRatio, &dv.m_fMaxAspectRatio );
      xmlNode.Attribute( TYPE_ID( float ), "MinAxisLengthRatio", &m_fMinAxisLengthRatio, &dv.m_fMinAxisLengthRatio );
      xmlNode.Attribute( TYPE_ID( float ), "MaxAxisLengthRatio", &m_fMaxAxisLengthRatio, &dv.m_fMaxAxisLengthRatio );
      xmlNode.Attribute( TYPE_ID( float ), "MinMajorAxisLength", &m_fMinMajorAxisLength, &dv.m_fMinMajorAxisLength );
      xmlNode.Attribute( TYPE_ID( float ), "MaxMajorAxisLength", &m_fMaxMajorAxisLength, &dv.m_fMaxMajorAxisLength );
      xmlNode.Attribute( TYPE_ID( float ), "MinMinorAxisLength", &m_fMinMinorAxisLength, &dv.m_fMinMinorAxisLength );
      xmlNode.Attribute( TYPE_ID( float ), "MaxMinorAxisLength", &m_fMaxMinorAxisLength, &dv.m_fMaxMinorAxisLength );
      xmlNode.Attribute( TYPE_ID( float ), "MinNorSpeed", &m_fMinNorSpeed, &dv.m_fMinNorSpeed );
      xmlNode.Attribute( TYPE_ID( float ), "MaxNorSpeed", &m_fMaxNorSpeed, &dv.m_fMaxNorSpeed );
      xmlNode.Attribute( TYPE_ID( float ), "MinSpeed", &m_fMinSpeed, &dv.m_fMinSpeed );
      xmlNode.Attribute( TYPE_ID( float ), "MaxSpeed", &m_fMaxSpeed, &dv.m_fMaxSpeed );
      xmlNode.Attribute( TYPE_ID( float ), "MinRealArea", &m_fMinRealArea, &dv.m_fMinRealArea );
      xmlNode.Attribute( TYPE_ID( float ), "MaxRealArea", &m_fMaxRealArea, &dv.m_fMaxRealArea );
      xmlNode.Attribute( TYPE_ID( float ), "MinRealDistance", &m_fMinRealDistance, &dv.m_fMinRealDistance );
      xmlNode.Attribute( TYPE_ID( float ), "MaxRealDistance", &m_fMaxRealDistance, &dv.m_fMaxRealDistance );
      xmlNode.Attribute( TYPE_ID( float ), "MinRealWidth", &m_fMinRealWidth, &dv.m_fMinRealWidth );
      xmlNode.Attribute( TYPE_ID( float ), "MaxRealWidth", &m_fMaxRealWidth, &dv.m_fMaxRealWidth );
      xmlNode.Attribute( TYPE_ID( float ), "MinRealHeight", &m_fMinRealHeight, &dv.m_fMinRealHeight );
      xmlNode.Attribute( TYPE_ID( float ), "MaxRealHeight", &m_fMaxRealHeight, &dv.m_fMaxRealHeight );
      xmlNode.Attribute( TYPE_ID( float ), "MinRealMajorAxisLength", &m_fMinRealMajorAxisLength, &dv.m_fMinRealMajorAxisLength );
      xmlNode.Attribute( TYPE_ID( float ), "MaxRealMajorAxisLength", &m_fMaxRealMajorAxisLength, &dv.m_fMaxRealMajorAxisLength );
      xmlNode.Attribute( TYPE_ID( float ), "MinRealMinorAxisLength", &m_fMinRealMinorAxisLength, &dv.m_fMinRealMinorAxisLength );
      xmlNode.Attribute( TYPE_ID( float ), "MaxRealMinorAxisLength", &m_fMaxRealMinorAxisLength, &dv.m_fMaxRealMinorAxisLength );
      xmlNode.Attribute( TYPE_ID( float ), "MinRealSpeed", &m_fMinRealSpeed, &dv.m_fMinRealSpeed );
      xmlNode.Attribute( TYPE_ID( float ), "MaxRealSpeed", &m_fMaxRealSpeed, &dv.m_fMaxRealSpeed );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPSearchObjectFilter::WriteXML( CXMLIO* pIO )
{
   static CIVCPSearchObjectFilter dv; // defalut value (기본값과 같으면 xml로 부터 읽거나 저장하지 않음.

   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "ObjectFilter" ) ) {
      xmlNode.Attribute( TYPE_ID( xint ), "Options", &m_nOptions, &dv.m_nOptions );
      xmlNode.Attribute( TYPE_ID( float ), "MinWidth", &m_fMinWidth, &dv.m_fMinWidth );
      xmlNode.Attribute( TYPE_ID( float ), "MaxWidth", &m_fMaxWidth, &dv.m_fMaxWidth );
      xmlNode.Attribute( TYPE_ID( float ), "MinHeight", &m_fMinHeight, &dv.m_fMinHeight );
      xmlNode.Attribute( TYPE_ID( float ), "MaxHeight", &m_fMaxHeight, &dv.m_fMaxHeight );
      xmlNode.Attribute( TYPE_ID( float ), "MinArea", &m_fMinArea, &dv.m_fMinArea );
      xmlNode.Attribute( TYPE_ID( float ), "MaxArea", &m_fMaxArea, &dv.m_fMaxArea );
      xmlNode.Attribute( TYPE_ID( float ), "MinAspectRatio", &m_fMinAspectRatio, &dv.m_fMinAspectRatio );
      xmlNode.Attribute( TYPE_ID( float ), "MaxAspectRatio", &m_fMaxAspectRatio, &dv.m_fMaxAspectRatio );
      xmlNode.Attribute( TYPE_ID( float ), "MinAxisLengthRatio", &m_fMinAxisLengthRatio, &dv.m_fMinAxisLengthRatio );
      xmlNode.Attribute( TYPE_ID( float ), "MaxAxisLengthRatio", &m_fMaxAxisLengthRatio, &dv.m_fMaxAxisLengthRatio );
      xmlNode.Attribute( TYPE_ID( float ), "MinMajorAxisLength", &m_fMinMajorAxisLength, &dv.m_fMinMajorAxisLength );
      xmlNode.Attribute( TYPE_ID( float ), "MaxMajorAxisLength", &m_fMaxMajorAxisLength, &dv.m_fMaxMajorAxisLength );
      xmlNode.Attribute( TYPE_ID( float ), "MinMinorAxisLength", &m_fMinMinorAxisLength, &dv.m_fMinMinorAxisLength );
      xmlNode.Attribute( TYPE_ID( float ), "MaxMinorAxisLength", &m_fMaxMinorAxisLength, &dv.m_fMaxMinorAxisLength );
      xmlNode.Attribute( TYPE_ID( float ), "MinNorSpeed", &m_fMinNorSpeed, &dv.m_fMinNorSpeed );
      xmlNode.Attribute( TYPE_ID( float ), "MaxNorSpeed", &m_fMaxNorSpeed, &dv.m_fMaxNorSpeed );
      xmlNode.Attribute( TYPE_ID( float ), "MinSpeed", &m_fMinSpeed, &dv.m_fMinSpeed );
      xmlNode.Attribute( TYPE_ID( float ), "MaxSpeed", &m_fMaxSpeed, &dv.m_fMaxSpeed );
      xmlNode.Attribute( TYPE_ID( float ), "MinRealArea", &m_fMinRealArea, &dv.m_fMinRealArea );
      xmlNode.Attribute( TYPE_ID( float ), "MaxRealArea", &m_fMaxRealArea, &dv.m_fMaxRealArea );
      xmlNode.Attribute( TYPE_ID( float ), "MinRealDistance", &m_fMinRealDistance, &dv.m_fMinRealDistance );
      xmlNode.Attribute( TYPE_ID( float ), "MaxRealDistance", &m_fMaxRealDistance, &dv.m_fMaxRealDistance );
      xmlNode.Attribute( TYPE_ID( float ), "MinRealWidth", &m_fMinRealWidth, &dv.m_fMinRealWidth );
      xmlNode.Attribute( TYPE_ID( float ), "MaxRealWidth", &m_fMaxRealWidth, &dv.m_fMaxRealWidth );
      xmlNode.Attribute( TYPE_ID( float ), "MinRealHeight", &m_fMinRealHeight, &dv.m_fMinRealHeight );
      xmlNode.Attribute( TYPE_ID( float ), "MaxRealHeight", &m_fMaxRealHeight, &dv.m_fMaxRealHeight );
      xmlNode.Attribute( TYPE_ID( float ), "MinRealMajorAxisLength", &m_fMinRealMajorAxisLength, &dv.m_fMinRealMajorAxisLength );
      xmlNode.Attribute( TYPE_ID( float ), "MaxRealMajorAxisLength", &m_fMaxRealMajorAxisLength, &dv.m_fMaxRealMajorAxisLength );
      xmlNode.Attribute( TYPE_ID( float ), "MinRealMinorAxisLength", &m_fMinRealMinorAxisLength, &dv.m_fMinRealMinorAxisLength );
      xmlNode.Attribute( TYPE_ID( float ), "MaxRealMinorAxisLength", &m_fMaxRealMinorAxisLength, &dv.m_fMaxRealMinorAxisLength );
      xmlNode.Attribute( TYPE_ID( float ), "MinRealSpeed", &m_fMinRealSpeed, &dv.m_fMinRealSpeed );
      xmlNode.Attribute( TYPE_ID( float ), "MaxRealSpeed", &m_fMaxRealSpeed, &dv.m_fMaxRealSpeed );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////

CIVCPSearchObjectReq::CIVCPSearchObjectReq()
{
   m_nPacketID    = IVCP_ID_Search_ObjectReq;
   m_nObjID       = 0; // (mkjang-140926)
   m_nSearchID    = 0;
   m_nChannelFlag = 0;
   m_nCondFlag    = 0; // 검색 조건에 대한 플래그
   m_nObjFlag     = 0;
   m_bThumbnail   = FALSE;
}
int CIVCPSearchObjectReq::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "SearchObject" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "SrchID", &m_nSearchID );
      xmlNode.Attribute( TYPE_ID( int ), "ObjID", &m_nObjID ); // (mkjang-140926)
      xmlNode.Attribute( TYPE_ID( UINT64 ), "ChFlag", &m_nChannelFlag );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "Start", &m_ftStart );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "End", &m_ftEnd );
      xmlNode.Attribute( TYPE_ID( int ), "CondFlag", &m_nCondFlag );
      xmlNode.Attribute( TYPE_ID( int ), "ObjFlag", &m_nObjFlag );
      m_ColorFilter.ReadXML( pIO );
      m_ObjectFilter.ReadXML( pIO );
      xmlNode.Attribute( TYPE_ID( BOOL ), "Thumbnail", &m_bThumbnail );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPSearchObjectReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "SearchObject" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "SrchID", &m_nSearchID );
      xmlNode.Attribute( TYPE_ID( int ), "ObjID", &m_nObjID ); // (mkjang-140926)
      xmlNode.Attribute( TYPE_ID( UINT64 ), "ChFlag", &m_nChannelFlag );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "Start", &m_ftStart );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "End", &m_ftEnd );
      xmlNode.Attribute( TYPE_ID( int ), "CondFlag", &m_nCondFlag );
      xmlNode.Attribute( TYPE_ID( int ), "ObjFlag", &m_nObjFlag );
      m_ColorFilter.WriteXML( pIO );
      m_ObjectFilter.WriteXML( pIO );
      xmlNode.Attribute( TYPE_ID( BOOL ), "Thumbnail", &m_bThumbnail );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchPlateReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPSearchPlateReq::CIVCPSearchPlateReq()
{
   m_nPacketID    = IVCP_ID_Search_PlateReq;
   m_nSearchID    = 0;
   m_nChannelFlag = 0;
}
int CIVCPSearchPlateReq::ReadXML( CXMLIO* pIO )
{
   int nEvtZoneNum = 0;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "SearchPlate" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "SrchID", &m_nSearchID );
      xmlNode.Attribute( TYPE_ID( UINT64 ), "ChFlag", &m_nChannelFlag );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "Start", &m_ftStart );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "End", &m_ftEnd );
      xmlNode.Attribute( TYPE_ID( char ), "PlateNum", &m_strPlateNumber, sizeof( m_strPlateNumber ) );
      xmlNode.Attribute( TYPE_ID( BOOL ), "Thumbnail", &m_bThumbnail );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPSearchPlateReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "SearchPlate" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "SrchID", &m_nSearchID );
      xmlNode.Attribute( TYPE_ID( UINT64 ), "ChFlag", &m_nChannelFlag );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "Start", &m_ftStart );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "End", &m_ftEnd );
      xmlNode.Attribute( TYPE_ID( char ), "PlateNum", &m_strPlateNumber, sizeof( m_strPlateNumber ) );
      xmlNode.Attribute( TYPE_ID( BOOL ), "Thumbnail", &m_bThumbnail );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchFaceReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPSearchFaceReq::CIVCPSearchFaceReq()
{
   m_nPacketID    = IVCP_ID_Search_FaceReq;
   m_nSearchID    = 0;
   m_nChannelFlag = 0;
}
int CIVCPSearchFaceReq::ReadXML( CXMLIO* pIO )
{
   int nEvtZoneNum = 0;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "SearchFace" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "SrchID", &m_nSearchID );
      xmlNode.Attribute( TYPE_ID( UINT64 ), "ChFlag", &m_nChannelFlag );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "Start", &m_ftStart );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "End", &m_ftEnd );
      xmlNode.Attribute( TYPE_ID( char ), "Name", &m_strName, sizeof( m_strName ) );
      xmlNode.Attribute( TYPE_ID( BOOL ), "Thumbnail", &m_bThumbnail );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPSearchFaceReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "SearchFace" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "SrchID", &m_nSearchID );
      xmlNode.Attribute( TYPE_ID( UINT64 ), "ChFlag", &m_nChannelFlag );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "Start", &m_ftStart );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "End", &m_ftEnd );
      xmlNode.Attribute( TYPE_ID( char ), "Name", &m_strName, sizeof( m_strName ) );
      xmlNode.Attribute( TYPE_ID( BOOL ), "Thumbnail", &m_bThumbnail );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchLogReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPSearchLogReq::CIVCPSearchLogReq()
{
   m_nPacketID    = IVCP_ID_Search_LogReq;
   m_nSearchID    = 0;
   m_nChannelFlag = 0;
}
int CIVCPSearchLogReq::ReadXML( CXMLIO* pIO )
{
   int nEvtZoneNum = 0;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "SearchLog" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "SrchID", &m_nSearchID );
      xmlNode.Attribute( TYPE_ID( UINT64 ), "ChFlag", &m_nChannelFlag );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "Start", &m_ftStart );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "End", &m_ftEnd );
      xmlNode.Attribute( TYPE_ID( char ), "Keyword", &m_strKeyword, sizeof( m_strKeyword ) );
      xmlNode.Attribute( TYPE_ID( int ), "Option", &m_nOption );
      xmlNode.Attribute( TYPE_ID( int ), "EvtFlag", &m_nEvtFlag );
      xmlNode.Attribute( TYPE_ID( int ), "ObjFlag", &m_nObjFlag );
      xmlNode.Attribute( TYPE_ID( BOOL ), "Thumbnail", &m_bThumbnail );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPSearchLogReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "SearchLog" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "SrchID", &m_nSearchID );
      xmlNode.Attribute( TYPE_ID( UINT64 ), "ChFlag", &m_nChannelFlag );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "Start", &m_ftStart );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "End", &m_ftEnd );
      xmlNode.Attribute( TYPE_ID( char ), "Keyword", &m_strKeyword, sizeof( m_strKeyword ) );
      xmlNode.Attribute( TYPE_ID( int ), "Option", &m_nOption );
      xmlNode.Attribute( TYPE_ID( int ), "EvtFlag", &m_nEvtFlag );
      xmlNode.Attribute( TYPE_ID( int ), "ObjFlag", &m_nObjFlag );
      xmlNode.Attribute( TYPE_ID( BOOL ), "Thumbnail", &m_bThumbnail );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchStopReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPSearchStopReq::CIVCPSearchStopReq()
{
   m_nPacketID = IVCP_ID_Search_StopReq;
   m_nSearchID = 0;
}
int CIVCPSearchStopReq::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "SearchStop" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "SrchID", &m_nSearchID );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPSearchStopReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "SearchStop" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "SrchID", &m_nSearchID );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchResultRes
//
///////////////////////////////////////////////////////////////////////////////

CIVCPSearchResultRes::CIVCPSearchResultRes()
{
   m_nPacketID = 0;
   m_nSearchID = 0;
   m_nResult   = 0;
}
int CIVCPSearchResultRes::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "SearchRet" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "SrchID", &m_nSearchID );
      xmlNode.Attribute( TYPE_ID( int ), "Result", &m_nResult );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPSearchResultRes::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "SearchRet" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "SrchID", &m_nSearchID );
      xmlNode.Attribute( TYPE_ID( int ), "Result", &m_nResult );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchProgress
//
///////////////////////////////////////////////////////////////////////////////

CIVCPSearchProgress::CIVCPSearchProgress()
{
   m_nPacketID = IVCP_ID_Search_Progress;
   m_nSearchID = 0;
   m_nStep     = 0;
   m_nParam    = 0;
}
int CIVCPSearchProgress::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "SearchProgress" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "SrchID", &m_nSearchID );
      xmlNode.Attribute( TYPE_ID( int ), "Step", &m_nStep );
      xmlNode.Attribute( TYPE_ID( int ), "Param", &m_nParam );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPSearchProgress::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "SearchProgress" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "SrchID", &m_nSearchID );
      xmlNode.Attribute( TYPE_ID( int ), "Step", &m_nStep );
      xmlNode.Attribute( TYPE_ID( int ), "Param", &m_nParam );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

   /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef __SUPPORT_IVCP_FACE
////////////////////////////////////////////////////////////////////////////////
// Face Image Data
////////////////////////////////////////////////////////////////////////////////
CFaceImageData::CFaceImageData()
{
   m_fAngle = 0;
}

////////////////////////////////////////////////////////////////////////////////
// Face Image List
////////////////////////////////////////////////////////////////////////////////
CFaceImageList::CFaceImageList()
{
}

CFaceImageList::~CFaceImageList()
{
   DeleteAll();
}

void CFaceImageList::Delete( POSITION pos )
{
   CFaceImageData* pNode = (CFaceImageData*)GetAt( pos );
   if( pNode ) {
      if( pNode->m_image_buffer )
         delete[] pNode->m_image_buffer;
      delete pNode;
      RemoveAt( pos );
   }
}

void CFaceImageList::DeleteAll()
{
   if( GetCount() <= 0 ) return;

   POSITION pre, pos = GetHeadPosition();
   CFaceImageData* pNode;
   while( pos ) {
      pre   = pos;
      pNode = NULL;
      pNode = (CFaceImageData*)GetNext( pos );
      if( pNode ) {
         if( pNode->m_image_buffer )
            delete[] pNode->m_image_buffer;

         delete pNode;
         pNode = NULL;
         RemoveAt( pre );
      }
   }

   RemoveAll();
}

CFaceImageData* CFaceImageList::GetData( int iIndex )
{
   int iCount = GetListCount();
   if( iCount <= 0 )
      return NULL;
   CFaceImageData* pNode = NULL;
   BOOL bFindFlag        = FALSE;
   POSITION pos          = GetHeadPosition();
   while( pos ) {
      pNode = NULL;
      pNode = (CFaceImageData*)GetNext( pos );
      if( pNode->m_iIndex == iIndex ) {
         bFindFlag = TRUE;
         break;
      }
   }

   if( bFindFlag )
      return pNode;
   else
      return NULL;
}

int CFaceImageList::GetListCount()
{
   return GetCount();
}

////////////////////////////////////////////////////////////////////////////////
// Search Data
////////////////////////////////////////////////////////////////////////////////
void CSearchData::SetData( _UserDataInfo* pInfo, int iIndex )
{
   m_iIndex      = iIndex;
   m_iChannel_no = pInfo->m_iChannel_no;
   m_strUserID   = pInfo->m_szUserID;
   m_strUserName = pInfo->m_szUserName;
   m_iAge        = pInfo->m_iAge;
   m_iGender     = pInfo->m_iGender;
   //m_iGlasses = pInfo->m_iGlasses;
   m_fRate          = pInfo->m_fRate;
   m_strBeloging_ID = pInfo->m_strBeloging_ID;
   m_strPosition_ID = pInfo->m_strPosition_ID;
   m_strTEL         = pInfo->m_strTEL;
   m_strHP          = pInfo->m_strHP;
   m_iBlacklist_chk = pInfo->m_iBlacklist_chk;

   m_iWidth     = pInfo->m_iWidth;
   m_iHeight    = pInfo->m_iHeight;
   m_iTotalSize = pInfo->iImageSize;

   if( pInfo->ImageBuffer ) {
      m_image_buffer = new byte[m_iTotalSize];
      memcpy( m_image_buffer, pInfo->ImageBuffer, sizeof( byte ) * m_iTotalSize );
   } else
      m_image_buffer = NULL;

   m_strVrifi_ID = pInfo->m_szVrifi_ID;
   // jmlee 2014 08 21 추가
   m_iEvtStatus  = pInfo->m_iEvtStatus;
   m_iEvt_D      = pInfo->m_iEvt_ID;
   m_iFrameIndex = pInfo->m_iFrameIndex;
   m_iFaceID     = pInfo->m_nFaceID;
}

////////////////////////////////////////////////////////////////////////////////
// Search List
////////////////////////////////////////////////////////////////////////////////
CSearchList::CSearchList()
{
}

CSearchList::~CSearchList()
{
   DeleteAll();
}

void CSearchList::Delete( POSITION pos )
{
   CSearchData* pNode = (CSearchData*)GetAt( pos );
   if( pNode ) {
      if( pNode->m_image_buffer )
         delete[] pNode->m_image_buffer;
      delete pNode;
      RemoveAt( pos );
   }
}

void CSearchList::DeleteAll()
{
   if( GetCount() <= 0 ) return;

   POSITION pre, pos = GetHeadPosition();
   CSearchData* pNode;
   while( pos ) {
      pre   = pos;
      pNode = NULL;
      pNode = (CSearchData*)GetNext( pos );
      if( pNode ) {
         if( pNode->m_image_buffer ) {
            delete[] pNode->m_image_buffer;
            pNode->m_image_buffer = NULL; // jmlee 2014 08 21 추가
         }
         delete pNode;
         pNode = NULL;
         RemoveAt( pre );
      }
   }

   RemoveAll();
}

BOOL CSearchList::AddData( _UserDataInfo* pInfo, int iIndex )
{
   CSearchData* pNode;

   pNode = new CSearchData;

   pNode->SetData( pInfo, iIndex );

   AddTail( pNode );

   delete pInfo;
   pInfo = NULL;

   return TRUE;
}

CSearchData* CSearchList::GetData( const std::string& strUderID )
{
   CSearchData* pNode = NULL;

   BOOL bFindFlag = FALSE;
   POSITION pos   = GetHeadPosition();
   while( pos ) {
      pNode = NULL;
      pNode = (CSearchData*)GetNext( pos );
      if( pNode ) {
         if( pNode->m_strUserID == strUderID ) {
            bFindFlag = TRUE;
            break;
         }
      }
   }

   if( bFindFlag )
      return pNode;
   else
      return NULL;
}

CSearchData* CSearchList::GetData( int iIndex )
{
   CSearchData* pNode = NULL;

   BOOL bFindFlag = FALSE;
   POSITION pos   = GetHeadPosition();
   while( pos ) {
      pNode = NULL;
      pNode = (CSearchData*)GetNext( pos );
      if( pNode ) {
         if( pNode->m_iIndex == iIndex ) {
            bFindFlag = TRUE;
            break;
         }
      }
   }

   if( bFindFlag )
      return pNode;
   else
      return NULL;
}

int CSearchList::GetListCount()
{
   return GetCount();
}

////////////////////////////////////////////////////////////////////////////////
// CFaceUserVerificationReqData
////////////////////////////////////////////////////////////////////////////////
void CFaceUserVerificationReqData::SetData( _FaceUserVerificationReqInfo* pInfo, int iIndex )
{
   m_iIndex     = iIndex;
   m_iWidth     = pInfo->m_iWidth;
   m_iHeight    = pInfo->m_iHeight;
   m_iImageSize = pInfo->m_iImageSize;

   strcpy( m_szVerifiID, pInfo->m_szVerifiID );

   if( pInfo->m_pImage != NULL ) {
      m_pImage = new byte[m_iImageSize];
      memcpy( m_pImage, (byte*)pInfo->m_pImage, sizeof( byte ) * m_iImageSize );
   } else {
      m_pImage = NULL;
   }

   m_pTemplate   = NULL;
   m_pUserInfo   = NULL;
   m_Gender      = pInfo->m_Gender;
   m_Glasses     = pInfo->m_Glasses;
   m_iChannel_No = pInfo->m_Channel_no;
}

////////////////////////////////////////////////////////////////////////////////
// CFaceUserVerificationReqList
////////////////////////////////////////////////////////////////////////////////

CCriticalSection FaceUserVerificationReqList_cs;

CFaceUserVerificationReqList::CFaceUserVerificationReqList()
{
}

CFaceUserVerificationReqList::~CFaceUserVerificationReqList()
{
   DeleteAll();
}

void CFaceUserVerificationReqList::Delete( int iIndex )
{

   FaceUserVerificationReqList_cs.Lock();
   POSITION pre, pos = GetHeadPosition();
   CFaceUserVerificationReqData* pNode;
   while( pos ) {
      pre   = pos;
      pNode = NULL;
      pNode = (CFaceUserVerificationReqData*)GetNext( pos );
      if( pNode ) {
         if( pNode->m_iIndex == iIndex ) {
            if( pNode->m_pImage )
               delete pNode->m_pImage;

            if( pNode->m_pUserInfo )
               delete pNode->m_pUserInfo;

            if( pNode->m_pTemplate )
               delete pNode->m_pTemplate;

            delete pNode;
            pNode = NULL;
            RemoveAt( pre );

            break;
         }
      }
   }
   FaceUserVerificationReqList_cs.Unlock();
}

void CFaceUserVerificationReqList::DeleteAll()
{
   FaceUserVerificationReqList_cs.Lock();

   if( GetCount() <= 0 ) {
      FaceUserVerificationReqList_cs.Unlock();
      return;
   }

   POSITION pre, pos = GetHeadPosition();
   CFaceUserVerificationReqData* pNode;
   while( pos ) {
      pre   = pos;
      pNode = NULL;
      pNode = (CFaceUserVerificationReqData*)GetNext( pos );
      if( pNode ) {
         if( pNode->m_pImage ) {
            delete pNode->m_pImage;
            pNode->m_pImage = NULL;
         }

         if( pNode->m_pUserInfo ) {
            if( pNode->m_pUserInfo->ImageBuffer ) {
               delete[] pNode->m_pUserInfo->ImageBuffer;
               pNode->m_pUserInfo->ImageBuffer = NULL;
            }

            if( pNode->m_pUserInfo->SubImagebuffer ) {
               delete[] pNode->m_pUserInfo->SubImagebuffer;
               pNode->m_pUserInfo->SubImagebuffer = NULL;
            }

            if( pNode->m_pUserInfo->SubImagebuffer_Svr ) {
               delete[] pNode->m_pUserInfo->SubImagebuffer_Svr;
               pNode->m_pUserInfo->SubImagebuffer_Svr = NULL;
            }

            delete pNode->m_pUserInfo;
            pNode->m_pUserInfo = NULL;
         }

         if( pNode->m_pTemplate ) {
            delete pNode->m_pTemplate;
            pNode->m_pTemplate = NULL;
         }

         delete pNode;
         pNode = NULL;
         RemoveAt( pre );
      }
   }

   RemoveAll();
   FaceUserVerificationReqList_cs.Unlock();
}

BOOL CFaceUserVerificationReqList::AddData( _FaceUserVerificationReqInfo* pInfo, int iIndex )
{
   FaceUserVerificationReqList_cs.Lock();
   CFaceUserVerificationReqData* pNode = NULL;
   pNode                               = new CFaceUserVerificationReqData;
   if( pNode ) {
      pNode->SetData( pInfo, iIndex );
      AddTail( pNode );
   }

   if( pInfo->m_pImage )
      delete pInfo->m_pImage;
   delete pInfo;
   pInfo = NULL;

   FaceUserVerificationReqList_cs.Unlock();

   return TRUE;
}

BOOL CFaceUserVerificationReqList::AddData( CFaceUserVerificationReqData* pInfo )
{
   AddTail( pInfo );
   return TRUE;
}

CFaceUserVerificationReqData* CFaceUserVerificationReqList::GetData( int iIndex )
{
   CFaceUserVerificationReqData* pNode = NULL;

   BOOL bFindFlag = FALSE;
   POSITION pos   = GetHeadPosition();
   while( pos ) {
      pNode = NULL;
      pNode = (CFaceUserVerificationReqData*)GetNext( pos );
      if( pNode ) {
         if( pNode->m_iIndex == iIndex ) {
            bFindFlag = TRUE;
            break;
         }
      }
   }

   if( bFindFlag )
      return pNode;
   else
      return NULL;
}

int CFaceUserVerificationReqList::GetListCount()
{
   FaceUserVerificationReqList_cs.Lock();
   POSITION pre, pos = GetHeadPosition();
   CFaceUserVerificationReqData* pNode;

   int iSize = 0;

   while( pos ) {
      pre   = pos;
      pNode = NULL;
      pNode = (CFaceUserVerificationReqData*)GetNext( pos );
      if( pNode ) {
         iSize++;
      }
   }
   FaceUserVerificationReqList_cs.Unlock();
   return iSize;
}

int CFaceUserVerificationReqList::GetImageTotalSize()
{
   if( GetCount() <= 0 ) return 0;

   POSITION pre, pos = GetHeadPosition();
   CFaceUserVerificationReqData* pNode;

   int iSize = 0;

   while( pos ) {
      pre   = pos;
      pNode = NULL;
      pNode = (CFaceUserVerificationReqData*)GetNext( pos );
      if( pNode ) {
         iSize += pNode->m_iImageSize;
      }
   }

   return iSize;
}

///////////////////////////////////////////////////////////////////////////////
//
// CIVCPFaceUserReq : 얼굴 데이터 등록/검색/편집/삭제 요청
//
///////////////////////////////////////////////////////////////////////////////

CIVCPFaceUserReq::CIVCPFaceUserReq()
{
   m_nPacketID = IVCP_ID_Data_FaceUserData;
   memset( m_szUserID, 0x00, sizeof( m_szUserID ) );
   memset( m_szUserName, 0x00, sizeof( m_szUserName ) );
   memset( m_strBeloging_ID, 0x00, sizeof( m_strBeloging_ID ) );
   memset( m_strPosition_ID, 0x00, sizeof( m_strPosition_ID ) );
   memset( m_strTEL, 0x00, sizeof( m_strTEL ) );
   memset( m_strHP, 0x00, sizeof( m_strHP ) );

   m_iAge = 0, m_iGender = 0;

   m_bImage_Attach = FALSE;
   strcpy( m_szImageFormat, "JPG" );
   m_iWidth     = 0;
   m_iHeight    = 0;
   m_iTotalSize = 0;

   memset( m_szRequestType, 0x00, sizeof( m_szRequestType ) );

   m_pBodyBuffer = NULL;
   m_pFaceImage  = NULL;

   m_pInfo = NULL;

   m_imageToken = NULL;

   m_iBlacklist_chk = -1;

   m_nImageNum   = 0;
   m_nXMLDataLen = 0;
}

CIVCPFaceUserReq::CIVCPFaceUserReq( _UserDataInfo* pInfo )
{
   m_nPacketID = IVCP_ID_Data_FaceUserData;

   memset( m_szUserID, 0x00, sizeof( m_szUserID ) );
   memset( m_szUserName, 0x00, sizeof( m_szUserName ) );
   memset( m_strBeloging_ID, 0x00, sizeof( m_strBeloging_ID ) );
   memset( m_strPosition_ID, 0x00, sizeof( m_strPosition_ID ) );
   memset( m_strTEL, 0x00, sizeof( m_strTEL ) );
   memset( m_strHP, 0x00, sizeof( m_strHP ) );

   strcpy( m_szUserID, pInfo->m_szUserID );
   strcpy( m_szUserName, pInfo->m_szUserName );
   m_iAge = pInfo->m_iAge, m_iGender = pInfo->m_iGender;
   strcpy( m_strBeloging_ID, pInfo->m_strBeloging_ID );
   strcpy( m_strPosition_ID, pInfo->m_strPosition_ID );
   strcpy( m_strTEL, pInfo->m_strTEL );
   strcpy( m_strHP, pInfo->m_strHP );

   m_bImage_Attach = pInfo->m_bImage_Attach;
   strcpy( m_szImageFormat, pInfo->m_szImageFormat );
   m_iWidth     = pInfo->m_iWidth;
   m_iHeight    = pInfo->m_iHeight;
   m_iTotalSize = pInfo->iSubImageSize;

   strcpy( m_szRequestType, pInfo->m_szRequestType );

   m_pBodyBuffer = NULL;
   m_pFaceImage  = NULL;

   m_imageToken = NULL;

   m_iBlacklist_chk = pInfo->m_iBlacklist_chk;

   m_pFaceList = NULL;

   m_nImageNum   = 0;
   m_nXMLDataLen = 0;
}

CIVCPFaceUserReq::~CIVCPFaceUserReq()
{
   if( m_pBodyBuffer )
      free( m_pBodyBuffer );

   if( m_pFaceImage )
      free( m_pFaceImage );

   if( m_imageToken ) {
      delete m_imageToken;
      m_imageToken = NULL;
   }

   m_pFaceList = NULL;
}

int CIVCPFaceUserReq::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Request" ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "Request_Type", m_szRequestType, sizeof( m_szRequestType ) );
      xmlNode.Attribute( TYPE_ID( char ), "Request_ID", m_szRequest_ID, sizeof( m_szRequest_ID ) );
      xmlNode.Attribute( TYPE_ID( char ), "Logon_ID", m_szRequest_LogonID, sizeof( m_szRequest_LogonID ) );
      xmlNode.Attribute( TYPE_ID( int ), "Image_Attach", &m_bImage_Attach );
      xmlNode.Attribute( TYPE_ID( int ), "Image_Num", &m_nImageNum );

      if( strcmp( m_szRequestType, "E" ) == 0 ) {
         if( m_nImageNum <= 0 )
            return ( 1 );

         if( m_imageToken ) {
            delete m_imageToken;
            m_imageToken = NULL;
         }

         m_imageToken = new int[m_nImageNum];
      }

      SubReadXML( pIO );
      xmlNode.End();
      return ( DONE );
   }

   return ( 1 );
}

int CIVCPFaceUserReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Request" ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "Request_Type", m_szRequestType, sizeof( m_szRequestType ) );
      xmlNode.Attribute( TYPE_ID( char ), "Request_ID", m_szRequest_ID, sizeof( m_szRequest_ID ) );
      xmlNode.Attribute( TYPE_ID( char ), "Request_Time", m_szRequest_Time, sizeof( m_szRequest_Time ) );
      xmlNode.Attribute( TYPE_ID( char ), "Logon_ID", m_szRequest_LogonID, sizeof( m_szRequest_LogonID ) );
      xmlNode.Attribute( TYPE_ID( int ), "Image_Attach", &m_bImage_Attach );
      xmlNode.Attribute( TYPE_ID( int ), "Image_Num", &m_nImageNum );
      SubWriteXML( pIO );
      xmlNode.End();
      return ( DONE );
   }

   return ( 1 );
}

int CIVCPFaceUserReq::SubWriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "User" ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "UserID", m_szUserID, sizeof( m_szUserID ) );
      xmlNode.Attribute( TYPE_ID( char ), "UserName", m_szUserName, sizeof( m_szUserName ) );
      xmlNode.Attribute( TYPE_ID( char ), "Beloging_ID", m_strBeloging_ID, sizeof( m_strBeloging_ID ) );
      xmlNode.Attribute( TYPE_ID( char ), "Position_ID", m_strPosition_ID, sizeof( m_strPosition_ID ) );
      xmlNode.Attribute( TYPE_ID( char ), "TEL", m_strTEL, sizeof( m_strTEL ) );
      xmlNode.Attribute( TYPE_ID( char ), "HP", m_strHP, sizeof( m_strHP ) );
      xmlNode.Attribute( TYPE_ID( int ), "Age", &m_iAge );
      xmlNode.Attribute( TYPE_ID( int ), "Gender", &m_iGender );
      xmlNode.Attribute( TYPE_ID( int ), "Blacklist_Check", &m_iBlacklist_chk );
      xmlNode.End();
   }

   if( !m_pFaceList ) {
      return ( 1 );
   }

   int iCount = m_pFaceList->GetListCount();

   if( m_iTotalSize > 0 && iCount > 0 ) {
      std::string strImage;
      int iNum;
      for( iNum = 0; iNum < iCount; iNum++ ) {
         CFaceImageData* pData = NULL;
         pData                 = (CFaceImageData*)m_pFaceList->GetData( iNum );

         //CFaceImageData* pData = NULL;

         //BOOL bFlag = FALSE;
         //POSITION pos = m_pFaceList->GetHeadPosition (   );
         //while (pos)
         //{
         // pData = NULL;
         // pData = (CFaceImageData*)m_pFaceList->GetNext(pos);
         // if(pData->m_iIndex == iNum)
         // {
         //    bFlag = TRUE;
         //    break;
         // }
         //}

         if( !pData ) {
            strImage = sformat( "Image%d", iNum );
            if( xmlNode.Start( strImage.c_str() ) ) {
               xmlNode.Attribute( TYPE_ID( char ), "Format", m_szImageFormat, sizeof( m_szImageFormat ) );
               char szSize[20];
               sprintf( szSize, "(%d,%d)", m_iWidth, m_iHeight );
               xmlNode.Attribute( TYPE_ID( char ), "Size", szSize, sizeof( szSize ) );
               int iLength = 0;
               xmlNode.Attribute( TYPE_ID( int ), "Length", &iLength );
               xmlNode.End();
            }
         } else {
            strImage = sformat( "Image%d", iNum );
            if( xmlNode.Start( strImage.c_str() ) ) {
               xmlNode.Attribute( TYPE_ID( char ), "Format", m_szImageFormat, sizeof( m_szImageFormat ) );
               char szSize[20];
               sprintf( szSize, "(%d,%d)", m_iWidth, m_iHeight );
               xmlNode.Attribute( TYPE_ID( char ), "Size", szSize, sizeof( szSize ) );
               int iLength = pData->m_iImageSize;
               xmlNode.Attribute( TYPE_ID( int ), "Length", &iLength );
               xmlNode.End();
            }
         }
      }

      return ( DONE );
   }

   return ( 1 );
}

int CIVCPFaceUserReq::SubReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "User" ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "UserID", m_szUserID, sizeof( m_szUserID ) );
      xmlNode.Attribute( TYPE_ID( char ), "UserName", m_szUserName, sizeof( m_szUserName ) );
      xmlNode.Attribute( TYPE_ID( char ), "Beloging_ID", m_strBeloging_ID, sizeof( m_strBeloging_ID ) );
      xmlNode.Attribute( TYPE_ID( char ), "Position_ID", m_strPosition_ID, sizeof( m_strPosition_ID ) );
      xmlNode.Attribute( TYPE_ID( char ), "TEL", m_strTEL, sizeof( m_strTEL ) );
      xmlNode.Attribute( TYPE_ID( char ), "HP", m_strHP, sizeof( m_strHP ) );

      xmlNode.Attribute( TYPE_ID( int ), "Age", &m_iAge );
      xmlNode.Attribute( TYPE_ID( int ), "Gender", &m_iGender );
      xmlNode.Attribute( TYPE_ID( int ), "Blacklist_Check", &m_iBlacklist_chk );
      xmlNode.End();
   }

   if( m_iTotalSize > 0 && m_nImageNum > 0 ) {
      std::string strImage;
      int iNum;
      for( iNum = 0; iNum < m_nImageNum; iNum++ ) {
         strImage = sformat( "Image%d", iNum );
         if( xmlNode.Start( strImage.c_str() ) ) {
            xmlNode.Attribute( TYPE_ID( char ), "Format", m_szImageFormat, sizeof( m_szImageFormat ) );
            char szSize[20];
            std::string strData;
            xmlNode.Attribute( TYPE_ID( char ), "Size", szSize, sizeof( szSize ) );
            strData      = szSize;
            int iFindLen = strData.find( "," );
            if( iFindLen != std::string::npos ) {
               m_iWidth  = ( atoi )( strData.assign( 1, iFindLen - 1 ).c_str() );
               m_iHeight = ( atoi )( strData.assign( iFindLen + 1, strData.length() - 1 ).c_str() );
            }

            int iLength;
            xmlNode.Attribute( TYPE_ID( int ), "Length", &iLength );
            m_imageToken[iNum] = iLength;

            xmlNode.End();
         }
      }

      return ( DONE );
   }

   return ( 1 );
}

int CIVCPFaceUserReq::WriteToPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      aPacket.FreeBody();
   }
   if( m_pBodyBuffer ) {
      aPacket.m_Header.m_nOptions    = 0;
      aPacket.m_Header.m_nXMLDataLen = m_nXMLDataLen;
      aPacket.m_Header.m_nBodyLen    = m_nBodyLen;
      aPacket.m_pBody                = (char*)malloc( m_nBodyLen );
      memcpy( aPacket.m_pBody, m_pBodyBuffer, m_nBodyLen );
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPFaceUserReq::ReadFromPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      m_nXMLDataLen   = aPacket.m_Header.m_nXMLDataLen;
      m_nBodyLen      = aPacket.m_Header.m_nBodyLen;
      m_pBodyBuffer   = aPacket.m_pBody;
      aPacket.m_pBody = NULL;

      m_iTotalSize = m_nXMLDataLen;

      ParseBodyBuffer(); // 바로 파싱한다.

      if( m_nXMLDataLen > 0 )
         ParseImageBuffer(); // 바로 이미지 파싱

      return ( DONE );
   }
   return ( 1 );
}

void CIVCPFaceUserReq::ParseBodyBuffer()
{
   FileIO buff;
   buff.Attach( (byte*)m_pBodyBuffer, m_nBodyLen - m_nXMLDataLen );
   // buff.Attach ((byte*)m_pBodyBuffer, m_nDataLen);

   CXMLIO xmlIO( &buff, XMLIO_Read );
   if( DONE != ReadXML( &xmlIO ) ) {
      buff.Detach();
      return;
   } else {
      m_pInfo = new _UserDataInfo;
      strcpy( m_pInfo->m_szRequestType, m_szRequestType );
      strcpy( m_pInfo->m_szUserID, m_szUserID );
      strcpy( m_pInfo->m_szUserName, m_szUserName );
      //strcpy(m_pInfo->m_strBeloging_ID, m_strBeloging_ID);
      //strcpy(m_pInfo->m_strPosition_ID, m_strPosition_ID);
      //strcpy(m_pInfo->m_strTEL, m_strTEL);
      //strcpy(m_pInfo->m_strHP, m_strHP);
      m_pInfo->m_iAge    = m_iAge;
      m_pInfo->m_iGender = m_iGender;
   }
}

void CIVCPFaceUserReq::CreateBodybuffer( byte* pImageBuff, int nImageLen )
{
   if( m_pBodyBuffer ) {
      free( m_pBodyBuffer );
      m_pBodyBuffer = NULL;
   }

   m_nXMLDataLen = 0;
   m_nBodyLen    = 0;

   FileIO buff;
   CXMLIO xmlIO( &buff, XMLIO_Write );
   if( WriteXML( &xmlIO ) ) return;

   m_nXMLDataLen = nImageLen;
   m_nBodyLen    = buff.GetLength() + nImageLen;

   // 먼저 버퍼를 만든다.
   m_pBodyBuffer = (char*)malloc( buff.GetLength() + nImageLen );
   if( m_pBodyBuffer ) {
      memcpy( m_pBodyBuffer, (byte*)buff, buff.GetLength() );

      if( nImageLen )
         memcpy( m_pBodyBuffer + buff.GetLength(), pImageBuff, nImageLen );
   }
}

void CIVCPFaceUserReq::CreateBodybuffer( CFaceImageList* pList, byte* pImageBuff, int nImageLen )
{
   if( m_pBodyBuffer ) {
      free( m_pBodyBuffer );
      m_pBodyBuffer = NULL;
   }

   m_nXMLDataLen = 0;
   m_nBodyLen    = 0;

   m_pFaceList = pList;

   FileIO buff;
   CXMLIO xmlIO( &buff, XMLIO_Write );
   if( WriteXML( &xmlIO ) ) return;

   m_nXMLDataLen = nImageLen;
   m_nBodyLen    = buff.GetLength() + nImageLen;

   // 먼저 버퍼를 만든다.
   m_pBodyBuffer = (char*)malloc( buff.GetLength() + nImageLen );
   if( m_pBodyBuffer ) {
      memcpy( m_pBodyBuffer, (byte*)buff, buff.GetLength() );

      if( nImageLen )
         memcpy( m_pBodyBuffer + buff.GetLength(), pImageBuff, nImageLen );
   }
}

void CIVCPFaceUserReq::ParseImageBuffer()
{
   if( m_pFaceImage != NULL ) {
      free( m_pFaceImage );
      m_pFaceImage = NULL;
   }

   m_pFaceImage = (byte*)malloc( sizeof( byte ) * m_nXMLDataLen );
   memcpy( m_pFaceImage, m_pBodyBuffer + ( m_nBodyLen - m_nXMLDataLen ), m_nXMLDataLen );

   //delete pImageBuffer;
}

void CIVCPFaceUserReq::DeleteBodyBuffer()
{
   SAFE_FREE( m_pBodyBuffer );
}

///////////////////////////////////////////////////////////////////////////////
//
// CIVCPFaceUserRes : 얼굴 데이터 등록/검색/편집/삭제 응답
//
///////////////////////////////////////////////////////////////////////////////

CIVCPFaceUserRes::CIVCPFaceUserRes()
{
   m_nPacketID = IVCP_ID_Data_FaceUserData;

   m_bImage_Attach = FALSE;
   strcpy( m_szImageFormat, "JPG" );
   memset( m_szResponseType, 0x00, sizeof( m_szResponseType ) );
   m_iTotSearchNum   = 0;
   m_iTotalImageSize = 0;
   m_iFaceImageSize  = 0;

   m_pBodyBuffer = NULL;

   m_pSearchList = NULL;

   m_image_buffer = NULL;
   m_nXMLDataLen  = 0;
}

CIVCPFaceUserRes::~CIVCPFaceUserRes()
{
   if( m_pBodyBuffer ) free( m_pBodyBuffer );
}

int CIVCPFaceUserRes::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Response" ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "Response_Type", m_szResponseType, sizeof( m_szResponseType ) );
      xmlNode.Attribute( TYPE_ID( char ), "Response_ID", m_szResponse_ID, sizeof( m_szResponse_ID ) );
      xmlNode.Attribute( TYPE_ID( char ), "Logon_ID", m_szResponse_LogonID, sizeof( m_szResponse_LogonID ) );
      xmlNode.Attribute( TYPE_ID( char ), "Result", m_szResult, sizeof( m_szResult ) );
      if( strcmp( m_szResult, "OK" ) == 0 ) {
         if( strcmp( m_szResponseType, "S" ) == 0 ) // 검색
            SubReadXML( pIO );
         //else if(m_strResponseType == "D") // 삭제
         // xmlNode.Attribute (TYPE_ID (CStringA), "UserID", &m_strUserID);
         //else if(m_strResponseType == "U") // 갱신
         //{
         // if(m_strResult == "OK")
         // {
         //    CXMLNode xmlSubNode (pIO);
         //    if (xmlSubNode.Start ("User"))
         //    {
         //       xmlSubNode.Attribute (TYPE_ID (CStringA), "UserID", &m_strUserID);
         //       xmlSubNode.Attribute (TYPE_ID (CStringA), "UserName",&m_strUserName);
         //       xmlSubNode.Attribute (TYPE_ID (int), "Age", &m_iAge);
         //       xmlSubNode.Attribute (TYPE_ID (int), "Gender", &m_iGender);
         //       //xmlSubNode.Attribute (TYPE_ID (CStringA), "Beloging_ID", &m_strBeloging_ID);
         //       //xmlSubNode.Attribute (TYPE_ID (CStringA), "TEL", &m_strTEL);
         //       //xmlSubNode.Attribute (TYPE_ID (CStringA), "HP", &m_strHP);
         //       xmlSubNode.End ();
         //    }
         // }
         //}
      }

      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPFaceUserRes::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Response" ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "Response_Type", m_szResponseType, sizeof( m_szResponseType ) );
      xmlNode.Attribute( TYPE_ID( char ), "Response_ID", m_szResponse_ID, sizeof( m_szResponse_ID ) );
      xmlNode.Attribute( TYPE_ID( char ), "Logon_ID", m_szResponse_LogonID, sizeof( m_szResponse_LogonID ) );
      xmlNode.Attribute( TYPE_ID( char ), "Result", m_szResult, sizeof( m_szResult ) );
      if( strcmp( m_szResponseType, "S" ) == 0 ) {
         SubWriteXML( pIO );
      }
      //else if(m_strResponseType == "D")
      //{
      // xmlNode.Attribute (TYPE_ID (CStringA), "UserID", &m_strUserID);
      //}
      //else if(m_strResponseType == "U")
      //{
      // if(m_strResult == "OK")
      // {
      //    CXMLNode xmlSubNode (pIO);
      //    if (xmlSubNode.Start ("User"))
      //    {
      //       xmlSubNode.Attribute (TYPE_ID (CStringA), "UserID", &m_strUserID);
      //       xmlSubNode.Attribute (TYPE_ID (CStringA), "UserName",&m_strUserName);
      //       xmlSubNode.Attribute (TYPE_ID (int), "Age", &m_iAge);
      //       xmlSubNode.Attribute (TYPE_ID (int), "Gender", &m_iGender);
      //       //xmlSubNode.Attribute (TYPE_ID (CStringA), "Beloging_ID", &m_strBeloging_ID);
      //       //xmlSubNode.Attribute (TYPE_ID (CStringA), "TEL", &m_strTEL);
      //       //xmlSubNode.Attribute (TYPE_ID (CStringA), "HP", &m_strHP);
      //       xmlSubNode.End ();
      //    }
      // }
      //}

      xmlNode.End();
      return ( DONE );
   }

   return ( 1 );
}

int CIVCPFaceUserRes::SubWriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );

   if( xmlNode.Start( "Search" ) ) {
      m_iTotSearchNum   = m_pSearchList->GetCount();
      m_iTotalImageSize = m_pSearchList->m_iTotalImageSize;

      xmlNode.Attribute( TYPE_ID( int ), "Total_Num", &m_iTotSearchNum );
      xmlNode.Attribute( TYPE_ID( int ), "TotalImageSize", &m_iTotalImageSize );

      std::string strNodeNum;
      for( int i = 0; i < m_iTotSearchNum; i++ ) {
         CXMLNode xmlSubNode( pIO );
         strNodeNum = sformat( "Num%d", i );
         if( xmlSubNode.Start( strNodeNum.c_str() ) ) {
            SubWriteXML_TO( pIO, i );
            xmlSubNode.End();
         }
      }
      xmlNode.End();
   }

   return ( 1 );
}

int CIVCPFaceUserRes::SubWriteXML_TO( CXMLIO* pIO, int iIndex )
{
   CSearchData* pNode = NULL;
   pNode              = m_pSearchList->GetData( iIndex );
   if( pNode == NULL )
      return ( 1 );

   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "User" ) ) {
      char szUserID[100];
      strcpy( szUserID, pNode->m_strUserID.c_str() );
      char szUserName[100];
      strcpy( szUserName, pNode->m_strUserName.c_str() );
      int iAge = pNode->m_iAge, iGender = pNode->m_iGender;

      xmlNode.Attribute( TYPE_ID( char ), "UserID", szUserID, sizeof( szUserID ) );
      xmlNode.Attribute( TYPE_ID( char ), "UserName", szUserName, sizeof( szUserName ) );
      xmlNode.Attribute( TYPE_ID( int ), "Age", &iAge );
      xmlNode.Attribute( TYPE_ID( int ), "Gender", &iGender );
      xmlNode.Attribute( TYPE_ID( string ), "Beloging_ID", &pNode->m_strBeloging_ID );
      xmlNode.Attribute( TYPE_ID( string ), "Position_ID", &pNode->m_strPosition_ID );
      xmlNode.Attribute( TYPE_ID( string ), "TEL", &pNode->m_strTEL );
      xmlNode.Attribute( TYPE_ID( string ), "HP", &pNode->m_strHP );
      xmlNode.End();
   }

   if( xmlNode.Start( "Image" ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "Format", m_szImageFormat, sizeof( m_szImageFormat ) );
      char szSize[20];
      sprintf( szSize, "(%d,%d)", pNode->m_iWidth, pNode->m_iHeight );
      xmlNode.Attribute( TYPE_ID( char ), "Size", szSize, sizeof( szSize ) );
      xmlNode.Attribute( TYPE_ID( int ), "Length", &pNode->m_iTotalSize );
      xmlNode.End();
   }

   return ( DONE );
}

int CIVCPFaceUserRes::SubReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );

   if( xmlNode.Start( "Search" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Total_Num", &m_iTotSearchNum );
      xmlNode.Attribute( TYPE_ID( int ), "TotalImageSize", &m_iTotalImageSize );

      if( !m_pSearchList )
         m_pSearchList = new CSearchList;
      else {
         m_pSearchList->DeleteAll();
         delete m_pSearchList;
         m_pSearchList = new CSearchList;
      }

      std::string strNodeNum;
      for( int i = 0; i < m_iTotSearchNum; i++ ) {
         CXMLNode xmlSubNode( pIO );
         strNodeNum = sformat( "Num%d", i );
         if( xmlSubNode.Start( strNodeNum.c_str() ) ) {
            SubReadXML_TO( pIO, i );
            xmlSubNode.End();
         }
      }
      xmlNode.End();
   }

   return ( 1 );
}

int CIVCPFaceUserRes::SubReadXML_TO( CXMLIO* pIO, int iIndex )
{
   _UserDataInfo* pInfo = new _UserDataInfo;

   pInfo->ImageBuffer   = NULL;
   pInfo->iSubImageSize = 0;

   pInfo->m_Index = iIndex;

   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "User" ) ) {
      memset( pInfo->m_szUserID, 0x00, sizeof( pInfo->m_szUserID ) );
      xmlNode.Attribute( TYPE_ID( char ), "UserID", pInfo->m_szUserID, sizeof( pInfo->m_szUserID ) );

      memset( pInfo->m_szUserName, 0x00, sizeof( pInfo->m_szUserName ) );
      xmlNode.Attribute( TYPE_ID( char ), "UserName", pInfo->m_szUserName, sizeof( pInfo->m_szUserName ) );

      memset( pInfo->m_strBeloging_ID, 0x00, sizeof( pInfo->m_strBeloging_ID ) );
      xmlNode.Attribute( TYPE_ID( char ), "Beloging_ID", pInfo->m_strBeloging_ID, sizeof( pInfo->m_strBeloging_ID ) );

      memset( pInfo->m_strPosition_ID, 0x00, sizeof( pInfo->m_strPosition_ID ) );
      xmlNode.Attribute( TYPE_ID( char ), "Position_ID", pInfo->m_strPosition_ID, sizeof( pInfo->m_strPosition_ID ) );

      memset( pInfo->m_strTEL, 0x00, sizeof( pInfo->m_strTEL ) );
      xmlNode.Attribute( TYPE_ID( char ), "TEL", pInfo->m_strTEL, sizeof( pInfo->m_strTEL ) );

      memset( pInfo->m_strHP, 0x00, sizeof( pInfo->m_strHP ) );
      xmlNode.Attribute( TYPE_ID( char ), "HP", pInfo->m_strHP, sizeof( pInfo->m_strHP ) );

      xmlNode.Attribute( TYPE_ID( int ), "Age", &pInfo->m_iAge );
      xmlNode.Attribute( TYPE_ID( int ), "Gender", &pInfo->m_iGender );
      xmlNode.Attribute( TYPE_ID( int ), "Blacklist_Check", &pInfo->m_iBlacklist_chk );

      xmlNode.End();
   } else {
      delete pInfo;
      pInfo = NULL;
      return ( 1 );
   }

   if( xmlNode.Start( "Image" ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "Format", pInfo->m_szImageFormat, sizeof( pInfo->m_szImageFormat ) );
      char szSize[20];
      xmlNode.Attribute( TYPE_ID( char ), "Size", szSize, sizeof( szSize ) );
      std::string strData = szSize;
      int iFindLen        = strData.find( "," );
      if( iFindLen != std::string::npos ) {
         pInfo->m_iWidth  = ( atoi )( strData.assign( 1, iFindLen - 1 ).c_str() );
         pInfo->m_iHeight = ( atoi )( strData.assign( iFindLen + 1, strData.length() - 1 ).c_str() );
      }
      xmlNode.Attribute( TYPE_ID( int ), "Length", &pInfo->iImageSize );
      xmlNode.End();
   } else {
      delete pInfo;
      pInfo = NULL;
      return ( 1 );
   }

   m_pSearchList->AddData( pInfo, iIndex );

   return ( DONE );
}

int CIVCPFaceUserRes::WriteToPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      aPacket.FreeBody();
   }
   if( m_pBodyBuffer ) {
      aPacket.m_Header.m_nOptions    = 0;
      aPacket.m_Header.m_nXMLDataLen = m_nXMLDataLen;
      aPacket.m_Header.m_nBodyLen    = m_nBodyLen;
      aPacket.m_pBody                = m_pBodyBuffer;
      m_pBodyBuffer                  = NULL;
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPFaceUserRes::ReadFromPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      m_nXMLDataLen   = aPacket.m_Header.m_nXMLDataLen;
      m_nBodyLen      = aPacket.m_Header.m_nBodyLen;
      m_pBodyBuffer   = aPacket.m_pBody;
      aPacket.m_pBody = NULL;

      ParseBodyBuffer(); // 바로 파싱한다.

      if( m_nXMLDataLen > 0 )
         ParseImageBuffer(); // 바로 이미지 파싱
      return ( DONE );
   }
   return ( 1 );
}

void CIVCPFaceUserRes::ParseBodyBuffer()
{
   FileIO buff;
   buff.Attach( (byte*)m_pBodyBuffer, m_nBodyLen - m_nXMLDataLen );
   CXMLIO xmlIO( &buff, XMLIO_Read );

   if( DONE != ReadXML( &xmlIO ) ) {
      buff.Detach();
      return;
   }
}

void CIVCPFaceUserRes::GetImageBuffer( byte* image_buffer )
{
   if( m_pSearchList == NULL )
      return;

   m_iTotSearchNum = m_pSearchList->GetCount();
   if( m_iTotSearchNum <= 0 ) {
      m_iTotalImageSize = 0;
      return;
   }

   m_iTotalImageSize = m_pSearchList->m_iTotalImageSize;

   int iCurLen = 0;

   for( int i = 0; i < m_pSearchList->GetCount(); i++ ) {
      CSearchData* pNode = NULL;
      pNode              = (CSearchData*)m_pSearchList->GetData( i );
      if( pNode && pNode->m_image_buffer && pNode->m_iTotalSize >= 300 ) {
         memcpy( image_buffer + iCurLen, pNode->m_image_buffer, pNode->m_iTotalSize );
         iCurLen += pNode->m_iTotalSize;
      }
   }
}

void CIVCPFaceUserRes::GetImageBuffer( const std::string& strImagePath )
{
   FileIO imageFile;
   imageFile.Open( strImagePath.c_str(), FIO_BINARY, FIO_READ );

   m_iFaceImageSize = imageFile.GetLength();
   if( m_image_buffer )
      free( m_image_buffer );
   m_image_buffer = (byte*)malloc( sizeof( byte ) * m_iFaceImageSize );
   memset( m_image_buffer, 0x00, m_iFaceImageSize );

   imageFile.Read( m_image_buffer, 1, sizeof( byte ) * m_iFaceImageSize );
   imageFile.Close();
}

void CIVCPFaceUserRes::CreateBodybuffer( byte* pImageBuff, int nImageLen )
{
   if( m_pBodyBuffer )
      free( m_pBodyBuffer );
   m_pBodyBuffer = NULL;
   m_nXMLDataLen = 0;
   m_nBodyLen    = 0;

   FileIO buff;
   CXMLIO xmlIO( &buff, XMLIO_Write );

   if( WriteXML( &xmlIO ) ) return;

   m_nXMLDataLen = nImageLen;
   m_nBodyLen    = buff.GetLength() + nImageLen;

   // 먼저 버퍼를 만든다.
   m_pBodyBuffer = (char*)malloc( buff.GetLength() + nImageLen );
   memset( m_pBodyBuffer, 0x00, sizeof( char ) * ( buff.GetLength() + nImageLen ) );
   if( m_pBodyBuffer ) {
      memcpy( m_pBodyBuffer, (byte*)buff, buff.GetLength() );

      if( nImageLen > 0 )
         memcpy( m_pBodyBuffer + buff.GetLength(), pImageBuff, nImageLen );
   }
}

void CIVCPFaceUserRes::ParseImageBuffer()
{
   if( m_nXMLDataLen < 0 )
      return;

   byte* pImageBuffer = (byte*)malloc( sizeof( byte ) * m_nXMLDataLen );
   memcpy( pImageBuffer, m_pBodyBuffer + ( m_nBodyLen - m_nXMLDataLen ), m_nXMLDataLen );

   printf( "DataLen = %d BodyLen = %d\n", m_nXMLDataLen, m_nBodyLen );

   if( m_pSearchList ) {
      if( m_pSearchList->GetListCount() > 0 ) {
         int iBuffSize = 0;

         POSITION pos = m_pSearchList->GetHeadPosition();
         CSearchData* pNode;
         while( pos != NULL ) {
            pNode = NULL;
            pNode = (CSearchData*)m_pSearchList->GetNext( pos );
            if( pNode ) {
               if( pNode->m_iTotalSize <= 0 )
                  continue;
               pNode->m_image_buffer = new byte[pNode->m_iTotalSize];
               memcpy( pNode->m_image_buffer, pImageBuffer + iBuffSize, sizeof( byte ) * pNode->m_iTotalSize );

               printf( "index = %d image res buffer = %d imagebuffer = %d\n", pNode->m_iIndex, iBuffSize, pNode->m_iTotalSize );

               Sleep_ms( 10 );
               iBuffSize += pNode->m_iTotalSize;
            }
         }
      }
   }

   free( pImageBuffer );
}

void CIVCPFaceUserRes::DeleteBodyBuffer()
{
   SAFE_FREE( m_pBodyBuffer );
}

///////////////////////////////////////////////////////////////////////////////
//
// CIVCPFaceImageReq : 얼굴 데이터 인식/인증/분석 요청
//
///////////////////////////////////////////////////////////////////////////////

CIVCPFaceImageReq::CIVCPFaceImageReq()
{
   m_nPacketID     = IVCP_ID_Data_FaceImageData;
   m_bImage_Attach = FALSE;
   strcpy( m_szImageFormat, "JPG" );
   m_iWidth     = 0;
   m_iHeight    = 0;
   m_iTotalSize = 0;

   strcpy( m_szRequestType, "R" );
   m_iNum        = 1;
   m_pBodyBuffer = NULL;

   m_pVerifiList = NULL;

   m_pAnalysisList = NULL;
   m_nXMLDataLen   = 0;
}

CIVCPFaceImageReq::CIVCPFaceImageReq( _ImageInfo_Req* pInfo )
{
   m_nPacketID     = IVCP_ID_Data_FaceImageData;
   m_bImage_Attach = pInfo->m_bImage_Attach;
   strcpy( m_szImageFormat, pInfo->m_szImageFormat );
   m_iWidth     = pInfo->m_iWidth;
   m_iHeight    = pInfo->m_iHeight;
   m_iImageSize = pInfo->m_iTotalSize;

   strcpy( m_szRequestType, pInfo->m_szRequestType );
   m_iNum        = pInfo->m_iNum;
   m_pBodyBuffer = NULL;

   strcpy( m_szVerifiID, pInfo->m_szVerifiID );
   m_iEvtStatus  = pInfo->m_nEvtStatus;
   m_iEvt_ID     = pInfo->m_nEvt_ID;
   m_iFaceID     = pInfo->m_nFaceID;
   m_iFrameIndex = pInfo->m_nFrameIndex;
   m_nXMLDataLen = 0;
}

CIVCPFaceImageReq::~CIVCPFaceImageReq()
{
   if( m_pBodyBuffer ) free( m_pBodyBuffer );
}

int CIVCPFaceImageReq::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Request" ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "Request_Type", m_szRequestType, sizeof( m_szRequestType ) );
      xmlNode.Attribute( TYPE_ID( char ), "Request_ID", m_szRequest_ID, sizeof( m_szRequest_ID ) );
      xmlNode.Attribute( TYPE_ID( char ), "Camera_ID", &m_szRequest_CameraID, sizeof( m_szRequest_CameraID ) ); // 카메라 channel_no

      std::string RequestType = m_szRequestType;

      if( RequestType.find( "R" ) != std::string::npos ) {
         xmlNode.Attribute( TYPE_ID( int ), "Rank_Num", &m_iNum );
         xmlNode.Attribute( TYPE_ID( int ), "AttachImage", &m_bImage_Attach );
         SubReadXML_R( pIO );
      } else if( RequestType.find( "V" ) != std::string::npos ) {
         xmlNode.Attribute( TYPE_ID( int ), "Face_Num", &m_iNum );
         xmlNode.Attribute( TYPE_ID( int ), "AttachImage", &m_bImage_Attach );

         m_pVerifiList = new CFaceUserVerificationReqList; // 복수의 얼굴 인증을 위하여 리스트로 담는다
         for( int i = 0; i < m_iNum; i++ ) {
            _FaceUserVerificationReqInfo* pInfo = new _FaceUserVerificationReqInfo;
            memset( pInfo, 0x00, sizeof( pInfo ) );
            SubReadXML_V( pIO, i, pInfo );
            pInfo->m_pImage = NULL; // 이미지 버퍼에서 읽어오기 전에 우선 NULL로 함
            m_pVerifiList->AddData( pInfo, i );
         }
      } else if( RequestType.find( "A" ) != std::string::npos ) {
         xmlNode.Attribute( TYPE_ID( int ), "Face_Num", &m_iNum );
         xmlNode.Attribute( TYPE_ID( int ), "AttachImage", &m_bImage_Attach );

         m_pAnalysisList = new CFaceUserVerificationReqList; // 복수의 얼굴 분석을 위하여 리스트로 담는다
         for( int i = 0; i < m_iNum; i++ ) {
            _FaceUserVerificationReqInfo* pInfo = new _FaceUserVerificationReqInfo;
            memset( pInfo, 0x00, sizeof( pInfo ) );
            SubReadXML_A( pIO, i, pInfo );
            pInfo->m_pImage = NULL; // 이미지 버퍼에서 읽어오기 전에 우선 NULL로 함
            m_pAnalysisList->AddData( pInfo, i );
         }
      }

      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPFaceImageReq::WriteXML_VA( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Request" ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "Request_Type", m_szRequestType, sizeof( m_szRequestType ) );
      xmlNode.Attribute( TYPE_ID( char ), "Request_ID", m_szRequest_ID, sizeof( m_szRequest_ID ) );
      xmlNode.Attribute( TYPE_ID( char ), "Camera_ID", &m_szRequest_CameraID, sizeof( m_szRequest_CameraID ) ); // 카메라 channel_no
      xmlNode.Attribute( TYPE_ID( int ), "Face_Num", &m_iNum );

      xmlNode.Attribute( TYPE_ID( char ), "HW_ID", m_szHWID, sizeof( m_szHWID ) );
      xmlNode.Attribute( TYPE_ID( int ), "System_ID", &m_iSystemID );
      xmlNode.Attribute( TYPE_ID( int ), "Channel_No", &m_iChannelNo );
      xmlNode.Attribute( TYPE_ID( int ), "Object_ID", &m_iObjectID );
      xmlNode.Attribute( TYPE_ID( int ), "Evt_Status", &m_iEvtStatus );

      xmlNode.Attribute( TYPE_ID( char ), "Date", &m_szDate, sizeof( m_szDate ) );

      xmlNode.Attribute( TYPE_ID( int ), "AttachImage", &m_bImage_Attach );

      xmlNode.Attribute( TYPE_ID( BOOL ), "FFV_Save", &m_bServerSideFFVSaveFlag );

      SubWriteXML_VA( pIO );

      xmlNode.End();
      return ( DONE );
   }

   return ( 1 );
}
int CIVCPFaceImageReq::WriteXML_VM( CXMLIO* pIO )
{
   if( m_pVerifiList == NULL )
      return ( 1 );

   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Request" ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "Request_Type", m_szRequestType, sizeof( m_szRequestType ) );
      xmlNode.Attribute( TYPE_ID( char ), "Request_ID", m_szRequest_ID, sizeof( m_szRequest_ID ) );
      xmlNode.Attribute( TYPE_ID( char ), "Camera_ID", &m_szRequest_CameraID, sizeof( m_szRequest_CameraID ) ); // 카메라 channel_no
      xmlNode.Attribute( TYPE_ID( int ), "Face_Num", &m_iNum );
      xmlNode.Attribute( TYPE_ID( int ), "AttachImage", &m_bImage_Attach );

      for( int i = 0; i < m_pVerifiList->GetListCount(); i++ ) {
         CFaceUserVerificationReqData* pNode = (CFaceUserVerificationReqData*)m_pVerifiList->GetData( i );
         SubWriteXML_VM( pIO, i, pNode );
      }

      xmlNode.End();
      return ( DONE );
   }

   return ( 1 );
}

int CIVCPFaceImageReq::WriteXML_R( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Request" ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "Request_Type", m_szRequestType, sizeof( m_szRequestType ) );
      xmlNode.Attribute( TYPE_ID( char ), "Request_ID", m_szRequest_ID, sizeof( m_szRequest_ID ) );
      xmlNode.Attribute( TYPE_ID( char ), "Camera_ID", &m_szRequest_CameraID, sizeof( m_szRequest_CameraID ) ); // 카메라 channel_no
      xmlNode.Attribute( TYPE_ID( int ), "Rank_Num", &m_iNum );
      xmlNode.Attribute( TYPE_ID( int ), "Image_Attach", &m_bImage_Attach );
      SubWriteXML_R( pIO );
      xmlNode.End();
      return ( DONE );
   }

   return ( 1 );
}

int CIVCPFaceImageReq::WriteXML_A( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Request" ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "Request_Type", m_szRequestType, sizeof( m_szRequestType ) );
      xmlNode.Attribute( TYPE_ID( char ), "Request_ID", m_szRequest_ID, sizeof( m_szRequest_ID ) );
      xmlNode.Attribute( TYPE_ID( char ), "Camera_ID", &m_szRequest_CameraID, sizeof( m_szRequest_CameraID ) ); // 카메라 channel_no
      xmlNode.Attribute( TYPE_ID( int ), "Face_Num", &m_iNum );
      xmlNode.Attribute( TYPE_ID( int ), "Image_Attach", &m_bImage_Attach );
      for( int i = 0; i < m_pVerifiList->GetListCount(); i++ ) {
         CFaceUserVerificationReqData* pNode = (CFaceUserVerificationReqData*)m_pVerifiList->GetData( i );
         SubWriteXML_A( pIO, i, pNode );
      }
      xmlNode.End();
      return ( DONE );
   }

   return ( 1 );
}

int CIVCPFaceImageReq::SubReadXML_R( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );

   if( m_nXMLDataLen > 0 ) {
      if( xmlNode.Start( "Image" ) ) {
         xmlNode.Attribute( TYPE_ID( char ), "Format", m_szImageFormat, sizeof( m_szImageFormat ) );
         //xmlNode.Attribute (TYPE_ID (CStringA), "Size", &strData);
         //int iFindLen = strData.Find(",");
         //if(iFindLen >= 0)
         //{
         // pInfo->m_iWidth = (atoi)(strData.Mid(1,iFindLen-1));
         // pInfo->m_iHeight = (atoi)(strData.Mid(iFindLen+1, strData.GetLength()-1));
         //}
         xmlNode.Attribute( TYPE_ID( int ), "Length", &m_iImageSize );
         xmlNode.End();
      }
   }

   return ( 1 );
}

int CIVCPFaceImageReq::SubReadXML_V( CXMLIO* pIO, int iIndex, _FaceUserVerificationReqInfo* pInfo )
{
   CXMLNode xmlNode( pIO );

   if( m_nXMLDataLen > 0 ) {
      std::string strNode;
      strNode = sformat( "Image%d", iIndex );
      if( xmlNode.Start( strNode.c_str() ) ) {
         xmlNode.Attribute( TYPE_ID( char ), "Verifi_ID", pInfo->m_szVerifiID, sizeof( pInfo->m_szVerifiID ) ); // Object_ID
         xmlNode.Attribute( TYPE_ID( char ), "Format", pInfo->m_szImageFormat, sizeof( pInfo->m_szImageFormat ) );
         char szSize[20];
         xmlNode.Attribute( TYPE_ID( char ), "Size", szSize, sizeof( szSize ) );
         std::string strData = szSize;
         int iFindLen        = strData.find( "," );
         if( iFindLen != std::string::npos ) {
            pInfo->m_iWidth  = ( atoi )( strData.assign( 1, iFindLen - 1 ).c_str() );
            pInfo->m_iHeight = ( atoi )( strData.assign( iFindLen + 1, strData.length() - 1 ).c_str() );
         }
         xmlNode.Attribute( TYPE_ID( int ), "Length", &pInfo->m_iImageSize );
         xmlNode.End();
      }
   }

   return ( 1 );
}

int CIVCPFaceImageReq::SubReadXML_A( CXMLIO* pIO, int iIndex, _FaceUserVerificationReqInfo* pInfo )
{
   CXMLNode xmlNode( pIO );

   if( m_nXMLDataLen > 0 ) {
      std::string strNode;
      strNode = sformat( "Image%d", iIndex );
      if( xmlNode.Start( strNode.c_str() ) ) {
         xmlNode.Attribute( TYPE_ID( char ), "User_ID", pInfo->m_szVerifiID, sizeof( pInfo->m_szVerifiID ) ); // Object_ID
         xmlNode.Attribute( TYPE_ID( char ), "Format", pInfo->m_szImageFormat, sizeof( pInfo->m_szImageFormat ) );
         char szSize[20];
         xmlNode.Attribute( TYPE_ID( char ), "Size", szSize, sizeof( szSize ) );
         std::string strData = szSize;
         int iFindLen        = strData.find( "," );
         if( iFindLen != std::string::npos ) {
            pInfo->m_iWidth  = ( atoi )( strData.assign( 1, iFindLen - 1 ).c_str() );
            pInfo->m_iHeight = ( atoi )( strData.assign( iFindLen + 1, strData.length() - 1 ).c_str() );
         }
         xmlNode.Attribute( TYPE_ID( int ), "Length", &pInfo->m_iImageSize );
         xmlNode.End();
      }
   }

   return ( 1 );
}

int CIVCPFaceImageReq::SubWriteXML_VA( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   std::string strNode = sformat( "Image%d", 0 );
   if( xmlNode.Start( strNode.c_str() ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "Verifi_ID", m_szVerifiID, sizeof( m_szVerifiID ) );
      xmlNode.Attribute( TYPE_ID( int ), "Evt_Status", &m_iEvtStatus );
      xmlNode.Attribute( TYPE_ID( int ), "Evt_ID", &m_iEvt_ID );
      xmlNode.Attribute( TYPE_ID( int ), "Face_ID", &m_iFaceID );
      xmlNode.Attribute( TYPE_ID( int ), "Frame_Index", &m_iFrameIndex );
      xmlNode.Attribute( TYPE_ID( char ), "Format", m_szImageFormat, sizeof( m_szImageFormat ) );
      char szSize[20];
      sprintf( szSize, "(%d,%d)", m_iWidth, m_iHeight );
      xmlNode.Attribute( TYPE_ID( char ), "Size", szSize, sizeof( szSize ) );
      xmlNode.Attribute( TYPE_ID( int ), "Length", &m_iImageSize );
      xmlNode.End();
   }

   return ( 1 );
}

int CIVCPFaceImageReq::SubWriteXML_VM( CXMLIO* pIO, int iIndex, CFaceUserVerificationReqData* pData )
{
   CXMLNode xmlNode( pIO );
   std::string strNode = sformat( "Image%d", 0 );
   if( xmlNode.Start( strNode.c_str() ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "Verifi_ID", pData->m_szVerifiID, sizeof( pData->m_szVerifiID ) );
      xmlNode.Attribute( TYPE_ID( char ), "Format", m_szImageFormat, sizeof( m_szImageFormat ) );
      char szSize[20];
      sprintf( szSize, "(%d,%d)", pData->m_iWidth, pData->m_iHeight );
      xmlNode.Attribute( TYPE_ID( char ), "Size", szSize, sizeof( szSize ) );
      xmlNode.Attribute( TYPE_ID( int ), "Length", &pData->m_iImageSize );
      xmlNode.End();
   }

   return ( 1 );
}

int CIVCPFaceImageReq::SubWriteXML_A( CXMLIO* pIO, int iIndex, CFaceUserVerificationReqData* pData )
{
   CXMLNode xmlNode( pIO );
   std::string strNode = sformat( "Image%d", 0 );
   if( xmlNode.Start( strNode.c_str() ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "User_ID", pData->m_szVerifiID, sizeof( pData->m_szVerifiID ) ); // Object_ID
      xmlNode.Attribute( TYPE_ID( char ), "Format", m_szImageFormat, sizeof( m_szImageFormat ) );
      char szSize[20];
      sprintf( szSize, "(%d,%d)", pData->m_iWidth, pData->m_iHeight );
      xmlNode.Attribute( TYPE_ID( char ), "Size", szSize, sizeof( szSize ) );
      xmlNode.Attribute( TYPE_ID( int ), "Length", &pData->m_iImageSize );
      xmlNode.End();
   }

   return ( 1 );
}

int CIVCPFaceImageReq::SubWriteXML_R( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );

   if( m_iImageSize > 0 ) {
      if( xmlNode.Start( "Image" ) ) {
         xmlNode.Attribute( TYPE_ID( char ), "Format", m_szImageFormat, sizeof( m_szImageFormat ) );
         char szSize[20];
         sprintf( szSize, "(%d,%d)", m_iWidth, m_iHeight );
         xmlNode.Attribute( TYPE_ID( char ), "Size", szSize, sizeof( szSize ) );
         xmlNode.Attribute( TYPE_ID( int ), "Length", &m_iImageSize );
         xmlNode.End();
      }
   }

   return ( 1 );
}

int CIVCPFaceImageReq::WriteToPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      aPacket.FreeBody();
   }
   if( m_pBodyBuffer ) {
      aPacket.m_Header.m_nOptions    = 0;
      aPacket.m_Header.m_nXMLDataLen = m_nXMLDataLen;
      aPacket.m_Header.m_nBodyLen    = m_nBodyLen;
      aPacket.m_pBody                = (char*)malloc( m_nBodyLen );
      memcpy( aPacket.m_pBody, m_pBodyBuffer, m_nBodyLen );
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPFaceImageReq::ReadFromPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      m_nXMLDataLen   = aPacket.m_Header.m_nXMLDataLen;
      m_nBodyLen      = aPacket.m_Header.m_nBodyLen;
      m_pBodyBuffer   = aPacket.m_pBody;
      aPacket.m_pBody = NULL;

      m_iTotalSize = m_nXMLDataLen;

      ParseBodyBuffer(); // 바로 파싱한다.
      return ( DONE );
   }
   return ( 1 );
}

void CIVCPFaceImageReq::ParseBodyBuffer()
{
   FileIO buff;
   buff.Attach( (byte*)m_pBodyBuffer, m_nBodyLen - m_nXMLDataLen );
   CXMLIO xmlIO( &buff, XMLIO_Read );
   if( DONE != ReadXML( &xmlIO ) ) {
      buff.Detach();
      return;
   }

   if( m_nXMLDataLen > 0 ) {
      ParseImageBuffer(); // 바로 이미지 파싱
   }
}

void CIVCPFaceImageReq::CreateBodybuffer( byte* pImageBuff, int nImageLen, int iType )
{
   if( m_pBodyBuffer ) free( m_pBodyBuffer );
   m_pBodyBuffer = NULL;
   m_nXMLDataLen = 0;
   m_nBodyLen    = 0;

   FileIO buff;
   CXMLIO xmlIO( &buff, XMLIO_Write );

   switch( iType ) {
   case FACE_TYPE_VERIFICATION_A:
      if( WriteXML_VA( &xmlIO ) != DONE ) // 인증 IVCP
         return;
      break;
   case FACE_TYPE_VERIFICATION_M:
      if( WriteXML_VM( &xmlIO ) != DONE ) // 인증 IVCP
         return;
      break;
   case FACE_TYPE_RECOGNITION:
      if( WriteXML_R( &xmlIO ) != DONE ) // 인식 IVCP
         return;
      break;
   case FACE_TYPE_ANALYSIS:
      if( WriteXML_A( &xmlIO ) != DONE ) // 분석 IVCP
         return;
      break;
   }

   m_nXMLDataLen = nImageLen;
   m_nBodyLen    = buff.GetLength() + nImageLen;

   // 먼저 버퍼를 만든다.
   m_pBodyBuffer = (char*)malloc( buff.GetLength() + nImageLen );
   if( m_pBodyBuffer ) {
      memcpy( m_pBodyBuffer, (byte*)buff, buff.GetLength() );

      if( nImageLen )
         memcpy( m_pBodyBuffer + buff.GetLength(), pImageBuff, nImageLen );
   }
}

void CIVCPFaceImageReq::ParseImageBuffer()
{
   byte* pImageBuffer = (byte*)malloc( sizeof( byte ) * m_nXMLDataLen );
   memcpy( pImageBuffer, m_pBodyBuffer + ( m_nBodyLen - m_nXMLDataLen ), m_nXMLDataLen );

   if( strcmp( m_szRequestType, "V" ) == 0 ) {
      int iBuffSize = 0;
      int iCount    = m_pVerifiList->GetListCount(); // 패킷을 분석하여 생성한 인증 요청 리스트

      POSITION pos = m_pVerifiList->GetHeadPosition(); //인증 요청 리스트에서 분석한 얼굴 이미지 파일의 크기에 따라 이미지를 분리하여 각강의 해당 리스트에 담는다.
      while( pos ) {
         CFaceUserVerificationReqData* pNode = (CFaceUserVerificationReqData*)m_pVerifiList->GetNext( pos );
         if( pNode->m_iImageSize > 200 ) //최소 얼굴 이미지 파일 사이즈
         {
            pNode->m_pImage = new byte[pNode->m_iImageSize];
            memcpy( pNode->m_pImage, pImageBuffer + iBuffSize, sizeof( byte ) * pNode->m_iImageSize );
            iBuffSize += pNode->m_iImageSize;
         }
      }

      free( pImageBuffer );
   } else if( strcmp( m_szRequestType, "A" ) == 0 ) {
      int iBuffSize = 0;
      int iCount    = m_pAnalysisList->GetListCount(); // 패킷을 분석하여 생성한 인증 요청 리스트

      POSITION pos = m_pAnalysisList->GetHeadPosition(); //인증 요청 리스트에서 분석한 얼굴 이미지 파일의 크기에 따라 이미지를 분리하여 각강의 해당 리스트에 담는다.
      while( pos ) {
         CFaceUserVerificationReqData* pNode = (CFaceUserVerificationReqData*)m_pAnalysisList->GetNext( pos );
         if( pNode->m_iImageSize > 200 ) //최소 얼굴 이미지 파일 사이즈
         {
            pNode->m_pImage = new byte[pNode->m_iImageSize];
            memcpy( pNode->m_pImage, pImageBuffer + iBuffSize, sizeof( byte ) * pNode->m_iImageSize );
            iBuffSize += pNode->m_iImageSize;
         }
      }

      free( pImageBuffer );
   } else {
      memcpy( pImageBuffer, m_pBodyBuffer + ( m_nBodyLen - m_nXMLDataLen ), m_nXMLDataLen );
      m_pFaceImage = pImageBuffer;
   }

   return;
}

void CIVCPFaceImageReq::DeleteBodyBuffer()
{
   SAFE_FREE( m_pBodyBuffer );
}

///////////////////////////////////////////////////////////////////////////////
//
// CIVCPFaceImageRes : 얼굴 데이터 등록/검색/편집/삭제 응답
//
///////////////////////////////////////////////////////////////////////////////

CIVCPFaceImageRes::CIVCPFaceImageRes()
{
   m_nPacketID = IVCP_ID_Data_FaceUserData;

   m_bImage_Attach = FALSE;
   strcpy( m_szImageFormat, "JPG" );
   memset( m_szResponseType, 0x00, sizeof( m_szResponseType ) );
   memset( m_szResponse_ID, 0x00, sizeof( m_szResponse_ID ) );
   memset( m_szResponse_CameraID, 0x00, sizeof( m_szResponse_CameraID ) );
   m_iTotSearchNum   = 0;
   m_iTotalImageSize = 0;

   m_pBodyBuffer = NULL;

   m_pSearchList = NULL;

   m_image_buffer = NULL;

   m_pVerifiList = NULL;

   m_pAnalysisList = NULL;
   m_nXMLDataLen   = 0;
}

CIVCPFaceImageRes::~CIVCPFaceImageRes()
{
   if( m_pBodyBuffer ) {
      free( m_pBodyBuffer );
      m_pBodyBuffer = NULL;
   }

   if( m_image_buffer ) {
      free( m_image_buffer );
      m_image_buffer = NULL;
   }
}

int CIVCPFaceImageRes::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Response" ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "Response_Type", m_szResponseType, sizeof( m_szResponseType ) );
      xmlNode.Attribute( TYPE_ID( char ), "Response_ID", m_szResponse_ID, sizeof( m_szResponse_ID ) );
      xmlNode.Attribute( TYPE_ID( char ), "Camera_ID", m_szResponse_CameraID, sizeof( m_szResponse_CameraID ) );
      xmlNode.Attribute( TYPE_ID( int ), "User_Num", &m_iUserNum );
      xmlNode.Attribute( TYPE_ID( char ), "Result", m_szResult, sizeof( m_szResult ) );
      if( strcmp( m_szResult, "OK" ) == 0 ) {
         m_pSearchList = new CSearchList;
         if( strcmp( m_szResponseType, "V" ) == 0 ) {
            SubReadXML_V( pIO );
         } else {
            xmlNode.End();
            return ( 1 );
         }
      }

      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPFaceImageRes::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Response" ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "Response_Type", m_szResponseType, sizeof( m_szResponseType ) );
      xmlNode.Attribute( TYPE_ID( char ), "Response_ID", m_szResponse_ID, sizeof( m_szResponse_ID ) );
      xmlNode.Attribute( TYPE_ID( char ), "Camera_ID", m_szResponse_CameraID, sizeof( m_szResponse_CameraID ) );
      xmlNode.Attribute( TYPE_ID( int ), "User_Num", &m_iUserNum );
      xmlNode.Attribute( TYPE_ID( char ), "Result", m_szResult, sizeof( m_szResult ) );

      if( strcmp( m_szResponseType, "R" ) == 0 )
         SubWriteXML_R( pIO );
      else if( strcmp( m_szResponseType, "V" ) == 0 )
         SubWriteXML_V( pIO );
      else if( strcmp( m_szResponseType, "A" ) == 0 )
         SubWriteXML_A( pIO );
      else {
         xmlNode.End();
         return ( 1 );
      }

      xmlNode.End();
   }

   return ( DONE );
}

int CIVCPFaceImageRes::SubWriteXML_V( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );

   if( xmlNode.Start( "Verification" ) ) {
      CXMLNode xmlSubNode( pIO );
      int iUserNum = 0;
      for( int i = 0; i < m_pVerifiList->GetListCount(); i++ ) {
         CFaceUserVerificationReqData* pNode = (CFaceUserVerificationReqData*)m_pVerifiList->GetData( i );
         if( pNode->m_pUserInfo ) {
            std::string strData = sformat( "User%d", iUserNum++ );
            if( xmlSubNode.Start( strData.c_str() ) ) {
               xmlSubNode.Attribute( TYPE_ID( char ), "Verifi_ID", pNode->m_szVerifiID, sizeof( pNode->m_szVerifiID ) );
               xmlSubNode.Attribute( TYPE_ID( int ), "Face_ID", &pNode->m_pUserInfo->m_nFaceID );
               xmlSubNode.Attribute( TYPE_ID( char ), "UserID", pNode->m_pUserInfo->m_szUserID, sizeof( pNode->m_pUserInfo->m_szUserID ) );
               xmlSubNode.Attribute( TYPE_ID( char ), "UserName", pNode->m_pUserInfo->m_szUserName, sizeof( pNode->m_pUserInfo->m_szUserName ) );
               xmlSubNode.Attribute( TYPE_ID( int ), "Age", &pNode->m_pUserInfo->m_iAge );
               xmlSubNode.Attribute( TYPE_ID( int ), "Gender", &pNode->m_pUserInfo->m_iGender );
               xmlSubNode.Attribute( TYPE_ID( char ), "Beloging_ID", pNode->m_pUserInfo->m_strBeloging_ID, sizeof( pNode->m_pUserInfo->m_strBeloging_ID ) );
               xmlSubNode.Attribute( TYPE_ID( char ), "Position_ID", pNode->m_pUserInfo->m_strPosition_ID, sizeof( pNode->m_pUserInfo->m_strPosition_ID ) );
               xmlSubNode.Attribute( TYPE_ID( char ), "TEL", pNode->m_pUserInfo->m_strTEL, sizeof( pNode->m_pUserInfo->m_strTEL ) );
               xmlSubNode.Attribute( TYPE_ID( char ), "HP", pNode->m_pUserInfo->m_strHP, sizeof( pNode->m_pUserInfo->m_strHP ) );
               xmlSubNode.Attribute( TYPE_ID( int ), "Blacklist_Check", &pNode->m_pUserInfo->m_iBlacklist_chk );

               float fRate = pNode->m_pUserInfo->m_fRate * 100.0;
               xmlSubNode.Attribute( TYPE_ID( float ), "Rate", &fRate );

               CXMLNode xmlSubImageNode( pIO );
               if( xmlSubImageNode.Start( "Image" ) ) {
                  xmlSubNode.Attribute( TYPE_ID( char ), "Format", m_szImageFormat, sizeof( m_szImageFormat ) );
                  char szSize[20];
                  sprintf( szSize, "(%d,%d)", pNode->m_iWidth, pNode->m_iHeight );
                  xmlSubImageNode.Attribute( TYPE_ID( char ), "Size", szSize, sizeof( szSize ) );
                  xmlSubImageNode.Attribute( TYPE_ID( int ), "Length", &pNode->m_iImageSize );
                  xmlSubImageNode.End();
               }

               xmlSubNode.End();
            }
         }
      }
      xmlNode.End();
   }

   return ( DONE );
}

int CIVCPFaceImageRes::SubWriteXML_R( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );

   if( xmlNode.Start( "Recognition" ) ) {
      CXMLNode xmlSubNode( pIO );
      int iUserNum = 0;
      for( int i = 0; i < m_pVerifiList->GetListCount(); i++ ) {
         CFaceUserVerificationReqData* pNode = (CFaceUserVerificationReqData*)m_pVerifiList->GetData( i );
         if( pNode->m_pUserInfo ) {
            std::string strData = sformat( "User%d", iUserNum++ );
            if( xmlSubNode.Start( strData.c_str() ) ) {
               CXMLNode xmlSubImageNode( pIO );
               if( xmlSubImageNode.Start( "Similarity" ) ) {
                  float fRate = pNode->m_fSimilarity * 100.0;
                  xmlSubImageNode.Attribute( TYPE_ID( float ), "Rate", &fRate );
                  xmlSubImageNode.End();
               }

               if( xmlSubImageNode.Start( "User" ) ) {
                  xmlSubNode.Attribute( TYPE_ID( char ), "UserID", pNode->m_pUserInfo->m_szUserID, sizeof( pNode->m_pUserInfo->m_szUserID ) );
                  xmlSubNode.Attribute( TYPE_ID( char ), "UserName", pNode->m_pUserInfo->m_szUserName, sizeof( pNode->m_pUserInfo->m_szUserName ) );
                  xmlSubNode.Attribute( TYPE_ID( int ), "Age", &pNode->m_pUserInfo->m_iAge );
                  xmlSubNode.Attribute( TYPE_ID( int ), "Gender", &pNode->m_pUserInfo->m_iGender );

                  xmlSubNode.Attribute( TYPE_ID( char ), "Beloging_ID", pNode->m_pUserInfo->m_strBeloging_ID, sizeof( pNode->m_pUserInfo->m_strBeloging_ID ) );
                  xmlSubNode.Attribute( TYPE_ID( char ), "Position_ID", pNode->m_pUserInfo->m_strPosition_ID, sizeof( pNode->m_pUserInfo->m_strPosition_ID ) );
                  xmlSubNode.Attribute( TYPE_ID( char ), "TEL", pNode->m_pUserInfo->m_strTEL, sizeof( pNode->m_pUserInfo->m_strTEL ) );
                  xmlSubNode.Attribute( TYPE_ID( char ), "HP", pNode->m_pUserInfo->m_strHP, sizeof( pNode->m_pUserInfo->m_strHP ) );
                  xmlSubImageNode.End();
               }

               if( xmlSubImageNode.Start( "Image" ) ) {
                  xmlSubNode.Attribute( TYPE_ID( char ), "Format", m_szImageFormat, sizeof( m_szImageFormat ) );
                  char szSize[20];
                  sprintf( szSize, "(%d,%d)", pNode->m_iWidth, pNode->m_iHeight );
                  xmlSubImageNode.Attribute( TYPE_ID( char ), "Size", szSize, sizeof( szSize ) );
                  xmlSubImageNode.Attribute( TYPE_ID( int ), "Length", &pNode->m_iImageSize );
                  xmlSubImageNode.End();
               }

               xmlSubNode.End();
            }
         }
      }
      xmlNode.End();
   }

   return ( DONE );
}

int CIVCPFaceImageRes::SubWriteXML_A( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Analysis" ) ) {
      CXMLNode xmlSubNode( pIO );
      int iUserNum = 0;
      int iCnt     = m_pAnalysisList->GetListCount();
      for( int i = 0; i < m_pAnalysisList->GetListCount(); i++ ) {
         CFaceUserVerificationReqData* pNode = (CFaceUserVerificationReqData*)m_pAnalysisList->GetData( i );
         if( pNode ) {
            std::string strData = sformat( "User%d", iUserNum++ );
            if( xmlSubNode.Start( strData.c_str() ) ) {
               xmlSubNode.Attribute( TYPE_ID( char ), "User_ID", pNode->m_szVerifiID, sizeof( pNode->m_szVerifiID ) );
               xmlSubNode.Attribute( TYPE_ID( int ), "Age", &iUserNum );
               xmlSubNode.Attribute( TYPE_ID( int ), "Gender", &pNode->m_Gender );
               xmlSubNode.Attribute( TYPE_ID( int ), "Glasses", &pNode->m_Glasses );
               xmlSubNode.End();
            }
         }
      }
      xmlNode.End();
   }

   return ( DONE );
}

int CIVCPFaceImageRes::SubWriteXML_TO( CXMLIO* pIO, int iIndex )
{
   CSearchData* pNode = NULL;
   pNode              = m_pSearchList->GetData( iIndex );
   if( pNode == NULL )
      return ( 1 );

   CXMLNode xmlNode( pIO );

   if( xmlNode.Start( "Similarity" ) ) {
      xmlNode.Attribute( TYPE_ID( float ), "Rate", &pNode->m_fRate );
      xmlNode.End();
   }

   if( xmlNode.Start( "User" ) ) {
      char szUserID[100];
      strcpy( szUserID, pNode->m_strUserID.c_str() );
      xmlNode.Attribute( TYPE_ID( char ), "UserID", szUserID, sizeof( szUserID ) );
      char szUserName[100];
      strcpy( szUserName, pNode->m_strUserName.c_str() );
      xmlNode.Attribute( TYPE_ID( char ), "UserName", szUserName, sizeof( szUserName ) );
      xmlNode.Attribute( TYPE_ID( int ), "Age", &pNode->m_iAge );
      xmlNode.Attribute( TYPE_ID( int ), "Gender", &pNode->m_iGender );
      xmlNode.Attribute( TYPE_ID( string ), "Beloging_ID", &pNode->m_strBeloging_ID );
      xmlNode.Attribute( TYPE_ID( string ), "Position_ID", &pNode->m_strPosition_ID );
      xmlNode.Attribute( TYPE_ID( string ), "TEL", &pNode->m_strTEL );
      xmlNode.Attribute( TYPE_ID( string ), "HP", &pNode->m_strHP );
      xmlNode.End();
   }

   if( xmlNode.Start( "Image" ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "Format", m_szImageFormat, sizeof( m_szImageFormat ) );
      char szSize[20];
      sprintf( szSize, "(%d,%d)", pNode->m_iWidth, pNode->m_iHeight );
      xmlNode.Attribute( TYPE_ID( char ), "Size", szSize, sizeof( szSize ) );
      xmlNode.Attribute( TYPE_ID( int ), "Length", &pNode->m_iTotalSize );
      xmlNode.End();
   }

   return ( DONE );
}

int CIVCPFaceImageRes::SubReadXML_R( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );

   if( xmlNode.Start( "Recognition" ) ) {
      for( int i = 0; i < m_iUserNum; i++ ) {
         _UserDataInfo* pNode = new _UserDataInfo;
         memset( pNode, 0x00, sizeof( pNode ) );
         memset( pNode->m_szUserID, 0x00, sizeof( pNode->m_szUserID ) );
         memset( pNode->m_szUserName, 0x00, sizeof( pNode->m_szUserName ) );
         memset( pNode->m_szVrifi_ID, 0x00, sizeof( pNode->m_szVrifi_ID ) );
         //memset(pNode->m_strPosition_ID, 0x00, sizeof(pNode->m_strPosition_ID));
         //memset(pNode->m_strBeloging_ID, 0x00, sizeof(pNode->m_strBeloging_ID));
         //memset(pNode->m_strTEL, 0x00, sizeof(pNode->m_strTEL));
         //memset(pNode->m_strHP, 0x00, sizeof(pNode->m_strHP));
         memset( pNode->m_szRequestType, 0x00, sizeof( pNode->m_szRequestType ) );
         memset( pNode->m_szImageFormat, 0x00, sizeof( pNode->m_szImageFormat ) );

         pNode->m_iChannel_no = ( atoi )( m_szResponse_CameraID );
         pNode->m_fRate       = 0;

         CXMLNode xmlSubNode( pIO );
         std::string strNode = sformat( "Rank%d", i );
         if( xmlSubNode.Start( strNode.c_str() ) ) {
            CXMLNode xmlSubImageNode( pIO );

            if( xmlSubImageNode.Start( "Similarity" ) ) {
               xmlSubImageNode.Attribute( TYPE_ID( float ), "Rate", &pNode->m_fRate );
               xmlSubImageNode.End();
            }

            if( xmlSubImageNode.Start( "User" ) ) {
               xmlSubNode.Attribute( TYPE_ID( char ), "UserID", pNode->m_szUserID, sizeof( pNode->m_szUserID ) );
               xmlSubNode.Attribute( TYPE_ID( char ), "UserName", pNode->m_szUserName, sizeof( pNode->m_szUserName ) );
               xmlSubNode.Attribute( TYPE_ID( char ), "Beloging_ID", pNode->m_strBeloging_ID, sizeof( pNode->m_strBeloging_ID ) );
               xmlSubNode.Attribute( TYPE_ID( char ), "Position_ID", pNode->m_strPosition_ID, sizeof( pNode->m_strPosition_ID ) );
               xmlSubNode.Attribute( TYPE_ID( char ), "TEL", pNode->m_strTEL, sizeof( pNode->m_strTEL ) );
               xmlSubNode.Attribute( TYPE_ID( char ), "HP", pNode->m_strHP, sizeof( pNode->m_strHP ) );
               xmlSubNode.Attribute( TYPE_ID( int ), "Age", &pNode->m_iAge );
               xmlSubNode.Attribute( TYPE_ID( int ), "Gender", &pNode->m_iGender );
               xmlSubImageNode.End();
            }

            if( xmlSubImageNode.Start( "Image" ) ) {
               xmlSubNode.Attribute( TYPE_ID( char ), "Format", m_szImageFormat, sizeof( m_szImageFormat ) );
               char szSize[20];
               xmlSubImageNode.Attribute( TYPE_ID( char ), "Size", szSize, sizeof( szSize ) );
               std::string strData = szSize;
               int iFindLen        = strData.find( "," );
               if( iFindLen != std::string::npos ) {
                  m_iWidth         = ( atoi )( strData.assign( 1, iFindLen - 1 ).c_str() );
                  pNode->m_iWidth  = m_iWidth;
                  m_iHeight        = ( atoi )( strData.assign( iFindLen + 1, strData.length() - 1 ).c_str() );
                  pNode->m_iHeight = m_iHeight;
               }
               xmlSubImageNode.Attribute( TYPE_ID( int ), "Length", &pNode->iSubImageSize );
               xmlSubImageNode.End();
            }

            xmlSubNode.End();

            pNode->ImageBuffer = NULL;

            m_pSearchList->AddData( pNode, i );
         }
      }

      xmlNode.End();
   }

   return ( DONE );
}

int CIVCPFaceImageRes::SubReadXML_V( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );

   if( xmlNode.Start( "Verification" ) ) {
      for( int i = 0; i < m_iUserNum; i++ ) {
         CXMLNode xmlSubNode( pIO );
         std::string strNode = sformat( "User%d", i );
         if( xmlSubNode.Start( strNode.c_str() ) ) {
            _UserDataInfo* pNode = new _UserDataInfo;
            memset( pNode, 0x00, sizeof( pNode ) );
            memset( pNode->m_szUserID, 0x00, sizeof( pNode->m_szUserID ) );
            memset( pNode->m_szUserName, 0x00, sizeof( pNode->m_szUserName ) );
            memset( pNode->m_szVrifi_ID, 0x00, sizeof( pNode->m_szVrifi_ID ) );
            memset( pNode->m_szRequestType, 0x00, sizeof( pNode->m_szRequestType ) );
            memset( pNode->m_szImageFormat, 0x00, sizeof( pNode->m_szImageFormat ) );

            pNode->ImageBuffer        = NULL;
            pNode->iImageSize         = 0;
            pNode->SubImagebuffer     = NULL;
            pNode->iSubImageSize      = 0;
            pNode->SubImagebuffer_Svr = NULL;

            pNode->m_iChannel_no = ( atoi )( m_szResponse_CameraID );
            pNode->m_fRate       = 0;

            xmlSubNode.Attribute( TYPE_ID( char ), "Verifi_ID", pNode->m_szVrifi_ID, sizeof( pNode->m_szVrifi_ID ) );
            xmlSubNode.Attribute( TYPE_ID( int ), "Evt_Status", &pNode->m_iEvtStatus );
            xmlSubNode.Attribute( TYPE_ID( int ), "Evt_ID", &pNode->m_iEvt_ID );
            xmlSubNode.Attribute( TYPE_ID( int ), "Face_ID", &pNode->m_nFaceID );
            xmlSubNode.Attribute( TYPE_ID( int ), "Frame_Index", &pNode->m_iFrameIndex );
            xmlSubNode.Attribute( TYPE_ID( char ), "UserID", pNode->m_szUserID, sizeof( pNode->m_szUserID ) );
            xmlSubNode.Attribute( TYPE_ID( char ), "UserName", pNode->m_szUserName, sizeof( pNode->m_szUserName ) );

            xmlSubNode.Attribute( TYPE_ID( char ), "Beloging_ID", pNode->m_strBeloging_ID, sizeof( pNode->m_strBeloging_ID ) );
            xmlSubNode.Attribute( TYPE_ID( char ), "Position_ID", pNode->m_strPosition_ID, sizeof( pNode->m_strPosition_ID ) );
            xmlSubNode.Attribute( TYPE_ID( char ), "TEL", pNode->m_strTEL, sizeof( pNode->m_strTEL ) );
            xmlSubNode.Attribute( TYPE_ID( char ), "HP", pNode->m_strHP, sizeof( pNode->m_strHP ) );

            xmlSubNode.Attribute( TYPE_ID( int ), "Age", &pNode->m_iAge );
            xmlSubNode.Attribute( TYPE_ID( int ), "Gender", &pNode->m_iGender );
            xmlSubNode.Attribute( TYPE_ID( int ), "Blacklist_Check", &pNode->m_iBlacklist_chk );
            xmlSubNode.Attribute( TYPE_ID( float ), "Rate", &pNode->m_fRate );

            CXMLNode xmlSubImageNode( pIO );
            if( xmlSubImageNode.Start( "Image" ) ) {
               xmlSubNode.Attribute( TYPE_ID( char ), "Format", m_szImageFormat, sizeof( m_szImageFormat ) );
               char szSize[20];
               xmlSubImageNode.Attribute( TYPE_ID( char ), "Size", szSize, sizeof( szSize ) );
               std::string strData = szSize;
               int iFindLen        = strData.find( "," );
               if( iFindLen != std::string::npos ) {
                  m_iWidth         = ( atoi )( strData.assign( 1, iFindLen - 1 ).c_str() );
                  pNode->m_iWidth  = m_iWidth;
                  m_iHeight        = ( atoi )( strData.assign( iFindLen + 1, strData.length() - 1 ).c_str() );
                  pNode->m_iHeight = m_iHeight;
               }
               xmlSubImageNode.Attribute( TYPE_ID( int ), "Length", &pNode->iImageSize );
               xmlSubImageNode.End();
            } else
               pNode->iImageSize = 0;

            xmlSubNode.End();

            pNode->ImageBuffer = NULL;

            m_pSearchList->AddData( pNode, i );
         }
      }

      xmlNode.End();
   }

   return ( DONE );
}

int CIVCPFaceImageRes::SubReadXML_A( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );

   if( xmlNode.Start( "Analysis" ) ) {
      for( int i = 0; i < m_iUserNum; i++ ) {
         CXMLNode xmlSubNode( pIO );
         std::string strNode = sformat( "User%d", i );
         if( xmlSubNode.Start( strNode.c_str() ) ) {
            _UserDataInfo* pNode = new _UserDataInfo;
            memset( pNode, 0x00, sizeof( pNode ) );
            memset( pNode->m_szUserID, 0x00, sizeof( pNode->m_szUserID ) );
            memset( pNode->m_szUserName, 0x00, sizeof( pNode->m_szUserName ) );
            memset( pNode->m_szVrifi_ID, 0x00, sizeof( pNode->m_szVrifi_ID ) );
            //memset(pNode->m_strPosition_ID, 0x00, sizeof(pNode->m_strPosition_ID));
            //memset(pNode->m_strBeloging_ID, 0x00, sizeof(pNode->m_strBeloging_ID));
            //memset(pNode->m_strTEL, 0x00, sizeof(pNode->m_strTEL));
            //memset(pNode->m_strHP, 0x00, sizeof(pNode->m_strHP));
            memset( pNode->m_szRequestType, 0x00, sizeof( pNode->m_szRequestType ) );
            memset( pNode->m_szImageFormat, 0x00, sizeof( pNode->m_szImageFormat ) );

            pNode->m_iChannel_no = ( atoi )( m_szResponse_CameraID );

            xmlSubNode.Attribute( TYPE_ID( char ), "User_ID", pNode->m_szVrifi_ID, sizeof( pNode->m_szVrifi_ID ) );
            xmlSubNode.Attribute( TYPE_ID( int ), "Age", &pNode->m_iAge );
            xmlSubNode.Attribute( TYPE_ID( int ), "Gender", &pNode->m_iGender );

            xmlSubNode.End();

            pNode->ImageBuffer = NULL;

            m_pSearchList->AddData( pNode, i );
         }
      }

      xmlNode.End();
   }

   return ( DONE );
}

int CIVCPFaceImageRes::SubReadXML_TO( CXMLIO* pIO, int iIndex )
{
   _UserDataInfo* pInfo = new _UserDataInfo;

   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Similarity" ) ) {
      xmlNode.Attribute( TYPE_ID( float ), "Rate", &pInfo->m_fRate );
      xmlNode.End();
   }

   if( xmlNode.Start( "User" ) ) {
      xmlNode.Attribute( TYPE_ID( float ), "Rate", &pInfo->m_fRate );
      xmlNode.Attribute( TYPE_ID( char ), "UserID", pInfo->m_szUserID, sizeof( pInfo->m_szUserID ) );
      xmlNode.Attribute( TYPE_ID( char ), "UserName", pInfo->m_szUserName, sizeof( pInfo->m_szUserName ) );

      xmlNode.Attribute( TYPE_ID( char ), "Beloging_ID", pInfo->m_strBeloging_ID, sizeof( pInfo->m_strBeloging_ID ) );
      xmlNode.Attribute( TYPE_ID( char ), "Position_ID", pInfo->m_strPosition_ID, sizeof( pInfo->m_strPosition_ID ) );
      xmlNode.Attribute( TYPE_ID( char ), "TEL", pInfo->m_strTEL, sizeof( pInfo->m_strTEL ) );
      xmlNode.Attribute( TYPE_ID( char ), "HP", pInfo->m_strHP, sizeof( pInfo->m_strHP ) );

      xmlNode.Attribute( TYPE_ID( int ), "Age", &pInfo->m_iAge );
      xmlNode.Attribute( TYPE_ID( int ), "Gender", &pInfo->m_iGender );

      xmlNode.End();
   }

   if( xmlNode.Start( "Image" ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "Format", pInfo->m_szImageFormat, strlen( pInfo->m_szImageFormat ) );
      char szSize[20];
      xmlNode.Attribute( TYPE_ID( char ), "Size", szSize, sizeof( szSize ) );
      std::string strData = szSize;
      int iFindLen        = strData.find( "," );
      if( iFindLen != std::string::npos ) {
         pInfo->m_iWidth  = ( atoi )( strData.assign( 1, iFindLen - 1 ).c_str() );
         pInfo->m_iHeight = ( atoi )( strData.assign( iFindLen + 1, strData.length() - 1 ).c_str() );
      }
      xmlNode.Attribute( TYPE_ID( int ), "Length", &pInfo->iSubImageSize );
      xmlNode.End();
   }

   m_pSearchList->AddData( pInfo, iIndex );

   return ( DONE );
}

int CIVCPFaceImageRes::WriteToPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      aPacket.FreeBody();
   }
   if( m_pBodyBuffer ) {
      aPacket.m_Header.m_nOptions    = 0;
      aPacket.m_Header.m_nXMLDataLen = m_nXMLDataLen;
      aPacket.m_Header.m_nBodyLen    = m_nBodyLen;
      aPacket.m_pBody                = m_pBodyBuffer;
      m_pBodyBuffer                  = NULL;
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPFaceImageRes::ReadFromPacket( CIVCPPacket& aPacket )
{
   int iLen = aPacket.m_Header.m_nBodyLen;
   if( iLen <= 0 )
      return ( 1 );
   if( !aPacket.m_pBody )
      return ( 1 );

   m_nXMLDataLen   = aPacket.m_Header.m_nXMLDataLen;
   m_nBodyLen      = aPacket.m_Header.m_nBodyLen;
   m_pBodyBuffer   = aPacket.m_pBody;
   aPacket.m_pBody = NULL;

   ParseBodyBuffer(); // 바로 파싱한다.

   if( m_nXMLDataLen > 0 )
      ParseImageBuffer(); // 바로 이미지 파싱

   return ( DONE );
}

void CIVCPFaceImageRes::ParseBodyBuffer()
{
   FileIO buff;
   buff.Attach( (byte*)m_pBodyBuffer, m_nBodyLen - m_nXMLDataLen );
   CXMLIO xmlIO( &buff, XMLIO_Read );
   if( DONE != ReadXML( &xmlIO ) ) {
      buff.Detach();
      return;
   }
}

void CIVCPFaceImageRes::ParseImageBuffer()
{
   if( !m_pSearchList ) {
      return;
   }

   m_image_buffer = (byte*)malloc( sizeof( byte ) * m_nXMLDataLen );
   memcpy( m_image_buffer, m_pBodyBuffer + ( m_nBodyLen - m_nXMLDataLen ), m_nXMLDataLen );

   int iBuffSize = 0;
   int iCount    = m_pSearchList->GetListCount();
   for( int i = 0; i < iCount; i++ ) {
      CSearchData* pNode    = (CSearchData*)m_pSearchList->GetData( i );
      pNode->m_image_buffer = new byte[pNode->m_iTotalSize];
      memcpy( pNode->m_image_buffer, m_image_buffer + iBuffSize, sizeof( byte ) * pNode->m_iTotalSize );
      iBuffSize += pNode->m_iTotalSize;
   }
}

void CIVCPFaceImageRes::GetImageBuffer( byte* image_buffer )
{
   if( m_pSearchList == NULL )
      return;

   m_iTotSearchNum = m_pSearchList->GetCount();
   if( m_iTotSearchNum <= 0 ) {
      m_iTotalImageSize = 0;
      return;
   }

   m_iTotalImageSize = m_pSearchList->m_iTotalImageSize;

   int iCurLen = 0;

   for( int i = 0; i < m_pSearchList->GetCount(); i++ ) {
      CSearchData* pNode = NULL;
      pNode              = (CSearchData*)m_pSearchList->GetData( i );
      if( pNode && pNode->m_image_buffer && pNode->m_iTotalSize >= 300 ) {
         memcpy( image_buffer + iCurLen, pNode->m_image_buffer, pNode->m_iTotalSize );
         iCurLen += pNode->m_iTotalSize;
      }
   }
}

void CIVCPFaceImageRes::GetImageBuffer( const std::string& strImagePath )
{
   std::string strResponseType = m_szResponseType;
   if( strResponseType.find( "A" ) != std::string::npos ) {
      if( m_pSearchList == NULL )
         return;

      m_iTotSearchNum = m_pSearchList->GetCount();
      if( m_iTotSearchNum <= 0 ) {
         m_iTotalImageSize = 0;
         return;
      }

      m_iTotalImageSize = m_pSearchList->m_iTotalImageSize;

      if( m_image_buffer ) {
         free( m_image_buffer );
         m_image_buffer = NULL;
      }
      m_image_buffer = (byte*)malloc( sizeof( byte ) * m_iTotalImageSize );
      memset( m_image_buffer, 0x00, m_iTotalImageSize );
      int iCurLen = 0;

      for( int i = 0; i < m_pSearchList->GetCount(); i++ ) {
         CSearchData* pNode = NULL;
         pNode              = (CSearchData*)m_pSearchList->GetData( i );
         if( pNode && pNode->m_image_buffer && pNode->m_iTotalSize >= 300 ) {
            memcpy( m_image_buffer + iCurLen, pNode->m_image_buffer, pNode->m_iTotalSize );
            iCurLen += pNode->m_iTotalSize;
         }
      }
   } else if( strResponseType.find( "V" ) != std::string::npos ) {
      FileIO imageFile;
      imageFile.Open( strImagePath.c_str(), FIO_BINARY, FIO_READ );

      m_iImageSize = imageFile.GetLength();
      if( m_image_buffer )
         free( m_image_buffer );
      m_image_buffer = (byte*)malloc( sizeof( byte ) * m_iImageSize );
      memset( m_image_buffer, 0x00, m_iImageSize );

      imageFile.Read( m_image_buffer, 1, sizeof( byte ) * m_iImageSize );
      imageFile.Close();
   } else {
      FileIO imageFile;
      imageFile.Open( strImagePath.c_str(), FIO_BINARY, FIO_READ );

      m_iImageSize = imageFile.GetLength();
      if( m_image_buffer )
         free( m_image_buffer );
      m_image_buffer = (byte*)malloc( sizeof( byte ) * m_iImageSize );
      memset( m_image_buffer, 0x00, m_iTotalImageSize );

      imageFile.Read( m_image_buffer, 1, sizeof( byte ) * m_iImageSize );
      imageFile.Close();
   }
}

void CIVCPFaceImageRes::CreateBodybuffer( byte* pImageBuff, int nImageLen )
{
   if( m_pBodyBuffer ) free( m_pBodyBuffer );
   m_pBodyBuffer = NULL;
   m_nXMLDataLen = 0;
   m_nBodyLen    = 0;

   FileIO buff;
   CXMLIO xmlIO( &buff, XMLIO_Write );
   if( WriteXML( &xmlIO ) ) return;

   m_nXMLDataLen = nImageLen;
   m_nBodyLen    = buff.GetLength() + nImageLen;

   // 먼저 버퍼를 만든다.
   m_pBodyBuffer = (char*)malloc( buff.GetLength() + nImageLen );
   if( m_pBodyBuffer ) {
      memcpy( m_pBodyBuffer, (byte*)buff, buff.GetLength() );

      if( nImageLen )
         memcpy( m_pBodyBuffer + buff.GetLength(), pImageBuff, nImageLen );
   }
}

void CIVCPFaceImageRes::DeleteBodyBuffer()
{
   SAFE_FREE( m_pBodyBuffer );
}

/*********************************************************************
   [jmlee] 
   얼굴 검색을 위한 얼굴인식 서버 연동 패킷 정의 
**********************************************************************/

///////////////////////////////////////////////////////////////////////////////
//
// CIVCPFaceMatchingReq : 얼굴 데이터 인식/인증/분석 요청
//
///////////////////////////////////////////////////////////////////////////////

CIVCPFaceMatchingReq::CIVCPFaceMatchingReq()
{
   m_nPacketID = IVCP_ID_Data_FaceMatchData;
   //strcpy(m_szRequestType, "R");

   m_iNum               = 0;
   m_iWidth             = 0;
   m_iHeight            = 0;
   m_iTN_Length         = 0;
   m_iImage_Length_Max  = 0;
   m_iFFV_Length_Max    = 0;
   m_iTotal_Length      = 0;
   m_iTotal_Data_Length = 0;

   m_pBodyBuffer = NULL;
   m_nXMLDataLen = 0;
}

CIVCPFaceMatchingReq::~CIVCPFaceMatchingReq()
{
   if( m_pBodyBuffer ) free( m_pBodyBuffer );
}

int CIVCPFaceMatchingReq::WriteToPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      aPacket.FreeBody();
   }
   if( m_pBodyBuffer ) {
      aPacket.m_Header.m_nOptions    = 0;
      aPacket.m_Header.m_nXMLDataLen = m_nXMLDataLen;
      aPacket.m_Header.m_nBodyLen    = m_nBodyLen;
      aPacket.m_pBody                = m_pBodyBuffer;
      m_pBodyBuffer                  = NULL;
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPFaceMatchingReq::ReadFromPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      m_nXMLDataLen   = aPacket.m_Header.m_nXMLDataLen;
      m_nBodyLen      = aPacket.m_Header.m_nBodyLen;
      m_pBodyBuffer   = aPacket.m_pBody;
      aPacket.m_pBody = NULL;

      m_iTotal_Length = m_nXMLDataLen;

      ParseBodyBuffer(); // 바로 파싱한다.
      return ( DONE );
   }
   return ( 1 );
}

void CIVCPFaceMatchingReq::ParseBodyBuffer()
{
   FileIO buff;
   buff.Attach( (byte*)m_pBodyBuffer, m_nBodyLen - m_nXMLDataLen );
   CXMLIO xmlIO( &buff, XMLIO_Read );
   if( DONE != ReadXML( &xmlIO ) ) {
      buff.Detach();
      return;
   }

   if( m_nXMLDataLen > 0 ) {
      ParseImageBuffer(); // 바로 이미지 파싱
   }
}

void CIVCPFaceMatchingReq::ParseImageBuffer()
{
   if( strcmp( m_szRequestType, "F" ) == 0 ) {
      m_pFaceImages = (byte*)malloc( sizeof( byte ) * m_nXMLDataLen );
      memcpy( m_pFaceImages, m_pBodyBuffer + ( m_nBodyLen - m_nXMLDataLen ), m_nXMLDataLen );
   } else {
      m_pFaceImage = (byte*)malloc( sizeof( byte ) * m_iTN_Length );
      memcpy( m_pFaceImage, m_pBodyBuffer + ( m_nBodyLen - m_nXMLDataLen ), m_iTN_Length );

      m_pFaceImages = (byte*)malloc( sizeof( byte ) * m_iTotal_Length );
      memcpy( m_pFaceImages, m_pBodyBuffer + ( m_nBodyLen - m_nXMLDataLen ) + m_iTN_Length, m_iTotal_Length );
   }
}

void CIVCPFaceMatchingReq::CreateBodybuffer( byte* pImageBuff, int nImageLen )
{
   if( m_pBodyBuffer ) free( m_pBodyBuffer );
   m_pBodyBuffer = NULL;
   m_nXMLDataLen = 0;
   m_nBodyLen    = 0;

   FileIO buff;
   CXMLIO xmlIO( &buff, XMLIO_Write );
   if( WriteXML( &xmlIO ) ) return;

   m_nXMLDataLen = nImageLen;
   m_nBodyLen    = buff.GetLength() + nImageLen;

   // 먼저 버퍼를 만든다.
   m_pBodyBuffer = (char*)malloc( buff.GetLength() + nImageLen );
   if( m_pBodyBuffer ) {
      memcpy( m_pBodyBuffer, (byte*)buff, buff.GetLength() );

      if( nImageLen )
         memcpy( m_pBodyBuffer + buff.GetLength(), pImageBuff, nImageLen );
   }
}

int CIVCPFaceMatchingReq::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Request" ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "Request_Type", m_szRequestType, sizeof( m_szRequestType ) );
      xmlNode.Attribute( TYPE_ID( char ), "Request_ID", m_szRequest_ID, sizeof( m_szRequest_ID ) );

      if( strcmp( m_szRequestType, "F" ) == 0 ) {
         xmlNode.Attribute( TYPE_ID( char ), "HW_ID", &m_szHWID, sizeof( m_szHWID ) );
         xmlNode.Attribute( TYPE_ID( int ), "System_ID", &m_iSystemID );
         xmlNode.Attribute( TYPE_ID( int ), "Channel_No", &m_iChannelNo );
         xmlNode.Attribute( TYPE_ID( int ), "Object_ID", &m_iObjectID );

         xmlNode.Attribute( TYPE_ID( int ), "Face_Num", &m_iNum );
         xmlNode.Attribute( TYPE_ID( int ), "TN_Max_Length", &m_iImage_Length_Max );
         xmlNode.Attribute( TYPE_ID( int ), "Total_Length", &m_iTotal_Length );

         xmlNode.Attribute( TYPE_ID( char ), "Date", &m_szRequest_Date, sizeof( m_szRequest_Date ) );
      } else if( strcmp( m_szRequestType, "M" ) == 0 ) {
         CXMLNode xmlSubNode( pIO );
         if( xmlSubNode.Start( "Image" ) ) {
            xmlSubNode.Attribute( TYPE_ID( int ), "TN_W_H_Size", &m_iWidth );
            m_iHeight = m_iWidth;
            xmlSubNode.Attribute( TYPE_ID( int ), "TN_Length", &m_iTN_Length );
            xmlSubNode.End();
         }

         if( xmlSubNode.Start( "Data" ) ) {
            xmlSubNode.Attribute( TYPE_ID( int ), "Face_Num", &m_iNum );
            xmlSubNode.Attribute( TYPE_ID( int ), "IMG_Max_Length", &m_iImage_Length_Max );
            xmlSubNode.Attribute( TYPE_ID( int ), "FFV_Max_Length", &m_iFFV_Length_Max );
            xmlSubNode.End();
         }
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPFaceMatchingReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Request" ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "Request_Type", m_szRequestType, sizeof( m_szRequestType ) );
      xmlNode.Attribute( TYPE_ID( char ), "Request_ID", m_szRequest_ID, sizeof( m_szRequest_ID ) );

      if( strcmp( m_szRequestType, "F" ) == 0 ) {
         xmlNode.Attribute( TYPE_ID( char ), "HW_ID", &m_szHWID, sizeof( m_szHWID ) );
         xmlNode.Attribute( TYPE_ID( int ), "System_ID", &m_iSystemID );
         xmlNode.Attribute( TYPE_ID( int ), "Channel_No", &m_iChannelNo );
         xmlNode.Attribute( TYPE_ID( int ), "Object_ID", &m_iObjectID );

         xmlNode.Attribute( TYPE_ID( int ), "Face_Num", &m_iNum );
         xmlNode.Attribute( TYPE_ID( int ), "TN_Max_Length", &m_iImage_Length_Max );
         xmlNode.Attribute( TYPE_ID( int ), "Total_Length", &m_iTotal_Length );

         xmlNode.Attribute( TYPE_ID( char ), "Date", &m_szRequest_Date, sizeof( m_szRequest_Date ) );
      } else if( strcmp( m_szRequestType, "M" ) == 0 ) {
         CXMLNode xmlSubNode( pIO );
         if( xmlSubNode.Start( "Image" ) ) {
            xmlSubNode.Attribute( TYPE_ID( int ), "TN_W_H_Size", &m_iWidth );
            xmlSubNode.Attribute( TYPE_ID( int ), "TN_Length", &m_iTN_Length );
            xmlSubNode.End();
         }

         if( xmlSubNode.Start( "Data" ) ) {
            xmlSubNode.Attribute( TYPE_ID( int ), "Face_Num", &m_iNum );
            xmlSubNode.Attribute( TYPE_ID( int ), "IMG_Max_Length", &m_iImage_Length_Max );
            xmlSubNode.Attribute( TYPE_ID( int ), "FFV_Max_Length", &m_iFFV_Length_Max );
            xmlSubNode.End();
         }
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

void CIVCPFaceMatchingReq::DeleteBodyBuffer()
{
   SAFE_FREE( m_pBodyBuffer );
}

///////////////////////////////////////////////////////////////////////////////
//
// CIVCPFaceMatchingRes : 얼굴 데이터 매칭 응답
//
///////////////////////////////////////////////////////////////////////////////

CIVCPFaceMatchingRes::CIVCPFaceMatchingRes()
{
   m_nPacketID    = IVCP_ID_Data_FaceUserData;
   m_image_buffer = NULL;
   m_nXMLDataLen  = 0;
}

CIVCPFaceMatchingRes::~CIVCPFaceMatchingRes()
{
   if( m_image_buffer ) {
      free( m_image_buffer );
      m_image_buffer = NULL;
   }
}

int CIVCPFaceMatchingRes::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Response" ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "Response_Type", m_szResponseType, sizeof( m_szResponseType ) );
      xmlNode.Attribute( TYPE_ID( char ), "Response_ID", m_szResponse_ID, sizeof( m_szResponse_ID ) );

      if( strcmp( m_szResponseType, "F" ) == 0 ) {
         xmlNode.Attribute( TYPE_ID( char ), "Result", m_szResult, sizeof( m_szResult ) );
      } else {
         xmlNode.Attribute( TYPE_ID( float ), "Max_Similarity", &m_fMaxSimilarity );
      }

      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPFaceMatchingRes::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Response" ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "Response_Type", m_szResponseType, sizeof( m_szResponseType ) );
      xmlNode.Attribute( TYPE_ID( char ), "Response_ID", m_szResponse_ID, sizeof( m_szResponse_ID ) );

      if( strcmp( m_szResponseType, "F" ) == 0 ) {
         xmlNode.Attribute( TYPE_ID( int ), "Total_Length", &m_iTotal_Length );
      } else {
         xmlNode.Attribute( TYPE_ID( float ), "Max_Similarity", &m_fMaxSimilarity );
      }

      xmlNode.End();
      return ( DONE );
   }

   return ( DONE );
}

void CIVCPFaceMatchingRes::CreateBodybuffer( byte* pImageBuff, int nImageLen )
{
   if( m_pBodyBuffer ) free( m_pBodyBuffer );
   m_pBodyBuffer = NULL;
   m_nXMLDataLen = 0;
   m_nBodyLen    = 0;

   FileIO buff;
   CXMLIO xmlIO( &buff, XMLIO_Write );
   if( WriteXML( &xmlIO ) ) return;

   if( strcmp( m_szResponseType, "F" ) == 0 ) {
      m_nXMLDataLen = nImageLen;
      m_nBodyLen    = buff.GetLength() + nImageLen;

      // 먼저 버퍼를 만든다.
      m_pBodyBuffer = (char*)malloc( buff.GetLength() + nImageLen );
      if( m_pBodyBuffer ) {
         memcpy( m_pBodyBuffer, (byte*)buff, buff.GetLength() );

         if( nImageLen )
            memcpy( m_pBodyBuffer + buff.GetLength(), pImageBuff, nImageLen );
      }

      m_iTotal_Length = m_nXMLDataLen;
   } else {
      m_nBodyLen = buff.GetLength();
   }
}

int CIVCPFaceMatchingRes::WriteToPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      aPacket.FreeBody();
   }

   if( m_pBodyBuffer ) {
      aPacket.m_Header.m_nOptions    = 0;
      aPacket.m_Header.m_nXMLDataLen = m_nXMLDataLen;
      aPacket.m_Header.m_nBodyLen    = m_nBodyLen;
      aPacket.m_pBody                = m_pBodyBuffer;
      m_pBodyBuffer                  = NULL;
      return ( DONE );
   }

   return ( 1 );
}

int CIVCPFaceMatchingRes::ReadFromPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      m_nXMLDataLen   = aPacket.m_Header.m_nXMLDataLen;
      m_nBodyLen      = aPacket.m_Header.m_nBodyLen;
      m_pBodyBuffer   = aPacket.m_pBody;
      aPacket.m_pBody = NULL;

      ParseBodyBuffer(); // 바로 파싱한다.

      if( m_nXMLDataLen > 0 ) {
         m_iTotal_Length = m_nXMLDataLen;
         ParseImageBuffer(); // 바로 이미지 파싱
      }
      return ( DONE );
   }
   return ( 1 );
}

void CIVCPFaceMatchingRes::ParseBodyBuffer()
{
   FileIO buff;
   buff.Attach( (byte*)m_pBodyBuffer, m_nBodyLen - m_nXMLDataLen );
   CXMLIO xmlIO( &buff, XMLIO_Read );

   if( DONE != ReadXML( &xmlIO ) ) {
      buff.Detach();
      return;
   }
}

void CIVCPFaceMatchingRes::ParseImageBuffer()
{
   if( m_nXMLDataLen < 0 )
      return;

   if( m_image_buffer ) {
      free( m_image_buffer );
      m_image_buffer = NULL;
   }

   m_image_buffer = (byte*)malloc( sizeof( byte ) * m_nXMLDataLen );
   memcpy( m_image_buffer, m_pBodyBuffer + ( m_nBodyLen - m_nXMLDataLen ), m_nXMLDataLen );
}

void CIVCPFaceMatchingRes::DeleteBodyBuffer()
{
   SAFE_FREE( m_pBodyBuffer );
}

/*******************************************************************
[jmlee] 
서버 연동 사진 기반 검색 요청
*******************************************************************/

CIVCPFaceDCSearchAndMatchingReq::CIVCPFaceDCSearchAndMatchingReq()
{
   m_nPacketID                 = IVCP_ID_Data_FaceMatchData;
   m_nXMLDataLen               = 0;
   m_nBodyLen                  = 0;
   m_pBodyBuffer               = NULL; // 전달용 버퍼
   m_pFaceImage                = NULL; // 사용자 인식용 얼굴 이미지
   m_nImageLength              = 0;
   m_nSize                     = 0;
   m_nCount                    = 0;
   m_pSearchQueryXMLBuffer     = NULL;
   m_nSearchQueryXMLBufferSize = 0;
}

CIVCPFaceDCSearchAndMatchingReq::~CIVCPFaceDCSearchAndMatchingReq()
{
}

int CIVCPFaceDCSearchAndMatchingReq::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Request" ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "Request_Type", m_szRequestType, sizeof( m_szRequestType ) );
      xmlNode.Attribute( TYPE_ID( float ), "SimilarityThreshold", &m_fSimilarityThreshold );
      xmlNode.Attribute( TYPE_ID( int ), "Total_Length", &m_nTotalLen );
      xmlNode.Attribute( TYPE_ID( char ), "HW_ID", &m_szHWID, sizeof( m_szHWID ) );

      CXMLNode xmlNode( pIO );
      if( xmlNode.Start( "Image" ) ) {
         xmlNode.Attribute( TYPE_ID( int ), "Size", &m_nSize );
         xmlNode.Attribute( TYPE_ID( int ), "Length", &m_nImageLength );
         xmlNode.End();
      }

      if( xmlNode.Start( "Search_Data" ) ) {
         xmlNode.Attribute( TYPE_ID( int ), "Count", &m_nCount );
         xmlNode.Attribute( TYPE_ID( int ), "Length", &m_nSearchQueryXMLBufferSize );
         xmlNode.End();
      }

      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPFaceDCSearchAndMatchingReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Request" ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "Request_Type", m_szRequestType, sizeof( m_szRequestType ) );
      xmlNode.Attribute( TYPE_ID( float ), "SimilarityThreshold", &m_fSimilarityThreshold );
      xmlNode.Attribute( TYPE_ID( int ), "Total_Length", &m_nTotalLen );
      xmlNode.Attribute( TYPE_ID( char ), "HW_ID", &m_szHWID, sizeof( m_szHWID ) );

      CXMLNode xmlNode( pIO );
      if( xmlNode.Start( "Image" ) ) {
         xmlNode.Attribute( TYPE_ID( int ), "Size", &m_nSize );
         xmlNode.Attribute( TYPE_ID( int ), "Length", &m_nImageLength );
         xmlNode.End();
      }

      if( xmlNode.Start( "Search_Data" ) ) {
         xmlNode.Attribute( TYPE_ID( int ), "Count", &m_nCount );
         xmlNode.Attribute( TYPE_ID( int ), "Length", &m_nSearchQueryXMLBufferSize );
         xmlNode.End();
      }

      xmlNode.End();
      return ( DONE );
   }

   return ( 1 );
}

void CIVCPFaceDCSearchAndMatchingReq::CreateBodybuffer( byte* pImageBuff, int nImageLen, byte* pSearchDataBuffer, int nSearchDataLen )
{
   if( m_pBodyBuffer ) free( m_pBodyBuffer );
   m_pBodyBuffer = NULL;
   m_nXMLDataLen = 0;
   m_nBodyLen    = 0;

   FileIO buff;
   CXMLIO xmlIO( &buff, XMLIO_Write );
   if( WriteXML( &xmlIO ) ) return;

   m_nXMLDataLen = nImageLen + nSearchDataLen;
   m_nBodyLen    = buff.GetLength() + nImageLen + nSearchDataLen;

   // 먼저 버퍼를 만든다.
   m_pBodyBuffer = (char*)malloc( buff.GetLength() + nImageLen + nSearchDataLen );
   if( m_pBodyBuffer && pSearchDataBuffer ) {
      memcpy( m_pBodyBuffer, (byte*)buff, buff.GetLength() );
      if( nImageLen )
         memcpy( m_pBodyBuffer + buff.GetLength(), pImageBuff, nImageLen );
      if( nSearchDataLen )
         memcpy( m_pBodyBuffer + buff.GetLength() + nImageLen, pSearchDataBuffer, nSearchDataLen );
      m_nTotalLen = m_nXMLDataLen;
   } else {
      m_nBodyLen = buff.GetLength();
   }
}

int CIVCPFaceDCSearchAndMatchingReq::WriteToPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      aPacket.FreeBody();
   }

   if( m_pBodyBuffer ) {
      aPacket.m_Header.m_nOptions    = 0;
      aPacket.m_Header.m_nXMLDataLen = m_nXMLDataLen;
      aPacket.m_Header.m_nBodyLen    = m_nBodyLen;
      aPacket.m_pBody                = (char*)malloc( m_nBodyLen );
      memcpy( aPacket.m_pBody, m_pBodyBuffer, m_nBodyLen );
      return ( DONE );
   }

   return ( 1 );
}

int CIVCPFaceDCSearchAndMatchingReq::ReadFromPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      m_nXMLDataLen   = aPacket.m_Header.m_nXMLDataLen;
      m_nBodyLen      = aPacket.m_Header.m_nBodyLen;
      m_pBodyBuffer   = aPacket.m_pBody;
      aPacket.m_pBody = NULL;

      ParseBodyBuffer(); // 바로 파싱한다.

      if( m_nXMLDataLen > 0 ) {
         m_nTotalLen = m_nXMLDataLen;
         ParseImageBuffer(); // 바로 이미지 파싱
      }
      return ( DONE );
   }
   return ( 1 );
}

void CIVCPFaceDCSearchAndMatchingReq::ParseBodyBuffer()
{
   FileIO buff;
   buff.Attach( (byte*)m_pBodyBuffer, m_nBodyLen - m_nXMLDataLen );
   CXMLIO xmlIO( &buff, XMLIO_Read );

   if( DONE != ReadXML( &xmlIO ) ) {
      buff.Detach();
      return;
   }
}

void CIVCPFaceDCSearchAndMatchingReq::ParseImageBuffer()
{
   if( m_nXMLDataLen < 0 )
      return;

   if( m_pFaceImage ) {
      free( m_pFaceImage );
      m_pFaceImage = NULL;
   }

   m_pFaceImage = (byte*)malloc( sizeof( byte ) * m_nImageLength );
   memcpy( m_pFaceImage, m_pBodyBuffer + m_nBodyLen, m_nImageLength );

   if( m_pSearchQueryXMLBuffer ) {
      free( m_pSearchQueryXMLBuffer );
      m_pSearchQueryXMLBuffer = NULL;
   }

   m_pSearchQueryXMLBuffer = (byte*)malloc( sizeof( byte ) * m_nSearchQueryXMLBufferSize );
   memcpy( m_pSearchQueryXMLBuffer, m_pBodyBuffer + m_nBodyLen + m_nImageLength, m_nSearchQueryXMLBufferSize );
}

void CIVCPFaceDCSearchAndMatchingReq::DeleteBodyBuffer()
{
   SAFE_FREE( m_pBodyBuffer );
}

/*******************************************************************
[jmlee] 
서버 연동 사진 기반 검색 응답
*******************************************************************/

CIVCPFaceDCSearchAndMatchingRes::CIVCPFaceDCSearchAndMatchingRes()
{
   m_nPacketID   = IVCP_ID_Data_FaceMatchData;
   m_nXMLDataLen = 0;
   m_nBodyLen    = 0;
   m_pBodyBuffer = NULL; // 전달용 버퍼

   m_nCount                    = 0;
   m_pSearchQueryXMLBuffer     = NULL;
   m_nSearchQueryXMLBufferSize = 0;
}

CIVCPFaceDCSearchAndMatchingRes::~CIVCPFaceDCSearchAndMatchingRes()
{
}

int CIVCPFaceDCSearchAndMatchingRes::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Reponse" ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "Response_Type", m_szResponseType, sizeof( m_szResponseType ) );
      xmlNode.Attribute( TYPE_ID( int ), "Count", &m_nCount );
      xmlNode.Attribute( TYPE_ID( int ), "Length", &m_nTotalLen );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPFaceDCSearchAndMatchingRes::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Reponse" ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "Response_Type", m_szResponseType, sizeof( m_szResponseType ) );
      xmlNode.Attribute( TYPE_ID( int ), "Count", &m_nCount );
      xmlNode.Attribute( TYPE_ID( int ), "Length", &m_nTotalLen );
      xmlNode.End();
      return ( DONE );
   }

   return ( 1 );
}

void CIVCPFaceDCSearchAndMatchingRes::CreateBodybuffer( byte* pSearchDataBuffer, int nSearchDataLen )
{
   if( m_pBodyBuffer ) free( m_pBodyBuffer );
   m_pBodyBuffer = NULL;
   m_nXMLDataLen = 0;
   m_nBodyLen    = 0;

   FileIO buff;
   CXMLIO xmlIO( &buff, XMLIO_Write );
   if( WriteXML( &xmlIO ) ) return;

   m_nXMLDataLen = nSearchDataLen;
   m_nBodyLen    = buff.GetLength() + nSearchDataLen;

   // 먼저 버퍼를 만든다.
   m_pBodyBuffer = (char*)malloc( buff.GetLength() + nSearchDataLen );
   if( m_pBodyBuffer && pSearchDataBuffer ) {
      memcpy( m_pBodyBuffer, (byte*)buff, buff.GetLength() );
      if( nSearchDataLen )
         memcpy( m_pBodyBuffer + buff.GetLength(), pSearchDataBuffer, nSearchDataLen );
      m_nTotalLen = m_nXMLDataLen;
   } else {
      m_nBodyLen = buff.GetLength();
   }
}

int CIVCPFaceDCSearchAndMatchingRes::WriteToPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      aPacket.FreeBody();
   }

   if( m_pBodyBuffer ) {
      aPacket.m_Header.m_nOptions    = 0;
      aPacket.m_Header.m_nXMLDataLen = m_nXMLDataLen;
      aPacket.m_Header.m_nBodyLen    = m_nBodyLen;
      aPacket.m_pBody                = m_pBodyBuffer;
      m_pBodyBuffer                  = NULL;
      return ( DONE );
   }

   return ( 1 );
}

int CIVCPFaceDCSearchAndMatchingRes::ReadFromPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      m_nXMLDataLen   = aPacket.m_Header.m_nXMLDataLen;
      m_nBodyLen      = aPacket.m_Header.m_nBodyLen;
      m_pBodyBuffer   = aPacket.m_pBody;
      aPacket.m_pBody = NULL;

      ParseBodyBuffer(); // 바로 파싱한다.

      if( m_nXMLDataLen > 0 ) {
         m_nTotalLen = m_nXMLDataLen;
         ParseImageBuffer(); // 바로 이미지 파싱
      }
      return ( DONE );
   }
   return ( 1 );
}

void CIVCPFaceDCSearchAndMatchingRes::ParseBodyBuffer()
{
   FileIO buff;
   buff.Attach( (byte*)m_pBodyBuffer, m_nBodyLen - m_nXMLDataLen );
   CXMLIO xmlIO( &buff, XMLIO_Read );

   if( DONE != ReadXML( &xmlIO ) ) {
      buff.Detach();
      return;
   }
}

void CIVCPFaceDCSearchAndMatchingRes::ParseImageBuffer()
{
   if( m_nXMLDataLen < 0 )
      return;

   if( m_pSearchQueryXMLBuffer ) {
      free( m_pSearchQueryXMLBuffer );
      m_pSearchQueryXMLBuffer = NULL;
   }

   m_nSearchQueryXMLBufferSize = m_nXMLDataLen;

   m_pSearchQueryXMLBuffer = (byte*)malloc( sizeof( byte ) * m_nSearchQueryXMLBufferSize );
   memcpy( m_pSearchQueryXMLBuffer, m_pBodyBuffer + ( m_nBodyLen - m_nXMLDataLen ), m_nSearchQueryXMLBufferSize );
}

void CIVCPFaceDCSearchAndMatchingRes::DeleteBodyBuffer()
{
   SAFE_FREE( m_pBodyBuffer );
}
#endif // __SUPPORT_IVCP_FACE

#ifdef __IVCP_SUPPORT_MMS

///////////////////////////////////////////////////////////////////////////////
//
// CIVCPMMSLoginRes
//
///////////////////////////////////////////////////////////////////////////////

CIVCPMMSLoginRes::CIVCPMMSLoginRes()
{
   m_nPacketID    = IVCP_ID_MMS_LoginRes;
   m_nLoginResult = 0;
   m_nGroupLevel  = 1;
}
int CIVCPMMSLoginRes::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( _T("Login") ) ) {
      xmlNode.Attribute( TYPE_ID( int ), _T("Result"), &m_nLoginResult );
      xmlNode.Attribute( TYPE_ID( int ), _T("GroupLevel"), &m_nGroupLevel );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPMMSLoginRes::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( _T("Login") ) ) {
      xmlNode.Attribute( TYPE_ID( int ), _T("Result"), &m_nLoginResult );
      xmlNode.Attribute( TYPE_ID( int ), _T("GroupLevel"), &m_nGroupLevel );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// CIVCPMMSFailoverConnInfoNotify : 통합관리서버 페일오버 접속정보 알림
//
///////////////////////////////////////////////////////////////////////////////

CIVCPMMSFailoverConnInfoNotify::CIVCPMMSFailoverConnInfoNotify()
{
   m_nPacketID = IVCP_ID_MMS_MMS_Failover_Conn_Info_Notify;
   m_szMMSPrimaryAddr.clear();
   m_szMMSStandbyAddr.clear();
}

int CIVCPMMSFailoverConnInfoNotify::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( _T("MMSFailoverConnInfo") ) ) {
      xmlNode.Attribute( TYPE_ID( string ), _T("MMSPrimaryAddr"), &m_szMMSPrimaryAddr );
      xmlNode.Attribute( TYPE_ID( string ), _T("MMSStandbyAddr"), &m_szMMSStandbyAddr );
      xmlNode.End();
      return ( DONE );
   }

   return ( 1 );
}

int CIVCPMMSFailoverConnInfoNotify::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( _T("MMSFailoverConnInfo") ) ) {
      xmlNode.Attribute( TYPE_ID( string ), _T("MMSPrimaryAddr"), &m_szMMSPrimaryAddr );
      xmlNode.Attribute( TYPE_ID( string ), _T("MMSStandbyAddr"), &m_szMMSStandbyAddr );
      xmlNode.End();
      return ( DONE );
   }

   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// CIVCPUserAccount_Notify : 사용자 계정 정보 전달
//
///////////////////////////////////////////////////////////////////////////////

CIVCPUserAccount_Notify::CIVCPUserAccount_Notify()
{
   m_nPacketID = IVCP_ID_MMS_UserAccount_Notify;
}

void CIVCPUserAccount_Notify::AddUserAccountInfo( USER_ACCOUNT_INFO* pInfo )
{
   m_listUserAccountInfo.push_back( *pInfo );
}

void CIVCPUserAccount_Notify::SetUserAccountInfoList( std::list<USER_ACCOUNT_INFO>* pInfoList )
{
   if( pInfoList )
      m_listUserAccountInfo = *pInfoList;
}

void CIVCPUserAccount_Notify::Clear()
{
   m_listUserAccountInfo.clear();
}

void CIVCPUserAccount_Notify::GetUserInfoList( OUT std::list<USER_ACCOUNT_INFO>* pInfoList )
{
   if( pInfoList )
      *pInfoList = m_listUserAccountInfo;
}

int CIVCPUserAccount_Notify::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( _T("UserAccount") ) ) {
      if( !m_listUserAccountInfo.empty() )
         m_listUserAccountInfo.clear();

      INT nUserAccountCount = 0;

      xmlNode.Attribute( TYPE_ID( INT ), _T("UserAccountInfoCount"), &nUserAccountCount );

      USER_ACCOUNT_INFO UserAccountInfo;
      for( UINT i = 0; i < nUserAccountCount; i++ ) {
         CXMLNode xmlNode2( pIO );
         if( xmlNode2.Start( _T("UserAccountInfo") ) ) {
            xmlNode2.Attribute( TYPE_ID( string ), _T("UserID"), &UserAccountInfo.m_szUserID );
            xmlNode2.Attribute( TYPE_ID( string ), _T("Password"), &UserAccountInfo.m_szPassword );
            xmlNode2.Attribute( TYPE_ID( string ), _T("Description"), &UserAccountInfo.m_szDescription );
            xmlNode2.Attribute( TYPE_ID( CHAR ), _T("Level"), &UserAccountInfo.m_cLevel );
            xmlNode2.Attribute( TYPE_ID( CHAR ), _T("UserType"), &UserAccountInfo.m_cUserType );

            xmlNode2.End();

            m_listUserAccountInfo.push_back( UserAccountInfo );
         }
      }

      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPUserAccount_Notify::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( _T("UserAccount") ) ) {
      INT nUserAccountCount = m_listUserAccountInfo.size();

      xmlNode.Attribute( TYPE_ID( INT ), _T("UserAccountInfoCount"), &nUserAccountCount );

      USER_ACCOUNT_INFO UserAccountInfo;
      std::list<USER_ACCOUNT_INFO>::iterator pIter;
      for( pIter = m_listUserAccountInfo.begin(); pIter != m_listUserAccountInfo.end(); pIter++ ) {
         UserAccountInfo = *pIter;

         CXMLNode xmlNode2( pIO );
         if( xmlNode2.Start( _T("UserAccountInfo") ) ) {
            xmlNode2.Attribute( TYPE_ID( string ), _T("UserID"), &UserAccountInfo.m_szUserID );
            xmlNode2.Attribute( TYPE_ID( string ), _T("Password"), &UserAccountInfo.m_szPassword );
            xmlNode2.Attribute( TYPE_ID( string ), _T("Description"), &UserAccountInfo.m_szDescription );
            xmlNode2.Attribute( TYPE_ID( CHAR ), _T("Level"), &UserAccountInfo.m_cLevel );
            xmlNode2.Attribute( TYPE_ID( CHAR ), _T("UserType"), &UserAccountInfo.m_cUserType );

            xmlNode2.End();
         }
      }

      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// CIVCPSystemStatusReq : 시스템 상태 정보 요청
//
///////////////////////////////////////////////////////////////////////////////

CIVCPSystemStatusInfo_Req::CIVCPSystemStatusInfo_Req()
{
   m_nPacketID = IVCP_ID_MMS_SystemStatusInfo_Req;
   m_bStart    = FALSE;
}

int CIVCPSystemStatusInfo_Req::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( _T("SystemStatusStart") ) ) {
      xmlNode.Attribute( TYPE_ID( BOOL ), _T("SystemStatusStart"), &m_bStart );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPSystemStatusInfo_Req::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( _T("SystemStatusStart") ) ) {
      xmlNode.Attribute( TYPE_ID( BOOL ), _T("SystemStatusStart"), &m_bStart );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// CIVCPSystemStatusInfo_Res : 시스템 상태 정보 응답
//
///////////////////////////////////////////////////////////////////////////////

CIVCPSystemStatusInfo_Res::CIVCPSystemStatusInfo_Res()
{
   m_nPacketID    = IVCP_ID_MMS_SystemStatusInfo_Res;
   m_cCPUUsage    = 0;
   m_nMemoryUsage = 0;
   m_nMemorySize  = 0;
   m_nHDDUsage    = 0;
   m_nHDDSize     = 0;
}

int CIVCPSystemStatusInfo_Res::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( _T("SystemSatatusInfo") ) ) {
      xmlNode.Attribute( TYPE_ID( BYTE ), _T("CPUUsage"), &m_cCPUUsage );
      xmlNode.Attribute( TYPE_ID( UINT ), _T("MemoryUsage"), &m_nMemoryUsage );
      xmlNode.Attribute( TYPE_ID( UINT ), _T("MemorySize"), &m_nMemorySize );
      xmlNode.Attribute( TYPE_ID( UINT ), _T("HDDUsage"), &m_nHDDUsage );
      xmlNode.Attribute( TYPE_ID( UINT ), _T("HDDSize"), &m_nHDDSize );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPSystemStatusInfo_Res::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( _T("SystemSatatusInfo") ) ) {
      xmlNode.Attribute( TYPE_ID( BYTE ), _T("CPUUsage"), &m_cCPUUsage );
      xmlNode.Attribute( TYPE_ID( UINT ), _T("MemoryUsage"), &m_nMemoryUsage );
      xmlNode.Attribute( TYPE_ID( UINT ), _T("MemorySize"), &m_nMemorySize );
      xmlNode.Attribute( TYPE_ID( UINT ), _T("HDDUsage"), &m_nHDDUsage );
      xmlNode.Attribute( TYPE_ID( UINT ), _T("HDDSize"), &m_nHDDSize );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// CIVCPNVRServer_ConnectInfo_Req : 저장/분배서버 접속정보 요청
//
///////////////////////////////////////////////////////////////////////////////

CIVCPNVRServer_ConnectInfo_Req::CIVCPNVRServer_ConnectInfo_Req()
{
   m_nPacketID = IVCP_ID_MMS_NVRServer_ConnectInfo_Req;
}

///////////////////////////////////////////////////////////////////////////////
//
// CIVCPNVRServer_ConnectInfo_Res : 저장/분배서버 접속정보 응답
//
///////////////////////////////////////////////////////////////////////////////

CIVCPNVRServer_ConnectInfo_Res::CIVCPNVRServer_ConnectInfo_Res()
{
   m_nPacketID = IVCP_ID_MMS_NVRServer_ConnectInfo_Res;
}

void CIVCPNVRServer_ConnectInfo_Res::AddConnectInfo( NVRSERVER_INFO* pInfo )
{
   if( pInfo )
      m_listNVRServerInfo.push_back( *pInfo );
}

void CIVCPNVRServer_ConnectInfo_Res::Clear()
{
   if( !m_listNVRServerInfo.empty() )
      m_listNVRServerInfo.clear();
}

int CIVCPNVRServer_ConnectInfo_Res::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( _T("NVRServer_ConnectInfo") ) ) {
      if( !m_listNVRServerInfo.empty() )
         m_listNVRServerInfo.clear();

      INT nNVRServerCount = 0;
      xmlNode.Attribute( TYPE_ID( INT ), _T("ServerCount"), &nNVRServerCount );

      NVRSERVER_INFO ServerInfo;
      for( UINT i = 0; i < nNVRServerCount; i++ ) {
         CXMLNode xmlNode2( pIO );
         if( xmlNode2.Start( _T("NVRServerInfo") ) ) {
            xmlNode2.Attribute( TYPE_ID( string ), _T("ServerName"), &ServerInfo.m_szNVRServerName );
            xmlNode2.Attribute( TYPE_ID( string ), _T("IPAddr"), &ServerInfo.m_szIPAddr );
            xmlNode2.Attribute( TYPE_ID( INT ), _T("Port"), &ServerInfo.m_nPort );
            xmlNode2.Attribute( TYPE_ID( string ), _T("Description"), &ServerInfo.m_szDescription );
            xmlNode2.End();
         }

         m_listNVRServerInfo.push_back( ServerInfo );
      }

      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CIVCPNVRServer_ConnectInfo_Res::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( _T("NVRServer_ConnectInfo") ) ) {
      INT nNVRServerCount = 0;
      if( !m_listNVRServerInfo.empty() )
         nNVRServerCount = m_listNVRServerInfo.size();

      xmlNode.Attribute( TYPE_ID( INT ), _T("ServerCount"), &nNVRServerCount );

      NVRSERVER_INFO ServerInfo;
      std::list<NVRSERVER_INFO>::iterator pIter;
      for( pIter = m_listNVRServerInfo.begin(); pIter != m_listNVRServerInfo.end(); pIter++ ) {
         CXMLNode xmlNode2( pIO );
         if( xmlNode2.Start( _T("NVRServerInfo") ) ) {
            ServerInfo = *pIter;

            xmlNode2.Attribute( TYPE_ID( string ), _T("ServerName"), &ServerInfo.m_szNVRServerName );
            xmlNode2.Attribute( TYPE_ID( string ), _T("IPAddr"), &ServerInfo.m_szIPAddr );
            xmlNode2.Attribute( TYPE_ID( INT ), _T("Port"), &ServerInfo.m_nPort );
            xmlNode2.Attribute( TYPE_ID( string ), _T("Description"), &ServerInfo.m_szDescription );
            xmlNode2.End();
         }
      }

      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

/////////////////////////////////////////////////////////////////////////////////////////
//
// IVCP_ID_MMS_NVRServer_ConfigFile_Transfer_Req : 저장/분배서버 설정 파일 전송 요청
//
/////////////////////////////////////////////////////////////////////////////////////////

CIVCPNVRServer_ConfigFile_Transfer_Req::CIVCPNVRServer_ConfigFile_Transfer_Req()
{
   m_nPacketID = IVCP_ID_MMS_NVRServer_ConfigFile_Transfer_Req;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
// IVCP_ID_MMS_NVRServer_ConfigFile_Transfer_Res : 저장/분배서버 설정 전송 응답 (XML 파일전송)
//
/////////////////////////////////////////////////////////////////////////////////////////

CIVCPNVRServer_ConfigFile_Transfer_Res::CIVCPNVRServer_ConfigFile_Transfer_Res()
{
   m_nPacketID       = IVCP_ID_MMS_NVRServer_ConfigFile_Transfer_Res;
   m_nTotalFileCount = 0;
   m_nCurFileIndex   = -1;
   m_pBodyBuffer     = NULL;
   m_nXMLDataLen     = 0;
}

CIVCPNVRServer_ConfigFile_Transfer_Res::~CIVCPNVRServer_ConfigFile_Transfer_Res()
{
   if( m_pBodyBuffer ) {
      free( m_pBodyBuffer );
      m_pBodyBuffer = NULL;
   }
}

int CIVCPNVRServer_ConfigFile_Transfer_Res::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( _T("NVRServer_ConfigFileInfo") ) ) {
      INT nNVRServerCount = 0;
      xmlNode.Attribute( TYPE_ID( INT ), _T("TotalFileCount"), &m_nTotalFileCount );
      xmlNode.Attribute( TYPE_ID( INT ), _T("CurFileIndex"), &m_nCurFileIndex );
      xmlNode.Attribute( TYPE_ID( string ), _T("CurFileName"), &m_szCurFileName );
      //xmlNode.Attribute (TYPE_ID (string), _T("CurFileContent"), &m_szCurFileContent);

      xmlNode.End();
      return ( DONE );
   }

   return ( 1 );
}

int CIVCPNVRServer_ConfigFile_Transfer_Res::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( _T("NVRServer_ConfigFileInfo") ) ) {
      INT nNVRServerCount = 0;
      xmlNode.Attribute( TYPE_ID( INT ), _T("TotalFileCount"), &m_nTotalFileCount );
      xmlNode.Attribute( TYPE_ID( INT ), _T("CurFileIndex"), &m_nCurFileIndex );
      xmlNode.Attribute( TYPE_ID( string ), _T("CurFileName"), &m_szCurFileName );
      //xmlNode.Attribute (TYPE_ID (string), _T("CurFileContent"), &m_szCurFileContent);

      xmlNode.End();
      return ( DONE );
   }

   return ( 1 );
}

int CIVCPNVRServer_ConfigFile_Transfer_Res::ReadFromPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      m_nXMLDataLen   = aPacket.m_Header.m_nBinaryOptionalLen;
      m_nBodyLen      = aPacket.m_Header.m_nBodyLen;
      m_pBodyBuffer   = aPacket.m_pBody;
      aPacket.m_pBody = NULL;

      ParseBodyBuffer(); // 바로 파싱한다.
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPNVRServer_ConfigFile_Transfer_Res::WriteToPacket( CIVCPPacket& aPacket )
{
   if( aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody ) {
      aPacket.FreeBody();
   }
   if( m_pBodyBuffer ) {
      aPacket.m_Header.m_nOptions    = 0;
      aPacket.m_Header.m_nBinaryOptionalLen = m_nXMLDataLen;
      aPacket.m_Header.m_nBodyLen    = m_nBodyLen;
      aPacket.m_pBody                = (char*)malloc( m_nBodyLen );
      memcpy( aPacket.m_pBody, m_pBodyBuffer, m_nBodyLen );
      return ( DONE );
   }
   return ( 1 );
}

void CIVCPNVRServer_ConfigFile_Transfer_Res::ParseBodyBuffer()
{
   FileIO buff;
   buff.Attach( (byte*)m_pBodyBuffer, m_nXMLDataLen );
   CXMLIO xmlIO( &buff, XMLIO_Read );
   if( DONE != ReadXML( &xmlIO ) ) {
      buff.Detach();
      return;
   }
}

void CIVCPNVRServer_ConfigFile_Transfer_Res::CreateBodybuffer( byte* pEncodedBuff, int nEncodedLen )
{
   if( m_pBodyBuffer ) {
      free( m_pBodyBuffer );
      m_pBodyBuffer = NULL;
   }
   m_nXMLDataLen = 0;
   m_nBodyLen    = 0;

   FileIO buff;
   CXMLIO xmlIO( &buff, XMLIO_Write );
   if( WriteXML( &xmlIO ) ) return;

   //m_nDataLen = buff.GetLength();;
   //m_nBodyLen = m_nDataLen + nEncodedLen;
   m_nXMLDataLen = nEncodedLen;
   m_nBodyLen    = buff.GetLength() + nEncodedLen;

   // 먼저 버퍼를 만든다.
   //m_pBodyBuffer = (char*)malloc(m_nDataLen + nEncodedLen);
   INT nBodyBufferSize = buff.GetLength() + nEncodedLen;
   m_pBodyBuffer       = (char*)malloc( nBodyBufferSize );
   if( m_pBodyBuffer ) {
      //memcpy(m_pBodyBuffer, (byte*)buff, m_nDataLen);
      //memcpy(m_pBodyBuffer+m_nDataLen, pEncodedBuff, nEncodedLen);
      memset( m_pBodyBuffer, '\0', nBodyBufferSize );
      //memcpy(m_pBodyBuffer, (byte*)buff, buff.GetLength());
      memcpy( m_pBodyBuffer, buff.GetBuffer(), buff.GetLength() );
      if( nEncodedLen )
         memcpy( m_pBodyBuffer + buff.GetLength(), pEncodedBuff, nEncodedLen );
   }
}

void CIVCPNVRServer_ConfigFile_Transfer_Res::DeleteBodyBuffer()
{
   SAFE_FREE( m_pBodyBuffer );
}

///////////////////////////////////////////////////////////////////////////////
//
// CIVCPGracefulCloseNotify : 시스템 정상 종료 메시지 전달
//
///////////////////////////////////////////////////////////////////////////////

CIVCPGracefulCloseNotify::CIVCPGracefulCloseNotify()
{
   m_nPacketID = IVCP_ID_MMS_NVRServer_GracefulClose_Notify;
}

///////////////////////////////////////////////////////////////////////////////
//
// CIVCPNVRServer_FailOver_Notify : 저장/분배서버 페일오버 알림
//
///////////////////////////////////////////////////////////////////////////////

CIVCPNVRServer_FailOver_Notify::CIVCPNVRServer_FailOver_Notify()
{
   m_nPacketID = IVCP_ID_MMS_NVRServer_FailOver_Notify;
}

void CIVCPNVRServer_FailOver_Notify::SetFailOverInfo( NVRSERVER_FAILOVER_INFO* pInfo )
{
   if( pInfo )
      m_NVRServerFailOverInfo = *pInfo;
}

void CIVCPNVRServer_FailOver_Notify::GetFailOverInfo( OUT NVRSERVER_FAILOVER_INFO* pInfo )
{
   if( pInfo )
      *pInfo = m_NVRServerFailOverInfo;
}

int CIVCPNVRServer_FailOver_Notify::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( _T("NVRServer_FailOverInfo") ) ) {
      xmlNode.Attribute( TYPE_ID( string ), _T("ServerName"), &m_NVRServerFailOverInfo.m_szNVRServerName );
      xmlNode.Attribute( TYPE_ID( string ), _T("IPAddr_Primary"), &m_NVRServerFailOverInfo.m_szIPAddr_Primary );
      xmlNode.Attribute( TYPE_ID( string ), _T("IPAddr_Standby"), &m_NVRServerFailOverInfo.m_szIPAddr_Standby );
      xmlNode.Attribute( TYPE_ID( INT ), _T("Port"), &m_NVRServerFailOverInfo.m_nPort );
      xmlNode.Attribute( TYPE_ID( string ), _T("Description"), &m_NVRServerFailOverInfo.m_szDescription );

      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPNVRServer_FailOver_Notify::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( _T("NVRServer_FailOverInfo") ) ) {
      xmlNode.Attribute( TYPE_ID( string ), _T("ServerName"), &m_NVRServerFailOverInfo.m_szNVRServerName );
      xmlNode.Attribute( TYPE_ID( string ), _T("IPAddr_Primary"), &m_NVRServerFailOverInfo.m_szIPAddr_Primary );
      xmlNode.Attribute( TYPE_ID( string ), _T("IPAddr_Standby"), &m_NVRServerFailOverInfo.m_szIPAddr_Standby );
      xmlNode.Attribute( TYPE_ID( INT ), _T("Port"), &m_NVRServerFailOverInfo.m_nPort );
      xmlNode.Attribute( TYPE_ID( string ), _T("Description"), &m_NVRServerFailOverInfo.m_szDescription );

      xmlNode.End();
      return ( DONE );
   }

   return ( 1 );
}

CIVCPVMS_KeepAlive_Notify::CIVCPVMS_KeepAlive_Notify()
{
   m_nPacketID = IVCP_ID_MMS_KeepAlive_Notify;

   memset( m_szGuid, '\0', sizeof( m_szGuid ) );
}

int CIVCPVMS_KeepAlive_Notify::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( _T("VRS_Keep_Alive") ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "GUID", m_szGuid, NULL, GUID_LENGTH + 1 /*31*/ );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPVMS_KeepAlive_Notify::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( _T("VRS_Keep_Alive") ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "GUID", m_szGuid, NULL, GUID_LENGTH + 1 /*31*/ );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

CIVCConnectInfo::CIVCConnectInfo()
{
   m_nPacketID   = IVCP_ID_VRS_Connect_Information;
   m_nPort       = 0;
   m_szAddr[0]   = 0;
   m_szId[0]     = 0;
   m_szPasswd[0] = 0;
   m_szGuid[0]   = 0;

   memset( m_szStandbyVRS_Addr, '\0', sizeof( m_szStandbyVRS_Addr ) );
   memset( m_szStandbyVRS_Guid, '\0', sizeof( m_szStandbyVRS_Guid ) );
   memset( m_szStandbyVRS_Id, '\0', sizeof( m_szStandbyVRS_Id ) );
   memset( m_szStandbyVRS_Passwd, '\0', sizeof( m_szStandbyVRS_Passwd ) );
   m_nStandbyVRS_Port = 0;
}

int CIVCConnectInfo::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( _T("VRS_Connect_infor") ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "MRS_IPADDR", m_szAddr, NULL, 16 );
      xmlNode.Attribute( TYPE_ID( int ), "MRS_PORT", &m_nPort );
      xmlNode.Attribute( TYPE_ID( char ), "MRS_ID", m_szId, NULL, 31 );
      xmlNode.Attribute( TYPE_ID( char ), "MRS_PASSWD", m_szPasswd, NULL, 31 );
      xmlNode.Attribute( TYPE_ID( char ), "MRS_GUID", m_szGuid, NULL, GUID_LENGTH + 1 /*31*/ );

      xmlNode.Attribute( TYPE_ID( char ), "STANDBY_IPADDR", m_szStandbyVRS_Addr, NULL, 16 );
      xmlNode.Attribute( TYPE_ID( int ), "STANDBY_PORT", &m_nStandbyVRS_Port );
      xmlNode.Attribute( TYPE_ID( char ), "STANDBY_ID", m_szStandbyVRS_Id, NULL, 31 );
      xmlNode.Attribute( TYPE_ID( char ), "STANDBY_PASSWD", m_szStandbyVRS_Passwd, NULL, 31 );
      xmlNode.Attribute( TYPE_ID( char ), "STANDBY_GUID", m_szStandbyVRS_Guid, NULL, GUID_LENGTH + 1 /*31*/ );

      xmlNode.Attribute( TYPE_ID( char ), "SYSTEM_GUID", m_szSystemGuid, NULL, GUID_LENGTH + 1 /*31*/ );

      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CIVCConnectInfo::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( _T("VRS_Connect_infor") ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "MRS_IPADDR", m_szAddr, NULL, 16 );
      xmlNode.Attribute( TYPE_ID( int ), "MRS_PORT", &m_nPort );
      xmlNode.Attribute( TYPE_ID( char ), "MRS_ID", m_szId, NULL, 31 );
      xmlNode.Attribute( TYPE_ID( char ), "MRS_PASSWD", m_szPasswd, NULL, 31 );
      xmlNode.Attribute( TYPE_ID( char ), "MRS_GUID", m_szGuid, NULL, GUID_LENGTH + 1 /*31*/ );

      xmlNode.Attribute( TYPE_ID( char ), "STANDBY_IPADDR", m_szStandbyVRS_Addr, NULL, 16 );
      xmlNode.Attribute( TYPE_ID( int ), "STANDBY_PORT", &m_nStandbyVRS_Port );
      xmlNode.Attribute( TYPE_ID( char ), "STANDBY_ID", m_szStandbyVRS_Id, NULL, 31 );
      xmlNode.Attribute( TYPE_ID( char ), "STANDBY_PASSWD", m_szStandbyVRS_Passwd, NULL, 31 );
      xmlNode.Attribute( TYPE_ID( char ), "STANDBY_GUID", m_szStandbyVRS_Guid, NULL, GUID_LENGTH + 1 /*31*/ );

      xmlNode.Attribute( TYPE_ID( char ), "SYSTEM_GUID", m_szSystemGuid, NULL, GUID_LENGTH + 1 /*31*/ );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

CIVCPXMLData::CIVCPXMLData()
{
   m_nPacketID   = IVCP_ID_Unknown;
   m_pBodyBuffer = NULL;
}

CIVCPXMLData::~CIVCPXMLData()
{
   if( m_pBodyBuffer ) {
      free( m_pBodyBuffer );
      m_pBodyBuffer = NULL;
   }
}

void CIVCPXMLData::CreateBodybuffer( byte* pEncodedBuff, int nEncodedLen )
{
   if( m_pBodyBuffer ) delete m_pBodyBuffer;

   m_pBodyBuffer = (char*)malloc( nEncodedLen );
   if( m_pBodyBuffer ) {
      memcpy( m_pBodyBuffer, pEncodedBuff, nEncodedLen );
   }
}

void CIVCPXMLData::DeleteBodyBuffer()
{
   SAFE_FREE( m_pBodyBuffer );
}

CVCWPLiveVideoStreamData::CVCWPLiveVideoStreamData()
{
   m_nCameraUID = -1;
   m_nStreamIdx = 0;
   m_nCodecID   = 0;
   m_nWidth     = 0;
   m_nHeight    = 0;
   m_fFrameRate = 0.0f;
   m_nFrameFlag = 0;
}

int CVCWPLiveVideoStreamData::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "LiveVideoStreamData" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "CameraUid", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "StreamIdx", &m_nStreamIdx );
      xmlNode.Attribute( TYPE_ID( int ), "CodecId", &m_nCodecID );
      xmlNode.Attribute( TYPE_ID( int ), "Width", &m_nWidth );
      xmlNode.Attribute( TYPE_ID( int ), "Height", &m_nHeight );
      xmlNode.Attribute( TYPE_ID( float ), "FrameRate", &m_fFrameRate );
      xmlNode.Attribute( TYPE_ID( int ), "FrameFlag", &m_nFrameFlag );
      xmlNode.Attribute( TYPE_ID( int ), "DataSize", &m_nDataSize );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CVCWPLiveVideoStreamData::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "LiveVideoStreamData" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "GUID", &m_cCameraGUID );
      xmlNode.Attribute( TYPE_ID( int ), "CameraUid", &m_nCameraUID );
      xmlNode.Attribute( TYPE_ID( int ), "StreamIdx", &m_nStreamIdx );
      xmlNode.Attribute( TYPE_ID( int ), "CodecId", &m_nCodecID );
      xmlNode.Attribute( TYPE_ID( int ), "Width", &m_nWidth );
      xmlNode.Attribute( TYPE_ID( int ), "Height", &m_nHeight );
      xmlNode.Attribute( TYPE_ID( float ), "FrameRate", &m_fFrameRate );
      xmlNode.Attribute( TYPE_ID( int ), "FrameFlag", &m_nFrameFlag );
      xmlNode.Attribute( TYPE_ID( int ), "DataSize", &m_nDataSize );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// CVCWPConnCameraListRes
//
///////////////////////////////////////////////////////////////////////////////

CVCWPConnCameraListRes::CVCWPConnCameraListRes()
{
   memset( m_cSystemGUID, '\0', sizeof( m_cSystemGUID ) );
   memset( m_cSystemIPAddr, '\0', sizeof( m_cSystemIPAddr ) );
}

int CVCWPConnCameraListRes::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "CameraList" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Start", &m_nStart );
      xmlNode.Attribute( TYPE_ID( int ), "Count", &m_nCount );
      xmlNode.Attribute( TYPE_ID( char ), "SystemGUID", m_cSystemGUID, sizeof( m_cSystemGUID ) );
      xmlNode.Attribute( TYPE_ID( char ), "SystemIPAddr", m_cSystemIPAddr, sizeof( m_cSystemIPAddr ) );
      //TRACE(_T("[CVCWPConnCameraListRes::ReadXML] Count[%d] SystemGUID[%s] SystemIPAddr[%s]\n"), m_nCount, m_cSystemGUID, m_cSystemIPAddr);
      ClearCameras();
      for( int i = 0; i < m_nCount; i++ ) {
         CIVCPCameraInfo* pCameraInfo = new CIVCPCameraInfo();
         pCameraInfo->ReadXML( pIO );
         m_CameraList.push_back( pCameraInfo );
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}
int CVCWPConnCameraListRes::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "CameraList" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Start", &m_nStart );
      xmlNode.Attribute( TYPE_ID( int ), "Count", &m_nCount );
      xmlNode.Attribute( TYPE_ID( char ), "SystemGUID", m_cSystemGUID, sizeof( m_cSystemGUID ) );
      xmlNode.Attribute( TYPE_ID( char ), "SystemIPAddr", m_cSystemIPAddr, sizeof( m_cSystemIPAddr ) );
      for( int i = 0; i < m_nCount; i++ ) {
         m_CameraList[i]->WriteXML( pIO );
      }
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// CIVCPCameraInfoList
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraInfoList::CIVCPCameraInfoList()
{
   memset( m_cSystemGUID, '\0', sizeof( m_cSystemGUID ) );
   memset( m_cSystemIPAddr, '\0', sizeof( m_cSystemIPAddr ) );
   m_nCameraCount = 0;
}

CIVCPCameraInfoList::~CIVCPCameraInfoList()
{
   Clear();
}

int CIVCPCameraInfoList::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "CameraInfoList" ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "SystemGUID", m_cSystemGUID, sizeof( m_cSystemGUID ) );
      xmlNode.Attribute( TYPE_ID( char ), "SystemIPAddr", m_cSystemIPAddr, sizeof( m_cSystemIPAddr ) );
      xmlNode.Attribute( TYPE_ID( int ), "CameraCount", &m_nCameraCount ); // 카메라 카운트

      CIVCPCameraInfo* pCameraInfo = NULL;
      for( INT i = 0; i < m_nCameraCount; i++ ) {
         pCameraInfo = new CIVCPCameraInfo;
         if( DONE == pCameraInfo->ReadXML( pIO ) ) {
            m_list.push_back( pCameraInfo );
         }
      }

      xmlNode.End();
      return ( DONE );
   }

   return ( 1 );
}

int CIVCPCameraInfoList::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "CameraInfoList" ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "SystemGUID", m_cSystemGUID, sizeof( m_cSystemGUID ) );
      xmlNode.Attribute( TYPE_ID( char ), "SystemIPAddr", m_cSystemIPAddr, sizeof( m_cSystemIPAddr ) );
      xmlNode.Attribute( TYPE_ID( int ), "CameraCount", &m_nCameraCount ); // 카메라 카운트

      //std::list<CIVCPCameraInfo>::iterator pIter;
      std::list<VOID*>::iterator pIter;
      CIVCPCameraInfo* pCameraInfo = NULL;

      for( pIter = m_list.begin(); pIter != m_list.end(); pIter++ ) {
         //pIter->WriteXML(pIO);
         pCameraInfo = (CIVCPCameraInfo*)( *pIter );
         pCameraInfo->WriteXML( pIO );
      }

      xmlNode.End();
      return ( DONE );
   }

   return ( 1 );
}

void CIVCPCameraInfoList::Clear()
{
   if( !m_list.empty() ) {
      std::list<VOID*>::iterator pIter;
      CIVCPCameraInfo* pCameraInfo = NULL;
      for( pIter = m_list.begin(); pIter != m_list.end(); pIter++ ) {
         pCameraInfo = (CIVCPCameraInfo*)( *pIter );
         if( pCameraInfo )
            delete pCameraInfo;
      }

      m_list.clear();
   }

   memset( m_cSystemGUID, '\0', sizeof( m_cSystemGUID ) );
   memset( m_cSystemIPAddr, '\0', sizeof( m_cSystemIPAddr ) );
   m_nCameraCount = 0;
}

///////////////////////////////////////////////////////////////////////////////
//
// CIVCPConnCameraListSetReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPConnCameraListSetReq::CIVCPConnCameraListSetReq()
{
   m_nPacketID = IVCP_ID_VMS_Conn_CameraListSetReq;
   m_nStart    = 0;
   m_nCount    = 0;
}

int CIVCPConnCameraListSetReq::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "CameraList" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Start", &m_nStart );
      xmlNode.Attribute( TYPE_ID( int ), "Count", &m_nCount );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPConnCameraListSetReq::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "CameraList" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "Start", &m_nStart );
      xmlNode.Attribute( TYPE_ID( int ), "Count", &m_nCount );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// CVCWPConnCameraListSetRes
//
///////////////////////////////////////////////////////////////////////////////

CVCWPConnCameraListSetRes::CVCWPConnCameraListSetRes()
{
   m_nPacketID     = IVCP_ID_VMS_Conn_CameraListSetRes;
   m_nListSetCount = 0;
}

CVCWPConnCameraListSetRes::~CVCWPConnCameraListSetRes()
{
   Clear();
}

int CVCWPConnCameraListSetRes::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "CameraListSet" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "CameraListSetCount", &m_nListSetCount ); // 카메라리스트Set 카운트

      for( INT i = 0; i < m_nListSetCount; i++ ) {
         /*
         CIVCPCameraInfoList CameraInfoList;
         if (DONE == CameraInfoList.ReadXML(pIO))
            m_listCameraInfoSet.push_back(CameraInfoList);
*/
         CIVCPCameraInfoList* pCameraInfoList = new CIVCPCameraInfoList;
         if( DONE == pCameraInfoList->ReadXML( pIO ) )
            m_listCameraInfoSet.push_back( pCameraInfoList );
      }

      xmlNode.End();
      return ( DONE );
   }

   return ( 1 );
}

int CVCWPConnCameraListSetRes::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "CameraListSet" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "CameraListSetCount", &m_nListSetCount ); // 카메라리스트Set 카운트

      //std::list<CIVCPCameraInfoList>::iterator pIter;
      std::list<VOID*>::iterator pIter;
      CIVCPCameraInfoList* pCameraInfoList = NULL;
      for( pIter = m_listCameraInfoSet.begin(); pIter != m_listCameraInfoSet.end(); pIter++ ) {
         //pIter->WriteXML(pIO);
         pCameraInfoList = (CIVCPCameraInfoList*)( *pIter );
         pCameraInfoList->WriteXML( pIO );
      }

      xmlNode.End();
      return ( DONE );
   }

   return ( 1 );
}

void CVCWPConnCameraListSetRes::Clear()
{
   if( !m_listCameraInfoSet.empty() ) {
      CIVCPCameraInfoList* pCameraInfoList = NULL;
      std::list<VOID*>::iterator pIter;
      for( pIter = m_listCameraInfoSet.begin(); pIter != m_listCameraInfoSet.end(); pIter++ ) {
         pCameraInfoList = (CIVCPCameraInfoList*)( *pIter );
         if( pCameraInfoList )
            delete pCameraInfoList;
      }
   }

   m_listCameraInfoSet.clear();
}

int CIVCPCarPlateAlarmInfo::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "AlarmInfo" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "AlarmId", &m_nAlarmId );
      xmlNode.Attribute( TYPE_ID( char ), "AlarmName", m_szAlarmName, sizeof( m_szAlarmName ) );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPCarPlateAlarmInfo::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "AlarmInfo" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "AlarmId", &m_nAlarmId );
      xmlNode.Attribute( TYPE_ID( char ), "AlarmName", m_szAlarmName, sizeof( m_szAlarmName ) );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

CIVCPCarPlateHotListInfo::CIVCPCarPlateHotListInfo()
{
   m_nHotInfoId    = 0;
   m_nAlarmPriorty = 0;
   m_nAlarmId      = 0;
}

int CIVCPCarPlateHotListInfo::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "HotInfo" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "HotInfoId", &m_nHotInfoId );
      xmlNode.Attribute( TYPE_ID( string ), "CarPlatNo", &m_szCarPlateNo );
      xmlNode.Attribute( TYPE_ID( string ), "CarOwner", &m_szCarOwner );
      xmlNode.Attribute( TYPE_ID( string ), "CarModel", &m_szCarModel );
      xmlNode.Attribute( TYPE_ID( string ), "CarColor", &m_szCarColor );
      xmlNode.Attribute( TYPE_ID( string ), "CompetentDep", &m_szCompetentDep );
      xmlNode.Attribute( TYPE_ID( string ), "RegLocate", &m_szRegLocate );
      xmlNode.Attribute( TYPE_ID( string ), "Description", &m_szDescription );
      xmlNode.Attribute( TYPE_ID( int ), "AlarmPriorty", &m_nAlarmPriorty );
      xmlNode.Attribute( TYPE_ID( int ), "AlarmId", &m_nAlarmId );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPCarPlateHotListInfo::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "HotInfo" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "HotInfoId", &m_nHotInfoId );
      xmlNode.Attribute( TYPE_ID( string ), "CarPlatNo", &m_szCarPlateNo );
      xmlNode.Attribute( TYPE_ID( string ), "CarOwner", &m_szCarOwner );
      xmlNode.Attribute( TYPE_ID( string ), "CarModel", &m_szCarModel );
      xmlNode.Attribute( TYPE_ID( string ), "CarColor", &m_szCarColor );
      xmlNode.Attribute( TYPE_ID( string ), "CompetentDep", &m_szCompetentDep );
      xmlNode.Attribute( TYPE_ID( string ), "RegLocate", &m_szRegLocate );
      xmlNode.Attribute( TYPE_ID( string ), "Description", &m_szDescription );
      xmlNode.Attribute( TYPE_ID( int ), "AlarmPriorty", &m_nAlarmPriorty );
      xmlNode.Attribute( TYPE_ID( int ), "AlarmId", &m_nAlarmId );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

CIVCPAlarmListInfo::CIVCPAlarmListInfo()
{
}

CIVCPAlarmListInfo::~CIVCPAlarmListInfo()
{
   ClearInfo();
}

int CIVCPAlarmListInfo::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "AlarmList" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "AlarmCnt", &m_nAlarmCnt );
      ClearInfo();
      for( int i = 0; i < m_nAlarmCnt; i++ ) {
         CIVCPCarPlateAlarmInfo* pAlarmInfo = new CIVCPCarPlateAlarmInfo();
         pAlarmInfo->ReadXML( pIO );
         m_AlarmList.push_back( pAlarmInfo );
      }
      xmlNode.End();
      return ( DONE );
   }
   return 1;
}

int CIVCPAlarmListInfo::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "AlarmList" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "AlarmCnt", &m_nAlarmCnt );
      for( int i = 0; i < m_nAlarmCnt; i++ ) {
         m_AlarmList[i]->WriteXML( pIO );
      }
      xmlNode.End();
      return ( DONE );
   }
   return 1;
}

void CIVCPAlarmListInfo::ClearInfo()
{
   int nAlarmNum = m_AlarmList.size();
   for( int i = 0; i < nAlarmNum; i++ ) {
      CIVCPCarPlateAlarmInfo* pAlarmInfo = m_AlarmList[i];
      if( pAlarmInfo ) delete pAlarmInfo;
   }
   m_AlarmList.clear();
}

CIVCPHotListListInfo::CIVCPHotListListInfo()
{
}

CIVCPHotListListInfo::~CIVCPHotListListInfo()
{
   ClearInfo();
}

int CIVCPHotListListInfo::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "HotList" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "HotListCnt", &m_nHListCnt );
      ClearInfo();
      for( int i = 0; i < m_nHListCnt; i++ ) {
         CIVCPCarPlateHotListInfo* pHisInfo = new CIVCPCarPlateHotListInfo();
         pHisInfo->ReadXML( pIO );
         m_HotList.push_back( pHisInfo );
      }
      xmlNode.End();
      return ( DONE );
   }
   return 1;
}

int CIVCPHotListListInfo::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "HotList" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "HotListCnt", &m_nHListCnt );
      for( int i = 0; i < m_nHListCnt; i++ ) {
         m_HotList[i]->WriteXML( pIO );
      }
      xmlNode.End();
      return ( DONE );
   }
   return 1;
}

void CIVCPHotListListInfo::ClearInfo()
{
   int nHotInfoNum = m_HotList.size();
   for( int i = 0; i < nHotInfoNum; i++ ) {
      CIVCPCarPlateHotListInfo* pHotInfo = m_HotList[i];
      if( pHotInfo ) delete pHotInfo;
   }
   m_HotList.clear();
}

int CIVCPCarPlateHotList::ReadXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "HotListInfo" ) ) {
      m_AlarmList.ReadXML( pIO );
      m_HotList.ReadXML( pIO );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CIVCPCarPlateHotList::WriteXML( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "HotListInfo" ) ) {
      m_AlarmList.WriteXML( pIO );
      m_HotList.WriteXML( pIO );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

#endif

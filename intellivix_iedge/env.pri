message("qmake === [$$TEMPLATE] $$TARGET ===")

# remove qt's dependencies.
QT -= gui core
CONFIG -= app_bundle qt qpa import_plugins import_qpa_plugin

# choose lib static or shared.
equals(TEMPLATE, lib):CONFIG += staticlib # to enable static lib.

# In shared lib case, plugin makes *.so instead of soname, *.so.1.2.3
equals(TEMPLATE, lib):CONFIG(shared, shared|staticlib):CONFIG += unversioned_libname unversioned_soname

# add compile support from QT.
CONFIG += c++11 link_pkgconfig thread

# debug define.
CONFIG(debug, debug|release): DEFINES += _DEBUG



#############################################################################################
linux-g++-64 | linux-g++ {
   message( Desktop : linux-g++-64 ) 
   
   # smart build location.
   CONFIG(debug, debug|release):  DESTDIR = "$$_PRO_FILE_PWD_/../build-debug-linux-x86_64"
   else:CONFIG(force_debug_info): DESTDIR = "$$_PRO_FILE_PWD_/../build-profile-linux-x86_64"
   else:                          DESTDIR = "$$_PRO_FILE_PWD_/../build-release-linux-x86_64"
   
   # defines. 
   #DEFINES += __SUPPORT_LICENSE_CERTIFY
   DEFINES += __CPP11
   DEFINES += __SUPPORT_PTZ_CAMERA
   #DEFINES += __SUPPORT_GRAY_IMAGE_USE
   DEFINES += SUPPORT_PTHREAD_NAME

   # defines libs.
   DEFINES += __LIB_FFMPEG
   DEFINES += __LIB_JPEG
   DEFINES += __LIB_OPENCV
   DEFINES += __LIB_CAFFE 
   #DEFINES += __LIB_PNG
   #DEFINES += __LIB_ZLIB
   #DEFINES += __LIB_SIMPLEWEBSOCKET # require boost or asio.
   DEFINES += __LIB_CASABLANCA # require boost
   DEFINES += __LIB_OPENSSL
   DEFINES += __LIB_BOOST

   # for centOS
   INCLUDEPATH += /usr/include/ffmpeg
   INCLUDEPATH += /usr/local/include

   equals(TEMPLATE, app) # App.
   {
      QMAKE_LFLAGS += -rdynamic # for symbols. 

      # internal static lib.
      LIBS += -L$$DESTDIR
      LIBS += -Wl,--start-group -lIVCP -lVCM -lLive555 -lCommon -lVACL -lBCCL -Wl,--end-group   # link with grouping staticlibs.

      # external shared lib.
      contains( DEFINES, __LIB_CASABLANCA ):LIBS += -L/usr/local/lib/ -lcpprest # casablanca
      contains( DEFINES, __LIB_OPENCV ):LIBS += -lopencv_imgproc -lopencv_ml -lopencv_objdetect -lopencv_calib3d -lopencv_video -lopencv_core    # opencv
      contains( DEFINES, __LIB_FFMPEG ):LIBS += -lswresample -lavutil -lavformat -lswscale -lavcodec                                             # ffmpeg 
      contains( DEFINES, __LIB_OPENSSL ):LIBS += -lssl -lcrypto # openssl
      contains( DEFINES, __LIB_BOOST ):LIBS += -lboost_system -lboost_thread -lboost_coroutine -lboost_context # boost
      contains( DEFINES, __LIB_CAFFE ):LIBS += -lcaffe # lcaffe

      LIBS += -ljpeg -lpng
      LIBS += -lpugixml
      LIBS += -lrt
      #LIBS += -ldl # for api dlopen
   }
}

#############################################################################################
 # smart build location. 'Shaddow build' option in QtCreator's project MUST BE TURNED OFF.
OBJECTS_DIR     = $$DESTDIR/.output/$$TARGET
MOC_DIR         = $$DESTDIR/.output/$$TARGET
UI_DIR          = $$DESTDIR/.output/$$TARGET
RCC_DIR         = $$DESTDIR/.output/$$TARGET

# inc. lib. header only.
INCLUDEPATH += $$PWD/../third_party/plog/include
contains( DEFINES, __LIB_SIMPLEWEBSOCKET ): INCLUDEPATH += $$PWD/../third_party/Simple-WebSocket-Server # require boost or asio.







######### examples ############

#message ( OUT_PWD: $$OUT_PWD )
#message ( PWD: $$PWD )

#QMAKE_RPATHDIR +=$$PWD/../third_party/
#message(QMAKE_RPATHDIR = $$QMAKE_RPATHDIR)

#QMAKE_CFLAGS_STATIC_LIB=
#QMAKE_CXXFLAGS_STATIC_LIB=
#message(QMAKE_CXXFLAGS_STATIC_LIB = $${QMAKE_CXXFLAGS_STATIC_LIB} )

# using pkg-config instead of LIB +=
#PKGCONFIG += opencv

# check.
#for( it, $$list( $$enumerate_vars() ) ):message([$$TARGET]ALL -> $$it = $$eval( $$it ) );
#message([$$TARGET]QT-> $${QT})
#message([$$TARGET]CONFIG-> $${CONFIG})

# compile & link options.
#QMAKE_LFLAGS += -Wl,--start-group

# to speed up build.
#CONFIG += precompile_header
#PRECOMPILED_HEADER = precompiled_header.h


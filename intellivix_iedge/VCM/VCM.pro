TARGET = VCM
TEMPLATE = lib
include( ../env.pri )


INCLUDEPATH += ../BCCL
INCLUDEPATH += ../VACL
INCLUDEPATH += ../Common
INCLUDEPATH += ../Live555

DEPENDPATH += ../BCCL
DEPENDPATH += ../VACL
DEPENDPATH += ../Common
DEPENDPATH += ../Live555

PRE_TARGETDEPS += $$DESTDIR/libBCCL.a
PRE_TARGETDEPS += $$DESTDIR/libVACL.a
PRE_TARGETDEPS += $$DESTDIR/libCommon.a
PRE_TARGETDEPS += $$DESTDIR/libLive555.a

HEADERS = \
   $$PWD/Camera_File.h \
   $$PWD/Camera_IP.h \
   $$PWD/Camera_VirtualPTZ.h \
   $$PWD/CameraInfo.h \
   $$PWD/CameraProductAndItsProperties.h \
   $$PWD/CompressImageBuffer.h \
   $$PWD/ContAbsPosTrack.h \
   $$PWD/DayNightDetection.h \
   $$PWD/DigitalIO.h \
   $$PWD/ICamera.h \
   $$PWD/IPCameraManager.h \
   $$PWD/IPCamStream.h \
   $$PWD/PTZArea.h \
   $$PWD/PTZCtrl.h \
   $$PWD/PTZCtrlEx.h \
   $$PWD/PTZVector.h \
   $$PWD/PTZxD.h \
   $$PWD/Quaternion.h \
   $$PWD/VCM.h \
   $$PWD/VCM_Define.h \
   $$PWD/VideoChannel.h \
   $$PWD/VideoChannelManager.h \
   $$PWD/IPCamStream_RTSP.h \
   $$PWD/PelcoDProtocol.h \
   $$PWD/PTZCtrl_Cynix.h \
   $$PWD/PTZCtrl_Linking.h \
   $$PWD/PTZCtrl_Virtual.h \
   $$PWD/PTZProtocol.h \
   $$PWD/stdafx.h \
   $$PWD/PTZCtrl_NIKO.h \

#   $$PWD/ui/GenericChildDialog.h \
#   $$PWD/ui/resource_VCM.h \
#   $$PWD/ui/SetupDialog.h \
#   $$PWD/ui/SetupDialog_VideoChannel.h \
#   $$PWD/ui/SetupDialog_VideoChannelEdit.h \
#   $$PWD/ui/SetupIPCameraDlg.h \
#   $$PWD/ui/SetupIPCameraStreamDlg.h \
#   $$PWD/ui/SetupPTZControlDlg.h

SOURCES = \
   $$PWD/Camera_File.cpp \
   $$PWD/Camera_IP.cpp \
   $$PWD/Camera_VirtualPTZ.cpp \
   $$PWD/CameraInfo.cpp \
   $$PWD/CameraProductAndItsProperties.cpp \
   $$PWD/CompressImageBuffer.cpp \
   $$PWD/ContAbsPosTrack.cpp \
   $$PWD/DayNightDetection.cpp \
   $$PWD/DigitalIO.cpp \
   $$PWD/ICamera.cpp \
   $$PWD/IPCameraManager.cpp \
   $$PWD/IPCamStream.cpp \
   $$PWD/IPCamStream_RTSP.cpp \
   $$PWD/PelcoDProtocol.cpp \
   $$PWD/PTZArea.cpp \
   $$PWD/PTZCtrl.cpp \
   $$PWD/PTZCtrl_Cynix.cpp \
   $$PWD/PTZCtrl_Linking.cpp \
   $$PWD/PTZCtrl_Virtual.cpp \
   $$PWD/PTZCtrlEx.cpp \
   $$PWD/PTZProtocol.cpp \
   $$PWD/PTZxD.cpp \
   $$PWD/Quaternion.cpp \
   $$PWD/stdafx.cpp \
   $$PWD/VCM.cpp \
   $$PWD/VCM_Define.cpp \
   $$PWD/VideoChannel.cpp \
   $$PWD/VideoChannelManager.cpp \
   $$PWD/PTZCtrl_NIKO.cpp \

#   $$PWD/ui/GenericChildDialog.cpp \
#   $$PWD/ui/SetupDialog.cpp \
#   $$PWD/ui/SetupDialog_VideoChannel.cpp \
#   $$PWD/ui/SetupDialog_VideoChannelEdit.cpp \
#   $$PWD/ui/SetupIPCameraDlg.cpp \
#   $$PWD/ui/SetupIPCameraStreamDlg.cpp \
#   $$PWD/ui/SetupPTZControlDlg.cpp


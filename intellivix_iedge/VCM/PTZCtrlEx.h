﻿#pragma once

#ifdef __SUPPORT_PTZ_CAMERA

#include "PTZVector.h"
#include "PTZCtrl.h"

#include <map>


extern BOOL g_bIRLightCtrl;

class CPTZCtrl;

///////////////////////////////////////////////////////////////////////////////
//
// Enumerations
//
///////////////////////////////////////////////////////////////////////////////


enum MapF2FType  // Map Type Float to Float
{
   MapF2FType_ZoomReal2HW,        // "실제 줌배율" 에 대응되는 "하드웨어 줌 값"
   MapF2FType_Zoom2FL,            // "줌배율"에 대응되는 "초점거리". 이때의 초점거리는 대응하는 줌배율로 나눈 값이다.
   MapF2FType_PanAngleVel2HWSpd,  // "팬 각속도"에 대응되는 "하드웨어 팬 값"
   MapF2FType_TiltAngleVel2HWSpd, // "틸트 각속도"에 대응되는 "하드웨어 틸트 값"
   MapF2FType_ZoomLogVel2HWSpd,   // "로그를 취한 줌속도" 에 대응하는 "하드웨어 속도"  --> 현재 사용하지 않음.
   MapF2FType_GotoAbsVel2HWSpd,   // 절대각 이동에 대한 각속도에 대응되는 값
   MapF2FType_Zoom2PTSpdRatio,    // 1x 줌을 기준의 속도를 기준으로 각 줌배율에서의 속도의 비를 나타냄. 
                                  // Proportional P/T을 지원하는 PTZ카메라에서 Proportional P/T기능을 끄지못하는 경우 
                                  // 이 속도비율로 실제속도에 해당하는 HW Pan,Tilt를 얻어낸다. 
};


enum INTERPOLATION_MODE 
{
   INTERPOLATION_MODE_STAIR,      // 계단방식의 보간.
   INTERPOLATION_MODE_LINEAR,     // 선형보간.
   INTERPOLATION_MODE_CURVE,      // 곡선 보간.  --> 사용안함.          
};


///////////////////////////////////////////////////////////////////////////////
//
// struct MapF2F
//
///////////////////////////////////////////////////////////////////////////////

struct MapF2F    // Map Float to Float
{
   float A;
   float B;
};

struct MapF2FTblInfo
{
   int     m_nITPMode;            // 보간방법 설정, enum INTERPOLATION_MODE 참고
   int     m_nTblSize;            // Map Float to Float 테이블의 크기. 
   MapF2F* m_pTbl;                // 테이블 포인터.

   MapF2FTblInfo ();
   void Set (int nITPMode, int nTblSize, MapF2F* pTbl);
};

float _GetAValue     (MapF2FTblInfo& map_tbl_info, float b);
float _GetBValue     (MapF2FTblInfo& map_tbl_info, float a);
float _GetAngleSpeed (float* pAngleVelocityTable, int nMaxHWSpeed, float* pAngleSpeedCheckZoomTable, int nAngleSpeedCheckZoomTableItemNum, float fHWSpeed, float fCurZoomValue);
float _GetHWSpeed    (float* pAngleVelocityTable, int nMaxHWSpeed, float* pAngleSpeedCheckZoomTable, int nAngleSpeedCheckZoomTableItemNum, float fAngleSpeed, float fCurZoomValue);

////////////////////////////////////////////////////////////////////////
//
// class PTZCtrlLogItem
//
////////////////////////////////////////////////////////////////////////

class PTZCtrlLogItem
{
public:
   uint32_t   m_nTime;
   float      m_fTgtLogZoom;
   FPTZVector m_pvTgtAbsPos;
   FPTZVector m_pvContMove;
   FPTZVector m_pvCurAbsPos;

public:
   PTZCtrlLogItem ();
};


class PTZCtrlLogItemQueue
{
public:
   PTZCtrlLogItem* m_pItems;

public:
   int   m_nStartIdx;
   int   m_nEndIdx;
   int   m_nItemNum;
   int   m_nMaxItemNum;
   int   m_nFramePeriod;
   int   m_nLogPeriod;
   float m_fFrameRate;

public:
   PTZCtrlLogItemQueue ();
   ~PTZCtrlLogItemQueue ();

public:
   void Initialize (float fFrameRate, float fLogPeriod);
   int  GetItemIdx (int nCurIdx, int nDeltaIdx);
   void AddItem (PTZCtrlLogItem& item);
   int  FindItemIdx (int nDeltaTime);
   PTZCtrlLogItem* GetLastItem ( );

public:
   float      CalcAvgTgtZoomValue (int nDeltaTime);
   FPTZVector CalcAvgPTSpdAvgZPos (int nDeltaTime);
   FPTZVector CalcAvgAbsPTZPos (int nDeltaTime);    // 나중에 구현예정.
   FPTZVector CalcAvgContMove (int nDeltaTime);     // 나중에 구현예정.
};

/////////////////////////////////////////////////////////////////////////////
//
// class CMapID2PTZCtrl
//
/////////////////////////////////////////////////////////////////////////////

class CMapID2PTZCtrl
{
public:
   std::map<long, CPTZCtrl*> m_mapClsID2PTZCtrl;
   CCriticalSection          m_csMap;

public:
   CMapID2PTZCtrl ();

   void Push (long nClsID, CPTZCtrl* pPTZCtrl);
   void Pop  (long nClsID);
   CPTZCtrl* Find (long nClsID); 
};

#endif

﻿#include "stdafx.h"
#include "PTZxD.h"


///////////////////////////////////////////////////////////////////////////////
//
// Class: CPTZ1D
//
///////////////////////////////////////////////////////////////////////////////

CPTZ1D::CPTZ1D()
{
   ID = -1;
}

int CPTZ1D::Read( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PTZ1D" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "ID", &ID );
      xmlNode.Attribute( TYPE_ID( float ), "Pan", &P );
      xmlNode.Attribute( TYPE_ID( float ), "Tilt", &T );
      xmlNode.Attribute( TYPE_ID( float ), "Zoom", &Z );
      xmlNode.Attribute( TYPE_ID( float ), "Focus", &F );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

int CPTZ1D::Write( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PTZ1D" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "ID", &ID );
      xmlNode.Attribute( TYPE_ID( float ), "Pan", &P );
      xmlNode.Attribute( TYPE_ID( float ), "Tilt", &T );
      xmlNode.Attribute( TYPE_ID( float ), "Zoom", &Z );
      xmlNode.Attribute( TYPE_ID( float ), "Focus", &F );
      xmlNode.End();
      return ( DONE );
   }
   return ( 1 );
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CPTZ2D
//
///////////////////////////////////////////////////////////////////////////////

CPTZ2D::CPTZ2D()
{
   SID[0]                 = '\0';
   NRef                   = 0;
   ConvertIntIPAddr2IPStr = TRUE;
}

CPTZ2D& CPTZ2D::operator=( CPTZ2D& from )
{
   memcpy( SID, from.SID, sizeof( SID ) );
   NRef    = from.NRef;
   RefTime = from.RefTime;
   return *this;
}

int CPTZ2D::AddPTZ1D( int id, FPTZVector& vec )
{
   CPTZ1D* ptz1d           = new CPTZ1D;
   ptz1d->ID               = id;
   *( (FPTZVector*)ptz1d ) = vec;
   CCriticalSectionSP co( _Lock );
   Add( ptz1d );
   return ( DONE );
}

int CPTZ2D::FindPTZ1D( int id, CPTZ1D** ppPTZ1D )
{
   *ppPTZ1D = NULL;
   CCriticalSectionSP co( _Lock );
   CPTZ1D* ptz1d = First();
   while( ptz1d ) {
      if( ptz1d->ID == id ) {
         *ppPTZ1D = ptz1d;
         break;
      }
      ptz1d = LinkedList<CPTZ1D>::Next( ptz1d );
   }
   if( *ppPTZ1D == NULL )
      return ( 1 );
   return ( DONE );
}

int CPTZ2D::DeletePTZ1D( int id )
{
   CPTZ1D* ptz1d;
   if( FindPTZ1D( id, &ptz1d ) )
      return ( 1 );

   CCriticalSectionSP co( _Lock );
   Remove( ptz1d );

   delete ptz1d;
   return ( DONE );
}

int CPTZ2D::Read( CXMLIO* pIO )
{
   CCriticalSectionSP co( _Lock );
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PTZ2D" ) ) {
      int n_items;
      xmlNode.Attribute( TYPE_ID( char ), "SID", SID, sizeof( SID ) );
      xmlNode.Attribute( TYPE_ID( int ), "ItemNum", &n_items );
      BOOL bReadAttr = xmlNode.Attribute( TYPE_ID( int ), "Cvt2IPStr", &ConvertIntIPAddr2IPStr );
      if( !bReadAttr ) ConvertSID_UIntIPAddr2ToIPAddrString();
      Delete();
      for( int i = 0; i < n_items; i++ ) {
         CPTZ1D* ptz1d = new CPTZ1D;
         if( DONE == ptz1d->Read( pIO ) )
            Add( ptz1d );
         else
            delete ptz1d;
      }
      xmlNode.End();
   }
   return ( DONE );
}

int CPTZ2D::Write( CXMLIO* pIO )
{
   CCriticalSectionSP co( _Lock );
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PTZ2D" ) ) {
      xmlNode.Attribute( TYPE_ID( char ), "SID", SID, sizeof( SID ) );
      int n_items = GetNumItems();
      xmlNode.Attribute( TYPE_ID( int ), "ItemNum", &n_items );
      xmlNode.Attribute( TYPE_ID( int ), "Cvt2IPStr", &ConvertIntIPAddr2IPStr );
      CPTZ1D* ptz1d = First();
      while( ptz1d ) {
         ptz1d->Write( pIO );
         ptz1d = LinkedList<CPTZ1D>::Next( ptz1d );
      }
      xmlNode.End();
   }
   return ( DONE );
}

void CPTZ2D::ConvertSID_UIntIPAddr2ToIPAddrString()
{
   if( 0 != strncmp( SID, "vc_", 3 ) ) {
      uint nIPAddr      = 0;
      uint nVideoPortNo = 0;
      sscanf( SID, "%08X_%02d", &nIPAddr, &nVideoPortNo );
      SID[0]                = 0;
      std::string strIPAddr = GetIPAddrString( nIPAddr );
      sprintf( SID, "%s_%02d", strIPAddr.c_str(), nVideoPortNo );
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CPTZ3D
//
///////////////////////////////////////////////////////////////////////////////

CPTZ3D::CPTZ3D()
{
   MaxN2D = 10;
}

int CPTZ3D::AddPTZ2D( LPCSTR sid, CPTZ2D** ppPTZ2D )
{
   *ppPTZ2D      = NULL;
   CPTZ2D* ptz2d = new CPTZ2D;
   strcpy( ptz2d->SID, sid );
   CCriticalSectionSP co( _Lock );
   Add( ptz2d );
   *ppPTZ2D = ptz2d;
   return ( DONE );
}

int CPTZ3D::FindPTZ2D( LPCSTR sid, CPTZ2D** ppPTZ2D )
{
   *ppPTZ2D = NULL;
   CCriticalSectionSP co( _Lock );
   CPTZ2D* ptz2d = First();
   while( ptz2d ) {
      if( !strcmp( ptz2d->SID, sid ) ) {
         *ppPTZ2D = ptz2d;
         break;
      }
      ptz2d = Next( ptz2d );
   }
   if( *ppPTZ2D == NULL )
      return ( 1 );
   return ( DONE );
}

int CPTZ3D::Dump()
{
   CCriticalSectionSP co( _Lock );
   int n_items = GetNumItems();
   CPTZ2D* next_ptz2d;
   CPTZ2D* ptz2d = First();
   while( ptz2d ) {
      if( n_items <= MaxN2D )
         break;
      next_ptz2d = Next( ptz2d );
      if( ptz2d->NRef == 0 ) {
         Remove( ptz2d );
         delete ptz2d;
         n_items--;
      }
      ptz2d = next_ptz2d;
   }
   return ( DONE );
}

int CPTZ3D::Read( CXMLIO* pIO )
{
   CCriticalSectionSP co( _Lock );
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PTZ3D" ) ) {
      int n_items;
      xmlNode.Attribute( TYPE_ID( int ), "ItemNum", &n_items );
      Delete();
      for( int i = 0; i < n_items; i++ ) {
         CPTZ2D* ptz2d = new CPTZ2D;
         if( DONE == ptz2d->Read( pIO ) )
            Add( ptz2d );
         else
            delete ptz2d;
      }
      xmlNode.End();
   }
   return ( DONE );
}

int CPTZ3D::Write( CXMLIO* pIO )
{
   CCriticalSectionSP co( _Lock );
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PTZ3D" ) ) {
      int n_items = GetNumItems();
      xmlNode.Attribute( TYPE_ID( int ), "ItemNum", &n_items );
      CPTZ2D* ptz2d = First();
      while( ptz2d ) {
         if( ptz2d->Write( pIO ) ) {
            return ( 1 );
         }
         ptz2d = Next( ptz2d );
      }
      xmlNode.End();
   }
   return ( DONE );
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CPTZ3DFile
//
///////////////////////////////////////////////////////////////////////////////

CPTZ3DFile::CPTZ3DFile()
{
   FileVer = 1.0f;
}

int CPTZ3DFile::LoadFile()
{
   if( FilePath.empty() ) return ( 1 );
   return Load( FilePath.c_str() );
}

int CPTZ3DFile::SaveFile()
{
   if( FilePath.empty() ) return ( 1 );
   return Save( FilePath.c_str() );
}

//CCriticalSection g_csPTZ3DFile;

int CPTZ3DFile::Load( LPCSTR path_name )
{
   // NOTE(yhpark): Do not use PresetToPTZPosMap.xml
   return ( DONE );
   
//   std::string strXmlCfgPathName = sutil::sformat( "%s/%s", path_name, "PresetToPTZPosMap.xml" );
//   if( IsExistFile( strXmlCfgPathName ) == false ) {
//      LOGW << "No Config File : " << strXmlCfgPathName;
//      return ( 1 );
//   }
//   CCriticalSectionSP co( g_csPTZ3DFile );
//   if( IsExistFile( strXmlCfgPathName ) ) {
//      CXMLIO xmlIO( strXmlCfgPathName, XMLIO_Read );
//      CXMLNode xmlNode( &xmlIO );
//      if( xmlNode.Start( "PresetToPTZPosMap" ) ) {
//         Read( &xmlIO );
//         xmlNode.End();
//      }
//      return ( DONE );
//   }
//   return ( 2 );
}

int CPTZ3DFile::Save( LPCSTR path_name )
{
   // NOTE(yhpark): Do not use PresetToPTZPosMap.xml
   return ( DONE );
   
//   CCriticalSectionSP co( g_csPTZ3DFile );
//   std::string strXmlCfgPathName = sutil::sformat( "%s/%s", path_name, "PresetToPTZPosMap.xml" );
//   CXMLIO xmlIO( strXmlCfgPathName, XMLIO_Write );
//   CXMLNode xmlNode( &xmlIO );
//   if( xmlNode.Start( "PresetToPTZPosMap" ) ) {
//      Write( &xmlIO );
//      xmlNode.End();
//   }
}

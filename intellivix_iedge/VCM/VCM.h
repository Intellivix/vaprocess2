#pragma once

//------------------------------------------------

#include <Common.h>
// Common
#ifdef __WIN32
#include <afxctl.h>						// Includes AfxConnectionAdvise()
#include <afxinet.h>
#endif

//------------------------------------------------
// image processing & computer vision
#include <bccl.h>

//------------------------------------------------
// standard template library
#include <map>
#include <stack>
#include <deque>
#include <list>
#include <vector>
#include <queue>
#include <algorithm>
#include <functional>

#include "VCM_Define.h"
#include "CameraProductAndItsProperties.h"
#include "CameraInfo.h"
#include "Camera_IP.h"
#include "VideoChannel.h"
#include "VideoChannelManager.h"
#include "IPCameraManager.h"
#include "IPCamStream.h"





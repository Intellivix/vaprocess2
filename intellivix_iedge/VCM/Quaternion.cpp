#include "stdafx.h"
#include "Quaternion.h"

namespace BCCL {
/////////////////////////////////////////////////////////////////////////////
//
// class: Quaternion
//
/////////////////////////////////////////////////////////////////////////////

double Quaternion::ms_fEpsilon = 1e-03f;
Quaternion Quaternion::ZERO( 0.0, 0.0, 0.0, 0.0 );
Quaternion Quaternion::IDENTITY( 1.0, 0.0, 0.0, 0.0 );

Quaternion::Quaternion( double fW, double fX, double fY, double fZ )
{
   W = fW;
   X = fX;
   Y = fY;
   Z = fZ;
}

Quaternion::Quaternion( const Quaternion& rkQ )
{
   W = rkQ.W;
   X = rkQ.X;
   Y = rkQ.Y;
   Z = rkQ.Z;
}

void Quaternion::FromRotationMatrix( const Matrix& kRot )
{
   ASSERT( kRot.NRows == 3 && kRot.NCols == 3 );

   // Algorithm in Ken Shoemake's article in 1987 SIGGRAPH course notes
   // article "Quaternion Calculus and Fast Animation".

   double fTrace = double( kRot[0][0] + kRot[1][1] + kRot[2][2] );
   double fRoot;

   if( fTrace > 0.0 ) {
      // |W| > 1/2, may as well choose W > 1/2
      fRoot = sqrt( fTrace + 1.0 ); // 2w
      W     = 0.5 * fRoot;
      fRoot = 0.5 / fRoot; // 1/(4w)
      X     = ( kRot[2][1] - kRot[1][2] ) * fRoot;
      Y     = ( kRot[0][2] - kRot[2][0] ) * fRoot;
      Z     = ( kRot[1][0] - kRot[0][1] ) * fRoot;
   } else {
      // |W| <= 1/2
      static int s_iNext[3] = { 1, 2, 0 };
      int i                 = 0;
      if( kRot[1][1] > kRot[0][0] )
         i = 1;
      if( kRot[2][2] > kRot[i][i] )
         i  = 2;
      int j = s_iNext[i];
      int k = s_iNext[j];

      fRoot              = sqrt( kRot[i][i] - kRot[j][j] - kRot[k][k] + 1.0 );
      double* apkQuat[3] = { &X, &Y, &Z };
      *apkQuat[i]        = 0.5 * fRoot;
      fRoot              = 0.5 / fRoot;
      W                  = kRot[k][j] - kRot[j][k] * fRoot;
      *apkQuat[j]        = kRot[j][i] + kRot[i][j] * fRoot;
      *apkQuat[k]        = kRot[k][i] + kRot[i][k] * fRoot;
   }
}

void Quaternion::ToRotationMatrix( Matrix& kRot ) const
{
   if( kRot.NRows != 3 && kRot.NCols != 3 )
      kRot.Create( 3, 3 );

   double fTx  = 2.0 * X;
   double fTy  = 2.0 * Y;
   double fTz  = 2.0 * Z;
   double fTwx = fTx * W;
   double fTwy = fTy * W;
   double fTwz = fTz * W;
   double fTxx = fTx * X;
   double fTxy = fTy * X;
   double fTxz = fTz * X;
   double fTyy = fTy * Y;
   double fTyz = fTz * Y;
   double fTzz = fTz * Z;

   kRot[0][0] = 1.0 - ( fTyy + fTzz );
   kRot[0][1] = fTxy - fTwz;
   kRot[0][2] = fTxz + fTwy;
   kRot[1][0] = fTxy + fTwz;
   kRot[1][1] = 1.0 - ( fTxx + fTzz );
   kRot[1][2] = fTyz - fTwx;
   kRot[2][0] = fTxz - fTwy;
   kRot[2][1] = fTyz + fTwx;
   kRot[2][2] = 1.0 - ( fTxx + fTyy );
}

void Quaternion::FromAngleAxis( const double& rfAngle, const FVector3D& rkAxis )
{
   // assert:  axis[] is unit length
   //
   // The quaternion representing the rotation is
   //   q = cos(A/2)+sin(A/2)*(X*i+Y*j+Z*k)

   double fHalfAngle = 0.5 * rfAngle;
   double fSin       = sin( fHalfAngle );
   W                 = cos( fHalfAngle );
   X                 = fSin * rkAxis.X;
   Y                 = fSin * rkAxis.Y;
   Z                 = fSin * rkAxis.Z;
}

void Quaternion::ToAngleAxis( double& rfAngle, FVector3D& rkAxis ) const
{
   // The quaternion representing the rotation is
   //   q = cos(A/2)+sin(A/2)*(X*i+Y*j+Z*k)

   double fSqrLength = ( X * X ) + ( Y * Y ) + ( Z * Z );
   if( fSqrLength > 0.0 ) {
      rfAngle           = 2.0 * acos( W );
      double fInvLength = 1.0 / sqrt( fSqrLength );
      rkAxis.X          = X * fInvLength;
      rkAxis.Y          = Y * fInvLength;
      rkAxis.Z          = Z * fInvLength;
   } else {
      // angle is 0 (mod 2*pi), so any axis will do
      rfAngle  = 0.0f;
      rkAxis.X = 1.0;
      rkAxis.Y = 0.0f;
      rkAxis.Z = 0.0f;
   }
}

void Quaternion::FromAxes( const FVector3D* akAxis )
{
   Matrix kRot( 3, 3 );

   for( int iCol = 0; iCol < 3; iCol++ ) {
      kRot[0][iCol] = akAxis[iCol].X;
      kRot[1][iCol] = akAxis[iCol].Y;
      kRot[2][iCol] = akAxis[iCol].Z;
   }

   FromRotationMatrix( kRot );
}

void Quaternion::ToAxes( FVector3D* akAxis ) const
{
   Matrix kRot( 3, 3 );

   ToRotationMatrix( kRot );

   for( int iCol = 0; iCol < 3; iCol++ ) {
      akAxis[iCol].X = kRot[0][iCol];
      akAxis[iCol].Y = kRot[1][iCol];
      akAxis[iCol].Z = kRot[2][iCol];
   }
}

Quaternion& Quaternion::operator=( const Quaternion& rkQ )
{
   W = rkQ.W;
   X = rkQ.X;
   Y = rkQ.Y;
   Z = rkQ.Z;
   return *this;
}

Quaternion Quaternion::operator+( const Quaternion& rkQ ) const
{
   return Quaternion( W + rkQ.W, X + rkQ.X, Y + rkQ.Y, Z + rkQ.Z );
}

Quaternion Quaternion::operator-( const Quaternion& rkQ ) const
{
   return Quaternion( W - rkQ.W, X - rkQ.X, Y - rkQ.Y, Z - rkQ.Z );
}

Quaternion Quaternion::operator*( const Quaternion& rkQ ) const
{
   // NOTE:  Multiplication is not generally commutative, so in most
   // cases p*q != q*p.

   return Quaternion(
       ( W * rkQ.W ) - ( X * rkQ.X ) - ( Y * rkQ.Y ) - ( Z * rkQ.Z ),
       ( W * rkQ.X ) + ( X * rkQ.W ) + ( Y * rkQ.Z ) - ( Z * rkQ.Y ),
       ( W * rkQ.Y ) + ( Y * rkQ.W ) + ( Z * rkQ.X ) - ( X * rkQ.Z ),
       ( W * rkQ.Z ) + ( Z * rkQ.W ) + ( X * rkQ.Y ) - ( Y * rkQ.X ) );
}

Quaternion Quaternion::operator*( double fScalar ) const
{
   return Quaternion( fScalar * W, fScalar * X, fScalar * Y, fScalar * Z );
}

Quaternion operator*( double fScalar, const Quaternion& rkQ )
{
   return Quaternion( fScalar * rkQ.W, fScalar * rkQ.X, fScalar * rkQ.Y, fScalar * rkQ.Z );
}

Quaternion Quaternion::operator-() const
{
   return Quaternion( -W, -X, -Y, -Z );
}

double Quaternion::Dot( const Quaternion& rkQ ) const
{
   return ( W * rkQ.W ) + ( X * rkQ.X ) + ( Y * rkQ.Y ) + ( Z * rkQ.Z );
}

double Quaternion::Norm() const
{
   return ( W * W ) + ( X * X ) + ( Y * Y ) + ( Z * Z );
}

Quaternion Quaternion::Inverse() const
{
   double fNorm = ( W * W ) + ( X * X ) + ( Y * Y ) + ( Z * Z );
   if( fNorm > 0.0 ) {
      double fInvNorm = 1.0 / fNorm;
      return Quaternion( W * fInvNorm, -X * fInvNorm, -Y * fInvNorm, -Z * fInvNorm );
   } else {
      // return an invalid result to flag the error
      return ZERO;
   }
}

Quaternion Quaternion::UnitInverse() const
{
   // assert:  'this' is unit length
   return Quaternion( W, -X, -Y, -Z );
}

Quaternion Quaternion::Exp() const
{
   // If q = A*(X*i+Y*j+Z*k) where (X,Y,Z) is unit length, then
   // exp(q) = cos(A)+sin(A)*(X*i+Y*j+Z*k).  If sin(A) is near zero,
   // use exp(q) = cos(A)+A*(X*i+Y*j+Z*k) since A/sin(A) has limit 1.

   double fAngle = sqrt( X * X + Y * Y + Z * Z );
   double fSin   = sin( fAngle );

   Quaternion kResult;
   kResult.W = cos( fAngle );

   if( fabs( fSin ) >= ms_fEpsilon ) {
      double fCoeff = fSin / fAngle;
      kResult.X     = fCoeff * X;
      kResult.Y     = fCoeff * Y;
      kResult.Z     = fCoeff * Z;
   } else {
      kResult.X = X;
      kResult.Y = Y;
      kResult.Z = Z;
   }

   return kResult;
}

Quaternion Quaternion::Log() const
{
   // If q = cos(A)+sin(A)*(X*i+Y*j+Z*k) where (X,Y,Z) is unit length, then
   // log(q) = A*(X*i+Y*j+Z*k).  If sin(A) is near zero, use log(q) =
   // sin(A)*(X*i+Y*j+Z*k) since sin(A)/A has limit 1.

   Quaternion kResult;
   kResult.W = 0.0;

   if( fabs( W ) < 1.0 ) {
      double fAngle = acos( W );
      double fSin   = sin( fAngle );
      if( fabs( fSin ) >= ms_fEpsilon ) {
         double fCoeff = fAngle / fSin;
         kResult.X     = fCoeff * X;
         kResult.Y     = fCoeff * Y;
         kResult.Z     = fCoeff * Z;
         return kResult;
      }
   }

   kResult.X = X;
   kResult.Y = Y;
   kResult.Z = Z;

   return kResult;
}

FVector3D Quaternion::operator*( const FVector3D& rkVector ) const
{
   Matrix rkVec( 3 );
   Matrix rkRotVec( 3 );

   rkVec( 0 ) = rkVector.X;
   rkVec( 1 ) = rkVector.Y;
   rkVec( 2 ) = rkVector.Z;

   Matrix kRot( 3, 3 );
   ToRotationMatrix( kRot );
   rkRotVec = kRot * rkVec;
   return FVector3D( double( rkRotVec( 0 ) ), double( rkRotVec( 1 ) ), double( rkRotVec( 2 ) ) );
}

Quaternion Quaternion::Slerp( double fT, const Quaternion& rkP, const Quaternion& rkQ )
{
   double fCos   = rkP.Dot( rkQ );
   double fAngle = acos( fCos );

   if( fabs( fAngle ) < ms_fEpsilon )
      return rkP;

   double fSin    = sin( fAngle );
   double fInvSin = 1.0 / fSin;
   double fCoeff0 = sin( ( 1.0 - fT ) * fAngle ) * fInvSin;
   double fCoeff1 = sin( fT * fAngle ) * fInvSin;
   return fCoeff0 * rkP + fCoeff1 * rkQ;
}

Quaternion Quaternion::SlerpExtraSpins( double fT, const Quaternion& rkP, const Quaternion& rkQ, int iExtraSpins )
{
   double fCos   = rkP.Dot( rkQ );
   double fAngle = acos( fCos );

   if( fabs( fAngle ) < ms_fEpsilon )
      return rkP;

   double fSin    = sin( fAngle );
   double fPhase  = MC_PI * iExtraSpins * fT;
   double fInvSin = 1.0 / fSin;
   double fCoeff0 = sin( ( 1.0 - fT ) * fAngle - fPhase ) * fInvSin;
   double fCoeff1 = sin( fT * fAngle + fPhase ) * fInvSin;
   return fCoeff0 * rkP + fCoeff1 * rkQ;
}

void Quaternion::Intermediate( const Quaternion& rkQ0, const Quaternion& rkQ1, const Quaternion& rkQ2, Quaternion& rkA, Quaternion& rkB )
{
   // assert:  q0, q1, q2 are unit quaternions

   Quaternion kQ0inv    = rkQ0.UnitInverse();
   Quaternion kQ1inv    = rkQ1.UnitInverse();
   Quaternion rkP0      = kQ0inv * rkQ1;
   Quaternion rkP1      = kQ1inv * rkQ2;
   Quaternion kArg      = 0.25f * ( rkP0.Log() - rkP1.Log() );
   Quaternion kMinusArg = -kArg;

   rkA = rkQ1 * kArg.Exp();
   rkB = rkQ1 * kMinusArg.Exp();
}

Quaternion Quaternion::Squad( double fT, const Quaternion& rkP, const Quaternion& rkA, const Quaternion& rkB, const Quaternion& rkQ )
{
   double fSlerpT     = 2.0 * fT * ( 1.0 - fT );
   Quaternion kSlerpP = Slerp( fT, rkP, rkQ );
   Quaternion kSlerpQ = Slerp( fT, rkA, rkB );
   return Slerp( fSlerpT, kSlerpP, kSlerpQ );
}
}

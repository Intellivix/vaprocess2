#pragma once
#ifdef WIN32
#pragma warning (disable:4819)
#endif
//#if _MSC_VER > 1400
//#define WINVER 0x0501 // Windows XP
//#define _WIN32_WINNT 0x0501
//#endif

#define WIN32_LEAN_AND_MEAN      // 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS   // 일부 CString 생성자는 명시적으로 선언됩니다.

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN             // 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
#endif
#define _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#define NO_WARN_MBCS_MFC_DEPRECATION

#ifdef WIN32
#pragma warning( disable : 4099 )
#pragma warning( disable : 4204 )
#endif
//#include <afx.h>
//#include <afxwin.h>         // MFC 핵심 및 표준 구성 요소입니다.
//#include <afxdisp.h>        // MFC Automation classes
//#include <afxinet.h>
//
//
//#include <afxwin.h>         // MFC core and standard components
//#include <afxext.h>         // MFC extensions
//#include <afxdisp.h>        // MFC Automation classes
//#include <afxdtctl.h>       // MFC support for Internet Explorer 4 Common Controls
//#ifndef _AFX_NO_AFXCMN_SUPPORT
//#include <afxcmn.h>         // mfc support for windows common controls
//#include <atlbase.h>        //required for various wchar functions
//#endif // _AFX_NO_AFXCMN_SUPPORT
//
//#include <afxmt.h>
//#include <afxctl.h>         // Includes AfxConnectionAdvise()
//#include <afxpriv.h>

#include "bccl_environ.h"
#include "bccl_define.h"
#include "bccl.h"
#include "Common.h"
#include "VCM.h"
#include "SupportFunction.h"

#ifdef WIN32
void DebugString(TCHAR *pFmt, ...);
#endif

// Support Omnicast
#define SAFE_DELETE(x)     { if (x) delete x; x = NULL; }
#define SAFE_DELETE_ARR(x) { if (x) delete[] x; x = NULL; }

//#if defined( _DEBUG ) && defined( _MSC_VER )
//#undef THIS_FILE
//static char THIS_FILE[] = __FILE__;
//#define new DEBUG_NEW
//#endif

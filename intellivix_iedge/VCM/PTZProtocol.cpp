#include "stdafx.h"
#include "PTZProtocol.h"

///////////////////////////////////////////////////////////////////////////////
//
// Class: CPTZProtocol
//
///////////////////////////////////////////////////////////////////////////////

CPTZProtocol::CPTZProtocol()
{
   m_nAbsPosMoveCmdType = AbsPosMoveCmdType_SendPTZCmdInOnePacket;
}

void CPTZProtocol::Initialize( int nCamAddr )
{
   m_nCamAddr = nCamAddr;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CPTZProtocol_Pelco
//
///////////////////////////////////////////////////////////////////////////////

class CPTZProtocol_Pelco : public CPTZProtocol {
public:
   CPTZProtocol_Pelco()
   {
      m_nAbsPosMoveCmdType = AbsPosMoveCmdType_SendPTZCmdInEachPacket;
   }

   ~CPTZProtocol_Pelco()
   {
   }

   void Clear( BYTE* pCmd, int nCmdLen )
   {
      int i;
      for( i     = 2; i <= nCmdLen; i++ )
         pCmd[i] = 0;
      pCmd[0]    = 0xFF;
      pCmd[1]    = (BYTE)m_nCamAddr;
   }

   void SetCheckSum( BYTE* pCmd, int nCmdLen )
   {
      int i;
      BYTE checksum = 0;
      int ei        = nCmdLen - 2;
      for( i = 1; i <= ei; i++ )
         checksum += pCmd[i];
      pCmd[ei + 1] = checksum;
   }

   void GetCmd_ContPTZMove( BYTE* pCmd, int& nCmdLen, IPTZVector move_vec )
   {
      nCmdLen = 0;
      Clear( pCmd, 7 );

      byte cmd_left  = 0x04;
      byte cmd_up    = 0x08;
      byte cmd_right = 0x02;
      byte cmd_down  = 0x10;
      byte cmd_in    = 0x20;
      byte cmd_out   = 0x40;

      // Direction
      if( move_vec.P < 0 )
         pCmd[3] |= cmd_left;
      else if( move_vec.P > 0 )
         pCmd[3] |= cmd_right;
      if( move_vec.T < 0 )
         pCmd[3] |= cmd_up;
      else if( move_vec.T > 0 )
         pCmd[3] |= cmd_down;
      if( move_vec.Z < 0 )
         pCmd[3] |= cmd_out;
      else if( move_vec.Z > 0 )
         pCmd[3] |= cmd_in;

      // Speed
      pCmd[4] = abs( move_vec.P );
      pCmd[5] = abs( move_vec.T );

      // Focus
      if( move_vec.F > 0 )
         pCmd[2] |= 0x01; // Near
      else if( move_vec.F < 0 )
         pCmd[3] |= 0x80; // Far

      // Iris
      if( move_vec.I > 0 ) pCmd[2] |= 0x20; // Open
      if( move_vec.I < 0 ) pCmd[2] |= 0x40; // Close

      SetCheckSum( pCmd, 7 );
      nCmdLen += 7;
   }

   int GetPTZPosHW( BYTE* pCmd, int& nCmdLen, FPTZVector& pvAbsPos )
   {
      int nPopCmdLen = 0;
      if( nCmdLen >= 7 ) {
         // Pan Angle
         if( pCmd[0] == 0xFF ) {
            if( pCmd[1] == BYTE( m_nCamAddr ) ) {
               if( pCmd[3] == 0x59 ) {
                  pvAbsPos.P = (float)( ( ( pCmd[4] ) * 256.0f + pCmd[5] ) / 100.0f );
                  nPopCmdLen = 8;
                  return PTZPosType_Pan;
               }
            }
         }
         // Tilt Angle
         if( pCmd[0] == 0xFF ) {
            if( pCmd[1] == BYTE( m_nCamAddr ) ) {
               if( pCmd[3] == 0x5B ) {
                  pvAbsPos.T = (float)( ( ( pCmd[4] ) * 256.0f + pCmd[5] ) / 100.0f );
                  nPopCmdLen = 8;
                  return PTZPosType_Tilt;
               }
            }
         }
         // Zoom Factor
         if( pCmd[0] == 0xFF ) {
            if( pCmd[1] == BYTE( m_nCamAddr ) ) {
               if( pCmd[3] == 0x5D ) {
                  pvAbsPos.Z = (float)( ( ( pCmd[4] ) * 256.0f + pCmd[5] ) / 100.0f );
                  nPopCmdLen = 8;
                  return PTZPosType_Zoom;
               }
            }
         }
      }
      return 0;
   }

   void GetCmd_ReqAbsPos( BYTE* pCmd, int& nCmdLen, uint nPTZPosType )
   {
      nCmdLen = 0;
      // Pan Angle Request
      if( PTZPosType_Pan & nPTZPosType ) {
         Clear( pCmd, 7 );
         pCmd[3] = 0x51;
         SetCheckSum( pCmd, 7 );
         nCmdLen += 7;
         pCmd += 7;
      }

      // Tilt Angle Request
      if( PTZPosType_Tilt & nPTZPosType ) {
         Clear( pCmd, 7 );
         pCmd[3] = 0x53;
         SetCheckSum( pCmd, 7 );
         nCmdLen += 7;
         pCmd += 7;
      }

      // Zoom Factor Request
      if( PTZPosType_Zoom & nPTZPosType ) {
         Clear( pCmd, 7 );
         pCmd[3] = 0x55;
         SetCheckSum( pCmd, 7 );
         nCmdLen += 7;
      }
   }

   void GetCmd_GotoAbsPos( BYTE* pCmd, int& nCmdLen, FPTZVector pvAbsPos, uint nPTZPosType )
   {
      ushort pa_hw = (int)( pvAbsPos.P * 100.0f );
      ushort ta_hw = (int)( pvAbsPos.T * 100.0f );
      ushort zf_hw = (int)( pvAbsPos.Z * 100.0f );
      nCmdLen      = 0;

      // Set Pan Position
      if( PTZPosType_Pan & nPTZPosType ) {
         Clear( pCmd, 7 );
         pCmd[3] = 0x4B;
         pCmd[4] = ( BYTE )( pa_hw >> 8 );
         pCmd[5] = ( BYTE )( pa_hw & 0xFF );
         SetCheckSum( pCmd, 7 );
         nCmdLen += 7;
         pCmd += 7;
      }

      // Set Tilt Position
      if( PTZPosType_Tilt & nPTZPosType ) {
         Clear( pCmd, 7 );
         pCmd[3] = 0x4D;
         pCmd[4] = ( BYTE )( ta_hw >> 8 );
         pCmd[5] = ( BYTE )( ta_hw & 0xFF );
         SetCheckSum( pCmd, 7 );
         nCmdLen += 7;
         pCmd += 7;
      }

      // Set Zoom Position
      if( PTZPosType_Zoom & nPTZPosType ) {
         Clear( pCmd, 7 );
         pCmd[3] = 0x4F;
         pCmd[4] = ( BYTE )( zf_hw >> 8 );
         pCmd[5] = ( BYTE )( zf_hw & 0xFF );
         SetCheckSum( pCmd, 7 );
         nCmdLen += 7;
      }
   }

   void GetCmd_SetPreset( BYTE* pCmd, int& nCmdLen, int preset_no )
   {
      Clear( pCmd, 7 );
      pCmd[3] = 0x03;
      pCmd[5] = (BYTE)preset_no;
      SetCheckSum( pCmd, 7 );
      nCmdLen = 7;
   }

   void GetCmd_ClearPreset( BYTE* pCmd, int& nCmdLen, int preset_no )
   {
      Clear( pCmd, 7 );
      pCmd[3] = 0x05;
      pCmd[5] = (BYTE)preset_no;
      SetCheckSum( pCmd, 7 );
      nCmdLen = 7;
   }

   void GetCmd_GotoPreset( BYTE* pCmd, int& nCmdLen, int preset_no )
   {
      Clear( pCmd, 7 );
      pCmd[3] = 0x07;
      pCmd[5] = (BYTE)preset_no;
      SetCheckSum( pCmd, 7 );
      nCmdLen = 7;
   }

   void GetCmd_SetAuxiliary( BYTE* pCmd, int& nCmdLen, int nAuxNo )
   {
      Clear( pCmd, 7 );
      pCmd[3] = 0x0B;
      pCmd[5] = (BYTE)nAuxNo;
      SetCheckSum( pCmd, 7 );
      nCmdLen = 7;
   }

   void GetCmd_GetAuxiliary( BYTE* pCmd, int& nCmdLen, int nAuxNo )
   {
      Clear( pCmd, 7 );
      pCmd[3] = 0x09;
      pCmd[5] = (BYTE)nAuxNo;
      SetCheckSum( pCmd, 7 );
      nCmdLen = 7;
   }

   void GetCmd_OSD_MenuDisplay( BYTE* pCmd, int& nCmdLen )
   {
      Clear( pCmd, 7 );
      pCmd[3] = 0x23;
      pCmd[5] = 0x5F;
      SetCheckSum( pCmd, 7 );
      nCmdLen = 7;
   }

   void GetCmd_OSD_Enter( BYTE* pCmd, int& nCmdLen )
   {
      Clear( pCmd, 7 );
      pCmd[2] = 0x01;
      SetCheckSum( pCmd, 7 );
      nCmdLen = 7;
   }

   void GetCmd_OSD_Cancel( BYTE* pCmd, int& nCmdLen )
   {
      nCmdLen = 0;
   }

   void GetCmd_SetIRLight( BYTE* pCmd, int& nCmdLen, int nIRLight )
   {
      nCmdLen = 0;
   }
};

CPTZProtocol_Pelco g_PTZProtocol_Pelco;

///////////////////////////////////////////////////////////////////////////////
//
// Class: CPTZProtocol_Pelco_Pelco
//
///////////////////////////////////////////////////////////////////////////////

class CPTZProtocol_SamsungTechWin : public CPTZProtocol {
public:
   CPTZProtocol_SamsungTechWin()
   {
   }

   ~CPTZProtocol_SamsungTechWin()
   {
   }

   void Clear( BYTE* pCmd, int nCmdLen )
   {
      int i;
      for( i     = 3; i < nCmdLen - 1; i++ )
         pCmd[i] = 0;

      pCmd[0]           = 0xA0;
      pCmd[1]           = (BYTE)m_nCamAddr;
      pCmd[2]           = 0x00;
      pCmd[nCmdLen - 2] = 0xAF;
   }

   void SetCheckSum( BYTE* pCmd, int nCmdLen )
   {
      pCmd[nCmdLen - 1] = ~( ( pCmd[1] + pCmd[2] + pCmd[3] + pCmd[4] + pCmd[5] + pCmd[6] + pCmd[7] + pCmd[8] ) & 0xFF );
   }

   void GetCmd_ContPTZMove( BYTE* pCmd, int& nCmdLen, IPTZVector move_vec )
   {
      nCmdLen = 0;

      Clear( pCmd, 11 );

      byte cmd_right = 0x02;
      byte cmd_left  = 0x04;
      byte cmd_up    = 0x08;
      byte cmd_down  = 0x10;
      byte cmd_in    = 0x20;
      byte cmd_out   = 0x40;

      if( move_vec.P < 0 )
         pCmd[4] |= cmd_left;
      else if( move_vec.P > 0 )
         pCmd[4] |= cmd_right;
      if( move_vec.T < 0 )
         pCmd[4] |= cmd_up;
      else if( move_vec.T > 0 )
         pCmd[4] |= cmd_down;
      pCmd[5] = ( abs( move_vec.P ) & 0x3F );
      pCmd[6] = ( abs( move_vec.T ) & 0x3F );

      if( move_vec.Z == 0 ) {
         if( move_vec.F > 0 ) pCmd[3] |= 0x02; // Near
         if( move_vec.F < 0 ) pCmd[3] |= 0x01; // Far
      }
      if( move_vec.I > 0 ) pCmd[3] |= 0x08; // Open
      if( move_vec.I < 0 ) pCmd[3] |= 0x10; // Close

      SetCheckSum( pCmd, 11 );
      nCmdLen += 11;
      pCmd += 11;
   }

   int GetPTZPosHW( BYTE* pCmd, int& nCmdLen, FPTZVector& pvAbsPos )
   {
      return 0;
   }

   void GetCmd_ReqAbsPos( BYTE* pCmd, int& nCmdLen, uint nPTZPosType )
   {
      nCmdLen = 0;
   }

   void GetCmd_GotoAbsPos( BYTE* pCmd, int& nCmdLen, FPTZVector pvAbsPos, uint nPTZPosType )
   {
      nCmdLen = 0;
   }

   void GetCmd_SetPreset( BYTE* pCmd, int& nCmdLen, int preset_no )
   {
      Clear( pCmd, 11 );
      pCmd[3] = 0x00;
      pCmd[4] = 0x03;
      pCmd[5] = (byte)preset_no;
      SetCheckSum( pCmd, 11 );
      nCmdLen = 11;
   }

   void GetCmd_ClearPreset( BYTE* pCmd, int& nCmdLen, int preset_no )
   {
      Clear( pCmd, 11 );
      pCmd[3] = 0x00;
      pCmd[4] = 0x05;
      pCmd[5] = (byte)preset_no;
      SetCheckSum( pCmd, 11 );
      nCmdLen = 11;
   }

   void GetCmd_GotoPreset( BYTE* pCmd, int& nCmdLen, int preset_no )
   {
      Clear( pCmd, 11 );
      pCmd[3] = 0x00;
      pCmd[4] = 0x07;
      pCmd[5] = (byte)preset_no;
      SetCheckSum( pCmd, 11 );
      nCmdLen = 11;
   }

   void GetCmd_SetAuxiliary( BYTE* pCmd, int& nCmdLen, int nAuxNo )
   {
      nCmdLen = 0;
   }

   void GetCmd_ClearAuxiliary( BYTE* pCmd, int& nCmdLen, int nAuxNo )
   {
      nCmdLen = 0;
   }

   void GetCmd_OSD_MenuDisplay( BYTE* pCmd, int& nCmdLen )
   {
      Clear( pCmd, 7 );
      pCmd[3] = 0x23;
      pCmd[5] = 0x5F;
      SetCheckSum( pCmd, 7 );
      nCmdLen = 7;
   }

   void GetCmd_OSD_Enter( BYTE* pCmd, int& nCmdLen )
   {
      Clear( pCmd, 11 );
      pCmd[3] = 0x01;
      pCmd[5] = 0x06;
      SetCheckSum( pCmd, 11 );
      nCmdLen = 11;
   }

   void GetCmd_OSD_Cancel( BYTE* pCmd, int& nCmdLen )
   {
      Clear( pCmd, 11 );
      pCmd[3] = 0x02;
      pCmd[5] = 0x07;
      SetCheckSum( pCmd, 11 );
      nCmdLen = 11;
   }

   void GetCmd_SetIRLight( BYTE* pCmd, int& nCmdLen, int nIRLight )
   {
      nCmdLen = 0;
   }
};

CPTZProtocol_Pelco g_PTZProtocol_SamsungTechWin;

CPTZProtocol* GetPTZProtocol( int nPTZProtocolType )
{
   if( PTZProtocolType_Pelco_D == nPTZProtocolType ) {
      return &g_PTZProtocol_Pelco;
   } else if( PTZProtocolType_Samsung == nPTZProtocolType ) {
   } else if( PTZProtocolType_SamsungTechWin == nPTZProtocolType ) {
      return &g_PTZProtocol_SamsungTechWin;
   } else if( PTZProtocolType_LG == nPTZProtocolType ) {
   } else if( PTZProtocolType_Honeywell == nPTZProtocolType ) {
   } else if( PTZProtocolType_BOSCH == nPTZProtocolType ) {
   } else if( PTZProtocolType_Woonwoo == nPTZProtocolType ) {
   }
   return NULL;
}

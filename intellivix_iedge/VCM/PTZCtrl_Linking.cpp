#include "stdafx.h"
#include "PTZCtrl_Linking.h"

#ifdef __SUPPORT_PTZ_CAMERA

#include "CameraProductAndItsProperties.h"

///////////////////////////////////////////////////////////////////////////////
//
// Class: CPTZCtrl_Linking
//
///////////////////////////////////////////////////////////////////////////////

CPTZCtrl_Linking::CPTZCtrl_Linking( int nPTZControlType, ISize2D s2VideoSize )
{
   m_nPTZControlType  = nPTZControlType;
   m_pVCSrc           = NULL;
   m_pVCM             = NULL;
   m_nPTZLinkingState = 0;
   m_pPTZCtrlInfo     = NULL;
   m_pIPCamInfo       = NULL;
}

CPTZCtrl_Linking::~CPTZCtrl_Linking()
{
}

int CPTZCtrl_Linking::Open( CIPCameraInfo* pIPCamInfo )
{
   // jun : PTZ연결관리 : 불필요
   PreOpen();
   m_pIPCamInfo          = pIPCamInfo;
   m_pPTZCtrlInfo        = &pIPCamInfo->m_PTZInfo;
   m_PTZInfo             = pIPCamInfo->m_PTZInfo;
   m_pPTZControlProperty = GetPTZControlProperty( 0 );
   m_nCapabilities       = m_pPTZControlProperty->m_nCapabilities;
   PostOpen();
   if( !( m_nPTZLinkingState & PTZLinkingState_SourceVideoChannelRemoved ) )
      m_nPTZLinkingState = PTZLinkingState_SearchingSourceVideoChannel;

   m_InputPTZMoveHW_Prev( MC_VSV, MC_VSV, MC_VSV, MC_VSV, MC_VSV );

   return ( DONE );
}

void CPTZCtrl_Linking::Close()
{
   CPTZCtrl::Close();
   // 스레드 종료 시도
   if( m_nState & PTZ_State_ThreadPTZControl )
      m_nState |= PTZ_State_ToBeStopThread;
}

void CPTZCtrl_Linking::SignalToClose()
{
   CPTZCtrl::Close();
}

int CPTZCtrl_Linking::LinkingPTZCtrlProc()
{
   if( !m_pVCM || !IsPTZConnected() ) return ( 1 );

   uint32_t nCurTime = GetTickCount();

   if( m_nPTZLinkingState & PTZLinkingState_SourceVideoChannelRemoved ) {
      return ( 2 );
   }
   // CVideoChannelManager로 부터 소스영상 카메라가 생성될 때까지 대기하는 상태
   else if( m_nPTZLinkingState & PTZLinkingState_SearchingSourceVideoChannel ) {
      if( !m_nPrevCheckTime || ( nCurTime - m_nPrevCheckTime > 1000 ) ) {
         m_pVCM->m_csVideoChannelList.Lock();
         // 소스영상 카메라가 생성되었으면 소스영상 카메라를 등록한다.
         m_pVCSrc = m_pVCM->GetVideoChannel( m_PTZInfo.m_nVCID_LikingPTZCtrl );
         if( m_pVCSrc && ( m_pVCSrc->m_nState & VCState_Activate ) && !( m_pVCSrc->m_nState & VCState_Deactivating ) ) {
            if( m_pVCSrc->m_pPTZCtrl ) {
               m_nPTZLinkingState &= ~PTZLinkingState_SearchingSourceVideoChannel;
               m_nPTZLinkingState |= PTZLinkingState_SourceVideoChannelRegisterd;
               logd( "[CH:%d] Source Video Channel is registerd\n", m_nChannelNo_IVX + 1 );
               CCriticalSectionSP co( m_csLinkingPTZControl );
               m_pLinkingPTZControl = m_pVCSrc->m_pPTZCtrl;
               m_pLinkingPTZControl->CopyZoomModuleFactors( m_PTZInfo.m_nZoomModuleType, this ),
                   m_pPTZControlProperty = GetPTZControlProperty( m_pPTZCtrlInfo->m_nPTZControlType );
               if( m_pPTZControlProperty ) {
                  m_nCapabilities = m_pPTZControlProperty->m_nCapabilities;
               }
            }
         }
         m_pVCM->m_csVideoChannelList.Unlock();
         m_nPrevCheckTime = nCurTime;
      }
      return ( 1 );
   } else if( m_nPTZLinkingState & PTZLinkingState_SourceVideoChannelRegisterd ) {
      if( m_pVCSrc && ( m_pVCSrc->m_nState & VCState_Deactivating ) ) {
         return ( 1 );
      }
      m_nPrevCheckTime = 0;
      {
         CCriticalSectionSP co( m_csLinkingPTZControl );
         if( m_pLinkingPTZControl ) {
            FPTZVector pvCurAbsPos = m_pLinkingPTZControl->GetAbsPos( m_PTZInfo.m_nZoomModuleType );
            SetAbsPos( pvCurAbsPos );
            m_PTZInfo.m_bThermalCamOn = m_pLinkingPTZControl->m_PTZInfo.m_bThermalCamOn;
         } else {
            logd( "[CH:%d] Source Video Channel is unregistered\n", m_nChannelNo_IVX + 1 );
            m_nPTZLinkingState &= ~PTZLinkingState_SourceVideoChannelRegisterd;
         }
      }
   }
   if( !( m_nPTZLinkingState & PTZLinkingState_SourceVideoChannelRegisterd ) )
      m_nPTZLinkingState |= PTZLinkingState_SearchingSourceVideoChannel;

   return ( DONE );
}

#endif //__SUPPORT_PTZ_CAMERA

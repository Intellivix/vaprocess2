 #pragma once

#ifdef __SUPPORT_DAYNIGHT_DETECTION

enum DayNightDetectionState
{
   DayNightDetectionState_DayTime              = 0x00000001,
   DayNightDetectionState_NightTime            = 0x00000002,
   DayNightDetectionState_DayMode              = 0x00000010,
   DayNightDetectionState_NightMode            = 0x00000020,
   DayNightDetectionState_DayModeJustChanded   = 0x00000040,
   DayNightDetectionState_NightModeJustChanded = 0x00000080,
};
 
class CSolarCalculator;

class DayNightDetection
{
public:
   uint  m_nState;
   uint  m_nPrevState;
   uint  m_nDayNightCheckTick;
   uint  m_nDayNightCheckPeriod;
   uint  m_nSunCalcTick;
   uint  m_nSunCalcPeriod;
   BCCL::Time m_tmSunRise;    // 일출 시간.
   BCCL::Time m_tmSunSet;     // 일몰 시간.
   FILETIME m_ftSunRise;
   FILETIME m_ftSunSet;
   CSolarCalculator* m_pSunCalculator;

public:
   BOOL  m_bUseUVAvg;
   float m_fUVDayThreshold;
   float m_fUVNightThreshold;

public:
   DayNightDetection (   );
   ~DayNightDetection (   );

public:
   void Initialize (CSolarCalculator* pSunCalculator);
   int  Perform  (   );
   int  Perform  (byte* pYUY2ImgBuff, int nYUY2ImgBuffLen);
   void CalcSunRiseSetTime();
   void CheckDayNightTime ();
   BOOL Read (CXMLIO* pIO);
   BOOL Write (CXMLIO* pIO);
};

#endif

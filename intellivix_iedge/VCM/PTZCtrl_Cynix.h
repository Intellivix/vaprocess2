#pragma once

//#include <afxwin.h>
#include "PTZCtrl.h"
#ifdef __SUPPORT_PTZ_CAMERA

///////////////////////////////////////////////////////////////////////////////
//
// Class: CPTZCtrl_Cynix
//
///////////////////////////////////////////////////////////////////////////////

class CPTZCtrl_Cynix : public CPTZCtrl

{
protected:
   BOOL   m_bPrevStopState;
   BOOL   m_bIsContMoving;
   uint   m_nContMoveStopTime;
   uint32_t   m_nLastInitHWTime;
   uint   m_nReqAbsPosCmdCnt;
   uint   m_nContMoveCmdCnt;
   uint   m_nPrevReqAbsPosCmdTime;
   uint32_t   m_nGotoAbsPosTime;
   float  DigitalZoomFactor;
   int    m_CynixPTZIRCtrlMode;

public:
   CPTZCtrl_Cynix (int nPTZControlType, ISize2D s2VideoSize);
   virtual ~CPTZCtrl_Cynix (   );

public:
   virtual int    Open                    (CPTZCtrlInfo* pPTZCtrlInfo);

public:                           
   virtual void   Clear                   (BYTE* pCmd, int nCmdLen);
   virtual void   CheckSum                (BYTE* pCmd, int nCmdLen);
   virtual BOOL   _InitHW                 (   );
   virtual int    _ContPTZMove            (IPTZVector move_vec);
   virtual int    _GotoAbsPos             (FPTZVector abs_pos);
   virtual int    _GetAbsPosReqCmd        (BYTE* pCmd, int& nCmdLen);
   virtual int    _PopCommand             (   );
   virtual int    _SetPreset              (int preset_no);
   virtual int    _ClearPreset            (int preset_no);
   virtual int    _GotoPreset             (int preset_no);
   virtual int    _AuxOn                  (int nAuxNo);
   virtual int    _AuxOff                 (int nAuxNo);
   virtual int    _MenuOpen               (   );
   virtual int    _MenuEnter              (   );
   virtual int    _MenuCancel             (   );
   virtual INT    _PosInit                (   );
   virtual int    _SetIRMode               (int nPTZIRCtrlMode);
public:                              
   virtual int    GetWaitingTime          (float pd);

protected:
   int            GetPositionValue        (int type, float& value) ;
   int            SetSpeed_Zoom           (   );
   int            SetSpeed_Focus          (   );
};

#endif //__SUPPORT_PTZ_CAMERA

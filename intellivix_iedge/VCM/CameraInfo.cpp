﻿#include "stdafx.h"
#include "VCM.h"
#include "CameraInfo.h"
#include "VCM_Define.h"
#include "version.h"
#include "Cryptor.h"
#include "SupportFunction.h"

///////////////////////////////////////////////////////////////////////////////
//
// Class: CTiltAngle2IRLight
//
///////////////////////////////////////////////////////////////////////////////

CTiltAngle2IRLight::CTiltAngle2IRLight()
{
   m_fTiltAngle = 0.0f;
   m_nIRLight   = 0;
}

std::string CTiltAngle2IRLight::GetItemString( int nItemIdx )
{
   std::string str;
   if( nItemIdx == 0 )
      str = sutil::sformat( "%5.1f", m_fTiltAngle );
   else if( nItemIdx == 1 )
      str = sutil::sformat( "%3d", m_nIRLight );
   return str;
}

void CTiltAngle2IRLight::CheckRange()
{
   if( m_fTiltAngle < -90.0f ) m_fTiltAngle = -90.0f;
   if( m_fTiltAngle > 90.0f ) m_fTiltAngle = 90.0f;
   if( m_nIRLight >= 512 ) m_nIRLight = 512;
   if( m_nIRLight < 0 ) m_nIRLight = 0;
}

int CTiltAngle2IRLight::Read( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "TA2IRB" ) ) {
      xmlNode.Attribute( TYPE_ID( float ), "TiltAngle", &m_fTiltAngle );
      xmlNode.Attribute( TYPE_ID( int ), "IRLight", &m_nIRLight );
      xmlNode.End();
   }
   CheckRange();
   return ( DONE );
}

int CTiltAngle2IRLight::Write( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "TA2IRB" ) ) {
      xmlNode.Attribute( TYPE_ID( float ), "TiltAngle", &m_fTiltAngle );
      xmlNode.Attribute( TYPE_ID( int ), "IRLight", &m_nIRLight );
      xmlNode.End();
   }
   return ( DONE );
}

// (mkjang-140612)
///////////////////////////////////////////////////////////////////////////////
//
// Class: CZoom2IRLight
//
///////////////////////////////////////////////////////////////////////////////

CZoom2IRLight::CZoom2IRLight()
{
   m_fZoom    = 1.0f;
   m_nIRLight = 0;
}

std::string CZoom2IRLight::GetItemString( int nItemIdx )
{
   std::string str = "";
   if( nItemIdx == 0 )
      str = sutil::sformat( "%5.1f", m_fZoom );
   else if( nItemIdx == 1 )
      str = sutil::sformat( "%3d", m_nIRLight );
   return str;
}

void CZoom2IRLight::CheckRange()
{
   if( m_fZoom < 1.0F ) m_fZoom = 1.0f;
   if( m_fZoom > 65535.0f ) m_fZoom = 65535.0f;
   if( m_nIRLight >= 512 ) m_nIRLight = 512;
   if( m_nIRLight < 0 ) m_nIRLight = 0;
}

int CZoom2IRLight::Read( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Z2IRB" ) ) {
      xmlNode.Attribute( TYPE_ID( float ), "Zoom", &m_fZoom );
      xmlNode.Attribute( TYPE_ID( int ), "IRLight", &m_nIRLight );
      xmlNode.End();
   }
   CheckRange();
   return ( DONE );
}

int CZoom2IRLight::Write( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Z2IRB" ) ) {
      xmlNode.Attribute( TYPE_ID( float ), "Zoom", &m_fZoom );
      xmlNode.Attribute( TYPE_ID( int ), "IRLight", &m_nIRLight );
      xmlNode.End();
   }
   return ( DONE );
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CIRLightCtrlInfo
//
///////////////////////////////////////////////////////////////////////////////

CIRLightCtrlInfo::CIRLightCtrlInfo()
{
   SetDefault();
   m_nZoomTblSize   = 0;
   m_nIRMappingMode = 0;
}

CIRLightCtrlInfo& CIRLightCtrlInfo::operator=( const CIRLightCtrlInfo& from )
{
   FileIO fioBuff;
   CXMLIO xmlWriteIO( &fioBuff, XMLIO_Write );
   ( (CIRLightCtrlInfo)from ).Write( &xmlWriteIO );
   fioBuff.GoToStartPos();
   CXMLIO xmlReadIO( &fioBuff, XMLIO_Read );
   Read( &xmlReadIO );
   return *this;
}

void CIRLightCtrlInfo::SetDefault()
{
   m_nIRMode = IRMode_DayTime_DayMode_NightTime_NightMode_IROff;

   // Tilt각 대응 테이블 초기화
   m_Tilt2IRLightTbl[0].m_fTiltAngle = -20.0f;
   m_Tilt2IRLightTbl[1].m_fTiltAngle = -10.0f;
   m_Tilt2IRLightTbl[2].m_fTiltAngle = 0.0f;
   m_Tilt2IRLightTbl[3].m_fTiltAngle = 5.0f;
   m_Tilt2IRLightTbl[4].m_fTiltAngle = 10.0f;
   m_Tilt2IRLightTbl[5].m_fTiltAngle = 15.0f;
   m_Tilt2IRLightTbl[6].m_fTiltAngle = 20.0f;
   m_Tilt2IRLightTbl[7].m_fTiltAngle = 25.0f;
   m_Tilt2IRLightTbl[8].m_fTiltAngle = 30.0f;
   m_Tilt2IRLightTbl[9].m_fTiltAngle = 35.0f;

   m_Tilt2IRLightTbl[0].m_nIRLight = 100;
   m_Tilt2IRLightTbl[1].m_nIRLight = 50;
   m_Tilt2IRLightTbl[2].m_nIRLight = 1;
   m_Tilt2IRLightTbl[3].m_nIRLight = 0;
   m_Tilt2IRLightTbl[4].m_nIRLight = 0;
   m_Tilt2IRLightTbl[5].m_nIRLight = 0;
   m_Tilt2IRLightTbl[6].m_nIRLight = 0;
   m_Tilt2IRLightTbl[7].m_nIRLight = 0;
   m_Tilt2IRLightTbl[8].m_nIRLight = 0;
   m_Tilt2IRLightTbl[9].m_nIRLight = 0;

   // Zoom배율 대응 테이블 초기화
   m_Zoom2IRLightTbl[0].m_fZoom = 1.0f;
   m_Zoom2IRLightTbl[1].m_fZoom = 2.0f;
   m_Zoom2IRLightTbl[2].m_fZoom = 4.0f;
   m_Zoom2IRLightTbl[3].m_fZoom = 8.0f;
   m_Zoom2IRLightTbl[4].m_fZoom = 10.0f;
   m_Zoom2IRLightTbl[5].m_fZoom = 12.0f;
   m_Zoom2IRLightTbl[6].m_fZoom = 18.0f;
   m_Zoom2IRLightTbl[7].m_fZoom = 22.0f;
   m_Zoom2IRLightTbl[8].m_fZoom = 26.0f;
   m_Zoom2IRLightTbl[9].m_fZoom = 32.0f;

   int i;
   for( i = 0; i < g_nMaxZoom2IRLightTblSize; i++ ) {
      m_Zoom2IRLightTbl[i].m_nIRLight = 0;
   }
}

int CIRLightCtrlInfo::Read( CXMLIO* pIO, BOOL bTableOnly )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "IRLightCtrl" ) ) {
      if( FALSE == bTableOnly ) {
         xmlNode.Attribute( TYPE_ID( int ), "IRMode", &m_nIRMode );
         xmlNode.Attribute( TYPE_ID( int ), "IRMappingMode", &m_nIRMappingMode );
      }

      {
         CXMLNode xmlNode2( pIO );
         if( xmlNode2.Start( "Tilt2IRLightValueTable" ) ) {
            int i;
            for( i = 0; i < g_nMaxTilt2IRLightTblSize; i++ ) {
               m_Tilt2IRLightTbl[i].Read( pIO );
            }
            xmlNode2.End();
         }
         if( xmlNode2.Start( "Zoom2IRLightValueTable" ) ) {
            int i;
            for( i = 0; i < g_nMaxZoom2IRLightTblSize; i++ ) {
               m_Zoom2IRLightTbl[i].Read( pIO );
            }
            xmlNode2.End();
         }
      }
      xmlNode.End();
      return ( DONE );
   }

   return ( 1 );
}

int CIRLightCtrlInfo::Write( CXMLIO* pIO, BOOL bTableOnly )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "IRLightCtrl" ) ) {
      if( FALSE == bTableOnly ) {
         xmlNode.Attribute( TYPE_ID( int ), "IRMode", &m_nIRMode );
         xmlNode.Attribute( TYPE_ID( int ), "IRMappingMode", &m_nIRMappingMode );
      }

      {
         CXMLNode xmlNode2( pIO );
         if( xmlNode2.Start( "Tilt2IRLightValueTable" ) ) {
            int i;
            for( i = 0; i < g_nMaxTilt2IRLightTblSize; i++ ) {
               m_Tilt2IRLightTbl[i].Write( pIO );
            }
            xmlNode2.End();
         }
         if( xmlNode2.Start( "Zoom2IRLightValueTable" ) ) {
            int i;
            for( i = 0; i < g_nMaxZoom2IRLightTblSize; i++ ) {
               m_Zoom2IRLightTbl[i].Write( pIO );
            }
            xmlNode2.End();
         }
      }
      xmlNode.End();
      return ( DONE );
   }

   return ( 1 );
}

int CIRLightCtrlInfo::GetCurTiltIRLightValue( float fTiltAngle )
{
   int nIRLight = 0;

   if( fTiltAngle < m_Tilt2IRLightTbl[0].m_fTiltAngle ) {
      nIRLight = m_Tilt2IRLightTbl[0].m_nIRLight;
   } else if( fTiltAngle > m_Tilt2IRLightTbl[9].m_fTiltAngle ) {
      nIRLight = m_Tilt2IRLightTbl[9].m_nIRLight;
   } else {
      int i;
      for( i = 1; i < g_nMaxTilt2IRLightTblSize; i++ ) {
         float fTS = m_Tilt2IRLightTbl[i - 1].m_fTiltAngle;
         float fTE = m_Tilt2IRLightTbl[i].m_fTiltAngle;
         if( fTS <= fTiltAngle && fTiltAngle < fTE ) {
            float fIRS   = static_cast<float>( m_Tilt2IRLightTbl[i - 1].m_nIRLight );
            float fIRE   = static_cast<float>( m_Tilt2IRLightTbl[i].m_nIRLight );
            float fRatio = 0.0f;
            float fTE_TS = GetAngleDiff( fTE, fTS );
            if( fTE_TS > 0.0f )
               fRatio = GetAngleDiff( fTiltAngle, fTS ) / fTE_TS;
            nIRLight = static_cast<int>( fIRS + fRatio * ( fIRE - fIRS ) );
            break;
         }
      }
   }

   //logd ("Tilt - %5.1f, IR Light = %d\n", fTiltAngle, nIRLight);
   return nIRLight;
}

int CIRLightCtrlInfo::GetCurZoomIRLightValue( float fZoom )
{
   int nIRLight = 0;

   if( fZoom < m_Zoom2IRLightTbl[0].m_fZoom )
      nIRLight = m_Zoom2IRLightTbl[0].m_nIRLight;
   else if( fZoom > m_Zoom2IRLightTbl[g_nMaxZoom2IRLightTblSize - 1].m_fZoom )
      nIRLight = m_Zoom2IRLightTbl[g_nMaxZoom2IRLightTblSize - 1].m_nIRLight;
   else {
      int i;
      for( i = 1; i < g_nMaxZoom2IRLightTblSize; i++ ) {
         float fZS = m_Zoom2IRLightTbl[i - 1].m_fZoom;
         float fZE = m_Zoom2IRLightTbl[i].m_fZoom;
         if( fZS <= fZoom && fZoom < fZE ) {
            float fIRS     = static_cast<float>( m_Zoom2IRLightTbl[i - 1].m_nIRLight );
            float fIRE     = static_cast<float>( m_Zoom2IRLightTbl[i].m_nIRLight );
            float fIRE_IRS = fIRE - fIRS;
            float fZE_ZS   = fZE - fZS;
            nIRLight       = static_cast<int>( fIRS + fIRE_IRS * ( ( fZoom - fZS ) / ( fZE_ZS ) ) );
            break;
         }
      }
   }
   //logd ("Zoom = %5.1f, IR Light = %d\n", fZoom, nIRLight);
   return nIRLight;
}

BOOL CIRLightCtrlInfo::CheckRestart( CIRLightCtrlInfo* pIRInfo )
{
   FileIO fioThis, fioFrom;
   CXMLIO xmlThis( &fioThis, XMLIO_Write );
   CXMLIO xmlFrom( &fioFrom, XMLIO_Write );
   Write( &xmlThis );
   pIRInfo->Write( &xmlFrom );
   return fioThis.IsDiff( fioFrom );
}

int CIRLightCtrlInfo::IsTheSame( CIRLightCtrlInfo* pIRInfo )
{
   FileIO fioThis, fioFrom;
   CXMLIO xmlThis( &fioThis, XMLIO_Write );
   CXMLIO xmlFrom( &fioFrom, XMLIO_Write );
   Write( &xmlThis );
   pIRInfo->Write( &xmlFrom );
   return !fioThis.IsDiff( fioFrom );
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CPTZCtrlInfo
//
///////////////////////////////////////////////////////////////////////////////

CPTZCtrlInfo::CPTZCtrlInfo()
{
   m_nPTZControlType     = 0;
   m_nCOMPortNo          = 1;
   m_nCamAddress         = 1;
   m_nBaudRateID         = 6; //CBR_9600
   m_nConnectType        = 0;
   m_nPortNo             = 0;
   m_nZoomPortNo         = 0;
   m_nTxOnly             = 0;
   m_nProtocolVersion    = 0;
   m_nPTZIdx             = 1;
   m_nBLCLevel           = 0;
   m_nVCID_LikingPTZCtrl = -1;
   m_pvCurPos( 0.0f, 0.0f, 1.0f, 0.0f, 0.0f );
   m_bUseOuterIRLightCtrlInfo  = FALSE;
   m_bIRControlByIntelliVIX    = TRUE; // (mkjang-141211)
   m_bUseCameraDayAndNigntMode = FALSE; // (mkjang-141211)
   m_bReversePanCoordinate     = FALSE;
   m_bReverseTiltCoordinate    = FALSE;
   m_bZoomProportionalJog      = FALSE; // (mkjang-140425)
   m_bZoomTriggeredAutoFocus   = FALSE;
   m_nZoomModuleType           = -1;
   m_bWiperOn                  = FALSE;
   m_bHeaterOn                 = FALSE;
   m_bThermalCamOn             = FALSE;
   m_bThermalCamMode           = FALSE;
   m_bAutoFocusOn              = TRUE; // 기본값은 True
   // (mkjang-140616)
   m_bDefogOn            = 0;
   m_bVideoStabilization = FALSE;
}

CPTZCtrlInfo::~CPTZCtrlInfo()
{
}

CPTZCtrlInfo& CPTZCtrlInfo::operator=( const CPTZCtrlInfo& from )
{
   FileIO fioBuff;
   CXMLIO xmlWriteIO( &fioBuff, XMLIO_Write );
   const_cast<CPTZCtrlInfo&>( from ).Write( &xmlWriteIO );
   fioBuff.GoToStartPos();
   CXMLIO xmlReadIO( &fioBuff, XMLIO_Read );
   Read( &xmlReadIO );
   return *this;
}

BOOL CPTZCtrlInfo::CheckChange( CPTZCtrlInfo* pPTZInfo )
{
   FileIO fioThis, fioFrom;
   CXMLIO xmlThis( &fioThis, XMLIO_Write );
   CXMLIO xmlFrom( &fioFrom, XMLIO_Write );
   Write( &xmlThis );
   pPTZInfo->Write( &xmlFrom );
   return fioThis.IsDiff( fioFrom );
}

BOOL CPTZCtrlInfo::CheckRestart( CPTZCtrlInfo* pPTZInfo )
{
   if( m_nPTZControlType != pPTZInfo->m_nPTZControlType ) return TRUE;
   if( m_nCOMPortNo != pPTZInfo->m_nCOMPortNo ) return TRUE;
   if( m_nCamAddress != pPTZInfo->m_nCamAddress ) return TRUE;
   if( m_nBaudRateID != pPTZInfo->m_nBaudRateID ) return TRUE;
   if( m_nConnectType != pPTZInfo->m_nConnectType ) return TRUE;
   if( m_nTxOnly != pPTZInfo->m_nTxOnly ) return TRUE;
   if( m_nProtocolVersion != pPTZInfo->m_nProtocolVersion ) return TRUE;
   if( m_nPTZIdx != pPTZInfo->m_nPTZIdx ) return TRUE;
   if( m_nVCID_LikingPTZCtrl != pPTZInfo->m_nVCID_LikingPTZCtrl ) return TRUE;
   if( m_nPortNo != pPTZInfo->m_nPortNo ) return TRUE;
   if( m_nZoomPortNo != pPTZInfo->m_nZoomPortNo ) return TRUE; // (mkjang-140613)
   if( m_strIPAddress != pPTZInfo->m_strIPAddress ) return TRUE; // (xinu_ba18)
   if( m_strUserName != pPTZInfo->m_strUserName ) return TRUE;
   if( m_strPassword != pPTZInfo->m_strPassword ) return TRUE;
   if( m_bUseOuterIRLightCtrlInfo != pPTZInfo->m_bUseOuterIRLightCtrlInfo ) return TRUE;
   if( m_bIRControlByIntelliVIX != pPTZInfo->m_bIRControlByIntelliVIX ) return TRUE; // (mkjang-141211)
   if( m_bUseCameraDayAndNigntMode != pPTZInfo->m_bUseCameraDayAndNigntMode ) return TRUE; // (mkjang-141211)
   if( m_bReversePanCoordinate != pPTZInfo->m_bReversePanCoordinate ) return TRUE;
   if( m_bReverseTiltCoordinate != pPTZInfo->m_bReverseTiltCoordinate ) return TRUE;
   if( m_bZoomProportionalJog != pPTZInfo->m_bZoomProportionalJog ) return TRUE; //(mkjang-140425)
   if( m_bZoomTriggeredAutoFocus != pPTZInfo->m_bZoomTriggeredAutoFocus ) return TRUE; //(mkjang-140425)
   if( m_pvOpticalAxisOffset.P != pPTZInfo->m_pvOpticalAxisOffset.P ) return TRUE;
   if( m_pvOpticalAxisOffset.T != pPTZInfo->m_pvOpticalAxisOffset.T ) return TRUE;
   if( m_nZoomModuleType != pPTZInfo->m_nZoomModuleType ) return TRUE;
   if( m_IRLightCtrlInfo.CheckRestart( &pPTZInfo->m_IRLightCtrlInfo ) ) return TRUE;
   return FALSE;
}

void CPTZCtrlInfo::CopyOnlyCommonValues( CPTZCtrlInfo* pPTZInfo )
{
   // 아래의 설정 값들은 메인객체와 서브객체와 동일하게 유지해야하는 변수임.
   m_bWiperOn                  = pPTZInfo->m_bWiperOn;
   m_bHeaterOn                 = pPTZInfo->m_bHeaterOn;
   m_bThermalCamOn             = pPTZInfo->m_bThermalCamOn;
   m_bThermalCamMode           = pPTZInfo->m_bThermalCamMode;
   m_bAutoFocusOn              = pPTZInfo->m_bAutoFocusOn;
   m_bDefogOn                  = pPTZInfo->m_bDefogOn; // (mkjang-140616)
   m_bVideoStabilization       = pPTZInfo->m_bVideoStabilization; // (mkjang-140616)
   m_bIRControlByIntelliVIX    = pPTZInfo->m_bIRControlByIntelliVIX; // (mkjang-141211)
   m_bUseCameraDayAndNigntMode = pPTZInfo->m_bUseCameraDayAndNigntMode; // (MKJANG-141211)
}

int GetNewPTZCtrlType( int nPTZControlType );

BOOL CPTZCtrlInfo::Read( CXMLIO* pIO )
{
   CPTZCtrlInfo dv; // default value

   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PTZCtrlInfo" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "CtrlType", &m_nPTZControlType );
      m_nPTZControlType = GetNewPTZCtrlType( m_nPTZControlType ); // 1099 버전까지 사용되었던 이전 PTZControlType 값를 새로운 값로 변경한다.
      // 호환성에 문제가 없어지면 GetNewPTZCtrlType 함수를 삭제해도 된다.
      xmlNode.Attribute( TYPE_ID( int ), "COMPortNo", &m_nCOMPortNo, &dv.m_nCOMPortNo );
      xmlNode.Attribute( TYPE_ID( int ), "CamAddr", &m_nCamAddress, &dv.m_nCamAddress );
      xmlNode.Attribute( TYPE_ID( int ), "BaudRateID", &m_nBaudRateID, &dv.m_nBaudRateID );
      xmlNode.Attribute( TYPE_ID( int ), "ConnType", &m_nConnectType, &dv.m_nConnectType );
      xmlNode.Attribute( TYPE_ID( int ), "TxOnly", &m_nTxOnly, &dv.m_nTxOnly );
      xmlNode.Attribute( TYPE_ID( int ), "ProtocolVersion", &m_nProtocolVersion, &dv.m_nProtocolVersion );
      xmlNode.Attribute( TYPE_ID( int ), "PTZIdx", &m_nPTZIdx, &dv.m_nPTZIdx );
      xmlNode.Attribute( TYPE_ID( int ), "BLCLevel", &m_nBLCLevel, &dv.m_nBLCLevel );
      xmlNode.Attribute( TYPE_ID( int ), "VCID_LikingPTZCtrl", &m_nVCID_LikingPTZCtrl, &dv.m_nVCID_LikingPTZCtrl );
      xmlNode.Attribute( TYPE_ID( BOOL ), "OuterIRInfo", &m_bUseOuterIRLightCtrlInfo, &dv.m_bUseOuterIRLightCtrlInfo );
      xmlNode.Attribute( TYPE_ID( BOOL ), "IRControlByIntelliVIX", &m_bIRControlByIntelliVIX, &dv.m_bIRControlByIntelliVIX ); // (mkjang-141211)
      xmlNode.Attribute( TYPE_ID( BOOL ), "UseCameraDayAndNight", &m_bUseCameraDayAndNigntMode, &dv.m_bUseCameraDayAndNigntMode ); // (mkjang-141211)
      m_bUseCameraDayAndNigntMode = FALSE;
      xmlNode.Attribute( TYPE_ID( BOOL ), "ReversePan", &m_bReversePanCoordinate, &dv.m_bReversePanCoordinate );
      xmlNode.Attribute( TYPE_ID( BOOL ), "ReverseTilt", &m_bReverseTiltCoordinate, &dv.m_bReverseTiltCoordinate );
      xmlNode.Attribute( TYPE_ID( BOOL ), "ZoomProportionalJog", &m_bZoomProportionalJog, &dv.m_bZoomProportionalJog ); // (mkjang-140425)
      xmlNode.Attribute( TYPE_ID( BOOL ), "ZoomTriggeredAutoFocus", &m_bZoomTriggeredAutoFocus, &dv.m_bZoomTriggeredAutoFocus );
      xmlNode.Attribute( TYPE_ID( BOOL ), "WiperOn", &m_bWiperOn, &dv.m_bWiperOn );
      xmlNode.Attribute( TYPE_ID( BOOL ), "HeaterOn", &m_bHeaterOn, &dv.m_bHeaterOn );
      xmlNode.Attribute( TYPE_ID( BOOL ), "ThermalCamOn", &m_bThermalCamOn, &dv.m_bThermalCamOn );
      xmlNode.Attribute( TYPE_ID( BOOL ), "ThermalCamMode", &m_bThermalCamMode, &dv.m_bThermalCamMode );
      xmlNode.Attribute( TYPE_ID( BOOL ), "AutoFocusOn", &m_bAutoFocusOn, &dv.m_bAutoFocusOn );
      xmlNode.Attribute( TYPE_ID( int ), "Defog", &m_bDefogOn, &dv.m_bDefogOn ); // (mkjang-140616)
      xmlNode.Attribute( TYPE_ID( BOOL ), "VideoStabilization", &m_bVideoStabilization, &dv.m_bVideoStabilization ); // (mkjang-140616)
      xmlNode.Attribute( TYPE_ID( float ), "Cur_P", &m_pvCurPos.P, &dv.m_pvCurPos.P );
      xmlNode.Attribute( TYPE_ID( float ), "Cur_T", &m_pvCurPos.T, &dv.m_pvCurPos.T );
      xmlNode.Attribute( TYPE_ID( float ), "Cur_Z", &m_pvCurPos.Z, &dv.m_pvCurPos.Z );
      xmlNode.Attribute( TYPE_ID( float ), "OpticalAxisOffset_Pan", &m_pvOpticalAxisOffset.P, &dv.m_pvOpticalAxisOffset.P );
      xmlNode.Attribute( TYPE_ID( float ), "OpticalAxisOffset_Tilt", &m_pvOpticalAxisOffset.T, &dv.m_pvOpticalAxisOffset.T );
      xmlNode.Attribute( TYPE_ID( int ), "ZoomModuleType", &m_nZoomModuleType, &dv.m_nZoomModuleType );
      xmlNode.Attribute( TYPE_ID( int ), "PortNo", &m_nPortNo, &dv.m_nPortNo );
      xmlNode.Attribute( TYPE_ID( int ), "ZoomPortNo", &m_nZoomPortNo, &dv.m_nZoomPortNo ); // (mkjang-140613)
      xmlNode.Attribute( TYPE_ID( string ), "IPAddr", &m_strIPAddress, &dv.m_strIPAddress ); // (xinu_bc23)
#ifdef __SUPPORT_WIN32_CRYPT
      int nLength = 0;
      BYTE aCryptBuffer[300];
      CCryption CryptoinObject;
      // 사용자 이름
      {
         ZeroMemory( aCryptBuffer, sizeof( aCryptBuffer ) );
         xmlNode.Attribute( TYPE_ID( int ), "EUNL", &nLength ); // EUNL : Encoded User Name Length
         xmlNode.Attribute( TYPE_ID( XBYTE ), "EUN", aCryptBuffer, nLength ); // EUN  : Encoded User Name
         ZeroMemory( CryptoinObject.m_szDecrypted, MAX_CRYPT_LETTER );
         CryptoinObject.Decrypt( (LPCSTR)aCryptBuffer, nLength );
         m_strUserName = CryptoinObject.m_szDecrypted;
      }
      // 패스워드
      {
         ZeroMemory( aCryptBuffer, sizeof( aCryptBuffer ) );
         ZeroMemory( CryptoinObject.m_szDecrypted, MAX_CRYPT_LETTER );
         xmlNode.Attribute( TYPE_ID( int ), "EPWL", &nLength ); // EPWL : Encoded User Name Length
         xmlNode.Attribute( TYPE_ID( XBYTE ), "EPW", aCryptBuffer, nLength ); // EPW  : Encoded Password
         CryptoinObject.Decrypt( (LPCSTR)aCryptBuffer, nLength );
         m_strPassword = CryptoinObject.m_szDecrypted;
      }
#else
      xmlNode.Attribute( TYPE_ID( string ), "EUN", &m_strUserName, &dv.m_strUserName );
      xmlNode.Attribute( TYPE_ID( string ), "EPW", &m_strPassword, &dv.m_strPassword );
#endif
      m_IRLightCtrlInfo.Read( pIO );
      xmlNode.End();

      // jun: 예외처리 추가
      if( PTZCtrlConnType_LinkToPTZCtrl != m_nConnectType ) {
         m_nVCID_LikingPTZCtrl = -1;
      }
      PTZControlProperty* pPTZProperty = GetPTZControlProperty( m_nPTZControlType );
      if( pPTZProperty ) {
         if( FALSE == ( pPTZProperty->m_nCapabilities & PTZCtrlCap_PanTiltCoordinateChangeable ) ) {
            m_bReversePanCoordinate  = FALSE;
            m_bReverseTiltCoordinate = FALSE;
         }
      }
   }
   return TRUE;
}

BOOL CPTZCtrlInfo::Write( CXMLIO* pIO )
{
   CPTZCtrlInfo dv; // default value
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PTZCtrlInfo" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "CtrlType", &m_nPTZControlType );
      xmlNode.Attribute( TYPE_ID( int ), "COMPortNo", &m_nCOMPortNo, &dv.m_nCOMPortNo );
      xmlNode.Attribute( TYPE_ID( int ), "CamAddr", &m_nCamAddress, &dv.m_nCamAddress );
      xmlNode.Attribute( TYPE_ID( int ), "BaudRateID", &m_nBaudRateID, &dv.m_nBaudRateID );
      xmlNode.Attribute( TYPE_ID( int ), "ConnType", &m_nConnectType, &dv.m_nConnectType );
      xmlNode.Attribute( TYPE_ID( int ), "TxOnly", &m_nTxOnly, &dv.m_nTxOnly );
      xmlNode.Attribute( TYPE_ID( int ), "ProtocolVersion", &m_nProtocolVersion, &dv.m_nProtocolVersion );
      xmlNode.Attribute( TYPE_ID( int ), "PTZIdx", &m_nPTZIdx, &dv.m_nPTZIdx );
      xmlNode.Attribute( TYPE_ID( int ), "BLCLevel", &m_nBLCLevel, &dv.m_nBLCLevel );
      xmlNode.Attribute( TYPE_ID( int ), "VCID_LikingPTZCtrl", &m_nVCID_LikingPTZCtrl, &dv.m_nVCID_LikingPTZCtrl );
      xmlNode.Attribute( TYPE_ID( BOOL ), "OuterIRInfo", &m_bUseOuterIRLightCtrlInfo, &dv.m_bUseOuterIRLightCtrlInfo );
      xmlNode.Attribute( TYPE_ID( BOOL ), "IRControlByIntelliVIX", &m_bIRControlByIntelliVIX, &dv.m_bIRControlByIntelliVIX ); // (mkjang-141211)
      m_bUseCameraDayAndNigntMode = FALSE;
      xmlNode.Attribute( TYPE_ID( BOOL ), "UseCameraDayAndNight", &m_bUseCameraDayAndNigntMode, &dv.m_bUseCameraDayAndNigntMode ); // (mkjang-141211)
      xmlNode.Attribute( TYPE_ID( BOOL ), "ReversePan", &m_bReversePanCoordinate, &dv.m_bReversePanCoordinate );
      xmlNode.Attribute( TYPE_ID( BOOL ), "ReverseTilt", &m_bReverseTiltCoordinate, &dv.m_bReverseTiltCoordinate );
      xmlNode.Attribute( TYPE_ID( BOOL ), "ZoomProportionalJog", &m_bZoomProportionalJog, &dv.m_bZoomProportionalJog ); // (mkjang-140425)
      xmlNode.Attribute( TYPE_ID( BOOL ), "ZoomTriggeredAutoFocus", &m_bZoomTriggeredAutoFocus, &dv.m_bZoomTriggeredAutoFocus );
      xmlNode.Attribute( TYPE_ID( BOOL ), "WiperOn", &m_bWiperOn, &dv.m_bWiperOn );
      xmlNode.Attribute( TYPE_ID( BOOL ), "HeaterOn", &m_bHeaterOn, &dv.m_bHeaterOn );
      xmlNode.Attribute( TYPE_ID( BOOL ), "ThermalCamOn", &m_bThermalCamOn, &dv.m_bThermalCamOn );
      xmlNode.Attribute( TYPE_ID( BOOL ), "ThermalCamMode", &m_bThermalCamMode, &dv.m_bThermalCamMode );
      xmlNode.Attribute( TYPE_ID( BOOL ), "AutoFocusOn", &m_bAutoFocusOn, &dv.m_bAutoFocusOn );
      xmlNode.Attribute( TYPE_ID( int ), "Defog", &m_bDefogOn, &dv.m_bDefogOn ); // (mkjang-140616)
      xmlNode.Attribute( TYPE_ID( BOOL ), "VideoStabilization", &m_bVideoStabilization, &dv.m_bVideoStabilization ); // (mkjang-140616)
      xmlNode.Attribute( TYPE_ID( float ), "Cur_P", &m_pvCurPos.P, &dv.m_pvCurPos.P );
      xmlNode.Attribute( TYPE_ID( float ), "Cur_T", &m_pvCurPos.T, &dv.m_pvCurPos.T );
      xmlNode.Attribute( TYPE_ID( float ), "Cur_Z", &m_pvCurPos.Z, &dv.m_pvCurPos.Z );
      xmlNode.Attribute( TYPE_ID( float ), "OpticalAxisOffset_Pan", &m_pvOpticalAxisOffset.P, &dv.m_pvOpticalAxisOffset.P );
      xmlNode.Attribute( TYPE_ID( float ), "OpticalAxisOffset_Tilt", &m_pvOpticalAxisOffset.T, &dv.m_pvOpticalAxisOffset.T );
      xmlNode.Attribute( TYPE_ID( int ), "ZoomModuleType", &m_nZoomModuleType, &dv.m_nZoomModuleType );
      xmlNode.Attribute( TYPE_ID( int ), "PortNo", &m_nPortNo, &dv.m_nPortNo );
      xmlNode.Attribute( TYPE_ID( int ), "ZoomPortNo", &m_nZoomPortNo, &dv.m_nZoomPortNo ); // (mkjang-140613)
      xmlNode.Attribute( TYPE_ID( string ), "IPAddr", &m_strIPAddress, &dv.m_strIPAddress ); // (xinu_bc23)
#ifdef __SUPPORT_WIN32_CRYPT
      int nLength = 0;
      CCryption CryptoinObject;
      if( m_strUserName.length() ) {
         CryptoinObject.Encrypt( (LPCTSTR)m_strUserName.c_str(), nLength );
         xmlNode.Attribute( TYPE_ID( int ), "EUNL", &nLength ); // EUNL : Encoded User Name Length
         xmlNode.Attribute( TYPE_ID( XBYTE ), "EUN", CryptoinObject.m_szEncrypted, nLength ); // EUN  : Encoded User Name
      }
      if( m_strPassword.length() ) {
         nLength = 0;
         CryptoinObject.Encrypt( (LPCTSTR)m_strPassword.c_str(), nLength );
         xmlNode.Attribute( TYPE_ID( int ), "EPWL", &nLength ); // EPWL : Encoded User Name Length
         xmlNode.Attribute( TYPE_ID( XBYTE ), "EPW", CryptoinObject.m_szEncrypted, nLength ); // EPW  : Encoded Password
      }
#else
      xmlNode.Attribute( TYPE_ID( string ), "EUN", &m_strUserName, &dv.m_strUserName );
      xmlNode.Attribute( TYPE_ID( string ), "EPW", &m_strPassword, &dv.m_strPassword );
#endif
      CIRLightCtrlInfo dv;
      if( !m_IRLightCtrlInfo.IsTheSame( &dv ) )
         m_IRLightCtrlInfo.Write( pIO );

      xmlNode.End();
      return TRUE;
   }
   return FALSE;
}

///////////////////////////////////////////////////////////////////////////
//
// Class : ICameraInfo
//
///////////////////////////////////////////////////////////////////////////

int DefValue_Width     = 0;
int DefValue_Height    = 0;
int DefValue_HWDecoder = 0;

ICameraInfo::ICameraInfo()
{
   m_bPanorama = FALSE;
   m_bRemote   = FALSE;

   m_nCameraType  = CameraType_None;
   m_nVideoType   = VideoType_None;
   m_nWidth       = DefValue_Width;
   m_nHeight      = DefValue_Height;
   m_bHWDecoder   = DefValue_HWDecoder;
   m_fDecodingFPS = 0;
}

ICameraInfo::~ICameraInfo()
{
}

ICameraInfo& ICameraInfo::operator=( const ICameraInfo& from )
{
   m_bPanorama = from.m_bPanorama;

   m_nCameraType = from.m_nCameraType;
   m_nVideoType  = from.m_nVideoType;
   m_nWidth      = from.m_nWidth;
   m_nHeight     = from.m_nHeight;
   return *this;
}

BOOL ICameraInfo::Read( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "ICamInfo" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "CamType", &m_nCameraType );
      xmlNode.Attribute( TYPE_ID( int ), "VideoType", &m_nVideoType );
      xmlNode.Attribute( TYPE_ID( int ), "Width", &m_nWidth, &DefValue_Width );
      xmlNode.Attribute( TYPE_ID( int ), "Height", &m_nHeight, &DefValue_Height );
      xmlNode.Attribute( TYPE_ID( int ), "HWDecoder", &m_bHWDecoder, &DefValue_HWDecoder );
      xmlNode.End();
   }

   // 예외처리
   {
      if( m_nWidth < 0 ) m_nWidth = 0;
      if( m_nHeight < 0 ) m_nHeight = 0;
   }
   return TRUE;
}

BOOL ICameraInfo::Write( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "ICamInfo" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "CamType", &m_nCameraType );
      xmlNode.Attribute( TYPE_ID( int ), "VideoType", &m_nVideoType );
      xmlNode.Attribute( TYPE_ID( int ), "Width", &m_nWidth, &DefValue_Width );
      xmlNode.Attribute( TYPE_ID( int ), "Height", &m_nHeight, &DefValue_Height );
      xmlNode.Attribute( TYPE_ID( int ), "HWDecoder", &m_bHWDecoder, &DefValue_HWDecoder );
      xmlNode.End();
      return TRUE;
   }
   return FALSE;
}

BOOL ICameraInfo::SetVideoSizeFromCameraInfo()
{
   return FALSE;
}

BOOL ICameraInfo::CheckChange( ICameraInfo* pICFrom )
{
   FileIO fioThis, fioFrom;
   CXMLIO xmlThis( &fioThis, XMLIO_Write );
   CXMLIO xmlFrom( &fioFrom, XMLIO_Write );
   Write( &xmlThis );
   pICFrom->Write( &xmlFrom );
   return fioThis.IsDiff( fioFrom );
}

BOOL ICameraInfo::CheckRestart( ICameraInfo* pICFrom )
{
   if( m_nCameraType != pICFrom->m_nCameraType ) return TRUE;
   if( m_nVideoType != pICFrom->m_nVideoType ) return TRUE;
   if( m_nWidth != pICFrom->m_nWidth ) return TRUE;
   if( m_nHeight != pICFrom->m_nHeight ) return TRUE;
   //if (m_fDecodingFPS != pICFrom->m_fDecodingFPS) return TRUE;
   return FALSE;
}

void ICameraInfo::SetVideoSize( int nWidth, int nHeight )
{
   m_nWidth  = nWidth;
   m_nHeight = nHeight;
}

void ICameraInfo::SetVideoSize( ISize2D videoSize )
{
   m_nWidth  = videoSize.Width;
   m_nHeight = videoSize.Height;
}

///////////////////////////////////////////////////////////////////////////
//
// Class : CAnalogCameraInfo
//
///////////////////////////////////////////////////////////////////////////

CAnalogCameraInfo::CAnalogCameraInfo()
{
   ICameraInfo::m_nCameraType = CameraType_AnalogCamera;

   m_nVCID             = -1; // 점유하고 있는 채널이 없음.
   m_nVideoChannelType = 0;

   m_nCaptureBoardType = 0;
   m_nCaptureBoardIdx  = 0;
   m_nVideoType        = VideoType_None;
   m_nResolutionIdx    = 0;
   m_fFrameRate        = 15.0f;
   m_nMaxBandWidth     = 0;
   m_nChannelNo        = 0;

   int i;
   for( i = 0; i < ImagePropertyTypeNum; i++ )
      m_nImageProperties[i] = -1;
}

CAnalogCameraInfo::~CAnalogCameraInfo()
{
}

CAnalogCameraInfo& CAnalogCameraInfo::operator=( const CAnalogCameraInfo& from )
{
   ICameraInfo* pThis  = (ICameraInfo*)this;
   ICameraInfo* pFrom  = (ICameraInfo*)&from;
   *pThis              = *pFrom;
   m_nVCID             = from.m_nVCID;
   m_nVideoChannelType = from.m_nVideoChannelType;

   m_nChannelNo        = from.m_nChannelNo;
   m_nCaptureBoardType = from.m_nCaptureBoardType;
   m_nCaptureBoardIdx  = from.m_nCaptureBoardIdx;
   m_nResolutionIdx    = from.m_nResolutionIdx;
   m_nMaxBandWidth     = from.m_nMaxBandWidth;
   m_fFrameRate        = from.m_fFrameRate;

   memcpy( m_nImageProperties, from.m_nImageProperties, sizeof( int ) * ImagePropertyTypeNum );

   return *this;
}

void CAnalogCameraInfo::SetCBChannelNo( int nChIdx )
{
   m_nChannelNo = nChIdx;
}

int CAnalogCameraInfo::GetCBChannelNo()
{
   return m_nChannelNo;
}

BOOL CAnalogCameraInfo::CheckChange( ICameraInfo* pICFrom )
{
   FileIO fioThis, fioFrom;
   CXMLIO xmlThis( &fioThis, XMLIO_Write );
   CXMLIO xmlFrom( &fioFrom, XMLIO_Write );
   Write( &xmlThis );
   pICFrom->Write( &xmlFrom );
   return fioThis.IsDiff( fioFrom );
}

BOOL CAnalogCameraInfo::CheckRestart( ICameraInfo* pICFrom )
{
   if( ICameraInfo::CheckRestart( pICFrom ) ) return TRUE;

   CAnalogCameraInfo* pFrom = (CAnalogCameraInfo*)pICFrom;
   if( m_nChannelNo != pFrom->m_nChannelNo ) return TRUE;
   if( m_nCaptureBoardType != pFrom->m_nCaptureBoardType ) return TRUE;
   if( m_nCaptureBoardIdx != pFrom->m_nCaptureBoardIdx ) return TRUE;
   if( m_nResolutionIdx != pFrom->m_nResolutionIdx ) return TRUE;
   if( m_fFrameRate != pFrom->m_fFrameRate ) return TRUE;
   return FALSE;
}

BOOL CAnalogCameraInfo::Read( CXMLIO* pIO )
{
   CAnalogCameraInfo dv;
   int nImagePropertyNum;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "AnalogCameraInfo" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "VCID", &m_nVCID );
      xmlNode.Attribute( TYPE_ID( int ), "VideoChannelType", &m_nVideoChannelType, &dv.m_nVideoChannelType );
      xmlNode.Attribute( TYPE_ID( int ), "ChannelNo", &m_nChannelNo, &dv.m_nChannelNo );
      xmlNode.Attribute( TYPE_ID( int ), "CaptureBoardType", &m_nCaptureBoardType, &dv.m_nCaptureBoardType );
      xmlNode.Attribute( TYPE_ID( int ), "CaptureBoardIdx", &m_nCaptureBoardIdx, &dv.m_nCaptureBoardIdx );
      xmlNode.Attribute( TYPE_ID( int ), "ResolutionIdx", &m_nResolutionIdx, &dv.m_nResolutionIdx );
      xmlNode.Attribute( TYPE_ID( float ), "FrameRate", &m_fFrameRate, &dv.m_fFrameRate );
      xmlNode.Attribute( TYPE_ID( int ), "ImagePropertyNum", &nImagePropertyNum );
      xmlNode.Attribute( TYPE_ID( int ), "ImageProperties", m_nImageProperties, nImagePropertyNum );
      ICameraInfo::Read( pIO );
      xmlNode.End();
      return TRUE;
   }
   return FALSE;
}

BOOL CAnalogCameraInfo::Write( CXMLIO* pIO )
{
   CAnalogCameraInfo dv;
   int nImagePropertyNum = ImagePropertyTypeNum;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "AnalogCameraInfo" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "VCID", &m_nVCID );
      xmlNode.Attribute( TYPE_ID( int ), "VideoChannelType", &m_nVideoChannelType, &dv.m_nVideoChannelType );
      xmlNode.Attribute( TYPE_ID( int ), "ChannelNo", &m_nChannelNo, &dv.m_nChannelNo );
      xmlNode.Attribute( TYPE_ID( int ), "CaptureBoardType", &m_nCaptureBoardType, &dv.m_nCaptureBoardType );
      xmlNode.Attribute( TYPE_ID( int ), "CaptureBoardIdx", &m_nCaptureBoardIdx, &dv.m_nCaptureBoardIdx );
      xmlNode.Attribute( TYPE_ID( int ), "ResolutionIdx", &m_nResolutionIdx, &dv.m_nResolutionIdx );
      xmlNode.Attribute( TYPE_ID( float ), "FrameRate", &m_fFrameRate, &dv.m_fFrameRate );
      xmlNode.Attribute( TYPE_ID( int ), "ImagePropertyNum", &nImagePropertyNum );
      xmlNode.Attribute( TYPE_ID( int ), "ImageProperties", m_nImageProperties, nImagePropertyNum );
      ICameraInfo::Write( pIO );
      xmlNode.End();
      return TRUE;
   }
   return FALSE;
}

BOOL CAnalogCameraInfo::SetInfo( ICameraInfo* pInfo )
{
   if( pInfo->m_nCameraType == CameraType_AnalogCamera ) {
      CAnalogCameraInfo* pAnalogCamInfo = (CAnalogCameraInfo*)pInfo;
      *this                             = *pAnalogCamInfo;
      return TRUE;
   } else
      return FALSE;
}

// SetVideoSizeFromCameraInfo를 해야한는 이유
// 설정에서는 비디오 해상도의 인덱스만 설정하기 때문에 영상 사이즈를 얻기 위해서는 아래 함수를 호출해야한다.
BOOL CAnalogCameraInfo::SetVideoSizeFromCameraInfo()
{
   CaptureBoardProperty* pProperty = GetCaptureBoardProporty( m_nCaptureBoardType );
   if( pProperty ) {
      ISize2D videoSize = pProperty->m_ResolutionListMgr.GetVideoRosolution( m_nVideoType, m_nResolutionIdx );
      if( videoSize.Width != 0 ) {
         m_nWidth  = videoSize.Width;
         m_nHeight = videoSize.Height;
         return TRUE;
      }
   }
   return FALSE;
}

///////////////////////////////////////////////////////////////////////////
//
// class: CIPCameraInfo
//
///////////////////////////////////////////////////////////////////////////

CIPCameraInfo::CIPCameraInfo()
{
   ICameraInfo::m_nCameraType = CameraType_IPCamera;

   // 저장용 변수
   m_nIPCameraType                = IPCameraType_None;
   m_nIPCameraTypeForExtraPTZCtrl = 0;
   m_nPortNo                      = 0;
   m_nPTZPortNo                   = 0;
   m_nVideoFormat_Cap             = 0;
   m_nVideoChNo                   = 0;
   m_nVideoStreamNo               = 0;
   m_nResolutionIdx               = 0;
   m_nCompression                 = 30;
   m_fFPS_Cap                     = 30.0;
   m_strIPAddress.clear();
   m_strURL = "";

   m_strUserName = "";
   m_strPassword = "";

   m_bUsePTZ              = FALSE;
   m_nPTZControlType      = 0;
   m_nDeviceID            = 0;
   m_bReceiveMetaDataOnly = FALSE;

   // 일반 변수
   m_pTimerMgr   = NULL;
   m_pSrcImgBuff = NULL;
   m_pStream     = NULL;
   m_nCompanyID  = 0;

   m_nSocketType     = 1;
   m_nUseAudioOut    = 1; // 기본값: 카메라에 직접 연결 (1)

   m_nStreamType  = 0;
   m_bNewHttpRule = FALSE;
}

CIPCameraInfo::~CIPCameraInfo()
{
}

CIPCameraInfo& CIPCameraInfo::operator=( const CIPCameraInfo& from )
{
   ////// 설정 저장용 변수 ///////
   m_nIPCamStreamIdx              = from.m_nIPCamStreamIdx;
   m_bUseStream                   = from.m_bUseStream;
   m_nIPCameraType                = from.m_nIPCameraType;
   m_nIPCameraTypeForExtraPTZCtrl = from.m_nIPCameraTypeForExtraPTZCtrl;
   m_nPortNo                      = from.m_nPortNo;
   m_nPTZPortNo                   = from.m_nPTZPortNo;
   m_nVideoFormat_Cap             = from.m_nVideoFormat_Cap;
   m_nVideoChNo                   = from.m_nVideoChNo;
   m_nVideoStreamNo               = from.m_nVideoStreamNo;
   m_nResolutionIdx               = from.m_nResolutionIdx;
   m_nCompression                 = from.m_nCompression;
   m_fFPS_Cap                     = from.m_fFPS_Cap;
   m_strIPAddress                 = from.m_strIPAddress; // (xinu_bc23)
   m_strURL                       = from.m_strURL;

   m_strUserName = from.m_strUserName;
   m_strPassword = from.m_strPassword;

   m_bUsePTZ      = from.m_bUsePTZ;
   m_bNewHttpRule = from.m_bNewHttpRule;

   m_nPTZControlType      = from.m_nPTZControlType;
   m_nDeviceID            = from.m_nDeviceID;
   m_strDeviceName        = from.m_strDeviceName;
   m_strDeviceGUID        = from.m_strDeviceGUID;
   m_bReceiveMetaDataOnly = from.m_bReceiveMetaDataOnly;

   m_PTZInfo = from.m_PTZInfo;

   ////// 일반 변수 ///////
   m_nCompanyID = from.m_nCompanyID;
   m_pTimerMgr  = from.m_pTimerMgr;

   // Support Omnicast
   m_strDeviceUID = from.m_strDeviceUID;
   m_strLogicalID = from.m_strLogicalID;
   // Support RealHub
   m_nSocketType     = from.m_nSocketType;
   m_nUseAudioOut    = from.m_nUseAudioOut;
   m_nStreamType     = from.m_nStreamType;

   ICameraInfo* pThis = (ICameraInfo*)this;
   ICameraInfo* pFrom = (ICameraInfo*)&from;
   *pThis             = *pFrom;

   return *this;
}

BOOL CIPCameraInfo::CheckParams()
{
   switch( m_nIPCameraType ) {
   case IPCameraType_RTSP:
   case IPCameraType_ACES:
   case IPCameraType_Omnicast:
   case IPCameraType_RealHub:
   case IPCameraType_Innodep:
   case IPCameraType_XProtect:
   case IPCameraType_Onvif: // (mkjang-150427)
      break;
   default: {
      // 주소값을 설정하지 않은 경우.
      if( m_strIPAddress.empty() )
         return ( IPCamErr_EmptyIPAddress );

      // IP 주소 값이 잘못 되어있을 경우
      std::string strIPAddress;
      if( !GetIPAddrString( strIPAddress, m_strIPAddress ) ) // (xinu_bc23)
         return ( IPCamErr_InvalidIPAddress );
   } break;
   }
   // IP 카메라 타입을 지정하지 않았을 경우
   if( m_nIPCameraType == 0 )
      return ( IPCamErr_EmptyCameraType );

   if( m_nIPCameraType != IPCameraType_RTSP ) {
      // 사용자 및 암호를 설정하지 않았을 경우
      if( m_strPassword.empty() )
         return ( IPCamErr_EmptyPassword );
   }
   return ( DONE );
}

BOOL CIPCameraInfo::CheckChange( ICameraInfo* pICFrom )
{
   return CheckRestart( pICFrom );
}

BOOL CIPCameraInfo::CheckRestart( ICameraInfo* pICFrom )
{
   if( ICameraInfo::CheckRestart( pICFrom ) ) return TRUE;

   CIPCameraInfo* pFrom = (CIPCameraInfo*)pICFrom;

   if( m_nIPCamStreamIdx != pFrom->m_nIPCamStreamIdx ) return TRUE;
   if( m_bUseStream != pFrom->m_bUseStream ) return TRUE;
   if( m_nIPCameraType != pFrom->m_nIPCameraType ) return TRUE;
   if( m_nIPCameraTypeForExtraPTZCtrl != pFrom->m_nIPCameraTypeForExtraPTZCtrl ) return TRUE;
   if( m_nPortNo != pFrom->m_nPortNo ) return TRUE;
   if( m_nPTZPortNo != pFrom->m_nPTZPortNo ) return TRUE;
   if( m_nVideoFormat_Cap != pFrom->m_nVideoFormat_Cap ) return TRUE;
   if( m_nVideoChNo != pFrom->m_nVideoChNo ) return TRUE;
   if( m_nVideoStreamNo != pFrom->m_nVideoStreamNo ) return TRUE;
   if( m_nResolutionIdx != pFrom->m_nResolutionIdx ) return TRUE;
   if( m_nCompression != pFrom->m_nCompression ) return TRUE;
   // 주의: 특정 IP 카메라의 경우 프레임 비율 값이 일정하지 않기 때문에
   //       이때는 카메라를 재시작 하지 않도록 한다.
   if( m_nPTZControlType != PTZControlType_ICanTek ) {
      if( m_fFPS_Cap != pFrom->m_fFPS_Cap ) return TRUE;
   }
   if( m_strIPAddress != pFrom->m_strIPAddress ) return TRUE; // (xinu_bc23)
   if( m_strURL != pFrom->m_strURL ) return TRUE;

   if( m_strUserName != pFrom->m_strUserName ) return TRUE;
   if( m_strPassword != pFrom->m_strPassword ) return TRUE;

   if( m_bUsePTZ != pFrom->m_bUsePTZ ) return TRUE;
   if( m_nPTZControlType != pFrom->m_nPTZControlType ) return TRUE;
   if( m_nDeviceID != pFrom->m_nDeviceID ) return TRUE;
   if( m_bReceiveMetaDataOnly != pFrom->m_bReceiveMetaDataOnly ) return TRUE;
   // Support Omnicast
   if( m_strDeviceUID != pFrom->m_strDeviceUID ) return TRUE;
   if( m_strLogicalID != pFrom->m_strLogicalID ) return TRUE;
   // Support RealHub
   if( m_nSocketType != pFrom->m_nSocketType ) return TRUE;
   if( m_nUseAudioOut != pFrom->m_nUseAudioOut ) return TRUE;
   if( m_nStreamType != pFrom->m_nStreamType ) return TRUE;

#ifdef __SUPPORT_FISHEYE_CAMERA
   if( m_FisheyeDewarpingSetup != pFrom->m_FisheyeDewarpingSetup ) return TRUE;
#endif
   if( m_bNewHttpRule != pFrom->m_bNewHttpRule ) return TRUE;
   if( m_PTZInfo.CheckRestart( &pFrom->m_PTZInfo ) ) return TRUE;
   return FALSE;
}

BOOL CIPCameraInfo::Read( const std::string& strNodeName, CXMLIO* pIO )
{
   static CIPCameraInfo dv; // Defalut Value
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( strNodeName.c_str() ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "IPCamStreamIdx", &m_nIPCamStreamIdx );
      xmlNode.Attribute( TYPE_ID( BOOL ), "UseStream", &m_bUseStream, &dv.m_bUseStream );
      xmlNode.Attribute( TYPE_ID( int ), "IPCamType", &m_nIPCameraType, &dv.m_nIPCameraType );
      xmlNode.Attribute( TYPE_ID( int ), "IPCamTypeEPTZC", &m_nIPCameraTypeForExtraPTZCtrl, &dv.m_nIPCameraTypeForExtraPTZCtrl );
      xmlNode.Attribute( TYPE_ID( int ), "PortNo", &m_nPortNo, &dv.m_nPortNo );
      xmlNode.Attribute( TYPE_ID( int ), "VF_Cap", &m_nVideoFormat_Cap, &dv.m_nVideoFormat_Cap );
      xmlNode.Attribute( TYPE_ID( int ), "VideoChNo", &m_nVideoChNo, &dv.m_nVideoChNo );
      xmlNode.Attribute( TYPE_ID( int ), "VideoStreamNo", &m_nVideoStreamNo, &dv.m_nVideoStreamNo );
      xmlNode.Attribute( TYPE_ID( int ), "ResolutionIdx", &m_nResolutionIdx, &dv.m_nResolutionIdx );
      xmlNode.Attribute( TYPE_ID( int ), "Compress", &m_nCompression, &dv.m_nCompression );
      xmlNode.Attribute( TYPE_ID( float ), "FPS_Cap", &m_fFPS_Cap, &dv.m_fFPS_Cap );
      xmlNode.Attribute( TYPE_ID( string ), "IPAddr", &m_strIPAddress, &dv.m_strIPAddress );
      xmlNode.Attribute( TYPE_ID( string ), "URL", &m_strURL, &dv.m_strURL );

      // 영상접속 포트와 PTZ제어포트를 구분한다.
      int nIPCamCaps                   = 0;
      IPCameraProperty* pIPCamProperty = GetIPCameraProperty( m_nIPCameraType );
      if( pIPCamProperty ) nIPCamCaps = pIPCamProperty->m_nCapabilities;
      BOOL bSupportPTZPortNo = FALSE;
      if( nIPCamCaps & IPCamCaps_NeedExtraPTZPortNo ) bSupportPTZPortNo = TRUE;
      BOOL bSupportRTSP = FALSE;
      if( nIPCamCaps & IPCamCaps_ReceiveVideoStreamByRTSP ) bSupportRTSP = TRUE;

      if( FALSE == xmlNode.Attribute( TYPE_ID( int ), "PTZPortNo", &m_nPTZPortNo, &dv.m_nPTZPortNo ) ) {
         // 하위버전 설정을 복원한다.
         if( bSupportPTZPortNo ) {
            if( pIPCamProperty && pIPCamProperty->m_nDefPTZPortNo )
               m_nPTZPortNo = pIPCamProperty->m_nDefPTZPortNo;
            else
               m_nPTZPortNo = m_nPortNo;
            if( bSupportRTSP )
               m_nPortNo = 554;
         }
      }

#ifdef __SUPPORT_WIN32_CRYPT
      // 인코딩되지 않은 IP/PW이면 인코딩한다.
      int nLength = 0;
      BYTE aCryptBuffer[300];
      CCryption CryptoinObject;

      if( FALSE == xmlNode.Attribute( TYPE_ID( string ), "UserName", &m_strUserName ) ) {
         ZeroMemory( aCryptBuffer, sizeof( aCryptBuffer ) );
         xmlNode.Attribute( TYPE_ID( int ), "EUNL", &nLength ); // EUNL : Encoded User Name Length
         xmlNode.Attribute( TYPE_ID( XBYTE ), "EUN", aCryptBuffer, nLength ); // EUN  : Encoded User Name
         ZeroMemory( CryptoinObject.m_szDecrypted, MAX_CRYPT_LETTER );
         CryptoinObject.Decrypt( (LPCSTR)aCryptBuffer, nLength );
         m_strUserName = CryptoinObject.m_szDecrypted;
      }

      if( FALSE == xmlNode.Attribute( TYPE_ID( string ), "Pwd", &m_strPassword ) ) {
         nLength = 0;
         ZeroMemory( aCryptBuffer, sizeof( aCryptBuffer ) );
         ZeroMemory( CryptoinObject.m_szDecrypted, MAX_CRYPT_LETTER );
         xmlNode.Attribute( TYPE_ID( int ), "EPWL", &nLength ); // EPWL : Encoded User Name Length
         xmlNode.Attribute( TYPE_ID( XBYTE ), "EPW", aCryptBuffer, nLength ); // EPW  : Encoded Password
         CryptoinObject.Decrypt( (LPCSTR)aCryptBuffer, nLength );
         m_strPassword = CryptoinObject.m_szDecrypted;
      }
#else
      xmlNode.Attribute( TYPE_ID( string ), "EUN", &m_strUserName );
      xmlNode.Attribute( TYPE_ID( string ), "EPW", &m_strPassword );
#endif

      xmlNode.Attribute( TYPE_ID( BOOL ), "IsUsePTZ", &m_bUsePTZ, &dv.m_bUsePTZ );
      xmlNode.Attribute( TYPE_ID( int ), "PTZCtrlType", &m_nPTZControlType, &dv.m_nPTZControlType );
      xmlNode.Attribute( TYPE_ID( string ), "DeviceName", &m_strDeviceName, &dv.m_strDeviceName );
      xmlNode.Attribute( TYPE_ID( BOOL ), "RecvMetadataOnly", &m_bReceiveMetaDataOnly, &dv.m_bReceiveMetaDataOnly );

      xmlNode.Attribute( TYPE_ID( string ), "DeviceUID", &m_strDeviceUID, &dv.m_strDeviceUID );
      xmlNode.Attribute( TYPE_ID( string ), "LogicalID", &m_strLogicalID, &dv.m_strLogicalID );
      xmlNode.Attribute( TYPE_ID( int ), "SocketType", &m_nSocketType, &dv.m_nSocketType );
      xmlNode.Attribute( TYPE_ID( int ), "UseAudioOut", &m_nUseAudioOut, &dv.m_nUseAudioOut );
      xmlNode.Attribute( TYPE_ID( string ), "DeviceGUID", &m_strDeviceGUID, &dv.m_strDeviceGUID );

      xmlNode.Attribute( TYPE_ID( int ), "StreamType", &m_nStreamType, &dv.m_nStreamType );

      xmlNode.Attribute( TYPE_ID( BOOL ), "NewHttpType", &m_bNewHttpRule, &dv.m_bNewHttpRule );

      if( m_fFPS_Cap > 30.0f ) m_fFPS_Cap = 30.0f;
      if( m_fFPS_Cap < 1.0f ) m_fFPS_Cap = 1.0f;

      SetVideoSizeFromCameraInfo();
      // 메인스트림에서만 PTZ정보 읽기.
      if( m_nIPCamStreamIdx == 0 )
         m_PTZInfo.Read( pIO );
      SetVideoSizeFromCameraInfo();
      ICameraInfo::Read( pIO );

#ifdef __SUPPORT_FISHEYE_CAMERA
      if( IS_SUPPORT( __SUPPORT_FISHEYE_DEWARPING ) ) {
         if( m_bUseStream ) m_FisheyeDewarpingSetup.ReadFile( pIO );
      }
#endif
      xmlNode.End();

      // 예외처리들
      if( m_nIPCamStreamIdx != 0 ) {
         m_bUsePTZ = FALSE;
      }
      if( m_nIPCameraType == IPCameraType_Innodep ) {
         if( m_PTZInfo.m_nConnectType == PTZCtrlConnType_Basic ) {
            if( m_PTZInfo.m_nPTZControlType != PTZControlType_Innodep ) {
               m_PTZInfo.m_nPTZControlType = PTZControlType_Innodep;
            }
         }
      }
      return TRUE;
   }

   return FALSE;
}

BOOL CIPCameraInfo::Write( const std::string& strNodeName, CXMLIO* pIO )
{
   static CIPCameraInfo dv; // Defalut Value
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( strNodeName.c_str() ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "IPCamStreamIdx", &m_nIPCamStreamIdx );
      xmlNode.Attribute( TYPE_ID( BOOL ), "UseStream", &m_bUseStream, &dv.m_bUseStream );
      xmlNode.Attribute( TYPE_ID( int ), "IPCamType", &m_nIPCameraType, &dv.m_nIPCameraType );
      xmlNode.Attribute( TYPE_ID( int ), "IPCamTypeEPTZC", &m_nIPCameraTypeForExtraPTZCtrl, &dv.m_nIPCameraTypeForExtraPTZCtrl );
      xmlNode.Attribute( TYPE_ID( int ), "PortNo", &m_nPortNo, &dv.m_nPortNo );
      xmlNode.Attribute( TYPE_ID( int ), "VF_Cap", &m_nVideoFormat_Cap, &dv.m_nVideoFormat_Cap );
      xmlNode.Attribute( TYPE_ID( int ), "VideoChNo", &m_nVideoChNo, &dv.m_nVideoChNo );
      xmlNode.Attribute( TYPE_ID( int ), "VideoStreamNo", &m_nVideoStreamNo, &dv.m_nVideoStreamNo );
      xmlNode.Attribute( TYPE_ID( int ), "ResolutionIdx", &m_nResolutionIdx, &dv.m_nResolutionIdx );
      xmlNode.Attribute( TYPE_ID( int ), "Compress", &m_nCompression, &dv.m_nCompression );
      xmlNode.Attribute( TYPE_ID( float ), "FPS_Cap", &m_fFPS_Cap, &dv.m_fFPS_Cap, "%4.1f" );
      xmlNode.Attribute( TYPE_ID( string ), "IPAddr", &m_strIPAddress, &dv.m_strIPAddress );
      xmlNode.Attribute( TYPE_ID( string ), "URL", &m_strURL, &dv.m_strURL );
      xmlNode.Attribute( TYPE_ID( int ), "PTZPortNo", &m_nPTZPortNo, &dv.m_nPTZPortNo );

#ifdef __SUPPORT_WIN32_CRYPT
      int nLength = 0;
      CCryption CryptoinObject;
      if( m_strUserName.length() ) {
         CryptoinObject.Encrypt( (LPCTSTR)m_strUserName.c_str(), nLength );
         xmlNode.Attribute( TYPE_ID( int ), "EUNL", &nLength ); // EUNL : Encoded User Name Length
         xmlNode.Attribute( TYPE_ID( XBYTE ), "EUN", CryptoinObject.m_szEncrypted, nLength ); // EUN  : Encoded User Name
      }
      if( m_strPassword.length() ) {
         nLength = 0;
         CryptoinObject.Encrypt( (LPCTSTR)m_strPassword.c_str(), nLength );
         xmlNode.Attribute( TYPE_ID( int ), "EPWL", &nLength ); // EPWL : Encoded User Name Length
         xmlNode.Attribute( TYPE_ID( XBYTE ), "EPW", CryptoinObject.m_szEncrypted, nLength ); // EPW  : Encoded Password
      }
#else
      if( m_strUserName.length() ) {
         xmlNode.Attribute( TYPE_ID( string ), "EUN", &m_strUserName );
      }
      if( m_strPassword.length() ) {
         xmlNode.Attribute( TYPE_ID( string ), "EPW", &m_strPassword );
      }
#endif

      xmlNode.Attribute( TYPE_ID( BOOL ), "IsUsePTZ", &m_bUsePTZ, &dv.m_bUsePTZ );
      xmlNode.Attribute( TYPE_ID( int ), "PTZCtrlType", &m_nPTZControlType, &dv.m_nPTZControlType );
      xmlNode.Attribute( TYPE_ID( string ), "DeviceName", &m_strDeviceName, &dv.m_strDeviceName );
      xmlNode.Attribute( TYPE_ID( BOOL ), "RecvMetadataOnly", &m_bReceiveMetaDataOnly, &dv.m_bReceiveMetaDataOnly );

      xmlNode.Attribute( TYPE_ID( string ), "DeviceUID", &m_strDeviceUID, &dv.m_strDeviceUID );
      xmlNode.Attribute( TYPE_ID( string ), "LogicalID", &m_strLogicalID, &dv.m_strLogicalID );
      xmlNode.Attribute( TYPE_ID( int ), "SocketType", &m_nSocketType, &dv.m_nSocketType );
      xmlNode.Attribute( TYPE_ID( int ), "UseAudioOut", &m_nUseAudioOut, &dv.m_nUseAudioOut );
      xmlNode.Attribute( TYPE_ID( string ), "DeviceGUID", &m_strDeviceGUID, &dv.m_strDeviceGUID );

      xmlNode.Attribute( TYPE_ID( int ), "StreamType", &m_nStreamType, &dv.m_nStreamType );

      xmlNode.Attribute( TYPE_ID( BOOL ), "NewHttpType", &m_bNewHttpRule, &dv.m_bNewHttpRule );
      // 메인스트림에서만 PTZ정보 저
      if( m_nIPCamStreamIdx == 0 )
         m_PTZInfo.Write( pIO );
      ICameraInfo::Write( pIO );
#ifdef __SUPPORT_FISHEYE_CAMERA
      if( IS_SUPPORT( __SUPPORT_FISHEYE_DEWARPING ) ) {
         if( m_bUseStream ) m_FisheyeDewarpingSetup.WriteFile( pIO );
      }
#endif
      xmlNode.End();
      return TRUE;
   }
   return FALSE;
}

BOOL CIPCameraInfo::SetInfo( ICameraInfo* pInfo )
{
   if( pInfo->m_nCameraType == CameraType_IPCamera ) {
      CIPCameraInfo* pIPCamInfo = (CIPCameraInfo*)pInfo;
      *this                     = *pIPCamInfo;
      return TRUE;
   } else
      return FALSE;
}

BOOL CIPCameraInfo::SetVideoSizeFromCameraInfo()
{
   IPCameraProperty* pProperty = GetIPCameraProperty( m_nIPCameraType );
   if( pProperty ) {
      ISize2D videoSize = pProperty->m_ResolutionListMgr.GetVideoRosolution( m_nVideoType, m_nResolutionIdx );
      if( videoSize.Width != 0 ) {
         m_nWidth  = videoSize.Width;
         m_nHeight = videoSize.Height;
         return TRUE;
      }
   }
   return FALSE;
}

///////////////////////////////////////////////////////////////////////////
//
// class: CWebCameraInfo
//
///////////////////////////////////////////////////////////////////////////

CWebCameraInfo::CWebCameraInfo()
{
   ICameraInfo::m_nCameraType = CameraType_WebCam;

   m_nIndex     = 0;
   m_fFrameRate = 15.0f;
   m_nWidth     = 1280;
   m_nHeight    = 720;
}

CWebCameraInfo::~CWebCameraInfo()
{
}

CWebCameraInfo& CWebCameraInfo::operator=( const CWebCameraInfo& from )
{
   ICameraInfo* pThis = (ICameraInfo*)this;
   ICameraInfo* pFrom = (ICameraInfo*)&from;
   *pThis             = *pFrom;
   return *this;
}

BOOL CWebCameraInfo::CheckChange( ICameraInfo* pICFrom )
{
   FileIO fioThis, fioFrom;
   CXMLIO xmlThis( &fioThis, XMLIO_Write );
   CXMLIO xmlFrom( &fioFrom, XMLIO_Write );
   Write( &xmlThis );
   pICFrom->Write( &xmlFrom );
   return fioThis.IsDiff( fioFrom );
}

BOOL CWebCameraInfo::CheckRestart( ICameraInfo* pICFrom )
{
   if( ICameraInfo::CheckRestart( pICFrom ) ) return TRUE;
   CWebCameraInfo* pFrom = (CWebCameraInfo*)pICFrom;
   if( m_fFrameRate != pFrom->m_fFrameRate ) return TRUE;
   if( m_nIndex != pFrom->m_nIndex ) return TRUE;
   if( m_nWidth != pFrom->m_nWidth ) return TRUE;
   if( m_nHeight != pFrom->m_nHeight ) return TRUE;
   return FALSE;
}

BOOL CWebCameraInfo::Read( CXMLIO* pIO )
{
   static CWebCameraInfo dv;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "WebCam" ) ) {
      xmlNode.Attribute( TYPE_ID( float ), "FrameRate", &m_fFrameRate, &dv.m_fFrameRate );
      xmlNode.Attribute( TYPE_ID( int ), "Index", &m_nIndex, &dv.m_nIndex );
      xmlNode.Attribute( TYPE_ID( int ), "Width", &m_nWidth, &dv.m_nWidth );
      xmlNode.Attribute( TYPE_ID( int ), "Height", &m_nHeight, &dv.m_nHeight );
      ICameraInfo::Read( pIO );
      xmlNode.End();
      return TRUE;
   }
   return FALSE;
}

BOOL CWebCameraInfo::Write( CXMLIO* pIO )
{
   static CWebCameraInfo dv;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "WebCam" ) ) {
      xmlNode.Attribute( TYPE_ID( float ), "FrameRate", &m_fFrameRate, &dv.m_fFrameRate );
      xmlNode.Attribute( TYPE_ID( int ), "Index", &m_nIndex, &dv.m_nIndex );
      xmlNode.Attribute( TYPE_ID( int ), "Width", &m_nWidth, &dv.m_nWidth );
      xmlNode.Attribute( TYPE_ID( int ), "Height", &m_nHeight, &dv.m_nHeight );
      ICameraInfo::Write( pIO );
      xmlNode.End();
      return TRUE;
   }
   return FALSE;
}

BOOL CWebCameraInfo::SetInfo( ICameraInfo* pInfo )
{
   if( pInfo->m_nCameraType == CameraType_WebCam ) {
      CWebCameraInfo* pFileCamInfo = (CWebCameraInfo*)pInfo;
      *this                        = *pFileCamInfo;
      return TRUE;
   } else
      return FALSE;
}

///////////////////////////////////////////////////////////////////////////
//
// class: CFileCameraInfo
//
///////////////////////////////////////////////////////////////////////////

CFileCameraInfo::CFileCameraInfo()
{
   ICameraInfo::m_nCameraType = CameraType_VideoFile;

   m_fFrameRate            = 15.0f;
   m_strPathName           = "";
   m_bSyncVCAFpsToVideoFps = FALSE;
}

CFileCameraInfo::~CFileCameraInfo()
{
}

CFileCameraInfo& CFileCameraInfo::operator=( const CFileCameraInfo& from )
{
   m_fFrameRate            = from.m_fFrameRate;
   m_strPathName           = from.m_strPathName;
   m_bSyncVCAFpsToVideoFps = from.m_bSyncVCAFpsToVideoFps;
   ICameraInfo* pThis      = (ICameraInfo*)this;
   ICameraInfo* pFrom      = (ICameraInfo*)&from;
   *pThis                  = *pFrom;
   return *this;
}

BOOL CFileCameraInfo::CheckChange( ICameraInfo* pICFrom )
{
   FileIO fioThis, fioFrom;
   CXMLIO xmlThis( &fioThis, XMLIO_Write );
   CXMLIO xmlFrom( &fioFrom, XMLIO_Write );
   Write( &xmlThis );
   pICFrom->Write( &xmlFrom );
   return fioThis.IsDiff( fioFrom );
}

BOOL CFileCameraInfo::CheckRestart( ICameraInfo* pICFrom )
{
   if( ICameraInfo::CheckRestart( pICFrom ) ) return TRUE;
   CFileCameraInfo* pFrom = (CFileCameraInfo*)pICFrom;
   if( m_strPathName != pFrom->m_strPathName ) return TRUE;
   if( m_bSyncVCAFpsToVideoFps != pFrom->m_bSyncVCAFpsToVideoFps ) return TRUE;
   return FALSE;
}

BOOL CFileCameraInfo::Read( CXMLIO* pIO )
{
   static CFileCameraInfo dv;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "FileCamInfo" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "PathName", &m_strPathName, &dv.m_strPathName );
      xmlNode.Attribute( TYPE_ID( float ), "FrameRate", &m_fFrameRate, &dv.m_fFrameRate );
      xmlNode.Attribute( TYPE_ID( BOOL ), "SyncToVideoFPS", &m_bSyncVCAFpsToVideoFps, &dv.m_bSyncVCAFpsToVideoFps );
      xmlNode.Attribute( TYPE_ID( BOOL ), "HWDecoder", &m_bHWDecoder, &dv.m_bHWDecoder );

      ICameraInfo::Read( pIO );
      xmlNode.End();
      return TRUE;
   }
   return FALSE;
}

BOOL CFileCameraInfo::Write( CXMLIO* pIO )
{
   static CFileCameraInfo dv;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "FileCamInfo" ) ) {
      xmlNode.Attribute( TYPE_ID( string ), "PathName", &m_strPathName, &dv.m_strPathName );
      xmlNode.Attribute( TYPE_ID( float ), "FrameRate", &m_fFrameRate, &dv.m_fFrameRate );
      xmlNode.Attribute( TYPE_ID( BOOL ), "SyncToVideoFPS", &m_bSyncVCAFpsToVideoFps, &dv.m_bSyncVCAFpsToVideoFps );
      xmlNode.Attribute( TYPE_ID( BOOL ), "HWDecoder", &m_bHWDecoder, &dv.m_bHWDecoder );
      ICameraInfo::Write( pIO );
      xmlNode.End();
      return TRUE;
   }
   return FALSE;
}

BOOL CFileCameraInfo::SetInfo( ICameraInfo* pInfo )
{
   if( pInfo->m_nCameraType == CameraType_VideoFile ) {
      CFileCameraInfo* pFileCamInfo = (CFileCameraInfo*)pInfo;
      *this                         = *pFileCamInfo;
      return TRUE;
   } else
      return FALSE;
}

///////////////////////////////////////////////////////////////////////////
//
// class: CVirtualPTZCameraInfo
//
///////////////////////////////////////////////////////////////////////////

CVirtualPTZCameraInfo::CVirtualPTZCameraInfo()
{
   ICameraInfo::m_nCameraType = CameraType_VirtualPTZ;
   ////// 설정 저장용 변수 ///////
   m_nVCID_SourceCamera                  = -1;
   m_nStreamIdx                          = 0;
   m_nWidth                              = 320;
   m_nHeight                             = 240;
   m_fFrameRate                          = 30.0f;
   m_bKeepAspectRatio                    = TRUE;
   m_fAspectRatio_X                      = 4.0f;
   m_fAspectRatio_Y                      = 3.0f;
   m_fMaxZoom                            = 4.0f;
   m_fSourceVideoReductionFactorOfZoom1x = 0.5f;
   ////// 일반 변수 ///////
}

CVirtualPTZCameraInfo::~CVirtualPTZCameraInfo()
{
}

CVirtualPTZCameraInfo& CVirtualPTZCameraInfo::operator=( const CVirtualPTZCameraInfo& from )
{
   m_nVCID_SourceCamera = from.m_nVCID_SourceCamera;
   m_nStreamIdx         = from.m_nStreamIdx;
   m_fFrameRate         = from.m_fFrameRate;
   ICameraInfo* pThis   = (ICameraInfo*)this;
   ICameraInfo* pFrom   = (ICameraInfo*)&from;
   *pThis               = *pFrom;
   return *this;
}

BOOL CVirtualPTZCameraInfo::CheckChange( ICameraInfo* pICFrom )
{
   FileIO fioThis, fioFrom;
   CXMLIO xmlThis( &fioThis, XMLIO_Write );
   CXMLIO xmlFrom( &fioFrom, XMLIO_Write );
   Write( &xmlThis );
   pICFrom->Write( &xmlFrom );
   return fioThis.IsDiff( fioFrom );
}

BOOL CVirtualPTZCameraInfo::CheckRestart( ICameraInfo* pICFrom )
{
   if( ICameraInfo::CheckRestart( pICFrom ) ) return TRUE;
   CVirtualPTZCameraInfo* pFrom = (CVirtualPTZCameraInfo*)pICFrom;
   if( m_nVCID_SourceCamera != pFrom->m_nVCID_SourceCamera ) return TRUE;
   if( m_nStreamIdx != pFrom->m_nStreamIdx ) return TRUE;
   if( m_bKeepAspectRatio != pFrom->m_bKeepAspectRatio ) return TRUE;
   if( m_fAspectRatio_X != pFrom->m_fAspectRatio_X ) return TRUE;
   if( m_fAspectRatio_Y != pFrom->m_fAspectRatio_Y ) return TRUE;
   if( m_fFrameRate != pFrom->m_fFrameRate ) return TRUE;
   if( m_fSourceVideoReductionFactorOfZoom1x != pFrom->m_fSourceVideoReductionFactorOfZoom1x ) return TRUE;
   return FALSE;
}

BOOL CVirtualPTZCameraInfo::Read( CXMLIO* pIO )
{
#ifdef __SUPPORT_VIRTUAL_PTZ_CAMERA
   CVirtualPTZCameraInfo dv;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "VirtualPTZCamInfo" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "VCID_SC", &m_nVCID_SourceCamera, &dv.m_nVCID_SourceCamera );
      xmlNode.Attribute( TYPE_ID( int ), "StrmIdx", &m_nStreamIdx, &dv.m_nStreamIdx );
      xmlNode.Attribute( TYPE_ID( BOOL ), "KeepAR", &m_bKeepAspectRatio, &dv.m_bKeepAspectRatio );
      xmlNode.Attribute( TYPE_ID( float ), "ARX", &m_fAspectRatio_Y, &dv.m_fAspectRatio_Y );
      xmlNode.Attribute( TYPE_ID( float ), "ARY", &m_fAspectRatio_Y, &dv.m_fAspectRatio_Y );
      xmlNode.Attribute( TYPE_ID( float ), "FR", &m_fFrameRate, &dv.m_fFrameRate );
      xmlNode.Attribute( TYPE_ID( float ), "MaxZoom", &m_fMaxZoom, &dv.m_fMaxZoom );
      xmlNode.Attribute( TYPE_ID( float ), "SVRFZ1x", &m_fSourceVideoReductionFactorOfZoom1x, &dv.m_fSourceVideoReductionFactorOfZoom1x );
      ICameraInfo::Read( pIO );
      xmlNode.End();
   }
#endif
   return TRUE;
}

BOOL CVirtualPTZCameraInfo::Write( CXMLIO* pIO )
{
   CVirtualPTZCameraInfo dv;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "VirtualPTZCamInfo" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "VCID_SC", &m_nVCID_SourceCamera, &dv.m_nVCID_SourceCamera );
      xmlNode.Attribute( TYPE_ID( int ), "StrmIdx", &m_nStreamIdx, &dv.m_nStreamIdx );
      xmlNode.Attribute( TYPE_ID( BOOL ), "KeepAR", &m_bKeepAspectRatio, &dv.m_bKeepAspectRatio );
      xmlNode.Attribute( TYPE_ID( float ), "ARX", &m_fAspectRatio_Y, &dv.m_fAspectRatio_Y );
      xmlNode.Attribute( TYPE_ID( float ), "ARY", &m_fAspectRatio_Y, &dv.m_fAspectRatio_Y );
      xmlNode.Attribute( TYPE_ID( float ), "FR", &m_fFrameRate, &dv.m_fFrameRate );
      xmlNode.Attribute( TYPE_ID( float ), "MaxZoom", &m_fMaxZoom, &dv.m_fMaxZoom );
      xmlNode.Attribute( TYPE_ID( float ), "SVRFZ1x", &m_fSourceVideoReductionFactorOfZoom1x, &dv.m_fSourceVideoReductionFactorOfZoom1x );
      ICameraInfo::Write( pIO );
      xmlNode.End();
   }
   return TRUE;
}

BOOL CVirtualPTZCameraInfo::SetInfo( ICameraInfo* pInfo )
{
   if( pInfo->m_nCameraType == CameraType_VirtualPTZ ) {
      CVirtualPTZCameraInfo* pVirtualPTZCamInfo = (CVirtualPTZCameraInfo*)pInfo;
      *this                                     = *pVirtualPTZCamInfo;
      return TRUE;
   } else
      return FALSE;
}

///////////////////////////////////////////////////////////////////////////
//
// class: CPanoramaCameraInfo
//
///////////////////////////////////////////////////////////////////////////

CPanoramaCameraInfo::CPanoramaCameraInfo()
{
   ICameraInfo::m_nCameraType = CameraType_Panorama;
   m_nInputCamNum             = 0;
   m_nInputCamInfoNum         = 0;
   m_bUseBC                   = TRUE;
   m_nBCBaseChNo              = 0;
   m_fFrameRate               = 15.0f;
   m_strPVTPath               = "";
   m_strVCTPath               = "";
}

CPanoramaCameraInfo::~CPanoramaCameraInfo()
{
   int i;
   for( i = 0; i < (int)m_InputCamInfos.size(); i++ )
      delete m_InputCamInfos[i];
   m_InputCamInfos.clear();
}

CPanoramaCameraInfo& CPanoramaCameraInfo::operator=( const CPanoramaCameraInfo& from )
{
   int i;
   for( i = 0; i < m_nInputCamInfoNum; i++ )
      delete m_InputCamInfos[i];
   m_InputCamInfos.clear();

   m_nInputCamNum     = from.m_nInputCamNum;
   m_nInputCamInfoNum = from.m_nInputCamInfoNum;
   m_fFrameRate       = from.m_fFrameRate;
   m_InputImgSize     = from.m_InputImgSize;
   m_strPVTPath       = from.m_strPVTPath;
   m_strVCTPath       = from.m_strVCTPath;

   for( i = 0; i < from.m_nInputCamInfoNum; i++ ) {
      if( from.m_InputCamInfos[i]->m_nCameraType == CameraType_IPCamera ) {
         CIPCameraInfo* pIPCameraInfo = new CIPCameraInfo;
         *pIPCameraInfo               = *( (CIPCameraInfo*)from.m_InputCamInfos[i] );
         m_InputCamInfos.push_back( pIPCameraInfo );
      }
      if( from.m_InputCamInfos[i]->m_nCameraType == CameraType_AnalogCamera ) {
         CAnalogCameraInfo* pAnalogCameraInfo = new CAnalogCameraInfo;
         *pAnalogCameraInfo                   = *( (CAnalogCameraInfo*)from.m_InputCamInfos[i] );
         m_InputCamInfos.push_back( pAnalogCameraInfo );
      }
   }
   int nCamInfoNum    = m_InputCamInfos.size();
   ICameraInfo* pThis = (ICameraInfo*)this;
   ICameraInfo* pFrom = (ICameraInfo*)&from;
   *pThis             = *pFrom;
   return *this;
}

BOOL CPanoramaCameraInfo::Read( CXMLIO* pIO )
{
#ifdef __SUPPORT_PANORAMIC_CAMERA
   if( IS_SUPPORT( __SUPPORT_PANORAMIC_CAMERA ) ) {
      static CPanoramaCameraInfo dv; // default value;
      int i;
      CXMLNode xmlNode( pIO );
      if( xmlNode.Start( "PanoramaCamInfo" ) ) {
         xmlNode.Attribute( TYPE_ID( int ), "InputCamInfoNum", &m_nInputCamInfoNum, &dv.m_nInputCamInfoNum );
         xmlNode.Attribute( TYPE_ID( float ), "FrameRate", &m_fFrameRate, &dv.m_fFrameRate );
         xmlNode.Attribute( TYPE_ID( string ), "PVTPath", &m_strPVTPath, &dv.m_strPVTPath );
         xmlNode.Attribute( TYPE_ID( string ), "VCTPath", &m_strVCTPath, &dv.m_strVCTPath );
         xmlNode.Attribute( TYPE_ID( BOOL ), "UseBC", &m_bUseBC, &dv.m_bUseBC );
         xmlNode.Attribute( TYPE_ID( int ), "BCBaseChNo", &m_nBCBaseChNo, &dv.m_nBCBaseChNo );

         std::vector<ICameraInfo*> tmpInputCamInfos;

         for( i = 0; i < m_nInputCamInfoNum; i++ ) {
            ICameraInfo* pICameraInfo = NULL;
            if( int( m_InputCamInfos.size() ) > 0 && i < int( m_InputCamInfos.size() ) )
               pICameraInfo = m_InputCamInfos[i];

            CXMLNode xmlNode( pIO );
            std::string strChildName = xmlNode.PeekUnLoadChildName();
            if( strChildName == "AnalogCam" ) {
               if( xmlNode.Start( "AnalogCam" ) ) {
                  CAnalogCameraInfo* pAnalogCamInfo = NULL;
                  if( pICameraInfo && ( CameraType_AnalogCamera == pICameraInfo->m_nCameraType ) )
                     pAnalogCamInfo = (CAnalogCameraInfo*)pICameraInfo;
                  else
                     pAnalogCamInfo = new CAnalogCameraInfo;

                  xmlNode.Attribute( TYPE_ID( int ), "ChannelNo", &pAnalogCamInfo->m_nChannelNo );
                  xmlNode.Attribute( TYPE_ID( int ), "CaptureBoardType", &pAnalogCamInfo->m_nCaptureBoardType );
                  pAnalogCamInfo->m_bPanorama = TRUE;

                  tmpInputCamInfos.push_back( pAnalogCamInfo );
                  xmlNode.End();
               }
            } else if( strChildName == NodeName_IPCameraInfo ) {
               BOOL bAddToInputCamInfos  = FALSE;
               CIPCameraInfo* pIPCamInfo = NULL;
               if( pICameraInfo && ( CameraType_IPCamera == pICameraInfo->m_nCameraType ) )
                  pIPCamInfo = (CIPCameraInfo*)pICameraInfo;
               else
                  pIPCamInfo = new CIPCameraInfo;

               pIPCamInfo->Read( NodeName_IPCameraInfo, pIO );
               pIPCamInfo->m_bPanorama = TRUE;

               tmpInputCamInfos.push_back( pIPCamInfo );
            }
         }

         if( m_InputCamInfos.size() > tmpInputCamInfos.size() ) {
            for( i = int( tmpInputCamInfos.size() ); i < int( m_InputCamInfos.size() ); i++ ) {
               delete m_InputCamInfos[i];
            }
         }
         m_InputCamInfos = tmpInputCamInfos;

         xmlNode.Attribute( TYPE_ID( int ), "InImgWidth", &m_InputImgSize.Width, &dv.m_InputImgSize.Width );
         xmlNode.Attribute( TYPE_ID( int ), "InImgHeight", &m_InputImgSize.Height, &dv.m_InputImgSize.Height );
         xmlNode.Attribute( TYPE_ID( int ), "InCamNum", &m_nInputCamNum, &dv.m_nInputCamNum );

         ICameraInfo::Read( pIO );
         xmlNode.End();
         return TRUE;
      }
      return FALSE;
   }
#endif
   return TRUE;
}

BOOL CPanoramaCameraInfo::Write( CXMLIO* pIO )
{
#ifdef __SUPPORT_PANORAMIC_CAMERA
   if( IS_SUPPORT( __SUPPORT_PANORAMIC_CAMERA ) ) {
      static CPanoramaCameraInfo dv; // default value;
      int i;
      CXMLNode xmlNode( pIO );
      if( xmlNode.Start( "PanoramaCamInfo" ) ) {
         xmlNode.Attribute( TYPE_ID( int ), "InputCamInfoNum", &m_nInputCamInfoNum, &dv.m_nInputCamInfoNum );
         xmlNode.Attribute( TYPE_ID( float ), "FrameRate", &m_fFrameRate, &dv.m_fFrameRate );
         xmlNode.Attribute( TYPE_ID( string ), "PVTPath", &m_strPVTPath, &dv.m_strPVTPath );
         xmlNode.Attribute( TYPE_ID( string ), "VCTPath", &m_strVCTPath, &dv.m_strVCTPath );
         xmlNode.Attribute( TYPE_ID( BOOL ), "UseBC", &m_bUseBC, &dv.m_bUseBC );
         xmlNode.Attribute( TYPE_ID( int ), "BCBaseChNo", &m_nBCBaseChNo, &dv.m_nBCBaseChNo );
         for( i = 0; i < m_nInputCamInfoNum; i++ ) {
            ICameraInfo* pICamInfo = m_InputCamInfos[i];
            if( CameraType_AnalogCamera == pICamInfo->m_nCameraType ) {
               CAnalogCameraInfo* pAnalogCamInfo = (CAnalogCameraInfo*)pICamInfo;
               CXMLNode xmlNode( pIO );
               if( xmlNode.Start( "AnalogCam" ) ) {
                  xmlNode.Attribute( TYPE_ID( int ), "ChannelNo", &pAnalogCamInfo->m_nChannelNo );
                  xmlNode.Attribute( TYPE_ID( int ), "CaptureBoardType", &pAnalogCamInfo->m_nCaptureBoardType );
                  xmlNode.End();
               }
            }
            if( CameraType_IPCamera == pICamInfo->m_nCameraType ) {
               ( (CIPCameraInfo*)pICamInfo )->Write( NodeName_IPCameraInfo, pIO );
            }
         }
         xmlNode.Attribute( TYPE_ID( int ), "InImgWidth", &m_InputImgSize.Width, &dv.m_InputImgSize.Width );
         xmlNode.Attribute( TYPE_ID( int ), "InImgHeight", &m_InputImgSize.Height, &dv.m_InputImgSize.Height );
         xmlNode.Attribute( TYPE_ID( int ), "InCamNum", &m_nInputCamNum, &dv.m_nInputCamNum );

         ICameraInfo::Write( pIO );
         xmlNode.End();
         return TRUE;
      }
      return FALSE;
   }
#endif
   return TRUE;
}

void CPanoramaCameraInfo::AddCamera( ICameraInfo* pCamInfo )
{
   pCamInfo->m_bPanorama = TRUE;
   m_InputCamInfos.push_back( pCamInfo );
   m_nInputCamInfoNum++;
}

void CPanoramaCameraInfo::DeleteCamera( ICameraInfo* pDeleteCamInfo )
{
   std::vector<ICameraInfo*>::iterator iter = m_InputCamInfos.begin();
   std::vector<ICameraInfo*>::iterator end  = m_InputCamInfos.end();

   while( iter != end ) {
      ICameraInfo* pCamInfo = *iter;
      if( pCamInfo == pDeleteCamInfo ) {
         delete pCamInfo;
         m_InputCamInfos.erase( iter );
         break;
      }
      iter++;
   }
   m_nInputCamInfoNum--;
}

// 파노라마 카메라 설정창에서 입력카메라 리스트 순서대로 정렬할 때 싸용함.
void CPanoramaCameraInfo::RemoveAllCameras()
{
   m_InputCamInfos.clear();
   m_nInputCamInfoNum = 0;
}

int CPanoramaCameraInfo::SetVideoSizeFromCameraInfo()
{
   int i;
   if( m_nInputCamInfoNum != m_InputCamInfos.size() )
      return ( DONE );
   for( i = 0; i < m_nInputCamInfoNum; i++ ) {
      ICameraInfo* pCamInfo = m_InputCamInfos[i];
      pCamInfo->SetVideoSizeFromCameraInfo();
   }
   return DONE;
}

BOOL CPanoramaCameraInfo::GetAnalogCameraChIdxs( std::vector<int>& chIdxList )
{
   int i;
   chIdxList.clear();
   for( i = 0; i < m_nInputCamInfoNum; i++ ) {
      ICameraInfo* pCamInfo = m_InputCamInfos[i];
      if( pCamInfo->m_nCameraType == CameraType_AnalogCamera ) {
         CAnalogCameraInfo* pAnalogCamInfo = (CAnalogCameraInfo*)pCamInfo;
         chIdxList.push_back( pAnalogCamInfo->GetCBChannelNo() );
      }
   }
   return TRUE;
}

UINT64 CPanoramaCameraInfo::GetAnalogCameraChFlag()
{
   int i;
   UINT64 nAnalogCameraChFlag = 0;
   for( i = 0; i < m_nInputCamInfoNum; i++ ) {
      ICameraInfo* pCamInfo = m_InputCamInfos[i];
      if( pCamInfo->m_nCameraType == CameraType_AnalogCamera ) {
         CAnalogCameraInfo* pAnalogCamInfo = (CAnalogCameraInfo*)pCamInfo;
         UINT64 nCurChFlag                 = UINT64( 1 ) << pAnalogCamInfo->GetCBChannelNo();
         nAnalogCameraChFlag |= nCurChFlag;
      }
   }
   return nAnalogCameraChFlag;
}

int CPanoramaCameraInfo::CheckParams()
{
   if( m_nInputCamInfoNum < m_nInputCamNum )
      return ( PanoramaCamErr_InputCameraNumber );
   if( m_strPVTPath.length() == 0 ) {
      return ( PanoramaCamErr_EmptyPVTPath );
   } else {
      if( !IsExistFile( m_strPVTPath ) ) {
         return ( PanoramaCamErr_NotExistPVTFile );
      } else {
         PVTInfo pvt_info;
         if( pvt_info.Read( m_strPVTPath.c_str() ) != DONE ) {
            return ( PanoramaCamErr_PVTFileReadErr );
         }
      }
   }
   return ( DONE );
}

BOOL CPanoramaCameraInfo::CheckChange( ICameraInfo* pICFrom )
{
   FileIO fioThis, fioFrom;
   CXMLIO xmlThis( &fioThis, XMLIO_Write );
   CXMLIO xmlFrom( &fioFrom, XMLIO_Write );
   Write( &xmlThis );
   pICFrom->Write( &xmlFrom );
   return fioThis.IsDiff( fioFrom );
}

BOOL CPanoramaCameraInfo::CheckRestart( ICameraInfo* pICFrom )
{
   int i;
   if( ICameraInfo::CheckRestart( pICFrom ) ) return TRUE;

   CPanoramaCameraInfo* pFrom = (CPanoramaCameraInfo*)pICFrom;
   if( m_nInputCamInfoNum != pFrom->m_nInputCamInfoNum ) return TRUE;
   if( m_InputImgSize.Width != pFrom->m_InputImgSize.Width ) return TRUE;
   if( m_InputImgSize.Height != pFrom->m_InputImgSize.Height ) return TRUE;
   if( m_strPVTPath != pFrom->m_strPVTPath ) return TRUE;
   if( m_strVCTPath != pFrom->m_strVCTPath ) return TRUE;
   for( i = 0; i < pFrom->m_nInputCamInfoNum; i++ ) {
      if( m_InputCamInfos[i]->CheckRestart( pFrom->m_InputCamInfos[i] ) ) return TRUE;
   }
   return FALSE;
}

BOOL CPanoramaCameraInfo::SetInfo( ICameraInfo* pInfo )
{
   if( pInfo->m_nCameraType == CameraType_Panorama ) {
      CPanoramaCameraInfo* pPanoramaCamInfo = (CPanoramaCameraInfo*)pInfo;
      *this                                 = *pPanoramaCamInfo;
      return TRUE;
   } else
      return FALSE;
}

///////////////////////////////////////////////////////////////////////////
//
// class: CVideoChannelInfo
//
///////////////////////////////////////////////////////////////////////////

CVideoChannelInfo::CVideoChannelInfo()
    : m_IPCamInfo( m_IPCamInfoArr[0] )
{
   ////// 일반 변수 ///////

   m_nWidth  = 0;
   m_nHeight = 0;

   m_bRestart = FALSE;
   m_bRemote  = FALSE;
   m_pParam   = NULL;

   ////// 설정 저장용 변수 ///////
   int i;
   for( i = 0; i < MAX_IP_CAMERA_STREAM_NUM; i++ ) {
      m_IPCamInfoArr[i].m_nIPCamStreamIdx = i;
      m_IPCamInfoArr[i].m_bUseStream      = FALSE;
      m_IPCamInfoArr[i].m_nVideoStreamNo  = i;
   }
   m_IPCamInfoArr[0].m_bUseStream = TRUE;

   m_nVCID             = -1;
   m_nVideoChannelType = 0;
#ifdef __SUPPORT_IMAGE_ENHANCEMENT
   m_bUseVideoStabilization      = FALSE;
   m_nVideoStabilizationFrameNum = 5;
#endif

#ifndef __DISTRIBUTION
//m_nVideoChannelType = VideoChannelType_VideoFile;
//m_FileCamInfo.m_strPathName = "C:\\물체 버려짐.avi";
#endif
   // 프레임 비율.
   m_fFPS_Cap  = 15.0f;
   m_fFPS_Disp = 15.0f;
   m_fFPS_IP   = 15.0f;
   m_fFPS_Rec  = 15.0f;

   // PTZ 카메라 정보.
   m_bPTZCamera   = FALSE;
   m_nCBChannelNo = 0;

   m_pfnSetImageProperty = NULL;
   m_pfnGetImageProperty = NULL;

   m_nChannelNo_IVX = -1;

   m_bFisheyeCam     = FALSE;
   m_bSyncDispWithIP = FALSE;
   m_pParam          = NULL;
}

CVideoChannelInfo::~CVideoChannelInfo()
{
}

CVideoChannelInfo& CVideoChannelInfo::operator=( const CVideoChannelInfo& from )
{
   ////// 일반 변수 ///////
   m_nWidth         = from.m_nWidth;
   m_nHeight        = from.m_nHeight;
   m_bRestart       = from.m_bRestart;
   m_bRemote        = from.m_bRemote;
   m_pParam         = from.m_pParam;
   m_nCameraFlag    = from.m_nCameraFlag;
   m_nChannelNo_IVX = from.m_nChannelNo_IVX;
   ////// 값 저장용 변수 ///////
   m_nVCID             = from.m_nVCID;
   m_nVideoChannelType = from.m_nVideoChannelType;
#ifdef __SUPPORT_IMAGE_ENHANCEMENT
   *( (CVideoStabilizationSetup*)this ) = *( (CVideoStabilizationSetup*)&from );
   *( (CVideoDefoggingSetup*)this )     = *( (CVideoDefoggingSetup*)&from );
#endif

   // 프레임 비율
   m_fFPS_Cap  = from.m_fFPS_Cap;
   m_fFPS_Disp = from.m_fFPS_Disp;
   m_fFPS_IP   = from.m_fFPS_IP;
   m_fFPS_Rec  = from.m_fFPS_Rec;
   // PTZ 카메라 정보.
   m_bPTZCamera = from.m_bPTZCamera;
   m_PTZInfo    = from.m_PTZInfo;
   // 카메라 정보 (아날로그, IP, 파일, 파노라마, 가상PTZ)
   m_nCBChannelNo = from.m_nCBChannelNo;

   m_bFisheyeCam     = from.m_bFisheyeCam;
   m_bSyncDispWithIP = from.m_bSyncDispWithIP;

   int i;
   for( i = 0; i < MAX_IP_CAMERA_STREAM_NUM; i++ )
      m_IPCamInfoArr[i] = from.m_IPCamInfoArr[i];
   m_FileCamInfo       = from.m_FileCamInfo;
   m_PanoramaCamInfo   = from.m_PanoramaCamInfo;
   m_VirtualPTZCamInfo = from.m_VirtualPTZCamInfo;
   m_WebCamInfo        = from.m_WebCamInfo;

   /////// 콜백함수 ///////
   m_pfnSetImageProperty = from.m_pfnSetImageProperty;
   m_pfnGetImageProperty = from.m_pfnGetImageProperty;
   return *this;
}

BOOL CVideoChannelInfo::Read( CXMLIO* pIO, BOOL bNetwork )
{
   static CVideoChannelInfo dv; // jun : default value
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "VideoChInfo" ) ) {
      if( !bNetwork && pIO->GetStorageType() == FIO_STORAGE_MEMORY )
         xmlNode.Attribute( TYPE_ID( int ), "IVXChNo", &m_nChannelNo_IVX );
      xmlNode.Attribute( TYPE_ID( int ), "VCID", &m_nVCID );
      xmlNode.Attribute( TYPE_ID( int ), "Type", &m_nVideoChannelType );
      xmlNode.Attribute( TYPE_ID( float ), "FPS_Cap", &m_fFPS_Cap, &dv.m_fFPS_Cap );
      xmlNode.Attribute( TYPE_ID( float ), "FPS_Disp", &m_fFPS_Disp, &dv.m_fFPS_Disp );
      xmlNode.Attribute( TYPE_ID( float ), "FPS_IP", &m_fFPS_IP, &dv.m_fFPS_IP );
      xmlNode.Attribute( TYPE_ID( float ), "FPS_Rec", &m_fFPS_Rec, &dv.m_fFPS_Rec );
      xmlNode.Attribute( TYPE_ID( BOOL ), "IsPTZCam", &m_bPTZCamera );
      if( IS_SUPPORT( __SUPPORT_FISHEYE_DEWARPING ) )
         xmlNode.Attribute( TYPE_ID( BOOL ), "IsFisheyeCam", &m_bFisheyeCam, &dv.m_bFisheyeCam );
      xmlNode.Attribute( TYPE_ID( BOOL ), "SyncDispWithIP", &m_bSyncDispWithIP, &dv.m_bSyncDispWithIP );
      xmlNode.Attribute( TYPE_ID( int ), "CBChNo", &m_nCBChannelNo, &dv.m_nCBChannelNo );

#ifdef __SUPPORT_PTZ_AREA
      m_csPTZAreas.lock();
      m_PTZAreas[PTZAreaType_PrivacyZone].ReadFile( pIO, "PrivacyArea" );
      m_PTZAreas[PTZAreaType_TrackingArea].ReadFile( pIO, "TrackingArea" );
      m_PTZAreas[PTZAreaType_NonTrackingArea].ReadFile( pIO, "NonTrackingArea" );
      m_PTZAreas[PTZAreaType_DynamicBackgroundArea].ReadFile( pIO, "DynamicBackgroundArea" );
      m_PTZAreas[PTZAreaType_MutualAsistanceArea].ReadFile( pIO, "MutualAssistanceArea" );
      m_PTZAreas[PTZAreaType_FloodLightArea].ReadFile( pIO, "IRLight" );
      m_csPTZAreas.unlock();
#endif

#ifdef __SUPPORT_AUGMENTED_REALITY
      if( IS_SUPPORT( __SUPPORT_AUGMENTED_REALITY ) ) {
         m_ARFeatureList.ReadFile( pIO, "ARFeatureInfoList" );
      }
#endif

      // 카메라 정보. (아날로그, IP, 파일, 파노라마)
      m_PTZInfo.Read( pIO );
      int i;
      for( i = 0; i < MAX_IP_CAMERA_STREAM_NUM; i++ )
         m_IPCamInfoArr[i].Read( NodeName_IPCameraInfo, pIO );
      m_FileCamInfo.Read( pIO );
      m_PanoramaCamInfo.Read( pIO );
      m_VirtualPTZCamInfo.Read( pIO );
#ifdef __SUPPORT_IMAGE_ENHANCEMENT
      ( (CVideoStabilizationSetup*)this )->ReadFile( pIO );
      ( (CVideoDefoggingSetup*)this )->ReadFile( pIO );
#endif

      xmlNode.End();

      CheckParameters();
      return TRUE;
   }
   return FALSE;
}

BOOL CVideoChannelInfo::Write( CXMLIO* pIO, BOOL bNetwork )
{
   CVideoChannelInfo dv; // jun : default value
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "VideoChInfo" ) ) {
      if( !bNetwork && pIO->GetStorageType() == FIO_STORAGE_MEMORY )
         xmlNode.Attribute( TYPE_ID( int ), "IVXChNo", &m_nChannelNo_IVX );
      xmlNode.Attribute( TYPE_ID( int ), "VCID", &m_nVCID );
      xmlNode.Attribute( TYPE_ID( int ), "Type", &m_nVideoChannelType );
      xmlNode.Attribute( TYPE_ID( float ), "FPS_Cap", &m_fFPS_Cap, &dv.m_fFPS_Cap );
      xmlNode.Attribute( TYPE_ID( float ), "FPS_Disp", &m_fFPS_Disp, &dv.m_fFPS_Disp );
      xmlNode.Attribute( TYPE_ID( float ), "FPS_IP", &m_fFPS_IP, &dv.m_fFPS_IP );
      xmlNode.Attribute( TYPE_ID( float ), "FPS_Rec", &m_fFPS_Rec, &dv.m_fFPS_Rec );
      xmlNode.Attribute( TYPE_ID( BOOL ), "IsPTZCam", &m_bPTZCamera );
      if( IS_SUPPORT( __SUPPORT_FISHEYE_DEWARPING ) )
         xmlNode.Attribute( TYPE_ID( BOOL ), "IsFisheyeCam", &m_bFisheyeCam, &dv.m_bFisheyeCam );
      xmlNode.Attribute( TYPE_ID( BOOL ), "SyncDispWithIP", &m_bSyncDispWithIP, &dv.m_bSyncDispWithIP );
      xmlNode.Attribute( TYPE_ID( int ), "CBChNo", &m_nCBChannelNo, &dv.m_nCBChannelNo );
#ifdef __SUPPORT_PTZ_AREA
      m_csPTZAreas.lock();
      m_PTZAreas[PTZAreaType_PrivacyZone].WriteFile( pIO, "PrivacyArea" );
      m_PTZAreas[PTZAreaType_TrackingArea].WriteFile( pIO, "TrackingArea" );
      m_PTZAreas[PTZAreaType_NonTrackingArea].WriteFile( pIO, "NonTrackingArea" );
      m_PTZAreas[PTZAreaType_DynamicBackgroundArea].WriteFile( pIO, "DynamicBackgroundArea" );
      m_PTZAreas[PTZAreaType_MutualAsistanceArea].WriteFile( pIO, "MutualAssistanceArea" );
      m_PTZAreas[PTZAreaType_FloodLightArea].WriteFile( pIO, "IRLight" );
      m_csPTZAreas.unlock();
#endif
#ifdef __SUPPORT_AUGMENTED_REALITY
      if( IS_SUPPORT( __SUPPORT_AUGMENTED_REALITY ) ) {
         m_ARFeatureList.WriteFile( pIO, "ARFeatureInfoList" );
      }
#endif
      // 카메라 정보. (아날로그, IP, 파일, 파노라마)
      m_PTZInfo.Write( pIO );
      int i;
      for( i = 0; i < MAX_IP_CAMERA_STREAM_NUM; i++ )
         m_IPCamInfoArr[i].Write( NodeName_IPCameraInfo, pIO );
      m_FileCamInfo.Write( pIO );
      m_PanoramaCamInfo.Write( pIO );
      m_VirtualPTZCamInfo.Write( pIO );
#ifdef __SUPPORT_IMAGE_ENHANCEMENT
      ( (CVideoStabilizationSetup*)this )->WriteFile( pIO );
      ( (CVideoDefoggingSetup*)this )->WriteFile( pIO );
#endif

      xmlNode.End();
      return TRUE;
   }
   return FALSE;
}

BOOL CVideoChannelInfo::CheckChange( CVideoChannelInfo& from )
{
   if( m_nVCID != from.m_nVCID ) return TRUE;
   if( m_nVideoChannelType != from.m_nVideoChannelType ) return TRUE;
#ifdef __SUPPORT_IMAGE_ENHANCEMENT
   if( *( (CVideoStabilizationSetup*)this ) != *( (CVideoStabilizationSetup*)&from ) ) return TRUE;
   if( *( (CVideoDefoggingSetup*)this ) != *( (CVideoDefoggingSetup*)&from ) ) return TRUE;
#endif

   if( m_fFPS_Cap != from.m_fFPS_Cap ) return TRUE;
   if( m_fFPS_Disp != from.m_fFPS_Disp ) return TRUE;
   if( m_fFPS_IP != from.m_fFPS_IP ) return TRUE;
   if( m_fFPS_Rec != from.m_fFPS_Rec ) return TRUE;
   if( m_bPTZCamera != from.m_bPTZCamera ) return TRUE;
   if( m_PTZInfo.CheckChange( &from.m_PTZInfo ) ) return TRUE;
   if( m_nCBChannelNo != from.m_nCBChannelNo ) return TRUE;
   int i;
   for( i = 0; i < MAX_IP_CAMERA_STREAM_NUM; i++ )
      if( m_IPCamInfoArr[i].CheckChange( &from.m_IPCamInfoArr[i] ) ) return TRUE;
   if( m_FileCamInfo.CheckChange( &from.m_FileCamInfo ) ) return TRUE;
   if( m_WebCamInfo.CheckChange( &from.m_WebCamInfo ) ) return TRUE;
   if( m_PanoramaCamInfo.CheckChange( &from.m_PanoramaCamInfo ) ) return TRUE;
   if( m_VirtualPTZCamInfo.CheckChange( &from.m_VirtualPTZCamInfo ) ) return TRUE;
   if( IS_SUPPORT( __SUPPORT_FISHEYE_DEWARPING ) )
      if( m_bFisheyeCam != from.m_bFisheyeCam ) return TRUE;
   FileIO fioThis, fioFrom;
   CXMLIO xmlThis( &fioThis, XMLIO_Write );
   CXMLIO xmlFrom( &fioFrom, XMLIO_Write );
#ifdef __SUPPORT_PTZ_AREA
   m_csPTZAreas.lock();
   m_PTZAreas[PTZAreaType_PrivacyZone].WriteFile( &xmlThis, "P" );
   m_PTZAreas[PTZAreaType_TrackingArea].WriteFile( &xmlThis, "T" );
   m_PTZAreas[PTZAreaType_NonTrackingArea].WriteFile( &xmlThis, "N" );
   m_PTZAreas[PTZAreaType_DynamicBackgroundArea].WriteFile( &xmlThis, "D" );
   m_PTZAreas[PTZAreaType_MutualAsistanceArea].WriteFile( &xmlThis, "M" );
   m_PTZAreas[PTZAreaType_FloodLightArea].WriteFile( &xmlThis, "I" );
   m_csPTZAreas.unlock();
   from.m_PTZAreas[PTZAreaType_PrivacyZone].WriteFile( &xmlFrom, "P" );
   from.m_PTZAreas[PTZAreaType_TrackingArea].WriteFile( &xmlFrom, "T" );
   from.m_PTZAreas[PTZAreaType_NonTrackingArea].WriteFile( &xmlFrom, "N" );
   from.m_PTZAreas[PTZAreaType_DynamicBackgroundArea].WriteFile( &xmlFrom, "D" );
   from.m_PTZAreas[PTZAreaType_MutualAsistanceArea].WriteFile( &xmlFrom, "M" );
   from.m_PTZAreas[PTZAreaType_FloodLightArea].WriteFile( &xmlFrom, "I" );
#endif
   if( fioThis.IsDiff( fioFrom ) ) return TRUE;
   return FALSE;
}

BOOL CVideoChannelInfo::CheckRestart( CVideoChannelInfo& from )
{
   if( m_nVideoChannelType != from.m_nVideoChannelType ) return TRUE;
   if( m_bPTZCamera != from.m_bPTZCamera ) return TRUE;
   if( IS_SUPPORT( __SUPPORT_FISHEYE_DEWARPING ) )
      if( m_bFisheyeCam != from.m_bFisheyeCam ) return TRUE;
   if( m_nVideoChannelType == VideoChannelType_AnalogCamera ) {
      if( m_nCBChannelNo != from.m_nCBChannelNo ) return TRUE;
      if( m_PTZInfo.CheckRestart( &from.m_PTZInfo ) ) return TRUE;
   } else if( m_nVideoChannelType == VideoChannelType_IPCamera ) {
      int i;
      for( i = 0; i < MAX_IP_CAMERA_STREAM_NUM; i++ ) {
         if( m_IPCamInfoArr[i].CheckRestart( &from.m_IPCamInfoArr[i] ) ) return TRUE;
      }
      // IP카메라의 IR Light 정보는 m_IPCamInfo.m_PTZInfo.m_IRLightCtrlInfo 변수를
      //                            this->m_PTZInfo.m_IRLightCtrlInfo 변수로 복사하여 사용하기 때문에 다음의 변수를 체크해야 한다.
      if( m_PTZInfo.CheckRestart( &from.m_PTZInfo ) ) return TRUE;
   } else if( m_nVideoChannelType == VideoChannelType_VideoFile ) {
      if( m_FileCamInfo.CheckRestart( &from.m_FileCamInfo ) ) return TRUE;
   } else if( m_nVideoChannelType == VideoChannelType_Panorama ) {
      if( m_PanoramaCamInfo.CheckRestart( &from.m_PanoramaCamInfo ) ) return TRUE;
   }
#ifdef __SUPPORT_VIRTUAL_PTZ_CAMERA
   else if( m_nVideoChannelType == VideoChannelType_VirtualPTZ ) {
      if( m_VirtualPTZCamInfo.CheckRestart( &from.m_VirtualPTZCamInfo ) ) return TRUE;
   }
#endif
   return FALSE;
}

// to determine PTZ or not.
int CVideoChannelInfo::CheckParameters()
{
   int i;

   m_nCameraFlag = 0;
   if( m_nVideoChannelType == VideoChannelType_IPCamera ) {
      int i;
      for( i = 0; i < MAX_IP_CAMERA_STREAM_NUM; i++ ) {
         if( m_IPCamInfoArr[i].m_bUseStream ) {
            ushort nCurCamFlag = ushort( 1 ) << i;
            m_nCameraFlag |= nCurCamFlag;
         }
      }
   } else {
      int nInputCameraNum = GetCameraNum();
      for( i = 0; i < nInputCameraNum; i++ ) {
         ushort nCurCamFlag = ushort( 1 ) << i;
         m_nCameraFlag |= nCurCamFlag;
      }
   }

   if( m_IPCamInfo.m_nCameraType ) {
      for( i = 0; i < MAX_IP_CAMERA_STREAM_NUM; i++ ) {
         m_IPCamInfoArr[i].SetVideoSizeFromCameraInfo();
      }
   }
   m_PanoramaCamInfo.SetVideoSizeFromCameraInfo();
   m_bPTZCamera = FALSE;
   if( m_nVideoChannelType == VideoChannelType_AnalogCamera ) {
      if( m_PTZInfo.m_nPTZControlType > 0 )
         m_bPTZCamera = TRUE;
   }
   if( m_nVideoChannelType == VideoChannelType_IPCamera ) {
      IPCameraProperty* pProperty = GetIPCameraProperty( m_IPCamInfo.m_nIPCameraType );
      if( m_IPCamInfo.m_bUsePTZ ) {
         m_bPTZCamera = TRUE;
      }
   }

#ifdef __SUPPORT_VIRTUAL_PTZ_CAMERA
   if( m_nVideoChannelType == VideoChannelType_VirtualPTZ ) {
      m_bPTZCamera = TRUE;
   }
#endif
   return DONE;
}

// 현재 사용해야하는 캡춰보드의 채널플레그를 얻는다.
// 캡춰보드 카메라를 점유할 때 사용하는 함수이다.
UINT64 CVideoChannelInfo::GetAnalogCameraChFlag()
{
   UINT64 nChFlag = 0;
   if( m_nVideoChannelType == VideoChannelType_AnalogCamera ) {
      nChFlag = UINT64( 1 ) << m_nCBChannelNo;
   }
   if( m_nVideoChannelType == VideoChannelType_Panorama ) {
      nChFlag = m_PanoramaCamInfo.GetAnalogCameraChFlag();
   }
   return nChFlag;
}

int CVideoChannelInfo::GetCameraNum()
{
   switch( m_nVideoChannelType ) {
   case VideoChannelType_Panorama:
      return m_PanoramaCamInfo.m_nInputCamNum;
   case VideoChannelType_IPCamera: {
      int nCameraNum = 0;
      int i;
      for( i = 0; i < MAX_IP_CAMERA_STREAM_NUM; i++ ) {
         if( m_IPCamInfoArr[i].m_bUseStream ) {
            nCameraNum++;
         }
      }
      return nCameraNum;
   }
   default:
      return 1;
   }
   return 0;
}

UINT64 CVideoChannelInfo::GetCameraFlag()
{
   return m_nCameraFlag;
}

void CVideoChannelInfo::SetVideoSize()
{
   switch( m_nVideoChannelType ) {
   case VideoChannelType_IPCamera:
      m_IPCamInfo.SetVideoSizeFromCameraInfo();
      m_nWidth  = m_IPCamInfo.m_nWidth;
      m_nHeight = m_IPCamInfo.m_nHeight;
      break;
   case VideoChannelType_Panorama:
      m_PanoramaCamInfo.SetVideoSizeFromCameraInfo();
      m_nWidth  = m_PanoramaCamInfo.m_nWidth;
      m_nHeight = m_PanoramaCamInfo.m_nHeight;
      break;
   case VideoChannelType_VideoFile:
      m_nWidth  = m_FileCamInfo.m_nWidth;
      m_nHeight = m_FileCamInfo.m_nHeight;
      break;
#ifdef __SUPPORT_VIRTUAL_PTZ_CAMERA
   case VideoChannelType_VirtualPTZ:
      m_nWidth  = m_VirtualPTZCamInfo.m_nWidth;
      m_nHeight = m_VirtualPTZCamInfo.m_nHeight;
      break;
#endif
   default:
      break;
   }
}

void CVideoChannelInfo::SetRemote( BOOL bRemote )
{
   m_bRemote = bRemote;
   int i;
   for( i = 0; i < MAX_IP_CAMERA_STREAM_NUM; i++ )
      m_IPCamInfoArr[i].m_bRemote = bRemote;
   m_FileCamInfo.m_bRemote     = bRemote;
   m_PanoramaCamInfo.m_bRemote = bRemote;
}

///////////////////////////////////////////////////////////////////////////
//
// Global Functions
//
///////////////////////////////////////////////////////////////////////////

BOOL CheckIPCamInfos( CIPCameraInfo* pInfo )
{
   if( pInfo->m_bUseStream ) {
      int nErrCode = pInfo->CheckParams();
      if( nErrCode == IPCamErr_EmptyIPAddress ) {
         logd( "Empty IP Address" );
         return FALSE;
      }
      if( nErrCode == IPCamErr_InvalidIPAddress ) {
         logd( "Invalid IP Address" );
         return FALSE;
      }
      if( nErrCode == IPCamErr_EmptyCameraType ) {
         logd( "IP camera type is not specified" );
         return FALSE;
      }
   }
   return TRUE;
}

BOOL CheckPanoramaInfos( CPanoramaCameraInfo* pInfo, int nVCSetupMode )
{
   int nErrCode = pInfo->CheckParams();
   if( nErrCode == PanoramaCamErr_InputCameraNumber ) {
      logd( "Input Camera Number is not matched" );
      return FALSE;
   }
   if( !( nVCSetupMode & VCSetupMode_SetupRemote ) ) {
      if( nErrCode == PanoramaCamErr_EmptyPVTPath ) {
         logd( "PVT file path is empty" );
         return FALSE;
      }
      if( nErrCode == PanoramaCamErr_NotExistPVTFile ) {
         logd( "PVT file is not exist" );
         return FALSE;
      }
      if( nErrCode == PanoramaCamErr_PVTFileReadErr ) {
         logd( "PVT file is invalid" );
         return FALSE;
      }
   }
   return TRUE;
}

int GetNewPTZCtrlType( int nPTZControlType )
{
   enum OldPTZContrlType {
      OldPTZControlType_CYNIX_CU_N22DC          = 1,
      OldPTZControlType_CYNIX_CU_N23DH          = 2,
      OldPTZControlType_CYNIX_CU_N26DC          = 3,
      OldPTZControlType_CYNIX_CV_N22C           = 4,
      OldPTZControlType_Samsung_C6435           = 5,
      OldPTZControlType_Axis                    = 7,
      OldPTZControlType_CryptoTelecom           = 8,
      OldPTZControlType_LG_LPT_EP551PS          = 9,
      OldPTZControlType_Honeywell_HSDC_251      = 10,
      OldPTZControlType_BOSCH_AUTODOME_500I     = 11,
      OldPTZControlType_IntelliVIX_iBOX         = 12,
      OldPTZControlType_Hitron_HF3S36AN         = 13,
      OldPTZControlType_Yujin_ETP5000S_33x      = 14,
      OldPTZControlType_Yujin_ETP5000S_22x      = 15,
      OldPTZControlType_ICanTek                 = 16,
      OldPTZControlType_AproMedia               = 17,
      OldPTZControlType_NIKO_NSD_S300           = 18,
      OldPTZControlType_NIKO_NSD_S360           = 19,
      OldPTZControlType_SamsungTechWin_SPD_1000 = 20,
      OldPTZControlType_SamsungTechWin_SPD_2300 = 21,
      OldPTZControlType_SamsungTechWin_SPD_2700 = 22,
      OldPTZControlType_SamsungTechWin_SPD_3000 = 23,
      OldPTZControlType_SamsungTechWin_SPD_3300 = 24,
      OldPTZControlType_SamsungTechWin_SPD_3350 = 25,
      OldPTZControlType_SamsungTechWin_SPD_3700 = 26,
      OldPTZControlType_SamsungTechWin_SPD_3750 = 27,
   };
   switch( nPTZControlType ) {
   case OldPTZControlType_CYNIX_CU_N22DC:
      return PTZControlType_CYNIX_CU_N22DC;
   case OldPTZControlType_CYNIX_CU_N23DH:
      return PTZControlType_CYNIX_CU_N23DH;
   case OldPTZControlType_CYNIX_CU_N26DC:
      return PTZControlType_CYNIX_CU_N26DC;
   case OldPTZControlType_CYNIX_CV_N22C:
      return PTZControlType_CYNIX_CV_N22C;
   case OldPTZControlType_Samsung_C6435:
      return PTZControlType_Samsung_C6435;
   case OldPTZControlType_Axis:
      return PTZControlType_Axis_214;
   case OldPTZControlType_CryptoTelecom:
      return PTZControlType_CryptoTelecom;
   case OldPTZControlType_LG_LPT_EP551PS:
      return PTZControlType_LG_LPT_EP551PS;
   case OldPTZControlType_Honeywell_HSDC_251:
      return PTZControlType_Honeywell_HSDC_251;
   case OldPTZControlType_BOSCH_AUTODOME_500I:
      return PTZControlType_BOSCH_AUTODOME_500I;
   case OldPTZControlType_Hitron_HF3S36AN:
      return PTZControlType_Hitron_HF3S36AN;
   case OldPTZControlType_Yujin_ETP5000S_33x:
      return PTZControlType_Yujin_ETP5000S_33x;
   case OldPTZControlType_Yujin_ETP5000S_22x:
      return PTZControlType_Yujin_ETP5000S_22x;
   case OldPTZControlType_ICanTek:
      return PTZControlType_ICanTek;
   case OldPTZControlType_AproMedia:
      return PTZControlType_AproMedia;
   case OldPTZControlType_NIKO_NSD_S300:
      return PTZControlType_NIKO_NSD_S300;
   case OldPTZControlType_NIKO_NSD_S360:
      return PTZControlType_NIKO_NSD_S360;
   case OldPTZControlType_SamsungTechWin_SPD_1000:
      return PTZControlType_SamsungTechWin_SPD_1000;
   case OldPTZControlType_SamsungTechWin_SPD_2300:
      return PTZControlType_SamsungTechWin_SPD_2300;
   case OldPTZControlType_SamsungTechWin_SPD_2700:
      return PTZControlType_SamsungTechWin_SPD_2700;
   case OldPTZControlType_SamsungTechWin_SPD_3000:
      return PTZControlType_SamsungTechWin_SPD_3000;
   case OldPTZControlType_SamsungTechWin_SPD_3300:
      return PTZControlType_SamsungTechWin_SPD_3300;
   case OldPTZControlType_SamsungTechWin_SPD_3350:
      return PTZControlType_SamsungTechWin_SPD_3350;
   case OldPTZControlType_SamsungTechWin_SPD_3700:
      return PTZControlType_SamsungTechWin_SPD_3700;
   case OldPTZControlType_SamsungTechWin_SPD_3750:
      return PTZControlType_SamsungTechWin_SPD_3750;
   }
   return ( nPTZControlType );
}

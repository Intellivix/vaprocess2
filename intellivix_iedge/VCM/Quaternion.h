#pragma once


typedef Point3D<double>  FVector3D;

namespace BCCL
{
   /////////////////////////////////////////////////////////////////////////////
   //
   // class: Quaternion
   //
   /////////////////////////////////////////////////////////////////////////////

   class Quaternion
   {
   public:
      double W, X, Y, Z;

   public:
      Quaternion (double fW = 1.0, double fX = 0.0, double fY = 0.0, double fZ = 0.0);
      Quaternion (const Quaternion& rkQ);

   public:
      // conversion between quaternions, matrices, and angle-axes
      void FromRotationMatrix (const Matrix& kRot);
      void ToRotationMatrix (Matrix& kRot) const;
      void FromAngleAxis (const double& rfAngle, const FVector3D& rkAxis);
      void ToAngleAxis (double& rfAngle, FVector3D& rkAxis) const;
      void FromAxes (const FVector3D* akAxis);
      void ToAxes (FVector3D* akAxis) const;
      void ToAngleXYZ (double& dfX, double& dfY, double& dfZ);
      void ToAngleZYX (double& dfX, double& dfY, double& dfZ);

      // 기본 산술 연산. 
      Quaternion& operator= (const Quaternion& rkQ);
      Quaternion operator+ (const Quaternion& rkQ) const;
      Quaternion operator- (const Quaternion& rkQ) const;
      Quaternion operator* (const Quaternion& rkQ) const;
      Quaternion operator* (double fScalar) const;
      friend Quaternion operator* (double fScalar, const Quaternion& rkQ);
      Quaternion operator- () const;
      FVector3D operator* (const FVector3D& rkVector) const;

      // 쿼터니온 연산. 
      void Normalize ();
      double Dot (const Quaternion& rkQ) const;  // dot product
      double Norm () const;                      // squared-length
      Quaternion Inverse () const;              // apply to non-zero quaternion
      Quaternion UnitInverse () const;          // apply to unit-length quaternion
      Quaternion Exp () const;
      Quaternion Log () const;


      // spherical linear interpolation
      static Quaternion Slerp (double fT, const Quaternion& rkP, const Quaternion& rkQ);
      static Quaternion SlerpExtraSpins (double fT, const Quaternion& rkP, const Quaternion& rkQ, int iExtraSpins);

      // setup for spherical quadratic interpolation
      static void Intermediate (const Quaternion& rkQ0, const Quaternion& rkQ1, const Quaternion& rkQ2, Quaternion& rka, Quaternion& rkB);

      // spherical quadratic interpolation
      static Quaternion Squad (double fT, const Quaternion& rkP, const Quaternion& rkA, const Quaternion& rkB, const Quaternion& rkQ);

      // cutoff for sine near zero
      static double ms_fEpsilon;

      // special values
      static Quaternion ZERO;
      static Quaternion IDENTITY;
   };

}
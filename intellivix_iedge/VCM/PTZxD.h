#pragma once
#include "PTZVector.h"

///////////////////////////////////////////////////////
// 
// Class: CPTZ1D    (PTZF 위치를 저장하는 클래스)
//             
///////////////////////////////////////////////////////

 
 class CPTZ1D : public FPTZVector

{
public:
   int   ID;
   
public:
   CPTZ1D *Prev, *Next;

public:
   CPTZ1D (   );

public:
   int   Read (CXMLIO* pIO);
   int   Write (CXMLIO* pIO);
};

///////////////////////////////////////////////////////
// 
// Class: CPTZ2D  (카메라별 PTZ위치 리스트)
//             
///////////////////////////////////////////////////////

  class CPTZ2D : public LinkedList<CPTZ1D>

{
public:
   char    SID[512];
   int     NRef;
   BCCL::Time   RefTime;
   CCriticalSection _Lock;
   BOOL    ConvertIntIPAddr2IPStr;  // 기존에는 Int형 IP주소를 사용하였으나 도메인주소가 가능하게 되면서 연결이 되기전까지는 IP주소정보가 의미가 없게되었다. 
                                    // 따라서 기존에 Int형 주소로 저장되어있던 IP주소는 모두 스트링으로 변환한다. (1313버전부터)

public:
   CPTZ2D (   );
   virtual ~CPTZ2D (   ) {   }
   CPTZ2D& operator= (CPTZ2D& from);
   
public:
   CPTZ2D *Prev, *Next;
   
public:
   int   AddPTZ1D (int id, FPTZVector &vec);
   int   FindPTZ1D (int id, CPTZ1D **ppPTZ1D);
   int   DeletePTZ1D (int id);
   int   Read (CXMLIO* pIO);
   int   Write (CXMLIO* pIO);

   void  IncreaseRef (   );
   void  ConvertSID_UIntIPAddr2ToIPAddrString (   );
};

///////////////////////////////////////////////////////
// 
// Class: CPTZ3D  (시스템별로 저장되는 카메라별 PTZ위치 리스트의 리스트)
//             
///////////////////////////////////////////////////////
 
 class CPTZ3D : public LinkedList<CPTZ2D>

{
protected:
   CCriticalSection _Lock;
   int MaxN2D;

public:
   CPTZ3D (   );
   virtual ~CPTZ3D (   ) {   }
   
public:
   int   AddPTZ2D (LPCSTR sid, CPTZ2D **ppPTZ2D);
   int   FindPTZ2D (LPCSTR sid, CPTZ2D **ppPTZ2D);
   int   Dump (   );

protected:
   int   Read (CXMLIO* pIO);
   int   Write (CXMLIO* pIO);
};

// Class: CPTZ3DFile

 class CPTZ3DFile : public CPTZ3D

{
protected:
   float       FileVer;
   std::string FilePath;
   int         MaxN2D;

public:
   CPTZ3DFile (   );
   
public:
   void  SetFilePath (LPCSTR path_name) { FilePath = path_name ; }
   int   LoadFile (   );
   int   SaveFile (   );

protected:
   int   Load (LPCSTR path_name);
   int   Save (LPCSTR path_name);
};

#include "stdafx.h"
#include "PelcoDProtocol.h"

#define PELCOD_SENSE       0x80
#define PELCOD_AUTO_SCAN   0x10
#define PELCOD_CAMERA_ON   0x08
#define PELCOD_IRIS_CLOSE  0x04
#define PELCOD_IRIS_OPEN   0x02
#define PELCOD_FOCUS_NEAR  0x01
#define PELCOD_FOCUS_FAR   0x80
#define PELCOD_ZOOM_OUT    0x40
#define PELCOD_ZOOM_IN     0x20
#define PELCOD_TILT_UP     0x10
#define PELCOD_TILT_DOWN   0x08
#define PELCOD_PAN_LEFT    0x04
#define PELCOD_PAN_RIGHT   0x02

///////////////////////////////////////////////////////////////////////////////
//
// Class: CPelcoDProtocol
//
///////////////////////////////////////////////////////////////////////////////

 CPelcoDProtocol::CPelcoDProtocol (   )
 
{
   m_nAddress    = 1;
   m_aMessage[0] = 0xFF;
}

 void CPelcoDProtocol::ContPTZMove (int nPanSpeed,int nTiltSpeed,int nZoomInOut)
 
{
   byte byCommand2 = 0;
   if (nPanSpeed > 0) {
      byCommand2 |=  PELCOD_PAN_RIGHT;
   }
   else if (nPanSpeed < 0) {
      byCommand2 |=  PELCOD_PAN_LEFT;
   }

   if (nTiltSpeed > 0) {
      byCommand2 |=  PELCOD_TILT_DOWN;
   }
   else if (nTiltSpeed < 0) {
      byCommand2 |=  PELCOD_TILT_UP;
   }

   if (nZoomInOut > 0) {
      byCommand2 |=  PELCOD_ZOOM_IN;
   }
   else if (nZoomInOut < 0) {
      byCommand2 |=  PELCOD_ZOOM_OUT;
   }

   byte byData1 = (byte)abs (nPanSpeed);
   byte byData2 = (byte)abs (nTiltSpeed);
   if (byData1 > 0x3F) byData1 = 0x3F;
   if (byData2 > 0x3F) byData2 = 0x3F;
   MakeMessage (0,byCommand2,byData1,byData2);
}

 void CPelcoDProtocol::MakeMessage (byte byCommand1,byte byCommand2,byte byData1,byte byData2)
 
{
   int i;
   
   m_aMessage[1] = m_nAddress;
   m_aMessage[2] = byCommand1;
   m_aMessage[3] = byCommand2;
   m_aMessage[4] = byData1;
   m_aMessage[5] = byData2;
   byte byCheckSum = 0;
   for (i = 1; i <= 5; i++) byCheckSum += m_aMessage[i];
   m_aMessage[6] = byCheckSum;
}

 void CPelcoDProtocol::SetZoomSpeed (int nZoomSpeed)
 
{
   if      (nZoomSpeed < 0) nZoomSpeed = 0;
   else if (nZoomSpeed > 3) nZoomSpeed = 3;
   MakeMessage (0,0x25,0,(byte)nZoomSpeed);
}

 void CPelcoDProtocol::AbsZoomMove (int nZoomPosHW)
 {
   BYTE zoomPosMSB = (BYTE)(nZoomPosHW >> 8);
   BYTE zoomPosLSB = (BYTE)(nZoomPosHW & 0xFF);
   MakeMessage (0, 0x4F, zoomPosMSB, zoomPosLSB);
 }
 

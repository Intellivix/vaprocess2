#pragma once
#if defined(__SUPPORT_PTZ_AREA)
#include "bccl_polygon.h"

using namespace VACL;

///////////////////////////////////////////////////////////////////////////////
//
// Enumerations
//
///////////////////////////////////////////////////////////////////////////////

enum MAPTZMoveType
{
   MAPTZMoveType_Preset = 0,     // 공조감시 영역에 진입하는 경우 연계 카메라에게 Preset 이동을 명령한다.
   MAPTZMoveType_AbsPos = 1,     // 공조감시 영역에 진입하는 경우 단순히 절대각 이동을 명령한다.  
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: PTZPolygonalArea
//
///////////////////////////////////////////////////////////////////////////////

class PTZPolygonalArea : public Polygon
{
public:
   int               PTZAreaType;          // CameraInfo.h 의 enum PAT 참고
   std::string       AreaName;
   Array1D<FPoint3D> PTZPositions;

   // 공조감시영역 관련 맴버들.
public:
   ulong    IPAddress;
   int      PortNo;
   int      ChannelNoMA;
   BOOL     Flag_SPTZTrack;
   int      PTZMoveType;            // 공조감시할 때 PTZ 이동 방식. enum MAPTZMoveType을 참조 할 것. 
   int      PresetNo;
   FPoint3D PTZPosMA;               // 공조카메라에서 이동해야할 위치를 지정한다. 
   float    WaitingTime;
   float    MinObjectLength;        // 본 영역에 진입항 물체의 길이가 본 변수보다 작다면 공조감시를 수행한다. 

   // IR Light영역 관련 맴버들.
public:
   int      IRLight;


public:
   PTZPolygonalArea *Prev, *Next;
   void* m_pVoid;

public:
   PTZPolygonalArea (   );
   virtual ~PTZPolygonalArea (   ) {   }

public:
   virtual int ReadFile   (FileIO &file) { return DONE; }
   virtual int WriteFile  (FileIO &file) { return DONE; }
   virtual int ReadFile   (CXMLIO* pIO);
   virtual int WriteFile  (CXMLIO* pIO);
   virtual int CheckSetup (PTZPolygonalArea* pg);

public:
   void Create (int n_vertices);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: PTZPolygonalAreaList
//
///////////////////////////////////////////////////////////////////////////////

class PTZPolygonalAreaList : public PolygonList<PTZPolygonalArea>
{
public:
   void RenderAreaMap (GImage &d_image, FPoint2D p2Scale, byte p_value = PG_WHITE);
};
#endif

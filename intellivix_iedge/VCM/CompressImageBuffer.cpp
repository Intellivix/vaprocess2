﻿#include "stdafx.h"
#include "CompressImageBuffer.h"

#ifndef DBL_MAX
#define DBL_MAX 1.7976931348623158e+308 // max value
#endif

//////////////////////////////////////////////////////
//
// Class : CCompressImageBufferQueue
//
//////////////////////////////////////////////////////

CCompressImageBufferQueue::CCompressImageBufferQueue()
{
   m_dwCountPush           = 0;
   m_nVideoFrmRecvCount    = 0;
   m_nVideoKeyFrmRecvCount = 0;
   m_fBitrate              = 0.0f;
   m_nBitrateCheckTime     = GetTickCount() - 1000;
   m_BitrateChecker.Initialize( 30.0f, 3.0f );
   m_dwlastPush = 0;
}

CCompressImageBufferQueue::~CCompressImageBufferQueue()
{
   Clear();
}

CCompressImageBufferItem* CCompressImageBufferQueue::operator[]( uint i )
{
   Lock();
   CCompressImageBufferItem* pItem = ( (std::deque<CCompressImageBufferItem*>)( *this ) )[i];
   Unlock();
   return pItem;
}

int CCompressImageBufferQueue::Size()
{
   Lock();
   int nSize = (int)size();
   Unlock();
   return nSize;
}

void CCompressImageBufferQueue::Lock()
{
   m_csQueue.Lock();
}

void CCompressImageBufferQueue::Unlock()
{
   m_csQueue.Unlock();
}

void CCompressImageBufferQueue::Push( CCompressImageBufferItem* pItem )
{
   if( pItem ) {
      m_nVideoFrmRecvCount++;
      if( pItem->m_dwCommFlag & VFRM_KEYFRAME )
         m_nVideoKeyFrmRecvCount++;
   }

   int nDataSizeInByte;
   int nSize = (int)size();
   if( nSize > 1000 ) {
      DeleteUntilFirstKeyFrame();
      return;
   }
   int nCodecID;
   m_csQueue.Lock();
   nCodecID        = pItem->m_nCodecID;
   nDataSizeInByte = pItem->m_CompBuff.Length;
   push_back( pItem );
   m_dwCountPush++;
   m_dwlastPush = GetTickCount();
   m_csQueue.Unlock();

   if( nCodecID != VCODEC_ID_RAW ) {
      CBitrateCheckItem bc_item;
      bc_item.m_nTime           = GetTickCount();
      bc_item.m_nDataSizeInByte = nDataSizeInByte;
      m_BitrateChecker.AddItem( bc_item );
      if( GetTickCount() - m_nBitrateCheckTime > 1000 ) {
         m_nBitrateCheckTime = GetTickCount();
         m_fBitrate          = m_BitrateChecker.CalcAvgBitrate( 3.0f );
      }
   }
}

CCompressImageBufferItem* CCompressImageBufferQueue::Pop()
{
   CCompressImageBufferItem* pItem = NULL;

   Lock();
   int nSize = (int)size();
   if( nSize ) {
      pItem = front();
      pop_front();
   }
   Unlock();

   return pItem;
}

CCompressImageBufferItem* CCompressImageBufferQueue::Front()
{
   Lock();
   CCompressImageBufferItem* pItem = front();
   Unlock();
   return pItem;
}

void CCompressImageBufferQueue::Clear()
{
   Lock();
   int nItem = (int)size();
   if( nItem ) {
      std::deque<CCompressImageBufferItem*>::iterator iter    = begin();
      std::deque<CCompressImageBufferItem*>::iterator iterEnd = end();
      while( iter != iterEnd ) {
         delete *iter;
         iter++;
      }
   }
   clear();
   m_dwCountPush = 0;
   Unlock();
}

void CCompressImageBufferQueue::DeleteUntilFirstKeyFrame()
{
   int i;

   Lock();
   // 키프레임을 역순으로 찾고 찾은 다음 프레임 들은 모두 삭제한다.
   int nItem          = (int)size();
   BOOL bFindKeyFrame = FALSE;
   if( nItem ) {
      std::deque<CCompressImageBufferItem*>::iterator iter      = end();
      std::deque<CCompressImageBufferItem*>::iterator iterBegin = begin();
      int nItemIdx                                              = nItem;
      int nFindItemIdx                                          = -1;
      do {
         iter--;
         nItemIdx--;
         CCompressImageBufferItem* pItem = *iter;
         if( FALSE == bFindKeyFrame ) {
            if( pItem->m_dwCommFlag & VFRM_KEYFRAME ) {
               bFindKeyFrame = TRUE;
               nFindItemIdx  = nItemIdx;
               break;
            }
         }
      } while( iter != iterBegin );

      if( nFindItemIdx >= 0 ) {
         for( i = 0; i < nFindItemIdx; i++ ) {
            CCompressImageBufferItem* pItem = front();
            if( pItem ) delete pItem;
            pop_front();
         }
      }
   }
   Unlock();
}

DWORD CCompressImageBufferQueue::GetLastPushTime()
{
   DWORD dwLastPush;
   Lock();
   dwLastPush = m_dwlastPush;
   Unlock();
   return dwLastPush;
}

void CCompressImageBufferQueue::ResetLastPushTime()
{
   Lock();
   m_dwlastPush = 0;
   Unlock();
}

//////////////////////////////////////////////////////
//
// Class : CDisplayImageBufferQueue
//
//////////////////////////////////////////////////////

double CDisplayImageBufferQueue::m_dfDelayQueueAveragingTime = 10.0f;

CDisplayImageBufferQueue::CDisplayImageBufferQueue()
{
   m_bInitialized            = FALSE;
   m_nMaxQueueSize           = 0;
   m_fFrameRate              = 0.0f;
   m_dfAvgDelayedQueueLength = 0.0;
}

CDisplayImageBufferQueue::~CDisplayImageBufferQueue()
{
}

void CDisplayImageBufferQueue::Initialize( float fFrameRate, int nMaxQueueSize, ISize2D imageSize, byte* pBuffer )
{
   m_fFrameRate = fFrameRate;

   CCriticalSectionSP co( m_csQueue );

   Clear();

   m_nMaxQueueSize = nMaxQueueSize;
   m_ImageSize     = imageSize;

   int i;
   for( i = 0; i < nMaxQueueSize; i++ ) {
      CCompressImageBufferItem* pItem = new CCompressImageBufferItem;
      pItem->m_nCompBuffSize          = GetYUY2ImageLength( imageSize.Width, imageSize.Height );
      pItem->m_CompBuff.Create( pItem->m_nCompBuffSize );
      pItem->m_nCodecID = VCODEC_ID_RAW;
      memcpy( (byte*)pItem->m_CompBuff, pBuffer, pItem->m_nCompBuffSize );
      push_back( pItem );
   }

   m_bInitialized = TRUE;
}

void CDisplayImageBufferQueue::Close()
{
   Clear();
   m_bInitialized = FALSE;
}

void CDisplayImageBufferQueue::Clear()
{
   Lock();
   int nItem = (int)size();
   if( nItem ) {
      std::deque<CCompressImageBufferItem*>::iterator iter    = begin();
      std::deque<CCompressImageBufferItem*>::iterator iterEnd = end();
      while( iter != iterEnd ) {
         delete *iter;
         iter++;
      }
   }
   clear();
   m_nMaxQueueSize = 0;
   m_ImageSize( 0, 0 );
   Unlock();
}

BOOL CDisplayImageBufferQueue::IsInitialized()
{
   return m_bInitialized;
}

BOOL CDisplayImageBufferQueue::HasChanged( int nMaxQueueSize, ISize2D imageSize )
{
   if( m_nMaxQueueSize != nMaxQueueSize ) return TRUE;
   if( imageSize.Width != m_ImageSize.Width ) return TRUE;
   if( imageSize.Height != m_ImageSize.Height ) return TRUE;
   return FALSE;
}

void CDisplayImageBufferQueue::Lock()
{
   m_csQueue.Lock();
}

void CDisplayImageBufferQueue::Unlock()
{
   m_csQueue.Unlock();
}

int CDisplayImageBufferQueue::Size()
{
   Lock();
   int nSize = (int)size();
   Unlock();
   return nSize;
}

CCompressImageBufferItem* CDisplayImageBufferQueue::GetLastBuffer()
{
   CCriticalSectionSP co( m_csQueue );
   if( size() > 1 ) {
      CCompressImageBufferItem* pLastBuffer = *rbegin();
      if( pLastBuffer ) {
         return pLastBuffer;
      }
   }
   return NULL;
}

CCompressImageBufferItem* CDisplayImageBufferQueue::GetDelayedBuffer( double dfCurrNormalPlaytime, double dfDelayTime )
{
   Lock();

   SortByPlayNormalTime();

   CCompressImageBufferItem* pDelayedBufferItem = NULL;

   int nQueueSize = (int)size();

   double dfRatioAveraging = 1.0 / ( m_fFrameRate * m_dfDelayQueueAveragingTime );

   if( 0.0 == m_dfAvgDelayedQueueLength )
      m_dfAvgDelayedQueueLength = dfDelayTime * m_fFrameRate;
   else
      m_dfAvgDelayedQueueLength = m_dfAvgDelayedQueueLength * ( 1.0 - dfRatioAveraging ) + ( dfDelayTime * m_fFrameRate ) * dfRatioAveraging;

   int nDelayedFrameCount                                          = 0;
   std::deque<CCompressImageBufferItem*>::reverse_iterator iter    = rbegin();
   std::deque<CCompressImageBufferItem*>::reverse_iterator iterEnd = rend();
   while( iter != iterEnd ) {
      CCompressImageBufferItem* pItem = *iter;
      if( DBL_MAX != pItem->m_dfNormalPlayTime ) {
         if( nDelayedFrameCount >= int( m_dfAvgDelayedQueueLength ) ) {
            pDelayedBufferItem                     = pItem;
            pDelayedBufferItem->m_dfNormalPlayTime = DBL_MAX;
         }
         nDelayedFrameCount++;
      }
      iter++;
   }

   if( NULL == pDelayedBufferItem ) {
      pDelayedBufferItem                     = *begin();
      pDelayedBufferItem->m_dfNormalPlayTime = DBL_MAX;
   }

   Unlock();

   return pDelayedBufferItem;
}

bool TimeStampGreater( CCompressImageBufferItem* item1, CCompressImageBufferItem* item2 )
{
   if( item1->m_PresentationTime.tv_sec < item2->m_PresentationTime.tv_sec )
      return true;
   else if( item1->m_PresentationTime.tv_sec == item2->m_PresentationTime.tv_sec ) {
      if( item1->m_PresentationTime.tv_usec < item2->m_PresentationTime.tv_usec )
         return true;
      else
         return false;
   } else
      return false;
}

bool NormalPlayTimeGreater( CCompressImageBufferItem* item1, CCompressImageBufferItem* item2 )
{
   if( item1->m_dfNormalPlayTime < item2->m_dfNormalPlayTime )
      return true;
   else
      return false;
}

void CDisplayImageBufferQueue::SortByPlayNormalTime()
{
   Lock();
   std::stable_sort( begin(), end(), NormalPlayTimeGreater );
   Unlock();
}

//////////////////////////////////////////////////////
//
// Class : CVCCameraEventCallbackParamQueue
//
//////////////////////////////////////////////////////

CVCCameraEventCallbackParamQueue::CVCCameraEventCallbackParamQueue()
{
}

CVCCameraEventCallbackParamQueue::~CVCCameraEventCallbackParamQueue()
{
   Clear();
}

CVCCameraEventCallbackParamItem* CVCCameraEventCallbackParamQueue::operator[]( uint i )
{
   Lock();
   CVCCameraEventCallbackParamItem* pItem = ( (std::deque<CVCCameraEventCallbackParamItem*>)( *this ) )[i];
   Unlock();
   return pItem;
}

int CVCCameraEventCallbackParamQueue::Size()
{
   Lock();
   int nSize = (int)size();
   Unlock();
   return nSize;
}

void CVCCameraEventCallbackParamQueue::Lock()
{
   m_csQueue.Lock();
}

void CVCCameraEventCallbackParamQueue::Unlock()
{
   m_csQueue.Unlock();
}

void CVCCameraEventCallbackParamQueue::Push( CVCCameraEventCallbackParamItem* pItem )
{
   m_csQueue.Lock();
   push_back( pItem );
   m_csQueue.Unlock();
}

CVCCameraEventCallbackParamItem* CVCCameraEventCallbackParamQueue::Pop()
{
   CVCCameraEventCallbackParamItem* pItem = NULL;

   Lock();
   int nSize = (int)size();
   if( nSize ) {
      pItem = front();
      pop_front();
   }
   Unlock();

   return pItem;
}

CVCCameraEventCallbackParamItem* CVCCameraEventCallbackParamQueue::Front()
{
   Lock();
   CVCCameraEventCallbackParamItem* pItem = front();
   Unlock();
   return pItem;
}

void CVCCameraEventCallbackParamQueue::Clear()
{
   Lock();
   int nItem = (int)size();
   if( nItem ) {
      std::deque<CVCCameraEventCallbackParamItem*>::iterator iter    = begin();
      std::deque<CVCCameraEventCallbackParamItem*>::iterator iterEnd = end();
      while( iter != iterEnd ) {
         delete *iter;
         iter++;
      }
   }
   clear();
   Unlock();
}

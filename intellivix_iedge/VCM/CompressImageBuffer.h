﻿#pragma once

#include <mutex>
#include "BitrateChecker.h"

//////////////////////////////////////////////////////
//
// Class : CCompressImageBufferItem
//
//////////////////////////////////////////////////////

class CCompressImageBufferItem
{
public:
   int         m_nCodecID;
   BArray1D    m_CompBuff;
   int         m_nCompBuffSize;
   DWORD       m_dwCommFlag;      // 업체와는 상관없이 공통적으로 사용하는 플레그
   DWORD       m_dwFlag;          
   DWORD       m_dwType;
   timeval     m_PresentationTime;
   double      m_dfNormalPlayTime;

   CCompressImageBufferItem()
   {
      m_dfNormalPlayTime = 0.0;
   }
};

//////////////////////////////////////////////////////
//
// Class : CCompressImageBufferQueue
//
//////////////////////////////////////////////////////

class CCompressImageBufferQueue : public std::deque<CCompressImageBufferItem*>
{
public:
   float           m_fBitrate;
protected:
   uint32_t            m_nBitrateCheckTime;
   CBitrateChecker m_BitrateChecker;   

public:
   int              m_dwCountPush;
   uint             m_nVideoFrmRecvCount;    // debug 용
   uint             m_nVideoKeyFrmRecvCount; // debug 용
   DWORD            m_dwlastPush;

   CCriticalSection m_csQueue;

public:
   CCompressImageBufferQueue  (void);
   ~CCompressImageBufferQueue (void);

public:
   CCompressImageBufferItem* operator[] (uint i);

public:
   void                      Lock   (  );
   void                      Unlock (  );
   int                       Size   (  );
   void                      Push   (CCompressImageBufferItem* pItem);
   CCompressImageBufferItem* Pop    (  );
   CCompressImageBufferItem* Front  (  );
   void                      Clear  (  );
   void                      DeleteUntilFirstKeyFrame (   );
   DWORD                     GetLastPushTime (   );
   void                      ResetLastPushTime (   );
}; 


//////////////////////////////////////////////////////
//
// Class : CDisplayImageBufferQueue
//
//////////////////////////////////////////////////////

class CDisplayImageBufferQueue: public std::deque<CCompressImageBufferItem*>
{
protected:
   BOOL             m_bInitialized;
   int              m_nMaxQueueSize;
   ISize2D          m_ImageSize;
   CCriticalSection m_csQueue;
   float            m_fFrameRate;
   double           m_dfAvgDelayedQueueLength;
   static double    m_dfDelayQueueAveragingTime;

public:
   CDisplayImageBufferQueue  (void);
   ~CDisplayImageBufferQueue (void);

public:
   void                      Initialize (float fFrameRate, int nMaxQueueSize, ISize2D imageSize, byte* pBuffer);
   void                      Close  ();
   void                      Clear  ();
   BOOL                      IsInitialized();
   BOOL                      HasChanged(int nMaxQueueSize,ISize2D imageSize);
   void                      Lock   ();
   void                      Unlock ();
   int                       Size   ();
   CCompressImageBufferItem* GetLastBuffer ();
   CCompressImageBufferItem* GetDelayedBuffer (double dfCurrNormalPlaytime, double dfDelayTime = 10.0);

protected:
   void SortByPlayNormalTime();
};

//////////////////////////////////////////////////////
//
// Class : CVCCameraEventCallbackParamItem
//
//////////////////////////////////////////////////////

// CVideoChannel의 상태 콜백함수의 파라메터 인자들을 저장하는 클래스임. (jun)
// 현재는 VCM 라이브러리 내에서 사용하지 않으며 IntelliVIX 의 CCamera 클래스에서 사용하고 있음. 

class CVCCameraEventCallbackParamItem
{
public:
   int m_nVCState;
   int m_nStreamIdx;
   int m_nCamState;
   int m_nCamEvt;
};

//////////////////////////////////////////////////////
//
// Class : CVCCameraEventCallbackParamQueue
//
//////////////////////////////////////////////////////

class CVCCameraEventCallbackParamQueue: public std::deque<CVCCameraEventCallbackParamItem*>
{
public:
   CCriticalSection m_csQueue;

public:
   CVCCameraEventCallbackParamQueue  (void);
   ~CVCCameraEventCallbackParamQueue (void);

public:
   CVCCameraEventCallbackParamItem* operator[] (uint i);

public:
   void                             Lock   (  );
   void                             Unlock (  );
   int                              Size   (  );
   void                             Push   (CVCCameraEventCallbackParamItem* pItem);
   CVCCameraEventCallbackParamItem* Pop    (  );
   CVCCameraEventCallbackParamItem* Front  (  );
   void                             Clear  (  );
}; 


#include "stdafx.h"
#include "Camera_VirtualPTZ.h"

#ifdef __SUPPORT_PTZ_CAMERA
#ifdef __SUPPORT_VIRTUAL_PTZ_CAMERA

#include "VCM.h"
#include "CameraProductAndItsProperties.h"
#include "VideoChannel.h"
#include "VideoChannelManager.h"
#include "PTZCtrl.h"


BOOL g_bSignalSourceCameraToVirtualPTZCamera = TRUE;

//////////////////////////////////////////////////////////////////////////////////
//
// Class : CCamera_VirtualPTZ
//
//////////////////////////////////////////////////////////////////////////////////

CCamera_VirtualPTZ::CCamera_VirtualPTZ ()
{
   m_nVirtualPTZCamState = 0;
   m_nPrevCheckTime = 0;
   m_nCaptureStartTime = 0;
   m_fZoomMagnifyingFactor = 1.0f;

   m_pVCSrc = NULL;
}

CCamera_VirtualPTZ::~CCamera_VirtualPTZ ()
{
}

int CCamera_VirtualPTZ::Initialize (CVirtualPTZCameraInfo* pInfo, CVideoChannel* pVC)
{
   m_nVirtualPTZCamState = 0;
   m_pInfo = pInfo;
   m_pVC   = pVC;
   m_pVCM  = pVC->m_pVCM;
   m_nChannelNo_IVX = m_pVC->m_nChannelNo_IVX;

   return (DONE);
}

int CCamera_VirtualPTZ::BeginCapture (   )
{
   if (m_nState & State_Capture) return (DONE);
   m_nState |= State_Capture;
   if (!(m_nVirtualPTZCamState & VirtualPTZCameraState_SourceVideoChannelRemoved))
      m_nVirtualPTZCamState = VirtualPTZCameraState_SearchingSourceVideoChannel;
   m_pVC->m_nState &= ~VCState_CanNotFindSourceVideoChannel;
   m_Capturer.Width  = m_pInfo->m_nWidth;
   m_Capturer.Height = m_pInfo->m_nHeight;
   m_Capturer.CapBoxSizeRatio = 1.0f;
   m_Capturer.MinObjectSizeRatio = 0.0f;
   m_YUY2Img.Create (m_pInfo->m_nWidth * m_pInfo->m_nHeight * 2);
   BlackOut (m_YUY2Img, 0x00);
   m_nCaptureStartTime = GetTickCount (    );
   return (DONE);
}

int CCamera_VirtualPTZ::EndCapture (   )
{
   if (!(m_nState & State_Capture)) return (DONE);
   m_evtSourceImageCapture.SetEvent (   );

   if (g_bSignalSourceCameraToVirtualPTZCamera) {
      if (m_pVCM) {
         if (m_pVCSrc && !(m_pVCSrc->m_nState & VCState_Deactivating)) {
            CCriticalSectionSP so3 (m_pVCSrc->m_csVirtualPTZCameras);
            int nVCID_Src = m_pVCSrc->m_nVCID;
            std::deque<CVideoChannel*>::iterator iter     = m_pVCSrc->m_VirtualPTZCameras.begin (   );
            std::deque<CVideoChannel*>::iterator iter_end = m_pVCSrc->m_VirtualPTZCameras.end (   );
            while (iter != iter_end) {
               CVideoChannel* pVC = (*iter);
               if (pVC->m_nVCID == m_pVC->m_nVCID) {
                  m_pVCSrc->m_VirtualPTZCameras.erase (iter);
                  break;
               }
               iter++;
            }
         }
      }
   }
   m_evtSourceImageCapture.SetEvent(   );

   m_nState &= ~State_Capture;
   return (DONE);
}

int CCamera_VirtualPTZ::GetImage_Lock (byte **d_buffer)
{
   byte* pImage = (byte*)m_YUY2Img;
   *d_buffer = pImage;

   if (!(m_nState & State_Capture)) return (1);

   int nCurTime = GetTickCount (   );

   if (m_nVirtualPTZCamState & VirtualPTZCameraState_SourceVideoChannelRemoved)
   {
      return (1);
   }
   // CVideoChannelManager로 부터 소스영상 카메라가 생성될 때까지 대기하는 상태
   else if (m_nVirtualPTZCamState & VirtualPTZCameraState_SearchingSourceVideoChannel)
   {
      if (!m_nPrevCheckTime || (nCurTime - m_nPrevCheckTime > 1000))
      {
         m_nPrevCheckTime = nCurTime;
         if (m_pVCM->DoesAnyOneChanneInDeactivating (   )) {
            return (2);
         }
         CCriticalSectionSP co (&m_pVCM->m_csVideoChannelList);
         // 소스영상 카메라가 생성되었으면 소스영상 카메라를 등록한다.
         m_pVCSrc = m_pVCM->GetVideoChannel (m_pVC->m_VirtualPTZCamInfo.m_nVCID_SourceCamera);
         if (m_pVCSrc && !(m_pVCSrc->m_nState & VCState_Deactivating)) {
            CCriticalSectionSP co2 (&m_pVCSrc->m_csVideoChannel);
            if (m_pVCSrc && !(m_pVCSrc->m_nState & VCState_Deactivating) && !m_pVCSrc->m_bStopThead[VC_Cap]) {
               if (g_bSignalSourceCameraToVirtualPTZCamera) {
                  CCriticalSectionSP so3 (&m_pVCSrc->m_csVirtualPTZCameras);
                  m_pVCSrc->m_VirtualPTZCameras.push_back (m_pVC);
               }
               m_nVirtualPTZCamState &= ~VirtualPTZCameraState_SearchingSourceVideoChannel;
               m_nVirtualPTZCamState |=  VirtualPTZCameraState_SourceVideoChannelRegisterd;
               ISize2D srcImgSize = m_pVCSrc->GetVideoSize (m_pVC->m_VirtualPTZCamInfo.m_nStreamIdx);
               m_p2CenterPos (srcImgSize.Width / 2, srcImgSize.Height / 2);
               float fWidthRatio  = float (srcImgSize.Width)  / m_pVC->m_nWidth;
               float fHeightRatio = float (srcImgSize.Height) / m_pVC->m_nHeight;
               float fMinRatio = fHeightRatio;
               if (fMinRatio > fWidthRatio) fMinRatio = fWidthRatio;
               m_fZoomMagnifyingFactor = fMinRatio * m_pVC->m_VirtualPTZCamInfo.m_fSourceVideoReductionFactorOfZoom1x;
               if (m_pVCSrc->m_fFPS_Cap < m_pVC->m_fFPS_Cap)
                  m_pInfo->m_fFrameRate =  m_pVC->m_fFPS_Cap = m_pVCSrc->m_fFPS_Cap;
            }
         }
      }
      if (GetTickCount (   ) - m_nCaptureStartTime > 4000) {
         m_pVC->m_nState |= VCState_CanNotFindSourceVideoChannel;
      }
      return (1);
   }
   else if (m_nVirtualPTZCamState & VirtualPTZCameraState_SourceVideoChannelRegisterd)
   {
      if (m_pVCSrc && (m_pVCSrc->m_nState & VCState_Deactivating)) {
         return (1);
      }
      CVirtualPTZCameraInfo& virtualPTZCamInfo = m_pVC->m_VirtualPTZCamInfo;

      if (g_bSignalSourceCameraToVirtualPTZCamera)
         m_evtSourceImageCapture.Wait_For(1000);

      float fZoomFactor = 1.0f;
      if (m_pVC->m_pPTZCtrl) {
         FPTZVector fpvAbsPos = m_pVC->m_pPTZCtrl->GetAbsPos (   );
         fZoomFactor  = fpvAbsPos.Z;
         fZoomFactor /= m_fZoomMagnifyingFactor;
      }
      ISize2D srcImgSize = m_pVCSrc->GetVideoSize (virtualPTZCamInfo.m_nStreamIdx);
      ISize2D dstImgSize (virtualPTZCamInfo.m_nWidth, virtualPTZCamInfo.m_nHeight);

      byte* si_buffer = m_pVCSrc->m_YUY2Buff;
      int nStreamIndex = 0;
      if (VideoChannelType_IPCamera == m_pVCSrc->m_nVideoChannelType) {
         nStreamIndex = m_pVC->m_VirtualPTZCamInfo.m_nStreamIdx;
         if (!m_pVCSrc->m_IPCamInfoArr[nStreamIndex].m_bUseStream) {
            nStreamIndex = 0;
         }
      }
      m_pVCSrc->GetVideoBuff (nStreamIndex, &si_buffer, srcImgSize.Width, srcImgSize.Height);

      if (si_buffer) {
         m_SrcImageSize = srcImgSize;
         // Fisheye 카메라이면 Dewarping 수행
         if (m_pVCSrc->IsFisheyeCamera()) {
            BOOL bDstSizeChanged = FALSE;
            if(m_nPrevDstSize.Width != dstImgSize.Width || m_nPrevDstSize.Height != dstImgSize.Height)
               bDstSizeChanged = TRUE;

            if(m_pVCSrc->IsFisheyeInit() && !m_pVC->m_bFisheyeInit_VPTZ) {
               if (VideoChannelType_VideoFile == m_pVCSrc->m_nVideoChannelType)
                  m_pVC->m_FisheyeInfo_VPTZ = m_pVCSrc->m_IPCamInfo.m_FisheyeDewarpingSetup.m_FisheyeInfo;
               else if (VideoChannelType_IPCamera == m_pVCSrc->m_nVideoChannelType)
                  m_pVC->m_FisheyeInfo_VPTZ = m_pVCSrc->m_IPCamInfoArr[nStreamIndex].m_FisheyeDewarpingSetup.m_FisheyeInfo;
               m_pVC->m_bFisheyeInit_VPTZ = TRUE;
            }
            if(bDstSizeChanged) {
               m_DewarpBuffer.Create(dstImgSize.Width * dstImgSize.Height * 2);
               m_nPrevDstSize = dstImgSize;
            }

            // Fisheye Image To Planar Image
            m_pVC->Dewarp_YUY2(si_buffer, srcImgSize.Width, srcImgSize.Height, m_DewarpBuffer, dstImgSize.Width, dstImgSize.Height);
            memcpy(m_YUY2Img, m_DewarpBuffer, m_DewarpBuffer.Length);
         }
         else {
            IBox2D  so_box;
            int hw        = int (dstImgSize.Width  / fZoomFactor / 2 / 2 * 2);
            int hh        = int (dstImgSize.Height / fZoomFactor / 2 / 2 * 2);
            so_box.X      = int (m_p2CenterPos.X - hw);
            so_box.Y      = int (m_p2CenterPos.Y - hh);
            so_box.Width  = hw * 2;
            so_box.Height = hh * 2;
            BArray1D di_buffer;
            ISize2D di_size;
            m_Capturer.Perform (si_buffer,m_SrcImageSize,so_box,di_buffer,di_size);
            m_YUY2Img = di_buffer;

         }
      }
      m_nPrevCheckTime = 0;
      // 등록된 소스영상 카메라로 부터 캡쳐신호가 나타나면 영상 패치 수행
   }
   return (DONE);
}

int CCamera_VirtualPTZ::GetImage_Unlock ()
{
   return (DONE);
}

int CCamera_VirtualPTZ::GetColorSpace (  )
{
   return 0;
}

int CCamera_VirtualPTZ::GetImageSize ()
{
   return m_pInfo->m_nWidth * m_pInfo->m_nHeight;
}

#endif // __SUPPORT_VIRTUAL_PTZ_CAMERA
#endif // __SUPPORT_PTZ_CAMERA

#pragma once

#ifdef __SUPPORT_PTZ_CAMERA
#ifdef __SUPPORT_VIRTUAL_PTZ_CAMERA

#include "PTZCtrl.h"
#include "CameraInfo.h"

class CCamera_VirtualPTZ;

///////////////////////////////////////////////////////////////////////////////
//
// Class: CPTZCtrl_Virtual
//
///////////////////////////////////////////////////////////////////////////////

class CPTZCtrl_Virtual : public CPTZCtrl, public MasterToSlaveCamera
{
public:
   uint m_nPrevTime;
   CCamera_VirtualPTZ* m_pVirtualPTZCamera;
   FPTZVector m_VirtualPTZPos;

protected:       
   BOOL m_bIsSrcFisheyeCam;

public:
   CPTZCtrl_Virtual (int nPTZControlType, ISize2D s2VideoSize);

public:
   virtual int   Open (CCamera_VirtualPTZ* pVirtualPTZCemera);
   virtual BOOL  IsFisheyeCam () { return m_bIsSrcFisheyeCam; }

public:
   virtual int   _SetPreset           (int preset_no) { return (DONE); }
   virtual int   _ClearPreset         (int preset_no) { return (DONE); }
   virtual int   _GotoPreset          (int preset_no) { return (DONE); }

public:                            
   virtual int   _GotoAbsPos          (FPTZVector abs_pos);
   virtual int   GetWaitingTime       (float pd);
   virtual int   OffsetPositionByBox  (   );

public:
   void VirtualPTZProc               (   );
   void CheckPTZValue_Fisheye        (FPTZVector& pos);
};

#endif // __SUPPORT_VIRTUAL_PTZ_CAMERA
#endif //__SUPPORT_PTZ_CAMERA



﻿#ifndef IPCAMSTREAM_RTSP_H
#define IPCAMSTREAM_RTSP_H

#include "version.h"
#include "IPCamStream.h"
#include "CameraInfo.h"
#include "RTSPCustomClient.h"

class IPCameraProperty;
class CIPCameraInfo;
class CVCMRTSPClient;

#pragma pack(8)

/////////////////////////////////////////////////////////////////////////////
//
// class CIPCamStream_RTSP
//
/////////////////////////////////////////////////////////////////////////////

class CIPCamStream_RTSP : public CIPCamStream, public CRTSPCustomClient
{

public:
   std::string         m_strRTSP_URL;
   CIPCameraInfo*      m_pInfo;
   CIPCameraInfo       m_TmpInfo;
   IPCameraProperty*   m_pProperty;   

   LPVIDEOCHANNEL_CALLBACK_RTSP_METADATA      m_pCBRTSPMetadata;
public:
   uint              m_nLocalState;
   uint              m_nReceiveFrameCount = 0;
   uint              m_nPrevReceiveFrameCountCheckTime = 0;

public:  
   CIPCamStream_RTSP(void);
   virtual ~CIPCamStream_RTSP(void);

   CAVCodecParam GetAVCodecParam(VideoCodecID emCodecID);

   virtual int  Initialize (CIPCameraInfo* pInfo);
   virtual int  Uninitialize ();
   virtual BOOL IsConnected ();
   virtual int  BeginCapture ();
   virtual int  EndCapture ();
   virtual int  SignalStopCapture ();
   virtual int  Close ();

   virtual int Decompress (CCompressImageBufferItem * pBuffItem, BArray1D& destBuff);

public:
   virtual void OnErrorRaised        (int err_code);
   virtual void OnAudioDataReceived  (int codec_id,byte *s_buffer,int s_buf_size,timeval& timestampl);
   virtual void OnVideoFrameReceived (int codec_id,byte *s_buffer,int s_buf_size,int flags,timeval& timestamp);
   virtual void OnMetaDataReceived   (byte * s_buffer,int s_buf_size,timeval & timestamp);
   virtual void OnVideoSizeChanged   (int nWidth, int nHeight);
};

#ifdef __SUPPORT_ONVIF
/////////////////////////////////////////////////////////////////////////////
//
// class CIPCamStream_Onvif
//
/////////////////////////////////////////////////////////////////////////////
class CIPCamStream_Onvif : public CIPCamStream_RTSP
{
public:
   CIPCamStream_Onvif(void);
   virtual ~CIPCamStream_Onvif(void);
   virtual int BeginCapture();
public:
   CCriticalSection  m_csOnvifCtrlLock;
};
#endif

#endif // IPCAMSTREAM_RTSP_H

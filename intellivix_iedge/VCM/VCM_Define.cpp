﻿
#include "stdafx.h"
#include "VCM_Define.h"
#include "Live555Define.h"


float g_fMaxDisplayFrameRate = 15.0f; // 최대 디스플레이 프레임 비율.

///////////////////////////////////////////////////////////////////////////
//
// class: VCM_Desc
//
///////////////////////////////////////////////////////////////////////////

VCM_Desc::VCM_Desc()
{
   m_bUseVCMConfigFile   = FALSE;
   m_strConfigPath       = "";
   m_pCBImageProcess     = NULL;
   m_pCBDisplay          = NULL;
   m_pCBRecord           = NULL;
   m_pCBCamEvent         = NULL;
   m_pCBDecodeVideoFrame = NULL;
   m_pCBEncodeVideoFrame = NULL;
   m_pCBVCConfigToBeLoad = NULL;   
   m_pCBRTSPMetadata     = NULL;
   m_bUSEVMSConnection   = TRUE;
}

VCM_Desc& VCM_Desc::operator=( VCM_Desc& from )
{
   m_bUseVCMConfigFile   = from.m_bUseVCMConfigFile;
   m_strConfigPath       = from.m_strConfigPath;
   m_pCBImageProcess     = from.m_pCBImageProcess;
   m_pCBDisplay          = from.m_pCBDisplay;
   m_pCBRecord           = from.m_pCBRecord;
   m_pCBCamEvent         = from.m_pCBCamEvent;
   m_pCBDecodeVideoFrame = from.m_pCBDecodeVideoFrame;
   m_pCBEncodeVideoFrame = from.m_pCBEncodeVideoFrame;
   m_pCBVCConfigToBeLoad = from.m_pCBVCConfigToBeLoad;   
   m_bUSEVMSConnection   = from.m_bUSEVMSConnection;
   m_pCBRTSPMetadata     = from.m_pCBRTSPMetadata;
   return *this;
}

///////////////////////////////////////////////////////////////////////////
//
// class: VC_Desc
//
///////////////////////////////////////////////////////////////////////////

VC_Desc::VC_Desc()
{
   ZeroMemory( this, sizeof( VC_Desc ) );
}

void VC_Desc::SetVideoChannelDesc( const VC_Desc& from )
{
   m_pInfo     = from.m_pInfo;
   m_pVCM      = from.m_pVCM;
   m_pIPCamMgr = from.m_pIPCamMgr;
   m_pTimerMgr = from.m_pTimerMgr;
#ifdef __SUPPORT_SOLAR_CALCULATOR
   m_pSunCalculator = from.m_pSunCalculator;
#endif
   m_pCBImageProcess     = from.m_pCBImageProcess;
   m_pCBDisplay          = from.m_pCBDisplay;
   m_pCBRecord           = from.m_pCBRecord;
   m_pCBCamEvent         = from.m_pCBCamEvent;
   m_pCBDecodeVideoFrame = from.m_pCBDecodeVideoFrame;
   m_pCBEncodeVideoFrame = from.m_pCBEncodeVideoFrame;
   m_pCBVCConfigToBeLoad = from.m_pCBVCConfigToBeLoad;
   m_pCBRTSPMetadata     = from.m_pCBRTSPMetadata;
}

///////////////////////////////////////////////////////////////////////////
//
// class: PVTInfo
//
///////////////////////////////////////////////////////////////////////////

PVTInfo::PVTInfo()
{
   P_width  = 0;
   P_height = 0;
   S_width  = 0;
   S_height = 0;
   N_images = 0;
}

int PVTInfo::Read( const char* path_name )
{
   char identifier[] = "PVFCT";

   if( !IsExistFile( path_name ) ) return ( 1 );
   FileIO file( path_name, FIO_BINARY, FIO_READ );
   if( !file ) return ( 2 );
   CArray1D buffer( sizeof( identifier ) );
   if( file.Read( (byte*)(char*)buffer, buffer.Length, sizeof( char ) ) ) return ( 3 );
   buffer[buffer.Length - 1] = 0;
   if( strcmp( (char*)buffer, identifier ) ) return ( 4 );
   if( file.Read( (byte*)&ProjectionType, 1, sizeof( int ) ) ) return ( 5 );
   if( file.Read( (byte*)&P_width, 1, sizeof( int ) ) ) return ( 6 );
   if( file.Read( (byte*)&P_height, 1, sizeof( int ) ) ) return ( 7 );
   if( file.Read( (byte*)&N_images, 1, sizeof( int ) ) ) return ( 8 );
   if( file.Read( (byte*)&S_width, 1, sizeof( int ) ) ) return ( 9 );
   if( file.Read( (byte*)&S_height, 1, sizeof( int ) ) ) return ( 10 );
   if( file.Read( (byte*)&HFOV, 1, sizeof( float ) ) ) return ( 11 );
   return ( DONE );
}

CPTZ3DFile __PTZ3DFile;

///////////////////////////////////////////////////////////////////////////
//
// Global Functions
//
///////////////////////////////////////////////////////////////////////////

void InitVCMLib()
{
   InitLive555();
}

void UninitVCMLib()
{
   UninitLive555();
}

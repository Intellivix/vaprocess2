#include "stdafx.h"

#ifdef NDEBUG
#pragma comment(lib,"Live555_RS.lib")
#endif

#ifdef _DEBUG
#pragma comment(lib,"Live555_DS.lib")
#endif

#define __DEBUG

#ifdef __WIN32
void DebugString(TCHAR *pFmt, ...)
{
#ifdef __DEBUG

   TCHAR Message_Buffer[1024] = _T("");

   if(_tcslen(pFmt) < 1024)
   {
      va_list arglist;

      va_start(arglist, pFmt);
      _vstprintf(Message_Buffer, pFmt, arglist);
      va_end(arglist);

      ::OutputDebugString(Message_Buffer);
   }
#endif
}
#endif

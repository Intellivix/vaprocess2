#include "stdafx.h"
#include "DigitalIO.h"
#include "VCM_Define.h"


//////////////////////////////////////////////////////////////
//
// Structure : DigitalOutputInfo
//
//////////////////////////////////////////////////////////////

DigitalInputInfo::DigitalInputInfo()
{
   m_strName      = "";
   m_bNormalClose = FALSE;
}

//////////////////////////////////////////////////////////////
//
// Class : DigitalOutputInfo
//
//////////////////////////////////////////////////////////////

DigitalOutputInfo::DigitalOutputInfo()
{
   m_strName      = "";
   m_nSignalCount = 0;
   m_bNormalClose = FALSE;
}

//////////////////////////////////////////////////////////////
//
// Class : CDigitalIO
//
//////////////////////////////////////////////////////////////

CDigitalIO::CDigitalIO()
{
   m_nInputNum  = 0;
   m_nOutputNum = 0;
}

CDigitalIO::~CDigitalIO()
{
}

CDigitalIO& CDigitalIO::operator=( CDigitalIO& from )
{
   FileIO buff;
   CXMLIO xmlWriteIO( &buff, XMLIO_Write );
   from.Write( &xmlWriteIO );
   buff.GoToStartPos();
   CXMLIO xmlReadIO( &buff, XMLIO_Read );
   this->Read( &xmlReadIO );

   return *this;
}

void CDigitalIO::Initialize( int nInputNum, int nOutputNum )
{
   m_nInputNum  = nInputNum;
   m_nOutputNum = nOutputNum;
}

BOOL CDigitalIO::Read( CXMLIO* pIO )
{
   int i;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "DigitalIO" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "InNum", &m_nInputNum );
      xmlNode.Attribute( TYPE_ID( int ), "OutNum", &m_nOutputNum );
      for( i = 0; i < m_nInputNum; i++ ) {
         CXMLNode xmlNode( pIO );
         if( xmlNode.Start( "Input" ) ) {
            xmlNode.Attribute( TYPE_ID( string ), "Name", &m_nInputs[i].m_strName );
            xmlNode.Attribute( TYPE_ID( BOOL ), "NC", &m_nInputs[i].m_bNormalClose );
            xmlNode.End();
         }
      }
      for( i = 0; i < m_nOutputNum; i++ ) {
         CXMLNode xmlNode( pIO );
         if( xmlNode.Start( "Output" ) ) {
            xmlNode.Attribute( TYPE_ID( string ), "Name", &m_nOutputs[i].m_strName );
            xmlNode.Attribute( TYPE_ID( BOOL ), "NC", &m_nOutputs[i].m_bNormalClose );
            xmlNode.End();
         }
      }
      xmlNode.End();
      return TRUE;
   }
   return FALSE;
}

BOOL CDigitalIO::Write( CXMLIO* pIO )
{
   int i;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "DigitalIO" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "InNum", &m_nInputNum );
      xmlNode.Attribute( TYPE_ID( int ), "OutNum", &m_nOutputNum );
      for( i = 0; i < m_nInputNum; i++ ) {
         CXMLNode xmlNode( pIO );
         if( xmlNode.Start( "Input" ) ) {
            xmlNode.Attribute( TYPE_ID( string ), "Name", &m_nInputs[i].m_strName );
            xmlNode.Attribute( TYPE_ID( BOOL ), "NC", &m_nInputs[i].m_bNormalClose );
            xmlNode.End();
         }
      }
      for( i = 0; i < m_nOutputNum; i++ ) {
         CXMLNode xmlNode( pIO );
         if( xmlNode.Start( "Output" ) ) {
            xmlNode.Attribute( TYPE_ID( string ), "Name", &m_nOutputs[i].m_strName );
            xmlNode.Attribute( TYPE_ID( BOOL ), "NC", &m_nOutputs[i].m_bNormalClose );
            xmlNode.End();
         }
      }
      xmlNode.End();
      return TRUE;
   }
   return FALSE;
}

int CDigitalIO::GetAlarmInputStatus( int nChIdx, BOOL& bSignal )
{
   return DONE;
}

int CDigitalIO::SetRelayOutputStatus( int nChIdx, BOOL bSignal )
{
   if( bSignal )
      m_nOutputs[nChIdx].m_nSignalCount++;
   else
      m_nOutputs[nChIdx].m_nSignalCount--;
   return DONE;
}

int CDigitalIO::GetRelayOutputStatus( int nChIdx, BOOL& bSignal )
{
   if( m_nOutputs[nChIdx].m_nSignalCount == 0 )
      bSignal = FALSE;
   else
      bSignal = TRUE;
   return DONE;
}

int CDigitalIO::SetInputNormalStatus( int nChIdx, BOOL bNormalClose )
{
   m_nInputs[nChIdx].m_bNormalClose = bNormalClose;
   return DONE;
}

int CDigitalIO::GetInputNormalStatus( int nChIdx, BOOL& bNormalClose )
{
   bNormalClose = m_nInputs[nChIdx].m_bNormalClose;
   return DONE;
}

int CDigitalIO::SetInputName( int nChIdx, const std::string& strInputName )
{
   m_nInputs[nChIdx].m_strName = strInputName;
   return DONE;
}

int CDigitalIO::GetInputName( int nChIdx, std::string& strInputName )
{
   strInputName = m_nInputs[nChIdx].m_strName;
   return DONE;
}

int CDigitalIO::SetOutputNormalStatus( int nChIdx, BOOL bNormalClose )
{
   m_nOutputs[nChIdx].m_bNormalClose = bNormalClose;
   return DONE;
}

int CDigitalIO::GetOutputNormalStatus( int nChIdx, BOOL& bNormalClose )
{
   bNormalClose = m_nOutputs[nChIdx].m_bNormalClose;
   return DONE;
}

int CDigitalIO::SetOutputName( int nChIdx, const std::string& strOutputName )
{
   m_nOutputs[nChIdx].m_strName = strOutputName;
   return DONE;
}

int CDigitalIO::GetOutputName( int nChIdx, std::string& strOutputName )
{
   strOutputName = m_nOutputs[nChIdx].m_strName;
   return DONE;
}

#pragma once

#ifdef __SUPPORT_PTZ_CAMERA

#include "PTZCtrl.h"
#include "CameraInfo.h"

///////////////////////////////////////////////////////////////////////////////
//
// Class: CPTZCtrl_Linking
//
///////////////////////////////////////////////////////////////////////////////

enum PTZLinkingState
{
   PTZLinkingState_SearchingSourceVideoChannel = 0x00000001,
   PTZLinkingState_SourceVideoChannelRegisterd = 0x00000002,
   PTZLinkingState_SourceVideoChannelRemoved   = 0x00000004,
};


class CPTZCtrl_Linking : public CPTZCtrl
#ifdef __SUPPORT_MASTER_SLAVE_BASED_AUTONOMOUS_PTZ_TRACKING
   , public MasterToSlaveCamera
#endif
{
protected:
   int                    m_nPTZLinkingState;
   uint                   m_nPrevCheckTime;
   CVideoChannel*         m_pVCSrc;
   CIPCameraInfo*         m_pIPCamInfo;
   CPTZCtrlInfo*          m_pPTZCtrlInfo;

   CCriticalSection       m_csContPTZMove;

public:
   CVideoChannelManager*  m_pVCM;

public:
   CPTZCtrl_Linking (int nPTZControlType, ISize2D s2VideoSize);
   ~CPTZCtrl_Linking (    );
public:
   virtual void Close         (   );
   virtual void SignalToClose (   );
   virtual int  Open          (CIPCameraInfo* pIPCamInfo);

public:
   int  LinkingPTZCtrlProc();

public:
   virtual int   _SetPreset   (int preset_no) { return (DONE); }
   virtual int   _ClearPreset (int preset_no) { return (DONE); }
   virtual int   _GotoPreset  (int preset_no) { return (DONE); }

};

#endif // __SUPPORT_PTZ_CAMERA


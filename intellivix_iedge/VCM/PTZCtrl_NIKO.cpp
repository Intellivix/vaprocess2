﻿#include "stdafx.h"
#include "PTZCtrl_NIKO.h"
#include "CameraProductAndItsProperties.h"


extern MapF2F g_ZoomReal2HW_DY_IR2036HL_WONWOO[];
extern float g_PanAngleVelTable_DY_IR2036HL_WONWOO[][65];
extern float g_TiltAngleVelTable_DY_IR2036HL_WONWOO[][65];
extern float g_PanAngleSpeedCheckZoomTable_DY_IR2036HL_WONWOO[];

extern MapF2F g_ZoomReal2HW_FS_IR307H [];
extern MapF2F g_PanAngleVelTable_FS_IR307H [];
extern MapF2F g_TiltAngleVelTable_FS_IR307H [];
///////////////////////////////////////////////////////////////////////////////
//
// Enumerations
//
///////////////////////////////////////////////////////////////////////////////

enum NICO_MENU_CMD
{
   NICO_MENU_CMD_HIDE  = 0,
   NICO_MENU_CMD_SHOW  = 1,
   NICO_MENU_CMD_UP    = 2,
   NICO_MENU_CMD_DOWN  = 3,
   NICO_MENU_CMD_LEFT  = 4,
   NICO_MENU_CMD_RIGHT = 5,
   NICO_MENU_CMD_STOP  = 6,
};

enum ProtocolType_Nico
{
   ProtocolType_NICO_Origin, 
   ProtocolType_NICO_Dongyang,
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: CPTZCtrl_NIKO
//
///////////////////////////////////////////////////////////////////////////////

CPTZCtrl_NIKO::CPTZCtrl_NIKO  (int nPTZCtontrolType)
{
   m_ProtocolType_NICO = ProtocolType_NICO_Origin;
   m_nPTZControlType = nPTZCtontrolType;
   m_pPTZCtrlInfo = NULL;

   // speed
   MaxPanSpeed = 255;
   MaxTiltSpeed = 255;
   MaxZoomSpeed = 1;

   // Pan, Tilt, Zoom Range
   MinPanAngle = 0.0f;
   MaxPanAngle = 360.0f;
   MinTiltAngle = 0.0f;
   MaxTiltAngle = 90.0f;
   MinZoomFactor = 1.0f;
   MaxZoomFactor = 16.0f;

   MinPresetNo = 1;
   MaxPresetNo = 128;

   m_nSendCmdPeriod = 95;

   m_nPTZCmdExecDelay = 200;
   m_nPrevZoomSpeed = -1;

   m_PrevFocusDir = 0;

   m_byRecvHeader = 0xA7;

   switch (m_nPTZControlType) {

   case PTZControlType_FSNetworks_FS_IR307H:
   {
      m_ProtocolType_NICO = ProtocolType_NICO_Dongyang;
      MaxPanSpeed = 64;
      MaxTiltSpeed = 64;
      MaxZoomSpeed = 7;
      MaxZoomFactor = 36.0f;

      m_fZoomLogVel_ContSpd1 = 0.5f;
      m_fZoomLogVel_AbsPosMove = 1.31f;
      m_fFocalLength = 300.0f;
      m_fFocalLengthRatio = 0.750f;
      m_nSendCmdPeriod = 70;
      
      SetMapTable(MapF2FType_PanAngleVel2HWSpd, g_PanAngleVelTable_FS_IR307H, 66, INTERPOLATION_MODE_STAIR);
      SetMapTable(MapF2FType_TiltAngleVel2HWSpd, g_TiltAngleVelTable_FS_IR307H, 66, INTERPOLATION_MODE_STAIR);
      SetMapTable (MapF2FType_ZoomReal2HW, g_ZoomReal2HW_FS_IR307H , 21, INTERPOLATION_MODE_LINEAR);

      m_nPanMaxHw = 0x7D00;
      m_nTiltMaxHw = 0x1C4C;
   }
   break;

   }
   ChangeZoomSpeed(4);

   m_nPrevZoomCmdTime = GetTickCount();

}

int CPTZCtrl_NIKO::Open (CPTZCtrlInfo* pPTZCtrlInfo)
{
   m_pPTZCtrlInfo = pPTZCtrlInfo;

   if (CPTZCtrl::Open (pPTZCtrlInfo))
      return (1);

   return (DONE);
}

void CPTZCtrl_NIKO::Clear (BYTE* pCmd, int nCmdLen)
{
   int i;
   for (i = 1; i <= nCmdLen; i++) pCmd[i] = 0;
   switch (m_ProtocolType_NICO)
   {
   case ProtocolType_NICO_Origin:
      pCmd[0] = 0xAA;
      break;
   case ProtocolType_NICO_Dongyang:
      pCmd[0] = 0xA7;
      break;
   }
      
   pCmd[6] = m_nCamAddr;
}

void CPTZCtrl_NIKO::CheckSum (BYTE* pCmd, int nCmdLen)

{
   int i;
   BYTE checksum = 0;
   int ei = nCmdLen - 2;
   for (i = 0; i <= ei; i++) checksum += pCmd[i];
   pCmd[ei + 1] = checksum;
}

int CPTZCtrl_NIKO::_ContPTZMove (IPTZVector move_vec)
{
   BOOL bDebug = FALSE;

   BYTE* pCmd = m_sCmd;
   m_nSendCmdLen = 0;
   //logd ("P:%3d, T:%3d, Z:%3d\n", move_vec.P, move_vec.T, move_vec.Z);

   ///////////////////
   // 
   ///////////////////
	if (abs (move_vec.Z) != m_nPrevZoomSpeed) {
		if (move_vec.Z) {
			//BCCL:Print ("ChangeZoomSpeed  %d %d\n", abs (m_PrevPTZMoveVec.Z), abs (move_vec.Z));
			ChangeZoomSpeed (abs (move_vec.Z));
         //Sleep (m_nSendCmdPeriod);
		}
      m_nPrevZoomSpeed = abs (move_vec.Z);
	}

   if (m_nState & PTZ_State_ShowMenu) {
      ////////////////
      // Menu 
      ////////////////
      Clear (pCmd, 8);      
      switch (m_ProtocolType_NICO)
      {
      case ProtocolType_NICO_Origin:
         pCmd[1] = 0xB0;
         break;
      case ProtocolType_NICO_Dongyang:
         pCmd[1] = 0x64;
         break;
      }
      
      if      (move_vec.P <  0)  pCmd[2] = NICO_MENU_CMD_LEFT;
      else if (move_vec.P >  0)  pCmd[2] = NICO_MENU_CMD_RIGHT;
      if      (move_vec.T <  0)  pCmd[2] = NICO_MENU_CMD_UP;
      else if (move_vec.T >  0)  pCmd[2] = NICO_MENU_CMD_DOWN;
      if (move_vec.P == 0 && move_vec.T == 0) {
         return (DONE);
      }
      CheckSum (pCmd, 8);
      m_nSendCmdLen += 8;
      pCmd += 8;
      //Sleep (200);
   }
   else {
      if (bDebug) 
         logd ("P:%3d, T:%3d, Z:%3d : ", move_vec.P, move_vec.T, move_vec.Z);
      ///////////////
      // PTZ 
      ///////////////
      byte cmd_left;
      byte cmd_up;
      byte cmd_right;
      byte cmd_down;

      switch (m_ProtocolType_NICO)
      {
      case ProtocolType_NICO_Origin:
         cmd_left  = 0x97;
         cmd_up    = 0x9B;
         cmd_right = 0x99;
         cmd_down  = 0x9D;
         break;
      case ProtocolType_NICO_Dongyang:
         cmd_left  = 0x10;
         cmd_up    = 0x12;
         cmd_right = 0x11;
         cmd_down  = 0x13;
         break;
      }

      if ((move_vec.P != m_PrevPTZMoveVec.P) && (move_vec.P == 0) && 
         (move_vec.T != m_PrevPTZMoveVec.T) && (move_vec.T == 0)) 
      {
         Clear (pCmd, 8);         
         switch(m_ProtocolType_NICO)
         {
         case ProtocolType_NICO_Origin:
            pCmd[1] = 0xE5;
            pCmd[2] = 0x00;
            break;
         case ProtocolType_NICO_Dongyang:
            pCmd[1] = 0x14;
            pCmd[2] = 0x00;
            break;
         }         
         CheckSum (pCmd, 8);
         m_nSendCmdLen += 8;
         pCmd += 8;
      }
      else {
         if (!(m_nCapabilities & PTZCtrlCap_ZoomProportionalJog) || m_PTZInfo.m_bZoomProportionalJog) {
            if (move_vec.P != m_PrevPTZMoveVec.P) {
               Clear (pCmd, 8);
               if      (move_vec.P <  0)  pCmd[1] = cmd_left ;
               else if (move_vec.P >  0)  pCmd[1] = cmd_right;
               else  {
                  switch(m_ProtocolType_NICO) {
                  case ProtocolType_NICO_Origin:
                     pCmd[1] = 0xE5;
                     pCmd[2] = 0x01;
                     break;
                  case ProtocolType_NICO_Dongyang:
                     pCmd[1] = 0x14;
                     pCmd[2] = 0x01;
                     break;
                  }
               }
               if (move_vec.P) {
                  switch(m_ProtocolType_NICO) {
                  case ProtocolType_NICO_Origin:
                     pCmd[2] = abs (move_vec.P);
                     break;
                  case ProtocolType_NICO_Dongyang:
                     if (abs (move_vec.P) > 0)
                        pCmd[2] = (abs (move_vec.P)-1)*4+1;
                     break;
                  }
               }
               CheckSum (pCmd, 8);
               m_nSendCmdLen += 8;
               pCmd += 8;
               if (bDebug) logd ("P, ");

            }
            if (move_vec.T != m_PrevPTZMoveVec.T) {
               Clear (pCmd, 8);
               if      (move_vec.T <  0)  pCmd[1] = cmd_up ;
               else if (move_vec.T >  0)  pCmd[1] = cmd_down;
               else  {
                  switch(m_ProtocolType_NICO) {
                  case ProtocolType_NICO_Origin:
                     pCmd[1] = 0xE5;
                     pCmd[2] = 0x02;
                     break;
                  case ProtocolType_NICO_Dongyang:
                     pCmd[1] = 0x14;
                     pCmd[2] = 0x02;
                     break;
                  }

               }
               if (move_vec.T) {
                  switch(m_ProtocolType_NICO) {
                  case ProtocolType_NICO_Origin:
                     pCmd[2] = abs (move_vec.T);
                     break;
                  case ProtocolType_NICO_Dongyang:
                     if (abs (move_vec.T) > 0)
                        pCmd[2] = (abs (move_vec.T)-1)*4+1;
                     break;
                  }
               }

               CheckSum (pCmd, 8);
               m_nSendCmdLen += 8;
               pCmd += 8;
               if (bDebug) logd ("T, ");
            }
         }
         else {            
            int pan_speed = (int)((float)move_vec.P / MaxPanSpeed * 255);
            int tilt_speed = (int)((float)move_vec.T / MaxTiltSpeed * 255);

            //logd("pan:%d, tilt:%d\n", pan_speed, tilt_speed);

            Clear (pCmd, 8);
            pCmd[1] = 0x1F;            
            if (move_vec.P > 0)      pCmd[2] = 0x02; // Pan Right
            else if (move_vec.P < 0) pCmd[2] = 0x01; //     Left
            else                     pCmd[2] = 0x00; //     Stop
            pCmd[3] = abs(pan_speed);                // Pan Speed            
            if (move_vec.T > 0)      pCmd[4] = 0x02; // Tilt Up
            else if (move_vec.T < 0) pCmd[4] = 0x01; //      Down
            else                     pCmd[4] = 0x00; //      Stop
            pCmd[5] = abs(tilt_speed);               // Tilt Speed
            CheckSum (pCmd, 8);
            m_nSendCmdLen += 8;
            pCmd += 8;
         }
      }
      
      if (1 /*!(m_nState & PTZ_State_ContPTZTracking)*/) {
         if (move_vec.Z != m_PrevPTZMoveVec.Z) {
            m_nZoomCmdSendCnt = 2;
         }
         if (m_nZoomCmdSendCnt > 0) {
            Clear (pCmd, 8);
            BYTE nCmd1, nCmdZOut, nCmdZIn, nCmdStop;
            if (ProtocolType_NICO_Origin   == m_ProtocolType_NICO) { nCmd1 = 0x5F; nCmdZOut = 0x04; nCmdZIn = 0x02; nCmdStop = 0x0C; }
            if (ProtocolType_NICO_Dongyang == m_ProtocolType_NICO) { nCmd1 = 0x72; nCmdZOut = 0x11; nCmdZIn = 0x10; nCmdStop = 0x12; }
            pCmd[1] = nCmd1;
            if      (move_vec.Z <  0)  pCmd[2] = nCmdZOut;
            else if (move_vec.Z >  0)  pCmd[2] = nCmdZIn;
            else                       pCmd[2] = nCmdStop;
            CheckSum (pCmd, 8);
            m_nSendCmdLen += 8;
            pCmd += 8;
            m_nZoomCmdSendCnt--;
         }

         if (move_vec.F != m_PrevFocusDir) {
            Clear (pCmd, 8);
            BYTE nCmd1, nCmdNear, nCmdFar, nCmdStop;
            if (ProtocolType_NICO_Origin   == m_ProtocolType_NICO) { nCmd1 = 0x5F; nCmdNear = 0x39; nCmdFar = 0x38; nCmdStop = 0x0C; }
            if (ProtocolType_NICO_Dongyang == m_ProtocolType_NICO) { nCmd1 = 0x72; nCmdNear = 0x13; nCmdFar = 0x14; nCmdStop = 0x12; }
            pCmd[1] = nCmd1;
            if      (move_vec.F <  0)  pCmd[2] = nCmdNear;
            else if (move_vec.F >  0)  pCmd[2] = nCmdFar;
            else                       pCmd[2] = nCmdStop;
            CheckSum (pCmd, 8);
            m_nSendCmdLen += 8;
            pCmd += 8;
         }
      }
      else {
         float fTarZoom = m_TgtPTZPos.Z;
         if (m_pMainPTZControl)
            fTarZoom = m_pMainPTZControl->GetTgtAbsPos().Z;

         uint32_t nSendZoomCmdPeriod = 1000;
         if (GetTickCount () - m_nPrevZoomCmdTime > nSendZoomCmdPeriod) {
            if (fabs (log2 (m_PrevTgtPTZPos.Z)- log2 (fTarZoom)) > 0.22f) 
            {
               int zf_hw = (int)GetZoomHW (fTarZoom);
               Clear (pCmd, 8);
               if (ProtocolType_NICO_Origin   == m_ProtocolType_NICO) pCmd[1] = 0x9E;
               if (ProtocolType_NICO_Dongyang == m_ProtocolType_NICO) pCmd[1] = 0x77;
               pCmd[2] = (BYTE)(zf_hw >> 8);
               pCmd[3] = (BYTE)(zf_hw & 0xFF);
               CheckSum (pCmd, 8);
               m_nSendCmdLen += 8;
               SetAbsPos (FPTZVector (PREV_CMD, PREV_CMD, fTarZoom));
               m_PrevTgtPTZPos.Z = fTarZoom;
               m_nPrevZoomCmdTime = GetTickCount ();
            }
         }
      }
   }
   if (bDebug)  logd ("\n");
   m_PrevPTZMoveVec.P = move_vec.P;
   m_PrevPTZMoveVec.T = move_vec.T;
   m_PrevPTZMoveVec.Z = move_vec.Z;
   m_PrevFocusDir = float (move_vec.F);
   return SendCommand ();
}


int CPTZCtrl_NIKO::_GotoAbsPos (FPTZVector abs_pos)
{
   if (DONE != CPTZCtrl::_GotoAbsPos (abs_pos))
      return (1);

   logd("[CH:%d][Cellinx][GotoAbsPos P:%f, T:%f, Z:%f]\n", m_nChannelNo_IVX, abs_pos.P, abs_pos.T, abs_pos.Z);
   BYTE* pCmd = m_sCmd;
   m_nSendCmdLen = 0;

   float pa = abs_pos.P;
   float ta = abs_pos.T;
   float zf = abs_pos.Z;

   if (NO_CMD != pa) {
      if (fabs (pa - m_PrevTgtPTZPos.P) > 0.05f || fabs (ta - m_PrevTgtPTZPos.T) > 0.05f) 
      {
         int pa_hw = (int)((360.0f - pa) / MaxPanAngle * m_nPanMaxHw);
         int ta_hw = (int)(ta / MaxTiltAngle * m_nTiltMaxHw);
         Clear (pCmd, 8);
         if (ProtocolType_NICO_Origin   == m_ProtocolType_NICO) pCmd[1] = 0x95;
         if (ProtocolType_NICO_Dongyang == m_ProtocolType_NICO) pCmd[1] = 0x16;
         pCmd[2] = (BYTE)(pa_hw >> 8);
         pCmd[3] = (BYTE)(pa_hw & 0xFF);
         pCmd[4] = (BYTE)(ta_hw >> 8);
         pCmd[5] = (BYTE)(ta_hw & 0xFF);
         CheckSum (pCmd, 8);
         m_nSendCmdLen += 8;
         pCmd += 8;
      }   }
   
   if (fabs (zf - m_PrevTgtPTZPos.Z) > 0.05f) 
   {
      int zf_hw = (int)GetZoomHW (zf);
      Clear (pCmd, 8);
      if (ProtocolType_NICO_Origin   == m_ProtocolType_NICO) pCmd[1] = 0x9E;
      if (ProtocolType_NICO_Dongyang == m_ProtocolType_NICO) pCmd[1] = 0x77;
      pCmd[2] = (BYTE)(zf_hw >> 8);
      pCmd[3] = (BYTE)(zf_hw & 0xFF);
      CheckSum (pCmd, 8);
      m_nSendCmdLen += 8;
   }

   m_PrevTgtPTZPos = abs_pos;

   return (SendCommand ());
}


int CPTZCtrl_NIKO::_GetAbsPosReqCmd (BYTE* pCmd, int& nCmdLen)
{
   int _nCmdLen = 0;
   nCmdLen = 0;
   BOOL bReqZoom = FALSE;
   BOOL bReqPanTilt = FALSE;
   if (m_ProtocolType_NICO == ProtocolType_NICO_Dongyang) {
      if (m_nReqAbsPosCnt % 2 == 0) bReqZoom = TRUE;
      bReqPanTilt = TRUE;
   }
   if (m_ProtocolType_NICO == ProtocolType_NICO_Origin) {
      if (m_nState & PTZ_State_ContPTZTracking) {
         bReqPanTilt = TRUE;
      }
      else {
         if (m_nReqAbsPosCnt % 10 != 0) bReqPanTilt = TRUE;
         else                           bReqZoom    = TRUE;
      }
   }
   if (bReqZoom) 
   {
      // Req Zoom
      Clear (pCmd, 8);
      if (ProtocolType_NICO_Origin   == m_ProtocolType_NICO) pCmd[1] = 0x9F;
      if (ProtocolType_NICO_Dongyang == m_ProtocolType_NICO) pCmd[1] = 0x78;
      CheckSum (pCmd, 8);
      _nCmdLen += 8;
      pCmd += 8;
   }
   if (bReqPanTilt) 
   {
      // Req PanTilt
      Clear (pCmd, 8);
      if (ProtocolType_NICO_Origin   == m_ProtocolType_NICO) pCmd[1] = 0x93;
      if (ProtocolType_NICO_Dongyang == m_ProtocolType_NICO) pCmd[1] = 0x17;
      CheckSum (pCmd, 8);
      _nCmdLen += 8;
   }
   m_nReqAbsPosCnt++;

   nCmdLen += _nCmdLen;
   //logi("_GetAbsPosReqCmd :: CPTZCtrl_NIKO");
   return (DONE);
}

#define ZOOM_RECV       1
#define PAN_TILT_RECV   2

int CPTZCtrl_NIKO::_PopCommand (   )
{
   int nPopCmdLen = 0;
   BYTE* pCmd = m_ResCmd;

   if (m_nRecvCmdCnt >= 8) 
   {
      FPTZVector pvCurPos (PREV_CMD, PREV_CMD, PREV_CMD, PREV_CMD, PREV_CMD);
      int StatusFlag = 0;

      int i;
      BYTE nCheckSum = 0;
      for (i = 0; i < 7; i++) nCheckSum += pCmd[i];
      if (nCheckSum != pCmd[7]) 
         return m_nRecvCmdCnt;

      byte nCmd1_Z  = 0x9F;
      byte nCmd1_PT = 0x93;
      if (m_ProtocolType_NICO == ProtocolType_NICO_Dongyang) {
         nCmd1_Z  = 0x78;
         nCmd1_PT = 0x17;
      }

      if(nCmd1_Z == pCmd[1])
         StatusFlag = ZOOM_RECV;
      else if (nCmd1_PT == pCmd [1])
         StatusFlag = PAN_TILT_RECV;

      // Zoom
      if (StatusFlag == ZOOM_RECV)
      {
         int zf_hw;
         zf_hw = pCmd[2] << 8 | pCmd[3];
         pvCurPos.Z = GetZoomReal (float (zf_hw));
         nPopCmdLen = 8;
         SetAbsPos (pvCurPos);
         //logd ("CPTZCtrl_NIKO::_PopCommand  zf_hw:%d\n", zf_hw);
      }
      // Pan Tilt
      else if (StatusFlag == PAN_TILT_RECV) 
      {
         int pa_hw, ta_hw;
         pa_hw = pCmd[2] << 8 | pCmd[3];
         pvCurPos.P = float (m_nPanMaxHw - pa_hw) / float (m_nPanMaxHw) * MaxPanAngle;
         ta_hw = pCmd[4] << 8 | pCmd[5];
         pvCurPos.T = float (ta_hw) / float (m_nTiltMaxHw) * MaxTiltAngle;
         nPopCmdLen = 8;
         SetAbsPos (pvCurPos);
         SignalPanTiltPosReceived (   );
         logv ("CPTZCtrl_NIKO::_PopCommand  (P:%6.1f,T:%6.1f,Z:%6.1f)\n", pvCurPos.P, pvCurPos.T, pvCurPos.Z);
      }
      m_PrevTgtPTZPos = pvCurPos;
   }
   return nPopCmdLen;
}

int CPTZCtrl_NIKO::_MenuOpen (   )
{
   Clear (m_sCmd, 8);
   if (ProtocolType_NICO_Origin   == m_ProtocolType_NICO) m_sCmd[1] = 0xB0;
   if (ProtocolType_NICO_Dongyang == m_ProtocolType_NICO) m_sCmd[1] = 0x64;
   m_sCmd[2]  = NICO_MENU_CMD_SHOW; 
   m_nState |= PTZ_State_ShowMenu;
   CheckSum (m_sCmd, 8);
   m_nSendCmdLen = 8;
   return (SendCommand ());
}

int CPTZCtrl_NIKO::_MenuCancel (   )
{
   Clear (m_sCmd, 8);
   if (ProtocolType_NICO_Origin   == m_ProtocolType_NICO) m_sCmd[1] = 0xB0;
   if (ProtocolType_NICO_Dongyang == m_ProtocolType_NICO) m_sCmd[1] = 0x64;
   m_sCmd[2]  = NICO_MENU_CMD_HIDE;
   m_nState &= ~PTZ_State_ShowMenu;
   CheckSum (m_sCmd, 8);
   m_nSendCmdLen = 8;
   return (SendCommand ());
}

int CPTZCtrl_NIKO::_SetPreset (int preset_no) 
{
   Clear (m_sCmd, 8);
   if (ProtocolType_NICO_Origin   == m_ProtocolType_NICO) m_sCmd[1] = 0xD5;
   if (ProtocolType_NICO_Dongyang == m_ProtocolType_NICO) m_sCmd[1] = 0x24;
   m_sCmd[2] = (BYTE)preset_no;
   CheckSum (m_sCmd, 8);
   m_nSendCmdLen = 8;
   return (SendCommand ());
}


int CPTZCtrl_NIKO::_ClearPreset (int preset_no) 
{
   Clear (m_sCmd, 8);
   if (ProtocolType_NICO_Origin   == m_ProtocolType_NICO) m_sCmd[1] = 0xD6;
   if (ProtocolType_NICO_Dongyang == m_ProtocolType_NICO) m_sCmd[1] = 0x27;
   m_sCmd[2] = (BYTE)preset_no;
   CheckSum (m_sCmd, 8);
   m_nSendCmdLen = 8;
   return (SendCommand ());
}


int CPTZCtrl_NIKO::_GotoPreset (int preset_no) 
{
   Clear (m_sCmd, 8);
   if (ProtocolType_NICO_Origin   == m_ProtocolType_NICO) m_sCmd[1] = 0xD7;
   if (ProtocolType_NICO_Dongyang == m_ProtocolType_NICO) m_sCmd[1] = 0x25;
   m_sCmd[2] = (BYTE)preset_no;
   CheckSum (m_sCmd, 8);
   m_nSendCmdLen = 8;
   return (SendCommand ());
}


int CPTZCtrl_NIKO::_AuxOn(int nAuxNo)
{
   Clear (m_sCmd, 8);
   if (ProtocolType_NICO_Origin   == m_ProtocolType_NICO) m_sCmd[1] = 0xFE;
   if (ProtocolType_NICO_Dongyang == m_ProtocolType_NICO)
   {
      m_sCmd[1] = 0x67;
      m_sCmd[2] = (byte)nAuxNo;   
      m_sCmd[3] = 0x01;
   }
      
   CheckSum (m_sCmd, 8);
   m_nSendCmdLen = 8;
   return (SendCommand ());
}


int CPTZCtrl_NIKO::_AuxOff(int nAuxNo)
{
   Clear (m_sCmd, 8);
   if (ProtocolType_NICO_Origin   == m_ProtocolType_NICO) m_sCmd[1] = 0xFF;
   if (ProtocolType_NICO_Dongyang == m_ProtocolType_NICO)
   {
      m_sCmd[1] = 0x67;
      m_sCmd[2] = (byte)nAuxNo;   
      m_sCmd[3] = 0x00;
   }
   
   CheckSum (m_sCmd, 8);
   m_nSendCmdLen = 8;
   return (SendCommand ());
}

int CPTZCtrl_NIKO::DeviceControl (int nType, int nCode)
{
   return (DONE);
}

int CPTZCtrl_NIKO::_SetWiper (BOOL bWiperOn)
{
   if (bWiperOn) _AuxOn  (4);
   else          _AuxOff (4);
   //Sleep_ms(300);

   if (bWiperOn) DeviceControl (4, 1);
   else          DeviceControl (4, 0);

   return (DONE);
}

int CPTZCtrl_NIKO::_SetHeatingWire (BOOL bHeatingWire)
{
   if (bHeatingWire) _AuxOn  (5);
   else              _AuxOff (5);
   //Sleep_ms(300);

   if (bHeatingWire) DeviceControl (5, 1);
   else              DeviceControl (5, 0);

   return (DONE);
}

int CPTZCtrl_NIKO::_SetFan (BOOL bFanOn)    
{
   Clear (m_sCmd, 8);
   m_sCmd[1] = 0x6B;
   m_sCmd[2] = (BYTE)bFanOn;
   CheckSum (m_sCmd, 8);
   m_nSendCmdLen = 8;
   return (SendCommand ());
}

int CPTZCtrl_NIKO::GetWaitingTime (float pd)
{
   return m_nSendCmdPeriod;
}

int CPTZCtrl_NIKO::_SetIRMode (int nPTZIRCtrlMode) 
{

   return (DONE);
}

int CPTZCtrl_NIKO::_SetIRLight (int nLight) 
{
  
   return (0);
}

int CPTZCtrl_NIKO::ChangeZoomSpeed (int nZoomSpeed) 
{
   Clear (m_sCmd, 8);
   m_sCmd[1] = 0x89; 
   m_sCmd[2] = 0x20;
   m_sCmd[3] = 0x00;
   m_sCmd[4] = nZoomSpeed;

   CheckSum (m_sCmd, 8);
   m_nSendCmdLen = 8;
   SendCommand ();

   return (DONE);
}

int CPTZCtrl_NIKO::_PosInit ()
{
   Clear (m_sCmd, 8);
   m_sCmd[1] = 0x1D; 
   m_sCmd[2] = 0x00;
   m_sCmd[3] = 0x00;
   m_sCmd[4] = 0x00;
   CheckSum (m_sCmd, 8);
   m_nSendCmdLen = 8;
   SendCommand ();
   
   return (DONE);
}


///////////////////////////////////////////////////////////////////////////////
//
// FS Networks IR-307H
//
///////////////////////////////////////////////////////////////////////////////
MapF2F g_ZoomReal2HW_FS_IR307H [21] =
{
   { 1.00f,       0.0f },
   { 1.19f,    1600.0f },
   { 1.41f,    3300.0f },
   { 1.68f,    4800.0f },
   { 2.00f,    5900.0f },
   { 2.38f,    7200.0f },
   { 2.83f,    8100.0f },
   { 3.36f,    9200.0f },
   { 4.00f,   10000.0f },
   { 4.75f,   10900.0f },
   { 5.65f,   11300.0f },
   { 6.72f,   12000.0f },
   { 8.00f,   12700.0f },
   { 9.51f,   13200.0f },
   { 11.31f,  13850.0f },
   { 13.45f,  14550.0f },
   { 16.00f,  15020.0f },
   { 19.03f,  15650.0f },
   { 22.63f,  16050.0f },
   { 28.00f,  16300.0f },
   { 36.00f,  16384.0f },
};
float g_PanAngleSpeedCheckZoomTable_FS_IR307H[21] =
{
   1.00f, 1.19f, 1.41f, 1.68f, 2.00f, 2.38f, 2.83f, 3.36f, 4.00f, 4.75f, 5.65f, 6.72f, 8.00f, 9.51f, 11.31f, 13.45f, 16.00f, 19.03f, 22.63f, 28.00f, 36.00f
};

MapF2F g_PanAngleVelTable_FS_IR307H [] =
{
   { 0.00f,   0.0f },
   { 0.19f,   1.0f },
   { 0.21f,   2.0f },
   { 0.22f,   3.0f },
   { 0.25f,   4.0f },
   { 0.28f,   5.0f },
   { 0.31f,   6.0f },
   { 0.37f,   7.0f },
   { 0.42f,   8.0f },
   { 0.46f,   9.0f },
   { 0.56f,  10.0f },
   { 0.65f,  11.0f },
   { 0.74f,  12.0f },
   { 0.90f,  13.0f },
   { 1.06f,  14.0f },
   { 1.34f,  15.0f },
   { 1.69f,  16.0f },
   { 2.27f,  17.0f },
   { 2.82f,  18.0f },
   { 3.41f,  19.0f },
   { 4.67f,  20.0f },
   { 6.71f,  21.0f },
   { 8.19f,  22.0f },
   { 10.42f, 23.0f },
   { 12.27f, 24.0f },
   { 14.16f, 25.0f },
   { 16.23f, 26.0f },
   { 18.15f, 27.0f },
   { 20.46f, 28.0f },
   { 22.50f, 29.0f },
   { 24.64f, 30.0f },
   { 27.97f, 31.0f },
   { 30.51f, 32.0f },
   { 32.14f, 33.0f },
   { 35.04f, 34.0f },
   { 38.34f, 35.0f },
   { 40.89f, 36.0f },
   { 43.27f, 37.0f },
   { 45.61f, 38.0f },
   { 48.21f, 39.0f },
   { 51.14f, 40.0f },
   { 53.57f, 41.0f },
   { 55.17f, 42.0f },
   { 58.19f, 43.0f },
   { 60.26f, 44.0f },
   { 63.68f, 45.0f },
   { 66.04f, 46.0f },
   { 70.09f, 47.0f },
   { 72.93f, 48.0f },
   { 76.71f, 49.0f },
   { 80.59f, 50.0f },
   { 82.09f, 51.0f },
   { 84.10f, 52.0f },
   { 91.22f, 53.0f },
   { 94.01f, 54.0f },
   { 96.43f, 55.0f },
   { 99.25f, 56.0f },
   { 102.22f, 57.0f },
   { 105.41f, 58.0f },
   { 112.45f, 59.0f },
   { 116.13f, 60.0f },
   { 120.48f, 61.0f },
   { 124.94f, 62.0f },
   { 124.95f, 63.0f },
   { 129.68f, 64.0f },
};


MapF2F g_TiltAngleVelTable_FS_IR307H[] =
{
   { 0.00f  ,   0.0f },
   { 0.19f  ,   1.0f },
   { 0.21f  ,   2.0f },
   { 0.24f  ,   3.0f },
   { 0.27f  ,   4.0f },
   { 0.31f  ,   5.0f },
   { 0.34f  ,   6.0f },
   { 0.41f  ,   7.0f },
   { 0.47f  ,   8.0f },
   { 0.51f  ,   9.0f },
   { 0.61f  ,  10.0f },
   { 0.72f  ,  11.0f },
   { 0.82f  ,  12.0f },
   { 1.00f  ,  13.0f },
   { 1.17f  ,  14.0f },
   { 1.41f  ,  15.0f },
   { 1.59f  ,  16.0f },
   { 1.73f  ,  17.0f },
   { 1.85f  ,  18.0f },
   { 2.03f  ,  19.0f },
   { 2.28f  ,  20.0f },
   { 2.51f  ,  21.0f },
   { 2.85f  ,  22.0f },
   { 3.16f  ,  23.0f },
   { 3.59f  ,  24.0f },
   { 4.01f  ,  25.0f },
   { 4.71f  ,  26.0f },
   { 5.25f  ,  27.0f },
   { 6.03f  ,  28.0f },
   { 6.43f  ,  29.0f },
   { 6.87f  ,  30.0f },
   { 7.58f  ,  31.0f },
   { 8.15f  ,  32.0f },
   { 9.02f  ,  33.0f },
   { 9.70f  ,  34.0f },
   { 10.57f ,  35.0f },
   { 11.31f ,  36.0f },
   { 12.02f ,  37.0f },
   { 12.78f ,  38.0f },
   { 13.38f ,  39.0f },
   { 14.05f ,  40.0f },
   { 14.94f ,  41.0f },
   { 15.72f ,  42.0f },
   { 16.62f ,  43.0f },
   { 17.63f ,  44.0f },
   { 18.85f ,  45.0f },
   { 20.71f ,  46.0f },
   { 21.87f ,  47.0f },
   { 23.05f ,  48.0f },
   { 24.57f ,  49.0f },
   { 25.59f ,  50.0f },
   { 26.95f ,  51.0f },
   { 27.85f ,  52.0f },
   { 29.33f ,  53.0f },
   { 31.17f ,  54.0f },
   { 32.24f ,  55.0f },
   { 33.79f ,  56.0f },
   { 35.20f , 	57.0f },
   { 36.80f , 	58.0f },
   { 37.68f , 	59.0f },
   { 38.97f , 	60.0f },
   { 40.76f , 	61.0f },
   { 43.15f , 	62.0f },
   { 44.64f , 	63.0f },
   { 45.87f , 	64.0f },
};

MapF2F g_ZoomReal2HW_DY_IR2036HL_WONWOO[21] =
{
   { 1.00f,       0.0f },
   { 1.19f,    1600.0f },
   { 1.41f,    3300.0f },
   { 1.68f,    4800.0f },
   { 2.00f,    5900.0f },
   { 2.38f,    7200.0f },
   { 2.83f,    8100.0f },
   { 3.36f,    9200.0f },
   { 4.00f,   10000.0f },
   { 4.75f,   10900.0f },
   { 5.65f,   11300.0f },
   { 6.72f,   12000.0f },
   { 8.00f,   12700.0f },
   { 9.51f,   13200.0f },
   { 11.31f,  13850.0f },
   { 13.45f,  14550.0f },
   { 16.00f,  15020.0f },
   { 19.03f,  15650.0f },
   { 22.63f,  16050.0f },
   { 28.00f,  16300.0f },
   { 36.00f,  16384.0f },
};
float g_PanAngleSpeedCheckZoomTable_DY_IR2036HL_WONWOO[21] =
{
   1.00f, 1.19f, 1.41f, 1.68f, 2.00f, 2.38f, 2.83f, 3.36f, 4.00f, 4.75f, 5.65f, 6.72f, 8.00f, 9.51f, 11.31f, 13.45f, 16.00f, 19.03f, 22.63f, 28.00f, 36.00f
};

float g_PanAngleVelTable_DY_IR2036HL_WONWOO[21][65] = 
{
   0.00f,  0.14f,  0.16f,  0.17f,  0.19f,  0.22f,  0.24f,  0.28f,  0.32f,  0.36f,  0.42f,  0.50f,  0.57f,  0.69f,  0.81f,  1.03f,  1.30f,  1.74f,  2.16f,  2.62f,  3.57f,  5.12f,  6.32f,  7.95f,  9.48f, 11.12f, 12.52f, 14.02f, 15.71f, 17.58f, 18.97f, 21.66f, 23.39f, 24.95f, 27.55f, 29.77f, 31.48f, 33.89f, 35.66f, 37.66f, 39.41f, 41.59f, 43.12f, 45.05f, 47.24f, 49.42f, 51.52f, 54.42f, 57.30f, 60.53f, 62.48f, 64.19f, 66.39f, 70.85f, 72.45f, 75.46f, 78.76f, 81.71f, 84.20f, 88.39f, 90.91f, 96.07f, 97.65f, 99.49f,101.87f,
   0.00f,  0.14f,  0.16f,  0.17f,  0.19f,  0.22f,  0.24f,  0.28f,  0.33f,  0.36f,  0.43f,  0.50f,  0.57f,  0.69f,  0.82f,  1.03f,  1.30f,  1.74f,  2.16f,  2.61f,  3.57f,  5.13f,  6.29f,  7.99f,  9.45f, 11.08f, 12.53f, 14.01f, 15.66f, 17.59f, 18.94f, 21.61f, 23.48f, 24.93f, 27.52f, 29.73f, 31.53f, 33.93f, 35.55f, 37.59f, 39.58f, 41.45f, 42.98f, 45.19f, 47.44f, 49.57f, 51.75f, 54.36f, 57.34f, 60.51f, 61.98f, 64.54f, 66.48f, 70.80f, 72.85f, 75.20f, 78.45f, 81.20f, 84.38f, 88.54f, 91.17f, 95.80f, 97.23f, 99.45f,102.08f,
   0.00f,  0.14f,  0.16f,  0.17f,  0.19f,  0.22f,  0.24f,  0.28f,  0.33f,  0.36f,  0.42f,  0.50f,  0.57f,  0.69f,  0.81f,  1.03f,  1.30f,  1.74f,  2.15f,  2.61f,  3.56f,  5.12f,  6.32f,  7.97f,  9.39f, 11.12f, 12.50f, 14.04f, 15.72f, 17.58f, 18.96f, 21.58f, 23.39f, 24.93f, 27.48f, 29.66f, 31.61f, 33.75f, 35.59f, 37.63f, 39.75f, 41.45f, 43.05f, 45.18f, 47.58f, 49.41f, 51.51f, 54.37f, 57.32f, 60.56f, 62.26f, 65.10f, 66.58f, 70.88f, 72.94f, 75.21f, 79.12f, 81.56f, 84.48f, 88.14f, 90.68f, 96.02f, 97.83f, 99.92f,102.16f,
   0.00f,  0.14f,  0.16f,  0.17f,  0.19f,  0.22f,  0.24f,  0.28f,  0.32f,  0.36f,  0.42f,  0.50f,  0.57f,  0.69f,  0.81f,  1.03f,  1.30f,  1.74f,  2.16f,  2.60f,  3.57f,  5.16f,  6.32f,  7.98f,  9.44f, 11.08f, 12.58f, 14.02f, 15.63f, 17.52f, 19.00f, 21.53f, 23.38f, 24.98f, 27.45f, 29.72f, 31.68f, 33.70f, 35.42f, 37.51f, 39.57f, 41.41f, 43.25f, 45.26f, 47.33f, 49.49f, 51.59f, 54.50f, 57.37f, 60.24f, 62.30f, 64.89f, 66.78f, 70.84f, 72.64f, 74.84f, 78.76f, 81.04f, 83.92f, 88.30f, 90.95f, 96.41f, 97.01f, 99.77f,101.12f,
   0.00f,  0.14f,  0.16f,  0.17f,  0.19f,  0.21f,  0.24f,  0.28f,  0.32f,  0.36f,  0.43f,  0.50f,  0.57f,  0.69f,  0.81f,  1.03f,  1.30f,  1.75f,  2.16f,  2.61f,  3.55f,  5.14f,  6.29f,  7.99f,  9.42f, 11.15f, 12.51f, 14.10f, 15.77f, 17.59f, 18.96f, 21.56f, 23.48f, 24.85f, 27.46f, 29.64f, 31.48f, 33.94f, 35.52f, 37.51f, 39.60f, 41.52f, 43.16f, 45.19f, 47.32f, 49.39f, 51.67f, 54.44f, 57.32f, 60.36f, 62.22f, 64.46f, 66.40f, 70.97f, 72.65f, 75.39f, 78.66f, 81.58f, 84.14f, 88.36f, 90.70f, 95.51f, 97.53f, 99.74f,101.78f,
   0.00f,  0.14f,  0.16f,  0.17f,  0.19f,  0.22f,  0.24f,  0.28f,  0.32f,  0.36f,  0.43f,  0.50f,  0.57f,  0.69f,  0.81f,  1.03f,  1.30f,  1.74f,  2.17f,  2.62f,  3.57f,  5.15f,  6.30f,  7.95f,  9.42f, 11.07f, 12.57f, 14.05f, 15.73f, 17.60f, 18.88f, 21.58f, 23.51f, 24.95f, 27.58f, 29.73f, 31.54f, 33.79f, 35.49f, 37.54f, 39.48f, 41.44f, 43.13f, 45.16f, 47.31f, 49.60f, 51.52f, 54.38f, 57.35f, 60.28f, 62.27f, 64.45f, 66.52f, 70.95f, 72.65f, 74.95f, 78.92f, 81.40f, 83.93f, 88.07f, 91.25f, 95.66f, 97.19f, 99.38f,101.61f,
   0.00f,  0.14f,  0.16f,  0.17f,  0.19f,  0.22f,  0.24f,  0.28f,  0.32f,  0.36f,  0.43f,  0.50f,  0.57f,  0.69f,  0.81f,  1.03f,  1.30f,  1.74f,  2.17f,  2.63f,  3.58f,  5.14f,  6.29f,  7.98f,  9.43f, 11.07f, 12.51f, 14.01f, 15.71f, 17.56f, 18.97f, 21.70f, 23.40f, 24.96f, 27.49f, 29.62f, 31.50f, 33.99f, 35.54f, 37.52f, 39.52f, 41.22f, 43.15f, 45.16f, 47.41f, 49.56f, 51.49f, 54.54f, 57.19f, 60.28f, 62.26f, 64.62f, 66.53f, 70.91f, 72.25f, 75.31f, 78.43f, 81.74f, 84.40f, 88.10f, 90.90f, 96.59f, 97.24f, 99.30f,102.21f,
   0.00f,  0.14f,  0.16f,  0.17f,  0.19f,  0.22f,  0.24f,  0.28f,  0.32f,  0.35f,  0.42f,  0.50f,  0.57f,  0.69f,  0.82f,  1.03f,  1.30f,  1.74f,  2.16f,  2.62f,  3.57f,  5.14f,  6.30f,  7.98f,  9.46f, 11.11f, 12.49f, 14.01f, 15.71f, 17.65f, 18.97f, 21.67f, 23.39f, 25.02f, 27.49f, 29.81f, 31.61f, 34.00f, 35.65f, 37.69f, 39.53f, 41.67f, 43.30f, 45.18f, 47.36f, 49.68f, 51.45f, 54.69f, 56.99f, 60.54f, 62.35f, 64.53f, 66.98f, 70.55f, 72.31f, 75.10f, 78.75f, 81.50f, 84.04f, 88.42f, 90.85f, 96.20f, 97.80f, 99.24f,102.01f,
   0.00f,  0.14f,  0.16f,  0.17f,  0.19f,  0.22f,  0.24f,  0.28f,  0.32f,  0.36f,  0.42f,  0.50f,  0.57f,  0.69f,  0.82f,  1.03f,  1.30f,  1.74f,  2.16f,  2.61f,  3.56f,  5.13f,  6.29f,  7.97f,  9.46f, 11.13f, 12.52f, 14.00f, 15.78f, 17.60f, 18.96f, 21.57f, 23.45f, 25.06f, 27.32f, 29.66f, 31.47f, 33.88f, 35.45f, 37.53f, 39.56f, 41.46f, 43.21f, 45.25f, 47.15f, 49.34f, 51.56f, 54.59f, 57.41f, 60.44f, 62.13f, 64.64f, 66.54f, 71.23f, 72.93f, 74.85f, 78.66f, 81.61f, 84.15f, 88.26f, 91.02f, 95.86f, 97.45f, 98.85f,101.15f,
   0.00f,  0.14f,  0.16f,  0.17f,  0.19f,  0.21f,  0.24f,  0.28f,  0.32f,  0.36f,  0.43f,  0.50f,  0.57f,  0.69f,  0.81f,  1.03f,  1.30f,  1.75f,  2.17f,  2.61f,  3.56f,  5.12f,  6.35f,  7.96f,  9.43f, 11.12f, 12.52f, 14.00f, 15.81f, 17.54f, 18.97f, 21.72f, 23.56f, 24.81f, 27.49f, 29.60f, 31.46f, 33.93f, 35.71f, 37.36f, 39.47f, 41.44f, 43.08f, 45.39f, 47.61f, 49.30f, 51.13f, 54.68f, 57.25f, 60.29f, 62.41f, 64.82f, 66.43f, 70.86f, 72.65f, 75.26f, 78.58f, 81.21f, 84.31f, 88.66f, 90.54f, 95.75f, 97.27f, 99.37f,101.90f,
   0.00f,  0.14f,  0.16f,  0.17f,  0.19f,  0.22f,  0.24f,  0.28f,  0.32f,  0.36f,  0.43f,  0.50f,  0.57f,  0.69f,  0.81f,  1.03f,  1.30f,  1.74f,  2.15f,  2.61f,  3.57f,  5.17f,  6.31f,  7.97f,  9.44f, 11.13f, 12.51f, 13.97f, 15.81f, 17.59f, 18.99f, 21.60f, 23.33f, 25.05f, 27.31f, 29.83f, 31.52f, 33.88f, 35.62f, 37.52f, 39.37f, 41.61f, 43.10f, 45.13f, 47.43f, 49.56f, 51.54f, 54.38f, 57.30f, 60.67f, 62.42f, 64.62f, 66.64f, 70.99f, 72.65f, 75.35f, 78.87f, 81.63f, 83.62f, 88.53f, 90.90f, 95.79f, 96.59f, 99.97f,101.85f,
   0.00f,  0.14f,  0.16f,  0.17f,  0.19f,  0.22f,  0.24f,  0.28f,  0.33f,  0.36f,  0.43f,  0.50f,  0.57f,  0.69f,  0.81f,  1.03f,  1.30f,  1.75f,  2.15f,  2.61f,  3.59f,  5.16f,  6.31f,  8.00f,  9.50f, 11.13f, 12.52f, 14.01f, 15.63f, 17.56f, 19.10f, 21.67f, 23.32f, 24.97f, 27.60f, 29.55f, 31.61f, 33.89f, 35.50f, 37.42f, 39.63f, 41.59f, 43.11f, 45.48f, 47.48f, 49.66f, 51.59f, 54.65f, 57.59f, 60.48f, 62.22f, 64.69f, 66.42f, 70.85f, 72.55f, 75.56f, 78.57f, 81.36f, 83.76f, 88.09f, 90.63f, 95.95f, 97.61f, 99.36f,101.76f,
   0.00f,  0.14f,  0.16f,  0.17f,  0.19f,  0.21f,  0.24f,  0.28f,  0.32f,  0.36f,  0.43f,  0.50f,  0.57f,  0.69f,  0.81f,  1.03f,  1.29f,  1.74f,  2.16f,  2.61f,  3.56f,  5.13f,  6.30f,  7.94f,  9.38f, 11.10f, 12.55f, 14.04f, 15.73f, 17.62f, 19.02f, 21.68f, 23.39f, 24.85f, 27.40f, 29.64f, 31.46f, 34.07f, 35.48f, 37.54f, 39.63f, 41.52f, 43.41f, 45.08f, 47.47f, 49.71f, 51.58f, 54.33f, 57.14f, 60.53f, 62.11f, 64.74f, 66.64f, 70.54f, 72.86f, 75.60f, 78.97f, 81.15f, 83.52f, 88.84f, 91.28f, 96.38f, 96.89f, 99.66f,101.61f,
   0.00f,  0.15f,  0.16f,  0.17f,  0.19f,  0.21f,  0.24f,  0.28f,  0.33f,  0.36f,  0.43f,  0.50f,  0.57f,  0.69f,  0.82f,  1.02f,  1.30f,  1.75f,  2.16f,  2.61f,  3.57f,  5.14f,  6.27f,  7.97f,  9.41f, 11.09f, 12.53f, 13.98f, 15.67f, 17.59f, 19.00f, 21.70f, 23.31f, 25.25f, 27.25f, 29.81f, 31.58f, 33.88f, 35.72f, 37.77f, 39.57f, 41.50f, 43.27f, 45.26f, 47.20f, 49.46f, 51.82f, 54.47f, 57.57f, 60.86f, 62.25f, 64.80f, 66.85f, 71.10f, 72.66f, 74.81f, 78.83f, 81.53f, 83.67f, 88.70f, 90.73f, 95.98f, 97.46f, 99.45f,101.89f,
   0.00f,  0.14f,  0.16f,  0.17f,  0.19f,  0.21f,  0.24f,  0.28f,  0.32f,  0.36f,  0.43f,  0.50f,  0.57f,  0.69f,  0.81f,  1.03f,  1.30f,  1.74f,  2.16f,  2.63f,  3.56f,  5.12f,  6.30f,  7.95f,  9.46f, 11.08f, 12.58f, 14.02f, 15.71f, 17.62f, 18.95f, 21.70f, 23.30f, 25.08f, 27.52f, 29.73f, 31.60f, 33.99f, 35.51f, 37.51f, 39.65f, 41.59f, 43.17f, 45.06f, 47.32f, 49.76f, 51.57f, 54.36f, 56.97f, 60.57f, 62.02f, 64.54f, 66.18f, 70.50f, 72.63f, 75.32f, 78.87f, 81.37f, 83.89f, 88.56f, 91.24f, 95.55f, 97.19f, 99.57f,102.24f,
   0.00f,  0.14f,  0.16f,  0.17f,  0.19f,  0.22f,  0.24f,  0.28f,  0.32f,  0.36f,  0.43f,  0.50f,  0.57f,  0.69f,  0.82f,  1.02f,  1.30f,  1.74f,  2.17f,  2.61f,  3.57f,  5.14f,  6.30f,  7.98f,  9.38f, 11.04f, 12.56f, 14.03f, 15.74f, 17.58f, 18.98f, 21.63f, 23.49f, 24.96f, 27.54f, 29.84f, 31.60f, 33.92f, 35.61f, 37.44f, 39.37f, 41.46f, 42.99f, 45.19f, 47.37f, 49.80f, 51.56f, 54.18f, 57.38f, 60.74f, 62.09f, 64.65f, 66.53f, 70.95f, 72.65f, 75.16f, 78.72f, 81.16f, 83.96f, 88.33f, 90.90f, 95.93f, 97.83f, 99.39f,101.75f,
   0.00f,  0.14f,  0.16f,  0.17f,  0.19f,  0.22f,  0.24f,  0.28f,  0.32f,  0.36f,  0.43f,  0.50f,  0.57f,  0.69f,  0.81f,  1.03f,  1.30f,  1.75f,  2.15f,  2.62f,  3.56f,  5.14f,  6.28f,  7.97f,  9.48f, 11.08f, 12.49f, 13.97f, 15.70f, 17.60f, 18.91f, 21.73f, 23.37f, 24.92f, 27.49f, 29.76f, 31.53f, 33.76f, 35.37f, 37.62f, 39.81f, 41.52f, 43.11f, 45.03f, 47.15f, 49.52f, 51.30f, 54.46f, 57.35f, 60.49f, 62.48f, 64.90f, 66.75f, 70.57f, 72.53f, 75.18f, 78.84f, 81.54f, 83.56f, 88.50f, 90.69f, 96.04f, 96.84f, 99.64f,102.01f,
   0.00f,  0.14f,  0.16f,  0.17f,  0.19f,  0.22f,  0.24f,  0.28f,  0.32f,  0.36f,  0.43f,  0.50f,  0.57f,  0.69f,  0.81f,  1.03f,  1.30f,  1.75f,  2.17f,  2.62f,  3.58f,  5.14f,  6.30f,  7.98f,  9.48f, 11.17f, 12.54f, 13.98f, 15.79f, 17.61f, 18.91f, 21.63f, 23.29f, 24.92f, 27.25f, 29.56f, 31.60f, 33.85f, 35.24f, 37.53f, 39.62f, 41.59f, 43.10f, 45.13f, 47.30f, 49.48f, 51.54f, 54.65f, 57.44f, 60.16f, 62.30f, 64.41f, 66.47f, 71.12f, 72.28f, 75.22f, 78.37f, 81.32f, 84.14f, 88.32f, 91.11f, 96.16f, 97.64f, 99.44f,101.63f,
   0.00f,  0.14f,  0.16f,  0.17f,  0.19f,  0.22f,  0.24f,  0.28f,  0.32f,  0.36f,  0.43f,  0.50f,  0.57f,  0.69f,  0.81f,  1.03f,  1.30f,  1.73f,  2.16f,  2.61f,  3.58f,  5.16f,  6.28f,  7.99f,  9.42f, 11.07f, 12.55f, 13.93f, 15.71f, 17.61f, 18.97f, 21.45f, 23.36f, 24.83f, 27.49f, 29.75f, 31.47f, 34.04f, 35.49f, 37.40f, 39.57f, 41.48f, 43.36f, 45.12f, 47.38f, 49.84f, 51.60f, 54.71f, 57.29f, 60.31f, 62.24f, 64.68f, 66.54f, 70.93f, 72.99f, 75.17f, 78.85f, 81.31f, 84.01f, 88.51f, 91.18f, 96.12f, 97.69f, 99.23f,102.15f,
   0.00f,  0.14f,  0.16f,  0.17f,  0.19f,  0.22f,  0.24f,  0.28f,  0.33f,  0.36f,  0.42f,  0.50f,  0.57f,  0.69f,  0.81f,  1.03f,  1.30f,  1.75f,  2.16f,  2.61f,  3.57f,  5.15f,  6.31f,  7.97f,  9.49f, 11.18f, 12.48f, 14.05f, 15.69f, 17.53f, 18.96f, 21.63f, 23.40f, 24.94f, 27.51f, 29.79f, 31.87f, 33.86f, 35.39f, 37.36f, 39.44f, 41.55f, 43.22f, 45.14f, 47.15f, 49.43f, 51.53f, 54.62f, 57.21f, 60.32f, 62.04f, 64.76f, 66.64f, 70.86f, 72.76f, 75.23f, 79.10f, 81.45f, 84.13f, 88.57f, 90.79f, 96.07f, 97.13f, 99.39f,101.52f,
   0.00f,  0.14f,  0.16f,  0.17f,  0.19f,  0.22f,  0.24f,  0.28f,  0.32f,  0.35f,  0.43f,  0.50f,  0.57f,  0.69f,  0.82f,  1.03f,  1.30f,  1.75f,  2.15f,  2.61f,  3.56f,  5.14f,  6.30f,  7.96f,  9.44f, 11.16f, 12.55f, 14.01f, 15.79f, 17.58f, 18.95f, 21.59f, 23.39f, 24.98f, 27.48f, 29.68f, 31.65f, 33.90f, 35.31f, 37.56f, 39.61f, 41.51f, 43.14f, 45.33f, 47.35f, 49.71f, 51.40f, 54.45f, 57.34f, 60.89f, 62.26f, 64.56f, 66.56f, 70.73f, 72.80f, 75.20f, 79.23f, 81.26f, 84.21f, 88.25f, 90.92f, 95.78f, 96.84f, 99.89f,101.77f,
};
float g_TiltAngleVelTable_DY_IR2036HL_WONWOO[21][65] = 
{
   0.00f,  0.19f,  0.22f,  0.24f,  0.28f,  0.31f,  0.35f,  0.41f,  0.47f,  0.52f,  0.62f,  0.73f,  0.83f,  1.00f,  1.18f,  1.43f,  1.60f,  1.74f,  1.87f,  2.05f,  2.29f,  2.51f,  2.88f,  3.19f,  3.64f,  4.05f,  4.71f,  5.24f,  6.03f,  6.44f,  7.00f,  7.61f,  8.18f,  9.07f,  9.85f, 10.63f, 11.37f, 12.07f, 12.84f, 13.48f, 14.09f, 14.86f, 15.73f, 16.91f, 17.96f, 18.79f, 20.74f, 21.84f, 23.46f, 24.94f, 25.94f, 27.05f, 28.24f, 29.54f, 31.33f, 32.47f, 34.10f, 35.14f, 36.79f, 37.99f, 40.24f, 41.63f, 42.68f, 45.55f, 46.67f,
   0.00f,  0.19f,  0.22f,  0.24f,  0.28f,  0.31f,  0.35f,  0.41f,  0.47f,  0.51f,  0.62f,  0.73f,  0.82f,  1.00f,  1.18f,  1.43f,  1.60f,  1.75f,  1.87f,  2.04f,  2.29f,  2.51f,  2.89f,  3.19f,  3.63f,  4.06f,  4.74f,  5.25f,  6.06f,  6.46f,  6.98f,  7.67f,  8.23f,  9.08f,  9.84f, 10.62f, 11.54f, 12.02f, 12.86f, 13.45f, 14.13f, 14.88f, 15.83f, 16.78f, 17.80f, 19.00f, 20.75f, 22.00f, 23.29f, 24.67f, 25.94f, 27.11f, 28.46f, 29.61f, 31.19f, 32.39f, 34.48f, 35.63f, 36.91f, 38.13f, 40.05f, 41.78f, 43.33f, 45.32f, 46.81f,
   0.00f,  0.19f,  0.22f,  0.24f,  0.28f,  0.31f,  0.35f,  0.41f,  0.47f,  0.52f,  0.62f,  0.73f,  0.83f,  1.00f,  1.18f,  1.43f,  1.60f,  1.74f,  1.87f,  2.05f,  2.30f,  2.51f,  2.87f,  3.19f,  3.66f,  4.04f,  4.73f,  5.23f,  6.04f,  6.47f,  6.94f,  7.60f,  8.22f,  9.03f,  9.82f, 10.66f, 11.38f, 12.04f, 12.86f, 13.54f, 14.23f, 15.03f, 15.68f, 16.80f, 17.77f, 19.19f, 20.41f, 22.33f, 23.27f, 24.58f, 25.70f, 27.27f, 28.40f, 29.34f, 31.14f, 32.09f, 33.75f, 35.35f, 36.74f, 38.82f, 39.86f, 40.91f, 43.43f, 45.23f, 47.55f,
   0.00f,  0.19f,  0.22f,  0.24f,  0.28f,  0.31f,  0.35f,  0.41f,  0.47f,  0.52f,  0.62f,  0.72f,  0.83f,  1.00f,  1.18f,  1.42f,  1.60f,  1.75f,  1.87f,  2.05f,  2.30f,  2.55f,  2.88f,  3.20f,  3.62f,  4.07f,  4.72f,  5.22f,  6.06f,  6.47f,  6.93f,  7.63f,  8.20f,  9.05f,  9.81f, 10.57f, 11.32f, 12.10f, 12.85f, 13.46f, 14.19f, 14.86f, 15.88f, 16.82f, 17.84f, 19.01f, 20.69f, 21.89f, 23.45f, 24.88f, 25.79f, 27.16f, 28.25f, 29.69f, 31.06f, 32.43f, 33.69f, 35.64f, 36.81f, 38.21f, 39.86f, 41.95f, 43.38f, 44.74f, 47.20f,
   0.00f,  0.19f,  0.21f,  0.24f,  0.28f,  0.31f,  0.35f,  0.41f,  0.47f,  0.52f,  0.61f,  0.73f,  0.82f,  1.00f,  1.18f,  1.43f,  1.61f,  1.74f,  1.87f,  2.05f,  2.30f,  2.54f,  2.87f,  3.19f,  3.63f,  4.05f,  4.73f,  5.24f,  6.04f,  6.47f,  6.97f,  7.62f,  8.23f,  9.13f,  9.83f, 10.65f, 11.42f, 12.01f, 12.88f, 13.50f, 14.08f, 14.87f, 15.74f, 16.92f, 17.78f, 19.01f, 20.67f, 22.16f, 23.60f, 24.85f, 26.07f, 26.90f, 28.24f, 29.74f, 31.04f, 32.51f, 34.40f, 35.73f, 37.17f, 38.06f, 39.63f, 41.47f, 42.71f, 45.21f, 47.43f,
   0.00f,  0.19f,  0.21f,  0.24f,  0.28f,  0.31f,  0.35f,  0.41f,  0.47f,  0.52f,  0.61f,  0.72f,  0.82f,  1.00f,  1.18f,  1.43f,  1.59f,  1.75f,  1.86f,  2.04f,  2.29f,  2.52f,  2.88f,  3.20f,  3.61f,  4.07f,  4.75f,  5.24f,  6.03f,  6.48f,  6.96f,  7.64f,  8.21f,  9.11f,  9.83f, 10.68f, 11.32f, 12.08f, 12.76f, 13.32f, 14.12f, 14.93f, 15.72f, 16.92f, 17.88f, 18.95f, 20.74f, 22.18f, 23.30f, 24.64f, 25.83f, 27.40f, 28.05f, 29.78f, 31.26f, 32.50f, 33.92f, 36.28f, 36.74f, 38.64f, 39.44f, 41.08f, 43.84f, 44.75f, 46.56f,
   0.00f,  0.19f,  0.21f,  0.24f,  0.28f,  0.31f,  0.35f,  0.41f,  0.47f,  0.52f,  0.62f,  0.73f,  0.83f,  1.00f,  1.17f,  1.43f,  1.60f,  1.75f,  1.87f,  2.04f,  2.30f,  2.51f,  2.86f,  3.21f,  3.61f,  4.05f,  4.75f,  5.24f,  6.06f,  6.49f,  6.93f,  7.57f,  8.21f,  9.02f,  9.83f, 10.62f, 11.46f, 12.10f, 12.81f, 13.42f, 14.05f, 14.98f, 15.68f, 16.78f, 17.89f, 19.04f, 20.54f, 22.09f, 23.29f, 24.57f, 26.14f, 27.10f, 28.25f, 29.56f, 31.10f, 32.50f, 34.27f, 35.51f, 36.93f, 38.48f, 39.53f, 41.35f, 43.05f, 45.21f, 46.23f,
   0.00f,  0.19f,  0.21f,  0.24f,  0.28f,  0.31f,  0.34f,  0.41f,  0.47f,  0.52f,  0.62f,  0.73f,  0.83f,  1.00f,  1.18f,  1.42f,  1.61f,  1.74f,  1.88f,  2.04f,  2.29f,  2.51f,  2.87f,  3.18f,  3.68f,  4.06f,  4.75f,  5.27f,  6.01f,  6.46f,  6.94f,  7.60f,  8.24f,  9.06f,  9.84f, 10.71f, 11.45f, 12.22f, 12.87f, 13.35f, 14.27f, 14.81f, 15.75f, 17.04f, 17.79f, 18.98f, 20.83f, 22.13f, 23.15f, 24.69f, 25.82f, 27.20f, 28.38f, 29.50f, 31.26f, 32.28f, 34.28f, 35.73f, 37.16f, 38.60f, 40.02f, 41.54f, 43.29f, 45.88f, 46.71f,
   0.00f,  0.19f,  0.22f,  0.24f,  0.28f,  0.31f,  0.35f,  0.41f,  0.47f,  0.52f,  0.62f,  0.73f,  0.83f,  1.00f,  1.18f,  1.43f,  1.59f,  1.74f,  1.88f,  2.04f,  2.28f,  2.52f,  2.88f,  3.20f,  3.62f,  4.05f,  4.77f,  5.26f,  6.07f,  6.46f,  6.97f,  7.59f,  8.22f,  9.13f,  9.85f, 10.64f, 11.40f, 12.05f, 12.81f, 13.60f, 14.20f, 15.12f, 15.76f, 16.72f, 17.87f, 18.82f, 20.90f, 21.94f, 23.28f, 24.63f, 25.96f, 26.82f, 28.39f, 29.70f, 31.38f, 32.72f, 33.77f, 35.67f, 36.78f, 38.49f, 39.92f, 41.58f, 43.71f, 45.33f, 46.68f,
   0.00f,  0.19f,  0.21f,  0.24f,  0.28f,  0.31f,  0.35f,  0.41f,  0.47f,  0.52f,  0.62f,  0.73f,  0.83f,  1.00f,  1.18f,  1.42f,  1.60f,  1.75f,  1.87f,  2.03f,  2.29f,  2.51f,  2.88f,  3.18f,  3.63f,  4.06f,  4.72f,  5.25f,  6.05f,  6.50f,  6.88f,  7.62f,  8.17f,  9.08f,  9.81f, 10.65f, 11.43f, 12.13f, 12.95f, 13.43f, 14.25f, 14.92f, 15.89f, 16.89f, 17.83f, 18.94f, 20.70f, 22.28f, 23.17f, 24.80f, 25.86f, 26.94f, 28.09f, 29.45f, 31.29f, 32.61f, 34.24f, 36.09f, 37.29f, 38.08f, 40.58f, 41.12f, 43.30f, 45.18f, 46.39f,
   0.00f,  0.19f,  0.21f,  0.24f,  0.28f,  0.31f,  0.35f,  0.41f,  0.47f,  0.52f,  0.62f,  0.73f,  0.83f,  1.00f,  1.18f,  1.43f,  1.60f,  1.74f,  1.86f,  2.05f,  2.28f,  2.51f,  2.87f,  3.19f,  3.58f,  4.08f,  4.73f,  5.22f,  6.04f,  6.46f,  6.91f,  7.54f,  8.14f,  9.04f,  9.83f, 10.77f, 11.30f, 12.02f, 12.82f, 13.45f, 14.18f, 14.86f, 15.84f, 16.87f, 18.00f, 19.30f, 20.67f, 22.01f, 23.12f, 24.66f, 25.84f, 27.01f, 28.18f, 29.73f, 30.98f, 32.33f, 34.14f, 35.17f, 37.54f, 38.21f, 40.03f, 41.72f, 42.76f, 44.86f, 47.34f,
   0.00f,  0.19f,  0.22f,  0.24f,  0.27f,  0.31f,  0.35f,  0.41f,  0.47f,  0.52f,  0.61f,  0.73f,  0.83f,  1.00f,  1.18f,  1.43f,  1.58f,  1.75f,  1.87f,  2.04f,  2.29f,  2.51f,  2.88f,  3.18f,  3.63f,  4.06f,  4.74f,  5.24f,  6.01f,  6.48f,  6.94f,  7.61f,  8.20f,  9.01f,  9.85f, 10.64f, 11.38f, 12.14f, 12.77f, 13.48f, 14.15f, 15.00f, 15.71f, 16.75f, 17.85f, 18.84f, 20.70f, 22.30f, 23.35f, 24.68f, 26.03f, 27.28f, 28.46f, 29.41f, 31.19f, 32.57f, 33.71f, 35.70f, 37.31f, 38.41f, 40.39f, 41.59f, 43.29f, 45.16f, 46.94f,
   0.00f,  0.19f,  0.22f,  0.24f,  0.28f,  0.31f,  0.35f,  0.41f,  0.47f,  0.52f,  0.62f,  0.73f,  0.83f,  1.00f,  1.19f,  1.42f,  1.59f,  1.76f,  1.87f,  2.05f,  2.28f,  2.52f,  2.89f,  3.20f,  3.61f,  4.04f,  4.73f,  5.22f,  6.04f,  6.47f,  6.93f,  7.68f,  8.23f,  9.04f,  9.85f, 10.63f, 11.33f, 12.01f, 12.85f, 13.52f, 14.21f, 14.96f, 15.78f, 16.77f, 17.81f, 18.73f, 20.79f, 21.99f, 23.29f, 24.71f, 25.86f, 27.10f, 28.62f, 29.27f, 31.23f, 32.57f, 33.94f, 35.64f, 37.42f, 38.25f, 40.07f, 41.13f, 42.94f, 45.03f, 47.03f,
   0.00f,  0.19f,  0.21f,  0.24f,  0.28f,  0.31f,  0.35f,  0.41f,  0.47f,  0.52f,  0.61f,  0.73f,  0.83f,  1.00f,  1.18f,  1.43f,  1.60f,  1.75f,  1.87f,  2.04f,  2.30f,  2.50f,  2.87f,  3.19f,  3.62f,  4.04f,  4.75f,  5.26f,  6.00f,  6.45f,  6.90f,  7.61f,  8.23f,  9.07f,  9.82f, 10.68f, 11.41f, 12.09f, 12.84f, 13.45f, 14.16f, 14.91f, 15.72f, 16.84f, 17.80f, 18.89f, 20.78f, 22.02f, 23.49f, 24.71f, 26.06f, 27.23f, 28.25f, 29.59f, 30.83f, 32.57f, 33.89f, 35.52f, 37.51f, 38.49f, 39.95f, 41.59f, 43.05f, 45.04f, 46.52f,
   0.00f,  0.19f,  0.22f,  0.24f,  0.28f,  0.31f,  0.35f,  0.41f,  0.47f,  0.52f,  0.62f,  0.72f,  0.83f,  1.01f,  1.17f,  1.43f,  1.60f,  1.75f,  1.86f,  2.06f,  2.28f,  2.51f,  2.86f,  3.19f,  3.65f,  4.04f,  4.71f,  5.22f,  6.03f,  6.46f,  6.97f,  7.55f,  8.22f,  9.12f,  9.80f, 10.62f, 11.38f, 12.03f, 12.83f, 13.39f, 14.06f, 14.83f, 15.70f, 16.74f, 17.81f, 19.05f, 20.82f, 22.06f, 23.28f, 24.38f, 25.70f, 27.21f, 28.30f, 29.86f, 31.24f, 32.47f, 34.04f, 35.29f, 36.68f, 38.56f, 40.17f, 41.79f, 42.99f, 44.43f, 46.37f,
   0.00f,  0.19f,  0.22f,  0.24f,  0.28f,  0.31f,  0.35f,  0.41f,  0.47f,  0.51f,  0.61f,  0.73f,  0.83f,  1.01f,  1.18f,  1.44f,  1.60f,  1.73f,  1.86f,  2.05f,  2.29f,  2.52f,  2.86f,  3.21f,  3.64f,  4.04f,  4.74f,  5.24f,  6.01f,  6.46f,  6.96f,  7.55f,  8.22f,  9.03f,  9.78f, 10.65f, 11.41f, 12.11f, 12.82f, 13.41f, 14.03f, 14.94f, 15.80f, 16.73f, 18.04f, 18.79f, 20.56f, 22.07f, 23.06f, 24.53f, 25.96f, 26.86f, 28.21f, 29.89f, 31.32f, 32.10f, 33.66f, 35.61f, 36.70f, 37.98f, 40.20f, 41.63f, 43.75f, 44.85f, 47.03f,
   0.00f,  0.19f,  0.21f,  0.24f,  0.28f,  0.31f,  0.35f,  0.41f,  0.47f,  0.51f,  0.62f,  0.73f,  0.83f,  1.00f,  1.18f,  1.42f,  1.59f,  1.75f,  1.87f,  2.05f,  2.29f,  2.52f,  2.87f,  3.20f,  3.63f,  4.06f,  4.74f,  5.23f,  6.05f,  6.46f,  6.94f,  7.65f,  8.27f,  9.09f,  9.82f, 10.66f, 11.39f, 12.02f, 12.75f, 13.55f, 14.11f, 15.05f, 15.72f, 16.85f, 17.89f, 19.04f, 21.01f, 22.00f, 23.39f, 24.60f, 25.74f, 26.89f, 28.26f, 29.70f, 31.54f, 32.52f, 33.89f, 35.78f, 36.74f, 38.45f, 41.03f, 41.53f, 43.48f, 45.41f, 46.96f,
   0.00f,  0.19f,  0.22f,  0.24f,  0.28f,  0.31f,  0.35f,  0.41f,  0.47f,  0.51f,  0.61f,  0.73f,  0.83f,  1.00f,  1.18f,  1.43f,  1.61f,  1.75f,  1.88f,  2.05f,  2.28f,  2.52f,  2.86f,  3.19f,  3.58f,  4.05f,  4.75f,  5.23f,  6.09f,  6.46f,  6.91f,  7.59f,  8.18f,  9.10f,  9.83f, 10.70f, 11.26f, 12.05f, 12.76f, 13.33f, 14.11f, 14.78f, 15.86f, 16.84f, 17.96f, 19.04f, 20.59f, 22.23f, 23.47f, 24.84f, 25.96f, 27.13f, 28.52f, 29.74f, 31.64f, 32.69f, 34.02f, 35.70f, 36.97f, 38.27f, 40.00f, 41.00f, 42.87f, 45.30f, 47.29f,
   0.00f,  0.19f,  0.22f,  0.24f,  0.28f,  0.31f,  0.35f,  0.41f,  0.47f,  0.52f,  0.61f,  0.73f,  0.83f,  1.00f,  1.17f,  1.43f,  1.60f,  1.74f,  1.88f,  2.05f,  2.29f,  2.53f,  2.86f,  3.18f,  3.63f,  4.04f,  4.71f,  5.25f,  6.02f,  6.44f,  6.94f,  7.60f,  8.25f,  8.99f,  9.86f, 10.74f, 11.41f, 12.08f, 12.88f, 13.39f, 14.23f, 14.90f, 15.72f, 16.85f, 17.96f, 19.17f, 20.55f, 22.09f, 23.34f, 24.84f, 25.94f, 26.87f, 27.97f, 29.45f, 31.30f, 32.52f, 34.02f, 35.55f, 37.41f, 38.12f, 39.77f, 41.19f, 42.83f, 45.19f, 48.35f,
   0.00f,  0.19f,  0.22f,  0.24f,  0.28f,  0.31f,  0.35f,  0.41f,  0.47f,  0.52f,  0.61f,  0.73f,  0.83f,  1.01f,  1.18f,  1.43f,  1.60f,  1.74f,  1.87f,  2.06f,  2.30f,  2.51f,  2.87f,  3.19f,  3.65f,  4.04f,  4.72f,  5.26f,  5.99f,  6.50f,  6.94f,  7.59f,  8.21f,  9.02f,  9.82f, 10.68f, 11.33f, 12.02f, 12.83f, 13.42f, 14.11f, 14.97f, 15.66f, 16.81f, 17.70f, 19.03f, 20.75f, 22.15f, 23.40f, 24.54f, 25.95f, 27.06f, 28.10f, 29.73f, 31.30f, 32.27f, 34.05f, 36.12f, 37.01f, 38.36f, 40.02f, 41.71f, 43.85f, 45.27f, 46.77f,
   0.00f,  0.19f,  0.22f,  0.24f,  0.28f,  0.31f,  0.35f,  0.41f,  0.47f,  0.52f,  0.62f,  0.73f,  0.83f,  1.00f,  1.18f,  1.42f,  1.60f,  1.74f,  1.87f,  2.05f,  2.27f,  2.52f,  2.87f,  3.18f,  3.60f,  4.03f,  4.76f,  5.23f,  6.08f,  6.48f,  6.90f,  7.60f,  8.21f,  9.11f,  9.87f, 10.70f, 11.37f, 12.08f, 12.82f, 13.50f, 14.02f, 15.03f, 15.89f, 16.74f, 17.79f, 18.97f, 20.69f, 21.79f, 23.41f, 24.72f, 25.89f, 27.12f, 28.59f, 29.29f, 31.08f, 32.52f, 34.19f, 35.73f, 37.25f, 38.16f, 40.08f, 41.00f, 43.56f, 45.05f, 46.21f,
};

﻿#pragma once


class CIPCameraInfo;
class CCompressImageBufferQueue;
class CCompressImageBufferItem;
class CCamera_IP;

#include "version.h"
#include <thread>

//////////////////////////////////////////////////////////////////
//
// Class : CIPCamStream
//
//////////////////////////////////////////////////////////////////

class CIPCamStream
{
public:
   enum State
   {
      State_Initialize                 = 0x00000001,
      State_Connecting                 = 0x00000002, // 연결시도상태
      State_Connected                  = 0x00000004, // 연결완료상태
      State_Capturing                  = 0x00000008,
      State_SignalConnectionToBeClosed = 0x00000010,
      State_AudioTransmitting          = 0x00000020, 
      State_SignalAudioTransmitStart   = 0x00000040, 
      State_00000080                   = 0x00000080,
      State_VideoSizeIsNotSupported    = 0x00000100,
      State_DecompressIsFailed         = 0x00000200,
      State_AlarmIOProcThread          = 0x00000400,
      State_SiganlStopAlarmIOProc      = 0x00000800,
      State_AuthorizationFailed        = 0x00001000,
      State_RTSP_ByePacketReceived     = 0x00002000,
   };
      
   enum ConnResult 
   {
      ConnResult_ConnectionOK        = 0, 
      ConnResult_ConnectionFailed    = 1, 
      ConnResult_AuthorizationFailed = 2,
      ConnResult_ProductNotMatched   = 3,
      ConnResult_VideoSignalNotOK    = 4,
      ConnResult_VideoSizeIsChanged  = 6, 
      ConnResult_SiganlStopCapture   = 7,
      ConnResult_SignalReactiveate   = 8,
   };

   enum DecompressResult
   {
      DecompressResult_OK                 = 0, 
      DecompressResult_VideoSizeIsChanged = 1, 
      DecompressResult_FrameRateIsChanged = 2, 
      DecompressResult_NeedMoreFrame      = 3, 
   };

public:
   int      m_nState;
   int      m_nCompanyID;
   int      m_nChannelNo_IVX;
   int      m_nStreamIdx;
   float    m_fFrameRate;
   BOOL     m_bRemote;
   uint     m_nVideoFrmPeriodForNoSignal;
   CAVCodec m_VideoDecoder;
   uint32_t m_dwLastCapture;  // 가장 최근에 캡춰된 시간.
   uint32_t m_dwConnected;    // 연결된 시간.
   uint32_t m_Debug_LastConnectionCheckTime;
   BOOL     m_Debug_CheckDecodingLoop;

public:
   CCriticalSection* m_pcsSrcImageBuff;
   CCompressImageBufferQueue* m_pCompBuffQueue;

public:
   LPVOID   m_pCBCamEventParam;  // CVideoChannel의 포인터임. IP 카메라에서 발생한 이벤트는 모두 CVideoChannel의 큐를 거친 후 콜백 함수가 호출.
   LPVIDEOCHANNEL_CALLBACK_CAMERA_EVENT m_pCBCamEvent;

public:
   CIPCamStream(void);
   virtual ~CIPCamStream(void);

public:
#ifdef WIN32
   virtual int CreateActiveXControls  (CWnd* pParentWnd) { return (DONE); }
#endif
   virtual int DestroyActiveXControls (   )              { return (DONE); }
   virtual int Initialize (CIPCameraInfo* pInfo) = 0;
   virtual int Uninitialize () = 0;
   virtual int BeginCapture ();
   virtual int EndCapture ();
   virtual int SignalStopCapture ();
   virtual int Close () = 0;

   virtual int Decompress (CCompressImageBufferItem * pBuffItem, BArray1D& destBuff) = 0;
#ifdef TRUEN_CAMERA_AUDIO_ENABLE
   virtual int DecompressAudio (CCompressImageBufferItem * pBuffItem, BArray1D& destBuff) { return (DONE); }
#endif

   virtual int SetAllImageProperty () { return (DONE);  }
   virtual int SetImageProperty (int nImagePropertyType, int nValue,  int nFlagAuto = FALSE)  { return (DONE);  }
   virtual int GetImageProperty (int nImagePropertyType, int &nValue, int &nFlagAuto)         { return (DONE);  }

   virtual int RequestIPCameraInfo ()  { return (DONE);  }

   virtual int GetMotionStatus      (BOOL& bMotion)              { return (DONE);  }
   virtual int GetAlarmInputFlags   (   )                        { return (DONE);  }
   virtual int GetAlarmInputStatus  (int nChIdx, BOOL& bSignal)  { return (DONE);  }
   virtual int SetRelayOutputStatus (int nChIdx, BOOL  bSignal)  { return (DONE);  }
   virtual int GetRelayOutputStatus (int nChIdx, BOOL& bSignal)  { return (DONE);  }

   virtual int  StopTalking ()  { return (DONE); }
   virtual int  StartTalking () { return (DONE); }

   virtual int  IsEnableAudioDataTransmit () { return FALSE; }
   virtual int  StartAudioInOut () { return (DONE); } // (xinu_be22)
   virtual int  StopAudioInOut () { return (DONE); }
   virtual int  PauseAudioInOut () { return (DONE); }
   virtual int  TransferAudio (const BYTE *pBuffer,int nBuffSize) { return (DONE); }

   virtual int  SendAudioStream (BYTE *pEncodeBuff, int nEncodeBuffLen, VOID * pSession = NULL) { return (DONE); }

public:
   virtual int SendPacket (int pkid, FileIO &io_packet) { return (DONE); }
   virtual int SendPTZCommand (BYTE* pCmd, int& nCmdLen) { return (DONE); }

   // Play Recorded Video Stream
   virtual int  StartPlayVideo (FILETIME& ftStartTime, float fPlaySpeed) { return (DONE);  }
   virtual int  StopPlayVideo  (   )                                     { return (DONE);  }
   virtual int  PausePlayVideo (   )                                     { return (DONE);  }
   virtual int  SetReversePlay (BOOL bReverse)                           { return (DONE);  }

public:
   virtual BOOL  IsConnected ();
   virtual BOOL  IsVideoSignalAlive ();

   // TYLEE: 2015-10-20
   virtual VOID *GetAudioSession() { return NULL; }

public:
   void  SetDisplayIndex (int nDisplayIndex);
};

/////////////////////////////////////////////////////////////////////////////
//
// class CMapID2IPCamStream
//
/////////////////////////////////////////////////////////////////////////////

class CMapID2IPCamStream
{
public:
   std::map<long, CIPCamStream*> m_mapClsID2Stream;
   CCriticalSection          m_csMap;

public:
   CMapID2IPCamStream ();

   void Push (long nClsID, CIPCamStream* pStream);
   void Pop (long nClsID);
   CIPCamStream* Find (long nClsID); 
};



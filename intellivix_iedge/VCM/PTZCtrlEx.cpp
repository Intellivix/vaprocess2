﻿#include "stdafx.h"
#include "PTZCtrlEx.h"

#ifdef __SUPPORT_PTZ_CAMERA

BOOL g_bIRLightCtrl = TRUE;

///////////////////////////////////////////////////////////////////////////////
//
// Class: MapF2FTblInfo
//
///////////////////////////////////////////////////////////////////////////////

MapF2FTblInfo::MapF2FTblInfo()
{
   m_nITPMode = 0;
   m_nTblSize = 0;
   m_pTbl     = NULL;
}

void MapF2FTblInfo::Set( int nITPMode, int nTblSize, MapF2F* pTbl )
{
   m_nITPMode = nITPMode;
   m_nTblSize = nTblSize;
   m_pTbl     = pTbl;
}

float _GetAValue( MapF2FTblInfo& map_tbl_info, float b )
{
   // 맵 테이블 정보와 입력값 b로 부터 보간된 a 값을 얻는다.
   int i;

   MapF2F* map_tbl  = map_tbl_info.m_pTbl;
   int map_tbl_size = map_tbl_info.m_nTblSize;
   int itp_mode     = map_tbl_info.m_nITPMode;

   // 계단방식의 보간모드의 경우
   if( itp_mode == INTERPOLATION_MODE_STAIR ) {
      if( b < map_tbl[0].B ) return map_tbl[0].A;
      for( i = 0; i < map_tbl_size - 1; i++ ) {
         if( map_tbl[i].B == b ) {
            return map_tbl[i].A;
         } else if( map_tbl[i].B <= b && b < map_tbl[i + 1].B ) {
            if( fabs( map_tbl[i].B - b ) < fabs( b - map_tbl[i + 1].B ) )
               return map_tbl[i].A;
            else
               return map_tbl[i + 1].A;
         }
      }
      return map_tbl[map_tbl_size - 1].A;
   }
   // 선형방식의 보간모드의 경우
   if( itp_mode == INTERPOLATION_MODE_LINEAR ) {
      if( map_tbl[0].B <= map_tbl[map_tbl_size - 1].B ) {
         if( b < map_tbl[0].B ) return map_tbl[0].A;
      } else {
         if( b >= map_tbl[0].B ) return map_tbl[0].A;
      }
      for( i = 0; i < map_tbl_size - 1; i++ ) {
         float b0 = map_tbl[i].B;
         float b1 = map_tbl[i + 1].B;
         if( ( b0 <= b && b < b1 ) || ( b1 <= b && b < b0 ) ) {
            float a0      = map_tbl[i].A;
            float a1      = map_tbl[i + 1].A;
            float a_range = ( a1 - a0 );
            float b_range = ( b1 - b0 );
            return a0 + a_range * ( ( b - b0 ) / ( b_range ) );
         }
      }
      return map_tbl[map_tbl_size - 1].A;
   }
   return ( 0.0f );
}

float _GetBValue( MapF2FTblInfo& map_tbl_info, float a )
{
   // 맵 테이블 정보와 입력값 a로 부터 보간된 b 값을 얻는다.
   int i;

   MapF2F* map_tbl  = map_tbl_info.m_pTbl;
   int map_tbl_size = map_tbl_info.m_nTblSize;
   int itp_mode     = map_tbl_info.m_nITPMode;

   if( itp_mode == INTERPOLATION_MODE_STAIR ) {
      if( a < map_tbl[0].A ) return map_tbl[0].B;
      for( i = 0; i < map_tbl_size - 1; i++ ) {
         if( map_tbl[i].A <= a && a < map_tbl[i + 1].A ) { // i 와 i+1 사이에 있으면 i에 대응되는 값을 취하는 방식 (수정 필요)
            if( fabs( map_tbl[i].A - a ) < fabs( a - map_tbl[i + 1].A ) )
               return map_tbl[i].B;
            else
               return map_tbl[i + 1].B;
         }
      }
      return map_tbl[map_tbl_size - 1].B;
   }
   if( itp_mode == INTERPOLATION_MODE_LINEAR ) {

      if( map_tbl[0].A <= map_tbl[map_tbl_size - 1].A ) {
         if( a <= map_tbl[0].A ) return map_tbl[0].B;
      } else {
         if( a >= map_tbl[0].A ) return map_tbl[0].B;
      }

      for( i = 0; i < map_tbl_size - 1; i++ ) {
         float a0 = map_tbl[i].A;
         float a1 = map_tbl[i + 1].A;
         if( ( a0 <= a && a < a1 ) || ( a1 <= a && a < a0 ) ) {
            float b0      = map_tbl[i].B;
            float b1      = map_tbl[i + 1].B;
            float a_range = ( a1 - a0 );
            float b_range = ( b1 - b0 );
            return b0 + b_range * ( ( a - a0 ) / ( a_range ) );
         }
      }
      return map_tbl[map_tbl_size - 1].B;
   }
   return ( 0.0f );
}

float _GetAngleSpeed( float* pAngleVelocityTable, int nMaxHWSpeed, float* pAngleSpeedCheckZoomTable, int nAngleSpeedCheckZoomTableItemNum, float fHWSpeed, float fCurZoomValue )
{
   int i;
   int nAngleVelocityIndex = 0;
   for( i = 0; i < nAngleSpeedCheckZoomTableItemNum; i++ ) {
      if( pAngleSpeedCheckZoomTable[i] <= fCurZoomValue && fCurZoomValue < pAngleSpeedCheckZoomTable[i + 1] ) {
         nAngleVelocityIndex = i;
      }
   }
   if( pAngleSpeedCheckZoomTable[nAngleSpeedCheckZoomTableItemNum] >= fCurZoomValue ) {
      nAngleVelocityIndex = nMaxHWSpeed;
   }
   return pAngleVelocityTable[nAngleVelocityIndex * nMaxHWSpeed + int( fHWSpeed )];
}

float _GetHWSpeed( float* pAngleVelocityTable, int nMaxHWSpeed, float* pAngleSpeedCheckZoomTable, int nAngleSpeedCheckZoomTableItemNum, float fAngleSpeed, float fCurZoomValue )
{
   int i;
   int nAngleVelocityIndex = 0;
   for( i = 0; i < nAngleSpeedCheckZoomTableItemNum; i++ ) {
      if( pAngleSpeedCheckZoomTable[i] <= fCurZoomValue && fCurZoomValue < pAngleSpeedCheckZoomTable[i + 1] ) {
         nAngleVelocityIndex = i;
         break;
      }
   }
   if( pAngleSpeedCheckZoomTable[nAngleSpeedCheckZoomTableItemNum - 1] <= fCurZoomValue ) {
      nAngleVelocityIndex = nAngleSpeedCheckZoomTableItemNum - 1;
   }
   int nMinDistanceHWSpeedIdx = 0;
   float nMinSpeed            = MC_VLV;
   for( i = 0; i <= nMaxHWSpeed; i++ ) {
      float fAngleSpeed_InTable = pAngleVelocityTable[nAngleVelocityIndex * ( nMaxHWSpeed + 1 ) + i];
      float fSpeedDiff          = fabs( fAngleSpeed_InTable - fAngleSpeed );
      if( fSpeedDiff < nMinSpeed ) {
         nMinSpeed              = fSpeedDiff;
         nMinDistanceHWSpeedIdx = i;
      }
   }
   if( nMinDistanceHWSpeedIdx == 0 && fAngleSpeed > 0.001f ) {
      nMinDistanceHWSpeedIdx = 1;
   }
   return float( nMinDistanceHWSpeedIdx );
}

////////////////////////////////////////////////////////////////////////
//
// class PTZCtrlLogItem
//
////////////////////////////////////////////////////////////////////////

PTZCtrlLogItem::PTZCtrlLogItem()
{
   m_nTime       = 0;
   m_fTgtLogZoom = 0.0f;
}

PTZCtrlLogItemQueue::PTZCtrlLogItemQueue()
{
   m_fFrameRate = 30.0f;
   m_nStartIdx  = 0;
   m_nEndIdx    = -1;
   m_nItemNum   = 0;
   m_pItems     = NULL;
}

PTZCtrlLogItemQueue::~PTZCtrlLogItemQueue()
{
   if( m_pItems )
      delete[] m_pItems;
}

void PTZCtrlLogItemQueue::Initialize( float fFrameRate, float fLogPeriod )
{
   m_fFrameRate   = fFrameRate;
   m_nFramePeriod = int( ceil( 1000.0f / fFrameRate ) );
   m_nLogPeriod   = int( ceil( fLogPeriod * 1000.0f ) );
   m_nMaxItemNum  = int( ceil( fFrameRate * fLogPeriod ) );
   if( m_pItems )
      delete[] m_pItems;
   m_pItems = new PTZCtrlLogItem[m_nMaxItemNum];
}

int PTZCtrlLogItemQueue::GetItemIdx( int nCurIdx, int nDeltaIdx )
{
   int nNextIdx = ( nCurIdx + nDeltaIdx ) % m_nMaxItemNum;
   if( nNextIdx < 0 )
      nNextIdx += m_nMaxItemNum;
   return nNextIdx;
}

void PTZCtrlLogItemQueue::AddItem( PTZCtrlLogItem& item )
{
   if( m_pItems ) {
      m_nEndIdx = GetItemIdx( m_nEndIdx, 1 );
      if( m_nItemNum < m_nMaxItemNum ) {
         m_nItemNum++;
      } else {
         m_nStartIdx = GetItemIdx( m_nStartIdx, 1 );
      }
      item.m_nTime = GetTickCount();

      item.m_fTgtLogZoom  = log( item.m_pvTgtAbsPos.Z ) * MC_LOG2_INV;
      m_pItems[m_nEndIdx] = item;
   }
}

int PTZCtrlLogItemQueue::FindItemIdx( int nDeltaTime )
{
   // 현재 시간으로 부터 nDeltaTime 만큼 과거에 있는 항목을 찾아라.
   int nFindIdx = -1;
   if( m_nItemNum > 0 ) {
      int nDeltaIdx = nDeltaTime / m_nFramePeriod;

      BOOL bFindOlderItem = TRUE;
      if( nDeltaIdx < m_nItemNum - 1 ) {
         nFindIdx = GetItemIdx( m_nEndIdx, -nDeltaIdx );
      } else {
         nFindIdx       = m_nStartIdx;
         bFindOlderItem = FALSE;
      }
   }
   return nFindIdx;
}

PTZCtrlLogItem* PTZCtrlLogItemQueue::GetLastItem()
{
   PTZCtrlLogItem* pItem = NULL;
   if( m_nItemNum > 0 ) {
      pItem = &m_pItems[m_nEndIdx];
   }
   return pItem;
}

float PTZCtrlLogItemQueue::CalcAvgTgtZoomValue( int nDeltaTime )
{
   // 평균 목표 줌값을 구한다. .
   float fAvgTgtZoom = 1.0f;

   double dfTgtLogZoomSum = 0.0f;

   int i, c;

   uint32_t nCurTime   = GetTickCount();
   uint nTimeLimit = nCurTime - nDeltaTime;

   if( m_nItemNum == 1 ) {
      PTZCtrlLogItem& sItemCurr = m_pItems[0];
      fAvgTgtZoom               = sItemCurr.m_pvTgtAbsPos.Z;
   } else if( m_nItemNum >= 2 ) {
      for( c = 0, i = m_nEndIdx; c < m_nItemNum; c++, i-- ) {
         if( i < 0 )
            i = m_nMaxItemNum - 1;
         ASSERT( 0 <= i && i < m_nItemNum );
         PTZCtrlLogItem& sItemCurr = m_pItems[i];
         if( nTimeLimit > sItemCurr.m_nTime ) {
            break;
         }
         dfTgtLogZoomSum += sItemCurr.m_fTgtLogZoom;
      }
      if( c > 0 ) {
         dfTgtLogZoomSum /= float( c );
         fAvgTgtZoom = pow( 2.0f, float( dfTgtLogZoomSum ) );
      }
   }

   return fAvgTgtZoom;
}

FPTZVector PTZCtrlLogItemQueue::CalcAvgPTSpdAvgZPos( int nDeltaTime )
{
   FPTZVector pvAvgPTSpdAvgZPos( 0.0f, 0.0f, 0.0f );
   DFPTZVector pvPTSpdZPosSum( 0.0, 0.0, 0.0 );

   int i, c;

   uint32_t nCurTime      = GetTickCount();
   int nCalcItemCount = int( nDeltaTime / m_nFramePeriod ) + 1;
   if( m_nItemNum == 1 ) {
      PTZCtrlLogItem& sItemCurr = m_pItems[0];
   } else if( m_nItemNum >= 2 ) {
      int i_prv;
      for( c = 0, i = m_nEndIdx; c < m_nItemNum; c++, i-- ) {
         if( i < 0 ) i         = m_nMaxItemNum - 1;
         i_prv                 = i - 1;
         if( i_prv < 0 ) i_prv = m_nMaxItemNum - 1;

         PTZCtrlLogItem& sItemCurr = m_pItems[i];
         PTZCtrlLogItem& sItemPrev = m_pItems[i_prv];
         if( c >= nCalcItemCount ) break;
         FPTZVector& pvCurAbsPos              = sItemCurr.m_pvCurAbsPos;
         FPTZVector& pvPrvAbsPos              = sItemPrev.m_pvCurAbsPos;
         float fDeltaTime                     = float( sItemCurr.m_nTime - sItemPrev.m_nTime ) / 1000.0f;
         if( fDeltaTime < 0.001f ) fDeltaTime = 0.001f;
         pvPTSpdZPosSum.P += GetOffsetAngleInDegree( pvPrvAbsPos.P, pvCurAbsPos.P ) / fDeltaTime;
         pvPTSpdZPosSum.T += GetOffsetAngleInDegree( pvPrvAbsPos.T, pvCurAbsPos.T ) / fDeltaTime;
         pvPTSpdZPosSum.Z += pvCurAbsPos.Z;
         pvPTSpdZPosSum.F += pvCurAbsPos.F;
      }
      if( c > 0 ) {
         double dfTotalCount = double( c );
         pvAvgPTSpdAvgZPos.P = float( pvPTSpdZPosSum.P / dfTotalCount );
         pvAvgPTSpdAvgZPos.T = float( pvPTSpdZPosSum.T / dfTotalCount );
         pvAvgPTSpdAvgZPos.Z = float( pvPTSpdZPosSum.Z / dfTotalCount );
         pvAvgPTSpdAvgZPos.F = float( pvPTSpdZPosSum.F / dfTotalCount );
      }
   }
   return pvAvgPTSpdAvgZPos;
}

FPTZVector PTZCtrlLogItemQueue::CalcAvgAbsPTZPos( int nDeltaTime )
{
   int i, c = 0;

   FPTZVector pvAvgPTZPos( 0.0f, 0.0f, 0.0f );
   uint32_t nCurTime      = GetTickCount();
   int nCalcItemCount = int( nDeltaTime / m_nFramePeriod ) + 1;
   if( m_nItemNum >= 2 ) {
      for( c = 0, i = m_nEndIdx; c < m_nItemNum; c++, i-- ) {
         if( i < 0 )
            i                      = m_nMaxItemNum - 1;
         PTZCtrlLogItem& sItemCurr = m_pItems[i];
         if( c >= nCalcItemCount ) break;
         pvAvgPTZPos += sItemCurr.m_pvCurAbsPos;
      }
      if( c > 0 ) {
         pvAvgPTZPos.P /= float( c );
         pvAvgPTZPos.T /= float( c );
         pvAvgPTZPos.Z /= float( c );
         pvAvgPTZPos.F /= float( c );
      }
   }
   return pvAvgPTZPos;
}

/////////////////////////////////////////////////////////////////////////////
//
// Class: CMapID2PTZCtrl
//
/////////////////////////////////////////////////////////////////////////////

CMapID2PTZCtrl::CMapID2PTZCtrl()
{
}

void CMapID2PTZCtrl::Push( long nClsID, CPTZCtrl* pStream )
{
   CCriticalSectionSP co( m_csMap );
   m_mapClsID2PTZCtrl.insert( std::map<long, CPTZCtrl*>::value_type( nClsID, pStream ) );
}

void CMapID2PTZCtrl::Pop( long nClsID )
{
   CCriticalSectionSP co( m_csMap );
   m_mapClsID2PTZCtrl.erase( m_mapClsID2PTZCtrl.find( nClsID ) );
}

CPTZCtrl* CMapID2PTZCtrl::Find( long nClsID )
{
   CCriticalSectionSP co( m_csMap );
   std::map<long, CPTZCtrl*>::iterator iter;
   iter              = m_mapClsID2PTZCtrl.find( nClsID );
   CPTZCtrl* pStream = NULL;
   if( iter == m_mapClsID2PTZCtrl.end() )
      return ( NULL );
   else {
      pStream = iter->second;
      return ( pStream );
   }
}

#endif

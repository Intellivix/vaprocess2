﻿#include "stdafx.h"
#include "ContAbsPosTrack.h"

#ifdef __SUPPORT_PTZ_CAMERA

#include "PTZCtrl.h"

float g_fAngleSpeedRatio_CynixMegaPixelPTZCam = 0.8f;
const FVector3D g_vtUnitX( -1.0f, 0.0f, 0.0f );
const FVector3D g_vtUnitY( 0.0f, -1.0f, 0.0f );

////////////////////////////////////////////////////////////////////////
//
// Functions
//
////////////////////////////////////////////////////////////////////////

Quaternion GetQuaternion( FPTZVector fvCurrPTZPos, FPTZVector fvPrevPTZPos )
{
   // 이전 Pan Tilt 위치와 현재 Pan,Tilt 위치로 부터 속도(쿼터니온)를 구한다.
   Quaternion qtVelocity;
   if( fvCurrPTZPos.P != NO_CMD && fvPrevPTZPos.P != NO_CMD ) {
      // Pan, Tilt의 차를 구한다.
      FPTZVector fvOffsetPos;
      fvOffsetPos.P = GetOffsetAngleInDegree( fvPrevPTZPos.P, fvCurrPTZPos.P );
      fvOffsetPos.T = GetOffsetAngleInDegree( fvPrevPTZPos.T, fvCurrPTZPos.T );

      // Pan, Tilt Angle Offset으로 부터 쿼터니온을 계산한다.
      Quaternion qtPA, qtTA;
      qtPA.FromAngleAxis( fvOffsetPos.P * MC_DEG2RAD, g_vtUnitY );
      qtTA.FromAngleAxis( fvOffsetPos.T * MC_DEG2RAD, g_vtUnitX );
      qtVelocity = qtPA * qtTA;
   }
   return qtVelocity;
}

////////////////////////////////////////////////////////////////////////
//
// class ContAbsPosTrkLogItem
//
////////////////////////////////////////////////////////////////////////

ContAbsPosTrkLogItem::ContAbsPosTrkLogItem()
{
   m_nTime       = 0;
   m_fZoomLogVel = 0.0f;
}

////////////////////////////////////////////////////////////////////////
//
// class ContAbsPosTrkLogItemQueue
//
////////////////////////////////////////////////////////////////////////

ContAbsPosTrkLogItemQueue::ContAbsPosTrkLogItemQueue()
{
   m_nStartIdx = 0;
   m_nEndIdx   = -1;
   m_nItemNum  = 0;
   m_pItems    = NULL;
}

ContAbsPosTrkLogItemQueue::~ContAbsPosTrkLogItemQueue()
{
   if( m_pItems )
      delete[] m_pItems;
}

void ContAbsPosTrkLogItemQueue::Initialize( float fFrameRate, float fLogPeriod )
{
   m_nFramePeriod = int( ceil( 1000.0f / fFrameRate ) );
   m_nLogPeriod   = int( ceil( fLogPeriod * 1000.0f ) );
   m_nMaxItemNum  = int( ceil( fFrameRate * fLogPeriod ) );
   if( m_pItems )
      delete[] m_pItems;
   m_pItems    = new ContAbsPosTrkLogItem[m_nMaxItemNum];
   m_nStartIdx = 0;
   m_nEndIdx   = -1;
   m_nItemNum  = 0;
}

int ContAbsPosTrkLogItemQueue::GetItemIdx( int nCurIdx, int nDeltaIdx )
{
   int nNextIdx = ( nCurIdx + nDeltaIdx ) % m_nMaxItemNum;
   if( nNextIdx < 0 )
      nNextIdx += m_nMaxItemNum;
   return nNextIdx;
}

void ContAbsPosTrkLogItemQueue::AddItem( ContAbsPosTrkLogItem& item )
{
   if( !m_pItems ) return;
   m_nEndIdx = GetItemIdx( m_nEndIdx, 1 );
   if( m_nItemNum < m_nMaxItemNum )
      m_nItemNum++;
   else
      m_nStartIdx      = GetItemIdx( m_nStartIdx, 1 );
   item.m_nTime        = GetTickCount();
   m_pItems[m_nEndIdx] = item;
}

int ContAbsPosTrkLogItemQueue::FindItemIdx( int nDeltaTime )
{
   // 현재 시간으로 부터 nDeltaTime 만큼 과거에 있는 항목을 찾아라.
   int nFindIdx = -1;
   if( m_nItemNum > 0 ) {
      int nDeltaIdx       = nDeltaTime / m_nFramePeriod;
      BOOL bFindOlderItem = TRUE;
      if( nDeltaIdx < m_nItemNum - 1 ) {
         nFindIdx = GetItemIdx( m_nEndIdx, -nDeltaIdx );
      } else {
         nFindIdx       = m_nStartIdx;
         bFindOlderItem = FALSE;
      }
   }
   return nFindIdx;
}

ContAbsPosTrkLogItem* ContAbsPosTrkLogItemQueue::GetLastItem()
{
   ContAbsPosTrkLogItem* pItem = NULL;
   if( m_nItemNum > 0 ) {
      pItem = &m_pItems[m_nEndIdx];
   }
   return pItem;
}

void ContAbsPosTrkLogItemQueue::CalcAvgVelocity( uint32_t nDeltaTime, Quaternion& qtAvgVelocity )
{
   int i, c;
   // 평균 속도 (쿼터니온 평균)을 구한다.
   double dfAngleSum = 0.0f;
   FVector3D fvAxisSum( 0.0f, 0.0f, 0.0f );
   float fZoomVelocitySum = 0.0f;

   uint32_t nCurTime   = GetTickCount();
   uint32_t nTimeLimit = nCurTime - nDeltaTime;
   if( m_nItemNum >= 2 ) {
      for( c = 0, i = m_nEndIdx; c < m_nItemNum; c++, i-- ) {
         if( i < 0 ) i = m_nMaxItemNum - 1;
         ASSERT( 0 <= i && i < m_nItemNum );
         ContAbsPosTrkLogItem& sItemCurr = m_pItems[i];
         if( nTimeLimit > sItemCurr.m_nTime ) break;
         double dfAngle;
         FVector3D fvAxis;
         sItemCurr.m_qtVelocity.ToAngleAxis( dfAngle, fvAxis );
         if( dfAngle > MC_PI )
            dfAngle = MC_2_PI - dfAngle;
         dfAngleSum += dfAngle;
         double dfAngleVelWeight = dfAngle * ( 1.0 / MC_PI );
         fvAxisSum += dfAngleVelWeight * fvAxis;
      }
      if( c > 0 ) {
         if( fabs( dfAngleSum ) > 0.00001 )
            qtAvgVelocity.FromAngleAxis( dfAngleSum / c, fvAxisSum * ( MC_PI / dfAngleSum ) );
      }
   }
}

void ContAbsPosTrkLogItemQueue::ResetAvgVelocity()
{
   int i;
   for( i = 0; i < m_nItemNum; i++ ) {
      ContAbsPosTrkLogItem& sItem = m_pItems[i];
      sItem.m_qtVelocity          = Quaternion::ZERO;
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CContAbsPosTrack
//
///////////////////////////////////////////////////////////////////////////////

CContAbsPosTrack::CContAbsPosTrack()
{
   m_nState = 0;
   m_fvTgtPos( NO_CMD, NO_CMD, NO_CMD );
   m_fvPrvPos = m_fvTgtPos;

   // 목표 위치 수렴속도 제어
   m_fTgtPosConvergeFactor   = 3.0f;
   m_fTargetConvergenceLimit = MC_VLV;
   m_fAngleSpeedRatio        = 0.0f;
   m_fTgtPosConvergeGamma    = 1.25f;
   m_fMaxGammaSpeed          = 60.0f;
   // 목표 위치 수렴여부 판단
   m_fMaxAngleOffsetForConvergence     = 0.15f;
   m_fMaxZoomLogOffsetForConvergence   = 0.24f;
   m_fMaxZoomLogOffsetForSlowZoomSpeed = 0.75f;
   // 절대위치 이동
   m_fMinAngleOffsetForPanTiltAbsPosMove = 40.0f;
   m_fPanTiltOffsetRatioForGotoAbsPos    = MC_VLV;
   m_fMinLog2OffsetForZoomAbsPosMove     = MC_VLV;
   m_nAdditionalAbsPosMovePeriod         = 150;
   m_nMaxAbsPosMovePeriod                = 4000;
   m_bUseAbsZoomPosMove                  = FALSE;
   m_fMaxZoomFactorForAbsPosMoveTracking = 0.0f;
   // 연속이동 제어
   m_fMaxContPanMoveAngleSpeed  = 360.0f;
   m_fMaxContTiltMoveAngleSpeed = 360.0f;
   m_nContMoveStopWaitTime      = 150;
   m_fContZoomSpeed_Fast        = 6.0f;
   m_fContZoomSpeed_Slow        = 4.0f;

   m_fConstantContPanMoveAngleSpeed  = MC_VLV;
   m_fConstantContTiltMoveAngleSpeed = MC_VLV;
   m_fConstantImageScrollSpeed       = MC_VLV;
}

CContAbsPosTrack::~CContAbsPosTrack()
{
}

void CContAbsPosTrack::Initialize( CPTZCtrl* pPTZCtrl, float fFrameRate )
{
   m_pPTZCtrl   = pPTZCtrl;
   m_fFrameRate = fFrameRate;
   m_ContAbsPosTrkLogItemQueue.Initialize( m_fFrameRate, 2.0f );
   InitPTZCamParams();
}

void CContAbsPosTrack::InitPTZCamParams()
{
   int nPTZCtrlType = m_pPTZCtrl->GetPTZControlTypeOfPTZCmd();
   switch( nPTZCtrlType ) {
   case PTZControlType_VirtualPTZ:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 5.0f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.1f;
      m_nAdditionalAbsPosMovePeriod     = -5000;
      m_nMaxAbsPosMovePeriod            = 0;
      // 절대위치 이동
      // 연속이동 제어
      m_fContZoomSpeed_Fast                 = 7.0f;
      m_fContZoomSpeed_Slow                 = 1.0f;
      m_nContMoveStopWaitTime               = 0;
      m_fMinAngleOffsetForPanTiltAbsPosMove = 90.0f;
      break;
   case PTZControlType_Axis_214:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 1.5f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.5f;
      m_fMaxZoomLogOffsetForConvergence = 0.24f;
      // 절대위치 이동
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 7.0f;
      m_fContZoomSpeed_Slow = 7.0f;
      break;
   case PTZControlType_Axis_Q6045_Mk_II:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 1.7f;
      m_fTgtPosConvergeGamma  = 1.35f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.25f;
      m_fMaxZoomLogOffsetForConvergence = 0.65f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 45.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 10.0f;

      m_fMaxContPanMoveAngleSpeed  = 360.0f;
      m_fMaxContTiltMoveAngleSpeed = 360.0f;

      m_nAdditionalAbsPosMovePeriod = 100;
      m_nMaxAbsPosMovePeriod        = 100;

      // 연속이동 제어
      m_nContMoveStopWaitTime = 100;
      m_fContZoomSpeed_Fast   = 10;
      m_fContZoomSpeed_Slow   = 1;

      break;

   case PTZControlType_SamsungTechWin_SPD_3700:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 3.9f;
      // 목표 위치 수렴여부 판단
      m_fMaxZoomLogOffsetForConvergence = 0.5f;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = float( m_pPTZCtrl->MaxZoomSpeed );
      m_fContZoomSpeed_Slow = float( m_pPTZCtrl->MaxZoomSpeed );
      break;
   case PTZControlType_SamsungTechWin_SPD_3750:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 3.9f;
      // 목표 위치 수렴여부 판단
      m_fMaxZoomLogOffsetForConvergence = 0.5f;
      // 절대위치 이동
      // 연속이동 제어
      m_fContZoomSpeed_Fast = float( m_pPTZCtrl->MaxZoomSpeed );
      m_fContZoomSpeed_Slow = float( m_pPTZCtrl->MaxZoomSpeed );
      break;
   case PTZControlType_SamsungTechWin_SCP_2370:
   case PTZControlType_SamsungTechWin_SCP_2370RH:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 3.5f;
      m_fTgtPosConvergeGamma  = 1.25f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.10f;
      m_fMaxZoomLogOffsetForConvergence = 0.15f;
      // 절대위치 이동
      //m_fMinAngleOffsetForPanTiltAbsPosMove = 9.0f;
      m_nAdditionalAbsPosMovePeriod = 500;
      // 연속이동 제어
      m_nContMoveStopWaitTime = 300;
      m_fContZoomSpeed_Fast   = 3;
      m_fContZoomSpeed_Slow   = 1;
      break;
   case PTZControlType_SamsungTechWin_SCP_2373N:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 3.5f;
      m_fTgtPosConvergeGamma  = 1.25f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.10f;
      m_fMaxZoomLogOffsetForConvergence = 0.15f;
      // 절대위치 이동
      //m_fMinAngleOffsetForPanTiltAbsPosMove = 9.0f;
      m_nAdditionalAbsPosMovePeriod         = 500;
      m_fMinAngleOffsetForPanTiltAbsPosMove = 60.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 4.5f;
      // 연속이동 제어
      m_nContMoveStopWaitTime = 300;
      m_fContZoomSpeed_Fast   = 3;
      m_fContZoomSpeed_Slow   = 1;
      break;
   case PTZControlType_Hitron_HF3S36AN:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 3.0f;
      // 목표 위치 수렴여부 판단
      m_fMaxZoomLogOffsetForConvergence = 0.24f;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 7.0f;
      m_fContZoomSpeed_Slow = 5.0f;
      break;
   case PTZControlType_WonWooEng_EWSJ_E360:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 2.3f;
      // 목표 위치 수렴여부 판단
      m_fMaxZoomLogOffsetForConvergence = 0.24f;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 5.0f;
      m_fContZoomSpeed_Slow = 3.0f;
      break;
   case PTZControlType_WonWooEng_WTK_E361:
   case PTZControlType_WonWooEng_WTK_M202:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 2.9f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.05f;
      m_fMaxZoomLogOffsetForConvergence = 0.1f;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 7.0f;
      m_fContZoomSpeed_Slow = 2.0f;
      break;
   case PTZControlType_WonWooEng_EWSJ_363:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 4.3f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.01f;
      m_fMaxZoomLogOffsetForConvergence = 0.1f;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 4.0f;
      m_fContZoomSpeed_Slow = 3.0f;
      break;
   case PTZControlType_WonWooEng_WCA_E361NR:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 3.5f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.05f;
      m_fMaxZoomLogOffsetForConvergence = 0.24f;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 6.0f;
      m_fContZoomSpeed_Slow = 2.0f;
      break;
   case PTZControlType_WonWooEng_WMK_M202:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor   = 3.5f;
      m_fTgtPosConvergeGamma    = 1.35f;
      m_fTargetConvergenceLimit = 4.0f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.05f;
      m_fMaxZoomLogOffsetForConvergence = 1.7f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 30.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 7.0f;
      m_bUseAbsZoomPosMove                  = TRUE;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 3.0f;
      m_fContZoomSpeed_Slow = 1.0f;
      break;
   case PTZControlType_WonWooEng_WMK_MM308:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor   = 3.0f;
      m_fTgtPosConvergeGamma    = 1.35f;
      m_fTargetConvergenceLimit = 4.0f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.05f;
      m_fMaxZoomLogOffsetForConvergence = 1.7f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 30.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 7.0f;
      m_bUseAbsZoomPosMove                  = TRUE;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 3.0f;
      m_fContZoomSpeed_Slow = 1.0f;
      break;
   case PTZControlType_WonWooEng_WMK_HS308:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor   = 5.0f;
      m_fTgtPosConvergeGamma    = 1.35f;
      m_fTargetConvergenceLimit = 4.0f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.15f;
      m_fMaxZoomLogOffsetForConvergence = 0.25f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 15.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 7.0f;
      m_bUseAbsZoomPosMove                  = TRUE;
      m_nAdditionalAbsPosMovePeriod         = 0;
      m_fMinLog2OffsetForZoomAbsPosMove     = 1.5f;
      // 연속이동 제어
      m_nContMoveStopWaitTime = 0;
      m_fContZoomSpeed_Fast   = 3.0f;
      m_fContZoomSpeed_Slow   = 1.0f;
      break;
   case PTZControlType_Youtech_7000WB_1000:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 3.3f;
      m_fTgtPosConvergeGamma  = 1.2f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.05f;
      m_fMaxZoomLogOffsetForConvergence = 0.20f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 15.0f;
      m_nAdditionalAbsPosMovePeriod         = 500;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 2.0f;
      m_fContZoomSpeed_Slow = 1.0f;
      break;
   case PTZControlType_Youtech_7000WB_750:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 2.3f;
      m_fTgtPosConvergeGamma  = 1.2f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.05f;
      m_fMaxZoomLogOffsetForConvergence = 0.20f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 15.0f;
      m_nAdditionalAbsPosMovePeriod         = 500;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 2.0f;
      m_fContZoomSpeed_Slow = 1.0f;
      break;
   case PTZControlType_Youtech_9000MR_750:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 2.3f;
      m_fTgtPosConvergeGamma  = 1.2f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.05f;
      m_fMaxZoomLogOffsetForConvergence = 0.20f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 90.0f;
      m_nAdditionalAbsPosMovePeriod         = 500;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 2.0f;
      m_fContZoomSpeed_Slow = 1.0f;
      break;
   case PTZControlType_SamsungTechWin_iPOLiS_SNB_7004_Shinwoo_SPT_7080B:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 2.3f;
      m_fTgtPosConvergeGamma  = 1.2f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.05f;
      m_fMaxZoomLogOffsetForConvergence = 0.20f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 90.0f;
      m_nAdditionalAbsPosMovePeriod         = 500;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 2.0f;
      m_fContZoomSpeed_Slow = 1.0f;
      break;
   case PTZControlType_SamsungTechWin_iPOLiS_SNB_6004_Shinwoo_SPT_7080B:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 2.3f;
      m_fTgtPosConvergeGamma  = 1.2f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.05f;
      m_fMaxZoomLogOffsetForConvergence = 0.20f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 90.0f;
      m_nAdditionalAbsPosMovePeriod         = 500;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 2.0f;
      m_fContZoomSpeed_Slow = 1.0f;
      break;
   case PTZControlType_SamsungTechWin_iPOLiS_SNB_6004_Shinwoo_SPT_8080BE:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 2.3f;
      m_fTgtPosConvergeGamma  = 1.2f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.05f;
      m_fMaxZoomLogOffsetForConvergence = 0.20f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 90.0f;
      m_nAdditionalAbsPosMovePeriod         = 500;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 2.0f;
      m_fContZoomSpeed_Slow = 1.0f;
      break;
   case PTZControlType_SamsungTechWin_iPOLiS_SNB_6005_Shinwoo_SPT_8080BE:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 2.3f;
      m_fTgtPosConvergeGamma  = 1.2f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.05f;
      m_fMaxZoomLogOffsetForConvergence = 0.20f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 90.0f;
      m_nAdditionalAbsPosMovePeriod         = 500;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 2.0f;
      m_fContZoomSpeed_Slow = 1.0f;
      break;
   case PTZControlType_SamsungTechWin_iPOLiS_SNZ_6320_Shinwoo_SPT_8080BE:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 2.3f;
      m_fTgtPosConvergeGamma  = 1.2f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.05f;
      m_fMaxZoomLogOffsetForConvergence = 0.20f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 90.0f;
      m_nAdditionalAbsPosMovePeriod         = 500;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 2.0f;
      m_fContZoomSpeed_Slow = 1.0f;
      break;
   case PTZControlType_CYNIX_EAI_N27T:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 3.0f;
      m_fTgtPosConvergeGamma  = 1.35f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.15f;
      m_fMaxZoomLogOffsetForConvergence = 0.40f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 90.0f;
      m_nAdditionalAbsPosMovePeriod         = 650;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 2.0f;
      m_fContZoomSpeed_Slow = 1.0f;
      break;
   case PTZControlType_CYNIX_HA_18H_S:
   case PTZControlType_CYNIX_HC_X18H:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 2.1f;
      m_fTgtPosConvergeGamma  = 1.35f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.15f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 90.0f;
      m_nAdditionalAbsPosMovePeriod         = 1000;
      m_nMaxAbsPosMovePeriod                = 6000;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 2.0f;
      m_fContZoomSpeed_Slow = 1.0f;
      break;
   case PTZControlType_CYNIX_HC_N20S:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 4.0f;
      m_fTgtPosConvergeGamma  = 1.15f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.15f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 12.0f;
      m_nAdditionalAbsPosMovePeriod         = 300;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 2.0f;
      m_fContZoomSpeed_Slow = 1.0f;
      break;
   case PTZControlType_CYNIX_MM_302:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 5.0f;
      m_fTgtPosConvergeGamma  = 1.4f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.25f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 60.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 20.0f;
      m_fMinLog2OffsetForZoomAbsPosMove     = 2.8f;
      m_nAdditionalAbsPosMovePeriod         = 300;
      m_nMaxAbsPosMovePeriod                = 0;
      m_bUseAbsZoomPosMove                  = TRUE;
      // 연속이동 제어
      m_fMaxContPanMoveAngleSpeed  = 15.0f;
      m_fMaxContTiltMoveAngleSpeed = 8.0f;
      m_fContZoomSpeed_Fast        = 30.0f;
      m_fContZoomSpeed_Slow        = 10.0f;
      break;
   case PTZControlType_CYNIX_MM_202:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 5.0f;
      m_fTgtPosConvergeGamma  = 1.4f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.25f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 40.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 15.0f;
      m_fMinLog2OffsetForZoomAbsPosMove     = 2.8f;
      m_nAdditionalAbsPosMovePeriod         = 300;
      m_nMaxAbsPosMovePeriod                = 0;
      m_bUseAbsZoomPosMove                  = TRUE;
      // 연속이동 제어
      m_fMaxContPanMoveAngleSpeed  = 15.0f;
      m_fMaxContTiltMoveAngleSpeed = 8.0f;
      m_fContZoomSpeed_Fast        = 180.0f;
      m_fContZoomSpeed_Slow        = 10.0f;
      break;
   case PTZControlType_Truen_TCAM_570_S22FIR:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 3.2f;
      m_fTgtPosConvergeGamma  = 1.25f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.65f;
      m_fMaxZoomLogOffsetForConvergence = 50.250f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 45.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 3.0f;
      //m_fMinLog2OffsetForZoomAbsPosMove   = 1.5f;
      m_nAdditionalAbsPosMovePeriod = 0;
      m_nMaxAbsPosMovePeriod        = 1500;
      m_bUseAbsZoomPosMove          = FALSE;
      // 연속이동 제어
      m_fMaxContPanMoveAngleSpeed  = 35.0f;
      m_fMaxContTiltMoveAngleSpeed = 12.0f;
      m_fContZoomSpeed_Fast        = 2.0f;
      m_fContZoomSpeed_Slow        = 1.0f;
      break;

   case PTZControlType_Truen_TCAM_PX220CS:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 2.2f;
      m_fTgtPosConvergeGamma  = 1.35f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.20f;
      m_fMaxZoomLogOffsetForConvergence = 0.15f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 20.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 2.0f;
      m_fMinLog2OffsetForZoomAbsPosMove     = 1.5f;
      m_nAdditionalAbsPosMovePeriod         = 1500;
      m_nMaxAbsPosMovePeriod                = 1500;
      m_bUseAbsZoomPosMove                  = TRUE;
      // 연속이동 제어
      m_fMaxContPanMoveAngleSpeed  = 70.0f;
      m_fMaxContTiltMoveAngleSpeed = 70.0f;
      m_fContZoomSpeed_Fast        = 2.0f;
      m_fContZoomSpeed_Slow        = 1.0f;
      break;

   case PTZControlType_Truen_TN_PX220CT:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 3.2f;
      m_fTgtPosConvergeGamma  = 1.25f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.15f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 45.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 3.0f;
      //m_fMinLog2OffsetForZoomAbsPosMove   = 1.5f;
      m_nAdditionalAbsPosMovePeriod = 0;
      m_nMaxAbsPosMovePeriod        = 1500;
      m_bUseAbsZoomPosMove          = FALSE;
      // 연속이동 제어
      m_fMaxContPanMoveAngleSpeed  = 35.0f;
      m_fMaxContTiltMoveAngleSpeed = 12.0f;
      m_fContZoomSpeed_Fast        = 2.0f;
      m_fContZoomSpeed_Slow        = 1.0f;

   case PTZControlType_Truen_TN_P4220CTIR:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 3.2f;
      m_fTgtPosConvergeGamma  = 1.25f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.15f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 45.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 3.0f;
      //m_fMinLog2OffsetForZoomAbsPosMove   = 1.5f;
      m_nAdditionalAbsPosMovePeriod = 0;
      m_nMaxAbsPosMovePeriod        = 1500;
      m_bUseAbsZoomPosMove          = FALSE;
      // 연속이동 제어
      m_fMaxContPanMoveAngleSpeed  = 35.0f;
      m_fMaxContTiltMoveAngleSpeed = 12.0f;
      m_fContZoomSpeed_Fast        = 2.0f;
      m_fContZoomSpeed_Slow        = 1.0f;
      //g_ParameterLoader_ContAbsPosTrack.Initialize(CString(g_szVCMConfigDirectory) + "\\__ContAbsPosTrkParams_Truen_4220CTIR.xml", (IParameterInputOutput*)this);
      break;

   case PTZControlType_Truen_TN_P5230CWIR:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 2.2f;
      m_fTgtPosConvergeGamma  = 1.35f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.20f;
      m_fMaxZoomLogOffsetForConvergence = 0.15f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 20.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 2.0f;
      m_fMinLog2OffsetForZoomAbsPosMove     = 1.5f;
      m_nAdditionalAbsPosMovePeriod         = 1500;
      m_nMaxAbsPosMovePeriod                = 1500;
      m_bUseAbsZoomPosMove                  = TRUE;
      // 연속이동 제어
      m_fMaxContPanMoveAngleSpeed  = 70.0f;
      m_fMaxContTiltMoveAngleSpeed = 70.0f;
      m_fContZoomSpeed_Fast        = 2.0f;
      m_fContZoomSpeed_Slow        = 1.0f;

      //g_ParameterLoader_ContAbsPosTrack.Initialize (CString (g_szVCMConfigDirectory) + "\\__ContAbsPosTrkParams_Tren_TN_P5230CwIR.xml", (IParameterInputOutput*)this);
      break;

   case PTZControlType_Xvas_XV_4120IRD:
   case PTZControlType_Xvas_XV_4120IRD_13Byte:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 3.0f;
      m_fTgtPosConvergeGamma  = 1.35f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.15f;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 2.0f;
      m_fContZoomSpeed_Slow = 1.0f;
   case PTZControlType_SonyIPELA_SNC_RH164_124:
   case PTZControlType_SonyIPELA_SNC_ER580:
   case PTZControlType_SonyIPELA_SNC_RX570:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor   = 3.2f;
      m_fTgtPosConvergeGamma    = 1.35f;
      m_fTargetConvergenceLimit = 4.0f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.25f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 9.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 2.0f;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 50.0f;
      m_fContZoomSpeed_Slow = 10.0f;
      break;
   case PTZControlType_SonyIPELA_SNC_WR630:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor   = 1.6f;
      m_fTgtPosConvergeGamma    = 1.45f;
      m_fTargetConvergenceLimit = 4.0f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.25f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 9.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 2.0f;
      m_bUseAbsZoomPosMove                  = TRUE;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 60.0f;
      m_fContZoomSpeed_Slow = 10.0f;
      break;
   case PTZControlType_SonyIPELA_SNC_ER585:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor   = 3.5f;
      m_fTgtPosConvergeGamma    = 1.45f;
      m_fTargetConvergenceLimit = 4.0f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.25f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 0.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 0.0f;
      m_nAdditionalAbsPosMovePeriod         = 0;
      m_bUseAbsZoomPosMove                  = FALSE;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 80.0f;
      m_fContZoomSpeed_Slow = 10.0f;
      break;
   case PTZControlType_HIKVision_NAIS_M138IR:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 4.5f;
      m_fTgtPosConvergeGamma  = 1.35f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.25f;
      // 절대위치 이동
      m_nAdditionalAbsPosMovePeriod = 1000;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 2.0f;
      m_fContZoomSpeed_Slow = 1.0f;
      break;
   case PTZControlType_HIKVision_NAIS_M138H:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 3.9f;
      m_fTgtPosConvergeGamma  = 1.35f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.25f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 15.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 2.0f;
      m_nAdditionalAbsPosMovePeriod         = 1000;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 2.0f;
      m_fContZoomSpeed_Slow = 1.0f;
      break;
   case PTZControlType_HIKVision_NAIS_DS_2DF1:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 4.5f;
      m_fTgtPosConvergeGamma  = 1.35f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.25f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 60.0f;
      m_nAdditionalAbsPosMovePeriod         = 1200;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 2.0f;
      m_fContZoomSpeed_Slow = 1.0f;
      break;
   case PTZControlType_HIKVision_NAIS_M200IR:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 4.5f;
      m_fTgtPosConvergeGamma  = 1.35f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.25f;
      // 절대위치 이동
      m_nAdditionalAbsPosMovePeriod = 1000;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 2.0f;
      m_fContZoomSpeed_Slow = 1.0f;
      break;
   case PTZControlType_HIKVision_DS_2DF7286_A:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 3.5f;
      m_fTgtPosConvergeGamma  = 1.35f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.50f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 15.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 10.0f;
      m_fMinLog2OffsetForZoomAbsPosMove     = 0.75f;
      m_nAdditionalAbsPosMovePeriod         = 300;
      m_nMaxAbsPosMovePeriod                = 1200;
      m_bUseAbsZoomPosMove                  = FALSE;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 2.0f;
      m_fContZoomSpeed_Slow = 1.0f;
      break;
   case PTZControlType_HIKVision_DS_2DE7186_A:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 3.5f;
      m_fTgtPosConvergeGamma  = 1.25f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.18f;
      m_fMaxZoomLogOffsetForConvergence = 0.50f;
      //m_fMaxZoomLogOffsetForSlowZoomSpeed = 1.75f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 15.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 10.0f;
      m_fMinLog2OffsetForZoomAbsPosMove     = 0.25f;
      m_nAdditionalAbsPosMovePeriod         = 300;
      //m_nMaxAbsPosMovePeriod              = 1200;
      m_bUseAbsZoomPosMove = FALSE;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 2.0f;
      m_fContZoomSpeed_Slow = 1.0f;
      break;
   case PTZControlType_HIKVision_DS_2DF8236I_AEL:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 2.0f;
      m_fTgtPosConvergeGamma  = 1.75f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.012f;
      m_fMaxZoomLogOffsetForConvergence = 2.3f;
      //m_fMaxZoomLogOffsetForSlowZoomSpeed = 1.75f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 15.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 3.0f;
      m_fMinLog2OffsetForZoomAbsPosMove     = 30.0f;

      m_nAdditionalAbsPosMovePeriod = 300;
      m_nMaxAbsPosMovePeriod        = 500;
      m_bUseAbsZoomPosMove          = FALSE;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 7.0f;
      m_fContZoomSpeed_Slow = 1.0f;
      //  g_ParameterLoader_ContAbsPosTrack.Close ();
      //  g_ParameterLoader_ContAbsPosTrack.Initialize (CString (g_szVCMConfigDirectory) + "\\__ContAbsPosTrkParams_HIKVision.xml", (IParameterInputOutput*)this);
      break;
   case PTZControlType_Dahua_NS_SD200IR:
   case PTZControlType_Dahua_NS_SD203IR:
   case PTZControlType_Dahua_NS_SD300IR:
   case PTZControlType_Dahua_NS_SD138:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 3.5f;
      m_fTgtPosConvergeGamma  = 1.35f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.15f;
      m_fMaxZoomLogOffsetForConvergence = 0.24f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 30.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 3.0f;
      m_fMinLog2OffsetForZoomAbsPosMove     = 1.0f;
      m_nAdditionalAbsPosMovePeriod         = 300;
      m_nMaxAbsPosMovePeriod                = 1200;
      // 연속이동 제어
      m_fMaxContPanMoveAngleSpeed  = 30.0f;
      m_fMaxContTiltMoveAngleSpeed = 15.0f;
      m_fContZoomSpeed_Fast        = 2.0f;
      m_fContZoomSpeed_Slow        = 1.0f;
      break;
   case PTZControlType_Dahua_NS_SD220IR:
   case PTZControlType_Dahua_NS_SD230IR:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 3.5f;
      m_fTgtPosConvergeGamma  = 1.25f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.15f;
      m_fMaxZoomLogOffsetForConvergence = 0.5f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 70.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 5.0f;
      m_fMinLog2OffsetForZoomAbsPosMove     = 2.8f;
      m_nAdditionalAbsPosMovePeriod         = 500;
      m_nMaxAbsPosMovePeriod                = 1200;
      m_bUseAbsZoomPosMove                  = TRUE;
      // 연속이동 제어
      m_fMaxContPanMoveAngleSpeed  = 150.0f;
      m_fMaxContTiltMoveAngleSpeed = 50.0f;
      m_fContZoomSpeed_Fast        = 2.0f;
      m_fContZoomSpeed_Slow        = 1.0f;

      m_fMaxZoomFactorForAbsPosMoveTracking = 3.360f;
      break;
   case PTZControlType_Dahua_NS_SD230IRC:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 3.0f;
      m_fTgtPosConvergeGamma  = 1.5f;
      m_fMaxGammaSpeed        = 70;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.5f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 70.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 5.0f;
      m_fMinLog2OffsetForZoomAbsPosMove     = 2.8f;
      m_nAdditionalAbsPosMovePeriod         = 300;
      m_nMaxAbsPosMovePeriod                = 1200;
      m_bUseAbsZoomPosMove                  = TRUE;
      // 연속이동 제어
      m_fMaxContPanMoveAngleSpeed  = 60.0f;
      m_fMaxContTiltMoveAngleSpeed = 30.0f;
      m_fContZoomSpeed_Fast        = 1.0f;
      m_fContZoomSpeed_Slow        = 1.0f;

      m_fMaxZoomFactorForAbsPosMoveTracking = 3.360f;
      break;
   case PTZControlType_Dahua_KADJ_NM2030IRA:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 3.0f;
      m_fTgtPosConvergeGamma  = 1.5f;
      m_fMaxGammaSpeed        = 70;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.5f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 70.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 5.0f;
      m_fMinLog2OffsetForZoomAbsPosMove     = 2.8f;
      m_nAdditionalAbsPosMovePeriod         = 300;
      m_nMaxAbsPosMovePeriod                = 1200;
      m_bUseAbsZoomPosMove                  = TRUE;
      // 연속이동 제어
      m_fMaxContPanMoveAngleSpeed  = 60.0f;
      m_fMaxContTiltMoveAngleSpeed = 30.0f;
      m_fContZoomSpeed_Fast        = 1.0f;
      m_fContZoomSpeed_Slow        = 1.0f;

      m_fMaxZoomFactorForAbsPosMoveTracking = 3.360f;
      break;
   case PTZControlType_Convex_CNT_20_MidDistanceCam:
      m_fTgtPosConvergeFactor = 0.25f;
      m_fTgtPosConvergeGamma  = 1.35f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.15f;
      m_fMaxZoomLogOffsetForConvergence = 0.20f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 1000.0f;
      m_nAdditionalAbsPosMovePeriod         = 1000;
      m_nMaxAbsPosMovePeriod                = 1200;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 4.0f;
      m_fContZoomSpeed_Slow = 1.0f;
      break;

   case PTZControlType_BestDigital_BVS_H2020R:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 2.0f;
      m_fTgtPosConvergeGamma  = 1.25f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.24f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 30.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 4.0f;
      m_fMinLog2OffsetForZoomAbsPosMove     = 2.5f;
      m_nAdditionalAbsPosMovePeriod         = 500;
      m_nMaxAbsPosMovePeriod                = 1500;
      m_bUseAbsZoomPosMove                  = TRUE;
      // 연속이동 제어
      m_fMaxContPanMoveAngleSpeed  = 10.0f;
      m_fMaxContTiltMoveAngleSpeed = 7.0f;
      m_fContZoomSpeed_Fast        = 3.0f;
      m_fContZoomSpeed_Slow        = 1.0f;
      break;
   case PTZControlType_BestDigital_BNC_5230HR_W:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 3.0f;
      m_fTgtPosConvergeGamma  = 1.35f;
      m_fMaxGammaSpeed        = 60;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.24f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 40.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 10.0f;
      m_fMinLog2OffsetForZoomAbsPosMove     = 2.5f;
      m_nAdditionalAbsPosMovePeriod         = 300;
      m_nMaxAbsPosMovePeriod                = 100;
      m_bUseAbsZoomPosMove                  = TRUE;
      // 연속이동 제어
      m_fMaxContPanMoveAngleSpeed  = 90.0f;
      m_fMaxContTiltMoveAngleSpeed = 15.0f;
      m_fContZoomSpeed_Fast        = 3.0f;
      m_fContZoomSpeed_Slow        = 1.0f;
      m_nContMoveStopWaitTime      = 100;
      break;
   case PTZControlType_BestDigital_XV8150IRD_2020WB:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 3.0f;
      m_fTgtPosConvergeGamma  = 1.35f;
      m_fMaxGammaSpeed        = 60;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.24f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 30.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 7.0f;
      m_fMinLog2OffsetForZoomAbsPosMove     = 2.5f;
      m_nAdditionalAbsPosMovePeriod         = 300;
      m_nMaxAbsPosMovePeriod                = 100;
      m_bUseAbsZoomPosMove                  = TRUE;
      // 연속이동 제어
      m_fMaxContPanMoveAngleSpeed  = 30.0f;
      m_fMaxContTiltMoveAngleSpeed = 15.0f;
      m_fContZoomSpeed_Fast        = 3.0f;
      m_fContZoomSpeed_Slow        = 1.0f;
      m_nContMoveStopWaitTime      = 100;
      break;
   // __SUPPORT_DONGYANG_CAMERA
   case PTZControlType_Dongyang_DY_IP020HD_SONY:
   case PTZControlType_Dongyang_DY_IP020HD_WONWOO:
   case PTZControlType_Dongyang_DY_IR2020HD_SONY:
   case PTZControlType_Dongyang_DY_IR2020HD_WONWOO:
   case PTZControlType_Dongyang_DY_IR3030HD_WONWOO:
   case PTZControlType_Dongyang_DY_IR2030HL_SONY:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 3.0f;
      m_fTgtPosConvergeGamma  = 1.25f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.20f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 20.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 2.5f;
      m_fMinLog2OffsetForZoomAbsPosMove     = 1.0f;
      m_nAdditionalAbsPosMovePeriod         = 500;
      m_nMaxAbsPosMovePeriod                = 0;
      m_bUseAbsZoomPosMove                  = TRUE;
      // 연속이동 제어
      m_fMaxContPanMoveAngleSpeed  = 15.0f;
      m_fMaxContTiltMoveAngleSpeed = 8.0f;
      m_fContZoomSpeed_Fast        = 3.0f;
      m_fContZoomSpeed_Slow        = 1.0f;
      break;

   case PTZControlType_FSNetworks_FS_IR203H:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 4.5f;
      m_fTgtPosConvergeGamma  = 1.45f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.20f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 15.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 7.0f;
      m_fMinLog2OffsetForZoomAbsPosMove     = 1.0f;
      m_nAdditionalAbsPosMovePeriod         = 100;
      m_nMaxAbsPosMovePeriod                = 0;
      m_bUseAbsZoomPosMove                  = TRUE;
      // 연속이동 제어
      m_fMaxContPanMoveAngleSpeed  = 15.0f;
      m_fMaxContTiltMoveAngleSpeed = 8.0f;
      m_fContZoomSpeed_Fast        = 3.0f;
      m_fContZoomSpeed_Slow        = 1.0f;
      break;
   case PTZControlType_FSNetworks_FS_IR303H:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 4.5f;
      m_fTgtPosConvergeGamma  = 1.45f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.20f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 15.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 7.0f;
      m_fMinLog2OffsetForZoomAbsPosMove     = 1.5f;
      m_nAdditionalAbsPosMovePeriod         = 100;
      m_nMaxAbsPosMovePeriod                = 0;
      m_bUseAbsZoomPosMove                  = TRUE;
      // 연속이동 제어
      m_fMaxContPanMoveAngleSpeed  = 15.0f;
      m_fMaxContTiltMoveAngleSpeed = 8.0f;
      m_fContZoomSpeed_Fast        = 3.0f;
      m_fContZoomSpeed_Slow        = 1.0f;
      break;
   case PTZControlType_FSNetworks_FS_IR306H:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 4.5f;
      m_fTgtPosConvergeGamma  = 1.45f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.20f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 15.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 7.0f;
      m_fMinLog2OffsetForZoomAbsPosMove     = 1.5f;
      m_nAdditionalAbsPosMovePeriod         = 100;
      m_nMaxAbsPosMovePeriod                = 0;
      m_bUseAbsZoomPosMove                  = TRUE;
      // 연속이동 제어
      m_fMaxContPanMoveAngleSpeed  = 15.0f;
      m_fMaxContTiltMoveAngleSpeed = 8.0f;
      m_fContZoomSpeed_Fast        = 3.0f;
      m_fContZoomSpeed_Slow        = 1.0f;
      break;
   case PTZControlType_FSNetworks_FS_IR307H:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor             = 3.0f;
      m_fTgtPosConvergeGamma              = 1.35f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence     = 0.04f;
      m_fMaxZoomLogOffsetForConvergence   = 0.20f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 15.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos  = 7.0f;
      m_fMinLog2OffsetForZoomAbsPosMove   = 1.5f;
      m_nAdditionalAbsPosMovePeriod       = 100;
      m_nMaxAbsPosMovePeriod              = 0;
      m_bUseAbsZoomPosMove                = TRUE;
      // 연속이동 제어
      m_fMaxContPanMoveAngleSpeed         = 40.0f;
      m_fMaxContTiltMoveAngleSpeed        = 20.0f;
      m_fContZoomSpeed_Fast               = 3.0f;
      m_fContZoomSpeed_Slow               = 1.0f;
      break;
   case PTZControlType_SamsungTechWin_iPOLiS_SNP_5300H:
   case PTZControlType_SamsungTechWin_iPOLiS_SNP_6200H:
   case PTZControlType_SamsungTechWin_iPOLiS_SNP_6200RH:
   case PTZControlType_SamsungTechWin_iPOLiS_SNP_3371H:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 2.8f;
      m_fTgtPosConvergeGamma  = 1.35f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence     = 0.12f;
      m_fMaxZoomLogOffsetForConvergence   = 0.24f;
      m_fMaxZoomLogOffsetForSlowZoomSpeed = 1.2f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 20.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 3.0f;
      m_fMinLog2OffsetForZoomAbsPosMove     = MC_VLV; // 중요: 6200RH 카메라의 경우 절대각 이동방식으로 줌을 자주 제어하는 경우
      // 실제 줌 이동이 거이 없는 경우가 발생하여 줌 변화 시에는
      // 절대각 제어가 되지 않도록 처리하였다.
      m_nAdditionalAbsPosMovePeriod = 150;
      m_nMaxAbsPosMovePeriod        = 1000;
      m_bUseAbsZoomPosMove          = FALSE;
      // 연속이동 제어
      m_fMaxContPanMoveAngleSpeed  = 20.0f;
      m_fMaxContTiltMoveAngleSpeed = 10.0f;
      m_fContZoomSpeed_Fast        = 2.0f;
      if( PTZControlType_SamsungTechWin_iPOLiS_SNP_6200H == nPTZCtrlType || PTZControlType_SamsungTechWin_iPOLiS_SNP_6200RH == nPTZCtrlType )
         m_fContZoomSpeed_Fast = 3.0f;
      m_fContZoomSpeed_Slow    = 1.0f;
      break;
   case PTZControlType_SamsungTechWin_iPOLiS_SNP_6320H:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 3.5f;
      m_fTgtPosConvergeGamma  = 1.35f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence     = 0.12f;
      m_fMaxZoomLogOffsetForConvergence   = 0.24f;
      m_fMaxZoomLogOffsetForSlowZoomSpeed = 1.2f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 15.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 3.0f;
      m_nAdditionalAbsPosMovePeriod         = 150;
      m_nMaxAbsPosMovePeriod                = 1000;
      // 연속이동 제어
      m_fMaxContPanMoveAngleSpeed  = 20.0f;
      m_fMaxContTiltMoveAngleSpeed = 10.0f;
      m_fContZoomSpeed_Fast        = 3.0f;
      m_fContZoomSpeed_Slow        = 1.0f;
      break;

   case PTZControlType_SamsungTechWin_iPOLiS_SNP_6320RH:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 3.5f;
      m_fTgtPosConvergeGamma  = 1.35f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence     = 0.12f;
      m_fMaxZoomLogOffsetForConvergence   = 0.24f;
      m_fMaxZoomLogOffsetForSlowZoomSpeed = 1.2f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 15.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 3.0f;
      m_nAdditionalAbsPosMovePeriod         = 150;
      m_nMaxAbsPosMovePeriod                = 1000;
      //m_bUseAbsZoomPosMove                = FALSE;
      // 연속이동 제어
      m_fMaxContPanMoveAngleSpeed  = 20.0f;
      m_fMaxContTiltMoveAngleSpeed = 10.0f;
      m_fContZoomSpeed_Fast        = 3.0f;
      m_fContZoomSpeed_Slow        = 1.0f;
      break;

   ////////// 주둔지 경계용 카메라 IR, 레이저, 열화상
   case PTZControlType_HanwhaTechWin_iPOLiS_SNZ_6320_CAMP:
   case PTZControlType_HanwhaTechWin_iPOLiS_SNZ_6320_CAMP2:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 1.75f;
      m_fTgtPosConvergeGamma  = 1.35f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence     = 0.15f;
      m_fMaxZoomLogOffsetForConvergence   = 0.34f;
      m_fMaxZoomLogOffsetForSlowZoomSpeed = 1.2f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 15.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 3.0f;
      m_nAdditionalAbsPosMovePeriod         = 150;
      m_nMaxAbsPosMovePeriod                = 1000;
      //m_bUseAbsZoomPosMove                = FALSE;
      // 연속이동 제어
      m_fMaxContPanMoveAngleSpeed  = 20.0f;
      m_fMaxContTiltMoveAngleSpeed = 10.0f;
      m_fContZoomSpeed_Fast        = 3.0f;
      m_fContZoomSpeed_Slow        = 1.0f;
      break;

   case PTZControlType_SamsungTechWin_iPOLiS_SNZ_5200_Shinwoo_SPT_7080B:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor   = 2.2f;
      m_fTgtPosConvergeGamma    = 1.35f;
      m_fTargetConvergenceLimit = 4.0f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.25f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 9.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 2.0f;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 8.0f;
      m_fContZoomSpeed_Slow = 4.0f;

      m_fMaxContPanMoveAngleSpeed  = 20.0f;
      m_fMaxContTiltMoveAngleSpeed = 10.0f;
      break;

   case PTZControlType_SamsungTechWin_iPOLiS_SNZ_6320_Shinwoo_SPT_7080B:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor   = 2.2f;
      m_fTgtPosConvergeGamma    = 1.35f;
      m_fTargetConvergenceLimit = 4.0f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.25f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 9.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 2.0f;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 8.0f;
      m_fContZoomSpeed_Slow = 4.0f;

      m_fMaxContPanMoveAngleSpeed  = 20.0f;
      m_fMaxContTiltMoveAngleSpeed = 10.0f;
      break;

   case PTZControlType_Panasonic_WVSC384:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 1.25f;
      m_fTgtPosConvergeGamma  = 1.35f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.25f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 7.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 2.0f;
      // 연속이동 제어
      m_fMaxContPanMoveAngleSpeed  = 30.0f;
      m_fMaxContTiltMoveAngleSpeed = 15.0f;
      m_fContZoomSpeed_Fast        = 4.0f;
      m_fContZoomSpeed_Slow        = 1.0f;
      break;

   case PTZControlType_Panasonic_WVSC385:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor   = 1.2f;
      m_fTgtPosConvergeGamma    = 1.35f;
      m_fTargetConvergenceLimit = 4.0f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.25f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 9.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 2.0f;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 4.0f;
      m_fContZoomSpeed_Slow = 1.0f;
      break;

   case PTZControlType_Panasonic_WVNS202A:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 1.2f;
      m_fTgtPosConvergeGamma  = 1.35f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.25f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 9.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 2.0f;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 4.0f;
      m_fContZoomSpeed_Slow = 1.0f;
      break;

   case PTZControlType_UDP_CND_20ZHO:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor   = 1.2f;
      m_fTgtPosConvergeGamma    = 1.35f;
      m_fTargetConvergenceLimit = 4.0f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.25f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 9.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 2.0f;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 4.0f;
      m_fContZoomSpeed_Slow = 1.0f;
      break;

   case PTZControlType_Truen_TN_B220CS_Shinwoo_SPT_7080B:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor   = 2.2f;
      m_fTgtPosConvergeGamma    = 1.35f;
      m_fTargetConvergenceLimit = 4.0f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.25f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 9.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 2.0f;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 8.0f;
      m_fContZoomSpeed_Slow = 4.0f;

      m_fMaxContPanMoveAngleSpeed  = 20.0f;
      m_fMaxContTiltMoveAngleSpeed = 10.0f;
      break;

   case PTZControlType_Visionhitech_BM2TI_IR:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 3.0f;
      m_fTgtPosConvergeGamma  = 1.4f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.25f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 15.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 5.0f;
      m_fMinLog2OffsetForZoomAbsPosMove     = 2.8f;
      m_nAdditionalAbsPosMovePeriod         = 150;
      m_bUseAbsZoomPosMove                  = TRUE;
      // 연속이동 제어
      m_fMaxContPanMoveAngleSpeed  = 60.0f;
      m_fMaxContTiltMoveAngleSpeed = 30.0f;
      m_fContZoomSpeed_Fast        = 2.0f;
      m_fContZoomSpeed_Slow        = 1.0f;
      break;

   case PTZControlTYpe_Pelco_D5230: // (mkjang-140627)
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 3.5f;
      m_fTgtPosConvergeGamma  = 1.25f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.25f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 15.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 7.0f;
      m_fMinLog2OffsetForZoomAbsPosMove     = 0.75f;
      m_bUseAbsZoomPosMove                  = TRUE;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 3.0f;
      m_fContZoomSpeed_Slow = 1.0f;
      break;

   case PTZControlTYpe_Pelco_S6220:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 3.5f;
      m_fTgtPosConvergeGamma  = 1.25f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.50f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 15.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 7.0f;
      m_fMinLog2OffsetForZoomAbsPosMove     = 2.8f;
      m_nMaxAbsPosMovePeriod                = 0;
      m_bUseAbsZoomPosMove                  = TRUE;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 1.0f;
      m_fContZoomSpeed_Slow = 1.0f;
      break;

   case PTZControlType_Vivotek_SD8363E: // (mkjang-140827)
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 3.0;
      m_fTgtPosConvergeGamma  = 1.35f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.25f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 30.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 5.0f;
      m_fMinLog2OffsetForZoomAbsPosMove     = 0.75f;
      m_nAdditionalAbsPosMovePeriod         = 100;
      m_bUseAbsZoomPosMove                  = FALSE;
      // 연속이동 제어
      m_fMaxContPanMoveAngleSpeed  = 30;
      m_fMaxContTiltMoveAngleSpeed = 15;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 5.0f;
      m_fContZoomSpeed_Slow = 1.0f;
      break;

   case PTZControlType_Vivotek_SD9362EHL:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 1.80f;
      m_fTgtPosConvergeGamma  = 1.35f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.058f;
      m_fMaxZoomLogOffsetForConvergence = 0.55f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 10.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 10.0f;
      m_fMinLog2OffsetForZoomAbsPosMove     = 3.0f;
      m_nAdditionalAbsPosMovePeriod         = 300;
      m_bUseAbsZoomPosMove                  = FALSE;
      // 연속이동 제어
      m_fMaxContPanMoveAngleSpeed  = 126;
      m_fMaxContTiltMoveAngleSpeed = 50;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 5.0f;
      m_fContZoomSpeed_Slow = 1.0f;

      //g_ParameterLoader_ContAbsPosTrack.Initialize(CString(g_szVCMConfigDirectory) + "\\__ContAbsPosTrkParams_VIVOTEK_SD9362.xml", (IParameterInputOutput*)this);
      break;

   case PTZControlType_IDIS_MNC221SH: // (mkjang-150203)
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 7.0;
      m_fTgtPosConvergeGamma  = 1.35f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.1f;
      m_fMaxZoomLogOffsetForConvergence = 0.25f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 10.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 25.0f;
      m_fMinLog2OffsetForZoomAbsPosMove     = 2.75f;
      m_nAdditionalAbsPosMovePeriod         = 150;
      m_nMaxAbsPosMovePeriod                = 1000;
      m_bUseAbsZoomPosMove                  = FALSE;
      // 연속이동 제어
      m_fMaxContPanMoveAngleSpeed  = 20;
      m_fMaxContTiltMoveAngleSpeed = 20;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 16.0f;
      m_fContZoomSpeed_Slow = 1.0f;
      break;
   // Probe Digital
   case PTZControlType_ProbeDigital_PRI_H3200:
      m_fTgtPosConvergeFactor = 2.3;
      m_fTgtPosConvergeGamma  = 1.35f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.15f;
      m_fMaxZoomLogOffsetForConvergence = 0.25f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 10.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 25.0f;
      m_fMinLog2OffsetForZoomAbsPosMove     = 2.75f;
      m_nAdditionalAbsPosMovePeriod         = 150;
      m_nMaxAbsPosMovePeriod                = 300;
      m_bUseAbsZoomPosMove                  = FALSE;
      // 연속이동 제어
      m_fMaxContPanMoveAngleSpeed  = 20;
      m_fMaxContTiltMoveAngleSpeed = 20;
      // 연속이동 제어
      m_fContZoomSpeed_Fast   = 16.0f;
      m_fContZoomSpeed_Slow   = 1.0f;
      m_nContMoveStopWaitTime = 100;

      //g_ParameterLoader_ContAbsPosTrack.Initialize (CString (g_szVCMConfigDirectory) + "\\__ContAbsPosTrkParams_Probe_PRIH3200.xml", (IParameterInputOutput*)this);

      break;

   //LG 수렴
   case PTZControlType_LG_LNP_3022:

      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 3.00f;
      m_fTgtPosConvergeGamma  = 1.35f;
      m_fMaxGammaSpeed        = 60.00f;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.25f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 75.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 0.8f;
      m_fMinLog2OffsetForZoomAbsPosMove     = MC_VLV;
      m_nAdditionalAbsPosMovePeriod         = 100;
      m_nMaxAbsPosMovePeriod                = 100;
      // 연속이동 제어
      m_fContZoomSpeed_Fast = 10.0f;
      m_fContZoomSpeed_Slow = 1.0f;

      m_fMaxContPanMoveAngleSpeed  = 40.0f;
      m_fMaxContTiltMoveAngleSpeed = 25.0f;
      m_nContMoveStopWaitTime      = 100;

      //  g_ParameterLoader_ContAbsPosTrack.Initialize (CString (g_szVCMConfigDirectory) + "\\__ContAbsPosTrkParams_LG_LNP3022.xml", (IParameterInputOutput*)this);
      break;

   case PTZControlType_Honeywell_HISD_2201WE_IR:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 3.0f;
      m_fTgtPosConvergeGamma  = 1.35f;
      m_fMaxGammaSpeed        = 60;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.50f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 6.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 7.0f;
      //m_fMinLog2OffsetForZoomAbsPosMove   = 2.5f;
      m_nAdditionalAbsPosMovePeriod = 100;
      m_nMaxAbsPosMovePeriod        = 100;
      m_bUseAbsZoomPosMove          = TRUE;
      // 연속이동 제어
      m_fMaxContPanMoveAngleSpeed  = 30.0f;
      m_fMaxContTiltMoveAngleSpeed = 15.0f;
      m_fContZoomSpeed_Fast        = 3.0f;
      m_fContZoomSpeed_Slow        = 1.0f;
      m_nContMoveStopWaitTime      = 100;
      break;

   case PTZControlType_Honeywell_HNP_232WI:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 2.3f;
      m_fTgtPosConvergeGamma  = 1.35f;
      m_fMaxGammaSpeed        = 60;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.15f;
      m_fMaxZoomLogOffsetForConvergence = 1.25f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = 40.0f;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 10.0f;
      m_fMinLog2OffsetForZoomAbsPosMove     = 3.0f;
      m_nAdditionalAbsPosMovePeriod         = 100;
      m_nMaxAbsPosMovePeriod                = 100;
      m_bUseAbsZoomPosMove                  = TRUE;
      // 연속이동 제어
      m_fMaxContPanMoveAngleSpeed  = 8.0f;
      m_fMaxContTiltMoveAngleSpeed = 8.0f;
      m_fContZoomSpeed_Fast        = 7.0f;
      m_fContZoomSpeed_Slow        = 1.0f;
      m_nContMoveStopWaitTime      = 100;
      // g_ParameterLoader_ContAbsPosTrack.Initialize (CString (g_szVCMConfigDirectory) + "\\__ContAbsPosTrkParams_HoneyWell.xml", (IParameterInputOutput*)this);
      break;

   case PTZControlType_YoungKook_YSDIRMP20S:
      // 목표 위치 수렴속도 제어
      m_fTgtPosConvergeFactor = 3.5f;
      m_fTgtPosConvergeGamma  = 1.35f;
      m_fMaxGammaSpeed        = 60;
      // 목표 위치 수렴여부 판단
      m_fMaxAngleOffsetForConvergence   = 0.12f;
      m_fMaxZoomLogOffsetForConvergence = 0.50f;
      // 절대위치 이동
      m_fMinAngleOffsetForPanTiltAbsPosMove = MC_VLV;
      m_fPanTiltOffsetRatioForGotoAbsPos    = 8.0f;
      m_fMinLog2OffsetForZoomAbsPosMove     = 3.0f;
      m_nAdditionalAbsPosMovePeriod         = 100;
      m_nMaxAbsPosMovePeriod                = 100;
      m_bUseAbsZoomPosMove                  = TRUE;
      // 연속이동 제어
      m_fMaxContPanMoveAngleSpeed  = 30.0f;
      m_fMaxContTiltMoveAngleSpeed = 30.0f;
      m_fContZoomSpeed_Fast        = 1.0f;
      m_fContZoomSpeed_Slow        = 1.0f;
      m_nContMoveStopWaitTime      = 100;
      //g_ParameterLoader_ContAbsPosTrack.Initialize (CString (g_szVCMConfigDirectory) + "\\__ContAbsPosTrkParams_YoungKook.xml", (IParameterInputOutput*)this);
      break;
   }
}

void CContAbsPosTrack::GotoTgtPtzPos()
{
   // PTZ 이동 상태를 체크한다.
   if( !( m_pPTZCtrl->m_nCapabilities & ( PTZCtrlCap_FastAbsPosMoveNotSupported | PTZCtrlCap_BadAbsPosMoveMoveCam ) ) ) {
      if( !( m_nState & ( ContAbsPosMvTrk_State_StopContAndGotoAbsPosOngoing | ContAbsPosMvTrk_State_ConstantAngleSpeed | ContAbsPosMvTrk_State_ConstantImageScrollSpeed ) ) ) {
         if( GetTickCount() - m_nGotoAbsPosTime > (uint)m_nMaxAbsPosMovePeriod ) {
            if( m_fPanTiltOffsetRatioForGotoAbsPos != MC_VLV ) {
               float fMaxPanOffsetAngle, fMaxTiltOffsetAngle;
               m_pPTZCtrl->GetMaxOffsetAngle( fMaxPanOffsetAngle, fMaxTiltOffsetAngle );
               // 화면 크기의 오프셋 각 보다 이동해야할 각이 크면 절대각 이동을 한다.
               if( fabs( m_fvOffsetPos.P ) > fMaxPanOffsetAngle * m_fPanTiltOffsetRatioForGotoAbsPos || fabs( m_fvOffsetPos.T ) > fMaxTiltOffsetAngle * m_fPanTiltOffsetRatioForGotoAbsPos || fabs( m_fvOffsetPos.Z ) > m_fMinLog2OffsetForZoomAbsPosMove ) {
                  m_nState |= ContAbsPosMvTrk_State_GotoABSPosToBeBegun;
                  /*
                     logd ("Goto Abs Pos - Tracking Object is\n");
                     if (fabs (m_fvOffsetPos.P) > fMaxPanOffsetAngle * m_fPanTiltOffsetRatioForGotoAbsPos)
                        logd ("    Pan Diff!!\n");
                     if (fabs (m_fvOffsetPos.T) > fMaxTiltOffsetAngle * m_fPanTiltOffsetRatioForGotoAbsPos)
                        logd ("    Tilt Diff!!\n");
                     if (fabs (m_fvOffsetPos.Z) > m_fMinLog2OffsetForZoomAbsPosMove)
                        logd ("    Zoom Diff - m_fvOffsetPos.Z:%f m_fMinLog2OffsetForZoomAbsPosMove:%f!!\n", fabs (m_fvOffsetPos.Z), m_fMinLog2OffsetForZoomAbsPosMove);
                     */
               }
            }
            if( !( m_nState & ContAbsPosMvTrk_State_GotoABSPosToBeBegun ) ) {
               // 이동해야 할 각이 지정된 각 보다 크다면 절대각 이동을 한다.
               if( fabs( m_fOffsetAngle ) > m_fMinAngleOffsetForPanTiltAbsPosMove * MC_DEG2RAD ) {
                  m_nState |= ContAbsPosMvTrk_State_GotoABSPosToBeBegun;
               }
            }
            if( !( m_nState & ContAbsPosMvTrk_State_GotoABSPosToBeBegun ) ) {
               if( m_fMaxZoomFactorForAbsPosMoveTracking >= m_fvCurPos.Z ) {
                  m_nState |= ContAbsPosMvTrk_State_GotoABSPosToBeBegun;
                  logd( "m_fMaxZoomFactorForAbsPosMoveTracking(%f) >= m_fvCurPos.Z(%f)\n", m_fMaxZoomFactorForAbsPosMoveTracking, m_fvCurPos.Z );
               }
            }
         }
      }
   }

   if( m_nState & ContAbsPosMvTrk_State_GotoABSPosToBeBegun ) {
      // 연속이동을 멈춘다.
      if( !( m_nState & ( ContAbsPosMvTrk_State_WaitContPTZMoveStop | ContAbsPosMvTrk_State_ContPTZMoveStopped ) ) ) {
         m_pPTZCtrl->_ContPTZMove( FPTZVector( 0.0f, 0.0f, 0.0f ) );
         m_nContPTZStopCmdSendTime = GetTickCount();
         m_nState |= ContAbsPosMvTrk_State_WaitContPTZMoveStop;
      }
      // 연속이동을 멈출때까지 일정시간 대기한다.
      if( m_nState & ContAbsPosMvTrk_State_WaitContPTZMoveStop ) {
         // 연속이동을 멈춘 후 절대각이동까지 일정시간 쉰다.
         // 삼성카메라PTZ의 경우 이 주기가 짧으면 PTZ위치값이 틀어지는 현상이 생긴다.
         if( GetTickCount() > m_nContPTZStopCmdSendTime + m_nContMoveStopWaitTime ) {
            m_nState |= ContAbsPosMvTrk_State_ContPTZMoveStopped;
            m_nState &= ~ContAbsPosMvTrk_State_WaitContPTZMoveStop;
         }
      }
      // 절대각이동을 한다.
      if( m_nState & ContAbsPosMvTrk_State_ContPTZMoveStopped ) {
         if( !( m_nState & ( ContAbsPosMvTrk_State_WaitAbsPTZMoveStop ) ) ) {
            float fTgtZoom = m_pPTZCtrl->GetAbsPos().Z;
            if( m_bUseAbsZoomPosMove )
               fTgtZoom = m_fvTgtPos.Z;
            FPTZVector tgtpos( m_fvTgtPos.P, m_fvTgtPos.T, fTgtZoom );
            m_nAbsPosMovePeriod                               = m_pPTZCtrl->GetPTZMovePeriodByTargetPos( tgtpos ) + m_nAdditionalAbsPosMovePeriod;
            if( m_nAbsPosMovePeriod < 0 ) m_nAbsPosMovePeriod = 0;
            m_pPTZCtrl->ConvertPanTiltOrientationRealToHW( tgtpos.P, tgtpos.T );
            m_pPTZCtrl->_GotoAbsPos( tgtpos );
            m_nGotoAbsPosTime = GetTickCount();
            //logd ("ContAbsPosTrack  - m_nAbsPosMovePeriod:%d\n", m_nAbsPosMovePeriod);
            m_fvContMoveVecHW( 0.0f, 0.0f, 0.0f );
            m_nState |= ( ContAbsPosMvTrk_State_WaitAbsPTZMoveStop );
         }
      }
      // 절대각 이동으로 목표지점까지 이동할 때까지 대기한다.
      if( m_nState & ContAbsPosMvTrk_State_WaitAbsPTZMoveStop ) {
         BOOL bAbsPosMoveStopped = FALSE;
         // 절대각 이동 명령 이후 일정시간이 지나면 절대각 이동 상태를 해제한다.
         if( GetTickCount() > m_nGotoAbsPosTime + m_nAbsPosMovePeriod ) {
            bAbsPosMoveStopped = TRUE;
            switch( m_pPTZCtrl->m_nPTZControlType ) {
            case PTZControlType_Dahua_NS_SD138:
            case PTZControlType_Dahua_NS_SD200IR:
            case PTZControlType_Dahua_NS_SD220IR:
            case PTZControlType_Dahua_NS_SD300IR:
               m_pPTZCtrl->_ContPTZMove( FPTZVector( 0.0f, 0.0f, 0.0f ) );
               break;
            }
         }
         if( bAbsPosMoveStopped )
            m_nState &= ~( ContAbsPosMvTrk_State_GotoABSPosToBeBegun | ContAbsPosMvTrk_State_StopContAndGotoAbsPosOngoing );
      }
   }
   if( !( m_nState & ContAbsPosMvTrk_State_StopContAndGotoAbsPosOngoing ) ) {
      if( !( m_pPTZCtrl->m_nState & PTZ_State_Stopped ) ) {
         m_pPTZCtrl->_ContPTZMove( m_fvContMoveVecHW );
      }
   }
}

void CContAbsPosTrack::MainProc()
{
   m_fvTgtPos = m_pPTZCtrl->GetOffsetTgtAbsPos();
   double dfOffsetAngleInRad; // 중요 : 단위는 Radian

   float fMinOffsetAngleSpeedForAvg = 90.0f; // 평균을 계산할 때 fMinOffsetAngleSpeedForAvg 보다 낮은 속도들로 부터 평균을 얻는다.
   // 갑자기 많이 이동할 때에도 속도평균을 낼때 포함시키면 속도 평균값이 너무 커지는 문제를 막는다.
   float fMaxOffsetAngleForAvgReset = 60.0f; // 본 값보다 큰 각을 이동하는 경우에는 이전 속도값들을 리셋한다.
   // 갑자기 큰 각을 이동하는 경우는 물체간의 전환의 경우이기 때문에
   // 이전 속도 평균을 반영하는 것은 맞지 않다.
   // 현재 Pan,Tilt의 각속도를 계산한다.
   Quaternion qtVelocity;
   qtVelocity = GetQuaternion( m_fvTgtPos, m_fvPrvPos );
   FVector3D pvRotationAxis;
   qtVelocity.ToAngleAxis( dfOffsetAngleInRad, pvRotationAxis );
   qtVelocity.FromAngleAxis( dfOffsetAngleInRad * m_fFrameRate, pvRotationAxis ); // 1초 동안의 Offset Angle로 변경한다.

   // 현재 Pan,Tilt의 평균 각속도를 계산한다.
   m_qtVelocity = Quaternion::ZERO;
   if( dfOffsetAngleInRad * MC_RAD2DEG * m_fFrameRate < fMinOffsetAngleSpeedForAvg ) // 급격한 속도변화는 제외
      m_qtVelocity = qtVelocity;
   if( dfOffsetAngleInRad * MC_RAD2DEG > fMaxOffsetAngleForAvgReset ) // 큰각을 이동하는 경우에는 이전 속도들을 리셋
      m_nState |= ContAbsPosMvTrk_State_AvgTgtPosVelocityToBeReset;
   if( m_nState & ContAbsPosMvTrk_State_AvgTgtPosVelocityToBeReset ) {
      m_ContAbsPosTrkLogItemQueue.ResetAvgVelocity();
      m_nState &= ~ContAbsPosMvTrk_State_AvgTgtPosVelocityToBeReset;
   }

   m_ContAbsPosTrkLogItemQueue.CalcAvgVelocity( m_pPTZCtrl->m_nPTZCmdExecDelay, m_qtAvgVelocity );
   double dfAvgAngleVelocity;
   FVector3D pvAvgRotationAxis;
   m_qtAvgVelocity.ToAngleAxis( dfAvgAngleVelocity, pvAvgRotationAxis );

   // Pan, Tilt, Zoom의 연속이동속도를 계산한다.
   m_fvCurPos          = m_pPTZCtrl->GetAbsPos();
   FPTZVector fvTgtPos = m_pPTZCtrl->GetOffsetTgtAbsPos();

   // Tilt 각이 90도가 넘으면 Tilt 및 Pan각을 보정한다.
   if( m_fvCurPos.T > 90.0f ) {
      m_fvCurPos.T = 180.0f - m_fvCurPos.T;
      m_fvCurPos.P += 180.0f;
      if( m_fvCurPos.P > 360.0f ) m_fvCurPos.P -= 360.0f;
   }

   // Pan, Tilt 각의 변위량을 계산한다.
   m_fvOffsetPos.P = GetOffsetAngleInDegree( m_fvCurPos.P, fvTgtPos.P );
   m_fvOffsetPos.T = GetOffsetAngleInDegree( m_fvCurPos.T, fvTgtPos.T );
   m_fvOffsetPos.Z = log2( fvTgtPos.Z ) - log2( m_fvCurPos.Z ); // 줌의 변위량은 log2를 취한 값으로 한다.
   // Pan, Tilt 각의 변위량에 감마보정을 시킨다.
   if( m_fTgtPosConvergeGamma != 1.0f ) {
      FPTZVector m_fvOffsetPosGamma;
      m_fvOffsetPosGamma.P = pow( fabs( m_fvOffsetPos.P ), m_fTgtPosConvergeGamma ) / pow( fabs( m_fMaxGammaSpeed ), m_fTgtPosConvergeGamma ) * m_fMaxGammaSpeed;
      m_fvOffsetPosGamma.T = pow( fabs( m_fvOffsetPos.T ), m_fTgtPosConvergeGamma ) / pow( fabs( m_fMaxGammaSpeed ), m_fTgtPosConvergeGamma ) * m_fMaxGammaSpeed;
      if( m_fvOffsetPos.P < 0.0f ) m_fvOffsetPosGamma.P *= -1.0f;
      if( m_fvOffsetPos.T < 0.0f ) m_fvOffsetPosGamma.T *= -1.0f;
      m_fvOffsetPos.P = m_fvOffsetPosGamma.P;
      m_fvOffsetPos.T = m_fvOffsetPosGamma.T;
   }

   // 목표 속도를 구한다.
   Quaternion qtPA, qtTA, qtCurPTZPos, qtOffset, qtTgtVel;
   //    현재 PTZ 위치로 회전하는 쿼터니온을 구한다.
   qtPA.FromAngleAxis( m_fvCurPos.P * MC_DEG2RAD, g_vtUnitY );
   qtTA.FromAngleAxis( m_fvCurPos.T * MC_DEG2RAD, g_vtUnitX );
   qtCurPTZPos = qtPA * qtTA;

   //    현재 위치에서 목표 PTZ 위치로 회전하는 쿼터니온을 구한다.
   FVector3D fvAxis;
   qtPA.FromAngleAxis( m_fvOffsetPos.P * MC_DEG2RAD, g_vtUnitY );
   qtTA.FromAngleAxis( m_fvOffsetPos.T * MC_DEG2RAD, g_vtUnitX );
   qtOffset = qtPA * qtTA;

   //    목표 PTZ 위치로 수렴하는 속도와 평균 절대각 이동 속도를 보간한다.
   double dfAvgAngleSpeed, dfTgtAngleSpeed;
   double dfFacter             = 0.01;
   float fTgtPosConvergeFactor = m_fTgtPosConvergeFactor;
   if( m_nState & ( ContAbsPosMvTrk_State_ConstantAngleSpeed | ContAbsPosMvTrk_State_ConstantImageScrollSpeed ) )
      fTgtPosConvergeFactor *= 4.0f;
   qtOffset.ToAngleAxis( m_fOffsetAngle, fvAxis ); // Offset 각
   dfOffsetAngleInRad                                  = m_fOffsetAngle * m_fTgtPosConvergeFactor * dfFacter;
   if( dfOffsetAngleInRad > MC_PI ) dfOffsetAngleInRad = MC_PI;
   qtOffset.FromAngleAxis( dfOffsetAngleInRad, fvAxis );
   m_qtAvgVelocity.ToAngleAxis( dfAvgAngleSpeed, fvAxis ); // 평균 각속도
   dfAvgAngleSpeed *= m_fAngleSpeedRatio * dfFacter;
   if( dfAvgAngleSpeed > MC_PI ) dfAvgAngleSpeed = MC_PI;
   m_qtAvgVelocity.FromAngleAxis( dfAvgAngleSpeed, fvAxis );
   if( m_nState & ( ContAbsPosMvTrk_State_ConstantAngleSpeed | ContAbsPosMvTrk_State_ConstantImageScrollSpeed ) )
      qtTgtVel = qtOffset; // 목표 속도 = Offset각
   else
      qtTgtVel = qtOffset * m_qtAvgVelocity; // 목표 속도 = Offset각 + 평균 각속도
   qtTgtVel.ToAngleAxis( dfTgtAngleSpeed, fvAxis );
   if( dfTgtAngleSpeed > MC_PI ) dfTgtAngleSpeed = MC_PI;
   qtTgtVel.FromAngleAxis( dfTgtAngleSpeed, fvAxis );

   // 카메라 중심 벡터를 현재 속도로 회전 시키고 이동된 벡터로 부터 Pan Tilt Angle을 얻는다.
   FPTZVector fvEstOffsetPos, fvEstTgtPos;
   FVector3D fvCamCenter( 0.0, 0.0, 1.0 );
   FVector3D fvTgtCamCenter;
   fvTgtCamCenter   = qtCurPTZPos * qtTgtVel * fvCamCenter;
   fvEstTgtPos.P    = -float( MC_RAD2DEG * atan2( fvTgtCamCenter.X, fvTgtCamCenter.Z ) );
   fvEstTgtPos.T    = float( MC_RAD2DEG * asin( fvTgtCamCenter.Y ) );
   fvEstOffsetPos.P = float( GetOffsetAngleInDegree( m_fvCurPos.P, fvEstTgtPos.P ) / dfFacter );
   fvEstOffsetPos.T = float( GetOffsetAngleInDegree( m_fvCurPos.T, fvEstTgtPos.T ) / dfFacter );

   /*
   float fMaxPanOffset, fMaxTiltOffset;
   m_pPTZCtrl->GetMaxOffsetAngle (fMaxPanOffset, fMaxTiltOffset);
   m_fTargetConvergenceLimit = 10000.0f;
   if (fabs (fvEstOffsetPos.P) > fabs (fMaxPanOffset) * m_fTargetConvergenceLimit) {
      if (fvEstOffsetPos.P < 0.0f) fvEstOffsetPos.P = -fabs (fMaxPanOffset);
      else                         fvEstOffsetPos.P =  fabs (fMaxPanOffset);
   }
   if (fabs (fvEstOffsetPos.T) > fabs (fMaxTiltOffset) * m_fTargetConvergenceLimit) {
      if (fvEstOffsetPos.T < 0.0f) fvEstOffsetPos.T = -fabs (fMaxTiltOffset);
      else                         fvEstOffsetPos.T =  fabs (fMaxTiltOffset);
   }
   */

   // Zoom 속도를 계산한다.
   float fZoomSpeed = 0.0f;
   if( FALSE == ( m_pPTZCtrl->m_nCapabilities & PTZCtrlCap_ContPTAndAbsZoomMoveCam ) ) {
      float fAbsZoomLogDiff = fabs( m_fvOffsetPos.Z );
      if( fAbsZoomLogDiff > m_fMaxZoomLogOffsetForConvergence ) {
         float fContZoomSpeed                                                       = m_fContZoomSpeed_Fast;
         if( fAbsZoomLogDiff < m_fMaxZoomLogOffsetForSlowZoomSpeed ) fContZoomSpeed = m_fContZoomSpeed_Slow;
         if( m_nState & ( ContAbsPosMvTrk_State_ConstantAngleSpeed | ContAbsPosMvTrk_State_ConstantImageScrollSpeed ) )
            fContZoomSpeed                       = 1;
         if( m_fvOffsetPos.Z < 0.0f ) fZoomSpeed = -fContZoomSpeed;
         if( m_fvOffsetPos.Z > 0.0f ) fZoomSpeed = fContZoomSpeed;
      }
   }
   //logd ("ZoomVel:%5.1f  TgtZ:%5.1f  CutZ:%5.1f\n", fZoomSpeed, fvTgtPos.Z, m_fvCurPos.Z);

   // 연속 Pan, Tilt 이동 속도 제한.
   if( fabs( fvEstOffsetPos.P ) > m_fMaxContPanMoveAngleSpeed )
      fvEstOffsetPos.P = m_fMaxContPanMoveAngleSpeed * ( fvEstOffsetPos.P / fabs( fvEstOffsetPos.P ) );
   if( fabs( fvEstOffsetPos.T ) > m_fMaxContTiltMoveAngleSpeed )
      fvEstOffsetPos.T = m_fMaxContTiltMoveAngleSpeed * ( fvEstOffsetPos.T / fabs( fvEstOffsetPos.T ) );

   //
   if( m_nState & ContAbsPosMvTrk_State_ConstantAngleSpeed ) {
      if( fabs( fvEstOffsetPos.P ) > m_fConstantContPanMoveAngleSpeed )
         fvEstOffsetPos.P = m_fConstantContPanMoveAngleSpeed * ( fvEstOffsetPos.P / fabs( fvEstOffsetPos.P ) );
      if( fabs( fvEstOffsetPos.T ) > m_fConstantContTiltMoveAngleSpeed )
         fvEstOffsetPos.T = m_fConstantContTiltMoveAngleSpeed * ( fvEstOffsetPos.T / fabs( fvEstOffsetPos.T ) );
   }
   if( m_nState & ContAbsPosMvTrk_State_ConstantImageScrollSpeed ) {
      float fPanOffsetAngle, fTiltOffsetAngle;
      m_pPTZCtrl->GetMaxOffsetAngle( fPanOffsetAngle, fTiltOffsetAngle );
      float fConstantContMoveAngleSpeed = fPanOffsetAngle * m_fConstantImageScrollSpeed;
      if( fabs( fvEstOffsetPos.P ) > fConstantContMoveAngleSpeed )
         fvEstOffsetPos.P = fConstantContMoveAngleSpeed * ( fvEstOffsetPos.P / fabs( fvEstOffsetPos.P ) );
      if( fabs( fvEstOffsetPos.T ) > fConstantContMoveAngleSpeed )
         fvEstOffsetPos.T = fConstantContMoveAngleSpeed * ( fvEstOffsetPos.T / fabs( fvEstOffsetPos.T ) );
   }

   // Pan, Tilt 속도를 계산한다.
   float fPanVel  = -fvEstOffsetPos.P;
   float fTiltVel = fvEstOffsetPos.T;
   FPTZVector fvContMove( fPanVel, fTiltVel, fZoomSpeed );
   m_pPTZCtrl->ConvertAngle2HWVel( fvContMove, m_fvContMoveVecHW );

   if( m_fOffsetAngle < m_fMaxAngleOffsetForConvergence * MC_DEG2RAD ) { // 이동해야할 Offset Angle 값이 카메라의 최소 절대값 보다 작으면 Pan, Tilt 이동을 멈춘다.
      float factor = 1.0f;
      if( m_nState & ( ContAbsPosMvTrk_State_ConstantAngleSpeed | ContAbsPosMvTrk_State_ConstantImageScrollSpeed ) )
         factor = 0.25f;
      if( fabs( m_fvOffsetPos.P ) < m_fMaxAngleOffsetForConvergence * factor ) {
         if( fabs( m_fvOffsetPos.T ) < m_fMaxAngleOffsetForConvergence * factor ) {
            m_fvContMoveVecHW.P = 0;
            m_fvContMoveVecHW.T = 0;
         }
      }
   }

   //   logd ("AV(%6.2f %6.2f %2.1f)   HW(%5.1f %5.1f %2.1f)\n", fvContMove.P, fvContMove.T, fvContMove.Z, m_fvContMoveVecHW.P, m_fvContMoveVecHW.T, m_fvContMoveVecHW.Z);

   if( m_fvContMoveVecHW.IsContPTZMoveStopped() ) {
      if( m_nState & ContAbsPosMvTrk_State_ConstantAngleSpeed ) {
         m_nState &= ~ContAbsPosMvTrk_State_ConstantAngleSpeed;
      }
      if( m_nState & ContAbsPosMvTrk_State_ConstantImageScrollSpeed ) {
         m_nState &= ~ContAbsPosMvTrk_State_ConstantImageScrollSpeed;
      }
   }

   // 현재 정보를 큐에 저장한다.
   ContAbsPosTrkLogItem apItem;
   apItem.m_qtVelocity  = m_qtVelocity;
   apItem.m_fZoomLogVel = m_fvOffsetPos.Z;
   m_ContAbsPosTrkLogItemQueue.AddItem( apItem );

   // 목표 위치로 이동한다.
   GotoTgtPtzPos();
   m_fvPrvPos = m_fvTgtPos;
}

void CContAbsPosTrack::ResetAll()
{
   m_nState = 0;
   m_ContAbsPosTrkLogItemQueue.Initialize( m_fFrameRate, 2.0f );
   m_fvContMoveVecHW( 0.0f, 0.0f, 0.0f, 0.0f, 0.0f );
}

void CContAbsPosTrack::ResetAvgTgtVelocity()
{
   m_nState |= ContAbsPosMvTrk_State_AvgTgtPosVelocityToBeReset;
}

void CContAbsPosTrack::SetConstantAngleSpeedMode( BOOL bConstantSpeedMode, float fConstantAngleSpeed, FPTZVector pvTgtPTZPos )
{
   m_nState |= ContAbsPosMvTrk_State_ConstantAngleSpeed;
   m_fConstantContPanMoveAngleSpeed  = fConstantAngleSpeed;
   m_fConstantContTiltMoveAngleSpeed = fConstantAngleSpeed;

   FPTZVector fvCurPTZPos = m_pPTZCtrl->GetAbsPos();

   // Tilt 각이 90도가 넘으면 Tilt 및 Pan각을 보정한다.
   if( fvCurPTZPos.T > 90.0f ) {
      fvCurPTZPos.T = 180.0f - fvCurPTZPos.T;
      fvCurPTZPos.P += 180.0f;
      if( fvCurPTZPos.P > 360.0f ) fvCurPTZPos.P -= 360.0f;
   }

   // Pan, Tilt 각의 변위량을 계산한다.
   float fPanOffSetAngle  = fabs( GetOffsetAngleInDegree( fvCurPTZPos.P, pvTgtPTZPos.P ) );
   float fTiltOffSetAngle = fabs( GetOffsetAngleInDegree( fvCurPTZPos.T, pvTgtPTZPos.T ) );

   if( fPanOffSetAngle >= fTiltOffSetAngle ) {
      if( fConstantAngleSpeed > 0.0f ) {
         float fPanMovingTime              = fPanOffSetAngle / fConstantAngleSpeed;
         m_fConstantContTiltMoveAngleSpeed = fTiltOffSetAngle / fPanMovingTime;
      }
   } else {
      if( fConstantAngleSpeed > 0.0f ) {
         float fTiltMovingTime            = fTiltOffSetAngle / fConstantAngleSpeed;
         m_fConstantContPanMoveAngleSpeed = fPanOffSetAngle / fTiltMovingTime;
      }
   }
}

void CContAbsPosTrack::SetConstantImageScrollSpeedMode( BOOL bConstantSpeedMode, float fConstantImageScrollSpeed )
{
   m_nState |= ContAbsPosMvTrk_State_ConstantImageScrollSpeed;
   m_fConstantImageScrollSpeed = fConstantImageScrollSpeed;
}

int CContAbsPosTrack::ReadParameter( CXMLIO* pXMLIO )
{
   CXMLNode xmlNode( pXMLIO );
   if( xmlNode.Start( "CContAbsPosTrackParam" ) ) {
      // 목표 위치 수렴속도 제어
      xmlNode.Attribute( TYPE_ID( float ), "TgtPosConvergeRate", &m_fTgtPosConvergeFactor );
      //xmlNode.Attribute (TYPE_ID (float), "TargetConvergenceLimit"                   , &m_fTargetConvergenceLimit);
      //xmlNode.Attribute (TYPE_ID (float), "AngleSpeedRatio"                          , &m_fAngleSpeedRatio);
      xmlNode.Attribute( TYPE_ID( float ), "TgtPosConvergeGamma", &m_fTgtPosConvergeGamma );
      xmlNode.Attribute( TYPE_ID( float ), "MaxGammaSpeed", &m_fMaxGammaSpeed );
      // 목표 위치 수렴여부 판단
      xmlNode.Attribute( TYPE_ID( float ), "MaxAngleOffsetForConvergence", &m_fMaxAngleOffsetForConvergence );
      xmlNode.Attribute( TYPE_ID( float ), "MaxZoomLogOffsetForConvergence", &m_fMaxZoomLogOffsetForConvergence );
      // 절대위치 이동
      xmlNode.Attribute( TYPE_ID( float ), "MinAngleOffsetForPanTiltAbsPosMove", &m_fMinAngleOffsetForPanTiltAbsPosMove );
      xmlNode.Attribute( TYPE_ID( float ), "PanTiltOffsetRatioForGotoAbsPos", &m_fPanTiltOffsetRatioForGotoAbsPos );
      xmlNode.Attribute( TYPE_ID( float ), "MinLog2OffsetForZoomAbsPosMove", &m_fMinLog2OffsetForZoomAbsPosMove );
      xmlNode.Attribute( TYPE_ID( int ), "AdditionalAbsPosMovePeriod", &m_nAdditionalAbsPosMovePeriod );
      xmlNode.Attribute( TYPE_ID( int ), "MaxAbsPosMovePeriod", &m_nMaxAbsPosMovePeriod );
      xmlNode.Attribute( TYPE_ID( BOOL ), "UseAbsZoomPosMove", &m_bUseAbsZoomPosMove );
      // 연속이동 제어
      xmlNode.Attribute( TYPE_ID( float ), "MaxContPanMoveAngleSpeed", &m_fMaxContPanMoveAngleSpeed );
      xmlNode.Attribute( TYPE_ID( float ), "MaxContTiltMoveAngleSpeed", &m_fMaxContTiltMoveAngleSpeed );
      xmlNode.Attribute( TYPE_ID( int ), "ContMoveStopWaitTime", &m_nContMoveStopWaitTime );
      xmlNode.Attribute( TYPE_ID( float ), "ContZoomSpeed_Fast", &m_fContZoomSpeed_Fast );
      xmlNode.Attribute( TYPE_ID( float ), "ContZoomSpeed_Slow", &m_fContZoomSpeed_Slow );
      xmlNode.Attribute( TYPE_ID( float ), "ConstantContPanMoveAngleSpeed", &m_fConstantContPanMoveAngleSpeed );
      xmlNode.Attribute( TYPE_ID( float ), "ConstantContTiltMoveAngleSpeed", &m_fConstantContTiltMoveAngleSpeed );
      xmlNode.Attribute( TYPE_ID( float ), "ConstantImageScrollSpeed", &m_fConstantImageScrollSpeed );

      xmlNode.End();
   }
   return ( DONE );
}

int CContAbsPosTrack::WriteParameter( CXMLIO* pXMLIO )
{
   CXMLNode xmlNode( pXMLIO );
   if( xmlNode.Start( "CContAbsPosTrackParam" ) ) {
      // 목표 위치 수렴속도 제어
      xmlNode.Attribute( TYPE_ID( float ), "TgtPosConvergeRate", &m_fTgtPosConvergeFactor );
      //xmlNode.Attribute (TYPE_ID (float), "TargetConvergenceLimit"                   , &m_fTargetConvergenceLimit);
      //xmlNode.Attribute (TYPE_ID (float), "AngleSpeedRatio"                          , &m_fAngleSpeedRatio);
      xmlNode.Attribute( TYPE_ID( float ), "TgtPosConvergeGamma", &m_fTgtPosConvergeGamma );
      xmlNode.Attribute( TYPE_ID( float ), "MaxGammaSpeed", &m_fMaxGammaSpeed );
      // 목표 위치 수렴여부 판단
      xmlNode.Attribute( TYPE_ID( float ), "MaxAngleOffsetForConvergence", &m_fMaxAngleOffsetForConvergence );
      xmlNode.Attribute( TYPE_ID( float ), "MaxZoomLogOffsetForConvergence", &m_fMaxZoomLogOffsetForConvergence );
      // 절대위치 이동
      xmlNode.Attribute( TYPE_ID( float ), "MinAngleOffsetForPanTiltAbsPosMove", &m_fMinAngleOffsetForPanTiltAbsPosMove );
      xmlNode.Attribute( TYPE_ID( float ), "PanTiltOffsetRatioForGotoAbsPos", &m_fPanTiltOffsetRatioForGotoAbsPos );
      xmlNode.Attribute( TYPE_ID( float ), "MinLog2OffsetForZoomAbsPosMove", &m_fMinLog2OffsetForZoomAbsPosMove );
      xmlNode.Attribute( TYPE_ID( int ), "AdditionalAbsPosMovePeriod", &m_nAdditionalAbsPosMovePeriod );
      xmlNode.Attribute( TYPE_ID( int ), "MaxAbsPosMovePeriod", &m_nMaxAbsPosMovePeriod );
      xmlNode.Attribute( TYPE_ID( BOOL ), "UseAbsZoomPosMove", &m_bUseAbsZoomPosMove );
      // 연속이동 제어
      xmlNode.Attribute( TYPE_ID( float ), "MaxContPanMoveAngleSpeed", &m_fMaxContPanMoveAngleSpeed );
      xmlNode.Attribute( TYPE_ID( float ), "MaxContTiltMoveAngleSpeed", &m_fMaxContTiltMoveAngleSpeed );
      xmlNode.Attribute( TYPE_ID( int ), "ContMoveStopWaitTime", &m_nContMoveStopWaitTime );
      xmlNode.Attribute( TYPE_ID( float ), "ContZoomSpeed_Fast", &m_fContZoomSpeed_Fast );
      xmlNode.Attribute( TYPE_ID( float ), "ContZoomSpeed_Slow", &m_fContZoomSpeed_Slow );
      xmlNode.Attribute( TYPE_ID( float ), "ConstantContPanMoveAngleSpeed", &m_fConstantContPanMoveAngleSpeed );
      xmlNode.Attribute( TYPE_ID( float ), "ConstantContTiltMoveAngleSpeed", &m_fConstantContTiltMoveAngleSpeed );
      xmlNode.Attribute( TYPE_ID( float ), "ConstantImageScrollSpeed", &m_fConstantImageScrollSpeed );
      xmlNode.End();
   }
   return ( DONE );
}

#endif //__SUPPORT_PTZ_CAMERA

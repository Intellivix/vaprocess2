#pragma  once
  
 const float NO_CMD   = (float)1E-20;    
 const float PREV_CMD = (float)1E-19;    

///////////////////////////////////////////////////////////////////////////////
//
// Class: PTZVector
//
///////////////////////////////////////////////////////////////////////////////

 template<class _T>
 class PTZVector

{
   public:
      _T P;    // ContMove: Pan   속도(Velicity), AbsMove: Zoom  위치
      _T T;    // ContMove: Tilt  속도(Velicity), AbsMove: Tilt  위치
      _T Z;    // ContMove: Zoom  속도(Velicity), AbsMove: Zoom  위치
      _T F;    // ContMove: Focus 속도(Velicity), AbsMove: Focus 위치
      _T I;    // ContMove: Iris  속도(Velicity), AbsMove: Iris  위치
      _T PS;   // ContMove: 사용 안함            , AbsMove: Pan  속력(Speed)
      _T TS;   // ContMove: 사용 안함            , AbsMove: Tilt 속력(Speed)
      int ZT;  // 줌모듈 타입

   public:
      PTZVector (   );
      PTZVector (_T p, _T t, _T z);
      PTZVector (_T p, _T t, _T z, _T f);
      PTZVector (_T p, _T t, _T z, _T f, _T i);

   public:
      void       operator () (   );
      void       operator () (_T p, _T t, _T z);
      void       operator () (_T p, _T t, _T z, _T f);
      void       operator () (_T p, _T t, _T z, _T f, _T i);
      PTZVector  operator +  (const PTZVector &vec);
      PTZVector  operator -  (const PTZVector &vec);
      PTZVector& operator += (const PTZVector &vec);
      PTZVector  operator *  (_T scale);
      int        operator == (const PTZVector &vec);
      BOOL       operator != (const PTZVector &vec);

   public:
      BOOL IsEqualPTZ   (const PTZVector &vec);
      BOOL IsEqualPTZFI (const PTZVector &vec);
      BOOL IsContPTZFIMoveStopped (   );
      BOOL IsContPTZMoveStopped (   );
      void Set (_T p, _T t, _T z);
      void Set (_T p, _T t, _T z, _T f);
};

 template<class _T>
 PTZVector<_T>::PTZVector (   )

{
   P = T = Z = F = I = PS = TS = (_T)0;
   ZT = 0;
}

 template<class _T>
 PTZVector<_T>::PTZVector (_T p, _T t, _T z)

{
   P = p; T = t; Z = z; 
   F = I = PS = TS = (_T)0;
   ZT = 0;
}

template<class _T>
PTZVector<_T>::PTZVector (_T p, _T t, _T z, _T f)

{
   P = p; T = t; Z = z; F = f; 
   I = PS = TS = (_T)0;
   ZT = 0;
}

template<class _T>
PTZVector<_T>::PTZVector (_T p, _T t, _T z, _T f, _T i)

{
   P = p; T = t; Z = z; F = f; I = i;  
   PS = TS = 0;
   ZT = 0;
}

 template<class _T>
 void PTZVector<_T>::operator () (_T p, _T t, _T z)

{
   P = p; T = t; Z = z; 
}

template<class _T>
void PTZVector<_T>::operator () (_T p, _T t, _T z, _T f)

{
   P = p; T = t; Z = z; F = f;
}

template<class _T>
void PTZVector<_T>::operator () (_T p, _T t, _T z, _T f, _T i)

{
   P = p; T = t; Z = z; F = f; I = i;
}


 template<class _T>
 PTZVector<_T> PTZVector<_T>::operator + (const PTZVector &vec)

{
   PTZVector tmp = *this;
   tmp.P += vec.P; tmp.T += vec.T; tmp.Z += vec.Z; tmp.F += vec.F; tmp.I += vec.I; 
   return (tmp);
};

 template<class _T>
 PTZVector<_T> PTZVector<_T>::operator - (const PTZVector &vec)

{
   PTZVector tmp = *this;
   tmp.P -= vec.P; tmp.T -= vec.T; tmp.Z -= vec.Z; tmp.F -= vec.F; tmp.I -= vec.I; 
   return (tmp);
};

 template<class _T> 
    PTZVector<_T>& PTZVector<_T>::operator += (const PTZVector &vec)
{
   P += vec.P; T += vec.T; Z += vec.Z; F += vec.F; I += vec.I;
   return (*this);
};


 template<class _T> 
    PTZVector<_T> PTZVector<_T>::operator * (const _T scale)
 {
    PTZVector tmp = *this;
    tmp.P *= scale; tmp.T *= scale; tmp.Z *= scale; tmp.F *= scale; tmp.I *= scale;
    return (tmp);
 };

 template<class _T> 
    BOOL PTZVector<_T>::operator != (const PTZVector &vec)
 {
    PTZVector tmp = *this;
    if (P != vec.P || T != vec.T || Z != vec.Z) return TRUE;
    return FALSE;
 };

 template<class _T> 
    BOOL PTZVector<_T>::operator == (const PTZVector &vec)
 {
    PTZVector tmp = *this;
    if (P == vec.P && T == vec.T && Z == vec.Z) return TRUE;
    return FALSE;
 };

 template<class _T> 
    BOOL PTZVector<_T>::IsEqualPTZ (const PTZVector &vec)
 {
    if (P == vec.P && T == vec.T && Z == vec.Z) return TRUE;
    return FALSE;
 };

 template<class _T> 
    BOOL PTZVector<_T>::IsEqualPTZFI (const PTZVector &vec)
 {
    if (P == vec.P && T == vec.T && Z == vec.Z && F == vec.F && I == vec.I) return TRUE;
    return FALSE;
 };

 template<class _T> 
    BOOL PTZVector<_T>::IsContPTZFIMoveStopped (   )
    {
       if(P == _T(0) && 
          T == _T(0) && 
          Z == _T(0) && 
          F == _T(0) && 
          I == _T(0))
          return TRUE;
       return FALSE;
    }

template<class _T> 
    BOOL PTZVector<_T>::IsContPTZMoveStopped (   )
    {
       if(P == _T(0) && 
          T == _T(0) && 
          Z == _T(0))
          return TRUE;
       return FALSE;
    }

 template<class _T>
    void PTZVector<_T>::Set (_T p, _T t, _T z)
 {
    P = p; T = t; Z = z; F = NO_CMD;
 };

 template<class _T>
    void PTZVector<_T>::Set (_T p, _T t, _T z, _T f)

 {
    P = p; T = t; Z = z; F = f;
 };

 ///////////////////////////////////////////////////////////////////////////////
//
// Type Redefinitions
//
///////////////////////////////////////////////////////////////////////////////

typedef PTZVector<int>    IPTZVector;
typedef PTZVector<float>  FPTZVector;
typedef PTZVector<double> DFPTZVector;


﻿#include "stdafx.h"
#include "Camera_IP.h"

#include "version.h"
#include "VCM.h"
#include "CameraProductAndItsProperties.h"
#include "IPCamStream.h"
#include "IPCamStream_RTSP.h"
#include "CommonUtil.h"

#include "CompressImageBuffer.h"
#include "PTZCtrl.h"
//#include "ElapseCounter.h"
#include "SupportFunction.h"
#include "FrameRateChecker.h"

#ifndef WIN32
#include <sys/stat.h>
#endif
//#include "LogDefine.h"

#define DEBUG_CAMERA_IP

BOOL g_bClosingSystem = FALSE;

////////////////////////////////////////////////////////////////////////////////////////////
// IP 카메라를 추가하는 순서
//    1.  CameraProperty.cpp 의  InitIPCameraProducts 함수에 IP 카메라 속성을 추가한다.
//    2.  CIPCamStream 클래스를 상속받은 클래스를 구현한다.
//    3.  Camera_IP.cpp 의 CreateIPCamStream 함수에
//        카메라 종류별로 클래스를 생성할 수 있도록 추가한다.
////////////////////////////////////////////////////////////////////////////////////////////

// 카메라 제품별로 IP 카메라를 생성시키는 Global 함수
CIPCamStream* CreateIPCamStream( CIPCameraInfo* pInfo )
{
   CIPCamStream* pNewStream = NULL;

   IPCameraProperty* pProperty = GetIPCameraProperty( pInfo->m_nIPCameraType );
   if( !pProperty ) return pNewStream;

   switch( pProperty->m_nCompanyID ) {
   case CompanyID_RTSP:
      pNewStream = new CIPCamStream_RTSP;
      break;
#ifdef __SUPPORT_ONVIF
   case CompanyID_Onvif:
      pNewStream = new CIPCamStream_Onvif;
      break;
#endif
   default:
      pNewStream = NULL;
   }

   if( pNewStream ) {
      pNewStream->m_bRemote = pInfo->m_bRemote;
   }
   return pNewStream;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// class CCamera_IP

CCamera_IP::CCamera_IP()
{
   m_pInfo     = NULL;
   m_pProperty = NULL;

   m_pcbDecodeFrame        = NULL;
   m_pCBDecodeFrameParam   = NULL;
   m_pcbReceiveEncodeFrame = NULL;
   m_pVideoChannel         = NULL;

   m_bDecodeVideo                = TRUE;
   m_bDecodePrevFrame            = TRUE;
   m_bFrameSkippingUntilKeyFrame = FALSE;
   m_bFindFirstKeyFrame          = FALSE;

   m_nIPCamState = 0x00000000;

   m_nDecodeCountBefore = 0;
   m_nDecodeCount       = 0;
   m_nLastGetImageIdx   = 0;
   m_pStream            = NULL;

   m_pTimer_ConnectionAndDecode = NULL;
#ifdef __SUPPORT_PTZ_CAMERA
   m_pPTZControl = NULL;
#endif
   m_fBitrate           = 0.0f;
   m_nChannelNo_IVX     = -1;
   m_bFirstDecompressOK = FALSE;
   m_bLastVideoSignal   = FALSE;
   m_nSrcImgLockCnt     = 0;
   m_pCBRTSPMetadata    = NULL;
   m_nSrcImgType        = 0;
}

CCamera_IP::~CCamera_IP()
{
   if( m_pStream ) {      
      delete m_pStream;
   }

   if( m_pTimer_ConnectionAndDecode ) {
      m_pTimerMgr->DeleteTimer( m_pTimer_ConnectionAndDecode->m_nTimerID );
   }
}

int CCamera_IP::Initialize( CIPCameraInfo* pInfo )
{
   CCriticalSectionSP co( m_csCamera_IP );

   m_pInfo = pInfo;
   m_pInfo->SetVideoSizeFromCameraInfo();
   m_pTimerMgr = m_pInfo->m_pTimerMgr;

   if( !m_pInfo->m_nIPCameraType ) {
      LOGE << "m_pInfo->m_nIPCameraType error";
      return ( 1 );
   }
   m_pProperty = GetIPCameraProperty( m_pInfo->m_nIPCameraType );

   if( !m_pInfo->m_bRemote ) {
      if( m_pProperty ) {
         if( m_pInfo->m_nWidth < 0 ) m_pInfo->m_nWidth = 0;
         if( m_pInfo->m_nHeight < 0 ) m_pInfo->m_nHeight = 0;
         m_PrevSrcImgSize( m_pInfo->m_nWidth, m_pInfo->m_nHeight );
         int nSrcImgBuffLen = GetYUY2ImageLength( m_pInfo->m_nWidth, m_pInfo->m_nHeight );
         if( nSrcImgBuffLen != m_SrcImg.Length ) {
            m_csSrcImg.Lock();
            m_SrcImg.Create( nSrcImgBuffLen + FFMPEG_BUFFER_PADDING_SIZE * 4 );
            m_SrcImg.Clear(); // 중요: 소스 영상 버퍼의 내용을 0으로 초기화 한다.
            //      초기화를 하지 않은 상태에서 버퍼에 담겨있는 임의의 내용으로 인코딩을 하게 되는 경우
            //      다운되는 현상이 발생 될 수 있다.
            m_SrcImg.Length = nSrcImgBuffLen;
            m_csSrcImg.Unlock();
            BlackOut( m_SrcImg, m_SrcImg.Length, 0x00 );
         } else {
            BlackOut( m_SrcImg, m_SrcImg.Length, 0x00 );
         }
      }
   }

   if( !m_pTimer_ConnectionAndDecode )
      m_pTimer_ConnectionAndDecode = m_pTimerMgr->GetNewTimer( "CameraIP_ConnectionAndDecode" );

   // IP 카메라 스트림을 생성한다.
   m_pStream = CreateIPCamStream( m_pInfo );
   if( !m_pStream ) {
      LOGE << "CreateIPCamStream failed.";
      return ( 2 );
   }
   m_pStream->m_nStreamIdx       = m_nICameraIdx;
   m_pStream->m_pCBCamEventParam = m_pCBCamEventParam;
   m_pStream->m_pCBCamEvent      = m_pCBCamEvent;
   m_pStream->m_nChannelNo_IVX   = m_nChannelNo_IVX;
   m_pStream->m_fFrameRate       = m_pInfo->m_fFPS_Cap;
   m_pInfo->m_nCompanyID        = m_pStream->m_nCompanyID;
   m_pInfo->m_pStream           = m_pStream;
   m_pInfo->m_pSrcImgBuff       = (BYTE*)m_SrcImg;
   m_pStream->m_pcsSrcImageBuff = &m_csSrcImg;
   if( CompanyID_RTSP == m_pStream->m_nCompanyID ) {
      CIPCamStream_RTSP* pRTSPStream = (CIPCamStream_RTSP*)m_pStream;
      pRTSPStream->m_pCBRTSPMetadata = m_pCBRTSPMetadata;
   }
   m_pStream->Initialize( m_pInfo );

   CDigitalIO::Initialize( m_pProperty->m_nInputNum, m_pProperty->m_nOutputNum );

   m_nState |= State_Initialize;
   return DONE;
}

int CCamera_IP::Uninitialize()
{
   CCriticalSectionSP co( m_csCamera_IP );

   if( m_nState & State_Initialize ) {
      m_pStream->EndCapture();
      m_pStream->Uninitialize();
      m_nState &= ~State_Initialize;
   }
   //m_SrcImg.Delete (   );

   return ( DONE );
}

int CCamera_IP::BeginCapture()
{
   CCriticalSectionSP co( m_csCamera_IP );
   if( FALSE == ( m_nState & State_Initialize ) ) return ( 1 );
   if( m_nState & State_Capture ) return ( DONE );
   m_nState &= ~State_SiganlStopCapture;
   m_nDecodeCount       = 0;
   m_nDecodeCountBefore = 0;
   m_nFrameCount        = 0;
   m_nLastGetImageIdx   = 0;
   m_bFirstDecompressOK = FALSE;
   m_nState |= State_Capture;
   m_pStream->m_dwConnected = GetTickCount();
   ZeroMemory( &m_ftLastDecodeFrameTime, sizeof( m_ftLastDecodeFrameTime ) );
   ZeroMemory( &m_ftOldestConnectionFailed, sizeof( m_ftOldestConnectionFailed ) );

   m_ConnectionAndDecodeThread = std::thread( Thread_ConnectionAndDecoding, (LPVOID)this );

   return ( DONE );
}

int CCamera_IP::EndCapture()
{
   CCriticalSectionSP co( m_csCamera_IP );

   if( FALSE == ( m_nState & State_Initialize ) ) return ( 1 );

   m_nState |= State_SiganlStopCapture;
   m_pStream->SignalStopCapture();
   if( m_pTimer_ConnectionAndDecode ) m_pTimer_ConnectionAndDecode->GetTimerEvent().SetEvent(); // Blocking 해제용
   m_evtGetImage.SetEvent(); // Blocking 해제용

   if( m_ConnectionAndDecodeThread.joinable() )
      m_ConnectionAndDecodeThread.join();

   m_bFirstDecompressOK = FALSE;

   m_nState &= ~State_SiganlStopCapture;
   m_nState &= ~State_Capture;
   return ( DONE );
}

int CCamera_IP::SignalStopCapture()
{
   CCriticalSectionSP co( m_csCamera_IP );

   if( FALSE == ( m_nState & State_Initialize ) ) return ( 1 );
   if( !( m_nState & State_Capture ) ) return ( DONE );

   m_nState |= State_SiganlStopCapture;
   m_pStream->SignalStopCapture();
   if( m_pTimer_ConnectionAndDecode ) m_pTimer_ConnectionAndDecode->GetTimerEvent().SetEvent(); // Blocking 해제용
   m_evtGetImage.SetEvent(); // Blocking 해제용
   return ( DONE );
}

int CCamera_IP::GetImage_Lock( byte** d_buffer )
{
   if( m_nState & State_ConnectionOk ) {
      // 중요: 동기화 문제
      //       디코딩이 완료되었을 때에만 이미지에 대한 포인터를 얻는 것을 허용하기 위한 대기함수를 호출하는 부분이다.
      //       이 버퍼에 접근하는 CVideoChannel의 캡쳐 스레드가 약간이라도 느려지게 되면 건너 뛰는 프레임이 생기면서
      //       프레임이 끊겨보이는 문제가 발생할 수 있다.

      //if (m_pVideoChannel->m_nVideoChannelType != VideoChannelType_Panorama)
      //{
      //   m_evtGetImage.Wait();
      //}
   }
   if( m_pStream ) {
      if( m_pStream->m_nState & CIPCamStream::State_RTSP_ByePacketReceived ) {
         m_pStream->m_nState &= ~CIPCamStream::State_RTSP_ByePacketReceived;
         return GetImgeLockRet_EndOfFile;
      }
   }
   m_nLastGetImageIdx = m_nDecodeCount;

   if( (byte*)m_SrcImg ) {
      m_nSrcImgLockCnt++;
      m_csSrcImg.Lock();

      *d_buffer = (byte*)m_SrcImg;
   }
   return ( DONE );
}

int CCamera_IP::GetImage_Unlock()
{

   if( m_nSrcImgLockCnt ) {
      m_nSrcImgLockCnt--;
      ASSERT( m_nSrcImgLockCnt >= 0 );
      m_csSrcImg.Unlock();
   }
   return ( DONE );
}

int CCamera_IP::GetColorSpace()
{
   return m_pProperty->m_nColorSpace;
}

int CCamera_IP::ConnectionAndDecodingProc()
{
   //logd( "[CH:%d][Stream:%d] ConnectionAndDecodingProc - Start\n", m_nChannelNo_IVX + 1, m_nICameraIdx + 1 );
   int i;

   BOOL bPrevTryingConnect = FALSE;
   m_nConnectStartTime     = GetTickCount() - 1000000;
   m_nState &= ~( State_ConnectionFailed | State_AuthorizationFailed | State_ProductNotMatched | State_ResolutionChanged );

   CCompressImageBufferQueue& compBuffQueue = *m_pStream->m_pCompBuffQueue;
   m_pTimer_ConnectionAndDecode->StartTimer();

   int nPrevIPCamState = m_nState; // 카메라 상태변화 체크를 위한 이전상태 백업용 변수.

   // Connection Loop : 연결이될 때까지
   while( 1 ) {
      m_nState &= ~( State_ConnectionOk | State_AuthorizationFailed );

#ifdef __SUPPORT_PTZ_CAMERA
      if( m_pPTZControl ) {
         if( m_pPTZControl->m_nCapabilities & PTZCtrlCap_ConnectionDependOnStream ) {
            m_pPTZControl->m_nState &= ~PTZ_State_Connected;
         }
      }
#endif

      m_pStream->m_nState &= ~( CIPCamStream::State_Connected | CIPCamStream::State_AuthorizationFailed );
      m_pStream->m_nState |= CIPCamStream::State_Connecting;

      if( m_nState & State_SiganlStopCapture ) {
         LOGE << "State_SiganlStopCapture break";
         break;
      }

      m_pStream->m_dwLastCapture = 0; // 마지막에 캡춰한 시간을 초기화 한다. 시간이 0 이면 비디오 신호가 없음으로 판단한다.
      m_pStream->m_pCompBuffQueue->ResetLastPushTime();

      m_evtGetImage.SetEvent();

      m_nState |= State_Connectting;
      compBuffQueue.Clear();

      //logd( "[CH:%d][Stream:%d] ConnectionAndDecodingProc - EndCapture\n", m_nChannelNo_IVX + 1, m_nICameraIdx + 1 );
      m_pStream->EndCapture();
      if( FALSE == bPrevTryingConnect ) {
         if( IPCameraType_RTSP == m_pInfo->m_nIPCameraType ) {
            logi( "[CH:%d][Stream:%d][%s] Trying to connect\n", m_nChannelNo_IVX + 1, m_nICameraIdx + 1, m_pInfo->m_strURL.c_str() );
         } else {
            logi( "[CH:%d][Stream:%d][%s-%d] Trying to connect\n", m_nChannelNo_IVX + 1, m_nICameraIdx + 1, m_pInfo->m_strIPAddress.c_str(), m_pInfo->m_nPortNo );
         }

         bPrevTryingConnect = TRUE;
      }
      int bRetBeginCapture = m_pStream->BeginCapture();

      BOOL bReAttempt_Connection  = FALSE;
      BOOL bEscape_ConnectionLoop = FALSE;

      if( bRetBeginCapture == CIPCamStream::ConnResult_ConnectionFailed ) {
         m_nState |= State_ConnectionFailed;
         bReAttempt_Connection = TRUE; // 재접속
      }
      if( bRetBeginCapture == CIPCamStream::ConnResult_AuthorizationFailed ) {
         m_nState |= State_AuthorizationFailed;
      }
      if( bRetBeginCapture == CIPCamStream::ConnResult_ProductNotMatched ) {
         m_nState |= State_ProductNotMatched;
      }
      if( bRetBeginCapture == CIPCamStream::ConnResult_VideoSizeIsChanged ) { // 비디오 해상도 변경D
         logd( "[CH:%d][Stream:%d] IPCam: ConnResult_VideoSizeIsChanged\n", m_nChannelNo_IVX + 1, m_nICameraIdx + 1 );
         m_nState |= State_ResolutionChanged;
      }
      if( bRetBeginCapture == CIPCamStream::ConnResult_SiganlStopCapture ) {
         logi( "[CH:%d][Stream:%d] IPCam: ConnResult_SiganlStopCapture\n", m_nChannelNo_IVX + 1, m_nICameraIdx + 1 );
         m_nState |= State_SiganlStopCapture;
         bEscape_ConnectionLoop = TRUE; // 탈출
      }

      if( DONE == bRetBeginCapture ) {
         m_pStream->m_nState |= CIPCamStream::State_Connecting;
         m_pStream->m_nState &= ~CIPCamStream::State_SignalConnectionToBeClosed;
      } else {
         if( m_nState & State_SiganlStopCapture )
            bEscape_ConnectionLoop = TRUE; // 탈출
         bReAttempt_Connection = TRUE; // 재접속
         m_nState &= ~State_Connectting;
      }
      float fReconnectFPS = 1.0f / 15.0f;

      switch( m_pInfo->m_nIPCameraType ) {
      case IPCameraType_Innodep:
      case IPCameraType_IVXVideoServer:
         fReconnectFPS = 1.0f / 5.0f;
         break;
      case IPCameraType_RTSP:
         fReconnectFPS = 1.0f / 2.0f;
         break;
      }

      m_pTimer_ConnectionAndDecode->SetFPS( fReconnectFPS );
      if( bReAttempt_Connection ) {
         m_pTimer_ConnectionAndDecode->Wait();
      }

      if( !bEscape_ConnectionLoop ) {
         if( !m_ftOldestConnectionFailed.dwHighDateTime && !m_ftOldestConnectionFailed.dwLowDateTime ) {
            AfxGetLocalFileTime( &m_ftOldestConnectionFailed );
         }
         if( m_ftOldestConnectionFailed.dwHighDateTime || m_ftOldestConnectionFailed.dwLowDateTime ) {
            FILETIME ftCurrent;
            AfxGetLocalFileTime( &ftCurrent );
            FILETIME ftEscape = m_ftOldestConnectionFailed;

            if( m_pProperty && m_pProperty->m_nCapabilities & IPCamCaps_ReceiveVideoStreamByRTSP ) {
               AddToFileTime( ftEscape, 0, 0, 10, rand() % 32 ); // 5분마다 카메라 재시작
            } else {
               AddToFileTime( ftEscape, 0, 0, 10, rand() % 32 ); // 5분마다 카메라 재시작
            }

            if( ftCurrent > ftEscape ) {
               ZeroMemory( &m_ftOldestConnectionFailed, sizeof( m_ftOldestConnectionFailed ) );
            }
         }
      }

      if( bEscape_ConnectionLoop ) {
         m_nState |= State_SiganlStopCapture; // 현재 Decoding Loop는 종료
         loge( "[CH:%d][Stream:%d] Connection failed: Escape connection loop. break", m_nChannelNo_IVX + 1, m_nICameraIdx + 1 );
         break;
      }
      if( bReAttempt_Connection ) {
         continue;
      }
      if( m_nState & State_SiganlStopCapture ) {
         break;
      }

      // 여기까지 살아남았으면 연결완료!!
      m_nState &= ~( State_ConnectionFailed | State_AuthorizationFailed | State_ProductNotMatched | State_ResolutionChanged );
      m_pStream->m_nState &= ~State_ConnectionFailed;
      m_pStream->m_nState |= CIPCamStream::State_Connected;
      m_nConnectStartTime = GetTickCount();

      m_pStream->m_dwConnected = GetTickCount();

      int nVideoFormat = m_pInfo->m_nVideoFormat_Cap;

      BOOL bUseKeyFrame = TRUE; // 키 프레임을 가지는 스트림인지 여부.

      if( nVideoFormat == VCODEC_ID_MJPEG ) bUseKeyFrame = FALSE;
      if( nVideoFormat == VCODEC_ID_RAW ) bUseKeyFrame = FALSE;
      if( m_pProperty->m_nCompanyID == CompanyID_IDIS ) bUseKeyFrame = FALSE;

      BOOL bSkipFindingKeyFrame = FALSE;

      m_nDecodeCount       = 0;
      m_nDecodeCountBefore = 0;

      compBuffQueue.Clear();

      //double dfFramePeriod = 1000.0 / m_pInfo->m_fFPS_Cap;
      if( GetTickCount() - m_nConnectStartTime < 3000 )
         m_pTimer_ConnectionAndDecode->SetFPS( m_pInfo->m_fFPS_Cap * 2 );
      else
         m_pTimer_ConnectionAndDecode->SetFPS( m_pInfo->m_fFPS_Cap );

      CFrameRateChecker framerate_checker;
      framerate_checker.Initialize( m_pInfo->m_fFPS_Cap );
      framerate_checker.CheckStart();
      m_nState &= ~State_FrameRateIsMismatched;

      m_evtGetImage.SetEvent(); // jun: 디코딩을 시작하기 전에 한번은 Lock을 풀어준다.

      //logi( "[CH:%d][Stream:%d] Connected to Input source.", m_nChannelNo_IVX + 1, m_nICameraIdx + 1 );

      // 비디오 프레임 디코딩 루프
      while( 1 ) {
         if( !IsConnected() ) {
            m_pStream->m_nState |= CIPCamStream::State_SignalConnectionToBeClosed;
         }
         // IP 카메라 멈춤 명령에 의한 종료
         if( m_nState & State_SiganlStopCapture ) {
            break;
         }
         // Stream 오류에 의한 종료
         else if( m_pStream->m_nState & CIPCamStream::State_SignalConnectionToBeClosed ) {
            LOGE << "CIPCamStream::State_SignalConnectionToBeClosed break";
            // TODO(jongoh): 통신사 프로젝트에서는 접속이 되지 않는다면 이벤트를 주어야 한다.
            m_nState |= State_VideoSignalNotOK;
            break;
         } else if( m_pStream->m_nState & CIPCamStream::State_AuthorizationFailed ) {
            LOGE << "CIPCamStream::State_AuthorizationFailed break";
            m_nState |= State_AuthorizationFailed;
            break;
         }

         BOOL bMpec4VideoConfig = FALSE; // MPEG4 비디오 설정 헤더사용하는지 체크하는 변수.
         // 키프레임을 사용하는 스트림의 경우
         if( bUseKeyFrame && !bSkipFindingKeyFrame ) {
            BOOL bFindKeyFrame   = FALSE;
            int nFindKeyFrameIdx = 0;

            compBuffQueue.Lock();
            {
               int nImgNum = compBuffQueue.Size();
               if( nImgNum > 0 && !bMpec4VideoConfig && !m_bFindFirstKeyFrame ) {
                  // 가장 최근 프레임에서 역방향으로 키프레임을 찾는다.
                  for( i = nImgNum - 1; i >= 0; i-- ) {
                     CCompressImageBufferItem* pBuffItem = compBuffQueue[i];
                     if( pBuffItem->m_dwCommFlag & VFRM_KEYFRAME ) {
                        bFindKeyFrame    = TRUE;
                        nFindKeyFrameIdx = i;
                        break;
                     }
                  }
                  // 키프레임을 찾았다면 키프레임의 이전 프레임들을 모두 삭제한다.
                  if( bFindKeyFrame ) {
                     for( i = 0; i < nFindKeyFrameIdx; i++ ) {
                        CCompressImageBufferItem* pItem = compBuffQueue.Pop();
                        if( pItem )
                           delete pItem;
                     }
                  }
                  m_bFindFirstKeyFrame = bFindKeyFrame;
               }
            }
            compBuffQueue.Unlock();
         } else {
            // 키 프레임을 사용하지 않는 스트림의 경우.
            // 프레임이 누적되는 경우 마지막 장만 남기고 모두 삭제한다.
            compBuffQueue.Lock();
            {
               int nImgNum = compBuffQueue.Size();
               if( nImgNum > 1 ) {
                  int nPopNum = nImgNum - 1;
                  for( i = 0; i < nPopNum; i++ ) {
                     CCompressImageBufferItem* pItem = compBuffQueue.Pop();
                     if( pItem )
                        delete pItem;
                  }
                  nImgNum = nImgNum - nPopNum;
               }
            }
            compBuffQueue.Unlock();
         }
         if( !bMpec4VideoConfig ) {
            if( bUseKeyFrame ) {
               if( !m_bFindFirstKeyFrame ) {
                  if( int( compBuffQueue.size() <= m_pInfo->m_fFPS_Cap * 3 ) ) { // 3초동안 키프레임을 기다린다. 없으면 없는데로 디코딩
                     m_pTimer_ConnectionAndDecode->Wait(); // 비디오 스트림을 전송받을 때 까지 대기한다.
                     continue;
                  }
               }
            }
         }

         const std::vector<std::string> str_codecs = { "MPEG4", "MJPEG", "RAW", "H264", "XVID" };
         LOGI << "Codec Info : " << str_codecs[m_pInfo->m_nVideoFormat_Cap];

         while( 1 ) {

            ///////////////////// 디코딩 루프 /////////////////////

            if( !IsConnected() ) {
               m_pStream->m_nState |= CIPCamStream::State_SignalConnectionToBeClosed;
            }
            // IP 카메라 멈춤 명령에 의한 종료
            if( m_nState & State_SiganlStopCapture ) {
               LOGW << "ICamera::State_SiganlStopCapture break";
               break;
            }
            // Stream 오류에 의한 종료
            else if( m_pStream->m_nState & CIPCamStream::State_SignalConnectionToBeClosed ) {
               LOGE << "CIPCamStream::State_SignalConnectionToBeClosed break";
               break;
            }

            CCompressImageBufferItem* pBuffItem = compBuffQueue.Pop();
            if( pBuffItem ) {
               // 연결 여부 체크 (중요)
               if( State_Connectting & m_nState ) {
                  m_nState &= ~State_Connectting;
                  // 첫번 째 프레임을 수신하는 시점을 연결완료 시점으로 본다.
                  m_nState |= State_ConnectionOk;

// 스트림 연결에 종속적인 PTZ카메라는 비디오를 수신하는 시점을 연결완료 시점으로 본다.
#ifdef __SUPPORT_PTZ_CAMERA
                  if( m_pPTZControl ) {
                     if( m_pPTZControl->m_nCapabilities & PTZCtrlCap_ConnectionDependOnStream ) {
                        if( FALSE == ( PTZ_State_Connected & m_pPTZControl->m_nState ) ) {
                           m_pPTZControl->m_nState |= PTZ_State_Connected;
                        }
                     }
                  }
#endif
               }

               ZeroMemory( &m_ftOldestConnectionFailed, sizeof( m_ftOldestConnectionFailed ) );
               m_fBitrate = compBuffQueue.m_fBitrate;

               if( m_bFrameSkippingUntilKeyFrame ) {
                  if( bUseKeyFrame ) {
                     if( pBuffItem->m_dwCommFlag & VFRM_KEYFRAME ) {
                        m_bFindFirstKeyFrame          = TRUE;
                        m_bFrameSkippingUntilKeyFrame = FALSE;
                     }
                  } else {
                     m_bFindFirstKeyFrame          = TRUE;
                     m_bFrameSkippingUntilKeyFrame = FALSE;
                  }
               } else {
                  if( FALSE == m_bFindFirstKeyFrame ) {
                     if( pBuffItem->m_dwCommFlag & VFRM_KEYFRAME )
                        m_bFindFirstKeyFrame = TRUE;
                  }
               }
               m_bDecodePrevFrame = m_bDecodeVideo;

               int decompress_result = CIPCamStream::DecompressResult_OK;
               if( !g_bClosingSystem ) {

                  // 비트 레이트 정보.
                  //LOGD << "Bitrate: " << static_cast<int>( m_fBitrate );

                  // 프레임 비율 체크.
                  // 30fps로 설정되어 있을 때 16.5fps 미만이면 이상있음.
                  // 10fps로 설정되어 있을 때  5.5fps 미만 또는 14.5fps이면 이상있음.
                  m_fFrameRate = framerate_checker.CalcFrameRate();
                  if( framerate_checker.GetElapsedSecFromCheckStart() > 60 ) { // 60초 경과시에만 체크..

                     // FIXME(yhpark): bug.
                     //LOGW << "framerate_checker more than 60 sec. now checking.";

                     if( fabs( m_fFrameRate - m_pInfo->m_fFPS_Cap ) > m_pInfo->m_fFPS_Cap * 0.45f ) {
                        m_nState |= State_FrameRateIsMismatched;
                     } else {
                        m_nState &= ~State_FrameRateIsMismatched;
                        if( CheckStateChange( nPrevIPCamState, m_nState, (int)State_FrameRateIsMismatched ) ) {
                           if( m_pCBCamEvent ) {
                              VC_PARAM_CAMERA_EVENT input;
                              input.pParam     = m_pCBCamEventParam;
                              input.nStreamIdx = m_nICameraIdx;
                              input.nCamEvent  = VCCamEvent_FrameRateIsMismatched;
                              m_pCBCamEvent( input );
                           }
                        }
                     }
                  } else {
                     m_nState &= ~State_FrameRateIsMismatched;
                  }

                  // 인코딩 비디오 스트림 전달
                  if( m_pcbReceiveEncodeFrame ) {
                     VC_PARAM_ENCODE input;
                     input.pParam           = m_pVideoChannel;
                     input.nVCState         = 0;
                     input.nCount           = 0;
                     input.nStreamIdx       = m_nICameraIdx;
                     input.nCamState        = 0;
                     input.nCodecID         = pBuffItem->m_nCodecID;
                     input.pCompBuff        = pBuffItem->m_CompBuff;
                     input.nCompBuffSize    = pBuffItem->m_nCompBuffSize;
                     input.dwFlag           = pBuffItem->m_nCompBuffSize;
                     input.PresentationTime = pBuffItem->m_PresentationTime;
                     input.dfNormalPlayTime = pBuffItem->m_dfNormalPlayTime;
                     m_pcbReceiveEncodeFrame( input );
                  }

                  if( TRUE == bPrevTryingConnect ) {
                     //logi( "[CH:%d][Stream:%d][%s-%d] ===> Connected!!\n", m_nChannelNo_IVX + 1, m_nICameraIdx + 1, m_pInfo->m_strIPAddress.c_str(), m_pInfo->m_nPortNo );
                     bPrevTryingConnect = FALSE;
                  }

                  // 디코딩
                  if( m_bDecodeVideo && m_bFindFirstKeyFrame ) {
                     m_nDecodeCountBefore++;
                     decompress_result = m_pStream->Decompress( pBuffItem, m_SrcImg );
                     if( CIPCamStream::DecompressResult_OK == decompress_result ) {

                        m_nDecodeCount++;

                        // NOTE(yhpark): make BMP file to check decoded image.
#ifdef DEBUG_BMP_IMAGE
                        LOGD << "DEBUG_BMP_IMAGE width[" << m_pInfo->m_nWidth
                             << "] height[" << m_pInfo->m_nHeight
                             << "] count:" << m_nDecodeCount;

                        if( m_SrcImg.Length > 0 ) {
#ifdef __SUPPORT_GRAY_IMAGE_USE
                           GImage dst_gray_image( m_pInfo->m_nWidth, m_pInfo->m_nHeight );
                           memcpy( dst_gray_image, m_SrcImg, m_pInfo->m_nWidth * m_pInfo->m_nHeight );
                           std::string filename = sutil::sformat(
                               "gray[%dx%d]_%d.bmp", m_pInfo->m_nWidth, m_pInfo->m_nHeight, m_pInfo->m_nIPCamStreamIdx );
                           dst_gray_image.WriteBMPFile( filename.c_str() );
#else
                           BGRImage dst_bgr_image( m_pInfo->m_nWidth, m_pInfo->m_nHeight );
                           ConvertYUY2ToBGR( m_SrcImg, dst_bgr_image );
                           std::string filename = sutil::sformat(
                               "bgr[%dx%d]_%d.bmp", m_pInfo->m_nWidth, m_pInfo->m_nHeight, m_pInfo->m_nIPCamStreamIdx );
                           dst_bgr_image.WriteBMPFile( filename.c_str() );
#endif // __SUPPORT_GRAY_IMAGE_USE
                        }
#endif // DEBUG_BMP_IMAGE

                        m_evtGetImage.SetEvent();

                        //------------------------------------------------------------------------------------------
                        if( m_pcbDecodeFrame ) {
                           VC_PARAM_STREAM input;
                           input.pParam     = m_pVideoChannel;
                           input.nVCState   = 0;
                           input.nCount     = m_nDecodeCount;
                           input.nStreamIdx = m_nICameraIdx;
                           input.nCamState  = 0;
                           input.pYUY2Buff  = m_SrcImg;
                           input.nWidth     = m_pInfo->m_nWidth;
                           input.nHeight    = m_pInfo->m_nHeight;
                           m_pcbDecodeFrame( input ); // 비디오 스트림 추출 콜백 함수 호출.
                        }
                        m_bFirstDecompressOK = TRUE;
                        if( m_dfNormalPlayTime < pBuffItem->m_dfNormalPlayTime )
                           m_dfNormalPlayTime = pBuffItem->m_dfNormalPlayTime;
                        else {
                           FILETIME ftCurrent;
                           AfxGetLocalFileTime( &ftCurrent );
                           double dfOffset = GetElapsedMilliSec( m_ftLastDecodeFrameTime, ftCurrent ) / 1000.0;
                           m_dfNormalPlayTime += dfOffset;
                        }

                        AfxGetLocalFileTime( &m_ftLastDecodeFrameTime );
                     }
                  } else {
                     // 디코드를 하지 않을 때에는 리소스를 절약하기 위헤 디코더를 닫는다.
                     if( m_pStream->m_VideoDecoder.IsOpen() ) {
                        m_pStream->m_VideoDecoder.Close();
                     }
                  }
               }

               m_pStream->m_dwLastCapture = GetTickCount();

               delete pBuffItem;
               m_nFrameCount++;

               if( CIPCamStream::DecompressResult_VideoSizeIsChanged == decompress_result ) {
                  logw( "[CH:%d][Stream:%d] Decompress Error - Video size has changed!!\n", m_nChannelNo_IVX + 1, m_nICameraIdx + 1 );
               }
               if( CIPCamStream::DecompressResult_FrameRateIsChanged == decompress_result ) {
                  logw( "[CH:%d][Stream:%d] Decompress Error - Frame rate has changed!!\n", m_nChannelNo_IVX + 1, m_nICameraIdx + 1 );
               }

               // [중요]소스버퍼 변경에 대한 처리
               // 소스버퍼의 크기가 변경되자마다 바로 버퍼의 크기를 변경하게되면 이 버퍼를 참조하여 사용하는 곳에서 메모리 참조 오류가 발생하게 된다.
               // 이를 방지하기 위해 버퍼를 변경하기 전에 미리 소스버퍼를 변경하는 중이라는 공지를 한 이후
               // 충분히 다른 스레드에서 버퍼 변경에 대한 대비를 할 정도의 시간만큼 기다린 후 소스버퍼의 크기를 변경한다.
               // 소스버퍼를 카피하지않고 참조하는 이유 --> 메모리 절감을 위해

               int nSrcImgBuffSize = GetYUY2ImageLength( m_pInfo->m_nWidth, m_pInfo->m_nHeight );
               if( m_SrcImg.Length != nSrcImgBuffSize ) {
                  // [중요] 소스버퍼의 크기가 변경되면 코덱을 미리 닫아 두어야 한다.
                  // CAVCodec 내부에는 Defog, Video Stabilization 알고리즘을 별도의 스레드에서 수행하도록 되어있고
                  // 이 알고리즘이 m_SrcImage버퍼에 쓰기 때문이다.
                  m_pStream->m_VideoDecoder.CloseImageEnhancement();
                  // 소스버퍼의 크기 변경을 공지
                  m_nState |= State_ChangingVideoBuffer;
                  // 약 3초간 대기 - 소스버퍼에 접근하는 스레드에서 모든 접근을 마칠 때까지 충분한 시간임.
                  float fSrcImgChangingWaitingTime = 3.0f;
                  float fFrameRate                 = m_pTimer_ConnectionAndDecode->GetFPS();
                  int nWaitCount                   = int( fFrameRate * fSrcImgChangingWaitingTime );
                  int i;
                  for( i = 0; i < nWaitCount; i++ ) {
                     if( m_nState & State_SiganlStopCapture ) {
                        break;
                     }
                     m_pTimer_ConnectionAndDecode->Wait();
                  }
                  // 소스 버퍼 크기 변경
                  m_csSrcImg.Lock();
                  m_SrcImg.Create( nSrcImgBuffSize + FFMPEG_BUFFER_PADDING_SIZE * 4 );
                  m_SrcImg.Length = nSrcImgBuffSize;
                  m_csSrcImg.Unlock();
                  m_nState &= ~State_ChangingVideoBuffer;
                  logw( "[CH:%d][Stream:%d]     (%d,%d) -> (%d,%d)\n", m_nChannelNo_IVX + 1, m_nICameraIdx + 1, m_PrevSrcImgSize.Width, m_PrevSrcImgSize.Height, m_pInfo->m_nWidth, m_pInfo->m_nHeight );
                  // 소스 버퍼 크기 변경을 이벤트로 알림.
                  if( m_pCBCamEvent ) {
                     VC_PARAM_CAMERA_EVENT input;
                     input.pParam      = m_pCBCamEventParam;
                     input.nStreamIdx  = m_nICameraIdx;
                     input.PrevImgSize = m_PrevSrcImgSize;
                     input.nCamEvent  = VCCamEvent_VideoSizeIsChanged;
                     
                     if(m_PrevSrcImgSize.Width < m_pInfo->m_nWidth || m_PrevSrcImgSize.Height < m_pInfo->m_nHeight) {
                        input.nCamEvent  = VCCamEvent_VideoSizeIsChanged_Bigger;
                     }
                     
                     m_pCBCamEvent( input ); 
                  }
                  m_PrevSrcImgSize( m_pInfo->m_nWidth, m_pInfo->m_nHeight );
               }

               if( CIPCamStream::DecompressResult_VideoSizeIsChanged == decompress_result || CIPCamStream::DecompressResult_FrameRateIsChanged == decompress_result ) {
                  m_nState |= State_ResolutionChanged;
                  break;
               }
            }
            int nFrameQueueSize = int(compBuffQueue.Size());

            // yhpark. why unused. ???
            //int nFrameLimit     = int( ceil( m_pInfo->m_fFPS_Cap ) );

            float fDecodingFPS = m_pInfo->m_fFPS_Cap;
            // 0.5초 분량 보다 많이 쌓였으면 프레임 비율을 2배 높인다.
            if( ( GetTickCount() - m_nConnectStartTime < 500 ) || ( nFrameQueueSize > int( ceil( m_pInfo->m_fFPS_Cap / 2.0f ) ) ) ) {
               fDecodingFPS *= 2.0f;
            }
            // 0.33초 분량 보다 많이 쌓였으면 프레임 비율을 1.2배 높인다.
            else if( nFrameQueueSize > int( ceil( m_pInfo->m_fFPS_Cap / 0.3f ) ) ) {
               fDecodingFPS *= 1.2f;
            }

            //fDecodingFPS = m_pInfo->m_fFPS_Cap;
            m_pTimer_ConnectionAndDecode->SetFPS( fDecodingFPS );
            m_pTimer_ConnectionAndDecode->Wait();

            // yhpark monitoring.
            //            LOGD << "CH[" << m_nChannelNo_IVX
            //                 << "] nFrameQueueSize[" << nFrameQueueSize
            //                 << std::fixed << std::setw( 4 ) << std::setprecision( 2 )
            //                 << "] Timer[" << m_pTimer_ConnectionAndDecode->GetFPS()
            //                 << "] m_fFrameRate[" << m_fFrameRate // 측정값.
            //                 << "] org_fps[" << m_pInfo->m_fFPS_Cap
            //                 << "] cur_fps[" << fDecodingFPS
            //                 << "]";

            // 프레임이 5초 분량 이상 쌓였으면 키프레임 찾기 상태로 전환하고 디코드루프를 빠져나온다.
            if( nFrameQueueSize > int( m_pInfo->m_fFPS_Cap ) * 5 ) {
               m_bFindFirstKeyFrame = FALSE;
               break;
            }
         }
         // IP 카메라 멈춤 명령에 의한 종료
         if( m_nState & State_SiganlStopCapture ) {
            break;
         }
         // Stream 오류에 의한 종료
         else if( m_pStream->m_nState & CIPCamStream::State_SignalConnectionToBeClosed ) {
            break;
         }

         nPrevIPCamState = m_nState;
      } // Frame Extract Loop : end-of-while (1)

#ifdef __SUPPORT_PTZ_CAMERA
      if( m_pPTZControl ) {
         if( m_pPTZControl->m_nCapabilities & PTZCtrlCap_ConnectionDependOnStream ) {
            m_pPTZControl->m_nState &= ~PTZ_State_Connected;
         }
      }
#endif

      m_pStream->m_nState &= ~CIPCamStream::State_Connecting;
      m_nState &= ~State_Connectting;

      // 인증 실패 시에는 잠시 대기..
      if( m_nState & State_AuthorizationFailed ) {
         uint nReconnectPeriodInMilliSec = int( 1000.0f / fReconnectFPS );
         uint32_t nWaitStartTime         = GetTickCount();
         while( 1 ) {
            if( m_nState & State_SiganlStopCapture )
               break;
            if( bEscape_ConnectionLoop )
               break;
            if( GetTickCount() - nWaitStartTime > nReconnectPeriodInMilliSec )
               break;
            m_pTimer_ConnectionAndDecode->Wait();
         }
      }

      if( m_nState & State_SiganlStopCapture ) {
         LOGW << "State_SiganlStopCapture break";
         break;
      }
      if( bEscape_ConnectionLoop ) {
         LOGE << "bEscape_ConnectionLoop break";
         break;
      }

   } // Connection Loop
   m_pTimer_ConnectionAndDecode->StopTimer();

   m_pStream->EndCapture();
   logw( "[CH:%d][Stream:%d] ConnectionAndDecodingProc - Exit\n", m_nChannelNo_IVX + 1, m_nICameraIdx + 1 );

   m_nState &= ~State_Capture;

   return ( DONE );
}

UINT CCamera_IP::Thread_ConnectionAndDecoding( LPVOID pVoid )
{
   CCamera_IP* pCamera = (CCamera_IP*)pVoid;

#ifdef SUPPORT_PTHREAD_NAME
   char thread_name[16] = {};
   sprintf( thread_name, "Decode_c%d_u%d", pCamera->m_nChannelNo_IVX, pCamera->m_nICameraIdx );
   pthread_setname_np( pthread_self(), thread_name );
#endif
   pCamera->ConnectionAndDecodingProc();

   return TRUE;
}

BOOL CCamera_IP::IsVideoSignalAlive()
{
   BOOL bVideoSignalalive = FALSE;
   if( IPCamState_PausingCapture & m_nIPCamState ) {
      bVideoSignalalive = m_bLastVideoSignal;
   } else {
      bVideoSignalalive = ( m_nState & State_VideoSignalNotOK ) ? FALSE : TRUE;
   }
   return bVideoSignalalive;
}

BOOL CCamera_IP::CheckingVideoSignalAlive()
{
   if( m_nState & State_Initialize ) // TODO(jongoh): 초기화 중일때는 신호 처리를 나중에 한다.
      return TRUE;

   BOOL bVideoSignalAlive = TRUE;
   if( !( m_nState & State_ConnectionOk ) )
      bVideoSignalAlive = FALSE;

   if( IPCamState_PausingCapture & m_nIPCamState ) {
      bVideoSignalAlive = m_bLastVideoSignal;
   } else {
      if( FALSE == m_pStream->IsVideoSignalAlive() )
         bVideoSignalAlive = FALSE;

      if( bVideoSignalAlive )
         m_nState &= ~State_VideoSignalNotOK;
      else
         m_nState |= State_VideoSignalNotOK;
   }

   return bVideoSignalAlive;
}

int CCamera_IP::SetDefaultImageProperty( int nImagePropertyType )
{
   int nDefaultValue;
   nDefaultValue = m_pProperty->m_ImageProperty[nImagePropertyType].m_nDefaultValue;
   return SetImageProperty( nImagePropertyType, nDefaultValue );
}

int CCamera_IP::SetAllImageProperty()
{
   return m_pStream->SetAllImageProperty();
}

int CCamera_IP::SetImageProperty( int nImagePropertyType, int nValue, int nFlagAuto )
{
   return m_pStream->SetImageProperty( nImagePropertyType, nValue, nFlagAuto );
}

int CCamera_IP::GetImageProperty( int nImagePropertyType, int& nValue, int& nFlagAuto )
{
   nFlagAuto = FALSE;
   return m_pStream->GetImageProperty( nImagePropertyType, nValue, nFlagAuto );
}

int CCamera_IP::GetImagePropertyRange( int nImagePropertyType, int& nMin, int& nMax, int& nTic )
{
   if( !m_pProperty ) return ( 1 );
   ImageProperty& ip = m_pProperty->m_ImageProperty[nImagePropertyType];
   if( ip.m_bUse ) {
      m_pProperty->m_ImageProperty[nImagePropertyType].GetRange( nMin, nMax, nTic );
      return ( DONE );
   }
   return ( 2 );
}

void CCamera_IP::GetSupportedImagePropertyList( std::vector<int>& vtImagePropertyType )
{
   int i;
   vtImagePropertyType.clear();
   for( i = 0; i < ImagePropertyTypeNum; i++ ) {
      if( m_pProperty->m_ImageProperty[i].m_bUse )
         vtImagePropertyType.push_back( i );
   }
}

int CCamera_IP::GetImageSize()
{
   return m_pInfo->m_nWidth * m_pInfo->m_nHeight;
}

void CCamera_IP::GetCameraDesc( std::string& strDesc )
{
   strDesc = sutil::sformat( "%s: (%s)", "IP Camera / Video Server", m_pProperty->m_strProdName.c_str() );
}

BOOL CCamera_IP::IsConnected()
{
   BOOL bCheck( FALSE );
   if( m_pStream ) {
      bCheck = m_pStream->IsConnected();
   }
   return bCheck;
}

BOOL CCamera_IP::IsFirstFrameJustDecoded()
{
   return m_bFirstDecompressOK;
}

int CCamera_IP::RequestIPCameraInfo()
{
   return m_pStream->RequestIPCameraInfo();
}

#ifdef __SUPPORT_PTZ_CAMERA
void CCamera_IP::SetPTZControl( CPTZCtrl* pPTZControl )
{
   m_pPTZControl = pPTZControl;
}
#endif

BOOL CCamera_IP::GetDecodeVideo()
{
   return m_bDecodeVideo;
}

void CCamera_IP::SetDecodeVideo( BOOL bDecodeVideo )
{
   if( !m_bDecodeVideo && bDecodeVideo ) {
      m_bFrameSkippingUntilKeyFrame = TRUE;
      m_bFindFirstKeyFrame          = FALSE;
   }
   m_bDecodeVideo = bDecodeVideo;
   if( !bDecodeVideo ) m_pInfo->m_fDecodingFPS = 0;
}

int CCamera_IP::GetMotionStatus( BOOL& bMotion )
{
   return m_pStream->GetMotionStatus( bMotion );
}

int CCamera_IP::GetAlarmInputStatus( int nChIdx, BOOL& bSignal )
{
   return m_pStream->GetAlarmInputStatus( nChIdx, bSignal );
}

int CCamera_IP::GetAlarmInputFlags()
{
   return m_pStream->GetAlarmInputFlags();
}

int CCamera_IP::SetRelayOutputStatus( int nChIdx, BOOL bSignal )
{
   CDigitalIO::SetRelayOutputStatus( nChIdx, bSignal );
   CDigitalIO::GetRelayOutputStatus( nChIdx, bSignal );
   BOOL bNormalClose;
   CDigitalIO::GetOutputNormalStatus( nChIdx, bNormalClose );
   if( bNormalClose )
      bSignal = !bSignal;
   m_pStream->SetRelayOutputStatus( nChIdx, bSignal );
   return DONE;
}

int CCamera_IP::GetRelayOutputStatus( int nChIdx, BOOL& bSignal )
{
   CDigitalIO::GetRelayOutputStatus( nChIdx, bSignal );
   m_pStream->GetRelayOutputStatus( nChIdx, bSignal );
   return DONE;
}

int CCamera_IP::StartAudioInOut() // (xinu_be22)
{
   if( NULL == m_pStream ) return ( 1 );
   return m_pStream->StartAudioInOut();
}

int CCamera_IP::StopAudioInOut()
{
   if( NULL == m_pStream ) return ( 1 );
   return m_pStream->StopAudioInOut();
}

int CCamera_IP::TransferAudio( const BYTE* pBuffer, int nBuffSize )
{
   if( NULL == m_pStream ) return ( 1 );
   return m_pStream->TransferAudio( pBuffer, nBuffSize );
}

int CCamera_IP::StopTalking()
{
   m_pStream->StopTalking();
   return ( DONE );
}

int CCamera_IP::StartTalking()
{
   m_pStream->StartTalking();
   return ( DONE );
}

int CCamera_IP::IsEnableAudioDataTransmit()
{
   if( NULL == m_pStream ) return FALSE;
   return m_pStream->IsEnableAudioDataTransmit();
}

int CCamera_IP::GetIPCamState()
{
   return m_nIPCamState;
}

void CCamera_IP::ModifyIPCamState( int nRemove, int nAdd )
{
   m_csIPCamState.Lock();
   m_nIPCamState &= ~nRemove;
   m_nIPCamState |= nAdd;
   m_csIPCamState.Unlock();
}

CIPCamStream* CCamera_IP::GetIPCamStream()
{
   return m_pStream;
}

#ifdef __SUPPORT_PTZ_CAMERA
void CCamera_IP::LoadPTZPosToPresetMap()
{
   if( m_pInfo == nullptr ) {
      LOGF << "m_pInfo is nullptr";
      assert( m_pInfo );
   }

   if( m_pPTZControl ) {
      std::string strSID, strSIDFull;
      DWORD nIPAddress = 0;
      std::string strIPAddress;

      GetIPAddrString( strIPAddress, m_pInfo->m_strIPAddress ); // 도메인 주소를 uint형 IP로 바꾼다.
      GetIPAddress( nIPAddress, strIPAddress );
      strSID     = sutil::sformat( "%s_%02d", m_pInfo->m_strIPAddress.c_str(), m_pInfo->m_nVideoChNo + 1 );
      strSIDFull = sutil::sformat( "%s_%02d_%02d_%02d", m_pInfo->m_strIPAddress.c_str(), m_pInfo->m_nVideoChNo + 1, m_pInfo->m_nPortNo, m_pInfo->m_nPTZPortNo );

      CPTZ2D* pPTZ2D;
      if( IPCameraType_RTSP == m_pInfo->m_nIPCameraType ) {
         std::string strSIDFullRTSP;
         strSIDFullRTSP = sutil::sformat( "%s_%02d_%02d_%02d", m_pInfo->m_strURL.c_str(), m_pInfo->m_nVideoChNo + 1, m_pInfo->m_nPortNo, m_pInfo->m_nPTZPortNo );
         if( DONE != __PTZ3DFile.FindPTZ2D( strSIDFullRTSP.c_str(), &pPTZ2D ) ) {
            if( DONE != __PTZ3DFile.FindPTZ2D( strSIDFull.c_str(), &pPTZ2D ) ) {
               if( DONE != __PTZ3DFile.FindPTZ2D( strSID.c_str(), &pPTZ2D ) ) {
                  __PTZ3DFile.AddPTZ2D( strSIDFullRTSP.c_str(), &pPTZ2D );
               }
            }
         }
         strcpy( pPTZ2D->SID, strSIDFullRTSP.c_str() );
      } else {
         if( DONE != __PTZ3DFile.FindPTZ2D( strSIDFull.c_str(), &pPTZ2D ) ) {
            if( DONE != __PTZ3DFile.FindPTZ2D( strSID.c_str(), &pPTZ2D ) ) {
               __PTZ3DFile.AddPTZ2D( strSIDFull.c_str(), &pPTZ2D );
            }
         }
         strcpy( pPTZ2D->SID, strSIDFull.c_str() );
      }

      if( pPTZ2D ) {
         m_pPTZControl->SetPTZ2D( pPTZ2D );
         pPTZ2D->NRef++;
      }
   }
}
#endif

void CCamera_IP::GetVideoFrmRecvCount( int& nVideoFrmRecvCount, int& nVideoKeyFrmRecvCount )
{
   if( m_pStream ) {
      if( m_pStream->m_pCompBuffQueue ) {
         nVideoFrmRecvCount                                   = m_pStream->m_pCompBuffQueue->m_nVideoFrmRecvCount;
         nVideoKeyFrmRecvCount                                = m_pStream->m_pCompBuffQueue->m_nVideoKeyFrmRecvCount;
         m_pStream->m_pCompBuffQueue->m_nVideoFrmRecvCount    = 0;
         m_pStream->m_pCompBuffQueue->m_nVideoKeyFrmRecvCount = 0;
      }
   }
}

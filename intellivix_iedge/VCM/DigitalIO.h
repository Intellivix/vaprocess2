#pragma once

const int MaxIONum = 64;

//////////////////////////////////////////////////////////////
//
// Structure : DigitalInforInfo
//
//////////////////////////////////////////////////////////////

struct DigitalInputInfo
{
   std::string m_strName;
   BOOL     m_bNormalClose;

   DigitalInputInfo ();
};

//////////////////////////////////////////////////////////////
//
// Structure : DigitalOutputInfo
//
//////////////////////////////////////////////////////////////

struct DigitalOutputInfo
{
   std::string m_strName;
   BOOL     m_bNormalClose;
   int      m_nSignalCount;

   DigitalOutputInfo ();
};

//////////////////////////////////////////////////////////////
//
// Class : CDigitalIO
//
//////////////////////////////////////////////////////////////

class CDigitalIO
{
public:
   int  m_nInputNum;
   int  m_nOutputNum;
   DigitalInputInfo   m_nInputs [MaxIONum];
   DigitalOutputInfo  m_nOutputs[MaxIONum];

public:
   CDigitalIO(void);
   virtual ~CDigitalIO(void);
   CDigitalIO& operator= (CDigitalIO& from);

   void Initialize (int nInputNum, int nOutputNum);

   BOOL Read  (CXMLIO* pIO);
   BOOL Write (CXMLIO* pIO);

   virtual int GetAlarmInputStatus  (int nChIdx, BOOL& bSignal);
   virtual int SetRelayOutputStatus (int nChIdx, BOOL  bSignal);
   virtual int GetRelayOutputStatus (int nChIdx, BOOL& bSignal);

   int SetInputNormalStatus (int nChIdx, BOOL bNormalClose);
   int GetInputNormalStatus (int nChIdx, BOOL& bNormalClose);
   int SetInputName         (int nChIdx, const std::string& strInputName);
   int GetInputName         (int nChIdx, std::string& strInputName);

   int SetOutputNormalStatus (int nChIdx, BOOL bNormalClose);
   int GetOutputNormalStatus (int nChIdx, BOOL& bNormalClose);
   int SetOutputName         (int nChIdx, const std::string& strOutputName);
   int GetOutputName         (int nChIdx, std::string& strOutputName);
};

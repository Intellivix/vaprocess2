﻿#include "stdafx.h"
#include "bccl_environ.h"
#include "vacl_ipp.h"
#include "version.h"
#include "CommonUtil.h"
#include "VideoChannel.h"
#include "CameraProductAndItsProperties.h"
#include "Camera_IP.h"
#include "Camera_File.h"
#include "Camera_VirtualPTZ.h"
#include "IPCameraManager.h"
#include "PTZCtrl.h"
#include "PTZCtrl_Linking.h"
#include "FrameRateChecker.h"
#include "SupportFunction.h"
#include "TimerCheckerManager.h"
#include "IPCamStream_RTSP.h"

extern BOOL g_bSignalSourceCameraToVirtualPTZCamera;

CVideoChannel::CVideoChannel()
{
   m_nState                     = VCState_None;
   m_nInnerState                = VCInnerState_None;
   m_nCameraNum                 = 0;
   m_nCountIP                   = 0;
   m_nCountCap                  = 0;
   m_nVideoSignalFlag           = 0;
   m_nNoVideoSignalFlag         = 0;
   m_nConnectingChFlag          = 0;
   m_nConnectionFailedChFlag    = 0;
   m_nAuthorizationFailedChFlag = 0;
   m_nPrevVideoSignalFlag       = 0;

   m_bAllSignalOK       = FALSE;
   m_bSetupTemp         = FALSE;
   m_nVideoSignalFlag   = 0;
   m_nVCIdx             = 0;
   m_nThreadNum         = VCThreadNum;
   m_nCurExecThreadFlag = 0;
   m_pYUY2Buff          = NULL;
   ZeroMemory( m_pTimer, sizeof( CMMTimer* ) * m_nThreadNum );
   int i;
#ifdef __SUPPORT_PTZ_AREA
   ISize2D refImageSize( REF_IMAGE_WIDTH, REF_IMAGE_HEIGHT );
   for( i = 0; i < PTZAreaTypeNum; i++ )
      m_PTZAreas[i].SetRefImageSize( refImageSize );
   m_pCurMutualAssistanceArea = NULL;
#endif
   m_bDrawPrivacyZone = TRUE;

#ifdef __SUPPORT_PTZ_CAMERA
   m_pPTZCtrl            = NULL;
   m_nPTZControlOpenMode = PTZCtrlOpenMode_Normal;
#endif

   for( i = 0; i < VCThreadNum; i++ ) {
      m_bThreadAlive[i]   = FALSE;
      m_bStopThead[i]     = FALSE;
      m_bNoVideoSignal[i] = FALSE;
      m_pTimer[i]         = NULL;
   }

   // Multi Streaming
   m_nVCDecodeAndConnectionPolicy = VCDecodeAndConnectionPolicy_AdaptiveDecoding;
   for( i = 0; i < VCProcCallbackFuncType_Num; i++ ) {
      m_bUseProcCallbackFunc[i]                 = 0;
      m_nDecodeStreamIndexOnProcCallbackFunc[i] = 0;
      m_bSignalToExecProcCallbackFunc[i]        = FALSE;
   }
   ZeroMemory( m_bSignalToVertualPTZCameras, sizeof( m_bSignalToVertualPTZCameras ) );
   ZeroMemory( m_bUseOfStreamIdxInDecodeCallBackFunc, sizeof( m_bUseOfStreamIdxInDecodeCallBackFunc ) );
   ZeroMemory( m_bUseOfStreamIdxInEncodeCallBackFunc, sizeof( m_bUseOfStreamIdxInEncodeCallBackFunc ) );

   m_PrevVideoSize( 0, 0 );
   m_nCylinderWidth  = 0;
   m_nCylinderHeight = 0;
   m_nDPWidth        = 0;
   m_nDPHeight       = 0;

#ifdef __SUPPORT_FISHEYE_CAMERA
   m_bFisheyeInit      = FALSE;
   m_bFisheyeInit_VPTZ = FALSE;
#endif
#ifdef __SUPPORT_IMAGE_ENHANCEMENT
   m_pImageEnhancer = NULL;
#endif
   m_fPan  = 0.0f;
   m_fTilt = 45.0f;

   m_nCountDefog         = 0;
   m_nCountStabilization = 0;

   m_dfNormalPlayTime = 0.0;

   m_nVCID      = -1;
   m_bEndOfFile = FALSE;

   m_ftTimestamp.dwHighDateTime            = 0;
   m_ftTimestamp.dwLowDateTime             = 0;
   m_ftLastImageProcessTime.dwHighDateTime = 0;
   m_ftLastImageProcessTime.dwLowDateTime  = 0;
   m_dfImageProcessDelayTime               = 0.0;
   m_dfAvgImageProcessDelayTime            = 0.0;
}

CVideoChannel::~CVideoChannel()
{
#ifdef __SUPPORT_IMAGE_ENHANCEMENT
   SAFE_DELETE( m_pImageEnhancer );
#endif

   if( !m_bSetupTemp )
      Destroy();
}

void CVideoChannel::Create( VC_Desc& desc )
{
   CCriticalSectionSP co( m_csVideoChannel );
   SetVideoChannelDesc( desc );
   m_nState &= ~( VCState_FailureOpenSerialPort );
   // 비디오 채널 정보를 복사하고 초기화 한다.
   CVideoChannelInfo& info = GetVideoChannelInfo();
   info                    = *desc.m_pInfo;
   info.CheckParameters();
   //info.SetVideoSize (m_pAnalogCamMgr->GetInfo ());
   if( m_bSetupTemp )
      return;
   //logd ("333 m_nCameraFlag:%x\n", m_nCameraFlag);
}

void CVideoChannel::Destroy()
{
   CCriticalSectionSP co( m_csVideoChannel );
   int i;

#ifdef __SUPPORT_PTZ_CAMERA
   DeletePTZControl(); // Deactivate시 연결을 끊기 때문에 그전에 PTZ카메라를 삭제한다.
#endif
   Deactivate();
#ifdef __SUPPORT_PTZ_CAMERA
#ifdef __SUPPORT_VIRTUAL_PTZ_CAMERA
   RemoveVirtualPTZCamerasOnDestroy();
#endif
#endif
   m_csCameras.Lock();
   if( m_pTimerMgr ) {
      for( i = 0; i < m_nThreadNum; i++ ) {
         if( m_pTimer[i] ) {
            m_pTimerMgr->DeleteTimer( m_pTimer[i]->m_nTimerID );
            m_pTimer[i] = NULL;
         }
      }
   }
   DeleteCameras();
   m_csCameras.Unlock();
}

int CVideoChannel::Initialize()
{
   INT nRet = -1;
   if( !m_pVCM ) return nRet;
   CCriticalSectionSP co1( m_pVCM->m_csVideoChannelList );
   CCriticalSectionSP co( m_csVideoChannel );
   //logd( "[CH:%d] CVideoChannel::Initialize - Start\n", m_nChannelNo_IVX + 1 );

   BOOL bActivate = ( m_nState & VCState_Activate ) ? TRUE : FALSE;
   if( bActivate ) {
      Deactivate();
   }
   m_csCameras.Lock();
   CVideoChannelInfo& info = GetVideoChannelInfo();
   info.CheckParameters();
#ifdef __SUPPORT_PTZ_CAMERA
   DeletePTZControl();
#endif
   DeleteCameras();
   nRet = CreateCameras();
   if( nRet == DONE ) {
      //logd( "[CH:%d] CVideoChannel::Initialize.", m_nChannelNo_IVX + 1 );
   } else {
      loge( "[CH:%d] CVideoChannel::Initialize. errCode[%d]", m_nChannelNo_IVX + 1, nRet );
   }
#ifdef __SUPPORT_PTZ_CAMERA
   if( nRet == DONE && m_bPTZCamera )
      CreatePTZControl( m_nPTZControlOpenMode );
#endif
#ifdef __SUPPORT_DAYNIGHT_DETECTION
   m_DayNightDetector.Initialize( m_pSunCalculator );
#endif
   m_csCameras.Unlock();

   if( bActivate ) {
      Activate();
   }
   return ( nRet );
}

void CVideoChannel::Initialize( CVideoChannelInfo& newInfo )
{
   if( m_nState & VCState_ChangeSetting )
      return;
   m_nState |= VCState_ChangeSetting;

   BOOL bActivate = ( m_nState & VCState_Activate ) ? TRUE : FALSE;

   m_csCameras.Lock();
   if( bActivate )
      Deactivate();
#ifdef __SUPPORT_PTZ_CAMERA
   DeletePTZControl();
#endif
   DeleteCameras();

   // 새로운 정보로 설정을 변경한다.
   CVideoChannelInfo& info = GetVideoChannelInfo();
   info                    = newInfo;
   CheckParameters();
   INT nRet = CreateCameras();
#ifdef __SUPPORT_PTZ_CAMERA
   if( nRet == DONE && m_bPTZCamera )
      CreatePTZControl( m_nPTZControlOpenMode );
#endif
   m_csCameras.Unlock();
#ifdef __SUPPORT_DAYNIGHT_DETECTION
   m_DayNightDetector.Initialize( m_pSunCalculator );
#endif
   if( bActivate )
      Activate();

   m_nState &= ~VCState_ChangeSetting;
}

void CVideoChannel::Activate( int nThreadFlags )
{
   //logd( "[CH:%d] VideoChannelActivate - Started\n", m_nChannelNo_IVX + 1 );
   if( !m_pVCM ) {
      LOGE << "CVideoChannel::Activate - no VCM";
      return;
   }

   // [중요] 본 함수 내부에 비디오 채널 리스트에 접근하는 함수를 사용하는 경우
   // "비디오 채널 리스트" 임계영역을 "비디오 채널" 임계영역보다 먼저 지정해 주어야 Dead Lock이 발생하지 않는다.
   if( m_nState & VCState_Activate ) {
      LOGE << "CVideoChannel::Activate - Already activated";
      return;
   }

   CCriticalSectionSP co1( m_pVCM->m_csVideoChannelList );
   CCriticalSectionSP co2( m_csVideoChannel );

   m_bEndOfFile = FALSE;
   m_nState |= VCState_Activate;
   m_nThreadFlag = nThreadFlags;

   m_csCameras.Lock();
   for( int i = 0; i < m_nCameraNum; i++ ) {
      ICamera* pCamera = m_Cameras[i];
      if( pCamera ) {
         m_Cameras[i]->BeginCapture();
      }
   }
   m_csCameras.Unlock();

   m_nVideoSignalFlag        = 0;
   m_nNoVideoSignalFlag      = 0;
   m_nConnectingChFlag       = 0;
   m_nConnectionFailedChFlag = 0;
   for( int i = 0; i < m_nCameraNum; i++ ) {
      UINT64 nChFlag = UINT64( 1 ) << i;
      m_nNoVideoSignalFlag |= nChFlag;
      m_nConnectionFailedChFlag |= nChFlag;
   }
   m_bAllSignalOK         = FALSE;
   m_nPrevVideoSignalFlag = 0;

   m_pYUY2Buff = NULL;

   m_dfImageProcessDelayTime    = 0.0;
   m_dfAvgImageProcessDelayTime = 0.0;

   DeactivateThread( VCThreadType_All );
   //ActivateThread (nThreadFlags);

   //logv( "[CH:%d] VideoChannelActivate - Ended\n", m_nChannelNo_IVX + 1 );
}

void CVideoChannel::Deactivate()
{
   if( !m_pVCM ) {
      LOGE << "no VCM";
      return;
   }

   CCriticalSectionSP co1( m_pVCM->m_csVideoChannelList );
   CCriticalSectionSP co2( m_csVideoChannel );

   if( !( m_nState & VCState_Activate ) )
      return;

   m_pVCM->m_nVCState[m_nVCIdx] |= VCState_Deactivating;

   logd( "[CH:%d] VideoChannelDeactivate - Start\n", m_nChannelNo_IVX + 1 );
   m_nState |= VCState_Deactivating;
   m_nState |= VCState_SignalDeactivate;

#ifdef __SUPPORT_PTZ_CAMERA
#ifdef __SUPPORT_VIRTUAL_PTZ_CAMERA
   // 가상 PTZ 채널이 본 채널을 참조하고 있는 경우 가상 PTZ 채널을 Deactivate 시킨다.
   ResetVirtualPTZCamerasOnDeactivate();

   // 연관 PTZ채널 포인터 해제
   ReleaseLinkedPTZControlPointers();
#endif // __SUPPORT_VIRTUAL_PTZ_CAMERA
#endif // __SUPPORT_PTZ_CAMERA

   DeactivateThread();

   m_nState &= ~VCState_SignalDeactivate;
   logd( "[CH:%d] VideoChannelDeactivate - 2\n", m_nChannelNo_IVX + 1 );

   // 모든 스레드가 종료되었음을 확인한 후 캡쳐를 중단한다.
   m_csCameras.Lock();
   logd( "[CH:%d] VideoChannelDeactivate - 3 m_nCameraNum:%d\n", m_nChannelNo_IVX + 1, m_nCameraNum );
   for( int i = 0; i < m_nCameraNum; i++ ) {
      ICamera* pCamera = m_Cameras[i];
      if( pCamera ) {
         logd( "[CH:%d] VideoChannelDeactivate - EndCapture(%d)\n", m_nChannelNo_IVX + 1, i );
         pCamera->EndCapture();
      }
   }
   m_csCameras.Unlock();

   m_nState &= ~VCState_Activate;
   m_nState &= ~VCState_Deactivating;

   m_pVCM->m_nVCState[m_nVCIdx] &= ~VCState_Deactivating;

   logd( "[CH:%d] VideoChannelDeactivate - OK\n", m_nChannelNo_IVX + 1 );
}

void CVideoChannel::SignalDeactivate()
{
   if( !m_pVCM ) return;
   CCriticalSectionSP co1( m_pVCM->m_csVideoChannelList );
   CCriticalSectionSP co2( m_csVideoChannel );

   if( !( m_nState & VCState_Activate ) )
      return;

   int i;
   if( m_pTimerMgr ) {
      for( i = 0; i < m_nThreadNum; i++ ) {
         if( m_pTimer[i] )
            m_pTimer[i]->GetTimerEvent().SetEvent();
      }
   }
   m_nState |= VCState_SignalDeactivate;

   m_csCameras.Lock();
   for( i = 0; i < m_nCameraNum; i++ ) {
      ICamera* pCamera = m_Cameras[i];
      if( pCamera )
         pCamera->SignalStopCapture();
   }
#ifdef __SUPPORT_PTZ_CAMERA
   if( m_pPTZCtrl ) {
      m_pPTZCtrl->SignalToClose(); // jun 2013-11-08 : PTZ카메라가 종료할 수 있도록 신호를 해야한다.
      // 특히 TCP 연결을 미리 종료하기 위해 필요하다.
   }
#endif
   m_csCameras.Unlock();
}

#ifdef __SUPPORT_PTZ_CAMERA
#ifdef __SUPPORT_VIRTUAL_PTZ_CAMERA
void CVideoChannel::ResetVirtualPTZCamerasOnDeactivate()
{
   if( !g_bSignalSourceCameraToVirtualPTZCamera ) return;

   int i;

   // 가상 PTZ 채널이 본 채널을 참조하고 있는 경우 가상 PTZ 채널을 Deactivate 시킨 후
   // 가상 PTZ가 참조하고 있던 소스 비디오채널 객체를 해제 시킨 후
   // 다시 해당 채널을 Activate 시킨다.
   if( !m_pVCM ) return;
   CCriticalSectionSP so1( m_pVCM->m_csVideoChannelList );
   CCriticalSectionSP so2( m_csVideoChannel );
   CCriticalSectionSP so3( m_csVirtualPTZCameras );
   std::deque<CVideoChannel*> virtual_ptz_cameras = m_VirtualPTZCameras;
   int nVirtualPTZCameraNum                       = (int)virtual_ptz_cameras.size();
   for( i = 0; i < nVirtualPTZCameraNum; i++ ) {
      CVideoChannel* pVideoChannel = virtual_ptz_cameras[i];
      if( VideoChannelType_VirtualPTZ == pVideoChannel->m_nVideoChannelType ) {
         CCamera_VirtualPTZ* pVirtualPTZCamera = (CCamera_VirtualPTZ*)pVideoChannel->m_Cameras[0];
         pVirtualPTZCamera->m_nVirtualPTZCamState |= VirtualPTZCameraState_ReactivatingForResetingSourceVideoChannel;
         pVideoChannel->Deactivate();
         if( pVideoChannel->m_Cameras.size() ) {
            pVirtualPTZCamera->m_nVirtualPTZCamState |= VirtualPTZCameraState_SearchingSourceVideoChannel;
            pVirtualPTZCamera->m_pVCSrc = NULL;
         }
         pVideoChannel->Activate();
         pVirtualPTZCamera->m_nVirtualPTZCamState &= ~VirtualPTZCameraState_ReactivatingForResetingSourceVideoChannel;
      }
   }
}

void CVideoChannel::ReleaseLinkedPTZControlPointers()
{
   int i;

   // 연관 PTZ채널 포인터 해제
   if( m_pPTZCtrl ) {
      int nVCNum = m_pVCM->GetVideoChannelNum();
      for( i = 0; i < nVCNum; i++ ) {
         CVideoChannel* pVC = m_pVCM->GetVideoChannelByIdx( i );
         if( pVC && pVC != this && pVC->m_pPTZCtrl && pVC->m_pPTZCtrl->m_pLinkingPTZControl ) {
            pVC->m_pPTZCtrl->m_csLinkingPTZControl.lock();
            if( m_pPTZCtrl == pVC->m_pPTZCtrl->m_pLinkingPTZControl ) {
               pVC->m_pPTZCtrl->m_pLinkingPTZControl = NULL;
            }
            pVC->m_pPTZCtrl->m_csLinkingPTZControl.unlock();
         }
      }
   }
}

void CVideoChannel::RemoveVirtualPTZCamerasOnDestroy()
{
   if( !g_bSignalSourceCameraToVirtualPTZCamera ) return;
   if( !m_pVCM ) return;

   int i;

   // 가상 PTZ 채널이 본 채널을 참조하고 있는 경우 가상 PTZ 채널을 Deactivate 시킨다.
   if( !m_pVCM ) return;
   CCriticalSectionSP so1( m_pVCM->m_csVideoChannelList );
   CCriticalSectionSP so2( m_csVideoChannel );
   CCriticalSectionSP so3( m_csVirtualPTZCameras );
   std::deque<CVideoChannel*> virtual_ptz_cameras = m_VirtualPTZCameras;
   int nVirtualPTZCameraNum                       = (int)virtual_ptz_cameras.size();
   for( i = 0; i < nVirtualPTZCameraNum; i++ ) {
      CVideoChannel* pVideoChannel = virtual_ptz_cameras[i];
      if( VideoChannelType_VirtualPTZ == pVideoChannel->m_nVideoChannelType ) {
         CCamera_VirtualPTZ* pVirtualPTZCamera = (CCamera_VirtualPTZ*)pVideoChannel->m_Cameras[0];
         pVirtualPTZCamera->m_nVirtualPTZCamState |= VirtualPTZCameraState_ReactivatingForResetingSourceVideoChannel;
         pVideoChannel->Deactivate();
         if( pVideoChannel->m_Cameras.size() ) {
            pVirtualPTZCamera->m_pVCSrc = NULL;
            pVirtualPTZCamera->m_nVirtualPTZCamState |= VirtualPTZCameraState_SourceVideoChannelRemoved;
         }
         pVideoChannel->Activate();
         pVirtualPTZCamera->m_nVirtualPTZCamState &= ~VirtualPTZCameraState_ReactivatingForResetingSourceVideoChannel;
      }
   }
}
#endif // __SUPPORT_VIRTUAL_PTZ_CAMERA
#endif //__SUPPORT_PTZ_CAMERA

void CVideoChannel::UpdateCameraFlag()
{
   int i;

   ushort nAllCamFlag = 0;
   if( VideoChannelType_IPCamera == m_nVideoChannelType ) {
      int i = 0;
      for( i = 0; i < MAX_IP_CAMERA_STREAM_NUM; i++ ) {
         if( m_IPCamInfoArr[i].m_bUseStream ) {
            ushort nCurCamFlag = 1 << i;
            nAllCamFlag |= nCurCamFlag;
         }
      }
   } else {
      for( i = 0; i < (int)m_Cameras.size(); i++ ) {
         ushort nCurCamFlag = 1 << i;
         nAllCamFlag |= nCurCamFlag;
      }
   }

   m_nVideoSignalFlag           = m_nVideoSignalFlag & m_nCameraFlag;
   m_nNoVideoSignalFlag         = m_nNoVideoSignalFlag & m_nCameraFlag;
   m_nConnectingChFlag          = m_nConnectingChFlag & m_nCameraFlag;
   m_nAuthorizationFailedChFlag = m_nAuthorizationFailedChFlag & m_nCameraFlag;
}

#ifdef __SUPPORT_PTZ_CAMERA
#ifdef __SUPPORT_VIRTUAL_PTZ_CAMERA

void CVideoChannel::SignalCaptureEventToVerturalPTZCameras()
{
   int i;
   // 본 비디오채널이 가상 PTZ 카메라의 소스채널이면 가상 PTZ채널들에게 캡쳐이벤트 신호를 보낸다.
   if( g_bSignalSourceCameraToVirtualPTZCamera ) {
      int nVirtualPTZCameraNum = (int)m_VirtualPTZCameras.size();
      if( nVirtualPTZCameraNum ) { // <- jun : 꼭, 반드시 체크...
         CCriticalSectionSP so3( m_csVirtualPTZCameras );
         std::deque<CVideoChannel*> virtual_ptz_cameras = m_VirtualPTZCameras;
         nVirtualPTZCameraNum                           = (int)virtual_ptz_cameras.size();
         for( i = 0; i < nVirtualPTZCameraNum; i++ ) {
            CVideoChannel* pVideoChannel = virtual_ptz_cameras[i];
            if( VideoChannelType_VirtualPTZ == pVideoChannel->m_nVideoChannelType ) {
               CCamera_VirtualPTZ* pVirtualPTZCamera = (CCamera_VirtualPTZ*)pVideoChannel->m_Cameras[0];
               pVirtualPTZCamera->m_evtSourceImageCapture.SetEvent();
            }
         }
      }
   }
}

void CVideoChannel::UpdateSourceStreamIdxsForVirtualPTZCamers()
{
   // Video Channel Manager에서 호출하는 함수이다.
   int i;

   if( VideoChannelType_IPCamera != m_nVideoChannelType ) return;

   // 본 비디오채널이 가상 PTZ 카메라의 소스채널이면 가상 PTZ채널들에게 캡쳐이벤트 신호를 보낸다.
   if( g_bSignalSourceCameraToVirtualPTZCamera ) {
      int nVirtualPTZCameraNum = (int)m_VirtualPTZCameras.size();
      if( nVirtualPTZCameraNum ) { // <- jun : 꼭, 반드시 체크...
         BOOL bSignalToVertualPTZCameras[MAX_IP_CAMERA_STREAM_NUM];
         ZeroMemory( &bSignalToVertualPTZCameras, sizeof( bSignalToVertualPTZCameras ) );
         {
            CCriticalSectionSP so3( m_csVirtualPTZCameras );
            std::deque<CVideoChannel*> virtual_ptz_cameras = m_VirtualPTZCameras;
            nVirtualPTZCameraNum                           = (int)virtual_ptz_cameras.size();
            for( i = 0; i < nVirtualPTZCameraNum; i++ ) {
               CVideoChannel* pVideoChannel = virtual_ptz_cameras[i];
               if( VideoChannelType_VirtualPTZ == pVideoChannel->m_nVideoChannelType ) {
                  CCamera_VirtualPTZ* pVirtualPTZCamera = (CCamera_VirtualPTZ*)pVideoChannel->m_Cameras[0];
                  int nStreamIdx                        = pVirtualPTZCamera->m_pInfo->m_nStreamIdx;
                  if( nStreamIdx >= 0 ) {
                     bSignalToVertualPTZCameras[nStreamIdx] = TRUE;
                  }
               }
            }
         }
         for( i = 0; i < MAX_IP_CAMERA_STREAM_NUM; i++ ) {
            if( bSignalToVertualPTZCameras[i] != m_bSignalToVertualPTZCameras[i] )
               m_bSignalToVertualPTZCameras[i] = bSignalToVertualPTZCameras[i];
         }
      }
   }
}

#endif // __SUPPORT_VIRTUAL_PTZ_CAMERA
#endif // __SUPPORT_PTZ_CAMERA

void CVideoChannel::DayNightDetectProc()
{
// MS: 멀티스트리밍을 지원하지 않는 경우 캡쳐스레드에서 동작되도록 한다.
//     소스영상을 사용할 필요가 없는 경우엔는 채널관리자 스레드에서 처리하는 것을 권장한다.
#ifdef __SUPPORT_DAYNIGHT_DETECTION
   m_DayNightDetector.Perform();
#endif

#ifdef __SUPPORT_PTZ_CAMERA
#ifdef __SUPPORT_DAYNIGHT_DETECTION
   if( m_pPTZCtrl ) {
      if( m_DayNightDetector.m_nState & DayNightDetectionState_DayModeJustChanded )
         m_pPTZCtrl->m_nState |= PTZ_State_SignalNightModeOff;
      if( m_DayNightDetector.m_nState & DayNightDetectionState_NightModeJustChanded )
         m_pPTZCtrl->m_nState |= PTZ_State_SignalNightModeOn;
      if( m_DayNightDetector.m_nState & DayNightDetectionState_NightMode )
         m_pPTZCtrl->m_nState |= PTZ_State_NightMode;
      else
         m_pPTZCtrl->m_nState &= ~PTZ_State_NightMode;
   }
#endif
#endif
}

#ifdef __SUPPORT_PTZ_CAMERA
void CVideoChannel::CopyPTZInfoToPTZCtrl()
{
   // PTZ 설정을 PTZ제어 객체로 복사를 한다.
   // 비디오 채널 시작시에 설정이 복사된 이후에는 설정이 복사되지 않기 때문에
   // 아래의 코드를 주기적으로 실행하여 실시간으로 동기화를 시켜주어야 한다.
   if( m_pPTZCtrl ) {
      if( m_pPTZCtrl->m_nState & PTZ_State_InitHWOK ) {
         m_PTZInfo.CopyOnlyCommonValues( &m_pPTZCtrl->m_PTZInfo );
      }
   }
}
#endif

int CVideoChannel::ActivateThread( int nThreadFlags )
{
   CCriticalSectionSP co( m_csVideoChannel );
   if( !( m_nState & VCState_Activate ) )
      return ( DONE );

   if( !m_bRemote ) {
      m_csVCThread.Lock();
      if( nThreadFlags & VCThreadType_Cap ) {
         if( !m_VCThread[VC_Cap].joinable() ) {
            if( !m_pTimer[VC_Cap] )
               m_pTimer[VC_Cap] = m_pTimerMgr->GetNewTimer( "VideoChannel_VC_Cap" );
            m_VCThread[VC_Cap] = std::thread( &CVideoChannel::Thread_Capture, this );
         }
      }
      if( nThreadFlags & VCThreadType_IP ) {
         if( !m_VCThread[VC_IP].joinable() ) {
            if( !m_pTimer[VC_IP] )
               m_pTimer[VC_IP] = m_pTimerMgr->GetNewTimer( "VideoChannel_VC_IP" );
            m_VCThread[VC_IP] = std::thread( &CVideoChannel::Thread_ImgProcess, this );
         }
      }
      if( nThreadFlags & VCThreadType_Rec ) {
         if( !m_VCThread[VC_Rec].joinable() ) {
            if( !m_pTimer[VC_Rec] )
               m_pTimer[VC_Rec] = m_pTimerMgr->GetNewTimer( "VideoChannel_VC_Rec" );
            m_VCThread[VC_Rec] = std::thread( &CVideoChannel::Thread_Record, this );
         }
      }
      if( nThreadFlags & VCThreadType_Disp ) {
         if( !m_VCThread[VC_Disp].joinable() ) {
            if( !m_pTimer[VC_Disp] )
               m_pTimer[VC_Disp] = m_pTimerMgr->GetNewTimer( "VideoChannel_VC_Disp" );
            m_VCThread[VC_Disp] = std::thread( &CVideoChannel::Thread_Display, this );
         }
      }
      m_csVCThread.Unlock();
   }
   return ( DONE );
}

int CVideoChannel::DeactivateThread( int nThreadFlags )
{
   CCriticalSectionSP co( m_csVideoChannel );

   if( !( m_nState & VCState_Activate ) )
      return ( DONE );

   for( int i = 0; i < m_nThreadNum; i++ ) {
      int nCurThreadFlag = 1 << i;
      if( nCurThreadFlag & nThreadFlags ) {
         if( m_pTimerMgr ) {
            if( m_pTimer[i] )
               m_pTimer[i]->GetTimerEvent().SetEvent();
         }
         m_bStopThead[i] = TRUE;
         if( m_VCThread[i].joinable() ) {
            m_VCThread[i].join();
         }
         m_fCurFrmRate[i] = 0.0f;
      }
   }

   for( int i = 0; i < m_nThreadNum; i++ ) {
      int nCurThreadFlag = 1 << i;
      if( nCurThreadFlag & nThreadFlags ) {
         m_bStopThead[i] = FALSE;
         if( m_pTimerMgr ) {
            if( m_pTimer[i] ) {
               m_pTimerMgr->DeleteTimer( m_pTimer[i]->m_nTimerID );
               m_pTimer[i] = NULL;
            }
         }
      }
   }

   return ( DONE );
}

ICamera* CVideoChannel::GetICamera( int nChIdx )
{
   CCriticalSectionSP co( m_csCameras );
   if( nChIdx < (int)m_Cameras.size() ) {
      return m_Cameras[nChIdx];
   }
   return 0;
}

CCamera_IP* CVideoChannel::GetIPCameraByStreamIdx( int nStreamIndex )
{
   if( VideoChannelType_IPCamera != m_nVideoChannelType )
      return NULL;
   int i;
   for( i = 0; i < m_nCameraNum; i++ ) {
      CCamera_IP* pCamera = (CCamera_IP*)m_Cameras[i];
      if( pCamera ) {
         if( nStreamIndex == pCamera->m_nICameraIdx ) {
            return pCamera;
         }
      }
   }
   return NULL;
}

int CVideoChannel::GetCamState( int nChIdx )
{
   CCriticalSectionSP co( m_csCameras );
   if( nChIdx < (int)m_Cameras.size() ) {
      return m_Cameras[nChIdx]->GetState();
   }
   return 0;
}

float CVideoChannel::GetFrameRate( int nVCProcCallbackFuncType )
{
   switch( nVCProcCallbackFuncType ) {
   case VCProcCallbackFuncType_Display:
      return m_fFPS_Cap;
   case VCProcCallbackFuncType_Recoding:
      return m_fFPS_Rec;
   case VCProcCallbackFuncType_ImgProc:
      return m_fFPS_IP;
   }
   return 0.0f;
}

uint CVideoChannel::GetPlayTime()
{
   uint nPlayTime = 0;
#ifdef __SUPPORT_VIDEO_FILE_CAMERA
   if( VideoChannelType_OffLineAnalyze == m_nVideoChannelType ) {
      CCriticalSectionSP co( m_csCameras );
      if( m_Cameras.size() ) {
         CCamera_File* pCameraFile = (CCamera_File*)m_Cameras[0];
         nPlayTime                 = pCameraFile->m_nPlayTime;
      }
   }
#endif
   return nPlayTime;
}

float CVideoChannel::GetRealTimeFrameRate( int nVCProcCallbackFuncType )
{
   switch( nVCProcCallbackFuncType ) {
   case VCProcCallbackFuncType_Display:
      return m_fCurFrmRate[VC_Cap];
   case VCProcCallbackFuncType_Recoding:
      return m_fCurFrmRate[VC_Disp];
   case VCProcCallbackFuncType_ImgProc:
      return m_fCurFrmRate[VC_IP];
   }
   return 0.0f;
}

void CVideoChannel::SetFrameRate( int nVCProcCallbackFuncType, float fFrameRate )
{
   switch( nVCProcCallbackFuncType ) {
   case VCProcCallbackFuncType_Display:
      m_fFPS_Cap  = fFrameRate;
      m_fFPS_Disp = fFrameRate;
      break;
   case VCProcCallbackFuncType_Recoding:
      m_fFPS_Rec = fFrameRate;
      break;
   case VCProcCallbackFuncType_ImgProc:
      m_fFPS_IP = fFrameRate;
      LOGI <<"m_fFPS_IP : "<< fFrameRate;
      break;
   }
}

float CVideoChannel::GetReceiveFrameRate()
{
   float fReceiveFrameRate = 0.0f;
   if( !( m_nState & VCState_Activate ) || ( m_nState & ( VCState_SignalDeactivate | VCState_Deactivating ) ) ) return m_fFPS_Cap;
   if( VideoChannelType_IPCamera == m_nVideoChannelType ) {
      m_csCameras.Lock();
      CCamera_IP* pIPCamera = (CCamera_IP*)m_Cameras[0];
      fReceiveFrameRate     = pIPCamera->m_fFrameRate;
      m_csCameras.Unlock();
   }
   return fReceiveFrameRate;
}

float CVideoChannel::GetDisplayBitrate()
{
   float fBitrate = 0.0f;
   if( !( m_nState & VCState_Activate ) || ( m_nState & ( VCState_SignalDeactivate | VCState_Deactivating ) ) ) return fBitrate;
   if( VideoChannelType_IPCamera == m_nVideoChannelType ) {
      m_csCameras.Lock();
      CCamera_IP* pIPCamera = (CCamera_IP*)m_Cameras[0];
      int nStreamIndex      = m_nDecodeStreamIndexOnProcCallbackFunc[VCProcCallbackFuncType_Display];
      pIPCamera             = GetIPCameraByStreamIdx( nStreamIndex );
      fBitrate              = pIPCamera->m_fBitrate;
      m_csCameras.Unlock();
   }
   return fBitrate;
}

void CVideoChannel::GetImage_Lock( byte** ppYUY2Buff )
{
   //m_csSrcImage.lock (   );
   if( ppYUY2Buff )
      *ppYUY2Buff = m_pYUY2Buff;
}

void CVideoChannel::GetImage_Unlock()
{
   //m_csSrcImage.unlock(   );
}

void CVideoChannel::GetImage( byte** pYUY2Buff )
{
   if( pYUY2Buff )
      *pYUY2Buff = m_pYUY2Buff;
}

void CVideoChannel::GetImage_Timestamp( FILETIME& ftTimestamp )
{
   ftTimestamp = m_ftTimestamp;
}

ushort CVideoChannel::GetVideoSignalFlag()
{
   return m_nVideoSignalFlag;
}

ushort CVideoChannel::GetNoVideoSignalFlag()
{
   return m_nNoVideoSignalFlag;
}

ushort CVideoChannel::GetConnectingChannelFlag()
{
   return m_nConnectingChFlag;
}

ushort CVideoChannel::GetConnectionFailedChannelFlag()
{
   return m_nConnectionFailedChFlag;
}

ushort CVideoChannel::GetAuthorizationFailedChannelFlag()
{
   return m_nAuthorizationFailedChFlag;
}

BOOL CVideoChannel::IsAllSignalOK()
{
   return m_bAllSignalOK;
}

BOOL CVideoChannel::IsAutoFocusingCamera()
{
   if( m_nVideoChannelType == VideoChannelType_AnalogCamera ) {
      PTZControlProperty* ptz_property = GetPTZControlProperty( m_PTZInfo.m_nPTZControlType );
      if( ptz_property ) {
         BOOL bSupport = ( ptz_property->m_nCapabilities & PTZCtrlCap_AutoFocusingByPTZCtrl ) ? TRUE : FALSE;
         return bSupport;
      }
   }
   return FALSE;
}

float CVideoChannel::GetCaptureFrameRate()
{
   if( m_nVideoChannelType == VideoChannelType_IPCamera ) {
      return m_IPCamInfo.m_fFPS_Cap;
   }
   if( m_nVideoChannelType == VideoChannelType_Panorama ) {
      return m_PanoramaCamInfo.m_fFrameRate;
   }
   if( m_nVideoChannelType == VideoChannelType_WebCam ) {
      return m_WebCamInfo.m_fFrameRate;
   }
   if( m_nVideoChannelType == VideoChannelType_VideoFile ) {
      return m_FileCamInfo.m_fFrameRate;
   }
#ifdef __SUPPORT_VIRTUAL_PTZ_CAMERA
   if( m_nVideoChannelType == VideoChannelType_VirtualPTZ ) {
      return m_fFPS_Cap;
   }
#endif

   return 0.0f;
}

#ifdef __SUPPORT_PTZ_CAMERA
CPTZCtrl* CVideoChannel::GetPTZControl()
{
   return m_pPTZCtrl;
}

int CVideoChannel::GetPTZControlTypeOfPTZCmd()
{
   int ptz_control_type = 0;
   if( m_nVideoChannelType == VideoChannelType_AnalogCamera ) {
      ptz_control_type = m_PTZInfo.m_nPTZControlType;
   } else if( m_nVideoChannelType == VideoChannelType_IPCamera ) {
      if( PTZCtrlConnType_TCPServer == m_IPCamInfo.m_PTZInfo.m_nConnectType ) {
         ptz_control_type = m_IPCamInfo.m_PTZInfo.m_nPTZControlType;
      } else if( PTZCtrlConnType_LinkToPTZCtrl == m_IPCamInfo.m_PTZInfo.m_nConnectType ) {
         ptz_control_type      = PTZControlType_LinkingToOtherPTZCtrl;
         CVideoChannel* pVCSrc = m_pVCM->GetVideoChannel( m_IPCamInfo.m_PTZInfo.m_nVCID_LikingPTZCtrl );
         if( pVCSrc ) {
            ptz_control_type = pVCSrc->GetPTZControlTypeOfPTZCmd();
         }
      } else if( PTZCtrlConnType_Basic == m_IPCamInfo.m_PTZInfo.m_nConnectType ) {
         IPCameraProperty* pIPCamProperty = GetIPCameraProperty( m_IPCamInfo.m_nIPCameraType );
         if( pIPCamProperty && ( pIPCamProperty->m_nCapabilities & IPCamCaps_PTZControl ) ) {
            ptz_control_type = m_IPCamInfo.m_nPTZControlType;
            if( pIPCamProperty->m_nCapabilities & IPCamCaps_SupportSameSubModel ) {
               ptz_control_type = m_IPCamInfo.m_PTZInfo.m_nPTZControlType;
            }
         }
      } else if( PTZCtrlConnType_OtherIPCamera == m_IPCamInfo.m_PTZInfo.m_nConnectType ) {
         IPCameraProperty* pIPCamProperty = GetIPCameraProperty( m_IPCamInfo.m_nIPCameraTypeForExtraPTZCtrl );
         if( pIPCamProperty && ( pIPCamProperty->m_nCapabilities & IPCamCaps_PTZControl ) ) {
            ptz_control_type = m_IPCamInfo.m_nPTZControlType;
            if( pIPCamProperty->m_nCapabilities & IPCamCaps_SupportSameSubModel ) {
               ptz_control_type = m_IPCamInfo.m_PTZInfo.m_nPTZControlType;
            }
         }
      }
      return ptz_control_type;
   }
   return ( 0 );
}

#endif

void CVideoChannel::SetSetupTemp( BOOL bSetupTemp )
{
   m_bSetupTemp = bSetupTemp;
}

BOOL CVideoChannel::Read( CXMLIO* pIO, BOOL bNetwork )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "VideoChannel" ) ) {
      CVideoChannelInfo::Read( pIO, bNetwork );
      CVideoChannelInfo& info = GetVideoChannelInfo();
      info.CheckParameters();
      xmlNode.End();
      return TRUE;
   }
   return FALSE;
}

BOOL CVideoChannel::Write( CXMLIO* pIO, BOOL bNetwork )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "VideoChannel" ) ) {
      CVideoChannelInfo& info = GetVideoChannelInfo();
      info.CheckParameters();
      // 주의 : 설정데이터를 Write할 때 SetVideoSize 함수를 호출해서는 안된다.
      //        비디오 채널이 Activate된 후 설정을 저장하기위하여 Write함수를 호출하는 경우
      //        비디오크기가 변경되어 오류를 발생시킨다.
      //info.SetVideoSize (m_pAnalogCamMgr->GetInfo ());
      CVideoChannelInfo::Write( pIO, bNetwork );
      xmlNode.End();
      return TRUE;
   }
   return FALSE;
}

int CVideoChannel::CreateCameras()
{
   m_nState &= ~( VCState_IncorecctPanoramSourceImageSize );
   m_nConnectionFailedChFlag = 0;
   m_nCameraFlag             = 0x0001;

   if( VideoChannelType_AnalogCamera == m_nVideoChannelType ) {
   } else if( VideoChannelType_IPCamera == m_nVideoChannelType ) {
      for( int i = 0; i < MAX_IP_CAMERA_STREAM_NUM; i++ ) {
         CCamera_IP* pIPCamera     = NULL;
         CIPCameraInfo* pIPCamInfo = &m_IPCamInfoArr[i];
         if( pIPCamInfo->m_bUseStream ) {
            pIPCamInfo->m_pTimerMgr = m_pTimerMgr;
            m_pIPCamMgr->CreateCamera( pIPCamInfo, &pIPCamera );
            pIPCamera->m_pCBCamEventParam = this;
            pIPCamera->m_pCBCamEvent      = CVideoChannel::CallbackCameraEvent;
            pIPCamera->m_nICameraIdx      = i;
            pIPCamera->m_nChannelNo_IVX   = m_nChannelNo_IVX;
            pIPCamera->m_szCameraName     = m_szCameraName;
            pIPCamera->m_pCBRTSPMetadata  = m_pCBRTSPMetadata;
            if( pIPCamera ) {
               pIPCamInfo->m_pTimerMgr = m_pTimerMgr;
               if( pIPCamera->Initialize( pIPCamInfo ) != DONE ) {
                  LOGE << "pIPCamera->Initialize failed.";
                  m_pIPCamMgr->DeleteCamera( pIPCamera->GetCameraID() );
                  return ( 1 );
               }
               if( pIPCamInfo->m_nIPCamStreamIdx == 0 ) {
                  m_nWidth   = pIPCamInfo->m_nWidth;
                  m_nHeight  = pIPCamInfo->m_nHeight;
                  m_fFPS_Cap = pIPCamInfo->m_fFPS_Cap;
               }
               pIPCamera->m_pcbDecodeFrame        = CVideoChannel::CallbackReceiveDecodeFrame;
               pIPCamera->m_pcbReceiveEncodeFrame = CVideoChannel::CallbackReceiveEncodeFrame;
               pIPCamera->m_pVideoChannel         = this;
               pIPCamera->m_pCBDecodeFrameParam   = this;
               m_csCameras.Lock();
               m_Cameras.push_back( pIPCamera );
               m_csCameras.Unlock();
               pIPCamera->m_pVideoChannel = this;
               m_pVCM->RegisterICamera( pIPCamera );
               if( pIPCamInfo->m_bUseStream ) {
                  ushort nCamFlag = 1 << i;
                  m_nCameraFlag |= nCamFlag;
               }
            }
         }
      }
   }
#ifdef __SUPPORT_VIDEO_FILE_CAMERA
   else if( VideoChannelType_VideoFile == m_nVideoChannelType ) {
      CCamera_File* pFileCamera = new CCamera_File( CCamera_File::SINGLE_STREAMING );
      int nRet                  = pFileCamera->Initialize( &m_FileCamInfo );
      if( nRet > 0 ) {
         m_nState |= VCState_FailureOpenVideoFile;
         delete pFileCamera;
         return ( 1 );
      }
      if( nRet == DONE ) {
         m_nWidth                             = pFileCamera->m_pInfo->m_nWidth;
         m_nHeight                            = pFileCamera->m_pInfo->m_nHeight;
         m_fFPS_Cap                           = pFileCamera->m_pInfo->m_fFrameRate;
         pFileCamera->m_pcbReceiveEncodeFrame = CVideoChannel::CallbackReceiveEncodeFrame;
         pFileCamera->m_pVideoChannel         = this;
         m_csCameras.lock();
         m_Cameras.push_back( pFileCamera );
         m_csCameras.unlock();
         pFileCamera->m_pVideoChannel = this;
         m_pVCM->RegisterICamera( pFileCamera );
      }
   }
#endif
#ifdef __SUPPORT_VIRTUAL_PTZ_CAMERA
   else if( VideoChannelType_VirtualPTZ == m_nVideoChannelType ) {
      if( IS_NOT_SUPPORT( __SUPPORT_VIRTUAL_PTZ_CAMERA ) )
         return ( 1 );

      CCamera_VirtualPTZ* pVirtualPTZCamera = new CCamera_VirtualPTZ;
      int nRet                              = pVirtualPTZCamera->Initialize( &m_VirtualPTZCamInfo, this );
      if( nRet > 0 ) {
         delete pVirtualPTZCamera;
         return ( 1 );
      }
      if( nRet == DONE ) {
         m_nWidth     = pVirtualPTZCamera->m_pInfo->m_nWidth;
         m_nHeight    = pVirtualPTZCamera->m_pInfo->m_nHeight;
         m_fFPS_Cap   = pVirtualPTZCamera->m_pInfo->m_fFrameRate;
         m_bPTZCamera = TRUE;
         m_csCameras.lock();
         m_Cameras.push_back( pVirtualPTZCamera );
         m_csCameras.unlock();
         pVirtualPTZCamera->m_pVideoChannel = this;
         m_pVCM->RegisterICamera( pVirtualPTZCamera );
      }
   }
#endif // __SUPPORT_VIRTUAL_PTZ_CAMERA
#ifdef __SUPPORT_PANORAMIC_CAMERA
   else if( VideoChannelType_Panorama == m_nVideoChannelType ) {
      if( IS_NOT_SUPPORT( __SUPPORT_PANORAMIC_CAMERA ) )
         return ( 1 );

      if( LoadPVTFiles() != ( DONE ) )
         return ( 1 );
      // 주의 : 파노라마카메라정보(m_PanoramaCamInfo)에서 영상의 높이와 넓이 값을 반드시 설정해야함.
      m_PanoramaCamInfo.m_nWidth       = PVFCompositor.Width;
      m_PanoramaCamInfo.m_nHeight      = PVFCompositor.Height;
      m_PanoramaCamInfo.m_nInputCamNum = PVFCompositor.NumSrcImages;
      m_nWidth                         = m_PanoramaCamInfo.m_nWidth;
      m_nHeight                        = m_PanoramaCamInfo.m_nHeight;
      CapImgSize.Width                 = PVFCompositor.SrcImgWidth;
      CapImgSize.Height                = PVFCompositor.SrcImgHeight;

      CVideoChannelInfo& info = GetVideoChannelInfo();
      m_fFPS_Cap              = m_PanoramaCamInfo.m_fFrameRate;

      int nCameraNum = std::min( m_PanoramaCamInfo.m_nInputCamInfoNum, m_PanoramaCamInfo.m_nInputCamNum );

      int nCaptureImgSize = GetYUY2ImageLength( CapImgSize.Width, CapImgSize.Height );
      CapturedImages.Create( nCaptureImgSize, nCameraNum );
      for( i = 0; i < nCameraNum; i++ )
         BlackOut( CapturedImages[i], nCaptureImgSize, 0x00 );

      int nYUY2BuffLen = GetYUY2ImageLength( m_nWidth, m_nHeight );
      if( nYUY2BuffLen != m_YUY2Buff.Length )
         m_YUY2Buff.Create( nYUY2BuffLen );

      for( i = 0; i < nCameraNum; i++ ) {
         ICameraInfo* pCamInfo = m_PanoramaCamInfo.m_InputCamInfos[i];
         // 캡춰보드 카메라 입력인 경우
         if( pCamInfo->m_nCameraType == CameraType_AnalogCamera ) {
         }
         // IP 카메라 입력인 경우
         else if( pCamInfo->m_nCameraType == CameraType_IPCamera ) {
            CIPCameraInfo* pIPCamInfo = (CIPCameraInfo*)pCamInfo;
            CCamera_IP* pIPCamera     = NULL;
            std::string strText;
            GetIPAddrString( strText, pIPCamInfo->m_strIPAddress ); // (xinu_bc23)
            pIPCamInfo->m_pTimerMgr = m_pTimerMgr;
            m_pIPCamMgr->CreateCamera( pIPCamInfo, &pIPCamera );
            if( pIPCamera ) {
               pIPCamera->m_nICameraIdx = i;
               pIPCamera->Initialize( pIPCamInfo );
               pIPCamera->m_pVideoChannel = this;
            }

            m_csCameras.lock();
            m_Cameras.push_back( pIPCamera );
            m_csCameras.unlock();
            pIPCamera->m_pVideoChannel = this;
            m_pVCM->RegisterICamera( pIPCamera );
         }
         ushort nCamFlag = 1 << i;
         m_nCameraFlag |= nCamFlag;
      }
      IBCorrector.Initialize( PVFCompositor );
      if( m_PanoramaCamInfo.m_nBCBaseChNo >= m_PanoramaCamInfo.m_nInputCamNum )
         m_PanoramaCamInfo.m_nBCBaseChNo = 0;
   }
#endif

   m_fFPS_Disp            = m_fFPS_Cap;
   m_fCurFrmRate[VC_Cap]  = m_fFPS_Cap;
   m_fCurFrmRate[VC_Disp] = m_fFPS_Disp;
   m_fCurFrmRate[VC_Rec]  = m_fFPS_Rec;
   m_fCurFrmRate[VC_IP]   = m_fFPS_IP;

   logi( "Cap(Decoding):%3.1f ImageProcessing:%3.1f", m_fFPS_Cap,  m_fFPS_IP );
   m_csCameras.Lock();
   m_nCameraNum = (int)m_Cameras.size();
   // 카메라 공통 속성들을 초기화 하기.
   for( int i = 0; i < m_nCameraNum; i++ ) {
      if( m_Cameras[i] ) {
         m_Cameras[i]->m_pCBCamEvent      = CVideoChannel::CallbackCameraEvent;
         m_Cameras[i]->m_pCBCamEventParam = this;         
      }
   }
   m_csCameras.Unlock();

   return ( DONE );
}

void CVideoChannel::DeleteCameras()
{
   int i;

   m_csCameras.Lock();
   for( i = 0; i < m_nCameraNum; i++ ) {
      ICamera* pCamera = m_Cameras[i];
      m_pVCM->UnregisterICamera( pCamera );
      if( pCamera ) {
         int nCameraType = pCamera->GetCameraType();
         switch( nCameraType ) {
         case CameraType_IPCamera:
            // IP 카메라 관리자에 등록되어있는 카메라를 삭제한다.
            pCamera->Uninitialize();
            m_pIPCamMgr->DeleteCamera( pCamera->GetCameraID() );
            break;
         case CameraType_VideoFile:
         case CameraType_VirtualPTZ:
            delete pCamera;
            break;
         default:
            break;
         }
      }
   }
   m_Cameras.clear();
   m_nCameraNum = (int)m_Cameras.size();
   m_csCameras.Unlock();
}

#ifdef __SUPPORT_PTZ_CAMERA
void CVideoChannel::SetPTZControlOpenMode( int nPTZControlOpenMode )
{
   m_nPTZControlOpenMode = nPTZControlOpenMode;
}

int CVideoChannel::CreatePTZControl( int nPTZControlOpenMode )
{
   m_nState &= ~VCState_FailureOpenSerialPort;

   // 접속방식 (Serial, EzTCP, IPCamera)에 따라 생성할 PTZ 종류를 결정한다.
   int ptz_control_type = 0;
   ptz_control_type     = m_PTZInfo.m_nPTZControlType;

   if( m_nVideoChannelType == VideoChannelType_IPCamera
       || m_nVideoChannelType == VideoChannelType_Hi3516A_Cellinx ) {
      if( IPCameraType_IVXVideoServer == m_IPCamInfo.m_nIPCameraType ) {
         nPTZControlOpenMode = PTZCtrlOpenMode_CreateOnly;
      }

      if( PTZCtrlConnType_TCPServer == m_IPCamInfo.m_PTZInfo.m_nConnectType ) {
         ptz_control_type = m_IPCamInfo.m_PTZInfo.m_nPTZControlType;
      } else if( PTZCtrlConnType_LinkToPTZCtrl == m_IPCamInfo.m_PTZInfo.m_nConnectType ) {
         ptz_control_type = PTZControlType_LinkingToOtherPTZCtrl;
      } else if( PTZCtrlConnType_Basic == m_IPCamInfo.m_PTZInfo.m_nConnectType ) {
         IPCameraProperty* pIPCamProperty = GetIPCameraProperty( m_IPCamInfo.m_nIPCameraType );
         if( pIPCamProperty && ( pIPCamProperty->m_nCapabilities & IPCamCaps_PTZControl ) ) {
            if( pIPCamProperty->m_nCapabilities & IPCamCaps_SupportSameSubModel ) {
               m_IPCamInfo.m_nPTZControlType = m_IPCamInfo.m_PTZInfo.m_nPTZControlType;
            }
            ptz_control_type = m_IPCamInfo.m_nPTZControlType;
         }
      } else if( PTZCtrlConnType_OtherIPCamera == m_IPCamInfo.m_PTZInfo.m_nConnectType ) {
         IPCameraProperty* pIPCamProperty = GetIPCameraProperty( m_IPCamInfo.m_nIPCameraTypeForExtraPTZCtrl );
         if( pIPCamProperty && ( pIPCamProperty->m_nCapabilities & IPCamCaps_PTZControl ) ) {
            if( pIPCamProperty->m_nCapabilities & IPCamCaps_SupportSameSubModel ) {
               m_IPCamInfo.m_nPTZControlType = m_IPCamInfo.m_PTZInfo.m_nPTZControlType;
            }
            ptz_control_type = pIPCamProperty->m_nPTZControlType;
         }
      }
   }
#ifdef __SUPPORT_VIRTUAL_PTZ_CAMERA
   else if( VideoChannelType_VirtualPTZ == m_nVideoChannelType ) {
      ptz_control_type = PTZControlType_VirtualPTZ;
   }
#endif

#ifdef __SUPPORT_PTZ_CAMERA
   m_pPTZCtrl = ::CreatePTZControl( ptz_control_type, ISize2D( m_nWidth, m_nHeight ) );
   if( m_pPTZCtrl != NULL ) {
      m_pPTZCtrl->m_nChannelNo_IVX = m_nChannelNo_IVX;
      m_pPTZCtrl->SetOpenMode( nPTZControlOpenMode );
      m_pPTZCtrl->m_pTimerMgr = m_pTimerMgr;
      if( m_nVideoChannelType == VideoChannelType_IPCamera
          || m_nVideoChannelType == VideoChannelType_Hi3516A_Cellinx ) {
         CCamera_IP* pIPCamera = NULL;
         if( m_nVideoChannelType == VideoChannelType_IPCamera ) {
            if( m_Cameras.size() ) {
               pIPCamera = (CCamera_IP*)m_Cameras[0];
               pIPCamera->SetPTZControl( m_pPTZCtrl );
            }
            m_pPTZCtrl->SetVideoSize( ISize2D( m_nWidth, m_nHeight ) );
            m_pPTZCtrl->SetAbsPos( m_IPCamInfo.m_PTZInfo.m_pvCurPos );
            // 상위 PTZ정보에서 IP PTZ정보로 전달해야 할 변수들.
            m_IPCamInfo.m_PTZInfo.m_IRLightCtrlInfo = m_PTZInfo.m_IRLightCtrlInfo;
            m_IPCamInfo.m_PTZInfo.CopyOnlyCommonValues( &m_PTZInfo );
            // IP Camera 이고 EZTCP 장비를 사용하는 경우에는 아날로그 방식으로 오픈한다.
            // 아날로그 방식으로 오픈하지만 ezTCP 장비와 통신하기 위한 코드가 수행된다.
            // 이 부분은 나중에 명확한 코드로 수정이 필요하다.
            if( PTZCtrlConnType_TCPServer == m_IPCamInfo.m_PTZInfo.m_nConnectType ) {
               m_pPTZCtrl->m_pIPCamInfo = &m_IPCamInfo; // GOP S1, LG CNS 카메라.
               m_pPTZCtrl->m_pStream    = m_IPCamInfo.m_pStream;
               m_pPTZCtrl->Open( &m_IPCamInfo.m_PTZInfo );
            } else if( PTZCtrlConnType_LinkToPTZCtrl == m_IPCamInfo.m_PTZInfo.m_nConnectType ) {
               CPTZCtrl_Linking* pPTZCtrl_Linking = (CPTZCtrl_Linking*)m_pPTZCtrl;
               pPTZCtrl_Linking->m_pVCM           = m_pVCM;
               m_pPTZCtrl->Open( &m_IPCamInfo );
            } else {
               // m_IPCamInfo를 넘기기 전에 IR Light설정을 m_IPCamInfo.m_PTZInfo 변수에 복사한다.
               // 자세한 내용은 class CPTZCtrlInfo::m_IRLightCtrlInfo 변수 선언부를 참조할 것.
               m_IPCamInfo.m_PTZInfo.m_bUseOuterIRLightCtrlInfo  = m_PTZInfo.m_bUseOuterIRLightCtrlInfo;
               m_IPCamInfo.m_PTZInfo.m_bIRControlByIntelliVIX    = m_PTZInfo.m_bIRControlByIntelliVIX; // (mkjang-141211)
               m_IPCamInfo.m_PTZInfo.m_bUseCameraDayAndNigntMode = m_PTZInfo.m_bUseCameraDayAndNigntMode; // (mkjang-141211)
               m_IPCamInfo.m_PTZInfo.m_IRLightCtrlInfo           = m_PTZInfo.m_IRLightCtrlInfo;
               m_pPTZCtrl->Open( &m_IPCamInfo );
            }
            if( pIPCamera )
               pIPCamera->LoadPTZPosToPresetMap();
         } else if( VideoChannelType_Hi3516A_Cellinx == m_nVideoChannelType ) {
            m_pPTZCtrl->SetAbsPos( m_PTZInfo.m_pvCurPos );
            m_pPTZCtrl->SetVideoSize( ISize2D( m_nWidth, m_nHeight ) );
            if( m_pPTZCtrl->Open( &m_PTZInfo ) ) {
               m_nState |= VCState_FailureOpenSerialPort;
            }
            if( m_nPTZControlOpenMode == PTZCtrlOpenMode_Normal ) {
               std::string sid = sutil::sformat( "vc_%02d", m_nVCID );
               CPTZ2D* pPTZ2D  = NULL;
               if( __PTZ3DFile.FindPTZ2D( sid.c_str(), &pPTZ2D ) )
                  __PTZ3DFile.AddPTZ2D( sid.c_str(), &pPTZ2D );
               if( pPTZ2D ) {
                  m_pPTZCtrl->SetPTZ2D( pPTZ2D );
                  pPTZ2D->NRef++;
               }
            }
         }
      } else if( VideoChannelType_AnalogCamera == m_nVideoChannelType ) {
         m_pPTZCtrl->SetAbsPos( m_PTZInfo.m_pvCurPos );
         m_pPTZCtrl->SetVideoSize( ISize2D( m_nWidth, m_nHeight ) );
         if( m_pPTZCtrl->Open( &m_PTZInfo ) ) {
            m_nState |= VCState_FailureOpenSerialPort;
         }
         if( m_nPTZControlOpenMode == PTZCtrlOpenMode_Normal ) {
            std::string sid = sutil::sformat( "vc_%02d", m_nVCID );
            CPTZ2D* pPTZ2D  = NULL;
            if( __PTZ3DFile.FindPTZ2D( sid.c_str(), &pPTZ2D ) )
               __PTZ3DFile.AddPTZ2D( sid.c_str(), &pPTZ2D );
            if( pPTZ2D ) {
               m_pPTZCtrl->SetPTZ2D( pPTZ2D );
               pPTZ2D->NRef++;
            }
         }
      }
#ifdef __SUPPORT_VIRTUAL_PTZ_CAMERA
      else if( VideoChannelType_VirtualPTZ == m_nVideoChannelType ) {
         m_pPTZCtrl->SetAbsPos( m_PTZInfo.m_pvCurPos );
         m_pPTZCtrl->SetVideoSize( ISize2D( m_nWidth, m_nHeight ) );
         m_pPTZCtrl->MaxZoomFactor = m_VirtualPTZCamInfo.m_fMaxZoom;
         if( m_Cameras.size() ) {
            CCamera_VirtualPTZ* pVirtualPTZCamera = (CCamera_VirtualPTZ*)m_Cameras[0];
            if( m_pPTZCtrl->Open( pVirtualPTZCamera ) ) {
               m_nState |= VCState_FailureOpenSerialPort;
            }
         }
         if( m_nPTZControlOpenMode == PTZCtrlOpenMode_Normal ) {
            std::string sid = sutil::sformat( "vc_%02d", m_nVCID );
            CPTZ2D* pPTZ2D  = NULL;
            if( __PTZ3DFile.FindPTZ2D( sid.c_str(), &pPTZ2D ) )
               __PTZ3DFile.AddPTZ2D( sid.c_str(), &pPTZ2D );
            if( pPTZ2D ) {
               m_pPTZCtrl->SetPTZ2D( pPTZ2D );
               pPTZ2D->NRef++;
            }
         }
      }
#endif
      else if( VideoChannelType_Hi3516A_Cellinx == m_nVideoChannelType ) {
         m_pPTZCtrl->SetAbsPos( m_PTZInfo.m_pvCurPos );
         m_pPTZCtrl->SetVideoSize( ISize2D( m_nWidth, m_nHeight ) );
         if( m_pPTZCtrl->Open( &m_PTZInfo ) ) {
            m_nState |= VCState_FailureOpenSerialPort;
         }
         if( m_nPTZControlOpenMode == PTZCtrlOpenMode_Normal ) {
            std::string sid = sutil::sformat( "vc_%02d", m_nVCID );
            CPTZ2D* pPTZ2D  = NULL;
            if( __PTZ3DFile.FindPTZ2D( sid.c_str(), &pPTZ2D ) )
               __PTZ3DFile.AddPTZ2D( sid.c_str(), &pPTZ2D );
            if( pPTZ2D ) {
               m_pPTZCtrl->SetPTZ2D( pPTZ2D );
               pPTZ2D->NRef++;
            }
         }
      }
   }

#endif //__SUPPORT_PTZ_CAMERA

   return DONE;
}

int CVideoChannel::DeletePTZControl( int nPTZControlOpenMode )
{
   if( m_pPTZCtrl ) {
      if( IPCameraType_IVXVideoServer == m_IPCamInfo.m_nIPCameraType ) {
         nPTZControlOpenMode = PTZCtrlOpenMode_CreateOnly;
      }

      if( nPTZControlOpenMode == PTZCtrlOpenMode_Normal ) {
         if( m_nVideoChannelType == VideoChannelType_IPCamera ) {
            m_IPCamInfo.m_PTZInfo.m_pvCurPos = m_pPTZCtrl->GetAbsPos();
         } else if( m_nVideoChannelType == VideoChannelType_AnalogCamera ) {
            m_PTZInfo.m_pvCurPos = m_pPTZCtrl->GetAbsPos();
         }
      }
      m_pPTZCtrl->Close();
      delete m_pPTZCtrl;
      m_pPTZCtrl = NULL;
   }
   return DONE;
}
#endif //__SUPPORT_PTZ_CAMERA

int CVideoChannel::LoadPVTFiles()
{
#ifdef __SUPPORT_PANORAMIC_CAMERA
   std::string& strPVTPathName = m_PanoramaCamInfo.m_strPVTPath;
   std::string& strVCTPathName = m_PanoramaCamInfo.m_strVCTPath;
   if( strPVTPathName.length() == 0 ) {
      logd( "Error: The cylinder mapping table path is empty.\n" );
      return ( 1 );
   }
   if( PVFCompositor.ReadFile( strPVTPathName.c_str() ) ) {
      logd( "Error: Cannot load the cylinder mapping table file.[%s]\n", strPVTPathName.c_str() );
      m_nState |= VCState_FailureOpenPVTFile;
      return ( 2 );
   }
   m_nInnerState &= ~VCInnerState_PerformVignettingCorrection;
   if( strVCTPathName.length() > 0 ) {
      if( LVCorrector.ReadFile( strVCTPathName.c_str() ) ) {
         logd( "Error: Cannot load the lens vignetting correction file.[%s]\n", strVCTPathName.c_str() );
         m_nState |= VCState_FailureOpenVCTFile;
         return ( 4 );
      }
      m_nInnerState |= VCInnerState_PerformVignettingCorrection;
   }
#if defined( __USE_DEBUG_NEW )
   logd( "Cylinder Mapping Table File: %s\n", strPVTPathName );
   logd( "     # of Source Images: %d\n", PVFCompositor.NumSrcImages );
   logd( "     Source Image Resolution: %d x %d\n", PVFCompositor.SrcImgWidth, PVFCompositor.SrcImgHeight );
   logd( "     Panoramic Image Resolution: %d x %d\n", PVFCompositor.Width, PVFCompositor.Height );
#endif
#endif
   return ( DONE );
}

int CVideoChannel::GetMotionStatus( BOOL& bMotion )
{
   if( !( m_nState & VCState_Activate ) || ( m_nState & VCState_Deactivating ) ) return ( -1 );
   bMotion = FALSE;
   switch( m_nVideoChannelType ) {
   case VideoChannelType_IPCamera:
      if( m_Cameras.size() && m_Cameras[0]->GetMotionStatus( bMotion ) ) {
         bMotion = FALSE;
         return -1;
      }
      break;
   default:
      bMotion = FALSE;
   }
   return DONE;
}

int CVideoChannel::GetAlarmInputStatus( int& nChIdx, BOOL& bSignal )
{
   if( !( m_nState & VCState_Activate ) || ( m_nState & VCState_Deactivating ) ) return ( -1 );
   bSignal = FALSE;
   switch( m_nVideoChannelType ) {
   case VideoChannelType_AnalogCamera:
   case VideoChannelType_VideoFile:
   case VideoChannelType_IPCamera:
      if( m_Cameras.size() && m_Cameras[0]->GetAlarmInputStatus( nChIdx, bSignal ) ) {
         bSignal = FALSE;
         return -1;
      }
      break;
   default:
      bSignal = FALSE;
   }
   return DONE;
}

int CVideoChannel::SetDecodingFPS( float fFPS )
{
   if( VideoChannelType_IPCamera == m_nVideoChannelType ) {
      if( m_Cameras.size() ) {
         CCamera_IP* pIPCamera              = (CCamera_IP*)m_Cameras[0];
         pIPCamera->m_pInfo->m_fDecodingFPS = fFPS;
      }
   }
   return DONE;
}

int CVideoChannel::SetRelayOutputStatus( int nChIdx, BOOL bSignal )
{
   if( !( m_nState & VCState_Activate ) || ( m_nState & VCState_Deactivating ) ) return ( -1 );
   if( VideoChannelType_IPCamera == m_nVideoChannelType ) {
      if( m_Cameras.size() ) {
         m_Cameras[0]->SetRelayOutputStatus( nChIdx, bSignal );
      }
   }
   return DONE;
}

int CVideoChannel::GetRelayOutputStatus( int nChIdx, BOOL& bSignal )
{
   if( !( m_nState & VCState_Activate ) || ( m_nState & VCState_Deactivating ) ) return ( -1 );
   if( VideoChannelType_IPCamera == m_nVideoChannelType ) {
      if( m_Cameras.size() ) {
         m_Cameras[0]->GetRelayOutputStatus( nChIdx, bSignal );
      }
   }
   return DONE;
}

void CVideoChannel::Proc_Capture()
{
   if( m_nCameraNum == 0 ) {
      LOGE << "cam size is 0";
      return;
   }

   int i;

   ISize2D s2CurrImgSize_Disp;
   ISize2D s2PrevImgSize_Disp;
   ISize2D s2CurrImgSize_IP;
   ISize2D s2PrevImgSize_IP;
   BOOL bYUY2BuffImported = FALSE;

   m_pTimer[VC_Cap]->StartTimer();
   m_pTimer[VC_Cap]->SetFPS( 60.0f ); // 초기에 설정하는 60fps는 의미가 없음.

   // 프레임비율 계산을 위한 초기화 .
   CFrameRateChecker framerate_checker;
   framerate_checker.Initialize( m_fFPS_Cap );
   framerate_checker.CheckStart();

   // VMC 메니저의 메인프로시져로 옮길 필요가 있음.
   // 비디오 신호가 있는지 확인하기 위한 플레그.
   UINT64 nCamFlag = 0;
   for( i = 0; i < m_nCameraNum; i++ ) {
      UINT64 nCurCamFlag = UINT64( 1 ) << i;
      nCamFlag |= nCurCamFlag;
   }

#ifdef __SUPPORT_IMAGE_ENHANCEMENT
   // 영상 개선 : 이 스레드에 생성된 영상개선 모듈은 비디오파일과 가상PTZ에서만 동작하도록 되어 있다.
   //            IP 카메라의 경우에는 CAVCodec의 디코딩하는 과정에서 영상 개선을 하도록 되어 있음.
   CImageEnhancement imageEnhancer;
   imageEnhancer.Initialize( m_nWidth, m_nHeight, m_fFPS_Cap, FALSE );
#endif
   m_pYUY2Buff             = NULL;
   m_bNoVideoSignal[VC_IP] = 0;
#ifdef __SUPPORT_PANORAMIC_CAMERA
   // 파노라마 카메라 Image Brightness Correction 기능 관련 초기화
   uint32_t nIBCCheckTime = 5000;
   uint32_t nIBCLastTime  = GetTickCount() - nIBCCheckTime * 2;
#endif

   uint nPrevVCState = m_nState;

   int nCurrDispStreamIndex   = 0;
   int nPrevDispStreamIndex   = 0;
   CCamera_IP* pIPCamera_Disp = NULL;

   m_nState &= ~VCState_NoiseReductorInitialized;
   m_nCountCap = 0;

   while( true ) {
      if( ( m_nState & VCState_SignalDeactivate ) || m_bStopThead[VC_Cap] ) {
         break;
      }

      if( VideoChannelType_IPCamera == m_nVideoChannelType ) {
         nCurrDispStreamIndex = m_nDecodeStreamIndexOnProcCallbackFunc[VCProcCallbackFuncType_Display];
         pIPCamera_Disp       = GetIPCameraByStreamIdx( nCurrDispStreamIndex ); // 수정 필요함.
      } else if( VideoChannelType_Hi3516A_Cellinx == m_nVideoChannelType ) {
         nCurrDispStreamIndex = m_nDecodeStreamIndexOnProcCallbackFunc[VCProcCallbackFuncType_Display];
      }

#ifdef __SUPPORT_IMAGE_ENHANCEMENT
      // 0. 영상 개선 관련 설정 하위 객체에 복사.
      if( 0 ) { // Test Code
         m_nCountDefog++;
         m_nCountStabilization++;
         if( m_nCountStabilization % 10 < 5 )
            m_bUseVideoStabilization = TRUE;
         else
            m_bUseVideoStabilization = FALSE;
         if( m_nCountDefog % 12 < 6 )
            m_bUseDefog = TRUE;
         else
            m_bUseDefog = FALSE;
      }
      if( VideoChannelType_IPCamera == m_nVideoChannelType ) {
         BOOL bPTZCameraMoving = FALSE;
#ifdef __SUPPORT_PTZ_CAMERA
         if( m_pPTZCtrl )
            bPTZCameraMoving = m_pPTZCtrl->IsPTZFMoving();
#endif
         for( i = 0; i < m_nCameraNum; i++ ) {
            CCamera_IP* pIPCamera = (CCamera_IP*)m_Cameras[i];

            CIPCamStream* pStream = pIPCamera->GetIPCamStream();
            if( pStream ) {
               m_bPTZCameraMoving                                = bPTZCameraMoving;
               pStream->m_VideoDecoder.m_VideoStabilizationSetup = *( (CVideoStabilizationSetup*)this );
               pStream->m_VideoDecoder.m_VideoDefoggingSetup     = *( (CVideoDefoggingSetup*)this );
            }
         }
      } else {
         imageEnhancer.m_VideoStabilizationSetup = *( (CVideoStabilizationSetup*)this );
         imageEnhancer.m_VideoDefoggingSetup     = *( (CVideoDefoggingSetup*)this );
      }
#endif

      Array1D<byte*> s_buffers( m_nCameraNum );
      if( m_nVideoChannelType != VideoChannelType_Panorama ) m_csSrcImage.Lock(); // 일반 카메라

      BOOL bCaptureOk = TRUE; // 캡처가 한채널(카메라) 이상 수행되었는지 여부

      // 1. GetImage Lock
      for( i = 0; i < m_nCameraNum; i++ ) {
         ICamera* pCamera = m_Cameras[i];
         UINT64 nChFlag   = UINT64( 1 ) << i;
         if( pCamera ) {
            // MS: IP 카메라일 경우에는 모든 카메라에서 이미지 버퍼를 얻을 필요가 없다. --> 완료
            if( pIPCamera_Disp && ( pIPCamera_Disp != pCamera ) )
               continue;
            s_buffers[i] = NULL;
            if( pCamera->IsVideoSignalAlive() ) {
               byte* s_buffer        = NULL;
               BOOL bGetImageLockRet = pCamera->GetImage_Lock( &s_buffer );
               if( ICamera::GetImgeLockRet_Done != bGetImageLockRet ) {
                  bCaptureOk = FALSE;
               }

               s_buffers[i] = s_buffer;
#ifdef __SUPPORT_PANORAMIC_CAMERA
               if( m_nVideoChannelType == VideoChannelType_Panorama ) {
                  if( m_nInnerState & VCInnerState_PerformVignettingCorrection ) {
                     LVCorrector.Perform_YUY2( i, s_buffers[i], CapturedImages[i] );
                  }
               }
#endif
               // IP 카메라가 아닌 경우에만 각 카메라의 비디오신호 상태를 체크한다.
               if( VideoChannelType_IPCamera != m_nVideoChannelType )
                  pCamera->m_nState &= ~ICamera::State_VideoSignalNotOK;
            } else {
               if( VideoChannelType_IPCamera != m_nVideoChannelType )
                  pCamera->m_nState |= ICamera::State_VideoSignalNotOK;
            }
         } else {
            s_buffers[i] = NULL;
            m_nVideoSignalFlag &= ~nChFlag;
         }
      }

      // 2. Set Source Image Buffer Pointer (m_pYUY2Buff)
      m_bAllSignalOK = ( m_nVideoSignalFlag & nCamFlag ) == nCamFlag;
#ifdef __SUPPORT_PANORAMIC_CAMERA
      if( m_nVideoChannelType == VideoChannelType_Panorama ) {
         for( i = 0; i < m_nCameraNum; i++ ) {
            if( s_buffers[i] )
               memcpy( (byte*)CapturedImages[i], s_buffers[i], CapImgSize.Width * CapImgSize.Height * 2 );
         }
         m_csSrcImage.lock();
         if( m_PanoramaCamInfo.m_bUseBC ) {
            if( !m_nNoVideoSignalFlag ) {
               if( GetTickCount() - nIBCLastTime > nIBCCheckTime ) {
                  IBCorrector.GetBrightnessDifferences_YUY2( CapturedImages );
                  nIBCLastTime = GetTickCount();
               }
            }
            if( !m_nNoVideoSignalFlag )
               IBCorrector.Perform_YUY2( CapturedImages, m_PanoramaCamInfo.m_nBCBaseChNo );
         }
         // Video Stabilization (Panorama) : CapturedImages -> m_TempYUY2Buff -> m_YUY2Buff
         PVFCompositor.Perform_YUY2( CapturedImages, m_YUY2Buff );
         m_pYUY2Buff = (byte*)m_YUY2Buff;
         AfxGetLocalFileTime( &m_ftTimestamp ); // 현재시간
         m_csSrcImage.unlock();
      } else
#endif
      {
         // MS: IP 카메라의 경우에는 현재 디스플레이용으로 설정된 버퍼만 Import해야 한다.
         byte* pSrcBuff    = s_buffers[nCurrDispStreamIndex];
         int nYUY2BuffSize = GetYUY2ImageLength( m_nWidth, m_nHeight );
         if( VideoChannelType_IPCamera == m_nVideoChannelType ) {
            if( pSrcBuff ) {
               m_YUY2Buff.Import( pSrcBuff, nYUY2BuffSize );
               bYUY2BuffImported = TRUE;
            }
         }

         if( VideoChannelType_VideoFile == m_nVideoChannelType
#ifdef __SUPPORT_VIRTUAL_PTZ_CAMERA
             || VideoChannelType_VirtualPTZ == m_nVideoChannelType
#endif
         ) {
#ifdef __SUPPORT_IMAGE_ENHANCEMENT
            if( FALSE == imageEnhancer.IsVideoEnhancementNotExist() ) {
               if( bYUY2BuffImported && m_YUY2Buff.Length ) {
                  m_YUY2Buff.Import( NULL, 0 );
               }
               if( m_YUY2Buff.Length != nYUY2BuffSize ) {
                  m_YUY2Buff.Create( nYUY2BuffSize );
                  if( pSrcBuff )
                     memcpy( (byte*)m_YUY2Buff, pSrcBuff, nYUY2BuffSize );
                  bYUY2BuffImported = FALSE;
               }
            } else
#endif
            {
               if( pSrcBuff ) {
                  m_YUY2Buff.Import( pSrcBuff, nYUY2BuffSize );
                  bYUY2BuffImported = TRUE;
               }
            }
#ifdef __SUPPORT_IMAGE_ENHANCEMENT
            imageEnhancer.ImageEnhancementInitProc( pSrcBuff, m_YUY2Buff );
            imageEnhancer.ImageEnhancementMainProc();
#endif
         } else if( VideoChannelType_AnalogCamera == m_nVideoChannelType ) {
            // 캡쳐보드(Onto)의 소스버퍼는 일반메모리가 아닌 캡쳐보드의 메모리로 추정된다.
            // 이 메모리는 일반메모리보다 속도가 느리기 때문에 일반메모리로 카피하여 사용한다
            // Video Stabilization : pSrcBuff -> m_YUY2Buff
            if( bYUY2BuffImported && m_YUY2Buff.Length ) {
               m_YUY2Buff.Import( NULL, 0 );
            }
            if( m_YUY2Buff.Length != nYUY2BuffSize ) {
               m_YUY2Buff.Create( nYUY2BuffSize );
               bYUY2BuffImported = FALSE;
            }
            if( pSrcBuff ) {
               memcpy( (byte*)m_YUY2Buff, pSrcBuff, nYUY2BuffSize );
            }
         }
         m_pYUY2Buff = m_YUY2Buff;
         AfxGetLocalFileTime( &m_ftTimestamp ); // 현재시간
      }

      // 3. GetImage Unlock
      for( i = 0; i < m_nCameraNum; i++ ) {
         ICamera* pCamera = m_Cameras[i];
         if( pCamera && s_buffers[i] ) {
            pCamera->GetImage_Unlock();
         }
      }
      if( m_nVideoChannelType != VideoChannelType_Panorama ) m_csSrcImage.Unlock(); // 일반 카메라

#ifdef __SUPPORT_PTZ_CAMERA
#ifdef __SUPPORT_VIRTUAL_PTZ_CAMERA
      // 본 비디오채널이 가상 PTZ 카메라의 소스채널이면 가상 PTZ채널들에게 캡쳐이벤트 신호를 보낸다.
      // MS: 멀티스트림을 지원하는 경우에는 이곳에서 가상PTZ들에게 캡쳐 이벤트를 보내어서는 안된다.
      // 각각의 채널이 캡쳐가 된 경우에만 이벤트를 보내도록 한다.
      if( VideoChannelType_IPCamera != m_nVideoChannelType ) {
         if( FALSE == m_bStopThead[VC_Cap] ) {
            SignalCaptureEventToVerturalPTZCameras();
         }
      }
#endif // __SUPPORT_VIRTUAL_PTZ_CAMERA
#endif // __SUPPORT_PTZ_CAMERA

// * PTZ 설정영역들의 좌표들을 업데이트 한다.
// MS: 멀티스트림을 지원하지 않는 카메라의 경우에는 이곳에서 PTZ영역을 업데이트하는 것이 맞다.
//     하지만 멀티스트림을 지원하는 경우에는 영상 처리용으로 사용되는 스트림에서 PTZ 영역을 업데이트 하여야 한다.
#ifdef __SUPPORT_PTZ_CAMERA
      if( VideoChannelType_IPCamera != m_nVideoChannelType )
         PTZAreaProc();
#endif

      // 화면 출력 콜백함수 호출
      if( bCaptureOk )
         DisplayOneFrame_CaptureProc( pIPCamera_Disp, s2CurrImgSize_Disp, nPrevDispStreamIndex, s2PrevImgSize_Disp, nCurrDispStreamIndex );

// 비디오 파일이면 서 영상처리 스레드를 수행하지 않는 경우 이 스레드에서 영상처리를 수행한다.
#ifdef __SUPPORT_VIDEO_FILE_CAMERA
      if( m_nVideoChannelType == VideoChannelType_VideoFile && !( m_nThreadFlag & VCThreadType_IP ) && ( m_nCameraNum > 0 ) ) {
         if( m_pCBImageProcess ) {
            m_nCountIP              = m_nCountCap;
            int nStreamIndex        = 0;
            int nCamState           = m_Cameras[nStreamIndex]->m_nState;
            s2CurrImgSize_IP.Width  = m_nWidth;
            s2CurrImgSize_IP.Height = m_nHeight;
            if( s2CurrImgSize_IP.Width != s2PrevImgSize_IP.Width || s2CurrImgSize_IP.Height != s2PrevImgSize_IP.Height ) {
               nCamState |= ICamera::State_ResolutionChanged;
               m_nCountIP = 0;
            }
            CCamera_File* pCamera = (CCamera_File*)m_Cameras[nStreamIndex];
            if( pCamera && pCamera->IsVideoSignalAlive() ) {
               int nImgProcSamplingRate = int( ceil( m_fFPS_Cap / m_fFPS_IP ) );
               if( m_nCountCap % nImgProcSamplingRate == 0 ) {
                  VC_PARAM_STREAM input;
                  input.pParam     = m_pParam;
                  input.nVCState   = m_nState;
                  input.nCount     = m_nCountIP;
                  input.nStreamIdx = nStreamIndex;
                  input.nCamState  = nCamState;
                  input.pYUY2Buff  = (byte*)m_YUY2Buff;
                  input.nWidth     = m_nWidth;
                  input.nHeight    = m_nHeight;
                  m_pCBImageProcess( input );
               }
            }
         }
         m_nCountCap          = m_nCountIP; // 갑작스런 장면변화의 경우 Count 콜백 함수에서 수정할 수 있음.
         m_fCurFrmRate[VC_IP] = m_fCurFrmRate[VC_Cap];
      }
#endif

      // MS: 프레임 비율 체크는 각 채널별로 체크되어야 한다.
      // 프레임 비율 체크
      float fFrameRate       = framerate_checker.CalcFrameRate();
      m_fCurFrmRate[VC_Disp] = m_fCurFrmRate[VC_Cap] = fFrameRate;

      if( m_nVideoChannelType == VideoChannelType_AnalogCamera ) {
         //CCamera_Analog* pCamera = (CCamera_Analog*) m_Cameras[0];
         //// MS: 멀티스트리밍 작업이 완료되면 반드시 OD4120, OD8240 카메라에 대하여 테스트 해볼 것..
         //if (m_nVideoSignalFlag) m_pTimer[VC_Cap]->SetFPS (m_fFPS_Cap);
         //else                    m_pTimer[VC_Cap]->SetFPS (1.0f);
         //m_pTimer[VC_Cap]->Wait ();
      } else {
         if( m_nVideoSignalFlag ) {
            float fTgtFrameRate = m_fFPS_Cap;
            if( m_nVideoChannelType == VideoChannelType_IPCamera ) {
               if( pIPCamera_Disp && pIPCamera_Disp->IsVideoSignalAlive() && !( pIPCamera_Disp->m_nState & ICamera::State_ChangingVideoBuffer ) ) {
                  fTgtFrameRate = pIPCamera_Disp->m_pInfo->m_fFPS_Cap;
                  if( fabs( pIPCamera_Disp->m_fFrameRate - fTgtFrameRate ) / fTgtFrameRate > 0.15f ) {
                     // jun : fFrameRate 값 예외처리. 아마도 메모리 침범에 의한 값의 변경을 의심해 본다.
                     if( pIPCamera_Disp->m_fFrameRate <= 0.0f ) pIPCamera_Disp->m_fFrameRate = 60.0f;
                     if( pIPCamera_Disp->m_fFrameRate > 60.0f ) pIPCamera_Disp->m_fFrameRate = 60.0f;
                     fTgtFrameRate = pIPCamera_Disp->m_fFrameRate;
                  }
               } else
                  fTgtFrameRate = 0.5f;
            }
            m_pTimer[VC_Cap]->SetFPS( fTgtFrameRate );
         } else
            m_pTimer[VC_Cap]->SetFPS( 1.0f );

         m_pTimer[VC_Cap]->Wait();
      }

      // 각종 상태 복사하기
      nPrevVCState     = m_nState;
      s2PrevImgSize_IP = s2CurrImgSize_IP;
   }

   m_pTimer[0]->StopTimer();

   if( bYUY2BuffImported ) {
      CCamera_IP* pIPCamera = (CCamera_IP*)m_Cameras[0];
      if( pIPCamera ) {
         m_csSrcImage.Lock();
         m_YUY2Buff.Import( 0, 0 );
         m_csSrcImage.Unlock();
      }
   }

   m_pYUY2Buff = NULL;

   if( VideoChannelType_IPCamera == m_nVideoChannelType ) {
      if( m_DisplayingImageQueue.IsInitialized() )
         m_DisplayingImageQueue.Close();
   }
}

void CVideoChannel::Proc_Record()
{
   if( m_nCameraNum == 0 )
      return;

   ISize2D s2CurrImgSize_Rec;
   ISize2D s2PrevImgSize_Rec;

   // 직접 저장하기를 하는 경우에는 별도의 녹화 스레드를 사용할 필요가 없다.
   if( !m_pCBRecord )
      return;
   m_pTimer[VC_Rec]->StartTimer();
   m_pTimer[VC_Rec]->SetFPS( m_fFPS_Rec );
   CFrameRateChecker framerate_checker;
   framerate_checker.Initialize( m_fFPS_Rec );
   framerate_checker.CheckStart();
   DWORD dwFlag = 0;
   m_nCountRec  = 0;

   while( 1 ) {
      if( ( m_nState & VCState_Activate ) && m_pCBRecord && m_nCameraNum > 0 && m_nVideoSignalFlag ) {
         if( VideoChannelType_IPCamera == m_nVideoChannelType ) {
            int& nStreamIndex     = m_nDecodeStreamIndexOnProcCallbackFunc[VCProcCallbackFuncType_Recoding];
            CCamera_IP* pIPCamera = GetIPCameraByStreamIdx( nStreamIndex );
            if( pIPCamera && pIPCamera->IsVideoSignalAlive() && !( pIPCamera->m_nState & ICamera::State_ChangingVideoBuffer ) ) {
               int nCamState            = pIPCamera->m_nState;
               byte* pYUY2Buff          = (byte*)pIPCamera->m_SrcImg;
               int& nImgWidth           = pIPCamera->m_pInfo->m_nWidth;
               int& nImgHeight          = pIPCamera->m_pInfo->m_nHeight;
               DWORD dwFlag             = 0;
               int nYUY2BuffLen         = GetYUY2ImageLength( nImgWidth, nImgHeight );
               s2CurrImgSize_Rec.Width  = nImgWidth;
               s2CurrImgSize_Rec.Height = nImgHeight;
               if( s2CurrImgSize_Rec.Width != s2PrevImgSize_Rec.Width || s2CurrImgSize_Rec.Height != s2PrevImgSize_Rec.Height ) {
                  nCamState |= ICamera::State_ResolutionChanged;
                  m_nCountRec = 0;
               }
               if( pIPCamera->m_SrcImg.Length == nImgWidth * nImgHeight * 2 ) {
                  VC_PARAM_STREAM input;
                  input.pParam     = m_pParam;
                  input.nVCState   = m_nState;
                  input.nCount     = m_nCountCap;
                  input.nStreamIdx = nStreamIndex;
                  input.nCamState  = nCamState;
                  input.pYUY2Buff  = pYUY2Buff;
                  input.nWidth     = s2CurrImgSize_Rec.Width;
                  input.nHeight    = s2CurrImgSize_Rec.Height;
                  m_pCBRecord( input );
                  m_nCountRec++;
               }
            }
         } else {
            if( ( m_nCountCap >= 1 ) && ( m_nWidth != 0 ) ) {
               INT nStreamIndex         = 0;
               int nCamState            = m_Cameras[nStreamIndex]->m_nState;
               int nYUY2BuffLen         = GetYUY2ImageLength( m_nWidth, m_nHeight );
               DWORD dwFlag             = 0;
               s2CurrImgSize_Rec.Width  = m_nWidth;
               s2CurrImgSize_Rec.Height = m_nHeight;
               if( s2CurrImgSize_Rec.Width != s2PrevImgSize_Rec.Width || s2CurrImgSize_Rec.Height != s2PrevImgSize_Rec.Height ) {
                  nCamState |= ICamera::State_ResolutionChanged;
                  m_nCountRec = 0;
               }
               VC_PARAM_STREAM input;
               input.pParam     = m_pParam;
               input.nVCState   = m_nState;
               input.nCount     = m_nCountCap;
               input.nStreamIdx = nStreamIndex;
               input.nCamState  = nCamState;
               input.pYUY2Buff  = m_YUY2Buff;
               input.nWidth     = m_nWidth;
               input.nHeight    = m_nHeight;
               m_pCBRecord( input );
               m_nCountRec++;
            }
         }
      }

      s2PrevImgSize_Rec = s2CurrImgSize_Rec;

      if( ( m_nState & VCState_SignalDeactivate ) || m_bStopThead[VC_Rec] ) {
         break;
      }
      m_fCurFrmRate[VC_Rec] = framerate_checker.CalcFrameRate();
      m_pTimer[VC_Rec]->SetFPS( m_fFPS_Rec );
      m_pTimer[VC_Rec]->Wait();
   }
   m_pTimer[VC_Rec]->StopTimer();
}

void CVideoChannel::Proc_ImgProcess()
{
   // MS: 영상처리를 하지 않는 경우에는 영상처리 스레드를 실행할 필요가 없다.
   //     IntelliVIX의 경우에는 영상처리 스레드에서 PTZ제어 처리 및 프리셋 투어링 기능을 동작시키기 때문에
   //     비디오 분석을 하지 않더라도 영상처리 콜백을 호출할 필요가 있다.

   ISize2D s2CurrImgSize_IP;
   ISize2D s2PrevImgSize_IP;

   if( !m_pCBImageProcess ) return;
   m_pTimer[VC_IP]->StartTimer();
   m_pTimer[VC_IP]->SetFPS( m_fFPS_IP );
   CFrameRateChecker framerate_checker;
   framerate_checker.Initialize( m_fFPS_IP );
   framerate_checker.CheckStart();   
   CPerformanceCounter pc;

   m_nCountIP = 0;

   UINT64 nDecodeCountPrev = 0;

   while( 1 ) {
      if( ( m_nState & VCState_SignalDeactivate ) || m_bStopThead[VC_IP] )
         break;
      if( ( m_nState & VCState_Activate ) && m_pCBImageProcess && m_nCameraNum > 0 && m_nVideoSignalFlag ) {
         if( VideoChannelType_IPCamera == m_nVideoChannelType ) {
            int& nStreamIndex     = m_nDecodeStreamIndexOnProcCallbackFunc[VCProcCallbackFuncType_ImgProc];
            CCamera_IP* pIPCamera = GetIPCameraByStreamIdx( nStreamIndex );
            if( pIPCamera && pIPCamera->IsVideoSignalAlive() && pIPCamera->IsFirstFrameJustDecoded() ) {
               //if (nDecodeCountPrev != pIPCamera->m_nDecodeCount)
               {
                  int nCamState           = pIPCamera->m_nState;
                  byte* pYUY2Buff         = (byte*)pIPCamera->m_SrcImg;
                  int& nImgWidth          = pIPCamera->m_pInfo->m_nWidth;
                  int& nImgHeight         = pIPCamera->m_pInfo->m_nHeight;
                  s2CurrImgSize_IP.Width  = nImgWidth;
                  s2CurrImgSize_IP.Height = nImgHeight;
                  if( s2CurrImgSize_IP.Width != s2PrevImgSize_IP.Width || s2CurrImgSize_IP.Height != s2PrevImgSize_IP.Height ) {
                     nCamState |= ICamera::State_ResolutionChanged;
                     m_nCountIP = 0;
                  }
                  CIPCamStream* pStream = pIPCamera->GetIPCamStream();
                  if( CIPCamStream::State_RTSP_ByePacketReceived & pStream->m_nState ) {
                     pStream->m_nState &= ~CIPCamStream::State_RTSP_ByePacketReceived;
                     if( FALSE == m_bEndOfFile ) {
                        m_bEndOfFile = TRUE;
                     }
                  }

                  if( pIPCamera->m_SrcImg.Length == nImgWidth * nImgHeight * 2 ) {
                     pc.CheckTimeStart();
                     VC_PARAM_STREAM input;
                     input.pParam     = m_pParam;
                     input.nVCState   = m_nState;
                     input.nCount     = m_nCountIP;
                     input.nStreamIdx = nStreamIndex;
                     input.nCamState  = nCamState;
                     input.pYUY2Buff  = pYUY2Buff;
                     input.nWidth     = nImgWidth;
                     input.nHeight    = nImgHeight;
                     m_pCBImageProcess( input );
                     m_dfImageProcessDelayTime = pc.GetElapsedTime() / 1000.0;
                     double dfRatioOfAveraging = 1.0 / ( m_fFPS_IP * 10.0 );
                     if( m_dfAvgImageProcessDelayTime == 0.0 )
                        m_dfAvgImageProcessDelayTime = m_dfImageProcessDelayTime;
                     else
                        m_dfAvgImageProcessDelayTime = m_dfAvgImageProcessDelayTime * ( 1.0 - dfRatioOfAveraging ) + m_dfImageProcessDelayTime * dfRatioOfAveraging;
                     nDecodeCountPrev = pIPCamera->m_nDecodeCount;
                     m_nCountIP++;
                  }
               }
            }
         } else {
            if( ( m_nCountCap >= 1 ) && ( m_nWidth != 0 ) ) {
               INT nStreamIndex        = 0;
               int nCamState           = 0;
               s2CurrImgSize_IP.Width  = m_nWidth;
               s2CurrImgSize_IP.Height = m_nHeight;
               if( s2CurrImgSize_IP.Width != s2PrevImgSize_IP.Width || s2CurrImgSize_IP.Height != s2PrevImgSize_IP.Height ) {
                  nCamState |= ICamera::State_ResolutionChanged;
                  m_nCountIP = 0;
               }
               VC_PARAM_STREAM input;
               input.pParam     = m_pParam;
               input.nVCState   = m_nState;
               input.nCount     = m_nCountIP;
               input.nStreamIdx = nStreamIndex;
               input.nCamState  = nCamState;
               input.pYUY2Buff  = (byte*)m_YUY2Buff;
               input.nWidth     = m_nWidth;
               input.nHeight    = m_nHeight;
               m_pCBImageProcess( input );
               m_nCountIP++;
            }
         }
      }

      s2PrevImgSize_IP = s2CurrImgSize_IP;

      m_fCurFrmRate[VC_IP]  = framerate_checker.CalcFrameRate();
      float fTgtIPFrameRate = m_fFPS_IP;

      m_pTimer[VC_IP]->SetFPS( fTgtIPFrameRate );
      m_pTimer[VC_IP]->Wait();
   }
   m_pTimer[VC_IP]->StopTimer();
}

FILETIME CVideoChannel::GetTimeStamp ()
{
   FILETIME ft;
   //m_csTimestamp.Lock();
   ft = m_ftTimestamp;
   //m_csTimestamp.Unlock();
   return ft;
}

void CVideoChannel::OnReceiveEncodeFrame( const VC_PARAM_ENCODE& param )
{
   m_dfNormalPlayTime = param.dfNormalPlayTime;
   timeval pt = param.PresentationTime;
   //m_csTimestamp.Lock();
   TimevalToFileTime (&pt, &m_ftTimestamp);
   //m_csTimestamp.Unlock();
   
   if( m_pCBEncodeVideoFrame ) {
      CCamera_IP* pIPCamera = GetIPCameraByStreamIdx( param.nStreamIdx );
      if( m_bUseOfStreamIdxInEncodeCallBackFunc[param.nStreamIdx] ) {
         int nCamState = param.nCamState;
         if( pIPCamera )
            nCamState = pIPCamera->m_nState;

         VC_PARAM_ENCODE input;
         input.pParam           = m_pParam;
         input.nVCState         = m_nState;
         input.nCount           = m_nCountRec;
         input.nStreamIdx       = param.nStreamIdx;
         input.nCamState        = param.nCamState;
         input.nCodecID         = param.nCodecID;
         input.pCompBuff        = param.pCompBuff;
         input.nCompBuffSize    = param.nCompBuffSize;
         input.dwFlag           = param.dwFlag;
         input.PresentationTime = param.PresentationTime;
         input.dfNormalPlayTime = param.dfNormalPlayTime;
         
         m_pCBEncodeVideoFrame( input );
      }
   }
}

BOOL CVideoChannel::CallbackReceiveEncodeFrame( const VC_PARAM_ENCODE& param )
{
   CVideoChannel* pVC = (CVideoChannel*)param.pParam;
   pVC->OnReceiveEncodeFrame( param );
   return ( TRUE );
}

void CVideoChannel::OnReceiveDecodeFrame( const VC_PARAM_STREAM& param )
{
   ISize2D& prevDecodeVideoSize = m_PrevDecodeVideoSize[param.nStreamIdx];
   if( prevDecodeVideoSize.Width != param.nWidth || prevDecodeVideoSize.Height != param.nHeight ) {
      prevDecodeVideoSize( param.nWidth, param.nHeight );
   }

   if( VideoChannelType_IPCamera == m_nVideoChannelType ) {
#ifdef __SUPPORT_PTZ_CAMERA
      // PTZ 영역 처리.
      int nImgProcStreamIdx = GetStreamIdxOfProcCallbackFunc( VCProcCallbackFuncType_ImgProc );
      if( param.nStreamIdx == nImgProcStreamIdx ) {
         // 영상처리 스트림이 디코딩 되었을 때 PTZ영역들에 대한 처리를 한다.
         PTZAreaProc();
      } else {
         // 영상처리 스트림에서 PTZ영역처리를 하지 수행하지 못하였을 경우에는 메인스트림에서 처리한다.
         if( param.nStreamIdx == 0 ) {
            PTZAreaProc();
         }
      }

#ifdef __SUPPORT_VIRTUAL_PTZ_CAMERA
      // 가상 PTZ카메라 캡쳐 신호 보내기
      if( m_bSignalToVertualPTZCameras[nStreamIdx] ) {
         SignalCaptureEventToVerturalPTZCameras();
      }
#endif // __SUPPORT_VIRTUAL_PTZ_CAMERA
#endif // __SUPPORT_PTZ_CAMERA
      if( m_pCBDecodeVideoFrame ) {
         CCamera_IP* pIPCamera = GetIPCameraByStreamIdx( param.nStreamIdx );
         if( pIPCamera ) {
            if( m_bUseOfStreamIdxInDecodeCallBackFunc[param.nStreamIdx] ) {
               int& nCamState = pIPCamera->m_nState;
               VC_PARAM_STREAM input;
               input.pParam     = m_pParam;
               input.nVCState   = param.nVCState;
               input.nCount     = param.nCount;
               input.nStreamIdx = param.nStreamIdx;
               input.nCamState  = nCamState;
               input.pYUY2Buff  = param.pYUY2Buff;
               input.nWidth     = param.nWidth;
               input.nHeight    = param.nHeight;
               m_pCBDecodeVideoFrame( input );
            }
         }
      }
   }
}

BOOL CVideoChannel::CallbackReceiveDecodeFrame( const VC_PARAM_STREAM& param )
{
   CVideoChannel* pVC = (CVideoChannel*)param.pParam;
   pVC->OnReceiveDecodeFrame( param );
   return ( TRUE );
}

void CVideoChannel::CallbackCameraEvent( const VC_PARAM_CAMERA_EVENT& param )
{
   CVideoChannel* pVC = (CVideoChannel*)param.pParam;
   pVC->AddCameraEventToQueue( param );
}

void CVideoChannel::AddCameraEventToQueue( const VC_PARAM_CAMERA_EVENT& param )
{
   // [2013-11-01] jun : 콜백함수내에서 직접처리하는 경우 문제가 발생할 소지가 있어 Queue 처리를 하였다.
   CVCCameraEventCallbackParamItem* pNewItem = new CVCCameraEventCallbackParamItem;
   pNewItem->m_nStreamIdx                    = param.nStreamIdx;
   pNewItem->m_nCamEvt                       = param.nCamEvent;
   m_VCCameraEventCBParamQueue.Push( pNewItem );
   m_evtVCCameraEvent.SetEvent();
}

void CVideoChannel::Proc_Display()
{
   ISize2D s2CurrImgSize_Disp;
   ISize2D s2PrevImgSize_Disp;

   if( !m_pCBImageProcess ) return;
   m_pTimer[VC_Disp]->StartTimer();
   m_pTimer[VC_Disp]->SetFPS( m_fFPS_Disp );
   CFrameRateChecker framerate_checker;
   framerate_checker.Initialize( m_fFPS_Disp );
   framerate_checker.CheckStart();
   m_nCountDisp = 0;

   while( 1 ) {
      if( ( m_nState & VCState_SignalDeactivate ) || m_bStopThead[VC_Disp] )
         break;

      DisplayOneFrame_DispProc( s2CurrImgSize_Disp, s2PrevImgSize_Disp );
      TimerCheckerManager::Get().DebugMsg( "Display", 0 );

      m_pTimer[VC_Disp]->SetFPS( m_fFPS_Disp );
      m_pTimer[VC_Disp]->Wait();
   }
   m_pTimer[VC_Disp]->StopTimer();

   if( m_DisplayingImageQueue.IsInitialized() )
      m_DisplayingImageQueue.Close();
}

void CVideoChannel::DisplayOneFrame_CaptureProc( CCamera_IP* pIPCamera_Disp, ISize2D& s2CurrImgSize_Disp, int& nCurrDispStreamIndex, ISize2D& s2PrevImgSize_Disp, int& nPrevDispStreamIndex )
{
   int nMaxDelayQueueSize = int( ( m_fFPS_Disp / m_fFPS_IP ) * 1.5 );

   // 화면 출력 콜백함수 호출
   if( ( m_nVideoSignalFlag && m_pCBDisplay && ( m_nCameraNum > 0 ) ) || m_bSignalToExecProcCallbackFunc[VCProcCallbackFuncType_Display] ) {
      // MS: 아이피 카메라의 경우에는 별도의 연결&디코딩 스레드가 동작되고 있으므로 별도의 캡쳐 스레드가 필요하지 않다.
      //     따라서 각 아이피 카메라의 디코딩 스레드에서 화면 출력 콜백 함수를 호출하는 것이 맞다고 본다.
      if( VideoChannelType_IPCamera == m_nVideoChannelType ) {
         if( pIPCamera_Disp && ( m_bSignalToExecProcCallbackFunc[VCProcCallbackFuncType_Display] || ( pIPCamera_Disp->IsVideoSignalAlive() && !( pIPCamera_Disp->m_nState & ICamera::State_ChangingVideoBuffer ) ) ) ) {
            int nCamState             = pIPCamera_Disp->m_nState;
            byte* pYUY2Buff           = (byte*)pIPCamera_Disp->m_SrcImg;
            s2CurrImgSize_Disp.Width  = pIPCamera_Disp->m_pInfo->m_nWidth;
            s2CurrImgSize_Disp.Height = pIPCamera_Disp->m_pInfo->m_nHeight;
            if( s2CurrImgSize_Disp.Width != s2PrevImgSize_Disp.Width || s2CurrImgSize_Disp.Height != s2PrevImgSize_Disp.Height ) {
               nCamState |= ICamera::State_ResolutionChanged;
               m_nCountCap = 0;
               if( nPrevDispStreamIndex != nCurrDispStreamIndex ) {
                  if( DetermineCopyingPrevStreamBuffToCurrStreamBuff( nPrevDispStreamIndex, nCurrDispStreamIndex ) )
                     CopyPrevStreamBuffToCurrStreamBuff( nPrevDispStreamIndex, nCurrDispStreamIndex );
               }
            }
            if( pIPCamera_Disp->m_SrcImg.Length == s2CurrImgSize_Disp.Width * s2CurrImgSize_Disp.Height * 2 ) {
               if( m_bSyncDispWithIP ) {
                  if( m_DisplayingImageQueue.HasChanged( nMaxDelayQueueSize, s2CurrImgSize_Disp ) ) {
                     pIPCamera_Disp->GetImage_Lock( &pYUY2Buff );
                     m_DisplayingImageQueue.Initialize( m_fFPS_Disp, nMaxDelayQueueSize, s2CurrImgSize_Disp, pYUY2Buff );
                     pIPCamera_Disp->GetImage_Unlock();
                  }
                  CCompressImageBufferItem* pDelayedBuffItem = m_DisplayingImageQueue.GetDelayedBuffer( pIPCamera_Disp->m_dfNormalPlayTime, 0.5 / m_fFPS_IP + m_dfAvgImageProcessDelayTime );
                  VC_PARAM_STREAM input;
                  input.pParam     = m_pParam;
                  input.nVCState   = m_nState;
                  input.nCount     = m_nCountDisp;
                  input.nStreamIdx = nCurrDispStreamIndex;
                  input.nCamState  = nCamState;
                  input.pYUY2Buff  = (byte*)pDelayedBuffItem->m_CompBuff;
                  input.nWidth     = s2CurrImgSize_Disp.Width;
                  input.nHeight    = s2CurrImgSize_Disp.Height;
                  m_pCBDisplay( input );
                  CCompressImageBufferItem* pLastBuffItem = m_DisplayingImageQueue.GetLastBuffer();
                  m_csSrcImage.Lock();
                  if( pLastBuffItem && (byte*)m_YUY2Buff ) {
                     pIPCamera_Disp->GetImage_Lock( &pYUY2Buff );
                     memcpy( (byte*)pLastBuffItem->m_CompBuff, pYUY2Buff, pLastBuffItem->m_CompBuff.Length );
                     pLastBuffItem->m_dfNormalPlayTime = pIPCamera_Disp->m_dfNormalPlayTime;
                     pIPCamera_Disp->GetImage_Unlock();
                  }
                  m_csSrcImage.Unlock();
               } else {
                  VC_PARAM_STREAM input;
                  input.pParam     = m_pParam;
                  input.nVCState   = m_nState;
                  input.nCount     = m_nCountCap;
                  input.nStreamIdx = nCurrDispStreamIndex;
                  input.nCamState  = nCamState;
                  input.pYUY2Buff  = pYUY2Buff;
                  input.nWidth     = s2CurrImgSize_Disp.Width;
                  input.nHeight    = s2CurrImgSize_Disp.Height;
                  m_pCBDisplay( input );

                  if( m_DisplayingImageQueue.IsInitialized() )
                     m_DisplayingImageQueue.Close();
               }
               m_nCountCap++;
            }
         }
      } else {
         if( m_nWidth != 0 ) {
            if( s2CurrImgSize_Disp.Width != s2PrevImgSize_Disp.Width || s2CurrImgSize_Disp.Height != s2PrevImgSize_Disp.Height ) {
               m_nCountCap = 0;
            }
            m_nCountCap++;
         }
      }
      if( m_bSignalToExecProcCallbackFunc[VCProcCallbackFuncType_Display] )
         m_bSignalToExecProcCallbackFunc[VCProcCallbackFuncType_Display] = FALSE;
   }

   s2PrevImgSize_Disp   = s2CurrImgSize_Disp;
   nPrevDispStreamIndex = nCurrDispStreamIndex;
}

void CVideoChannel::DisplayOneFrame_DispProc( ISize2D& s2CurrImgSize_Disp, ISize2D& s2PrevImgSize_Disp )
{
   int nMaxDelayQueueSize = int( ( m_fFPS_Disp / m_fFPS_IP ) * 1.5 );

   if( ( m_nState & VCState_Activate ) && m_pCBDisplay && m_nCameraNum > 0 && m_nVideoSignalFlag ) {
      if( VideoChannelType_IPCamera != m_nVideoChannelType ) {
         if( ( m_nCountCap >= 1 ) && ( m_nWidth != 0 ) ) {
            INT nStreamIndex          = 0;
            int nCamState             = 0;
            s2CurrImgSize_Disp.Width  = m_nWidth;
            s2CurrImgSize_Disp.Height = m_nHeight;
            if( s2CurrImgSize_Disp.Width != s2PrevImgSize_Disp.Width || s2CurrImgSize_Disp.Height != s2PrevImgSize_Disp.Height ) {
               nCamState |= ICamera::State_ResolutionChanged;
               m_nCountDisp = 0;
            }
            if( (byte*)m_YUY2Buff && m_YUY2Buff.Length > 0 ) {
               if( m_bSyncDispWithIP ) {
                  if( m_DisplayingImageQueue.HasChanged( nMaxDelayQueueSize, ISize2D( m_nWidth, m_nHeight ) ) ) {
                     m_DisplayingImageQueue.Initialize( m_fFPS_Disp, nMaxDelayQueueSize, ISize2D( m_nWidth, m_nHeight ), (byte*)m_YUY2Buff );
                  }
                  CCompressImageBufferItem* pDelayedBuffItem = m_DisplayingImageQueue.GetDelayedBuffer( m_dfNormalPlayTime, 0.75 / m_fFPS_IP + m_dfAvgImageProcessDelayTime );
                  VC_PARAM_STREAM input;
                  input.pParam     = m_pParam;
                  input.nVCState   = m_nState;
                  input.nCount     = m_nCountDisp;
                  input.nStreamIdx = nStreamIndex;
                  input.nCamState  = nCamState;
                  input.pYUY2Buff  = (byte*)pDelayedBuffItem->m_CompBuff;
                  input.nWidth     = m_nWidth;
                  input.nHeight    = m_nHeight;
                  m_pCBDisplay( input );
                  CCompressImageBufferItem* pLastBuffItem = m_DisplayingImageQueue.GetLastBuffer();
                  m_csSrcImage.Lock();
                  if( pLastBuffItem && (byte*)m_YUY2Buff && m_YUY2Buff.Length > 0 ) {
                     pLastBuffItem->m_CompBuff         = m_YUY2Buff;
                     pLastBuffItem->m_dfNormalPlayTime = m_dfNormalPlayTime;
                  }
                  m_csSrcImage.Unlock();
               } else {
                  VC_PARAM_STREAM input;
                  input.pParam     = m_pParam;
                  input.nVCState   = m_nState;
                  input.nCount     = m_nCountDisp;
                  input.nStreamIdx = nStreamIndex;
                  input.nCamState  = nCamState;
                  input.pYUY2Buff  = (byte*)m_YUY2Buff;
                  input.nWidth     = m_nWidth;
                  input.nHeight    = m_nHeight;
                  m_pCBDisplay( input );
                  if( m_DisplayingImageQueue.IsInitialized() )
                     m_DisplayingImageQueue.Close();
               }
               m_nCountDisp++;
            }
         }
      }
   }
   s2PrevImgSize_Disp = s2CurrImgSize_Disp;
}

UINT CVideoChannel::Thread_Capture( LPVOID pVoid )
{
   CVideoChannel* pVideoChannel = (CVideoChannel*)pVoid;
#ifdef SUPPORT_PTHREAD_NAME
   char thread_name[16] = {};
   sprintf( thread_name, "VideoCh_%d", pVideoChannel->m_nChannelNo_IVX );
   pthread_setname_np( pthread_self(), thread_name );
#endif

   logd( "[CH:%d]    Thread_Capture - Start\n", pVideoChannel->m_nChannelNo_IVX + 1 );
   pVideoChannel->m_csCurExecThreadFlag.Lock();
   pVideoChannel->m_nCurExecThreadFlag |= VCThreadType_Cap;
   pVideoChannel->m_csCurExecThreadFlag.Unlock();
   pVideoChannel->m_bThreadAlive[VC_Cap] = TRUE;
   pVideoChannel->Proc_Capture();
   logd( "[CH:%d]    Thread_Capture - End\n", pVideoChannel->m_nChannelNo_IVX + 1 );
   pVideoChannel->m_bThreadAlive[VC_Cap] = FALSE;
   pVideoChannel->m_csCurExecThreadFlag.Lock();
   pVideoChannel->m_nCurExecThreadFlag &= ~VCThreadType_Cap;
   pVideoChannel->m_csCurExecThreadFlag.Unlock();
   return DONE;
}

UINT CVideoChannel::Thread_Display( LPVOID pVoid )
{
   CVideoChannel* pVideoChannel = (CVideoChannel*)pVoid;
   logd( "[CH:%d]    Thread_Display - Start\n", pVideoChannel->m_nChannelNo_IVX + 1 );
   pVideoChannel->m_csCurExecThreadFlag.Lock();
   pVideoChannel->m_nCurExecThreadFlag |= VCThreadType_Disp;
   pVideoChannel->m_csCurExecThreadFlag.Unlock();
   pVideoChannel->m_bThreadAlive[VC_Disp] = TRUE;
   pVideoChannel->Proc_Display();
   logd( "[CH:%d]    Thread_Display - End\n", pVideoChannel->m_nChannelNo_IVX + 1 );
   pVideoChannel->m_bThreadAlive[VC_Disp] = FALSE;
   pVideoChannel->m_csCurExecThreadFlag.Lock();
   pVideoChannel->m_nCurExecThreadFlag &= ~VCThreadType_Disp;
   pVideoChannel->m_csCurExecThreadFlag.Unlock();
   return DONE;
}

UINT CVideoChannel::Thread_Record( LPVOID pVoid )
{
   CVideoChannel* pVideoChannel = (CVideoChannel*)pVoid;
   //logd ("[CH:%d]    Thread_Record[%d] - Start\n", pVideoChannel->m_nChannelNo_IVX+1, pVideoChannel->m_hVCThread[VC_Rec]);
   pVideoChannel->m_csCurExecThreadFlag.Lock();
   pVideoChannel->m_nCurExecThreadFlag |= VCThreadType_Rec;
   pVideoChannel->m_csCurExecThreadFlag.Unlock();
   pVideoChannel->m_bThreadAlive[VC_Rec] = TRUE;
   pVideoChannel->Proc_Record();
   //logd ("[CH:%d]    Thread_Record[%d] - End\n", pVideoChannel->m_nChannelNo_IVX+1, pVideoChannel->m_hVCThread[VC_Rec]);
   pVideoChannel->m_bThreadAlive[VC_Rec] = FALSE;
   pVideoChannel->m_csCurExecThreadFlag.Lock();
   pVideoChannel->m_nCurExecThreadFlag &= ~VCThreadType_Rec;
   pVideoChannel->m_csCurExecThreadFlag.Unlock();
   return DONE;
}

UINT CVideoChannel::Thread_ImgProcess( LPVOID pVoid )
{
   CVideoChannel* pVideoChannel = (CVideoChannel*)pVoid;
   //logd ("[CH:%d]    Thread_ImgProcess[%d] - Start\n", pVideoChannel->m_nChannelNo_IVX+1, pVideoChannel->m_hVCThread[VC_IP]);
   pVideoChannel->m_csCurExecThreadFlag.Lock();
   pVideoChannel->m_nCurExecThreadFlag |= VCThreadType_IP;
   pVideoChannel->m_csCurExecThreadFlag.Unlock();
   pVideoChannel->m_bThreadAlive[VC_IP] = TRUE;
   pVideoChannel->Proc_ImgProcess();
   //logd ("[CH:%d]    Thread_ImgProcess[%d] - End\n", pVideoChannel->m_nChannelNo_IVX+1, pVideoChannel->m_hVCThread[VC_IP]);
   pVideoChannel->m_bThreadAlive[VC_IP] = FALSE;
   pVideoChannel->m_csCurExecThreadFlag.Lock();
   pVideoChannel->m_nCurExecThreadFlag &= ~VCThreadType_IP;
   pVideoChannel->m_csCurExecThreadFlag.Unlock();
   return DONE;
}

int CVideoChannel::SetDefaultImageProperty( int nCamIdx, int nImagePropertyType )
{
   if( !( m_nState & VCState_Activate ) || ( m_nState & VCState_Deactivating ) ) return ( -1 );

   int nCamNum = (int)m_Cameras.size();
   if( 0 <= nCamIdx && nCamIdx < nCamNum ) {
      return m_Cameras[nCamIdx]->SetDefaultImageProperty( nImagePropertyType );
   }
   return ( 1 );
}

int CVideoChannel::SetAllImageProperty( int nCamIdx )
{
   if( !( m_nState & VCState_Activate ) || ( m_nState & VCState_Deactivating ) ) return ( -1 );

   int nCamNum = (int)m_Cameras.size();
   if( 0 <= nCamIdx && nCamIdx < nCamNum ) {
      return m_Cameras[nCamIdx]->SetAllImageProperty();
   }
   return ( 1 );
}

int CVideoChannel::SetImageProperty( int nCamIdx, int nImagePropertyType, int nValue, int nFlagAuto )
{
   if( !( m_nState & VCState_Activate ) || ( m_nState & VCState_Deactivating ) ) return ( -1 );

   int nCamNum = (int)m_Cameras.size();
   if( 0 <= nCamIdx && nCamIdx < nCamNum ) {
      return m_Cameras[nCamIdx]->SetImageProperty( nImagePropertyType, nValue, nFlagAuto );
   }
   return ( 1 );
}

int CVideoChannel::GetImageProperty( int nCamIdx, int nImagePropertyType, int& nValue, int& nFlagAuto )
{
   if( !( m_nState & VCState_Activate ) || ( m_nState & VCState_Deactivating ) ) return ( -1 );

   nFlagAuto   = FALSE;
   int nCamNum = (int)m_Cameras.size();
   if( 0 <= nCamIdx && nCamIdx < nCamNum )
      return m_Cameras[nCamIdx]->GetImageProperty( nImagePropertyType, nValue, nFlagAuto );
   return ( 1 );
}

ImageProperty* CVideoChannel::GetImageProperties( int nCamIdx )
{
   if( !( m_nState & VCState_Activate ) || ( m_nState & VCState_Deactivating ) ) return ( NULL );

   ImageProperty* pImageProperty = NULL;
   ICameraInfo* pICamInfo        = NULL;
   switch( m_nVideoChannelType ) {
   case VideoChannelType_IPCamera:
      pICamInfo = &m_IPCamInfo;
      break;
   case VideoChannelType_Panorama:
      pICamInfo = m_PanoramaCamInfo.m_InputCamInfos[nCamIdx];
      break;
   case VideoChannelType_WebCam:
   case VideoChannelType_VideoFile:
      break;
   default:
      break;
   }
   if( pICamInfo ) {
      if( pICamInfo->m_nCameraType == CameraType_AnalogCamera ) {
         CAnalogCameraInfo* pAnalogCamInfo = (CAnalogCameraInfo*)pICamInfo;
         CaptureBoardProperty* pCBProperty = GetCaptureBoardProporty( pAnalogCamInfo->m_nCaptureBoardType );
         if( pCBProperty )
            pImageProperty = pCBProperty->m_ImageProperty;
      } else if( pICamInfo->m_nCameraType == CameraType_IPCamera ) {
         CIPCameraInfo* pIPCamInfo     = (CIPCameraInfo*)pICamInfo;
         IPCameraProperty* pIPProperty = GetIPCameraProperty( pIPCamInfo->m_nIPCameraType );
         if( pIPProperty )
            pImageProperty = pIPProperty->m_ImageProperty;
      }
   }
   return pImageProperty;
}

void CVideoChannel::GetDefaultImageProperty( int nCamIdx, int nImagePropertyType, int& nValue )
{
   nValue                        = -1;
   ImageProperty* pImageProperty = GetImageProperties( nCamIdx );
   if( pImageProperty == NULL )
      return;
   nValue = pImageProperty[nImagePropertyType].m_nDefaultValue;
}

void CVideoChannel::GetImagePropertyRange( int nCamIdx, int nImagePropertyType, int& nMin, int& nMax, int& nTic )
{
   ImageProperty* pImageProperty = GetImageProperties( nCamIdx );
   if( pImageProperty == NULL )
      return;
   pImageProperty[nImagePropertyType].GetRange( nMin, nMax, nTic );
}

void CVideoChannel::GetSupportedImagePropertyList( int nCamIdx, std::vector<int>& vtImagePropertyType )
{
   ImageProperty* pImageProperty = GetImageProperties( nCamIdx );
   if( pImageProperty == NULL )
      return;

   int i;
   vtImagePropertyType.clear();
   for( i = 0; i < ImagePropertyTypeNum; i++ ) {
      if( pImageProperty[i].m_bUse )
         vtImagePropertyType.push_back( i );
   }
}

void CVideoChannel::GetInputCameraDesc( int nCamIdx, std::string& strDesc )
{
   if( !( m_nState & VCState_Activate ) || ( m_nState & VCState_Deactivating ) ) return;

   int nCamNum = (int)m_Cameras.size();
   if( 0 <= nCamIdx && nCamIdx < nCamNum )
      m_Cameras[nCamIdx]->GetCameraDesc( strDesc );
}

UINT64 CVideoChannel::CheckVideoSignalChange()
{
   return ( m_nPrevVideoSignalFlag != m_nVideoSignalFlag );
}

BOOL CVideoChannel::IsICantekVideoServer()
{
   if( m_nVideoChannelType == VideoChannelType_IPCamera && m_IPCamInfo.m_nIPCameraType == IPCameraType_ICanServer )
      return TRUE;
   else
      return FALSE;
}

uint CVideoChannel::GetSensorSignalFlags()
{
   if( !( m_nState & VCState_Activate ) || ( m_nState & ( VCState_SignalDeactivate | VCState_Deactivating ) ) ) return ( 0 );
   m_csCameras.Lock();
   int nSensorSignalFlags = 0;
   switch( m_nVideoChannelType ) {
   case VideoChannelType_WebCam:
   case VideoChannelType_VideoFile:
   case VideoChannelType_IPCamera:
      nSensorSignalFlags = m_Cameras[0]->GetAlarmInputFlags();
   case VideoChannelType_Panorama:
      break;
   }
   m_csCameras.Unlock();
   return nSensorSignalFlags;
}

void CVideoChannel::SetCenterImagePos( IPoint2D p2CenterPos )
{
   if( !( m_nState & VCState_Activate ) || ( m_nState & ( VCState_SignalDeactivate | VCState_Deactivating ) ) ) return;
#ifdef __SUPPORT_PTZ_CAMERA
#ifdef __SUPPORT_VIRTUAL_PTZ_CAMERA
   if( m_nVideoChannelType == VideoChannelType_VirtualPTZ ) {
      m_csCameras.lock();
      if( m_Cameras.size() > 0 ) {
         CCamera_VirtualPTZ* pVirtualPTZCamera = (CCamera_VirtualPTZ*)m_Cameras[0];
         pVirtualPTZCamera->m_p2CenterPos      = p2CenterPos;
      }
      m_csCameras.unlock();
   }
#endif // __SUPPORT_VIRTUAL_PTZ_CAMERA
#endif // __SUPPORT_PTZ_CAMERA
}

#ifdef __SUPPORT_IMAGE_ENHANCEMENT

void CVideoChannel::SetDefog( BOOL bDefogOn )
{
#ifdef __SUPPORT_PTZ_CAMERA
   PTZControlProperty* pPTZCtrlPropoerty = GetPTZControlProperty( GetPTZControlTypeOfPTZCmd() );
   if( m_bPTZCamera && pPTZCtrlPropoerty ) {
      if( PTZCtrlCap_PTZDefogSupported & pPTZCtrlPropoerty->m_nCapabilities ) {
         if( m_pPTZCtrl ) {
            m_pPTZCtrl->SetDefog( bDefogOn );
         }
      } else {
         m_bUseDefog = bDefogOn;
      }
   } else
#endif //__SUPPORT_PTZ_CAMERA
   {
      m_bUseDefog = bDefogOn;
   }
}

void CVideoChannel::SetSoftDefog( BOOL bDefogOn )
{
   PTZControlProperty* pPTZCtrlPropoerty = GetPTZControlProperty( GetPTZControlTypeOfPTZCmd() );
   if( m_bPTZCamera && pPTZCtrlPropoerty ) {
      if( PTZCtrlCap_PTZDefogSupported & pPTZCtrlPropoerty->m_nCapabilities ) {
         if( m_pPTZCtrl ) {
            m_pPTZCtrl->SetSoftDefog( bDefogOn );
         }
      } else {
         m_bUseDefog = bDefogOn;
      }
   } else {
      m_bUseDefog = bDefogOn;
   }
}

BOOL CVideoChannel::GetDefog()
{
#ifdef __SUPPORT_PTZ_CAMERA
   PTZControlProperty* pPTZCtrlProperty = GetPTZControlProperty( GetPTZControlTypeOfPTZCmd() );
   if( m_bPTZCamera && pPTZCtrlProperty ) {
      if( PTZCtrlCap_PTZDefogSupported & pPTZCtrlProperty->m_nCapabilities ) {
         if( m_pPTZCtrl ) {
            return m_pPTZCtrl->m_PTZInfo.m_bDefogOn;
         }
      } else {
         return m_bUseDefog;
      }
   } else
#endif
   {
      return m_bUseDefog;
   }
   return FALSE;
}

void CVideoChannel::SetDefogLevel( int nDefogLevel )
{
#ifdef __SUPPORT_PTZ_CAMERA
   PTZControlProperty* pPTZCtrlPropoerty = GetPTZControlProperty( GetPTZControlTypeOfPTZCmd() );
   if( pPTZCtrlPropoerty ) {
      if( PTZCtrlCap_PTZDefogSupported & pPTZCtrlPropoerty->m_nCapabilities ) {
         if( m_pPTZCtrl ) {
            m_pPTZCtrl->SetDefog( nDefogLevel );
         }
      } else {
         m_nDefogLevel = nDefogLevel;
      }
   } else
#endif
   {
      m_nDefogLevel = nDefogLevel;
   }
}

void CVideoChannel::SetVideoStabilization( BOOL bVideoStabilizationOn )
{
#ifdef __SUPPORT_PTZ_CAMERA
   PTZControlProperty* pPTZCtrlPropoerty = GetPTZControlProperty( GetPTZControlTypeOfPTZCmd() );
   if( pPTZCtrlPropoerty ) {
      if( PTZCtrlCap_PTZVideoStabilizationSupported & pPTZCtrlPropoerty->m_nCapabilities ) {
         if( m_pPTZCtrl ) {
            m_pPTZCtrl->SetVideoStabilizer( bVideoStabilizationOn );
         }
      } else {
         m_bUseVideoStabilization = bVideoStabilizationOn;
      }
   } else
#endif
   {
      m_bUseVideoStabilization = bVideoStabilizationOn;
   }
}

BOOL CVideoChannel::GetVideoStabilization()
{
#ifdef __SUPPORT_PTZ_CAMERA
   PTZControlProperty* pPTZCtrlPropoerty = GetPTZControlProperty( GetPTZControlTypeOfPTZCmd() );
   if( pPTZCtrlPropoerty ) {
      if( PTZCtrlCap_PTZVideoStabilizationSupported & pPTZCtrlPropoerty->m_nCapabilities ) {
         if( m_pPTZCtrl ) {
            return m_pPTZCtrl->m_PTZInfo.m_bVideoStabilization;
         }
      } else {
         return m_bUseVideoStabilization;
      }
   } else
#endif
   {
      return m_bUseVideoStabilization;
   }
   return FALSE;
}
#endif // __SUPPORT_IMAGE_ENHANCEMENT

int CVideoChannel::StopTalking()
{
   if( !( m_nState & VCState_Activate ) || ( m_nState & ( VCState_SignalDeactivate | VCState_Deactivating ) ) ) return ( -1 );
   if( m_nVideoChannelType == VideoChannelType_IPCamera ) {
      if( m_nState & VCState_Talking ) {
         m_Cameras[0]->StopTalking();
         m_nState &= ~( VCState_Talking );
      }
   }
   return ( DONE );
}

int CVideoChannel::StartTalking()
{
   if( !( m_nState & VCState_Activate ) || ( m_nState & ( VCState_SignalDeactivate | VCState_Deactivating ) ) ) return ( -1 );
   if( m_nVideoChannelType == VideoChannelType_IPCamera ) {
      if( DONE == m_Cameras[0]->StartTalking() ) {
         m_nState |= VCState_Talking;
      }
   }
   return ( DONE );
}

int CVideoChannel::IsTaking()
{
   if( !( m_nState & VCState_Activate ) || ( m_nState & ( VCState_SignalDeactivate | VCState_Deactivating ) ) ) return ( FALSE );
   if( m_nVideoChannelType == VideoChannelType_IPCamera ) {
      if( m_nState & VCState_Talking ) {
         return ( TRUE );
      }
      return ( FALSE );
   }
   return ( FALSE );
}

int CVideoChannel::IsSupportAudioDataTransmit()
{
   if( m_nVideoChannelType == VideoChannelType_IPCamera ) {
      IPCameraProperty* pIPCamProperty = GetIPCameraProperty( m_IPCamInfo.m_nIPCameraType );
      if( pIPCamProperty ) {
         if( pIPCamProperty->m_nCapabilities & IPCamCaps_SupportAudioDataTransmit )
            return ( TRUE );
      }
   }
   return ( FALSE );
}

int CVideoChannel::IsEnableAudioDataTransmit()
{
   // 주의: ICamera 객체 타입으로 체크하는 함수이기 때문에 반드시 로컬 시스템에서만 사용해야 한다.
   if( !( m_nState & VCState_Activate ) || ( m_nState & ( VCState_SignalDeactivate | VCState_Deactivating ) ) )
      return ( FALSE );
   if( m_nVideoChannelType == VideoChannelType_IPCamera ) {
      CCriticalSectionSP co( m_csCameras );
      if( m_Cameras.size() > 0 ) {
         return m_Cameras[0]->IsEnableAudioDataTransmit();
      }
   }
   return ( FALSE );
}

BOOL CVideoChannel::IsSupportDirectVideoStreaming( int nStreamIdx )
{
   BOOL bRet = FALSE;

   switch( m_nVideoChannelType ) {
   case VideoChannelType_IPCamera:
      ASSERT( 0 <= nStreamIdx && nStreamIdx < MAX_IP_CAMERA_STREAM_NUM );
      if( 0 <= nStreamIdx && nStreamIdx < MAX_IP_CAMERA_STREAM_NUM ) {
         CIPCameraInfo* pIPCamInfo           = &m_IPCamInfoArr[nStreamIdx];
         IPCameraProperty* pIPCameraProperty = GetIPCameraProperty( pIPCamInfo->m_nIPCameraType );
         if( pIPCameraProperty ) {
            if( ( pIPCameraProperty->m_nCapabilities & IPCamCaps_SurpportDirectVideoStreaming ) ) {
               bRet = TRUE;
            }
         }
      }
   }
   if( m_nVideoChannelType == VideoChannelType_VideoFile ) {
      //return TRUE;
   }

   return bRet;
}

BOOL CVideoChannel::IsIVXVideoServer()
{
   if( m_nVideoChannelType == VideoChannelType_IPCamera ) {
      if( m_IPCamInfo.m_nIPCameraType == IPCameraType_IVXVideoServer )
         return TRUE;
   }
   return FALSE;
}

BOOL CVideoChannel::IsVideoServerType( int nVideoServerType )
{
   if( m_nVideoChannelType == VideoChannelType_IPCamera ) {
      if( m_IPCamInfo.m_nIPCameraType == nVideoServerType )
         return TRUE;
   }
   return FALSE;
}

BOOL CVideoChannel::IsVideoAnalyticsVideoServer()
{
   if( m_nVideoChannelType == VideoChannelType_IPCamera ) {
      IPCameraProperty* pIPCameraProperty = GetIPCameraProperty( m_IPCamInfo.m_nIPCameraType );
      if( pIPCameraProperty ) {
         if( pIPCameraProperty->m_nCapabilities & IPCamCaps_VideoAnalyticsVideoServer )
            return TRUE;
         else
            return FALSE;
      }
   }
   return FALSE;
}

BOOL CVideoChannel::IsSupportSendEventAlarmToVideoServer()
{
   // [20160304FRI:SGLEE]
   // 모든 카메라가 비디오 서버로 알람 전송 가능하도록 설정
   return TRUE;

   //if (m_nVideoChannelType == VideoChannelType_IPCamera) {
   //   IPCameraProperty *pIPCameraProperty = GetIPCameraProperty (m_IPCamInfo.m_nIPCameraType);
   //   if (pIPCameraProperty && (pIPCameraProperty->m_nCapabilities & IPCamCaps_SupportSendEventAlarmMessage)) {
   //      return TRUE;
   //   }
   //}
   //return FALSE;
}

#ifdef __SUPPORT_PTZ_CAMERA

void CVideoChannel::PTZAreaProc()
{
   if( m_bPTZCamera && m_pPTZCtrl ) {
      UpdatePTZAreaCoordinate();
      if( m_bDrawPrivacyZone )
         DrawPrivacyZone();
      CheckMutualAssistanceAreaProc( m_b2CurTrkBox, m_s2ProcImgSize );
      CheckFloodLightAreaProc();
   }
}

void CVideoChannel::UpdatePTZAreaCoordinate()
{
#ifdef __SUPPORT_PTZ_AREA
   // 현재 PTZ 위치에 대응되는 폴리곤 영역좌표를 업데이트한다.
   if( m_bPTZCamera && m_pPTZCtrl && m_pPTZCtrl->IsPTZConnected() ) {
      m_csPTZAreas.lock();
      int k;
      int swh = REF_IMAGE_WIDTH / 2;
      int shh = REF_IMAGE_HEIGHT / 2;
      ISize2D s2Src( REF_IMAGE_WIDTH, REF_IMAGE_HEIGHT );
      for( k = 0; k < PTZAreaTypeNum; k++ ) {
         PTZPolygonalAreaList* pPTZAreas = &m_PTZAreas[k];

         PTZPolygonalArea* pPTZPolygonalArea = (PTZPolygonalArea*)pPTZAreas->First();
         while( pPTZPolygonalArea ) {
            int i;
            int nVerticeNum = pPTZPolygonalArea->Vertices.Length;
            BOOL bDrawArea  = TRUE;
            for( i = 0; i < nVerticeNum; i++ ) {
               IPoint2D& ptVertice = pPTZPolygonalArea->Vertices[i];
               FPoint3D& pvPTZPos  = pPTZPolygonalArea->PTZPositions[i];
               FPTZVector pvPos;
               pvPos.P = pvPTZPos.X;
               pvPos.T = pvPTZPos.Y;
               pvPos.Z = pvPTZPos.Z;
               FPoint2D p2ImgPos;
               m_pPTZCtrl->GetImagePnt( pvPos, s2Src, p2ImgPos );
               ptVertice.X = int( p2ImgPos.X + swh );
               ptVertice.Y = int( p2ImgPos.Y + shh );
               if( ptVertice.X < -10e7 || ptVertice.X > 10e7 || ptVertice.Y < -10e7 || ptVertice.Y > 10e7 ) {
                  bDrawArea = FALSE;
                  break;
               }
            }

            if( FALSE == bDrawArea ) {
               for( i = 0; i < nVerticeNum; i++ ) {
                  IPoint2D& ptVertice = pPTZPolygonalArea->Vertices[i];
                  ptVertice.X = ptVertice.Y = 0xFFFFFFFF;
               }
            }
            pPTZPolygonalArea = (PTZPolygonalArea*)pPTZAreas->Next( pPTZPolygonalArea );
         }
      }
      m_csPTZAreas.unlock();
   }
#endif
}

#endif //__SUPPORT_PTZ_CAMERA

void CVideoChannel::DrawPrivacyZone()
{
#ifdef __SUPPORT_PTZ_AREA
   PTZPolygonalAreaList* pPTZAreas = &m_PTZAreas[PTZAreaType_PrivacyZone];

   FPoint2D p2Scale( float( m_nWidth ) / pPTZAreas->Width, float( m_nHeight ) / pPTZAreas->Height );
   PTZPolygonalArea* pPTZPolygonalArea = (PTZPolygonalArea*)pPTZAreas->First();
   while( pPTZPolygonalArea ) {
      BCCL::Polygon* polygon = (BCCL::Polygon*)pPTZPolygonalArea;
      polygon->Fill( 0x8000, p2Scale, m_pYUY2Buff, m_nWidth, m_nHeight );
      pPTZPolygonalArea = (PTZPolygonalArea*)pPTZAreas->Next( pPTZPolygonalArea );
   }
#endif
}

void CVideoChannel::GetTrackingAreaMap( GImage& trackingAreaMap, GImage& dynamicBackgroundAreaMap, BOOL bAdditionalAreas )
{
   if( m_bPTZCamera ) {
      if( FALSE == bAdditionalAreas ) {
         trackingAreaMap.Clear();
         dynamicBackgroundAreaMap.Clear();
      }
#ifdef __SUPPORT_PTZ_AREA
      PTZPolygonalAreaList* pTrackingAreas;

      m_csPTZAreas.lock();

      // Obtaining TrkAreaMap
      pTrackingAreas = &m_PTZAreas[PTZAreaType_TrackingArea];
      FPoint2D p2Scale( float( trackingAreaMap.Width ) / pTrackingAreas->Width, float( trackingAreaMap.Height ) / pTrackingAreas->Height );
      if( pTrackingAreas->GetNumItems() ) {
         if( FALSE == bAdditionalAreas ) trackingAreaMap.Clear();
         pTrackingAreas->RenderAreaMap( trackingAreaMap, p2Scale, PG_WHITE );
      } else {
         if( FALSE == bAdditionalAreas )
            trackingAreaMap.Set( 0, 0, trackingAreaMap.Width, trackingAreaMap.Height, PG_WHITE );
      }

      // Obtaining NonTrackingAreaMap
      pTrackingAreas = &m_PTZAreas[PTZAreaType_NonTrackingArea];
      if( pTrackingAreas->GetNumItems() ) pTrackingAreas->RenderAreaMap( trackingAreaMap, p2Scale, 128 );

      // Obtaining DynBkgAreaMap
      pTrackingAreas = &m_PTZAreas[PTZAreaType_DynamicBackgroundArea];
      if( pTrackingAreas->GetNumItems() ) pTrackingAreas->RenderAreaMap( dynamicBackgroundAreaMap, p2Scale, PG_WHITE );

      m_csPTZAreas.unlock();
#endif
   }
}

void CVideoChannel::SetTrkObjBox( FBox2D b2TrkBox, ISize2D s2ProcImgSize )
{
   m_b2CurTrkBox   = b2TrkBox;
   m_s2ProcImgSize = s2ProcImgSize;
}

void CVideoChannel::CheckMutualAssistanceAreaProc( FBox2D b2TrkBox, ISize2D s2SrcImgSize )
{
#ifdef __SUPPORT_PTZ_CAMERA
#ifdef __SUPPORT_PTZ_AREA
   BOOL bFindArea = FALSE;

   if( b2TrkBox.Width != 0.0f ) {
      m_csPTZAreas.lock();

      PTZPolygonalAreaList* pMAAreas = &m_PTZAreas[PTZAreaType_MutualAsistanceArea];
      FPoint2D p2Scale( float( pMAAreas->Width ) / s2SrcImgSize.Width, float( pMAAreas->Height ) / s2SrcImgSize.Height );
      PTZPolygonalArea* pPTZPolygonalArea = (PTZPolygonalArea*)pMAAreas->First();
      while( pPTZPolygonalArea ) {
         FPoint2D fp2TrkPos = GetCenterPos( b2TrkBox );
         fp2TrkPos.X *= p2Scale.X;
         fp2TrkPos.Y *= p2Scale.Y;
         // 물체의 중심이 포함되었고...
         if( pPTZPolygonalArea->IsPointInsidePolygon( fp2TrkPos ) ) {
            // 물체의 길이가 지정된 길이보다 작으면...
            float fCurZoom = 1.0f;
            if( m_bPTZCamera && m_pPTZCtrl && m_pPTZCtrl->IsPTZConnected() ) {
               float fCurZoom = m_pPTZCtrl->GetAbsPos().Z;
            }
            float fMinObjectLength = pPTZPolygonalArea->MinObjectLength * fCurZoom;
            float fCurObjLen       = ( b2TrkBox.Width > b2TrkBox.Height ) ? b2TrkBox.Width : b2TrkBox.Height;
            BOOL bDebug            = FALSE;
            if( bDebug ) logd( "MAA  c:%f  m:%f", fCurObjLen, fMinObjectLength );
            if( fCurObjLen < fMinObjectLength ) {
               m_pCurMutualAssistanceArea = pPTZPolygonalArea;
               if( bDebug ) logd( "Small !!\n" );
               bFindArea = TRUE;
               break;
            }
            if( bDebug ) logd( "\n" );
         }
         pPTZPolygonalArea = (PTZPolygonalArea*)pMAAreas->Next( pPTZPolygonalArea );
      }

      m_csPTZAreas.unlock();
   }
   if( FALSE == bFindArea )
      m_pCurMutualAssistanceArea = NULL;
#endif
#endif
}

void CVideoChannel::CheckFloodLightAreaProc()
{
#ifdef __SUPPORT_PTZ_CAMERA

   BOOL bFindArea = FALSE;

   int nCurIRLight = -1;
#ifdef __SUPPORT_PTZ_AREA
   if( m_bPTZCamera && m_pPTZCtrl && m_pPTZCtrl->IsPTZConnected() ) {
      m_csPTZAreas.lock();

      FPoint2D p2Center( m_PTZAreas[PTZAreaType_FloodLightArea].Width * 0.5f, m_PTZAreas[PTZAreaType_FloodLightArea].Height * 0.5f );
      PTZPolygonalArea* pPTZPolygonalArea = (PTZPolygonalArea*)m_PTZAreas[PTZAreaType_FloodLightArea].First();
      while( pPTZPolygonalArea ) {
         if( pPTZPolygonalArea->IsPointInsidePolygon( p2Center ) ) {
            nCurIRLight = pPTZPolygonalArea->IRLight;
         }
         pPTZPolygonalArea = pPTZPolygonalArea->Next;
      }

      m_csPTZAreas.unlock();
      m_pPTZCtrl->m_nCurIRLightValueOnIRArea = nCurIRLight;
   }
#endif
#endif
}

void CVideoChannel::SetTrkObjPTZPos( FPTZVector pvTrkObjPos )
{
   m_pvTrkObjPos = pvTrkObjPos;
}

void CVideoChannel::SetDrawPrivacyZone( BOOL bDraw )
{
   m_bDrawPrivacyZone = bDraw;
}

UINT64 CVideoChannel::GetIPCount()
{
   return m_nCountIP;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////

int CVideoChannel::StartAudioInOut() // (xinu_be22)
{
   if( m_nVideoChannelType == VideoChannelType_IPCamera ) {
      if( m_Cameras.size() ) {
         CCamera_IP* pIPCamera = (CCamera_IP*)m_Cameras[0];
         if( pIPCamera ) {
            m_nState |= VCState_AudioInOut;
            m_Cameras[0]->StartAudioInOut(); // 에러나도 내부에서 처리함.
         }
      }
   }
   return ( DONE );
}

int CVideoChannel::StopAudioInOut()
{
   if( m_nVideoChannelType == VideoChannelType_IPCamera ) {
      if( m_Cameras.size() ) {
         CCamera_IP* pIPCamera = (CCamera_IP*)m_Cameras[0];
         if( pIPCamera ) {
            if( m_nState & VCState_AudioInOut ) {
               m_Cameras[0]->StopAudioInOut();
               m_nState &= ~( VCState_AudioInOut );
            }
         }
      }
   }
   return ( DONE );
}

int CVideoChannel::SendAudioInOut( const BYTE* pDecodeBuff, int nDecodeBuffLen )
{
   if( m_nVideoChannelType == VideoChannelType_IPCamera ) {
      m_csCameras.Lock(); // Lock 추가
      if( m_Cameras.size() ) {
         CCamera_IP* pIPCamera = (CCamera_IP*)m_Cameras[0];
         if( pIPCamera ) {
            if( m_nState & VCState_AudioInOut ) {
               m_Cameras[0]->TransferAudio( pDecodeBuff, nDecodeBuffLen );
            }
         }
      }
      m_csCameras.Unlock();
   }
   return ( DONE );
}

////////////////////////////////////////////////////////////////////////////////////
//                            Multi Streamming
////////////////////////////////////////////////////////////////////////////////////

BOOL CVideoChannel::IsVideoSignalAlive( int nStreamIdx )
{
   if( !( m_nState & VCState_Activate ) || ( m_nState & ( VCState_SignalDeactivate | VCState_Deactivating ) ) ) return ( FALSE );

   if( VideoChannelType_IPCamera == m_nVideoChannelType ) {
      int i;
      for( i = 0; i < m_nCameraNum; i++ ) {
         CCamera_IP* pCameraIP = (CCamera_IP*)m_Cameras[i];
         if( pCameraIP ) {
            if( pCameraIP->m_nICameraIdx == nStreamIdx ) {
               return pCameraIP->IsVideoSignalAlive();
            }
         }
      }
   } else {
      if( GetVideoSignalFlag() )
         return TRUE;
      else
         return FALSE;
   }
   return FALSE;
}

void CVideoChannel::GetVideoBuff( int nStreamIdx, byte** ppYUY2Buff, int& nWidth, int& nHeight )
{
   if( !( m_nState & VCState_Activate ) || ( m_nState & ( VCState_SignalDeactivate | VCState_Deactivating ) ) ) return;

   *ppYUY2Buff = NULL;

   if( VideoChannelType_IPCamera == m_nVideoChannelType ) {
      int i;
      for( i = 0; i < m_nCameraNum; i++ ) {
         CCamera_IP* pCameraIP = (CCamera_IP*)m_Cameras[i];
         if( pCameraIP ) {
            if( pCameraIP->m_nICameraIdx == nStreamIdx ) {
               int nYUY2BuffLen = GetYUY2ImageLength( m_IPCamInfoArr[nStreamIdx].m_nWidth, m_IPCamInfoArr[nStreamIdx].m_nHeight );
               if( pCameraIP->m_SrcImg.Length == nYUY2BuffLen ) {
                  *ppYUY2Buff = (byte*)pCameraIP->m_SrcImg;
               }
               nWidth  = m_IPCamInfoArr[nStreamIdx].m_nWidth;
               nHeight = m_IPCamInfoArr[nStreamIdx].m_nHeight;
               break;
            }
         }
      }
   } else {
      if( ppYUY2Buff )
         *ppYUY2Buff = m_pYUY2Buff;
      nWidth  = m_nWidth;
      nHeight = m_nHeight;
   }
}

ISize2D CVideoChannel::GetVideoSize( int nStreamIdx )
{
   ISize2D s2ImageSize;

   if( VideoChannelType_IPCamera == m_nVideoChannelType ) {
      ASSERT( 0 <= nStreamIdx && nStreamIdx < MAX_IP_CAMERA_STREAM_NUM );
      if( 0 <= nStreamIdx && nStreamIdx < MAX_IP_CAMERA_STREAM_NUM ) {
         s2ImageSize.Width  = m_IPCamInfoArr[nStreamIdx].m_nWidth;
         s2ImageSize.Height = m_IPCamInfoArr[nStreamIdx].m_nHeight;
      }
   } else {
      s2ImageSize.Width  = m_nWidth;
      s2ImageSize.Height = m_nHeight;
   }
   return s2ImageSize;
}

float CVideoChannel::GetVideoFrameRate( int nStreamIdx )
{
   float fFrameRate = 30.0f;

   if( VideoChannelType_IPCamera == m_nVideoChannelType ) {
      ASSERT( 0 <= nStreamIdx && nStreamIdx < MAX_IP_CAMERA_STREAM_NUM );
      if( 0 <= nStreamIdx && nStreamIdx < MAX_IP_CAMERA_STREAM_NUM ) {
         fFrameRate = m_IPCamInfoArr[nStreamIdx].m_fFPS_Cap;
      }
   } else {
      fFrameRate = m_fFPS_Cap;
   }
   return fFrameRate;
}

void CVideoChannel::GetAvailableStreamIndex( std::vector<int>& vtAvailableStreamIndexs )
{
   int i;

   vtAvailableStreamIndexs.clear();

   if( VideoChannelType_IPCamera == m_nVideoChannelType ) {
      for( i = 0; i < MAX_IP_CAMERA_STREAM_NUM; i++ ) {
         if( m_IPCamInfoArr[i].m_bUseStream ) {
            vtAvailableStreamIndexs.push_back( i );
         }
      }
   } else {
      vtAvailableStreamIndexs.push_back( 0 );
   }
}

int CVideoChannel::GetStreamIdxOfMostSimillarInSize( const ISize2D& imgSize, float fDispQuality )
{
   int nStreamIdx = 0;

   std::vector<int> vtStreamIdxs;
   int i;
   for( i = 0; i < MAX_IP_CAMERA_STREAM_NUM; i++ ) {
      vtStreamIdxs.push_back( i );
   }

   nStreamIdx = GetStreamIdxOfMostSimillarInSize( vtStreamIdxs, imgSize, fDispQuality );
   return nStreamIdx;
}

int CVideoChannel::GetStreamIdxOfMostSimillarInSizeOnALive( const ISize2D& imgSize, float fDispQuality )
{
   int nStreamIdx = -1;

   if( !( m_nState & VCState_Activate ) || ( m_nState & ( VCState_SignalDeactivate | VCState_Deactivating ) ) ) return nStreamIdx;

   if( VideoChannelType_IPCamera == m_nVideoChannelType ) {
      std::vector<int> vtStreamIdxs;
      int i;
      for( i = 0; i < m_nCameraNum; i++ ) {
         CCamera_IP* pCameraIP = (CCamera_IP*)m_Cameras[i];
         if( pCameraIP ) {
            int nStreamIdx = pCameraIP->m_nICameraIdx;
            if( pCameraIP->IsVideoSignalAlive() ) {
               vtStreamIdxs.push_back( pCameraIP->m_nICameraIdx );
            }
         }
      }
      if( 1 == vtStreamIdxs.size() ) {
         return vtStreamIdxs[0];
      } else if( vtStreamIdxs.size() > 1 ) {
         nStreamIdx = GetStreamIdxOfMostSimillarInSize( vtStreamIdxs, imgSize, fDispQuality );
      }
   }

   return nStreamIdx;
}

int CVideoChannel::GetStreamIdxOfMostSimillarInSize( std::vector<int>& vtStreamIdxs, const ISize2D& imgSize, float fDispQuality )
{
   int nStreamIdxOfMostSimillarInSize = 0;

   if( VideoChannelType_IPCamera == m_nVideoChannelType ) {
      std::vector<__StreamItem> vtStreamItemArr;
      int i;
      int nStreamIdxSize = (int)vtStreamIdxs.size();
      for( i = 0; i < nStreamIdxSize; i++ ) {
         int nStreamIdx = vtStreamIdxs[i];
         if( 0 <= nStreamIdx && nStreamIdx < MAX_IP_CAMERA_STREAM_NUM ) {
            CIPCameraInfo& ipCamInfo = m_IPCamInfoArr[nStreamIdx];
            if( ipCamInfo.m_bUseStream ) {
               __StreamItem item;
               item.nStreamIdx = nStreamIdx;
               item.nSqrtArea  = (int)sqrt( float( ipCamInfo.m_nWidth * ipCamInfo.m_nHeight ) );
               vtStreamItemArr.push_back( item );
            }
         }
      }

      nStreamIdxOfMostSimillarInSize = ::GetStreamIdxOfMostSimillarInSize( vtStreamItemArr, imgSize, fDispQuality );
   }
   return nStreamIdxOfMostSimillarInSize;
}

int CVideoChannel::GetStreamIdxOfTheLargestVideoSizeOnLive()
{
   int nStreamIdxOfMostLargerVideoSizeOnLive = 0;
   if( !( m_nState & VCState_Activate ) || ( m_nState & ( VCState_SignalDeactivate | VCState_Deactivating ) ) )
      return nStreamIdxOfMostLargerVideoSizeOnLive;

   if( VideoChannelType_IPCamera == m_nVideoChannelType ) {
      int nMaxVideoSize = 0;
      int i;
      for( i = 0; i < m_nCameraNum; i++ ) {
         CCamera_IP* pCameraIP = (CCamera_IP*)m_Cameras[i];
         if( pCameraIP ) {
            int nStreamIdx = pCameraIP->m_nICameraIdx;
            if( pCameraIP->IsVideoSignalAlive() ) {
               int nCurVideoSize = pCameraIP->m_pInfo->m_nWidth * pCameraIP->m_pInfo->m_nHeight;
               if( nCurVideoSize > nMaxVideoSize ) {
                  nStreamIdxOfMostLargerVideoSizeOnLive = nStreamIdx;
                  nMaxVideoSize                         = nCurVideoSize;
               }
            }
         }
      }
   }
   return nStreamIdxOfMostLargerVideoSizeOnLive;
}

int CVideoChannel::GetStreamIdxOfTheLargestVideoSizeInDecoding()
{
   int nStreamIdxOfMostLargerVideoSizeInDecoding = 0;
   if( !( m_nState & VCState_Activate ) || ( m_nState & ( VCState_SignalDeactivate | VCState_Deactivating ) ) ) return nStreamIdxOfMostLargerVideoSizeInDecoding;

   if( VideoChannelType_IPCamera == m_nVideoChannelType ) {
      int nMaxVideoSize = 0;
      int i;
      for( i = 0; i < m_nCameraNum; i++ ) {
         CCamera_IP* pCameraIP = (CCamera_IP*)m_Cameras[i];
         if( pCameraIP ) {
            int nStreamIdx = pCameraIP->m_nICameraIdx;
            BOOL bUseStream, bDecodeStream;
            CheckStreamUsage( nStreamIdx, bUseStream, bDecodeStream );
            if( bDecodeStream ) {
               if( pCameraIP->IsVideoSignalAlive() ) {
                  int nCurVideoSize = pCameraIP->m_pInfo->m_nWidth * pCameraIP->m_pInfo->m_nHeight;
                  if( nCurVideoSize > nMaxVideoSize ) {
                     nStreamIdxOfMostLargerVideoSizeInDecoding = nStreamIdx;
                     nMaxVideoSize                             = nCurVideoSize;
                  }
               }
            }
         }
      }
   }
   return nStreamIdxOfMostLargerVideoSizeInDecoding;
}

int CVideoChannel::GetStreamIdxOfTheSmallestVideoSizeInDecoding()
{
   int nStreamIdxOfMostSmallestVideoSizeInDecoding = 0;
   if( !( m_nState & VCState_Activate ) || ( m_nState & ( VCState_SignalDeactivate | VCState_Deactivating ) ) ) return nStreamIdxOfMostSmallestVideoSizeInDecoding;

   if( VideoChannelType_IPCamera == m_nVideoChannelType ) {
      int nMinVideoSize = INT_MAX;
      int i;
      for( i = 0; i < m_nCameraNum; i++ ) {
         CCamera_IP* pCameraIP = (CCamera_IP*)m_Cameras[i];
         if( pCameraIP ) {
            int nStreamIdx = pCameraIP->m_nICameraIdx;
            BOOL bUseStream, bDecodeStream;
            CheckStreamUsage( nStreamIdx, bUseStream, bDecodeStream );
            if( bDecodeStream ) {
               if( pCameraIP->IsVideoSignalAlive() ) {
                  int nCurVideoSize = pCameraIP->m_pInfo->m_nWidth * pCameraIP->m_pInfo->m_nHeight;
                  if( nCurVideoSize < nMinVideoSize ) {
                     nStreamIdxOfMostSmallestVideoSizeInDecoding = nStreamIdx;
                     nMinVideoSize                               = nCurVideoSize;
                  }
               }
            }
         }
      }
   }
   return nStreamIdxOfMostSmallestVideoSizeInDecoding;
}

void CVideoChannel::SetDecodeAndConnectionPolicy( int nVCDecodeAndConnectionPolicy )
{
   ASSERT( VCDecodeAndConnectionPolicy_FullDecoding <= nVCDecodeAndConnectionPolicy && nVCDecodeAndConnectionPolicy <= VCDecodeAndConnectionPolicy_AdaptiveConnection );
   m_nVCDecodeAndConnectionPolicy = nVCDecodeAndConnectionPolicy;
}

int CVideoChannel::GetDecodeAndConnectionPolicy()
{
   return m_nVCDecodeAndConnectionPolicy;
}

int CVideoChannel::GetStreamIdxOfProcCallbackFunc( VCProcCallbackFuncType nVCProcCallbackFuncType )
{
   int nStreamIdx = 0;
   if( VideoChannelType_IPCamera == m_nVideoChannelType ) {
      nStreamIdx = m_nDecodeStreamIndexOnProcCallbackFunc[nVCProcCallbackFuncType];
   }
   return nStreamIdx;
}

BOOL CVideoChannel::SetStreamIdxOfProcCallbackFunc( VCProcCallbackFuncType nVCProcCallbackFuncType, int nStreamIndex )
{
   if( m_nVideoChannelType != VideoChannelType_IPCamera ) return FALSE;

   // 디버그용 택스트 출력
   if( 0 )
      if( m_nDecodeStreamIndexOnProcCallbackFunc[nVCProcCallbackFuncType] != nStreamIndex ) {
         if( VCProcCallbackFuncType_Display == nVCProcCallbackFuncType ) {
            logd( "[CH:%d][Stream:%d] Display Callback\n", m_nChannelNo_IVX + 1, nStreamIndex + 1 );
         }
         if( VCProcCallbackFuncType_ImgProc == nVCProcCallbackFuncType ) {
            logd( "[CH:%d][Stream:%d] Image Process Callback\n", m_nChannelNo_IVX + 1, nStreamIndex + 1 );
         }
         if( VCProcCallbackFuncType_Recoding == nVCProcCallbackFuncType ) {
            logd( "[CH:%d][Stream:%d] Recoding Callback\n", m_nChannelNo_IVX + 1, nStreamIndex + 1 );
         }
      }

   m_nDecodeStreamIndexOnProcCallbackFunc[nVCProcCallbackFuncType] = nStreamIndex;

   if( !m_IPCamInfoArr[nStreamIndex].m_bUseStream ) {
      return FALSE;
   }
   return TRUE;
}

int CVideoChannel::GetUseOfProcCallbackFunc( VCProcCallbackFuncType nVCProcCallbackFuncType, BOOL& bUseProcCallbackFunc )
{
   ASSERT( 0 <= nVCProcCallbackFuncType && nVCProcCallbackFuncType < VCProcCallbackFuncType_Num );

   bUseProcCallbackFunc = m_bUseProcCallbackFunc[nVCProcCallbackFuncType];

   return ( DONE );
}

int CVideoChannel::SetUseOfProcCallbackFunc( VCProcCallbackFuncType nVCProcCallbackFuncType, BOOL bUseProcCallbackFunc )
{
   ASSERT( 0 <= nVCProcCallbackFuncType && nVCProcCallbackFuncType < VCProcCallbackFuncType_Num );

   // 중요: 본 함수의 호출은 받드시 비디오채널 임계영역 안에서 호출되어야 한다.
   CCriticalSectionSP co( m_csVideoChannel );

   if( VideoChannelType_IPCamera != m_nVideoChannelType ) {
      if( VCProcCallbackFuncType_Display == nVCProcCallbackFuncType ) {
         bUseProcCallbackFunc = TRUE;
      }
   }
   if( VideoChannelType_Unused == m_nVideoChannelType ) {
      bUseProcCallbackFunc = FALSE;
   }

#ifdef __SUPPORT_PTZ_CAMERA
#ifdef __SUPPORT_VIRTUAL_PTZ_CAMERA
   if( VideoChannelType_VirtualPTZ == m_nVideoChannelType ) {
      if( m_Cameras.size() ) {
         CCamera_VirtualPTZ* pVertualPTZCam = (CCamera_VirtualPTZ*)m_Cameras[0];
         if( pVertualPTZCam->m_nVirtualPTZCamState & VirtualPTZCameraState_ReactivatingForResetingSourceVideoChannel ) {
            return ( DONE );
         }
      }
   }
#endif // __SUPPORT_VIRTUAL_PTZ_CAMERA
#endif // __SUPPORT_PTZ_CAMERA

   m_bUseProcCallbackFunc[nVCProcCallbackFuncType] = bUseProcCallbackFunc;

   if( !( m_nState & VCState_Activate ) || ( m_nState & ( VCState_SignalDeactivate | VCState_Deactivating ) ) ) return ( DONE );

   int nThreadFlag = GetThreadFlagFromProcCallbackFuncType( nVCProcCallbackFuncType );

   if( bUseProcCallbackFunc ) {
      BOOL bActivateThread = FALSE;
      m_csCurExecThreadFlag.Lock();
      if( ( nThreadFlag & m_nCurExecThreadFlag ) != nThreadFlag ) {
         bActivateThread = TRUE;
      }
      m_csCurExecThreadFlag.Unlock();
      if( bActivateThread )
         ActivateThread( nThreadFlag );

   } else {
      BOOL bDectivateThread = FALSE;
      m_csCurExecThreadFlag.Lock();
      if( nThreadFlag & m_nCurExecThreadFlag ) {
         bDectivateThread = TRUE;
      }
      m_csCurExecThreadFlag.Unlock();
      if( bDectivateThread )
         DeactivateThread( nThreadFlag );
   }
   return ( DONE );
}

int CVideoChannel::GetUseOfStreamIdxInDecodeCallBackFunc( int nStreamIdx, BOOL& bUseStreamIdx )
{
   ASSERT( 0 <= nStreamIdx && nStreamIdx < MAX_IP_CAMERA_STREAM_NUM );

   if( 0 <= nStreamIdx && nStreamIdx < MAX_IP_CAMERA_STREAM_NUM ) {
      bUseStreamIdx = m_bUseOfStreamIdxInDecodeCallBackFunc[nStreamIdx];
   }
   return ( DONE );
}

int CVideoChannel::SetUseOfStreamIdxInDecodeCallBackFunc( int nStreamIdx, BOOL bUseStreamIdx )
{
   ASSERT( 0 <= nStreamIdx && nStreamIdx < MAX_IP_CAMERA_STREAM_NUM );

   if( 0 <= nStreamIdx && nStreamIdx < MAX_IP_CAMERA_STREAM_NUM ) {
      m_bUseOfStreamIdxInDecodeCallBackFunc[nStreamIdx] = bUseStreamIdx;
   }
   return ( DONE );
}

int CVideoChannel::GetUseOfStreamIdxInEncodeCallBackFunc( int nStreamIdx, BOOL& bUseStreamIdx )
{
   ASSERT( 0 <= nStreamIdx && nStreamIdx < MAX_IP_CAMERA_STREAM_NUM );

   if( 0 <= nStreamIdx && nStreamIdx < MAX_IP_CAMERA_STREAM_NUM ) {
      bUseStreamIdx = m_bUseOfStreamIdxInEncodeCallBackFunc[nStreamIdx];
   }
   return ( DONE );
}

int CVideoChannel::SetUseOfStreamIdxInEncodeCallBackFunc( int nStreamIdx, BOOL bUseStreamIdx )
{
   ASSERT( 0 <= nStreamIdx && nStreamIdx < MAX_IP_CAMERA_STREAM_NUM );

   if( 0 <= nStreamIdx && nStreamIdx < MAX_IP_CAMERA_STREAM_NUM ) {
      m_bUseOfStreamIdxInEncodeCallBackFunc[nStreamIdx] = bUseStreamIdx;
   }
   return ( DONE );
}

void CVideoChannel::IPCameraDecodeAndConnectionManageProc()
{
   CCriticalSectionSP co( m_csVideoChannel );

   if( VideoChannelType_IPCamera != m_nVideoChannelType ) return;

   if( m_nState & ( VCState_SignalDeactivate | VCState_Deactivating ) ) return;
   if( FALSE == ( m_nState & VCState_Activate ) ) return;

   int i;

   for( i = 0; i < MAX_IP_CAMERA_STREAM_NUM; i++ ) {
      BOOL bUseStream;
      BOOL bDecodeStream;
      CheckStreamUsage( i, bUseStream, bDecodeStream );

      CCamera_IP* pIPCamera = GetIPCameraByStreamIdx( i );
      if( !pIPCamera ) continue;
      if( FALSE == ( pIPCamera->GetState() & ICamera::State_Initialize ) )
         continue;

      BOOL bPTZConnectionDependOnStream = FALSE;
      if( 0 == pIPCamera->m_nICameraIdx ) {
#ifdef __SUPPORT_PTZ_CAMERA
         if( m_bPTZCamera && m_pPTZCtrl ) {
            if( PTZCtrlConnType_Basic == m_PTZInfo.m_nConnectType ) {
               if( PTZCtrlCap_ConnectionDependOnStream & m_pPTZCtrl->m_nCapabilities ) {
                  bPTZConnectionDependOnStream = TRUE;
               }
            }
         }
#endif
      }

      if( VCDecodeAndConnectionPolicy_FullDecoding == m_nVCDecodeAndConnectionPolicy ) {
         if( FALSE == pIPCamera->GetDecodeVideo() ) {
            logd( "[CH:%d][Stream:%d] FullDecoding SetDecodeVideo (TRUE)\n", m_nChannelNo_IVX + 1, i + 1 );
            pIPCamera->SetDecodeVideo( TRUE );
         }
         if( FALSE == ( pIPCamera->GetState() & ICamera::State_Capture ) ) {
            logd( "[CH:%d][Stream:%d] FullDecoding BeginCapture (TRUE)\n", m_nChannelNo_IVX + 1, i + 1 );
            pIPCamera->BeginCapture();
         }
      } else if( VCDecodeAndConnectionPolicy_AdaptiveDecoding == m_nVCDecodeAndConnectionPolicy ) {
         if( FALSE == ( pIPCamera->GetState() & ICamera::State_Capture ) ) {
            //logd ("[CH:%d][Stream:%d] AdaptiveDecoding BeginCapture (TRUE)\n", m_nChannelNo_IVX+1, i+1);
            pIPCamera->BeginCapture();
         }
         if( m_nState & VCState_IPCamStreamVideoSizeToBeChecked ) {
            if( pIPCamera->m_bFirstDecompressOK ) {
               m_nState &= ~VCState_IPCamStreamVideoSizeToBeChecked;
            } else {
               pIPCamera->SetDecodeVideo( TRUE );
            }
         } else {
            BOOL bDecodePrev = pIPCamera->GetDecodeVideo();
            if( bDecodeStream != bDecodePrev && pIPCamera->m_bFirstDecompressOK ) {
               logd( "[CH:%d][Stream:%d] AdaptiveDecoding SetDecodeVideo (%d)\n", m_nChannelNo_IVX + 1, i + 1, bDecodeStream );
               pIPCamera->SetDecodeVideo( bDecodeStream );
            }
         }
      } else if( VCDecodeAndConnectionPolicy_AdaptiveConnection == m_nVCDecodeAndConnectionPolicy ) {
         pIPCamera->SetDecodeVideo( TRUE );
         BOOL bCloseConnection = !bUseStream;
         // 최초 접속한 후에 한 번이라도 디코딩에 성공한 스트림에 대해서만 연결을 끊는다.
         // 왜? 한번이라도 디코딩에 성공을 해야 영상의 크기를 얻을 수 있기 때문이다.
         // 또한 알람IO를 체크하기 위한 루틴이 스트림에 종속적이면 연결을 끊지 않는다.
         if( bCloseConnection && /*pIPCamera->m_bFirstDecompressOK &&*/ !bPTZConnectionDependOnStream ) {
            if( pIPCamera->GetState() & ICamera::State_Capture ) {
               logd( "[CH:%d][Stream:%d] AdaptiveConnection SignalStopCapture (TRUE)\n", m_nChannelNo_IVX + 1, i + 1 );
               if( FALSE == ( CCamera_IP::IPCamState_PausingCapture & pIPCamera->GetIPCamState() ) )
                  pIPCamera->m_bLastVideoSignal = ( pIPCamera->GetState() & ICamera::State_VideoSignalNotOK ) ? FALSE : TRUE;
               pIPCamera->ModifyIPCamState( 0, CCamera_IP::IPCamState_PausingCapture );
               pIPCamera->SignalStopCapture();
            }
         } else {
            if( FALSE == ( pIPCamera->GetState() & ICamera::State_Capture ) ) {
               logd( "[CH:%d][Stream:%d] AdaptiveConnection BeginCapture (TRUE)\n", m_nChannelNo_IVX + 1, i + 1 );
               pIPCamera->ModifyIPCamState( CCamera_IP::IPCamState_PausingCapture, 0 );
               pIPCamera->BeginCapture();
            }
         }
      }
   }
}

void CVideoChannel::CheckStreamUsage( int nStreamIdx, BOOL& bUseStream, BOOL& bDecodeStream )
{
   int i;

   ASSERT( 0 <= nStreamIdx && nStreamIdx < MAX_IP_CAMERA_STREAM_NUM );

   BOOL _bUseStream    = FALSE;
   BOOL _bDecodeStream = FALSE;

   if( 0 <= nStreamIdx && nStreamIdx < MAX_IP_CAMERA_STREAM_NUM ) {
      CIPCameraInfo* pIPCamInfo = &m_IPCamInfoArr[nStreamIdx];
      if( pIPCamInfo->m_bUseStream ) {
         if( m_bUseOfStreamIdxInEncodeCallBackFunc[nStreamIdx] ) {
            _bUseStream = TRUE;
         }
         if( m_bUseOfStreamIdxInDecodeCallBackFunc[nStreamIdx] ) {
            _bUseStream    = TRUE;
            _bDecodeStream = TRUE;
         }
         if( m_nState & VCState_IPCamStreamVideoSizeToBeChecked ) {
            bUseStream = TRUE;
         }
      }

      if( !_bUseStream || !_bDecodeStream ) {
         for( i = 0; i < VCProcCallbackFuncType_Num; i++ ) {
            if( m_bUseProcCallbackFunc[i] && ( nStreamIdx == m_nDecodeStreamIndexOnProcCallbackFunc[i] ) ) {
               CIPCameraInfo* pIPCamInfo = &m_IPCamInfoArr[nStreamIdx];
               if( pIPCamInfo->m_bUseStream ) {
                  _bUseStream    = TRUE;
                  _bDecodeStream = TRUE;
               }
            }
            if( _bUseStream && _bDecodeStream )
               break;
            if( m_bSignalToVertualPTZCameras[nStreamIdx] ) {
               _bUseStream    = TRUE;
               _bDecodeStream = TRUE;
            }
            if( _bUseStream && _bDecodeStream )
               break;
         }
      }
   }
   bUseStream    = _bUseStream;
   bDecodeStream = _bDecodeStream;
}

BOOL CVideoChannel::IsDecodeStream( int nStreamIdx )
{
   BOOL bUseStream;
   BOOL bDecodeStream;
   CheckStreamUsage( nStreamIdx, bUseStream, bDecodeStream );
   return bDecodeStream;
}

BOOL CVideoChannel::IsStreamIsUsedInAnyCallBackFunc( int nStreamIdx )
{
   BOOL bUseStream;
   BOOL bDecodeStream;
   CheckStreamUsage( nStreamIdx, bUseStream, bDecodeStream );
   return bUseStream;
}

uint CVideoChannel::GetThreadFlagFromProcCallbackFuncType( VCProcCallbackFuncType nVCProcCallbackFuncType )
{
   uint nThreadFlag = 0;

   if( VCProcCallbackFuncType_Display == nVCProcCallbackFuncType ) {
      if( VideoChannelType_IPCamera == m_nVideoChannelType ) {
         nThreadFlag = VCThreadType_Cap;
      } else {
         nThreadFlag = VCThreadType_Cap | VCThreadType_Disp;
      }
   }
   if( VCProcCallbackFuncType_Recoding == nVCProcCallbackFuncType ) {
      nThreadFlag = VCThreadType_Rec;
   }
   if( VCProcCallbackFuncType_ImgProc == nVCProcCallbackFuncType ) {
      nThreadFlag = VCThreadType_IP;
   }
   return ( nThreadFlag );
}

BOOL CVideoChannel::SignalToExecProcCallBackFunction( VCProcCallbackFuncType nVCProcCallbackFuncType )
{
   if( nVCProcCallbackFuncType < 0 )
      return FALSE;
   if( nVCProcCallbackFuncType >= VCProcCallbackFuncType_Num )
      return FALSE;
   m_bSignalToExecProcCallbackFunc[nVCProcCallbackFuncType] = TRUE;
   return TRUE;
}

BOOL CVideoChannel::DetermineCopyingPrevStreamBuffToCurrStreamBuff( int nPrevStreamIndex, int nCurrStreamIndex )
{
   BOOL bCopy                 = FALSE;
   CCamera_IP* pIPCamera_Prev = GetIPCameraByStreamIdx( nPrevStreamIndex );
   CCamera_IP* pIPCamera_Curr = GetIPCameraByStreamIdx( nCurrStreamIndex );
   if( pIPCamera_Prev && pIPCamera_Curr ) {
      if( !IsNullFileTime( pIPCamera_Curr->m_ftLastDecodeFrameTime ) && !IsNullFileTime( pIPCamera_Prev->m_ftLastDecodeFrameTime ) ) {
         if( pIPCamera_Prev->m_ftLastDecodeFrameTime > pIPCamera_Curr->m_ftLastDecodeFrameTime ) {
            FILETIME ftCurTime;
            AfxGetLocalFileTime( &ftCurTime );
            FILETIME ftPrevDecodeElapsed = ftCurTime - pIPCamera_Curr->m_ftLastDecodeFrameTime;
            int nPrevDecodeElapsedInSec  = GetElapsedSec( ftPrevDecodeElapsed );
            if( nPrevDecodeElapsedInSec > 6 ) {
               bCopy = TRUE;
            }
         }
      }
   }
   return bCopy;
}

void CVideoChannel::CopyPrevStreamBuffToCurrStreamBuff( int nPrevStreamIndex, int nCurrStreamIndex )
{
   CCamera_IP* pIPCamera_Prev = GetIPCameraByStreamIdx( nPrevStreamIndex );
   CCamera_IP* pIPCamera_Curr = GetIPCameraByStreamIdx( nCurrStreamIndex );
   if( pIPCamera_Prev && pIPCamera_Curr ) {
      int nVideoWidth_PrevStream  = pIPCamera_Prev->m_pInfo->m_nWidth;
      int nVideoHeight_PrevStream = pIPCamera_Prev->m_pInfo->m_nHeight;
      int nYUY2BuffLen_PrevStream = GetYUY2ImageLength( nVideoWidth_PrevStream, nVideoHeight_PrevStream );
      if( nYUY2BuffLen_PrevStream == pIPCamera_Prev->m_SrcImg.Length ) {
         int nVideoWidth_CurrStream  = pIPCamera_Curr->m_pInfo->m_nWidth;
         int nVideoHeight_CurrStream = pIPCamera_Curr->m_pInfo->m_nHeight;
         int nYUY2BuffLen_CurrStream = GetYUY2ImageLength( nVideoWidth_CurrStream, nVideoHeight_CurrStream );
         if( nYUY2BuffLen_CurrStream == pIPCamera_Curr->m_SrcImg.Length ) {
            Resize_YUY2( (byte*)pIPCamera_Prev->m_SrcImg, nVideoWidth_PrevStream, nVideoHeight_PrevStream,
                         (byte*)pIPCamera_Curr->m_SrcImg, nVideoWidth_CurrStream, nVideoHeight_CurrStream );
         }
      }
   }
}

#ifdef __SUPPORT_FISHEYE_CAMERA
BOOL CVideoChannel::IsFisheyeCamera()
{
   if( IS_NOT_SUPPORT( __SUPPORT_FISHEYE_DEWARPING ) )
      return FALSE;

   return m_bFisheyeCam;
}

void CVideoChannel::FisheyeInit( byte* fisheye_buffer, int width, int height )
{
   int nStreamIdx             = m_nDecodeStreamIndexOnProcCallbackFunc[VCProcCallbackFuncType_Display];
   FISHEYE_INFO& rFisheyeInfo = m_IPCamInfoArr[nStreamIdx].m_FisheyeDewarpingSetup.m_FisheyeInfo;
   BOOL bFisheyeInit          = FALSE;

   // 해상도가 바뀌면 초기화를 다시 해야함 (mkjang-151124)
   ISize2D CurVideoSize = GetVideoSize( nStreamIdx );
   if( m_PrevVideoSize.Width != CurVideoSize.Width || m_PrevVideoSize.Height != CurVideoSize.Height )
      m_IPCamInfoArr[nStreamIdx].m_FisheyeDewarpingSetup.m_bDefaultFisheyeInfo = TRUE;

   if( m_IPCamInfoArr[nStreamIdx].m_FisheyeDewarpingSetup.m_bDefaultFisheyeInfo ) {
      GImage g_image( width, height );
      ConvertYUY2ToGray( fisheye_buffer, g_image );
      if( DONE == ExtractCircumcircle( rFisheyeInfo, g_image, 70 ) ) {
         rFisheyeInfo.up_range = 35.0f;
         rFisheyeInfo.lp_range = 35.0f;

         logd( ">>>Fisheye Info : stream[%d] radius: %.3f, center_x: %.3f, center_y: %.3f, focal_length: %.2f\n",
               nStreamIdx, rFisheyeInfo.radius, rFisheyeInfo.center.X, rFisheyeInfo.center.Y, rFisheyeInfo.f_length );
         bFisheyeInit = TRUE;
      }
   } else {
      bFisheyeInit = TRUE;
   }

   if( bFisheyeInit ) {
      rFisheyeInfo.f_length = GetFocalLengthFromVFOV( height, 90.0f );
      if( NULL == m_pImageEnhancer )
         m_pImageEnhancer = new CImageEnhancement;
   }
   m_bFisheyeInit  = bFisheyeInit;
   m_PrevVideoSize = CurVideoSize;
}

BOOL CVideoChannel::IsFisheyeInit()
{
   return m_bFisheyeInit;
}

void CVideoChannel::GetPanoramaRect_Fisheye( BOOL bFitVideoBox, CRect rectBox, CRect& rectVideo )
{
   // 화면에 출력되는 싱글, 더블 파노라마 영상의 크기를 계산하는 함수
   // in:  bFitVideoBox : 영상을 박스 프레임에 맞출지 여부
   // in:  rectBox      : 영상이 출력되는 전체 영역
   // out: rectVideo    : 영상이 실제로 그려지는 영역

   int nCylinderWidth = 0, nCylinderHeight = 0;
   int nDPWidth = 0, nDPHeight = 0;
   int nVideoBoxWidth         = rectBox.right - rectBox.left;
   int nVideoBoxHeight        = rectBox.bottom - rectBox.top;
   int nStreamIdx             = m_nDecodeStreamIndexOnProcCallbackFunc[VCProcCallbackFuncType_Display];
   FISHEYE_INFO& rFisheyeInfo = m_IPCamInfoArr[nStreamIdx].m_FisheyeDewarpingSetup.m_FisheyeInfo;
   FisheyeViewType curView    = GetFisheyeView();

   if( SINGLE_PANORAMA == curView ) {
      // 비디오 박스의 너비를 이용하여 파노라마 영상의 높이를 구함
      nCylinderWidth  = nVideoBoxWidth;
      nCylinderHeight = GetCylindricalImageHeight( nCylinderWidth, rFisheyeInfo.up_range, rFisheyeInfo.lp_range );

      if( nCylinderHeight > nVideoBoxHeight ) {
         // 만약 구해진 파노라마의 높이가 비디오 박스의 높이보다 크다면
         // 비디오 박스의 높이를 이용하여 파노라마의 너비를 다시 구한다.
         nCylinderWidth  = GetCylindricalImageWidth( nVideoBoxHeight, rFisheyeInfo.up_range, rFisheyeInfo.lp_range );
         nCylinderHeight = nVideoBoxHeight;

         // 파노라마 영상을 가로 중앙에 배치
         rectVideo.left   = rectBox.left + ( nVideoBoxWidth - nCylinderWidth ) / 2;
         rectVideo.right  = rectBox.right - ( nVideoBoxWidth - nCylinderWidth ) / 2;
         rectVideo.top    = rectBox.top;
         rectVideo.bottom = rectBox.bottom;
      } else {
         // 파노라마 영상을 세로 중앙에 배치
         rectVideo.left   = rectBox.left;
         rectVideo.right  = rectBox.right;
         rectVideo.top    = ( nVideoBoxHeight - nCylinderHeight ) / 2 + rectBox.top;
         rectVideo.bottom = rectVideo.top + nCylinderHeight;
      }

      if( bFitVideoBox ) {
         rectVideo = rectBox;
      }

      m_nCylinderWidth  = nCylinderWidth;
      m_nCylinderHeight = nCylinderHeight;
   } else if( DOUBLE_PANORAMA == curView ) {
      // 주의: 아래 계산의 순서는 바꾸지 말 것
      nCylinderHeight = nVideoBoxHeight / 2; // 1
      nCylinderWidth  = GetCylindricalImageWidth( nCylinderHeight, rFisheyeInfo.up_range, rFisheyeInfo.lp_range ); // 2
      nDPHeight       = nCylinderHeight * 2; // 3
      nDPWidth        = nCylinderWidth / 2; // 4

      if( nDPWidth < nVideoBoxWidth ) {
         rectVideo.left   = ( nVideoBoxWidth - nDPWidth ) / 2 + rectBox.left;
         rectVideo.right  = rectVideo.left + nDPWidth;
         rectVideo.top    = rectBox.top;
         rectVideo.bottom = rectVideo.top + nDPHeight;
      } else {
         nCylinderWidth  = nVideoBoxWidth;
         nCylinderHeight = GetCylindricalImageHeight( nCylinderWidth, rFisheyeInfo.up_range, rFisheyeInfo.lp_range );
         nDPHeight       = nCylinderHeight * 2;
         nDPWidth        = nCylinderWidth / 2;

         rectVideo.left   = rectBox.left;
         rectVideo.right  = rectBox.right;
         rectVideo.top    = ( rectBox.bottom + rectBox.top - nDPHeight ) / 2;
         rectVideo.bottom = rectVideo.top + nDPHeight;
      }

      if( bFitVideoBox ) {
         rectVideo = rectBox;
      }

      m_nDPWidth  = nDPWidth;
      m_nDPHeight = nDPHeight;
   } else if( HALF_PANORAMA == curView ) {
      // 주의: 아래 계산의 순서는 바꾸지 말 것
      nCylinderWidth  = nVideoBoxWidth;
      nCylinderHeight = GetCylindricalImageHeight( nCylinderWidth * 2, rFisheyeInfo.up_range, rFisheyeInfo.lp_range );

      if( nCylinderHeight > nVideoBoxHeight ) {
         // 만약 구해진 파노라마의 높이가 비디오 박스의 높이보다 크다면
         // 비디오 박스의 높이를 이용하여 파노라마의 너비를 다시 구한다.
         nCylinderWidth  = GetCylindricalImageWidth( nVideoBoxHeight, rFisheyeInfo.up_range, rFisheyeInfo.lp_range );
         nCylinderWidth  = nCylinderWidth / 2;
         nCylinderHeight = nVideoBoxHeight;

         // 파노라마 영상을 가로 중앙에 배치
         rectVideo.left   = rectBox.left + ( ( nVideoBoxWidth - nCylinderWidth ) / 2 );
         rectVideo.right  = rectBox.right - ( ( nVideoBoxWidth - nCylinderWidth ) / 2 );
         rectVideo.top    = rectBox.top;
         rectVideo.bottom = rectBox.bottom;
      } else {
         // 파노라마 영상을 세로 중앙에 배치
         rectVideo.left   = rectBox.left;
         rectVideo.right  = rectBox.right;
         rectVideo.top    = ( nVideoBoxHeight - nCylinderHeight ) / 2 + rectBox.top;
         rectVideo.bottom = rectVideo.top + nCylinderHeight;
      }

      if( bFitVideoBox ) {
         rectVideo = rectBox;
      }

      m_nCylinderWidth  = nCylinderWidth;
      m_nCylinderHeight = nCylinderHeight;
   }
}

FisheyeViewType CVideoChannel::GetFisheyeView()
{
   int nStreamIdx = m_nDecodeStreamIndexOnProcCallbackFunc[VCProcCallbackFuncType_Display];
   return m_IPCamInfoArr[nStreamIdx].m_FisheyeDewarpingSetup.GetFisheyeView();
}

//// VPTZ
void CVideoChannel::SetPtzPosition_Fisheye( FPTZVector Pos, int nHeight )
{
   m_fPan                      = Pos.P;
   m_fTilt                     = Pos.T;
   float VFOV                  = 90.0f / Pos.Z;
   m_FisheyeInfo_VPTZ.f_length = GetFocalLengthFromVFOV( nHeight, VFOV );
}

void CVideoChannel::GetPtzPosition_Fisheye( FPTZVector& Pos, int nHeight )
{
   Pos.P      = m_fPan;
   Pos.T      = m_fTilt;
   float VFOV = 2.0f * MC_RAD2DEG * atan( 0.5f * nHeight / m_FisheyeInfo_VPTZ.f_length );
   Pos.Z      = 90.0f / VFOV;
}

void CVideoChannel::GotoAbsPos_Fisheye( FPoint2D Coord, float fZoom )
{
   float fPan, fTilt;
   GetNewPanTiltPos( m_nWidth, m_nHeight, m_fPan, m_fTilt, m_FisheyeInfo_VPTZ.f_length, Coord, fPan, fTilt );
   m_fPan  = fPan;
   m_fTilt = fTilt;
}
void CVideoChannel::Dewarp_YUY2( byte* pSrcBuffer, int nSrcWidth, int nSrcHeight, byte* pDstBuffer, int nDstWidth, int nDstHeight )
{
   m_csDewarp.lock();
   ConvertFisheyeToPlanarImage_YUY2( m_FisheyeInfo_VPTZ, pSrcBuffer, nSrcWidth, nSrcHeight, m_fPan, m_fTilt, pDstBuffer, nDstWidth, nDstHeight );
   m_csDewarp.unlock();
}

#endif

void CVideoChannel::UpdateARAreaCoordinate( int nWidth, int nHeight )
{
#ifdef __SUPPORT_PTZ_CAMERA
#ifdef __SUPPORT_AUGMENTED_REALITY
   if( IS_SUPPORT( __SUPPORT_AUGMENTED_REALITY ) ) {
      // 현재 PTZ 위치에 대응되는 폴리곤 영역좌표를 업데이트한다.
      if( m_bPTZCamera && m_pPTZCtrl && m_pPTZCtrl->IsPTZConnected() ) {
         m_csARFeatureList.lock();
         int swh = nWidth / 2;
         int shh = nHeight / 2;
         ISize2D s2Src( nWidth, nHeight );
         CARFeatureInfo* pARFeatureInfo = (CARFeatureInfo*)m_ARFeatureList.First();
         while( pARFeatureInfo ) {
            int i;
            int nVerticeNum = pARFeatureInfo->Vertices.Length;
            BOOL bDrawArea  = TRUE;
            for( i = 0; i < nVerticeNum; i++ ) {
               IPoint2D& ptVertice = pARFeatureInfo->Vertices[i];
               FPoint3D& pvPTZPos  = pARFeatureInfo->PTZPositions[i];
               FPTZVector pvPos;
               pvPos.P = pvPTZPos.X;
               pvPos.T = pvPTZPos.Y;
               pvPos.Z = pvPTZPos.Z;
               FPoint2D p2ImgPos;
               m_pPTZCtrl->GetImagePnt( pvPos, s2Src, p2ImgPos );
               ptVertice.X = int( p2ImgPos.X + swh );
               ptVertice.Y = int( p2ImgPos.Y + shh );
               if( ptVertice.X < -10e7 || ptVertice.X > 10e7 || ptVertice.Y < -10e7 || ptVertice.Y > 10e7 ) {
                  bDrawArea = FALSE;
                  break;
               }
            }
            if( FALSE == bDrawArea ) {
               for( i = 0; i < nVerticeNum; i++ ) {
                  IPoint2D& ptVertice = pARFeatureInfo->Vertices[i];
                  ptVertice.X = ptVertice.Y = 0xFFFFFFFF;
               }
            }
            pARFeatureInfo = (CARFeatureInfo*)m_ARFeatureList.Next( pARFeatureInfo );
         }
         m_csARFeatureList.unlock();
      }
   }
#endif //__SUPPORT_AUGMENTED_REALITY
#endif //__SUPPORT_PTZ_CAMERA
}

void CVideoChannel::GetVideoFrmRecvCount( int& nVideoFrmRecvCount, int& nVideoKeyFrmRecvCount )
{
   m_csCameras.Lock();
   if( VideoChannelType_IPCamera == m_nVideoChannelType ) {
      if( m_nCameraNum >= 1 ) {
         CCamera_IP* pIPCamera = (CCamera_IP*)m_Cameras[0];
         if( pIPCamera ) {
            pIPCamera->GetVideoFrmRecvCount( nVideoFrmRecvCount, nVideoKeyFrmRecvCount );
         }
      }
   }
   m_csCameras.Unlock();
}

BOOL CVideoChannel::SendSetParameterCommand( char const* parameterName, char const* parameterValue )
{
   m_csCameras.Lock();
   if( VideoChannelType_IPCamera == m_nVideoChannelType ) {
      if( m_nCameraNum >= 1 ) {
         CCamera_IP* pIPCamera = (CCamera_IP*)m_Cameras[0];
         if( pIPCamera ) {
            if( IPCameraType_RTSP == pIPCamera->m_pInfo->m_nIPCameraType ) {
               CIPCamStream_RTSP* pRTSPStream = (CIPCamStream_RTSP*)( pIPCamera->GetIPCamStream() );
               unsigned ret                   = pRTSPStream->SendSetParamerCommand( parameterName, parameterValue );
               if( ret > 0 ) return TRUE;
            }
         }
      }
   }
   m_csCameras.Unlock();
   return FALSE;
}

BOOL CVideoChannel::SendGetParameterCommand( char const* parameterName )
{
   m_csCameras.Lock();
   if( VideoChannelType_IPCamera == m_nVideoChannelType ) {
      if( m_nCameraNum >= 1 ) {
         CCamera_IP* pIPCamera = (CCamera_IP*)m_Cameras[0];
         if( pIPCamera ) {
            if( IPCameraType_RTSP == pIPCamera->m_pInfo->m_nIPCameraType ) {
               CIPCamStream_RTSP* pRTSPStream = (CIPCamStream_RTSP*)( pIPCamera->GetIPCamStream() );
               unsigned ret                   = pRTSPStream->SendGetParamerCommand( parameterName );
               if( ret > 0 ) return TRUE;
            }
         }
      }
   }
   m_csCameras.Unlock();
   return FALSE;
}

﻿#include "stdafx.h"
#include "version.h"
#include "CommonUtil.h"
#include "VideoChannel.h"
#include "VideoChannelManager.h"
//#include "NMEAConnection.h"
#include "SupportFunction.h"

//////////////////////////////////////////////////////////////////////////////////////////
//
// Class: CVideoChannelManager
//
//////////////////////////////////////////////////////////////////////////////////////////

CVideoChannelManager::CVideoChannelManager()
{
   m_nState                                  = 0;
   m_nCurVCID                                = 0;
   m_nVideoChannelNum                        = 0;
   m_bSetupTemp                              = FALSE;
   m_bVCMForRemote                           = FALSE;
   m_bSignalStopVideoChannelManagementThread = FALSE;

   InitGlobalProperties(); // 비디오 채널과 관계된 모든 전역 속성 변수들을 초기화 한다.

   ZeroMemory( m_nVCState, sizeof( m_nVCState ) );
}

CVideoChannelManager::~CVideoChannelManager()
{
   int i;
   Uninitialize();
   for( i = 0; i < (int)m_VideoChannelList.size(); i++ )
      delete m_VideoChannelList[i];
   m_VideoChannelList.clear();
}

void CVideoChannelManager::Initialize( VCM_Desc& desc )
{
   if( m_nState & State_Initialize ) return;
   m_nState |= State_Initialize;
   m_Desc = desc;
   if( !m_bVCMForRemote ) {
      __PTZ3DFile.SetFilePath( desc.m_strConfigPath.c_str() );
#ifdef __SUPPORT_PTZ_CAMERA
      __PTZ3DFile.LoadFile();
#endif
      if( m_Desc.m_bUseVCMConfigFile ) {
         LoadCFGFile();
         for( int i = 0; i < (int)m_VideoChannelList.size(); i++ ) {
            m_VideoChannelList[i]->SetRemote( m_bVCMForRemote );
            m_VideoChannelList[i]->Initialize();
         }
      }
   }
}

void CVideoChannelManager::Uninitialize()
{
   int i;

   if( !( m_nState & State_Initialize ) ) return;
   DeactivateAllVideoChannels();
   Deactivate();
   if( m_Desc.m_bUseVCMConfigFile )
      SaveCFGFile();
   for( i = 0; i < (int)m_VideoChannelList.size(); i++ ) {
      delete m_VideoChannelList[i];
   }
   m_VideoChannelList.clear();

   if( !m_bVCMForRemote ) {
      __PTZ3DFile.Dump();
      __PTZ3DFile.SaveFile();
   }
   m_nState &= ~State_Initialize;
}

void CVideoChannelManager::Activate()
{
   if( !( m_nState & State_Initialize ) ) return;
   if( m_nState & State_Activate ) return;

   if( !m_VideoChannelManagementThread.joinable() ) {
      m_bSignalStopVideoChannelManagementThread = FALSE;
      m_VideoChannelManagementThread            = std::thread( Thread_VideoChannelManagement, this );
   }

   m_nState |= State_Activate;
}

void CVideoChannelManager::Deactivate()
{
   if( !( m_nState & State_Initialize ) ) return;
   if( !( m_nState & State_Activate ) ) return;

   if( m_VideoChannelManagementThread.joinable() ) {
      m_bSignalStopVideoChannelManagementThread = TRUE;
      m_VideoChannelManagementThread.join();
      m_bSignalStopVideoChannelManagementThread = FALSE;
   }

   DeactivateAllVideoChannels();

   m_nState &= ~State_Activate;
}

void CVideoChannelManager::ActivateAllVideoCahnnels()
{
   if( !( m_nState & State_Initialize ) ) return;
   if( !( m_nState & State_Activate ) ) return;

   int i;
   for( i = 0; i < (int)m_VideoChannelList.size(); i++ ) {
      m_VideoChannelList[i]->Activate();
   }
}

void CVideoChannelManager::DeactivateAllVideoChannels()
{
   if( !( m_nState & State_Initialize ) ) return;
   if( !( m_nState & State_Activate ) ) return;
   int i;
   for( i = 0; i < (int)m_VideoChannelList.size(); i++ )
      m_VideoChannelList[i]->SignalDeactivate();
   for( i = 0; i < (int)m_VideoChannelList.size(); i++ )
      m_VideoChannelList[i]->Deactivate();
}

int CVideoChannelManager::GetVideoChannelNum()
{
   return (int)m_VideoChannelList.size();
}

BOOL CVideoChannelManager::DoesAnyOneChanneInDeactivating()
{
   BOOL bExist = FALSE;
   int i;
   for( i = 0; i < 1024; i++ ) {
      if( m_nVCState[i] & VCState_Deactivating ) {
         bExist = TRUE;
         break;
      }
   }
   return bExist;
}

BOOL CVideoChannelManager::Read( CXMLIO* pIO )
{
   CCriticalSectionSP co( m_csVideoChannelList );

   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "VCM" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "CurVCID", &m_nCurVCID );
      xmlNode.Attribute( TYPE_ID( int ), "VideoChannelNum", &m_nVideoChannelNum );
#ifdef __SUPPORT_SOLAR_CALCULATOR
      m_SunCalculator.Read( pIO );
#endif
      xmlNode.End();
   }
   return TRUE;
}

BOOL CVideoChannelManager::Write( CXMLIO* pIO )
{
   CCriticalSectionSP co( m_csVideoChannelList );
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "VCM" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "CurVCID", &m_nCurVCID );
      xmlNode.Attribute( TYPE_ID( int ), "VideoChannelNum", &m_nVideoChannelNum );
      m_nVideoChannelNum = (int)m_VideoChannelList.size();
#ifdef __SUPPORT_SOLAR_CALCULATOR
      m_SunCalculator.Write( pIO );
#endif
      xmlNode.End();
   }
   return TRUE;
}

// 비디오 채널 관리자 객체가 임시 정보 저장용으로 사용할지를 설정한다.
// 임시 저장용으로 생성될 경우 필요한 변수들만 저장될 뿐 제대로 동작되지 않는다.
void CVideoChannelManager::SetSetupTemp( BOOL bSetupTemp )
{
   m_bSetupTemp = bSetupTemp;
}

void CVideoChannelManager::SetRemote( BOOL bRemote )
{
   m_bVCMForRemote = bRemote;
}

BOOL CVideoChannelManager::CheckRestart( CVideoChannelManager& from )
{
   return FALSE;
}

BOOL CVideoChannelManager::CheckRestart( int nVCID )
{
   return FALSE;
}

BOOL CVideoChannelManager::CheckChange( int nVCID )
{
   return CheckRestart( nVCID );
}

BOOL CVideoChannelManager::CheckChange( CVideoChannelManager& from )
{
   return FALSE;
}

BOOL CVideoChannelManager::ReadAll( CXMLIO* pIO )
{
   int i;

   if( !Read( pIO ) ) return FALSE;
   for( i = 0; i < m_nVideoChannelNum; i++ ) {
      CVideoChannelInfo vcInfo;
      CXMLNode xmlNode( pIO );
      if( xmlNode.Start( "VideoChannel" ) ) {
         if( !vcInfo.Read( pIO, FALSE ) ) return FALSE;
         xmlNode.End();
      }
      CVideoChannel* pVC = CreateVideoChannel( vcInfo );
      if( m_Desc.m_bUseVCMConfigFile && pVC ) {
         pVC->m_nChannelNo_IVX = vcInfo.m_nVCID;
      }
   }
   return TRUE;
}

BOOL CVideoChannelManager::WriteAll( CXMLIO* pIO )
{
   int i;

   if( !Write( pIO ) ) return FALSE;
   for( i = 0; i < m_nVideoChannelNum; i++ ) {
      if( !m_VideoChannelList[i]->Write( pIO, FALSE ) ) return FALSE;
   }
   return TRUE;
}

BOOL CVideoChannelManager::LoadCFGFile()
{
   std::string strXmlCfgPathName = m_Desc.m_strConfigPath + g_fnVCMSetup;
   if( IsExistFile( strXmlCfgPathName ) ) {
      FileIO file( strXmlCfgPathName.c_str(), FIO_BINARY, FIO_READ );
      CXMLIO xmlIO( &file, XMLIO_Read );
      if( FALSE == ReadAll( &xmlIO ) )
         return FALSE;
   } else
      return FALSE;
   return TRUE;
}

BOOL CVideoChannelManager::SaveCFGFile()
{
   // 설정 내용을 메모리에 Write 한다.
   FileIO buffer;
   CXMLIO xmlIO( &buffer, XMLIO_Write );
   if( FALSE == WriteAll( &xmlIO ) )
      return ( FALSE );

   std::string strXmlCfgPathName = m_Desc.m_strConfigPath + g_fnVCMSetup;
   FileIO file( strXmlCfgPathName.c_str(), FIO_BINARY, FIO_CREATE );
   if( (FILE*)file )
      file.Write( (BYTE*)buffer, 1, buffer.GetLength() );
   return FALSE;
}

CVideoChannel* CVideoChannelManager::CreateVideoChannel( CVideoChannelInfo& info )
{
   CCriticalSectionSP co( m_csVideoChannelList );
   BOOL bNewVideoChannel = FALSE;
   if( info.m_nVCID == -1 ) {
      info.m_nVCID = m_nCurVCID;
      m_nCurVCID++;
      bNewVideoChannel = TRUE;
   }
   CVideoChannel* pVC = new CVideoChannel;
   VC_Desc vc_desc;
   vc_desc.m_pInfo     = &info;
   vc_desc.m_pVCM      = this;
   vc_desc.m_pIPCamMgr = &m_IPCamMgr;
   vc_desc.m_pTimerMgr = m_pTimerMgr.Get();
#ifdef __SUPPORT_SOLAR_CALCULATOR
   vc_desc.m_pSunCalculator = &m_SunCalculator;
#endif
   vc_desc.m_pCBImageProcess     = m_Desc.m_pCBImageProcess;
   vc_desc.m_pCBDisplay          = m_Desc.m_pCBDisplay;
   vc_desc.m_pCBRecord           = m_Desc.m_pCBRecord;
   vc_desc.m_pCBCamEvent         = m_Desc.m_pCBCamEvent;
   vc_desc.m_pCBDecodeVideoFrame = m_Desc.m_pCBDecodeVideoFrame;
   vc_desc.m_pCBEncodeVideoFrame = m_Desc.m_pCBEncodeVideoFrame;
   vc_desc.m_pCBVCConfigToBeLoad = m_Desc.m_pCBVCConfigToBeLoad;
   vc_desc.m_pCBRTSPMetadata     = m_Desc.m_pCBRTSPMetadata;

   pVC->SetSetupTemp( m_bSetupTemp );
   pVC->SetRemote( m_bVCMForRemote );
   pVC->m_nVCID = info.m_nVCID; // 임시 비디오 채널도 비디오 채널 아이디를 가져야함.
   pVC->Create( vc_desc );
   m_VideoChannelList.push_back( pVC );
   if( bNewVideoChannel )
      m_nVideoChannelNum = (int)m_VideoChannelList.size();
   ResetVideoChannelIdx();
   return pVC;
}

BOOL CVideoChannelManager::DeleteVideoChannel( int nVCID )
{
   CCriticalSectionSP co( m_csVideoChannelList );
   BOOL bRet                                 = FALSE;
   std::deque<CVideoChannel*>::iterator iter = m_VideoChannelList.begin();
   std::deque<CVideoChannel*>::iterator end  = m_VideoChannelList.end();
   while( iter != end ) {
      CVideoChannel* pVC = *iter;
      if( pVC->m_nVCID == nVCID ) {
         pVC->SetSetupTemp( m_bSetupTemp );
         if( !m_bSetupTemp ) {
            pVC->Deactivate();
            pVC->Destroy();
         }
         delete pVC;
         m_VideoChannelList.erase( iter );
         m_nVideoChannelNum = (int)m_VideoChannelList.size();
         bRet               = TRUE;
         break;
      }
      iter++;
   }
   ResetVideoChannelIdx();
   return FALSE;
}

CVideoChannel* CVideoChannelManager::GetVideoChannel( int nVCID_Find )
{
   int i;

   CVideoChannel* vc = NULL;
   CCriticalSectionSP co( m_csVideoChannelList );
   int nChNum = (int)m_VideoChannelList.size();
   for( i = 0; i < nChNum; i++ ) {
      int nVCID = m_VideoChannelList[i]->m_nVCID;
      if( nVCID == nVCID_Find ) {
         vc = m_VideoChannelList[i];
         break;
      }
   }
   return vc;
}

CVideoChannel* CVideoChannelManager::GetVideoChannelByIdx( int nVCIdx )
{
   int i;

   CVideoChannel* vc = NULL;
   CCriticalSectionSP co( m_csVideoChannelList );
   int nChNum = (int)m_VideoChannelList.size();
   for( i = 0; i < nChNum; i++ ) {
      if( i == nVCIdx ) {
         vc = m_VideoChannelList[i];
         break;
      }
   }
   return vc;
}

// 비디오 채널이 등록되어있는 순서대로 인덱스를 재정의 한다.
void CVideoChannelManager::ResetVideoChannelIdx()
{
   int i;
   int nChNum = (int)m_VideoChannelList.size();
   for( i = 0; i < nChNum; i++ ) {
      m_VideoChannelList[i]->m_nVCIdx = i;
   }
}

///////////////////////// 카메라 관리 /////////////////////////

void CVideoChannelManager::RegisterICamera( ICamera* pICamera )
{
   CCriticalSectionSP co( m_csCameras );
   m_Cameras.push_back( pICamera );
}

BOOL CVideoChannelManager::UnregisterICamera( ICamera* pICamera )
{
   CCriticalSectionSP co( m_csCameras );
   BOOL bRet                           = FALSE;
   std::deque<ICamera*>::iterator iter = m_Cameras.begin();
   std::deque<ICamera*>::iterator end  = m_Cameras.end();
   while( iter != end ) {
      ICamera* pCurICamera = *iter;
      if( pCurICamera == pICamera ) {
         m_Cameras.erase( iter );
         bRet = TRUE;
         break;
      }
      iter++;
   }
   return bRet;
}

BOOL CVideoChannelManager::IsExistInRegisteredCamera( ICamera* pICamera )
{
   int i;

   CCriticalSectionSP co( m_csCameras );

   int nCameraNum = (int)m_Cameras.size();
   for( i = 0; i < nCameraNum; i++ ) {
      ICamera* pCurCamera = m_Cameras[i];
      if( pCurCamera == pICamera ) {
         return TRUE;
      }
   }
   return FALSE;
}

void CVideoChannelManager::ProcCameraState( ICamera* pICamera )
{
   CVideoChannel* pVC = pICamera->m_pVideoChannel;
   int nICameraIdx    = pICamera->m_nICameraIdx;
   ushort nChFlag     = ushort( 1 ) << nICameraIdx;

   pICamera->CheckingVideoSignalAlive();
   int nCurrState  = pICamera->GetState();
   int& nPrevState = pICamera->m_nPrevState;

   ///////////////////////////////////////////
   // 비디오 신호 관련 처리

   //  비디오 신호 플레그 세팅
   if( nCurrState & ICamera::State_VideoSignalNotOK ) {
      pVC->m_nVideoSignalFlag &= ~nChFlag;
      pVC->m_nNoVideoSignalFlag |= nChFlag;
   } else {
      pVC->m_nVideoSignalFlag |= nChFlag;
      pVC->m_nNoVideoSignalFlag &= ~nChFlag;
   }

   // 연결중 상태 플레그 세팅하기
   if( CheckStateChange( nCurrState, nPrevState, int(ICamera::State_Connectting) ) ) {
      if( nCurrState & ICamera::State_Connectting )
         pVC->m_nConnectingChFlag |= nChFlag;
      else
         pVC->m_nConnectingChFlag &= ~nChFlag;
   }

   // 비디오 신호 없음 이벤트 발생시키기
   if( CheckStateChange( nCurrState, nPrevState, int(ICamera::State_VideoSignalNotOK) ) ) {
      if( m_Desc.m_pCBCamEvent ) {
         VC_PARAM_CAMERA_EVENT input;
         input.pParam     = pVC->m_pParam;
         input.nStreamIdx = pICamera->m_nICameraIdx;
         input.nCamEvent  = VCCamEvent_VideoSignalNotOK;
         m_Desc.m_pCBCamEvent( input );
      }
   }

   // 인증 실패 플레그 세팅 및 이벤트 발생 시키기
   if( CheckStateChange( nCurrState, nPrevState, int(ICamera::State_AuthorizationFailed) ) ) {
      if( nCurrState & ICamera::State_AuthorizationFailed )
         pVC->m_nAuthorizationFailedChFlag |= nChFlag;
      else
         pVC->m_nAuthorizationFailedChFlag &= ~nChFlag;
      if( m_Desc.m_pCBCamEvent ) {
         VC_PARAM_CAMERA_EVENT input;
         input.pParam     = pVC->m_pParam;
         input.nStreamIdx = pICamera->m_nICameraIdx;
         input.nCamEvent  = VCCamEvent_AuthorizationFailed;
         m_Desc.m_pCBCamEvent( input );
      }
   }

   nPrevState = nCurrState;
}

void CVideoChannelManager::VideoChannelManagementProc()
{
   // 비디오 채널 관리자 스레드는 모든 비디오 채널 객체(CVideoChannel)와 모든 카메라 객체(ICamera)를 관리한다.
   int i;

   CMMTimer* m_pTimer = m_pTimerMgr->GetNewTimer( "VideoChannelManagementProc 10ms" );
   m_pTimer->StartTimer();
   m_pTimer->SetFPS( 100.0f );

   BOOL bProcPer1Sec  = FALSE;
   BOOL bProcPer200ms = FALSE;
   BOOL bProcPer100ms = FALSE;
   BOOL bProcPer50ms  = FALSE;
   BOOL bProcPer10ms  = FALSE;

   uint c_loop;
   for( c_loop = 0;; c_loop++ ) {
      if( m_bSignalStopVideoChannelManagementThread )
         break;

      bProcPer1Sec  = ( c_loop % 50 == 0 ) ? TRUE : FALSE;
      bProcPer200ms = ( c_loop % 10 == 0 ) ? TRUE : FALSE;
      bProcPer100ms = ( c_loop % 5 == 0 ) ? TRUE : FALSE;
      bProcPer50ms  = ( c_loop % 2 == 0 ) ? TRUE : FALSE; // 20fps
      bProcPer10ms  = ( c_loop % 1 == 0 ) ? TRUE : FALSE; // 100fps

      //////////     모든 ICamera 타입 객체에 대한 관리   /////////////

      m_csCameras.Lock();
      {
         int nCameraNum = (int)m_Cameras.size();
         for( i = 0; i < nCameraNum; i++ ) {
            ICamera* pICamera = m_Cameras[i];

            if( bProcPer10ms ) {
               ProcCameraState( pICamera );
            } // bProcPer10ms
         }
      }
      m_csCameras.Unlock();

      ///////////  모든 비디오 채널에 대한 관리 ///////////////////

      m_csVideoChannelList.Lock();
      {
         int nVideoChannelNum = (int)m_VideoChannelList.size();
         for( i = 0; i < nVideoChannelNum; i++ ) {
            CVideoChannel* pVC = m_VideoChannelList[i];

            BOOL bDebug = FALSE;
            if( bDebug && bProcPer1Sec ) {
               int j = 0;
               logd( "VS:" );
               for( j = 0; j < MAX_IP_CAMERA_STREAM_NUM; j++ ) {
                  ushort nChFlag = 1 << j;
                  logd( "%d", ( pVC->m_nVideoSignalFlag & nChFlag ) ? TRUE : FALSE );
               }
               logd( " CN:" );
               for( j = 0; j < MAX_IP_CAMERA_STREAM_NUM; j++ ) {
                  ushort nChFlag = 1 << j;
                  logd( "%d", ( pVC->m_nConnectingChFlag & nChFlag ) ? TRUE : FALSE );
               }
               logd( "\n" );
            }

            ////////////////////////////////////////////

            pVC->UpdateCameraFlag();

            // 카메라 이벤트 큐 처리하기
            //   IP 카메라의 디코딩 루프내에서 발생되는 이벤트 발생에 대한 콜백함 수 호출은 이곳에서 처리한다.
            while( pVC->m_VCCameraEventCBParamQueue.size() ) {
               CVCCameraEventCallbackParamItem* pItem = pVC->m_VCCameraEventCBParamQueue.Pop();
               if( pItem ) {
                  if( m_Desc.m_pCBCamEvent ) {
                     VC_PARAM_CAMERA_EVENT input;
                     input.pParam     = pVC->m_pParam;
                     input.nStreamIdx = pItem->m_nStreamIdx;
                     input.nCamEvent  = pItem->m_nCamEvt;
                     m_Desc.m_pCBCamEvent( input );
                     delete pItem;
                  }
               }
            }

            // 밤, 낮 구분하기.
            // IP 카메라 이면서 소스영상으로 밤,낮을 판단하지 않는 경우에만 해당함.
            if( bProcPer1Sec ) {
               pVC->DayNightDetectProc();
            }

#ifdef __SUPPORT_PTZ_CAMERA
            // PTZ 설정을 PTZ제어 객체로 복사하기.
            if( bProcPer1Sec ) {
               pVC->CopyPTZInfoToPTZCtrl();
            }

// 가상 PTZ의 소스 스트림 인지를 판단하는 테이블을 업데이트 한다.
#ifdef __SUPPORT_VIRTUAL_PTZ_CAMERA
            if( bProcPer1Sec ) {
               pVC->UpdateSourceStreamIdxsForVirtualPTZCamers();
            }
#endif
#endif //__SUPPORT_PTZ_CAMERA

            if( bProcPer100ms ) {
               pVC->IPCameraDecodeAndConnectionManageProc();
            }
         }
      }
      m_csVideoChannelList.Unlock();

      m_pTimer->Wait();
   }

   m_pTimerMgr->DeleteTimer( m_pTimer->m_nTimerID );
}

UINT CVideoChannelManager::Thread_VideoChannelManagement( LPVOID pVoid )
{
#ifdef SUPPORT_PTHREAD_NAME
   pthread_setname_np( pthread_self(), "VCM" );
#endif
   CVideoChannelManager* pVCM = (CVideoChannelManager*)pVoid;
   pVCM->VideoChannelManagementProc();
   return ( DONE );
}

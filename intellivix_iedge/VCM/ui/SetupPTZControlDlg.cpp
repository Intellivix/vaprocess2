#include "stdafx.h"
#include "VCM.h"
#include "SetupPTZControlDlg.h"
#include "CameraInfo.h"
#include "CameraProductAndItsProperties.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

static int g_AllDlgCtrlIDs [] = 
{
   IDC_STATIC_DEVICE_MODEL,
   IDC_COMBO_DEVICE_MODEL, 
   IDC_STATIC_PTZ_CONNECTION_TYPE,
   IDC_COMBO_PTZ_CONNECTION_TYPE,
   IDC_STATIC_PTZ_TYPE,
   IDC_STATIC_PTZ_COM_PORT,
   IDC_STATIC_BAUD_RATE,
   IDC_STATIC_CAMERA_ADDRESS,
   IDC_STATIC_IP_ADDRESS,
   IDC_STATIC_PORT_NO,
   IDC_STATIC_ZOOM_PORT_NO, // (mkjang-140613)
   IDC_STATIC_PTZ_CTRL_CHANNEL,
   IDC_STATIC_PTZ_CTRL_CHANNEL_INFO_STATIC,
   IDC_COMBO_PTZ_TYPE,
   IDC_COMBO_PTZ_COM_PORT,
   IDC_COMBO_BAUD_RATE,
   IDC_EDIT_CAM_ADDRESS,
   IDC_EDIT_IP_ADDRESS,
   IDC_EDIT_PORT_NUMBER,
   IDC_EDIT_ZOOM_PORT_NUMBER, // (mkjang_140613)
   IDC_BUTTON_SET_IP_TO_VIDEO_SERVER_IP,
   IDC_BUTTON_SET_DEFAULT_ID_PW,
   IDC_COMBO_PTZ_CTRL_CHANNEL_ID,
   IDC_STATIC_PTZ_CTRL_CHANNEL_INFO,
   IDC_STATIC_ZOOM_MODULE_TYPE,
   IDC_COMBO_ZOOM_MODULE_TYPE,
   IDC_CHECK_TX_ONLY,
   IDC_CHECK_REVERSE_PAN_COORDINATE,
   IDC_CHECK_REVERSE_TILT_COORDINATE,
   IDC_CHECK_ZOOM_PROPORTIONAL_JOG, // (mkjang-140425)
   IDC_CHECK_ZOOM_TRIGGERED_AUTO_FOCUS,
   //IDC_STATIC_OPTICAL_AXIS, 
   //IDC_EDIT_OPTICAL_AXIS_PAN, 
   //IDC_EDIT_OPTICAL_AXIS_TILT, 
   IDC_STATIC_PROTOCOL_VERSION,
   IDC_COMBO_PROTOCOL_VERSION,
   IDC_STATIC_PTZ_INDEX,
   IDC_EDIT_PTZ_INDEX,
   IDC_STATIC_USER_NAME,
   IDC_EDIT_USER_NAME,
   IDC_STATIC_PASSWORD,
   IDC_EDIT_PASSWORD,
   0
};

//////////////////////////////////////////////////////////////////////////
// 
// class CSetupPTZControlDlg
// 
//////////////////////////////////////////////////////////////////////////

IMPLEMENT_DYNAMIC(CSetupPTZControlDlg, CBCGPDialog)

CSetupPTZControlDlg::CSetupPTZControlDlg()
{
   m_pPTZInfo = NULL;
   m_pPTZControlProperty = NULL;
   m_bSupportCustomPTZCommand = FALSE;
   m_nPTZControlType_Linking = -1;
}

CSetupPTZControlDlg::~CSetupPTZControlDlg()
{
}

int CSetupPTZControlDlg::CRHGetDialogID () 
{ 
   return (IDD_SETUP_PTZ_CONTROL);
}

void CSetupPTZControlDlg::DoDataExchange(CDataExchange* pDX)
{
   CBCGPDialog::DoDataExchange(pDX);
   DDX_Control(pDX, IDC_COMBO_PTZ_CONNECTION_TYPE, m_cbPTZConnectionType);
   DDX_Control(pDX, IDC_COMBO_PTZ_TYPE           , m_cbPTZType);
   DDX_Control(pDX, IDC_COMBO_PTZ_COM_PORT       , m_cbPTZComPort);
   DDX_Control(pDX, IDC_COMBO_BAUD_RATE		    , m_cbBaudRate);
   DDX_Control(pDX, IDC_COMBO_PROTOCOL_VERSION   , m_cbProtocolVersion);
   DDX_Control(pDX, IDC_COMBO_ZOOM_MODULE_TYPE   , m_cbZoomModuleType);
   DDX_Control(pDX, IDC_COMBO_PTZ_CTRL_CHANNEL_ID, m_cbPTZChannelId);
   DDX_Control(pDX, IDC_STATIC_PTZ_CTRL_CHANNEL_INFO, m_stcChannelInfo);

   DDX_Text(pDX, IDC_EDIT_IP_ADDRESS       , m_pPTZInfo->m_strIPAddress); // (xinu_bc23)
   DDX_Text(pDX, IDC_EDIT_USER_NAME        , m_pPTZInfo->m_strUserName);
   DDX_Text(pDX, IDC_EDIT_PASSWORD         , m_pPTZInfo->m_strPassword);
   DDX_Text(pDX, IDC_EDIT_OPTICAL_AXIS_PAN , m_pPTZInfo->m_pvOpticalAxisOffset.P);
   DDX_Text(pDX, IDC_EDIT_OPTICAL_AXIS_TILT, m_pPTZInfo->m_pvOpticalAxisOffset.T);

   DDX_Text(pDX, IDC_EDIT_PORT_NUMBER        , m_pPTZInfo->m_nPortNo);
   DDX_Text(pDX, IDC_EDIT_ZOOM_PORT_NUMBER   , m_pPTZInfo->m_nZoomPortNo); // (mkjang-140613)
   DDX_Text(pDX, IDC_EDIT_PTZ_INDEX          , m_pPTZInfo->m_nPTZIdx);

   DDX_Check (pDX,IDC_CHECK_REVERSE_PAN_COORDINATE   ,m_pPTZInfo->m_bReversePanCoordinate);
   DDX_Check (pDX,IDC_CHECK_REVERSE_TILT_COORDINATE  ,m_pPTZInfo->m_bReverseTiltCoordinate);
   DDX_Check (pDX,IDC_CHECK_ZOOM_PROPORTIONAL_JOG    ,m_pPTZInfo->m_bZoomProportionalJog); // (mkjang-140425)
   DDX_Check (pDX,IDC_CHECK_ZOOM_TRIGGERED_AUTO_FOCUS,m_pPTZInfo->m_bZoomTriggeredAutoFocus);
   // 실시간 절대위치 얻기가 안되는 카메라는 Tx Only 설정값을 반영하지 않는다. 
   if (FALSE == (m_pPTZControlProperty && m_pPTZControlProperty->m_nCapabilities & PTZCtrlCap_RealTimeGetAbsPosNotSupported)) 
      DDX_Check (pDX,IDC_CHECK_TX_ONLY, m_pPTZInfo->m_nTxOnly);
}

BOOL CSetupPTZControlDlg::OnInitDialog()
{
	CBCGPDialog::OnInitDialog ();
   m_bInitUI = FALSE;

   SetMultiLangInfo(GetSafeHwnd(), IDD_TEXT(IDD_SETUP_PTZ_CONTROL));

   int        i;
   CRect      rtCtrl;
   CString    strText;

   // PTZ Control Connect Type
   static int CtrlID_Serial [ ] = 
   {
      IDC_STATIC_PTZ_COM_PORT,
      IDC_STATIC_BAUD_RATE   ,
      IDC_COMBO_PTZ_COM_PORT ,
      IDC_COMBO_BAUD_RATE    ,
      0                          
   };

   static int CtrlID_Network [ ] = 
   {
      IDC_STATIC_IP_ADDRESS ,
      IDC_STATIC_PORT_NO,
      IDC_EDIT_IP_ADDRESS ,
      IDC_EDIT_PORT_NUMBER ,
      0                          
   };

   for (i = 0; CtrlID_Serial[i]; i++) {
      GetWndRect (rtCtrl, this, GetDlgItem (CtrlID_Serial[i]));
      GetDlgItem (CtrlID_Network[i])->SetWindowPos (NULL, rtCtrl.left, rtCtrl.top, 0,0, SWP_NOSIZE | SWP_NOZORDER);
   }

   // 정적 컨트롤과 데이터 컨트롤들의 위치를 기억해둔다. 
   static int StaticControls [ ] = {
      IDC_STATIC_PTZ_TYPE,
      IDC_STATIC_PTZ_COM_PORT,
      IDC_STATIC_BAUD_RATE,
      IDC_STATIC_CAMERA_ADDRESS,
      0
   };

   m_vtStaticCtrlPos.clear (   );
   for (i = 0; StaticControls[i]; i++) {
      GetWndRect (rtCtrl, this, GetDlgItem (StaticControls[i]));
      m_vtStaticCtrlPos.push_back (CPoint (rtCtrl.left, rtCtrl.top));
   }

   static int DataControls [ ] = {
      IDC_COMBO_PTZ_TYPE,
      IDC_COMBO_PTZ_COM_PORT,
      IDC_COMBO_BAUD_RATE,
      IDC_EDIT_CAM_ADDRESS,
      0
   };

   m_vtDataCtrlPos.clear (   );
   for (i = 0; DataControls[i]; i++) {
      GetWndRect (rtCtrl, this, GetDlgItem (DataControls[i]));
      m_vtDataCtrlPos.push_back (CPoint (rtCtrl.left, rtCtrl.top));
   }

   // COM port
   m_cbPTZComPort.SetRedraw (FALSE);
   m_cbPTZComPort.ResetContent (   );
   for (i = 0; i < 9; i++)  {
      strText.Format (_T("COM %d"), i+1);
      m_cbPTZComPort.AddString (strText);
      m_cbPTZComPort.SetItemData (i, DWORD_PTR (i+1));
   }
   m_cbPTZComPort.SetRedraw (TRUE);
   if (m_pPTZInfo)
      SetCurSel_ByData (&m_cbPTZComPort, m_pPTZInfo->m_nCOMPortNo);

   // BaudRate
   m_cbBaudRate.SetRedraw (FALSE);
   m_cbBaudRate.ResetContent (   );
   for (i = 0 ; i < sizeof(BaudRates) / sizeof(int) ; i++) {
      strText.Format(_T("%d BPS"), BaudRates[i]);
      m_cbBaudRate.AddString(strText);
   }
   m_cbBaudRate.SetRedraw (TRUE);
   if (m_pPTZInfo)
      SetCurSel_ByIndex (&m_cbBaudRate, m_pPTZInfo->m_nBaudRateID);

   // Optical Axis
   strText.Format(_T("%3.1f"), m_pPTZInfo->m_pvOpticalAxisOffset.P);
   SetDlgItemText(IDC_EDIT_OPTICAL_AXIS_PAN, strText);
   strText.Format(_T("%3.1f"), m_pPTZInfo->m_pvOpticalAxisOffset.T);
   SetDlgItemText(IDC_EDIT_OPTICAL_AXIS_TILT, strText);

   //Cam Address
   strText.Format(_T("%d"), m_pPTZInfo->m_nCamAddress);
   SetDlgItemText(IDC_EDIT_CAM_ADDRESS, strText);

   // PTZ 제어 채널 링크
   InitLinkingPTZChannelComboBox (   );    // 원본채널 콤보박스
   OnCbnSelchangeComboPTZCtrlChannelId (   );

   // 윈도우 크기를 그룹 컨트롤 보다 약간 크게..
   GetDlgItem (IDC_STATIC_PTZ_GROUP)->GetClientRect (rtCtrl);
   SetWindowPos (NULL, 0,0,rtCtrl.Width (), rtCtrl.Height ()+20, SWP_NOMOVE | SWP_NOZORDER);

   InitUI (   );
   LoadLanguage();
   m_bInitUI = TRUE;
   
   return TRUE;
}

void CSetupPTZControlDlg::LoadLangEvent()
{   
   UpdatePTZConnectionTypeComboBox (   );
}

////////////////////////////////////////////////////////////////////////////////////////////
// Message Handlers

BEGIN_MESSAGE_MAP(CSetupPTZControlDlg, CBCGPDialog)
   ON_CBN_SELCHANGE(IDC_COMBO_DEVICE_MODEL         , OnCbnSelchangeComboDeviceModel)
   ON_CBN_SELCHANGE(IDC_COMBO_PTZ_CONNECTION_TYPE  , OnCbnSelchangeComboPtzConnectionType)
   ON_CBN_SELCHANGE(IDC_COMBO_PTZ_TYPE			      , OnCbnSelchangeComboPtzType)
   ON_CBN_SELCHANGE(IDC_COMBO_PTZ_COM_PORT	      , OnCbnSelchangeComboPtzComPort)
   ON_CBN_SELCHANGE(IDC_COMBO_BAUD_RATE		      , OnCbnSelchangeBaudRate)
   ON_CBN_SELCHANGE(IDC_COMBO_PROTOCOL_VERSION     , OnCbnSelchangeProtocolVersion)
   ON_CBN_SELCHANGE(IDC_COMBO_PTZ_CTRL_CHANNEL_ID  , OnCbnSelchangeComboPTZCtrlChannelId)
   ON_CBN_SELCHANGE(IDC_COMBO_ZOOM_MODULE_TYPE     , OnCbnSelchangeComboZoomModuleType)
   ON_EN_CHANGE(IDC_EDIT_CAM_ADDRESS               , OnEditChangeCamAddress)
   ON_EN_CHANGE(IDC_EDIT_IP_ADDRESS                , DoUpdateData)
   ON_EN_CHANGE(IDC_EDIT_USER_NAME                 , DoUpdateData)
   ON_EN_CHANGE(IDC_EDIT_PASSWORD                  , DoUpdateData)
   ON_EN_CHANGE(IDC_EDIT_PORT_NUMBER               , DoUpdateData)
   ON_EN_CHANGE(IDC_EDIT_ZOOM_PORT_NUMBER          , DoUpdateData) // (mkjang-140613)
   ON_EN_CHANGE(IDC_EDIT_OPTICAL_AXIS_PAN          , DoUpdateData)
   ON_EN_CHANGE(IDC_EDIT_OPTICAL_AXIS_TILT         , DoUpdateData)
   ON_EN_CHANGE(IDC_EDIT_PTZ_INDEX                 , DoUpdateData)
   ON_BN_CLICKED(IDC_CHECK_TX_ONLY                 , DoUpdateData)
   ON_BN_CLICKED(IDC_CHECK_REVERSE_PAN_COORDINATE  , DoUpdateData) 
   ON_BN_CLICKED(IDC_CHECK_REVERSE_TILT_COORDINATE , DoUpdateData) 
   ON_BN_CLICKED(IDC_CHECK_ZOOM_PROPORTIONAL_JOG   , DoUpdateData) // (mkjang-140425)
   ON_BN_CLICKED(IDC_BUTTON_SET_IP_TO_VIDEO_SERVER_IP, &CSetupPTZControlDlg::OnBnClickedButtonSetIpToVideoServerIp)
   ON_BN_CLICKED(IDC_BUTTON_SET_DEFAULT_ID_PW,        &CSetupPTZControlDlg::OnBnClickedButtonSet)

END_MESSAGE_MAP()


void CSetupPTZControlDlg::OnCbnSelchangeComboDeviceModel()
{
   CComboBox* pComboBox;
   pComboBox = (CComboBox*) GetDlgItem (IDC_COMBO_DEVICE_MODEL);
   int nCurSelIdx = pComboBox->GetCurSel ( );
   int nIPCameraType = pComboBox->GetItemData (pComboBox->GetCurSel ( ));
   if (m_nVideoChannelType == VideoChannelType_IPCamera) {
      m_pVCInfo->m_IPCamInfo.m_nIPCameraTypeForExtraPTZCtrl = nIPCameraType;
      m_pIPCameraProperty = GetIPCameraProperty (nIPCameraType);
      if (m_pIPCameraProperty) {
         if (m_pPTZControlProperty) 
            m_pPTZControlProperty = GetPTZControlProperty (m_pPTZControlProperty->m_nPTZControlType);
      }
      ShowEnableCtrls (   );
   }
}

void CSetupPTZControlDlg::OnCbnSelchangeComboPtzConnectionType()
{
   GetCurData (&m_cbPTZConnectionType, m_pPTZInfo->m_nConnectType);
   UpdatePTZConnectionTypeComboBox (   );
   if (m_nVideoChannelType == VideoChannelType_IPCamera) {
      if (PTZCtrlConnType_OtherIPCamera == m_pPTZInfo->m_nConnectType) {
         OnCbnSelchangeComboDeviceModel (   );
      }
      else {
         // 별도 IP카메라 연결방식 이었다가 연결방식을 변경하는 경우 
         // IP 카메라에 맞는 속성 객체에 대한 포인터를 얻는다. 
         m_pIPCameraProperty = GetIPCameraProperty (m_pVCInfo->m_IPCamInfo.m_nIPCameraType);
         if (m_pIPCameraProperty) {
            if ((m_pIPCameraProperty->m_nPTZControlType / 10000) != (m_pPTZInfo->m_nPTZControlType / 10000))
               m_pPTZControlProperty = GetPTZControlProperty (m_pIPCameraProperty->m_nPTZControlType);
         }
      }
   }
   ShowEnableCtrls ();
}

void CSetupPTZControlDlg::OnCbnSelchangeComboPtzType()
{
   GetCurData (&m_cbPTZType, m_pPTZInfo->m_nPTZControlType);
   if (m_pPTZInfo) {
      m_pPTZControlProperty = GetPTZControlProperty (m_pPTZInfo->m_nPTZControlType);
   }
   ShowEnableCtrls ();
}

void CSetupPTZControlDlg::OnCbnSelchangeComboPtzComPort()
{
   GetCurData (&m_cbPTZComPort, m_pPTZInfo->m_nCOMPortNo);
}

void CSetupPTZControlDlg::OnCbnSelchangeBaudRate()
{
   m_pPTZInfo->m_nBaudRateID = m_cbBaudRate.GetCurSel();
}

void CSetupPTZControlDlg::OnCbnSelchangeProtocolVersion()
{
   GetCurData (&m_cbProtocolVersion, m_pPTZInfo->m_nProtocolVersion);
}

void CSetupPTZControlDlg::OnCbnSelchangeComboPTZCtrlChannelId()
{
   m_nPTZControlType_Linking = -1;
   if (PTZCtrlConnType_LinkToPTZCtrl != m_pPTZInfo->m_nConnectType) {
      return;
   }
   CComboBox* pComboBox = (CComboBox*)GetDlgItem (IDC_COMBO_PTZ_CTRL_CHANNEL_ID);
   GetCurData (pComboBox, m_pPTZInfo->m_nVCID_LikingPTZCtrl); 
   CVideoChannel* pVideoChannel = m_pVCM->GetVideoChannel (m_pPTZInfo->m_nVCID_LikingPTZCtrl);
   // PTZ 제어 채널 정보 출력
   if (pVideoChannel) {
      CStatic* pStatic = (CStatic*)GetDlgItem (IDC_STATIC_PTZ_CTRL_CHANNEL_INFO);
      std::string strSourceChannelInfo;
      std::string str;
      // PTZ 타입
      int nPTZControltype = 0;
      switch (pVideoChannel->m_nVideoChannelType) {
      case VideoChannelType_AnalogCamera:
         nPTZControltype = pVideoChannel->m_PTZInfo.m_nPTZControlType;
         break;
      case VideoChannelType_IPCamera:
         nPTZControltype = pVideoChannel->m_IPCamInfo.m_PTZInfo.m_nPTZControlType;
         break;
      case VideoChannelType_VirtualPTZ:
         nPTZControltype = PTZControlType_VirtualPTZ;
         break;
      }
      m_nPTZControlType_Linking = nPTZControltype;
      std::string strPTZControlType = PTZControlTypeString.GetString (nPTZControltype);      
      strSourceChannelInfo += strPTZControlType;
      pStatic->SetWindowText (strSourceChannelInfo.c_str());
   }
}

void CSetupPTZControlDlg::OnCbnSelchangeComboZoomModuleType()
{
   GetCurData (&m_cbZoomModuleType, m_pPTZInfo->m_nZoomModuleType);
}

void CSetupPTZControlDlg::OnEditChangeCamAddress()
{
	if (!m_bInitUI) return;
	CString s;
	GetDlgItemText(IDC_EDIT_CAM_ADDRESS, s);
	m_pPTZInfo->m_nCamAddress = atoi(s);
}

void CSetupPTZControlDlg::DoUpdateData ()
{
   UpdateData (TRUE);
}

////////////////////////////////////////////////////////////////////////////////////////////
// Implementation

void CSetupPTZControlDlg::Initialize (int nVideoChannelType, int nVCSetupMode, CVideoChannelManager* pVCM, CVideoChannelInfo* pVCInfo, CPTZCtrlInfo* pPTZInfo)
{
   m_nVideoChannelType = nVideoChannelType;
   m_nVCSetupMode      = nVCSetupMode;
   m_pVCM              = pVCM;
   m_pVCInfo           = pVCInfo;
   m_pPTZInfo          = pPTZInfo;
}

void CSetupPTZControlDlg::InitUI (   )
{
   m_bInitUI = FALSE;

   if (m_pPTZInfo) {
      m_pPTZControlProperty = GetPTZControlProperty (m_pPTZInfo->m_nPTZControlType);
   }
   if (m_nVideoChannelType == VideoChannelType_IPCamera) {
      m_pIPCameraProperty = NULL;
      if (m_pPTZInfo->m_nConnectType == PTZCtrlConnType_OtherIPCamera) {
         m_pIPCameraProperty = GetIPCameraProperty (m_pVCInfo->m_IPCamInfo.m_nIPCameraTypeForExtraPTZCtrl);
      }
      else {
         m_pIPCameraProperty = GetIPCameraProperty (m_pVCInfo->m_IPCamInfo.m_nIPCameraType);
      }
   }
   else {
      m_pIPCameraProperty    = NULL;
   }

   int        i;
   CRect      rtCtrl;
   CString    strText;
   CComboBox* pComboBox;

   if (VideoChannelType_IPCamera == m_nVideoChannelType) {
      // Device Model - IP 카메라 종류 추가
      pComboBox = (CComboBox*) GetDlgItem (IDC_COMBO_DEVICE_MODEL);
      pComboBox->SetRedraw (FALSE);
      pComboBox->ResetContent (   );
      std::vector<int> vtIPCameraType;
      GetIPCameraTypeListByCapabilities (IPCamCaps_SupportOtherConnectionForPTZControl, vtIPCameraType);

      int nItem = vtIPCameraType.size ();
      for (i = 0; i < nItem; i++) {
         int nIPCameraType = vtIPCameraType[i];
         pComboBox->AddString (GetIPCameraProductName (nIPCameraType).c_str());
         pComboBox->SetItemData (i, (DWORD_PTR)nIPCameraType);
      }
      pComboBox->SetRedraw (TRUE);
      SetCurSel_ByData (pComboBox, m_pVCInfo->m_IPCamInfo.m_nIPCameraTypeForExtraPTZCtrl);
   }

   // LoadLangEvent() 에서 UpdatePTZConnectionTypeComboBox 함수를 호출 하도록 함
   //UpdatePTZConnectionTypeComboBox (   );
   UpdatePTZContorlTypeComboBox (   );
   ShowEnableCtrls ();

   if( IS_PRODUCT(__PRODUCT_B200) )
      GetDlgItem(IDC_CHECK_TX_ONLY)->ShowWindow(SW_HIDE);

   SetRedraw (TRUE);
   
   m_bInitUI = TRUE;
}

void CSetupPTZControlDlg::SetCtrlPos (int* pCtrlIDs, std::vector<CPoint>& vtCtrlPos, BOOL bShow)
{
   // vcCtrlPos 배열에 저장되어 있는 위치로 pCtrlIDs 아이디를 갖는 컨트롤들을 이동시킨다. 
   int i;
   for (i = 0; pCtrlIDs[i]; i++) {
      CWnd* pWnd = GetDlgItem (pCtrlIDs[i]);
      CPoint pnt = vtCtrlPos[i];
      pWnd->SetWindowPos (NULL, pnt.x, pnt.y, 0, 0, SWP_NOSIZE | SWP_HIDEWINDOW | SWP_NOZORDER);
   }
   for (i = 0; pCtrlIDs[i]; i++) {
      CWnd* pWnd = GetDlgItem (pCtrlIDs[i]);
      pWnd->ShowWindow (bShow);
   }
}

void CSetupPTZControlDlg::SetCtrlPos (CWnd* pInsertBeforeCtrl, CWnd* pCtrl, int nOffsetX, int nOffsetY)
{
   CRect rtCtrl;
   GetWndRect (rtCtrl, this, pInsertBeforeCtrl);
   pCtrl->SetWindowPos (NULL, rtCtrl.right + nOffsetX, rtCtrl.top + nOffsetY, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
}

void CSetupPTZControlDlg::ShowCtrls (int* pCtrlIDs, BOOL bShow)
{
   int i;
   for (i = 0; pCtrlIDs[i]; i++) {
      CWnd* pWnd = GetDlgItem (pCtrlIDs[i]);
      if(pWnd) {
         pWnd->SetWindowPos (NULL, 0,0,0,0, SWP_NOSIZE | SWP_NOMOVE | SWP_NOZORDER);
         pWnd->ShowWindow (bShow);
      }
   }
}

void CSetupPTZControlDlg::UpdatePTZConnectionTypeComboBox (   )
{
   int i;

   // 연결 방식 콤보박스
   m_cbPTZConnectionType.SetRedraw (FALSE);
   m_cbPTZConnectionType.ResetContent (   );
   // 아날로그 타입일 때 연결방식 (PC 시리얼 포트, ezTCP);
   if (m_nVideoChannelType == VideoChannelType_AnalogCamera) {
      for (i = 0; i < (int) PTZConnectionTypeString_Analog.size(); i++)  {
         m_cbPTZConnectionType.AddString (PTZConnectionTypeString_Analog[i].m_String.c_str());
         m_cbPTZConnectionType.SetItemData (i, DWORD_PTR (PTZConnectionTypeString_Analog[i].m_nID));
      }
   }
   // IP카메라(비디오서버) 타입일 때 연결방식 (비디오서버의 시리얼 포트, ezTCP);
   else if (m_nVideoChannelType == VideoChannelType_IPCamera) {
      int nItemCount = 0;
      for (i = 0; i < (int) PTZConnectionTypeString_IPCamera.size(); i++)  {
         CIDToString& ptzConnectionTypeString = PTZConnectionTypeString_IPCamera[i];
         if (m_pIPCameraProperty && (m_pIPCameraProperty->m_nIPCameraType == IPCameraType_RTSP) &&         // RTSP 연결은 기본제어 API가 없으므로 넘어간다.  
            (ptzConnectionTypeString.m_nID == PTZCtrlConnType_Basic)) 
            continue;
         m_cbPTZConnectionType.AddString (ptzConnectionTypeString.m_String.c_str());
         m_cbPTZConnectionType.SetItemData (nItemCount, DWORD_PTR (ptzConnectionTypeString.m_nID));
         nItemCount++;
      }
   }
   m_cbPTZConnectionType.SetRedraw (TRUE);
   if (m_pPTZInfo)
      SetCurSel_ByData (&m_cbPTZConnectionType, m_pPTZInfo->m_nConnectType);
}

void CSetupPTZControlDlg::UpdatePTZContorlTypeComboBox (   )
{
   int i;
   // PTZ 카메라 종류 (Combo box)
   m_cbPTZType.SetRedraw (FALSE);
   m_cbPTZType.ResetContent (   );
   if (IsShowSameIPCamManufacurePTZControlTypeComboBox ()) {
      if (m_pIPCameraProperty) {
         std::vector<int> vtPTZControlTypeList;
         GetSameManufacturePTZList (m_pIPCameraProperty->m_nPTZControlType, vtPTZControlTypeList);
         int j;
         int nAddItemCount = 0;
         int nStrListNum = (int)PTZControlTypeString.size ();
         for (i = 0; i < nStrListNum; i++) {
            int nItemSize = int (vtPTZControlTypeList.size ());
            for (j = 0; j < nItemSize; j++) {
               int nPTZControlType = vtPTZControlTypeList[j];
               if (PTZControlTypeString[i].m_nID == nPTZControlType) {
                  m_cbPTZType.AddString (PTZControlTypeString[i].m_String.c_str());
                  m_cbPTZType.SetItemData (nAddItemCount, DWORD_PTR (nPTZControlType));
                  nAddItemCount++;
               }
            }
         }
      }
   }
   else {
      for (i = 0; SerialPTZControlList [i]; i++)  {
         int nPTZControlType = SerialPTZControlList [i];
         if (IsVideoServerSupportPTZControl () && SerialPTZControlList[i] == -1)
            m_cbPTZType.AddString (MLANG_WND(IDS_CONTROL_BY_PTZ_API)); // 1563: "직접 PTZ 명령 전송"
         else {
            std::string strPTZControlType = PTZControlTypeString.GetString (nPTZControlType);
            m_cbPTZType.AddString (strPTZControlType.c_str());
         }         
         // 아래 SetCurSel_ByData (&m_cbPTZType, m_pPTZInfo->m_nPTZControlType); 에서 초기 값으로 -1을 세팅해서
         // GetPTZControlProperty 값을 가져올때 제대로된 값을 가져 오지 못한다 JJong (일감#194)
         if( -1 == nPTZControlType )
            nPTZControlType = 0;

         m_cbPTZType.SetItemData (i, DWORD_PTR (nPTZControlType));
      }
   }
   
   m_cbPTZType.SetRedraw (TRUE);
   if (m_pPTZInfo)
      SetCurSel_ByData (&m_cbPTZType, m_pPTZInfo->m_nPTZControlType);
}

void CSetupPTZControlDlg::InitLinkingPTZChannelComboBox (   )
{
   // 원본채널 콤보박스
   CComboBox* pComboBox = (CComboBox*)GetDlgItem (IDC_COMBO_PTZ_CTRL_CHANNEL_ID);
   int i;
   for (i = 0; i < m_pVCM->GetVideoChannelNum (   ); i++) {
      CVideoChannel* pVC_Cur = m_pVCM->m_VideoChannelList[i];
      if (pVC_Cur->m_nVCID == m_pVCInfo->m_nVCID) 
         continue;
      if (!pVC_Cur->m_bPTZCamera) 
         continue;
      CString strItem; 
      int nVCIdx = pVC_Cur->m_nVCIdx;
      if (pVC_Cur->m_nChannelNo_IVX >= 0) {
         nVCIdx = pVC_Cur->m_nChannelNo_IVX;
      }
      strItem.Format (_T("%d"), nVCIdx+1);
      AddComboBoxItem (pComboBox, strItem, pVC_Cur->m_nVCID);
   }
   if (0 == pComboBox->GetCount (   )) {
      AddComboBoxItem (pComboBox, _T("Not Exist"), -1);
      m_pPTZInfo->m_nVCID_LikingPTZCtrl = -1;
   }
   SetCurSel_ByData (pComboBox, m_pPTZInfo->m_nVCID_LikingPTZCtrl);
}

BOOL CSetupPTZControlDlg::IsVideoServerSupportPTZControl (   )
{
   BOOL bVideoServerSupportPTZControl = FALSE;
   if (m_nVideoChannelType == VideoChannelType_IPCamera && 
      m_pIPCameraProperty && m_pIPCameraProperty->m_nIPCameraType != IPCameraType_RTSP)
      bVideoServerSupportPTZControl = TRUE;
   return bVideoServerSupportPTZControl;
}

BOOL CSetupPTZControlDlg::IsShowSameIPCamManufacurePTZControlTypeComboBox (   )
{
   BOOL bShowSameIPCamManufacurePTZControlType = FALSE;
   if (VideoChannelType_IPCamera == m_nVideoChannelType) {
      switch (m_pPTZInfo->m_nConnectType) {
      case PTZCtrlConnType_Basic:
      case PTZCtrlConnType_OtherIPCamera:
         if (!IsSupportCustomPTZCommand ()) {
            bShowSameIPCamManufacurePTZControlType = TRUE;
         }
         // (mkjang-140422)
         else if (IsSupportCustomPTZCommand () && (IPCamCaps_ShowSameManufacturer & m_pIPCameraProperty->m_nCapabilities) )
         {
            bShowSameIPCamManufacurePTZControlType = TRUE;
         }
         break;
      default:
         break;
      }
   }
   return bShowSameIPCamManufacurePTZControlType;
}

BOOL CSetupPTZControlDlg::IsSupportCustomPTZCommand (   )
{
   BOOL bSupportCustomPTZCommand = FALSE;
   if (m_pIPCameraProperty) {
      if (m_pIPCameraProperty->m_nCapabilities & IPCamCaps_SupportCustomPTZCommand) {
         bSupportCustomPTZCommand = TRUE;
      }
   }
   return bSupportCustomPTZCommand;
}

BOOL CSetupPTZControlDlg::IsSupportMultiPTZControlIPCamera (   )
{
   BOOL bSupportMultiPTZControlIPCamera = FALSE;
   if (m_pIPCameraProperty && m_pPTZInfo) {
      if (m_pIPCameraProperty->m_nCapabilities & IPCamCaps_SupportMultiPTZControl) {
         if (PTZCtrlConnType_LinkToPTZCtrl != m_pPTZInfo->m_nConnectType) {
            bSupportMultiPTZControlIPCamera = TRUE;
         }
      }
   }
   return bSupportMultiPTZControlIPCamera;
}

BOOL CSetupPTZControlDlg::IsShowZoomModuleTypeCtrls (   )
{
   if (m_pPTZControlProperty) {
      if (m_pPTZControlProperty->m_nCapabilities & PTZCtrlCap_MultiZoomModule)
         return TRUE;
      if (m_nPTZControlType_Linking > 0)
      {
         PTZControlProperty* pProperty_Linking = GetPTZControlProperty (m_nPTZControlType_Linking);
         if (pProperty_Linking) {
            if (pProperty_Linking->m_nCapabilities & PTZCtrlCap_MultiZoomModule)
               return TRUE;
         }
      }
   }
   return FALSE;
}

void CSetupPTZControlDlg::SetPTZCtrlGroupCtrlWndPos (   ) 
{
   CRect rtCtrl;
   m_MaxBottomPosAmongVisibleCtrls = 0;
   int i;
   int nDlgCtrlID_MaxBottom = 0;
   int nControlHeight = 0;
   for (i = 0; g_AllDlgCtrlIDs[i]; i++) {
      int nDlgCtrlID = g_AllDlgCtrlIDs [i];
      
      if( IS_PRODUCT(__PRODUCT_B200) ) {      
         if( IDC_CHECK_TX_ONLY == nDlgCtrlID )
            continue;
      }

      CWnd* pCtrlWnd = GetDlgItem (nDlgCtrlID);
      GetWndRect (rtCtrl, this, pCtrlWnd);
      if (pCtrlWnd->GetStyle () & WS_VISIBLE) {
         if (rtCtrl.bottom > m_MaxBottomPosAmongVisibleCtrls) {
            m_MaxBottomPosAmongVisibleCtrls = rtCtrl.bottom;
            nDlgCtrlID_MaxBottom = nDlgCtrlID;
            nControlHeight = rtCtrl.Height ();
         }
      }
   }

   CRect rtPTZGroup;
   GetDlgItem (IDC_STATIC_PTZ_GROUP)->GetClientRect (rtPTZGroup);
   GetWndRect (rtPTZGroup, this, GetDlgItem (IDC_STATIC_PTZ_GROUP));

   rtPTZGroup.bottom = m_MaxBottomPosAmongVisibleCtrls + nControlHeight / 2;
   GetDlgItem (IDC_STATIC_PTZ_GROUP)->SetWindowPos (NULL, 0, 0, rtPTZGroup.Width (), rtPTZGroup.Height(), SWP_NOMOVE | SWP_NOZORDER);
}

void CSetupPTZControlDlg::ShowEnableCtrls()
{
   CRect rtCtrl;

   // 모든 컨트롤 숨기기
   ShowCtrls (g_AllDlgCtrlIDs, FALSE);
   if (m_nVCSetupMode & VCSetupMode_DoNotAllowPTZCamera) {
      GetDlgItem (IDC_STATIC_PTZ_GROUP)->ShowWindow (FALSE);
   }
   else
   {
      int i;
      if (NULL == m_pPTZInfo) return;
      ASSERT(m_pPTZInfo);

      // 직접 명령 전송 모드이면 PTZ 제어 종류 설정 컨트롤만 제외하고 모두 Disable 시킨다. 
      BOOL bEnable;
      for (i = 0; g_AllDlgCtrlIDs[i]; i++) {
         if (g_AllDlgCtrlIDs[i] == IDC_STATIC_PTZ_TYPE || g_AllDlgCtrlIDs[i] == IDC_COMBO_PTZ_TYPE ||
            g_AllDlgCtrlIDs[i] == IDC_STATIC_PTZ_CONNECTION_TYPE || g_AllDlgCtrlIDs[i] == IDC_COMBO_PTZ_CONNECTION_TYPE)
            continue;
         bEnable = m_pPTZInfo->m_nPTZControlType != 0;
         int nControlID = g_AllDlgCtrlIDs[i];

         if( IS_PRODUCT(__PRODUCT_B200) ) {      
            if( IDC_CHECK_TX_ONLY == nControlID )
               continue;
         }

         GetDlgItem (nControlID)->EnableWindow (bEnable);
      }
      // 항상 보여야 할 컨트롤들을 우선 Show 시킨다. 
      static int ConnectionTypeCtrls [] = 
      {
         IDC_STATIC_PTZ_CONNECTION_TYPE,
         IDC_COMBO_PTZ_CONNECTION_TYPE,
         IDC_CHECK_REVERSE_PAN_COORDINATE,
         IDC_CHECK_REVERSE_TILT_COORDINATE,
         IDC_CHECK_ZOOM_PROPORTIONAL_JOG,
         IDC_EDIT_OPTICAL_AXIS_TILT, 
         IDC_STATIC_PTZ_INDEX,
         IDC_EDIT_PTZ_INDEX,
         IDC_CHECK_TX_ONLY,
         0
      };

      if( IS_PRODUCT(__PRODUCT_B200) ) {
        ConnectionTypeCtrls[8] = 0; // IDC_CHECK_TX_ONLY 제외
      }

      ShowCtrls (ConnectionTypeCtrls, TRUE);

      // 팬&틸트 방향 뒤집기 설정 컨트롤 
      BOOL bShow = FALSE;
      if (0 < m_pPTZInfo->m_nPTZControlType && m_pPTZInfo->m_nPTZControlType < 10000)
         bShow = TRUE;
      if (m_pPTZControlProperty && m_pPTZControlProperty->m_nCapabilities & PTZCtrlCap_PanTiltCoordinateChangeable) {
         bShow = TRUE;
      }
      CButton* pCheckReversePan = (CButton*)GetDlgItem (IDC_CHECK_REVERSE_PAN_COORDINATE);
      CButton* pCheckReverseTilt = (CButton*)GetDlgItem (IDC_CHECK_REVERSE_TILT_COORDINATE);
      pCheckReversePan->ShowWindow (bShow);
      pCheckReverseTilt->ShowWindow (bShow);

      if(!bShow) {
         pCheckReversePan->SetCheck(FALSE);
         pCheckReverseTilt->SetCheck(FALSE);
         m_pPTZInfo->m_bReversePanCoordinate = FALSE;
         m_pPTZInfo->m_bReverseTiltCoordinate = FALSE;
      }

      // Zoom 이동 후 자동 Auto Focus 작동 지원 시
      bShow = FALSE;
      if (m_pPTZControlProperty && m_pPTZControlProperty->m_nCapabilities & PTZCtrlCap_ZoomTriggeredAutoFocus)
         bShow = TRUE;

      CButton* pCheck = (CButton *)GetDlgItem(IDC_CHECK_ZOOM_TRIGGERED_AUTO_FOCUS);
      pCheck->ShowWindow(bShow);
      pCheck->EnableWindow(bShow);
      if (!bShow) 
         pCheck->SetCheck(FALSE);
      
      // 실시간 절대위치 얻기가 안되는 카메라는 Tx Only를 체크한 것 처럼 보이게 한다. 
      CButton* pButton = (CButton*)GetDlgItem (IDC_CHECK_TX_ONLY);
      pButton->EnableWindow (TRUE);
      if (m_pPTZControlProperty && m_pPTZControlProperty->m_nCapabilities & PTZCtrlCap_RealTimeGetAbsPosNotSupported) {
         pButton->SetCheck (TRUE);
         pButton->EnableWindow (FALSE);
      }

      // Zoom Proportional Jog 설정 컨트롤 (mkjang-140425)
      bShow = FALSE;
      if (m_pPTZControlProperty && m_pPTZControlProperty->m_nCapabilities & PTZCtrlCap_ZoomProportionalJog)
         bShow = TRUE;
      GetDlgItem (IDC_CHECK_ZOOM_PROPORTIONAL_JOG)->ShowWindow (bShow);
      
      // 줌 포트번호 설정이 필요할 때 (연결방식:별도 비디오서버에 연결) (mkjang-140613)
      bShow = FALSE;
      if (m_pPTZInfo->m_nConnectType == PTZCtrlConnType_OtherIPCamera && PTZControlType_Truen_TN_B220CS_Shinwoo_SPT_7080B == m_pPTZInfo->m_nPTZControlType)
         bShow = TRUE;
      GetDlgItem (IDC_STATIC_ZOOM_PORT_NO)->ShowWindow(bShow);
      GetDlgItem (IDC_EDIT_ZOOM_PORT_NUMBER)->ShowWindow(bShow);

      UpdatePTZContorlTypeComboBox (   );

      if (!(m_nVCSetupMode & VCSetupMode_DoNotAllowPTZCamera)) {
         BOOL bEnableSerialConnectionCtrls = FALSE;
         BOOL bEnablePTZCtrlTCPServerCtrls = FALSE;
         BOOL bEnableLinkingOtherPTZCtrlCtrls = FALSE;
         BOOL bEnableOtherVideoServerCtrls = FALSE;
         if (VideoChannelType_AnalogCamera == m_nVideoChannelType) {
            if (PTZCtrlConnType_Basic == m_pPTZInfo->m_nConnectType) 
               bEnableSerialConnectionCtrls = TRUE;
            else if (PTZCtrlConnType_TCPServer == m_pPTZInfo->m_nConnectType) 
               bEnablePTZCtrlTCPServerCtrls = TRUE;
         }
         else if (VideoChannelType_IPCamera == m_nVideoChannelType) {
            if (PTZCtrlConnType_Basic == m_pPTZInfo->m_nConnectType) {
               if (IsSupportCustomPTZCommand () || IsShowSameIPCamManufacurePTZControlTypeComboBox ()) {
                  static int StaticControls [] = {
                     IDC_STATIC_PTZ_TYPE,
                     IDC_STATIC_CAMERA_ADDRESS,
                     0
                  };
                  SetCtrlPos (StaticControls, m_vtStaticCtrlPos, TRUE);
                  static int DataControls [] = {
                     IDC_COMBO_PTZ_TYPE,
                     IDC_EDIT_CAM_ADDRESS,
                     0
                  };
                  SetCtrlPos (DataControls, m_vtDataCtrlPos, TRUE);
               }
               else {
                  static int StaticControls [] = {
                     IDC_STATIC_CAMERA_ADDRESS,
                     0
                  };
                  SetCtrlPos (StaticControls, m_vtStaticCtrlPos, TRUE);
                  static int DataControls [] = {
                     IDC_EDIT_CAM_ADDRESS,
                     0
                  };
                  SetCtrlPos (DataControls, m_vtDataCtrlPos, TRUE);
               }
            }
            else if (PTZCtrlConnType_TCPServer == m_pPTZInfo->m_nConnectType) {
               bEnablePTZCtrlTCPServerCtrls = TRUE;
            }
            else if (PTZCtrlConnType_LinkToPTZCtrl == m_pPTZInfo->m_nConnectType) {
               bEnableLinkingOtherPTZCtrlCtrls = TRUE;
            }
            else if (PTZCtrlConnType_OtherIPCamera == m_pPTZInfo->m_nConnectType) {
               bEnableOtherVideoServerCtrls = TRUE;
               bEnablePTZCtrlTCPServerCtrls = TRUE;
            }
         }
         if (bEnableSerialConnectionCtrls) {
            static int StaticControls [] = {
               IDC_STATIC_PTZ_TYPE,
               IDC_STATIC_PTZ_COM_PORT,
               IDC_STATIC_BAUD_RATE,
               IDC_STATIC_CAMERA_ADDRESS,
               0
            };
            SetCtrlPos (StaticControls, m_vtStaticCtrlPos, TRUE);
            static int DataControls [] = {
               IDC_COMBO_PTZ_TYPE,
               IDC_COMBO_PTZ_COM_PORT,
               IDC_COMBO_BAUD_RATE,
               IDC_EDIT_CAM_ADDRESS,
               0
            };
            SetCtrlPos (DataControls, m_vtDataCtrlPos, TRUE);
         }
         if (bEnablePTZCtrlTCPServerCtrls) {
            static int StaticControls [] = {
               IDC_STATIC_PTZ_TYPE,
               IDC_STATIC_IP_ADDRESS,
               IDC_STATIC_PORT_NO,
               IDC_STATIC_CAMERA_ADDRESS,
               0
            };
            SetCtrlPos (StaticControls, m_vtStaticCtrlPos, TRUE);
            static int DataControls [] = {
               IDC_COMBO_PTZ_TYPE,
               IDC_EDIT_IP_ADDRESS,
               IDC_EDIT_PORT_NUMBER,
               IDC_EDIT_CAM_ADDRESS,
               0
            };
            SetCtrlPos (DataControls, m_vtDataCtrlPos, TRUE);
         }
         if (bEnablePTZCtrlTCPServerCtrls || bEnableOtherVideoServerCtrls) {
            GetDlgItem (IDC_BUTTON_SET_IP_TO_VIDEO_SERVER_IP)->ShowWindow (SW_SHOW);
         }
         if (bEnableOtherVideoServerCtrls)
            GetDlgItem (IDC_BUTTON_SET_DEFAULT_ID_PW)->ShowWindow (SW_SHOW);

         if (bEnableLinkingOtherPTZCtrlCtrls) {
            static int StaticControls [] = {
               IDC_STATIC_PTZ_CTRL_CHANNEL,
               IDC_STATIC_PTZ_CTRL_CHANNEL_INFO_STATIC,
               0
            };
            SetCtrlPos (StaticControls, m_vtStaticCtrlPos, TRUE);
            static int DataControls [] = {
               IDC_COMBO_PTZ_CTRL_CHANNEL_ID,
               IDC_STATIC_PTZ_CTRL_CHANNEL_INFO,
               0
            };
            SetCtrlPos (DataControls, m_vtDataCtrlPos, TRUE);

            GetDlgItem (IDC_CHECK_TX_ONLY                    )->ShowWindow (SW_HIDE);
            CButton* pButton = (CButton*)GetDlgItem (IDC_CHECK_REVERSE_PAN_COORDINATE);
            pButton->ShowWindow(SW_HIDE);
            pButton->SetCheck(FALSE);
            pButton = (CButton*)GetDlgItem (IDC_CHECK_REVERSE_TILT_COORDINATE);
            pButton->ShowWindow(SW_HIDE);
            pButton->SetCheck(FALSE);
            GetDlgItem (IDC_CHECK_ZOOM_PROPORTIONAL_JOG      )->ShowWindow (SW_HIDE);
            GetDlgItem (IDC_STATIC_ZOOM_PORT_NO              )->ShowWindow (SW_HIDE);
            GetDlgItem (IDC_EDIT_ZOOM_PORT_NUMBER            )->ShowWindow (SW_HIDE);
            GetDlgItem (IDC_CHECK_ZOOM_TRIGGERED_AUTO_FOCUS  )->ShowWindow (SW_HIDE);

            m_pPTZInfo->m_bReversePanCoordinate = FALSE;
            m_pPTZInfo->m_bReverseTiltCoordinate = FALSE;
         }
         if (bEnableOtherVideoServerCtrls) {
            static int Controls [] = {
               IDC_STATIC_DEVICE_MODEL,
               IDC_COMBO_DEVICE_MODEL,
               IDC_STATIC_USER_NAME,
               IDC_EDIT_USER_NAME,
               IDC_STATIC_PASSWORD,
               IDC_EDIT_PASSWORD,
               0
            };
            for (i = 0; i < Controls[i]; i++) {
               GetDlgItem (Controls [i])->ShowWindow (TRUE);
            }
         }

         // 프로토콜 버전
         BOOL bEnable = FALSE;         
         static int ProtocolVersionCtrls [] = {
            IDC_STATIC_PROTOCOL_VERSION,
            IDC_COMBO_PROTOCOL_VERSION,
            0
         };
         ShowCtrls (ProtocolVersionCtrls, bEnable);
         // PTZ 인덱스
         BOOL bSupportMultiPTZControlIPCamera = IsSupportMultiPTZControlIPCamera ();
         if (bSupportMultiPTZControlIPCamera) {
            CRect rtStaticAdress, rtEditAddress;
            GetWndRect (rtStaticAdress, this, GetDlgItem (IDC_STATIC_CAMERA_ADDRESS));
            GetWndRect (rtEditAddress , this, GetDlgItem (IDC_EDIT_CAM_ADDRESS));
            CRect rtStaticPTZIdx, rtEditPTZIdx;
            GetWndRect (rtStaticPTZIdx, this, GetDlgItem (IDC_STATIC_PTZ_INDEX));
            GetWndRect (rtEditPTZIdx  , this, GetDlgItem (IDC_EDIT_PTZ_INDEX));
            rtStaticPTZIdx.top    = rtStaticAdress.top;
            rtStaticPTZIdx.bottom = rtStaticAdress.bottom;
            rtEditPTZIdx.top      = rtEditAddress.top;
            rtEditPTZIdx.bottom   = rtEditAddress.bottom;
            GetDlgItem (IDC_STATIC_PTZ_INDEX)->SetWindowPos (NULL, rtStaticPTZIdx.left, rtStaticPTZIdx.top, 0,0, SWP_NOSIZE | SWP_NOZORDER);
            GetDlgItem (IDC_EDIT_PTZ_INDEX  )->SetWindowPos (NULL, rtEditPTZIdx.left, rtEditPTZIdx.top, 0,0, SWP_NOSIZE | SWP_NOZORDER);
        }
         GetDlgItem (IDC_STATIC_PTZ_INDEX)->ShowWindow (bSupportMultiPTZControlIPCamera);
         GetDlgItem (IDC_EDIT_PTZ_INDEX)->ShowWindow (bSupportMultiPTZControlIPCamera);
         // 줌 모듈의 종류
         BOOL bShowZoomModuleTypeCtrls = IsShowZoomModuleTypeCtrls ();
         if (bShowZoomModuleTypeCtrls) {
            m_cbZoomModuleType.ResetContent (   );
            int nZoomMuduleNum = (int) ZoomModuleTypeString.size () - 1; // jun : 광각 카메라는 제외
            for (i = 0; i < nZoomMuduleNum; i++) {
               m_cbZoomModuleType.AddString (ZoomModuleTypeString[i].m_String.c_str());
               m_cbZoomModuleType.SetItemData (i, ZoomModuleTypeString[i].m_nID);
            }
            SetCurSel_ByData (&m_cbZoomModuleType, m_pPTZInfo->m_nZoomModuleType);
         }
         GetDlgItem (IDC_STATIC_ZOOM_MODULE_TYPE)->ShowWindow (bShowZoomModuleTypeCtrls);
         GetDlgItem (IDC_COMBO_ZOOM_MODULE_TYPE)->ShowWindow (bShowZoomModuleTypeCtrls);
      }
   }
   SetPTZCtrlGroupCtrlWndPos (   );
}


void CSetupPTZControlDlg::OnBnClickedButtonSetIpToVideoServerIp()
{
   m_pPTZInfo->m_strIPAddress = m_pVCInfo->m_IPCamInfoArr[0].m_strIPAddress;
   GetDlgItem (IDC_EDIT_IP_ADDRESS)->SetWindowText (m_pPTZInfo->m_strIPAddress.c_str());
}

void CSetupPTZControlDlg::OnBnClickedButtonSet()
{
   CString str;
   if (m_pIPCameraProperty->m_nDefPortNo) {
      m_pPTZInfo->m_nPortNo = m_pIPCameraProperty->m_nDefPortNo;
      str.Format (_T("%d"), m_pPTZInfo->m_nPortNo);
      GetDlgItem (IDC_EDIT_PORT_NUMBER)->SetWindowText (str);
   }
   if (m_pIPCameraProperty->m_strDefUserName.length ()) {
      m_pPTZInfo->m_strUserName = m_pIPCameraProperty->m_strDefUserName;
      GetDlgItem (IDC_EDIT_USER_NAME)->SetWindowText (m_pPTZInfo->m_strUserName.c_str());
   }
   if (m_pIPCameraProperty->m_strDefUserName.length()) {
       m_pPTZInfo->m_strPassword = m_pIPCameraProperty->m_strDefPassword;
      GetDlgItem (IDC_EDIT_PASSWORD)->SetWindowText (m_pPTZInfo->m_strPassword.c_str());
   }

}


#pragma once

#include "GenericChildDialog.h"
#include "afxwin.h"
#include "BCGPCtrlEx.h"

class CPTZCtrlInfo;
class PTZControlProperty;

///////////////////////////////////////////////////////////////////////////////
//
// Class: CSetupPTZControlDlg
//
///////////////////////////////////////////////////////////////////////////////

class CSetupPTZControlDlg : public CRHGenericChildDialog
{
   DECLARE_DYNAMIC(CSetupPTZControlDlg)

protected:
   // Property
   int m_nVideoChannelType;  // 현재 비디오 채널 타입
   int m_nVCSetupMode;
   int m_nPTZControlType_Linking;
   int m_MaxBottomPosAmongVisibleCtrls;
   CVideoChannelManager* m_pVCM;
   CVideoChannelInfo*    m_pVCInfo;
   CPTZCtrlInfo*         m_pPTZInfo;
   PTZControlProperty*   m_pPTZControlProperty;
   IPCameraProperty*     m_pIPCameraProperty;

	BOOL			m_bInitUI;
   BOOL        m_bSupportCustomPTZCommand;
   // UI
   CBCGPComboBox	m_cbPTZConnectionType;
   CBCGPComboBox	m_cbPTZType;
   CBCGPComboBox	m_cbPTZComPort;
   CBCGPComboBox	m_cbRotateAxis;
   CBCGPComboBox	m_cbBaudRate;
   CBCGPComboBox	m_cbProtocolVersion;
   CBCGPComboBox	m_cbZoomModuleType;
   CBCGPComboBox	m_cbPTZChannelId;

   CILStatic      m_stcChannelInfo;

   std::vector<CPoint> m_vtStaticCtrlPos;
   std::vector<CPoint> m_vtDataCtrlPos;
	
public:
   CSetupPTZControlDlg();
   virtual ~CSetupPTZControlDlg();
   virtual int CRHGetDialogID ();

protected:
   virtual BOOL OnInitDialog ();
   virtual void LoadLangEvent();
   virtual void DoDataExchange(CDataExchange* pDX);
   virtual void OnCancel () {}

public:
   DECLARE_MESSAGE_MAP()
   afx_msg void OnCbnSelchangeComboDeviceModel();
   afx_msg void OnCbnSelchangeComboPtzConnectionType();
   afx_msg void OnCbnSelchangeComboPtzType();
   afx_msg void OnCbnSelchangeComboPtzComPort();
   afx_msg void OnCbnSelchangeBaudRate();
   afx_msg void OnCbnSelchangeProtocolVersion();
   afx_msg void OnCbnSelchangeComboPTZCtrlChannelId();
   afx_msg void OnCbnSelchangeComboZoomModuleType();
	afx_msg void OnEditChangeCamAddress();
   afx_msg void OnCbnSelchangeZoomModuleType();
   afx_msg void OnBnClickedButtonSetIpToVideoServerIp();
   afx_msg void OnBnClickedButtonSet();
   afx_msg void DoUpdateData();

public:
   void Initialize (int nVideoChannelType, int nVCSetupMode, CVideoChannelManager* pVCM, CVideoChannelInfo* pVCInfo, CPTZCtrlInfo* pInfo);
   void InitUI (   );
   void UpdatePTZConnectionTypeComboBox ();
   void UpdatePTZContorlTypeComboBox ();
   void InitLinkingPTZChannelComboBox ();
   BOOL IsVideoServerSupportPTZControl ();
   BOOL IsShowSameIPCamManufacurePTZControlTypeComboBox ();
   BOOL IsSupportCustomPTZCommand ();
   BOOL IsSupportMultiPTZControlIPCamera ();
   BOOL IsShowZoomModuleTypeCtrls ();
   void SetPTZCtrlGroupCtrlWndPos ();
   void ShowEnableCtrls ();

protected:
   void SetCtrlPos (int* pCtrlIDs, std::vector<CPoint>& vtCtrlPos, BOOL bShow);
   void SetCtrlPos (CWnd* pInsertBeforeCtrl, CWnd* pCtrl, int nOffsetX, int nOffsetY);
   void ShowCtrls (int* pCtrlIDs, BOOL bShow);
};

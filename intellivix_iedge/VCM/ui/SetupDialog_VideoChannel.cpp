#include "stdafx.h"
#include "version.h"
#include "VCM.h"
#include "SetupDialog_VideoChannel.h"
#include "SupportFunction.h"

int g_nPrevVideoChannelType = VideoChannelType_IPCamera;


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////////////
// class CSetupDialog_VideoChannel

IMPLEMENT_DYNAMIC(CSetupDialog_VideoChannel, CBCGPDialog)
CSetupDialog_VideoChannel::CSetupDialog_VideoChannel(CWnd* pParent): CRHGenericChildDialog (pParent)
{
   m_pVCM = NULL;   
   m_nState = 0;
   m_nVCSetupMode = 0;

   TemplateID = IDD_SETUP_CAMERA_VIDEO_CHANNEL; 
}

CSetupDialog_VideoChannel::~CSetupDialog_VideoChannel()
{
}

int CSetupDialog_VideoChannel::CRHGetDialogID ()
{
   return TemplateID;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Virtual Functions

void CSetupDialog_VideoChannel::DoDataExchange(CDataExchange* pDX)
{
   CBCGPDialog::DoDataExchange(pDX);
   DDX_Control(pDX, IDC_COMBO_VIDEO_CHANNEL_TYPE, m_cbVideoChannelType);
   if (IS_SUPPORT(__SUPPORT_FISHEYE_DEWARPING))
      DDX_Check (pDX, IDC_CHECK_FISHEYE_CAMERA, m_pVCInfo->m_bFisheyeCam);   
}

BOOL CSetupDialog_VideoChannel::OnInitDialog (  )
{
   int i;

   SetRedraw (FALSE);
   CRHGenericChildDialog::OnInitDialog ();

   MLANG_WND_LOAD(GetSafeHwnd(), IDD_SETUP_CAMERA_VIDEO_CHANNEL);   

   ShowWindow (FALSE);
   if (!m_pVCM) { 
      BCGPMessageBox(_T("Initialize Error"));
      return FALSE;
   }
  
   ShowFisheyeCheckbox();

   // 각각 종류의 비디오 채널 타입에 따른 다이얼로그를 초기화하고 생성한다.
   
   m_IPCamStreamDlg.Initialize (m_nVCSetupMode, m_pVCM, m_pVCInfo);
   m_IPCamStreamDlg.CRHCreateGenericChildDialog (this, IDC_CHILDPLACEMARKER, 0, NULL);
   m_IPCamStreamDlg.m_nVideoChannelType = VideoChannelType_IPCamera;      
      
   m_PTZDlg.Initialize (VideoChannelType_AnalogCamera, m_nVCSetupMode, m_pVCM, m_pVCInfo, &m_pVCInfo->m_PTZInfo);
   m_PTZDlg.CRHCreateGenericChildDialog (this, IDC_CHILDPLACEMARKER, 0, NULL);  // OnInitDialog에서 속성값들이 변경될 수 있음.
   //m_PTZDlg_IPCam.Initialize (VideoChannelType_IPCamera, m_nVCSetupMode, m_pVCM, m_pVCInfo, &m_pVCInfo->m_IPCamInfo.m_PTZInfo);
   //m_PTZDlg_IPCam.CRHCreateGenericChildDialog (this, IDC_CHILDPLACEMARKER, 0, NULL);  // OnInitDialog에서 속성값들이 변경될 수 있음.

   // 설정 다이얼로그들을 설정 다이얼로그 리스트에 추가한다.    
   m_SetupDlgs.push_back (&m_IPCamStreamDlg);      
   
   for (i = 0; i < (int) m_SetupDlgs.size (); i++) {
      m_SetupDlgs[i]->ShowWindow (FALSE);
   }
   SetRedraw (TRUE);
   return TRUE;
}

void CSetupDialog_VideoChannel::LoadLangEvent()
{      
   m_cbVideoChannelType.ResetContent();
   m_cbVideoChannelType.SetRedraw (FALSE);
   // 비디오 채널 형식에 대한 콤보박스 아이템을 초기화 한다. 
   int nItemCount = 0;
   for (int i = 0; i < (int) VideoChannelTypeString.size(); i++)  
   {
      int nVCType = VideoChannelTypeString[i].m_nID;
      
      // 현재 ActiveX 에서 지원하지 않음
      if (nVCType == VideoChannelType_AnalogCamera)   continue;
      if (nVCType == VideoChannelType_Panorama)       continue;
      if (nVCType == VideoChannelType_VideoFile)      continue;
      if (nVCType == VideoChannelType_WebCam)         continue;

      // PTZ 카메라의 경우 파노라마 또는 비디오 파일은 추가하지 않는다. 
      if (m_nVCSetupMode & VCSetupMode_SetupPTZCamera) {
         if (nVCType == VideoChannelType_Panorama)  continue;
         if (nVCType == VideoChannelType_VideoFile) continue;
         if (nVCType == VideoChannelType_WebCam)    continue;
      }
      if( IS_NOT_SUPPORT(__SUPPORT_VIRTUAL_PTZ_CAMERA) )
         if (nVCType == VideoChannelType_VirtualPTZ) continue;

      m_cbVideoChannelType.AddString (VideoChannelTypeString[i].m_String.c_str());
      m_cbVideoChannelType.SetItemData (nItemCount, DWORD_PTR (nVCType));
      nItemCount++;
   }
   if (m_cbVideoChannelType.GetCount () == 0) 
      return;
   m_cbVideoChannelType.SetRedraw (TRUE);
   SetCurSel_ByData (&m_cbVideoChannelType, m_pVCInfo->m_nVideoChannelType);
}

BOOL CSetupDialog_VideoChannel::ShowSetupWindow(int nCmdShow)
{
   CBCGPDialog::ShowWindow (nCmdShow);
   ShowVideoChannelSetupDlg (  );
   return TRUE;
}

BOOL CSetupDialog_VideoChannel::Apply (  )
{
   UpdateVCInfo ();
   return IsOK ();
}

void CSetupDialog_VideoChannel::Cancel()
{
   // IntelliVIX 에서는 임시시스템을 생성하여 파라메터를 변경하기 때문에 취소할 때도 UpdateVCInfo를 호출해야한다.
   UpdateVCInfo ();
}

void CSetupDialog_VideoChannel::SetActive()
{
}

void CSetupDialog_VideoChannel::KillActive()
{
   UpdateVCInfo ();
   g_nPrevVideoChannelType = m_pVCInfo->m_nVideoChannelType;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Massage Handlers

BEGIN_MESSAGE_MAP(CSetupDialog_VideoChannel, CBCGPDialog)
   ON_WM_DESTROY ()
   ON_CBN_SELCHANGE(IDC_COMBO_VIDEO_CHANNEL_TYPE, OnCbnSelchangeComboVideoChannelType)
   ON_BN_CLICKED(IDC_CHECK_FISHEYE_CAMERA, OnBnClickedCheckFisheyeCamera)
END_MESSAGE_MAP()

void CSetupDialog_VideoChannel::OnCbnSelchangeComboVideoChannelType()
{
   ShowVideoChannelSetupDlg (  );
   ShowFisheyeCheckbox();      
}

void CSetupDialog_VideoChannel::OnDestroy()
{
   CBCGPDialog::OnDestroy ();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// User Defined Functions

int CSetupDialog_VideoChannel::Initialize (CVideoChannelManager* pVCM, int nVCID, int nVCSetupMode)
{
   m_pVCM = pVCM;
   m_nVCID = nVCID;
   m_nVCSetupMode = nVCSetupMode;

   CVideoChannelInfo* pVCInfoOrg = m_pVCM->GetVideoChannel (nVCID);    

   ASSERT (pVCInfoOrg);

   if (m_nVCSetupMode & VCSetupMode_TempVC) {
      m_pVCInfo = pVCInfoOrg;      
   }
   else {
      CVideoChannelInfo& vcInfo = pVCInfoOrg->GetVideoChannelInfo ();
      m_VCInfoTmp = vcInfo;      

      m_pVCInfo = &m_VCInfoTmp;      
   }

   int i;

   // 추가할 카메라가 있는지 테스트 한다.    
   int nItemCount = 0;
   for (i = 0; i < (int) VideoChannelTypeString.size(); i++)  
   {
      int nVCType = VideoChannelTypeString[i].m_nID;
      
      if (m_nVCSetupMode & VCSetupMode_SetupPTZCamera) {
         if (nVCType == VideoChannelType_Panorama) continue;
         if (nVCType == VideoChannelType_VideoFile) continue;         
      }
      nItemCount++;
   }

   if (VCSetupMode_AddCamera & m_nVCSetupMode) {
      m_pVCInfo->m_nVideoChannelType = g_nPrevVideoChannelType;
   }

   if (pVCInfoOrg == NULL) return (1);
   if (nItemCount == 0)    return (2);
   return (DONE);
}

void CSetupDialog_VideoChannel::ShowVideoChannelSetupDlg ()
{
   int i;

   int nCurSel = m_cbVideoChannelType.GetCurSel ();
   int nCurVideoChannelType = 0;
   GetCurData (&m_cbVideoChannelType, nCurVideoChannelType);

   m_pVCInfo->m_nVideoChannelType = nCurVideoChannelType;

   if (this->GetStyle () & WS_VISIBLE) {
      for (i = 0; i < (int) m_SetupDlgs.size (); i++) {
         m_SetupDlgs[i]->ShowWindow (m_SetupDlgs[i]->m_nVideoChannelType == nCurVideoChannelType);
      }
   }

   BOOL bShowAddCamNum = FALSE;
   if (m_nVCSetupMode & VCSetupMode_AddCamera && nCurVideoChannelType != VideoChannelType_AnalogCamera)
      bShowAddCamNum = TRUE;

   if (nCurVideoChannelType == VideoChannelType_AnalogCamera)
   {
      CRect rt;
      GetWndRect (rt, this, m_SetupDlgs[nCurSel]);
      m_PTZDlg.SetWindowPos (NULL, rt.left, rt.top + rt.Height(), 0, 0, SWP_NOSIZE | SWP_SHOWWINDOW);
   }
   else
      m_PTZDlg.ShowWindow (SW_HIDE);

   // IP 카메라에서 PTZ 제어 속성 설정창의 오픈여부에 대한 속성 값을 얻는다. 
   BOOL bSetupPTZContorlInfo = FALSE;
   IPCameraProperty* pIPCamProperty = GetIPCameraProperty (m_pVCInfo->m_IPCamInfo.m_nIPCameraType);
   if (pIPCamProperty) {
      if (pIPCamProperty->m_nCapabilities & IPCamCaps_SetupPTZContorlInfo)
         bSetupPTZContorlInfo = TRUE;
   }

   // 현재 페이지를 제외하고 모두 KillActive 호출
   int nPrevActivePageIdx = -1;
   for (i = 0; i < (int) m_SetupDlgs.size (); i++) {
      if (m_SetupDlgs[i]->m_bActivePage) {
         nPrevActivePageIdx = i;
         break;
      }
   }
   if (nPrevActivePageIdx != -1) {
      if (nCurSel != nPrevActivePageIdx) {
         m_SetupDlgs[nPrevActivePageIdx]->KillActive ();
         m_SetupDlgs[nPrevActivePageIdx]->m_bActivePage = FALSE;
      }
   }
   if (nCurSel < (int) m_SetupDlgs.size ()) {
      // 현재 페이지 SetActive 호출
      m_SetupDlgs[nCurSel]->m_bActivePage = TRUE;
      m_SetupDlgs[nCurSel]->SetActive ();
   }
}

BOOL CSetupDialog_VideoChannel::IsOK ()
{
   if (m_pVCInfo->m_nVideoChannelType == VideoChannelType_IPCamera) {
      if (!CheckIPCamInfos (&m_pVCInfo->m_IPCamInfo)) return FALSE;
   }
   if (m_pVCInfo->m_nVideoChannelType == VideoChannelType_Panorama) {
      if (!CheckPanoramaInfos (&m_pVCInfo->m_PanoramaCamInfo, m_nVCSetupMode)) return FALSE;
   }
   if (m_nVCSetupMode & VCSetupMode_SetupPTZCamera) {
      if (!m_pVCInfo->m_bPTZCamera) {
         // Virtual Camera 일 시에 미사용이 적용 안되는 문제로 인해 선택적인 Return 값을 사용하도록 수정 JJong
         BOOL bRet = TRUE; 
         if (m_pVCInfo->m_nVideoChannelType == VideoChannelType_AnalogCamera) {
            BCGPMessageBox(MLANG_WND(IDS_SHOULD_SELECT_PTZ_CAMERA));  // PTZ 카메라 종류를 선택하지 않았습니다. 
            bRet = FALSE;
         } else if (m_pVCInfo->m_nVideoChannelType == VideoChannelType_IPCamera) {
            BCGPMessageBox(MLANG_WND(IDS_NOT_SUPPORT_PTZ));  // 선택한 IP 카메라는 PTZ 기능을 지원하지 않습니다. 
            bRet = FALSE;
         }
         return bRet;
      }
      // 절대 위치로 이동이 가능한 프로토콜인지 체크
      PTZControlProperty* pPTZCtrlProperty = GetPTZControlProperty (m_pVCInfo->m_PTZInfo.m_nPTZControlType);
      if (pPTZCtrlProperty) {
         if (pPTZCtrlProperty->m_nCapabilities & PTZCtrlCap_SetAbsPosNotSupported) {
            BCGPMessageBox(MLANG_WND(IDS_PTZ_CAMERA_NOT_AUTO_TRACKING));
            return FALSE;
         }
      }
   }
   UpdateData(TRUE);
   return TRUE;
}

BOOL CSetupDialog_VideoChannel::IsCancel ()
{
   return TRUE;
}

void CSetupDialog_VideoChannel::UpdateVCInfo ()
{
   // 변경된 캡춰보드 카메라 정보 수정      
   m_IPCamStreamDlg.UpdateData (TRUE);   
  
   m_pVCInfo->CheckParameters ();
   // 임시 비디오 채널 객체가 아닌 경우 원본 정보에 복사를 한다. (MDBLibTest 에서 사용)
   if (!(m_nVCSetupMode & VCSetupMode_TempVC)) {
      CVideoChannel* pVC = m_pVCM->GetVideoChannel (m_nVCID);
      pVC->Initialize (m_VCInfoTmp);         // 카메라 재생성
   }
}

void CSetupDialog_VideoChannel::OnBnClickedCheckFisheyeCamera()
{
   // (mkjang-150703)
   // "어안렌즈 카메라" 체크가 되면
   // "IP 카메라 / 비디오 서버"의 "PTZ 사용" 체크가 해제가 되고 비활성화하고
   // 그 아래에 있는 컨트롤들을 비활성화 한다.
   UpdateData(TRUE);
   UpdateVCInfo ();
   m_IPCamStreamDlg.SetCheckPTZControl();
   m_IPCamStreamDlg.ShowChildWindows();
}

void CSetupDialog_VideoChannel::ShowFisheyeCheckbox()
{
   CButton* pButton = (CButton*)GetDlgItem (IDC_CHECK_FISHEYE_CAMERA);
   if (!pButton)
      return;

   if (IS_NOT_SUPPORT(__SUPPORT_FISHEYE_DEWARPING)) {
      pButton->ShowWindow(SW_HIDE);
   } 
   else {
      switch (m_pVCInfo->m_nVideoChannelType) {
      case VideoChannelType_AnalogCamera:
      case VideoChannelType_IPCamera:
      case VideoChannelType_VideoFile:      
         pButton->ShowWindow(TRUE);
         break;
      case VideoChannelType_WebCam:
      default:      
         m_pVCInfo->m_bFisheyeCam = FALSE;
         pButton->SetCheck(m_pVCInfo->m_bFisheyeCam);
         pButton->ShowWindow(FALSE);
         break;
      }  
   }
}
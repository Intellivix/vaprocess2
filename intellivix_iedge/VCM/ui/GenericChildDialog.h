#pragma once


#include "VCM_Define.h"
#include "SetupDialog.h"

/////////////////////////////////////////////////////////////////////////////
// CRHGenericChildDialog dialog

class CRHGenericChildDialog : public CSetupDialog
{
   // Construction

public:
   int  m_nVideoChannelType;
   BOOL m_bActivePage;
   CWnd*  CRHpParent;

private:
   WPARAM CRHId; 

public:
   CRHGenericChildDialog(CWnd* pParent = NULL);

   virtual int CRHGetDialogID() = 0;
   virtual void SetActive  () { }
   virtual void KillActive () { }
   virtual void OnOK(){}
   virtual void OnCancel(){}


   void CRHCreateGenericChildDialog (CWnd *pParent, int PlaceMarkerCtrlID, int Id, CRect *ABorderRect = NULL);
   void CRHCreateGenericChildDialog (CWnd *pParent, CRect *pPlaceMarkerRect, int Id, CRect *ABorderRect = NULL);
   static CRect CRHRectForGroupBox;

protected:
   virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

protected:
   DECLARE_MESSAGE_MAP()

protected:
   void CRHPostMessageToParent(UINT message, LPARAM lParam);
};


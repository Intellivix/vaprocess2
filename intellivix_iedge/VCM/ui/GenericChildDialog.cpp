// GenericChildDialog.cpp : implementation file
//

#include "stdafx.h"
#include "VCM.h"
#include "GenericChildDialog.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////

CRect CRHGenericChildDialog::CRHRectForGroupBox(4,15,4,6);

/////////////////////////////////////////////////////////////////////////////
// CRHGenericChildDialog dialog
CRHGenericChildDialog::CRHGenericChildDialog(CWnd* pParent) : CSetupDialog (pParent)
{
   m_nVideoChannelType = 0;
   m_bActivePage = FALSE;
}

void CRHGenericChildDialog::DoDataExchange(CDataExchange* pDX)
{
   CBCGPDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CRHGenericChildDialog, CBCGPDialog)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRHGenericChildDialog message handlers

void CRHGenericChildDialog::CRHCreateGenericChildDialog(CWnd *pParent, int PlaceMarkerCtrlID, int Id, CRect *ABorderRect /*= NULL*/)
{
   CRHpParent = pParent;
   CRHId = Id;

   int prev_tick = GetTickCount (   );
   VERIFY(CBCGPDialog::Create( CRHGetDialogID(), pParent ));

   if (ABorderRect == NULL)
   {
      static CRect NoBordersRect(0,0,0,0);
      ABorderRect = &NoBordersRect;
   }

   CRect PlaceMarkerRect;
   CWnd *pPlaceMarkerWnd = pParent->GetDlgItem( PlaceMarkerCtrlID );
   if( !pPlaceMarkerWnd )
      return;
   pPlaceMarkerWnd->GetWindowRect(&PlaceMarkerRect);
   pParent->ScreenToClient(&PlaceMarkerRect);

   CRect ChildDBRect;
   GetClientRect(&ChildDBRect);

   MoveWindow(PlaceMarkerRect.left + ABorderRect->left, PlaceMarkerRect.top + ABorderRect->top, ChildDBRect.right, ChildDBRect.bottom);

   int cx = ChildDBRect.right  + ABorderRect->left + ABorderRect->right;
   int cy = ChildDBRect.bottom + ABorderRect->top  + ABorderRect->bottom;
   VERIFY(pPlaceMarkerWnd->SetWindowPos(NULL, 0,0, cx,cy, SWP_NOMOVE | SWP_NOZORDER));

   VERIFY(SetWindowPos(pPlaceMarkerWnd, 0,0, 0,0, SWP_NOMOVE | SWP_NOSIZE));
   // logd ("CRHCreateGenericChildDialog = %d\n", GetTickCount (   ) - prev_tick);

}

void CRHGenericChildDialog::CRHCreateGenericChildDialog(CWnd *pParent, CRect *pPlaceMarkerRect, int Id, CRect *ABorderRect /*= NULL*/)
{
   CRHpParent = pParent;
   CRHId = Id;

   VERIFY(CBCGPDialog::Create(CRHGetDialogID(), pParent ));

   if (ABorderRect == NULL)
   {
      static CRect NoBordersRect(0,0,0,0);
      ABorderRect = &NoBordersRect;
   }

   CRect ChildDBRect;
   GetClientRect(&ChildDBRect);

   MoveWindow(pPlaceMarkerRect->left + ABorderRect->left, pPlaceMarkerRect->top + ABorderRect->top, 
      ChildDBRect.right, ChildDBRect.bottom);

}

void CRHGenericChildDialog::CRHPostMessageToParent(UINT message, LPARAM lParam)
{
   CRHpParent->PostMessage(message, CRHId, lParam);
}

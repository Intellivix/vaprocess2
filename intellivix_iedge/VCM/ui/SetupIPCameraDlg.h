#pragma once

#include "GenericChildDialog.h"


class CCamera_IP;
class CIPCameraInfo;
class IPCameraProperty;
class CSetupPTZControlDlg;


///////////////////////////////////////////////////////////////////////////////
//
// Class: CSetupIPCameraDlg
//
///////////////////////////////////////////////////////////////////////////////

class CSetupIPCameraDlg : public CRHGenericChildDialog
{
   DECLARE_DYNAMIC(CSetupIPCameraDlg)
public:
   enum State 
   {
      State_SetupPanorama = 0x00000001,
   };

protected:
   int  m_nState;
   int  m_nVCSetupMode;
   BOOL m_nUseMasterPassword;
   CBCGPToolTipCtrl m_ToolTip;

   CVideoChannelInfo*   m_pVCInfo;
   CIPCameraInfo*       m_pIPCamInfo;
   CPTZCtrlInfo*        m_pPTZInfo;
   IPCameraProperty*    m_pIPCamProperty;
   CSetupPTZControlDlg* m_pSetupPTZControlDlg;

public:
   CSetupIPCameraDlg();
   virtual ~CSetupIPCameraDlg();
   virtual int CRHGetDialogID ();


protected:
   virtual void DoDataExchange(CDataExchange* pDX); 
   virtual BOOL OnInitDialog();
   virtual BOOL PreTranslateMessage(MSG* pMsg);
public:
   virtual void SetActive ();
   virtual void KillActive ();

public:
   DECLARE_MESSAGE_MAP()
   afx_msg void OnCbnSelchangeComboDeviceModel();
   afx_msg void OnCbnSelchangeComboVideoType();
   afx_msg void OnCbnSelchangeComboStreamType();
   afx_msg void OnCbnSelchangeComboResolution();
   afx_msg void OnBnClickedButtonOpenWebSetupPage();      
   afx_msg void OnBnClickedButtonSetDefault();
   afx_msg void OnBnClickedCheckPTZControl();
   afx_msg void OnCbnSelchangeComboVideoChNo();
   afx_msg void OnCbnSelchangeComboVideoFormat();
   afx_msg void OnCbnSelchangeComboFrameRate();
   afx_msg void OnCbnSelchangeComboVideoStreamIndex();
   afx_msg void OnEnChangeEditIpAddress();
   // Support RealHub
   afx_msg void OnCbnSelchangeComboSocketType();
   afx_msg void OnCbnSelchangeComboAudioOut();
   afx_msg void OnBnClickedCheckHttpAddr();   

public:
   void    Initialize (int nVCSetupMode, CVideoChannelInfo* pVCInfo, CIPCameraInfo* pIPCamInfo, CPTZCtrlInfo* pPTZInfo, CSetupPTZControlDlg* pSetupPTZControlDlg);
   void    Initialize (int nVCSetupMode, CIPCameraInfo* pIPCamInfo, CPTZCtrlInfo* pPTZInfo, CSetupPTZControlDlg* pSetupPTZControlDlg);
   void    InitUI ();
   void    UpdateIPCameraType ();
   void    UpdatePortNo ();
   void    UpdateVideoFormat ();
   void    SetState (int nState);
   void    EnableAllCtrls (BOOL bEnable);
   void    SetCheckPTZControl();

protected:
   virtual void LoadLangEvent (  );
};

#pragma once

#include "SetupDialog_VideoChannel.h"
#include "MultiLangWnd.h"

#define MSG_VIDEOCHANNEL_EDIT_CLOSED         (WM_USER+202)

class CSetupDialog_VideoChannelEdit : public CBCGPDialog, public CMultiLangWnd
{
   DECLARE_DYNAMIC(CSetupDialog_VideoChannelEdit)

public:

protected:
   int                       m_nVCSetupMode;
   BOOL                      m_bModaless;
   CSetupDialog_VideoChannel m_VCDlg;

public:
   CSetupDialog_VideoChannelEdit(CWnd* pParent = NULL);
   virtual ~CSetupDialog_VideoChannelEdit();	

protected:
   DECLARE_MESSAGE_MAP();
   virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
   virtual BOOL OnInitDialog();
   virtual void OnOK();
   virtual void OnCancel();

public:
   int  Initialize (CVideoChannelManager* pVCM, int nVCID, int nVCSetupMode);
   BOOL Create(CWnd* pParentWnd);
};

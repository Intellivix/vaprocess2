#include "stdafx.h"
#include "VCM.h"
#include "SetupDialog_VideoChannelEdit.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

////////////////////////////////////////////////////////////////////////////////////////////////
// class CSetupPanoramaCamDlg

IMPLEMENT_DYNAMIC(CSetupDialog_VideoChannelEdit, CBCGPDialog)
CSetupDialog_VideoChannelEdit::CSetupDialog_VideoChannelEdit(CWnd* pParent /*=NULL*/)
: CBCGPDialog(IDD_SETUP_CAMERA_VIDEO_CHANNEL_EDIT, pParent)
{
   EnableVisualManagerStyle(TRUE, TRUE);
   m_bModaless = FALSE;
}

CSetupDialog_VideoChannelEdit::~CSetupDialog_VideoChannelEdit()
{
}

/////////////////////////////////////////////////////////////////////////////////////////////////
// message handlers

void CSetupDialog_VideoChannelEdit::DoDataExchange(CDataExchange* pDX)
{
   CBCGPDialog::DoDataExchange(pDX);
}

BOOL CSetupDialog_VideoChannelEdit::OnInitDialog()
{
   CBCGPDialog::OnInitDialog();

   MLANG_WND_LOAD(GetSafeHwnd(), IDD_SETUP_CAMERA_VIDEO_CHANNEL_EDIT);   

   if (m_nVCSetupMode & VCSetupMode_SetupPTZCamera) {
      CString str = MLANG_WND(IDS_ADD_PTZ_CAMERA);
      SetWindowText (str);
   }
   m_VCDlg.CRHCreateGenericChildDialog (this, IDC_CHILDPLACEMARKER, 0, NULL);
   m_VCDlg.ShowSetupWindow (TRUE);
   return TRUE;
}

void CSetupDialog_VideoChannelEdit::OnOK ()
{
   m_VCDlg.UpdateData (   );
   m_VCDlg.UpdateVCInfo ();

   if (m_bModaless) {
      m_VCDlg.IsOK();
      if (m_pParentWnd)
         ::SendMessage(m_pParentWnd->m_hWnd, MSG_VIDEOCHANNEL_EDIT_CLOSED, (WPARAM)1, (LPARAM)0);
   }
   else {
      if (m_VCDlg.IsOK()) CBCGPDialog::OnOK();
   }   
}

void CSetupDialog_VideoChannelEdit::OnCancel ()
{
   if (m_bModaless) {
      m_VCDlg.IsCancel();
      if (m_pParentWnd)
         ::SendMessage(m_pParentWnd->m_hWnd, MSG_VIDEOCHANNEL_EDIT_CLOSED, (WPARAM)0, (LPARAM)0);
   }
   else {
      if (m_VCDlg.IsCancel())
         CBCGPDialog::OnCancel();
   }   
}

/////////////////////////////////////////////////////////////////////////////////////////////////
// message handlers

BEGIN_MESSAGE_MAP(CSetupDialog_VideoChannelEdit, CBCGPDialog)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////////////////////////
// implementation

int CSetupDialog_VideoChannelEdit::Initialize (CVideoChannelManager* pVCM, int nVCID, int nVCSetupMode)
{
   m_nVCSetupMode = nVCSetupMode;
   return m_VCDlg.Initialize (pVCM, nVCID, nVCSetupMode);
}

BOOL CSetupDialog_VideoChannelEdit::Create(CWnd* pParentWnd)
{
   m_pParentWnd = pParentWnd;
   if (!CBCGPDialog::Create(IDD_SETUP_CAMERA_VIDEO_CHANNEL_EDIT, pParentWnd))
      return FALSE;

   CenterWindow(pParentWnd);
 
   m_bModaless = TRUE;
   return TRUE;
}
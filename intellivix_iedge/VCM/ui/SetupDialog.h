#pragma once


enum SetupDialogOption
{
   SetupDlgOpt_EnableApplyToAllChannel = 0x00000001,    // 모든 채널에 적용
   SetupDlgOpt_SystemUnit              = 0x00000002,    // 시스템 설정
};

#include "MultiLangWnd.h"

class CSystem;
class CCamera;

class CSetupDialog : public CBCGPDialog, public CMultiLangWnd
{
	DECLARE_DYNAMIC(CSetupDialog)

public:
	CSetupDialog(CWnd* pParent);
	virtual ~CSetupDialog();

	UINT			TemplateID;
	CString		Title;
	int			ID;
	int			Attribute;
	BOOL			bCreated;
	CSystem*		System;
	CCamera*		Camera;
	int			ChannelNo;
	
public:
	void	  SetTitle(LPCSTR szTitle) { Title = szTitle ;}
	void	  SetAttribute(int nAttribute) { Attribute = nAttribute ;}
	void	  SetID(int nID) { ID = nID;  }
   int	  GetID()        { return ID; }
	BOOL	  Create();
	virtual BOOL Check(){return TRUE;}
	virtual BOOL Apply(){return UpdateData();}
	virtual void Cancel(){;}
	virtual void OnOK(){;}
	virtual void OnCancel(){;}
   virtual BOOL ShowSetupWindow(int nCmdShow);
   virtual void SetActive  () { }
   virtual void KillActive () { }
   virtual void UpdateDialog () { }

protected:
	DECLARE_MESSAGE_MAP()
};

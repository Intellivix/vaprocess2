#pragma once

#include "GenericChildDialog.h"
#include "SetupIPCameraDlg.h"
#include "SetupPTZControlDlg.h"
#include "BCGPCtrlEx.h"

class CCamera_IP;
class CIPCameraInfo;
class IPCameraProperty;
class CSetupPTZControlDlg;

///////////////////////////////////////////////////////////////////////////////
//
// Class: CSetupIPCameraStreamDlg
//
///////////////////////////////////////////////////////////////////////////////

class CSetupIPCameraStreamDlg : public CRHGenericChildDialog
{
   DECLARE_DYNAMIC(CSetupIPCameraStreamDlg)

protected:
   BOOL                  m_bInit;
   int                   m_nState;
   CBCGPToolTipCtrl      m_ToolTip;
   CILTabCtrl            m_tabVideoStream;
   static int            m_nTabIdx_VideoStream;

protected:
   int                   m_nVCSetupMode;
   CIPCameraInfo*        m_pIPCamInfo;
   CIPCameraInfo*        m_pIPCamInfoArr;
   CVideoChannelManager* m_pVCM;
   CVideoChannelInfo*    m_pVCInfo;


protected:
   CSetupIPCameraDlg    m_IPCamDlg;
   CSetupPTZControlDlg  m_PTZDlg_IPCam;

public:
   CSetupIPCameraStreamDlg();
   virtual ~CSetupIPCameraStreamDlg();
   virtual int CRHGetDialogID ();

protected:
   virtual void DoDataExchange(CDataExchange* pDX); 
   virtual BOOL OnInitDialog();
   virtual void LoadLangEvent();
   virtual BOOL PreTranslateMessage(MSG* pMsg);

public:
   afx_msg void OnBnClickedCheckUseStream();
   afx_msg void OnBnClickedGetMainStreamConnectionInfo();   
   afx_msg LRESULT OnChangingActiveTab(WPARAM wp, LPARAM lp);

public:
   virtual void SetActive ();
   virtual void KillActive ();

public:
   DECLARE_MESSAGE_MAP()

public:
   void Initialize (int nVCSetupMode, CVideoChannelManager* pVCM, CVideoChannelInfo* pVCInfo);
   void InitUI ();
   void SetState (int nState);
   BOOL UpdateData(BOOL bSaveAndValidate = TRUE);
   void ShowChildWindows (   );
   void OnChangeVideoStream (int nStreamIdx);
   void SetCheckPTZControl();
};

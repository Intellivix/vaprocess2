
#include "stdafx.h"
#include "version.h"
#include "VCM.h"
#include "SetupIPCameraStreamDlg.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: CSetupIPCameraStreamDlg
//
///////////////////////////////////////////////////////////////////////////////

CIPCameraInfo g_pIPCamInfo_LastSetupCam[MAX_IP_CAMERA_STREAM_NUM];

int CSetupIPCameraStreamDlg::m_nTabIdx_VideoStream = 0;

IMPLEMENT_DYNAMIC(CSetupIPCameraStreamDlg, CBCGPDialog)

CSetupIPCameraStreamDlg::CSetupIPCameraStreamDlg()
{
   m_nState = 0;
	m_nVCSetupMode = 0;
   m_bInit = FALSE;
}

CSetupIPCameraStreamDlg::~CSetupIPCameraStreamDlg()
{
}

int CSetupIPCameraStreamDlg::CRHGetDialogID ()
{
   return IDD_SETUP_VIDEO_INPUT_IP_CAMERA_STREAM;
}

void CSetupIPCameraStreamDlg::DoDataExchange(CDataExchange* pDX)
{
   CRHGenericChildDialog::DoDataExchange(pDX);
   //DDX_Control (pDX, IDC_TAB_VIDEO_STREAM, m_tabVideoStream);
   DDX_Check   (pDX, IDC_CHECK_USE_STREAM, m_pIPCamInfo->m_bUseStream);
   if (pDX->m_bSaveAndValidate) {
      int i;
      for (i = 0; i < MAX_IP_CAMERA_STREAM_NUM; i++) {
         g_pIPCamInfo_LastSetupCam[i] = m_pIPCamInfoArr[i];
      }
   }
}

BEGIN_MESSAGE_MAP(CSetupIPCameraStreamDlg, CRHGenericChildDialog)   
   ON_REGISTERED_MESSAGE(BCGM_CHANGE_ACTIVE_TAB, OnChangingActiveTab)
   ON_BN_CLICKED(IDC_CHECK_USE_STREAM                      , &CSetupIPCameraStreamDlg::OnBnClickedCheckUseStream)
   ON_BN_CLICKED(IDC_BUTTON_GET_MAIN_STREAM_CONNECTION_INFO, &CSetupIPCameraStreamDlg::OnBnClickedGetMainStreamConnectionInfo)
END_MESSAGE_MAP()


BOOL CSetupIPCameraStreamDlg::OnInitDialog()
{  
   int i;

   CRHGenericChildDialog::OnInitDialog ();

   // 새로운 카메라를 추가하는 경우 마지막에 설정을 복사한다. (빠른 설정을 위해)
   if (VCSetupMode_AddCamera & m_nVCSetupMode) {
      for (i = 0 ; i < MAX_IP_CAMERA_STREAM_NUM; i++) {
         m_pIPCamInfoArr[i] = g_pIPCamInfo_LastSetupCam[i];
         m_pIPCamInfoArr[i].m_nIPCamStreamIdx = i;
         m_pIPCamInfoArr[i].m_nVideoStreamNo  = i;
      }
   }

   CRect rtChildPlaceMarker;
   GetDlgItem (IDC_CHILDPLACEMARKER)->GetWindowRect (rtChildPlaceMarker);

   GetDlgItem(IDC_TAB_VIDEO_STREAM)->ShowWindow(SW_HIDE);
   CRect rectTab;      
   GetClientRect(rectTab);
   rectTab.DeflateRect(0, 0, 0, 10);   

   m_IPCamDlg.CRHCreateGenericChildDialog (this, IDC_CHILDPLACEMARKER, 0, NULL);
   m_PTZDlg_IPCam.CRHCreateGenericChildDialog (this, IDC_CHILDPLACEMARKER, 0, NULL);  // OnInitDialog에서 속성값들이 변경될 수 있음.         

   m_tabVideoStream.SetCtrlMode(TRUE);
   m_tabVideoStream.UpdateUiType(1);
   m_tabVideoStream.Create(CBCGPTabWnd::STYLE_3D_ONENOTE, rectTab, this, 1, CBCGPTabWnd::LOCATION_TOP);   
   m_tabVideoStream.SetTabBorderSize(0);
   m_tabVideoStream.SetButtonsVisible(FALSE);

   for (int i = 0; i < MAX_IP_CAMERA_STREAM_NUM; i++)
   {
      CString str;
      str.Format (GetStringML(IDD_TEXT(IDS_STREAM_CH)), i+1);

      m_tabVideoStream.AddTab(NULL, str, -1, FALSE);
   }   

   OnChangeVideoStream (m_nTabIdx_VideoStream);
   m_tabVideoStream.SetActiveTab(m_nTabIdx_VideoStream);

   MLANG_WND_LOAD(GetSafeHwnd(), IDD_SETUP_VIDEO_INPUT_IP_CAMERA_STREAM);

   m_bInit = TRUE;

   return TRUE;
}

void CSetupIPCameraStreamDlg::LoadLangEvent()
{
   for (int i = 0; i < MAX_IP_CAMERA_STREAM_NUM; i++)
   {
      CString str;
      str.Format (GetStringML(IDD_TEXT(IDS_STREAM_CH)), i+1);
    
      m_tabVideoStream.SetTabLabel(i, str);
   }
}

BOOL CSetupIPCameraStreamDlg::PreTranslateMessage(MSG* pMsg)
{
   return CRHGenericChildDialog::PreTranslateMessage(pMsg);
}

LRESULT CSetupIPCameraStreamDlg::OnChangingActiveTab(WPARAM wp, LPARAM lp)
{
   int nIndex = (int)wp;
   if( -1 == nIndex || FALSE == m_bInit )
      return 0;

   OnChangeVideoStream (nIndex);

   return 0;
}

void CSetupIPCameraStreamDlg::OnBnClickedCheckUseStream()
{
   CButton* pButton = (CButton*)GetDlgItem (IDC_CHECK_USE_STREAM);
   pButton->GetCheck (   );
   m_pIPCamInfo->m_bUseStream = pButton->GetCheck (   );
   m_IPCamDlg.SetActive (   );
}

void CSetupIPCameraStreamDlg::OnBnClickedGetMainStreamConnectionInfo()
{
   if (0 == m_nTabIdx_VideoStream) return;
   m_IPCamDlg.KillActive (   );
   CIPCameraInfo* pIPCamInfo_MainStream = &m_pVCInfo->m_IPCamInfoArr[0];
   CIPCameraInfo* pIPCamInfo_CurrStream = &m_pVCInfo->m_IPCamInfoArr[m_nTabIdx_VideoStream];
   pIPCamInfo_CurrStream->m_nIPCameraType = pIPCamInfo_MainStream->m_nIPCameraType;
   if (IPCameraType_RTSP == pIPCamInfo_CurrStream->m_nIPCameraType) 
   {
      pIPCamInfo_CurrStream->m_strURL = pIPCamInfo_MainStream->m_strURL;
   }
   else
   {
      pIPCamInfo_CurrStream->m_strIPAddress  = pIPCamInfo_MainStream->m_strIPAddress;
      pIPCamInfo_CurrStream->m_nPortNo       = pIPCamInfo_MainStream->m_nPortNo;
      pIPCamInfo_CurrStream->m_nPTZPortNo    = pIPCamInfo_MainStream->m_nPTZPortNo;
   }
   pIPCamInfo_CurrStream->m_strUserName   = pIPCamInfo_MainStream->m_strUserName;
   pIPCamInfo_CurrStream->m_strPassword   = pIPCamInfo_MainStream->m_strPassword;
   UpdateData (FALSE);
   m_IPCamDlg.Initialize (m_nVCSetupMode, m_pVCInfo, m_pIPCamInfo, &m_pVCInfo->m_PTZInfo, &m_PTZDlg_IPCam);
   ShowChildWindows (   );
}

void CSetupIPCameraStreamDlg::SetActive ()
{
   m_IPCamDlg.SetActive (   );
   m_PTZDlg_IPCam.SetActive (   );
}

void CSetupIPCameraStreamDlg::KillActive ()
{
   m_IPCamDlg.KillActive (   );
   m_PTZDlg_IPCam.KillActive (   );
}

void CSetupIPCameraStreamDlg::Initialize (int nVCSetupMode, CVideoChannelManager* pVCM, CVideoChannelInfo* pVCInfo)
{
   m_nVCSetupMode  = nVCSetupMode;
   m_pVCM          = pVCM;
   m_pVCInfo       = pVCInfo;
   m_pIPCamInfo    = &pVCInfo->m_IPCamInfoArr[m_nTabIdx_VideoStream];
   m_pIPCamInfoArr = pVCInfo->m_IPCamInfoArr;

   m_IPCamDlg.Initialize (m_nVCSetupMode, m_pVCInfo, m_pIPCamInfo, &m_pVCInfo->m_IPCamInfo.m_PTZInfo, &m_PTZDlg_IPCam);
   m_PTZDlg_IPCam.Initialize (VideoChannelType_IPCamera, m_nVCSetupMode, m_pVCM, m_pVCInfo, &m_pVCInfo->m_IPCamInfo.m_PTZInfo);
}

void CSetupIPCameraStreamDlg::InitUI ()
{

}

void CSetupIPCameraStreamDlg::SetState (int nState)
{
   m_nState = nState;
}

BOOL CSetupIPCameraStreamDlg::UpdateData (BOOL bSaveAndValidate)
{
   m_IPCamDlg.UpdateData (bSaveAndValidate);
   m_PTZDlg_IPCam.UpdateData (bSaveAndValidate);
   return CBCGPDialog::UpdateData (bSaveAndValidate);
}


void CSetupIPCameraStreamDlg::ShowChildWindows (   )
{
   m_IPCamDlg.ShowWindow (SW_SHOW);
   m_IPCamDlg.m_bActivePage = TRUE;
   m_IPCamDlg.SetActive (   );

   BOOL bSRSChannel = FALSE;
   if (IPCameraType_IVXVideoServer == m_pVCInfo->m_IPCamInfoArr[0].m_nIPCameraType) bSRSChannel = TRUE;
   m_PTZDlg_IPCam.EnableWindow (!bSRSChannel);

   if (0 == m_pIPCamInfo->m_nIPCamStreamIdx && 
      m_pIPCamInfo->m_bUsePTZ) 
   {
      CRect rt;
      GetWndRect (rt, this, &m_IPCamDlg);
      int nY = rt.top + rt.Height();
      m_PTZDlg_IPCam.SetWindowPos (NULL, rt.left, nY, 0, 0, SWP_NOSIZE | SWP_SHOWWINDOW);
      m_PTZDlg_IPCam.ShowEnableCtrls ();
   }
   else {
      m_PTZDlg_IPCam.ShowWindow (SW_HIDE);
   }
}

void CSetupIPCameraStreamDlg::OnChangeVideoStream (int nStreamIdx) 
{
   UpdateData (TRUE);
   if( m_bInit )
      m_IPCamDlg.KillActive (   );

   // 스트림 탭이 변경되는 경우 메인스트림의 아이피카메라 타입과 일치하도록 한다. (RTSP는 제외)
   m_pIPCamInfo = &m_pIPCamInfoArr[nStreamIdx];
   if (0 == m_nTabIdx_VideoStream && 0 != nStreamIdx) {
      CIPCameraInfo* pIPCamInfoMain = &m_pIPCamInfoArr[0];
      BOOL bSRSChannel = FALSE;
      if (IPCameraType_IVXVideoServer == m_pVCInfo->m_IPCamInfoArr[0].m_nIPCameraType) bSRSChannel = TRUE;
      if (FALSE == bSRSChannel) {
         if (IPCameraType_RTSP != m_pIPCamInfo->m_nIPCameraType) {
            m_pIPCamInfo->m_nIPCameraType = pIPCamInfoMain->m_nIPCameraType;
         }
      }
   }

   m_nTabIdx_VideoStream = nStreamIdx;
   UpdateData (FALSE);

   // 메인스트림은 항상 "스트림 사용" 체크버튼을 활성화 시키고 항상 "체크"되도록 한다.  
   CButton* pButton_CheckUseStream = (CButton*)GetDlgItem (IDC_CHECK_USE_STREAM);
   if (0 == m_nTabIdx_VideoStream) {
      m_pIPCamInfo->m_bUseStream = TRUE;
      pButton_CheckUseStream->SetCheck (TRUE);
      pButton_CheckUseStream->EnableWindow (FALSE);
   }
   else {
      pButton_CheckUseStream->EnableWindow (TRUE);
   }

   m_IPCamDlg.Initialize (m_nVCSetupMode, m_pVCInfo, m_pIPCamInfo, &m_pVCInfo->m_IPCamInfo.m_PTZInfo, &m_PTZDlg_IPCam);
   ShowChildWindows (   );

   // 메인스트림의 접속 설정 가져오기 버튼 활성화
   CButton* pButton_GetMainStreamConnectionInfo = (CButton*)GetDlgItem (IDC_BUTTON_GET_MAIN_STREAM_CONNECTION_INFO);
   BOOL bEnableGetMainStreamConnectionInfo = TRUE;
   if (0 == m_nTabIdx_VideoStream) bEnableGetMainStreamConnectionInfo = FALSE;
   pButton_GetMainStreamConnectionInfo->EnableWindow (bEnableGetMainStreamConnectionInfo);
}

void CSetupIPCameraStreamDlg::SetCheckPTZControl()
{
   m_IPCamDlg.SetCheckPTZControl();
}



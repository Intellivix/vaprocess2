
#include "stdafx.h"
#include "VCM.h"
#include "SetupDialog.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNAMIC(CSetupDialog, CBCGPDialog)

CSetupDialog::CSetupDialog(CWnd* pParent)
{
	Attribute	 = 0;
	ID				 = 0;
	TemplateID   = 0;
	bCreated		 = FALSE;
	m_pParentWnd = pParent;
   System = NULL;

   EnableVisualManagerStyle(TRUE);
}

CSetupDialog::~CSetupDialog()
{
}


BEGIN_MESSAGE_MAP(CSetupDialog, CBCGPDialog)
END_MESSAGE_MAP()

// CSetupDialog 메시지 처리기입니다.
BOOL CSetupDialog::Create()
{
	ASSERT(!bCreated);
	ASSERT(m_pParentWnd);

   if( bCreated )
      return FALSE;

	return (bCreated = CBCGPDialog::Create(TemplateID, m_pParentWnd));
}

BOOL CSetupDialog::ShowSetupWindow(int nCmdShow)
{
   return CBCGPDialog::ShowWindow (nCmdShow);
}


#include "stdafx.h"
#include "version.h"
#include "VCM.h"
#include "SetupIPCameraDlg.h"
#include "Camera_IP.h"
#include "SetupDialog_VideoChannel.h"
#include "SupportFunction.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

static int g_nChildCtrlsID [] = 
{
   IDC_STATIC_DEVICE_MODEL,
   IDC_COMBO_DEVICE_MODEL,
   IDC_STATIC_IP_ADDRESS,
   IDC_EDIT_IP_ADDRESS,
   IDC_STATIC_PORT_NUM,
   IDC_EDIT_PORT_NUMBER,
   IDC_STATIC_PTZ_PORT_NUMBER,
   IDC_EDIT_PTZ_PORT_NUMBER,
   IDC_BUTTON_SET_DEFAULT,
   IDC_STATIC_USER_NAME,
   IDC_EDIT_USER_NAME,
   IDC_STATIC_PASSWORD,
   IDC_EDIT_PASSWORD,
   IDC_STATIC_VIDEO_CH_NO,
   IDC_COMBO_VIDEO_CH_NO,
   IDC_STATIC_VIDEO_STREAM_INDEX,
   IDC_COMBO_VIDEO_STREAM_INDEX,
   IDC_STATIC_LOGICALID,
   IDC_EDIT_LOGICALID,
   IDC_STATIC_URL,
   IDC_EDIT_URL,
   IDC_STATIC_DEVICE_NAME,
   IDC_STATIC_DEVICE_ID,
   IDC_EDIT_DEVICE_ID,
   IDC_CHECK_PTZ_CONTROL,
   IDC_BUTTON_OPEN_WEP_SETUP_PAGE,
   IDC_BUTTON_OPEN_VMS_CONNECTION_SETUP_DIALOG,
   IDC_BUTTON_TEST_IP_CAMERA_WINDOW,
   IDC_STATIC_VIDEO_PROPERTY_GROUP,
   IDC_STATIC_VIDEO_FORMAT,
   IDC_COMBO_VIDEO_FORMAT,
   IDC_STATIC_VIDEO_TYPE,
   IDC_COMBO_VIDEO_TYPE,
   ReductionOfInputVideoResolution_Group,
   IDC_COMBO_VIDEO_RESOLUTION,
   FrameRate_Static,
   IDC_COMBO_FRAME_RATE,
   IDC_STATIC_IMAGE_QUALITY,
   IDC_EDIT_IMAGE_QUALITY,
   IDC_SPIN_IMAGE_QUALITY,
   IDC_STATIC_FPS,
   IDC_STATIC_SOCKET_TYPE,
   IDC_COMBO_SOCKET_TYPE,
   IDC_COMBO_AUDIO_OUT,
   IDC_COMBO_STREAM_TYPE,
   IDC_CHECK_HTTP_TYPE,
   0,
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: CSetupIPCameraDlg
//
///////////////////////////////////////////////////////////////////////////////

IMPLEMENT_DYNAMIC(CSetupIPCameraDlg, CDialog)

CSetupIPCameraDlg::CSetupIPCameraDlg()
{
   m_nState = 0;
	m_nVCSetupMode = 0;
   m_pPTZInfo = NULL;
   m_pSetupPTZControlDlg = NULL;
}

CSetupIPCameraDlg::~CSetupIPCameraDlg()
{
}

int CSetupIPCameraDlg::CRHGetDialogID ()
{
   return IDD_SETUP_VIDEO_INPUT_IP_CAMERA;
}

void CSetupIPCameraDlg::DoDataExchange(CDataExchange* pDX)
{   
   CRHGenericChildDialog::DoDataExchange(pDX);

   DDX_Text(pDX, IDC_EDIT_IP_ADDRESS,     m_pIPCamInfo->m_strIPAddress); // (xinu_bc23)
   DDX_Text(pDX, IDC_EDIT_URL,            m_pIPCamInfo->m_strURL);
   if (m_pIPCamInfo->m_nIPCameraType != IPCameraType_RTSP) 
      DDX_Text(pDX, IDC_EDIT_PORT_NUMBER,    m_pIPCamInfo->m_nPortNo);
   DDX_Text(pDX, IDC_EDIT_PTZ_PORT_NUMBER,m_pIPCamInfo->m_nPTZPortNo);
   DDX_Text(pDX, IDC_EDIT_USER_NAME,      m_pIPCamInfo->m_strUserName);
   DDX_Text(pDX, IDC_EDIT_PASSWORD,       m_pIPCamInfo->m_strPassword);
   DDX_Text(pDX, IDC_EDIT_IMAGE_QUALITY,  m_pIPCamInfo->m_nCompression);

   switch (m_pIPCamInfo->m_nIPCameraType) 
   {
      case IPCameraType_Innodep:
         DDX_Text(pDX, IDC_EDIT_DEVICE_ID, m_pIPCamInfo->m_nDeviceID);
         break;
      case IPCameraType_XProtect:
         DDX_Text(pDX, IDC_EDIT_DEVICE_ID, m_pIPCamInfo->m_strDeviceGUID);
         break;
      case IPCameraType_Omnicast:
      case IPCameraType_SecurityCenter:
         DDX_Text(pDX, IDC_EDIT_DEVICE_ID, m_pIPCamInfo->m_strDeviceUID);
         DDX_Text(pDX, IDC_EDIT_LOGICALID, m_pIPCamInfo->m_strLogicalID);
         break;
   }

   DDX_Text  (pDX, IDC_EDIT_DEVICE_NAME,  m_pIPCamInfo->m_strDeviceName);
   DDX_Check (pDX, IDC_CHECK_PTZ_CONTROL, m_pIPCamInfo->m_bUsePTZ);
   DDX_Check (pDX, IDC_CHECK_HTTP_TYPE,   m_pIPCamInfo->m_bNewHttpRule);
   
}

BOOL CSetupIPCameraDlg::OnInitDialog()
{
   m_pIPCamProperty = GetIPCameraProperty (m_pIPCamInfo->m_nIPCameraType);

   InitUI (  );
   if (m_nState & State_SetupPanorama) {
      GetDlgItem (IDC_COMBO_VIDEO_TYPE)->EnableWindow (FALSE);
      GetDlgItem (IDC_COMBO_VIDEO_RESOLUTION)->EnableWindow (FALSE);
      GetDlgItem (IDC_COMBO_FRAME_RATE)->EnableWindow (FALSE);
   }
   
	((CSpinButtonCtrl*)GetDlgItem(IDC_SPIN_IMAGE_QUALITY))->SetRange(1,100);

   m_ToolTip.Create(this, TTS_ALWAYSTIP);

   if (m_nState & State_SetupPanorama) {
      UpdateIPCameraType (   );
   }

   if( IS_PRODUCT(__PRODUCT_B200) )
      GetDlgItem (IDC_BUTTON_OPEN_VMS_CONNECTION_SETUP_DIALOG)->ShowWindow (SW_HIDE);

   CRHGenericChildDialog::OnInitDialog ();
   SetMultiLangInfo(GetSafeHwnd(), IDD_TEXT(IDD_SETUP_VIDEO_INPUT_IP_CAMERA));
   LoadLanguage();

   return TRUE;
}

BOOL CSetupIPCameraDlg::PreTranslateMessage(MSG* pMsg)
{
   if (pMsg->message >= WM_MOUSEFIRST && pMsg->message <= WM_MOUSELAST)
      m_ToolTip.RelayEvent(pMsg);
   return CRHGenericChildDialog::PreTranslateMessage(pMsg);
}

void CSetupIPCameraDlg::SetActive ()
{
   InitUI (   );
   CComboBox* pComboBox;
   pComboBox = (CComboBox*) GetDlgItem (IDC_COMBO_DEVICE_MODEL);
   SetCurSel_ByData (pComboBox, m_pIPCamInfo->m_nIPCameraType);
   UpdateIPCameraType (   );
   UpdateData (FALSE);
}

void CSetupIPCameraDlg::KillActive ()
{
   //if( IS_SUPPORT(__SUPPORT_FISHEYE_DEWARPING) ) {
   //   // 카메라 종류가 바뀔 때는 "어안렌즈 카메라" 체크박스를 해제 (mkjang-150615)
   //   m_pVCInfo->m_bIsFisheyeCam = FALSE;
   //   ((CButton*)GetDlgItem(IDC_CHECK_FISHEYE_CAMERA))->SetCheck(m_pVCInfo->m_bIsFisheyeCam);
   //}
   UpdateData (TRUE);
}

BEGIN_MESSAGE_MAP(CSetupIPCameraDlg, CRHGenericChildDialog)
   ON_CBN_SELCHANGE(IDC_COMBO_DEVICE_MODEL, OnCbnSelchangeComboDeviceModel)
   ON_CBN_SELCHANGE(IDC_COMBO_VIDEO_TYPE, OnCbnSelchangeComboVideoType)
   ON_CBN_SELCHANGE(IDC_COMBO_VIDEO_RESOLUTION, OnCbnSelchangeComboResolution)
   ON_BN_CLICKED(IDC_BUTTON_OPEN_WEP_SETUP_PAGE, OnBnClickedButtonOpenWebSetupPage)      
   ON_BN_CLICKED(IDC_BUTTON_SET_DEFAULT, OnBnClickedButtonSetDefault)
   ON_BN_CLICKED(IDC_CHECK_PTZ_CONTROL, OnBnClickedCheckPTZControl)
   ON_CBN_SELCHANGE(IDC_COMBO_VIDEO_CH_NO, OnCbnSelchangeComboVideoChNo)
   ON_CBN_SELCHANGE(IDC_COMBO_VIDEO_FORMAT, OnCbnSelchangeComboVideoFormat)
   ON_CBN_SELCHANGE(IDC_COMBO_FRAME_RATE, OnCbnSelchangeComboFrameRate)
   ON_CBN_SELCHANGE(IDC_COMBO_VIDEO_STREAM_INDEX, OnCbnSelchangeComboVideoStreamIndex)
   ON_EN_CHANGE(IDC_EDIT_IP_ADDRESS, OnEnChangeEditIpAddress)
   ON_CBN_SELCHANGE(IDC_COMBO_SOCKET_TYPE, OnCbnSelchangeComboSocketType)
   ON_CBN_SELCHANGE(IDC_COMBO_AUDIO_OUT, OnCbnSelchangeComboAudioOut)   
   ON_CBN_SELCHANGE(IDC_COMBO_STREAM_TYPE, OnCbnSelchangeComboStreamType)
   ON_BN_CLICKED(IDC_CHECK_HTTP_TYPE, OnBnClickedCheckHttpAddr)
END_MESSAGE_MAP()

void CSetupIPCameraDlg::OnCbnSelchangeComboDeviceModel()
{
   CComboBox* pComboBox;
   pComboBox = (CComboBox*) GetDlgItem (IDC_COMBO_DEVICE_MODEL);
   m_pIPCamInfo->m_nIPCameraType = pComboBox->GetItemData (pComboBox->GetCurSel ( ));

   UpdateData(FALSE);
   UpdateIPCameraType ();
   if (CRHpParent->IsKindOf (RUNTIME_CLASS(CSetupDialog_VideoChannel)))
      ((CSetupDialog_VideoChannel*)CRHpParent)->ShowVideoChannelSetupDlg ();

   if (IPCameraType_IVXVideoServer == m_pIPCamInfo->m_nIPCameraType) m_pIPCamInfo->m_bUsePTZ = FALSE;

   if (CRHpParent->IsKindOf (RUNTIME_CLASS(CSetupIPCameraStreamDlg)))
      ((CSetupIPCameraStreamDlg*)CRHpParent)->ShowChildWindows ();

   if (m_pIPCamInfo->m_bUsePTZ) {
      if (m_pSetupPTZControlDlg) {
         if (WS_VISIBLE & m_pSetupPTZControlDlg->GetStyle (   ))
            m_pSetupPTZControlDlg->InitUI (   );
      }
   }   
}

void CSetupIPCameraDlg::OnCbnSelchangeComboSocketType()
{
   CComboBox* pComboBox = (CComboBox*) GetDlgItem (IDC_COMBO_SOCKET_TYPE);
   if(pComboBox)
      m_pIPCamInfo->m_nSocketType = pComboBox->GetItemData (pComboBox->GetCurSel ( ));
}

void CSetupIPCameraDlg::OnCbnSelchangeComboAudioOut()
{
   CComboBox* pComboBox = (CComboBox*) GetDlgItem (IDC_COMBO_AUDIO_OUT);
   if(pComboBox)
      m_pIPCamInfo->m_nUseAudioOut = pComboBox->GetItemData (pComboBox->GetCurSel ( ));
}

void CSetupIPCameraDlg::OnCbnSelchangeComboVideoType()
{
   CComboBox* pComboBox;
   pComboBox = (CComboBox*) GetDlgItem (IDC_COMBO_VIDEO_TYPE);
   m_pIPCamInfo->m_nVideoType = pComboBox->GetItemData (pComboBox->GetCurSel ( ));
   UpdateVideoFormat ();
}

void CSetupIPCameraDlg::OnCbnSelchangeComboStreamType()
{
   CComboBox* pComboBox = (CComboBox*) GetDlgItem (IDC_COMBO_STREAM_TYPE);
   if(pComboBox) {
      m_pIPCamInfo->m_nStreamType = pComboBox->GetItemData (pComboBox->GetCurSel ( ));

      // (mkjang-141219) 
      CString strPortNo, strPTZPortNo;
      int nCurSel = pComboBox->GetCurSel ();
      if (0 == nCurSel) { // RTSP
         m_pIPCamInfo->m_nPortNo = 554;
         m_pIPCamInfo->m_nPTZPortNo = 80;         
      }
      else if (1 == nCurSel) { // SDK
         if (m_pIPCamProperty->m_nDefPortNo) {
            m_pIPCamInfo->m_nPortNo = m_pIPCamProperty->m_nDefPortNo;
         }
         if (m_pIPCamProperty->m_nDefPTZPortNo) {
            m_pIPCamInfo->m_nPTZPortNo = m_pIPCamProperty->m_nDefPTZPortNo;
         }         
      }
      strPortNo.Format (_T("%d"), m_pIPCamInfo->m_nPortNo);
      strPTZPortNo.Format (_T("%d"), m_pIPCamInfo->m_nPTZPortNo);

      GetDlgItem (IDC_EDIT_PORT_NUMBER)->SetWindowText (strPortNo);
      GetDlgItem (IDC_EDIT_PTZ_PORT_NUMBER)->SetWindowText (strPTZPortNo);
   }
}

void CSetupIPCameraDlg::OnCbnSelchangeComboResolution()
{
   CComboBox* pComboBox;
   pComboBox = (CComboBox*) GetDlgItem (IDC_COMBO_VIDEO_RESOLUTION);
   m_pIPCamInfo->m_nResolutionIdx = pComboBox->GetCurSel ( );
}

void CSetupIPCameraDlg::OnBnClickedButtonOpenWebSetupPage()
{
   std::string strWebSetupURL;

   std::string strIPAddress = m_pIPCamInfo->m_strIPAddress;
   if (IPCameraType_RTSP == m_pIPCamInfo->m_nIPCameraType) {
      int nAdd1, nAddr2, nAddr3, nAddr4;
      nAdd1 = nAddr2 = nAddr3 = nAddr4 = 0;
      sscanf (m_pIPCamInfo->m_strURL.c_str(), _T("rtsp://%d.%d.%d.%d"), &nAdd1, &nAddr2, &nAddr3, &nAddr4);
      strIPAddress = sutil::sformat (_T("%d.%d.%d.%d"), nAdd1, nAddr2, nAddr3, nAddr4);
   }

   strWebSetupURL = sutil::sformat(_T("http://%s"), strIPAddress);

   std::string strWebPortNo;
   if (m_pIPCamProperty && m_pIPCamProperty->m_nCapabilities & IPCamCaps_PTZPortIsWebPort) {
      strWebPortNo = sutil::sformat(":%d", m_pIPCamInfo->m_nPTZPortNo);
   }

   if (strWebPortNo.length (   ))
      strWebSetupURL += strWebPortNo;

   std::string strSetupURL;
   if (m_pIPCamProperty) {
      if (m_pIPCamProperty->m_strSetupURL.length(   )) {
         strSetupURL = sutil::sformat("/%s", m_pIPCamProperty->m_strSetupURL);
      }
   }
   if (strSetupURL.length(   ))
      strWebSetupURL += strSetupURL;

   ::ShellExecute(this->m_hWnd, _T("open"), _T("IEXPLORE.EXE"), strWebSetupURL.c_str(), NULL, SW_SHOWNORMAL);
}

void CSetupIPCameraDlg::OnBnClickedButtonSetDefault()
{
   CString str;
   if (m_pIPCamProperty->m_nDefPortNo) {
      m_pIPCamInfo->m_nPortNo = m_pIPCamProperty->m_nDefPortNo;
      str.Format (_T("%d"), m_pIPCamInfo->m_nPortNo);
      GetDlgItem (IDC_EDIT_PORT_NUMBER)->SetWindowText (str);
   }
   if (m_pIPCamProperty->m_nDefPTZPortNo) {
      m_pIPCamInfo->m_nPTZPortNo = m_pIPCamProperty->m_nDefPTZPortNo;
      str.Format (_T("%d"), m_pIPCamInfo->m_nPTZPortNo);
      GetDlgItem (IDC_EDIT_PTZ_PORT_NUMBER)->SetWindowText (str);
   }
   if (m_pIPCamProperty->m_strDefUserName.length()) {
      m_pIPCamInfo->m_strUserName = m_pIPCamProperty->m_strDefUserName;
      GetDlgItem (IDC_EDIT_USER_NAME)->SetWindowText (m_pIPCamInfo->m_strUserName.c_str());
   }
   if (m_pIPCamProperty->m_strDefUserName.length()) {
      m_pIPCamInfo->m_strPassword = m_pIPCamProperty->m_strDefPassword;
      GetDlgItem (IDC_EDIT_PASSWORD)->SetWindowText (m_pIPCamInfo->m_strPassword.c_str());
   }
}

void CSetupIPCameraDlg::OnBnClickedCheckPTZControl()
{
   m_pIPCamInfo->m_bUsePTZ = ((CButton*)GetDlgItem (IDC_CHECK_PTZ_CONTROL))->GetCheck();
   
   UpdateData(TRUE); // 컨트롤 내용이 초기화 되는 문제 때문에 추가 (mkjang-151027)
   if (CRHpParent->IsKindOf (RUNTIME_CLASS(CSetupIPCameraStreamDlg)))
      ((CSetupIPCameraStreamDlg*)CRHpParent)->ShowChildWindows ();  

   if (m_pIPCamInfo->m_bUsePTZ) {
      if (m_pSetupPTZControlDlg) {
         if (WS_VISIBLE & m_pSetupPTZControlDlg->GetStyle (   ))
            m_pSetupPTZControlDlg->InitUI (   );
      }
   }

   if (IS_SUPPORT(__SUPPORT_FISHEYE_DEWARPING)) {
      if (m_pIPCamInfo->m_bUsePTZ) m_pVCInfo->m_bFisheyeCam = FALSE;
   }

}

void CSetupIPCameraDlg::OnCbnSelchangeComboVideoChNo()
{
   CComboBox* pComboBox;
   pComboBox = (CComboBox*) GetDlgItem (IDC_COMBO_VIDEO_CH_NO);
   GetCurData (pComboBox, m_pIPCamInfo->m_nVideoChNo);
}

void CSetupIPCameraDlg::OnCbnSelchangeComboVideoFormat()
{
   CComboBox* pComboBox;
   pComboBox = (CComboBox*) GetDlgItem (IDC_COMBO_VIDEO_FORMAT);
   GetCurData (pComboBox, m_pIPCamInfo->m_nVideoFormat_Cap);
}

void CSetupIPCameraDlg::OnCbnSelchangeComboFrameRate()
{
   CComboBox* pComboBox;
   pComboBox = (CComboBox*) GetDlgItem (IDC_COMBO_FRAME_RATE);
   float fFrameRate;
   GetCurData (pComboBox,  fFrameRate);
   m_pIPCamInfo->m_fFPS_Cap = (float) fFrameRate;
}

void CSetupIPCameraDlg::OnCbnSelchangeComboVideoStreamIndex()
{
   CComboBox* pComboBox;
   pComboBox = (CComboBox*) GetDlgItem (IDC_COMBO_VIDEO_STREAM_INDEX);
   GetCurData (pComboBox,  m_pIPCamInfo->m_nVideoStreamNo);
}


void CSetupIPCameraDlg::OnBnClickedCheckHttpAddr()
{
  m_pIPCamInfo->m_bNewHttpRule = ((CButton*)GetDlgItem (IDC_CHECK_HTTP_TYPE))->GetCheck();
   UpdateData(TRUE);
}
void CSetupIPCameraDlg::OnEnChangeEditIpAddress()
{
   UpdateData (TRUE);
}

void CSetupIPCameraDlg::Initialize (int nVCSetupMode, CVideoChannelInfo* pVCInfo, CIPCameraInfo* pIPCamInfo, CPTZCtrlInfo* pPTZInfo, CSetupPTZControlDlg* pSetupPTZControlDlg)
{
   m_pVCInfo             = pVCInfo;
   m_nVCSetupMode        = nVCSetupMode;
   m_pIPCamInfo          = pIPCamInfo;
   m_pPTZInfo            = pPTZInfo;
   m_pIPCamProperty      = GetIPCameraProperty (m_pIPCamInfo->m_nIPCameraType);
   m_pSetupPTZControlDlg = pSetupPTZControlDlg;
   // Multi Streaming의 경우 첫번째 스트림에서만 PTZ제어 설정을 한다. 
   if (m_pIPCamInfo) {
      if (m_pIPCamInfo->m_nIPCamStreamIdx > 0) {
         m_pIPCamInfo->m_bUsePTZ = FALSE;
      }
   }
}

void CSetupIPCameraDlg::Initialize (int nVCSetupMode, CIPCameraInfo* pIPCamInfo, CPTZCtrlInfo* pPTZInfo, CSetupPTZControlDlg* pSetupPTZControlDlg)
{
   Initialize (nVCSetupMode, NULL, pIPCamInfo, pPTZInfo, pSetupPTZControlDlg);
}

void CSetupIPCameraDlg::InitUI ()
{
   int i;
   int nItem;
   CComboBox* pComboBox;
   // Device Model
   pComboBox = (CComboBox*) GetDlgItem (IDC_COMBO_DEVICE_MODEL);
   pComboBox->SetRedraw (FALSE);
   pComboBox->ResetContent (   );
   std::vector<int> vtIPCameraType;
   GetIPCameraTypeList (vtIPCameraType);

   // IP 카메라 종류 추가.
   pComboBox->AddString (MLANG(IDS_NOT_SELECTED)); // 선택 안함 추가.
   pComboBox->SetItemData (0, 0);

   nItem = vtIPCameraType.size ();
   BOOL bSRSChannel = FALSE;
   if (m_pVCInfo && IPCameraType_IVXVideoServer == m_pVCInfo->m_IPCamInfoArr[0].m_nIPCameraType) bSRSChannel = TRUE;
   BOOL bProductSRS = IS_PRODUCT(__PRODUCT_SRS);

   if (m_pIPCamInfo->m_nIPCamStreamIdx == 0 || bSRSChannel) {
      for (i = 1; i <= nItem; i++) {
#ifndef __KISA_2016_TEST
         if (!bProductSRS && !(m_nVCSetupMode & VCSetupMode_SetupRemote))
            if (vtIPCameraType[i-1] == IPCameraType_IVXVideoServer)
               continue;
#endif
         int idx = pComboBox->AddString (GetIPCameraProductName (vtIPCameraType[i-1]).c_str());
         pComboBox->SetItemData (idx, (DWORD_PTR)vtIPCameraType[i-1]);

      }
   }
   else {
      if (m_pVCInfo) {
         CIPCameraInfo* pMainIPCamInfo = &m_pVCInfo->m_IPCamInfoArr[0];
         pComboBox->AddString (GetIPCameraProductName (pMainIPCamInfo->m_nIPCameraType).c_str());
         pComboBox->SetItemData (1, (DWORD_PTR)pMainIPCamInfo->m_nIPCameraType);
         pComboBox->AddString (GetIPCameraProductName (IPCameraType_RTSP).c_str());
         pComboBox->SetItemData (2, (DWORD_PTR)IPCameraType_RTSP);
      }
   }
   pComboBox->SetRedraw (TRUE);
   SetCurSel_ByData (pComboBox, m_pIPCamInfo->m_nIPCameraType);
   GetCurData (pComboBox, m_pIPCamInfo->m_nIPCameraType);

   
	if (m_nVCSetupMode & VCSetupMode_SetupRemote) 
		GetDlgItem(IDC_BUTTON_TEST_IP_CAMERA_WINDOW)->EnableWindow(FALSE);

   // PTZ 카메라 사용 여부 체크 박스
   if (m_nVCSetupMode & VCSetupMode_DoNotAllowPTZCamera) {
      m_pIPCamInfo->m_bUsePTZ = FALSE;
      GetDlgItem(IDC_CHECK_PTZ_CONTROL)->EnableWindow(FALSE);
   }
   if (m_nVCSetupMode & VCSetupMode_SetupPTZCamera) {
      m_pIPCamInfo->m_bUsePTZ = TRUE;
      GetDlgItem(IDC_CHECK_PTZ_CONTROL)->EnableWindow(TRUE);
   }

   CRect rt;
   GetWndRect (rt, this, GetDlgItem (IDC_STATIC_IP_ADDRESS));
   GetDlgItem (IDC_STATIC_URL)->SetWindowPos (NULL, rt.left, rt.top, rt.Width (), rt.Height (), SWP_HIDEWINDOW);
   GetWndRect (rt, this, GetDlgItem (IDC_EDIT_IP_ADDRESS));
   GetDlgItem (IDC_EDIT_URL)->SetWindowPos (NULL, rt.left, rt.top, rt.Width (), rt.Height (), SWP_HIDEWINDOW);
   GetWndRect(rt, this, GetDlgItem(IDC_STATIC_USER_NAME));
   GetDlgItem(IDC_STATIC_DEVICE_NAME)->SetWindowPos(NULL, rt.left, rt.top, 0, 0, SWP_HIDEWINDOW | SWP_NOSIZE);
   GetWndRect(rt, this, GetDlgItem(IDC_EDIT_USER_NAME));
   GetDlgItem(IDC_EDIT_DEVICE_NAME)->SetWindowPos(NULL, rt.left, rt.top, 0, 0, SWP_HIDEWINDOW | SWP_NOSIZE);
   GetWndRect(rt, this, GetDlgItem(IDC_STATIC_PASSWORD));
   GetDlgItem(IDC_STATIC_DEVICE_ID)->SetWindowPos(NULL, rt.left, rt.top, 0, 0, SWP_HIDEWINDOW | SWP_NOSIZE);
   GetWndRect(rt, this, GetDlgItem(IDC_EDIT_PASSWORD));
   GetDlgItem(IDC_EDIT_DEVICE_ID)->SetWindowPos(NULL, rt.left, rt.top, 0, 0, SWP_HIDEWINDOW | SWP_NOSIZE);
   GetWndRect(rt, this, GetDlgItem(IDC_STATIC_VIDEO_CH_NO));
   GetDlgItem(IDC_STATIC_LOGICALID)->SetWindowPos(NULL, rt.left, rt.top, 0, 0, SWP_HIDEWINDOW | SWP_NOSIZE);
   GetWndRect(rt, this, GetDlgItem(IDC_COMBO_VIDEO_CH_NO));
   GetDlgItem(IDC_EDIT_LOGICALID)->SetWindowPos(NULL, rt.left, rt.top, 0, 0, SWP_HIDEWINDOW | SWP_NOSIZE);

   // 소켓 타입추가
   const int nCnt(2);
   CString strItems[nCnt] = {_T("UDP"), _T("TCP")};
   int nDatas[nCnt] = {0, 1};
   pComboBox = (CComboBox*) GetDlgItem (IDC_COMBO_SOCKET_TYPE);
   pComboBox->SetRedraw (FALSE);
   pComboBox->ResetContent (   );
   for(int i = 0; i < nCnt; i++) {
      pComboBox->AddString (strItems[i]);
      pComboBox->SetItemData (i, (DWORD_PTR)nDatas[i]);
   }
   pComboBox->SetRedraw (TRUE);
   SetCurSel_ByData (pComboBox, m_pIPCamInfo->m_nSocketType);
   

   // 오디오 방송 여부
   pComboBox = (CComboBox *)GetDlgItem(IDC_COMBO_AUDIO_OUT);
   pComboBox->SetRedraw (FALSE);
   pComboBox->ResetContent();
   AddComboBoxItem (pComboBox, _T("Off"), 0);
   AddComboBoxItem (pComboBox, _T("On"), 1);
   pComboBox->SetRedraw (TRUE);
   SetCurSel_ByData (pComboBox, m_pIPCamInfo->m_nUseAudioOut);

   // Stream Type
   pComboBox = (CComboBox *)GetDlgItem(IDC_COMBO_STREAM_TYPE);
   pComboBox->SetRedraw (FALSE);
   pComboBox->ResetContent();
   AddComboBoxItem (pComboBox, _T("RTSP"), IPCamStreamType_RTSP);
   AddComboBoxItem (pComboBox, _T("SDK") , IPCamStreamType_SDK);
   pComboBox->SetRedraw (TRUE);   
   SetCurSel_ByData (pComboBox, m_pIPCamInfo->m_nStreamType);
}

void CSetupIPCameraDlg::UpdateIPCameraType (  )
{
   int i;
   int nItem;
   CComboBox* pComboBox;

   m_pIPCamProperty = GetIPCameraProperty (m_pIPCamInfo->m_nIPCameraType);
   if (m_pIPCamProperty) {
      if (m_pPTZInfo) {
         if (m_pIPCamProperty->m_nPTZControlType / 10000 != m_pIPCamInfo->m_nPTZControlType / 10000) 
            m_pIPCamInfo->m_nPTZControlType = m_pIPCamProperty->m_nPTZControlType;
         m_pIPCamInfo->m_nCompanyID      = m_pIPCamProperty->m_nCompanyID;
         // 러시아-중요: 아이피 카메라의 종류가 바뀌었을 때만 아래와 같은 코드가 실행되어야 함. (러시아)
         // m_pIPCamInfo->m_PTZInfo.m_nPTZControlType = m_pIPCamProperty->m_nPTZControlType;
      }
      EnableAllCtrls (TRUE);
   }

   BOOL bShow = TRUE;
   BOOL bShowIPAddress = TRUE;
   BOOL bEnablePortNo = TRUE;
   BOOL bShowURL = FALSE;
   BOOL bShowSocketType = FALSE;
   if (IPCameraType_RTSP == m_pIPCamInfo->m_nIPCameraType)
   {
      bShowIPAddress = FALSE;      
      bEnablePortNo = FALSE;
      bShowURL = TRUE;
   }
   if (m_pIPCamProperty && m_pIPCamProperty->m_nCapabilities & IPCamCaps_ReceiveVideoStreamByRTSP) {
      bShowSocketType = TRUE;
   }
   GetDlgItem (IDC_STATIC_IP_ADDRESS)->ShowWindow (bShowIPAddress);
   GetDlgItem (IDC_EDIT_IP_ADDRESS)->ShowWindow (bShowIPAddress);
   GetDlgItem (IDC_EDIT_PORT_NUMBER)->EnableWindow (bEnablePortNo);
   GetDlgItem (IDC_STATIC_URL)->ShowWindow (bShowURL);
   GetDlgItem (IDC_EDIT_URL)->ShowWindow (bShowURL);

   // Video Port
   pComboBox = (CComboBox*) GetDlgItem (IDC_COMBO_VIDEO_CH_NO);
   pComboBox->SetRedraw (FALSE);
   pComboBox->ResetContent ( );

   int nComboBoxIdx = 0;
   if (IPCameraType_IVXVideoServer == m_pIPCamInfo->m_nIPCameraType) {
      pComboBox->AddString (GetStringML (IDD_TEXT(IDS_COMPOSED_VIDEO_CHANNEL)));    // 합성영상채널.
      pComboBox->SetItemData (0, -1);
      nComboBoxIdx++;
   }
   if (m_pIPCamProperty) {
      for (i = 0; i < m_pIPCamProperty->m_nVideoPortNum; i++) {
         CString str;
         str.Format (_T("%d"), i+1);
         pComboBox->AddString (str);
         pComboBox->SetItemData (nComboBoxIdx, i);
         nComboBoxIdx++;
      }
   }

   pComboBox->SetRedraw (TRUE);
   SetCurSel_ByData (pComboBox, m_pIPCamInfo->m_nVideoChNo);

   // Video Format (MJPG, MPEG-4)
   pComboBox = (CComboBox*) GetDlgItem (IDC_COMBO_VIDEO_FORMAT);
   pComboBox->ResetContent ( );
   if (m_pIPCamProperty) {
      pComboBox->SetRedraw (FALSE);
      nItem = (int) m_pIPCamProperty->m_VideoFormatList.size ();
      for (i = 0; i < nItem; i++) {
         pComboBox->AddString (VideoFormatString.GetString (m_pIPCamProperty->m_VideoFormatList[i]).c_str());
         pComboBox->SetItemData (i, m_pIPCamProperty->m_VideoFormatList[i]);
      }
      pComboBox->SetRedraw (TRUE);
      SetCurSel_ByData (pComboBox, m_pIPCamInfo->m_nVideoFormat_Cap);
      GetCurData (pComboBox, m_pIPCamInfo->m_nVideoFormat_Cap);
   }

   // 파노라마 카메라 설정 모드일 경우 파노라마 입력 영상 사이즈로 설정한다.
   if (m_pIPCamProperty) {
      if (m_nState & State_SetupPanorama) {
         int nVideoType       = m_pIPCamInfo->m_nVideoType;
         int nResolutionType  = m_pIPCamInfo->m_nResolutionIdx;
         int nWidth           = m_pIPCamInfo->m_nWidth;
         int nHeight          = m_pIPCamInfo->m_nHeight;
         if (m_pIPCamProperty->m_ResolutionListMgr.FindVideoTypeAndResolutionIdx (nVideoType, nResolutionType, nWidth, nHeight)) {
            m_pIPCamInfo->m_nVideoType       = nVideoType;
            m_pIPCamInfo->m_nResolutionIdx   = nResolutionType;
            m_pIPCamInfo->m_nWidth           = nWidth;
            m_pIPCamInfo->m_nHeight          = nHeight;
         }
      }
   }

   // Video Type (Progressive, NTSC, PAL)
   pComboBox = (CComboBox*) GetDlgItem (IDC_COMBO_VIDEO_TYPE);
   pComboBox->ResetContent ( );
   std::vector<int> vtSurpportedVideoTypes;
   if (m_pIPCamProperty) {
      pComboBox->SetRedraw (FALSE);
      m_pIPCamProperty->m_ResolutionListMgr.GetSupportedVideoTypeList (vtSurpportedVideoTypes);
      nItem = vtSurpportedVideoTypes.size ();
      for (i = 0; i < nItem; i++) {
         pComboBox->AddString (VideoTypeString.GetString (vtSurpportedVideoTypes[i]).c_str());
         pComboBox->SetItemData (i, vtSurpportedVideoTypes[i]);
      }
      pComboBox->SetRedraw (TRUE);
      SetCurSel_ByData (pComboBox, m_pIPCamInfo->m_nVideoType);
   }

   UpdateVideoFormat ();

   // ImageQuality
   CEdit* pEdit = (CEdit*) GetDlgItem (IDC_EDIT_IMAGE_QUALITY);
   int nImageQuality = m_pIPCamInfo->m_nCompression;
   if (m_pIPCamProperty) {
      ImageProperty& imageQualityRange = m_pIPCamProperty->m_ImageCompression;
      GetInt (pEdit, nImageQuality);
      if (imageQualityRange.m_nMin > nImageQuality) 
         nImageQuality = imageQualityRange.m_nMin;
      if (imageQualityRange.m_nMax < nImageQuality) 
         nImageQuality = imageQualityRange.m_nMax;
      SetStr (pEdit, nImageQuality);
   }
   else {
      CString text;
      text.Format (_T("%d"), nImageQuality);
      pEdit->SetWindowText (text);
   }

   BOOL bEnableTestIPCamWndBtn = TRUE;
   BOOL bEnableOpenVMSConnectionDlgBtn = FALSE;
   BOOL bShowGeneralCtrls = TRUE;
   BOOL bShowInnodepCtrls = FALSE;
   BOOL bReadOnlyIPAddrAndPortNo = FALSE;
   BOOL bShowPTZPortNo = FALSE;
   BOOL bShowSetupWebPage = TRUE;
   BOOL bReadOnlyIDAndPW = FALSE;
   BOOL bEnableVideoChNo = TRUE;
   // Support Omnicast
   BOOL bShowVideoChNo =  TRUE;
   BOOL bShowOmniCtrls = FALSE;
   // Support RealHub
   BOOL bShowRealHubCtrls = FALSE;
   BOOL bShowPTZCtrl   = TRUE;
   BOOL bEnablePTZCtrl = TRUE;   
   BOOL bShowStreamType = FALSE;

   if (m_pIPCamProperty) {
      if (m_pIPCamProperty->m_nCapabilities & IPCamCaps_NeedStreamingTypeChoice) bShowStreamType = TRUE;
      if (!(m_pIPCamProperty->m_nCapabilities & IPCamCaps_TestIPCam))            bEnableTestIPCamWndBtn = FALSE;
      if (m_pIPCamProperty->m_nCapabilities & IPCamCaps_NeedExtraPTZPortNo)      bShowPTZPortNo = TRUE;      
   }
   if (m_pPTZInfo) {
      PTZControlProperty* pPTZCtrlProperty = GetPTZControlProperty (m_pPTZInfo->m_nPTZControlType);
      if (pPTZCtrlProperty) {
         if (PTZCtrlCap_NeedExtraPTZPortNo & pPTZCtrlProperty->m_nCapabilities) 
            bShowPTZPortNo = TRUE;
         if (PTZCtrlCap_DoNotOpenSetupWebPage & pPTZCtrlProperty->m_nCapabilities) 
            bShowSetupWebPage = FALSE;
      }
   }

   switch (m_pIPCamInfo->m_nIPCameraType) 
   {
      case IPCameraType_RTSP:
         bEnableVideoChNo = FALSE;
         break;
      case IPCameraType_Innodep:
         bEnableVideoChNo = TRUE;   // TYLEE: 2015-10-29, 하위채널 선택가능하도록 수정
      case IPCameraType_XProtect:
         m_ToolTip.AddTool (GetDlgItem (IDC_BUTTON_OPEN_VMS_CONNECTION_SETUP_DIALOG), GetStringML (IDD_TEXT(IDS_OPEN_INNODEP_DIALOG)));
         bShowGeneralCtrls = FALSE;
         bShowInnodepCtrls = TRUE;
         bReadOnlyIPAddrAndPortNo = TRUE;
         if (m_pIPCamInfo->m_nIPCameraType == IPCameraType_XProtect)
            bEnableVideoChNo = FALSE;
         bEnableOpenVMSConnectionDlgBtn = TRUE;
         if (m_nVCSetupMode & VCSetupMode_SetupRemote)
            bEnableOpenVMSConnectionDlgBtn = FALSE;
         break;

      case IPCameraType_Omnicast:
      case IPCameraType_SecurityCenter:
         m_ToolTip.AddTool (GetDlgItem (IDC_BUTTON_OPEN_VMS_CONNECTION_SETUP_DIALOG), GetStringML (IDD_TEXT(IDS_OPEN_OMNICAST_DIALOG)));
         bShowGeneralCtrls = FALSE;
         bShowOmniCtrls = TRUE;
         bShowInnodepCtrls = TRUE;
         bReadOnlyIPAddrAndPortNo = TRUE;
         bEnableVideoChNo = FALSE;
         bShowVideoChNo = FALSE;
         bEnableOpenVMSConnectionDlgBtn = TRUE;
         if (m_nVCSetupMode & VCSetupMode_SetupRemote)
            bEnableOpenVMSConnectionDlgBtn = FALSE;
         break;

      case IPCameraType_RealHub:
         m_ToolTip.AddTool (GetDlgItem (IDC_BUTTON_OPEN_VMS_CONNECTION_SETUP_DIALOG), GetStringML (IDD_TEXT(IDS_OPEN_REALHUB_DIALOG)));
         bReadOnlyIPAddrAndPortNo = TRUE;
         bReadOnlyIDAndPW = TRUE;
         bShowRealHubCtrls = TRUE;
         bEnableVideoChNo = FALSE;
         bShowVideoChNo =  FALSE;
         bShowSocketType = TRUE;
         bEnableOpenVMSConnectionDlgBtn = TRUE;
         if (m_nVCSetupMode & VCSetupMode_SetupRemote)
            bEnableOpenVMSConnectionDlgBtn = FALSE;
         break;

      case IPCameraType_Onvif:
         bEnableVideoChNo = FALSE;
         bShowPTZPortNo = TRUE;
         break;


   }

   // 오디오 방송을 지원하는 카메라일 경우
   BOOL bShowControl = FALSE;
   if (m_pIPCamProperty && m_pIPCamProperty->m_nCapabilities & IPCamCaps_SupportAudioDataTransmit)
	{
	   bShowControl = TRUE;
	}
   GetDlgItem (IDC_STATIC_AUDIO_OUT)->ShowWindow(bShowControl);
   GetDlgItem (IDC_COMBO_AUDIO_OUT)->ShowWindow(bShowControl);
   if (SICode_2013_GOP_S1_BMT == g_nSICode) {
      GetDlgItem (IDC_STATIC_AUDIO_OUT)->EnableWindow(FALSE);
      GetDlgItem (IDC_COMBO_AUDIO_OUT)->EnableWindow(FALSE);
   }

   if (m_pIPCamInfo->m_nIPCamStreamIdx > 0) bShowPTZCtrl = FALSE;
   if (m_nVCSetupMode & VCSetupMode_DoNotAllowPTZCamera) bEnablePTZCtrl = FALSE;
   if (m_nState       & State_SetupPanorama)             bEnablePTZCtrl = FALSE;

   GetDlgItem (IDC_BUTTON_TEST_IP_CAMERA_WINDOW)->EnableWindow(bEnableTestIPCamWndBtn);
   GetDlgItem (IDC_BUTTON_OPEN_VMS_CONNECTION_SETUP_DIALOG   )->EnableWindow(bEnableOpenVMSConnectionDlgBtn);
   GetDlgItem (IDC_CHECK_PTZ_CONTROL)->ShowWindow(bShowPTZCtrl);
   GetDlgItem (IDC_CHECK_PTZ_CONTROL)->EnableWindow(bEnablePTZCtrl);
   GetDlgItem (IDC_STATIC_SOCKET_TYPE)->ShowWindow(bShowSocketType);
   GetDlgItem (IDC_COMBO_SOCKET_TYPE)->ShowWindow(bShowSocketType);
   GetDlgItem (IDC_CHECK_HTTP_TYPE)->ShowWindow(FALSE);

   if(m_pIPCamInfo->m_nIPCameraType == IPCameraType_YoungKook)
   {
   GetDlgItem (IDC_CHECK_HTTP_TYPE)->ShowWindow(TRUE);
   }

   int VideoPropertyControls [] =
   {
      IDC_COMBO_VIDEO_FORMAT,
      IDC_COMBO_VIDEO_TYPE,
      IDC_COMBO_VIDEO_RESOLUTION,
      IDC_COMBO_FRAME_RATE,
      IDC_EDIT_IMAGE_QUALITY,
      IDC_SPIN_IMAGE_QUALITY,
      0
   };

   BOOL bEnableVideoPropertyControls = TRUE;
   if (m_pIPCamProperty) {
      if (m_pIPCamProperty->m_nCapabilities & IPCamCaps_DisableVideoProperties) bEnableVideoPropertyControls = FALSE;
   }
   for (i = 0; VideoPropertyControls [i]; i++) {
      GetDlgItem (VideoPropertyControls [i])->EnableWindow (bEnableVideoPropertyControls);
   }

   int ImageCompressionControls [] =
   {
      IDC_STATIC_IMAGE_QUALITY,
      IDC_EDIT_IMAGE_QUALITY,
      IDC_SPIN_IMAGE_QUALITY,
      0
   };
   BOOL bEnableImgCompControls = TRUE;
   if (m_pIPCamProperty) {
      if (m_pIPCamProperty->m_nCapabilities & IPCamCaps_SetCompressionLevelNotSupported) bEnableImgCompControls = FALSE;
      if (m_pIPCamProperty->m_nCapabilities & IPCamCaps_GetCompressionLevelNotSupported) bEnableImgCompControls = FALSE;
   }
   for (i = 0; ImageCompressionControls [i]; i++) {
      GetDlgItem (ImageCompressionControls [i])->EnableWindow (bEnableImgCompControls);
   }

   int nGeneralShowCtrls [] = {
      IDC_STATIC_USER_NAME,
      IDC_EDIT_USER_NAME,
      IDC_STATIC_PASSWORD,
      IDC_EDIT_PASSWORD,
      0
   };
   int nInnodepShowCtrls [] = {
      IDC_STATIC_DEVICE_NAME,
      IDC_EDIT_DEVICE_NAME,
      IDC_STATIC_DEVICE_ID,
      IDC_EDIT_DEVICE_ID,
      0
   };
   // Support Omnicast
   int nOmniShowCtrls [] = {
      IDC_STATIC_LOGICALID,
      IDC_EDIT_LOGICALID,
      0
   };
   // Support RealHub
   int nRealHubShowCtrls [] = {
      IDC_STATIC_DEVICE_NAME,
      IDC_EDIT_DEVICE_NAME,
      0
   };
   for (i = 0; nGeneralShowCtrls[i]; i++) {
      if (GetDlgItem(nGeneralShowCtrls[i])) {
         GetDlgItem(nGeneralShowCtrls[i])->ShowWindow (bShowGeneralCtrls);
      }
   }
   for (i = 0; nInnodepShowCtrls[i]; i++) {
      if (GetDlgItem(nInnodepShowCtrls[i])) {
         GetDlgItem(nInnodepShowCtrls[i])->ShowWindow (bShowInnodepCtrls);
      }
   }
   // Support Omnicast
   for (i = 0; nOmniShowCtrls[i]; i++) {
      if (GetDlgItem(nOmniShowCtrls[i])) {
         GetDlgItem(nOmniShowCtrls[i])->ShowWindow (bShowOmniCtrls);
      }
   }
    // Support RealHub
   for (i = 0; nRealHubShowCtrls[i]; i++) {
      if (GetDlgItem(nRealHubShowCtrls[i])) {
         GetDlgItem(nRealHubShowCtrls[i])->ShowWindow (bShowRealHubCtrls);
      }
   }
   ((CEdit*)GetDlgItem(IDC_EDIT_IP_ADDRESS))->SetReadOnly (bReadOnlyIPAddrAndPortNo);
   ((CEdit*)GetDlgItem(IDC_EDIT_PORT_NUMBER))->SetReadOnly (bReadOnlyIPAddrAndPortNo);

   if (m_pIPCamInfo->m_nIPCameraType == IPCameraType_RTSP || m_pIPCamInfo->m_nIPCameraType == IPCameraType_Onvif) {
      ((CEdit*)GetDlgItem(IDC_EDIT_PORT_NUMBER))->SetWindowText ("");
   }

   GetDlgItem(IDC_STATIC_PTZ_PORT_NUMBER)->ShowWindow (bShowPTZPortNo);
   ((CEdit*)GetDlgItem(IDC_EDIT_PTZ_PORT_NUMBER))->ShowWindow (bShowPTZPortNo);
   GetDlgItem(IDC_STATIC_STREAM_TYPE)->ShowWindow (bShowStreamType);
   ((CEdit*)GetDlgItem(IDC_COMBO_STREAM_TYPE))->ShowWindow (bShowStreamType);
   ((CEdit*)GetDlgItem(IDC_EDIT_USER_NAME))->SetReadOnly (bReadOnlyIPAddrAndPortNo);
   ((CEdit*)GetDlgItem(IDC_EDIT_PASSWORD))->SetReadOnly (bReadOnlyIPAddrAndPortNo);
   GetDlgItem(IDC_COMBO_VIDEO_CH_NO)->EnableWindow (bEnableVideoChNo);
   GetDlgItem(IDC_BUTTON_OPEN_WEP_SETUP_PAGE)->EnableWindow (bShowSetupWebPage);
   // Support Omnicast
   GetDlgItem(IDC_COMBO_VIDEO_CH_NO)->ShowWindow(bShowVideoChNo);
   GetDlgItem(IDC_STATIC_VIDEO_CH_NO)->ShowWindow(bShowVideoChNo);

   int nShowVideoStreamSetupCtrl = SW_HIDE;
   if (m_pIPCamProperty && m_pIPCamProperty->m_nCapabilities & IPCamCaps_SupportMultiStream) {
      nShowVideoStreamSetupCtrl = SW_SHOW;
   }

   // TYLEE: 2015-01-06, GSC 멀티스트림 지원
   if (m_pIPCamInfo->m_nIPCameraType == IPCameraType_SecurityCenter)
   {
      CWnd *pWndLogicalID = GetDlgItem(IDC_EDIT_LOGICALID);
      CWnd *pWndCobmo = GetDlgItem (IDC_COMBO_VIDEO_STREAM_INDEX);
      CWnd *pWndStatic = GetDlgItem (IDC_STATIC_VIDEO_STREAM_INDEX);

      RECT rcLogicalID, rcStatic, rcCombo;

      if (pWndLogicalID && pWndLogicalID->GetSafeHwnd())
      {
         pWndLogicalID->GetWindowRect(&rcLogicalID);
         ScreenToClient(&rcLogicalID);

         if (pWndStatic && pWndStatic->GetSafeHwnd())
         {
            pWndStatic->GetWindowRect(&rcStatic);
            ScreenToClient(&rcStatic);
            pWndStatic->SetWindowPos(NULL, rcLogicalID.right + 20, rcStatic.top, 0, 0, SWP_NOZORDER|SWP_NOSIZE);

            if (pWndCobmo && pWndCobmo->GetSafeHwnd())
            {
               pWndCobmo->GetWindowRect(&rcCombo);
               ScreenToClient(&rcCombo);
               pWndCobmo->SetWindowPos(NULL, rcStatic.right + 5, rcCombo.top, 155, rcCombo.bottom - rcCombo.top, SWP_NOZORDER);
            }
         }
      }
   }

   GetDlgItem (IDC_STATIC_VIDEO_STREAM_INDEX)->ShowWindow (nShowVideoStreamSetupCtrl);
   GetDlgItem (IDC_COMBO_VIDEO_STREAM_INDEX)->ShowWindow (nShowVideoStreamSetupCtrl);
   pComboBox = (CComboBox*) GetDlgItem (IDC_COMBO_VIDEO_STREAM_INDEX);
   pComboBox->ResetContent ( );
   if (nShowVideoStreamSetupCtrl) {     
      
      if (m_pIPCamInfo->m_nIPCameraType == IPCameraType_SecurityCenter) // TYLEE: 2015-01-06, GSC 멀티스트림 지원
      {
         CString strItem = _T(""), szNo = _T("");
         for (i = 0; i < 5; i++) {
            szNo.Format(_T("%d. "), i + 1);
            strItem.LoadString(IDS_STRING1631 + i);
            pComboBox->AddString (szNo + strItem);
            pComboBox->SetItemData (i, i);
         }
      }
      else
      {
         for (i = 0; i < 4; i++) {
            CString strItem;
            strItem.Format ("%d", i+1);
            pComboBox->AddString (strItem);
            pComboBox->SetItemData (i, i);
         }
      }
      SetCurSel_ByData (pComboBox, m_pIPCamInfo->m_nVideoStreamNo);
   }

   CRect rtIP, rtPass;
   GetWndRect (rtPass, this, GetDlgItem (IDC_EDIT_PASSWORD));
   GetWndRect (rtIP, this, GetDlgItem (IDC_EDIT_IP_ADDRESS));
   UINT nFlag = SWP_HIDEWINDOW;
   if(bShowInnodepCtrls)
      nFlag = SWP_SHOWWINDOW;
   nFlag |= SWP_NOMOVE;
   
   switch (m_pIPCamInfo->m_nIPCameraType) 
   {
      case IPCameraType_Innodep:
         GetDlgItem(IDC_EDIT_DEVICE_ID)->SetWindowPos(NULL, 0, 0, rtPass.right - rtPass.left, rtPass.bottom - rtPass.top, nFlag);
      case IPCameraType_XProtect:
         GetDlgItem(IDC_EDIT_DEVICE_ID)->SetWindowPos(NULL, 0, 0, rtIP.right - rtIP.left, rtIP.bottom - rtIP.top, nFlag);
         break;
      case IPCameraType_Omnicast:
      case IPCameraType_SecurityCenter:
         GetDlgItem(IDC_EDIT_DEVICE_ID)->SetWindowPos(NULL, 0, 0, rtIP.right - rtIP.left, rtIP.bottom - rtIP.top, nFlag);
         pComboBox = (CComboBox*) GetDlgItem (IDC_COMBO_VIDEO_FORMAT);
         pComboBox->SetCurSel(-1);
         GetDlgItem (IDC_COMBO_VIDEO_FORMAT)->EnableWindow(FALSE);      
         break;
   }

   switch (m_pIPCamInfo->m_nIPCameraType) 
   {
      case IPCameraType_Innodep:
      case IPCameraType_Omnicast:
      case IPCameraType_SecurityCenter:
      case IPCameraType_XProtect:
      {
         CRect rtSize;
         GetWndRect (rtSize, this, GetDlgItem (IDC_STATIC_USER_NAME));
         GetDlgItem(IDC_STATIC_DEVICE_NAME)->SetWindowPos(NULL, rtSize.left, rtSize.top, 0, 0, SWP_NOSIZE|SWP_SHOWWINDOW);
         GetWndRect (rtSize, this, GetDlgItem (IDC_EDIT_USER_NAME));
         GetDlgItem(IDC_EDIT_DEVICE_NAME)->SetWindowPos(NULL, rtSize.left, rtSize.top, 0, 0, SWP_NOSIZE|SWP_SHOWWINDOW);
         break;
      }
      case IPCameraType_RealHub:
      {
         CRect rtSize;
         GetWndRect (rtSize, this, GetDlgItem (IDC_STATIC_VIDEO_CH_NO));
         GetDlgItem(IDC_STATIC_DEVICE_NAME)->SetWindowPos(NULL, rtSize.left, rtSize.top, 0, 0, SWP_NOSIZE|SWP_SHOWWINDOW);
         GetWndRect (rtSize, this, GetDlgItem (IDC_COMBO_VIDEO_CH_NO));
         GetDlgItem(IDC_EDIT_DEVICE_NAME)->SetWindowPos(NULL, rtSize.left, rtSize.top, 0, 0, SWP_NOSIZE|SWP_SHOWWINDOW);
         break;
      }
   }
   
   CButton* pButton;
   if (m_pIPCamProperty) {
      BOOL bSupportPTZControl = (m_pIPCamProperty->m_nCapabilities & IPCamCaps_PTZControl) ? TRUE : FALSE;
      pButton = (CButton*)GetDlgItem (IDC_CHECK_PTZ_CONTROL);
      if (IS_SUPPORT(__SUPPORT_FISHEYE_DEWARPING)) {
         if (m_pVCInfo && m_pVCInfo->m_bFisheyeCam) {
            m_pIPCamInfo->m_bUsePTZ = FALSE;
            pButton->EnableWindow(FALSE);
         }
      }
      else {
         pButton->EnableWindow (bSupportPTZControl);
      }

      if (!bSupportPTZControl) {
         pButton->SetCheck (FALSE);
      }
   }
   if (0 == m_pIPCamInfo->m_nIPCameraType) {
      EnableAllCtrls (FALSE);
   }
}

void CSetupIPCameraDlg::UpdateVideoFormat ()
{
   int i;
   int nItem;
   CComboBox* pComboBox;

   // Video Resolution
   pComboBox = (CComboBox*) GetDlgItem (IDC_COMBO_VIDEO_RESOLUTION);
   nItem = pComboBox->GetCount ();
   pComboBox->ResetContent ( );
   pComboBox->SetRedraw (FALSE);
   if (m_pIPCamProperty) {
      VideoResolutionList* resolution_list = m_pIPCamProperty->m_ResolutionListMgr.GetResolutionList (m_pIPCamInfo->m_nVideoType);
      if (resolution_list) {
         nItem = resolution_list->size ();
         for (i = 0; i < nItem; i++) {
            std::string strItem;
            GetResolutionTypeString (strItem, (*resolution_list)[i]);
            pComboBox->AddString (strItem.c_str());
         }
         pComboBox->SetRedraw (TRUE);
         SetCurSel_ByIndex (pComboBox, m_pIPCamInfo->m_nResolutionIdx);
      }
      else {
         std::string strItem;
         strItem = sutil::sformat(_T("%d x %d"), m_pIPCamInfo->m_nWidth, m_pIPCamInfo->m_nHeight);
         pComboBox->AddString (strItem.c_str());
         int nItemIndex = 0;
         pComboBox->SetRedraw (TRUE);
         SetCurSel_ByIndex (pComboBox, nItemIndex);
      }

      // 해상도 인덱스가 변경되면  영상 사이즈 정보를 업데이트 해야한다.
      // 단 파노라마 카메라 영상의 경우에는 업데이트 해선 안된다.
      if (!(m_nState & State_SetupPanorama)) {
         VideoResolutionList* pVideoResolutionList = m_pIPCamProperty->m_ResolutionListMgr.GetResolutionList (m_pIPCamInfo->m_nVideoType);
         if (pVideoResolutionList) {
            int& nResolutionIdx = m_pIPCamInfo->m_nResolutionIdx;
            m_pIPCamInfo->m_nWidth  = (*pVideoResolutionList)[nResolutionIdx].m_nWidth;
            m_pIPCamInfo->m_nHeight = (*pVideoResolutionList)[nResolutionIdx].m_nHeight;
         }
      }
   }

   // Frame Rate
   pComboBox = (CComboBox*) GetDlgItem (IDC_COMBO_FRAME_RATE);
   nItem = pComboBox->GetCount ();
   pComboBox->ResetContent ( );
   if (m_pIPCamProperty) {
      pComboBox->SetRedraw (FALSE);
      FrameRateList* pFrameRateList = m_pIPCamProperty->m_FrameRateListMgr.GetFrameRateList (m_pIPCamInfo->m_nVideoType);
      if (pFrameRateList) {
         nItem = pFrameRateList->size ();
         for (i = 0; i < nItem; i++) {
            CString strItem;
            strItem.Format (_T("%1.0f"), (*pFrameRateList)[i]);
            AddComboBoxItem (pComboBox, strItem, (*pFrameRateList)[i]);
         }
         pComboBox->SetRedraw (TRUE);
         SetCurSel_ByData (pComboBox, m_pIPCamInfo->m_fFPS_Cap);
      }
   }
}

void CSetupIPCameraDlg::SetState (int nState)
{
   m_nState = nState;
}

void CSetupIPCameraDlg::EnableAllCtrls (BOOL bEnable)
{
   int i;
   for (i = 0; g_nChildCtrlsID[i]; i++) {
      CWnd* pChildWnd = GetDlgItem (g_nChildCtrlsID[i]);
      if (pChildWnd) {
         pChildWnd->EnableWindow (bEnable);
      }
   }
   if (m_pIPCamInfo->m_bUseStream == TRUE) {
      GetDlgItem (IDC_COMBO_DEVICE_MODEL)->EnableWindow (TRUE);
   }
}


void CSetupIPCameraDlg::LoadLangEvent()
{

}

void CSetupIPCameraDlg::SetCheckPTZControl()
{
   if( IS_SUPPORT(__SUPPORT_FISHEYE_DEWARPING) ) {
      CButton *pButton = (CButton *)GetDlgItem(IDC_CHECK_PTZ_CONTROL);
      pButton->EnableWindow(!m_pVCInfo->m_bFisheyeCam);      
      if (m_pVCInfo->m_bFisheyeCam) {
         m_pIPCamInfo->m_bUsePTZ = FALSE;    
         pButton->SetCheck(FALSE);
      }
   }
}

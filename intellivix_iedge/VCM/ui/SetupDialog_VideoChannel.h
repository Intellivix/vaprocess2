#pragma once

#include "afxwin.h"

#include "GenericChildDialog.h"
#include "SetupIPCameraDlg.h"
#include "SetupIPCameraStreamDlg.h"

//////////////////////////////////////////////////////////////////////
//
// Class : CSetupDialog_VideoChannel
//
//////////////////////////////////////////////////////////////////////

class CSetupDialog_VideoChannel : public CRHGenericChildDialog
{
   DECLARE_DYNAMIC(CSetupDialog_VideoChannel)

   enum State 
   {
      State_InitCaptureBoard = 0x00000001, 
      State_ForAddCamera     = 0x00000002,
   };

protected:
   // 속성
   int m_nState;
   int m_nVCSetupMode;
   int m_nVCID;
   CVideoChannelManager* m_pVCM;

   CVideoChannelInfo*        m_pVCInfo;
   CVideoChannelInfo         m_VCInfoTmp;               // VCSetupMode_TempVC 가 OFF 일 경우에만 사용됨.

   // UI
   CBCGPComboBox m_cbVideoChannelType;

   CSetupPTZControlDlg  m_PTZDlg;
   CSetupPTZControlDlg  m_PTZDlg_IPCam;

   // 카메라 종류별 설정 다이얼로그    
   CSetupIPCameraStreamDlg m_IPCamStreamDlg;   
   std::vector<CRHGenericChildDialog*> m_SetupDlgs;

public:
   CSetupDialog_VideoChannel(CWnd* pParent = NULL);
   virtual ~CSetupDialog_VideoChannel();
   virtual int CRHGetDialogID ();
   virtual BOOL Apply ();
   virtual void Cancel ();
   virtual void SetActive  ();
   virtual void KillActive ();
   virtual void OnCancel () {}

protected:
   virtual void DoDataExchange (CDataExchange* pDX);
   virtual BOOL OnInitDialog (  );
   virtual void LoadLangEvent (  );
public:
   virtual BOOL ShowSetupWindow(int nCmdShow);

public:
   DECLARE_MESSAGE_MAP()
   afx_msg void OnDestroy( ); 
   afx_msg void OnCbnSelchangeComboVideoChannelType();
   afx_msg void OnBnClickedCheckFisheyeCamera();

public:
   int  Initialize (CVideoChannelManager* pVCM, int nVCID, int nVCSetupMode);   
   BOOL IsOK ();
   BOOL IsCancel ();
   void UpdateVCInfo ();
   void ShowFisheyeCheckbox();

public:
   void ShowVideoChannelSetupDlg ();
};


﻿#include "stdafx.h"
#include "IPCamStream.h"
#include "CompressImageBuffer.h"

/////////////////////////////////////////////////////////////////////////////
//
// Class: CIPCamStream
//
/////////////////////////////////////////////////////////////////////////////

CIPCamStream::CIPCamStream()
{
   m_nState                        = 0;
   m_bRemote                       = FALSE;
   m_pCompBuffQueue                = new CCompressImageBufferQueue;
   m_nVideoFrmPeriodForNoSignal    = 10000;
   m_nChannelNo_IVX                = -1;
   m_pcsSrcImageBuff               = NULL;
   m_Debug_LastConnectionCheckTime = GetTickCount();
   m_Debug_CheckDecodingLoop       = FALSE;
}

CIPCamStream::~CIPCamStream()
{
   delete m_pCompBuffQueue;
}

int CIPCamStream::BeginCapture()
{
   return DONE;
}

int CIPCamStream::EndCapture()
{
   m_VideoDecoder.Close();
   return DONE;
}

int CIPCamStream::SignalStopCapture()
{
   return DONE;
}

BOOL CIPCamStream::IsConnected()
{
   uint32_t dwCurrent = GetTickCount();

   //FILETIME& ftLastPush = m_pCompBuffQueue->m_ftLastPush;
   // [2013-11-01] jun : (중요) 위와 같이 m_pCompBuffQueue->m_ftLastPush 변수를 참조연산자(&)를 사용하는 경우
   //                    다른 스레드에서 이 값을 계속 업데이트 하기 때문에 잘못된 시간이 얻어질 수 있다.
   //                    수정된 코드에서는 이 변수가 임계영역으로 보호되도록 처리하였다.

   // 연결이 되고나서 지정된 시간동안 프레임이 수신되지 않으면 연결이 끊긴 것으로 간주한다.

   DWORD dwLastPush = m_pCompBuffQueue->GetLastPushTime();
   if( dwLastPush == 0 ) {
      if( dwCurrent - m_dwConnected > 14000 ) {
         loge ("[CH:%d][Stream:%d] Disconnected : Video stream was not received in specified time(15 sec(from first connection)).\n", m_nChannelNo_IVX+1, m_nStreamIdx+1);
         return FALSE;
      }
   } else {
      // 비디오 프레임을 수신받은 적이 있고 일정 시간동안 프레임을 수신을 받지 못하면 연결이 끊긴 것으로 간주한다.
      if( dwCurrent - dwLastPush > 7000 ) {
         loge( "[CH:%d][Stream:%d] Disconnected: Stream was not received for 7 sec(from last received time).\n", m_nChannelNo_IVX + 1, m_nStreamIdx + 1 );
         return FALSE;
      }
   }

   // 캡쳐(디코딩)이 한번이상 진행되있는데 마지막에 캡춰한 시간이 지정된 시간을 넘으면 연결이 끊긴것으로 간주한다.
   BOOL bDebug_CheckDecodingLoop = FALSE;
   if( m_dwLastCapture > 0 ) {
      DWORD nElapsedMilliSec = dwCurrent - m_dwLastCapture;
      if( GetTickCount() - m_Debug_LastConnectionCheckTime > 1000 ) {
         if( nElapsedMilliSec > 5000 ) {
            int nLastPushElapsedMilliSec = dwCurrent - dwLastPush;
            logd( "[CH:%d][Stream:%d] Not Decoding in(%dms),  Not Pushed in(%dms)\n", m_nChannelNo_IVX + 1, m_nStreamIdx + 1, nElapsedMilliSec, nLastPushElapsedMilliSec );
            bDebug_CheckDecodingLoop = TRUE;
         }
         m_Debug_LastConnectionCheckTime = GetTickCount();
      }
      if( nElapsedMilliSec > 8000 ) {
         loge( "[CH:%d][Stream:%d] Disconnected: Stream was not decoded for 8 sec(from last decoded time).", m_nChannelNo_IVX + 1, m_nStreamIdx + 1 );
         return FALSE;
      }
   }
   m_Debug_CheckDecodingLoop = bDebug_CheckDecodingLoop;

   return TRUE;
}

BOOL CIPCamStream::IsVideoSignalAlive()
{
   // 마지막 캡쳐한 시간이 0 이면 프레임을 수신하더라도 디코딩을 안하였으므로 비디오 신호가 없음으로 판단한다.
   if( m_dwLastCapture == 0 ) {
      //logd ("[CH:%d][Stream:%d] CIPCamStream::IsVideoSignalAlive - NO Signal (1)\n", m_nChannelNo_IVX+1, m_nStreamIdx+1);
      return FALSE;
   }

   // 마지막에 캡춰한 시간이 제한시간을 넘으면 비디오 신호가 없는 것으로 간주한다.
   uint32_t dwCurrent      = GetTickCount();
   uint32_t dwDiff         = dwCurrent - m_dwLastCapture;
   float fCaptureFrameRate = m_fFrameRate;
   if( fCaptureFrameRate < 1.0f ) fCaptureFrameRate = 1.0f;
   uint32_t nElapsedTime = dwDiff;
   uint32_t nLimitTime   = m_nVideoFrmPeriodForNoSignal + uint32_t( 2.0f / fCaptureFrameRate );
   if( nElapsedTime > nLimitTime ) {
      //logd ("[CH:%d][Stream:%d] CIPCamStream::IsVideoSignalAlive - NO Signal (2)\n", m_nChannelNo_IVX+1, m_nStreamIdx+1);
      return FALSE;
   }

   return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
//
// Class: CMapID2IPCamStream
//
/////////////////////////////////////////////////////////////////////////////

CMapID2IPCamStream::CMapID2IPCamStream()
{
}

void CMapID2IPCamStream::Push( long nClsID, CIPCamStream* pStream )
{
   CCriticalSectionSP co( m_csMap );
   m_mapClsID2Stream.insert( std::map<long, CIPCamStream*>::value_type( nClsID, pStream ) );
}

void CMapID2IPCamStream::Pop( long nClsID )
{
   CCriticalSectionSP co( m_csMap );
   m_mapClsID2Stream.erase( m_mapClsID2Stream.find( nClsID ) );
}

CIPCamStream* CMapID2IPCamStream::Find( long nClsID )
{
   CCriticalSectionSP co( m_csMap );
   std::map<long, CIPCamStream*>::iterator iter;
   iter                  = m_mapClsID2Stream.find( nClsID );
   CIPCamStream* pStream = NULL;
   if( iter == m_mapClsID2Stream.end() )
      return ( NULL );
   else {
      pStream = iter->second;
      return ( pStream );
   }
}

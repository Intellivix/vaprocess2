﻿#pragma once

#include <thread>
#include "ICamera.h"
#include "CameraProductAndItsProperties.h"
#include "CameraInfo.h"
#include "DigitalIO.h"
#include "VCM_Define.h"
#include "EventEx.h"
#include "MMTimer.h"


class ICamera;
class CIPCamStream;
class IPCameraProperty;
#ifdef __SUPPORT_PTZ_CAMERA
class CPTZCtrl;
#endif


////////////////////////////////////////////////////////////////////////////
//
// Class : CCamera_IP
//
////////////////////////////////////////////////////////////////////////////

class CCamera_IP : public ICamera, public CDigitalIO
{
public: 

   // IP 카메라에서만 사용되는 상태
   enum IPCamState 
   {
      IPCamState_Create_TimerMgr              = 0x00000002,  // IP 카메라가 타이머를 생성시킨 상태
      IPCamState_PausingCapture               = 0X00000004,
   };

public:
   int                  m_nChannelNo_IVX;
   std::string          m_szCameraName;
   float                m_fBitrate;
   float                m_fFrameRate;
   CIPCameraInfo*       m_pInfo;
   IPCameraProperty*    m_pProperty;
   
   // Callback 함수 관련 
   LPVIDEOCHANNEL_CALLBACK_DECODE_VIDEO_FRAME m_pcbDecodeFrame;
   LPVOID                                     m_pCBDecodeFrameParam;

   LPVIDEOCHANNEL_CALLBACK_RTSP_METADATA      m_pCBRTSPMetadata;

   LPVIDEOCHANNEL_CALLBACK_ENCODE_VIDEO_FRAME m_pcbReceiveEncodeFrame; // 인코드 프레임 수신 콜백 함수.

protected:
   int    m_nIPCamState;                 // IP 카메라만의 상태  (m_nState는 카메라들의 공통의 상태만 저장함)
   BOOL   m_bDecodeVideo;                // 디코딩 여부. 디코딩을 할 필요가 없는 경우가 있다. 
   BOOL   m_bDecodePrevFrame;            // 이전 프레임에서 디코딩을 하였는지의 여부.
   BOOL   m_bFrameSkippingUntilKeyFrame; 
   BOOL   m_bFindFirstKeyFrame;
   CCriticalSection m_csIPCamState;

public:
   BOOL   m_bFirstDecompressOK;          // 최초 비디오 스트림의 디코딩을 성공하였는지의 여부.
   BOOL   m_bLastVideoSignal;            // 카메라 접속을 종료할 당시의 비디오 신호 상태.      멀티스트리밍에서 잠시 스트림을 사용하지 않아 연결을 끊었을 경우에는 비디오 신호 없음 상태로 표시되는 것을 막기위한 것임. 

public:
   //--------------------------------------------------------------------------------   
   UINT m_nFPSDecodingCnt = 0;
   chrono_tp m_tpDecoding;
   //--------------------------------------------------------------------------------

   uint32_t   m_nConnectStartTime;          
   UINT64 m_nDecodeCountBefore;
   UINT64 m_nDecodeCount;                // 현재 디코드한 영상의 카운트 (정확히는 Decompress 한 횟수)
   UINT64 m_nFrameCount;                 // 현재 디코드된 프레임의 카운트
   UINT64 m_nLastGetImageIdx;            // 마지막에 획득한 영상의 인덱스
   CEventEx    m_evtGetImage;                 // 새로운 영상이 획득될 때 까지 GetImage_Lock을 대기시킴
   std::thread m_ConnectionAndDecodeThread;
               
public:
   int              m_nSrcImgType;
   BArray1D         m_SrcImg;            // 소스 이미지 (디코딩된 영상이 저장되는)
protected:
   ISize2D          m_PrevSrcImgSize;
   long             m_nSrcImgLockCnt;
   CCriticalSection m_csSrcImg;      // 소스 이미지 동시 접근 방지용
   CCriticalSection m_csCamera_IP;
                                       
   CIPCamStream* m_pStream;              // 영상(음성 포함)을 전달하는 스트림, 현재는 1개의 IP 카메라 연결당 1개의 Stream만 사용하지만 
                                         // 나중에 1개의 연결당 1개 이상의 Stream을 사용할 수 있다 (ex 디스플레이용, 녹화용)
                                       
   CMMTimer*   m_pTimer_ConnectionAndDecode; 
   CTimerManager*  m_pTimerMgr;
public:
   FILETIME    m_ftLastDecodeFrameTime;    // 가장 최근 디코딩된 시간.
   double      m_dfNormalPlayTime;
protected:
   FILETIME    m_ftOldestConnectionFailed; // 가장 오래전에 연결에 실패한 시간.

   
public:
   CCamera_IP (void);
   virtual ~CCamera_IP (void);

   // IP 카메라 전용
   virtual int   Initialize (CIPCameraInfo* pInfo);
   virtual int   Uninitialize (  );

   // 공통
   virtual int   BeginCapture (  );
   virtual int   EndCapture (  );
   virtual int   SignalStopCapture (  );
               
   virtual int   GetImage_Lock (byte **d_buffer);
   virtual int   GetImage_Unlock ();
               
   virtual int   GetCameraType (  ) { return CameraType_IPCamera; }
   virtual int   GetColorSpace (  );
               
   virtual BOOL  IsVideoSignalAlive ();
   virtual BOOL  CheckingVideoSignalAlive ();

   virtual int   SetDefaultImageProperty (int nImagePropertyType);
   virtual int   SetAllImageProperty (  );
   virtual int   SetImageProperty (int nImagePropertyType, int nValue,  int nFlagAuto = FALSE);
   virtual int   GetImageProperty (int nImagePropertyType, int &nValue, int &nFlagAuto);
   virtual int   GetImagePropertyRange (int nImagePropertyType, int& nMin, int& nMax, int& nTic);
   virtual void  GetSupportedImagePropertyList (std::vector<int>& vtImagePropertyType);
   virtual int   GetImageSize ();
   virtual void  GetCameraDesc (std::string& strDesc);

   virtual int  GetMotionStatus      (BOOL& bMotion);
   virtual int  GetAlarmInputFlags   (   );
   virtual int  GetAlarmInputStatus  (int nChIdx, BOOL& bSignal);
   virtual int  SetRelayOutputStatus (int nChIdx, BOOL  bSignal);
   virtual int  GetRelayOutputStatus (int nChIdx, BOOL& bSignal);

   virtual int  StopTalking (   );
   virtual int  StartTalking (   );
   virtual int  IsEnableAudioDataTransmit (   );

   virtual int  StartAudioInOut (); // (xinu_be22)
   virtual int  StopAudioInOut ();
   virtual int  TransferAudio (const BYTE *pBuffer,int nBuffSize);

public:
   int  ConnectionAndDecodingProc (  );
   static UINT Thread_ConnectionAndDecoding (LPVOID pVoid);

public:
   BOOL  IsConnected ();
   BOOL  IsFirstFrameJustDecoded (   );
   int   RequestIPCameraInfo ();

#ifdef __SUPPORT_PTZ_CAMERA
   CPTZCtrl*   m_pPTZControl;
public:
   void          SetPTZControl  (CPTZCtrl* pPTZControl);
   CPTZCtrl*     GetPTZControl  (   ) {return m_pPTZControl;}
   void          LoadPTZPosToPresetMap (   );
#endif
   CIPCamStream* GetIPCamStream (   );

public:
   BOOL  GetDecodeVideo (   );
   void  SetDecodeVideo (BOOL bDecodeVideo);
   int   GetIPCamState (   );
   void  ModifyIPCamState (int nRemove, int nAdd);
   void  GetVideoFrmRecvCount (int& nVideoFrmRecvCount, int& nVideoKeyFrmRecvCount);
};

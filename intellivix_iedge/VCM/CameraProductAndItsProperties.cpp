﻿#include "stdafx.h"
#include "version.h"
#include "VCM.h"
#include "CameraProductAndItsProperties.h"
#include "SupportFunction.h"

/////////////////////////////////////////////////////////////////////////////////
//
// Variables
//
/////////////////////////////////////////////////////////////////////////////////

PTZControlList   g_PTZControlList;
CaptureBoardList g_CaptureBoardList;
IPCameraList     g_IPCameraList;

CMapIDToString CaptureBoardTypeString;
CMapIDToString VideoChannelTypeString;
CMapIDToString ResolutionTypeString;
CMapIDToString VideoTypeString;
CMapIDToString VideoFormatString;
CMapIDToString PTZControlTypeString;
CMapIDToString ImagePropertyTypeString;
CMapIDToString PTZConnectionTypeString_Analog;
CMapIDToString PTZConnectionTypeString_IPCamera;
CMapIDToString ZoomModuleTypeString;

CMapIDToString _AxisResolutionTypeString;
CMapIDToString _VideoTypeString;

int EntirePTZControlTypeList[] = {
   PTZControlType_VirtualPTZ,
   PTZControlType_Pelco_D,
   PTZControlType_Samsung,
   PTZControlType_SamsungTechWin,
   PTZControlType_LG,
   PTZControlType_Honeywell,
   PTZControlType_BOSCH,
   PTZControlType_Woonwoo,
   // CYNIX
   PTZControlType_CYNIX_CU_N22DC,
   PTZControlType_CYNIX_CU_N23DH,
   PTZControlType_CYNIX_CU_N26DC,
   PTZControlType_CYNIX_CV_N22C,
   PTZControlType_CYNIX_EAI_N27T,
   PTZControlType_CYNIX_HA_18H_S,
   PTZControlType_CYNIX_HC_X18H,
   PTZControlType_CYNIX_HA_26H_S,
   PTZControlType_CYNIX_MM_302,
   PTZControlType_CYNIX_MM_202,
   // Samsung
   PTZControlType_Samsung_C6435,
   // Axis
   PTZControlType_Axis_212,
   PTZControlType_Axis_213,
   PTZControlType_Axis_214,
   PTZControlType_Axis_233D,
   PTZControlType_Axis_Q6045_Mk_II,
   // CryptoTelecom
   PTZControlType_CryptoTelecom,
   // LG
   PTZControlType_LG_LPT_EP551PS,
   PTZControlType_LG_LND_7210R,
   PTZControlType_LG_LNP_3022,
   // Honeywell
   PTZControlType_Honeywell_HSDC_251,
   PTZControlType_Honeywell_HSDC_351,
   PTZControlType_Honeywell_HISD_2201WE_IR,
   PTZControlType_Honeywell_HNP_232WI,
   // BOSCH
   PTZControlType_BOSCH_AUTODOME_500I,
   // Illisis
   PTZControlType_LinkingToOtherPTZCtrl,
   // Hitron
   PTZControlType_Hitron_HF3S36AN,
   // Yujin
   PTZControlType_Yujin_ETP5000S_33x,
   PTZControlType_Yujin_ETP5000S_22x,
   PTZControlType_Yujin_ETP5000S_Pelco_D,
   PTZControlType_Yujin_ETP7000B,
   // ICanTek
   PTZControlType_ICanTek,
   // AproMedia
   PTZControlType_AproMedia,
   // Apro Http
   PTZControlType_AproHttp,
   // NIKO
   // __SUPPORT_NICO_CAMERA
   PTZControlType_NIKO_NSD_S300,
   PTZControlType_NIKO_NSD_S360,

   // SamsungTechWin
   PTZControlType_SamsungTechWin_SPD_1000,
   PTZControlType_SamsungTechWin_SPD_2300,
   PTZControlType_SamsungTechWin_SPD_2700,
   PTZControlType_SamsungTechWin_SPD_3000,
   PTZControlType_SamsungTechWin_SPD_3300,
   PTZControlType_SamsungTechWin_SPD_3350,
   PTZControlType_SamsungTechWin_SPD_3700,
   PTZControlType_SamsungTechWin_SPD_3750,
   PTZControlType_SamsungTechWin_SCU_2370,
   PTZControlType_SamsungTechWin_SCP_2370,
   PTZControlType_SamsungTechWin_SCP_2370RH,
   PTZControlType_SamsungTechWin_SCP_2373N,
   PTZControlType_SamsungTechWin_SCP_2371N,
   // ICanTek
   PTZControlType_Truen,
   PTZControlType_Truen_TCAM_570_S22FIR,
   PTZControlType_Truen_TN_B220CS_Shinwoo_SPT_7080B,
   // PTZControlType_Truen_TN_B220CS_Convex_CVX_5000, // 삭제
   PTZControlType_Truen_TCAM_PX220CS,
   PTZControlType_Truen_TN_PX220CT,
   PTZControlType_Truen_TN_P4220CTIR,
   PTZControlType_Truen_TN_P5230CWIR,
   // Cellinx
   PTZControlType_Cellinx,
   // WonWooEng
   PTZControlType_WonWooEng_EWSJ_E360,
   PTZControlType_WonWooEng_WCA_E361NR,
   PTZControlType_WonWooEng_WTK_E361,
   PTZControlType_WonWooEng_EWSJ_363,
   PTZControlType_WonWooEng_WTK_M202,
   PTZControlType_WonWooEng_WMK_M202,
   PTZControlType_WonWooEng_WMK_MM308,
   PTZControlType_WonWooEng_WMK_HS308,
   // IDIS
   PTZControlType_IDIS,
   PTZControlType_IDIS_MNC221SH,
   // Bosch
   PTZControlType_Bosch_VIPX1,
   // Pelco
   PTZControlType_PelcoEndura, // (xinu_bc15)
   PTZControlTYpe_Pelco_D5230,
   PTZControlTYpe_Pelco_S6220, // (qch1004 150605)
   // Xvas
   PTZControlType_Xvas_XV_4120IRD,
   PTZControlType_Xvas_XV_4120IRD_13Byte, // Pelco D와 동일하면서 13 Byte 형식
   // Youtech
   PTZControlType_Youtech_7000WB_1000,
   PTZControlType_Youtech_7000WB_750,
   PTZControlType_Youtech_9000MR_750,
   PTZControlType_SamsungTechWin_iPOLiS_SNB_7004_Shinwoo_SPT_7080B,
   PTZControlType_SamsungTechWin_iPOLiS_SNB_6004_Shinwoo_SPT_7080B,
   PTZControlType_SamsungTechWin_iPOLiS_SNB_6004_Shinwoo_SPT_8080BE,
   PTZControlType_SamsungTechWin_iPOLiS_SNB_6005_Shinwoo_SPT_8080BE,
   PTZControlType_SamsungTechWin_iPOLiS_SNZ_6320_Shinwoo_SPT_8080BE,
   // Sony
   PTZControlType_SonyIPELA_SNC_RH164_124,
   PTZControlType_SonyIPELA_SNC_ER580,
   PTZControlType_SonyIPELA_SNC_RX570,
   PTZControlType_SonyIPELA_SNC_WR630,
   PTZControlType_SonyIPELA_SNC_ER585,
   // Innodep
   PTZControlType_Innodep,
   // HIK
   PTZControlType_HIKVision_NAIS_M138IR,
   PTZControlType_HIKVision_NAIS_M138H,
   PTZControlType_HIKVision_NAIS_DS_2DF1,
   PTZControlType_HIKVision_NAIS_M200IR,
   PTZControlType_HIKVision_DS_2DF7286_A,
   PTZControlType_HIKVision_DS_2DZ2116,
   PTZControlType_HIKVision_DS_2DE7186_A,
   PTZControlType_HIKVision_DS_2DF8236I_AEL,
   // Prosys
   //PTZControlType_Prosys_PPT_350 ,
   // Omnicast
   PTZControlType_Omnicast,
   // RealHub
   PTZControlType_RealHub,
   // Dahua
   PTZControlType_Dahua_NS_SD138,
   PTZControlType_Dahua_NS_SD200IR,
   PTZControlType_Dahua_NS_SD203IR,
   PTZControlType_Dahua_NS_SD220IR,
   PTZControlType_Dahua_NS_SD230IR,
   PTZControlType_Dahua_NS_SD230IRC,
   PTZControlType_Dahua_NS_SD300IR,
   PTZControlType_Dahua_KADJ_NM2030IRA,
   // Win4net
   PTZControlType_Win4net_CLEBO_PM10HT,
#if defined( __SUPPORT_ROBOMEC )
   // Robomec
   PTZControlType_Robomec_RPT_10,
   PTZControlType_Robomec_RPT_30,
#endif
   // SecurityCenter
   PTZControlType_SecurityCenter,
   // Convex
   PTZControlType_Convex_CNT_20_MidDistanceCam,
   // Hitachi
   PTZControlType_Hitachi,
   // Best Digital
   PTZControlType_BestDigital_BVS_H2020R,
   PTZControlType_BestDigital_BNC_5230HR_W,
   PTZControlType_BestDigital_XV8150IRD_2020WB,
   // SamsungTechWin iPOLiS
   PTZControlType_SamsungTechWin_iPOLiS_SNP_3371H,
   PTZControlType_SamsungTechWin_iPOLiS_SNP_5300H,
   PTZControlType_SamsungTechWin_iPOLiS_SNP_6200H,
   PTZControlType_SamsungTechWin_iPOLiS_SNP_6200RH,
   PTZControlType_SamsungTechWin_iPOLiS_SNP_6320H,
   PTZControlType_SamsungTechWin_iPOLiS_SNP_6320RH,
   PTZControlType_SamsungTechWin_iPOLiS_SNZ_5200_Convex_CNT_20,
   PTZControlType_SamsungTechWin_iPOLiS_SNZ_5200_Shinwoo_SPT_7080B,
   PTZControlType_SamsungTechWin_iPOLiS_SNZ_6320_XV_570_IRPT,
   PTZControlType_SamsungTechWin_iPOLiS_SNZ_6320_Convex_CNT_20,
   PTZControlType_SamsungTechWin_iPOLiS_SNZ_6320_Shinwoo_SPT_7080B,
   PTZControlType_HanwhaTechWin_iPOLiS_SNZ_6320_CAMP,
   PTZControlType_HanwhaTechWin_iPOLiS_SNZ_6320_CAMP2,
   PTZControlType_YoungKook_YSDIRMP10,
   PTZControlType_YoungKook_YSDIRMP20S,
   PTZControlType_YoungKook_YSD_PN20MH,
   // __SUPPORT_PANASONIC_CAMERA
   PTZControlType_Panasonic_WVSC385,
   PTZControlType_Panasonic_WVNS202A,
   PTZControlType_Panasonic_WVSC384,
   // __SUPPORT_DONGYANG_CAMERA)   //jmlee 동양전자
   PTZControlType_Dongyang_DY_IR2020HD_SONY,
   PTZControlType_Dongyang_DY_IR2020HD_WONWOO,
   PTZControlType_Dongyang_DY_IP020HD_SONY,
   PTZControlType_Dongyang_DY_IP020HD_WONWOO,
   PTZControlType_Dongyang_DY_IR3030HD_WONWOO,
   PTZControlType_Dongyang_DY_IR2030HL_SONY,
   // FSNetworks
   PTZControlType_FSNetworks_FS_IR203H,
   PTZControlType_FSNetworks_FS_IR303H,
   PTZControlType_FSNetworks_FS_IR306H,
   PTZControlType_FSNetworks_FS_IR307H,
   // XProtect
   PTZControlType_XProtect,
   PTZControlType_FlexWatch_FW1177_DEF,
   PTZControlType_UDP_CND_20ZHO,
   //Probe Digital
   PTZControlType_ProbeDigital_PRI_H2010,
   PTZControlType_ProbeDigital_PRI_H3200,
   // Hitron IP
   PTZControlType_Hitron_NFX_12053B1,
   PTZControlType_Hitron_NFX_22053H3,
   // Doowon
   PTZControlType_Doowon_DMH_5001IR,
#ifdef __SUPPORT_ONVIF
   // Support Onvif
   PTZControlType_Onvif,
#endif
   // Zenotech
   PTZControlType_Zenotech_MEGA_IPCAM,
   // Vistion Hitech
   PTZControlType_Visionhitech_BM2TI_IR,
#ifdef __SUPPORT_HUAWEI
   // HuaWei
   PTZControlType_HuaWei_IPC6621_Z30_I,
#endif
   // Vivotek
   PTZControlType_Vivotek_SD8363E,
   PTZControlType_Vivotek_SD9362EHL,
   // 장애인 협회
   PTZControlType_EW_IRSD2020HDP,
   // Apro Http
   PTZControlType_AproHttp,
   // Dallmeier
   PTZControlType_Dallmeier_DDZ4020_YY_HS_HD, // (qch1004-150602)
   0
};

int SerialPTZControlList[] = {
   -1,
   PTZControlType_Pelco_D,
   PTZControlTYpe_Pelco_D5230,
   PTZControlTYpe_Pelco_S6220, // (qch1004-150605)
   //PTZControlType_CYNIX_CU_N22DC,
   //PTZControlType_CYNIX_CU_N23DH,
   //PTZControlType_CYNIX_CU_N26DC,
   //PTZControlType_CYNIX_CV_N22C ,
   PTZControlType_CYNIX_EAI_N27T,
   PTZControlType_CYNIX_HA_18H_S,
   PTZControlType_CYNIX_HC_N20S,
   PTZControlType_CYNIX_HC_X18H,
   PTZControlType_CYNIX_MM_302,
   PTZControlType_CYNIX_MM_202,
   PTZControlType_Truen_TCAM_570_S22FIR,
   PTZControlType_Truen_TN_B220CS_Shinwoo_SPT_7080B,
   PTZControlType_Truen_TCAM_PX220CS,
   PTZControlType_Truen_TN_PX220CT,
   PTZControlType_Truen_TN_P4220CTIR,
   PTZControlType_Truen_TN_P5230CWIR,
   PTZControlType_SamsungTechWin_SPD_3700,
   PTZControlType_SamsungTechWin_SPD_3750,
   //PTZControlType_SamsungTechWin_SCU_2370,
   PTZControlType_SamsungTechWin_SCP_2370,
   PTZControlType_SamsungTechWin_SCP_2370RH,
   PTZControlType_SamsungTechWin_SCP_2373N,
   PTZControlType_SamsungTechWin_SCP_2371N,
   PTZControlType_SamsungTechWin_iPOLiS_SNZ_5200_Shinwoo_SPT_7080B,
   PTZControlType_SamsungTechWin_iPOLiS_SNZ_6320_Shinwoo_SPT_7080B,
   //PTZControlType_Samsung_C6435,
   //PTZControlType_LG_LPT_EP551PS,
   PTZControlType_LG_LND_7210R,
   //PTZControlType_Honeywell_HSDC_251,
   //PTZControlType_Honeywell_HSDC_351,
   PTZControlType_BOSCH_AUTODOME_500I,
   PTZControlType_Hitron_HF3S36AN,
   // __SUPPORT_NICO_CAMERA
   PTZControlType_NIKO_NSD_S300,
   PTZControlType_NIKO_NSD_S360,
   PTZControlType_WonWooEng_EWSJ_E360,
   PTZControlType_WonWooEng_EWSJ_363,
   PTZControlType_WonWooEng_WCA_E361NR,
   PTZControlType_WonWooEng_WTK_E361,
   PTZControlType_WonWooEng_WTK_M202,
   PTZControlType_WonWooEng_WMK_M202,
   PTZControlType_WonWooEng_WMK_MM308,
   PTZControlType_WonWooEng_WMK_HS308,
   PTZControlType_Xvas_XV_4120IRD,
   PTZControlType_Xvas_XV_4120IRD_13Byte,
   //PTZControlType_Yujin_ETP5000S_33x,
   //PTZControlType_Yujin_ETP5000S_22x,
   //PTZControlType_Yujin_ETP5000S_Pelco_D,
   //PTZControlType_Yujin_ETP7000B,
   PTZControlType_Youtech_7000WB_1000,
   PTZControlType_Youtech_7000WB_750,
   PTZControlType_Youtech_9000MR_750,
   PTZControlType_SamsungTechWin_iPOLiS_SNB_7004_Shinwoo_SPT_7080B,
   PTZControlType_SamsungTechWin_iPOLiS_SNB_6004_Shinwoo_SPT_7080B,
   PTZControlType_SamsungTechWin_iPOLiS_SNB_6004_Shinwoo_SPT_8080BE,
   PTZControlType_SamsungTechWin_iPOLiS_SNB_6005_Shinwoo_SPT_8080BE,
   PTZControlType_SamsungTechWin_iPOLiS_SNZ_6320_Shinwoo_SPT_8080BE,
//PTZControlType_Prosys_PPT_350,
#if defined( __SUPPORT_ROBOMEC )
   // Robomec
   PTZControlType_Robomec_RPT_10,
   PTZControlType_Robomec_RPT_30,
#endif
   // Convex
   PTZControlType_Convex_CNT_20_MidDistanceCam,
   PTZControlType_SamsungTechWin_iPOLiS_SNZ_5200_Convex_CNT_20,
   PTZControlType_SamsungTechWin_iPOLiS_SNZ_6320_Convex_CNT_20,
   // Hitachi
   PTZControlType_Hitachi,
   //  YoungKook
   PTZControlType_YoungKook_YSDIRMP10,
   PTZControlType_YoungKook_YSDIRMP20S,
   PTZControlType_YoungKook_YSD_PN20MH,
   // __SUPPORT_PANASONIC_CAMERA
   PTZControlType_Panasonic_WVSC385,
   PTZControlType_Panasonic_WVNS202A,
   PTZControlType_Panasonic_WVSC384,
   // __SUPPORT_DONGYANG_CAMERA)   //jmlee 동양전자
   PTZControlType_Dongyang_DY_IR2020HD_SONY,
   PTZControlType_Dongyang_DY_IR2020HD_WONWOO,
   PTZControlType_Dongyang_DY_IP020HD_SONY,
   PTZControlType_Dongyang_DY_IP020HD_WONWOO,
   PTZControlType_Dongyang_DY_IR3030HD_WONWOO,
   PTZControlType_Dongyang_DY_IR2030HL_SONY,
   PTZControlType_FSNetworks_FS_IR203H,
   PTZControlType_FSNetworks_FS_IR303H,
   PTZControlType_FSNetworks_FS_IR306H,
   PTZControlType_FSNetworks_FS_IR307H,
   0
};

/////////////////////////////////////////////////////////////////////////////////
//
// Class :  ImageProperty
//
/////////////////////////////////////////////////////////////////////////////////

ImageProperty::ImageProperty()
{
   m_nImagePropertyType = -1;
   m_bUse               = FALSE;
   m_nMin = m_nMax = m_nTic = 0;
}

void ImageProperty::SetProperty( int nMin, int nMax, int nTic, int nDefaultValue )
{
   m_bUse          = TRUE;
   m_nMin          = nMin;
   m_nMax          = nMax;
   m_nTic          = nTic;
   m_nDefaultValue = nDefaultValue;
}

void ImageProperty::GetRange( int& nMin, int& nMax, int& nTic )
{
   nMin = m_nMin;
   nMax = m_nMax;
   nTic = m_nTic;
}

/////////////////////////////////////////////////////////////////////////////////
//
// Class :  PTZControlProperty
//
/////////////////////////////////////////////////////////////////////////////////

PTZControlProperty::PTZControlProperty()
{
   m_nPTZControlType            = 0;
   m_nCapabilities              = 0;
   m_fPredictionFactor_MSTrack  = 0.7f;
   m_fThermalCameraZoomStepSize = 0.0f;
   m_nMaxDefogLevel             = 0;
}

/////////////////////////////////////////////////////////////////////////////////
//
// Class :  VideoResolutionListMgr
//
/////////////////////////////////////////////////////////////////////////////////

VideoResolutionListMgr::~VideoResolutionListMgr()
{
   m_nResolutionIdx_CIF = 0;
   DeleteAllList();
}

void VideoResolutionListMgr::DeleteAllList()
{
   int i;
   int nItem = (int)m_List.size();
   for( i = 0; i < nItem; i++ ) {
      VideoResolutionList* pVideoResolutionList = m_List[i];
      if( FALSE == pVideoResolutionList->m_bGlobal )
         delete pVideoResolutionList;
   }
   m_List.clear();
}

void VideoResolutionListMgr::AddResolutionList( VideoResolutionList* pResolutionList )
{
   m_List.push_back( pResolutionList );
}

void VideoResolutionListMgr::GetSupportedVideoTypeList( std::vector<int>& vtVideoTypes )
{
   int i;
   vtVideoTypes.clear();
   int nItem = (int)m_List.size();
   for( i = 0; i < nItem; i++ ) {
      int nVideotype = m_List[i]->m_nVideoType;
      vtVideoTypes.push_back( m_List[i]->m_nVideoType );
   }
}

int VideoResolutionListMgr::GetVideoResolutionType( int nVideoType, int nResolutionIdx )
{
   if( nVideoType >= 0 ) {
      VideoResolutionList* pResolutionList = GetResolutionList( nVideoType );
      if( pResolutionList ) {
         if( 0 <= nResolutionIdx && nResolutionIdx < (int)pResolutionList->size() ) {
            ResolutionItem& item = ( *pResolutionList )[nResolutionIdx];
            return item.m_nResolutionType;
         }
      }
   }
   return 0;
}

ISize2D VideoResolutionListMgr::GetVideoRosolution( int nVideoType, int nResolutionIdx )
{
   ISize2D videoSize( 0, 0 );
   if( nVideoType >= 0 ) {
      VideoResolutionList* pResolutionList = GetResolutionList( nVideoType );
      if( pResolutionList ) {
         if( 0 <= nResolutionIdx && nResolutionIdx < (int)pResolutionList->size() ) {
            ResolutionItem& item = ( *pResolutionList )[nResolutionIdx];
            videoSize.Width      = item.m_nWidth;
            videoSize.Height     = item.m_nHeight;
            return videoSize;
         }
      }
   }
   return videoSize;
}

ISize2D VideoResolutionListMgr::GetCIFVideoResolution( int nVideoType )
{
   return GetVideoRosolution( nVideoType, m_nResolutionIdx_CIF );
}

VideoResolutionList* VideoResolutionListMgr::GetResolutionList( int nVideoType )
{
   int i;
   int nItem = (int)m_List.size();
   if( nItem ) {
      for( i = 0; i < nItem; i++ ) {
         if( nVideoType == m_List[i]->m_nVideoType )
            return m_List[i];
      }
      return m_List[0];
   }
   return ( NULL );
}

int VideoResolutionListMgr::FindResolutionIdx( int nVideoType, int nWidth, int nHeight )
{
   int i;
   VideoResolutionList& resolution_list = *GetResolutionList( nVideoType );
   int nItem                            = (int)resolution_list.size();
   for( i = nItem - 1; i >= 0; i-- ) {
      // 고해상도부터 검색한다. (4CIF 와 2CIF EXP를 선택해야한다면 4CIF를 해야하기 때문이다)
      if( resolution_list[i].m_nWidth == nWidth && resolution_list[i].m_nHeight == nHeight ) {
         return ( i );
      }
   }
   return ( -1 );
}

int VideoResolutionListMgr::FindResolutionIdx( int nVideoType, int nResolutionType )
{
   int i;
   if( nResolutionType == ResolutionType_Size ) return ( -1 );
   VideoResolutionList& resolution_list = *GetResolutionList( nVideoType );
   int nItem                            = (int)resolution_list.size();

   // 고해상도부터 검색한다. (4CIF 와 2CIF EXP를 선택해야한다면 4CIF를 해야하기 때문이다)
   for( i = nItem - 1; i >= 0; i-- ) {
      if( resolution_list[i].m_nResolutionType == nResolutionType ) {
         return ( i );
      }
   }
   return ( -1 );
}

BOOL VideoResolutionListMgr::FindVideoTypeAndResolutionIdx( int& nVideoType, int& nResolutionIdx, int nWidth, int nHeight )
{
   int i, j;
   std::vector<int> vtVideoTypeList;
   GetSupportedVideoTypeList( vtVideoTypeList );
   int nVideoTypeNum = (int)vtVideoTypeList.size();

   for( i = 0; i < nVideoTypeNum; i++ ) {
      nVideoType                           = vtVideoTypeList[i];
      VideoResolutionList& resolution_list = *GetResolutionList( nVideoType );
      int nItem                            = (int)resolution_list.size();
      for( j = nItem - 1; j >= 0; j-- ) {
         if( resolution_list[j].m_nWidth == nWidth && resolution_list[j].m_nHeight == nHeight ) {
            nResolutionIdx = j;
            return TRUE;
         }
      }
   }
   return FALSE;
}

/////////////////////////////////////////////////////////////////////////////////
//
// Class :  FrameRateListMgr
//
/////////////////////////////////////////////////////////////////////////////////

FrameRateListMgr::~FrameRateListMgr()
{
   Clear();
}

void FrameRateListMgr::Clear()
{
   int i;
   int nItem = (int)m_List.size();
   for( i = 0; i < nItem; i++ ) {
      delete m_List[i];
   }
   m_List.clear();
}

void FrameRateListMgr::AddNewFrameRateList( int nVideoType, float* pFrameRateArr, int nItems, float fDefaultFrameRate )
{
   int i;
   m_fDefaultFrameRate           = fDefaultFrameRate;
   FrameRateList* pFrameRateList = new FrameRateList;
   pFrameRateList->m_nVideoType  = nVideoType;
   for( i = 0; i < nItems; i++ ) {
      pFrameRateList->push_back( pFrameRateArr[i] );
   }
   m_List.push_back( pFrameRateList );
}

void FrameRateListMgr::AddNewFrameRateList( int nVideoType, float fMinFPS, float fMaxFPS, float fTic, float fDefaultFrameRate )
{
   float fFPS;
   m_fDefaultFrameRate           = fDefaultFrameRate;
   FrameRateList* pFrameRateList = new FrameRateList;
   pFrameRateList->m_nVideoType  = nVideoType;
   for( fFPS = fMinFPS; fFPS < fMaxFPS; fFPS += fTic ) {
      pFrameRateList->push_back( fFPS );
   }
   if( fFPS > fMinFPS )
      pFrameRateList->push_back( fMaxFPS );
   m_List.push_back( pFrameRateList );
}

FrameRateList* FrameRateListMgr::GetFrameRateList( int nVideoType )
{
   int i;
   int nItem = (int)m_List.size();
   if( nItem ) {
      for( i = 0; i < nItem; i++ ) {
         if( nVideoType == m_List[i]->m_nVideoType )
            return m_List[i];
      }
      return m_List[0];
   }
   return ( NULL );
}

/////////////////////////////////////////////////////////////////////////////////
//
// Class :  CaptureBoardProperty
//
/////////////////////////////////////////////////////////////////////////////////

CaptureBoardProperty::CaptureBoardProperty()
{
   m_nCaptureBoardType  = 0;
   m_nChannelNum        = 0;
   m_nBitPerPixel       = 0;
   m_nColorSpace        = 0;
   m_nInputNum          = 0;
   m_nOutputNum         = 0;
   m_bStoreImgeProperty = FALSE;
   m_dfMaxBandWidth     = 0.0;
}

/////////////////////////////////////////////////////////////////////////////////
//
// Class :  IPCameraList
//
/////////////////////////////////////////////////////////////////////////////////

IPCameraProperty::IPCameraProperty()
{
   m_nInputNum      = 1;
   m_nOutputNum     = 1;
   m_nDefPortNo     = 0;
   m_nDefPTZPortNo  = 0;
   m_nDefZoomPortNo = 0; // (mkjang-140613)
}

BOOL IPCameraProperty::IsSameProductName( const std::string& strProdName )
{
   int i;

   for( i = 0; i < 4; i++ ) {
      if( m_strProdNbr[i] == strProdName )
         return TRUE;
   }
   return FALSE;
}

void IPCameraProperty::InitDigitalIO( int nInputNum, int nOutputNum )
{
   m_nInputNum  = nInputNum;
   m_nOutputNum = nOutputNum;
}

/////////////////////////////////////////////////////////////////////////////////
//
// Class :  PTZControlList
//
/////////////////////////////////////////////////////////////////////////////////

PTZControlList::~PTZControlList()
{
   int i;
   PTZControlList& This = *this;
   int nItem            = (int)This.size();
   for( i = 0; i < nItem; i++ ) {
      delete This[i];
   }
}

/////////////////////////////////////////////////////////////////////////////////
//
// Class :  CaptureBoardList
//
/////////////////////////////////////////////////////////////////////////////////

CaptureBoardList::~CaptureBoardList()
{
   int i;
   CaptureBoardList& This = *this;
   int nItem              = (int)This.size();
   for( i = 0; i < nItem; i++ ) {
      delete This[i];
   }
}

/////////////////////////////////////////////////////////////////////////////////
//
// Class :  IPCameraList
//
/////////////////////////////////////////////////////////////////////////////////

IPCameraList::~IPCameraList()
{
   int i;
   IPCameraList& This = *this;
   int nItem          = (int)This.size();
   for( i = 0; i < nItem; i++ ) {
      delete This[i];
   }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// 비디오 채널 매니져에서 사용되는 리소스들을 정의한다.
void InitVCMResources()
{
   _AxisResolutionTypeString.clear();
   _AxisResolutionTypeString.push_back( CIDToString( ResolutionType_QCIF, "QCIF" ) );
   _AxisResolutionTypeString.push_back( CIDToString( ResolutionType_CIF, "CIF" ) );
   _AxisResolutionTypeString.push_back( CIDToString( ResolutionType_2CIF, "2CIF" ) );
   _AxisResolutionTypeString.push_back( CIDToString( ResolutionType_2CIF_Expanded, "2CIFEXP" ) );
   _AxisResolutionTypeString.push_back( CIDToString( ResolutionType_4CIF, "4CIF" ) );
   _AxisResolutionTypeString.push_back( CIDToString( ResolutionType_720P, "720P" ) );
   _AxisResolutionTypeString.push_back( CIDToString( ResolutionType_1080P, "1080P" ) );

   _VideoTypeString.clear();
   _VideoTypeString.push_back( CIDToString( VideoType_None, "" ) );
   _VideoTypeString.push_back( CIDToString( VideoType_NTSC, "NTSC" ) );
   _VideoTypeString.push_back( CIDToString( VideoType_PAL, "PAL" ) );

   CaptureBoardTypeString.clear();
   CaptureBoardTypeString.push_back( CIDToString( 0, "Not Use" ) ); // "사용안 함"
   CaptureBoardTypeString.push_back( CIDToString( CaptureBoardType_OD4120, "OD-4120" ) );
   CaptureBoardTypeString.push_back( CIDToString( CaptureBoardType_OD4120SE, "OD-4120SE" ) );
   CaptureBoardTypeString.push_back( CIDToString( CaptureBoardType_OD8240, "OD-8240" ) );
   CaptureBoardTypeString.push_back( CIDToString( CaptureBoardType_OD16240, "OD-16240" ) );
   CaptureBoardTypeString.push_back( CIDToString( CaptureBoardType_OD16240SE, "OD-16240SE" ) );
   CaptureBoardTypeString.push_back( CIDToString( CaptureBoardType_OD16480, "OD-16480" ) );
   CaptureBoardTypeString.push_back( CIDToString( CaptureBoardType_OD16480SE, "OD-16480SE" ) );
   CaptureBoardTypeString.push_back( CIDToString( CaptureBoardType_OD16G, "OD-16G" ) );
   CaptureBoardTypeString.push_back( CIDToString( CaptureBoardType_OD32960, "OD-32960" ) );
   CaptureBoardTypeString.push_back( CIDToString( CaptureBoardType_OS4000A, "OS-4000A" ) );
   CaptureBoardTypeString.push_back( CIDToString( CaptureBoardType_XCAP_400EH, "OS-XCAP-400EH" ) );

   // 주의 아래의 순서를 변경해야 하는 경우 셋업 다이얼로그의 생성 순서도 변경해야 합니다!!!
   VideoChannelTypeString.clear();
   VideoChannelTypeString.push_back( CIDToString( VideoChannelType_AnalogCamera, "Analog Camera" ) ); // "일반 카메라"
   VideoChannelTypeString.push_back( CIDToString( VideoChannelType_IPCamera, "IP Camera" ) ); // "IP 카메라"
#ifdef __SUPPORT_VIRTUAL_PTZ_CAMERA
   if( IS_SUPPORT( __SUPPORT_VIRTUAL_PTZ_CAMERA ) )
      VideoChannelTypeString.push_back( CIDToString( VideoChannelType_VirtualPTZ, "Virtual PTZ Camera" ) ); // "가상 PTZ 카메라"
#endif
#ifdef __SUPPORT_PANORAMIC_CAMERA
   if( IS_SUPPORT( __SUPPORT_PANORAMIC_CAMERA ) )
      VideoChannelTypeString.push_back( CIDToString( VideoChannelType_Panorama, "Panoramic Camera" ) ); // "파노라마 카메라"
#endif

   if( IS_NOT_PRODUCT( __PRODUCT_B200 ) )
      VideoChannelTypeString.push_back( CIDToString( VideoChannelType_VideoFile, "Video File" ) ); // "비디오 파일"

   VideoChannelTypeString.push_back( CIDToString( VideoChannelType_Unused, "Not Use" ) ); // "미사용"

   ResolutionTypeString.clear();
   ResolutionTypeString.push_back( CIDToString( ResolutionType_QCIF, "QCIF" ) );
   ResolutionTypeString.push_back( CIDToString( ResolutionType_CIF, "CIF" ) );
   ResolutionTypeString.push_back( CIDToString( ResolutionType_2CIF, "2CIF" ) );
   ResolutionTypeString.push_back( CIDToString( ResolutionType_2CIF_Expanded, "2CIF Expanded" ) );
   ResolutionTypeString.push_back( CIDToString( ResolutionType_4CIF, "4CIF" ) );
   ResolutionTypeString.push_back( CIDToString( ResolutionType_720P, "720P" ) );
   ResolutionTypeString.push_back( CIDToString( ResolutionType_1080P, "1080P" ) );

   VideoTypeString.clear();
   VideoTypeString.push_back( CIDToString( VideoType_None, "None" ) ); // "없음"
   VideoTypeString.push_back( CIDToString( VideoType_NTSC, "NTSC" ) );
   VideoTypeString.push_back( CIDToString( VideoType_PAL, "PAL" ) );

   VideoFormatString.clear();
   VideoFormatString.push_back( CIDToString( VCODEC_ID_MPEG4, "MPEG4" ) );
   VideoFormatString.push_back( CIDToString( VCODEC_ID_MJPEG, "MJPEG" ) );
   VideoFormatString.push_back( CIDToString( VCODEC_ID_RAW, "RAW" ) );
   VideoFormatString.push_back( CIDToString( VCODEC_ID_H265, "H.265" ) );
   VideoFormatString.push_back( CIDToString( VCODEC_ID_H264, "H.264" ) );
   VideoFormatString.push_back( CIDToString( VCODEC_ID_H263, "H.263" ) );

   PTZControlTypeString.clear();
   PTZControlTypeString.push_back( CIDToString( -1, "Not Use" ) ); // "사용 안함   "
   //PTZControlTypeString.push_back (CIDToString (PTZControlType_CYNIX_CU_N22DC,                   "CYNIX CU-N22DC"));
   //PTZControlTypeString.push_back (CIDToString (PTZControlType_CYNIX_CU_N23DH,                   "CYNIX CU-N23DH"));
   //PTZControlTypeString.push_back (CIDToString (PTZControlType_CYNIX_CU_N26DC,                   "CYNIX CU-N26DC"));
   //PTZControlTypeString.push_back (CIDToString (PTZControlType_CYNIX_CV_N22C ,                   "CYNIX CV-N22C"));
   PTZControlTypeString.push_back( CIDToString( PTZControlType_CYNIX_EAI_N27T, "CYNIX EAI-N27T" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_CYNIX_HA_18H_S, "CYNIX HA-18H-S" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_CYNIX_HC_X18H, "CYNIX HC-X18H" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_CYNIX_HC_N20S, "CYNIX HC-N20S" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_CYNIX_MM_302, "CYNIX MM-302" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_CYNIX_MM_202, "CYNIX MM-202" ) );
#ifdef __SUPPORT_DALLMEIER
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Dallmeier_DDZ4020_YY_HS_HD, "Dallmeier DDZ4020-YY/HS/HD" ) );
#endif
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Truen_TCAM_570_S22FIR, "Truen TCAM-570-S22FIR" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Truen_TCAM_PX220CS, "Truen TCAM-PX220CS" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Truen_TN_PX220CT, "Truen TN-PX220CT" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Truen_TN_P4220CTIR, "Truen TN-P4220CTIR" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Truen_TN_P5230CWIR, "Truen TN-P5230CWIR" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Truen_TN_B220CS_Shinwoo_SPT_7080B, "Truen TN-B220CS & Shinwoo SPT-7080B" ) );

   PTZControlTypeString.push_back( CIDToString( PTZControlType_SamsungTechWin_SPD_3700, "Techwin SPD-3700" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_SamsungTechWin_SPD_3750, "Techwin SPD-3750" ) );
   //PTZControlTypeString.push_back (CIDToString (PTZControlType_SamsungTechWin_SCU_2370,          "Techwin SCU-2370"));
   PTZControlTypeString.push_back( CIDToString( PTZControlType_SamsungTechWin_SCP_2370, "Techwin SCP-2370" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_SamsungTechWin_SCP_2370RH, "Techwin SCP-2370RH" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_SamsungTechWin_SCP_2373N, "Techwin SCP-2373N" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_SamsungTechWin_SCP_2371N, "Techwin SCP-2371N" ) );
   //PTZControlTypeString.push_back (CIDToString (PTZControlType_Samsung_C6435,                    "SAMSUNG SCC-6435"));
   //PTZControlTypeString.push_back (CIDToString (PTZControlType_LG_LPT_EP551PS,                   "LG LPT-EP551PS"));
   PTZControlTypeString.push_back( CIDToString( PTZControlType_LG_LND_7210R, "LG LND7210R" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_LG_LNP_3022, "LG LNP3022" ) );

   if( IS_SUPPORT( __SUPPORT_HONEYWELL ) ) {
      //PTZControlTypeString.push_back (CIDToString (PTZControlType_Honeywell_HSDC_251,               "Honeywell HSDC-251"));
      PTZControlTypeString.push_back( CIDToString( PTZControlType_Honeywell_HSDC_351, "Honeywell HSDC-351" ) );
      PTZControlTypeString.push_back( CIDToString( PTZControlType_Honeywell_HISD_2201WE_IR, "Honeywell HISD-2201WE-IR" ) );
      PTZControlTypeString.push_back( CIDToString( PTZControlType_Honeywell_HNP_232WI, "Honeywell HNP_232WI" ) );
   }
   PTZControlTypeString.push_back( CIDToString( PTZControlType_BOSCH_AUTODOME_500I, "BOSCH AutoDome 500i" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Hitron_HF3S36AN, "Hitron HF3S36AN" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Hitron_NFX_12053B1, "Hitron NFX-12053B1" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Hitron_NFX_22053H3, "Hitron NFX-22053H3" ) );
   // __SUPPORT_NICO_CAMERA
   PTZControlTypeString.push_back( CIDToString( PTZControlType_NIKO_NSD_S300, "NIKO NSD-S300" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_NIKO_NSD_S360, "NIKO NSD-S360" ) );

   PTZControlTypeString.push_back( CIDToString( PTZControlType_WonWooEng_EWSJ_E360, "WonWoo Eng EWSJ-E360" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_WonWooEng_EWSJ_363, "WonWoo Eng EWSJ-363" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_WonWooEng_WCA_E361NR, "WonWoo Eng WCA-E361NR" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_WonWooEng_WTK_E361, "WonWoo Eng WTK-E361" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_WonWooEng_WTK_M202, "WonWoo Eng WTK-M202" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_WonWooEng_WMK_M202, "WonWoo Eng WMK-M202" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_WonWooEng_WMK_MM308, "WonWoo Eng WMK-MM308" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_WonWooEng_WMK_HS308, "WonWoo Eng WMK-HS308" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Xvas_XV_4120IRD, "Xvas XV-4120IRD" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Xvas_XV_4120IRD_13Byte, "Xvas XV-4120IRD (Pelco D 13byte)" ) );
   // __SUPPORT_DONGYANG_CAMERA)   // jmlee 동양전자
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Dongyang_DY_IR2020HD_SONY, "Dongyang DY-IR2020HD_SONY" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Dongyang_DY_IR2020HD_WONWOO, "Dongyang DY-IR2020HD_WONWOO" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Dongyang_DY_IP020HD_SONY, "Dongyang DY-IP020HD_SONY" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Dongyang_DY_IP020HD_WONWOO, "Dongyang DY-IP020HD_WONWOO" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Dongyang_DY_IR3030HD_WONWOO, "Dongyang DY-IR3030HD_WONWOO" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Dongyang_DY_IR2030HL_SONY, "Dongyang DY-IR2030HL_SONY" ) );

   PTZControlTypeString.push_back( CIDToString( PTZControlType_FSNetworks_FS_IR203H, "FS Networks IR-203H (20x)" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_FSNetworks_FS_IR303H, "FS Networks IR-303H (30x)" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_FSNetworks_FS_IR306H, "FS Networks IR-306H (20x)" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_FSNetworks_FS_IR307H, "FS Networks IR-307H (20x)" ) );

#ifdef __SUPPORT_ONVIF
   // Support Onvif
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Onvif, "Onvif PTZ Control" ) );
#endif

   // __SUPPORT_YOUTECH_CAMERA
   //PTZControlTypeString.push_back (CIDToString (PTZControlType_Yujin_ETP5000S_33x,               "YUJIN SYSTEM ETP-5000S-33x"));
   //PTZControlTypeString.push_back (CIDToString (PTZControlType_Yujin_ETP5000S_22x,               "YUJIN SYSTEM ETP-5000S-22x"));
   //PTZControlTypeString.push_back (CIDToString (PTZControlType_Yujin_ETP5000S_Pelco_D,           "YUJIN SYSTEM ETP-5000S-Pelco-D"));
   //PTZControlTypeString.push_back (CIDToString (PTZControlType_Yujin_ETP7000B,                   "YUJIN SYSTEM ETP-7000B"));
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Youtech_7000WB_1000, "Youtech 7000WB 1000mm" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Youtech_7000WB_750, "Youtech 7000WB 750mm" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Youtech_9000MR_750, "Youtech IVS-9000MR 750mm" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Bosch_VIPX1, "Bosch VIPX1" ) );
   //PTZControlTypeString.push_back (CIDToString (PTZControlType_Bosch_VIPX1,                      "Bosch VIPX1"));
   PTZControlTypeString.push_back( CIDToString( PTZControlType_PelcoEndura, "PELCO Endura NET5400T" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlTYpe_Pelco_D5230, "PELCO D5230" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlTYpe_Pelco_S6220, "PELCO D6220" ) ); // (qch1004-150605)
   PTZControlTypeString.push_back( CIDToString( PTZControlType_SonyIPELA_SNC_RH164_124, "Sony IPELA SNC RH164/124" ) );
   //PTZControlTypeString.push_back (CIDToString (PTZControlType_SonyIPELA_SNC_ER580,              "Sony IPELA SNC ER580"));
   PTZControlTypeString.push_back( CIDToString( PTZControlType_SonyIPELA_SNC_RX570, "Sony IPELA SNC RX570" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_SonyIPELA_SNC_WR630, "Sony IPELA SNC WR630" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_SonyIPELA_SNC_ER585, "Sony IPELA SNC ER585" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Innodep, "Innodep VMS SDK" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_HIKVision_NAIS_M138IR, "HIK NAIS-M138IR (18x)" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_HIKVision_NAIS_M138H, "HIK NAIS-M138H (18x)" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_HIKVision_NAIS_M200IR, "HIK NAIS-M200IR (10x)" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_HIKVision_NAIS_DS_2DF1, "HIK DS-2DF1 (20x)" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_HIKVision_DS_2DF7286_A, "HIK DS-2DF7286-A (30x)" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_HIKVision_DS_2DZ2116, "HIK DS-2DZ2116 (30x)" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_HIKVision_DS_2DE7186_A, "HIK DS-2DE7186-A (30x)" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_HIKVision_DS_2DF8236I_AEL, "HIK DS-2DF8236I-AEL (30x)" ) );
   //PTZControlTypeString.push_back (CIDToString (PTZControlType_Prosys_PPT_350,                   "Prosys PPT-350"));
   PTZControlTypeString.push_back( CIDToString( PTZControlType_SamsungTechWin_iPOLiS, "Techwin iPOLiS" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_SamsungTechWin_iPOLiS_SNP_5300H, "Techwin iPOLiS SNP-5300H" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_SamsungTechWin_iPOLiS_SNP_6200H, "Techwin iPOLiS SNP-6200H" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_SamsungTechWin_iPOLiS_SNP_6200RH, "Techwin iPOLiS SNP-6200RH" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_SamsungTechWin_iPOLiS_SNP_6320H, "Techwin iPOLiS SNP-6320H (32x)" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_SamsungTechWin_iPOLiS_SNP_6320RH, "Techwin iPOLiS SNP-6320RH (32x)" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_SamsungTechWin_iPOLiS_SNP_3371H, "Techwin iPOLiS SNP-3371H" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_HanwhaTechWin_iPOLiS_SNZ_6320_CAMP, "Techwin iPOLiS SNZ-6320 (주둔지 경계1)" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_HanwhaTechWin_iPOLiS_SNZ_6320_CAMP2, "Techwin iPOLiS SNZ-6320_L (주둔지 경계2)" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_SamsungTechWin_iPOLiS_SNZ_5200_Convex_CNT_20, "Techwin SNZ-5200 & Convex CNT-20" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_SamsungTechWin_iPOLiS_SNZ_6320_Convex_CNT_20, "Techwin SNZ-6320 & Convex CNT-20" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_SamsungTechWin_iPOLiS_SNZ_5200_Shinwoo_SPT_7080B, "Techwin SNZ-5200 & Shinwoo SPT-7080B" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_SamsungTechWin_iPOLiS_SNZ_6320_Shinwoo_SPT_7080B, "Techwin SNZ-6320 & Shinwoo SPT-7080B" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_SamsungTechWin_iPOLiS_SNB_7004_Shinwoo_SPT_7080B, "Techwin SNB-7004 & Shinwoo SPT-7080B" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_SamsungTechWin_iPOLiS_SNB_6004_Shinwoo_SPT_7080B, "Techwin SNB-6004 & Shinwoo SPT-7080B" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_SamsungTechWin_iPOLiS_SNB_6004_Shinwoo_SPT_8080BE, "Techwin SNB-6004 & Shinwoo SPT-8080BE" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_SamsungTechWin_iPOLiS_SNB_6005_Shinwoo_SPT_8080BE, "Techwin SNB-6005 & Shinwoo SPT-8080BE" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_SamsungTechWin_iPOLiS_SNZ_6320_Shinwoo_SPT_8080BE, "Techwin SNZ-6320 & Shinwoo SPT-8080BE" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_SamsungTechWin_iPOLiS_SNZ_6320_XV_570_IRPT, "Techwin SNZ-6320 & XV-570-IRPT" ) );

   // VMS
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Omnicast, "Omnicast VMS SDK" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Dahua_NS_SD138, "Dahua NS-SD138" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Dahua_NS_SD200IR, "Dahua NS-SD200IR" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Dahua_NS_SD203IR, "Dahua NS-SD203IR" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Dahua_NS_SD220IR, "Dahua NS-SD220IR" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Dahua_NS_SD230IR, "Dahua NS-SD230IR" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Dahua_NS_SD230IRC, "Dahua NS-SD230IRC" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Dahua_NS_SD300IR, "Dahua NS-SD300IR" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Dahua_KADJ_NM2030IRA, "Dahua KADJ-NM2030IRA" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Win4net_CLEBO_PM10HT, "Win4net CLEBO_PM10HT" ) );
#if defined( __SUPPORT_ROBOMEC )
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Robomec_RPT_10, "Robomec RPT-10" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Robomec_RPT_30, "Robomec RPT-30" ) );
#endif
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Convex_CNT_20_MidDistanceCam, "Convex CNT-20" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_SecurityCenter, "SecurityCenter PTZ Control" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Hitachi, "Hitachi" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_BestDigital_BVS_H2020R, "Hitron BVS-H2020R" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_BestDigital_BNC_5230HR_W, "BestDigital BNC-5230HR-W" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_BestDigital_XV8150IRD_2020WB, "BestDigital XV8150IRD(2020WB)" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_YoungKook_YSDIRMP10, "YoungKook YSDIRMP10 VidiCom" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_YoungKook_YSDIRMP20S, "YoungKook YSDIRMP20S VidiCom" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_YoungKook_YSD_PN20MH, "YoungKook YSD-PN20MH" ) );
   // __SUPPORT_PANASONIC_CAMERA
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Panasonic_WVSC384, "Panasonic WV-SC384" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Panasonic_WVSC385, "Panasonic WV-SC385" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Panasonic_WVNS202A, "Panasonic WV-NS202A" ) );

   PTZControlTypeString.push_back( CIDToString( PTZControlType_ProbeDigital_PRI_H2010, "Probe Digital PRI-H2010" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_ProbeDigital_PRI_H3200, "Probe Digital PRI-H3200" ) );

   PTZControlTypeString.push_back( CIDToString( PTZControlType_Axis_212, "Axis 212" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Axis_213, "Axis 213" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Axis_214, "Axis 214" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Axis_233D, "Axis 233D" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Axis_Q6045_Mk_II, "Axis Q6045 Mk II" ) );

   // Support XProtect
   PTZControlTypeString.push_back( CIDToString( PTZControlType_XProtect, "XProtect VMS" ) );

   PTZControlTypeString.push_back( CIDToString( PTZControlType_FlexWatch_FW1177_DEF, "Convex CND-1370" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_UDP_CND_20ZHO, "Convex CND-5200" ) );

   // Zenotech
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Zenotech_MEGA_IPCAM, "Zenotech MEGA-IPCAM" ) );

   // Vistion Hitech
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Visionhitech_BM2TI_IR, "Vision Hitech BM2Ti-IR" ) );
#ifdef __SUPPORT_HUAWEI
   // HuaWei
   if( IS_SUPPORT( __SUPPORT_HUAWEI ) )
      PTZControlTypeString.push_back( CIDToString( PTZControlType_HuaWei_IPC6621_Z30_I, "HuaWei IPC6621-Z30-I" ) );
#endif

   // Vivotek
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Vivotek_SD8363E, "Vivotek SD8363E" ) );
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Vivotek_SD9362EHL, "Vivotek SD9362EHL" ) );

   // Apro
   PTZControlTypeString.push_back( CIDToString( PTZControlType_AproHttp, "Apro Http" ) );

   // 장애인협회
   PTZControlTypeString.push_back( CIDToString( PTZControlType_EW_IRSD2020HDP, "EW-IRSD2020HDP" ) );

   if( IS_SUPPORT( __SUPPORT_IDIS_CAMERA ) ) {
      // IDIS
      PTZControlTypeString.push_back( CIDToString( PTZControlType_IDIS_MNC221SH, "IDIS MNC221SH(18x)" ) );
   }
   PTZControlTypeString.push_back( CIDToString( PTZControlType_Doowon_DMH_5001IR, "Doowon DMH_5001IR(18x)" ) );
   // 줌 모듈 타입
   ZoomModuleTypeString.clear();
   ZoomModuleTypeString.push_back( CIDToString( ZoomModuleType_HighMagnification, "High Magnification" ) );
   ZoomModuleTypeString.push_back( CIDToString( ZoomModuleType_ThermalImaging, "Thermal Imaging" ) );
   ZoomModuleTypeString.push_back( CIDToString( ZoomModuleType_WideAngle, "Wide Angle" ) );
   // 이미지 속성
   ImagePropertyTypeString.clear();
   ImagePropertyTypeString.push_back( CIDToString( ImagePropertyType_Brightness, "Brightness" ) );
   ImagePropertyTypeString.push_back( CIDToString( ImagePropertyType_Contrast, "Contrast" ) );
   ImagePropertyTypeString.push_back( CIDToString( ImagePropertyType_Saturation, "Saturation" ) );
   ImagePropertyTypeString.push_back( CIDToString( ImagePropertyType_SaturationU, "Saturation U" ) );
   ImagePropertyTypeString.push_back( CIDToString( ImagePropertyType_SaturationV, "Saturation V" ) );
   ImagePropertyTypeString.push_back( CIDToString( ImagePropertyType_Hue, "Hue" ) );
   ImagePropertyTypeString.push_back( CIDToString( ImagePropertyType_Sharpness, "Sharpness" ) );
   ImagePropertyTypeString.push_back( CIDToString( ImagePropertyType_Exposure, "Exposure" ) );
   ImagePropertyTypeString.push_back( CIDToString( ImagePropertyType_BLC, "BLC" ) );
   ImagePropertyTypeString.push_back( CIDToString( ImagePropertyType_AWB, "AWB" ) );
   ImagePropertyTypeString.push_back( CIDToString( ImagePropertyType_DayAndNight, "DN" ) );
   ImagePropertyTypeString.push_back( CIDToString( ImagePropertyType_WDR, "WDR" ) );
   ImagePropertyTypeString.push_back( CIDToString( ImagePropertyType_DNR, "DNR" ) );
   // PTZ 연결 방식
   PTZConnectionTypeString_Analog.clear();
   PTZConnectionTypeString_Analog.push_back( CIDToString( PTZCtrlConnType_Basic, "Serial Port" ) ); // 시리얼 포트를 통한 제어
   PTZConnectionTypeString_Analog.push_back( CIDToString( PTZCtrlConnType_TCPServer, "Serial To TCP Port" ) ); // Serial To TCP 서버 연결

   PTZConnectionTypeString_IPCamera.clear();
   PTZConnectionTypeString_IPCamera.push_back( CIDToString( PTZCtrlConnType_Basic, "Video Server API" ) ); // 비디오서버를 통한 제어
   if( IS_NOT_PRODUCT( __PRODUCT_B200 ) ) {
      PTZConnectionTypeString_IPCamera.push_back( CIDToString( PTZCtrlConnType_TCPServer, "Serial To TCP" ) ); // Serial To TCP 서버 연결
      PTZConnectionTypeString_IPCamera.push_back( CIDToString( PTZCtrlConnType_LinkToPTZCtrl, "Link To Other PTZ" ) ); // 외부 PTZ 제어채널에 연결
      PTZConnectionTypeString_IPCamera.push_back( CIDToString( PTZCtrlConnType_OtherIPCamera, "Independent Connection" ) ); // 별도 비디오서버를 통한 제어
   }
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//               CAPTURE BOARD PRODUCT DEFINES
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void InitCaptureBoardProducts()
{
   CaptureBoardProperty* pCB;
   VideoResolutionList* pResolutionList;

   //-----------------------------------------------------------------------------
   // OYTO 0D4120
   pCB                      = new CaptureBoardProperty;
   pCB->m_nCaptureBoardType = CaptureBoardType_OD4120;
   pCB->m_nChannelNum       = 4;
   pCB->m_nBitPerPixel      = 16;
   pCB->m_nColorSpace       = ColorSpace_YUY2;
   pCB->m_dfMaxBandWidth    = 704 * 480 * 120 * double( pCB->m_nBitPerPixel ) / 8.0;
   pCB->m_nInputNum         = 4;
   pCB->m_nOutputNum        = 4;

   pCB->m_ImageProperty[ImagePropertyType_Brightness].SetProperty( 1, 100, 1, 50 );
   pCB->m_ImageProperty[ImagePropertyType_Contrast].SetProperty( 1, 100, 1, 50 );
   pCB->m_ImageProperty[ImagePropertyType_Saturation].SetProperty( 1, 100, 1, 50 );
   pCB->m_ImageProperty[ImagePropertyType_Hue].SetProperty( 1, 100, 1, 50 );

   pResolutionList               = new VideoResolutionList;
   pResolutionList->m_nVideoType = VideoType_NTSC;
   pResolutionList->push_back( ResolutionItem( 352, 240, ResolutionType_CIF ) );
   pResolutionList->push_back( ResolutionItem( 704, 480, ResolutionType_4CIF ) );
   pCB->m_ResolutionListMgr.AddResolutionList( pResolutionList );

   pResolutionList               = new VideoResolutionList;
   pResolutionList->m_nVideoType = VideoType_PAL;
   pResolutionList->push_back( ResolutionItem( 352, 288, ResolutionType_CIF ) );
   pResolutionList->push_back( ResolutionItem( 704, 576, ResolutionType_4CIF ) );
   pCB->m_ResolutionListMgr.AddResolutionList( pResolutionList );

   pCB->m_ResolutionListMgr.m_nResolutionIdx_CIF = 0;
   g_CaptureBoardList.push_back( pCB );

   //-----------------------------------------------------------------------------
   // OYTO 0D4120SE
   pCB                      = new CaptureBoardProperty;
   pCB->m_nCaptureBoardType = CaptureBoardType_OD4120SE;
   pCB->m_nChannelNum       = 4;
   pCB->m_nBitPerPixel      = 16;
   pCB->m_nColorSpace       = ColorSpace_YV12;
   pCB->m_dfMaxBandWidth    = 704 * 480 * 120 * double( pCB->m_nBitPerPixel ) / 8.0;
   pCB->m_nInputNum         = 4;
   pCB->m_nOutputNum        = 4;

   pCB->m_ImageProperty[ImagePropertyType_Brightness].SetProperty( 1, 100, 1, 50 );
   pCB->m_ImageProperty[ImagePropertyType_Contrast].SetProperty( 1, 100, 1, 50 );
   pCB->m_ImageProperty[ImagePropertyType_Saturation].SetProperty( 1, 100, 1, 50 );
   pCB->m_ImageProperty[ImagePropertyType_Hue].SetProperty( 1, 100, 1, 50 );

   pResolutionList               = new VideoResolutionList;
   pResolutionList->m_nVideoType = VideoType_NTSC;
   pResolutionList->push_back( ResolutionItem( 352, 240, ResolutionType_CIF ) );
   pResolutionList->push_back( ResolutionItem( 704, 480, ResolutionType_4CIF ) );
   pCB->m_ResolutionListMgr.AddResolutionList( pResolutionList );

   pResolutionList               = new VideoResolutionList;
   pResolutionList->m_nVideoType = VideoType_PAL;
   pResolutionList->push_back( ResolutionItem( 352, 288, ResolutionType_CIF ) );
   pResolutionList->push_back( ResolutionItem( 704, 576, ResolutionType_4CIF ) );
   pCB->m_ResolutionListMgr.AddResolutionList( pResolutionList );

   pCB->m_ResolutionListMgr.m_nResolutionIdx_CIF = 0;
   g_CaptureBoardList.push_back( pCB );

   //-----------------------------------------------------------------------------
   // OYTO 0D8240
   pCB                      = new CaptureBoardProperty;
   pCB->m_nCaptureBoardType = CaptureBoardType_OD8240;
   pCB->m_nChannelNum       = 8;
   pCB->m_nBitPerPixel      = 12;
   pCB->m_dfMaxBandWidth    = 704 * 480 * 120 * double( pCB->m_nBitPerPixel ) / 8.0;
   pCB->m_nColorSpace       = ColorSpace_YV12;
   pCB->m_nInputNum         = 8;
   pCB->m_nOutputNum        = 8;

   pCB->m_ImageProperty[ImagePropertyType_Brightness].SetProperty( 1, 100, 1, 50 );
   pCB->m_ImageProperty[ImagePropertyType_Contrast].SetProperty( 1, 100, 1, 50 );
   pCB->m_ImageProperty[ImagePropertyType_Saturation].SetProperty( 1, 100, 1, 50 );
   pCB->m_ImageProperty[ImagePropertyType_Hue].SetProperty( 1, 100, 1, 50 );

   pResolutionList               = new VideoResolutionList;
   pResolutionList->m_nVideoType = VideoType_NTSC;
   pResolutionList->push_back( ResolutionItem( 352, 240, ResolutionType_CIF ) );
   pResolutionList->push_back( ResolutionItem( 704, 480, ResolutionType_4CIF ) );
   pCB->m_ResolutionListMgr.AddResolutionList( pResolutionList );

   pResolutionList               = new VideoResolutionList;
   pResolutionList->m_nVideoType = VideoType_PAL;
   pResolutionList->push_back( ResolutionItem( 352, 288, ResolutionType_CIF ) );
   pResolutionList->push_back( ResolutionItem( 704, 576, ResolutionType_4CIF ) );
   pCB->m_ResolutionListMgr.AddResolutionList( pResolutionList );

   pCB->m_ResolutionListMgr.m_nResolutionIdx_CIF = 0;
   g_CaptureBoardList.push_back( pCB );

   //-----------------------------------------------------------------------------
   // OYTO 0D16240
   pCB                      = new CaptureBoardProperty;
   pCB->m_nCaptureBoardType = CaptureBoardType_OD16240;
   pCB->m_nChannelNum       = 16;
   pCB->m_nBitPerPixel      = 12;
   pCB->m_dfMaxBandWidth    = 352 * 240 * 240 * double( pCB->m_nBitPerPixel ) / 8.0;
   pCB->m_nColorSpace       = ColorSpace_YV12;
   pCB->m_nInputNum         = 16;
   pCB->m_nOutputNum        = 16;

   pCB->m_ImageProperty[ImagePropertyType_Brightness].SetProperty( 1, 100, 1, 50 );
   pCB->m_ImageProperty[ImagePropertyType_Contrast].SetProperty( 1, 100, 1, 50 );
   pCB->m_ImageProperty[ImagePropertyType_Saturation].SetProperty( 1, 100, 1, 50 );
   pCB->m_ImageProperty[ImagePropertyType_Hue].SetProperty( 1, 100, 1, 50 );

   pResolutionList               = new VideoResolutionList;
   pResolutionList->m_nVideoType = VideoType_NTSC;
   pResolutionList->push_back( ResolutionItem( 352, 240, ResolutionType_CIF ) );
   pResolutionList->push_back( ResolutionItem( 704, 480, ResolutionType_4CIF ) );
   pCB->m_ResolutionListMgr.AddResolutionList( pResolutionList );

   pResolutionList               = new VideoResolutionList;
   pResolutionList->m_nVideoType = VideoType_PAL;
   pResolutionList->push_back( ResolutionItem( 352, 288, ResolutionType_CIF ) );
   pResolutionList->push_back( ResolutionItem( 704, 576, ResolutionType_4CIF ) );
   pCB->m_ResolutionListMgr.AddResolutionList( pResolutionList );

   pCB->m_ResolutionListMgr.m_nResolutionIdx_CIF = 0;
   g_CaptureBoardList.push_back( pCB );
   //-----------------------------------------------------------------------------
   // OYTO 0D16240SE
   pCB                      = new CaptureBoardProperty;
   pCB->m_nCaptureBoardType = CaptureBoardType_OD16240SE;
   pCB->m_nChannelNum       = 16;
   pCB->m_nBitPerPixel      = 12;
   pCB->m_dfMaxBandWidth    = 352 * 240 * 240 * double( pCB->m_nBitPerPixel ) / 8.0;
   pCB->m_nColorSpace       = ColorSpace_YV12;
   pCB->m_nInputNum         = 16;
   pCB->m_nOutputNum        = 16;

   pCB->m_ImageProperty[ImagePropertyType_Brightness].SetProperty( 1, 100, 1, 50 );
   pCB->m_ImageProperty[ImagePropertyType_Contrast].SetProperty( 1, 100, 1, 50 );
   pCB->m_ImageProperty[ImagePropertyType_Saturation].SetProperty( 1, 100, 1, 50 );
   pCB->m_ImageProperty[ImagePropertyType_Hue].SetProperty( 1, 100, 1, 50 );

   pResolutionList               = new VideoResolutionList;
   pResolutionList->m_nVideoType = VideoType_NTSC;
   pResolutionList->push_back( ResolutionItem( 352, 240, ResolutionType_CIF ) );
   pResolutionList->push_back( ResolutionItem( 704, 480, ResolutionType_4CIF ) );
   pCB->m_ResolutionListMgr.AddResolutionList( pResolutionList );

   pResolutionList               = new VideoResolutionList;
   pResolutionList->m_nVideoType = VideoType_PAL;
   pResolutionList->push_back( ResolutionItem( 352, 288, ResolutionType_CIF ) );
   pResolutionList->push_back( ResolutionItem( 704, 576, ResolutionType_4CIF ) );
   pCB->m_ResolutionListMgr.AddResolutionList( pResolutionList );

   pCB->m_ResolutionListMgr.m_nResolutionIdx_CIF = 0;
   g_CaptureBoardList.push_back( pCB );
   //-----------------------------------------------------------------------------
   // OYTO 0D16480
   pCB                      = new CaptureBoardProperty;
   pCB->m_nCaptureBoardType = CaptureBoardType_OD16480;
   pCB->m_nChannelNum       = 16;
   pCB->m_nBitPerPixel      = 12;
   pCB->m_dfMaxBandWidth    = 352 * 240 * 480 * double( pCB->m_nBitPerPixel ) / 8.0;
   pCB->m_nColorSpace       = ColorSpace_YV12;
   pCB->m_nInputNum         = 16;
   pCB->m_nOutputNum        = 16;

   pCB->m_ImageProperty[ImagePropertyType_Brightness].SetProperty( 1, 100, 1, 50 );
   pCB->m_ImageProperty[ImagePropertyType_Contrast].SetProperty( 1, 100, 1, 50 );
   pCB->m_ImageProperty[ImagePropertyType_Saturation].SetProperty( 1, 100, 1, 50 );
   pCB->m_ImageProperty[ImagePropertyType_Hue].SetProperty( 1, 100, 1, 50 );

   pResolutionList               = new VideoResolutionList;
   pResolutionList->m_nVideoType = VideoType_NTSC;
   pResolutionList->push_back( ResolutionItem( 352, 240, ResolutionType_CIF ) );
   pResolutionList->push_back( ResolutionItem( 704, 480, ResolutionType_4CIF ) );
   pCB->m_ResolutionListMgr.AddResolutionList( pResolutionList );

   pResolutionList               = new VideoResolutionList;
   pResolutionList->m_nVideoType = VideoType_PAL;
   pResolutionList->push_back( ResolutionItem( 352, 288, ResolutionType_CIF ) );
   pResolutionList->push_back( ResolutionItem( 704, 576, ResolutionType_4CIF ) );
   pCB->m_ResolutionListMgr.AddResolutionList( pResolutionList );

   pCB->m_ResolutionListMgr.m_nResolutionIdx_CIF = 0;
   g_CaptureBoardList.push_back( pCB );
   //-----------------------------------------------------------------------------
   // OYTO 0D16480SE
   pCB                      = new CaptureBoardProperty;
   pCB->m_nCaptureBoardType = CaptureBoardType_OD16480SE;
   pCB->m_nChannelNum       = 16;
   pCB->m_nBitPerPixel      = 12;
   pCB->m_dfMaxBandWidth    = 352 * 240 * 480 * double( pCB->m_nBitPerPixel ) / 8.0;
   pCB->m_nColorSpace       = ColorSpace_YV12;
   pCB->m_nInputNum         = 16;
   pCB->m_nOutputNum        = 16;

   pCB->m_ImageProperty[ImagePropertyType_Brightness].SetProperty( 1, 100, 1, 50 );
   pCB->m_ImageProperty[ImagePropertyType_Contrast].SetProperty( 1, 100, 1, 50 );
   pCB->m_ImageProperty[ImagePropertyType_Saturation].SetProperty( 1, 100, 1, 50 );
   pCB->m_ImageProperty[ImagePropertyType_Hue].SetProperty( 1, 100, 1, 50 );

   pResolutionList               = new VideoResolutionList;
   pResolutionList->m_nVideoType = VideoType_NTSC;
   pResolutionList->push_back( ResolutionItem( 352, 240, ResolutionType_CIF ) );
   pResolutionList->push_back( ResolutionItem( 704, 480, ResolutionType_4CIF ) );
   pCB->m_ResolutionListMgr.AddResolutionList( pResolutionList );

   pResolutionList               = new VideoResolutionList;
   pResolutionList->m_nVideoType = VideoType_PAL;
   pResolutionList->push_back( ResolutionItem( 352, 288, ResolutionType_CIF ) );
   pResolutionList->push_back( ResolutionItem( 704, 576, ResolutionType_4CIF ) );
   pCB->m_ResolutionListMgr.AddResolutionList( pResolutionList );

   pCB->m_ResolutionListMgr.m_nResolutionIdx_CIF = 0;
   g_CaptureBoardList.push_back( pCB );

   //-----------------------------------------------------------------------------
   // OYTO 0D16G
   pCB                      = new CaptureBoardProperty;
   pCB->m_nCaptureBoardType = CaptureBoardType_OD16G;
   pCB->m_nChannelNum       = 16;
   pCB->m_nBitPerPixel      = 12;
   pCB->m_dfMaxBandWidth    = 704 * 480 * 480 * double( pCB->m_nBitPerPixel ) / 8.0;
   pCB->m_nColorSpace       = ColorSpace_YV12;
   pCB->m_nInputNum         = 16;
   pCB->m_nOutputNum        = 16;

   pCB->m_ImageProperty[ImagePropertyType_Brightness].SetProperty( 1, 100, 1, 50 );
   pCB->m_ImageProperty[ImagePropertyType_Contrast].SetProperty( 1, 100, 1, 50 );
   pCB->m_ImageProperty[ImagePropertyType_Saturation].SetProperty( 1, 100, 1, 50 );
   pCB->m_ImageProperty[ImagePropertyType_Hue].SetProperty( 1, 100, 1, 50 );

   pResolutionList               = new VideoResolutionList;
   pResolutionList->m_nVideoType = VideoType_NTSC;
   pResolutionList->push_back( ResolutionItem( 352, 240, ResolutionType_CIF ) );
   pResolutionList->push_back( ResolutionItem( 704, 480, ResolutionType_4CIF ) );
   pCB->m_ResolutionListMgr.AddResolutionList( pResolutionList );

   pResolutionList               = new VideoResolutionList;
   pResolutionList->m_nVideoType = VideoType_PAL;
   pResolutionList->push_back( ResolutionItem( 352, 288, ResolutionType_CIF ) );
   pResolutionList->push_back( ResolutionItem( 704, 576, ResolutionType_4CIF ) );
   pCB->m_ResolutionListMgr.AddResolutionList( pResolutionList );

   pCB->m_ResolutionListMgr.m_nResolutionIdx_CIF = 0;
   g_CaptureBoardList.push_back( pCB );

   //-----------------------------------------------------------------------------
   // OYTO 0D32960
   pCB                      = new CaptureBoardProperty;
   pCB->m_nCaptureBoardType = CaptureBoardType_OD32960;
   pCB->m_nChannelNum       = 16;
   pCB->m_nBitPerPixel      = 12;
   pCB->m_dfMaxBandWidth    = 352 * 240 * 960 * double( pCB->m_nBitPerPixel ) / 8.0;
   pCB->m_nColorSpace       = ColorSpace_YV12;
   pCB->m_nInputNum         = 16;
   pCB->m_nOutputNum        = 16;

   pCB->m_ImageProperty[ImagePropertyType_Brightness].SetProperty( 1, 100, 1, 50 );
   pCB->m_ImageProperty[ImagePropertyType_Contrast].SetProperty( 1, 100, 1, 50 );
   pCB->m_ImageProperty[ImagePropertyType_Saturation].SetProperty( 1, 100, 1, 50 );
   pCB->m_ImageProperty[ImagePropertyType_Hue].SetProperty( 1, 100, 1, 50 );

   pResolutionList               = new VideoResolutionList;
   pResolutionList->m_nVideoType = VideoType_NTSC;
   pResolutionList->push_back( ResolutionItem( 352, 240, ResolutionType_CIF ) );
   pResolutionList->push_back( ResolutionItem( 704, 480, ResolutionType_4CIF ) );
   pCB->m_ResolutionListMgr.AddResolutionList( pResolutionList );

   pResolutionList               = new VideoResolutionList;
   pResolutionList->m_nVideoType = VideoType_PAL;
   pResolutionList->push_back( ResolutionItem( 352, 288, ResolutionType_CIF ) );
   pResolutionList->push_back( ResolutionItem( 704, 576, ResolutionType_4CIF ) );
   pCB->m_ResolutionListMgr.AddResolutionList( pResolutionList );

   pCB->m_ResolutionListMgr.m_nResolutionIdx_CIF = 0;
   g_CaptureBoardList.push_back( pCB );

   //-----------------------------------------------------------------------------
   //OS 4000A
   pCB                      = new CaptureBoardProperty;
   pCB->m_nCaptureBoardType = CaptureBoardType_OS4000A;
   pCB->m_nChannelNum       = 4;
   pCB->m_nBitPerPixel      = 16;
   pCB->m_dfMaxBandWidth    = 1280 * 720 * 480 * double( pCB->m_nBitPerPixel ) / 8.0; // 60Frame...120frame total
   pCB->m_nColorSpace       = ColorSpace_UYVY;
   pCB->m_nInputNum         = 4;
   pCB->m_nOutputNum        = 4;

   pCB->m_ImageProperty[ImagePropertyType_Brightness].SetProperty( 1, 255, 1, 127 );
   pCB->m_ImageProperty[ImagePropertyType_Contrast].SetProperty( 1, 255, 1, 127 );
   pCB->m_ImageProperty[ImagePropertyType_SaturationU].SetProperty( 1, 255, 1, 127 );
   pCB->m_ImageProperty[ImagePropertyType_SaturationV].SetProperty( 1, 255, 1, 127 );

   pResolutionList               = new VideoResolutionList;
   pResolutionList->m_nVideoType = VideoType_None;
   pResolutionList->push_back( ResolutionItem( 1280, 720, ResolutionType_720P ) );
   pResolutionList->push_back( ResolutionItem( 1920, 1080, ResolutionType_1080P ) );
   pCB->m_ResolutionListMgr.AddResolutionList( pResolutionList );

   pCB->m_ResolutionListMgr.m_nResolutionIdx_CIF = 0;
   g_CaptureBoardList.push_back( pCB );

   //-----------------------------------------------------------------------------
   // XCAP-400EH
   pCB                      = new CaptureBoardProperty;
   pCB->m_nCaptureBoardType = CaptureBoardType_XCAP_400EH;
   pCB->m_nChannelNum       = 16;
   pCB->m_nBitPerPixel      = 16;
   pCB->m_dfMaxBandWidth    = 704 * 480 * 30 * 16 * double( pCB->m_nBitPerPixel ) / 8.0;
   pCB->m_nColorSpace       = ColorSpace_YUY2;
   pCB->m_nInputNum         = 4;
   pCB->m_nOutputNum        = 4;

   //pCB->m_ImageProperty[ImagePropertyType_Brightness  ].SetProperty (1, 255, 1, 127);
   //pCB->m_ImageProperty[ImagePropertyType_Contrast    ].SetProperty (1, 255, 1, 127);
   //pCB->m_ImageProperty[ImagePropertyType_SaturationU ].SetProperty (1, 255, 1, 127);
   //pCB->m_ImageProperty[ImagePropertyType_SaturationV ].SetProperty (1, 255, 1, 127);

   pResolutionList               = new VideoResolutionList;
   pResolutionList->m_nVideoType = VideoType_NTSC;
   pResolutionList->push_back( ResolutionItem( 352, 240, ResolutionType_CIF ) );
   pResolutionList->push_back( ResolutionItem( 704, 480, ResolutionType_4CIF ) );
   pCB->m_ResolutionListMgr.AddResolutionList( pResolutionList );

   pResolutionList               = new VideoResolutionList;
   pResolutionList->m_nVideoType = VideoType_PAL;
   pResolutionList->push_back( ResolutionItem( 352, 288, ResolutionType_CIF ) );
   pResolutionList->push_back( ResolutionItem( 704, 576, ResolutionType_4CIF ) );
   pCB->m_ResolutionListMgr.AddResolutionList( pResolutionList );

   pCB->m_ResolutionListMgr.m_nResolutionIdx_CIF = 0;
   g_CaptureBoardList.push_back( pCB );
}

VideoResolutionList g_GlobalResolutionList;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//               IP CAMERA PRODUCT DEFINES
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void InitIPCameraProducts()
{
   g_GlobalResolutionList.m_nVideoType = VideoType_None;
   g_GlobalResolutionList.push_back( ResolutionItem( 120, 80, ResolutionType_Size ) );
   g_GlobalResolutionList.push_back( ResolutionItem( 160, 90, ResolutionType_Size ) );
   g_GlobalResolutionList.push_back( ResolutionItem( 160, 120, ResolutionType_Size ) );
   g_GlobalResolutionList.push_back( ResolutionItem( 176, 144, ResolutionType_Size ) );
   g_GlobalResolutionList.push_back( ResolutionItem( 176, 120, ResolutionType_Size ) );
   g_GlobalResolutionList.push_back( ResolutionItem( 240, 136, ResolutionType_Size ) );
   g_GlobalResolutionList.push_back( ResolutionItem( 240, 180, ResolutionType_Size ) );
   g_GlobalResolutionList.push_back( ResolutionItem( 320, 180, ResolutionType_Size ) );
   g_GlobalResolutionList.push_back( ResolutionItem( 320, 192, ResolutionType_Size ) ); // temp
   g_GlobalResolutionList.push_back( ResolutionItem( 320, 240, ResolutionType_Size ) );
   g_GlobalResolutionList.push_back( ResolutionItem( 352, 240, ResolutionType_Size ) );
   g_GlobalResolutionList.push_back( ResolutionItem( 360, 240, ResolutionType_Size ) );
   g_GlobalResolutionList.push_back( ResolutionItem( 352, 288, ResolutionType_Size ) );
   g_GlobalResolutionList.push_back( ResolutionItem( 480, 270, ResolutionType_Size ) );
   g_GlobalResolutionList.push_back( ResolutionItem( 480, 360, ResolutionType_Size ) );
   g_GlobalResolutionList.push_back( ResolutionItem( 640, 360, ResolutionType_Size ) );
   g_GlobalResolutionList.push_back( ResolutionItem( 640, 480, ResolutionType_Size ) );
   g_GlobalResolutionList.push_back( ResolutionItem( 704, 480, ResolutionType_Size ) );
   g_GlobalResolutionList.push_back( ResolutionItem( 704, 576, ResolutionType_Size ) );
   g_GlobalResolutionList.push_back( ResolutionItem( 720, 480, ResolutionType_Size ) );
   g_GlobalResolutionList.push_back( ResolutionItem( 720, 576, ResolutionType_Size ) );
   g_GlobalResolutionList.push_back( ResolutionItem( 864, 486, ResolutionType_Size ) );
   g_GlobalResolutionList.push_back( ResolutionItem( 960, 540, ResolutionType_Size ) );
   g_GlobalResolutionList.push_back( ResolutionItem( 1024, 768, ResolutionType_Size ) );
   g_GlobalResolutionList.push_back( ResolutionItem( 1280, 720, ResolutionType_Size ) );
   g_GlobalResolutionList.push_back( ResolutionItem( 1280, 960, ResolutionType_Size ) );
   g_GlobalResolutionList.push_back( ResolutionItem( 1280, 1024, ResolutionType_Size ) );
   g_GlobalResolutionList.push_back( ResolutionItem( 1600, 1200, ResolutionType_Size ) );
   g_GlobalResolutionList.push_back( ResolutionItem( 1920, 1080, ResolutionType_Size ) );
   g_GlobalResolutionList.push_back( ResolutionItem( 2048, 1536, ResolutionType_Size ) );
   g_GlobalResolutionList.m_bGlobal = TRUE;

   IPCameraList tmp_ipcam_list;
   IPCameraProperty* pIPC;
   VideoResolutionList* pResolutionList;

   //-----------------------------------------------------------------------------
   // IntelliVIX - IVX Video Server
   pIPC                    = new IPCameraProperty;
   pIPC->m_nCompanyID      = CompanyID_Illisis;
   pIPC->m_nDefPortNo      = 10000;
   pIPC->m_nDefPTZPortNo   = 10000;
   pIPC->m_strDefUserName  = "ivx0001";
   pIPC->m_strDefPassword  = "ivxpw01";
   pIPC->m_nCapabilities   = IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_DisableVideoProperties | IPCamCaps_VideoAnalyticsVideoServer | IPCamCaps_SupportMultiStream;
   pIPC->m_nPTZControlType = 0;
   pIPC->m_nIPCameraType   = IPCameraType_IVXVideoServer;
   pIPC->m_strProdName     = "IntelliVIX";
   pIPC->m_strProdNbr[0]   = "IVX";
   pIPC->m_strProdNbr[1]   = "IVX";
   pIPC->m_nVideoPortNum   = 64;
   pIPC->m_nColorSpace     = ColorSpace_YUY2;
   pIPC->InitDigitalIO( 4, 4 );

   // 해상도 설정 없음 - 접속 시 해상도 결정
   pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 15.0f );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_MPEG4 );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_H263 );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
   tmp_ipcam_list.push_back( pIPC );

   //-----------------------------------------------------------------------------
   // AXIS
   pIPC                    = new IPCameraProperty;
   pIPC->m_nCompanyID      = CompanyID_AXIS;
   pIPC->m_nDefPortNo      = 80;
   pIPC->m_nDefPTZPortNo   = 80;
   pIPC->m_strDefUserName  = "root";
   pIPC->m_strDefPassword  = "pass";
   pIPC->m_nCapabilities   = IPCamCaps_PTZControl | IPCamCaps_TestIPCam | IPCamCaps_GetIPCamInfo | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_PTZPortIsWebPort | IPCamCaps_SupportSameSubModel | IPCamCaps_SurpportDirectVideoStreaming;
   pIPC->m_nPTZControlType = PTZControlType_Axis_212;
   pIPC->m_nIPCameraType   = IPCameraType_AXIS;
   pIPC->m_strProdName     = "AXIS";
   pIPC->m_strProdNbr[0]   = "AXIS";
   pIPC->m_nVideoPortNum   = 1;
   pIPC->m_nColorSpace     = ColorSpace_YUY2;
   pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 30 );
   pIPC->m_ImageProperty[ImagePropertyType_Brightness].SetProperty( 1, 100, 1, 50 );
   pIPC->m_ImageProperty[ImagePropertyType_Sharpness].SetProperty( 1, 100, 1, 50 );
   pIPC->InitDigitalIO( 1, 1 );

   // 해상도 리스트가 있어야 함.
   pIPC->m_ResolutionListMgr.AddResolutionList( &g_GlobalResolutionList );
   pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_NTSC, 1.0f, 30.0f, 1.0f, 15.0f );

   pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
   //pIPC->m_VideoFormatList.push_back (VCODEC_ID_MJPEG);
   //pIPC->m_VideoFormatList.push_back (VCODEC_ID_MPEG4);
   tmp_ipcam_list.push_back( pIPC );

   // __SUPPORT_YOUTECH_CAMERA
   //-----------------------------------------------------------------------------
   // CryptoTelecom
   pIPC                    = new IPCameraProperty;
   pIPC->m_nCompanyID      = CompanyID_CryptoTelecom;
   pIPC->m_nCapabilities   = ( IPCamCaps_PTZControl | IPCamCaps_AlarmIODependOnStream | IPCamCaps_TestIPCam );
   pIPC->m_nPTZControlType = PTZControlType_CryptoTelecom;
   pIPC->m_nIPCameraType   = IPCameraType_CryptoTelecom_S152;
   pIPC->m_strProdName     = "CryptoTelecom";
   pIPC->m_strProdNbr[0]   = "S152";
   pIPC->m_nVideoPortNum   = 2;
   pIPC->m_nColorSpace     = ColorSpace_YUY2;
   pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 30 );
   pIPC->m_ImageProperty[ImagePropertyType_Brightness].SetProperty( 1, 100, 1, 50 );
   pIPC->m_ImageProperty[ImagePropertyType_Contrast].SetProperty( 1, 100, 1, 50 );
   pIPC->m_ImageProperty[ImagePropertyType_Hue].SetProperty( 1, 100, 1, 50 );
   pIPC->InitDigitalIO( 2, 2 );

   // 해상도 설정 없음 - 접속 시 해상도 결정
   pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_NTSC, 1.0f, 30.0f, 1.0f, 15.0f );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_MPEG4 );
   tmp_ipcam_list.push_back( pIPC );

   //-----------------------------------------------------------------------------
   // ICanTek
   pIPC                    = new IPCameraProperty;
   pIPC->m_nCompanyID      = CompanyID_ICanTek;
   pIPC->m_nCapabilities   = IPCamCaps_PTZControl | IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_GetCompressionLevelNotSupported | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_SupportCustomPTZCommand | IPCamCaps_AlarmIODependOnStream;
   pIPC->m_nPTZControlType = PTZControlType_ICanTek;
   pIPC->m_nIPCameraType   = IPCameraType_ICanServer;
   pIPC->m_strProdName     = "ICanServer";
   pIPC->m_nVideoPortNum   = 4;
   pIPC->m_nColorSpace     = ColorSpace_YUY2;
   pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );
   pIPC->m_ImageProperty[ImagePropertyType_Brightness].SetProperty( 1, 100, 1, 50 );
   pIPC->m_ImageProperty[ImagePropertyType_Contrast].SetProperty( 1, 100, 1, 50 );
   pIPC->InitDigitalIO( 4, 2 );

   // 해상도 설정 없음 - 접속 시 해상도 결정
   pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_NTSC, 1.0f, 30.0f, 1.0f, 15.0f );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_MPEG4 );

   tmp_ipcam_list.push_back( pIPC );

   //-----------------------------------------------------------------------------
   // Apro Media (크립토 텔레콤)
   pIPC                    = new IPCameraProperty;
   pIPC->m_nCompanyID      = CompanyID_AproMedia;
   pIPC->m_nCapabilities   = IPCamCaps_PTZControl | IPCamCaps_TestIPCam | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_AlarmIODependOnStream | IPCamCaps_ShowSameManufacturer | IPCamCaps_SupportCustomPTZCommand;
   pIPC->m_nPTZControlType = PTZControlType_AproMedia;
   pIPC->m_nIPCameraType   = IPCameraType_AproMedia;
   pIPC->m_strProdName     = "AproMedia Video Servers";
   pIPC->m_strProdNbr[0]   = "S152";
   pIPC->m_nVideoPortNum   = 2;
   pIPC->m_nColorSpace     = ColorSpace_YUY2;
   pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 30 );
   pIPC->m_ImageProperty[ImagePropertyType_Brightness].SetProperty( 1, 100, 1, 50 );
   pIPC->m_ImageProperty[ImagePropertyType_Contrast].SetProperty( 1, 100, 1, 50 );
   pIPC->m_ImageProperty[ImagePropertyType_Hue].SetProperty( 1, 100, 1, 50 );
   pIPC->InitDigitalIO( 2, 2 );

   // 해상도 설정 없음 - 접속 시 해상도 결정
   pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_NTSC, 1.0f, 30.0f, 1.0f, 15.0f );

   pIPC->m_VideoFormatList.push_back( VCODEC_ID_MPEG4 );
   tmp_ipcam_list.push_back( pIPC );

   // (mkjang-140829)
   //-----------------------------------------------------------------------------
   // Apro Http
   pIPC                  = new IPCameraProperty;
   pIPC->m_nCompanyID    = CompanyID_AproHttp;
   pIPC->m_nCapabilities = IPCamCaps_PTZControl | IPCamCaps_TestIPCam | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_AlarmIODependOnStream | IPCamCaps_ShowSameManufacturer;
   //IPCamCaps_SupportCustomPTZCommand;
   pIPC->m_nPTZControlType = PTZControlType_AproHttp;
   pIPC->m_nIPCameraType   = IPCameraType_AproHttp;
   pIPC->m_strProdName     = "Apro(Http)";
   pIPC->m_nVideoPortNum   = 2;
   pIPC->m_nColorSpace     = ColorSpace_YUY2;
   pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 30 );
   pIPC->m_ImageProperty[ImagePropertyType_Brightness].SetProperty( 1, 100, 1, 50 );
   pIPC->m_ImageProperty[ImagePropertyType_Contrast].SetProperty( 1, 100, 1, 50 );
   pIPC->m_ImageProperty[ImagePropertyType_Hue].SetProperty( 1, 100, 1, 50 );
   pIPC->InitDigitalIO( 2, 2 );

   // 해상도 설정 없음 - 접속 시 해상도 결정
   pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_NTSC, 1.0f, 30.0f, 1.0f, 15.0f );

   pIPC->m_VideoFormatList.push_back( VCODEC_ID_MPEG4 );
   tmp_ipcam_list.push_back( pIPC );

   //-----------------------------------------------------------------------------
   // Truen
   pIPC                    = new IPCameraProperty;
   pIPC->m_nCompanyID      = CompanyID_Truen;
   pIPC->m_nDefPortNo      = 2222;
   pIPC->m_nDefPTZPortNo   = 80;
   pIPC->m_nDefZoomPortNo  = 80;
   pIPC->m_strDefUserName  = "admin";
   pIPC->m_strDefPassword  = "1234";
   pIPC->m_nCapabilities   = IPCamCaps_PTZControl | IPCamCaps_TestIPCam | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_GetCompressionLevelNotSupported | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_SupportCustomPTZCommand | IPCamCaps_SupportOtherConnectionForPTZControl | IPCamCaps_SupportAudioDataTransmit | IPCamCaps_SupportMultiPTZControl | IPCamCaps_SupportMultiStream | IPCamCaps_NeedExtraPTZPortNo | IPCamCaps_AlarmIODependOnStream;
   pIPC->m_nPTZControlType = PTZControlType_Truen;
   pIPC->m_nIPCameraType   = IPCameraType_Truen;
   pIPC->m_strProdName     = "Truen";
   pIPC->m_nVideoPortNum   = 4;
   pIPC->m_nColorSpace     = ColorSpace_YUY2;
   pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );
   // jun : 20100502 청와대에서 테스트한 결과 색상설정에 문제가 있음이 확인되어 설정 기능을 막았음.
   //       청와대 이외의 장비에서는 재현되지 않았었음.
   //pIPC->m_ImageProperty[ImagePropertyType_Brightness ].SetProperty (1, 100, 1, 50);
   //pIPC->m_ImageProperty[ImagePropertyType_Contrast   ].SetProperty (1, 100, 1, 50);
   //pIPC->m_ImageProperty[ImagePropertyType_Hue        ].SetProperty (1, 100, 1, 50);  // 색조 (Color)
   //pIPC->m_ImageProperty[ImagePropertyType_Saturation ].SetProperty (1, 100, 1, 50);
   pIPC->m_ImageProperty[ImagePropertyType_DayAndNight].SetProperty( 0, 2, 1, 2 );
   pIPC->m_ImageProperty[ImagePropertyType_AWB].SetProperty( 0, 3, 1, 0 );
   pIPC->m_ImageProperty[ImagePropertyType_BLC].SetProperty( 0, 1, 1, 0 );
   //   pIPC->m_ImageProperty[ImagePropertyType_WDR        ].SetProperty (0, 1, 1, 0); IMSI
   //   pIPC->m_ImageProperty[ImagePropertyType_DNR        ].SetProperty (0, 5, 1, 0); IMSI
   pIPC->InitDigitalIO( 2, 2 );

   // 해상도 설정 없음 - 접속 시 해상도 결정
   pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 15.0f );

   pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );

   tmp_ipcam_list.push_back( pIPC );

   //-----------------------------------------------------------------------------
   // Cellinx
   pIPC                    = new IPCameraProperty;
   pIPC->m_nDefPortNo      = 1852;
   pIPC->m_nDefPTZPortNo   = 1852;
   pIPC->m_strDefUserName  = "root";
   pIPC->m_strDefPassword  = "pass";
   pIPC->m_nCompanyID      = CompanyID_Cellinx;
   pIPC->m_nCapabilities   = IPCamCaps_PTZControl | IPCamCaps_TestIPCam | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_DisableVideoProperties | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_SupportCustomPTZCommand | IPCamCaps_AlarmIODependOnStream | IPCamCaps_SupportAudioDataTransmit;
   pIPC->m_nPTZControlType = PTZControlType_FSNetworks_FS_IR307H;
   pIPC->m_nIPCameraType   = IPCameraType_Cellinx_MR904;
   pIPC->m_strProdName     = "Cellinx";
   pIPC->m_nVideoPortNum   = 4;
   pIPC->m_nColorSpace     = ColorSpace_YUY2;
   pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );
   pIPC->m_ImageProperty[ImagePropertyType_Brightness].SetProperty( 1, 100, 1, 50 );
   pIPC->m_ImageProperty[ImagePropertyType_Contrast].SetProperty( 1, 100, 1, 50 );
   pIPC->m_ImageProperty[ImagePropertyType_Hue].SetProperty( 1, 100, 1, 50 ); // 색조 (Color)
   pIPC->m_ImageProperty[ImagePropertyType_Saturation].SetProperty( 1, 100, 1, 50 );
   pIPC->InitDigitalIO( 2, 2 );

   // 해상도 설정 없음 - 접속 시 해상도 결정
   pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 15.0f );

   pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_MJPEG );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_MPEG4 );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_H265 );

   tmp_ipcam_list.push_back( pIPC );

   if( IS_SUPPORT( __SUPPORT_IDIS_CAMERA ) ) {
      //-----------------------------------------------------------------------------
      // IDIS
      pIPC                    = new IPCameraProperty;
      pIPC->m_nCompanyID      = CompanyID_IDIS;
      pIPC->m_nDefPortNo      = 8016;
      pIPC->m_nDefPTZPortNo   = 8020;
      pIPC->m_strDefUserName  = "admin";
      pIPC->m_strDefPassword  = "1234";
      pIPC->m_nCapabilities   = IPCamCaps_PTZControl | IPCamCaps_TestIPCam | IPCamCaps_SupportSameSubModel | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_ReceiveVideoStreamByRTSP | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_NeedExtraPTZPortNo;
      pIPC->m_nPTZControlType = PTZControlType_IDIS;
      pIPC->m_nIPCameraType   = IPCameraType_IDIS;
      pIPC->m_strProdName     = "IDIS";
      pIPC->m_nVideoPortNum   = 1;
      pIPC->m_nColorSpace     = ColorSpace_YUY2;
      pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );
      pIPC->m_ImageProperty[ImagePropertyType_Brightness].SetProperty( 1, 100, 1, 50 );
      pIPC->m_ImageProperty[ImagePropertyType_Contrast].SetProperty( 1, 100, 1, 50 );
      pIPC->m_ImageProperty[ImagePropertyType_Hue].SetProperty( 1, 100, 1, 50 ); // 색조 (Color)
      pIPC->m_ImageProperty[ImagePropertyType_Saturation].SetProperty( 1, 100, 1, 50 );
      pIPC->InitDigitalIO( 1, 1 );

      // 해상도 설정 없음 - 접속 시 해상도 결정
      pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_NTSC, 1.0f, 30.0f, 1.0f, 15.0f );

      pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
      pIPC->m_VideoFormatList.push_back( VCODEC_ID_MPEG4 );
      pIPC->m_VideoFormatList.push_back( VCODEC_ID_MJPEG );

      tmp_ipcam_list.push_back( pIPC );
   }
   //-----------------------------------------------------------------------------
   // RTSP
   pIPC                    = new IPCameraProperty;
   pIPC->m_nCompanyID      = CompanyID_RTSP;
   pIPC->m_nDefPTZPortNo   = 0;
   pIPC->m_nDefPortNo      = 554;
   pIPC->m_strDefUserName  = "";
   pIPC->m_strDefPassword  = "";
   pIPC->m_nCapabilities   = IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_GetCompressionLevelNotSupported | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_SupportCustomPTZCommand | IPCamCaps_ReceiveVideoStreamByRTSP | IPCamCaps_PTZControl | IPCamCaps_SetupPTZContorlInfo;
   pIPC->m_nPTZControlType = 0;
   pIPC->m_nIPCameraType   = IPCameraType_RTSP;
   pIPC->m_strProdName     = "RTSP Streaming Server";
   pIPC->m_nVideoPortNum   = 1;
   pIPC->m_nColorSpace     = ColorSpace_YUY2;
   pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );

   // 해상도 설정 없음 - 접속 시 해상도 결정
   pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 15.0f );

   pIPC->m_VideoFormatList.push_back( VCODEC_ID_MPEG4 );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_MJPEG );
   tmp_ipcam_list.push_back( pIPC );

   //-----------------------------------------------------------------------------
   // Support Omnicast
   pIPC               = new IPCameraProperty;
   pIPC->m_nCompanyID = CompanyID_Omnicast;
   ///pIPC->m_nCapabilities       = IPCamCaps_SupportSendEventAlarmMessage;
   pIPC->m_nCapabilities = IPCamCaps_SupportSendEventAlarmMessage | IPCamCaps_PTZControl | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_SupportCtrolPTZByOuterServer | //카메라 속성수정
       IPCamCaps_SupportSendEventAlarmMsgByOuterServer; //외부 서버 알람전송 관련
   pIPC->m_nPTZControlType = PTZControlType_Omnicast;
   pIPC->m_nIPCameraType   = IPCameraType_Omnicast;
   pIPC->m_strProdName     = "[VMS] Omnicast";
   pIPC->m_nVideoPortNum   = 4;
   pIPC->m_nColorSpace     = ColorSpace_YUY2;
   pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );

   pIPC->m_ResolutionListMgr.AddResolutionList( &g_GlobalResolutionList );
   pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 15.0f );

   pIPC->m_VideoFormatList.push_back( VCODEC_ID_MPEG4 );
   tmp_ipcam_list.push_back( pIPC );

   // [20160203WED:SGLEE] bosch 카메라가 무조건 추가되도록..
   //if( IS_PRODUCT(__PRODUCT_SRS) ) {
   //-----------------------------------------------------------------------------
   // Bosch
   pIPC                    = new IPCameraProperty;
   pIPC->m_nCompanyID      = CompanyID_Bosch;
   pIPC->m_nCapabilities   = IPCamCaps_PTZControl | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_GetCompressionLevelNotSupported | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_SupportCustomPTZCommand;
   pIPC->m_nPTZControlType = PTZControlType_Bosch_VIPX1;
   pIPC->m_nIPCameraType   = IPCameraType_Bosch_VIPX1;
   pIPC->m_strProdName     = "Bosch";
   pIPC->m_nVideoPortNum   = 1;
   pIPC->m_nColorSpace     = ColorSpace_YUY2;
   pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );

   // 색조, 이미지 제어 안됨...

   pResolutionList               = new VideoResolutionList;
   pResolutionList->m_nVideoType = VideoType_NTSC;
   pResolutionList->push_back( ResolutionItem( 704, 480, ResolutionType_4CIF ) ); // 4CIF
   pResolutionList->push_back( ResolutionItem( 352, 240, ResolutionType_CIF ) ); // CIF
   pResolutionList->push_back( ResolutionItem( 176, 120, ResolutionType_QCIF ) ); // QCIF

   pIPC->m_ResolutionListMgr.AddResolutionList( pResolutionList );
   pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_NTSC, 1.0f, 30.0f, 1.0f, 15.0f );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_H263 );
   tmp_ipcam_list.push_back( pIPC );

//}

#ifdef __SUPPORT_PELCO_ENDURA
   //-----------------------------------------------------------------------------
   // PELCO Endura NET5400T
   if( IS_SUPPORT( __SUPPORT_PELCO_ENDURA ) ) {
      pIPC                    = new IPCameraProperty;
      pIPC->m_nDefPortNo      = 554;
      pIPC->m_nDefPTZPortNo   = 80;
      pIPC->m_nCompanyID      = CompanyID_Pelco;
      pIPC->m_strDefUserName  = "admin";
      pIPC->m_strDefPassword  = "admin";
      pIPC->m_nCapabilities   = IPCamCaps_PTZControl | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_GetCompressionLevelNotSupported | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_NeedExtraPTZPortNo | IPCamCaps_SupportSameSubModel | IPCamCaps_ReceiveVideoStreamByRTSP | IPCamCaps_PTZPortIsWebPort;
      pIPC->m_nPTZControlType = PTZControlType_PelcoEndura;
      pIPC->m_nIPCameraType   = IPCameraType_Pelco_EnduraNET5400T;
      pIPC->m_strProdName     = "PELCO Endura";
      pIPC->m_nVideoPortNum   = 1;
      pIPC->m_nColorSpace     = ColorSpace_YUY2;
      pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );
      pIPC->InitDigitalIO( 4, 4 );

      // 해상도 설정 없음 - 접속 시 해상도 결정
      pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 15.0f );

      pIPC->m_VideoFormatList.push_back( VCODEC_ID_MPEG4 );
      pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
      tmp_ipcam_list.push_back( pIPC );
   }
#endif

   //-----------------------------------------------------------------------------
   // Sony IPELA
   pIPC                    = new IPCameraProperty;
   pIPC->m_nCompanyID      = CompanyID_Sony;
   pIPC->m_nDefPortNo      = 80;
   pIPC->m_nDefPTZPortNo   = 80;
   pIPC->m_strDefUserName  = "admin";
   pIPC->m_strDefPassword  = "admin";
   pIPC->m_nCapabilities   = IPCamCaps_PTZControl | IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_GetCompressionLevelNotSupported | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_PTZPortIsWebPort;
   pIPC->m_nPTZControlType = PTZControlType_SonyIPELA_SNC_RH164_124;
   pIPC->m_nIPCameraType   = IPCameraType_Sony_IPELA;
   pIPC->m_strProdName     = "Sony IPELA SNC Series";
   pIPC->m_nVideoPortNum   = 1;
   pIPC->m_nColorSpace     = ColorSpace_YUY2;
   pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );
   pIPC->InitDigitalIO( 0, 0 );

   // 해상도 설정 없음 - 접속 시 해상도 결정
   pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 15.0f );

   pIPC->m_VideoFormatList.push_back( VCODEC_ID_MPEG4 );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
   tmp_ipcam_list.push_back( pIPC );

   //---------------------------------------------------------------------------------------
   // Innodep
   if( IS_SUPPORT( __SUPPORT_INNODEP ) ) {
      pIPC                  = new IPCameraProperty;
      pIPC->m_nCompanyID    = CompanyID_Innodep;
      pIPC->m_nDefPortNo    = 6001;
      pIPC->m_nDefPTZPortNo = 6001;
      pIPC->m_nCapabilities = IPCamCaps_PTZControl | IPCamCaps_TestIPCam | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_SupportSendEventAlarmMessage | IPCamCaps_SupportCtrolPTZByOuterServer | //카메라 속성수정
          IPCamCaps_SupportSendEventAlarmMsgByOuterServer; //외부 서버 알람전송 관련
      pIPC->m_nPTZControlType = PTZControlType_Innodep;
      pIPC->m_nIPCameraType   = IPCameraType_Innodep;
      pIPC->m_strProdName     = "[VMS] Innodep";
      pIPC->m_nVideoPortNum   = 64;
      pIPC->m_nColorSpace     = ColorSpace_YUY2;
      pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );
      //sesoni : ImagePropertyType 생략 - VMS SDK manual 및 예제프로그램에 내용 없음
      pIPC->InitDigitalIO( 2, 2 );

      // 해상도 설정 없음 - 접속 시 해상도 결정
      pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 30.0f );

      pIPC->m_VideoFormatList.push_back( VCODEC_ID_MPEG4 );
      pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
      pIPC->m_VideoFormatList.push_back( VCODEC_ID_H263 );
      pIPC->m_VideoFormatList.push_back( VCODEC_ID_MJPEG );

      tmp_ipcam_list.push_back( pIPC );
   }

   //---------------------------------------------------------------------------------------
   // HIK Vision
   pIPC                   = new IPCameraProperty;
   pIPC->m_nCompanyID     = CompanyID_HIKVision;
   pIPC->m_nDefPortNo     = 554;
   pIPC->m_nDefPTZPortNo  = 8000;
   pIPC->m_strDefUserName = "admin";
   pIPC->m_strDefPassword = "12345";
   pIPC->m_nCapabilities  = IPCamCaps_PTZControl | IPCamCaps_TestIPCam | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_SupportSendEventAlarmMessage | IPCamCaps_SupportPlayingRecordingVideos | IPCamCaps_SupportSameSubModel | IPCamCaps_SupportOtherConnectionForPTZControl | IPCamCaps_NeedExtraPTZPortNo | IPCamCaps_NeedStreamingTypeChoice | IPCamCaps_AlarmIODependOnStream | IPCamCaps_SupportMultiStream;

   pIPC->m_nPTZControlType = PTZControlType_HIKVision_NAIS_M138IR;
   pIPC->m_nIPCameraType   = IPCameraType_HIKVision;
   pIPC->m_strProdName     = "HIK Vision";
   pIPC->m_nVideoPortNum   = 4;
   pIPC->m_nColorSpace     = ColorSpace_YUY2;
   pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );

   pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 30.0f );

   pIPC->m_VideoFormatList.push_back( VCODEC_ID_MPEG4 );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_H263 );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_MJPEG );

   tmp_ipcam_list.push_back( pIPC );

   //---------------------------------------------------------------------------------------
   // Samsung Techwin iPOLiS
   pIPC                   = new IPCameraProperty;
   pIPC->m_nDefPortNo     = 554;
   pIPC->m_nDefPTZPortNo  = 80;
   pIPC->m_strDefUserName = "admin";
   pIPC->m_strDefPassword = "4321";
   pIPC->m_nCompanyID     = CompanyID_SamsungTechwin;
   pIPC->m_nCapabilities  = IPCamCaps_PTZControl | IPCamCaps_TestIPCam | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_GetCompressionLevelNotSupported | IPCamCaps_SupportPlayingRecordingVideos | IPCamCaps_ReceiveVideoStreamByRTSP | IPCamCaps_NeedExtraPTZPortNo | IPCamCaps_SupportOtherConnectionForPTZControl | IPCamCaps_SupportSameSubModel | IPCamCaps_SupportAudioDataTransmit | IPCamCaps_PTZPortIsWebPort;

   pIPC->m_nPTZControlType = PTZControlType_SamsungTechWin_iPOLiS;
   pIPC->m_nIPCameraType   = IPCameraType_SamsungTechwin;
   pIPC->m_strProdName     = "Hanhwa Techwin iPOLiS";
   pIPC->m_nVideoPortNum   = 16;
   pIPC->m_nColorSpace     = ColorSpace_YUY2;

   pIPC->m_ImageProperty[ImagePropertyType_DayAndNight].SetProperty( 0, 2, 1, 2 );
   pIPC->m_ImageProperty[ImagePropertyType_AWB].SetProperty( 0, 1, 1, 0 );
   pIPC->m_ImageProperty[ImagePropertyType_BLC].SetProperty( 0, 1, 1, 0 );

   // 해상도 설정 없음 - 접속 시 해상도 결정
   pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 30.0f );

   pIPC->m_VideoFormatList.push_back( VCODEC_ID_MPEG4 );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_MJPEG );

   tmp_ipcam_list.push_back( pIPC );

   //---------------------------------------------------------------------------------------
   // RealHub
   if( IS_SUPPORT( __SUPPORT_REALHUB ) ) {
      pIPC                  = new IPCameraProperty;
      pIPC->m_nCompanyID    = CompanyID_RealHub;
      pIPC->m_nCapabilities = IPCamCaps_PTZControl | IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_GetCompressionLevelNotSupported | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_SupportCtrolPTZByOuterServer | // TYLEE: 2013-10-14
          IPCamCaps_SupportSendEventAlarmMsgByOuterServer | IPCamCaps_SupportSendEventAlarmMessage | IPCamCaps_ReceiveVideoStreamByRTSP;
      pIPC->m_nPTZControlType = PTZControlType_RealHub; //미구현
      pIPC->m_nIPCameraType   = IPCameraType_RealHub;
      pIPC->m_strProdName     = "[VMS] RealHub NVR";
      pIPC->m_nVideoPortNum   = 1;
      pIPC->m_nColorSpace     = ColorSpace_YUY2;
      pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );

      // 해상도 설정 없음 - 접속 시 해상도 결정
      pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 15.0f );

      pIPC->m_VideoFormatList.push_back( VCODEC_ID_MPEG4 );
      pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
      tmp_ipcam_list.push_back( pIPC );
   }

   //---------------------------------------------------------------------------------------
   // Dahua
   pIPC                    = new IPCameraProperty;
   pIPC->m_nCompanyID      = CompanyID_Dahua;
   pIPC->m_nDefPortNo      = 554;
   pIPC->m_nDefPTZPortNo   = 37777;
   pIPC->m_strDefUserName  = "admin";
   pIPC->m_strDefPassword  = "admin";
   pIPC->m_nCapabilities   = IPCamCaps_PTZControl | IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_GetCompressionLevelNotSupported | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_NeedExtraPTZPortNo | IPCamCaps_SupportAudioDataTransmit | IPCamCaps_SupportSameSubModel | IPCamCaps_ReceiveVideoStreamByRTSP | IPCamCaps_SupportMultiStream;
   pIPC->m_nPTZControlType = PTZControlType_Dahua_NS_SD200IR;
   pIPC->m_nIPCameraType   = IPCameraType_Dahua;
   pIPC->m_strProdName     = "Dahua";
   pIPC->m_nVideoPortNum   = 4;
   pIPC->m_nColorSpace     = ColorSpace_YUY2;
   pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );
   pIPC->InitDigitalIO( 8, 2 ); // DAHUA_CAMERA_AUDIO_ENABLE

   // 해상도 설정 없음 - 접속 시 해상도 결정
   pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 15.0f );

   pIPC->m_VideoFormatList.push_back( VCODEC_ID_MPEG4 );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
   tmp_ipcam_list.push_back( pIPC );

   //---------------------------------------------------------------------------------------
   // Win4net
   pIPC                    = new IPCameraProperty;
   pIPC->m_nCompanyID      = CompanyID_Win4net;
   pIPC->m_nDefPortNo      = 554;
   pIPC->m_nDefPTZPortNo   = 80;
   pIPC->m_nCapabilities   = IPCamCaps_PTZControl | IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_GetCompressionLevelNotSupported | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_NeedExtraPTZPortNo | IPCamCaps_ReceiveVideoStreamByRTSP;
   pIPC->m_nPTZControlType = PTZControlType_Win4net_CLEBO_PM10HT;
   pIPC->m_nIPCameraType   = IPCameraType_Win4net;
   pIPC->m_strProdName     = "Win4net";
   pIPC->m_nVideoPortNum   = 1;
   pIPC->m_nColorSpace     = ColorSpace_YUY2;
   pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );

   // 해상도 설정 없음 - 접속 시 해상도 결정
   pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 15.0f );

   pIPC->m_VideoFormatList.push_back( VCODEC_ID_MPEG4 );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
   tmp_ipcam_list.push_back( pIPC );

   //---------------------------------------------------------------------------------------
   // SecurityCenter
   if( IS_SUPPORT( __SUPPORT_SECURITYCENTER ) ) {
      pIPC                  = new IPCameraProperty;
      pIPC->m_nCompanyID    = CompanyID_SecurityCenter;
      pIPC->m_nCapabilities = IPCamCaps_SupportSendEventAlarmMessage | IPCamCaps_PTZControl | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_SupportCtrolPTZByOuterServer | //카메라 속성수정
          IPCamCaps_SupportSendEventAlarmMsgByOuterServer | //외부 서버 알람전송 관련
          IPCamCaps_SupportMultiStream; // GSC 멀티스트림 지원
      pIPC->m_nPTZControlType = PTZControlType_SecurityCenter;
      pIPC->m_nIPCameraType   = IPCameraType_SecurityCenter;
      pIPC->m_strProdName     = "[VMS] SecurityCenter";
      pIPC->m_nVideoPortNum   = 4;
      pIPC->m_nColorSpace     = ColorSpace_YV12;
      pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );

      // 해상도 설정 없음 - 접속 시 해상도 결정
      pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 15.0f );

      pIPC->m_VideoFormatList.push_back( VCODEC_ID_MPEG4 );
      tmp_ipcam_list.push_back( pIPC );
   }

   //---------------------------------------------------------------------------------------
   // Hitron OEM Best Digital
   pIPC                    = new IPCameraProperty;
   pIPC->m_nCompanyID      = CompanyID_BestDigital;
   pIPC->m_nDefPortNo      = 554;
   pIPC->m_nDefPTZPortNo   = 7011;
   pIPC->m_strDefUserName  = "admin";
   pIPC->m_strDefPassword  = "pass";
   pIPC->m_nCapabilities   = IPCamCaps_PTZControl | IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_GetCompressionLevelNotSupported | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_NeedExtraPTZPortNo | IPCamCaps_ReceiveVideoStreamByRTSP | IPCamCaps_SupportMultiStream | IPCamCaps_SupportSameSubModel;
   pIPC->m_nPTZControlType = PTZControlType_BestDigital_BVS_H2020R;
   pIPC->m_nIPCameraType   = IPCameraType_BestDigital;
   pIPC->m_strProdName     = "Best Digital";
   pIPC->m_nVideoPortNum   = 1;
   pIPC->m_nColorSpace     = ColorSpace_YUY2;
   pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );

   // 해상도 설정 없음 - 접속 시 해상도 결정
   pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 15.0f );

   pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_MPEG4 );
   tmp_ipcam_list.push_back( pIPC );

   //---------------------------------------------------------------------------------------
   // Hitron
   pIPC                    = new IPCameraProperty;
   pIPC->m_nCompanyID      = CompanyID_Hitron;
   pIPC->m_nDefPortNo      = 554;
   pIPC->m_nDefPTZPortNo   = 80;
   pIPC->m_strDefUserName  = "admin";
   pIPC->m_strDefPassword  = "admin";
   pIPC->m_nCapabilities   = IPCamCaps_PTZControl | IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_GetCompressionLevelNotSupported | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_NeedExtraPTZPortNo | IPCamCaps_ReceiveVideoStreamByRTSP | IPCamCaps_SupportSameSubModel | IPCamCaps_SupportMultiStream;
   pIPC->m_nPTZControlType = PTZControlType_Hitron_NFX_12053B1;
   pIPC->m_nIPCameraType   = IPCameraType_Hitron;
   pIPC->m_strProdName     = "Hitron";
   pIPC->m_nVideoPortNum   = 4;
   pIPC->m_nColorSpace     = ColorSpace_YUY2;
   pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );

   // 해상도 설정 없음 - 접속 시 해상도 결정
   pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 15.0f );

   pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_MPEG4 );
   tmp_ipcam_list.push_back( pIPC );

   //---------------------------------------------------------------------------------------
   // YoungKook
   pIPC                    = new IPCameraProperty;
   pIPC->m_nCompanyID      = CompanyID_YoungKook;
   pIPC->m_nDefPortNo      = 554;
   pIPC->m_nDefPTZPortNo   = 3000;
   pIPC->m_strDefUserName  = "admin";
   pIPC->m_strDefPassword  = "admin";
   pIPC->m_nCapabilities   = IPCamCaps_PTZControl | IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_GetCompressionLevelNotSupported | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_ReceiveVideoStreamByRTSP | IPCamCaps_NeedExtraPTZPortNo | IPCamCaps_SupportMultiStream | IPCamCaps_SupportSameSubModel | IPCamCaps_ShowSameManufacturer | IPCamCaps_AlarmIODependOnStream;
   pIPC->m_nPTZControlType = PTZControlType_YoungKook_YSDIRMP10;
   pIPC->m_nIPCameraType   = IPCameraType_YoungKook;
   pIPC->m_strProdName     = "YoungKook Electronics";
   pIPC->m_nVideoPortNum   = 4;
   pIPC->m_nColorSpace     = ColorSpace_YUY2;
   pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );

   // 해상도 설정 없음 - 접속 시 해상도 결정
   pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 15.0f );

   pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_MPEG4 );
   tmp_ipcam_list.push_back( pIPC );

   //---------------------------------------------------------------------------------------
   // Panasonic
   // __SUPPORT_PANASONIC_CAMERA
   pIPC                    = new IPCameraProperty;
   pIPC->m_nCompanyID      = CompanyID_Panasonic;
   pIPC->m_nDefPortNo      = 554;
   pIPC->m_nDefPTZPortNo   = 80;
   pIPC->m_strDefUserName  = "admin";
   pIPC->m_strDefPassword  = "12345";
   pIPC->m_nCapabilities   = IPCamCaps_PTZControl | IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_GetCompressionLevelNotSupported | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_ReceiveVideoStreamByRTSP | IPCamCaps_SupportSameSubModel | IPCamCaps_SupportOtherConnectionForPTZControl | IPCamCaps_NeedExtraPTZPortNo | IPCamCaps_PTZPortIsWebPort;
   pIPC->m_nPTZControlType = PTZControlType_Panasonic_WVSC385;
   pIPC->m_nIPCameraType   = IPCameraType_Panasonic;
   pIPC->m_strProdName     = "Panasonic";
   pIPC->m_nVideoPortNum   = 4;
   pIPC->m_nColorSpace     = ColorSpace_YUY2;
   pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );

   // 해상도 설정 없음 - 접속 시 해상도 결정
   pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 15.0f );

   pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_MPEG4 );

   tmp_ipcam_list.push_back( pIPC );

   //---------------------------------------------------------------------------------------
   // XProtect
   if( IS_SUPPORT( __SUPPORT_XPROTECT ) ) {
      pIPC                  = new IPCameraProperty;
      pIPC->m_nCompanyID    = CompanyID_XProtect;
      pIPC->m_nCapabilities = IPCamCaps_SupportSendEventAlarmMessage | IPCamCaps_PTZControl | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_SupportCtrolPTZByOuterServer | IPCamCaps_SupportSendEventAlarmMsgByOuterServer | IPCamCaps_SupportCustomPTZCommand;

      pIPC->m_nPTZControlType = PTZControlType_XProtect;
      pIPC->m_nIPCameraType   = IPCameraType_XProtect;
      pIPC->m_strProdName     = "[VMS] XProtect";
      pIPC->m_nVideoPortNum   = 32;
      pIPC->m_nColorSpace     = ColorSpace_RGB; //ColorSpace_YUY2;
      pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );

      // 해상도 설정 없음 - 접속 시 해상도 결정
      pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 15.0f );

      pIPC->m_VideoFormatList.push_back( VCODEC_ID_RAW );
      tmp_ipcam_list.push_back( pIPC );
   }

   if( IS_PRODUCT( __PRODUCT_B200 ) ) {
      //---------------------------------------------------------------------------------------
      // FlexWatch (Convex CNB-1000)
      pIPC                   = new IPCameraProperty;
      pIPC->m_nCompanyID     = CompanyID_FlexWatch;
      pIPC->m_nDefPortNo     = 554;
      pIPC->m_nDefPTZPortNo  = 80;
      pIPC->m_strDefUserName = "root";
      pIPC->m_strDefPassword = "root";
      pIPC->m_nCapabilities  = IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_GetCompressionLevelNotSupported | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_SupportAudioDataTransmit | IPCamCaps_ReceiveVideoStreamByRTSP | IPCamCaps_SupportMotionDetection;
      pIPC->m_nIPCameraType  = IPCameraType_FlexWatch;
      pIPC->m_strProdName    = "Convex CNB-1000";
      pIPC->m_nVideoPortNum  = 4;
      pIPC->m_nColorSpace    = ColorSpace_YUY2;
      pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );
      pIPC->InitDigitalIO( 8, 2 );

      // 해상도 설정 없음 - 접속 시 해상도 결정
      pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 15.0f );

      pIPC->m_VideoFormatList.push_back( VCODEC_ID_MPEG4 );
      pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
      tmp_ipcam_list.push_back( pIPC );

      //---------------------------------------------------------------------------------------
      // FlexWatch
      pIPC                    = new IPCameraProperty;
      pIPC->m_nCompanyID      = CompanyID_FlexWatch;
      pIPC->m_nDefPortNo      = 554;
      pIPC->m_nDefPTZPortNo   = 80;
      pIPC->m_strDefUserName  = "root";
      pIPC->m_strDefPassword  = "root";
      pIPC->m_nCapabilities   = IPCamCaps_PTZControl | IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_GetCompressionLevelNotSupported | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_NeedExtraPTZPortNo | IPCamCaps_SupportAudioDataTransmit | IPCamCaps_ReceiveVideoStreamByRTSP | IPCamCaps_SupportMotionDetection;
      pIPC->m_nPTZControlType = PTZControlType_FlexWatch_FW1177_DEF;
      pIPC->m_nIPCameraType   = IPCameraType_FlexWatch_FW1177_DE;
      pIPC->m_strProdName     = "Convex CND-1370";
      pIPC->m_nVideoPortNum   = 4;
      pIPC->m_nColorSpace     = ColorSpace_YUY2;
      pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );
      pIPC->InitDigitalIO( 8, 2 );

      // 해상도 설정 없음 - 접속 시 해상도 결정
      pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 15.0f );

      pIPC->m_VideoFormatList.push_back( VCODEC_ID_MPEG4 );
      pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
      tmp_ipcam_list.push_back( pIPC );

      //---------------------------------------------------------------------------------------
      // UDP (CNB-2000)
      pIPC                   = new IPCameraProperty;
      pIPC->m_nCompanyID     = CompanyID_UDP;
      pIPC->m_nDefPortNo     = 554;
      pIPC->m_nDefPTZPortNo  = 80;
      pIPC->m_strDefUserName = "root";
      pIPC->m_strDefPassword = "pass";
      pIPC->m_nCapabilities  = IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_GetCompressionLevelNotSupported | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_SupportAudioDataTransmit | IPCamCaps_ReceiveVideoStreamByRTSP | IPCamCaps_SupportMotionDetection;
      pIPC->m_nIPCameraType  = IPCameraType_UDP;
      pIPC->m_strProdName    = "Convex CNB-2000";
      pIPC->m_nVideoPortNum  = 4;
      pIPC->m_nColorSpace    = ColorSpace_YUY2;
      pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );
      pIPC->InitDigitalIO( 8, 2 );

      // 해상도 설정 없음 - 접속 시 해상도 결정
      pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 15.0f );

      pIPC->m_VideoFormatList.push_back( VCODEC_ID_MPEG4 );
      pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
      tmp_ipcam_list.push_back( pIPC );

      //---------------------------------------------------------------------------------------
      // UDP (CND-5200)
      pIPC                    = new IPCameraProperty;
      pIPC->m_nCompanyID      = CompanyID_UDP;
      pIPC->m_nDefPortNo      = 554;
      pIPC->m_nDefPTZPortNo   = 80;
      pIPC->m_strDefUserName  = "root";
      pIPC->m_strDefPassword  = "pass";
      pIPC->m_nCapabilities   = IPCamCaps_PTZControl | IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_GetCompressionLevelNotSupported | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_NeedExtraPTZPortNo | IPCamCaps_SupportAudioDataTransmit | IPCamCaps_ReceiveVideoStreamByRTSP | IPCamCaps_SupportMotionDetection;
      pIPC->m_nPTZControlType = PTZControlType_UDP_CND_20ZHO;
      pIPC->m_nIPCameraType   = IPCameraType_UDP_CND_20Z_HO;
      pIPC->m_strProdName     = "Convex CND-5200";
      pIPC->m_nVideoPortNum   = 4;
      pIPC->m_nColorSpace     = ColorSpace_YUY2;
      pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );
      pIPC->InitDigitalIO( 8, 2 );

      // 해상도 설정 없음 - 접속 시 해상도 결정
      pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 15.0f );

      pIPC->m_VideoFormatList.push_back( VCODEC_ID_MPEG4 );
      pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
      tmp_ipcam_list.push_back( pIPC );
   }

   //---------------------------------------------------------------------------------------
   // Probe
   pIPC                    = new IPCameraProperty;
   pIPC->m_nCompanyID      = CompanyID_ProbeDigital;
   pIPC->m_nDefPortNo      = 554;
   pIPC->m_nDefPTZPortNo   = 7777;
   pIPC->m_strDefUserName  = "admin";
   pIPC->m_strDefPassword  = "admin";
   pIPC->m_nCapabilities   = IPCamCaps_PTZControl | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_GetCompressionLevelNotSupported | IPCamCaps_ShowSameManufacturer | IPCamCaps_ReceiveVideoStreamByRTSP | IPCamCaps_SupportMultiStream | IPCamCaps_SupportSameSubModel | IPCamCaps_ShowSameManufacturer | IPCamCaps_NeedStreamingTypeChoice;
   pIPC->m_nPTZControlType = PTZControlType_ProbeDigital_PRI_H2010;
   pIPC->m_nIPCameraType   = IPCameraType_ProbeDigital;
   pIPC->m_strProdName     = "Probe Digital";
   pIPC->m_nVideoPortNum   = 4;
   pIPC->m_nColorSpace     = ColorSpace_YUY2;
   pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );
   pIPC->InitDigitalIO( 8, 2 );

   // 해상도 설정 없음 - 접속 시 해상도 결정
   pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 15.0f );

   pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
   tmp_ipcam_list.push_back( pIPC );

   //---------------------------------------------------------------------------------------
   // Vision Hitech
   pIPC                   = new IPCameraProperty;
   pIPC->m_nCompanyID     = CompanyID_Visionhitech;
   pIPC->m_nDefPortNo     = 554;
   pIPC->m_nDefPTZPortNo  = 80;
   pIPC->m_strDefUserName = "admin";
   pIPC->m_strDefPassword = "1234";
   pIPC->m_nCapabilities  = IPCamCaps_PTZControl | IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_GetCompressionLevelNotSupported | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_NeedExtraPTZPortNo | IPCamCaps_ReceiveVideoStreamByRTSP | IPCamCaps_SupportMultiStream;

   pIPC->m_nPTZControlType = PTZControlType_Visionhitech_BM2TI_IR;
   pIPC->m_nIPCameraType   = IPCameraType_Visionhitech;
   pIPC->m_strProdName     = "Vision Hitech";
   pIPC->m_nVideoPortNum   = 4;
   pIPC->m_nColorSpace     = ColorSpace_YUY2;
   pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );
   pIPC->InitDigitalIO( 8, 2 );

   // 해상도 설정 없음 - 접속 시 해상도 결정
   pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 15.0f );

   pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
   tmp_ipcam_list.push_back( pIPC );

#ifdef __SUPPORT_ONVIF
   // Support Onvif
   //-----------------------------------------------------------------------------
   // Onvif
   pIPC                    = new IPCameraProperty;
   pIPC->m_nDefPTZPortNo   = 0;
   pIPC->m_nCompanyID      = CompanyID_Onvif;
   pIPC->m_nDefPortNo      = 554;
   pIPC->m_nDefPTZPortNo   = 80;
   pIPC->m_strDefUserName  = "";
   pIPC->m_strDefPassword  = "";
   pIPC->m_nCapabilities   = IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_GetCompressionLevelNotSupported | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_ReceiveVideoStreamByRTSP | IPCamCaps_PTZControl | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_ShowSameManufacturer | IPCamCaps_SupportMultiStream;
   pIPC->m_nPTZControlType = PTZControlType_Onvif;
   pIPC->m_nIPCameraType   = IPCameraType_Onvif;
   pIPC->m_strProdName     = "Onvif";
   pIPC->m_nVideoPortNum   = 1;
   pIPC->m_nColorSpace     = ColorSpace_YUY2;
   pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );

   //pIPC->m_ResolutionListMgr.AddResolutionList (&g_GlobalResolutionList);
   pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 15.0f );

   //pIPC->m_VideoFormatList.push_back (VCODEC_ID_MPEG4);
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
   //pIPC->m_VideoFormatList.push_back (VCODEC_ID_MJPEG);
   tmp_ipcam_list.push_back( pIPC );
#endif
   // (mkjang-140410)
   //---------------------------------------------------------------------------------------
   // Zenotech
   pIPC                    = new IPCameraProperty;
   pIPC->m_nCompanyID      = CompanyID_Zenotech;
   pIPC->m_nDefPortNo      = 554;
   pIPC->m_nDefPTZPortNo   = 80;
   pIPC->m_strDefUserName  = "admin";
   pIPC->m_strDefPassword  = "12345";
   pIPC->m_nCapabilities   = IPCamCaps_PTZControl | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_GetCompressionLevelNotSupported | IPCamCaps_SupportMultiStream | IPCamCaps_ReceiveVideoStreamByRTSP;
   pIPC->m_nPTZControlType = PTZControlType_Zenotech_MEGA_IPCAM;
   pIPC->m_nIPCameraType   = IPCameraType_Zenotech;
   pIPC->m_strProdName     = "Zenotech";
   pIPC->m_nVideoPortNum   = 4;
   pIPC->m_nColorSpace     = ColorSpace_YUY2;

   // 해상도 설정 없음 - 접속 시 해상도 결정
   pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 30.0f );

   pIPC->m_VideoFormatList.push_back( VCODEC_ID_MPEG4 );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_MJPEG );

   tmp_ipcam_list.push_back( pIPC );

   // (mkjang-140527)
   //-----------------------------------------------------------------------------
   // LG
   pIPC                    = new IPCameraProperty;
   pIPC->m_nCompanyID      = CompanyID_LG;
   pIPC->m_nDefPortNo      = 554;
   pIPC->m_nDefPTZPortNo   = 80;
   pIPC->m_strDefUserName  = "admin";
   pIPC->m_strDefPassword  = "admin";
   pIPC->m_nCapabilities   = IPCamCaps_PTZControl | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_GetCompressionLevelNotSupported | IPCamCaps_ShowSameManufacturer | IPCamCaps_ReceiveVideoStreamByRTSP | IPCamCaps_SupportMultiStream | IPCamCaps_SupportSameSubModel | IPCamCaps_ShowSameManufacturer | IPCamCaps_PTZPortIsWebPort;
   pIPC->m_nPTZControlType = PTZControlType_LG_LND_7210R;
   pIPC->m_nIPCameraType   = IPCameraType_LG;
   pIPC->m_strProdName     = "LG";
   pIPC->m_nVideoPortNum   = 4;
   pIPC->m_nColorSpace     = ColorSpace_YUY2;

   // 해상도 설정 없음 - 접속 시 해상도 결정
   pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 30.0f );

   pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_MJPEG );

   tmp_ipcam_list.push_back( pIPC );

#ifdef __SUPPORT_HUAWEI
   // HuaWei
   if( IS_SUPPORT( __SUPPORT_HUAWEI ) ) {
      pIPC                    = new IPCameraProperty;
      pIPC->m_strDefUserName  = "admin";
      pIPC->m_strDefPassword  = "HuaWei123";
      pIPC->m_nDefPortNo      = 554;
      pIPC->m_nDefPTZPortNo   = 6060;
      pIPC->m_nCompanyID      = CompanyID_HuaWei;
      pIPC->m_nCapabilities   = IPCamCaps_PTZControl | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_DisableVideoProperties | IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_GetCompressionLevelNotSupported | IPCamCaps_ShowSameManufacturer | IPCamCaps_SupportSameSubModel | IPCamCaps_ReceiveVideoStreamByRTSP | IPCamCaps_SurpportDirectVideoStreaming;
      pIPC->m_nPTZControlType = PTZControlType_HuaWei_IPC6621_Z30_I;
      pIPC->m_nIPCameraType   = IPCameraType_HuaWei;
      pIPC->m_strProdName     = "HuaWei";
      pIPC->m_nVideoPortNum   = 4;
      pIPC->m_nColorSpace     = ColorSpace_YUY2;

      // 해상도 설정 없음 - 접속 시 해상도 결정
      pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 15.0f );
      pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
      pIPC->m_VideoFormatList.push_back( VCODEC_ID_MJPEG );
      tmp_ipcam_list.push_back( pIPC );
   }
#endif

   // Vivotek
   pIPC                    = new IPCameraProperty;
   pIPC->m_nCompanyID      = CompanyID_Vivotek;
   pIPC->m_nDefPortNo      = 554;
   pIPC->m_nDefPTZPortNo   = 80;
   pIPC->m_nCapabilities   = IPCamCaps_PTZControl | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_GetCompressionLevelNotSupported | IPCamCaps_ReceiveVideoStreamByRTSP | IPCamCaps_SupportMultiStream | IPCamCaps_SupportSameSubModel | IPCamCaps_PTZPortIsWebPort;
   pIPC->m_nPTZControlType = PTZControlType_Vivotek_SD8363E;
   pIPC->m_nIPCameraType   = IPCameraType_Vivotek;
   pIPC->m_strProdName     = "Vivotek";
   pIPC->m_nVideoPortNum   = 4;
   pIPC->m_nColorSpace     = ColorSpace_YUY2;

   // 해상도 설정 없음 - 접속 시 해상도 결정
   pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 30.0f );

   pIPC->m_VideoFormatList.push_back( VCODEC_ID_MPEG4 );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_MJPEG );

   tmp_ipcam_list.push_back( pIPC );

   // Seyeon Tech
   pIPC                    = new IPCameraProperty;
   pIPC->m_nCompanyID      = CompanyID_FlexWatch;
   pIPC->m_nDefPortNo      = 554;
   pIPC->m_nDefPTZPortNo   = 80;
   pIPC->m_strDefUserName  = "root";
   pIPC->m_strDefPassword  = "root";
   pIPC->m_nCapabilities   = IPCamCaps_PTZControl | IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_GetCompressionLevelNotSupported | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_NeedExtraPTZPortNo | IPCamCaps_ReceiveVideoStreamByRTSP | IPCamCaps_SupportMotionDetection | IPCamCaps_SupportCustomPTZCommand | IPCamCaps_PTZPortIsWebPort;
   pIPC->m_nPTZControlType = PTZControlType_FlexWatch_FW1177_DEF;
   pIPC->m_nIPCameraType   = IPCameraType_FlexWatch_FW1177_DE;
   pIPC->m_strProdName     = "Seyeon Tech";
   pIPC->m_nVideoPortNum   = 4;
   pIPC->m_nColorSpace     = ColorSpace_YUY2;
   pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );
   pIPC->InitDigitalIO( 8, 2 );

   // 해상도 설정 없음 - 접속 시 해상도 결정
   pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 15.0f );

   pIPC->m_VideoFormatList.push_back( VCODEC_ID_MPEG4 );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
   tmp_ipcam_list.push_back( pIPC );

   // Coax
   pIPC                   = new IPCameraProperty;
   pIPC->m_nCompanyID     = CompanyID_Coax;
   pIPC->m_nDefPortNo     = 554;
   pIPC->m_nDefPTZPortNo  = 80;
   pIPC->m_strDefUserName = "root";
   pIPC->m_strDefPassword = "root";
   pIPC->m_nCapabilities  = IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_GetCompressionLevelNotSupported | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_ReceiveVideoStreamByRTSP;
   pIPC->m_nIPCameraType  = IPCameraType_Coax;
   pIPC->m_strProdName    = "Coax";
   pIPC->m_nVideoPortNum  = 4;
   pIPC->m_nColorSpace    = ColorSpace_YUY2;
   pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );
   pIPC->InitDigitalIO( 8, 2 );

   // 해상도 설정 없음 - 접속 시 해상도 결정
   pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 15.0f );

   pIPC->m_VideoFormatList.push_back( VCODEC_ID_MPEG4 );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
   tmp_ipcam_list.push_back( pIPC );

   // Doowon
   pIPC                    = new IPCameraProperty;
   pIPC->m_nCompanyID      = CompanyID_DooWon;
   pIPC->m_nDefPortNo      = 554;
   pIPC->m_nDefPTZPortNo   = 80;
   pIPC->m_strDefUserName  = "admin";
   pIPC->m_strDefPassword  = "admin";
   pIPC->m_nCapabilities   = IPCamCaps_PTZControl | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_GetCompressionLevelNotSupported | IPCamCaps_ShowSameManufacturer | IPCamCaps_ReceiveVideoStreamByRTSP | IPCamCaps_SupportMultiStream | IPCamCaps_SupportSameSubModel;
   pIPC->m_nPTZControlType = PTZControlType_Doowon_DMH_5001IR;
   pIPC->m_nIPCameraType   = IPCameraType_DooWon;
   pIPC->m_strProdName     = "DOOWON";
   pIPC->m_nVideoPortNum   = 4;
   pIPC->m_nColorSpace     = ColorSpace_YUY2;
   pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );
   pIPC->InitDigitalIO( 8, 2 );

   // 해상도 설정 없음 - 접속 시 해상도 결정
   pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 15.0f );

   pIPC->m_VideoFormatList.push_back( VCODEC_ID_MPEG4 );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
   tmp_ipcam_list.push_back( pIPC );

   //////////////////////////////////////////////////////////////////////////
   // (mkjang-150421)
   // EOC
   pIPC                    = new IPCameraProperty;
   pIPC->m_nCompanyID      = CompanyID_EOC;
   pIPC->m_nDefPortNo      = 554;
   pIPC->m_nDefPTZPortNo   = 80;
   pIPC->m_strDefUserName  = "admin";
   pIPC->m_strDefPassword  = "1111";
   pIPC->m_nCapabilities   = IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_GetCompressionLevelNotSupported | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_ReceiveVideoStreamByRTSP;
   pIPC->m_nPTZControlType = 0;
   pIPC->m_nIPCameraType   = IPCameraType_EOC;
   pIPC->m_strProdName     = "EOC";
   pIPC->m_nVideoPortNum   = 2;
   pIPC->m_nColorSpace     = ColorSpace_YUY2;
   pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );
   pIPC->InitDigitalIO( 8, 2 );

   // 해상도 설정 없음 - 접속 시 해상도 결정
   pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 15.0f );

   pIPC->m_VideoFormatList.push_back( VCODEC_ID_MPEG4 );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
   tmp_ipcam_list.push_back( pIPC );

//////////////////////////////////////////////////////////////////////////
// (qch1004-150602)
// Dallmeier
#ifdef __SUPPORT_DALLMEIER
   pIPC                    = new IPCameraProperty;
   pIPC->m_nCompanyID      = CompanyID_Dallmeier;
   pIPC->m_nDefPortNo      = 554;
   pIPC->m_strDefUserName  = "admin";
   pIPC->m_strDefPassword  = "3";
   pIPC->m_nCapabilities   = IPCamCaps_PTZControl | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_ShowSameManufacturer | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_GetCompressionLevelNotSupported | IPCamCaps_ReceiveVideoStreamByRTSP;
   pIPC->m_nPTZControlType = PTZControlType_Dallmeier_DDZ4020_YY_HS_HD;
   pIPC->m_nIPCameraType   = IPCameraType_Dallmeier;
   pIPC->m_strProdName     = "Dallmeier";
   pIPC->m_nVideoPortNum   = 4;
   pIPC->m_nColorSpace     = ColorSpace_YUY2;
   pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );
   pIPC->InitDigitalIO( 8, 2 );

   // 해상도 설정 없음 - 접속 시 해상도 결정
   pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 15.0f );
   pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
   tmp_ipcam_list.push_back( pIPC );
#endif

   if( IS_SUPPORT( __SUPPORT_HONEYWELL ) ) {
      //////////////////////////////////////////////////////////////////////////
      // Honeywell
      pIPC                   = new IPCameraProperty;
      pIPC->m_nCompanyID     = CompanyID_Honeywell;
      pIPC->m_nDefPortNo     = 554;
      pIPC->m_nDefPTZPortNo  = 80;
      pIPC->m_strDefUserName = "admin";
      pIPC->m_strDefPassword = "admin";
      pIPC->m_nCapabilities  = IPCamCaps_PTZControl | IPCamCaps_SetupPTZContorlInfo | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_GetCompressionLevelNotSupported | IPCamCaps_NeedExtraPTZPortNo | IPCamCaps_SupportMultiStream |
          //IPCamCaps_ShowSameManufacturer|
          IPCamCaps_SupportSameSubModel | IPCamCaps_ReceiveVideoStreamByRTSP;
      pIPC->m_nPTZControlType = PTZControlType_Honeywell_HISD_2201WE_IR;
      pIPC->m_nIPCameraType   = IPCameraType_Honeywell;
      pIPC->m_strProdName     = "Honeywell";
      pIPC->m_nVideoPortNum   = 4;
      pIPC->m_nColorSpace     = ColorSpace_YUY2;
      pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );
      pIPC->InitDigitalIO( 8, 2 );

      // 해상도 설정 없음 - 접속 시 해상도 결정
      pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 15.0f );

      pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
      tmp_ipcam_list.push_back( pIPC );
   }

   //////////////////////////////////////////////////////////////////////////
   // Oncam Grandeye
   pIPC                    = new IPCameraProperty;
   pIPC->m_nCompanyID      = CompanyID_Oncam;
   pIPC->m_nDefPortNo      = 554;
   pIPC->m_nDefPTZPortNo   = 80;
   pIPC->m_strDefUserName  = "admin";
   pIPC->m_strDefPassword  = "admin";
   pIPC->m_nCapabilities   = IPCamCaps_SetCompressionLevelNotSupported | IPCamCaps_GetCompressionLevelNotSupported | IPCamCaps_SurpportDirectVideoStreaming | IPCamCaps_ReceiveVideoStreamByRTSP;
   pIPC->m_nPTZControlType = 0;
   pIPC->m_nIPCameraType   = IPCameraType_Oncam_Grandeye;
   pIPC->m_strProdName     = "Oncam Grandeye";
   pIPC->m_nVideoPortNum   = 4;
   pIPC->m_nColorSpace     = ColorSpace_YUY2;
   pIPC->m_ImageCompression.SetProperty( 1, 100, 1, 20 );
   pIPC->InitDigitalIO( 8, 2 );

   // 해상도 설정 없음 - 접속 시 해상도 결정
   pIPC->m_FrameRateListMgr.AddNewFrameRateList( VideoType_None, 1.0f, 30.0f, 1.0f, 15.0f );

   pIPC->m_VideoFormatList.push_back( VCODEC_ID_H264 );
   tmp_ipcam_list.push_back( pIPC );

   ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   if( IS_NOT_PRODUCT( __PRODUCT_B200 ) ) {
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_RTSP );
#ifdef __SUPPORT_ONVIF
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_Onvif ); // Support Onvif
#endif
      //MoveIPCameraProperty (g_IPCameraList, tmp_ipcam_list, IPCameraType_ACES); // (mkjang-140618)
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_AproMedia );
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_AproHttp ); // (mkjang140829)
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_AXIS );
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_BestDigital );
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_Bosch_VIPX1 );
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_Cellinx_MR904 );
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_CryptoTelecom_S152 );
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_Dahua );
#ifdef __SUPPORT_DALLMEIER
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_Dallmeier );
#endif
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_EOC ); // (mkjang-150421)
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_SamsungTechwin );
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_HIKVision );
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_HIKVision_RTSP );
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_Hitron );
      if( IS_SUPPORT( __SUPPORT_HONEYWELL ) ) {
         MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_Honeywell );
      }
#ifdef __SUPPORT_HUAWEI
      if( IS_SUPPORT( __SUPPORT_HUAWEI ) )
         MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_HuaWei ); // (mkjang-140721)
#endif
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_ICanServer );
      if( IS_SUPPORT( __SUPPORT_IDIS_CAMERA ) ) {
         MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_IDIS ); // (mkjang-150120)
      }
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_IVXVideoServer );
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_LG ); // (mkjang-140527)
      //MoveIPCameraProperty (g_IPCameraList, tmp_ipcam_list, IPCameraType_Linudix);
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_Oncam_Grandeye );
      // __SUPPORT_PANASONIC_CAMERA
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_Panasonic );

#ifdef __SUPPORT_PELCO_ENDURA
      if( IS_SUPPORT( __SUPPORT_PELCO_ENDURA ) )
         MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_Pelco_EnduraNET5400T ); // (xinu_bc25)
#endif
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_ProbeDigital );
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_FlexWatch_FW1177_DE ); // Seyeon Tech (mkjang-150211)
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_Coax );
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_DooWon );
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_Sony_IPELA );
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_Truen );
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_Visionhitech );
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_Vivotek ); // (mkjang140827)
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_Win4net );
#ifndef __SUPPORT_YOUNGKOOK
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_YoungKook );
#endif
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_Zenotech ); // (mkjang-140410)
   } else {
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_FlexWatch );
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_FlexWatch_FW1177_DE );
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_Coax );
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_DooWon );
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_UDP );
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_UDP_CND_20Z_HO );
   }

   // [VMS]
   if( IS_SUPPORT( __SUPPORT_INNODEP ) )
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_Innodep );
   if( IS_SUPPORT( __SUPPORT_OMNICAST ) )
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_Omnicast );
   if( IS_SUPPORT( __SUPPORT_REALHUB ) )
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_RealHub );
   if( IS_SUPPORT( __SUPPORT_SECURITYCENTER ) )
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_SecurityCenter );
   if( IS_SUPPORT( __SUPPORT_XPROTECT ) )
      MoveIPCameraProperty( g_IPCameraList, tmp_ipcam_list, IPCameraType_XProtect );
}

void InitPTZControlProducts()
{
   PTZControlProperty* pPCP;
   ////////////   Default   //////////////
   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = 0;
   pPCP->m_nCapabilities   = 0;
   g_PTZControlList.push_back( pPCP );

   ////////////   Virtual PTZ Camera   //////////////
   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_VirtualPTZ;
   pPCP->m_nCapabilities   = 0;
   pPCP->m_nCapabilities |= 0;
   //PTZCtrlCap_PTZPosInitSupported |
   //PTZCtrlCap_PTZWiperSupported |
   //PTZCtrlCap_PTZHeatingWireSupported |
   //PTZCtrlCap_PTZThermalCamSupported |
   //PTZCtrlCap_PTZDayAndNightModeSupported |
   //PTZCtrlCap_PTZDefogSupported |
   //PTZCtrlCap_PTZVideoStabilizationSupported;

   g_PTZControlList.push_back( pPCP );

   ////////////   Common Protocol   //////////////
   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Pelco_D;
   pPCP->m_nCapabilities |= PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_RealTimeGetAbsPosNotSupported | PTZCtrlCap_PanTiltCoordinateChangeable;
   g_PTZControlList.push_back( pPCP );

   ////////////   CYNIX   //////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_CYNIX_CU_N22DC;
   pPCP->m_nCapabilities |= ( PTZCtrlCap_RealTimeGetAbsPosNotSupported );
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_CYNIX_CU_N23DH;
   pPCP->m_nCapabilities |= ( PTZCtrlCap_RealTimeGetAbsPosNotSupported );
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_CYNIX_CU_N26DC;
   pPCP->m_nCapabilities |= PTZCtrlCap_RealTimeGetAbsPosNotSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_CYNIX_CV_N22C;
   pPCP->m_nCapabilities |= PTZCtrlCap_RealTimeGetAbsPosNotSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_CYNIX_EAI_N27T;
   pPCP->m_nCapabilities |= PTZCtrlCap_PTZPosInitSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_CYNIX_HA_18H_S;
   // 절대위치이동 명령 이후에 연속이동 명령을 내리면 제어에 문제가 있어서
   // PTZCtrlCap_FastAbsPosMoveNotSupported 플레그를 추가함
   pPCP->m_nCapabilities |= PTZCtrlCap_PTZPosInitSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_CYNIX_HC_N20S;
   pPCP->m_nCapabilities |= 0;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_CYNIX_HC_X18H;
   pPCP->m_nCapabilities |= PTZCtrlCap_PTZPosInitSupported;
   g_PTZControlList.push_back( pPCP );

   // CYNIX STH790 MM-302(mkjang-140401)
   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_CYNIX_MM_302;
   pPCP->m_nCapabilities |= PTZCtrlCap_IRLigtControlSupported | PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_ZoomProportionalJog | PTZCtrlCap_PTZPosInitSupported;
   g_PTZControlList.push_back( pPCP );

   // CYNIX STH790 MM-202(mkjang-140408)
   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_CYNIX_MM_202;
   pPCP->m_nCapabilities |= PTZCtrlCap_IRLigtControlSupported | PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_ZoomProportionalJog | PTZCtrlCap_PTZPosInitSupported;
   g_PTZControlList.push_back( pPCP );

   ////////////   SAMSUNG   //////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Samsung_C6435;
   pPCP->m_nCapabilities   = 0;
   g_PTZControlList.push_back( pPCP );

   ////////////   AXIS   //////////////
   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Axis_Q6045_Mk_II;
   pPCP->m_nCapabilities |= PTZCtrlCap_RealTimeGetAbsPosNotSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Axis_214;
   pPCP->m_nCapabilities   = PTZCtrlCap_RealTimeGetAbsPosNotSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Axis_212;
   pPCP->m_nCapabilities   = PTZCtrlCap_RealTimeGetAbsPosNotSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Axis_213;
   pPCP->m_nCapabilities   = PTZCtrlCap_RealTimeGetAbsPosNotSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Axis_233D;
   pPCP->m_nCapabilities   = PTZCtrlCap_RealTimeGetAbsPosNotSupported;
   g_PTZControlList.push_back( pPCP );

   ////////////   CRYPTO TELECAOM   //////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_CryptoTelecom;
   pPCP->m_nCapabilities |= PTZCtrlCap_GetAbsPosNotSupported | PTZCtrlCap_RealTimeGetAbsPosNotSupported | PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_ConnectionDependOnStream;
   g_PTZControlList.push_back( pPCP );

   ////////////   LG   //////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_LG_LPT_EP551PS;
   pPCP->m_nCapabilities |= PTZCtrlOpt_AllAbsPosNotSupported | PTZCtrlCap_SinglePTZTrackNotSupported;
   g_PTZControlList.push_back( pPCP );

   // (mkjang-140527)
   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_LG_LND_7210R;
   pPCP->m_nCapabilities |= PTZCtrlCap_GetAbsPosNotSupported | PTZCtrlCap_SinglePTZTrackNotSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_LG_LNP_3022;
   pPCP->m_nCapabilities |= PTZCtrlCap_ZoomProportionalJog;
   g_PTZControlList.push_back( pPCP );

   if( IS_SUPPORT( __SUPPORT_HONEYWELL ) ) {
      ////////////   HONEYWELL   //////////////
      pPCP                    = new PTZControlProperty;
      pPCP->m_nPTZControlType = PTZControlType_Honeywell_HSDC_351;
      pPCP->m_nCapabilities   = PTZCtrlCap_DoesNotReleasePropPanTiltSpeedOpt;
      g_PTZControlList.push_back( pPCP );

      pPCP                    = new PTZControlProperty;
      pPCP->m_nPTZControlType = PTZControlType_Honeywell_HISD_2201WE_IR;
      pPCP->m_nCapabilities   = 0 | PTZCtrlCap_IRLigtControlSupported;
      g_PTZControlList.push_back( pPCP );

      pPCP                    = new PTZControlProperty;
      pPCP->m_nPTZControlType = PTZControlType_Honeywell_HNP_232WI;
      pPCP->m_nCapabilities   = PTZCtrlCap_ZoomProportionalJog;
      g_PTZControlList.push_back( pPCP );
   }
   ////////////   BOSCH   //////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_BOSCH_AUTODOME_500I;
   pPCP->m_nCapabilities |= PTZCtrlCap_RealTimeGetAbsPosNotSupported | PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_ConnectionDependOnStream;
   g_PTZControlList.push_back( pPCP );

   ////////////   HITRON   //////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Hitron_HF3S36AN;
   pPCP->m_nCapabilities   = PTZCtrlCap_ConnectionDependOnStream | PTZCtrlCap_ConnectionDependOnStream;
   g_PTZControlList.push_back( pPCP );

   ////////////   HITRON IP  //////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Hitron_NFX_12053B1;
   pPCP->m_nCapabilities   = PTZCtrlOpt_AllAbsPosNotSupported | PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_NeedExtraPTZPortNo | PTZCtrlCap_GotoPresetByPTZPreset;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Hitron_NFX_22053H3;
   pPCP->m_nCapabilities   = PTZCtrlCap_SinglePTZTrackNotSupported;
   g_PTZControlList.push_back( pPCP );
   ////////////   YUJIN   //////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Yujin_ETP5000S_33x;
   pPCP->m_nCapabilities |= PTZCtrlCap_AutoFocusingByPTZCtrl | PTZCtrlCap_RealTimeGetAbsPosNotSupported | PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_PTZGetSetFocusAbs;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Yujin_ETP5000S_22x;
   pPCP->m_nCapabilities |= PTZCtrlCap_AutoFocusingByPTZCtrl | PTZCtrlCap_RealTimeGetAbsPosNotSupported | PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_PTZGetSetFocusAbs;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Yujin_ETP5000S_Pelco_D;
   pPCP->m_nCapabilities |= PTZCtrlCap_AutoFocusingByPTZCtrl | PTZCtrlCap_RealTimeGetAbsPosNotSupported | PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_PTZGetSetFocusAbs;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Yujin_ETP7000B;
   pPCP->m_nCapabilities |= 0;
   g_PTZControlList.push_back( pPCP );

   ////////////   Youtech   //////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Youtech_7000WB_1000;
   pPCP->m_nCapabilities   = PTZCtrlCap_PTZWiperSupported | PTZCtrlCap_PTZDayAndNightModeSupported | PTZCtrlCap_PTZHeatingWireSupported;
   //PTZCtrlCap_PTZGetSetFocusAbs;
   pPCP->m_fPredictionFactor_MSTrack = 0.6f;
   g_PTZControlList.push_back( pPCP );

   pPCP                              = new PTZControlProperty;
   pPCP->m_nPTZControlType           = PTZControlType_Youtech_7000WB_750;
   pPCP->m_nCapabilities             = PTZCtrlCap_PTZWiperSupported | PTZCtrlCap_PTZDayAndNightModeSupported | PTZCtrlCap_PTZHeatingWireSupported | PTZCtrlCap_PTZGetSetFocusAbs;
   pPCP->m_fPredictionFactor_MSTrack = 0.6f;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Youtech_9000MR_750;
   pPCP->m_nCapabilities   = PTZCtrlCap_PTZWiperSupported | PTZCtrlCap_PTZDayAndNightModeSupported | PTZCtrlCap_PTZHeatingWireSupported | PTZCtrlCap_PTZGetSetFocusAbs;

   pPCP->m_fPredictionFactor_MSTrack = 0.6f;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_SamsungTechWin_iPOLiS_SNB_7004_Shinwoo_SPT_7080B;
   pPCP->m_nCapabilities   = PTZCtrlCap_PTZWiperSupported | PTZCtrlCap_PTZDayAndNightModeSupported | PTZCtrlCap_PTZGetSetFocusAbs | PTZCtrlCap_PresetMoveByUserSpecifiedSpeedSupported;

   pPCP->m_fPredictionFactor_MSTrack = 0.6f;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_SamsungTechWin_iPOLiS_SNB_6004_Shinwoo_SPT_7080B;
   pPCP->m_nCapabilities   = PTZCtrlCap_AutoFocusOnOff | PTZCtrlCap_PTZOneShotAutoFocusSupported | PTZCtrlCap_PTZWiperSupported | PTZCtrlCap_PTZDayAndNightModeSupported | PTZCtrlCap_ZoomTriggeredAutoFocus | PTZCtrlCap_PTZGetSetFocusAbs | PTZCtrlCap_PresetMoveByUserSpecifiedSpeedSupported;

   pPCP->m_fPredictionFactor_MSTrack = 0.6f;
   g_PTZControlList.push_back( pPCP );

   pPCP                               = new PTZControlProperty;
   pPCP->m_nPTZControlType            = PTZControlType_SamsungTechWin_iPOLiS_SNB_6004_Shinwoo_SPT_8080BE;
   pPCP->m_nCapabilities              = PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_PTZWiperSupported | PTZCtrlCap_PTZThermalCamSupported | PTZCtrlCap_MultiZoomModule | PTZCtrlCap_PTZGetSetFocusAbs | PTZCtrlCap_PresetMoveByUserSpecifiedSpeedSupported;
   pPCP->m_fThermalCameraZoomStepSize = 1.0f;
   pPCP->m_fPredictionFactor_MSTrack  = 0.6f;
   g_PTZControlList.push_back( pPCP );

   pPCP                               = new PTZControlProperty;
   pPCP->m_nPTZControlType            = PTZControlType_SamsungTechWin_iPOLiS_SNB_6005_Shinwoo_SPT_8080BE;
   pPCP->m_nCapabilities              = PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_PTZWiperSupported | PTZCtrlCap_PTZThermalCamSupported | PTZCtrlCap_MultiZoomModule | PTZCtrlCap_PTZDefogSupported | PTZCtrlCap_PTZOneShotAutoFocusSupported | PTZCtrlCap_PTZGetSetFocusAbs | PTZCtrlCap_PresetMoveByUserSpecifiedSpeedSupported;
   pPCP->m_fThermalCameraZoomStepSize = 1.0f;
   pPCP->m_fPredictionFactor_MSTrack  = 0.6f;
   pPCP->m_nMaxDefogLevel             = 10;
   g_PTZControlList.push_back( pPCP );

   pPCP                               = new PTZControlProperty;
   pPCP->m_nPTZControlType            = PTZControlType_SamsungTechWin_iPOLiS_SNZ_6320_Shinwoo_SPT_8080BE;
   pPCP->m_nCapabilities              = PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_PTZWiperSupported | PTZCtrlCap_PTZThermalCamSupported | PTZCtrlCap_MultiZoomModule | PTZCtrlCap_PTZGetSetFocusAbs | PTZCtrlCap_IRLigtControlSupported | PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_PresetMoveByUserSpecifiedSpeedSupported;
   pPCP->m_fThermalCameraZoomStepSize = 1.0f;
   pPCP->m_fPredictionFactor_MSTrack  = 0.6f;
   g_PTZControlList.push_back( pPCP );
   ////////////   ICanTek   //////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_ICanTek;
   pPCP->m_nCapabilities |= PTZCtrlCap_RealTimeGetAbsPosNotSupported | PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_ConnectionDependOnStream;
   g_PTZControlList.push_back( pPCP );

   ////////////   APROMEDIA   //////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_AproMedia;
   pPCP->m_nCapabilities |= PTZCtrlCap_GetAbsPosNotSupported | PTZCtrlCap_RealTimeGetAbsPosNotSupported | PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_GotoPresetByPTZPreset | PTZCtrlCap_ConnectionDependOnStream;
   g_PTZControlList.push_back( pPCP );

   ////////////   APRO HTTP   //////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_AproHttp;
   pPCP->m_nCapabilities |= PTZCtrlCap_GetAbsPosNotSupported | PTZCtrlCap_RealTimeGetAbsPosNotSupported | PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_GotoPresetByPTZPreset | PTZCtrlCap_ConnectionDependOnStream;
   g_PTZControlList.push_back( pPCP );

   // __SUPPORT_NICO_CAMERA
   ////////////   NIKO   //////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_NIKO_NSD_S300;
   pPCP->m_nCapabilities |= 0;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_NIKO_NSD_S360;
   pPCP->m_nCapabilities   = 0;
   g_PTZControlList.push_back( pPCP );

   ////////////   SAMSUNG TECHWIN   //////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_SamsungTechWin_SPD_3750;
   // 삼성카메라에 FastAbsPosMoveNotSupported 옵션을 추가한것은 연속이동방식추적시 연속이동도중
   // 빠르게 절대각으로 이동하는 경우 카메라의 절대각이 틀어지는 현상때문이다.
   pPCP->m_nCapabilities |= PTZCtrlCap_ContPTAndAbsZoomMoveCam | PTZCtrlCap_FastAbsPosMoveNotSupported | PTZCtrlCap_PanTiltCoordinateChangeable;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_SamsungTechWin_SPD_3700;
   // 삼성카메라에 FastAbsPosMoveNotSupported 옵션을 추가한것은 연속이동방식추적시 연속이동도중
   // 빠르게 절대각으로 이동하는 경우 카메라의 절대각이 틀어지는 현상때문이다.
   pPCP->m_nCapabilities |= PTZCtrlCap_ContPTAndAbsZoomMoveCam | PTZCtrlCap_FastAbsPosMoveNotSupported | PTZCtrlCap_PanTiltCoordinateChangeable;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_SamsungTechWin_SCU_2370;
   // 삼성카메라에 FastAbsPosMoveNotSupported 옵션을 추가한것은 연속이동방식추적시 연속이동도중
   // 빠르게 절대각으로 이동하는 경우 카메라의 절대각이 틀어지는 현상때문이다.
   pPCP->m_nCapabilities |= PTZCtrlCap_ContPTAndAbsZoomMoveCam | PTZCtrlCap_FastAbsPosMoveNotSupported | PTZCtrlCap_DelayedABSPosMoveCam | PTZCtrlCap_PanTiltCoordinateChangeable;

   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_SamsungTechWin_SCP_2370;
   // 연속이동에의한 절대위치이동기능에서 큰각을 절대위치 이동방식으로 이동해야하는 경우
   // Pan,Tilt 이동만 하는 방식으로 변경되어 Pan, Tilt 틀어짐 현상이 없어졌을 것이라 예상하고
   // PTZCtrlCap_FastAbsPosMoveNotSupported 옵션을 제거하였음.
   pPCP->m_nCapabilities |= PTZCtrlCap_DelayedABSPosMoveCam;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_SamsungTechWin_SCP_2370RH;
   pPCP->m_nCapabilities |= PTZCtrlCap_DelayedABSPosMoveCam | PTZCtrlCap_PanTiltCoordinateChangeable;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_SamsungTechWin_SCP_2373N;
   pPCP->m_nCapabilities |= PTZCtrlCap_DelayedABSPosMoveCam | PTZCtrlCap_PanTiltCoordinateChangeable;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_SamsungTechWin_SCP_2371N;
   pPCP->m_nCapabilities |= PTZCtrlCap_DelayedABSPosMoveCam | PTZCtrlCap_PanTiltCoordinateChangeable;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_SamsungTechWin_SCP_2371N;
   pPCP->m_nCapabilities |= PTZCtrlCap_DelayedABSPosMoveCam | PTZCtrlCap_PanTiltCoordinateChangeable;
   g_PTZControlList.push_back( pPCP );

   ////////////   TRUEN   //////////////

   // 중요: 트루엔 카메라의 경우 연결 방식이 스트림의 연결객체 참조 방식과 별도의 연결객체를 생성하는 방식이 존재하기 때문에
   //       PTZCtrlCap_ConnectionDependOnStream 옵션을 설정하지 않았다.
   //       대신 CPTZCtrl_Truen::Connect에서 각각의 연결방식마다 연결여부를 체크하도록 처리하였다.

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Truen;
   pPCP->m_nCapabilities |= 0;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Truen_TCAM_570_S22FIR;
   pPCP->m_nCapabilities |= PTZCtrlCap_PTZPosInitSupported;
   g_PTZControlList.push_back( pPCP );

   //pPCP = new PTZControlProperty;
   //pPCP->m_nPTZControlType = PTZControlType_Truen_TN_B220CS_Convex_CVX_5000;
   //pPCP->m_nCapabilities |=
   //   PTZCtrlCap_SinglePTZTrackNotSupported |
   //   PTZCtrlCap_PTZPosInitSupported |
   //   PTZCtrlCap_PTZWiperSupported |
   //   PTZCtrlCap_PTZHeatingWireSupported |
   //   PTZCtrlCap_NeedExtraPTZPortNo |
   //   PTZCtrlCap_GotoPresetByPTZPreset |       // 트루엔 줌 모듈의 프리셋 기능이 구현될 때 까지 대기..
   //   PTZCtrlCap_IRLigtControlSupported |
   //   PTZCtrlCap_IRLightAreaSupported |
   //   PTZCtrlCap_PTZDefogSupported |
   //   PTZCtrlCap_AutoFocusOnOff;
   //pPCP->m_nMaxDefogLevel = 3;
   //g_PTZControlList.push_back (pPCP);

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Truen_TCAM_PX220CS;
   pPCP->m_nCapabilities |= PTZCtrlCap_PTZPosInitSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Truen_TN_PX220CT;
   pPCP->m_nCapabilities |= PTZCtrlCap_PTZPosInitSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Truen_TN_P4220CTIR;
   pPCP->m_nCapabilities |= PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_IRLigtControlSupported | PTZCtrlCap_IRLightAreaSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Truen_TN_P5230CWIR;
   pPCP->m_nCapabilities |= PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_IRLigtControlSupported | PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_PTZPosInitSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Truen_TN_B220CS_Shinwoo_SPT_7080B;
   pPCP->m_nCapabilities |= PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_PTZWiperSupported | PTZCtrlCap_PTZHeatingWireSupported | PTZCtrlCap_PTZDefogSupported |
       //PTZCtrlCap_SinglePTZTrackNotSupported |
       PTZCtrlCap_IRLigtControlSupported | PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_AutoFocusOnOff | PTZCtrlCap_NeedExtraPTZPortNo | PTZCtrlCap_DoNotOpenSetupWebPage | PTZCtrlCap_DoNotPeridicallyReqAbsPos | PTZCtrlCap_PresetMoveByUserSpecifiedSpeedSupported;
   g_PTZControlList.push_back( pPCP );

   ////////////   CELLINX   //////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Cellinx;
   pPCP->m_nCapabilities |= PTZCtrlCap_ConnectionDependOnStream;
   g_PTZControlList.push_back( pPCP );

   /////////// Bosch VIPX1 ////////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Bosch_VIPX1;
   pPCP->m_nCapabilities |= PTZCtrlCap_GetAbsPosNotSupported | PTZCtrlCap_RealTimeGetAbsPosNotSupported;
   g_PTZControlList.push_back( pPCP );

   /////////// Pelco Endura NET5400T //////////////// (xinu_bc15)

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_PelcoEndura;
   pPCP->m_nCapabilities |= PTZCtrlCap_SetAbsPosNotSupported | PTZCtrlCap_GetAbsPosNotSupported | PTZCtrlCap_RealTimeGetAbsPosNotSupported;
   g_PTZControlList.push_back( pPCP );

   // (mkjang-140626)
   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlTYpe_Pelco_D5230;
   pPCP->m_nCapabilities |= PTZCtrlCap_ContPTAndAbsZoomMoveCam;
   g_PTZControlList.push_back( pPCP );

   // (qch1004-150605)
   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlTYpe_Pelco_S6220;
   pPCP->m_nCapabilities |= PTZCtrlCap_ContPTAndAbsZoomMoveCam;
   g_PTZControlList.push_back( pPCP );

   ////////////   WONWOO   //////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_WonWooEng_EWSJ_E360;
   pPCP->m_nCapabilities |= PTZCtrlCap_FastAbsPosMoveNotSupported | PTZCtrlCap_BadAbsPosMoveMoveCam | PTZCtrlCap_PTZPosInitSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_WonWooEng_EWSJ_363;
   pPCP->m_nCapabilities |= PTZCtrlCap_FastAbsPosMoveNotSupported | PTZCtrlCap_BadAbsPosMoveMoveCam | PTZCtrlCap_PTZPosInitSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_WonWooEng_WCA_E361NR;
   pPCP->m_nCapabilities |= PTZCtrlCap_FastAbsPosMoveNotSupported | PTZCtrlCap_BadAbsPosMoveMoveCam | PTZCtrlCap_IRLigtControlSupported | // (mkjang-141211)
       PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_PTZPosInitSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_WonWooEng_WTK_E361;
   pPCP->m_nCapabilities |= PTZCtrlCap_FastAbsPosMoveNotSupported | PTZCtrlCap_AttachReqAbsPosSupported | PTZCtrlCap_BadAbsPosMoveMoveCam | PTZCtrlCap_IRLigtControlSupported | // (mkjang-141211)
       PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_PTZPosInitSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_WonWooEng_WTK_M202;
   pPCP->m_nCapabilities |= PTZCtrlCap_IRLigtControlSupported | PTZCtrlCap_FastAbsPosMoveNotSupported | PTZCtrlCap_AttachReqAbsPosSupported | PTZCtrlCap_BadAbsPosMoveMoveCam | PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_PTZPosInitSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_WonWooEng_WMK_M202;
   pPCP->m_nCapabilities |= PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_IRLigtControlSupported;
   //PTZCtrlCap_FastAbsPosMoveNotSupported |
   //PTZCtrlCap_BadAbsPosMoveMoveCam |
   //PTZCtrlCap_PTZPosInitSupported;
   g_PTZControlList.push_back( pPCP );

   // 청음공방
   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_WonWooEng_WMK_MM308;
   pPCP->m_nCapabilities |= PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_IRLigtControlSupported;
   //PTZCtrlCap_FastAbsPosMoveNotSupported |
   //PTZCtrlCap_BadAbsPosMoveMoveCam |
   //PTZCtrlCap_PTZPosInitSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_WonWooEng_WMK_HS308;
   pPCP->m_nCapabilities |= PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_IRLigtControlSupported;
   g_PTZControlList.push_back( pPCP );

   ////////////   IDIS   //////////////
   if( IS_SUPPORT( __SUPPORT_IDIS_CAMERA ) ) {
      pPCP                    = new PTZControlProperty;
      pPCP->m_nPTZControlType = PTZControlType_IDIS;
      pPCP->m_nCapabilities |= 0;
      g_PTZControlList.push_back( pPCP );

      // (mkjang-150120)
      pPCP                    = new PTZControlProperty;
      pPCP->m_nPTZControlType = PTZControlType_IDIS_MNC221SH;
      pPCP->m_nCapabilities |= 0;
      g_PTZControlList.push_back( pPCP );
   }
   ////////////   Xvas   //////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Xvas_XV_4120IRD;
   pPCP->m_nCapabilities |= PTZCtrlCap_IRLigtControlSupported | PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_FastAbsPosMoveNotSupported | PTZCtrlCap_BadAbsPosMoveMoveCam | PTZCtrlCap_RealTimeGetAbsPosNotSupported | PTZCtrlCap_GetAbsPosNotSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Xvas_XV_4120IRD_13Byte;
   pPCP->m_nCapabilities   = PTZCtrlCap_FastAbsPosMoveNotSupported | PTZCtrlCap_BadAbsPosMoveMoveCam | PTZCtrlCap_RealTimeGetAbsPosNotSupported | PTZCtrlCap_IRLigtControlSupported | PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_GetAbsPosNotSupported;
   g_PTZControlList.push_back( pPCP );

   // IP 카메라에서 PTZ 카메라를 지원하지 않음. ezTCP를 사용하기 위해 별도로 추가함.
   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = 0;
   pPCP->m_nCapabilities |= 0;
   g_PTZControlList.push_back( pPCP );

   ////////////   Sony   //////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_SonyIPELA_SNC_RH164_124;
   pPCP->m_nCapabilities   = PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_DoesNotReleasePropPanTiltSpeedOpt;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_SonyIPELA_SNC_ER580;
   pPCP->m_nCapabilities   = PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_DoesNotReleasePropPanTiltSpeedOpt;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_SonyIPELA_SNC_RX570;
   pPCP->m_nCapabilities   = PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_DoesNotReleasePropPanTiltSpeedOpt;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_SonyIPELA_SNC_WR630;
   pPCP->m_nCapabilities   = PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_DoesNotReleasePropPanTiltSpeedOpt;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_SonyIPELA_SNC_ER585;
   pPCP->m_nCapabilities   = PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_DoesNotReleasePropPanTiltSpeedOpt;
   g_PTZControlList.push_back( pPCP );

   ////////////   Innodep   //////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Innodep;
   pPCP->m_nCapabilities   = PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_SetAbsPosNotSupported | PTZCtrlCap_GetAbsPosNotSupported | PTZCtrlCap_RealTimeGetAbsPosNotSupported | PTZCtrlCap_ConnectionDependOnStream;
   g_PTZControlList.push_back( pPCP );

   ////////////   HIK Vision   //////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_HIKVision_NAIS_M138IR;
   pPCP->m_nCapabilities   = PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_AttachReqAbsPosSupported | PTZCtrlCap_PTZPosInitSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_HIKVision_NAIS_M138H;
   pPCP->m_nCapabilities   = PTZCtrlCap_DoesNotReleasePropPanTiltSpeedOpt | PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_AttachReqAbsPosSupported | PTZCtrlCap_PTZPosInitSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_HIKVision_NAIS_DS_2DF1;
   pPCP->m_nCapabilities   = PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_AttachReqAbsPosSupported | PTZCtrlCap_FastAbsPosMoveNotSupported | PTZCtrlCap_PTZPosInitSupported;
   g_PTZControlList.push_back( pPCP );

   // (mkjang-140623)
   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_HIKVision_NAIS_M200IR;
   pPCP->m_nCapabilities   = PTZCtrlCap_DoesNotReleasePropPanTiltSpeedOpt | PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_AttachReqAbsPosSupported | PTZCtrlCap_PTZPosInitSupported;
   g_PTZControlList.push_back( pPCP );

   // (mkjang-140819)
   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_HIKVision_DS_2DF7286_A;
   pPCP->m_nCapabilities   = PTZCtrlCap_DoesNotReleasePropPanTiltSpeedOpt | PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_IRLigtControlSupported | PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_PTZPosInitSupported;
   g_PTZControlList.push_back( pPCP );

   // (mkjang-140822)
   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_HIKVision_DS_2DZ2116;
   pPCP->m_nCapabilities   = PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_AttachReqAbsPosSupported | PTZCtrlCap_FastAbsPosMoveNotSupported | PTZCtrlCap_GotoPresetByPTZPreset | PTZCtrlCap_PTZPosInitSupported;
   g_PTZControlList.push_back( pPCP );

   // (mkjang-150717)
   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_HIKVision_DS_2DE7186_A;
   pPCP->m_nCapabilities   = PTZCtrlCap_DoesNotReleasePropPanTiltSpeedOpt | PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_IRLigtControlSupported | PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_PTZPosInitSupported;
   g_PTZControlList.push_back( pPCP );

   // (LEEJH - 160308)
   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_HIKVision_DS_2DF8236I_AEL;
   pPCP->m_nCapabilities   = PTZCtrlCap_IRLigtControlSupported | PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_PTZPosInitSupported;
   g_PTZControlList.push_back( pPCP );

   ////////////   Pro sys   //////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Prosys_PPT_350;
   pPCP->m_nCapabilities   = PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_PTZGetSetFocusAbs;
   g_PTZControlList.push_back( pPCP );

   ////////////   Omnicast   //////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Omnicast;
   pPCP->m_nCapabilities   = PTZCtrlCap_ConnectionDependOnStream | PTZCtrlCap_ConnectionDependOnStream;
   g_PTZControlList.push_back( pPCP );

   ////////////   RealHub   ////////////// TYLEE: 2013-09-10

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_RealHub;
   pPCP->m_nCapabilities   = 0;
   g_PTZControlList.push_back( pPCP );

   ////////////   Dahua   //////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Dahua_NS_SD200IR;
   pPCP->m_nCapabilities   = PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_AttachReqAbsPosSupported | PTZCtrlCap_DoesNotReleasePropPanTiltSpeedOpt | PTZCtrlCap_GotoPresetByPTZPreset;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Dahua_NS_SD203IR;
   pPCP->m_nCapabilities   = PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_AttachReqAbsPosSupported | PTZCtrlCap_DoesNotReleasePropPanTiltSpeedOpt | PTZCtrlCap_GotoPresetByPTZPreset;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Dahua_NS_SD220IR;
   pPCP->m_nCapabilities   = PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_AttachReqAbsPosSupported | PTZCtrlCap_DoesNotReleasePropPanTiltSpeedOpt | PTZCtrlCap_GotoPresetByPTZPreset;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Dahua_NS_SD230IR;
   pPCP->m_nCapabilities   = PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_AttachReqAbsPosSupported | PTZCtrlCap_DoesNotReleasePropPanTiltSpeedOpt | PTZCtrlCap_GotoPresetByPTZPreset;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Dahua_NS_SD230IRC;
   pPCP->m_nCapabilities   = PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_AttachReqAbsPosSupported | PTZCtrlCap_DoesNotReleasePropPanTiltSpeedOpt | PTZCtrlCap_GotoPresetByPTZPreset;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Dahua_NS_SD300IR;
   pPCP->m_nCapabilities   = PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_AttachReqAbsPosSupported | PTZCtrlCap_DoesNotReleasePropPanTiltSpeedOpt | PTZCtrlCap_GotoPresetByPTZPreset;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Dahua_NS_SD138;
   pPCP->m_nCapabilities   = PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_AttachReqAbsPosSupported | PTZCtrlCap_DoesNotReleasePropPanTiltSpeedOpt | PTZCtrlCap_GotoPresetByPTZPreset;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Dahua_KADJ_NM2030IRA;
   pPCP->m_nCapabilities   = PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_AttachReqAbsPosSupported | PTZCtrlCap_DoesNotReleasePropPanTiltSpeedOpt | PTZCtrlCap_PTZWiperSupported | PTZCtrlCap_GotoPresetByPTZPreset;
   g_PTZControlList.push_back( pPCP );

   ////////////   Win4net   //////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Win4net_CLEBO_PM10HT;
   pPCP->m_nCapabilities   = PTZCtrlCap_AttachReqAbsPosSupported | PTZCtrlCap_DoesNotReleasePropPanTiltSpeedOpt | PTZCtrlCap_RealTimeGetAbsPosNotSupported | PTZCtrlCap_GotoPresetByPTZPreset | PTZCtrlCap_SinglePTZTrackNotSupported;
   g_PTZControlList.push_back( pPCP );

#if defined( __SUPPORT_ROBOMEC )
   ////////////   Robomec   //////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Robomec_RPT_30;
   pPCP->m_nCapabilities   = PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_PTZGetSetFocusAbs;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Robomec_RPT_10;
   pPCP->m_nCapabilities   = PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_PTZGetSetFocusAbs;
   g_PTZControlList.push_back( pPCP );
#endif

   // Support SecurityCenter

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_SecurityCenter;
   pPCP->m_nCapabilities   = 0;
   g_PTZControlList.push_back( pPCP );

   ////////////   Convex   //////////////

   pPCP                               = new PTZControlProperty;
   pPCP->m_nPTZControlType            = PTZControlType_Convex_CNT_20_MidDistanceCam;
   pPCP->m_nCapabilities              = PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_PTZWiperSupported | PTZCtrlCap_PTZHeatingWireSupported | PTZCtrlCap_PTZThermalCamSupported | PTZCtrlCap_PTZOneShotAutoFocusSupported | PTZCtrlCap_PTZGetSetFocusAbs | PTZCtrlCap_MultiZoomModule | PTZCtrlCap_DoNotOpenSetupWebPage;
   pPCP->m_fThermalCameraZoomStepSize = 1.0f;

   g_PTZControlList.push_back( pPCP );

   ////////////   Hitachi   //////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Hitachi;
   pPCP->m_nCapabilities   = PTZCtrlCap_SetAbsPosNotSupported | PTZCtrlCap_GetAbsPosNotSupported | PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_PTZOneShotAutoFocusSupported;
   g_PTZControlList.push_back( pPCP );

   ////////////   Hitron BVS-H2020R  //////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_BestDigital_BVS_H2020R;
   pPCP->m_nCapabilities   = PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_PTZOneShotAutoFocusSupported;
   g_PTZControlList.push_back( pPCP );

   // (mkjang-140610)
   ////////////   BestDigital BNC-5230HR-W  //////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_BestDigital_BNC_5230HR_W;
   pPCP->m_nCapabilities   = PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_IRLigtControlSupported;
   g_PTZControlList.push_back( pPCP );

   // (mkjang-140812)
   ////////////   BestDigital VX-8150IRD(2020WB)  //////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_BestDigital_XV8150IRD_2020WB;
   pPCP->m_nCapabilities   = PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_IRLigtControlSupported;
   g_PTZControlList.push_back( pPCP );

   ////////////   SamsungTechWin_iPOLiS  //////////////
   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_SamsungTechWin_iPOLiS;
   pPCP->m_nCapabilities |= PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_PanTiltCoordinateChangeable;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_SamsungTechWin_iPOLiS_SNP_6200H;
   pPCP->m_nCapabilities |= PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_PanTiltCoordinateChangeable;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_SamsungTechWin_iPOLiS_SNP_6200RH;
   pPCP->m_nCapabilities |= PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_PanTiltCoordinateChangeable | PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_IRLigtControlSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_SamsungTechWin_iPOLiS_SNP_5300H;
   pPCP->m_nCapabilities |= PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_PanTiltCoordinateChangeable;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_SamsungTechWin_iPOLiS_SNZ_5200_Convex_CNT_20;
   pPCP->m_nCapabilities |= PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_PTZWiperSupported | PTZCtrlCap_PTZHeatingWireSupported | PTZCtrlCap_AutoFocusOnOff | PTZCtrlCap_IRLigtControlSupported | PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_DoNotOpenSetupWebPage |
       //PTZCtrlCap_GotoPresetByPTZPreset|
       0;

   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_SamsungTechWin_iPOLiS_SNZ_6320_Convex_CNT_20;
   pPCP->m_nCapabilities |= PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_PTZWiperSupported | PTZCtrlCap_PTZHeatingWireSupported | PTZCtrlCap_AutoFocusOnOff | PTZCtrlCap_IRLigtControlSupported | PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_DoNotOpenSetupWebPage |
       //PTZCtrlCap_GotoPresetByPTZPreset|
       0;

   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_SamsungTechWin_iPOLiS_SNP_3371H;
   pPCP->m_nCapabilities |= PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_PanTiltCoordinateChangeable | PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_IRLigtControlSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_SamsungTechWin_iPOLiS_SNP_6320H;
   pPCP->m_nCapabilities |=
       //PTZCtrlCap_SinglePTZTrackNotSupported |
       PTZCtrlCap_PanTiltCoordinateChangeable;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_SamsungTechWin_iPOLiS_SNP_6320RH;
   pPCP->m_nCapabilities |=
       //PTZCtrlCap_SinglePTZTrackNotSupported |
       PTZCtrlCap_PanTiltCoordinateChangeable | PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_IRLigtControlSupported;
   g_PTZControlList.push_back( pPCP );

   ////////// 주둔지 경계용 카메라 IR, 레이저, 열화상
   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_HanwhaTechWin_iPOLiS_SNZ_6320_CAMP;
   pPCP->m_nCapabilities |=
       //PTZCtrlCap_SinglePTZTrackNotSupported |
       PTZCtrlCap_PTZWiperSupported | PTZCtrlCap_PTZDefogSupported | PTZCtrlCap_PanTiltCoordinateChangeable | PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_MultiZoomModule | PTZCtrlCap_PresetMoveByUserSpecifiedSpeedSupported | PTZCtrlCap_GotoPresetByPTZPreset | PTZCtrlCap_IRLigtControlSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_HanwhaTechWin_iPOLiS_SNZ_6320_CAMP2;
   pPCP->m_nCapabilities |=
       //PTZCtrlCap_SinglePTZTrackNotSupported |
       PTZCtrlCap_PTZWiperSupported | PTZCtrlCap_PanTiltCoordinateChangeable | PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_MultiZoomModule | PTZCtrlCap_GotoPresetByPTZPreset | PTZCtrlCap_IRLigtControlSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_SamsungTechWin_iPOLiS_SNZ_5200_Shinwoo_SPT_7080B;
   pPCP->m_nCapabilities |= PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_PTZWiperSupported | PTZCtrlCap_PTZHeatingWireSupported | PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_IRLigtControlSupported | PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_AutoFocusOnOff | PTZCtrlCap_NeedExtraPTZPortNo | PTZCtrlCap_DoNotOpenSetupWebPage | PTZCtrlCap_DoNotPeridicallyReqAbsPos | PTZCtrlCap_PresetMoveByUserSpecifiedSpeedSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_SamsungTechWin_iPOLiS_SNZ_6320_Shinwoo_SPT_7080B;
   pPCP->m_nCapabilities |= PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_PTZWiperSupported | PTZCtrlCap_PTZHeatingWireSupported | PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_IRLigtControlSupported | PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_AutoFocusOnOff | PTZCtrlCap_NeedExtraPTZPortNo | PTZCtrlCap_DoNotOpenSetupWebPage | PTZCtrlCap_DoNotPeridicallyReqAbsPos | PTZCtrlCap_PresetMoveByUserSpecifiedSpeedSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_SamsungTechWin_iPOLiS_SNZ_6320_XV_570_IRPT;
   pPCP->m_nCapabilities |= PTZCtrlCap_SetAbsPosNotSupported | PTZCtrlCap_GetAbsPosNotSupported | PTZCtrlCap_RealTimeGetAbsPosNotSupported | PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_IRLigtControlSupported | PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_NeedExtraPTZPortNo | PTZCtrlCap_GotoPresetByPTZPreset;
   g_PTZControlList.push_back( pPCP );

   ////////////	YoungKook   //////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_YoungKook_YSDIRMP10;
   pPCP->m_nCapabilities   = PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_NeedExtraPTZPortNo | PTZCtrlCap_ConnectionDependOnStream;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_YoungKook_YSDIRMP20S;
   pPCP->m_nCapabilities   = PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_NeedExtraPTZPortNo
       //PTZCtrlCap_IRLigtControlSupported |
       //PTZCtrlCap_IRLightAreaSupported
       ;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_YoungKook_YSD_PN20MH;
   pPCP->m_nCapabilities   = PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_GotoPresetByPTZPreset;
   g_PTZControlList.push_back( pPCP );
   ////////////	Panasonic	//////////////

   // __SUPPORT_PANASONIC_CAMERA
   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Panasonic_WVSC385;
   pPCP->m_nCapabilities   = PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_PTZOneShotAutoFocusSupported;
   g_PTZControlList.push_back( pPCP );

   // (mkjang-140624)
   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Panasonic_WVNS202A;
   pPCP->m_nCapabilities   = PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_PTZOneShotAutoFocusSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Panasonic_WVSC384;
   pPCP->m_nCapabilities   = PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_PTZOneShotAutoFocusSupported;
   g_PTZControlList.push_back( pPCP );

   ///////////  Dongyang ////////////////
   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Dongyang_DY_IR2020HD_SONY;
   pPCP->m_nCapabilities |= PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_PTZWiperSupported | PTZCtrlCap_PTZHeatingWireSupported | PTZCtrlCap_IRLigtControlSupported | PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_ZoomProportionalJog | PTZCtrlCap_PTZFanSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Dongyang_DY_IR2020HD_WONWOO;
   pPCP->m_nCapabilities |= PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_PTZWiperSupported | PTZCtrlCap_PTZHeatingWireSupported | PTZCtrlCap_IRLigtControlSupported | PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_ZoomProportionalJog | PTZCtrlCap_PTZFanSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Dongyang_DY_IP020HD_SONY;
   pPCP->m_nCapabilities |= PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_PTZWiperSupported | PTZCtrlCap_PTZHeatingWireSupported |
       //PTZCtrlCap_ZoomProportionalJog |
       PTZCtrlCap_PTZFanSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Dongyang_DY_IP020HD_WONWOO;
   pPCP->m_nCapabilities |= PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_PTZWiperSupported | PTZCtrlCap_PTZHeatingWireSupported |
       //PTZCtrlCap_ZoomProportionalJog |
       PTZCtrlCap_PTZFanSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Dongyang_DY_IR3030HD_WONWOO;
   pPCP->m_nCapabilities |= PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_PTZWiperSupported | PTZCtrlCap_PTZHeatingWireSupported | PTZCtrlCap_IRLigtControlSupported | PTZCtrlCap_IRLightAreaSupported |
       //PTZCtrlCap_ZoomProportionalJog |
       PTZCtrlCap_PTZFanSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Dongyang_DY_IR2030HL_SONY;
   pPCP->m_nCapabilities |= PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_PTZWiperSupported | PTZCtrlCap_IRLigtControlSupported | PTZCtrlCap_IRLightAreaSupported |
       //	PTZCtrlCap_ZoomProportionalJog |
       PTZCtrlCap_PTZFanSupported;
   g_PTZControlList.push_back( pPCP );

   /////////// FS Networks ///////////
   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_FSNetworks_FS_IR203H;
   pPCP->m_nCapabilities |= PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_PTZWiperSupported | PTZCtrlCap_PTZHeatingWireSupported | PTZCtrlCap_IRLigtControlSupported | PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_ZoomProportionalJog | PTZCtrlCap_PTZFanSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_FSNetworks_FS_IR303H;
   pPCP->m_nCapabilities |= PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_PTZWiperSupported | PTZCtrlCap_PTZHeatingWireSupported | PTZCtrlCap_IRLigtControlSupported | PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_ZoomProportionalJog | PTZCtrlCap_PTZFanSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_FSNetworks_FS_IR307H;
   pPCP->m_nCapabilities |= PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_PTZWiperSupported | PTZCtrlCap_PTZHeatingWireSupported | PTZCtrlCap_IRLigtControlSupported | PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_ZoomProportionalJog | PTZCtrlCap_PTZFanSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_FSNetworks_FS_IR306H;
   pPCP->m_nCapabilities |= PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_PTZWiperSupported | PTZCtrlCap_PTZHeatingWireSupported | PTZCtrlCap_IRLigtControlSupported | PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_ZoomProportionalJog | PTZCtrlCap_PTZFanSupported;
   g_PTZControlList.push_back( pPCP );

   /////////// XProtect ///////////
   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_XProtect;
   pPCP->m_nCapabilities   = 0;
   g_PTZControlList.push_back( pPCP );

   ///////////  FlexWatch  ////////////////
   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_FlexWatch_FW1177_DEF;

   pPCP->m_nCapabilities = PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_RealTimeGetAbsPosNotSupported | PTZCtrlCap_GotoPresetByPTZPreset;
   g_PTZControlList.push_back( pPCP );

   ///////////  UDP  ////////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_UDP_CND_20ZHO;

   pPCP->m_nCapabilities = PTZCtrlCap_SinglePTZTrackNotSupported;
   g_PTZControlList.push_back( pPCP );

   ///////////  Probe Digital  ////////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_ProbeDigital_PRI_H2010;
   pPCP->m_nCapabilities   = PTZCtrlCap_GotoPresetByPTZPreset |
       //PTZCtrlCap_RealTimeGetAbsPosNotSupported |
       PTZCtrlCap_PanTiltCoordinateChangeable | PTZCtrlCap_SinglePTZTrackNotSupported;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_ProbeDigital_PRI_H3200;
   pPCP->m_nCapabilities   = PTZCtrlCap_GotoPresetByPTZPreset |
       //PTZCtrlCap_RealTimeGetAbsPosNotSupported |
       PTZCtrlCap_PanTiltCoordinateChangeable | PTZCtrlCap_SinglePTZTrackNotSupported;
   g_PTZControlList.push_back( pPCP );

   // (mkjang-140401)
   ////////////////  Zenotech MEGA-IPCAM  ////////////////
   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Zenotech_MEGA_IPCAM;
   pPCP->m_nCapabilities |= PTZCtrlCap_GotoPresetByPTZPreset;
   g_PTZControlList.push_back( pPCP );

   // (mkjang-140429)
   ////////////////  Vision Hitech BM2Ti-IR  ////////////////
   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Visionhitech_BM2TI_IR;
   pPCP->m_nCapabilities |=
       //PTZCtrlCap_PTZPosInitSupported |
       PTZCtrlCap_SinglePTZTrackNotSupported;
   g_PTZControlList.push_back( pPCP );

   ////////////////// Doowon /////////////////////
   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Doowon_DMH_5001IR;
   pPCP->m_nCapabilities |= PTZCtrlCap_IRLigtControlSupported | PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_ZoomProportionalJog;
   g_PTZControlList.push_back( pPCP );

#ifdef __SUPPORT_HUAWEI
   // (mkjang-140721)
   ///////////  HuaWei ////////////////
   if( IS_SUPPORT( __SUPPORT_HUAWEI ) ) {
      pPCP                    = new PTZControlProperty;
      pPCP->m_nPTZControlType = PTZControlType_HuaWei_IPC6621_Z30_I;
      pPCP->m_nCapabilities |= PTZCtrlCap_SinglePTZTrackNotSupported;
      g_PTZControlList.push_back( pPCP );
   }
#endif

   ////////////////  Vivotek SD8363E  ////////////////
   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Vivotek_SD8363E;
   pPCP->m_nCapabilities |= 0;
   g_PTZControlList.push_back( pPCP );

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Vivotek_SD9362EHL;
   pPCP->m_nCapabilities |= 0;
   g_PTZControlList.push_back( pPCP );
#ifdef __SUPPORT_ONVIF
   ////////// // Support Onvif   //////////////

   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Onvif;
   pPCP->m_nCapabilities |= PTZCtrlCap_GotoPresetByPTZPreset;
   g_PTZControlList.push_back( pPCP );
#endif

   ////////////////  장애인 협회 EW-IRSD2020HDP  ////////////////
   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_EW_IRSD2020HDP;
   pPCP->m_nCapabilities |= PTZCtrlCap_GotoPresetByPTZPreset;
   g_PTZControlList.push_back( pPCP );

#ifdef __SUPPORT_DALLMEIER
   ////////////////  Dallmeier  ////////////////
   pPCP                    = new PTZControlProperty;
   pPCP->m_nPTZControlType = PTZControlType_Dallmeier_DDZ4020_YY_HS_HD;
   pPCP->m_nCapabilities |= PTZCtrlCap_ContPTAndAbsZoomMoveCam | PTZCtrlCap_RealTimeGetAbsPosNotSupported;
   g_PTZControlList.push_back( pPCP );
#endif
}

/////////////////////////////////////////////////////////////////////////////////
//
// Functions
//
/////////////////////////////////////////////////////////////////////////////////

void InitGlobalProperties()
{
   static int nInitNum = 0;
   if( nInitNum > 0 ) return;
   nInitNum++;
   InitVCMResources();

   InitCaptureBoardProducts();
   InitIPCameraProducts();
   InitPTZControlProducts();
}

PTZControlProperty* GetPTZControlProperty( int nPTZControlType )
{
   int i;
   int nItem = (int)g_PTZControlList.size();
   for( i = 0; i < nItem; i++ )
      if( g_PTZControlList[i]->m_nPTZControlType == nPTZControlType )
         return g_PTZControlList[i];
   return NULL;
}

CaptureBoardProperty* GetCaptureBoardProporty( int nCaptureBoardType )
{
   int i;
   int nItem = (int)g_CaptureBoardList.size();
   for( i = 0; i < nItem; i++ )
      if( g_CaptureBoardList[i]->m_nCaptureBoardType == nCaptureBoardType )
         return g_CaptureBoardList[i];
   return NULL;
}

IPCameraProperty* GetIPCameraProperty( int nIPCameraType )
{
   int i;
   int nItem = (int)g_IPCameraList.size();
   for( i = 0; i < nItem; i++ )
      if( g_IPCameraList[i]->m_nIPCameraType == nIPCameraType )
         return g_IPCameraList[i];
   return NULL;
}

IPCameraProperty* GetIPCameraPropertyOfSamePTZControlGroupe( int nPTZControlType )
{
   int i;
   int nItem = (int)g_IPCameraList.size();
   for( i = 0; i < nItem; i++ ) {
      IPCameraProperty* pIPCameraProperty = g_IPCameraList[i];
      if( pIPCameraProperty->m_nPTZControlType / 10000 == nPTZControlType / 10000 ) {
         pIPCameraProperty;
         break;
      }
   }
   return NULL;
}

IPCameraProperty* PopIPCameraProperty( IPCameraList& ip_cam_list, int nIPCameraType )
{
   std::vector<IPCameraProperty*>::iterator iter     = ip_cam_list.begin();
   std::vector<IPCameraProperty*>::iterator iter_end = ip_cam_list.end();
   while( iter != iter_end ) {
      IPCameraProperty* pProperty = *iter;
      if( pProperty->m_nIPCameraType == nIPCameraType ) {
         ip_cam_list.erase( iter );
         return pProperty;
      }
      iter++;
   }
   return NULL;
}

BOOL PushIPCameraProperty( IPCameraList& ip_cam_list, IPCameraProperty* pProperty )
{
   if( pProperty ) {
      g_IPCameraList.push_back( pProperty );
      return TRUE;
   }
   return FALSE;
}

BOOL MoveIPCameraProperty( IPCameraList& list_dest, IPCameraList& list_src, int nIPCameraType )
{
   return PushIPCameraProperty( list_dest, PopIPCameraProperty( list_src, nIPCameraType ) );
}

void GetResolutionTypeString( std::string& strOutput, ResolutionItem& item )
{
   if( item.m_nResolutionType == ResolutionType_Size ) {
      strOutput = sutil::sformat( "%4d x %4d", item.m_nWidth, item.m_nHeight );
   } else {
      strOutput = sutil::sformat( "%4d x %4d (%s)", item.m_nWidth, item.m_nHeight, ResolutionTypeString.GetString( item.m_nResolutionType ).c_str() );
   }
}

void GetSameManufacturePTZList( int nPTZControlType, std::vector<int>& vtPTZControlTypeList )
{
   int i;

   for( i = 0; i < EntirePTZControlTypeList[i]; i++ ) {
      int nPTZControlType_i = EntirePTZControlTypeList[i];
      if( nPTZControlType_i / 10000 == nPTZControlType / 10000 ) {
         vtPTZControlTypeList.push_back( nPTZControlType_i );
      } else if( nPTZControlType_i < 10000 && nPTZControlType < 10000 ) {
         vtPTZControlTypeList.push_back( nPTZControlType_i );
      } else if( nPTZControlType == 0 ) {
         vtPTZControlTypeList.push_back( nPTZControlType_i );
      }
   }
}

void GetIPCameraTypeList( std::vector<int>& vtIPCameraList )
{
   GetIPCameraTypeList( 0, vtIPCameraList );
}

void GetIPCameraTypeList( uint nIPCamCapabilities, std::vector<int>& vtIPCameraList )
{
   int i;
   int nItem = (int)g_IPCameraList.size();
   vtIPCameraList.clear();
   for( i = 0; i < nItem; i++ ) {
      IPCameraProperty* pIPCameraProperty = g_IPCameraList[i];
      if( nIPCamCapabilities ) {
         if( nIPCamCapabilities & pIPCameraProperty->m_nCapabilities )
            vtIPCameraList.push_back( g_IPCameraList[i]->m_nIPCameraType );
      } else
         vtIPCameraList.push_back( g_IPCameraList[i]->m_nIPCameraType );
   }
}

void GetIPCameraTypeListByCapabilities( IPCamCaps eCaps, std::vector<int>& vtIPCameraList )
{
   int i;
   int nItem = (int)g_IPCameraList.size();
   vtIPCameraList.clear();
   for( i = 0; i < nItem; i++ ) {
      if( g_IPCameraList[i]->m_nCapabilities & eCaps ) {
         vtIPCameraList.push_back( g_IPCameraList[i]->m_nIPCameraType );
      }
   }
}

std::string GetIPCameraProductName( int nIPCameraType )
{
   int i;
   int nItemNum = (int)g_IPCameraList.size();

   for( i = 0; i < nItemNum; i++ ) {
      if( nIPCameraType == g_IPCameraList[i]->m_nIPCameraType )
         return g_IPCameraList[i]->m_strProdName;
   }
   return std::string( "Not Selected" ); // 선택 안 함
}

int GetIPCameraType( const std::string& strProdName )
{
   int i;
   int nItemNum = (int)g_IPCameraList.size();

   for( i = 0; i < nItemNum; i++ ) {
      if( g_IPCameraList[i]->IsSameProductName( strProdName ) ) {
         int nIPCamType = g_IPCameraList[i]->m_nIPCameraType;
         return g_IPCameraList[i]->m_nIPCameraType;
      }
   }
   return 0;
}

VideoPixelFormat GetPixelFormat( int nColorSpace )
{
   switch( nColorSpace ) {
   case ColorSpace_YUY2:
      return VPIX_FMT_YUY2;
   case ColorSpace_YV12:
      return VPIX_FMT_YV12;
   case ColorSpace_RGB:
      return VPIX_FMT_BGR24;
   case ColorSpace_UYVY:
      return VPIX_FMT_YUY2;
   default:
      break;
   }
   return VPIX_FMT_NONE;
}

float GetBytePerPixel( int nColorSpace )
{
   switch( nColorSpace ) {
   case ColorSpace_YUY2:
      return 2.0f;
   case ColorSpace_YV12:
      return 1.5f;
   case ColorSpace_UYVY:
      return 2.0f;
   case ColorSpace_RGB:
      return 3.0f;
   default:
      break;
   }
   return 3.0f;
}

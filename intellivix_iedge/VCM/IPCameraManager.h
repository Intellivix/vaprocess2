#pragma once

#include "DigitalIO.h"


class CIPCameraInfo;
class CCamera_IP;


class CIPCameraManager 
{
public:

   CCriticalSection m_csMgr;
   std::vector<CCamera_IP*> m_Cameras;

public:
   CIPCameraManager(void);
   ~CIPCameraManager(void);

   void Initialize (  );
   void Uninitialize (  );

   int  CreateCamera (CIPCameraInfo* pCamInfo, CCamera_IP** ppNewCamera);
   BOOL DeleteCamera (CameraID* pCameraID);
};

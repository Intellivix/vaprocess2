#pragma once

#include "PTZVector.h"


enum PTZProtocolType    
{
   PTZProtocolType_Pelco_D        = 100,   
   PTZProtocolType_Samsung        = 200,   
   PTZProtocolType_SamsungTechWin = 300,   
   PTZProtocolType_LG             = 400,   
   PTZProtocolType_Honeywell      = 500,   
   PTZProtocolType_BOSCH          = 600,   
   PTZProtocolType_Woonwoo        = 700,   
};

enum PTZPosType
{
   PTZPosType_Pan     = 0x00000001, 
   PTZPosType_Tilt    = 0x00000002, 
   PTZPosType_Zoom    = 0x00000004, 
};

const uint PTZPosType_PTZ = (PTZPosType_Pan | PTZPosType_Tilt | PTZPosType_Zoom);


enum AbsPosMoveCmdType
{
   AbsPosMoveCmdType_SendPTZCmdInOnePacket  = 0,  // Pan, Tilt, Zoom 명령을 한번에 보내는 방식. 
   AbsPosMoveCmdType_SendPTZCmdInEachPacket = 1,  // Pan, Tilt, Zoom 명령을 각각의 패킷에 담아 보내는 방식. 
};


///////////////////////////////////////////////////////////////////////////////
//
// Class: CPTZProtocol
//
///////////////////////////////////////////////////////////////////////////////

class CPTZProtocol
{
public:
   int   m_nCamAddr;
   int   m_nAbsPosMoveCmdType;

public:
   CPTZProtocol ();
   virtual ~CPTZProtocol () {}

public:
   void  Initialize  (int nCamAddr);
public:
   virtual void  Clear                  (BYTE* pCmd, int nCmdLen) {}
   virtual void  SetCheckSum            (BYTE* pCmd, int nCmdLen) {}
   virtual void  GetCmd_ContPTZMove     (BYTE* pCmd, int& nCmdLen, IPTZVector move_vec) {}
   virtual int   GetPTZPosHW            (BYTE* pCmd, int& nCmdLen, FPTZVector &abs_pos) { return 0; }
   virtual void  GetCmd_ReqAbsPos       (BYTE* pCmd, int& nCmdLen, uint nPTZPosType) {}
   virtual void  GetCmd_GotoAbsPos      (BYTE* pCmd, int& nCmdLen, FPTZVector abs_pos, uint nPTZPosType = PTZPosType_PTZ) {}
   virtual void  GetCmd_SetPreset       (BYTE* pCmd, int& nCmdLen, int preset_no) {}
   virtual void  GetCmd_ClearPreset     (BYTE* pCmd, int& nCmdLen, int preset_no) {}
   virtual void  GetCmd_GotoPreset      (BYTE* pCmd, int& nCmdLen, int preset_no) {}
   virtual void  GetCmd_SetAuxiliary    (BYTE* pCmd, int& nCmdLen, int nAuxNo) {}
   virtual void  GetCmd_ClearAuxiliary  (BYTE* pCmd, int& nCmdLen, int nAuxNo) {}
   virtual void  GetCmd_OSD_MenuDisplay (BYTE* pCmd, int& nCmdLen) {}
   virtual void  GetCmd_OSD_Enter       (BYTE* pCmd, int& nCmdLen) {}
   virtual void  GetCmd_OSD_Cancel      (BYTE* pCmd, int& nCmdLen) {}
   virtual void  GetCmd_SetIRLight      (BYTE* pCmd, int& nCmdLen, int nIRLight) {}
};


CPTZProtocol* GetPTZProtocol (int nPTZProtocolType);

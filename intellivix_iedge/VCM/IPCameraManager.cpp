#include "stdafx.h"
#include "Camera_IP.h"
#include "IPCameraManager.h"

CIPCameraManager::CIPCameraManager()
{
}

CIPCameraManager::~CIPCameraManager()
{
   Uninitialize();
}

void CIPCameraManager::Initialize()
{
}

void CIPCameraManager::Uninitialize()
{
   int i;
   int nItem = (int)m_Cameras.size();
   for( i = 0; i < nItem; i++ ) {
      delete m_Cameras[i];
   }
}

int CIPCameraManager::CreateCamera( CIPCameraInfo* pCamInfo, CCamera_IP** ppNewCamera )
{

   m_csMgr.Lock();
   *ppNewCamera        = NULL;
   int nRetuen         = ( DONE );
   bool bValid_CamInfo = TRUE;

   //for( int i = 0; i < (int)m_Cameras.size(); i++ ) {
      //CCamera_IP* pCamera = m_Cameras[i];
      //if (pCamera->m_pInfo->m_nIPAddress == pCamInfo->m_nIPAddress) {
      //   BCGPMessageBox ("동일한 아이피 주소를 다른 채널에서 사용하고 있습니다");
      //   bValid_CamInfo = FALSE;
      //   nRetuen = (1);
      //}
   //}
   
   if( bValid_CamInfo ) {
      CCamera_IP* pCamera = new CCamera_IP;
      m_Cameras.push_back( pCamera );
      *ppNewCamera = pCamera;
   }
   
   m_csMgr.Unlock();
   return ( nRetuen );
}

BOOL CIPCameraManager::DeleteCamera( CameraID* pCameraID )
{
   m_csMgr.Lock();
   BOOL bDelete                               = FALSE;
   int nItem                                  = (int)m_Cameras.size();
   std::vector<CCamera_IP*>::iterator iter    = m_Cameras.begin();
   std::vector<CCamera_IP*>::iterator iterEnd = m_Cameras.end();
   while( iter != iterEnd ) {
      CCamera_IP* pCamera = *iter;
      if( ( *pCamera->GetCameraID() ) == *pCameraID ) {
         delete pCamera;
         bDelete = TRUE;
         m_Cameras.erase( iter );
         break;
      }
      iter++;
   }
   m_csMgr.Unlock();
   return bDelete;
}

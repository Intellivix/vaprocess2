﻿#pragma once

#include "version.h"
#include "PTZxD.h"

extern CPTZ3DFile __PTZ3DFile;

class CVideoChannelInfo;
class CIPCameraManager;
class CCPacket;
class CVideoChannelManager;
class CTimerManager;
#ifdef __SUPPORT_SOLAR_CALCULATOR
class CSolarCalculator;
#endif
class CIVCP_Packet;

///////////////////////////////////////////////////////////////////////////
//
// Defines
//
///////////////////////////////////////////////////////////////////////////
#ifdef WIN32
#define MSG_VCM_START_FILE_TRANSFER ( WM_USER + 500 )
#endif
const int VCM_CBMaxChNum       = 32; // 캡춰보드의 최대 채널 수
const std::string g_fnVCMSetup = _T("VCMConfig.xml");
extern float g_fMaxDisplayFrameRate; // 최대 디스플레이 프레임 비율

///////////////////////////////////////////////////////////////////////////
//
// Enumerations
//
///////////////////////////////////////////////////////////////////////////

enum VCSetupMode {
   VCSetupMode_SetupPTZCamera      = 0x00000001,
   VCSetupMode_SetupRemote         = 0x00000002,
   VCSetupMode_DoNotAllowPTZCamera = 0x00000004,
   VCSetupMode_TempVC              = 0x00000008, // 현재 설정하려는 비디오 채널이 복사된 임시 채널인지에 대한 플레그.
   // ON  인 경우 : 임시 채널로 설정하는 것이므로 변경된 정보를 바로 적용한다.
   // OFF 인 경우 : 원본 정보 대신 복사본을 생성하여 복사본에 정보를 적용하고
   //               사용자가 적용을 하고자하면 원본에 복사를 한다.
   VCSetupMode_AddCamera = 0x00000010,
};

enum VCThreadType {
   VCThreadType_Cap  = 0x01,
   VCThreadType_Disp = 0x02,
   VCThreadType_Rec  = 0x04,
   VCThreadType_IP   = 0x08,
};

const uint VCThreadType_All = VCThreadType_Cap | VCThreadType_Disp | VCThreadType_Rec | VCThreadType_IP;

enum VCCamEvent {
   VCCamEvent_Unknown                 = 0,
   VCCamEvent_PTZCtrlByServer         = 1,
   VCCamEvent_PTZCtrlByServerReleased = 2,
   VCCamEvent_VideoSizeIsChanged      = 3,
   VCCamEvent_FrameRateIsMismatched   = 4,
   VCCamEvent_AuthorizationFailed     = 5,
   VCCamEvent_ConnectionFailed        = 6,
   VCCamEvent_VideoSignalNotOK        = 7,
   VCCamEvent_VideoSizeIsChanged_Bigger = 8,
};

// SendEventAlarmToVideoServer 함수의 3번째 인자 alarm_state 에 사용되는 열겨형
enum VCEventAlarmState {
   VCEventAlarmState_EventBegun = 1,
   VCEventAlarmState_EventEnded = 2,
};

///////////////////////////////////////////////////////////////////////////
//
// Callback Functions
//
///////////////////////////////////////////////////////////////////////////

struct VC_PARAM_STREAM {
   LPVOID pParam;
   int nVCState;
   UINT64 nCount;
   int nStreamIdx;
   int nCamState;
   BYTE* pYUY2Buff;
   int nWidth;
   int nHeight;
};

struct VC_PARAM_ENCODE {
   LPVOID pParam;
   int nVCState;
   UINT64 nCount;
   int nStreamIdx;
   int nCamState;
   int nCodecID;
   BYTE* pCompBuff;
   int nCompBuffSize;
   DWORD dwFlag;
   timeval PresentationTime;
   double dfNormalPlayTime;
};

struct VC_PARAM_CAMERA_EVENT {
   LPVOID pParam;
   int nStreamIdx;
   int nCamEvent;
   ISize2D PrevImgSize;
};

struct VC_PARAM_RTSP_META {
   LPVOID pParam;
   byte* pBuffer;
   int nBufferSize;
   timeval Timestamp;
};

// 영상처리용 콜백
typedef BOOL ( *LPVIDEOCHANNEL_CALLBACK_IMAGEPROCESS )( const VC_PARAM_STREAM& param );
// 디스플레이용 콜백
typedef BOOL ( *LPVIDEOCHANNEL_CALLBACK_DISPLAY )( const VC_PARAM_STREAM& param );
// 녹화용 콜백
typedef BOOL ( *LPVIDEOCHANNEL_CALLBACK_RECORD )( const VC_PARAM_STREAM& param );
// 디코드 비디오 프래임 콜백
typedef BOOL ( *LPVIDEOCHANNEL_CALLBACK_DECODE_VIDEO_FRAME )( const VC_PARAM_STREAM& param );
// 인코드 비디오 프래임 콜백
typedef BOOL ( *LPVIDEOCHANNEL_CALLBACK_ENCODE_VIDEO_FRAME )( const VC_PARAM_ENCODE& param );
// 상태가 변경 될 때마다 호출되는 콜백
typedef void ( *LPVIDEOCHANNEL_CALLBACK_CAMERA_EVENT )( const VC_PARAM_CAMERA_EVENT& param );
typedef void ( *LPVIDEOCHANNEL_CALLBACK_RTSP_METADATA )( const VC_PARAM_RTSP_META& param );

typedef BOOL ( *LPVIDEOCHANNEL_CALLBACK_VC_CONFIG_TO_BE_LOADED )( LPVOID pParam );

typedef BOOL ( *LPFUNCTION_SET_IMAGE_PROPERTY )( LPVOID, int, int, int );
typedef BOOL ( *LPFUNCTION_GET_IMAGE_PROPERTY )( LPVOID, int, int, int& );

///////////////////////////////////////////////////////////////////////////
//
// class: VCM_Desc
//
///////////////////////////////////////////////////////////////////////////

// 비디오 채널 관리자를 초기화시키는데 필요한 파라메터들을 정의한 클래스

class VCM_Desc {
public:
   BOOL m_bUSEVMSConnection;
   BOOL m_bUseVCMConfigFile; // 설정 로드, 저장 및 비디오채널 객체 생성을 VCM에서 전담하는지 여부. IntelliVIX는 이 옵션을 끄고 사용한다.
   std::string m_strConfigPath; // VCM 설정을 저장하는 디렉터리 경로
   std::string m_strStartupDirPath;

public:
   LPVIDEOCHANNEL_CALLBACK_IMAGEPROCESS m_pCBImageProcess;
   LPVIDEOCHANNEL_CALLBACK_DISPLAY m_pCBDisplay;
   LPVIDEOCHANNEL_CALLBACK_RECORD m_pCBRecord;
   LPVIDEOCHANNEL_CALLBACK_CAMERA_EVENT m_pCBCamEvent;
   LPVIDEOCHANNEL_CALLBACK_DECODE_VIDEO_FRAME m_pCBDecodeVideoFrame;
   LPVIDEOCHANNEL_CALLBACK_ENCODE_VIDEO_FRAME m_pCBEncodeVideoFrame;
   LPVIDEOCHANNEL_CALLBACK_VC_CONFIG_TO_BE_LOADED m_pCBVCConfigToBeLoad;
   LPVIDEOCHANNEL_CALLBACK_RTSP_METADATA m_pCBRTSPMetadata;

public:
   VCM_Desc();
   VCM_Desc& operator=( VCM_Desc& desc );
};

///////////////////////////////////////////////////////////////////////////
//
// class: VC_Desc
//
///////////////////////////////////////////////////////////////////////////

// 비디오 채널을 초기화시키는데 필요한 파라메터들을 정의한 클래스 (내부에서 사용)

class VC_Desc {
public:
   CVideoChannelInfo* m_pInfo;
   CVideoChannelManager* m_pVCM;
   CIPCameraManager* m_pIPCamMgr;
   CTimerManager* m_pTimerMgr = nullptr;
#ifdef __SUPPORT_SOLAR_CALCULATOR
   CSolarCalculator* m_pSunCalculator;
#endif

   LPVIDEOCHANNEL_CALLBACK_CAMERA_EVENT m_pCBCamEvent;
   LPVIDEOCHANNEL_CALLBACK_IMAGEPROCESS m_pCBImageProcess;
   LPVIDEOCHANNEL_CALLBACK_DISPLAY m_pCBDisplay;
   LPVIDEOCHANNEL_CALLBACK_RECORD m_pCBRecord;
   LPVIDEOCHANNEL_CALLBACK_DECODE_VIDEO_FRAME m_pCBDecodeVideoFrame;
   LPVIDEOCHANNEL_CALLBACK_ENCODE_VIDEO_FRAME m_pCBEncodeVideoFrame;   
   LPVIDEOCHANNEL_CALLBACK_VC_CONFIG_TO_BE_LOADED m_pCBVCConfigToBeLoad;
   LPVIDEOCHANNEL_CALLBACK_RTSP_METADATA m_pCBRTSPMetadata;

   VC_Desc();
   void SetVideoChannelDesc( const VC_Desc& from );
};

///////////////////////////////////////////////////////////////////////////
//
// class: PVTInfo
//
///////////////////////////////////////////////////////////////////////////

class PVTInfo {
public:
   int P_width;
   int P_height;
   int S_width;
   int S_height;
   int N_images;

public:
   int ProjectionType;
   float HFOV;

public:
   PVTInfo();
   int Read( const char* path_name );
};

///////////////////////////////////////////////////////////////////////////
//
// Global Functions
//
///////////////////////////////////////////////////////////////////////////

void InitVCMLib();
void UninitVCMLib();

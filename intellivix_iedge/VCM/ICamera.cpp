﻿#include "stdafx.h"

//#include <afxpriv.h>
#include "ICamera.h"

int CameraID::m_nCameraCount = 0;

CameraID::CameraID()
{
   AfxGetLocalFileTime( &m_ftCreate );
   m_nID = m_nCameraCount;
   m_nCameraCount++;
}

BOOL CameraID::operator==( CameraID& CameraID )
{
   if( ( this->m_nID == CameraID.m_nID ) && ( this->m_ftCreate == CameraID.m_ftCreate ) ) return TRUE;
   return ( FALSE );
}

void CameraID::SetID( CameraID& CameraID )
{
   *this = CameraID;
}

CameraID& CameraID::GetID()
{
   return ( *this );
}

int CameraID::Size()
{
   static int nSize = sizeof( FILETIME ) + sizeof( UINT );
   return ( nSize );
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// class ICamera

ICamera::ICamera()
{
   m_nICameraIdx   = 0;
   m_nState        = 0;
   m_nPrevState    = 0;
   m_pCBCamEvent   = NULL;
   m_pVideoChannel = NULL;
}

ICamera::~ICamera()
{
}

BOOL ICamera::IsCaptureState()
{
   if( m_nState & State_Capture )
      return ( TRUE );
   return ( FALSE );
}

BOOL ICamera::IsVideoSignalAlive()
{
   return ( TRUE );
}

BOOL ICamera::CheckingVideoSignalAlive()
{
   return ( TRUE );
}

int ICamera::SetDefaultImageProperty( int nImagePropertyType )
{
   return ( DONE );
}

int ICamera::SetAllImageProperty()
{
   return ( DONE );
}

int ICamera::SetImageProperty( int nImagePropertyType, int nValue, int nFlagAuto )
{
   return ( DONE );
}

int ICamera::GetImageProperty( int nImagePropertyType, int& nValue, int& nFlagAuto )
{
   nFlagAuto = FALSE;
   return ( DONE );
}

int ICamera::GetImagePropertyRange( int nImagePropertyType, int& nMin, int& nMax, int& nTic )
{
   return ( DONE );
}

void ICamera::GetSupportedImagePropertyList( std::vector<int>& vtImagePropertyType )
{
}

int ICamera::GetImageSize()
{
   return ( 0 );
}

void ICamera::GetCameraDesc( std::string& strDesc )
{
}

int ICamera::GetMotionStatus( BOOL& bMotion )
{
   return ( DONE );
}

int ICamera::GetAlarmInputFlags()
{
   return ( DONE );
}

int ICamera::GetAlarmInputStatus( int nChIdx, BOOL& bSignal )
{
   return ( DONE );
}

int ICamera::SetRelayOutputStatus( int nChIdx, BOOL bSignal )
{
   return ( DONE );
}

int ICamera::GetRelayOutputStatus( int nChIdx, BOOL& bSignal )
{
   return ( DONE );
}

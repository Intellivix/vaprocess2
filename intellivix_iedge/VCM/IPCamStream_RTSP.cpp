﻿#include "stdafx.h"
#include "IPCamStream_RTSP.h"
#include "version.h"
#ifdef __SUPPORT_ONVIF
#include "COnvifCtrl.h"
#endif

/////////////////////////////////////////////////////////////////////////////
//
// class CIPCamStream_RTSP
//
/////////////////////////////////////////////////////////////////////////////

CIPCamStream_RTSP::CIPCamStream_RTSP()
{
   m_nCompanyID      = CompanyID_RTSP;
   m_nState          = 0;
   m_nLocalState     = 0;
   m_pCBRTSPMetadata = NULL;
}

CIPCamStream_RTSP::~CIPCamStream_RTSP()
{
}

int CIPCamStream_RTSP::Initialize( CIPCameraInfo* pInfo )
{
   if( m_nState & State_Initialize ) return ( DONE );
   m_nState |= State_Initialize;

   m_pInfo = pInfo;
   // 2013-11-01 jun : Live555 라이브러리의 경우 포트번호가 0인 경우다운되는 문제가 발생하여 아래와 같이 처리함.
   if( m_pInfo->m_nPortNo <= 0 )
      m_pInfo->m_nPortNo = 554;
   m_pProperty = GetIPCameraProperty( m_pInfo->m_nIPCameraType );
   return TRUE;
}

int CIPCamStream_RTSP::Uninitialize()
{
   return TRUE;
}

BOOL CIPCamStream_RTSP::IsConnected()
{
   return CIPCamStream::IsConnected();
}

int CIPCamStream_RTSP::BeginCapture()
{
   m_bStreamUsingTCP = ( m_pInfo->m_nSocketType ) ? true : false;
   if( m_strRTSP_URL.length() ) {
      m_strURL = m_strRTSP_URL;
   } else {
      m_strURL = m_pInfo->m_strURL;
   }

   logd( "[CH:%02d] RTSP BeginCapture     URL[%s]", m_nChannelNo_IVX + 1, m_strURL.c_str() );
   m_strID     = m_pInfo->m_strUserName;
   m_strPasswd = m_pInfo->m_strPassword;

   m_nState &= ~State_VideoSizeIsNotSupported;
   Run();
   return ( DONE );
}

int CIPCamStream_RTSP::EndCapture()
{
   //logd( "[CH:%d] RTSP EndCapture \n", m_nChannelNo_IVX + 1 );
   Stop();
   CIPCamStream::EndCapture();
   return ( DONE );
}

int CIPCamStream_RTSP::Close()
{
   return ( DONE );
}

int CIPCamStream_RTSP::SignalStopCapture()
{
   return ( DONE );
}

CAVCodecParam CIPCamStream_RTSP::GetAVCodecParam( VideoCodecID emCodecID )
{
   CAVCodecParam avOpen;
   avOpen.m_nCodecID           = emCodecID;
   avOpen.m_nWidth             = m_pInfo->m_nWidth;
   avOpen.m_nHeight            = m_pInfo->m_nHeight;
   avOpen.m_fFrameRate         = m_pInfo->m_fFPS_Cap;
   avOpen.m_nPixelFormat       = VPIX_FMT_YUY2;
   avOpen.m_fDecodingFPS       = m_pInfo->m_fDecodingFPS;
   avOpen.m_bUseHWAcceleration = m_pInfo->m_bHWDecoder;

   return avOpen;
}

int CIPCamStream_RTSP::Decompress( CCompressImageBufferItem* pBuffItem, BArray1D& destBuff )
{
   int ret = DecompressResult_OK;

   if( m_pInfo->m_nWidth <= 0 || m_pInfo->m_nHeight <= 0 ) {
      m_pInfo->m_nWidth  = 160;
      m_pInfo->m_nHeight = 120;
   }

   CAVCodecParam avParam = GetAVCodecParam( (VideoCodecID)pBuffItem->m_nCodecID );
   m_VideoDecoder.OpenDecoder( avParam );
   if( m_pcsSrcImageBuff ) m_pcsSrcImageBuff->Lock();
   int nDecompressRet = m_VideoDecoder.Decompress( pBuffItem->m_CompBuff, pBuffItem->m_nCompBuffSize, destBuff, 0 );
   if( m_pcsSrcImageBuff ) m_pcsSrcImageBuff->Unlock();

   if( nDecompressRet == AVCodecDecompressRet_ImageSizeIsChanged ) {
      ISize2D video_size = m_VideoDecoder.GetImageSizeOfEncodedData();
      m_pInfo->m_nWidth  = video_size.Width;
      m_pInfo->m_nHeight = video_size.Height;

      return DecompressResult_VideoSizeIsChanged;
   }

   // 디코더 에러가 발생하면 닫기
   if( nDecompressRet > 0 ) {
      logd( "RTSP Decoding Error Ret[%d] BuffSize[%d]", nDecompressRet, pBuffItem->m_nCompBuffSize );
      m_VideoDecoder.Close();
   }
   if( nDecompressRet < 0 ) {
      ret = DecompressResult_NeedMoreFrame;
   }

   m_pInfo->m_nVideoFormat_Cap = pBuffItem->m_nCodecID;

   // 영상크기가 다르면 영상크기 설정 변경
   if( nDecompressRet == AVCodecDecompressRet_ImageSizeDoesNotMatched ) {
      ISize2D video_size = m_VideoDecoder.GetImageSizeOfEncodedData();
      m_VideoDecoder.Close();
      if( m_pInfo->m_nWidth != video_size.Width || m_pInfo->m_nHeight != video_size.Height ) {
         m_pInfo->m_nWidth  = video_size.Width;
         m_pInfo->m_nHeight = video_size.Height;
         return ( DecompressResult_VideoSizeIsChanged );
      }
   }

   return ( ret );
}

void CIPCamStream_RTSP::OnErrorRaised( int err_code )
{
   if( LIVE555_ERROR_BYE_PACKET == err_code ) {
      logd( "LIVE555_ERROR_BYE_PACKET" );
      m_nState |= State_RTSP_ByePacketReceived;
   }
   if( LIVE555_ERROR_LOGIN_FAILED == err_code ) {
      logd( "LIVE555_ERROR_LOGIN_FAILED" );
      m_nState |= State_AuthorizationFailed;
   }
}

void CIPCamStream_RTSP::OnAudioDataReceived( int codec_id, byte* s_buffer, int s_buf_size, timeval& timestampl )
{
}

void CIPCamStream_RTSP::OnVideoFrameReceived( int codec_id, byte* s_buffer, int s_buf_size, int flags, timeval& timestamp )
{
   CCompressImageBufferItem* pNewItem = new CCompressImageBufferItem;
   pNewItem->m_CompBuff.Create( s_buf_size );
   pNewItem->m_nCompBuffSize = s_buf_size;
   memcpy( (byte*)pNewItem->m_CompBuff, s_buffer, s_buf_size );
   pNewItem->m_dwCommFlag       = flags;
   pNewItem->m_PresentationTime = timestamp;
   pNewItem->m_dfNormalPlayTime = GetNormalPlayTime( timestamp );

   switch( codec_id ) {
   case LIVE555_VCODEC_ID_MPEG4:
      pNewItem->m_nCodecID = VCODEC_ID_MPEG4;
      break;
   case LIVE555_VCODEC_ID_H264:
      pNewItem->m_nCodecID = VCODEC_ID_H264;
      break;
   case LIVE555_VCODEC_ID_MJPEG:
      pNewItem->m_nCodecID = VCODEC_ID_MJPEG;
      break;
   default:
      ASSERT( 0 );
      break;
   }
   // jun[중요]: 이 콜백 함수내에 지연이 있는 작업을 해서는 안됨!!!
   //            UDP로 수신받는 경우 약간의 지연이 영상 깨짐문제를 발생시킴.

   m_pCompBuffQueue->Push( pNewItem );

   //logv( "RTSP VideoFrameReceived  Codec[%d]  Size[%6d]  Flag[%08x]\n", codec_id, s_buf_size, pNewItem->m_dwCommFlag );
   ++m_nReceiveFrameCount;

   if (0)
   if (!m_nPrevReceiveFrameCountCheckTime || GetTickCount() - m_nPrevReceiveFrameCountCheckTime > 1000*10) {
      logi( "[CH:%02d] RTSP::FrameCount=%d", m_nChannelNo_IVX + 1, m_nReceiveFrameCount);
      m_nReceiveFrameCount = 0;
      m_nPrevReceiveFrameCountCheckTime = GetTickCount();
   }
}

void CIPCamStream_RTSP::OnMetaDataReceived( byte* s_buffer, int s_buf_size, timeval& timestamp )
{
   //logd ("%s", s_buffer);
   if( m_pCBRTSPMetadata ) {
      VC_PARAM_RTSP_META input;
      input.pParam      = m_pCBCamEventParam;
      input.pBuffer     = s_buffer;
      input.nBufferSize = s_buf_size;
      input.Timestamp   = timestamp;
      m_pCBRTSPMetadata( input );
   }
}

void CIPCamStream_RTSP::OnVideoSizeChanged( int nWidth, int nHeight )
{
   if( m_pInfo->m_nWidth != nWidth || m_pInfo->m_nHeight != nHeight ) {
      // NOTE(yhpark): SystemConfig.xml 의 <ICamInfo> 해상도(width,height) 정보와 실제 RTSP 해상도 보다 작을 경우,
      // 디코딩 된 영상을 담을 Camera_IP::s_srcImg 의 크기를 바뀐 해상도에 맞게 크게 다시 할당하지 않아서 오류 발생.
      // 오류가 나는 코드는 삭제함. 이유 : 해상도가 다를 경우, 다른 루트(해상도변경)를 타면서 정상작동하기 때문.

      logd( "[CH:%02d] RTSP::OnVideoSizeChanged [%dx%d] -> [%dx%d]",
            m_nChannelNo_IVX + 1, m_pInfo->m_nWidth, m_pInfo->m_nHeight, nWidth, nHeight );
   }
}

#ifdef __SUPPORT_ONVIF
/////////////////////////////////////////////////////////////////////////////
//
// class CIPCamStream_Onvif
//
/////////////////////////////////////////////////////////////////////////////

CIPCamStream_Onvif::CIPCamStream_Onvif()
{
   m_nCompanyID = CompanyID_Onvif;
}

CIPCamStream_Onvif::~CIPCamStream_Onvif()
{
}

int CIPCamStream_Onvif::BeginCapture()
{
   if( m_pInfo->m_strIPAddress.length() ) {
      CCriticalSectionSP co( m_csOnvifCtrlLock );

      logd( "[CH:%02d] ONVIF::BeginCapture START [IP: %s] [PTZPortNo: %d] [VideoStreamNo: %d]\n", m_nChannelNo_IVX + 1, m_pInfo->m_strIPAddress.c_str(), m_pInfo->m_nPTZPortNo, m_pInfo->m_nVideoStreamNo );

      COnvifCtrl onvifCtrl( m_pInfo->m_strIPAddress.c_str(), m_pInfo->m_nPTZPortNo, m_pInfo->m_strUserName.c_str(), m_pInfo->m_strPassword.c_str() );
      int nRet = onvifCtrl.InitOnvif();
      if( SOAP_OK == nRet || SOAP_TYPE == nRet ) {
         char* szURL              = onvifCtrl.GetStreamUrl( m_pInfo->m_nVideoStreamNo );
         char szURLTemp[MAX_PATH] = "";
         strcpy( szURLTemp, szURL );
         m_strRTSP_URL = szURLTemp;
         if( m_strRTSP_URL.length() > 5 )
            logd( "[CH:%02d] ONVIF::BeginCapture END [URL: %s]\n", m_nChannelNo_IVX + 1, m_strRTSP_URL.c_str() );
      }
   }

   return CIPCamStream_RTSP::BeginCapture();
}
#endif

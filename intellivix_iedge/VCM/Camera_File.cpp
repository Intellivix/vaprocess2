#include "stdafx.h"
#include "VCM.h"
#include "CameraProductAndItsProperties.h"
#include "Camera_File.h"
#include "vacl_ipp.h"

#ifdef __SUPPORT_VIDEO_FILE_CAMERA

CCamera_File::CCamera_File( TYPE_ANALYZE emType )
    : m_emType( emType )
{
   m_bRepeat               = TRUE;
   m_nCurFrame             = 0;
   m_nPlayTime             = 0;
   m_dfNormalPlayTime      = 0.0;
   m_FileCameraItem.first  = -1;
   m_FileCameraItem.second = "";
}

CCamera_File::~CCamera_File()
{
}

int CCamera_File::Initialize( CFileCameraInfo* pInfo )
{
   m_pInfo = pInfo;

   if( SINGLE_STREAMING == m_emType ) {
      if( !IsExistFile( m_pInfo->m_strPathName ) ) return ( 1 );
      if( m_FileReader.Open( m_pInfo->m_strPathName ) != DONE ) {
         logd( "ERROR: CCamera_File: OpenVideoStream\n" );
         return ( 2 );
      }

      m_pInfo->m_nWidth  = m_FileReader.FrameWidth;
      m_pInfo->m_nHeight = m_FileReader.FrameHeight;
      m_FileReader.Close();
      m_bRepeat = TRUE;
   } else if( OFFLINE == m_emType ) {
      m_bRepeat = FALSE;
   } else {
      return 1;
   }

   return ( DONE );
}

BOOL CCamera_File::IsEndAvi()
{
   int iNextPos = m_nCurFrame + 1;
   int iEndPos  = m_FileReader.NumFrames;

   if( iNextPos >= iEndPos )
      return TRUE;
   return FALSE;
}

int CCamera_File::SetFileCameraItem( const FileCameraItem& item )
{
   if( OFFLINE != m_emType )
      return 1;

   m_FileCameraItem = item;

   if( m_FileCameraItem.second.empty() )
      return 3;
   if( !IsExistFile( m_FileCameraItem.second ) )
      return 4;

   switch( m_FileCameraItem.first ) {
   case FILE_TYPE_AVI: {
      FFMPEG_FileReader reader;
      if( reader.Open( m_FileCameraItem.second.c_str() ) )
         return 5;

      m_pInfo->m_strPathName = m_FileCameraItem.second;
      m_pInfo->m_fFrameRate  = reader.FrameRate;
      m_pInfo->m_nWidth      = reader.FrameWidth;
      m_pInfo->m_nHeight     = reader.FrameHeight;

      m_pVideoChannel->m_fFPS_Cap = reader.FrameRate;
      m_pVideoChannel->m_nWidth   = reader.FrameWidth;
      m_pVideoChannel->m_nHeight  = reader.FrameHeight;

      reader.Close();
   } break;
   case FILE_TYPE_JPEG: {
      BGRImage imgFile;
      if( DONE != imgFile.ReadJPGFile( m_FileCameraItem.second.c_str() ) )
         return 5;

      m_pInfo->m_strPathName = m_FileCameraItem.second;
      m_pInfo->m_fFrameRate  = 30;
      m_pInfo->m_nWidth      = imgFile.Width;
      m_pInfo->m_nHeight     = imgFile.Height;

      m_pVideoChannel->m_fFPS_Disp = 30;
      m_pVideoChannel->m_fFPS_Cap  = 1000;
      m_pVideoChannel->m_fFPS_IP   = 1000;
      m_pVideoChannel->m_nWidth    = imgFile.Width;
      m_pVideoChannel->m_nHeight   = imgFile.Height;
   } break;
   default:
      return 5;
   }

   logd( "[%s] File = %s\n", __FUNCTION__, m_FileCameraItem.second );

   return ( DONE );
}

int CCamera_File::BeginCapture()
{
   if( m_nState & State_Capture ) return ( DONE );
   m_nState |= State_Capture;

   BOOL bVideo = TRUE;

   if( OFFLINE == m_emType ) {
      switch( m_FileCameraItem.first ) {
      case FILE_TYPE_AVI:
         bVideo = TRUE;
         break;
      case FILE_TYPE_JPEG:
         bVideo = FALSE;
         break;
      default:
         return 100;
      }
      m_bRepeat = TRUE;
   } else {
      m_bRepeat = FALSE;
   }

   m_YUY2Img.Create( m_pInfo->m_nWidth * m_pInfo->m_nHeight * 2 + FFMPEG_BUFFER_PADDING_SIZE * 4 );
   if( bVideo ) {
      if( m_FileReader.Open( m_pInfo->m_strPathName.c_str() ) ) {
         return ( 101 );
      }

      m_nPlayTime        = 0;
      m_dfNormalPlayTime = 0.0;
   } else {
      if( m_ImgFile.ReadJPGFile( m_pInfo->m_strPathName.c_str() ) )
         return 102;
   }

   m_nState |= State_ConnectionOk;

   return ( DONE );
}

int CCamera_File::EndCapture()
{
   if( !( m_nState & State_Capture ) ) return ( DONE );
   m_VideoDecoder.Close();
   m_FileReader.Close();
   m_nState &= ~( State_Capture | State_ConnectionOk );
   return ( DONE );
}

int CCamera_File::GetImage_Lock( byte** d_buffer )
{
   byte* pImage = (byte*)m_YUY2Img;
   if( !pImage )
      return GetImgeLockRet_ImageBufferNotExist;

   if( OFFLINE == m_emType ) {
      if( FILE_TYPE_JPEG == m_FileCameraItem.first ) {
         IPP_ConvertBGR24ToYUY2( m_ImgFile, pImage );
         *d_buffer = pImage;
         return ( DONE );
      } else if( FILE_TYPE_AVI == m_FileCameraItem.first ) {
         if( IsEndAvi() ) {
            return GetImgeLockRet_EndOfFile;
         }
      }
   }

   if( m_VideoDecoder.OpenDecoder( m_FileReader.VideoCodecCtx, m_pInfo->m_bHWDecoder ) != DONE )
      return ( GetImgeLockRet_OpenDecoderFailed );

   byte* s_buffer;
   int s_buf_size, flag_key_frame, time_pos, duration;
   if( m_FileReader.ReadVideoTrack( &s_buffer, s_buf_size, time_pos, duration, flag_key_frame ) ) {
      *d_buffer   = NULL;
      m_nCurFrame = 0;
      m_FileReader.Seek( 0 );
      return ( GetImgeLockRet_EndOfFile );
   }
   m_nCurFrame++;

   m_nPlayTime += ( uint )( 1000 / m_pInfo->m_fFrameRate );
   m_dfNormalPlayTime += 1.0 / m_pInfo->m_fFrameRate;

   AddToFileTime( m_ftTimeStamp, 0, 0, 0, 0, ( uint )( 1000 / m_pInfo->m_fFrameRate ) );
   timeval* ppresentation_time = NULL;
   timeval presentation_time;
   if( m_pInfo->m_bOfflineMaximumSpeedAnalytics ) {
      FileTimeToTimeval( &m_ftTimeStamp, &presentation_time );
      ppresentation_time = &presentation_time;
   }
   DWORD dwFlag = 0;
   if( flag_key_frame ) dwFlag |= VFRM_KEYFRAME;
   int nStreamIdx             = 0;
   VideoCodecID nVideoCodecID = m_VideoDecoder.m_nCodecID;
   if( m_pcbReceiveEncodeFrame ) m_pcbReceiveEncodeFrame( m_pVideoChannel, 0, 0, nStreamIdx, 0, nVideoCodecID, s_buffer, s_buf_size, dwFlag, ppresentation_time, m_dfNormalPlayTime );

   if( m_VideoDecoder.Decompress( s_buffer, s_buf_size, pImage ) ) {
      *d_buffer = NULL;
      return ( GetImgeLockRet_DecompressError );
   }

   *d_buffer = pImage;

   return ( GetImgeLockRet_Done );
}

int CCamera_File::GetImage_Unlock()
{
   return ( DONE );
}

int CCamera_File::GetColorSpace()
{
   return 0;
}

int CCamera_File::GetImageSize()
{
   return m_pInfo->m_nWidth * m_pInfo->m_nHeight;
}

void CCamera_File::GetCameraDesc( std::string& strDesc )
{
   strDesc = sutil::sformat( _T("%s: (%s)"), "Video File", m_pInfo->m_strPathName );
}

BOOL CCamera_File::IsVideoSignalAlive()
{
   BOOL bVideoSignalalive = ( m_nState & State_VideoSignalNotOK ) ? FALSE : TRUE;

   return bVideoSignalalive;
}

BOOL CCamera_File::CheckingVideoSignalAlive()
{
   BOOL bVideoSignalAlive = ( m_nState & State_Capture ) ? TRUE : FALSE;

   if( bVideoSignalAlive )
      m_nState &= ~State_VideoSignalNotOK;
   else
      m_nState |= State_VideoSignalNotOK;

   return bVideoSignalAlive;
}

#endif

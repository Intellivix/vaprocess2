﻿#pragma once

#ifdef __SUPPORT_PTZ_CAMERA

#include <mutex>
#include <thread>
#include "Common.h"
#include "PTZCtrlEx.h"
#include "CameraProductAndItsProperties.h"
#include "CameraInfo.h"
#include "PTZVector.h"
#include "PTZxD.h"
#include "MMTimer.h"

#include <thread>
//#include "ComChannel.h"

class CIPCameraInfo;
class CPTZCtrl;
class CIPCamStream;
class CSerial;
class PTZCtrlLogItemQueue;

const float MOUSE_WHEEL_ZOOM_UNIT = 0.25f;

///////////////////////////////////////////////////////////////////////////////
//
// Enumerations
//
///////////////////////////////////////////////////////////////////////////////

enum PTZ_State {
   PTZ_State_Connected              = 0x00000001,
   PTZ_State_ContMoveToBeStopped    = 0x00000002,
   PTZ_State_ThreadPTZControl       = 0x00000004,
   PTZ_State_XXXXXXXXX              = 0x00000008, // 사용안함
   PTZ_State_ToBeStopThread         = 0x00000010,
   PTZ_State_Stopped                = 0x00000020, // 연속이동의 경우 Stop 명령을 내린 후 약 500ms 이후가 되면 본상태가 된다.
   PTZ_State_ToBeStopped            = 0x00000040,
   PTZ_State_AbsolutePosBeExcuted   = 0x00000080,
   PTZ_State_AutoFocusToBeExcuted   = 0x00000100,
   PTZ_State_AutoFocusToBeStopped   = 0x00000200,
   PTZ_State_00000400               = 0x00000400,
   PTZ_State_OffsetByBox            = 0x00000800,
   PTZ_State_AutoFocus              = 0x00001000,
   PTZ_State_PosInitializing        = 0x00002000,
   PTZ_State_ShowMenu               = 0x00004000,
   PTZ_State_ZoomToBeStarted        = 0x00008000,
   PTZ_State_Zooming                = 0x00010000,
   PTZ_State_ZoomToBeStopped        = 0x00020000,
   PTZ_State_ContPTZTracking        = 0x00040000,
   PTZ_State_FirstGotoAbsPos        = 0x00080000,
   PTZ_State_ContPTZSpeedCheck      = 0x00100000,
   PTZ_State_InitHWOK               = 0x00200000,
   PTZ_State_SmoothZoomCtrl         = 0x00400000,
   PTZ_State_SignalNightModeOn      = 0x00800000,
   PTZ_State_SignalNightModeOff     = 0x01000000,
   PTZ_State_NightMode              = 0x02000000,
   PTZ_State_IROnOffJustChanged     = 0x04000000,
   PTZ_State_GetAbsPosToBeRequested = 0x08000000, // 실시간 절대각 수신이 가능하지 않는 PTZ 카메라에서 연속이동 멈춤명령 직 후
   // 1회에 한하여 절대각 송신요청 할 때 본 플레그를 On 시킨다.
   PTZ_State_UserCtrl            = 0x10000000, // 사용자에 의한 제어
   PTZ_State_AutomousPTZTracking = 0x20000000,
   PTZ_State_ContPTZTrackingMode = 0x40000000,
};

enum PTZMoveState {
   PTZMoveState_JustStarted = 0x00000001,
   PTZMoveState_Moving      = 0x00000002,
   PTZMoveState_JustStopped = 0x00000004,
};

enum PTZCmdType {
   PTZCmdType_Any                   = 0, // 연속이동
   PTZCmdType_ContinuousMove        = 1, // 연속이동
   PTZCmdType_GotoAbsPos            = 2, // 절대위치이동
   PTZCmdType_SetPreset             = 3, // 프리셋 설정
   PTZCmdType_GotoPreset            = 4, // 프리셋 이동
   PTZCmdType_ClearPreset           = 5, // 프리셋 초기화
   PTZCmdType_MenuOpen              = 6, // 메뉴 오픈
   PTZCmdType_MenuEnter             = 7, // 엔터
   PTZCmdType_MenuCancel            = 8, // 취소
   PTZCmdType_AuxOn                 = 9, // Aux On
   PTZCmdType_AuxOff                = 10, // Aux Off
   PTZCmdType_PosInit               = 11, // 위치 초기화
   PTZCmdType_SetIRMode             = 12, // IR 제어모드 변경
   PTZCmdType_SetIRLight            = 13, // IR 밝기 변경
   PTZCmdType_ReqABSPos             = 14, // PTZ 위치 얻기
   PTZCmdType_SetDefog              = 15, // 안개 제거
   PTZCmdType_BLCOn                 = 16, // 역광 보정 On/Off
   PTZCmdType_SetBLCLevel           = 17, // 역광 보정 레벨
   PTZCmdType_SetDZoomMode          = 18, // Digital Zoom Mode: 카메라 모듈에 디지털 줌 값을 미리 입력해 놓은 상태에서 디지털 줌을 켜고 끄는 기능.
   PTZCmdType_SetWiper              = 19,
   PTZCmdType_SetHeatingWire        = 20,
   PTZCmdType_SetFan                = 21,
   PTZCmdType_SetDayAndNightMode    = 22,
   PTZCmdType_SetLaserIlluminator   = 23, // 레이저 투광기 On/Off
   PTZCmdType_OneShotAutoFocus      = 24,
   PTZCmdType_ThermalCameraOn       = 25,
   PTZCmdType_SetZoomControlMode    = 26,
   PTZCmdType_SetAutoFocusOn        = 27,
   PTZCmdType_SetVideoStabilization = 28, // H/W 비디오 안정화 (mkjang-140616)
   PTZCmdType_SetSoftDefog          = 29,
};

enum LastPTZCmdMode {
   LastPTZCmdMode_ContinuousMove = 0,
   LastPTZCmdMode_GotoAbsPos     = 1,
};

enum PTZIRCtrlMode {
   PTZIRCtrlMode_Auto              = 0,
   PTZIRCtrlMode_DayMode           = 1,
   PTZIRCtrlMode_NightModeAndIROff = 2,
   PTZIRCtrlMode_NightModeAndIROn  = 3,
};

enum GotoPreset_HWRet {
   GotoPreset_HWRet_Done         = 0,
   GotoPreset_HWRet_ByGotoAbsPos = 1,
};

enum PTZCtrlOpenMode {
   PTZCtrlOpenMode_Normal     = 0, // PTZ제어 객체를 생성하고 모든 기능을 동작시킨다.
   PTZCtrlOpenMode_CreateOnly = 1, // PTZ제어 객체만 생성한다. 모든 스레드는 생성 안 됨.
};

const int g_nMaxCmdLen = 128;

///////////////////////////////////////////////////////////////////////////////
//
// Class: CPTZStatusCallback
//
///////////////////////////////////////////////////////////////////////////////

class CPTZStatusCallback {
public:
   virtual void OnPTZMoveStarted() {}
   virtual void OnPTZMoveStopped() {}
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: CPTZCtrlCallback
//
///////////////////////////////////////////////////////////////////////////////

class CPTZCtrlCallback {
public:
   virtual void ContPTZMove( FPTZVector move_vel ) = 0;
   virtual int GotoAbsPos( FPTZVector pv )         = 0;
};

class CContAbsPosTrack;

///////////////////////////////////////////////////////////////////////////////
//
// Class: CPTZCtrl
//
///////////////////////////////////////////////////////////////////////////////

// IntelliVIX에서는 PTZ 카메라의 좌표계로 오른손 좌표계를 사용한다.
// PTZ 카메라들 중에 오른손 좌표계를 사용하지 않는 카메라들은 적절한 변환이 필요함.
class CPTZCtrl {
public:
   // Speed Range
public:
   int MinPanSpeed;
   int MaxPanSpeed;
   int MinTiltSpeed;
   int MaxTiltSpeed;
   int MinZoomSpeed;
   int MaxZoomSpeed;
   int MinFocusSpeed;
   int MaxFocusSpeed;
   int MinIrisSpeed;
   int MaxIrisSpeed;

   // Pan, Tilt, Zoom, Focus, Iris Range
public:
   float MinPanAngle;
   float MaxPanAngle;
   float MinTiltAngle;
   float MaxTiltAngle;
   float MinZoomFactor;
   float MaxZoomFactor;
   float MinFocus;
   float MaxFocus;
   float MinIris;
   float MaxIris;

public:
   float MaxAbsPosPanMoveSpeed;
   float MaxAbsPosTiltMoveSpeed;
   uint AbsPosMoveDelayTimeInMilliSec;

public:
   int MinPresetNo;
   int MaxPresetNo;

public:
   int MaxDefogLevel;
   int MaxBLCLevel;
   uint MaxWiperOnTimeInSec;

public:
   int m_nCamAddr; // 카메라 주소.
   int m_nRecvCmdCnt; // 수신 명령 카운트.
   int m_nSendCmdLen; // 송신 명령 카운트.
   int m_nSendCmdCntNet; // 네트워크를 통하여 송신되는 명령의 카운트.
   int m_nSendReqCmdLen;
   int m_nSendReqCmdCntNet;
   int m_nCmdPresetNo;
   int m_nCmdAuxNo;
   int m_nCmdIRMode;
   int m_nCmdIRLight;
   int m_nCmdDefogLevel;
   int m_nCmdVideoStabilization; // H/W 비디오 안정화 (mkjang-140616)
   BOOL m_nCmdBLCOn;
   int m_nCmdBLCLevel;
   BOOL m_bCmdAutoFocusOn;
   BOOL m_bCmdDZoomOn;
   BOOL m_bCmdWiperOn;
   BOOL m_bCmdHeatingWireOn;
   BOOL m_bCmdFanOn;
   int m_nCmdDayAndNightMode;
   uint32_t m_nWiperOnTime;
   BOOL m_bCmdLaserIlluminatorOn;
   BOOL m_bCmdThermalCameraOn;
   BOOL m_nCmdZoomControlType;
   int m_nChannelNo_IVX;
   uint32_t m_nSendCmdPeriod;
   uint m_nPTZCmdExecDelay;
   uint32_t m_nPrvReqAbsPosTime;
   uint32_t m_nLastSendCmdTime;
   uint32_t m_nSetIRModeTime; // _SetIRMode 함수가 호출된 시간
   uint32_t m_nContPTZMoveToBeStoppedCheckTime;
   uint32_t m_nContPTZMoveStartTime;
   BOOL m_bSendReqCmd;
   BYTE m_sCmd[g_nMaxCmdLen];
   BYTE m_ReqCmd[g_nMaxCmdLen * 2]; // 요청 명령. 요청을 하면 응답패킷이 수신된다.
   BYTE m_ResCmd[g_nMaxCmdLen * 2]; // 응답 명령. 요청에 의한 응답임.
   BYTE m_byRecvHeader;
   BYTE m_byRecvHeader2;

public:
   int m_nState;
   int m_nPTZMoveState;
   UINT64 m_nCapabilities;
   int m_nOpenMode;
   int m_nPTZControlType;
   int m_nCurZoomModuleType;
   int m_nCurrCmdType;
   int m_nPrevCmdType;
   int m_nLastPTZCmdMode;
   int m_nAbsPosStopCount;
   uint32_t m_nPTZMoveStopCmdTick;
   uint32_t m_nGotoAbsPosMovingTime;
   uint32_t m_nGotoAbsPosCompleteTime;
   uint32_t m_nGotoAbsPosTime;
   BOOL m_bGetAbsPosError;
   BOOL m_bPrvGetAbsPosError;
   BOOL m_bRequestStandMode; // 카메라가 똑바로 또는 거꾸로 세워졌는지에 대한 모드.
   BOOL m_bConnectionProcNeed;
   //uint32_t m_nGetAbsPosErrorStartTime;
   uint32_t m_nDelayedGetAbsPosTime;
   uint32_t m_nWiperOffTime;
   uint m_nReconnectionPeriod; // PTZ 연결 재접속 주기. 단위:ms

   CIPCameraInfo* m_pIPCamInfo;
   CPTZCtrlInfo m_PTZInfo;
   PTZControlProperty* m_pPTZControlProperty;
   CCriticalSection m_csSendCmd;
   CIPCamStream* m_pStream;
   CStreamSocketEx m_StreamSocket;
   CMMTimer* m_pTimerMainThread;
   CMMTimer* m_pTimerNetThread;
   CTimerManager* m_pTimerMgr;
   CContAbsPosTrack* m_pContAbsPosTracker;
   PTZCtrlLogItemQueue* m_pPTZCtrlLogItemQueue;
   CPTZStatusCallback* m_pPTZStatusCallback;
   CPTZCtrlCallback* m_pPTZCtrlCallback;
   CCriticalSection m_csCmdSet;

public:
   int m_nPTZIRCtrlMode;
   int m_nCustomIRLightCtrlMode;
   BOOL m_bUseIRLightCtrlInfo;
   CIRLightCtrlInfo* m_pOuterIRLightCtrlInfo;

protected:
   MapF2FTblInfo m_ZoomReal2HWMapInfo;
   MapF2FTblInfo m_Zoom2FLMapInfo;
   MapF2FTblInfo m_PanAngleVel2HWSpdMapInfo;
   MapF2FTblInfo m_TiltAngleVel2HWSpdMapInfo;
   MapF2FTblInfo m_ZoomLogVel2HWSpdMapInfo;
   MapF2FTblInfo m_GotoAbsVel2HWSpdMapInfo;
   MapF2FTblInfo m_Zoom2PTSpdRatioMapInfo;
   float* m_PanAngleVelocityTable;
   float* m_TiltAngleVelocityTable;
   float* m_AngleSpeedCheckZoomTable;
   int m_nAngleSpeedCheckZoomTableItemNum;

protected:
   int m_nContPTZMoveStopCmdCount;
   FPTZVector m_PrevPTZMoveHWForPosEst; // 이전 HW 연속이동 값 - 위치 예측용
   FPTZVector m_CurrPTZMoveHW; // 현재 HW 연속이동 값. 사용자 입력에 의한 제어값 또는 연속이동에의한 절대위치이동에 의한 제어값이 이다.
   FPTZVector m_InputPTZMoveHW; // 사용자 입력 HW연속이동 값
   FPTZVector m_InputPTZMoveHW_Prev; // 이전 사용자 입력 HW연속이동 값
   FPTZVector m_OffsetPos;

protected:
   FPTZVector m_TgtPTZPos; // 절대각 이동 명령시에 이동해야할 절대각 값을 저장하는 변수.
   FPTZVector m_CorrectedTgtPTZPos; // 보정된 절대각 위치. 여기서 보정은 다음과 같이 2종류가 있다.
   ; // 1. PTZ 카메라의 광축중심 위치 보정
   ; // 2. 부드러운 줌제어를 위한 줌 이동값 보정.
   FPTZVector m_CurPTZPos;
   FPTZVector m_PrevPTZPos;
   FPTZVector m_PrevAvgPTZPos;
   FPTZVector m_PrevPTZPosZE; // 줌값 예측 시에 사용되는 이전 PTZ 위치값
   FPTZVector m_PrevPTZPosSC; // PTZ 멈춤 체크를 위한 이전 PTZ 위치값
   FPTZVector m_PTZPosDiff;
   FPTZVector m_CurPTZPos_ThermalImaging; // 열화상 카메라의 현재 절대 위치
   FPTZVector m_CurPTZPos_WideAngle; // 광각 카메라의 현재 절대 위치

protected:
   //uint32_t m_nPrevTime;
   uint32_t m_nZoomPosChangedTime;
   uint32_t m_nZoomStartTime; // 줌 명령이 시작된시간.
   uint32_t m_nZoomStopTime; // 줌 명령이 종료된시간.
   uint m_nZoomMovePeriod_Estimate; // 목표 줌까지 이동하는데 걸리는 예상(계산)된시간
   uint32_t m_nPTZStopCmdTime; // PTZ 멈춤 명령이 마지막으로 전송된 시간.
       // PTZ 멈춤 명령은 연속이동에서 PTZ 모두 0의 속도로 설정하거나
       // 절대각 명령을 보내는 것으로 한다.
   BOOL m_bOnceReceivePanTiltPos;
   uint32_t m_nReqAbsPosTime; // 절대위치 요청을 보낸 시간
   uint32_t m_nGetAbsPosTime; // 절대위치 요청에의한 응답을 받은시간.
   float m_fZoomMoveStartDir; // 줌 명령이 시작되었을 때의 방향 (단일 PTZ 추적)
   float m_fZoomEstimate; // 계산된 현재 줌값
   float m_fZoom_Start; // 줌 명령이 시작될 때의 줌값. CPTZCtrl::Zoom은 목표 줌값을 뜻한다.
   float m_fMaxFocusMoveTime;
   float m_fFocalLength; // Focal Length (x 축)
   float m_fFocalLengthRatio; // x축 FL와 y축 FL과의 비율 (x/y)
   float m_fZoomLogVel_ContSpd1; // 연동이동 방식으로 줌 이동을 하고 있을 때 단위 시간(1초)당 변화된 log (줌값)을 말한다.
       // 줌 속도가 가장 느린 1단계의 속도이다.
   float m_fZoomLogVel_AbsPosMove;

   ISize2D m_s2VideoSize; // 현재의 비디오영상 크기를 나타낸다.
   FBox2D m_b2TargetBox;
   FSize2D m_s2RefImgSize_FL; // Focal Length를 측정할 당시의 영상의 크기.

public:
   CEventEx m_evtWait;
   CPTZ2D* m_pPTZ2D;
   CPTZCtrl* m_pMainPTZControl; // 메인 PTZ 제어 클래스. 명령을 보내는 역할을 함.
   CPTZCtrl* m_pSubPTZControl; // 하위 PTZ 제어 클래스. 명령을 만드는 역할을 함.
   CPTZCtrl* m_pLinkingPTZControl;
   CCriticalSection m_csLinkingPTZControl;

   // Continuous PTZ Speed Check
public:
   BOOL m_bPanSpeedCheck;
   BOOL m_bTiltSpeedCheck;
   BOOL m_bLimitPanAngleRange;
   int m_nSpeedCheckCount;
   float m_fMinPanAngleForSpeedCheck;
   float m_fMaxPanAngleForSpeedCheck;
   float m_fMinTiltAngleForSpeedCheck;
   float m_fMaxTiltAngleForSpeedCheck;
   int m_nPanSpeedChangeWaitTime;
   int m_nTiltSpeedChangeWaitTime;
   int m_nPanDirectionChangeWaitTime;
   int m_nTiltDirectionChangeWaitTime;
   BOOL m_bLogZoomStep;
   float m_fStartZoomPos;
   float m_fEndZoomPos;
   int m_nHWSpeedStep;
   float m_fLog2ZoomStepSize;

#if defined( __linux )
   float m_fWaitTime  = 1;
   float m_fWaitTime2 = 100;
#else
   float m_fWaitTime  = 0;
   float m_fWaitTime2 = 0;
#endif
public:
   CPTZCtrl();
   virtual ~CPTZCtrl();

   // Virtual 함수 정의
public:
   virtual void SignalToClose();
   virtual void Close();

protected:
   virtual void PreOpen();
   virtual void PostOpen();

public:
   virtual int Open( CPTZCtrlInfo* pPTZCtrlInfo );
   virtual int Open( CIPCameraInfo* pIPCamInfo );
   virtual int Connect();
   virtual int Disconnect();
   virtual BOOL IsPTZConnected();
   virtual BOOL IsPTZCtrlReady();
   virtual BOOL IsPTZReady();
   virtual BOOL _InitHW();
   virtual BOOL IsFisheyeCam()
   {
      return FALSE;
   }

protected:
   virtual void Clear( BYTE* pCmd, int nCmdLen ){};
   virtual void CheckSum( BYTE* pCmd, int nCmdLen ){};
   virtual int _PopCommand();
   virtual int _ContPTZMove( IPTZVector move_vec );
   virtual int _GotoAbsPos( FPTZVector abs_pos );
   virtual int _GetAbsPosReqCmd( BYTE* pCmd, int& nCmdLen );
   virtual void _GetStandModeCmd( BYTE* pCmd, int& nCmdLen );
   virtual int _SetPreset( int preset_no );
   virtual int _ClearPreset( int preset_no );
   virtual int _GotoPreset( int preset_no );
   virtual int _AuxOn( int nAuxNo );
   virtual int _AuxOff( int nAuxNo );
   virtual int _MenuOpen();
   virtual int _MenuEnter();
   virtual int _MenuCancel();
   virtual int _PosInit();

public:
   virtual int _SetIRMode( int nIRMode );
   virtual int _SetIRLight( int nIRLight );

protected:
   virtual int _SetAutoFocusOn( int bAutoFocusOn ); // 자동 포커스
   virtual int _BLCOn( BOOL bBLCOn );
   virtual int _SetBLCLevel( int nBLCLevel ); // BLC (Back Light Compensation) : 역광보정
   virtual int _SetDZoomMode( BOOL bDZoomOn ); // Digitlal Zoom On/Off
   virtual int _SetWiper( BOOL bWiperOn );
   virtual int _SetHeatingWire( BOOL bHeatingWireOn );
   virtual int _SetFan( BOOL bFanOn );
   virtual int _SetDayAndNightMode( BOOL bDayAndNightMode );
   virtual int _SetLaserIlluminator( BOOL bLaserIlluminatorOn );
   virtual int _OneShotAutoFocus();
   virtual int _ThermalCameraOn( BOOL bThermalCameraOn );
   virtual int _SetZoomControlMode( int nZoomContrlType, BOOL bSendHWCmd = FALSE );
   virtual int _SetVideoStabilization( int nVideoStabilization ); // 비디오 안정화 (mkjang-140616)
   virtual int _ChangeMaxPTSpeed( BOOL bZoomProportionalJog );
   virtual int _SetDefog( int nDefogLevel ); // 안개 제거
   virtual int _SetSoftwareDefog( int nDefogLevel ); // 안개 제거

public:
   int SetPreset( int preset_no );
   int ClearPreset( int preset_no );
   int GotoPreset( int preset_no );
   int AuxOn( int nAuxNo );
   int AuxOff( int nAuxNo );
   int MenuOpen();
   int MenuEnter();
   int MenuCancel();
   int PosInit();
   int SetIRMode( int nIRMode );
   int SetIRLight( int nIRLight );
   int SetAutoFocusOn( BOOL bAutoFocusOn );
   int SetDefog( int nDefogLevel );
   int SetSoftDefog( int nDefogLevel );
   int SetVideoStabilizer( BOOL bVideoStabilizer ); // 비디오 안정화 (mkjang-140616)
   int BLCOn( BOOL bBLCOn );
   int SetBLCLevel( int nBLCLevel );
   int SetDZoomMode( BOOL bDZoomOn );
   int SetWiper( BOOL bWiperOn );
   int SetHeatingWire( BOOL bHeatingWireOn );
   int SetFan( BOOL bFanOn );
   int SetDayAndNightMode( BOOL bDayAndNightMode );
   int SetLaserIlluminator( BOOL bLaserIlluminatorOn );
   int OneShotAutoFocus();
   int ThermalCameraOn( BOOL bThermalCameraOn );
   int SetZoomControlMode( int nZoomModuleType );
   int SetActiveZoomCtrl();

public:
   virtual int GetWaitingTime( float pd );
   virtual int GetWaitingTime( float dp, float dt, float dzl ); // poa: Pan Offset Angle, toa: Tilt Offset Angle, zlo: Zoom Log Offset

protected:
   virtual int SendCommand();
   virtual int SendCommand( BYTE* pCmd, int nCmdLen, int nCmdType = PTZCmdType_Any );
   virtual int SendCommandEx( BYTE* pCmd, int& nCmdLen );
   virtual int ReqAbsPos();
   virtual void ReqStandMode();
   virtual void CheckPanTiltPositionInReal( float& pa, float& ta );
   virtual void CheckPanTiltPositionInHW( float& pa, float& ta );
   virtual void ConvertPanTiltOrientationRealToHW( float& pa, float& ta );
   virtual void ConvertPanTiltOrientationHWToReal( float& pa, float& ta );
   virtual int GetMovingTime( FPTZVector pos1, FPTZVector pos2 );
   virtual int GetAbsPos_HW( float& pa, float& ta, float& zf, float& f );
   virtual int OffsetPositionByPTZ();
   virtual int OffsetPositionByBox();
   virtual int CopyZoomModuleFactors( int nZoomModuleType, CPTZCtrl* pDestPTZCtrl );

public:
   BYTE GetCheckSum( BYTE* pCmd, int nCmdLen );
   virtual void ContPTZMoveAV( FPTZVector pvContMoveAV, BOOL bPropPT = TRUE ); // 초당 이동각속도를 입력으로 받음.
   void ContPTZMoveHW( FPTZVector pvContMoveHW, BOOL bPropPT = TRUE ); // HW 속도값을 입력으로 받음.
protected:
   void _ContPTZMove( FPTZVector pvContMoveHW );

public:
   int GetAbsPos( float& pa, float& ta, float& zf );
   int GetAbsPos( float& pa, float& ta, float& zf, float& f );
   int GetAbsPos( FPTZVector& pv );
   FPTZVector GetAbsPos();
   FPTZVector GetAbsPos( int nZoomModuleType );
   FPTZVector GetAbsPosOfPreset( int nPresetNo );
   FPTZVector GetTgtAbsPos();
   FPTZVector GetOffsetTgtAbsPos();
   float GetCurrZoom();
   int GotoAbsPos( FPTZVector abs_pos );
   int GotoAbsPosByConstantAngleSpeed( FPTZVector abs_pos, float fAngleSpeed );
   int GotoAbsPosByConstantImageScrollSpeed( FPTZVector abs_pos, float fImageScrollSpeed );
   void SetAbsPos( FPTZVector abs_pos );
   void SetAbsPos( int nZoomModuleType, FPTZVector abs_pos );
   void SetTgtAbsPos( FPTZVector abs_pos );
   void SetRefCameraParams( float fFocalLength, float fHFOV ); // 가상 PTZ카메라에서만 사용되는 함수.

public:
   void GetRange_PanAngle( float& min_angle, float& max_angle );
   void GetRange_PanSpeed( int& min_speed, int& max_speed );
   void GetRange_TiltAngle( float& min_angle, float& max_angle );
   void GetRange_TiltSpeed( int& min_speed, int& max_speed );
   void GetRange_ZoomFactor( float& min_zf, float& max_zf );
   void GetRange_ZoomSpeed( int& min_speed, int& max_speed );
   void GetRange_PresetNo( int& min_preset_no, int& max_preset_no );

protected:
   void CopyFrom( CPTZCtrl* pFrom );
   void CheckContMoveStop( FPTZVector& fvContPTZMoveHW );
   void CheckPTZMoving();
   BOOL IsNeedConnectionProc();

public:
   BOOL IsContPTZMoveStopped();
   BOOL IsContPTZFIMoveStopped();
   BOOL IsAbsMoveStopped();
   BOOL IsPTZFMoving();
   BOOL IsSinglePTZTrkSupported();
   BOOL IsRealTimeGetAbsPosSupported();
   BOOL IsRealTimeGetAbsPosMode();
   BOOL IsGetAbsPosSupported();
   BOOL IsSetAbsPosSupported();
   BOOL IsPresetMoveByUserSpecifiedSpeedSupported();
   BOOL IsBadAbsPosMoveCam();
   BOOL IsIRLightOn();
   BOOL IsPTZPosInitSupported();
   BOOL IsPresetByPTZPresetMove();
   BOOL IsTxOnlyMode();
   BOOL IsSamePTZPos();

   BOOL DoesNotReleasePropPanTiltSpeedOpt();
   int OffsetPosition( float cx, float cy, int zt = 0 ); // zt: Zoom Module Type
   int OffsetPosition( FBox2D tgt_box, int zt = 0 ); // zt: Zoom Module Type
   void SetOpenMode( int nOpenMode );
   void SetSWAutoFocus( int flag_on );

public:
   void SetContPTZTrackMode( BOOL bContPTZTrack );
   BOOL IsContPTZTrackMode();
   void SetUserCtrlMode( BOOL bUserCtrl );
   void SetSmoothZoomCtrlMode( BOOL bSmoothPTZCtrl );
   void ResetAvgTgtVelocity();
   void SetPTZ2D( CPTZ2D* ptz2d );
   int GetCurrContPTZMoveSpeed( FPTZVector& pvCurPTZMoveSpeed );
   int GetPTZControlTypeOfPTZCmd();
   void GetMaxOffsetAngle( float& pa, float& ta );
   void GetOffsetAngle( int nSrcWidth, int nSrcHeight, float x, float y, float& pa, float& ta );
   int GetPTAngle2ImgPos( float pa, float ta, float& x, float& y );
   int GetPTZPos( FPoint2D ptPos, ISize2D s2Src, FPTZVector& pvTgtPos );
   int GetPTZPos( FPoint2D ptPos, FPTZVector pvCurPos, ISize2D s2Src, FPTZVector& pvTgtPos );
   int GetImagePnt( FPTZVector pvPos, ISize2D s2Src, FPoint2D& ptPos );
   int AddToRecvCmd( BYTE* pBuff, int nLen );
   int ParseRecvCmd( BOOL bShiftToHead = TRUE );
   ISize2D GetVideoSize();
   void SetVideoSize( ISize2D video_size );
   float GetFocalLength();
   float GetFocalLength( float zoom );
   float GetHFOV();
   FSize2D GetRefImgSizeForcalLengthEstimation();

protected:
   uint32_t GetMaxZoomInTime();
   uint32_t GetZoomMoveTime( float zoom_start, float zoom_end );
   float GetZoomLogSpeed( int nZoomSpeed );
   int GetPTZMovePeriodByOffsetPos( FPTZVector offset_pos );
   int GetPTZMovePeriodByTargetPos( FPTZVector tgt_pos );
   float GetHorizontalFOV(); // 수평방향 Field Of View (단위: 도)
   float GetVerticalFOV(); // 수직방향 Field Of View (단위: 도)
   void CheckAbsPosRes();
   void SignalPanTiltPosReceived();

protected:
   float GetZoomHW( float zoom_real );
   float GetZoomReal( float zoom_hw );
   float GetFocalLength( float zoom, FSize2D s2VideoSize );
   float GetPositionDifference( FPTZVector& o_pos );
   uint GetLastSendCmdTime();
   void ConvertAngle2HWVel( FPTZVector angle_vel, FPTZVector& hw_vel );
   BOOL IsSendingContPTZFIMoveCmd();
   void WaitingInMilliSec( uint nMilliSec );

   int SendCommandByOuterPTZControl();
   int SetMapTable( int map_type, MapF2F* map_table, int tbl_size, int itp_mode );
   void PTZPosEstProc();
   void AddPTZCtrlLogItem();
   void IRLightCtrlProc();
   void MainProc();
   void NetPTZClientProc();

protected:
   std::thread m_ThreadMainProc;
   std::thread m_ThreadNetPTZClient;

   static UINT Thread_MainProc( LPVOID params );
   static UINT Thread_NetPTZClient( LPVOID params );

public:
   int m_nCurIRLightValueOnIRArea;
   int m_nCurIRLightValue;
   int m_nPrvIRLightValue;
   uint32_t m_nIRLightChangeTime;
   uint32_t m_nSetIRLightTime;

public:
   virtual int ReadParameter( CXMLIO* pXMLIO );
   virtual int WriteParameter( CXMLIO* pXMLIO );

public:
   BOOL IsSameResCmd( BYTE byte1, BYTE byte2, BYTE byte3, BYTE byte4 );

   // PTZ 속도 측정관련 맴버들.
public:
   BOOL m_bSignalStopContSpdChkThread;
   FPTZVector m_PrevChkPTZPos;
   CEventEx m_evtWaitCSC;
   CEventEx m_evtRecvAbsPos;
   std::thread m_ThreadContSpdChk;
   CMMTimer* m_pTimerContSpdChk;

   void StartContSpdChk(); // 연속이동 속도 측정시작.
   void StopContSpdChk(); // 연속이동 속도 측정중지.
   void ContSpdChkProc(); // 연속이동 속도 측정프로시저.
   void WriteContSpeedChkFile( const std::string& strFileName, FArray2D& panSpeedArray, FArray2D& tiltSpeedArray );
   static UINT Thread_ContSpdChk( LPVOID params );
   // Support  PTZCtrl Omnicast
   virtual void _Lock( BOOL bLock ) {}

   friend class CContAbsPosTrack;
   friend class CVideoChannel;
   friend class CPTZCtrl_Convex;
   friend class CPTZCtrl_ShinwooTech;
   friend class CPTZCtrl_Linking;
   friend class CPTZCtrl_SamsumgTechWin_iPOLiS;
};

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

CPTZCtrl* CreatePTZControl( int nPTZControlType, ISize2D s2VideoSize );

#endif // __SUPPORT_PTZ_CAMERA

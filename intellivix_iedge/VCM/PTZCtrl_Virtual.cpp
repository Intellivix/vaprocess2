#include "stdafx.h"
#include "PTZCtrl_Virtual.h"

#ifdef __SUPPORT_PTZ_CAMERA
#ifdef __SUPPORT_VIRTUAL_PTZ_CAMERA

#include "CameraProductAndItsProperties.h"
#include "Camera_VirtualPTZ.h"
#include "SupportFunction.h"

///////////////////////////////////////////////////////////////////////////////
//
// Class: CPTZCtrl_Virtual
//
///////////////////////////////////////////////////////////////////////////////

CPTZCtrl_Virtual::CPTZCtrl_Virtual( int nPTZControlType, ISize2D s2VideoSize )
{
   m_nPTZControlType = nPTZControlType;

   m_nSendCmdPeriod = 33;
   m_CurPTZPos( 0.0f, 0.0f, 1.0f, 0.0f, 0.0f );

   m_nPrevTime         = 0;
   m_pVirtualPTZCamera = NULL;

   // MasterToSlaveCamera 부모클래스 멤버변수 초기화
   FocalLength      = 500;
   HFOV             = 60.0f;
   ImageSize.Width  = 320;
   ImageSize.Height = 240;

   m_bIsSrcFisheyeCam = FALSE;
}

int CPTZCtrl_Virtual::Open( CCamera_VirtualPTZ* pVirtualPTZCemera )
{
   PreOpen();
   m_pPTZControlProperty = GetPTZControlProperty( m_nPTZControlType );
   m_nCapabilities       = m_pPTZControlProperty->m_nCapabilities;
   m_pVirtualPTZCamera   = pVirtualPTZCemera;
   ImageSize             = m_pVirtualPTZCamera->m_SrcImageSize;
   PostOpen();
   m_nState |= PTZ_State_Connected;

   return ( DONE );
}

int CPTZCtrl_Virtual::_GotoAbsPos( FPTZVector abs_pos )
{
   if( DONE != CPTZCtrl::_GotoAbsPos( abs_pos ) )
      return ( 1 );

   if( m_bIsSrcFisheyeCam && m_pVirtualPTZCamera->m_pVC ) {
      if( abs_pos.P < 0 ) abs_pos.P += 360.0f;
      m_pVirtualPTZCamera->m_pVC->m_fPan  = 360.0f - abs_pos.P;
      m_pVirtualPTZCamera->m_pVC->m_fTilt = abs_pos.T;
      float VFOV                          = 90.0f / abs_pos.Z;
      float f_length                      = GetFocalLengthFromVFOV( m_s2VideoSize.Height, VFOV );

      m_pVirtualPTZCamera->m_pVC->m_FisheyeInfo_VPTZ.f_length = f_length;
   } else {
      SetAbsPos( abs_pos );
   }
   // jun : 절대각 제어시 연속 이동 속도를 0으로 초기화 시켜야 Pan, Tilt 방향으로 흐르는 현상을 방지할 수 있다.
   m_CurrPTZMoveHW( 0.0f, 0.0f, 0.0f, 0.0f, 0.0f );
   return ( DONE );
}

int CPTZCtrl_Virtual::GetWaitingTime( float pd )
{
   return ( 0 );
}

int CPTZCtrl_Virtual::OffsetPositionByBox()
{
   if( m_bIsSrcFisheyeCam ) {
      FPoint2D coord;
      coord.X = ( m_b2TargetBox.X + 1 ) / 2 * m_s2VideoSize.Width;
      coord.Y = ( m_b2TargetBox.Y + 1 ) / 2 * m_s2VideoSize.Height;

      if( m_pVirtualPTZCamera->m_pVC ) {
         m_pVirtualPTZCamera->m_pVC->GotoAbsPos_Fisheye( coord, m_TgtPTZPos.Z );
         m_TgtPTZPos.P = m_pVirtualPTZCamera->m_pVC->m_fPan;
         m_TgtPTZPos.T = m_pVirtualPTZCamera->m_pVC->m_fTilt;
      }
   } else {
      float zoom = m_TgtPTZPos.Z;
      if( m_nState & ( PTZ_State_Zooming | PTZ_State_ZoomToBeStarted ) )
         zoom = m_fZoomEstimate;

      if( zoom < 1.0f )
         zoom = 1.0f;
      else if( zoom > MaxZoomFactor )
         zoom = MaxZoomFactor;

      float ox = m_b2TargetBox.X + m_b2TargetBox.Width * 0.5f;
      float oy = m_b2TargetBox.Y + m_b2TargetBox.Height * 0.5f;
      ox *= m_s2VideoSize.Width * 0.5f;
      oy *= m_s2VideoSize.Height * 0.5f;
      ox /= m_TgtPTZPos.Z / m_pVirtualPTZCamera->m_fZoomMagnifyingFactor;
      oy /= m_TgtPTZPos.Z / m_pVirtualPTZCamera->m_fZoomMagnifyingFactor;
      IPoint2D& cp                 = m_pVirtualPTZCamera->m_p2CenterPos;
      PTZPosition ptz_offset_moved = GetPTPosition( FPoint2D( cp.X + ox, cp.Y + oy ) );

      m_TgtPTZPos.P = ptz_offset_moved.PanAngle;
      m_TgtPTZPos.T = ptz_offset_moved.TiltAngle;

      // 줌이동량을 계산한다.
      if( m_b2TargetBox.Width > 2.0f || m_b2TargetBox.Height > 2.0f ) {
         float zoom_ratio = 1.0f;

         if( m_b2TargetBox.Width > m_s2VideoSize.Width ) m_b2TargetBox.Width    = (float)m_s2VideoSize.Width;
         if( m_b2TargetBox.Height > m_s2VideoSize.Height ) m_b2TargetBox.Height = (float)m_s2VideoSize.Height;

         float img_ratio = float( m_s2VideoSize.Height ) / m_s2VideoSize.Width;
         float box_ratio = m_b2TargetBox.Height / m_b2TargetBox.Width;

         if( box_ratio < img_ratio )
            zoom_ratio = m_s2VideoSize.Width / m_b2TargetBox.Width;
         else
            zoom_ratio = m_s2VideoSize.Height / m_b2TargetBox.Height;

         zoom *= zoom_ratio;
      }

      if( zoom < 1.0f )
         zoom = 1.0f;
      else if( zoom > MaxZoomFactor )
         zoom = MaxZoomFactor;

      if( !( m_nState & ( PTZ_State_Zooming | PTZ_State_ZoomToBeStarted ) ) )
         m_TgtPTZPos.Z = zoom;
   }

   return ( DONE );
}

void CPTZCtrl_Virtual::VirtualPTZProc()
{
   if( !IsPTZConnected() ) return;
   if( !m_pVirtualPTZCamera ) return;

   FPTZVector tmpCurPTZPos = m_CurPTZPos;

   if( IS_SUPPORT( __SUPPORT_FISHEYE_DEWARPING ) ) {
      if( m_pVirtualPTZCamera->m_pVCSrc ) {
         if( m_pVirtualPTZCamera->m_pVCSrc->IsFisheyeCamera() ) {
            CPTZCtrl::MinTiltAngle = 0.0f;
            m_bIsSrcFisheyeCam     = TRUE;
         } else {
            CPTZCtrl::MinTiltAngle = -90.0f;
         }
      }
   }

   if( m_bIsSrcFisheyeCam ) {
      if( m_pVirtualPTZCamera->m_pVC ) {
         m_pVirtualPTZCamera->m_pVC->GetPtzPosition_Fisheye( tmpCurPTZPos, m_s2VideoSize.Height );
         tmpCurPTZPos.P = 360.0f - tmpCurPTZPos.P;
      }
   }

   ImageSize = m_pVirtualPTZCamera->m_SrcImageSize;
   // 1프레임 경과시간 측정.
   uint nElapsed                = m_nSendCmdPeriod;
   if( m_nPrevTime ) nElapsed   = GetTickCount() - m_nPrevTime;
   float fElapsedInSec          = 0.0f;
   if( nElapsed ) fElapsedInSec = float( nElapsed ) / 1000.0f;
   //fElapsedInSec = 0.033f;
   m_nPrevTime = GetTickCount();

   // 연속이동을 수행하는 경우 Pan, Tilt 절대위치 업데이트
   tmpCurPTZPos.P += -m_CurrPTZMoveHW.P * fElapsedInSec;
   tmpCurPTZPos.T += m_CurrPTZMoveHW.T * fElapsedInSec;

   if( !m_bIsSrcFisheyeCam ) {
      PTZPosition minPTZValue = GetPTPosition( FPoint2D( 0.0f, 0.0f ) );
      PTZPosition maxPTZValue = GetPTPosition( FPoint2D( (float)ImageSize.Width, (float)ImageSize.Height ) );
      float fMinPan           = maxPTZValue.PanAngle;
      if( fMinPan > 180.0f ) fMinPan -= 360.0f;
      if( fMinPan < -180.0f ) fMinPan += 360.0f;
      float fMaxPan = minPTZValue.PanAngle;
      if( fMaxPan > 180.0f ) fMaxPan -= 360.0f;
      if( fMaxPan < -180.0f ) fMaxPan += 360.0f;
      if( tmpCurPTZPos.P < 0.0f && tmpCurPTZPos.P < fMinPan )
         tmpCurPTZPos.P = fMinPan;
      if( tmpCurPTZPos.P > 0.0f && tmpCurPTZPos.P > fMaxPan )
         tmpCurPTZPos.P = fMaxPan;

      float fMaxTilt = maxPTZValue.TiltAngle;
      float fMinTilt = minPTZValue.TiltAngle;
      if( tmpCurPTZPos.T < 0.0f && tmpCurPTZPos.T < fMinTilt )
         tmpCurPTZPos.T = fMinTilt;
      if( tmpCurPTZPos.T > 0.0f && tmpCurPTZPos.T > fMaxTilt )
         tmpCurPTZPos.T = fMaxTilt;
   }

   // 줌이동을 하면 줌 절대위치 업데이트.
   MaxZoomFactor                                     = m_pVirtualPTZCamera->m_pInfo->m_fMaxZoom;
   float fCurLogZoomValue                            = log( tmpCurPTZPos.Z ) / log( 2.0f );
   float fZoomLogSpeed                               = ( m_CurrPTZMoveHW.Z / ( MaxZoomSpeed - 1 ) ) * fElapsedInSec * 1.5f;
   float fTgtLogZoomValue                            = fCurLogZoomValue + fZoomLogSpeed;
   if( fTgtLogZoomValue < 0.0f ) fTgtLogZoomValue    = 0.0f;
   float fTgtZoomValue                               = pow( 2.0f, fTgtLogZoomValue );
   if( fTgtZoomValue > MaxZoomFactor ) fTgtZoomValue = MaxZoomFactor;
   tmpCurPTZPos.Z                                    = fTgtZoomValue;

   if( m_bIsSrcFisheyeCam ) {
      tmpCurPTZPos.P = 360.0f - tmpCurPTZPos.P;
      CheckPTZValue_Fisheye( tmpCurPTZPos );

      if( m_pVirtualPTZCamera->m_pVC ) {
         m_pVirtualPTZCamera->m_pVC->SetPtzPosition_Fisheye( tmpCurPTZPos, m_s2VideoSize.Height );
         tmpCurPTZPos.P = 360.0f - tmpCurPTZPos.P;
         m_CurPTZPos    = tmpCurPTZPos;
      }
   } else {
      m_CurPTZPos = tmpCurPTZPos;

      // 현재 Pan, Tilt, Zoom 위치로 부터 영상중심 위치 업데이트.
      FPoint2D d_pos = GetPixelPosition( PTZPosition( m_CurPTZPos.P, m_CurPTZPos.T, m_CurPTZPos.Z ) );
      m_pVirtualPTZCamera->m_p2CenterPos( int( d_pos.X + 0.5f ), int( d_pos.Y + 0.5f ) );
   }

   m_bOnceReceivePanTiltPos = TRUE; // 가상 PTZ카메라의 경우에는 현재 위치 값을 알고 있기에...
}

void CPTZCtrl_Virtual::CheckPTZValue_Fisheye( FPTZVector& pos )
{
   if( pos.P < 0.0f ) pos.P += 360.0f;
   if( pos.P > 360.0f ) pos.P -= 360.0f;

   if( pos.T < 0.0f ) pos.T  = 0.0f;
   if( pos.T > 90.0f ) pos.T = 90.0f;
}

#endif // __SUPPORT_VIRTUAL_PTZ_CAMERA
#endif // __SUPPORT_PTZ_CAMERA

﻿#include "stdafx.h"
#include "PTZCtrl.h"

#ifdef __SUPPORT_PTZ_CAMERA

#include "PTZCtrl_NIKO.h"
#include "PTZCtrl_Linking.h"
#include "IPCamStream.h"
#include "ContAbsPosTrack.h"
#include "version.h"
#include "vacl_geometry.h"

#ifndef WIN32
#include <sys/ioctl.h>
#include <sys/socket.h>
#endif

#include "TimerCheckerManager.h"

BOOL g_bPTZTest                  = FALSE;
BOOL g_bDebugZoomEstimate        = FALSE;
BOOL g_bIRCtrlByKeyboard         = FALSE;
uint g_nLastTime_IRCtrlByKeybord = 0;

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

CPTZCtrl* CreatePTZControl( int nPTZControlType, ISize2D s2VideoSize )
{
   switch( nPTZControlType ) {
   case PTZControlType_FSNetworks_FS_IR307H:
      return new CPTZCtrl_NIKO( nPTZControlType );
   case PTZControlType_LinkingToOtherPTZCtrl:
      return new CPTZCtrl_Linking( nPTZControlType, s2VideoSize );
   default:
      return new CPTZCtrl_NIKO( nPTZControlType );
   }
   return NULL;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CPTZCtrl
//
///////////////////////////////////////////////////////////////////////////////

CPTZCtrl::CPTZCtrl()
{
   m_nPTZControlType    = 0;
   m_nCurZoomModuleType = 0;

   // speed
   MinPanSpeed   = 0;
   MaxPanSpeed   = 63;
   MinTiltSpeed  = 0;
   MaxTiltSpeed  = 63;
   MinZoomSpeed  = 0;
   MaxZoomSpeed  = 7;
   MinFocusSpeed = 0;
   MaxFocusSpeed = 7;
   MinIrisSpeed  = 0;
   MaxIrisSpeed  = 7;

   // Pan, Tilt, Zoom, Focus, Iris Range
   MinPanAngle         = 0.0f;
   MaxPanAngle         = 360.0f;
   MinTiltAngle        = -90.0f;
   MaxTiltAngle        = 90.0f;
   MinZoomFactor       = 1.0f;
   MaxZoomFactor       = 36.0f;
   MinFocus            = 0.0f;
   MaxFocus            = 10.0f;
   MinIris             = 0.0f;
   MaxIris             = 10.f;
   MinPresetNo         = 1;
   MaxPresetNo         = 128;
   MaxDefogLevel       = 0;
   MaxBLCLevel         = 0;
   MaxWiperOnTimeInSec = 30;

   MaxAbsPosPanMoveSpeed         = 180.0f;
   MaxAbsPosTiltMoveSpeed        = 90.0f;
   AbsPosMoveDelayTimeInMilliSec = 250;

   m_nState                   = 0;
   m_nPTZMoveState            = 0;
   m_nCapabilities            = 0;
   m_nOpenMode                = PTZCtrlOpenMode_Normal;
   m_nCurrCmdType             = -1;
   m_nPrevCmdType             = m_nCurrCmdType;
   m_nLastPTZCmdMode          = LastPTZCmdMode_GotoAbsPos;
   m_nPTZIRCtrlMode           = -1;
   m_nSendCmdLen              = 0;
   m_nSendReqCmdLen           = 0;
   m_nContPTZMoveStopCmdCount = 0;
   m_bSendReqCmd              = FALSE;
   m_nReconnectionPeriod      = 500;

   m_CurPTZPos( 0.0f, 0.0f, 1.0f, 0.0f, 0.0f );
   m_PrevPTZPos( NO_CMD, NO_CMD, NO_CMD, NO_CMD );
   m_PrevAvgPTZPos( NO_CMD, NO_CMD, NO_CMD, NO_CMD );
   m_PrevPTZPosSC = m_PrevPTZPosZE = m_TgtPTZPos = m_CurPTZPos;

   m_nCamAddr         = 0;
   m_nSendCmdPeriod   = 33;
   m_nPTZCmdExecDelay = 300;

   m_s2VideoSize( 352, 240 );

   m_fFocalLength           = 700.0f;
   m_fFocalLengthRatio      = 1.0f;
   m_fZoomLogVel_ContSpd1   = 0.23f;
   m_fZoomLogVel_AbsPosMove = 1.6426f;
   m_s2RefImgSize_FL( 352.0f, 240.0f );

   m_nZoomStartTime    = 0;
   m_fMaxFocusMoveTime = 5.0f;

   m_OffsetPos( 0.0f, 0.0f, 0.0f, 0.0f, 0.0f );

   m_pMainPTZControl    = NULL;
   m_pSubPTZControl     = NULL;
   m_pLinkingPTZControl = NULL;

   m_pPTZControlProperty = NULL;
   m_pStream             = NULL;

   m_pPTZ2D           = NULL;
   m_pTimerMainThread = NULL;
   m_pTimerNetThread  = NULL;

   m_pContAbsPosTracker   = new CContAbsPosTrack;
   m_pPTZCtrlLogItemQueue = new PTZCtrlLogItemQueue;
   m_pPTZStatusCallback   = NULL;
   m_pPTZCtrlCallback     = NULL;
   m_pTimerContSpdChk     = NULL;

   m_nCmdPresetNo = 0;

   m_fWaitTime  = 0;
   m_fWaitTime2 = 0;

   m_nCurIRLightValueOnIRArea = -1;
   m_nCurIRLightValue         = 0;
   m_nPrvIRLightValue         = -1;
   m_nPTZMoveStopCmdTick      = GetTickCount() - 1000;

   m_nCustomIRLightCtrlMode = PTZIRCtrlMode_NightModeAndIROff;
   m_bUseIRLightCtrlInfo    = TRUE;
   m_pOuterIRLightCtrlInfo  = NULL;

   ZeroMemory( m_ResCmd, sizeof( m_ResCmd ) );
   ZeroMemory( m_ReqCmd, sizeof( m_ReqCmd ) );

   m_bGetAbsPosError    = FALSE;
   m_bPrvGetAbsPosError = FALSE;
   m_bRequestStandMode  = FALSE;

   m_bPanSpeedCheck               = TRUE;
   m_bTiltSpeedCheck              = TRUE;
   m_bLimitPanAngleRange          = FALSE;
   m_nSpeedCheckCount             = 30;
   m_fMinPanAngleForSpeedCheck    = 0.0f;
   m_fMaxPanAngleForSpeedCheck    = 360.0f;
   m_fMinTiltAngleForSpeedCheck   = 10.0f;
   m_fMaxTiltAngleForSpeedCheck   = 80.0f;
   m_nPanSpeedChangeWaitTime      = 200;
   m_nTiltSpeedChangeWaitTime     = 200;
   m_nPanDirectionChangeWaitTime  = 1000;
   m_nTiltDirectionChangeWaitTime = 1000;
   m_bLogZoomStep                 = TRUE;
   m_fStartZoomPos                = 1.0f;
   m_fEndZoomPos                  = 32.0f;
   m_nHWSpeedStep                 = 1;
   m_fLog2ZoomStepSize            = 0.25f;

   m_nSetIRModeTime                   = 0;
   m_nContPTZMoveToBeStoppedCheckTime = 0;
   m_nContPTZMoveStartTime            = 0;
   m_nAbsPosStopCount                 = 0;
   m_nGotoAbsPosMovingTime            = 0;
   m_nGotoAbsPosTime                  = 0;
   m_nChannelNo_IVX                   = -1;
   m_byRecvHeader2                    = 0;
   m_nSendReqCmdCntNet                = 0;
   m_nSendCmdCntNet                   = 0;
   m_nRecvCmdCnt                      = 0;
   m_nSendReqCmdLen                   = 0;
   m_bCmdDZoomOn                      = -1;
   m_bCmdWiperOn                      = -1;
   m_bCmdHeatingWireOn                = -1;
   m_bCmdFanOn                        = -1;
   m_nCmdDayAndNightMode              = -1;
   m_bCmdLaserIlluminatorOn           = -1;
   m_bCmdThermalCameraOn              = -1;
   m_PanAngleVelocityTable            = NULL;
   m_AngleSpeedCheckZoomTable         = NULL;
   m_nAngleSpeedCheckZoomTableItemNum = 0;
   m_nCmdZoomControlType              = -1;
   m_bOnceReceivePanTiltPos           = FALSE;
   m_nReqAbsPosTime                   = GetTickCount();
   m_nGetAbsPosTime                   = GetTickCount();
   m_nDelayedGetAbsPosTime            = 0;
   m_nWiperOffTime                    = 0;
   m_nWiperOnTime                     = 0;
}

CPTZCtrl::~CPTZCtrl()
{
   Close();
   delete m_pContAbsPosTracker;
   delete m_pPTZCtrlLogItemQueue;
}

void CPTZCtrl::SignalToClose()
{
   // 스레드 종료 시도
   m_nState |= PTZ_State_ToBeStopThread;
   m_evtWait.SetEvent();
   m_StreamSocket.Close();
   if( m_pTimerMainThread )
      m_pTimerMainThread->GetTimerEvent().SetEvent(); // Blocking 해제용
   if( m_pTimerNetThread )
      m_pTimerNetThread->GetTimerEvent().SetEvent(); // Blocking 해제용
}

void CPTZCtrl::Close()
{
   // 스레드 종료 시도
   m_nState |= PTZ_State_ToBeStopThread;
   m_evtWait.SetEvent();
   if( m_pTimerMainThread )
      m_pTimerMainThread->GetTimerEvent().SetEvent(); // Blocking 해제용
   if( m_pTimerNetThread )
      m_pTimerNetThread->GetTimerEvent().SetEvent(); // Blocking 해제용
   m_StreamSocket.Close();
   // 스레드 종료 대기
   if( m_ThreadNetPTZClient.joinable() )
      m_ThreadNetPTZClient.join();
   if( m_ThreadMainProc.joinable() )
      m_ThreadMainProc.join();

   if( !m_CurrPTZMoveHW.IsContPTZFIMoveStopped() )
      _ContPTZMove( IPTZVector( 0, 0, 0, 0, 0 ) );
   if( m_pTimerMainThread ) {
      m_pTimerMgr->DeleteTimer( m_pTimerMainThread->m_nTimerID );
      m_pTimerMainThread = NULL;
   }
   if( m_pTimerNetThread ) {
      m_pTimerMgr->DeleteTimer( m_pTimerNetThread->m_nTimerID );
      m_pTimerNetThread = NULL;
   }
   if( m_pSubPTZControl ) {
      m_pSubPTZControl->Close();
      delete m_pSubPTZControl;
      m_pSubPTZControl = NULL;
   }
   m_nState &= ~PTZ_State_Connected;
   m_nState &= ~( PTZ_State_ToBeStopThread );
}

void CPTZCtrl::PreOpen()
{
   Close();

   m_nState = PTZ_State_FirstGotoAbsPos;

   m_nSendReqCmdCntNet      = 0;
   m_nSendCmdCntNet         = 0;
   m_nRecvCmdCnt            = 0;
   m_nSendReqCmdLen         = 0;
   m_bOnceReceivePanTiltPos = FALSE;

   m_nLastSendCmdTime   = GetTickCount();
   m_nIRLightChangeTime = GetTickCount() - 1000;
   m_nSetIRLightTime    = GetTickCount() - 1000;

   if( NULL == m_pTimerMainThread )
      m_pTimerMainThread = m_pTimerMgr->GetNewTimer();
   if( NULL == m_pTimerNetThread )
      m_pTimerNetThread = m_pTimerMgr->GetNewTimer();
}

void CPTZCtrl::PostOpen()
{
   if( m_nOpenMode == PTZCtrlOpenMode_CreateOnly ) return;

   m_pContAbsPosTracker->Initialize( this, 1000.0f / float( m_nSendCmdPeriod ) );
   m_pPTZCtrlLogItemQueue->Initialize( 1000.0f / float( m_nSendCmdPeriod ), 5.0f );

   // 현재 위치를 목표 PTZ 위치를 설정한다.
   // 제일 처음 절대각 마우스 클릭으로 방식 이동 시, 즉 Offset PTZ 위치이동을 하는 경우
   // 현재 목표 위치를 기준으로 Offset 위치로 이동하게 되는데 현재 목표 위치가 지정되어있지 않으면
   // Pan, Tilt 위치가 0, 0 부터 시작되기 때문에 지정한 위치로 이동하지 않는 오류가 생긴다.
   m_TgtPTZPos = GetAbsPos();

   if( m_nPTZControlType ) {
      SetContPTZTrackMode( FALSE );
      m_ThreadMainProc = std::thread( Thread_MainProc, this );
   }
}

int CPTZCtrl::Open( CPTZCtrlInfo* pPTZCtrlInfo )
{
   if( m_pMainPTZControl ) return ( DONE ); // jun : 명령만 만들어내는 Sub객체의 경우에는 리턴한다.
   PreOpen();
   m_PTZInfo = *pPTZCtrlInfo;

   m_pPTZControlProperty = GetPTZControlProperty( m_nPTZControlType );
   ASSERT( m_pPTZControlProperty );
   if( m_pPTZControlProperty ) // Support RealHub
      m_nCapabilities = m_pPTZControlProperty->m_nCapabilities;

   if( m_nOpenMode != PTZCtrlOpenMode_CreateOnly ) {
      if( PTZCtrlConnType_Basic == m_PTZInfo.m_nConnectType ) {
         LOGD << "PTZ ConnType : PTZCtrlConnType_Basic(analog):" << m_PTZInfo.m_nConnectType;
         if( m_nPTZControlType > 0 ) {
            //m_pSerial->Close ();
            //m_pSerial->Open (pPTZCtrlInfo->m_nCOMPortNo, BaudRates[pPTZCtrlInfo->m_nBaudRateID]);
            //m_pSerial->Purge ();
         }
         m_nState |= PTZ_State_Connected;
      } else if( PTZCtrlConnType_TCPServer == m_PTZInfo.m_nConnectType ) {
         LOGD << "PTZ ConnType : PTZCtrlConnType_TCPServer:" << m_PTZInfo.m_nConnectType;
         m_ThreadNetPTZClient = std::thread( Thread_NetPTZClient, this );
      }
   } else {
      m_nState |= PTZ_State_Connected;
   }
   m_nCamAddr = pPTZCtrlInfo->m_nCamAddress;
   PostOpen();
   g_bIRCtrlByKeyboard = FALSE;
   if( m_nOpenMode == PTZCtrlOpenMode_CreateOnly )
      return ( -1 );

   return ( DONE );
}

int CPTZCtrl::Open( CIPCameraInfo* pIPCamInfo )
{
   PreOpen();

   IPCameraProperty* pProperty = GetIPCameraProperty( pIPCamInfo->m_nIPCameraType );
   if( !pProperty )
      return 1;
   if( PTZCtrlConnType_OtherIPCamera == pIPCamInfo->m_PTZInfo.m_nConnectType ) {
      pProperty = GetIPCameraProperty( pIPCamInfo->m_nIPCameraTypeForExtraPTZCtrl );
   }

   if( pProperty->m_nCapabilities & IPCamCaps_SupportCustomPTZCommand ) {
      m_pPTZControlProperty = GetPTZControlProperty( m_nPTZControlType );
   } else {
      m_nPTZControlType     = pIPCamInfo->m_nPTZControlType;
      m_pPTZControlProperty = GetPTZControlProperty( pIPCamInfo->m_nPTZControlType );
   }
   if( m_pPTZControlProperty ) // Support RealHub
      m_nCapabilities = m_pPTZControlProperty->m_nCapabilities;

   m_PTZInfo.m_bUseOuterIRLightCtrlInfo  = pIPCamInfo->m_PTZInfo.m_bUseOuterIRLightCtrlInfo;
   m_PTZInfo.m_bIRControlByIntelliVIX    = pIPCamInfo->m_PTZInfo.m_bIRControlByIntelliVIX; // (mkjang-141211)
   m_PTZInfo.m_bUseCameraDayAndNigntMode = pIPCamInfo->m_PTZInfo.m_bUseCameraDayAndNigntMode; // (mkjang-141211)
   m_PTZInfo.m_IRLightCtrlInfo           = pIPCamInfo->m_PTZInfo.m_IRLightCtrlInfo;
   // IP 카메라의 PTZ 설정 변수를 상위 PTZ카메라의 설정변수로 복사한다.
   m_PTZInfo.m_nTxOnly                = pIPCamInfo->m_PTZInfo.m_nTxOnly;
   m_PTZInfo.m_pvOpticalAxisOffset    = pIPCamInfo->m_PTZInfo.m_pvOpticalAxisOffset;
   m_PTZInfo.m_bReversePanCoordinate  = pIPCamInfo->m_PTZInfo.m_bReversePanCoordinate; // (mkjang-140616)
   m_PTZInfo.m_bReverseTiltCoordinate = pIPCamInfo->m_PTZInfo.m_bReverseTiltCoordinate; // (mkjang-140616)
   m_PTZInfo.m_bZoomProportionalJog   = pIPCamInfo->m_PTZInfo.m_bZoomProportionalJog;

   m_PTZInfo.CopyOnlyCommonValues( &pIPCamInfo->m_PTZInfo );

   if( pProperty->m_nCapabilities & IPCamCaps_SupportCustomPTZCommand ) {
      ISize2D s2VideoSize = pProperty->m_ResolutionListMgr.GetVideoRosolution( pIPCamInfo->m_nVideoFormat_Cap, pIPCamInfo->m_nResolutionIdx );

      if( pIPCamInfo->m_PTZInfo.m_nPTZControlType > 0 )
         m_pSubPTZControl = ::CreatePTZControl( pIPCamInfo->m_PTZInfo.m_nPTZControlType, s2VideoSize );

      if( m_pSubPTZControl ) {
         m_pSubPTZControl->m_nChannelNo_IVX = m_nChannelNo_IVX;
         m_pSubPTZControl->m_s2VideoSize    = s2VideoSize;
         // Sub PTZ제어 객체에 필요한 변수를 복사한다.
         m_pSubPTZControl->m_pMainPTZControl = this;
         m_pSubPTZControl->m_pStream         = pIPCamInfo->m_pStream;
         m_pSubPTZControl->m_nCamAddr        = pIPCamInfo->m_PTZInfo.m_nCamAddress;
         m_pSubPTZControl->m_PTZInfo         = pIPCamInfo->m_PTZInfo;
         CopyFrom( m_pSubPTZControl );
         // 명령 전송 주기 관련.
         // 하위 PTZ 가 ezTCP를 사용하는 경우.
         if( m_pSubPTZControl->m_PTZInfo.m_nConnectType == PTZCtrlConnType_TCPServer ) {
            m_nSendCmdPeriod = m_pSubPTZControl->m_nSendCmdPeriod;
         } else {
            // 하위 PTZ 카메라의 명령 주기가 메인 PTZ (IP) 카메라의 주기를 넘을 수 없다.
            if( m_nSendCmdPeriod < m_pSubPTZControl->m_nSendCmdPeriod )
               m_nSendCmdPeriod = m_pSubPTZControl->m_nSendCmdPeriod;
         }
         PTZControlProperty* pSubPTZCtrlProperty = GetPTZControlProperty( pIPCamInfo->m_PTZInfo.m_nPTZControlType );

         UINT64 nSubPTZCapabilities = 0;
         if( pSubPTZCtrlProperty )
            nSubPTZCapabilities = pSubPTZCtrlProperty->m_nCapabilities;

         UINT64 nMainPTZCapabilities = m_nCapabilities;
         m_nCapabilities             = 0;
         // 지원옵션
         m_nCapabilities |= ( nMainPTZCapabilities & g_nSupportPTZOptions_And ) & ( nSubPTZCapabilities & g_nSupportPTZOptions_And );
         m_nCapabilities |= ( nMainPTZCapabilities & g_nSupportPTZOptions_Or ) | ( nSubPTZCapabilities & g_nSupportPTZOptions_Or );
         // 지원안됨 옵션
         m_nCapabilities |= ( nMainPTZCapabilities & g_nNotSupportPTZOptions_Or ) | ( nSubPTZCapabilities & g_nNotSupportPTZOptions_Or );
         m_s2RefImgSize_FL                 = m_pSubPTZControl->m_s2RefImgSize_FL;
         m_fFocalLength                    = m_pSubPTZControl->m_fFocalLength;
         m_fFocalLengthRatio               = m_pSubPTZControl->m_fFocalLengthRatio;
         m_pSubPTZControl->m_nCapabilities = m_nCapabilities;
         m_pSubPTZControl->m_nRecvCmdCnt   = 0;
         MinPresetNo                       = m_pSubPTZControl->MinPresetNo;
         MaxPresetNo                       = m_pSubPTZControl->MaxPresetNo;
         MinPanSpeed                       = m_pSubPTZControl->MinPanSpeed;
         MaxPanSpeed                       = m_pSubPTZControl->MaxPanSpeed;
         MinTiltSpeed                      = m_pSubPTZControl->MinTiltSpeed;
         MaxTiltSpeed                      = m_pSubPTZControl->MaxTiltSpeed;
         MinZoomSpeed                      = m_pSubPTZControl->MinZoomSpeed;
         MaxZoomSpeed                      = m_pSubPTZControl->MaxZoomSpeed;
         MinZoomFactor                     = m_pSubPTZControl->MinZoomFactor;
         MaxZoomFactor                     = m_pSubPTZControl->MaxZoomFactor;
         MinFocus                          = m_pSubPTZControl->MinFocus;
         MaxFocus                          = m_pSubPTZControl->MaxFocus;
      }
   } else {
      m_PTZInfo = pIPCamInfo->m_PTZInfo;
   }
   // jun : PTZ 명령 생성을 담당하는 Sub PTZ 제어 객체도 Open 함수를 호출 시켜야 한다.
   //       실제 시리얼 포트를 오픈하는 것은 아니고 PTZ 초기화 명령만 전송하기 위해 사용된다.
   {
      m_pSubPTZControl->m_pIPCamInfo = pIPCamInfo; // 중거리-LG CNS카메라
      m_pSubPTZControl->Open( &pIPCamInfo->m_PTZInfo );
   }
   PostOpen();
   g_bIRCtrlByKeyboard = FALSE;
   if( m_nOpenMode == PTZCtrlOpenMode_CreateOnly )
      return ( -1 );
   return ( DONE );
}

int CPTZCtrl::Connect()
{
   return ( DONE );
}

int CPTZCtrl::Disconnect()
{
   return ( DONE );
}

BOOL CPTZCtrl::IsPTZConnected()
{
   return ( m_nState & PTZ_State_Connected ? TRUE : FALSE );
}

BOOL CPTZCtrl::IsPTZCtrlReady()
{
   if( IsPTZConnected() ) {
      return ( m_nState & PTZ_State_InitHWOK ? TRUE : FALSE );
   }
   return FALSE;
}

BOOL CPTZCtrl::IsPTZReady()
{
   if( m_nState & PTZ_State_Connected ) {
      if( m_nState & PTZ_State_InitHWOK ) {
         return TRUE;
      }
   }
   return FALSE;
}

int CPTZCtrl::SendCommand()
{
   return SendCommand( m_sCmd, m_nSendCmdLen );
}

int CPTZCtrl::SendCommand( BYTE* pCmd, int nCmdLen, int nCmdType )
{
   int ret = DONE;

   if( ( FALSE == m_PTZInfo.m_nTxOnly ) && m_nCapabilities & PTZCtrlCap_AttachReqAbsPosSupported && nCmdType != PTZCmdType_ReqABSPos ) {
      _GetAbsPosReqCmd( pCmd + nCmdLen, nCmdLen );
      m_nReqAbsPosTime = GetTickCount();
   }
   BOOL bExecSendCmdEx = FALSE;
   // 하위 PTZ가 생성되었다는 것은 비디오 서버를 통한 PTZ 제어임을 뜻한다.
   // 이 때에는 명령 전송을 SendCommandEx를 통하여 수행하여야한다.
   if( m_pSubPTZControl ) {
      bExecSendCmdEx = TRUE;
      if( m_pSubPTZControl->m_PTZInfo.m_nConnectType == PTZCtrlConnType_TCPServer )
         bExecSendCmdEx = FALSE;
   }
   if( bExecSendCmdEx ) {
      CCriticalSectionSP co( m_csSendCmd );
      int nCmdWritten = nCmdLen;
      if( DONE != this->SendCommandEx( pCmd, nCmdWritten ) )
         return ( 1 );
   } else {
      if( PTZCtrlConnType_Basic == m_PTZInfo.m_nConnectType || PTZCtrlConnType_OtherIPCamera == m_PTZInfo.m_nConnectType ) {
         if( m_pMainPTZControl ) {
            return SendCommandByOuterPTZControl();
         }
         m_csSendCmd.Lock();
         if( nCmdLen > 0 ) {
            DWORD dwCmdWritten = nCmdLen;
            //m_pSerial->Write (pCmd, dwCmdWritten);
         }
         m_csSendCmd.Unlock();
      }
      // ezTCP 와 같은 별도의 네트워크 장비를 통하여 통신하는 경우.
      else if( PTZCtrlConnType_TCPServer == m_PTZInfo.m_nConnectType ) {
         if( m_bSendReqCmd ) 
         {
            m_nSendReqCmdCntNet++;
         } 
         else 
         {
            m_nSendCmdCntNet++;
         }
      }
   }
   return ( ret );
}

int CPTZCtrl::SendCommandEx( BYTE* pCmd, int& nCmdLen )
{
   return ( DONE );
}

int CPTZCtrl::ReqAbsPos()
{
   // 아래 세줄은 Convex 카메라 전용 코드이다. 원샷 오토포커스가 진행중일 때는 좀더 오랬동안 위치값을 얻어야 한다.
   BOOL bDelayedGetAbsPos = FALSE;
   if( m_nDelayedGetAbsPosTime && GetTickCount() > m_nDelayedGetAbsPosTime )
      bDelayedGetAbsPos = TRUE;

   if( IsGetAbsPosSupported() && ( IsRealTimeGetAbsPosMode() || ( m_nState & PTZ_State_GetAbsPosToBeRequested ) || bDelayedGetAbsPos ) ) {
      int nCmdLen = 0;
      m_csSendCmd.Lock();
      _GetAbsPosReqCmd( m_ReqCmd, nCmdLen );
      m_nSendReqCmdLen = nCmdLen;
      m_bSendReqCmd    = TRUE;
      m_csSendCmd.Unlock();
      //LOGD << "ReqAbsPos. count:" << m_nSendReqCmdCntNet;
      SendCommand( m_ReqCmd, m_nSendReqCmdLen, PTZCmdType_ReqABSPos );
      m_bSendReqCmd    = FALSE;
      m_nReqAbsPosTime = GetTickCount();
   }

   // Convex 전용 코드
   if( bDelayedGetAbsPos )
      m_nDelayedGetAbsPosTime = 0;

   return ( DONE );
}

void CPTZCtrl::ReqStandMode()
{
   _GetStandModeCmd( m_ReqCmd, m_nSendReqCmdLen );
   m_bSendReqCmd = TRUE;
   LOGD << "ReqStandMode";
   SendCommand( m_ReqCmd, m_nSendReqCmdLen );
   m_bSendReqCmd = FALSE;
}

int CPTZCtrl::_GetAbsPosReqCmd( BYTE* pCmd, int& nCmdLen )
{
   if( m_pSubPTZControl )
      return m_pSubPTZControl->_GetAbsPosReqCmd( pCmd, nCmdLen );
   return ( DONE );
}

void CPTZCtrl::_GetStandModeCmd( BYTE* pCmd, int& nCmdLen )
{
   if( m_pSubPTZControl )
      return m_pSubPTZControl->_GetStandModeCmd( pCmd, nCmdLen );
}

BOOL CPTZCtrl::_InitHW()
{
   if( m_pSubPTZControl )
      return m_pSubPTZControl->_InitHW();
   return ( TRUE );
}

int CPTZCtrl::_PopCommand()
{
   if( m_pSubPTZControl )
      return m_pSubPTZControl->_PopCommand();
   return ( 0 );
}

int CPTZCtrl::_ChangeMaxPTSpeed( BOOL bZoomProportionalJog )
{
   if( m_pSubPTZControl )
      return m_pSubPTZControl->_ChangeMaxPTSpeed( bZoomProportionalJog );
   return ( DONE );
}

void CPTZCtrl::CheckPanTiltPositionInReal( float& pa, float& ta )
{
   if( pa != PREV_CMD ) {
      if( pa < 0.0f ) pa += 360.0f;
      if( pa > 360.0f ) pa -= 360.0f;
      if( pa < MinPanAngle ) pa = MinPanAngle;
      if( pa > MaxPanAngle ) pa = MaxPanAngle;
   }
   if( ta != PREV_CMD ) {
      if( m_PTZInfo.m_bReverseTiltCoordinate ) {
         float minTiltAngle = -MaxTiltAngle;
         float maxTiltAngle = -MinTiltAngle;
         if( ta < minTiltAngle ) ta = minTiltAngle;
         if( ta > maxTiltAngle ) ta = maxTiltAngle;
      } else {
         if( ta < MinTiltAngle ) ta = MinTiltAngle;
         if( ta > MaxTiltAngle ) ta = MaxTiltAngle;
      }
   }
}

void CPTZCtrl::CheckPanTiltPositionInHW( float& pa, float& ta )
{
   if( pa != PREV_CMD ) {
      if( pa < 0.0f ) pa += 360.0f;
      if( pa > 360.0f ) pa -= 360.0f;
      if( pa < MinPanAngle ) pa = MinPanAngle;
      if( pa > MaxPanAngle ) pa = MaxPanAngle;
   }
   if( ta != PREV_CMD ) {
      if( ta < MinTiltAngle ) ta = MinTiltAngle;
      if( ta > MaxTiltAngle ) ta = MaxTiltAngle;
   }
}

void CPTZCtrl::ConvertPanTiltOrientationRealToHW( float& pa, float& ta )
{
   if( pa != PREV_CMD ) {
      if( m_PTZInfo.m_bReversePanCoordinate ) {
         pa = 360.0f - pa;
      }
   }

   if( ta != PREV_CMD ) {
      if( m_PTZInfo.m_bReverseTiltCoordinate ) {
         ta = -ta;
      }
   }
   CheckPanTiltPositionInHW( pa, ta );
}

void CPTZCtrl::ConvertPanTiltOrientationHWToReal( float& pa, float& ta )
{
   if( pa != PREV_CMD ) {
      if( m_PTZInfo.m_bReversePanCoordinate ) {
         pa = 360.0f - pa;
      }
   }

   if( ta != PREV_CMD ) {
      if( m_PTZInfo.m_bReverseTiltCoordinate ) {
         ta = -ta;
      }
   }
   CheckPanTiltPositionInReal( pa, ta );
}

int CPTZCtrl::GetWaitingTime( float pd )
{
   if( m_pSubPTZControl ) {
      UINT wait_time = m_pSubPTZControl->GetWaitingTime( pd );
      if( wait_time < m_nSendCmdPeriod )
         wait_time = m_nSendCmdPeriod;
      return wait_time;
   }
   int w_time = (int)( pd / 270.0f * 1000.0f );
   if( w_time < 70 ) w_time = 70;
   return ( w_time );
}

int CPTZCtrl::GetWaitingTime( float dp, float dt, float dzl )
{
   dp = fabs( dp );
   if( fabs( MaxPanAngle - MinPanAngle ) >= 360.0f )
      if( dp > 180.0f ) dp = 360.0f - dp;
   float pd         = (float)sqrt( dp * dp + dt * dt );
   int nPanTiltTime = int( GetWaitingTime( pd ) );
   int nZoomingTime = int( dzl * 1250 );
   int nWaitingTime = nPanTiltTime;
   if( nZoomingTime > nWaitingTime ) nWaitingTime = nZoomingTime;
   if( nWaitingTime > 7000 ) nWaitingTime = 7000;
   nWaitingTime += AbsPosMoveDelayTimeInMilliSec;
   return ( nWaitingTime );
}

int CPTZCtrl::GetMovingTime( FPTZVector pos1, FPTZVector pos2 )
{
   if( NO_CMD == pos1.P || NO_CMD == pos2.P )
      return ( 1000 );
   float fPanDiff      = fabs( GetAngleDiff( pos1.P, pos2.P ) );
   float fTiltDiff     = fabs( GetAngleDiff( pos1.T, pos2.T ) );
   float fZoomDiffLog2 = fabs( log2( pos1.Z ) - log2( pos2.Z ) );
   return GetWaitingTime( fPanDiff, fTiltDiff, fZoomDiffLog2 );
}

void CPTZCtrl::ContPTZMoveAV( FPTZVector pvContMoveAV, BOOL bUsePropPT )
{
   if( PTZCtrlConnType_LinkToPTZCtrl == m_PTZInfo.m_nConnectType ) {
      CCriticalSectionSP co( m_csLinkingPTZControl );
      if( m_pLinkingPTZControl && ( PTZCtrlConnType_LinkToPTZCtrl != m_pLinkingPTZControl->m_PTZInfo.m_nConnectType ) ) {
         pvContMoveAV.ZT = m_PTZInfo.m_nZoomModuleType;
         m_pLinkingPTZControl->ContPTZMoveAV( pvContMoveAV, bUsePropPT );
         return;
      }
   }
   m_nCurZoomModuleType = pvContMoveAV.ZT;

   FPTZVector pvContMoveHW;
   if( bUsePropPT ) {
      float fSpeedScale = 1.0f / GetAbsPos().Z;
      if( pvContMoveAV.P != NO_CMD ) {
         if( pvContMoveAV.P != 0.0f )
            pvContMoveAV.P = pvContMoveAV.P * fSpeedScale;
      }
      if( pvContMoveAV.T != NO_CMD ) {
         if( pvContMoveAV.T != 0.0f )
            pvContMoveAV.T = pvContMoveAV.T * fSpeedScale;
      }
   }
   if( pvContMoveAV.T == 0.0f )
      int jun = 0;
   ConvertAngle2HWVel( pvContMoveAV, pvContMoveHW );

   FPTZVector __inputPTZMoveHW = m_InputPTZMoveHW;
   m_csCmdSet.Lock();
   if( pvContMoveHW.P != NO_CMD )
      __inputPTZMoveHW.P = pvContMoveHW.P;
   if( pvContMoveHW.T != NO_CMD )
      __inputPTZMoveHW.T = pvContMoveHW.T;
   if( pvContMoveHW.Z != NO_CMD )
      __inputPTZMoveHW.Z = pvContMoveHW.Z;
   if( pvContMoveHW.F != NO_CMD )
      __inputPTZMoveHW.F = pvContMoveHW.F;
   if( pvContMoveHW.I != NO_CMD )
      __inputPTZMoveHW.I = pvContMoveHW.I;
   __inputPTZMoveHW.ZT = pvContMoveHW.ZT;

   if( __inputPTZMoveHW.IsContPTZFIMoveStopped() && !m_InputPTZMoveHW.IsContPTZFIMoveStopped() ) { // 연속이동 멈춤
      if( GetTickCount() - m_nContPTZMoveStartTime < m_nSendCmdPeriod * 2 ) {
         m_nState |= PTZ_State_ContMoveToBeStopped;
         m_nContPTZMoveToBeStoppedCheckTime = GetTickCount();
      } else {
         m_InputPTZMoveHW = __inputPTZMoveHW;
      }
   } else {
      m_InputPTZMoveHW = __inputPTZMoveHW;
      if( !__inputPTZMoveHW.IsContPTZFIMoveStopped() && !( m_nState & PTZMoveState_Moving ) ) {
         m_nState &= ~PTZ_State_Stopped;
         if( m_nState & PTZ_State_ContMoveToBeStopped ) {
            m_nState &= ~PTZ_State_ContMoveToBeStopped;
         }
      }
   }

   m_nCurrCmdType     = PTZCmdType_ContinuousMove;
   m_nLastSendCmdTime = GetTickCount();
   m_csCmdSet.Unlock();

   if( m_InputPTZMoveHW_Prev.IsContPTZFIMoveStopped() && !m_InputPTZMoveHW.IsContPTZFIMoveStopped() ) {
      m_nContPTZMoveStartTime = GetTickCount();
   }
}

void CPTZCtrl::ContPTZMoveHW( FPTZVector pvContMoveHW, BOOL bUsePropPT )
{
   if( PTZCtrlConnType_LinkToPTZCtrl == m_PTZInfo.m_nConnectType ) {
      CCriticalSectionSP co( m_csLinkingPTZControl );
      if( m_pLinkingPTZControl && ( PTZCtrlConnType_LinkToPTZCtrl != m_pLinkingPTZControl->m_PTZInfo.m_nConnectType ) ) {
         pvContMoveHW.ZT = m_PTZInfo.m_nZoomModuleType;
         m_pLinkingPTZControl->ContPTZMoveHW( pvContMoveHW, bUsePropPT );
      }
   }
   m_csCmdSet.Lock();
   if( pvContMoveHW.P != NO_CMD ) m_InputPTZMoveHW.P = pvContMoveHW.P;
   if( pvContMoveHW.T != NO_CMD ) m_InputPTZMoveHW.T = pvContMoveHW.T;
   if( pvContMoveHW.Z != NO_CMD ) m_InputPTZMoveHW.Z = pvContMoveHW.Z;
   if( pvContMoveHW.I != NO_CMD ) m_InputPTZMoveHW.I = pvContMoveHW.I;
   if( pvContMoveHW.F != NO_CMD ) m_InputPTZMoveHW.F = pvContMoveHW.F;
   if( pvContMoveHW.P != NO_CMD && m_InputPTZMoveHW.P > MaxPanSpeed ) m_InputPTZMoveHW.P = float( MaxPanSpeed );
   if( pvContMoveHW.P != NO_CMD && m_InputPTZMoveHW.P < -MaxPanSpeed ) m_InputPTZMoveHW.P = float( -MaxPanSpeed );
   if( pvContMoveHW.T != NO_CMD && m_InputPTZMoveHW.T > MaxTiltSpeed ) m_InputPTZMoveHW.T = float( MaxTiltSpeed );
   if( pvContMoveHW.T != NO_CMD && m_InputPTZMoveHW.T < -MaxTiltSpeed ) m_InputPTZMoveHW.T = float( -MaxTiltSpeed );
   if( pvContMoveHW.Z != NO_CMD && m_InputPTZMoveHW.Z > MaxZoomSpeed ) m_InputPTZMoveHW.Z = float( MaxZoomSpeed );
   if( pvContMoveHW.Z != NO_CMD && m_InputPTZMoveHW.Z < -MaxZoomSpeed ) m_InputPTZMoveHW.Z = float( -MaxZoomSpeed );
   if( pvContMoveHW.F != NO_CMD && m_InputPTZMoveHW.F > MaxFocusSpeed ) m_InputPTZMoveHW.F = float( MaxFocusSpeed );
   if( pvContMoveHW.F != NO_CMD && m_InputPTZMoveHW.F < -MaxFocusSpeed ) m_InputPTZMoveHW.F = float( -MaxFocusSpeed );
   if( pvContMoveHW.I != NO_CMD && m_InputPTZMoveHW.I > MaxIrisSpeed ) m_InputPTZMoveHW.I = float( MaxIrisSpeed );
   if( pvContMoveHW.I != NO_CMD && m_InputPTZMoveHW.I < -MaxIrisSpeed ) m_InputPTZMoveHW.I = float( -MaxIrisSpeed );
   if( m_InputPTZMoveHW.P == 0 && m_InputPTZMoveHW.T == 0 && m_InputPTZMoveHW.Z == 0 ) {
      m_nState |= PTZ_State_AutoFocusToBeExcuted;
   } else {
      m_nState |= PTZ_State_AutoFocusToBeStopped;
      m_nState &= ~PTZ_State_AutoFocusToBeExcuted;
   }
   m_nCurrCmdType     = PTZCmdType_ContinuousMove;
   m_nLastSendCmdTime = GetTickCount();
   m_csCmdSet.Unlock();
}

void CPTZCtrl::_ContPTZMove( FPTZVector pvContMoveHW )
{
   m_CurrPTZMoveHW = pvContMoveHW;
   if( m_CurrPTZMoveHW.P != NO_CMD && m_CurrPTZMoveHW.P > MaxPanSpeed ) m_CurrPTZMoveHW.P = float( MaxPanSpeed );
   if( m_CurrPTZMoveHW.P != NO_CMD && m_CurrPTZMoveHW.P < -MaxPanSpeed ) m_CurrPTZMoveHW.P = float( -MaxPanSpeed );
   if( m_CurrPTZMoveHW.T != NO_CMD && m_CurrPTZMoveHW.T > MaxTiltSpeed ) m_CurrPTZMoveHW.T = float( MaxTiltSpeed );
   if( m_CurrPTZMoveHW.T != NO_CMD && m_CurrPTZMoveHW.T < -MaxTiltSpeed ) m_CurrPTZMoveHW.T = float( -MaxTiltSpeed );
   if( m_CurrPTZMoveHW.Z != NO_CMD && m_CurrPTZMoveHW.Z > MaxZoomSpeed ) m_CurrPTZMoveHW.Z = float( MaxZoomSpeed );
   if( m_CurrPTZMoveHW.Z != NO_CMD && m_CurrPTZMoveHW.Z < -MaxZoomSpeed ) m_CurrPTZMoveHW.Z = float( -MaxZoomSpeed );
   if( m_CurrPTZMoveHW.F != NO_CMD && m_CurrPTZMoveHW.F > MaxFocusSpeed ) m_CurrPTZMoveHW.F = float( MaxFocusSpeed );
   if( m_CurrPTZMoveHW.F != NO_CMD && m_CurrPTZMoveHW.F < -MaxFocusSpeed ) m_CurrPTZMoveHW.F = float( -MaxFocusSpeed );
   if( m_CurrPTZMoveHW.I != NO_CMD && m_CurrPTZMoveHW.I > MaxIrisSpeed ) m_CurrPTZMoveHW.I = float( MaxIrisSpeed );
   if( m_CurrPTZMoveHW.I != NO_CMD && m_CurrPTZMoveHW.I < -MaxIrisSpeed ) m_CurrPTZMoveHW.I = float( -MaxIrisSpeed );
   m_CurrPTZMoveHW.ZT = pvContMoveHW.ZT;
   m_nLastSendCmdTime = GetTickCount();
   IPTZVector move_vec_hw;
   move_vec_hw.P  = (int)m_CurrPTZMoveHW.P;
   move_vec_hw.T  = (int)m_CurrPTZMoveHW.T;
   move_vec_hw.Z  = (int)m_CurrPTZMoveHW.Z;
   move_vec_hw.F  = (int)m_CurrPTZMoveHW.F;
   move_vec_hw.I  = (int)m_CurrPTZMoveHW.I;
   move_vec_hw.ZT = (int)m_CurrPTZMoveHW.ZT;

   _ContPTZMove( move_vec_hw );
}

int CPTZCtrl::GetAbsPos( float& pa, float& ta, float& zf )
{
   float f;
   return GetAbsPos( pa, ta, zf, f );
}

int CPTZCtrl::GetAbsPos( float& pa, float& ta, float& zf, float& f )
{
   if( m_CurPTZPos.Z < 1.0f ) m_CurPTZPos.Z = 1.0f;
   if( m_CurPTZPos.Z > 100000.0f ) m_CurPTZPos.Z = 100000.0f;
   float zoomlog_tgt  = log( m_CurPTZPos.Z ) * MC_LOG2_INV;
   float zoomlog_max  = log( MaxZoomFactor ) * MC_LOG2_INV;
   float offset_ratio = 1.0f;
   if( zoomlog_max > 0.0f )
      offset_ratio = zoomlog_tgt / zoomlog_max;
   pa = m_CurPTZPos.P;
   pa -= m_PTZInfo.m_pvOpticalAxisOffset.P * offset_ratio;
   ta = m_CurPTZPos.T;
   ta -= m_PTZInfo.m_pvOpticalAxisOffset.T * offset_ratio;
   zf = m_CurPTZPos.Z;
   f  = m_CurPTZPos.F;
   return ( DONE );
}

int CPTZCtrl::GetAbsPos( FPTZVector& abs_pos )
{
   GetAbsPos( abs_pos.P, abs_pos.T, abs_pos.Z, abs_pos.F );
   return ( DONE );
}

FPTZVector CPTZCtrl::GetAbsPos()
{
   FPTZVector abs_pos;
   GetAbsPos( abs_pos.P, abs_pos.T, abs_pos.Z, abs_pos.F );
   return abs_pos;
}

FPTZVector CPTZCtrl::GetAbsPos( int nZoomModuleType )
{
   FPTZVector abs_pos;
   GetAbsPos( abs_pos.P, abs_pos.T, abs_pos.Z, abs_pos.F );
   if( ZoomModuleType_ThermalImaging == nZoomModuleType ) {
      abs_pos.Z = m_CurPTZPos_ThermalImaging.Z;
      abs_pos.F = m_CurPTZPos_ThermalImaging.F;
      abs_pos.I = m_CurPTZPos_ThermalImaging.I;
   }
   if( ZoomModuleType_WideAngle == nZoomModuleType ) {
      abs_pos.Z = m_CurPTZPos_WideAngle.Z;
      abs_pos.F = m_CurPTZPos_WideAngle.F;
      abs_pos.I = m_CurPTZPos_WideAngle.I;
   }
   return abs_pos;
}

FPTZVector CPTZCtrl::GetAbsPosOfPreset( int nPresetNo )
{
   FPTZVector pvAbsPosOfPreset;
   if( MinPresetNo <= nPresetNo && nPresetNo <= MaxPresetNo ) {
      // 현재 Preset 위치에 대한 PTZ 절대각 항목을 찾아낸 후
      // 그 값을 현재 PTZ 위치 값으로 복사한다.
      CPTZ1D* ptz1d = NULL;
      if( m_pPTZ2D ) m_pPTZ2D->FindPTZ1D( nPresetNo, &ptz1d );
      if( ptz1d ) {
         pvAbsPosOfPreset = FPTZVector( ptz1d->P, ptz1d->T, ptz1d->Z, ptz1d->F );
      }
   }
   return pvAbsPosOfPreset;
}

FPTZVector CPTZCtrl::GetTgtAbsPos()
{
   return m_TgtPTZPos;
}

FPTZVector CPTZCtrl::GetOffsetTgtAbsPos()
{
   return m_CorrectedTgtPTZPos;
}

float CPTZCtrl::GetCurrZoom()
{
   float zf;
   if( m_nState & PTZ_State_Zooming )
      m_CurPTZPos.Z = m_fZoomEstimate;
   if( !( m_nState & ( PTZ_State_Zooming | PTZ_State_ZoomToBeStarted ) ) )
      zf = m_CurPTZPos.Z;
   else
      zf = m_fZoomEstimate;
   return ( zf );
}

int CPTZCtrl::GotoAbsPos( FPTZVector abs_pos )
{
   if( PTZCtrlConnType_LinkToPTZCtrl == m_PTZInfo.m_nConnectType ) {
      CCriticalSectionSP co( m_csLinkingPTZControl );
      if( m_pLinkingPTZControl && ( PTZCtrlConnType_LinkToPTZCtrl != m_pLinkingPTZControl->m_PTZInfo.m_nConnectType ) ) {
         abs_pos.ZT = m_PTZInfo.m_nZoomModuleType;
         m_pLinkingPTZControl->GotoAbsPos( abs_pos );
         return ( DONE );
      }
   }

   FPTZVector pvCurAbsPos = m_TgtPTZPos;
   if( IsGetAbsPosSupported() && !m_PTZInfo.m_nTxOnly ) {
      pvCurAbsPos = m_CurPTZPos;
   }
   if( abs_pos.P == PREV_CMD )
      abs_pos.P = pvCurAbsPos.P;
   if( abs_pos.T == PREV_CMD )
      abs_pos.T = pvCurAbsPos.T;
   if( abs_pos.Z == PREV_CMD )
      abs_pos.Z = pvCurAbsPos.Z;
   if( abs_pos.F == PREV_CMD )
      abs_pos.F = pvCurAbsPos.F;
   if( abs_pos.Z > MaxZoomFactor )
      abs_pos.Z = MaxZoomFactor;
   if( abs_pos.Z < MinZoomFactor )
      abs_pos.Z = MinZoomFactor;
   m_csCmdSet.Lock();
   m_TgtPTZPos = abs_pos;
   CheckPanTiltPositionInReal( m_TgtPTZPos.P, m_TgtPTZPos.T );

   m_nState |= PTZ_State_AbsolutePosBeExcuted;
   m_nCurrCmdType     = PTZCmdType_GotoAbsPos;
   m_nLastSendCmdTime = GetTickCount();
   m_csCmdSet.Unlock();

   return ( DONE );
}

int CPTZCtrl::GotoAbsPosByConstantAngleSpeed( FPTZVector abs_pos, float fAngleSpeed )
{
   // 중거리 철원 테스트 시 막음. 연속이동방식 절대위치이동이 잘 안됨.
   BOOL bIsSupportABSPosMoveByContMove = TRUE;
   // 연속 이동 방식은 지원 안함
   if( IsRealTimeGetAbsPosSupported() && !m_PTZInfo.m_nTxOnly && !m_bGetAbsPosError )
      bIsSupportABSPosMoveByContMove = TRUE;

   if( IsPresetMoveByUserSpecifiedSpeedSupported() ) {
      SetContPTZTrackMode( FALSE );
      abs_pos.PS = fAngleSpeed;
      abs_pos.TS = fAngleSpeed;
   } else if( bIsSupportABSPosMoveByContMove ) {
      SetContPTZTrackMode( TRUE );
      m_pContAbsPosTracker->SetConstantAngleSpeedMode( TRUE, fAngleSpeed, abs_pos );
   }

   GotoAbsPos( abs_pos );
   return ( DONE );
}

int CPTZCtrl::GotoAbsPosByConstantImageScrollSpeed( FPTZVector abs_pos, float fImageScrollSpeed )
{
   if( 0 && IsRealTimeGetAbsPosSupported() && !m_PTZInfo.m_nTxOnly && !m_bGetAbsPosError ) {
      SetContPTZTrackMode( TRUE );
      m_pContAbsPosTracker->SetConstantImageScrollSpeedMode( TRUE, fImageScrollSpeed );
   } else {
      SetContPTZTrackMode( FALSE );
      abs_pos.PS = fImageScrollSpeed;
      abs_pos.TS = fImageScrollSpeed;
   }
   GotoAbsPos( abs_pos );
   return ( DONE );
}

void CPTZCtrl::SetAbsPos( FPTZVector abs_pos )
{
   FPTZVector tmp_abs_pos = abs_pos;
   ConvertPanTiltOrientationHWToReal( tmp_abs_pos.P, tmp_abs_pos.T );
   //logd ("2 : (%5.1f, %5.1f) <- (%5.1f, %5.1f)\n", tmp_abs_pos.P, tmp_abs_pos.T, abs_pos.P, abs_pos.T);
   if( tmp_abs_pos.P != PREV_CMD ) m_CurPTZPos.P = tmp_abs_pos.P;
   if( tmp_abs_pos.T != PREV_CMD ) m_CurPTZPos.T = tmp_abs_pos.T;
   if( tmp_abs_pos.Z != PREV_CMD ) {
      m_CurPTZPos.Z = tmp_abs_pos.Z;
      if( m_CurPTZPos.Z < 1.0f ) m_CurPTZPos.Z = 1.0f;
      if( m_CurPTZPos.Z > 100000.0f ) m_CurPTZPos.Z = 100000.0f;
   }
   if( tmp_abs_pos.F != PREV_CMD ) m_CurPTZPos.F = tmp_abs_pos.F;
   if( tmp_abs_pos.I != PREV_CMD ) m_CurPTZPos.I = tmp_abs_pos.I;

   if( m_pMainPTZControl )
      m_pMainPTZControl->SetAbsPos( abs_pos );

   m_nGetAbsPosTime = GetTickCount();
}

void CPTZCtrl::SetAbsPos( int nZoomModuleType, FPTZVector abs_pos )
{
   FPTZVector* pCurPTZPos = &m_CurPTZPos;
   if( ZoomModuleType_ThermalImaging == nZoomModuleType ) {
      pCurPTZPos = &m_CurPTZPos_ThermalImaging;
   } else if( ZoomModuleType_WideAngle == nZoomModuleType ) {
      pCurPTZPos = &m_CurPTZPos_WideAngle;
   }

   if( abs_pos.P != PREV_CMD ) pCurPTZPos->P = abs_pos.P;
   if( abs_pos.T != PREV_CMD ) pCurPTZPos->T = abs_pos.T;
   if( abs_pos.Z != PREV_CMD ) {
      pCurPTZPos->Z = abs_pos.Z;
      if( pCurPTZPos->Z < 1.0f ) pCurPTZPos->Z = 1.0f;
      if( pCurPTZPos->Z > 100000.0f ) pCurPTZPos->Z = 100000.0f;
   }
   if( abs_pos.F != PREV_CMD ) pCurPTZPos->F = abs_pos.F;
   if( abs_pos.I != PREV_CMD ) pCurPTZPos->I = abs_pos.I;
   if( m_pMainPTZControl )
      m_pMainPTZControl->SetAbsPos( abs_pos );
}

void CPTZCtrl::SetTgtAbsPos( FPTZVector abs_pos )
{
   if( abs_pos.P != PREV_CMD ) m_TgtPTZPos.P = abs_pos.P;
   if( abs_pos.T != PREV_CMD ) m_TgtPTZPos.T = abs_pos.T;
   if( abs_pos.Z != PREV_CMD ) {
      m_TgtPTZPos.Z = abs_pos.Z;
      if( m_TgtPTZPos.Z < 1.0f ) m_TgtPTZPos.Z = 1.0f;
      if( m_TgtPTZPos.Z > 100000.0f ) m_TgtPTZPos.Z = 100000.0f;
   }
   if( abs_pos.F != PREV_CMD ) m_TgtPTZPos.F = abs_pos.F;
   if( abs_pos.I != PREV_CMD ) m_TgtPTZPos.I = abs_pos.I;
}

void CPTZCtrl::SetRefCameraParams( float fFocalLength, float fHFOV )
{
}

int CPTZCtrl::_ContPTZMove( IPTZVector move_vec )
{
   if( m_pSubPTZControl )
      return m_pSubPTZControl->_ContPTZMove( move_vec );
   return ( DONE );
}

int CPTZCtrl::GetAbsPos_HW( float& pa, float& ta, float& zf, float& f )
{
   if( m_pSubPTZControl )
      return m_pSubPTZControl->GetAbsPos_HW( pa, ta, zf, f );
   else {
      pa = m_CurPTZPos.P;
      ta = m_CurPTZPos.T;
      zf = m_CurPTZPos.Z;
      f  = m_CurPTZPos.F;
   }
   return ( DONE );
}

int CPTZCtrl::_GotoAbsPos( FPTZVector abs_pos )
{
   if( m_pSubPTZControl ) {
      return m_pSubPTZControl->_GotoAbsPos( abs_pos );
   }
   return ( DONE );
}

int CPTZCtrl::SetPreset( int preset_no )
{
   if( preset_no < MinPresetNo || preset_no > MaxPresetNo ) return ( 1 );
   m_csCmdSet.Lock();
   m_nCmdPresetNo     = preset_no;
   m_nCurrCmdType     = PTZCmdType_SetPreset;
   m_nLastSendCmdTime = GetTickCount();
   m_csCmdSet.Unlock();
   return ( 0 );
}

int CPTZCtrl::_SetPreset( int preset_no )
{
   if( preset_no < MinPresetNo || preset_no > MaxPresetNo ) return ( 1 );

   if( m_pPTZ2D ) {
      CPTZ1D* ptz1d;
      FPTZVector pvCurPos = GetAbsPos();
      // 현재 preset id와 대응되는 항목이 있다면 PTZF 절대각을 새로운 값으로 업데이트 한다.
      if( m_pPTZ2D->FindPTZ1D( preset_no, &ptz1d ) == DONE )
         *( (FPTZVector*)ptz1d ) = pvCurPos;
      // 현재 preset id와 대응되는 항목이 없다면 현재 PTZ 값을 가지는 항목을 추가한다.
      else
         m_pPTZ2D->AddPTZ1D( preset_no, pvCurPos );
      __PTZ3DFile.SaveFile();
   }
   if( m_pSubPTZControl ) {
      return m_pSubPTZControl->_SetPreset( preset_no );
   } else
      _SetPreset( preset_no );
   return ( DONE );
}

int CPTZCtrl::ClearPreset( int preset_no )
{
   if( preset_no < MinPresetNo || preset_no > MaxPresetNo ) return ( 1 );
   m_csCmdSet.Lock();
   m_nCmdPresetNo     = preset_no;
   m_nCurrCmdType     = PTZCmdType_ClearPreset;
   m_nLastSendCmdTime = GetTickCount();
   m_csCmdSet.Unlock();
   return ( DONE );
}

int CPTZCtrl::_ClearPreset( int preset_no )
{
   if( preset_no < MinPresetNo || preset_no > MaxPresetNo ) return ( 1 );
   // 현재 Preset 위치와 대응되는 PTZF 절대각 항목을 삭제한다.
   if( m_pPTZ2D )
      m_pPTZ2D->DeletePTZ1D( preset_no );
   if( m_pSubPTZControl )
      return m_pSubPTZControl->_ClearPreset( preset_no );
   else
      _ClearPreset( preset_no );
   return ( DONE );
}

int CPTZCtrl::GotoPreset( int preset_no )
{
   if( preset_no < MinPresetNo || preset_no > MaxPresetNo ) return ( 1 );

   m_csCmdSet.Lock();
   m_nCmdPresetNo     = preset_no;
   m_nCurrCmdType     = PTZCmdType_GotoPreset;
   m_nLastSendCmdTime = GetTickCount();
   m_nPTZMoveState |= PTZMoveState_JustStarted;
   m_nState &= ~PTZ_State_Stopped;
   m_csCmdSet.Unlock();

   SetContPTZTrackMode( FALSE );

   CPTZ1D* ptz1d = NULL;
   if( m_pPTZ2D ) m_pPTZ2D->FindPTZ1D( preset_no, &ptz1d );

   FPTZVector abs_pos;
   if( ptz1d ) {
      abs_pos( ptz1d->P, ptz1d->T, ptz1d->Z, ptz1d->F );
      // 필요한 경우 현재 위치를 업데이트 한다.
      if( !IsRealTimeGetAbsPosSupported() || m_PTZInfo.m_nTxOnly )
         SetAbsPos( abs_pos );
      m_TgtPTZPos = abs_pos;
   }

   return ( DONE );
}

int CPTZCtrl::_GotoPreset( int preset_no )
{
   if( PTZCtrlConnType_LinkToPTZCtrl == m_PTZInfo.m_nConnectType ) {
      CCriticalSectionSP co( m_csLinkingPTZControl );
      if( m_pLinkingPTZControl ) {
         CPTZ1D* ptz1d = NULL;
         if( m_pPTZ2D ) m_pPTZ2D->FindPTZ1D( preset_no, &ptz1d );
         FPTZVector abs_pos;
         if( ptz1d ) abs_pos( ptz1d->P, ptz1d->T, ptz1d->Z, ptz1d->F );
         GotoAbsPos( abs_pos );
      }
   }

   if( preset_no < MinPresetNo || preset_no > MaxPresetNo ) return ( 1 );

   m_nState |= PTZ_State_AutoFocusToBeStopped;
   m_nState &= ~PTZ_State_AutoFocusToBeExcuted;

   // 현재 Preset 위치에 대한 PTZ 절대각 항목을 찾아낸 후
   // 그 값을 현재 PTZ 위치 값으로 복사한다.
   CPTZ1D* ptz1d = NULL;
   if( m_pPTZ2D ) m_pPTZ2D->FindPTZ1D( preset_no, &ptz1d );
   FPTZVector abs_pos;
   if( ptz1d ) {
      abs_pos( ptz1d->P, ptz1d->T, ptz1d->Z, ptz1d->F );
   }

   BOOL bGotoPresetByPTZPreset = FALSE;
   if( NULL == ptz1d ) bGotoPresetByPTZPreset = TRUE;
   if( !IsSetAbsPosSupported() || !IsGetAbsPosSupported() ) bGotoPresetByPTZPreset = TRUE;
   if( !IsBadAbsPosMoveCam() && IsPresetByPTZPresetMove() ) bGotoPresetByPTZPreset = TRUE;
   if( IsBadAbsPosMoveCam() && !IsRealTimeGetAbsPosSupported() ) bGotoPresetByPTZPreset = TRUE;

   if( bGotoPresetByPTZPreset ) {
      if( m_pSubPTZControl )
         m_pSubPTZControl->_GotoPreset( preset_no );
      else
         _GotoPreset( preset_no );
   } else {
      if( IsBadAbsPosMoveCam() && IsRealTimeGetAbsPosSupported() ) {
         GotoAbsPos( abs_pos );
         return ( GotoPreset_HWRet_ByGotoAbsPos );
      } else {
         FPTZVector tgtPos = abs_pos;
         if( m_pSubPTZControl ) {
            m_pSubPTZControl->ConvertPanTiltOrientationRealToHW( tgtPos.P, tgtPos.T );
            m_pSubPTZControl->_GotoAbsPos( tgtPos );
         } else {
            ConvertPanTiltOrientationRealToHW( tgtPos.P, tgtPos.T );
            _GotoAbsPos( tgtPos );
         }
      }
   }

   return ( GotoPreset_HWRet_Done );
}

int CPTZCtrl::AuxOn( int nAuxNo )
{
   //m_csCmdSet.Lock (   );
   //m_nCmdAuxNo = nAuxNo;
   //m_nCurrCmdType = PTZCmdType_AuxOn;
   //m_nLastSendCmdTime = GetTickCount ();
   //m_csCmdSet.Unlock (   );
   return ( DONE );
}

int CPTZCtrl::_AuxOn( int nAuxNo )
{
   if( m_pSubPTZControl )
      return m_pSubPTZControl->_AuxOn( nAuxNo );
   return ( DONE );
}

int CPTZCtrl::AuxOff( int nAuxNo )
{
   m_csCmdSet.Lock();
   m_nCmdAuxNo        = nAuxNo;
   m_nCurrCmdType     = PTZCmdType_AuxOff;
   m_nLastSendCmdTime = GetTickCount();
   m_csCmdSet.Unlock();
   return ( DONE );
}

int CPTZCtrl::_AuxOff( int nAuxNo )
{
   if( m_pSubPTZControl )
      return m_pSubPTZControl->_AuxOff( nAuxNo );
   return ( DONE );
}

int CPTZCtrl::MenuOpen()
{
   if( PTZCtrlConnType_LinkToPTZCtrl == m_PTZInfo.m_nConnectType ) {
      CCriticalSectionSP co( m_csLinkingPTZControl );
      if( m_pLinkingPTZControl && ( PTZCtrlConnType_LinkToPTZCtrl != m_pLinkingPTZControl->m_PTZInfo.m_nConnectType ) ) {
         m_pLinkingPTZControl->MenuOpen();
         return ( DONE );
      }
   }

   m_csCmdSet.Lock();
   m_nCurrCmdType     = PTZCmdType_MenuOpen;
   m_nLastSendCmdTime = GetTickCount();
   m_csCmdSet.Unlock();
   return ( DONE );
}

int CPTZCtrl::_MenuOpen()
{
   if( m_pSubPTZControl ) {
      if( DONE == m_pSubPTZControl->_MenuOpen() ) {
         if( m_pSubPTZControl->m_nState & PTZ_State_ShowMenu )
            m_nState |= PTZ_State_ShowMenu;
         else
            m_nState &= ~( PTZ_State_ShowMenu );
      } else
         return ( 1 );
   }
   return ( DONE );
}

int CPTZCtrl::MenuEnter()
{
   if( PTZCtrlConnType_LinkToPTZCtrl == m_PTZInfo.m_nConnectType ) {
      CCriticalSectionSP co( m_csLinkingPTZControl );
      if( m_pLinkingPTZControl && ( PTZCtrlConnType_LinkToPTZCtrl != m_pLinkingPTZControl->m_PTZInfo.m_nConnectType ) ) {
         m_pLinkingPTZControl->MenuEnter();
         return ( DONE );
      }
   }

   m_csCmdSet.Lock();
   m_nCurrCmdType     = PTZCmdType_MenuEnter;
   m_nLastSendCmdTime = GetTickCount();
   m_csCmdSet.Unlock();
   return ( DONE );
}

int CPTZCtrl::_MenuEnter()
{
   if( m_pSubPTZControl )
      return m_pSubPTZControl->_MenuEnter();
   return ( DONE );
}

int CPTZCtrl::MenuCancel()
{
   if( PTZCtrlConnType_LinkToPTZCtrl == m_PTZInfo.m_nConnectType ) {
      CCriticalSectionSP co( m_csLinkingPTZControl );
      if( m_pLinkingPTZControl && ( PTZCtrlConnType_LinkToPTZCtrl != m_pLinkingPTZControl->m_PTZInfo.m_nConnectType ) ) {
         m_pLinkingPTZControl->MenuCancel();
         return ( DONE );
      }
   }
   m_csCmdSet.Lock();
   m_nCurrCmdType     = PTZCmdType_MenuCancel;
   m_nLastSendCmdTime = GetTickCount();
   m_csCmdSet.Unlock();
   return ( DONE );
}

int CPTZCtrl::_MenuCancel()
{
   if( m_pSubPTZControl )
      return m_pSubPTZControl->_MenuCancel();
   return ( DONE );
}

int CPTZCtrl::PosInit()
{
   int i;

   if( m_nState & PTZ_State_PosInitializing )
      return ( 1 );
   if( !( m_nState & PTZ_State_ThreadPTZControl ) )
      return ( 2 );
   for( i = 0; i < 3; i++ ) {
      m_csCmdSet.Lock();
      m_nCurrCmdType     = PTZCmdType_PosInit;
      m_nLastSendCmdTime = GetTickCount();
      m_csCmdSet.Unlock();
      if( m_nState & PTZ_State_PosInitializing )
         break;
      else {
         Sleep_ms( 60 );
      }
   }
   return ( DONE );
}

int CPTZCtrl::_PosInit()
{
   if( m_pSubPTZControl )
      m_pSubPTZControl->_PosInit();
   return ( DONE );
}

int CPTZCtrl::SetIRMode( int nIRMode )
{
   m_csCmdSet.Lock();
   m_PTZInfo.m_IRLightCtrlInfo.m_nIRMode = nIRMode;
   m_nCmdIRMode                          = nIRMode;
   m_nCurrCmdType                        = PTZCmdType_SetIRMode;
   m_nLastSendCmdTime                    = GetTickCount();
   m_csCmdSet.Unlock();
   return ( DONE );
}

int CPTZCtrl::_SetIRMode( int nIRMode )
{
   BOOL bSetIRMode = FALSE;
   if( m_nPTZIRCtrlMode != nIRMode ) {
      bSetIRMode = TRUE;
   } else if( m_nSetIRModeTime && ( GetTickCount() - m_nSetIRModeTime > 600000 ) ) {
      if( !( m_nPTZMoveState & PTZMoveState_Moving ) ) {
         bSetIRMode = TRUE;
      }
   }
   if( bSetIRMode ) {
      m_nPTZIRCtrlMode = nIRMode;
      m_nState |= PTZ_State_IROnOffJustChanged;
      if( m_nPTZIRCtrlMode == PTZIRCtrlMode_NightModeAndIROn ) {
         m_nPrvIRLightValue = 101;
      }
      if( m_pSubPTZControl )
         return m_pSubPTZControl->_SetIRMode( nIRMode );

      m_nSetIRModeTime = GetTickCount();
      return ( DONE );
   }
   return ( 1 );
}

int CPTZCtrl::SetIRLight( int nLight )
{
   m_csCmdSet.Lock();
   m_nCmdIRLight      = nLight;
   m_nCurrCmdType     = PTZCmdType_SetIRLight;
   m_nLastSendCmdTime = GetTickCount();
   m_csCmdSet.Unlock();
   return ( DONE );
}

int CPTZCtrl::_SetIRLight( int nLight )
{
   if( m_pSubPTZControl )
      return m_pSubPTZControl->_SetIRLight( nLight );
   return ( DONE );
}

int CPTZCtrl::SetAutoFocusOn( int bAutoFocusOn )
{
   m_csCmdSet.Lock();
   m_PTZInfo.m_bAutoFocusOn = bAutoFocusOn;
   m_bCmdAutoFocusOn        = bAutoFocusOn;
   m_nCurrCmdType           = PTZCmdType_SetAutoFocusOn;
   m_nLastSendCmdTime       = GetTickCount();
   m_csCmdSet.Unlock();
   return ( DONE );
}

int CPTZCtrl::_SetAutoFocusOn( int bAutoFocusOn )
{
   if( m_pSubPTZControl )
      return m_pSubPTZControl->_SetAutoFocusOn( bAutoFocusOn );
   return ( DONE );
}

int CPTZCtrl::SetDefog( int nDefogLevel )
{
   //m_csCmdSet.Lock (   );
   m_PTZInfo.m_bDefogOn = nDefogLevel; // UI동기화 문제로 이곳에서도 값을 복사하게 함.
   //m_nCmdDefogLevel = nDefogLevel;
   //m_nCurrCmdType = PTZCmdType_SetDefog;
   //m_nLastSendCmdTime = GetTickCount ();
   //m_csCmdSet.Unlock (   );
   return ( DONE );
}

int CPTZCtrl::SetSoftDefog( int nDefogLevel )
{
   m_csCmdSet.Lock();
   m_PTZInfo.m_bDefogOn = nDefogLevel; // UI동기화 문제로 이곳에서도 값을 복사하게 함.
   m_nCmdDefogLevel     = nDefogLevel;
   m_nCurrCmdType       = PTZCmdType_SetSoftDefog;
   m_nLastSendCmdTime   = GetTickCount();
   m_csCmdSet.Unlock();
   return ( DONE );
}

int CPTZCtrl::_SetDefog( int nDefogLevel )
{
   if( m_pSubPTZControl )
      return m_pSubPTZControl->_SetDefog( nDefogLevel );
   return ( DONE );
}

int CPTZCtrl::_SetSoftwareDefog( int nDefogLevel )
{
   if( m_pSubPTZControl )
      return m_pSubPTZControl->_SetSoftwareDefog( nDefogLevel );
   return ( DONE );
}

int CPTZCtrl::SetVideoStabilizer( int nVideoStabilization )
{
   m_csCmdSet.Lock();
   m_PTZInfo.m_bVideoStabilization = nVideoStabilization; // UI동기화 문제로 이곳에서도 값을 복사하게 함.
   m_nCmdVideoStabilization        = nVideoStabilization;
   m_nCurrCmdType                  = PTZCmdType_SetVideoStabilization;
   m_nLastSendCmdTime              = GetTickCount();
   m_csCmdSet.Unlock();
   return ( DONE );
}

int CPTZCtrl::_SetVideoStabilization( int nVideoStabilization )
{
   if( m_pSubPTZControl )
      return m_pSubPTZControl->_SetVideoStabilization( nVideoStabilization );
   return ( DONE );
}

int CPTZCtrl::BLCOn( BOOL bBLCOn )
{
   m_csCmdSet.Lock();
   m_nCmdBLCOn        = bBLCOn;
   m_nCurrCmdType     = PTZCmdType_BLCOn;
   m_nLastSendCmdTime = GetTickCount();
   m_csCmdSet.Unlock();
   return ( DONE );
}

int CPTZCtrl::_BLCOn( BOOL bBLCOn )
{
   if( m_pSubPTZControl )
      return m_pSubPTZControl->_SetBLCLevel( bBLCOn );
   return ( DONE );
}

int CPTZCtrl::SetBLCLevel( int nBLCLevel )
{
   m_csCmdSet.Lock();
   m_nCmdBLCLevel     = nBLCLevel;
   m_nCurrCmdType     = PTZCmdType_SetBLCLevel;
   m_nLastSendCmdTime = GetTickCount();
   m_csCmdSet.Unlock();
   return ( DONE );
}

int CPTZCtrl::_SetBLCLevel( int nBLCLevel )
{
   if( m_pSubPTZControl )
      return m_pSubPTZControl->_SetBLCLevel( nBLCLevel );
   return ( DONE );
}

int CPTZCtrl::SetDZoomMode( int bDZoomOn )
{
   if( PTZCtrlConnType_LinkToPTZCtrl == m_PTZInfo.m_nConnectType ) {
      CCriticalSectionSP co( m_csLinkingPTZControl );
      if( m_pLinkingPTZControl && ( PTZCtrlConnType_LinkToPTZCtrl != m_pLinkingPTZControl->m_PTZInfo.m_nConnectType ) ) {
         return m_pLinkingPTZControl->SetDZoomMode( bDZoomOn );
      }
   }
   m_csCmdSet.Lock();
   m_bCmdDZoomOn      = bDZoomOn;
   m_nCurrCmdType     = PTZCmdType_SetDZoomMode;
   m_nLastSendCmdTime = GetTickCount();
   m_csCmdSet.Unlock();
   return ( DONE );
}

int CPTZCtrl::_SetDZoomMode( int bDZoomOn )
{
   if( m_pSubPTZControl )
      return m_pSubPTZControl->_SetDZoomMode( bDZoomOn );
   return ( DONE );
}

int CPTZCtrl::SetWiper( int bWiperOn )
{
   if( PTZCtrlConnType_LinkToPTZCtrl == m_PTZInfo.m_nConnectType ) {
      CCriticalSectionSP co( m_csLinkingPTZControl );
      if( m_pLinkingPTZControl && ( PTZCtrlConnType_LinkToPTZCtrl != m_pLinkingPTZControl->m_PTZInfo.m_nConnectType ) ) {
         return m_pLinkingPTZControl->SetWiper( bWiperOn );
      }
   }
   m_PTZInfo.m_bWiperOn = bWiperOn;
   m_csCmdSet.Lock();
   m_bCmdWiperOn      = bWiperOn;
   m_nCurrCmdType     = PTZCmdType_SetWiper;
   m_nLastSendCmdTime = GetTickCount();
   m_csCmdSet.Unlock();
   return ( DONE );
}

int CPTZCtrl::_SetWiper( int bWiperOn )
{
   if( m_pSubPTZControl )
      return m_pSubPTZControl->_SetWiper( bWiperOn );
   return ( DONE );
}

int CPTZCtrl::SetHeatingWire( int bHeatingWireOn )
{
   if( PTZCtrlConnType_LinkToPTZCtrl == m_PTZInfo.m_nConnectType ) {
      CCriticalSectionSP co( m_csLinkingPTZControl );
      if( m_pLinkingPTZControl && ( PTZCtrlConnType_LinkToPTZCtrl != m_pLinkingPTZControl->m_PTZInfo.m_nConnectType ) ) {
         return m_pLinkingPTZControl->SetHeatingWire( bHeatingWireOn );
      }
   }
   m_PTZInfo.m_bHeaterOn = bHeatingWireOn;
   m_csCmdSet.Lock();
   m_bCmdHeatingWireOn = bHeatingWireOn;
   m_nCurrCmdType      = PTZCmdType_SetHeatingWire;
   m_nLastSendCmdTime  = GetTickCount();
   m_csCmdSet.Unlock();
   return ( DONE );
}

int CPTZCtrl::_SetHeatingWire( int bHeatingWireOn )
{
   if( m_pSubPTZControl )
      return m_pSubPTZControl->_SetHeatingWire( bHeatingWireOn );
   return ( DONE );
}

int CPTZCtrl::SetFan( BOOL bFanOn )
{
   m_csCmdSet.Lock();
   m_bCmdFanOn        = bFanOn;
   m_nCurrCmdType     = PTZCmdType_SetFan;
   m_nLastSendCmdTime = GetTickCount();
   m_csCmdSet.Unlock();
   return ( DONE );
}

int CPTZCtrl::_SetFan( BOOL bFanOn )
{
   if( m_pSubPTZControl )
      return m_pSubPTZControl->_SetFan( bFanOn );
   return ( DONE );
}

int CPTZCtrl::SetDayAndNightMode( int nDayAndNightMode )
{
   if( PTZCtrlConnType_LinkToPTZCtrl == m_PTZInfo.m_nConnectType ) {
      CCriticalSectionSP co( m_csLinkingPTZControl );
      if( m_pLinkingPTZControl && ( PTZCtrlConnType_LinkToPTZCtrl != m_pLinkingPTZControl->m_PTZInfo.m_nConnectType ) ) {
         return m_pLinkingPTZControl->SetDayAndNightMode( nDayAndNightMode );
      }
   }
   m_csCmdSet.Lock();
   m_nCmdDayAndNightMode = nDayAndNightMode;
   m_nCurrCmdType        = PTZCmdType_SetDayAndNightMode;
   m_nLastSendCmdTime    = GetTickCount();
   m_csCmdSet.Unlock();
   return ( DONE );
}

int CPTZCtrl::_SetDayAndNightMode( int nDayAndNightMode )
{
   if( m_pSubPTZControl )
      return m_pSubPTZControl->_SetDayAndNightMode( nDayAndNightMode );
   return ( DONE );
}

int CPTZCtrl::SetLaserIlluminator( int bLaserIlluminatorOn )
{
   if( PTZCtrlConnType_LinkToPTZCtrl == m_PTZInfo.m_nConnectType ) {
      CCriticalSectionSP co( m_csLinkingPTZControl );
      if( m_pLinkingPTZControl && ( PTZCtrlConnType_LinkToPTZCtrl != m_pLinkingPTZControl->m_PTZInfo.m_nConnectType ) ) {
         return m_pLinkingPTZControl->SetLaserIlluminator( bLaserIlluminatorOn );
      }
   }
   m_csCmdSet.Lock();
   m_bCmdLaserIlluminatorOn = bLaserIlluminatorOn;
   m_nCurrCmdType           = PTZCmdType_SetLaserIlluminator;
   m_nLastSendCmdTime       = GetTickCount();
   m_csCmdSet.Unlock();
   return ( DONE );
}

int CPTZCtrl::_SetLaserIlluminator( BOOL bLaserIlluminatorOn )
{
   if( m_pSubPTZControl )
      return m_pSubPTZControl->_SetLaserIlluminator( bLaserIlluminatorOn );
   return ( DONE );
}

int CPTZCtrl::OneShotAutoFocus()
{
   if( PTZCtrlConnType_LinkToPTZCtrl == m_PTZInfo.m_nConnectType ) {
      CCriticalSectionSP co( m_csLinkingPTZControl );
      if( m_pLinkingPTZControl && ( PTZCtrlConnType_LinkToPTZCtrl != m_pLinkingPTZControl->m_PTZInfo.m_nConnectType ) ) {
         return m_pLinkingPTZControl->OneShotAutoFocus();
      }
   }
   m_csCmdSet.Lock();
   m_nCurrCmdType     = PTZCmdType_OneShotAutoFocus;
   m_nLastSendCmdTime = GetTickCount();
   m_csCmdSet.Unlock();
   m_nPTZMoveState |= PTZMoveState_JustStarted;
   return ( DONE );
}

int CPTZCtrl::_OneShotAutoFocus()
{
   if( m_pSubPTZControl )
      return m_pSubPTZControl->_OneShotAutoFocus();
   return ( DONE );
}

int CPTZCtrl::ThermalCameraOn( BOOL bThermalCameraOn )
{
   if( PTZCtrlConnType_LinkToPTZCtrl == m_PTZInfo.m_nConnectType ) {
      CCriticalSectionSP co( m_csLinkingPTZControl );
      if( m_pLinkingPTZControl && ( PTZCtrlConnType_LinkToPTZCtrl != m_pLinkingPTZControl->m_PTZInfo.m_nConnectType ) ) {
         return m_pLinkingPTZControl->ThermalCameraOn( bThermalCameraOn );
      }
   }
   m_csCmdSet.Lock();
   m_bCmdThermalCameraOn = bThermalCameraOn;
   m_nCurrCmdType        = PTZCmdType_ThermalCameraOn;
   m_nLastSendCmdTime    = GetTickCount();
   m_csCmdSet.Unlock();
   return ( DONE );
}

int CPTZCtrl::_ThermalCameraOn( BOOL bThermalCameraOn )
{
   if( m_pSubPTZControl )
      return m_pSubPTZControl->_ThermalCameraOn( m_bCmdThermalCameraOn );
   return ( DONE );
}

int CPTZCtrl::SetZoomControlMode( int nZoomControlType )
{
   if( PTZCtrlConnType_LinkToPTZCtrl == m_PTZInfo.m_nConnectType ) {
      CCriticalSectionSP co( m_csLinkingPTZControl );
      if( m_pLinkingPTZControl && ( PTZCtrlConnType_LinkToPTZCtrl != m_pLinkingPTZControl->m_PTZInfo.m_nConnectType ) ) {
         return m_pLinkingPTZControl->SetZoomControlMode( nZoomControlType );
      }
   }
   m_csCmdSet.Lock();
   m_nCmdZoomControlType = nZoomControlType;
   m_nCurrCmdType        = PTZCmdType_SetZoomControlMode;
   m_nLastSendCmdTime    = GetTickCount();
   m_csCmdSet.Unlock();
   return ( DONE );
}

int CPTZCtrl::_SetZoomControlMode( int nZoomControlType, BOOL bSendHWCmd )
{
   if( m_pSubPTZControl )
      return m_pSubPTZControl->_SetZoomControlMode( nZoomControlType );
   return ( DONE );
}

int CPTZCtrl::SetActiveZoomCtrl()
{
   if( PTZCtrlConnType_LinkToPTZCtrl == m_PTZInfo.m_nConnectType ) {
      CCriticalSectionSP co( m_csLinkingPTZControl );
      if( m_pLinkingPTZControl && ( PTZCtrlConnType_LinkToPTZCtrl != m_pLinkingPTZControl->m_PTZInfo.m_nConnectType ) ) {
         return m_pLinkingPTZControl->SetZoomControlMode( m_PTZInfo.m_nZoomModuleType );
      }
   }
   if( -1 != m_PTZInfo.m_nZoomModuleType ) {
      m_csCmdSet.Lock();
      m_nCmdZoomControlType = m_PTZInfo.m_nZoomModuleType;
      m_nCurrCmdType        = PTZCmdType_SetZoomControlMode;
      m_nLastSendCmdTime    = GetTickCount();
      m_csCmdSet.Unlock();
   }
   return ( DONE );
}

void CPTZCtrl::GetRange_PanAngle( float& min_angle, float& max_angle )
{
   if( m_pSubPTZControl ) {
      m_pSubPTZControl->GetRange_PanAngle( min_angle, max_angle );
   } else {
      min_angle = MinPanAngle;
      max_angle = MaxPanAngle;
   }
}

void CPTZCtrl::GetRange_PanSpeed( int& min_speed, int& max_speed )
{
   if( m_pSubPTZControl ) {
      m_pSubPTZControl->GetRange_PanSpeed( min_speed, max_speed );
   } else {
      min_speed = MinPanSpeed;
      max_speed = MaxPanSpeed;
   }
}

void CPTZCtrl::GetRange_TiltAngle( float& min_angle, float& max_angle )
{
   if( m_pSubPTZControl ) {
      m_pSubPTZControl->GetRange_TiltAngle( min_angle, max_angle );
   } else {
      min_angle = MinTiltAngle;
      max_angle = MaxTiltAngle;
   }
}

void CPTZCtrl::GetRange_TiltSpeed( int& min_speed, int& max_speed )
{
   if( m_pSubPTZControl ) {
      m_pSubPTZControl->GetRange_TiltSpeed( min_speed, max_speed );
   } else {
      min_speed = MinTiltSpeed;
      max_speed = MaxTiltSpeed;
   }
}

void CPTZCtrl::GetRange_ZoomFactor( float& min_zf, float& max_zf )
{
   if( m_pSubPTZControl ) {
      m_pSubPTZControl->GetRange_ZoomFactor( min_zf, max_zf );
   } else {
      min_zf = MinZoomFactor;
      max_zf = MaxZoomFactor;
   }
}

void CPTZCtrl::GetRange_ZoomSpeed( int& min_speed, int& max_speed )
{
   if( m_pSubPTZControl ) {
      m_pSubPTZControl->GetRange_ZoomSpeed( min_speed, max_speed );
   } else {
      min_speed = MinZoomSpeed;
      max_speed = MaxZoomSpeed;
   }
}

void CPTZCtrl::GetRange_PresetNo( int& min_preset_no, int& max_preset_no )
{
   if( m_pSubPTZControl ) {
      m_pSubPTZControl->GetRange_PresetNo( min_preset_no, max_preset_no );
   } else {
      min_preset_no = MinPresetNo;
      max_preset_no = MaxPresetNo;
   }
}

BYTE CPTZCtrl::GetCheckSum( BYTE* pCmd, int nCmdLen )
{
   BYTE cmd[g_nMaxCmdLen];
   memcpy( cmd, pCmd, sizeof( cmd ) );
   CheckSum( cmd, nCmdLen );
   return cmd[nCmdLen - 1];
}

void CPTZCtrl::CopyFrom( CPTZCtrl* pFrom )
{
   //m_nSendCmdPeriod = pFrom->m_nSendCmdPeriod;

   // Speed Range
   MinPanSpeed  = pFrom->MinPanSpeed;
   MaxPanSpeed  = pFrom->MaxPanSpeed;
   MinTiltSpeed = pFrom->MinTiltSpeed;
   MaxTiltSpeed = pFrom->MaxTiltSpeed;
   MinZoomSpeed = pFrom->MinZoomSpeed;
   MaxZoomSpeed = pFrom->MaxZoomSpeed;

   // Pan, Tilt, Zoom Range
   MinPanAngle   = pFrom->MinPanAngle;
   MaxPanAngle   = pFrom->MaxPanAngle;
   MinTiltAngle  = pFrom->MinTiltAngle;
   MaxTiltAngle  = pFrom->MaxTiltAngle;
   MinZoomFactor = pFrom->MinZoomFactor;
   MaxZoomFactor = pFrom->MaxZoomFactor;

   MinPresetNo = pFrom->MinPresetNo;
   MaxPresetNo = pFrom->MaxPresetNo;

   m_fFocalLength      = pFrom->m_fFocalLength;
   m_fFocalLengthRatio = pFrom->m_fFocalLengthRatio;

   m_fZoomLogVel_ContSpd1   = pFrom->m_fZoomLogVel_ContSpd1;
   m_fZoomLogVel_AbsPosMove = pFrom->m_fZoomLogVel_AbsPosMove;

   m_ZoomReal2HWMapInfo               = pFrom->m_ZoomReal2HWMapInfo;
   m_Zoom2FLMapInfo                   = pFrom->m_Zoom2FLMapInfo;
   m_PanAngleVel2HWSpdMapInfo         = pFrom->m_PanAngleVel2HWSpdMapInfo;
   m_TiltAngleVel2HWSpdMapInfo        = pFrom->m_TiltAngleVel2HWSpdMapInfo;
   m_ZoomLogVel2HWSpdMapInfo          = pFrom->m_ZoomLogVel2HWSpdMapInfo;
   m_GotoAbsVel2HWSpdMapInfo          = pFrom->m_GotoAbsVel2HWSpdMapInfo;
   m_Zoom2PTSpdRatioMapInfo           = pFrom->m_Zoom2PTSpdRatioMapInfo;
   m_PanAngleVelocityTable            = pFrom->m_PanAngleVelocityTable;
   m_TiltAngleVelocityTable           = pFrom->m_TiltAngleVelocityTable;
   m_AngleSpeedCheckZoomTable         = pFrom->m_AngleSpeedCheckZoomTable;
   m_nAngleSpeedCheckZoomTableItemNum = pFrom->m_nAngleSpeedCheckZoomTableItemNum;
}

BOOL CPTZCtrl::IsNeedConnectionProc()
{
   // 아이피 카메라 이고
   //    메인 스트림 연결에 의존적이지 않는 경우 OK
   // TCP 제어서버에 직접 연결하는 방식의 경우에는 무조건 OK
   return TRUE;
}

int CPTZCtrl::IsContPTZMoveStopped()
{
   return m_InputPTZMoveHW.IsContPTZMoveStopped();
}

int CPTZCtrl::IsContPTZFIMoveStopped()
{
   return m_InputPTZMoveHW.IsContPTZFIMoveStopped();
}

int CPTZCtrl::GetCurrContPTZMoveSpeed( FPTZVector& pvCurPTZMoveSpeed )
{
   if( IsPTZConnected() ) return ( 1 );
   pvCurPTZMoveSpeed = m_CurrPTZMoveHW;
   return DONE;
}

BOOL CPTZCtrl::IsPTZFMoving()
{
   if( m_nPTZMoveState & PTZMoveState_Moving )
      return ( TRUE );
   return FALSE;
}

void CPTZCtrl::CheckPTZMoving()
{
   BOOL bDebugPTZMoveStartStop = FALSE;
   int nAvgPeriod              = 250 * 2;
   if( PTZCmdType_GotoAbsPos == m_nCurrCmdType ) nAvgPeriod = 300 * 2;
   FPTZVector pvAvgPTZPos = m_pPTZCtrlLogItemQueue->CalcAvgAbsPTZPos( nAvgPeriod );
   float fZoomLogDiff     = 0.0f;

   if( m_PrevAvgPTZPos.P != NO_CMD ) {
      if( m_nPTZMoveState & PTZMoveState_Moving ) // 이동중이면
      {
         BOOL bSameAbsPos = FALSE;
         if( IsRealTimeGetAbsPosMode() ) {
            if( fabs( pvAvgPTZPos.P - m_PrevAvgPTZPos.P ) < 0.01f && fabs( pvAvgPTZPos.T - m_PrevAvgPTZPos.T ) < 0.01f && fabs( log2( pvAvgPTZPos.Z ) - log2( m_PrevAvgPTZPos.Z ) ) < 0.01f && fabs( pvAvgPTZPos.F - m_PrevAvgPTZPos.F ) < 3.0f ) {
               m_nAbsPosStopCount++;
            }
            if( m_nAbsPosStopCount > 2 ) {
               bSameAbsPos = TRUE;
               if( m_nAbsPosStopCount == 3 )
                  m_nPTZMoveStopCmdTick = GetTickCount();
            }
         } else {
            bSameAbsPos = TRUE;
         }
         if( m_CurrPTZMoveHW.IsContPTZFIMoveStopped() && 0 == m_nPTZMoveStopCmdTick )
            m_nPTZMoveStopCmdTick = GetTickCount();

         BOOL bMoveCmdOngoing = FALSE;
         if( m_nCurrCmdType == PTZCmdType_GotoAbsPos ) {
            if( m_PrevPTZPos != m_CurPTZPos ) {
               bMoveCmdOngoing = TRUE;
            }
         }
         BOOL bAbsPosCmdJusExcuted = FALSE;
         if( m_nGotoAbsPosTime && ( GetTickCount() - m_nGotoAbsPosTime <= m_nGotoAbsPosMovingTime ) ) {
            bAbsPosCmdJusExcuted = TRUE;
         }

         // 연속이동방식으로 멈추었거나 동일한 절대위치의 경우

         if( m_CurrPTZMoveHW.IsContPTZFIMoveStopped() && !( m_nState & PTZ_State_Stopped ) && bSameAbsPos && !bMoveCmdOngoing && !bAbsPosCmdJusExcuted ) {
            uint nWaitTime = m_nSendCmdPeriod * 3 + 50;

            if( !IsRealTimeGetAbsPosMode() ) {
               if( !( m_nState & PTZ_State_ContPTZTracking ) ) {
                  if( PTZCmdType_GotoAbsPos == m_nPrevCmdType || PTZCmdType_GotoPreset == m_nPrevCmdType ) {
                     nWaitTime = m_nGotoAbsPosMovingTime;
                  } else if( PTZCmdType_ContinuousMove == m_nPrevCmdType ) {
                     nWaitTime = m_nSendCmdPeriod * 4 + 50;
                  }
               }
            }
            if( m_PTZInfo.m_bVideoStabilization )
               nWaitTime += 2000;
            if( GetTickCount() - m_nPTZMoveStopCmdTick > nWaitTime ) {
               logv( "[CheckPTZMoving]GetTick - MoveStopTick = nWaitTime : %d", nWaitTime );
               m_nPTZMoveState |= PTZMoveState_JustStopped;
               m_nState |= PTZ_State_Stopped;
               // 목표 위치와 이동한 위치의 좌표 값이 일치하지 않는 카메라가 있음. (특히 다후와)
               if( !( m_nState & PTZ_State_ContPTZTracking ) ) {
                  BOOL bUpdateCurPosByTgtPos = FALSE;
                  if( !IsGetAbsPosSupported() ) {
                     bUpdateCurPosByTgtPos = TRUE;
                  }
                  if( IsRealTimeGetAbsPosMode() && !m_bOnceReceivePanTiltPos ) {
                     if( PTZControlType_LinkingToOtherPTZCtrl != m_nPTZControlType )
                        bUpdateCurPosByTgtPos = TRUE;
                  }
                  if( bUpdateCurPosByTgtPos ) {
                     m_CurPTZPos = m_TgtPTZPos;
                  }
               }
            }
         }
      } else // 이동중이 아니면
      {
         // 연속이동에 의하여 이동하기 시작함.
         if( FALSE == m_InputPTZMoveHW.IsContPTZFIMoveStopped() ) {
            m_nPTZMoveState |= PTZMoveState_JustStarted;
            if( bDebugPTZMoveStartStop ) logd( "[CH:%d] +++ PTZMoveState_JustStarted - (1)\n", m_nChannelNo_IVX + 1 );
         }
         if( FALSE == m_CurrPTZMoveHW.IsContPTZFIMoveStopped() ) {
            m_nPTZMoveState |= PTZMoveState_JustStarted;
            if( bDebugPTZMoveStartStop ) logd( "[CH:%d] +++ PTZMoveState_JustStarted - (2)\n", m_nChannelNo_IVX + 1 );
         }
         if( ( m_nState & PTZ_State_ContPTZTracking ) && IsRealTimeGetAbsPosMode() ) {
            if( FALSE == m_pContAbsPosTracker->m_fvContMoveVecHW.IsContPTZMoveStopped() ) {
               m_nPTZMoveState |= PTZMoveState_JustStarted;
               if( bDebugPTZMoveStartStop ) logd( "[CH:%d] +++ PTZMoveState_JustStarted - (3)\n", m_nChannelNo_IVX + 1 );
            }
         }

         // 절대각 이동 방식으로 이동하기 시작함.
         if( fabs( pvAvgPTZPos.P - m_PrevAvgPTZPos.P ) > 0.01f || fabs( pvAvgPTZPos.T - m_PrevAvgPTZPos.T ) > 0.01f || fabs( log2( pvAvgPTZPos.Z ) - log2( m_PrevAvgPTZPos.Z ) ) > 0.01f ) {
            if( IsRealTimeGetAbsPosMode() ) {
               m_nPTZMoveState |= PTZMoveState_JustStarted;
               if( bDebugPTZMoveStartStop ) logd( "[CH:%d] +++ PTZMoveState_JustStarted - (4)\n", m_nChannelNo_IVX + 1 );
            }
         }
      }
   }
   if( m_nPTZMoveState & PTZMoveState_JustStarted ) {
      m_nState &= ~PTZ_State_Stopped;
      // PTZ 카메라가 막 움직이기 시작함.
      if( ( PTZCmdType_GotoAbsPos == m_nCurrCmdType || PTZCmdType_GotoPreset == m_nCurrCmdType ) ) {
         if( !IsRealTimeGetAbsPosMode() ) {
            fZoomLogDiff          = fabs( log( m_CurPTZPos.Z ) * MC_LOG2_INV - log( m_PrevPTZPosSC.Z ) * MC_LOG2_INV );
            m_nPTZMoveStopCmdTick = uint( GetTickCount() + 1000 + fZoomLogDiff * 1000 );
         } else {
            m_nPTZMoveStopCmdTick = GetTickCount() + 1000;
         }
      } else
         m_nPTZMoveStopCmdTick = 0;
      m_nAbsPosStopCount = 0;
      m_nPTZMoveState |= PTZMoveState_Moving;
      if( m_pPTZStatusCallback )
         m_pPTZStatusCallback->OnPTZMoveStarted();
      m_nPTZMoveState &= ~PTZMoveState_JustStarted;
   }
   if( m_nPTZMoveState & PTZMoveState_JustStopped ) {
      if( bDebugPTZMoveStartStop ) logd( "[CH:%d] --- PTZMoveState_JustStopped\n", m_nChannelNo_IVX + 1 );
      // PTZ 카메라가 막 멈추었음.
      if( m_pPTZStatusCallback )
         m_pPTZStatusCallback->OnPTZMoveStopped();

      m_nPTZMoveState &= ~PTZMoveState_Moving;
      m_nPTZMoveState &= ~PTZMoveState_JustStopped;
   }
   m_PrevAvgPTZPos = pvAvgPTZPos;
   m_PrevPTZPosSC  = m_CurPTZPos;
}

BOOL CPTZCtrl::IsSinglePTZTrkSupported()
{
   if( m_nCapabilities & PTZCtrlCap_SinglePTZTrackNotSupported )
      return FALSE;
   return TRUE;
}

BOOL CPTZCtrl::IsRealTimeGetAbsPosSupported()
{
   if( !IsGetAbsPosSupported() )
      return FALSE;
   if( m_nCapabilities & PTZCtrlCap_RealTimeGetAbsPosNotSupported )
      return FALSE;
   return TRUE;
}

BOOL CPTZCtrl::IsRealTimeGetAbsPosMode()
{
   if( PTZCtrlConnType_LinkToPTZCtrl == m_PTZInfo.m_nConnectType ) {
      if( m_pLinkingPTZControl ) {
         return m_pLinkingPTZControl->IsRealTimeGetAbsPosMode();
      }
   } else {
      if( !IsRealTimeGetAbsPosSupported() )
         return FALSE;
      if( m_PTZInfo.m_nTxOnly )
         return FALSE;
   }
   return TRUE;
}

int CPTZCtrl::IsGetAbsPosSupported()
{
   if( PTZCtrlConnType_LinkToPTZCtrl == m_PTZInfo.m_nConnectType ) {
      if( m_pLinkingPTZControl ) {
         return m_pLinkingPTZControl->IsGetAbsPosSupported();
      }
   } else {
      if( m_nCapabilities & PTZCtrlCap_GetAbsPosNotSupported )
         return FALSE;
   }
   return TRUE;
}

BOOL CPTZCtrl::IsSetAbsPosSupported()
{
   if( PTZCtrlConnType_LinkToPTZCtrl == m_PTZInfo.m_nConnectType ) {
      if( m_pLinkingPTZControl ) {
         return m_pLinkingPTZControl->IsGetAbsPosSupported();
      }
   } else {
      if( m_nCapabilities & PTZCtrlCap_SetAbsPosNotSupported )
         return FALSE;
   }
   return TRUE;
}

BOOL CPTZCtrl::IsPresetMoveByUserSpecifiedSpeedSupported()
{
   if( PTZCtrlConnType_LinkToPTZCtrl == m_PTZInfo.m_nConnectType ) {
      if( m_pLinkingPTZControl ) {
         return m_pLinkingPTZControl->IsPresetMoveByUserSpecifiedSpeedSupported();
      }
   } else {
      if( m_nCapabilities & PTZCtrlCap_PresetMoveByUserSpecifiedSpeedSupported )
         return TRUE;
   }
   return FALSE;
}

BOOL CPTZCtrl::IsBadAbsPosMoveCam()
{
   if( IsSetAbsPosSupported() )
      if( m_nCapabilities & PTZCtrlCap_BadAbsPosMoveMoveCam )
         return TRUE;
   return FALSE;
}

BOOL CPTZCtrl::IsIRLightOn()
{
   if( m_pSubPTZControl ) {
      if( m_pSubPTZControl->m_nCapabilities & PTZCtrlCap_IRLigtControlSupported ) {
         if( m_nPTZIRCtrlMode == PTZIRCtrlMode_NightModeAndIROn )
            return TRUE;
      }
   } else {
      if( m_nCapabilities & PTZCtrlCap_IRLigtControlSupported ) {
         if( m_nPTZIRCtrlMode == PTZIRCtrlMode_NightModeAndIROn )
            return TRUE;
      }
   }
   return FALSE;
}

BOOL CPTZCtrl::IsPTZPosInitSupported()
{
   if( m_nCapabilities & PTZCtrlCap_PTZPosInitSupported )
      return TRUE;
   return FALSE;
}

BOOL CPTZCtrl::IsPresetByPTZPresetMove()
{
   if( m_nCapabilities & PTZCtrlCap_GotoPresetByPTZPreset )
      return TRUE;
   return FALSE;
}

BOOL CPTZCtrl::DoesNotReleasePropPanTiltSpeedOpt()
{
   if( m_nCapabilities & PTZCtrlCap_DoesNotReleasePropPanTiltSpeedOpt )
      return TRUE;
   return FALSE;
}

int CPTZCtrl::OffsetPosition( float cx, float cy, int zt )
{
   if( PTZCtrlConnType_LinkToPTZCtrl == m_PTZInfo.m_nConnectType ) {
      CCriticalSectionSP co( m_csLinkingPTZControl );
      if( m_pLinkingPTZControl && ( PTZCtrlConnType_LinkToPTZCtrl != m_pLinkingPTZControl->m_PTZInfo.m_nConnectType ) ) {
         m_pLinkingPTZControl->OffsetPosition( cx, cx, m_PTZInfo.m_nZoomModuleType );
      }
   }
   m_nCurZoomModuleType = zt;
   FBox2D tgt_box( cx - 0.00001f, cy - 0.00001f, 0.00002f, 0.00002f );
   OffsetPosition( tgt_box );
   return ( DONE );
}

int CPTZCtrl::OffsetPosition( FBox2D tgt_box, int zt )
{
   if( PTZCtrlConnType_LinkToPTZCtrl == m_PTZInfo.m_nConnectType ) {
      CCriticalSectionSP co( m_csLinkingPTZControl );
      if( m_pLinkingPTZControl && ( PTZCtrlConnType_LinkToPTZCtrl != m_pLinkingPTZControl->m_PTZInfo.m_nConnectType ) ) {
         m_pLinkingPTZControl->OffsetPosition( tgt_box, m_PTZInfo.m_nZoomModuleType );
         return ( DONE );
      }
   }
   m_nCurZoomModuleType = zt;

   m_csCmdSet.Lock();
   m_b2TargetBox  = tgt_box;
   m_TgtPTZPos.PS = 0;
   m_TgtPTZPos.TS = 0;

   m_nState |= ( PTZ_State_OffsetByBox | PTZ_State_AbsolutePosBeExcuted );
   m_nCurrCmdType     = PTZCmdType_GotoAbsPos;
   m_nLastSendCmdTime = GetTickCount();
   m_csCmdSet.Unlock();
   return ( DONE );
}

void CPTZCtrl::SetOpenMode( int nOpenMode )
{
   m_nOpenMode = nOpenMode;
}

void CPTZCtrl::SetSWAutoFocus( int flag_on )
{
   if( PTZCtrlConnType_LinkToPTZCtrl == m_PTZInfo.m_nConnectType ) {
      CCriticalSectionSP co( m_csLinkingPTZControl );
      if( m_pLinkingPTZControl && ( PTZCtrlConnType_LinkToPTZCtrl != m_pLinkingPTZControl->m_PTZInfo.m_nConnectType ) ) {
         return m_pLinkingPTZControl->SetSWAutoFocus( flag_on );
      }
   }
   if( !flag_on )
      m_nState &= ~( PTZ_State_AutoFocus );
   else
      m_nState |= ( PTZ_State_AutoFocus );
}

void CPTZCtrl::SetContPTZTrackMode( BOOL bContPTZTrack )
{
   if( PTZCtrlConnType_LinkToPTZCtrl == m_PTZInfo.m_nConnectType ) {
      CCriticalSectionSP co( m_csLinkingPTZControl );
      if( m_pLinkingPTZControl && ( PTZCtrlConnType_LinkToPTZCtrl != m_pLinkingPTZControl->m_PTZInfo.m_nConnectType ) ) {
         return m_pLinkingPTZControl->SetContPTZTrackMode( bContPTZTrack );
      }
   }

   if( bContPTZTrack )
      m_nState |= PTZ_State_ContPTZTrackingMode;
   else
      m_nState &= ~PTZ_State_ContPTZTrackingMode;

   if( m_pSubPTZControl ) {
      if( bContPTZTrack )
         m_pSubPTZControl->m_nState |= PTZ_State_ContPTZTrackingMode;
      else
         m_pSubPTZControl->m_nState &= ~PTZ_State_ContPTZTrackingMode;
   }

   if( !IsRealTimeGetAbsPosMode() )
      bContPTZTrack = FALSE;

   // 절대각 이동에 문제가 있는 카메라의 경우 연속이동에의한 절대가 이동모드를 사용한다.
   if( IsRealTimeGetAbsPosMode() && IsBadAbsPosMoveCam() )
      bContPTZTrack = TRUE;

   if( bContPTZTrack )
      m_nState |= PTZ_State_ContPTZTracking;
   else
      m_nState &= ~PTZ_State_ContPTZTracking;

   if( m_pSubPTZControl ) {
      if( bContPTZTrack )
         m_pSubPTZControl->m_nState |= PTZ_State_ContPTZTracking;
      else
         m_pSubPTZControl->m_nState &= ~PTZ_State_ContPTZTracking;
   }
}

BOOL CPTZCtrl::IsContPTZTrackMode()
{
   if( PTZCtrlConnType_LinkToPTZCtrl == m_PTZInfo.m_nConnectType ) {
      CCriticalSectionSP co( m_csLinkingPTZControl );
      if( m_pLinkingPTZControl && ( PTZCtrlConnType_LinkToPTZCtrl != m_pLinkingPTZControl->m_PTZInfo.m_nConnectType ) ) {
         return m_pLinkingPTZControl->IsContPTZTrackMode();
      }
   }

   BOOL bContAbsMove = FALSE;
   if( !m_pSubPTZControl ) {
      if( m_nState & PTZ_State_ContPTZTracking )
         bContAbsMove = TRUE;
   } else {
      if( m_pSubPTZControl->m_nState & PTZ_State_ContPTZTracking )
         bContAbsMove = TRUE;
   }
   return bContAbsMove;
}

void CPTZCtrl::SetUserCtrlMode( BOOL bUserCtrl )
{
   if( PTZCtrlConnType_LinkToPTZCtrl == m_PTZInfo.m_nConnectType ) {
      CCriticalSectionSP co( m_csLinkingPTZControl );
      if( m_pLinkingPTZControl && ( PTZCtrlConnType_LinkToPTZCtrl != m_pLinkingPTZControl->m_PTZInfo.m_nConnectType ) ) {
         return m_pLinkingPTZControl->SetUserCtrlMode( bUserCtrl );
      }
   }
   if( bUserCtrl )
      m_nState |= PTZ_State_UserCtrl;
   else
      m_nState &= ~PTZ_State_UserCtrl;
}

void CPTZCtrl::SetSmoothZoomCtrlMode( BOOL bContPTZTrack )
{
   if( PTZCtrlConnType_LinkToPTZCtrl == m_PTZInfo.m_nConnectType ) {
      CCriticalSectionSP co( m_csLinkingPTZControl );
      if( m_pLinkingPTZControl && ( PTZCtrlConnType_LinkToPTZCtrl != m_pLinkingPTZControl->m_PTZInfo.m_nConnectType ) ) {
         return m_pLinkingPTZControl->SetSmoothZoomCtrlMode( bContPTZTrack );
      }
   }

   // 부드러운 줌제어 모드는 연속모드PTZ추적모드시 줌이 서서히 부드럽게 이동되도록 하는 모드이다.
   // PTZ_State_SmoothZoomCtrl상태는 PTZ_State_ContPTZTracking 상태가 On인 경우에만 적용된다.

   if( bContPTZTrack )
      m_nState |= PTZ_State_SmoothZoomCtrl;
   else
      m_nState &= ~PTZ_State_SmoothZoomCtrl;

   if( m_pSubPTZControl ) {
      if( bContPTZTrack )
         m_pSubPTZControl->m_nState |= PTZ_State_SmoothZoomCtrl;
      else
         m_pSubPTZControl->m_nState &= ~PTZ_State_SmoothZoomCtrl;
   }
}

void CPTZCtrl::ResetAvgTgtVelocity()
{
   if( PTZCtrlConnType_LinkToPTZCtrl == m_PTZInfo.m_nConnectType ) {
      CCriticalSectionSP co( m_csLinkingPTZControl );
      if( m_pLinkingPTZControl && ( PTZCtrlConnType_LinkToPTZCtrl != m_pLinkingPTZControl->m_PTZInfo.m_nConnectType ) ) {
         return m_pLinkingPTZControl->ResetAvgTgtVelocity();
      }
   }
   m_pContAbsPosTracker->ResetAvgTgtVelocity();
}

float CPTZCtrl::GetZoomHW( float zoom_real )
{
   if( m_ZoomReal2HWMapInfo.m_pTbl )
      return _GetBValue( m_ZoomReal2HWMapInfo, zoom_real );
   else
      return zoom_real;
}

float CPTZCtrl::GetZoomReal( float zoom_hw )
{
   if( m_ZoomReal2HWMapInfo.m_pTbl )
      return _GetAValue( m_ZoomReal2HWMapInfo, zoom_hw );
   else
      return zoom_hw;
}

float CPTZCtrl::GetFocalLength()
{
   float fCurZoom = GetCurrZoom();
   return GetFocalLength( fCurZoom );
}

float CPTZCtrl::GetFocalLength( float zoom )
{
   return GetFocalLength( zoom, FSize2D( float( m_s2VideoSize.Width ), float( m_s2VideoSize.Height ) ) );
}

float CPTZCtrl::GetFocalLength( float zoom, FSize2D s2VideoSize )
{
   FPoint2D scale( s2VideoSize.Width / m_s2RefImgSize_FL.Width, s2VideoSize.Height / m_s2RefImgSize_FL.Height );
   if( m_Zoom2FLMapInfo.m_pTbl ) {
      float fFL = _GetBValue( m_Zoom2FLMapInfo, zoom );
      return fFL * zoom * scale.X;
   } else
      return m_fFocalLength * zoom * scale.X;

   return ( 0.0f );
}

float CPTZCtrl::GetHFOV()
{
   float fFocalLength = GetFocalLength();
   return atan( m_s2VideoSize.Width * 0.5f / fFocalLength ) * MC_RAD2DEG;
}

FSize2D CPTZCtrl::GetRefImgSizeForcalLengthEstimation()
{
   return m_s2RefImgSize_FL;
}

uint32_t CPTZCtrl::GetMaxZoomInTime()
{
   return GetZoomMoveTime( MaxZoomFactor, GetAbsPos().Z );
}

uint32_t CPTZCtrl::GetZoomMoveTime( float zoom_start, float zoom_end )
{
   float delta_zoom_log = fabs( log( zoom_end ) * MC_LOG2_INV - log( zoom_start ) * MC_LOG2_INV );
   float delta_time     = fabs( delta_zoom_log ) / m_fZoomLogVel_AbsPosMove;
   if( delta_time < 0.0f )
      delta_time = 0.0f;
   return uint32_t( delta_time * 1000 );
}

float CPTZCtrl::GetZoomLogSpeed( int nZoomSpeed )
{
   float fZoomLogSpeed = 0.0f;
   if( m_ZoomLogVel2HWSpdMapInfo.m_pTbl ) {
      fZoomLogSpeed = _GetBValue( m_ZoomLogVel2HWSpdMapInfo, fabs( float( nZoomSpeed ) ) );
      if( nZoomSpeed < 0 )
         fZoomLogSpeed *= -1.0f;
   } else {
      fZoomLogSpeed = m_fZoomLogVel_ContSpd1 * nZoomSpeed;
   }
   return fZoomLogSpeed;
}

int CPTZCtrl::GetPTZMovePeriodByOffsetPos( FPTZVector offset_pos )
{
   uint32_t nPTZMoveTime = 0;
   if( m_pSubPTZControl ) {
      nPTZMoveTime = m_pSubPTZControl->GetPTZMovePeriodByOffsetPos( offset_pos );
   } else {
      if( IsRealTimeGetAbsPosSupported() ) {
         uint32_t nPanMoveTime, nTiltMoveTime, nZoomMoveTime;
         if( offset_pos.P > 180.0f ) offset_pos.P = 360.0f - offset_pos.P;
         if( offset_pos.T > 180.0f ) offset_pos.T = 360.0f - offset_pos.T;
         nPanMoveTime  = uint( fabs( offset_pos.P ) / MaxAbsPosPanMoveSpeed * 1000 );
         nTiltMoveTime = uint( fabs( offset_pos.T ) / MaxAbsPosTiltMoveSpeed * 1000 );
         nPTZMoveTime  = nPanMoveTime;
         if( nPTZMoveTime < nTiltMoveTime )
            nPTZMoveTime = nTiltMoveTime;
         nZoomMoveTime = uint( fabs( offset_pos.Z ) / m_fZoomLogVel_AbsPosMove * 1000 );
         if( nPTZMoveTime < nZoomMoveTime )
            nPTZMoveTime = nZoomMoveTime;
         nPTZMoveTime += AbsPosMoveDelayTimeInMilliSec;
      } else {
         nPTZMoveTime = uint( ( log( MaxZoomFactor ) * MC_LOG2_INV ) / m_fZoomLogVel_AbsPosMove * 1000 + AbsPosMoveDelayTimeInMilliSec );
      }
   }
   return ( nPTZMoveTime );
}

int CPTZCtrl::GetPTZMovePeriodByTargetPos( FPTZVector tgt_pos )
{
   uint32_t nPTZMoveTime = 0;
   if( m_pSubPTZControl ) {
      nPTZMoveTime = m_pSubPTZControl->GetPTZMovePeriodByTargetPos( tgt_pos );
   } else {
      FPTZVector pvOffsetPos;
      FPTZVector pvCurPos = GetAbsPos();
      pvOffsetPos.P       = fabs( tgt_pos.P - pvCurPos.P );
      pvOffsetPos.T       = fabs( tgt_pos.T - pvCurPos.T );
      if( pvOffsetPos.P > 180.0f ) pvOffsetPos.P = 360.0f - pvOffsetPos.P;
      if( pvOffsetPos.T > 180.0f ) pvOffsetPos.T = 360.0f - pvOffsetPos.T;
      pvOffsetPos.Z = log( tgt_pos.Z ) * MC_LOG2_INV - log( pvCurPos.Z ) * MC_LOG2_INV;
      nPTZMoveTime  = GetPTZMovePeriodByOffsetPos( pvOffsetPos );
   }
   return ( nPTZMoveTime );
}

float CPTZCtrl::GetHorizontalFOV()
{
   float fZoom        = GetCurrZoom();
   float fFocalLength = m_fFocalLength * fZoom;
   return atan( m_s2RefImgSize_FL.Width * 0.5f / fFocalLength ) * MC_RAD2DEG;
}

float CPTZCtrl::GetVerticalFOV()
{
   float fZoom        = GetCurrZoom();
   float fFocalLength = m_fFocalLength * fZoom;
   return atan( m_s2RefImgSize_FL.Height * 0.5f / fFocalLength );
}

float CPTZCtrl::GetPositionDifference( FPTZVector& o_pos )
{
   float dp = fabs( m_TgtPTZPos.P - o_pos.P );
   if( dp > 180.0f ) dp = 360.0f - dp;
   float dt = m_TgtPTZPos.T - o_pos.T;
   float pd = (float)sqrt( dp * dp + dt * dt );
   return ( pd );
}

uint CPTZCtrl::GetLastSendCmdTime()
{
   int nLastSendCmdTime = m_nLastSendCmdTime;
   if( m_pMainPTZControl )
      nLastSendCmdTime = m_pMainPTZControl->m_nLastSendCmdTime;
   return nLastSendCmdTime;
}

void CPTZCtrl::CheckAbsPosRes()
{
   // 절대각 요청에 의한 응답이 제대로 오는지 체크하는 함수이다.

   uint nCurTick = GetTickCount();

   if( IsGetAbsPosSupported() && !m_PTZInfo.m_nTxOnly ) {
      uint nDiffTime;
      // 요청시간과 응답시간과의 차이시간을 계산한다.
      if( m_nReqAbsPosTime > m_nGetAbsPosTime )
         nDiffTime = m_nReqAbsPosTime - m_nGetAbsPosTime;
      else
         nDiffTime = 0;

      // 한동안 제어를 하지 않다가 제어를 시작하게 되는 경우는 체크하지 않도록 한다.
      if( m_nReqAbsPosTime - m_nPrvReqAbsPosTime < 1000 ) {
         // 시간차이가 5초이상 나면 우선 지연상태로 본다.
         if( nDiffTime > 5000 )
            m_bGetAbsPosError = TRUE;
         else
            m_bGetAbsPosError = FALSE;
      }

      if( !m_bPrvGetAbsPosError && m_bGetAbsPosError ) {
         //logd ("Getting PTZ abs position error just begun !!\n");
      }
      if( m_bPrvGetAbsPosError && !m_bGetAbsPosError ) {
         //logd ("Getting PTZ abs position error just ended ^^\n");
      }
   }

   m_nPrvReqAbsPosTime  = m_nReqAbsPosTime;
   m_bPrvGetAbsPosError = m_bGetAbsPosError;
}

void CPTZCtrl::SignalPanTiltPosReceived()
{
   if( m_pMainPTZControl ) {
      m_pMainPTZControl->SignalPanTiltPosReceived();
   } else {
      m_evtRecvAbsPos.SetEvent();
      m_bOnceReceivePanTiltPos = TRUE;
   }
}

void CPTZCtrl::ConvertAngle2HWVel( FPTZVector angle_vel, FPTZVector& hw_vel )
{
   hw_vel = angle_vel;

   float fCurZoom = m_CurPTZPos.Z;

   // Zoom Proportional Jog 미사용시 1배줌의 H/W 각속도로 설정됨 (mkjang-140425), 수정(mkjang-151125)
   if( m_nCapabilities & PTZCtrlCap_ZoomProportionalJog ) {
      if( m_pSubPTZControl && ( FALSE == m_pSubPTZControl->m_PTZInfo.m_bZoomProportionalJog ) )
         fCurZoom = 1.0f;
   }

   // Pan, Tilt의 Angle Velocity에 대응되는 하드웨어 줌 값을 얻는다.
   float fPTSpdFactor = 1.0f;
   // PTZ 카메라가 Proportional Pan/Tilt Speed를 지원하는데
   // 그 값을 보정해야할 필요가 있는 경우.
   if( m_Zoom2PTSpdRatioMapInfo.m_pTbl && DoesNotReleasePropPanTiltSpeedOpt() && !( m_nState & PTZ_State_ContPTZSpeedCheck ) ) {
      float fPanTiltSpeedRatioTo1x = 1.0f;
      fPanTiltSpeedRatioTo1x       = _GetBValue( m_Zoom2PTSpdRatioMapInfo, fCurZoom );
      fPTSpdFactor                 = 1.0f / fPanTiltSpeedRatioTo1x;
   }
   if( angle_vel.PS != NO_CMD ) {
      float fPanAbsAngleVel = fabs( angle_vel.PS );
      if( m_GotoAbsVel2HWSpdMapInfo.m_pTbl ) {
         hw_vel.PS = _GetBValue( m_GotoAbsVel2HWSpdMapInfo, fPanAbsAngleVel );
         if( int( hw_vel.PS ) == 0 && fPanAbsAngleVel > 0.0001f ) {
            hw_vel.PS = m_GotoAbsVel2HWSpdMapInfo.m_pTbl[1].B;
         }
      }
   }
   if( angle_vel.P != NO_CMD ) {
      float fPanAngleVel        = fabs( angle_vel.P ) * fPTSpdFactor;
      BOOL bGetHWSpeedFromTable = FALSE;
      if( m_PanAngleVel2HWSpdMapInfo.m_pTbl ) {
         hw_vel.P = _GetBValue( m_PanAngleVel2HWSpdMapInfo, fPanAngleVel );
         // 매우 작은 각속도로 인하여 하드웨어 속도가 0으로 된 경우에는
         // 해당 PTZ카메라의 최소 속도에 해당하는 하드웨어 속도값을 얻는다.
         if( int( hw_vel.P ) == 0 && fPanAngleVel > 0.0001f ) {
            hw_vel.P = m_PanAngleVel2HWSpdMapInfo.m_pTbl[1].B;
         }
         bGetHWSpeedFromTable = TRUE;
      } else if( m_PanAngleVelocityTable && m_AngleSpeedCheckZoomTable && m_nAngleSpeedCheckZoomTableItemNum > 0 ) {
         hw_vel.P             = _GetHWSpeed( m_PanAngleVelocityTable, MaxPanSpeed, m_AngleSpeedCheckZoomTable, m_nAngleSpeedCheckZoomTableItemNum, fabs( angle_vel.P ), fCurZoom );
         bGetHWSpeedFromTable = TRUE;
      }
      if( bGetHWSpeedFromTable ) {
         if( angle_vel.P < 0.0f )
            hw_vel.P *= -1.0f;
      } else {
         hw_vel.P = angle_vel.P;
      }
   }

   if( angle_vel.T != NO_CMD ) {
      float fTiltAngleVel       = fabs( angle_vel.T ) * fPTSpdFactor;
      BOOL bGetHWSpeedFromTable = FALSE;
      if( m_TiltAngleVel2HWSpdMapInfo.m_pTbl ) {
         hw_vel.T = _GetBValue( m_TiltAngleVel2HWSpdMapInfo, fTiltAngleVel );
         // 매우 작은 각속도로 인하여 하드웨어 속도가 0으로 된 경우에는
         // 해당 PTZ카메라의 최소 속도에 해당하는 하드웨어 속도값을 얻는다.
         if( int( hw_vel.T ) == 0 && fTiltAngleVel > 0.0001f ) {
            hw_vel.T = m_TiltAngleVel2HWSpdMapInfo.m_pTbl[1].B;
         }
         bGetHWSpeedFromTable = TRUE;
      } else if( m_TiltAngleVelocityTable && m_AngleSpeedCheckZoomTable && m_nAngleSpeedCheckZoomTableItemNum > 0 ) {
         hw_vel.T             = _GetHWSpeed( m_TiltAngleVelocityTable, MaxTiltSpeed, m_AngleSpeedCheckZoomTable, m_nAngleSpeedCheckZoomTableItemNum, fabs( angle_vel.T ), fCurZoom );
         bGetHWSpeedFromTable = TRUE;
      }
      if( bGetHWSpeedFromTable ) {
         if( angle_vel.T < 0.0f )
            hw_vel.T *= -1.0f;
      } else {
         hw_vel.T = angle_vel.T;
      }
   }
   //logd ("(%5.1f, %5.1f)  (%f,%f)\n", angle_vel.P, angle_vel.T, hw_vel.P, hw_vel.T);
}

BOOL CPTZCtrl::IsSendingContPTZFIMoveCmd()
{
   BOOL bStopPTZFIStop = FALSE;
   if( m_CurrPTZMoveHW.IsContPTZFIMoveStopped() ) {
      m_nContPTZMoveStopCmdCount++;
      bStopPTZFIStop = TRUE;
   } else {
      m_nContPTZMoveStopCmdCount = 0;
   }
   BOOL bSendContPTZMoveCmd = TRUE;
   if( bStopPTZFIStop && m_nContPTZMoveStopCmdCount > 2 )
      bSendContPTZMoveCmd = FALSE;
   return bSendContPTZMoveCmd;
}

int CPTZCtrl::SendCommandByOuterPTZControl()
{
   memcpy( m_pMainPTZControl->m_sCmd, m_sCmd, sizeof( m_sCmd ) );
   m_pMainPTZControl->m_nSendCmdLen = m_nSendCmdLen;
   return m_pMainPTZControl->SendCommand();
}

int CPTZCtrl::SetMapTable( int map_table_type, MapF2F* map_table, int tbl_size, int itp_mode )
{
   if( map_table_type == MapF2FType_ZoomReal2HW ) m_ZoomReal2HWMapInfo.Set( itp_mode, tbl_size, map_table );
   if( map_table_type == MapF2FType_Zoom2FL ) m_Zoom2FLMapInfo.Set( itp_mode, tbl_size, map_table );
   if( map_table_type == MapF2FType_PanAngleVel2HWSpd ) m_PanAngleVel2HWSpdMapInfo.Set( itp_mode, tbl_size, map_table );
   if( map_table_type == MapF2FType_TiltAngleVel2HWSpd ) m_TiltAngleVel2HWSpdMapInfo.Set( itp_mode, tbl_size, map_table );
   if( map_table_type == MapF2FType_ZoomLogVel2HWSpd ) m_ZoomLogVel2HWSpdMapInfo.Set( itp_mode, tbl_size, map_table );
   if( map_table_type == MapF2FType_GotoAbsVel2HWSpd ) m_GotoAbsVel2HWSpdMapInfo.Set( itp_mode, tbl_size, map_table );
   if( map_table_type == MapF2FType_Zoom2PTSpdRatio ) m_Zoom2PTSpdRatioMapInfo.Set( itp_mode, tbl_size, map_table );
   return ( DONE );
}

int CPTZCtrl::OffsetPositionByPTZ()
{
   m_TgtPTZPos += m_OffsetPos;
   m_OffsetPos( 0.0f, 0.0f, 0.0f, 0.0f );
   if( m_TgtPTZPos.Z > MaxZoomFactor )
      m_TgtPTZPos.Z = MaxZoomFactor;
   if( m_TgtPTZPos.Z < MinZoomFactor )
      m_TgtPTZPos.Z = MinZoomFactor;
   if( m_TgtPTZPos.F > MaxFocus )
      m_TgtPTZPos.F = MaxFocus;
   if( m_TgtPTZPos.F < MinFocus )
      m_TgtPTZPos.F = MinFocus;
   return ( DONE );
}

int CPTZCtrl::GetPTZControlTypeOfPTZCmd()
{
   if( m_pSubPTZControl )
      return m_pSubPTZControl->m_nPTZControlType;
   else
      return m_nPTZControlType;
}

void CPTZCtrl::GetMaxOffsetAngle( float& pa, float& ta )
{
   float zoom          = GetAbsPos().Z;
   FPTZVector pvCurPos = m_CurPTZPos;
   FPTZVector pvTgtPos = m_CurPTZPos;
   if( m_nState & ( PTZ_State_Zooming | PTZ_State_ZoomToBeStarted ) )
      zoom = m_fZoomEstimate;
   if( zoom < 1.0f )
      zoom = 1.0f;
   else if( zoom > MaxZoomFactor )
      zoom = MaxZoomFactor;
   FPoint2D pd = FPoint2D( m_s2VideoSize.Width * 0.5f, -m_s2VideoSize.Height * 0.5f );
   Matrix vec_x1( 3 );
   float fFL   = GetFocalLength( zoom );
   vec_x1( 0 ) = pd.X;
   vec_x1( 1 ) = pd.Y * m_fFocalLengthRatio;
   vec_x1( 2 ) = fFL;
   vec_x1      = Nor( vec_x1 );

   int i;
   for( i = 0; i < 2; i++ ) {
      vec_x1( 0 ) = pd.X;
      vec_x1( 1 ) = pd.Y * m_fFocalLengthRatio;
      vec_x1( 2 ) = fFL;
      if( 0 == i ) vec_x1( 1 ) = 0.0f;
      if( 1 == i ) vec_x1( 0 ) = 0.0f;
      vec_x1 = Nor( vec_x1 );
      Matrix mat_Rx( 3, 3 ), mat_Ry( 3, 3 );
      VACL::GetRotationMatrix_XYZ( MC_DEG2RAD * pvCurPos.T, 0.0f, 0.0f, mat_Rx );
      VACL::GetRotationMatrix_XYZ( 0.0f, MC_DEG2RAD * pvCurPos.P, 0.0f, mat_Ry );
      Matrix vec_x2 = mat_Ry * mat_Rx * vec_x1;
      pvTgtPos.P    = float( -MC_RAD2DEG * atan2( vec_x2( 0 ), vec_x2( 2 ) ) );
      pvTgtPos.T    = float( MC_RAD2DEG * asin( vec_x2( 1 ) ) );
      if( 0 == i ) pa = GetAngleDiff( pvCurPos.P, pvTgtPos.P );
      if( 1 == i ) ta = GetAngleDiff( pvCurPos.T, pvTgtPos.T );
   }
   //logd ("(%f,%f)\n",  pa, ta);
}

void CPTZCtrl::GetOffsetAngle( int nSrcWidth, int nSrcHeight, float x, float y, float& pa, float& ta )
{
   float zoom = GetAbsPos().Z;
   if( m_nState & ( PTZ_State_Zooming | PTZ_State_ZoomToBeStarted ) )
      zoom = m_fZoomEstimate;
   if( zoom < 1.0f )
      zoom = 1.0f;
   else if( zoom > MaxZoomFactor )
      zoom = MaxZoomFactor;
   FPoint2D scale( m_s2RefImgSize_FL.Width / nSrcWidth, m_s2RefImgSize_FL.Height / nSrcHeight );
   FPoint2D pd = FPoint2D( x, y );
   pd.X *= scale.X;
   pd.Y *= scale.Y;
   Matrix vec_x1( 3 );
   float fFL   = m_s2RefImgSize_FL.Width * zoom;
   vec_x1( 0 ) = pd.X;
   vec_x1( 1 ) = pd.Y * m_fFocalLengthRatio;
   vec_x1( 2 ) = fFL;
   vec_x1      = Nor( vec_x1 );
   pa          = float( MC_RAD2DEG * atan2( vec_x1( 0 ), vec_x1( 2 ) ) );
   ta          = float( MC_RAD2DEG * asin( vec_x1( 1 ) ) );
}

int CPTZCtrl::GetPTAngle2ImgPos( float pa, float ta, float& x, float& y )
{
   float zoom = GetAbsPos().Z;
   if( m_nState & ( PTZ_State_Zooming | PTZ_State_ZoomToBeStarted ) )
      zoom = m_fZoomEstimate;
   if( zoom < 1.0f )
      zoom = 1.0f;
   else if( zoom > MaxZoomFactor )
      zoom = MaxZoomFactor;
   float fFL           = GetFocalLength( zoom );
   FPTZVector pvCurPos = GetAbsPos();
   float fPanOffset    = pa - pvCurPos.P;
   float fTiltOffset   = ta - pvCurPos.T;
   float fTan_x1_x3    = tan( fPanOffset * MC_DEG2RAD );
   float fSin_x2_x3    = sin( fTiltOffset * MC_DEG2RAD );
   x                   = fTan_x1_x3 * fFL;
   y                   = fSin_x2_x3 * fFL * m_fFocalLengthRatio;
   FPoint2D scale( m_s2RefImgSize_FL.Width / m_s2VideoSize.Width, m_s2RefImgSize_FL.Height / m_s2VideoSize.Height );
   x /= scale.X;
   y /= scale.Y;
   return ( DONE );
}

int CPTZCtrl::GetPTZPos( FPoint2D p2Pos, ISize2D s2Src, FPTZVector& pvTgtPos )
{
   return GetPTZPos( p2Pos, GetAbsPos(), s2Src, pvTgtPos );
}

int CPTZCtrl::GetPTZPos( FPoint2D p2Pos, FPTZVector pvCurPos, ISize2D s2Src, FPTZVector& pvTgtPos )
{
   float flw = float( m_s2RefImgSize_FL.Width );
   float flh = float( m_s2RefImgSize_FL.Height );
   float sw  = float( s2Src.Width );
   float sh  = float( s2Src.Height );
   // 초점거리 참조 영상의 좌표계로 한다.
   float ox = p2Pos.X / sw * flw;
   float oy = p2Pos.Y / sh * flh;
   float fl = m_fFocalLength * GetCurrZoom();
   Matrix vec_x1( 3 );
   vec_x1( 0 ) = vec_x1( 1 ) = 0.0;
   vec_x1( 2 )               = 1.0;
   // 원점으로 부터 영상포인트로 향하는 벡터를 구한다.
   vec_x1( 0 ) = ox;
   vec_x1( 1 ) = oy * m_fFocalLengthRatio;
   vec_x1( 2 ) = fl;
   vec_x1      = Nor( vec_x1 );
   // 벡터를 현재 Pan, Tilt에 대해 회전시킨다.
   Matrix mat_Rx( 3, 3 ), mat_Ry( 3, 3 );
   VACL::GetRotationMatrix_XYZ( MC_DEG2RAD * pvCurPos.T, 0.0f, 0.0f, mat_Rx );
   VACL::GetRotationMatrix_XYZ( 0.0f, MC_DEG2RAD * pvCurPos.P, 0.0f, mat_Ry );
   Matrix vec_x2 = mat_Ry * mat_Rx * vec_x1;
   // 회전된 벡터로 부터 Pan, Tilt 위치를 계산한다.
   pvTgtPos.P = float( -MC_RAD2DEG * atan2( vec_x2( 0 ), vec_x2( 2 ) ) );
   pvTgtPos.T = float( MC_RAD2DEG * asin( vec_x2( 1 ) ) );
   pvTgtPos.Z = pvCurPos.Z;
   return ( DONE );
}

int CPTZCtrl::GetImagePnt( FPTZVector pvPos, ISize2D s2Src, FPoint2D& p2Pos )
{
   float flw           = float( m_s2RefImgSize_FL.Width );
   float flh           = float( m_s2RefImgSize_FL.Height );
   float fl            = m_fFocalLength * GetCurrZoom();
   FPTZVector pvCurPos = GetAbsPos();
   Matrix vec_x1( 3 );
   vec_x1( 0 ) = vec_x1( 1 ) = 0.0;
   vec_x1( 2 )               = 1.0f;
   Matrix mat_Rx( 3, 3 ), mat_Ry( 3, 3 );
   VACL::GetRotationMatrix_XYZ( MC_DEG2RAD * pvPos.T, 0.0f, 0.0f, mat_Rx );
   VACL::GetRotationMatrix_XYZ( 0.0f, MC_DEG2RAD * pvPos.P, 0.0f, mat_Ry );
   Matrix vec_x2 = mat_Ry * mat_Rx * vec_x1;
   // vec_x2 의 크기가 1.0f인지 검증할 필요가 있다.
   VACL::GetRotationMatrix_XYZ( -MC_DEG2RAD * pvCurPos.T, 0.0f, 0.0f, mat_Rx );
   VACL::GetRotationMatrix_XYZ( 0.0f, -MC_DEG2RAD * pvCurPos.P, 0.0f, mat_Ry );
   Matrix vec_x3 = mat_Rx * mat_Ry * vec_x2;
   float ox, oy;
   ox = oy = MC_VLV;
   if( vec_x3( 2 ) > 0.0f ) {
      ox = float( vec_x3( 0 ) / vec_x3( 2 ) * fl );
      oy = float( vec_x3( 1 ) / vec_x3( 2 ) * fl );
   }
   FPoint2D scale( s2Src.Width / flw, s2Src.Height / flh );
   p2Pos.X = ox * scale.X;
   p2Pos.Y = oy * scale.Y;
   return ( DONE );
}

int CPTZCtrl::OffsetPositionByBox()
{
   if( IsGetAbsPosSupported() && !m_bGetAbsPosError )
      m_TgtPTZPos = m_CurPTZPos;
   float zoom = m_TgtPTZPos.Z;
   if( ZoomModuleType_ThermalImaging == m_nCurZoomModuleType ) {
      zoom = m_CurPTZPos_ThermalImaging.Z;
   }
   if( m_nState & ( PTZ_State_Zooming | PTZ_State_ZoomToBeStarted ) )
      zoom = m_fZoomEstimate;
   if( zoom < 1.0f )
      zoom = 1.0f;
   else if( zoom > MaxZoomFactor )
      zoom = MaxZoomFactor;
   float hvw = 0.5f;
   float hvh = 0.5f;
   FBox2D targetBox;
   targetBox.X      = m_b2TargetBox.X * hvw;
   targetBox.Y      = m_b2TargetBox.Y * hvh;
   targetBox.Width  = m_b2TargetBox.Width * hvw;
   targetBox.Height = m_b2TargetBox.Height * hvh;
   float cx         = targetBox.X + targetBox.Width / 2.0f;
   float cy         = targetBox.Y + targetBox.Height / 2.0f;
   // Focal Length를 측정하였던 영상크기를 기준으로 좌표를 변환한다.
   FPoint2D scale( m_s2RefImgSize_FL.Width, m_s2RefImgSize_FL.Height );
   FPoint2D pd = FPoint2D( float( cx ), float( cy ) );
   pd.X *= scale.X;
   pd.Y *= scale.Y;
   float fFL = m_fFocalLength * zoom;
   Matrix vec_x1( 3 );
   vec_x1( 0 ) = pd.X;
   vec_x1( 1 ) = pd.Y * m_fFocalLengthRatio;
   vec_x1( 2 ) = fFL;
   vec_x1      = Nor( vec_x1 );
   Matrix mat_Rx( 3, 3 ), mat_Ry( 3, 3 );
   VACL::GetRotationMatrix_XYZ( MC_DEG2RAD * m_TgtPTZPos.T, 0.0f, 0.0f, mat_Rx );
   VACL::GetRotationMatrix_XYZ( 0.0f, MC_DEG2RAD * m_TgtPTZPos.P, 0.0f, mat_Ry );
   Matrix vec_x2 = mat_Ry * mat_Rx * vec_x1;
   m_TgtPTZPos.P = float( -MC_RAD2DEG * atan2( vec_x2( 0 ), vec_x2( 2 ) ) );
   m_TgtPTZPos.T = float( MC_RAD2DEG * asin( vec_x2( 1 ) ) );
   // 줌 이동량을 계산한다.
   if( targetBox.Width > 0.01f || targetBox.Height > 0.01f ) {
      float zoom_ratio = 1.0f;
      if( targetBox.Width > m_s2VideoSize.Width ) targetBox.Width = float( m_s2VideoSize.Width );
      if( targetBox.Height > m_s2VideoSize.Height ) targetBox.Height = float( m_s2VideoSize.Height );
      float img_ratio = float( m_s2VideoSize.Height ) / m_s2VideoSize.Width;
      float box_ratio = float( targetBox.Height ) / m_b2TargetBox.Width;
      if( box_ratio < img_ratio )
         zoom_ratio = m_s2VideoSize.Width / targetBox.Width;
      else
         zoom_ratio = m_s2VideoSize.Height / targetBox.Height;
      zoom *= zoom_ratio;
   }
   if( zoom < 1.0f )
      zoom = 1.0f;
   else if( zoom > MaxZoomFactor )
      zoom = MaxZoomFactor;
   if( !( m_nState & ( PTZ_State_Zooming | PTZ_State_ZoomToBeStarted ) ) )
      m_TgtPTZPos.Z = zoom;
   m_TgtPTZPos.F = m_CurPTZPos.F;

   m_TgtPTZPos.ZT = m_nCurZoomModuleType;

   return ( DONE );
}

int CPTZCtrl::CopyZoomModuleFactors( int nZoomModuleType, CPTZCtrl* pDestPTZCtrl )
{
   if( m_pSubPTZControl )
      return m_pSubPTZControl->CopyZoomModuleFactors( nZoomModuleType, pDestPTZCtrl );
   return ( -1 );
}

ISize2D CPTZCtrl::GetVideoSize()
{
   return m_s2VideoSize;
}

void CPTZCtrl::SetVideoSize( ISize2D video_size )
{
   m_s2VideoSize = video_size;
   if( m_pSubPTZControl ) m_pSubPTZControl->m_s2VideoSize = video_size;
}

void CPTZCtrl::SetPTZ2D( CPTZ2D* ptz2d )
{
   m_pPTZ2D = ptz2d;
}

void CPTZCtrl::WaitingInMilliSec( uint nMilliSec )
{
   uint32_t nStartTime = GetTickCount();
   while( 1 ) {
      if( m_nState & PTZ_State_ToBeStopThread ) break;
      if( GetTickCount() - nStartTime > nMilliSec ) break;
      Sleep_ms( m_nSendCmdPeriod );
   }
}

void CPTZCtrl::PTZPosEstProc()
{
   if( m_nState & PTZ_State_ZoomToBeStarted ) {
      // "줌 측정 시작" 상태인데 "예상 줌 이동 완료시간을 넘어서면 줌 측정을 하지 않는다.
      if( GetTickCount() > m_nZoomPosChangedTime + m_nPTZCmdExecDelay + 500 ) {
         m_nState &= ~( PTZ_State_ZoomToBeStarted | PTZ_State_Zooming );
         if( g_bDebugZoomEstimate )
            logd( "PTZ_State_ZoomToBeStarted  Ended\n" );
      }
   }
   ///////////////////////////////////////////////////////////
   // 줌 측정 시작 대기 상태 전환
   {
      BOOL bEstimate = FALSE;
      if( !IsRealTimeGetAbsPosSupported() )
         bEstimate = TRUE;
      if( bEstimate ) {
         // 연속 이동
         if( PTZCmdType_ContinuousMove == m_nCurrCmdType ) {
            // 줌이동을 막 시작한 상태
            if( 0.0f == m_PrevPTZMoveHWForPosEst.Z && 0.0f != m_CurrPTZMoveHW.Z ) {
               m_nState |= PTZ_State_ZoomToBeStarted;
               m_nZoomPosChangedTime = GetTickCount();
               m_fZoom_Start         = GetAbsPos().Z;
               m_fZoomEstimate       = m_fZoom_Start;
               m_fZoomMoveStartDir   = m_CurrPTZMoveHW.Z;
               if( g_bDebugZoomEstimate )
                  logd( "Cont: PTZ_State_ZoomToBeStarted\n" );
            }
         }
         // 절대각 이동
         else if( PTZCmdType_GotoAbsPos == m_nCurrCmdType ) {
            if( ( m_TgtPTZPos.Z != m_PrevPTZPosZE.Z ) || ( PTZCmdType_ContinuousMove == m_nPrevCmdType ) ) {
               m_nState |= PTZ_State_ZoomToBeStarted;
               m_nZoomPosChangedTime = GetTickCount();
               if( m_nState & PTZ_State_FirstGotoAbsPos ) {
                  m_nState &= ~PTZ_State_FirstGotoAbsPos;
                  m_fZoom_Start = GetAbsPos().Z;
               } else {
                  m_fZoom_Start = m_PrevPTZPosZE.Z;
               }
               m_fZoomEstimate            = m_fZoom_Start;
               m_nZoomMovePeriod_Estimate = 0;
               if( m_PrevPTZPosZE.Z != NO_CMD && m_TgtPTZPos.Z != NO_CMD )
                  m_nZoomMovePeriod_Estimate = GetZoomMoveTime( m_PrevPTZPosZE.Z, m_TgtPTZPos.Z );
               m_PrevPTZPosZE = m_TgtPTZPos;
               if( g_bDebugZoomEstimate ) {
                  logd( "Abs: PTZ_State_ZoomToBeStarted\n" );
                  logd( "     m_nZoomMovePeriod_Estimate = %d\n", m_nZoomMovePeriod_Estimate );
               }
            }
         }
      }
   }
   ///////////////////////////////////////////////////////////
   // "줌 측정 시작" -->  "줌측정 중" 상태 전이
   if( m_nState & PTZ_State_ZoomToBeStarted ) {
      // PTZ 명령 전송 지연시간을 고려한다.
      // 명령이 기계에 전달되어 PTZ가 움직이면 상태를 전이한다.
      if( GetTickCount() > m_nZoomPosChangedTime + m_nPTZCmdExecDelay ) {
         m_nState &= ~( PTZ_State_ZoomToBeStarted );
         m_nState |= PTZ_State_Zooming;
         uint32_t nCurTime = GetTickCount();
         m_nZoomStartTime  = m_nZoomPosChangedTime + m_nPTZCmdExecDelay;
         if( g_bDebugZoomEstimate ) {
            logd( "    PTZ_State_Zooming  Started >>>>>>>>>>\n" );
         }
      }
   }
   ///////////////////////////////////////////////////////////
   // 현재 줌 이동을 하고 있는 상태
   if( m_nState & PTZ_State_Zooming ) {
      float fDeltaTime = 0.0f;
      if( PTZCmdType_GotoAbsPos == m_nCurrCmdType ) {
         // 줌 이동을 하고 있는 중이면 현재 줌 값을 계산한다.
         int zoom_elapled_time = GetTickCount() - m_nZoomStartTime;
         float ztr             = 0.0f;
         if( m_nZoomMovePeriod_Estimate )
            ztr = float( zoom_elapled_time ) / float( m_nZoomMovePeriod_Estimate );
         float zls       = log( m_fZoom_Start ) * MC_LOG2_INV;
         float zlt       = log( m_TgtPTZPos.Z ) * MC_LOG2_INV;
         m_fZoomEstimate = pow( 2.0f, zls + ( zlt - zls ) * ztr );
      } else if( PTZCmdType_ContinuousMove == m_nCurrCmdType ) {
         fDeltaTime      = float( GetTickCount() - m_nZoomPosChangedTime ) / 1000.0f;
         float zls       = log( m_fZoom_Start ) * MC_LOG2_INV;
         m_fZoomEstimate = pow( 2.0f, zls + GetZoomLogSpeed( int( m_fZoomMoveStartDir ) ) * fDeltaTime );
      }
      if( m_fZoomEstimate < MinZoomFactor ) m_fZoomEstimate = MinZoomFactor;
      if( m_fZoomEstimate > MaxZoomFactor ) m_fZoomEstimate = MaxZoomFactor;

      if( g_bDebugZoomEstimate )
         logd( "Zooming: ZS=%6.3f  ZT=%6.3f  ZE=%6.3f fDeltaTime:%f\n", m_fZoom_Start, m_TgtPTZPos.Z, m_fZoomEstimate, fDeltaTime );
   }
   m_fZoomEstimate = GetCurrZoom();
   ///////////////////////////////////////////////////////////
   // 줌 이동 멈춤 대기 상태로 전이
   {
      if( PTZCmdType_GotoAbsPos == m_nCurrCmdType ) {
         // 현재 줌 이동을 하고 있는 상태이고 줌 이동 예상 시간을 벗어났으면 줌 이동상태 해제
         if( ( m_nState & PTZ_State_Zooming ) ) {
            if( !( m_nState & PTZ_State_ZoomToBeStopped ) ) {
               if( GetTickCount() > ( m_nZoomStartTime + m_nZoomMovePeriod_Estimate ) ) {
                  m_nState |= ( PTZ_State_ZoomToBeStopped );
                  m_nZoomStopTime = GetTickCount();
                  if( g_bDebugZoomEstimate )
                     logd( "Abs  : PTZ_State_Zooming  To Be Ended\n" );
               }
            }
         }
      } else if( PTZCmdType_ContinuousMove == m_nCurrCmdType ) {
         // 줌이동을 막 종료한 상태
         if( !( m_nState & PTZ_State_ZoomToBeStopped ) ) {
            if( 0.0f != m_PrevPTZMoveHWForPosEst.Z && 0.0f == m_CurrPTZMoveHW.Z ) {
               m_nState |= ( PTZ_State_ZoomToBeStopped );
               m_nZoomStopTime = GetTickCount();
               if( g_bDebugZoomEstimate )
                  logd( "Cont: PTZ_State_Zooming  To Be Ended (%f, %f)\n", m_PrevPTZMoveHWForPosEst.Z, m_CurrPTZMoveHW.Z );
            }
         }
      }
      // 절대각요청이 가능한 카메라인데 이제 막 멈추었다면 절대각 수신요청을 한다.
      if( IsGetAbsPosSupported() && !m_PrevPTZMoveHWForPosEst.IsContPTZFIMoveStopped() && m_CurrPTZMoveHW.IsContPTZFIMoveStopped() ) {
         m_nState |= PTZ_State_GetAbsPosToBeRequested;
      }
      m_PrevPTZMoveHWForPosEst = m_CurrPTZMoveHW;
   }
   ///////////////////////////////////////////////////////////
   // 줌이동 멈춤
   if( m_nState & PTZ_State_ZoomToBeStopped ) {
      if( GetTickCount() > ( m_nZoomStopTime + m_nPTZCmdExecDelay ) ) {
         m_nState &= ~( PTZ_State_ZoomToBeStopped | PTZ_State_Zooming );
         if( PTZCmdType_GotoAbsPos == m_nCurrCmdType ) {
            SetAbsPos( m_TgtPTZPos );
         } else if( PTZCmdType_ContinuousMove == m_nCurrCmdType ) {
            //logd ("m_fZoomEstimate            : %f\n", m_fZoomEstimate);
            SetAbsPos( FPTZVector( PREV_CMD, PREV_CMD, m_fZoomEstimate, PREV_CMD, PREV_CMD ) );
            if( ( m_nState & PTZ_State_ContPTZTracking ) && IsRealTimeGetAbsPosMode() /*&& !m_bGetAbsPosError*/ )
               m_nState |= PTZ_State_GetAbsPosToBeRequested;
         }
         if( g_bDebugZoomEstimate )
            logd( "----: PTZ_State_Zooming  Ended\n" );
      }
   }
}

int CPTZCtrl::AddToRecvCmd( BYTE* pBuff, int nLen )
{
   int i, j;
   int nRemain = g_nMaxCmdLen - m_nRecvCmdCnt;
   if( nRemain >= nLen ) {
      for( i = 0; i < nLen; i++ ) {
         j = m_nRecvCmdCnt + i;
         ASSERT( i < nLen );
         ASSERT( ( 0 <= j ) && ( j < g_nMaxCmdLen ) );
         m_ResCmd[j] = pBuff[i];
      }
      m_nRecvCmdCnt += nLen;
   } else {
      int nDel   = nLen - nRemain;
      int nShift = g_nMaxCmdLen - nDel;
      for( i = 0; i < nShift; i++ ) {
         j = nDel + i;
         ASSERT( i < g_nMaxCmdLen );
         ASSERT( ( 0 <= j ) && ( j < g_nMaxCmdLen ) );
         m_ResCmd[i] = m_ResCmd[j];
      }
      for( i = g_nMaxCmdLen - nLen, j = 0; i < g_nMaxCmdLen; i++, j++ ) {
         ASSERT( ( 0 <= i ) && ( i < g_nMaxCmdLen ) );
         ASSERT( j < nLen );
         m_ResCmd[i] = pBuff[j];
      }
   }
   return ( DONE );
}

int CPTZCtrl::ParseRecvCmd( BOOL bShiftToHead )
{
   // PTZ 수신버퍼에 채워진 데이터를 파싱하는 루프이다.

   int i;
   int nStart;
   int nParseCnt = 0;
   while( 1 ) {
      // 헤더를 찾는다.
      nStart             = -1;
      int nFindHeaderCnt = 0;
      for( i = 0; i < m_nRecvCmdCnt; i++ ) {
         if( m_byRecvHeader == m_ResCmd[i] ) {
            nStart = i;
            nFindHeaderCnt++;
            if( nFindHeaderCnt >= 1 )
               break;
         }
      }
      if( m_byRecvHeader2 > 0 && nStart == -1 ) {
         for( i = 0; i < m_nRecvCmdCnt; i++ ) {
            if( m_byRecvHeader2 == m_ResCmd[i] ) {
               nStart = i;
               nFindHeaderCnt++;
               if( nFindHeaderCnt >= 1 )
                  break;
            }
         }
      }
      // 헤더가 버퍼의 처음 위치에 오도록 복사한다.
      if( nStart > 0 ) {
         m_nRecvCmdCnt -= nStart;
         for( i = 0; i < m_nRecvCmdCnt; i++ ) {
            m_ResCmd[i]          = m_ResCmd[nStart + i];
            m_ResCmd[nStart + i] = 0;
         }
      }

      // 헤더를 찾지 못하였으면 버퍼를 비운다.
      if( -1 == nStart ) {
         m_nRecvCmdCnt = 0;
         ZeroMemory( m_ResCmd, sizeof( m_ResCmd ) );
      }

      int nPopCmdLen = _PopCommand();
      // 적어도 1개의 패킷이 포함되어있어야만할 길이(20)이고 가장 첫바이트가 헤더인 경우
      // 다음 패킷이 처리될 수 있도록 헤더바이트를 Pop 시킨다.
      // 패킷 수신에 손실이 발생하는 경우 데이터가 헤더로 인식될 수 있다.
      if( nPopCmdLen == 0 && m_nRecvCmdCnt > 40 && m_byRecvHeader == m_ResCmd[0] )
         nPopCmdLen = 1;
      if( nPopCmdLen == 0 )
         break;
      if( nPopCmdLen > 0 ) {
         // 명령이 처리되었으면 처리된 명령 만큼 앞으로 당긴다.
         if( m_nRecvCmdCnt >= nPopCmdLen ) {
            m_nRecvCmdCnt -= nPopCmdLen;
            for( i = 0; i < m_nRecvCmdCnt; i++ ) {
               m_ResCmd[i]              = m_ResCmd[nPopCmdLen + i];
               m_ResCmd[nPopCmdLen + i] = 0;
            }
         }
         // 메인 PTZ의 절대각 위치를 업데이트한다.
         if( m_pMainPTZControl && nPopCmdLen > 1 ) {
            m_pMainPTZControl->m_CurPTZPos = m_CurPTZPos;
         }
         // 7바이트 이상 처리되었다면 절대위치가 수신된 것으로 간주한다.
         if( nPopCmdLen >= 7 ) {
            m_nGetAbsPosTime = GetTickCount();
            if( m_pMainPTZControl )
               m_pMainPTZControl->m_nGetAbsPosTime = GetTickCount();
         }
      }
      nParseCnt++;
      if( nParseCnt > 16 )
         break;
   }
   return ( DONE );
}

void CPTZCtrl::AddPTZCtrlLogItem()
{
   PTZCtrlLogItem item;
   item.m_pvContMove  = m_CurrPTZMoveHW;
   item.m_pvTgtAbsPos = m_TgtPTZPos;
   item.m_pvCurAbsPos = GetAbsPos();
   m_pPTZCtrlLogItemQueue->AddItem( item );
}

void CPTZCtrl::IRLightCtrlProc()
{
   if( !IsPTZConnected() )
      return;

   if( !( m_nCapabilities & PTZCtrlCap_IRLigtControlSupported ) )
      return;

   if( m_PTZInfo.m_bIRControlByIntelliVIX ) { // IntelliVIX가 IR을 제어에 체크가 되어 있으면 수행 됨 (mkjang-141211)
      if( g_nLastTime_IRCtrlByKeybord && ( GetTickCount() - g_nLastTime_IRCtrlByKeybord > 300000 ) ) {
         g_bIRCtrlByKeyboard = FALSE;
      }
      if( g_bIRLightCtrl && !g_bIRCtrlByKeyboard ) { // IR 제어
         if( !m_bUseIRLightCtrlInfo && !m_PTZInfo.m_bUseCameraDayAndNigntMode ) { // (mkjang-141211)
            _SetIRMode( m_nCustomIRLightCtrlMode );
         } else {
            CIRLightCtrlInfo* pIRCtrlInfo = &m_PTZInfo.m_IRLightCtrlInfo;
            if( m_PTZInfo.m_bUseOuterIRLightCtrlInfo && m_pOuterIRLightCtrlInfo )
               pIRCtrlInfo = m_pOuterIRLightCtrlInfo;

            int nIRCtrlMode = pIRCtrlInfo->m_nIRMode;

            if( m_PTZInfo.m_bUseCameraDayAndNigntMode ) {
               // 카메라의 Day/Night 설정 사용 체크 -> 카메라 제어 도킹 창의 IR 버튼을 off 했을 시
               if( nIRCtrlMode == IRMode_DayTime_DayMode_NightTime_NightMode_IROff ) {
                  _SetIRLight( 0 );
               }
               // IR 버튼 on 상태에서는 Tilt각/Zoom배율 별 설정된 IR 조명 값으로 IR 설정
            }

            else { // 카메라의 Day/Night 설정 사용 체크 해제 시 수행 됨 (mkjang-141211)
               // PTZ가 IR을 자동으로 제어하는 모드
               if( nIRCtrlMode == IRMode_PTZAuto ) {
                  _SetIRMode( PTZIRCtrlMode_Auto );
               }
               // 항상 Day Mode
               else if( nIRCtrlMode == IRMode_Always_DayMode ) {
                  _SetIRMode( PTZIRCtrlMode_DayMode );
               }
               // 항상 Night Mode & IR Off
               else if( nIRCtrlMode == IRMode_Always_NightMode_IROff ) {
                  _SetIRMode( PTZIRCtrlMode_NightModeAndIROff );
               }
               // 항상 Night Mode & IR On
               else if( nIRCtrlMode == IRMode_Always_NightMode_IROn ) {
                  _SetIRMode( PTZIRCtrlMode_NightModeAndIROn );
               } else if( nIRCtrlMode == IRMode_DayTime_DayMode_NightTime_NightMode_IROff || nIRCtrlMode == IRMode_DayTime_DayMode_NightTime_NightMode_IROn ) {

                  // DayNightDetector를 통해 낮/밤의 변화를 측정
                  if( m_nState & PTZ_State_SignalNightModeOn ) {
                     m_nState &= ~PTZ_State_SignalNightModeOn;
                     m_nState |= PTZ_State_NightMode;
                  }
                  if( m_nState & PTZ_State_SignalNightModeOff ) {
                     m_nState &= ~PTZ_State_SignalNightModeOff;
                     m_nState &= ~PTZ_State_NightMode;
                  }

                  if( m_nState & PTZ_State_NightMode ) {
                     // 낮에는 Day Mode 밤에는 Night Mode & IR Off --> IR Off 모드
                     if( nIRCtrlMode == IRMode_DayTime_DayMode_NightTime_NightMode_IROff )
                        _SetIRMode( PTZIRCtrlMode_NightModeAndIROff );
                     // 낮에는 Day Mode 밤에는 Night Mode & IR On  --> IR Auto 모드
                     else if( nIRCtrlMode == IRMode_DayTime_DayMode_NightTime_NightMode_IROn )
                        _SetIRMode( PTZIRCtrlMode_NightModeAndIROn );
                  } else {
                     // 항상 Day Mode
                     _SetIRMode( PTZIRCtrlMode_DayMode );
                  }
               }
            }

            BOOL bIRLightCtrl = FALSE;
            if( nIRCtrlMode == IRMode_DayTime_DayMode_NightTime_NightMode_IROn ) bIRLightCtrl = TRUE;
            if( nIRCtrlMode == IRMode_Always_NightMode_IROn ) bIRLightCtrl = TRUE;

            // 현재의 Tilt각으로 부터 IR 밝기 값을 얻고 IR조명 값을 설정한다.
            if( bIRLightCtrl ) {
               // Tilt각이나 Zoom배율에 따라 IR 밝기 값을 구함 (mkjang-140612)
               if( pIRCtrlInfo->m_nIRMappingMode == IRMappingMode_TiltAngle )
                  m_nCurIRLightValue = pIRCtrlInfo->GetCurTiltIRLightValue( GetAbsPos().T );
               else if( pIRCtrlInfo->m_nIRMappingMode == IRMappingMode_Zoom )
                  m_nCurIRLightValue = pIRCtrlInfo->GetCurZoomIRLightValue( GetAbsPos().Z );

               if( m_nCurIRLightValueOnIRArea >= 0 )
                  m_nCurIRLightValue = m_nCurIRLightValueOnIRArea;
               BOOL bLightOnOff = FALSE;
               if( m_nCurIRLightValue == 0 && m_nPrvIRLightValue > 0 )
                  bLightOnOff = TRUE;
               if( m_nPrvIRLightValue == 0 && m_nCurIRLightValue > 0 )
                  bLightOnOff = TRUE;
               BOOL bIRModeJustChanged  = ( m_nState & PTZ_State_IROnOffJustChanged ) ? TRUE : FALSE;
               BOOL bIRValueJustChanged = m_nCurIRLightValue != m_nPrvIRLightValue;
               if( bLightOnOff || bIRModeJustChanged || bIRValueJustChanged ) {
                  m_nPrvIRLightValue   = m_nCurIRLightValue;
                  m_nIRLightChangeTime = GetTickCount();
               }
               if( ( nIRCtrlMode == IRMode_DayTime_DayMode_NightTime_NightMode_IROn ) && !( m_nState & PTZ_State_NightMode ) ) {
                  // IR Auto 모드이고 낮이면 IR 라이트를 0으로 설정한다. (mkjang-150630)
                  m_nCurIRLightValue = 0;
               }
               BOOL bMustSetIRLightCtrl = bLightOnOff || bIRModeJustChanged;
               if( bMustSetIRLightCtrl || ( GetTickCount() - m_nIRLightChangeTime < 1000 && GetTickCount() - m_nSetIRLightTime > 500 ) ) {
                  _SetIRLight( m_nCurIRLightValue );
                  m_nSetIRLightTime = GetTickCount();
                  m_pTimerMainThread->Wait();
               }
            }
         }
         m_nState &= ~PTZ_State_IROnOffJustChanged;
      }
   }
}

void CPTZCtrl::MainProc()
{

   ASSERT( m_pPTZControlProperty );

   // 실시간으로 절대각 수신이 가능한 카메라의 경우
   // 제어 명령어 사이 마다 요청 패킷을 수신하도록 한다.
   // 단 PTZ제어 명령에 절대각 요청명령을 붙여 전송이 가능한 경우는 제외한다.
   BOOL bInterReqAbsPos = FALSE;
   if( IsRealTimeGetAbsPosSupported() ) {
      if( !( m_nCapabilities & PTZCtrlCap_AttachReqAbsPosSupported ) )
         bInterReqAbsPos = TRUE;
   }
   m_pTimerMainThread->StartTimer();
   m_pTimerMainThread->SetFPS( 1000.0f / m_nSendCmdPeriod );
   m_nPrevCmdType  = m_nCurrCmdType;
   m_fZoomEstimate = GetAbsPos().Z;

   BOOL bDelayedABSPosMoveCam = ( m_nCapabilities & PTZCtrlCap_DelayedABSPosMoveCam ) ? TRUE : FALSE;
   m_nGotoAbsPosCompleteTime  = GetTickCount() - 1000;

   int i;
   UINT64 nCntSend = 0;

   for( i = 0;; i++ ) {
      if( m_nState & PTZ_State_ToBeStopThread )
         break;

      if( !IsPTZConnected() ) {
         m_nState &= ~PTZ_State_InitHWOK;

         float fReconnectionFPS = 1000.0f / m_nReconnectionPeriod;
         m_pTimerMainThread->SetFPS( fReconnectionFPS );

         if( FALSE == ( m_nCapabilities & PTZCtrlCap_ConnectionDependOnStream ) ) {
            if( PTZCtrlConnType_TCPServer == m_PTZInfo.m_nConnectType ) {
               if( !( m_nState & PTZ_State_Connected ) ) {
                  TimerChecker timer0( "PTZ_timer_0", m_pTimerMainThread->GetPeriod() );
                  m_pTimerMainThread->Wait();
                  continue;
               }
            } else {
               Disconnect();
               int nConnectRet = Connect();
               if( DONE == nConnectRet ) {
                  m_nState |= PTZ_State_Connected;
               } else {
                  TimerChecker timer1( "PTZ_timer_1", m_pTimerMainThread->GetPeriod() );
                  m_pTimerMainThread->Wait();
                  continue;
               }
            }
         } else {
            // 비디오 스트림 연결에 의존하는 경우 비디오 신호가 있으면 PTZ도 연결되어있는 것으로 한다.
            if( m_pStream && m_pStream->IsVideoSignalAlive() )
               m_nState |= PTZ_State_Connected;
            TimerChecker timer2( "PTZ_timer_2", m_pTimerMainThread->GetPeriod() );
            m_pTimerMainThread->Wait();
            continue;
         }
      }

      if( !( m_nState & PTZ_State_InitHWOK ) ) {
         m_pTimerMainThread->SetFPS( 6.6f ); // 하드웨어 초기화 명령들은 약 150ms 에 한번씩 수행.

         if( IsPTZConnected() ) {
            TimerChecker timer3( "PTZ_timer_3", m_pTimerMainThread->GetPeriod() );
            m_pTimerMainThread->Wait();
            _ContPTZMove( IPTZVector( 0, 0, 0, 0, 0 ) );

            if( _InitHW() ) {
               TimerChecker timer4( "PTZ_timer_4", m_pTimerMainThread->GetPeriod() );
               m_pTimerMainThread->Wait();
               m_nState |= PTZ_State_GetAbsPosToBeRequested;
               ReqAbsPos();
               m_pTimerMainThread->Wait();
               if( m_bRequestStandMode ) {
                  ReqStandMode();
                  TimerChecker timer5( "PTZ_timer_5", m_pTimerMainThread->GetPeriod() );
                  m_pTimerMainThread->Wait();
               }

               if( m_PTZInfo.m_bWiperOn ) {
                  if( GetTickCount() - m_nWiperOnTime > MaxWiperOnTimeInSec * 1000 ) {
                     _SetWiper( FALSE );
                     m_nWiperOffTime      = 0;
                     m_PTZInfo.m_bWiperOn = FALSE;
                     if( m_pSubPTZControl ) m_pSubPTZControl->m_PTZInfo.m_bWiperOn = m_bCmdWiperOn;
                  }
               }

               m_nState |= PTZ_State_InitHWOK;
            }
         }
         TimerChecker timer6( "PTZ_timer_6", m_pTimerMainThread->GetPeriod() );
         m_pTimerMainThread->Wait();
         continue;
      }

      if( PTZCtrlConnType_LinkToPTZCtrl == m_PTZInfo.m_nConnectType ) {
         CPTZCtrl_Linking* pPTZCtrl_Linking = (CPTZCtrl_Linking*)this;
         pPTZCtrl_Linking->LinkingPTZCtrlProc();
      }

      // 기동 시작시점 그리고 멈춤 시점에는 위치 얻기 명령보다 이동명령을 우선으로 한다.
      BOOL bJustStartOrStopContMove = FALSE;
      if( PTZCmdType_ContinuousMove == m_nCurrCmdType ) {
         if( m_InputPTZMoveHW.IsContPTZFIMoveStopped() && !m_InputPTZMoveHW_Prev.IsContPTZFIMoveStopped() )
            bJustStartOrStopContMove = TRUE;
         if( !m_InputPTZMoveHW.IsContPTZFIMoveStopped() && m_InputPTZMoveHW_Prev.IsContPTZFIMoveStopped() )
            bJustStartOrStopContMove = TRUE;
         m_InputPTZMoveHW_Prev = m_InputPTZMoveHW;
      }
      if( bJustStartOrStopContMove ) {
         if( nCntSend % 2 == 0 )
            nCntSend++;
      }

      if( m_nState & PTZ_State_ContMoveToBeStopped ) {
         if( m_nContPTZMoveStartTime ) {
            if( GetTickCount() - m_nContPTZMoveStartTime > ceil( m_nSendCmdPeriod * 1.1f ) ) {
               m_InputPTZMoveHW( 0, 0, 0, 0, 0 );
               m_nContPTZMoveStartTime = 0;
               m_nState &= ~PTZ_State_ContMoveToBeStopped;
            }
         }
      }

      if( m_nState & PTZ_State_ContPTZTrackingMode ) {
         bDelayedABSPosMoveCam = TRUE;
      }

      // PTZ 카메라 속도 측정시에는 절대각 위치를 계속요청한다.
      // bInterReqAbsPos플레그가 On 인 경우 짝수번째 루프에서 전송한다.
      if( !bInterReqAbsPos || ( bInterReqAbsPos && nCntSend % 2 == 0 ) ) {
         if( m_nState & PTZ_State_ContPTZSpeedCheck ) {
            ReqAbsPos();
         } else {
            // 일반모드이고 이동명령 패킷 사이에 보내는 모드(bInterReqAbsPos == TRUE)의 경우에는
            uint nReqAbsPosPeriod = 5000;
            BOOL bReqAbsPos       = FALSE;
            if( IsContPTZTrackMode() ) {
               bReqAbsPos = TRUE;
            } else if( !m_CurrPTZMoveHW.IsContPTZFIMoveStopped() ) {
               bReqAbsPos = TRUE;
            } else if( m_CurrPTZMoveHW.IsContPTZFIMoveStopped() && m_nLastPTZCmdMode == LastPTZCmdMode_ContinuousMove ) {
               bReqAbsPos       = TRUE;
               nReqAbsPosPeriod = 500;
            } else if( m_CurrPTZMoveHW.IsContPTZFIMoveStopped() && m_nLastPTZCmdMode == LastPTZCmdMode_GotoAbsPos ) {
               if( m_nPTZMoveState & PTZMoveState_Moving ) {
                  nReqAbsPosPeriod = 10000;
                  if( IsRealTimeGetAbsPosMode() ) {
                     if( !m_PrevAvgPTZPos.IsContPTZMoveStopped() ) {
                        nReqAbsPosPeriod = 100000; // 100초
                     } else {
                        nReqAbsPosPeriod = 10000; // 10초
                     }
                  }
               } else {
                  nReqAbsPosPeriod = 0;
               }
            }
            if( GetTickCount() < ( m_nLastSendCmdTime + nReqAbsPosPeriod ) ) {
               ReqAbsPos();
               if( m_nState & PTZ_State_GetAbsPosToBeRequested )
                  m_nState &= ~PTZ_State_GetAbsPosToBeRequested;
            } else if( m_nState & PTZ_State_GetAbsPosToBeRequested ) {
               if( GetTickCount() > ( m_nLastSendCmdTime + nReqAbsPosPeriod ) ) {
                  ReqAbsPos();
                  m_nState &= ~PTZ_State_GetAbsPosToBeRequested;
               }
            } else {
               if( !( m_nCapabilities & PTZCtrlCap_DoNotPeridicallyReqAbsPos ) ) {
                  if( GetTickCount() - m_nLastSendCmdTime > 10000 ) {
                     if( GetTickCount() - m_nReqAbsPosTime > 10000 ) {
                        ReqAbsPos();
                     }
                  }
               }
            }
         }
      }

      CheckAbsPosRes();
      // 연속이동 또는 절대각 이동 명령을 내린다.
      if( ( !bInterReqAbsPos ) || ( bInterReqAbsPos && ( nCntSend % 2 == 1 ) ) ) { // bInterReqAbsPos플레그가 On 인 경우 홀수 번째 루프에서 전송.
         // 임계영역 m_csCmdSet 을 제거할 만한 객체인지를 판단해야됨.
         BOOL bLockCmdSet = FALSE;
         if( bLockCmdSet )
            m_csCmdSet.Lock();
         IRLightCtrlProc();
         if( GetTickCount() < m_nLastSendCmdTime + 10000 ) { // 마지막 명령을 보내고 10초 이내동안.
            // 절대각 이동 (연속이동에 의한)방식으로 동작되다가 연속이동방식으로 전환하는 경우 Stop 명령을 1회 보내준다.
            if( m_nCurrCmdType == PTZCmdType_ContinuousMove && m_nPrevCmdType == PTZCmdType_GotoAbsPos ) {
               _ContPTZMove( IPTZVector( 0, 0, 0, 0, 0 ) );
               m_evtWait.Wait_For( 135 );
            }
            if( m_nCapabilities & PTZCtrlCap_SetAbsPosNotSupported ) {
               if( m_nCurrCmdType == PTZCmdType_GotoAbsPos ) {
                  m_nCurrCmdType = 0;
               }
            }

            BOOL bPresetMoveByAbsPos = FALSE;
            //////////////////////////////////////////////////////
            // 연속이동 제어
            if( PTZCmdType_ContinuousMove == m_nCurrCmdType ) {
               // PTZ 이동이 멈추기 전까지 Loop를 계속 수행.
               if( !( m_nState & PTZ_State_Stopped ) ) {
                  m_CurrPTZMoveHW = m_InputPTZMoveHW;
                  if( IsSendingContPTZFIMoveCmd() )
                     _ContPTZMove( m_CurrPTZMoveHW );
               }
               PTZPosEstProc();
            }
            //////////////////////////////////////////////////////
            // 절대각 이동 제어
            else if( PTZCmdType_GotoAbsPos == m_nCurrCmdType ) {
               BOOL bTestContAbsTracker = TRUE;
               // 이전 명령이 연속이동 이었다면 절대각 이동위치가 이동되었음을 살짝 표시한다.
               if( PTZCmdType_ContinuousMove == m_nPrevCmdType || PTZCmdType_GotoPreset == m_nPrevCmdType ) {
                  m_PrevPTZPos = m_TgtPTZPos;
                  m_PrevPTZPos += FPTZVector( 0.06f, 0.06f, 0.06f );
                  if( m_nState & ( PTZ_State_OffsetByBox ) ) {
                     m_TgtPTZPos = GetAbsPos();
                  }
               }
               //---------------------------------------------------------
               BOOL bGotoPTZByOffset = FALSE;
               // PTZ Offset 이동을 한다.
               if( m_nState & PTZ_State_OffsetByBox ) {
                  m_nState &= ~PTZ_State_OffsetByBox;
                  OffsetPositionByBox();
                  bGotoPTZByOffset = TRUE;
               }
               //---------------------------------------------------------
               // PTZ 카메라의 광축중심 위치 보정을 위한 값을 얻는다.
               m_CorrectedTgtPTZPos = m_TgtPTZPos;

               if( !bGotoPTZByOffset ) {
                  float zoomlog_tgt  = log( m_CorrectedTgtPTZPos.Z ) * MC_LOG2_INV;
                  float zoomlog_max  = log( MaxZoomFactor ) * MC_LOG2_INV;
                  float offset_ratio = 1.0f;
                  if( zoomlog_max > 0.0f )
                     offset_ratio = zoomlog_tgt / zoomlog_max;
                  // 절대각 명령을 보낼때 보정된 좌표를 보내기때문에
                  // 절대각 수신시에는 원래의 좌표계로 변환해야한다.
                  // 본래의 좌표계로의 변환은 GetAbsPos 함수 구현되어있기 때문에
                  // 수신된 절대각을 얻으려면 반드시 GetAbsPos 함수를 호출하여 얻어야한다.
                  m_CorrectedTgtPTZPos.P += m_PTZInfo.m_pvOpticalAxisOffset.P * offset_ratio;
                  m_CorrectedTgtPTZPos.T += m_PTZInfo.m_pvOpticalAxisOffset.T * offset_ratio;
                  // 부드러운 줌이동을 위하여 5초동안 입력된 목표줌값으로부터 평균 줌값을 계산한다.
                  if( ( m_nState & PTZ_State_ContPTZTracking ) && ( m_nState & PTZ_State_SmoothZoomCtrl ) && !( m_nState & PTZ_State_UserCtrl ) ) {
                     int nZoomAvgPeriod = 1000;
                     float fAvgTgtZoom  = m_pPTZCtrlLogItemQueue->CalcAvgTgtZoomValue( nZoomAvgPeriod );
                     float fZoomDiff1   = fabs( log2( m_TgtPTZPos.Z ) - log2( m_CurPTZPos.Z ) );
                     float fZoomDiff2   = fabs( log2( fAvgTgtZoom ) - log2( m_TgtPTZPos.Z ) );
                     if( fZoomDiff1 >= 0.35f )
                        m_CorrectedTgtPTZPos.Z = m_TgtPTZPos.Z;
                     else if( fZoomDiff2 < 0.35 )
                        m_CorrectedTgtPTZPos.Z = fAvgTgtZoom;
                  } else
                     m_CorrectedTgtPTZPos.Z = m_TgtPTZPos.Z;
               }
               CheckPanTiltPositionInReal( m_CorrectedTgtPTZPos.P, m_CorrectedTgtPTZPos.T );

               // jun_111004 : 사용자 제어 시에 절대각 이동을 연속이동 방식으로 할 지를 검토해 볼 필요가 있음.
               // (단... 연속모드 이동에 의하여 목표 절대각 이동이 가능한 경우)
               if( ( m_nState & PTZ_State_ContPTZTracking ) && IsRealTimeGetAbsPosMode() /*&& !m_bGetAbsPosError*/ ) {
                  m_pContAbsPosTracker->MainProc();
                  m_PrevPTZPos = m_CurPTZPos;
               }
               // 절대각 제어의 경우.
               else if( !bDelayedABSPosMoveCam || ( bDelayedABSPosMoveCam && ( GetTickCount() > m_nGotoAbsPosCompleteTime ) ) ) {
                  // PTZ 위치가 변경되었으면 PTZ 이동을 한다.

                  //if (FALSE == IsSamePTZPos (   ))
                  if( !IsFisheyeCam() || !bGotoPTZByOffset ) {
                     if( !m_CurrPTZMoveHW.IsContPTZFIMoveStopped() ) {
                        _ContPTZMove( IPTZVector( 0, 0, 0, 0, 0 ) );
                        m_CurrPTZMoveHW( 0, 0, 0, 0, 0 );
                        m_pTimerMainThread->Wait(); //100
                     }
                     // 이전 PTZ 위치와의 차이로 부터 PTZ 명령 주기를 얻는다.
                     m_nGotoAbsPosTime         = GetTickCount();
                     m_nGotoAbsPosMovingTime   = GetMovingTime( m_PrevPTZPos, m_CorrectedTgtPTZPos );
                     m_nGotoAbsPosCompleteTime = m_nGotoAbsPosMovingTime + GetTickCount() - m_nSendCmdPeriod / 2;

                     CheckPanTiltPositionInReal( m_CorrectedTgtPTZPos.P, m_CorrectedTgtPTZPos.T );
                     FPTZVector tmp_abs_pos = m_CorrectedTgtPTZPos;
                     ConvertPanTiltOrientationRealToHW( tmp_abs_pos.P, tmp_abs_pos.T );
                     //logd ("1 : (%5.1f, %5.1f) -> (%5.1f, %5.1f)\n", m_CorrectedTgtPTZPos.P, m_CorrectedTgtPTZPos.T, tmp_abs_pos.P, tmp_abs_pos.T);

                     _GotoAbsPos( tmp_abs_pos );
                     m_TgtPTZPos = m_CorrectedTgtPTZPos;
                     m_pContAbsPosTracker->ResetAll();

                     if( !IsGetAbsPosSupported() || ( IsGetAbsPosSupported() && m_PTZInfo.m_nTxOnly ) || m_bGetAbsPosError ) {
                        if( m_nCapabilities & PTZCtrlCap_MultiZoomModule ) {
                           if( m_CorrectedTgtPTZPos.ZT == m_PTZInfo.m_nZoomModuleType ) {
                              // 줌 모듈의 종류가 같을 때만 Z, F, I 값을 업데이트 한다.
                              SetAbsPos( tmp_abs_pos );
                           } else {
                              FPTZVector pvTgtPos = tmp_abs_pos;
                              pvTgtPos.Z          = PREV_CMD;
                              pvTgtPos.F          = PREV_CMD;
                              pvTgtPos.I          = PREV_CMD;
                              SetAbsPos( pvTgtPos );
                           }
                        } else {
                           SetAbsPos( tmp_abs_pos );
                        }
                     }
                     m_nState |= PTZ_State_AutoFocusToBeExcuted;
                     m_nPTZMoveState |= PTZMoveState_JustStarted;
                     m_nState &= ~( PTZ_State_AbsolutePosBeExcuted );
                  }
                  m_PrevPTZPos      = m_CorrectedTgtPTZPos;
                  m_nPTZStopCmdTime = GetTickCount();

                  PTZPosEstProc();
               }
            } else if( m_nCurrCmdType == PTZCmdType_SetPreset ) {
               if( m_nCmdPresetNo != -1 ) {
                  CPTZCtrl::_SetPreset( m_nCmdPresetNo );
                  m_nCmdPresetNo = -1;
               }
            } else if( m_nCurrCmdType == PTZCmdType_GotoPreset ) {
               if( m_nCmdPresetNo != -1 ) {
                  if( !m_CurrPTZMoveHW.IsContPTZFIMoveStopped() ) {
                     m_CurrPTZMoveHW( 0, 0, 0, 0, 0 );
                     _ContPTZMove( IPTZVector( 0, 0, 0, 0, 0 ) );
                     TimerChecker timer_preset( "PTZ_timer_preset", m_pTimerMainThread->GetPeriod() );
                     m_pTimerMainThread->Wait();
                  }
                  m_pContAbsPosTracker->ResetAll();
                  m_PrevPTZPos = m_CurPTZPos;
                  CPTZCtrl::_GotoPreset( m_nCmdPresetNo );
                  m_nCmdPresetNo = -1;
               }
               // 주의 : 프리셋 이동을 절대위치 이동방식으로 하는 경우
               //        이를 기억해두어야 한다.
               if( m_nCurrCmdType == PTZCmdType_GotoAbsPos ) {
                  bPresetMoveByAbsPos = TRUE;
               }
               if( m_nCurrCmdType == PTZCmdType_GotoPreset ) {
                  m_nPTZStopCmdTime = GetTickCount();
               }
               m_nPrevCmdType          = PTZCmdType_GotoPreset; // 특별하게 이전명령 기억
               m_nGotoAbsPosMovingTime = 5000;
            } else if( m_nCurrCmdType == PTZCmdType_ClearPreset ) {
               if( m_nCmdPresetNo != -1 ) {
                  CPTZCtrl::_ClearPreset( m_nCmdPresetNo );
                  m_nCmdPresetNo = -1;
               }
            } else if( m_nCurrCmdType == PTZCmdType_MenuOpen ) {
               _MenuOpen();
            } else if( m_nCurrCmdType == PTZCmdType_MenuEnter ) {
               _MenuEnter();
            } else if( m_nCurrCmdType == PTZCmdType_MenuCancel ) {
               _MenuCancel();
            }
            //else if (m_nCurrCmdType == PTZCmdType_AuxOn) {
            //   if (m_nCmdAuxNo != -1) {
            //      _AuxOn (m_nCmdAuxNo);
            //      m_nCmdAuxNo = -1;
            //   }
            //}
            else if( m_nCurrCmdType == PTZCmdType_AuxOff ) {
               if( m_nCmdAuxNo != -1 ) {
                  _AuxOff( m_nCmdAuxNo );
                  m_nCmdAuxNo = -1;
               }
            } else if( m_nCurrCmdType == PTZCmdType_PosInit ) {
               m_nState |= PTZ_State_PosInitializing;
               _PosInit();
               m_nState &= ~PTZ_State_PosInitializing;
            } else if( m_nCurrCmdType == PTZCmdType_SetIRMode ) {
               if( m_PTZInfo.m_bIRControlByIntelliVIX && !m_PTZInfo.m_bUseCameraDayAndNigntMode ) { // (mkjang-141211)
                  if( m_nCmdIRMode != -1 ) {
                     _SetIRMode( m_nCmdIRMode );
                     m_nCmdIRMode = -1;
                  }
               }
            } else if( m_nCurrCmdType == PTZCmdType_SetIRLight ) {
               if( m_PTZInfo.m_bIRControlByIntelliVIX ) { // (mkjang-141211)
                  if( m_nCmdIRLight != -1 ) {
                     if( !m_PTZInfo.m_bUseCameraDayAndNigntMode ) { // (mkjang-141211)
                        if( m_nCmdIRLight == 0 )
                           _SetIRMode( PTZIRCtrlMode_NightModeAndIROff );
                        else
                           _SetIRMode( PTZIRCtrlMode_NightModeAndIROn );
                     }
                     _SetIRLight( m_nCmdIRLight );
                     m_nCmdIRLight = -1;
                  }
               }
            } else if( m_nCurrCmdType == PTZCmdType_SetAutoFocusOn ) {
               if( m_bCmdAutoFocusOn != -1 ) {
                  m_PTZInfo.m_bAutoFocusOn = m_bCmdAutoFocusOn;
                  _SetAutoFocusOn( m_bCmdAutoFocusOn );
                  m_bCmdAutoFocusOn = -1;
               }
            }
            //else if (m_nCurrCmdType == PTZCmdType_SetDefog) {
            //   if (m_nCmdDefogLevel != -1) {
            //      m_PTZInfo.m_bDefogOn = m_nCmdDefogLevel;
            //      _SetDefog (m_nCmdDefogLevel);
            //      m_nCmdDefogLevel = -1;
            //   }
            //}
            else if( m_nCurrCmdType == PTZCmdType_SetSoftDefog ) {
               if( m_nCmdDefogLevel != -1 ) {
                  m_PTZInfo.m_bDefogOn = m_nCmdDefogLevel;
                  _SetSoftwareDefog( m_nCmdDefogLevel );
                  m_nCmdDefogLevel = -1;
               }
            }
            // (mkjang-140616)
            else if( m_nCurrCmdType == PTZCmdType_SetVideoStabilization ) {
               if( m_nCmdVideoStabilization != -1 ) {
                  m_PTZInfo.m_bVideoStabilization = m_nCmdVideoStabilization;
                  _SetVideoStabilization( m_nCmdVideoStabilization );
                  m_nCmdVideoStabilization = -1;
               }
            } else if( m_nCurrCmdType == PTZCmdType_BLCOn ) {
               if( m_nCmdBLCLevel != -1 ) {
                  _BLCOn( m_nCmdBLCOn );
                  m_nCmdBLCOn = -1;
               }
            } else if( m_nCurrCmdType == PTZCmdType_SetBLCLevel ) {
               if( m_nCmdBLCLevel != -1 ) {
                  _SetBLCLevel( m_nCmdBLCLevel );
                  m_nCmdBLCLevel = -1;
               }
            } else if( m_nCurrCmdType == PTZCmdType_SetDZoomMode ) {
               if( m_bCmdDZoomOn != -1 ) {
                  _SetDZoomMode( m_bCmdDZoomOn );
                  m_bCmdDZoomOn = -1;
               }
            } else if( m_nCurrCmdType == PTZCmdType_SetWiper ) {
               if( m_bCmdWiperOn != -1 ) {
                  if( !m_bCmdWiperOn )
                     m_nWiperOffTime = GetTickCount();
                  _SetWiper( m_bCmdWiperOn );
                  m_PTZInfo.m_bWiperOn = m_bCmdWiperOn;
                  m_nWiperOnTime       = GetTickCount();
                  if( m_pSubPTZControl ) m_pSubPTZControl->m_PTZInfo.m_bWiperOn = m_bCmdWiperOn;
                  m_bCmdWiperOn = -1;
               }
            } else if( m_nCurrCmdType == PTZCmdType_SetHeatingWire ) {
               if( m_bCmdHeatingWireOn != -1 ) {
                  m_PTZInfo.m_bHeaterOn = m_bCmdHeatingWireOn;
                  _SetHeatingWire( m_bCmdHeatingWireOn );
                  if( m_pSubPTZControl ) m_pSubPTZControl->m_PTZInfo.m_bHeaterOn = m_bCmdHeatingWireOn;
                  m_bCmdHeatingWireOn = -1;
               }
            } else if( m_nCurrCmdType == PTZCmdType_SetFan ) {
               if( m_bCmdFanOn != -1 ) {
                  _SetFan( m_bCmdFanOn );
                  m_bCmdFanOn = -1;
               }
            } else if( m_nCurrCmdType == PTZCmdType_SetDayAndNightMode ) {
               if( m_nCmdDayAndNightMode != -1 ) {
                  _SetDayAndNightMode( m_nCmdDayAndNightMode );
                  m_nCmdDayAndNightMode = -1;
               }
            } else if( m_nCurrCmdType == PTZCmdType_SetLaserIlluminator ) {
               if( m_bCmdLaserIlluminatorOn != -1 ) {
                  _SetLaserIlluminator( m_bCmdLaserIlluminatorOn );
                  m_bCmdLaserIlluminatorOn = -1;
               }
            } else if( m_nCurrCmdType == PTZCmdType_OneShotAutoFocus ) {
               _OneShotAutoFocus();
               m_nCurrCmdType = -1;
            } else if( m_nCurrCmdType == PTZCmdType_ThermalCameraOn ) {
               if( m_bCmdThermalCameraOn != -1 ) {
                  _ThermalCameraOn( m_bCmdThermalCameraOn );
                  m_PTZInfo.m_bThermalCamOn = m_bCmdThermalCameraOn;
                  if( m_pSubPTZControl ) m_pSubPTZControl->m_PTZInfo.m_bThermalCamOn = m_bCmdThermalCameraOn;
                  m_bCmdThermalCameraOn = -1;
               }
            } else if( m_nCurrCmdType == PTZCmdType_SetZoomControlMode ) {
               if( m_nCmdZoomControlType != -1 ) {
                  _SetZoomControlMode( m_nCmdZoomControlType );
                  BOOL bThermalMode           = ( ZoomModuleType_ThermalImaging == m_nCmdZoomControlType ) ? TRUE : FALSE;
                  m_PTZInfo.m_bThermalCamMode = bThermalMode;
                  if( m_pSubPTZControl ) m_pSubPTZControl->m_PTZInfo.m_bThermalCamMode = bThermalMode;
                  m_nCmdZoomControlType = -1;
               }
            }
            // 주의 : 프리셋에 의한 절대각 이동의 경우 이전 명령을 프리셋으로 해두어야
            //        절대위치 이동을 할 수 있다.
            if( !bPresetMoveByAbsPos ) {
               m_nPrevCmdType = m_nCurrCmdType;
            }
            if( PTZCmdType_GotoAbsPos == m_nCurrCmdType ) {
               m_nLastPTZCmdMode = LastPTZCmdMode_GotoAbsPos;
            }
            if( PTZCmdType_GotoPreset == m_nCurrCmdType ) {
               m_nLastPTZCmdMode = LastPTZCmdMode_GotoAbsPos;
            }
            if( PTZCmdType_ContinuousMove == m_nCurrCmdType ) {
               m_nLastPTZCmdMode = LastPTZCmdMode_ContinuousMove;
            }

            if( PTZCmdType_ContinuousMove != m_nCurrCmdType && ( ( PTZCmdType_GotoAbsPos == m_nCurrCmdType ) && !( ( m_nState & PTZ_State_ContPTZTracking ) && IsRealTimeGetAbsPosMode() ) ) )
               m_nCurrCmdType = -1;

         } // 마지막 명령을 보내고 10초 이내동안.

         if( bLockCmdSet )
            m_csCmdSet.Unlock();

         AddPTZCtrlLogItem();
         CheckPTZMoving();
      } // 연속이동 또는 절대각 이동.
      //m_pTimerMainThread->SetFPS( 1000.0f / m_nSendCmdPeriod );
      m_pTimerMainThread->SetFPS( 50 );
      TimerChecker time_move( "PTZ_timer_move", m_pTimerMainThread->GetPeriod() );
      m_pTimerMainThread->Wait();
      nCntSend++;
   }
   m_pTimerMainThread->StopTimer();
   Disconnect();

   logd( "[CH:%d] PTZ MainProc Exit!\n", m_nChannelNo_IVX + 1 );
}

void CPTZCtrl::NetPTZClientProc()
{
   if( m_pTimerNetThread )
      m_pTimerNetThread->StartTimer();

   BOOL bCreateAndBind                       = FALSE;
   uint32_t nTryingConnectionStartTime       = 0;
   uint32_t nConnectionErrorMessagePrintTime = 0;
   m_pTimerNetThread->SetFPS( 2.0f );
   std::string strIPAddress;
   GetIPAddrString( strIPAddress, m_PTZInfo.m_strIPAddress ); // (xinu_bc23)

   while( 1 ) {
      if( m_nState & PTZ_State_ToBeStopThread )
         break;

      if( !bCreateAndBind ) {
         m_StreamSocket.Create();
         m_StreamSocket.Bind( strIPAddress.c_str(), m_PTZInfo.m_nPortNo );
         bCreateAndBind = TRUE;
      }
      if( !nTryingConnectionStartTime )
         nTryingConnectionStartTime = GetTickCount();
      int nConnectRet = m_StreamSocket.ConnectNB( strIPAddress.c_str(), m_PTZInfo.m_nPortNo );

      if( DONE == nConnectRet ) {
         logi( "////////////////////////////////////////////////////\n" );
         logi( "[CH:%d] PTZ Control TCP Server Connection OK !! ip:%s port:%d\n", m_nChannelNo_IVX + 1, strIPAddress.c_str(), m_PTZInfo.m_nPortNo );
         //LOGI << "Socket : " << (SOCKET)m_StreamSocket;
         nConnectionErrorMessagePrintTime = 0;

         SOCKET sock = (SOCKET)m_StreamSocket;
         m_nState |= PTZ_State_Connected;
         uint32_t dwSendCmdPeriod = m_nSendCmdPeriod;
         if( !( m_nCapabilities & PTZCtrlCap_AttachReqAbsPosSupported ) )
            dwSendCmdPeriod /= 2;
         // 명령 전송 주기보다 조금 빠르게 처리한다.
         m_pTimerNetThread->SetFPS( 1000.0f / float( dwSendCmdPeriod / 2 ) + 1.0f );

         while( true ) {
            if( m_nState & PTZ_State_ToBeStopThread )
               break;

            // 요청 명령(GetAbsPos)을 보낸다.
            if( m_nSendReqCmdCntNet > 0 ) {
               u_long arg = 0;
               ioctlsocket( sock, FIONBIO, &arg );
               int nSent = m_StreamSocket.SendData( m_ReqCmd, m_nSendReqCmdLen, TRUE );
               LOGV << "Sent : org_len:" << m_nSendReqCmdLen << " sent_len:" << nSent
                    << " hex:" << sutil::StringToHex( m_ReqCmd, m_nSendReqCmdLen );
               if( m_nSendReqCmdLen != nSent ) {
                  m_nState &= ~( PTZ_State_Connected );
                  loge( "Ret: (%d)  Error Code:%d\n", nSent, m_StreamSocket.GetLastError() );
                  break;
               }
               m_nSendReqCmdCntNet--;

            }
            // 일반 명령을 보낸다.
            else if( m_nSendCmdCntNet > 0 ) {
               if( m_nSendCmdLen > 0 ) {
                  u_long arg = 0;
                  ioctlsocket( sock, FIONBIO, &arg );
                  int nSent = m_StreamSocket.SendData( m_sCmd, m_nSendCmdLen, TRUE );
                  LOGV << "Sent : org_len:" << m_nSendCmdLen << " sent_len:" << nSent
                       << " hex:" << sutil::StringToHex( m_sCmd, m_nSendCmdLen );
                  if( nSent != m_nSendCmdLen ) {
                     m_nState &= ~( PTZ_State_Connected );
                     break;
                  }
               }
               m_nSendCmdCntNet--;
            }

            // 응답 명령을 수신한다.
            int nReceived = 0;
            byte tmp;
            while( 1 ) {
               u_long arg = 1;
               ioctlsocket( sock, FIONBIO, &arg );
               nReceived = recv( sock, (char*)&tmp, 1, MSG_PEEK );
               if( nReceived == 1 ) {
                  u_long arg = 0;
                  ioctlsocket( sock, FIONBIO, &arg );
                  if( 1 == m_StreamSocket.ReceiveData( &tmp, 1, TRUE ) ) {
                     sutil::StringToHex( &tmp, 1 );
                     AddToRecvCmd( &tmp, 1 );
                     ParseRecvCmd();
                  }
               } else {
                  break;
               }
            }
            if (!(m_nState & PTZ_State_Connected))
               break;
            if( m_nState & PTZ_State_ToBeStopThread )
               break;
            if( m_pTimerNetThread )
               m_pTimerNetThread->Wait();
         }
      } else if( nConnectRet == ERR_WOULDBLOCK || nConnectRet == EINPROGRESS ) {
         m_pTimerNetThread->SetFPS( 10.0f ); // 잠깐 대기 모드 일 때는 100ms 마다 시도
      } else {
         if( !nConnectionErrorMessagePrintTime || ( GetTickCount() - nConnectionErrorMessagePrintTime > 30000 ) ) {
            loge( "[CH:%d] PTZ Control TCP Server Connection - Failed !! ip:%s port:%d\n",
                  m_nChannelNo_IVX + 1, strIPAddress.c_str(), m_PTZInfo.m_nPortNo );
            nConnectionErrorMessagePrintTime = GetTickCount();
         }
         m_pTimerNetThread->SetFPS( 1.0f ); // 접속이 안될 때에는 1초에 한번씩 만 연결 시도
      }

      if( m_nState & PTZ_State_ToBeStopThread )
         break;
      if( m_pTimerNetThread ) {
         if( GetTickCount() - nTryingConnectionStartTime > 5000 ) {
            if( !( m_nState & PTZ_State_Connected ) ) {
               m_StreamSocket.Close();
               bCreateAndBind = FALSE;
               m_pTimerNetThread->SetFPS( 1.0f / 5.0f ); // 접속이 안될 때에는 5초에 한번씩 만 연결 시도
               nTryingConnectionStartTime = 0;
            }
         }
         m_pTimerNetThread->Wait();
      }
   }
   m_StreamSocket.Close();
   if( m_pTimerNetThread )
      m_pTimerNetThread->StopTimer();
   m_nState &= ~( PTZ_State_Connected );
   logw( "[CH:%d] PTZ NetPTZClientProc Exit!\n", m_nChannelNo_IVX + 1 );
}

UINT CPTZCtrl::Thread_MainProc( LPVOID params )
{
#ifdef SUPPORT_PTHREAD_NAME
   pthread_setname_np( pthread_self(), "PTZMain" );
#endif
   CPTZCtrl* pCamera = (CPTZCtrl*)params;
   pCamera->m_nState |= PTZ_State_ThreadPTZControl;
   pCamera->MainProc();
   pCamera->m_nState &= ~PTZ_State_ThreadPTZControl;
   return ( DONE );
}

UINT CPTZCtrl::Thread_NetPTZClient( LPVOID params )
{
#ifdef SUPPORT_PTHREAD_NAME
   pthread_setname_np( pthread_self(), "PTZClient" );
#endif
   LOGD << "CPTZCtrl Create Thread";
   CPTZCtrl* pCamera = (CPTZCtrl*)params;
   pCamera->NetPTZClientProc();
   return ( DONE );
}

int CPTZCtrl::ReadParameter( CXMLIO* pXMLIO )
{
   int i;

   CXMLNode xmlNode_Table( pXMLIO );
   if( xmlNode_Table.Start( "ZoomReal2HW" ) ) {
      xmlNode_Table.Attribute( TYPE_ID( float ), "ForcalLength", &m_fFocalLength );
      xmlNode_Table.Attribute( TYPE_ID( float ), "ForcalLengthRatio", &m_fFocalLengthRatio );
      if( m_pMainPTZControl ) {
         m_pMainPTZControl->m_fFocalLength      = m_fFocalLength;
         m_pMainPTZControl->m_fFocalLengthRatio = m_fFocalLengthRatio;
      }
      CXMLNode xmlNode_TableItem( pXMLIO );
      for( i = 0; i < m_ZoomReal2HWMapInfo.m_nTblSize; i++ ) {
         if( xmlNode_TableItem.Start( "Item" ) ) {
            xmlNode_TableItem.Attribute( TYPE_ID( float ), "A", &m_ZoomReal2HWMapInfo.m_pTbl[i].A );
            xmlNode_TableItem.Attribute( TYPE_ID( float ), "B", &m_ZoomReal2HWMapInfo.m_pTbl[i].B );
            xmlNode_TableItem.End();
         }
      }
      xmlNode_Table.End();
   }
   return ( DONE );
}

int CPTZCtrl::WriteParameter( CXMLIO* pXMLIO )
{
   int i;

   CXMLNode xmlNode_Table( pXMLIO );
   if( xmlNode_Table.Start( "ZoomReal2HW" ) ) {
      xmlNode_Table.Attribute( TYPE_ID( float ), "ForcalLength", &m_fFocalLength );
      xmlNode_Table.Attribute( TYPE_ID( float ), "ForcalLengthRatio", &m_fFocalLengthRatio );
      CXMLNode xmlNode_TableItem( pXMLIO );
      for( i = 0; i < m_ZoomReal2HWMapInfo.m_nTblSize; i++ ) {
         if( xmlNode_TableItem.Start( "Item" ) ) {
            xmlNode_TableItem.Attribute( TYPE_ID( float ), "A", &m_ZoomReal2HWMapInfo.m_pTbl[i].A );
            xmlNode_TableItem.Attribute( TYPE_ID( float ), "B", &m_ZoomReal2HWMapInfo.m_pTbl[i].B );
            xmlNode_TableItem.End();
         }
      }
      xmlNode_Table.End();
   }
   return ( DONE );
}

BOOL CPTZCtrl::IsSameResCmd( BYTE byte1, BYTE byte2, BYTE byte3, BYTE byte4 )
{
   if( m_ResCmd[0] != byte1 ) return FALSE;
   if( m_ResCmd[1] != byte2 ) return FALSE;
   if( m_ResCmd[2] != byte3 ) return FALSE;
   if( m_ResCmd[3] != byte4 ) return FALSE;
   return TRUE;
}

BOOL CPTZCtrl::IsSamePTZPos()
{
   if( fabs( m_TgtPTZPos.P - m_PrevPTZPos.P ) > 0.05f )
      return FALSE;
   if( fabs( m_TgtPTZPos.T - m_PrevPTZPos.T ) > 0.05f )
      return FALSE;
   float fZoomLog2Diff = fabs( log( m_TgtPTZPos.Z ) * MC_LOG2_INV - log( m_PrevPTZPos.Z ) * MC_LOG2_INV ) > 0.1f;
   if( fZoomLog2Diff )
      return FALSE;
   if( fabs( m_TgtPTZPos.F - m_PrevPTZPos.F ) > 0.05f )
      return FALSE;
   if( fabs( m_TgtPTZPos.I - m_PrevPTZPos.I ) > 0.05f )
      return FALSE;
   return TRUE;
}

////////////////////////////////////////////////////////////////////////////
//
// Pan, Tilt 속도 측정관련 함수
//
////////////////////////////////////////////////////////////////////////////

void CPTZCtrl::StartContSpdChk()
{
   m_bSignalStopContSpdChkThread = FALSE;

   if( NULL == m_pTimerContSpdChk )
      m_pTimerContSpdChk = m_pTimerMgr->GetNewTimer();

   m_ThreadContSpdChk = std::thread( Thread_ContSpdChk, this );
}

void CPTZCtrl::StopContSpdChk()
{
   if( m_ThreadContSpdChk.joinable() ) {
      m_bSignalStopContSpdChkThread = TRUE;

      m_evtRecvAbsPos.SetEvent();
      m_evtWaitCSC.SetEvent();

      if( m_ThreadContSpdChk.joinable() ) {
         m_ThreadContSpdChk.join();
      }
      m_bSignalStopContSpdChkThread = FALSE;

      if( m_pTimerContSpdChk ) {
         m_pTimerMgr->DeleteTimer( m_pTimerContSpdChk->m_nTimerID );
         m_pTimerContSpdChk = NULL;
      }
   }
}

void CPTZCtrl::ContSpdChkProc()
{
   int i, j;

   std::string strCheckTime          = sutil::scurrent_time( "%Y%m%d-%H%M" );
   //std::string strPTZControlTypeName = PTZControlTypeString.GetString( GetPTZControlTypeOfPTZCmd() );
   //std::string strFileName           = sutil::sformat( "[%s]PTZSpeedCheck_%s.txt", strCheckTime.c_str(), strPTZControlTypeName.c_str() );

   BOOL bZoomSpeedCheck = 0;

   std::vector<float> fZoomTableForSpeedCheck;

   if( m_bLogZoomStep ) {
      float fCurZoomPos = m_fStartZoomPos;
      for( i = 0; fCurZoomPos <= m_fEndZoomPos; i++ ) {
         fZoomTableForSpeedCheck.push_back( fCurZoomPos );
         fCurZoomPos = pow( 2.0f, log2( m_fStartZoomPos ) + m_fLog2ZoomStepSize * ( i + 1 ) );
      }
      if( fCurZoomPos != m_fEndZoomPos && m_fStartZoomPos != m_fEndZoomPos ) {
         fZoomTableForSpeedCheck.push_back( m_fEndZoomPos );
      }
   } else {
      for( i = int( m_fStartZoomPos ); i <= int( m_fEndZoomPos ); i++ ) {
         fZoomTableForSpeedCheck.push_back( float( i ) );
      }
   }

   //fZoomTableForSpeedCheck.push_back (1.00f);
   //fZoomTableForSpeedCheck.push_back (1.19f);
   //fZoomTableForSpeedCheck.push_back (1.41f);
   //fZoomTableForSpeedCheck.push_back (1.68f);
   //fZoomTableForSpeedCheck.push_back (2.00f);
   //fZoomTableForSpeedCheck.push_back (2.38f);
   //fZoomTableForSpeedCheck.push_back (2.83f);
   //fZoomTableForSpeedCheck.push_back (3.36f);
   //fZoomTableForSpeedCheck.push_back (4.00f);
   //fZoomTableForSpeedCheck.push_back (4.75f);
   //fZoomTableForSpeedCheck.push_back (5.66f);
   //fZoomTableForSpeedCheck.push_back (6.72f);
   //fZoomTableForSpeedCheck.push_back (8.00f);
   //fZoomTableForSpeedCheck.push_back (9.51f);
   //fZoomTableForSpeedCheck.push_back (11.31f);
   //fZoomTableForSpeedCheck.push_back (13.45f);
   //fZoomTableForSpeedCheck.push_back (16.00f);
   //fZoomTableForSpeedCheck.push_back (19.02f);
   //fZoomTableForSpeedCheck.push_back (22.62f);
   //fZoomTableForSpeedCheck.push_back (26.91f);
   //fZoomTableForSpeedCheck.push_back (30.00f);

   int nZoomTestNum      = (int)fZoomTableForSpeedCheck.size();
   int nPanSpeedCheckNum = 1;
   for( j = 1; j <= MaxPanSpeed; j += m_nHWSpeedStep )
      nPanSpeedCheckNum++;
   int nTiltSpeedCheckNum = 1;
   for( j = 1; j <= MaxTiltSpeed; j += m_nHWSpeedStep )
      nTiltSpeedCheckNum++;

   FArray2D panSpeedArray, tiltSpeedArray;
   panSpeedArray.Create( nPanSpeedCheckNum, nZoomTestNum );
   panSpeedArray.Clear();
   tiltSpeedArray.Create( nTiltSpeedCheckNum, nZoomTestNum );
   tiltSpeedArray.Clear();

   //WriteContSpeedChkFile( strFileName, panSpeedArray, tiltSpeedArray );

   uint nZoomSpeedChangeWaitTime = 200; // Zoom 속도 변경 후 대기시간
   double dfCheckTimeSum;
   double dfPrevCheckTime;
   double dfCurrCheckTime;
   CPerformanceCounter pfCounter;

   ////////////////////////////////////////////////////////////////////
   // Pan Speed Check
   ////////////////////////////////////////////////////////////////////
   if( m_bPanSpeedCheck ) {
      for( i = 0; i < nZoomTestNum; i++ ) {
         if( m_bSignalStopContSpdChkThread )
            break;
         logd( "////////////////////    Zoom=%g   /////////////////////\n", fZoomTableForSpeedCheck[i] );
         ContPTZMoveHW( FPTZVector( 0.0f, 0.0f, 0.0f ) );
         Sleep_ms( 2000 );
         float fPanDir = 1.0f;
         if( m_bSignalStopContSpdChkThread )
            break;
         else
            m_evtWaitCSC.Wait_For( 1000 );
         GotoAbsPos( FPTZVector( 0.0f, 50.0f, fZoomTableForSpeedCheck[i], PREV_CMD ) );
         if( m_bSignalStopContSpdChkThread )
            break;
         else
            m_evtWaitCSC.Wait_For( 7000 );
         //logd ("Current Zoom : %f\n", m_CurPTZPos.Z);
         logd( "Zoom Pan  ContSpd      Velocity    CheckCount\n" );
         for( j = 1; j <= MaxPanSpeed; j += m_nHWSpeedStep ) {
            if( m_bSignalStopContSpdChkThread )
               break;
            // Pan 속도를 변경한다.
            float fPanSpeed = float( j );
            ContPTZMoveHW( FPTZVector( fPanSpeed * fPanDir, 0.0f, 0.0f ) );
            m_evtWaitCSC.Wait_For( m_nPanSpeedChangeWaitTime );
            m_PrevChkPTZPos( NO_CMD, NO_CMD, NO_CMD );
            pfCounter.CheckTimeStart();
            int nCheckCount        = 0;
            float fPanAngleDiffSum = 0.0f;
            dfCheckTimeSum         = 0.0;
            m_evtRecvAbsPos.ResetEvent();
            while( 1 ) {
               if( m_bSignalStopContSpdChkThread )
                  break;
               // 방향 전환을 한다.
               if( m_bLimitPanAngleRange ) {
                  BOOL bChangeDir = FALSE;
                  if( -1.0f == fPanDir ) {
                     if( GetAbsPos().P > m_fMaxPanAngleForSpeedCheck ) {
                        fPanDir = 1.0f;
                        fPanDir = TRUE;
                     }
                  } else if( 1.0f == fPanDir ) {
                     if( GetAbsPos().P < m_fMinPanAngleForSpeedCheck ) {
                        fPanDir    = -1.0f;
                        bChangeDir = TRUE;
                     }
                  }
                  if( bChangeDir ) {
                     ContPTZMoveHW( FPTZVector( fPanSpeed * fPanDir, NO_CMD, NO_CMD ) );
                     m_PrevChkPTZPos( NO_CMD, NO_CMD, NO_CMD );
                     m_evtWaitCSC.Wait_For( m_nPanDirectionChangeWaitTime );
                     m_evtRecvAbsPos.ResetEvent();
                  }
               }
               if( !m_bSignalStopContSpdChkThread )
                  m_evtRecvAbsPos.Wait();
               dfCurrCheckTime = pfCounter.GetElapsedTime();
               if( m_PrevChkPTZPos.P != NO_CMD ) {
                  {
                     float dp = fabs( m_PrevChkPTZPos.P - GetAbsPos().P );
                     if( dp > 180.0f ) dp = 360.0f - dp;
                     fPanAngleDiffSum += dp;
                     dfCheckTimeSum += dfCurrCheckTime - dfPrevCheckTime;
                     nCheckCount++;
                     //logd ("dp:%f cc:%d\n", dp, nCheckCount);
                  }
               }
               dfPrevCheckTime = dfCurrCheckTime;
               m_PrevChkPTZPos = GetAbsPos();
               if( nCheckCount >= m_nSpeedCheckCount )
                  break;
            }
            float fPanVelocity = fPanAngleDiffSum / ( float( dfCheckTimeSum ) / 1000.0f );
            logd( "%2.2f %.1f, %7.3f\n", m_CurPTZPos.Z, fPanSpeed, fPanVelocity );
            int nSpeedIdx               = ( j - 1 ) / m_nHWSpeedStep + 1;
            panSpeedArray[i][nSpeedIdx] = fPanVelocity;
            //if (j == 1)                 m_nSpeedCheckCount = 30;
            //if (fPanVelocity <= 0.5f)   m_nSpeedCheckCount = 30;
            //if (fPanVelocity > 0.5f)    m_nSpeedCheckCount = 25;
            //if (fPanVelocity > 5.0f)    m_nSpeedCheckCount = 20;
            //if (fPanVelocity > 5.0f)    m_nSpeedCheckCount = 15;
            //if (fPanVelocity > 10.0f)   m_nSpeedCheckCount = 12;
            //if (fPanVelocity > 25.0f)   m_nSpeedCheckCount = 10;
         }
         //WriteContSpeedChkFile( strFileName, panSpeedArray, tiltSpeedArray );
      }
   }

   ////////////////////////////////////////////////////////////////////
   // Tilt Speed Check
   ////////////////////////////////////////////////////////////////////
   if( m_bTiltSpeedCheck ) {
      for( i = 0; i < nZoomTestNum; i++ ) {
         if( m_bSignalStopContSpdChkThread )
            break;
         logd( "////////////////////    Zoom=%g   /////////////////////\n", fZoomTableForSpeedCheck[i] );
         ContPTZMoveHW( FPTZVector( 0.0f, 0.0f, 0.0f ) );
         Sleep_ms( 2000 );
         float fTiltDir = 1.0f;
         if( m_bSignalStopContSpdChkThread )
            break;
         else
            m_evtWaitCSC.Wait_For( 1000 );
         GotoAbsPos( FPTZVector( 30.0f, 15.0f, fZoomTableForSpeedCheck[i], PREV_CMD ) );
         if( m_bSignalStopContSpdChkThread )
            break;
         else
            m_evtWaitCSC.Wait_For( 5000 );
         logd( "Zoom Tilt ContSpd      Velocity    CheckCount\n" );
         for( j = 1; j <= MaxTiltSpeed; j += m_nHWSpeedStep ) {
            if( m_bSignalStopContSpdChkThread )
               break;
            float fTiltSpeed = float( j );
            ContPTZMoveHW( FPTZVector( NO_CMD, fTiltSpeed * fTiltDir, NO_CMD ) );
            m_PrevChkPTZPos( NO_CMD, NO_CMD, NO_CMD );
            pfCounter.CheckTimeStart();
            m_evtRecvAbsPos.ResetEvent();
            m_evtWaitCSC.Wait_For( m_nTiltSpeedChangeWaitTime );
            int nCheckCount         = 0;
            float fTiltAngleDiffSum = 0.0f;
            dfCheckTimeSum          = 0.0;
            m_evtRecvAbsPos.ResetEvent();
            while( 1 ) {
               if( m_bSignalStopContSpdChkThread )
                  break;

               // 방향 전환을 한다.
               {
                  BOOL bChangeDir = FALSE;
                  if( -1.0f == fTiltDir ) {
                     float fTiltAngle = GetAbsPos().T;
                     if( fTiltAngle < m_fMinTiltAngleForSpeedCheck ) {
                        fTiltDir *= -1.0f;
                        bChangeDir = TRUE;
                     }
                  } else if( 1.0f == fTiltDir ) {
                     float fTiltAngle = GetAbsPos().T;
                     if( fTiltAngle > m_fMaxTiltAngleForSpeedCheck ) {
                        fTiltDir *= -1.0f;
                        bChangeDir = TRUE;
                     }
                  }
                  if( bChangeDir ) {
                     ContPTZMoveHW( FPTZVector( NO_CMD, fTiltSpeed * fTiltDir, NO_CMD ) );
                     m_PrevChkPTZPos( NO_CMD, NO_CMD, NO_CMD );
                     m_evtWaitCSC.Wait_For( m_nPanDirectionChangeWaitTime );
                     m_evtRecvAbsPos.ResetEvent();
                  }
               }

               if( !m_bSignalStopContSpdChkThread )
                  m_evtRecvAbsPos.Wait();
               dfCurrCheckTime = pfCounter.GetElapsedTime();
               if( m_PrevChkPTZPos.T != NO_CMD ) {
                  float dp = fabs( m_PrevChkPTZPos.T - GetAbsPos().T );
                  if( dp > 180.0f ) dp = 360.0f - dp;
                  fTiltAngleDiffSum += dp;
                  dfCheckTimeSum += dfCurrCheckTime - dfPrevCheckTime;
                  nCheckCount++;
               }
               dfPrevCheckTime = dfCurrCheckTime;
               m_PrevChkPTZPos = GetAbsPos();

               if( nCheckCount >= m_nSpeedCheckCount )
                  break;
            }
            float fTiltVelocity = fTiltAngleDiffSum / ( float( dfCheckTimeSum ) / 1000.0f );
            logd( "%2.2f %.1f, %7.3f\n", m_CurPTZPos.Z, fTiltSpeed, fTiltVelocity );
            int nSpeedIdx                = ( j - 1 ) / m_nHWSpeedStep + 1;
            tiltSpeedArray[i][nSpeedIdx] = fTiltVelocity;

            //if (j == 1)                  m_nSpeedCheckCount = 35;
            //if (fTiltVelocity <= 0.5f)   m_nSpeedCheckCount = 35;
            //if (fTiltVelocity > 0.5f)    m_nSpeedCheckCount = 25;
            //if (fTiltVelocity > 5.0f)    m_nSpeedCheckCount = 20;
            //if (fTiltVelocity > 5.0f)    m_nSpeedCheckCount = 15;
            //if (fTiltVelocity > 10.0f)   m_nSpeedCheckCount = 12;
            //if (fTiltVelocity > 25.0f)   m_nSpeedCheckCount = 10;
         }
         //WriteContSpeedChkFile( strFileName, panSpeedArray, tiltSpeedArray );
      }
   }
   ////////////////////////////////////////////////////////////////////
   // Zoom Speed Check
   ////////////////////////////////////////////////////////////////////
   //    if (bZoomSpeedCheck)
   //    {
   //       float fZoomDir = -1.0f;
   //       int   nZoomDirectionChangeWaitTime = 100;
   //       ContPTZMoveHW (FPTZVector (0.0f, 0.0f, 0.0f));
   //       GotoAbsPos (FPTZVector (0.0f, 0.0f, 1.0f, PREV_CMD));
   //       {
   //          std::lock_guard<std::mutex> lk(m_mWaitCSC);
   //          m_evtWaitCSC.wait_for(lk, std::chrono::milliseconds(6000));
   //       }
   //       logd ("Zoom ContSpd      Velocity    CheckCount\n");
   //       for (j = MinZoomSpeed; j <= MaxZoomSpeed; j += 1) {
   //          if (m_bSignalStopContSpdChkThread)
   //             break;
   //          float fZoomSpeed = float (j);
   //          ContPTZMoveHW (FPTZVector (NO_CMD, NO_CMD, fZoomSpeed * fZoomDir));
   //          m_PrevChkPTZPos (NO_CMD, NO_CMD, NO_CMD);
   //          pfCounter.CheckTimeStart (   );
   //          m_evtRecvAbsPos.ResetEvent ();
   //          {
   //             std::lock_guard<std::mutex> lk(m_mWaitCSC);
   //             m_evtWaitCSC.wait_for(lk, std::chrono::milliseconds(nZoomSpeedChangeWaitTime));
   //          }
   //          int   nCheckCount = 0;
   //          float fZoomDiffSum = 0.0f;
   //          dfCheckTimeSum = 0.0;
   //          m_evtRecvAbsPos.ResetEvent ();
   //          while (1)
   //          {
   //             if (m_bSignalStopContSpdChkThread)
   //                break;
   //             // 방향 전환을 한다.
   //             {
   //                BOOL bChangeDir = FALSE;
   //                if (-1.0f == fZoomDir) {
   //                   if (GetAbsPos().Z < 2.0f) {
   //                      fZoomDir = 1.0f;
   //                      bChangeDir = TRUE;
   //                   }
   //                }
   //                else if (1.0f == fZoomDir) {
   //                   if (GetAbsPos().Z > 8.0f) {
   //                      fZoomDir = -1.0f;
   //                      bChangeDir = TRUE;
   //                   }
   //                }
   //                if (bChangeDir) {
   //                   ContPTZMoveHW (FPTZVector (NO_CMD, NO_CMD, fZoomSpeed * fZoomDir));
   //                   m_PrevChkPTZPos (NO_CMD, NO_CMD, NO_CMD);
   //                   {
   //                      std::lock_guard<std::mutex> lk(m_mWaitCSC);
   //                      m_evtWaitCSC.wait_for(lk, std::chrono::milliseconds(nZoomDirectionChangeWaitTime));
   //                   }
   //                   m_evtRecvAbsPos.ResetEvent ();
   //                }
   //             }
   //             m_evtRecvAbsPos.Wait();
   //             dfCurrCheckTime = pfCounter.GetElapsedTime ();
   //             if (m_PrevChkPTZPos.T != NO_CMD) {
   //                float dp = fabs (log (m_PrevChkPTZPos.Z) * MC_LOG2_INV - log (GetAbsPos().Z) * MC_LOG2_INV);
   //                fZoomDiffSum += dp;
   //                dfCheckTimeSum += dfCurrCheckTime - dfPrevCheckTime;
   //                nCheckCount++;
   //             }
   //             dfPrevCheckTime = dfCurrCheckTime;
   //             m_PrevChkPTZPos = GetAbsPos();
   //             if (nCheckCount >= m_nSpeedCheckCount)
   //                break;
   //          }
   //          float fZoomLogVelocity = fZoomDiffSum / (float (dfCheckTimeSum) / 1000.0f);
   //       }
   //    }
   logd( "Check Stopped\n" );
   ContPTZMoveHW( FPTZVector( 0.0f, 0.0f, 0.0f ) );

   //WriteContSpeedChkFile( strFileName, panSpeedArray, tiltSpeedArray );
}

void CPTZCtrl::WriteContSpeedChkFile( const std::string& strFileName, FArray2D& panSpeedArray, FArray2D& tiltSpeedArray )
{
   int i, j;
   //  파일에 저장하기
   FileIO file;

   std::string strPathName = sutil::sformat( "C:\\%s", strFileName.c_str() );
   file.Open( strPathName.c_str(), FIO_TEXT, FIO_CREATE );
   file.Print( "///////// Pan Speed /////////\n" );
   for( i = 0; i < panSpeedArray.Height; i++ ) {
      for( j = 0; j < panSpeedArray.Width; j++ ) {
         file.Print( "%6.2ff,", panSpeedArray[i][j] );
      }
      file.Print( "\n" );
   }
   file.Print( "///////// Tilt Speed /////////\n" );
   for( i = 0; i < tiltSpeedArray.Height; i++ ) {
      for( j = 0; j < tiltSpeedArray.Width; j++ ) {
         file.Print( "%6.2ff,", tiltSpeedArray[i][j] );
      }
      file.Print( "\n" );
   }
   file.Close();
}

UINT CPTZCtrl::Thread_ContSpdChk( LPVOID params )
{
   CPTZCtrl* pCamera = (CPTZCtrl*)params;
   pCamera->m_nState |= PTZ_State_ContPTZSpeedCheck;
   pCamera->ContSpdChkProc();
   pCamera->m_nState &= ~PTZ_State_ContPTZSpeedCheck;
   return ( DONE );
}

#endif // __SUPPORT_PTZ_CAMERA

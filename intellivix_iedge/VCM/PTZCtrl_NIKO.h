#pragma  once

//#include <afxwin.h>
#include "PTZCtrl.h"

///////////////////////////////////////////////////////////////////////////////
//
// Class: CPTZCtrl_NIKO
//
///////////////////////////////////////////////////////////////////////////////

class CPTZCtrl_NIKO : public CPTZCtrl
{
public:
   int    m_ProtocolType_NICO;
   int    m_nReqAbsPosCnt;
   int    m_nZoomCmdSendCnt;
   int    m_nPrevZoomSpeed;
   uint32_t   m_nPrevZoomCmdTime;
   FPTZVector m_PrevTgtPTZPos;

public:
   IPTZVector m_PrevPTZMoveVec;
   float      m_PrevFocusDir;
   float      m_PrevIrisDir;

public:
   ushort     m_nPanMaxHw;
   ushort     m_nTiltMaxHw;
   int        m_nDongyangPTZIRCtrlMode;      // ���� ī�޶�� IR�� ��⸦ 0���� �Ϸ��� IR�� Off�� ���Ѿ��ϴµ� IR On Off ���¸� �����ϴ� �����̴�.
   
protected:
   CPTZCtrlInfo* m_pPTZCtrlInfo;

public:
   CPTZCtrl_NIKO (int nPTZCtontrolType);
   virtual ~CPTZCtrl_NIKO (   ) {   };

public:                              
   virtual int   GetWaitingTime         (float pd);
   virtual int   Open                   (CPTZCtrlInfo* pPTZCtrlInfo);

public:                           
   virtual void  Clear                  (BYTE* pCmd, int nCmdLen);
   virtual void  CheckSum               (BYTE* pCmd, int nCmdLen);
   virtual int   _ContPTZMove           (IPTZVector move_vec);
   virtual int   _GotoAbsPos            (FPTZVector abs_pos);
   virtual int   _GetAbsPosReqCmd       (BYTE* pCmd, int& nCmdLen);
   virtual int   _PopCommand            (   );
   virtual int   _SetPreset             (int preset_no);
   virtual int   _ClearPreset           (int preset_no);
   virtual int   _GotoPreset            (int preset_no);
   virtual int   _MenuOpen              (   );
   virtual int   _MenuCancel            (   );
   virtual int   _AuxOn                 (int nAuxNo);
   virtual int   _AuxOff                (int nAuxNo);

public:         
   virtual int   _PosInit              (   );
   virtual int   _SetIRMode            (int nPTZIRCtrlMode);
   virtual int   _SetIRLight           (int nIRLight);
   virtual int   _SetWiper             (BOOL bWiperOn);    
   virtual int   _SetHeatingWire       (BOOL bHeatingWireOn); 
   virtual int   _SetFan               (BOOL bFanOn);

public:
   int           ChangeZoomSpeed       (int nZoomSpeed);
   int           DeviceControl         (int nType, int nCode);

};


﻿#pragma once

#include "VCM.h"
#include "IPCameraManager.h"
#include "SolarCalculator.h"

class CVideoChannel;

//////////////////////////////////////////////////////////////////////////////////////////
//
// Class: CVideoChannelManager
//
//////////////////////////////////////////////////////////////////////////////////////////

class CVideoChannelManager {
   friend class CVideoChannel;

   enum State {
      State_Initialize              = 0x00000001,
      State_Activate                = 0x00000002,
      State_CaptureBoard_Initialize = 0x00000004,
   };

protected:
   int m_nState; // VCM의 상태. enum State 참조
   int m_nCurVCID; // 새로 생성할 비디오 채널의 아이디
   int m_nVideoChannelNum; // 비디오 채널의 수

protected:
   BOOL m_bSetupTemp; // VCM을 설정 저장용으로만 사용할 때 On.
   BOOL m_bVCMForRemote; // VCM을 원격에서 설정을 해야할 때 On.  주로 원격에서 설정가능하지 못한 항목을 Disable 처리함.

public:
   int m_nVCState[1024];
   std::deque<CVideoChannel*> m_VideoChannelList;
   CCriticalSection m_csVideoChannelList; // 비디오 채널 추가 삭제시의 동기화를 위한 객체

protected:
   VCM_Desc m_Desc; // 비디오 채널 관리자 초기화용 변수
   CIPCameraManager m_IPCamMgr; // 아이피 카메라 관리자
   CTimerManagerSP m_pTimerMgr;
#ifdef __SUPPORT_SOLAR_CALCULATOR
   CSolarCalculator m_SunCalculator; // 일출,일몰시간 계산기
#endif

public:
   CVideoChannelManager();
   ~CVideoChannelManager();

public: // 기본 함수
   void Initialize( VCM_Desc& desc );
   void Uninitialize();
   void Activate(); // VCM을 활성화 한다.  비디오채널 활성화와는 상관없음.
   void Deactivate();
   void ActivateAllVideoCahnnels();
   void DeactivateAllVideoChannels();
   int GetVideoChannelNum();

   // MS: 아래의 함수는 삭제해야 한다.
   VOID GetVCMDesc( VCM_Desc* pDesc )
   {
      if( pDesc ) *pDesc = m_Desc;
   }; // TYLEE: 2013-10-14
   VCM_Desc* GetVCMDesc() { return &m_Desc; }; // TYLEE: 2013-10-25

public: // 캡쳐보드
   BOOL DoesAnyOneChanneInDeactivating();

public: // 설정
   int Read( CXMLIO* pIO ); // VCM설정을 로드한다. CVideoChannel객체 설정은 제외
   int Write( CXMLIO* pIO ); // VCM설정을 저장한다. CVideoChannel객체 설정은 제외
   void SetSetupTemp( BOOL bSetupTemp ); // VCM 객체를 임시설정저장모드로 지정한다.
   void SetRemote( BOOL bRemote ); // VCM 객체를 원격설정모드로 지정한다.
   BOOL CheckRestart( CVideoChannelManager& from ); // 설정을 비교하여 VCM객체를 재시작해야하는지 체크한다.
   BOOL CheckRestart( int nVCID ); // 지정된 비디오채널이 재시작되어야 하는지 체크한다.
   BOOL CheckChange( CVideoChannelManager& from ); // 설정이 변경되었는지를 체크한다.
   BOOL CheckChange( int nVCID ); // 지정된 비디오채널의 설정이 변경되었는지를 체크한다.

protected:
   BOOL ReadAll( CXMLIO* pIO ); // VCM설정 및 모든 CVideoChannel객체의 설정을 로드한다.
   BOOL WriteAll( CXMLIO* pIO ); // VCM설정 및 모든 CVideoChannel객체의 설정을 저장한다.
   BOOL LoadCFGFile(); // 지정된 파일로부터 설정을 로드한다.
   BOOL SaveCFGFile(); // 지정된 파일에 설정을 저장한다.

public:
   CVideoChannel* CreateVideoChannel( CVideoChannelInfo& info );
   BOOL DeleteVideoChannel( int nVCID );
   CVideoChannel* GetVideoChannel( int nVCID );
   CVideoChannel* GetVideoChannelByIdx( int nVCIdx );
   CIPCameraManager* GetIPCameraManager() { return &m_IPCamMgr; }

protected:
   void ResetVideoChannelIdx();

public:
   friend class CCamera_VirtualPTZ;
   friend class CPTZCtrl_Linking;
   friend class CIPCameraConnectionManagerSelectionDlg;
   friend class CIVCPConnectionEventHandlerEx;

protected:
   std::deque<ICamera*> m_Cameras;
   CCriticalSection m_csCameras;
   BOOL m_bSignalStopVideoChannelManagementThread;
   std::thread m_VideoChannelManagementThread;

   void RegisterICamera( ICamera* pICamera );
   BOOL UnregisterICamera( ICamera* pICamera );
   BOOL IsExistInRegisteredCamera( ICamera* pICamera );
   void ProcCameraState(ICamera* pICamera);

   void VideoChannelManagementProc();
   static UINT Thread_VideoChannelManagement( LPVOID pVoid );
};

﻿#pragma once

#ifdef __SUPPORT_PTZ_CAMERA

#include "PTZVector.h"
#include "Quaternion.h"
#include "CommonUtil.h"

class CPTZCtrl;

////////////////////////////////////////////////////////////////////////
//
// Functions
//
////////////////////////////////////////////////////////////////////////

Quaternion CalcVelocity (FPTZVector fvCurrPTZPos, FPTZVector fvPrevPTZPos);

////////////////////////////////////////////////////////////////////////
//
// Enumerations
//
////////////////////////////////////////////////////////////////////////

enum ContAbsPosMove_State
{
   ContAbsPosMvTrk_State_GotoABSPosToBeBegun          = 0x00000001,
   ContAbsPosMvTrk_State_WaitContPTZMoveStop          = 0x00000010,   // 연속이동멈춤 명령을 보낸후 실제 멈출때까지 대기하는 상태
   ContAbsPosMvTrk_State_ContPTZMoveStopped           = 0x00000020,   // 연속이동멈춤 명령을 보낸다음 실제 PTZ가 멈춘 상태
   ContAbsPosMvTrk_State_WaitAbsPTZMoveStop           = 0x00000040,   // 절대각이동 명령 후 멈출때까지 대기하는 상태
   ContAbsPosMvTrk_State_StopContAndGotoAbsPosOngoing = 0x00000070,
   ContAbsPosMvTrk_State_AvgTgtPosVelocityToBeReset   = 0x00000100,
   ContAbsPosMvTrk_State_ConstantAngleSpeed           = 0x00000200,
   ContAbsPosMvTrk_State_ConstantImageScrollSpeed     = 0x00000400,
};

////////////////////////////////////////////////////////////////////////
//
// class ContAbsPosTrkLogItem
//
////////////////////////////////////////////////////////////////////////

class ContAbsPosTrkLogItem
{
public:
   uint32_t       m_nTime;
   Quaternion m_qtVelocity;
   float      m_fZoomLogVel;
public:
   ContAbsPosTrkLogItem (   );
};

////////////////////////////////////////////////////////////////////////
//
// class ContAbsPosTrkLogItemQueue
//
////////////////////////////////////////////////////////////////////////

class ContAbsPosTrkLogItemQueue
{
public:
   ContAbsPosTrkLogItem* m_pItems;

public:
   int   m_nStartIdx;
   int   m_nEndIdx;
   int   m_nItemNum;
   int   m_nMaxItemNum;
   int   m_nFramePeriod;
   int   m_nLogPeriod;

public:
   ContAbsPosTrkLogItemQueue (   );
   ~ContAbsPosTrkLogItemQueue (   );

public:
   void Initialize  (float fFrameRate, float fLogPeriod);
   int  GetItemIdx  (int nCurIdx, int nDeltaIdx);
   void AddItem     (ContAbsPosTrkLogItem& item);
   int  FindItemIdx (int nDeltaTime);
   ContAbsPosTrkLogItem* GetLastItem ( );

public:
   void ResetAvgVelocity (   );
   void CalcAvgVelocity  (uint32_t nDeltaTime, Quaternion& qtAvgVelocity);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: CContAbsPosMove
//
///////////////////////////////////////////////////////////////////////////////

class CContAbsPosTrack
{  
public:
   // 목표 위치 수렴속도 제어
   float m_fTgtPosConvergeFactor;                    // 목표 위치로 수렴하기 위한 비율.
   float m_fTargetConvergenceLimit;                  // 목표 위치로 수렴하기 위한 속도벡터의 제한 값. (화면 길이의 비율을 기준으로 할 때)
   float m_fAngleSpeedRatio;                         // 평균 각속도의 적용비율 -> 현재는 사용하지 않는다 
   float m_fTgtPosConvergeGamma;                     // 목표 이동 속도벡터에 감마비율 적용
   float m_fMaxGammaSpeed;                           // 감마속도 비율 기준점

   // 목표 위치 수렴여부 판단
   float m_fMaxAngleOffsetForConvergence;            // 수렴했음을 판단하는 범위각
   float m_fMaxZoomLogOffsetForConvergence;          // 줌이 수렴했음을 판단하는 범위 값 -> log2(Zoom)
   float m_fMaxZoomLogOffsetForSlowZoomSpeed;        // 느린 줌속도가 적용되는 Offset 

   // 절대위치 이동
   float m_fMinAngleOffsetForPanTiltAbsPosMove;      // 절대 위치 이동을 해야하는 최소각. 이 각 보다 크면 절대위치 이동을 함. 
   float m_fPanTiltOffsetRatioForGotoAbsPos;         // 절대 위치 이동을 해야하는 범위 비율. 예를들어 2.0인 경우 화면의 2배크기보다 이상 벗어나 있으면 절대위치 이동함.
   float m_fMinLog2OffsetForZoomAbsPosMove;          // 절대 위치 이동을 해야하는 최소 줌 범위. 이 때 줌 값은 log2를 취한 값으로 한다. 
   int   m_nAdditionalAbsPosMovePeriod;              // 절대 위치 이동 후 이동 대기시간에 추가적으로 쉬어주어야 하는 시간.
   int   m_nMaxAbsPosMovePeriod;                     // 절대 위치 이동 후 다음 절대위치 이동을 하기까지 대기해야 하는 시간.
   BOOL  m_bUseAbsZoomPosMove;                       // 절대 위치 이동 시 줌이동도 같이 함.
   float m_fMaxZoomFactorForAbsPosMoveTracking;      // 절대 위치 이동 방식으로 추적하기 위한 줌 값

   // 연속이동 제어
   float m_fMaxContPanMoveAngleSpeed;                // 연속이동방식 최대 Pan Speed 제한
   float m_fMaxContTiltMoveAngleSpeed;               // 연속이동방식 최대 Tilt Speed 제한
   int   m_nContMoveStopWaitTime;                    // 연속이동 후 절대각 이동을 하기까지의 대기 시간 (특수한 경우에만 해당)
   float m_fContZoomSpeed_Fast;                      // 줌 최대속도
   float m_fContZoomSpeed_Slow;                      // 줌 최소속도
   float m_fConstantContPanMoveAngleSpeed;           // 항속 Pan, Tilt 속도 각속도 <- 추적용이 아님
   float m_fConstantContTiltMoveAngleSpeed;          // 항속 Pan, Tilt 속도 각속도 <- 추적용이 아님
   float m_fConstantImageScrollSpeed;                // 항속 영상스크롤 속도       <- 추적용이 아님

public:
   int   m_nState;
   uint32_t  m_nGotoAbsPosTime;
   uint32_t  m_nContPTZStopCmdSendTime;
   uint  m_nAbsPosMovePeriod;
   float m_fFrameRate;
   double m_fOffsetAngle;

public:
   CPTZCtrl*  m_pPTZCtrl;
   FPTZVector m_fvTgtPos;
   FPTZVector m_fvCurPos;
   FPTZVector m_fvPrvPos;
   FPTZVector m_fvOffsetPos;
   FPTZVector m_fvContMoveVecHW;
   Quaternion m_qtVelocity;
   Quaternion m_qtAvgVelocity;
   ContAbsPosTrkLogItemQueue m_ContAbsPosTrkLogItemQueue;

public:
   CContAbsPosTrack (   );
   virtual ~CContAbsPosTrack (   );

public:
   void   Initialize          (CPTZCtrl* pPTZCtrl, float fFrameRate); 
   void   InitPTZCamParams     (   );
   void   MainProc             (   );
   void   GotoTgtPtzPos        (   );
   void   ResetAll             (   );
   void   ResetAvgTgtVelocity  (   );
   void   SetConstantAngleSpeedMode (BOOL bConstantSpeedMode, float fConstantAngleSpeed, FPTZVector pvTgtPTZPos);
   void   SetConstantImageScrollSpeedMode (BOOL bConstantSpeedMode, float fConstantImageScrollSpeed);

public:
   virtual int ReadParameter  (CXMLIO* pXMLIO);
   virtual int WriteParameter (CXMLIO* pXMLIO);
};                                     

#endif
#include "stdafx.h"
#include "VCM.h"
#include "PTZArea.h"
#if defined( __SUPPORT_PTZ_AREA )

///////////////////////////////////////////////////////////////////////////////
//
// Class: PTZPolygonalArea
//
///////////////////////////////////////////////////////////////////////////////

PTZPolygonalArea::PTZPolygonalArea()
{
   static int n_polygons = 1;
   ID                    = n_polygons++;
   Type                  = 0;
   PTZAreaType           = 0;
   AreaName              = "Mutual Assistance";

   IPAddress      = 0;
   PortNo         = 10000;
   ChannelNoMA    = 0;
   WaitingTime    = 15.0f;
   Flag_SPTZTrack = FALSE;
   PTZMoveType    = MAPTZMoveType_Preset;
   PresetNo       = -1;
   PTZPosMA( 0.0f, 0.0f, 1.0f );
   MinObjectLength = 1000.0f;

   IRLight = 0;

   m_pVoid = NULL;
}

void PTZPolygonalArea::Create( int n_vertices )
{
   Vertices.Create( n_vertices );
   PTZPositions.Create( n_vertices );
}

int PTZPolygonalArea::CheckSetup( PTZPolygonalArea* pg )
{
   int ret_flag = 0;

   FileIO buffThis, buffFrom;
   CXMLIO xmlIOThis, xmlIOFrom;

   // 변경된 아이템이 있는지 체크.

   xmlIOThis.SetFileIO( &buffThis, XMLIO_Write );
   xmlIOFrom.SetFileIO( &buffFrom, XMLIO_Write );
   WriteFile( &xmlIOThis );
   pg->WriteFile( &xmlIOFrom );

   if( buffThis.IsDiff( buffFrom ) )
      ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength( 0 );
   buffFrom.SetLength( 0 );

   if( ret_flag & CHECK_SETUP_RESULT_CHANGED ) {
      ret_flag |= CHECK_SETUP_RESULT_RESTART;
   }
   return ( ret_flag );
}

int PTZPolygonalArea::ReadFile( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PTZPolygonalArea" ) ) {
      int length;

      xmlNode.Attribute( TYPE_ID( int ), "ID", &ID );
      xmlNode.Attribute( TYPE_ID( int ), "Type", &Type );
      xmlNode.Attribute( TYPE_ID( int ), "PTZAreaType", &PTZAreaType );
      xmlNode.Attribute( TYPE_ID( string ), "AreaName", &AreaName );
      xmlNode.Attribute( TYPE_ID( int ), "length", &length );
      Create( length );
      xmlNode.Attribute( TYPE_ID( IPoint2D ), "Vertices", (IPoint2D*)Vertices, Vertices.Length );
      xmlNode.Attribute( TYPE_ID( FPoint3D ), "PTZPositions", (FPoint3D*)PTZPositions, PTZPositions.Length );

      if( PTZAreaType_MutualAsistanceArea == PTZAreaType ) {
         xmlNode.Attribute( TYPE_ID( xint ), "IPAddress", &IPAddress );
         xmlNode.Attribute( TYPE_ID( int ), "PortNo", &PortNo );
         xmlNode.Attribute( TYPE_ID( int ), "ChannelNoMA", &ChannelNoMA );
         xmlNode.Attribute( TYPE_ID( float ), "WaitingTime", &WaitingTime );
         xmlNode.Attribute( TYPE_ID( BOOL ), "Flag_SPTZTrack", &Flag_SPTZTrack );
         xmlNode.Attribute( TYPE_ID( int ), "PTZMoveType", &PTZMoveType );
         xmlNode.Attribute( TYPE_ID( int ), "PresetNo", &PresetNo );
         xmlNode.Attribute( TYPE_ID( FPoint3D ), "PTZPosMA", &PTZPosMA );
         xmlNode.Attribute( TYPE_ID( float ), "MinObjectLength", &MinObjectLength );
      }
      if( PTZAreaType_FloodLightArea == PTZAreaType ) {
         xmlNode.Attribute( TYPE_ID( int ), "IRLight", &IRLight );
      }

      xmlNode.End();
   }
   return ( DONE );
}

int PTZPolygonalArea::WriteFile( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "PTZPolygonalArea" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "ID", &ID );
      xmlNode.Attribute( TYPE_ID( int ), "Type", &Type );
      xmlNode.Attribute( TYPE_ID( int ), "PTZAreaType", &PTZAreaType );
      xmlNode.Attribute( TYPE_ID( string ), "AreaName", &AreaName );
      xmlNode.Attribute( TYPE_ID( int ), "length", &Vertices.Length );
      xmlNode.Attribute( TYPE_ID( IPoint2D ), "Vertices", (IPoint2D*)Vertices, Vertices.Length );
      xmlNode.Attribute( TYPE_ID( FPoint3D ), "PTZPositions", (FPoint3D*)PTZPositions, PTZPositions.Length );

      if( PTZAreaType_MutualAsistanceArea == PTZAreaType ) {
         xmlNode.Attribute( TYPE_ID( xint ), "IPAddress", &IPAddress );
         xmlNode.Attribute( TYPE_ID( int ), "PortNo", &PortNo );
         xmlNode.Attribute( TYPE_ID( int ), "ChannelNoMA", &ChannelNoMA );
         xmlNode.Attribute( TYPE_ID( float ), "WaitingTime", &WaitingTime );
         xmlNode.Attribute( TYPE_ID( BOOL ), "Flag_SPTZTrack", &Flag_SPTZTrack );
         xmlNode.Attribute( TYPE_ID( int ), "PTZMoveType", &PTZMoveType );
         xmlNode.Attribute( TYPE_ID( int ), "PresetNo", &PresetNo );
         xmlNode.Attribute( TYPE_ID( FPoint3D ), "PTZPosMA", &PTZPosMA );
         xmlNode.Attribute( TYPE_ID( float ), "MinObjectLength", &MinObjectLength );
      }
      if( PTZAreaType_FloodLightArea == PTZAreaType ) {
         xmlNode.Attribute( TYPE_ID( int ), "IRLight", &IRLight );
      }
      xmlNode.End();
   }
   return ( DONE );
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: PTZPolygonalAreaList
//
///////////////////////////////////////////////////////////////////////////////

void PTZPolygonalAreaList::RenderAreaMap( GImage& d_image, FPoint2D p2Scale, byte p_value )
{
   PTZPolygonalArea* pPTZPolygonalArea = (PTZPolygonalArea*)First();
   while( pPTZPolygonalArea ) {
      BCCL::Polygon* polygon = (BCCL::Polygon*)pPTZPolygonalArea;
      polygon->Fill( p_value, p2Scale, d_image );
      pPTZPolygonalArea = (PTZPolygonalArea*)Next( pPTZPolygonalArea );
   }
}
#endif

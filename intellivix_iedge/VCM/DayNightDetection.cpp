#include "stdafx.h"
#include "SolarCalculator.h"
#include "DayNightDetection.h"
#include "CommonUtil.h"

#ifdef __SUPPORT_DAYNIGHT_DETECTION

#define NUM_PREVIOUS_FRAMES 5

DayNightDetection::DayNightDetection()
{
   m_bUseUVAvg            = FALSE;
   m_nState               = 0;
   m_nPrevState           = 0;
   m_pSunCalculator       = NULL;
   m_nDayNightCheckPeriod = 1000 * 10;
   m_nSunCalcPeriod       = 1000 * 60 * 60; // 1시간 마다.
   m_fUVDayThreshold      = 0.001f;
   m_fUVNightThreshold    = 0.1f;
}

DayNightDetection::~DayNightDetection()
{
}

void DayNightDetection::Initialize( CSolarCalculator* pSunCalculator )
{
   m_pSunCalculator     = pSunCalculator;
   m_nDayNightCheckTick = GetTickCount() - m_nDayNightCheckPeriod;
   CalcSunRiseSetTime();
   CheckDayNightTime();
}

int DayNightDetection::Perform()
{
   return Perform( NULL, 0 );
}

int DayNightDetection::Perform( byte* pYUY2ImgBuff, int nYUY2ImgBuffLen )
{
   if( GetTickCount() - m_nSunCalcTick > m_nSunCalcPeriod ) {
      CalcSunRiseSetTime();
   }

   if( GetTickCount() - m_nDayNightCheckTick > m_nDayNightCheckPeriod ) {
      CheckDayNightTime();

      if( FALSE == m_bUseUVAvg || NULL == pYUY2ImgBuff ) {
         if( m_nState & DayNightDetectionState_DayTime ) {
            m_nState |= DayNightDetectionState_DayMode;
            m_nState &= ~DayNightDetectionState_NightMode;
         } else if( m_nState & DayNightDetectionState_NightTime ) {
            m_nState |= DayNightDetectionState_NightMode;
            m_nState &= ~DayNightDetectionState_DayMode;
         }
      } else {
         int i;

         float fUSum = 0.0f;
         float fVSum = 0.0f;

         const BYTE& bc  = 128;
         const float& fc = 128.0f;

         BOOL bUComponent = TRUE;
         for( i = 1; i < nYUY2ImgBuffLen; i += 2 ) {
            if( bUComponent ) {
               BYTE& u = pYUY2ImgBuff[i];
               fUSum += float( abs( u - bc ) ) / fc;

               bUComponent = FALSE;
            } else {
               BYTE& v = pYUY2ImgBuff[i];
               fVSum += float( abs( v - bc ) ) / fc;

               bUComponent = TRUE;
            }
         }

         int nUCnt    = nYUY2ImgBuffLen / 4;
         float fUAvg  = fUSum / nUCnt;
         float fVAvg  = fVSum / nUCnt;
         float fUVAvg = ( fUAvg + fVAvg ) * 0.5f;

         if( m_nState & DayNightDetectionState_DayTime ) {
            if( fUVAvg < m_fUVDayThreshold ) {
               m_nState |= DayNightDetectionState_NightMode;
               m_nState &= ~DayNightDetectionState_DayMode;
            } else {
               m_nState |= DayNightDetectionState_DayMode;
               m_nState &= ~DayNightDetectionState_NightMode;
            }
         } else if( m_nState & DayNightDetectionState_NightTime ) {
            if( fUVAvg < m_fUVNightThreshold ) {
               m_nState |= DayNightDetectionState_NightMode;
               m_nState &= ~DayNightDetectionState_DayMode;
            } else {
               m_nState |= DayNightDetectionState_DayMode;
               m_nState &= ~DayNightDetectionState_NightMode;
            }
         }

         BOOL bDayMode = ( m_nState & DayNightDetectionState_DayMode ) ? TRUE : FALSE;
         //logd ("UAvg = %f , VAvg = %f DayNightMode:%d \n", fUAvg, fVAvg, bDayMode);
      }
      m_nDayNightCheckTick = GetTickCount();
   }

   if( !( m_nPrevState & DayNightDetectionState_DayMode ) && ( m_nState & DayNightDetectionState_DayMode ) ) {
      m_nState |= DayNightDetectionState_DayModeJustChanded;
      //logd ("DayNightDetectionState_DayModeJustChanded\n");
   } else {
      m_nState &= ~DayNightDetectionState_DayModeJustChanded;
   }

   if( !( m_nPrevState & DayNightDetectionState_NightMode ) && ( m_nState & DayNightDetectionState_NightMode ) ) {
      m_nState |= DayNightDetectionState_NightModeJustChanded;
      //logd ("DayNightDetectionState_NightModeJustChanded\n");
   } else {
      m_nState &= ~DayNightDetectionState_NightModeJustChanded;
   }

   m_nPrevState = m_nState;

   return ( DONE );
}

void DayNightDetection::CalcSunRiseSetTime()
{
   if( m_pSunCalculator ) {
      BCCL::Time tmCurr;
      tmCurr.GetLocalTime();
      m_tmSunRise = m_pSunCalculator->CalculateSunRise( tmCurr );
      m_tmSunSet  = m_pSunCalculator->CalculateSunSet( tmCurr );

      BCCL::Convert( m_tmSunRise, m_ftSunRise );
      BCCL::Convert( m_tmSunSet, m_ftSunSet );
      m_nSunCalcTick = GetTickCount();
   }
}

void DayNightDetection::CheckDayNightTime()
{
   BCCL::Time tmCurr;
   tmCurr.GetLocalTime();
   FILETIME ftCur;
   BCCL::Convert( tmCurr, ftCur );
   if( m_ftSunRise <= ftCur && ftCur <= m_ftSunSet ) {
      m_nState |= DayNightDetectionState_DayTime;
      m_nState &= ~DayNightDetectionState_NightTime;
   } else {
      m_nState |= DayNightDetectionState_NightTime;
      m_nState &= ~DayNightDetectionState_DayTime;
   }
}

BOOL DayNightDetection::Read( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "DayNightDetection" ) ) {
      xmlNode.Attribute( TYPE_ID( BOOL ), "UseUVAvg", &m_bUseUVAvg );
      xmlNode.Attribute( TYPE_ID( float ), "UVDayTh", &m_fUVDayThreshold );
      xmlNode.Attribute( TYPE_ID( float ), "UVNightTh", &m_fUVNightThreshold );
      xmlNode.End();
   }
   return TRUE;
}

BOOL DayNightDetection::Write( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "DayNightDetection" ) ) {
      xmlNode.Attribute( TYPE_ID( BOOL ), "UseUVAvg", &m_bUseUVAvg );
      xmlNode.Attribute( TYPE_ID( float ), "UVDayTh", &m_fUVDayThreshold );
      xmlNode.Attribute( TYPE_ID( float ), "UVNightTh", &m_fUVNightThreshold );
      xmlNode.End();
   }
   return TRUE;
}
#endif

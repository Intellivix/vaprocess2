﻿#pragma once

#include <mutex>
#include "VCM_Define.h"
#include "CameraProductAndItsProperties.h"
#include "PTZArea.h"
#include "ARFeature.h"

class CaptureBoardProperty;
class CVideoChannelManager;
class CIPCamStream;

///////////////////////////////////////////////////////////////////////////////
//
// Class: CTiltAngle2IRLight
//
///////////////////////////////////////////////////////////////////////////////

class CTiltAngle2IRLight {
public:
   float m_fTiltAngle;
   int m_nIRLight;

public:
   CTiltAngle2IRLight();

public:
   std::string GetItemString( int nItemIdx );
   void CheckRange();
   int Read( CXMLIO* pIO );
   int Write( CXMLIO* pIO );
};

// (mkjang-140612)
///////////////////////////////////////////////////////////////////////////////
//
// Class: CZoom2IRLight
//
///////////////////////////////////////////////////////////////////////////////

class CZoom2IRLight {
public:
   float m_fZoom;
   int m_nIRLight;

public:
   CZoom2IRLight();

public:
   std::string GetItemString( int nItemIdx );
   void CheckRange();
   int Read( CXMLIO* pIO );
   int Write( CXMLIO* pIO );
};

///////////////////////////////////////////////////////////////////////////////
//
// Enumeration: IRMode
//
///////////////////////////////////////////////////////////////////////////////

enum IRMode {
   IRMode_PTZAuto                                   = 0, // PTZ가 IR을 자동으로 제어하는 모드.
   IRMode_Always_DayMode                            = 1, // 항상 Day Mode
   IRMode_Always_NightMode_IROff                    = 2, // 항상 Night Mode & IR Off
   IRMode_Always_NightMode_IROn                     = 3, // 항상 Night Mode & IR On                   --> IR On   모드
   IRMode_DayTime_DayMode_NightTime_NightMode_IROff = 4, // 낮에는 Day Mode 밤에는 Night Mode & IR Off --> IR Off  모드
   IRMode_DayTime_DayMode_NightTime_NightMode_IROn  = 5, // 낮에는 Day Mode 밤에는 Night Mode & IR On  --> IR Auto 모드
};

// (mkjang-140612)
enum IRMappingMode {
   IRMappingMode_TiltAngle = 0,
   IRMappingMode_Zoom      = 1,
};

const int g_nMaxTilt2IRLightTblSize = 10;
const int g_nMaxZoom2IRLightTblSize = 10; // (mkjang-140612)

///////////////////////////////////////////////////////////////////////////
//
// class: CIRLightCtrlInfo
//
///////////////////////////////////////////////////////////////////////////

class CIRLightCtrlInfo {
public:
   int m_nIRMode;
   int m_nIRMappingMode; // (mkjang-140612)
   int m_nZoomTblSize;
   CTiltAngle2IRLight m_Tilt2IRLightTbl[g_nMaxTilt2IRLightTblSize];
   CZoom2IRLight m_Zoom2IRLightTbl[g_nMaxZoom2IRLightTblSize]; // (mkjang-140612)
public:
   CIRLightCtrlInfo();
   CIRLightCtrlInfo& operator=( const CIRLightCtrlInfo& from );

public:
   void SetDefault();
   int Read( CXMLIO* pIO, BOOL bTableOnly = FALSE );
   int Write( CXMLIO* pIO, BOOL bTableOnly = FALSE );

public:
   int GetCurTiltIRLightValue( float fTilt ); // (mkjang-140612) - 수정
   int GetCurZoomIRLightValue( float fZoom ); // (mkjang-140612)
   float GetCurDThreshold_ROA();
   BOOL CheckRestart( CIRLightCtrlInfo* pIRInfo );
   BOOL IsTheSame( CIRLightCtrlInfo* pIRInfo );
};

////////////////////////////////////////////////////////////////////////////////
//

enum PTZCtrlConnType {
   PTZCtrlConnType_Basic         = 0, // 기본 연결모드.    아날로그 카메라의 경우 PC의 시리얼 포트, IP카메라의 경우 IP카메라 제공 API 사용.
   PTZCtrlConnType_TCPServer     = 1, // Serial To TCP 장비에 연결.
   PTZCtrlConnType_LinkToPTZCtrl = 2, // 다른 PTZ제어에 연결.
   PTZCtrlConnType_OtherIPCamera = 3, // 외부 비디오 서버에 연결
};

///////////////////////////////////////////////////////////////////////////
//
// class: CPTZCtrlInfo
//
///////////////////////////////////////////////////////////////////////////

class CPTZCtrlInfo {
public:
   int m_nPTZControlType;
   int m_nCOMPortNo;
   int m_nCamAddress;
   int m_nBaudRateID;
   int m_nConnectType;
   int m_nTxOnly;
   int m_nProtocolVersion;
   int m_nPTZIdx;
   int m_nBLCLevel;
   int m_nVCID_LikingPTZCtrl;
   BOOL m_bReversePanCoordinate;
   BOOL m_bReverseTiltCoordinate;
   BOOL m_bZoomProportionalJog;
   BOOL m_bZoomTriggeredAutoFocus;

public:
   int m_nPortNo;
   int m_nZoomPortNo;
   std::string m_strIPAddress;
   std::string m_strUserName;
   std::string m_strPassword;

public:
   FPTZVector m_pvCurPos;
   FPTZVector m_pvOpticalAxisOffset;

public:
   // 아래의 변수 값들은 CVideoChannelInfo::m_PTZInfo 와 CVideoChannelInfo::m_IPCamInfo.m_PTZInfo 에서 동일하게 유지해야한다.
   // CopyOnlyCommonValues 함수에 이들 변수들을 복사하는 코드를 추가해야 한다.
   BOOL m_bWiperOn;
   BOOL m_bHeaterOn;
   BOOL m_bThermalCamOn;
   BOOL m_bThermalCamMode;
   BOOL m_bAutoFocusOn;
   // (mkjang-140616)
   BOOL m_bDefogOn;
   BOOL m_bVideoStabilization;

   // IR 설정과 관련된 변수
   // IR 설정은 CVideoChannelInfo::m_PTZInfo 와 CVideoChannelInfo::m_IPCamInfo.m_PTZInfo 에 저장이 된다.
   // 하지만 실제로는 CVideoChannelInfo::m_PTZInfo변수만 사용된다.
   // IR 설정을 CPTZCtrlInfo에서 떼어내어 CVideoChannelInfo로 옮겨야 하는 것이 옳지만
   // 호환성 문제로 이곳에 유지하고 있다.

public:
   BOOL m_bUseOuterIRLightCtrlInfo;
   BOOL m_bIRControlByIntelliVIX; // IntelliVIX가 IR을 제어 (mkjang-141211)
   BOOL m_bUseCameraDayAndNigntMode; // 카메라의 Day/Night 설정을 사용 (mkjang-141211)
   CIRLightCtrlInfo m_IRLightCtrlInfo;

public:
   int m_nZoomModuleType; // PTZ카메라가 별도의 줌모듈을 선택할 수 있는 경우에만 사용된다.

public:
   CPTZCtrlInfo();
   ~CPTZCtrlInfo();
   CPTZCtrlInfo& operator=( const CPTZCtrlInfo& from );

   BOOL CheckChange( CPTZCtrlInfo* pPTZInfo );
   BOOL CheckRestart( CPTZCtrlInfo* pPTZInfo );
   void CopyOnlyCommonValues( CPTZCtrlInfo* pPTZInfo );
   BOOL Read( CXMLIO* pIO );
   BOOL Write( CXMLIO* pIO );
};

///////////////////////////////////////////////////////////////////////////
//
// class: ICameraInfo
//
///////////////////////////////////////////////////////////////////////////

class ICameraInfo {
public:
   ////// 일반 변수 ///////
   int m_bPanorama;
   BOOL m_bRemote;
   BOOL m_bGetEncodedVideoForRec;

   ////// 설정 저장용 변수 ///////
public:
   CameraType m_nCameraType; // enum CameraType 참조 (Analog, IP, etc)
   VideoType m_nVideoType; // enum VideoType 참조 (NTSC, PAL , Prograssive)
   int m_nWidth;
   int m_nHeight;
   float m_fDecodingFPS;
   BOOL m_bHWDecoder; // temp

public:
   ICameraInfo();
   virtual ~ICameraInfo();
   ICameraInfo& operator=( const ICameraInfo& from );
   void SetVideoSize( int nWidth, int nHeight );
   void SetVideoSize( ISize2D videoSize );
   virtual BOOL CheckChange( ICameraInfo* pICFrom );
   virtual BOOL CheckRestart( ICameraInfo* pICFrom );
   virtual BOOL SetInfo( ICameraInfo* pInfo ) = 0;
   virtual BOOL SetVideoSizeFromCameraInfo();

   virtual BOOL Read( CXMLIO* pIO );
   virtual BOOL Write( CXMLIO* pIO );
};

///////////////////////////////////////////////////////////////////////////
//
// class: CAnalogCameraInfo
//
///////////////////////////////////////////////////////////////////////////

class CAnalogCameraInfo : public ICameraInfo {
public:
   ////// 설정 저장용 변수 ///////

   int m_nVCID; // 카메라를 사용하고 있는 비디오 채널 아이디.
   int m_nVideoChannelType; // 카메라를 사용하고 있는 비디오 채널의 타입.

   int m_nChannelNo; // 캡춰보드의 채널번호.
   int m_nCaptureBoardType;
   int m_nCaptureBoardIdx; // 하나 이상의 캡춰보드일 경우의 인덱스를 말함 (현재 사용안함)
   int m_nResolutionIdx;
   int m_nMaxBandWidth; // 현 채널이 처리할 수 있는 최대 대역폭
   float m_fFrameRate;

   int m_nImageProperties[ImagePropertyTypeNum]; // 화면 조정정보를 담고 있는 배열

public:
   ////// 일반 변수 ///////

public:
   CAnalogCameraInfo();
   virtual ~CAnalogCameraInfo();
   CAnalogCameraInfo& operator=( const CAnalogCameraInfo& from );
   CAnalogCameraInfo& GetInfo()
   {
      return *this;
   }
   void SetCBChannelNo( int nChIdx ); // 캡춰보드의 채널번호를 할당한다.
   int GetCBChannelNo();

   virtual BOOL CheckChange( ICameraInfo* pICFrom );
   virtual BOOL CheckRestart( ICameraInfo* pICFrom );
   virtual BOOL SetInfo( ICameraInfo* pInfo );
   virtual BOOL SetVideoSizeFromCameraInfo();

   virtual BOOL Read( CXMLIO* pIO );
   virtual BOOL Write( CXMLIO* pIO );
};

///////////////////////////////////////////////////////////////////////////
//
// Enumeration: IPCamErr
//
///////////////////////////////////////////////////////////////////////////

enum IPCamErr {
   IPCamErr_EmptyIPAddress   = 1,
   IPCamErr_InvalidIPAddress = 2,
   IPCamErr_EmptyCameraType  = 3,
   IPCamErr_EmptyPassword    = 4,
};

enum IPCamStreamType {
   IPCamStreamType_RTSP = 0,
   IPCamStreamType_SDK  = 1,
};

// MS: 최대 스트림 수 정의.
const int MAX_IP_CAMERA_STREAM_NUM = 3;

///////////////////////////////////////////////////////////////////////////
//
// class: CIPCameraInfo
//
///////////////////////////////////////////////////////////////////////////
const std::string NodeName_IPCameraInfo         = _T("IPCamInfo");
const std::string NodeName_IPCameraInfo_Playing = _T("IPCamInfo_Play");

class CIPCameraInfo : public ICameraInfo {
   ////// 설정 저장용 변수 ///////
public:
   int m_nIPCamStreamIdx;
   BOOL m_bUseStream;
   IPCameraType m_nIPCameraType;
   int m_nPortNo; // 포트 번호 (IP 주소와 같이 사용되는..)
   int m_nPTZPortNo; // PTZ제어용 포트번호
   int m_nVideoFormat_Cap; // enum VideoFormat 참조 (MJPG, MPEG-4)
   int m_nVideoChNo; // n 채널 비디오 서버인 경우 특정 채널 번호
   int m_nVideoStreamNo; // 스트림 인덱스임. m_nVideoChNo에 종속적이다.  예) 비디오 채널1번의 스트림2번
   int m_nResolutionIdx;
   int m_nCompression;
   float m_fFPS_Cap;
   std::string m_strIPAddress;
   std::string m_strURL;
   std::string m_strUserName;
   std::string m_strPassword;
   BOOL m_bUsePTZ;
   int m_nCompanyID;
   int m_nIPCameraTypeForExtraPTZCtrl;
   int m_nPTZControlType; // IP 카메라의 PTZ 제어 타입. 설정 저장에 사용되는 변수가 아님. IPCameraProperty::m_nPTZControlType 값이 복사되는 것임.
       // SubPTZControl Type이 정의되어 있는 경우에는 m_PTZInfo.m_nPTZControlType 이 복사되어 질 수 있다.
   CPTZCtrlInfo m_PTZInfo; // IP 카메라와 연결된 PTZ 카메라의 제어 정보.
       // iBOX 나 ICanTek 비디오 서버에서 사용됨.
   long m_nDeviceID; // 장치 아이디 (Innodep 에만 해당)
   std::string m_strDeviceName; // 장치 이름 (Innodep 에만 해당, RealHub에서는 nvt Name에 해당)
   BOOL m_bReceiveMetaDataOnly;

   std::string m_strDeviceUID; // 장치 UID (RealHub에서는 nvt Servce Name에 해당)
   std::string m_strLogicalID; // 논리적 ID
   std::string m_strDeviceGUID; // Device GUID

   int m_nSocketType;
   int m_nUseAudioOut; // 오디오 방송 사용
   int m_nStreamType; // Streaming Type  ==> enum IPCamStreamType
   BOOL m_bNewHttpRule; // 새로운 접속 http API 규약 적용 (현재 영국전자만 적용)

#ifdef __SUPPORT_FISHEYE_CAMERA
   CFisheyeDewarpingSetup m_FisheyeDewarpingSetup;
#endif

   ////// 일반 변수 ///////
   // 일반 변수는 저장되는 변수가 아닙니다.
public:
   BYTE* m_pSrcImgBuff;
   CTimerManager* m_pTimerMgr; // 아이피 카메라 객체에 타이머 객체를 전달하기 위한 변수.
   CIPCamStream* m_pStream;

public:
   CIPCameraInfo();
   virtual ~CIPCameraInfo();
   CIPCameraInfo& operator=( const CIPCameraInfo& from );

public:
   CIPCameraInfo* GetInfo()
   {
      return this;
   }
   int CheckParams();

public:
   virtual BOOL CheckChange( ICameraInfo* pICFrom );
   virtual BOOL CheckRestart( ICameraInfo* pICFrom );
   virtual BOOL SetInfo( ICameraInfo* pInfo );
   virtual BOOL SetVideoSizeFromCameraInfo();

   virtual BOOL Read( const std::string& strNodeName, CXMLIO* pIO );
   virtual BOOL Write( const std::string& strNodeName, CXMLIO* pIO );
};

typedef std::vector<CIPCameraInfo*> IPCameraInfoList;

///////////////////////////////////////////////////////////////////////////
//
// class: CWebCameraInfo
//
///////////////////////////////////////////////////////////////////////////

class CWebCameraInfo : public ICameraInfo {
public:
   ////// 설정 저장용 변수 ///////
   int m_nIndex;
   float m_fFrameRate;
   int m_nWidth;
   int m_nHeight;

public:
   ////// 일반 변수 ///////

public:
   CWebCameraInfo();
   virtual ~CWebCameraInfo();
   CWebCameraInfo& operator=( const CWebCameraInfo& from );
   CWebCameraInfo* GetInfo()
   {
      return this;
   }

   virtual BOOL CheckChange( ICameraInfo* pICFrom );
   virtual BOOL CheckRestart( ICameraInfo* pICFrom );
   virtual BOOL Read( CXMLIO* pIO );
   virtual BOOL Write( CXMLIO* pIO );
   virtual BOOL SetInfo( ICameraInfo* pInfo );
};

///////////////////////////////////////////////////////////////////////////
//
// class: CFileCameraInfo
//
///////////////////////////////////////////////////////////////////////////

enum FileCameraType {
   FILE_TYPE_NONE = -1,
   FILE_TYPE_JPEG = 0,
   FILE_TYPE_AVI  = 1,
};

typedef std::pair<int, std::string> FileCameraItem;

class CFileCameraInfo : public ICameraInfo {
public:
   ////// 설정 저장용 변수 ///////
   float m_fFrameRate;
   std::string m_strPathName;
   BOOL m_bSyncVCAFpsToVideoFps;

public:
   ////// 일반 변수 ///////

public:
   CFileCameraInfo();
   virtual ~CFileCameraInfo();
   CFileCameraInfo& operator=( const CFileCameraInfo& from );
   CFileCameraInfo* GetInfo()
   {
      return this;
   }

   virtual BOOL CheckChange( ICameraInfo* pICFrom );
   virtual BOOL CheckRestart( ICameraInfo* pICFrom );
   virtual BOOL Read( CXMLIO* pIO );
   virtual BOOL Write( CXMLIO* pIO );
   virtual BOOL SetInfo( ICameraInfo* pInfo );
};

///////////////////////////////////////////////////////////////////////////
//
// class: CVirtualPTZCameraInfo
//
///////////////////////////////////////////////////////////////////////////

const int m_nVirtualPTZCamera_MaxImageWidth  = 1024;
const int m_nVirtualPTZCamera_MaxImageHeight = 768;
const int m_nVirtualPTZCamera_MinImageWidth  = 128;
const int m_nVirtualPTZCamera_MinImageHeight = 128;

class CVirtualPTZCameraInfo : public ICameraInfo {
public:
   ////// 설정 저장용 변수 ///////
   int m_nVCID_SourceCamera;
   int m_nStreamIdx;
   BOOL m_bKeepAspectRatio;
   float m_fAspectRatio_X;
   float m_fAspectRatio_Y;
   float m_fMaxZoom;
   float m_fSourceVideoReductionFactorOfZoom1x;

public:
   ////// 일반 변수 ///////
   float m_fFrameRate;

public:
   CVirtualPTZCameraInfo();
   virtual ~CVirtualPTZCameraInfo();
   CVirtualPTZCameraInfo& operator=( const CVirtualPTZCameraInfo& from );
   CVirtualPTZCameraInfo* GetInfo()
   {
      return this;
   }

   virtual BOOL CheckChange( ICameraInfo* pICFrom );
   virtual BOOL CheckRestart( ICameraInfo* pICFrom );
   virtual BOOL Read( CXMLIO* pIO );
   virtual BOOL Write( CXMLIO* pIO );
   virtual BOOL SetInfo( ICameraInfo* pInfo );
};

///////////////////////////////////////////////////////////////////////////
//
// Enumeration: PanoramaCamErr
//
///////////////////////////////////////////////////////////////////////////

enum PanoramaCamErr {
   PanoramaCamErr_InputCameraNumber = 1,
   PanoramaCamErr_EmptyPVTPath      = 2,
   PanoramaCamErr_NotExistPVTFile   = 3,
   PanoramaCamErr_PVTFileReadErr    = 4,
};

///////////////////////////////////////////////////////////////////////////
//
// Class: CPanoramaCameraInfo
//
///////////////////////////////////////////////////////////////////////////

class CPanoramaCameraInfo : public ICameraInfo {
public:
   ////// 일반 변수 ///////
   int m_nInputCamNum; // 파노라마 카메라의 입력채널 수
   ISize2D m_InputImgSize; // 각 입력 카메라의 공통 이미지 해상도

public:
   ////// 설정 저장용 변수 ///////
   int m_nInputCamInfoNum; // 파노라마 카메라에 저장된 정보의 수
   float m_fFrameRate; // 모든 입력카메라들의 공통 프레임 비율임
   BOOL m_bUseBC;
   int m_nBCBaseChNo;
   std::string m_strPVTPath;
   std::string m_strVCTPath;
   std::vector<ICameraInfo*> m_InputCamInfos;

public:
   CPanoramaCameraInfo();
   virtual ~CPanoramaCameraInfo();
   CPanoramaCameraInfo& operator=( const CPanoramaCameraInfo& from );

   virtual BOOL Read( CXMLIO* pIO );
   virtual BOOL Write( CXMLIO* pIO );
   virtual BOOL CheckChange( ICameraInfo* pICFrom );
   virtual BOOL CheckRestart( ICameraInfo* pICFrom );
   virtual BOOL SetInfo( ICameraInfo* pInfo );
   void AddCamera( ICameraInfo* pCamInfo );
   void DeleteCamera( ICameraInfo* pDeleteCamInfo ); // 리스트에서 제거할 때 메모리도 해제
   void RemoveAllCameras();
   int SetVideoSizeFromCameraInfo();
   BOOL GetAnalogCameraChIdxs( std::vector<int>& chIdxList );
   UINT64 GetAnalogCameraChFlag();
   int CheckParams();
};

///////////////////////////////////////////////////////////////////////////
//
// Enumeration: PTZAreaType
//
///////////////////////////////////////////////////////////////////////////

enum PTZAreaType {
   PTZAreaType_PrivacyZone           = 0,
   PTZAreaType_TrackingArea          = 1,
   PTZAreaType_NonTrackingArea       = 2,
   PTZAreaType_MutualAsistanceArea   = 3,
   PTZAreaType_FloodLightArea        = 4,
   PTZAreaType_DynamicBackgroundArea = 5,
};

const int PTZAreaTypeNum = 6;

const int MaxAlarmInputNum             = 32;
const int MaxCaptureBoardAlarmInputNum = 16;

///////////////////////////////////////////////////////////////////////////
//
// Class: CVideoChannelInfo
//
///////////////////////////////////////////////////////////////////////////

class CVideoChannelInfo
#ifdef __SUPPORT_IMAGE_ENHANCEMENT
    : public CVideoStabilizationSetup,
      public CVideoDefoggingSetup
// , public CFisheyeDewarpingSetup
#endif
{
public:
   ////// 일반 변수 ///////

   int m_nWidth;
   int m_nHeight;

   BOOL m_bRemote;
   BOOL m_bRestart; // 캡춰보드 카메라와 관련된 속성이 변경되는 경우 해당 카메라와 연관된 비디오 채널을 재시작이 필요한지를 나타내는 변수
   LPVOID m_pParam; // 비디오 채널의 콜백함수들에게 공통적으로 필요한 파라메터
       // IntelliVIX의 경우 Camera* 값이 입력된다.
   ushort m_nCameraFlag;
   int m_nChannelNo_IVX;
   std::string m_szCameraName;

public:
   ////// 설정 저장용 변수 ///////

   int m_nVCID;
   int m_nVideoChannelType;

   // 프레임 비율.
   float m_fFPS_Cap;
   float m_fFPS_Disp;
   float m_fFPS_IP;
   float m_fFPS_Rec;

   // PTZ 카메라 정보.
   BOOL m_bPTZCamera; // 비디오 채널의 카메라가 PTZ 카메라인지를 나타내는 변수
   CPTZCtrlInfo m_PTZInfo; // IntelliVIX와 시리얼 통신으로 제어하는 PTZ의 제어 정보.

   BOOL m_bFisheyeCam; // 어안렌즈 카메라

   // 영상속성
   BOOL m_bSyncDispWithIP;

   // 카메라 정보 (아날로그, IP, 파일, 파노라마)
   int m_nCBChannelNo; // 캡춰보드의 채널번호, 아날로그 카메라 정보는 캡춰보드 관리자가 저장한다.
   CIPCameraInfo& m_IPCamInfo;
   CIPCameraInfo m_IPCamInfoArr[MAX_IP_CAMERA_STREAM_NUM];
   CFileCameraInfo m_FileCamInfo;
   CWebCameraInfo m_WebCamInfo;
   CVirtualPTZCameraInfo m_VirtualPTZCamInfo;
   CPanoramaCameraInfo m_PanoramaCamInfo;

public:
#ifdef __SUPPORT_PTZ_AREA
   CCriticalSection m_csPTZAreas;
   PTZPolygonalAreaList m_PTZAreas[PTZAreaTypeNum];
#endif

public:
#ifdef __SUPPORT_AUGMENTED_REALITY
   CCriticalSection m_csARFeatureList;
   CARFeatureInfoList m_ARFeatureList; // AR
#endif

public:
   std::string m_szEventAlarmItemData; // 이벤트 알람 아이템 XML 데이터

public:
   /////// 콜백함수 ///////
   LPFUNCTION_SET_IMAGE_PROPERTY m_pfnSetImageProperty;
   LPFUNCTION_GET_IMAGE_PROPERTY m_pfnGetImageProperty;

public:
   CVideoChannelInfo();
   virtual ~CVideoChannelInfo();
   CVideoChannelInfo& operator=( const CVideoChannelInfo& from );
   BOOL Read( CXMLIO* pIO, BOOL bNetwork );
   BOOL Write( CXMLIO* pIO, BOOL bNetwork );

   CVideoChannelInfo& GetVideoChannelInfo()
   {
      return *this;
   }
   BOOL CheckChange( CVideoChannelInfo& from ); // 비디오 채널 속성이 변경되었는지 검사한다.
   BOOL CheckRestart( CVideoChannelInfo& from ); // 비디오 채널을 재시작해야하는지 검사한다.
   int CheckParameters(); // 비디오 채널 속성들을 체크하여 수정한다.
   UINT64 GetAnalogCameraChFlag(); // 현재 사용해야하는 캡춰보드의 채널플레그를 얻는다.
   int GetCameraNum();
   UINT64 GetCameraFlag();
   void SetVideoSize();
   void SetRemote( BOOL bRemote );
};

///////////////////////////////////////////////////////////////////////////
//
// Global Functions
//
///////////////////////////////////////////////////////////////////////////

BOOL CheckIPCamInfos( CIPCameraInfo* pInfo );
BOOL CheckPanoramaInfos( CPanoramaCameraInfo* pInfo, int nVCSetupMode );

﻿#pragma once

#include "CameraInfo.h"

#include "version.h"

class CVideoChannel;
class CCompressImageBufferItem;

//////////////////////////////////////////////////////////////////
//
// class : CameraID 
//
//////////////////////////////////////////////////////////////////
// 아이템의 식별하기 위한 ID를 정의한다. 

class CameraID
{
   static int m_nCameraCount;

public:
   FILETIME  m_ftCreate;         // 아이템이 생성된 시간.
   UINT      m_nID;              // 아이템이 생성된 시간에의해 유일하지 않기 때문에 UINT 아이디를 추가.
public:
   CameraID ( );
   ~CameraID ( ) {  }
   BOOL       operator == (CameraID& CameraID);
   void       SetID   (CameraID& CameraID);
   CameraID&  GetID   (  );

   static int Size   (  );
};

//////////////////////////////////////////////////////////////////
//
// class : ICamera 
//
//////////////////////////////////////////////////////////////////

class ICamera
{
public:
   friend class CVideoChannelManager;

   enum State
   {
      State_Initialize            = 0x00000001,
      State_Capture               = 0x00000002,
      State_SiganlStopCapture     = 0x00000004,
      State_ResolutionChanged     = 0x00000008,
      State_ProductNotMatched     = 0x00000010,
      State_AuthorizationFailed   = 0x00000020,
      State_Connectting           = 0x00000040,
      State_ConnectionFailed      = 0x00000080,
      State_ConnectionOk          = 0x00000100,
      State_VideoSignalNotOK      = 0x00000200,
      State_ChangingVideoBuffer   = 0x00000400,   
      State_AudioInOut            = 0x00000800,   // (xinu_be22)
      State_FrameRateIsMismatched = 0x00001000,
   };

   enum GetImgeLockRet
   {
      GetImgeLockRet_Done                = 0,
      GetImgeLockRet_UnknownError        = 1,
      GetImgeLockRet_EndOfFile           = 2,
      GetImgeLockRet_ImageBufferNotExist = 3,
      GetImgeLockRet_OpenDecoderFailed   = 4,
      GetImgeLockRet_DecompressError     = 5,
   };

public:
   int      m_nPrevState;  // 이전 카메라 상태 (VCM의 카메라 관리 스레드에서 사용한다.)

protected:
   int      m_nICameraIdx;
   CameraID m_ID;
   int      m_nState;      // 카메라 상태

public:
   CVideoChannel* m_pVideoChannel;

   LPVOID   m_pCBCamEventParam;  // CVideoChannel의 포인터임. IP 카메라에서 발생한 이벤트는 모두 CVideoChannel의 큐를 거친 후 콜백 함수가 호출.
   LPVIDEOCHANNEL_CALLBACK_CAMERA_EVENT m_pCBCamEvent;

public:
   ICamera(void);
   virtual ~ICamera(void);

public:
   int       GetState ()      { return m_nState; }
   CameraID* GetCameraID ()   { return &m_ID; }
   BOOL      IsCaptureState ();

   // CB 카메라 전용
   virtual int Initialize (  ) { return (DONE); }
   virtual int GetCBChannelNo (  ) { return -1; } 


   // IP 카메라 전용
   virtual int   Initialize (CIPCameraInfo* pInfo) { return (DONE); }

   // File 카메라 전용
   virtual int   Initialize (CFileCameraInfo* pInfo) { return (DONE); }

   // WebCam 전용
   virtual int   Initialize(CWebCameraInfo* pInfo) { return (DONE); }
              
   // 공통    
   virtual int   Uninitialize (  ) { return (DONE); }
              
   virtual int   BeginCapture (  ) = 0;
   virtual int   EndCapture (  ) = 0;
   virtual int   SignalStopCapture (   ) { return (DONE); };
              
   virtual int   GetImage_Lock (byte **d_buffer) = 0;
   virtual int   GetImage_Unlock () = 0;
              
   virtual int   GetCameraType (  ) = 0;
   virtual int   GetColorSpace (  ) = 0;
              
   virtual BOOL  IsVideoSignalAlive (  );
   virtual BOOL  CheckingVideoSignalAlive (  );

   // 공통 (영상 속성 관련)
   virtual int   SetDefaultImageProperty (int nImagePropertyType);
   virtual int   SetAllImageProperty (  );
   virtual int   SetImageProperty (int nImagePropertyType, int nValue,  int nFlagAuto = FALSE);
   virtual int   GetImageProperty (int nImagePropertyType, int &nValue, int &nFlagAuto);
   virtual int   GetImagePropertyRange (int nImagePropertyType, int& nMin, int& nMax, int& nTic);  
   virtual void  GetSupportedImagePropertyList (std::vector<int>& vtImagePropertyType);                 // 카메라에서 설정가능한 이미지 속성 타입들의 리스트를 리턴
   virtual int   GetImageSize ();
   virtual void  GetCameraDesc (std::string& strDesc);

   virtual int  GetMotionStatus      (BOOL& bMotion);
   virtual int  GetAlarmInputFlags   (   );
   virtual int  GetAlarmInputStatus  (int nChIdx, BOOL& bSignal);
   virtual int  SetRelayOutputStatus (int nChIdx, BOOL  bSignal);
   virtual int  GetRelayOutputStatus (int nChIdx, BOOL& bSignal);

   virtual int  StopTalking (   ) { return (DONE); }
   virtual int  StartTalking (   ) { return (DONE); }
   virtual int  IsEnableAudioDataTransmit (   ) { return (FALSE); }
   
   virtual int  SendPacket (int pkid, FileIO &io_packet) { return (0);}
   
   virtual int   StartAudioInOut () { return (DONE); } // (xinu_be22)
   virtual int   StopAudioInOut () { return (DONE); }
#ifdef TRUEN_CAMERA_AUDIO_ENABLE
   virtual int   StartServerAudioIn () { return (DONE); }
   virtual int   StopServerAudioIn () { return (DONE); }
#endif   
   virtual int   TransferAudio (const BYTE *pBuffer,int nBuffSize) { return (DONE); }

   friend class CVideoChannel;
};



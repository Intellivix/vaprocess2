#pragma once

#include "bccl.h"

///////////////////////////////////////////////////////////////////////////////
//
// Class: CPelcoDProtocol
//
///////////////////////////////////////////////////////////////////////////////

 class CPelcoDProtocol
 
{
   public: // Input Pamameters
      byte m_nAddress;

   public: // Output Parameters
      byte m_aMessage[7];
      
   public:
      CPelcoDProtocol (   );

   protected:
      void MakeMessage (byte byCommand1,byte byCommand2,byte byData1,byte byData2);
   
   public:
      void ContPTZMove  (int nPanSpeed,int nTiltSpeed,int nZoomInOut);
      void SetZoomSpeed (int nZoomSpeed);
      void AbsZoomMove  (int nZoomPosHW);
};

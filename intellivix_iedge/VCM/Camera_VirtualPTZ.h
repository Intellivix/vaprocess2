#pragma once

#ifdef __SUPPORT_PTZ_CAMERA
#ifdef __SUPPORT_VIRTUAL_PTZ_CAMERA

#include "version.h"
#include "Win32CL\\avifile.h"
#include "CameraProductAndItsProperties.h"
#include "ICamera.h"
#include "vacl_thumbcap.h"
using namespace VACL;


class CVideoChannel;
class CVideoChannelManager;

//////////////////////////////////////////////////////////////////////////////////
//
// Enumeration
//
//////////////////////////////////////////////////////////////////////////////////

enum VirtualPTZCameraState
{
   VirtualPTZCameraState_SearchingSourceVideoChannel               = 0x00000001,
   VirtualPTZCameraState_SourceVideoChannelRegisterd               = 0x00000002,
   VirtualPTZCameraState_SourceVideoChannelRemoved                 = 0x00000004,
   VirtualPTZCameraState_ReactivatingForResetingSourceVideoChannel = 0x00000008,

};

//////////////////////////////////////////////////////////////////////////////////
//
// Class : CCamera_VirtualPTZ
//
//////////////////////////////////////////////////////////////////////////////////

class CCamera_VirtualPTZ : public ICamera
{
public:
   int      m_nChannelNo_IVX;
   int      m_nVirtualPTZCamState;
   uint     m_nPrevCheckTime;
   uint     m_nCaptureStartTime;
   float    m_fZoomMagnifyingFactor;
   IPoint2D m_p2CenterPos; 
   ISize2D  m_SrcImageSize;
   BArray1D m_YUY2Img;
   ThumbnailCapture_YUY2  m_Capturer;
   CVirtualPTZCameraInfo* m_pInfo;
   CVideoChannel*         m_pVC;
   CVideoChannel*         m_pVCSrc;
   CVideoChannelManager*  m_pVCM;
   CEventEx               m_evtSourceImageCapture;

   ISize2D                m_nPrevDstSize;
   BArray1D               m_DewarpBuffer;

public:
   CCamera_VirtualPTZ(void);
   virtual ~CCamera_VirtualPTZ(void);

   // Virtual Camera 전용
   virtual int   Initialize (CVirtualPTZCameraInfo* pInfo, CVideoChannel* pVC);
               
   // 공통     
   virtual int   Uninitialize (  ) { return (DONE); }
               
   virtual int   BeginCapture (  );
   virtual int   EndCapture (  );
               
   virtual int   GetImage_Lock (byte **d_buffer);
   virtual int   GetImage_Unlock ();

   virtual int   GetCameraType (  ) { return CameraType_VirtualPTZ; }
   virtual int   GetColorSpace (  );
   virtual int   GetImageSize ();
};

#endif // __SUPPORT_VIRTUAL_PTZ_CAMERA
#endif //__SUPPORT_PTZ_CAMERA
﻿#pragma once

#include "AVCodec.h"

/////////////////////////////////////////////////////////////////////////////////
//
// Variables
//
/////////////////////////////////////////////////////////////////////////////////
/* Global variable hide... and getter function use
extern CMapIDToString _AxisResolutionTypeString; // Axis 카메라의 해상도를 나타내는 스트링
extern CMapIDToString _VideoTypeString;

extern CMapIDToString CaptureBoardTypeString;
extern CMapIDToString VideoChannelTypeString;
extern CMapIDToString ResolutionTypeString;
extern CMapIDToString VideoFormatString;
extern CMapIDToString VideoTypeString;
extern CMapIDToString PTZControlTypeString;
extern CMapIDToString ImagePropertyTypeString;
extern CMapIDToString PTZConnectionTypeString_Analog;
extern CMapIDToString PTZConnectionTypeString_IPCamera;
extern CMapIDToString ZoomModuleTypeString;
*/
/////////////////////////////////////////////////////////////////////////////////
//
// Enumerations
//
/////////////////////////////////////////////////////////////////////////////////

enum VideoType {
   VideoType_None = 0, // Prograssive 카메라인 경우 None에 해당한다.
   VideoType_NTSC = 1,
   VideoType_PAL  = 2,
};

// Video Channel Type
const int VideoChannelTypeNum = 5;

enum VideoChannelType {
   VideoChannelType_None            = 0,
   VideoChannelType_AnalogCamera    = 1,
   VideoChannelType_IPCamera        = 2,
   VideoChannelType_Panorama        = 3,
   VideoChannelType_VideoFile       = 4,
   VideoChannelType_Unused          = 5,
   VideoChannelType_VirtualPTZ      = 6,
   VideoChannelType_OffLineAnalyze  = 7,
   VideoChannelType_WebCam          = 8,
   VideoChannelType_Hi3516A_Cellinx = 9, // __LIB_Hi3516A_CELLINX
};

enum CameraType {
   CameraType_None            = 0,
   CameraType_AnalogCamera    = 1,
   CameraType_IPCamera        = 2,
   CameraType_Panorama        = 3,
   CameraType_VideoFile       = 4,
   CameraType_VirtualPTZ      = 5,
   CameraType_OffLineAnalyze  = 6,
   CameraType_WebCam          = 7,
   CameraType_Hi3156A_Cellinx = 8, // __LIB_Hi3516A_CELLINX
};

// Resolution
const int ResolutionTypeNum = 5;

enum ResolutionType {
   ResolutionType_Size          = 1,
   ResolutionType_QCIF          = 2,
   ResolutionType_CIF           = 3,
   ResolutionType_2CIF          = 4,
   ResolutionType_2CIF_Expanded = 5,
   ResolutionType_4CIF          = 6,
   ResolutionType_720P          = 7,
   ResolutionType_1080P         = 8,
};

// Pixel Format
const int ColorSpaceNum = 3;

enum ColorSpace {
   ColorSpace_RGB  = 1,
   ColorSpace_YUY2 = 2,
   ColorSpace_YV12 = 3,
   ColorSpace_UYVY = 4,
};

// 이미 정의되어있는 값을 변경하지 말것.

enum IPCameraType {
   IPCameraType_None                 = 0,
   IPCameraType_AXIS                 = 10,
   IPCameraType_CryptoTelecom_S152   = 101,
   IPCameraType_IntelliVIX_iBOX      = 201,
   IPCameraType_IVXVideoServer       = 202,
   IPCameraType_ICanServer           = 301,
   IPCameraType_AproMedia            = 401,
   IPCameraType_Truen                = 501,
   IPCameraType_Cellinx_MR904        = 601,
   IPCameraType_IDIS                 = 701,
   IPCameraType_RTSP                 = 801,
   IPCameraType_Linudix              = 901,
   IPCameraType_Omnicast             = 1001, // VMS
   IPCameraType_Cynix                = 1101,
   IPCameraType_Bosch_VIPX1          = 1201,
   IPCameraType_Pelco_EnduraNET5400T = 1301,
   IPCameraType_Sony_IPELA           = 1401,
   IPCameraType_SamsungTechwin       = 1500,
   IPCameraType_Flex_Watch           = 1600,
   IPCameraType_HIKVision            = 1700,
   IPCameraType_HIKVision_RTSP       = 1701,
   IPCameraType_Innodep              = 1800, // VMS
   IPCameraType_RealHub              = 1900, // VMS
   IPCameraType_Dahua                = 2000,
   IPCameraType_Win4net              = 2100,
   IPCameraType_SecurityCenter       = 2200,
   IPCameraType_BestDigital          = 2300,
   IPCameraType_SamsungTechwinXNS    = 2400,
   IPCameraType_YoungKook            = 2600,
   IPCameraType_Panasonic            = 2700,
   IPCameraType_XProtect             = 2800, // VMS
   IPCameraType_FlexWatch            = 2900,
   IPCameraType_FlexWatch_FW1177_DE  = 2901,
   IPCameraType_UDP                  = 3000,
   IPCameraType_UDP_CND_20Z_HO       = 3001,
   IPCameraType_ProbeDigital         = 3100,
   IPCameraType_Visionhitech         = 3200,
   IPCameraType_Hitron               = 3300,
   IPCameraType_Zenotech             = 3400,
   IPCameraType_LG                   = 3500,
   IPCameraType_ACES                 = 3600,
   IPCameraType_HuaWei               = 3700,
   IPCameraType_Vivotek              = 3800,
   IPCameraType_Onvif                = 3900,
   IPCameraType_AproHttp             = 4000,
   IPCameraType_Indigo               = 4100,
   IPCameraType_EOC                  = 4300,
   IPCameraType_Dallmeier            = 4400,
   IPCameraType_Honeywell            = 4600,
   IPCameraType_Oncam_Grandeye       = 4700,
   IPCameraType_Sanmsung_SDS         = 4800,
   IPCameraType_Coax                 = 4900,
   IPCameraType_DooWon               = 5000,
};

enum CompanyID {
   CompanyID_None           = 0,
   CompanyID_AXIS           = 1,
   CompanyID_CryptoTelecom  = 2,
   CompanyID_Illisis        = 3,
   CompanyID_ICanTek        = 4,
   CompanyID_AproMedia      = 5,
   CompanyID_Truen          = 6,
   CompanyID_Cellinx        = 7,
   CompanyID_IDIS           = 8,
   CompanyID_RTSP           = 9,
   CompanyID_Linudix        = 10,
   CompanyID_Omnicast       = 11,
   CompanyID_Cynix          = 12,
   CompanyID_Bosch          = 13,
   CompanyID_Pelco          = 14,
   CompanyID_Sony           = 15,
   CompanyID_Innodep        = 16,
   CompanyID_HIKVision      = 18,
   CompanyID_SamsungTechwin = 19,
   CompanyID_RealHub        = 20,
   CompanyID_Dahua          = 21,
   CompanyID_Win4net        = 22,
   CompanyID_SecurityCenter = 23,
   CompanyID_BestDigital    = 24,
   CompanyID_IMI_Tech       = 25,
   CompanyID_YoungKook      = 26,
   CompanyID_Panasonic      = 27,
   CompanyID_XProtect       = 28,
   CompanyID_FlexWatch      = 29,
   CompanyID_UDP            = 30,
   CompanyID_ProbeDigital   = 31,
   CompanyID_Visionhitech   = 32,
   CompanyID_Hitron         = 33,
   CompanyID_Zenotech       = 34,
   CompanyID_Dongyang       = 35,
   CompanyID_LG             = 36,
   CompanyID_ACES           = 37,
   CompanyID_HuaWei         = 38,
   CompanyID_Vivotek        = 39,
   CompanyID_Onvif          = 40,
   CompanyID_AproHttp       = 41,
   CompanyID_Indigo         = 42,
   CompanyID_SamsungSDS     = 43,
   CompanyID_EOC            = 44,
   CompanyID_Dallmeier      = 45,
   CompanyID_FSNetworks     = 46,
   CompanyID_Honeywell      = 47,
   CompanyID_Oncam          = 49,
   CompanyID_Coax           = 50,
   CompanyID_DooWon         = 51,
};

enum CaptureBoardType {
   CaptureBoardType_OD4120     = 1,
   CaptureBoardType_OD8240     = 2,
   CaptureBoardType_OD16240    = 3,
   CaptureBoardType_OD16480    = 4,
   CaptureBoardType_OD8120     = 5,
   CaptureBoardType_OD16120    = 6,
   CaptureBoardType_OD16120A   = 7,
   CaptureBoardType_OD16120SE  = 8,
   CaptureBoardType_OD16240SE  = 9,
   CaptureBoardType_OD16480SE  = 10,
   CaptureBoardType_OD24720    = 11,
   CaptureBoardType_OD32960    = 12,
   CaptureBoardType_OD4120SE   = 13,
   CaptureBoardType_OS4000A    = 14,
   CaptureBoardType_OD16G      = 15,
   CaptureBoardType_XCAP_400EH = 16,
};

enum BLCMode {
   BLCMode_Off = 0,
   BLCMode_On  = 1
};

enum DayAndNightMode {
   DayAndNightMode_Day   = 0,
   DayAndNightMode_Night = 1,
   DayAndNightMode_Auto  = 2
};

enum WhiteBalanceMode {
   WhiteBalanceMode_Auto    = 0,
   WhiteBalanceMode_Manual  = 1,
   WhiteBalanceMode_Indoor  = 2,
   WhiteBalanceMode_Outdoor = 3
};

enum DefogMode {
   DefogMode_Off    = 0,
   DefogMode_Auto   = 1,
   DefogMode_Manual = 2
};

enum DISMode {
   DISMode_Off = 0,
   DISMode_On  = 1
};

// (mkjang-140618)
enum WideDynamicRangeMode {
   WDRMode_Off = 0,
   WDRMode_On  = 1,
};

// 배열 인덱스에 사용되므로 주의하기 바람.
enum ImagePropertyType {
   ImagePropertyType_Brightness  = 0,
   ImagePropertyType_Contrast    = 1,
   ImagePropertyType_Saturation  = 2,
   ImagePropertyType_SaturationU = 3,
   ImagePropertyType_SaturationV = 4,
   ImagePropertyType_Hue         = 5,
   ImagePropertyType_Sharpness   = 6,
   ImagePropertyType_Exposure    = 7,
   ImagePropertyType_BLC         = 8, // BLC: Back Light Compensation
   ImagePropertyType_AWB         = 9, // AWB: Auto White Balance
   ImagePropertyType_DayAndNight = 10,
   ImagePropertyType_WDR         = 11, // WDR : Wide Dynamic Range (mkjang-140618)
   ImagePropertyType_DNR         = 12, // NDR : Digital Noise Reduction (mkjang-140618)
   ImagePropertyType_Defog       = 13, // Defog (mkjang-151027)
   ImagePropertyType_DefogLevel  = 14, // Defog Level (mkjang-151027)
   ImagePropertyType_DIS         = 15, // DIS : Digital Image Stabilization (mkjang-151027)
   // 중요!!!: 열거형 변수 추가시
   //          ImagePropertyTypeNum 갯수를 업데이트 할 것.
};

const int ImagePropertyTypeNum = 16;

/////////////////////////////////////////////////////////////////////////////////
//
// Class :  ImageProperty
//
/////////////////////////////////////////////////////////////////////////////////

class ImageProperty {
public:
   int m_nImagePropertyType;
   BOOL m_bUse;
   int m_nMin;
   int m_nMax;
   int m_nTic;
   int m_nDefaultValue;

public:
   ImageProperty();
   void SetProperty( int nMin, int nMax, int nTic, int nDefaultValue );
   void GetRange( int& nMin, int& nMax, int& nTic );
};

/////////////////////////////////////////////////////////////////////////////////
//
// Class :  PTZControlProperty
//
/////////////////////////////////////////////////////////////////////////////////

enum PTZControlType {
   PTZControlType_VirtualPTZ     = 5,
   PTZControlType_Pelco_D        = 100,
   PTZControlType_Samsung        = 200,
   PTZControlType_SamsungTechWin = 300,
   PTZControlType_LG             = 400,
   PTZControlType_Honeywell      = 500,
   PTZControlType_BOSCH          = 600,
   PTZControlType_Woonwoo        = 700,
   // CYNIX
   PTZControlType_CYNIX_CU_N22DC = 10000,
   PTZControlType_CYNIX_CU_N23DH = 10001,
   PTZControlType_CYNIX_CU_N26DC = 10002,
   PTZControlType_CYNIX_CV_N22C  = 10003,
   PTZControlType_CYNIX_EAI_N27T = 10004,
   PTZControlType_CYNIX_HA_18H_S = 10005,
   PTZControlType_CYNIX_HA_26H_S = 10006,
   PTZControlType_CYNIX_HC_N20S  = 10007,
   PTZControlType_CYNIX_HC_X18H  = 10008,
   PTZControlType_CYNIX_MM_302   = 10009, // (mkjang-140401)
   PTZControlType_CYNIX_MM_202   = 10010, // (mkjang-140408)
   // Samsung
   PTZControlType_Samsung_C6435 = 20000,
   // Axis
   PTZControlType_Axis_212         = 30000,
   PTZControlType_Axis_213         = 30001,
   PTZControlType_Axis_214         = 30002,
   PTZControlType_Axis_233D        = 30003,
   PTZControlType_Axis_Q6045_Mk_II = 30004,
   // CryptoTelecom
   PTZControlType_CryptoTelecom = 40000,
   // LG
   PTZControlType_LG_LPT_EP551PS = 50000,
   PTZControlType_LG_LND_7210R   = 50001, // (mkjang-140527)
   PTZControlType_LG_LNP_3022    = 50002,
   // Honeywell
   PTZControlType_Honeywell_HSDC_251       = 60000,
   PTZControlType_Honeywell_HSDC_351       = 60001,
   PTZControlType_Honeywell_HISD_2201WE_IR = 60002,
   PTZControlType_Honeywell_HNP_232WI      = 60003,
   // BOSCH
   PTZControlType_BOSCH_AUTODOME_500I = 70000,
   // Illisis
   PTZControlType_LinkingToOtherPTZCtrl = 80000,
   // Hitron
   PTZControlType_Hitron_HF3S36AN = 90000,
   // Yujin
   PTZControlType_Yujin_ETP5000S_33x     = 100000,
   PTZControlType_Yujin_ETP5000S_22x     = 100001,
   PTZControlType_Yujin_ETP5000S_Pelco_D = 100002,
   PTZControlType_Yujin_ETP7000B         = 100003,
   // ICanTek
   PTZControlType_ICanTek = 110000,
   // AproMedia
   PTZControlType_AproMedia = 120000,
   // NIKO
   PTZControlType_NIKO_NSD_S300 = 130000,
   PTZControlType_NIKO_NSD_S360 = 130001,
   // SamsungTechWin
   PTZControlType_SamsungTechWin_SPD_1000   = 140001,
   PTZControlType_SamsungTechWin_SPD_2300   = 140002,
   PTZControlType_SamsungTechWin_SPD_2700   = 140003,
   PTZControlType_SamsungTechWin_SPD_3000   = 140004,
   PTZControlType_SamsungTechWin_SPD_3300   = 140005,
   PTZControlType_SamsungTechWin_SPD_3350   = 140006,
   PTZControlType_SamsungTechWin_SPD_3700   = 140007,
   PTZControlType_SamsungTechWin_SPD_3750   = 140008,
   PTZControlType_SamsungTechWin_SCU_2370   = 140009,
   PTZControlType_SamsungTechWin_SCP_2370   = 140010,
   PTZControlType_SamsungTechWin_SCP_2370RH = 140011,
   PTZControlType_SamsungTechWin_SCP_2373N  = 140012,
   PTZControlType_SamsungTechWin_SCP_2371N  = 140013, // (mkjang-140703)
   // Truen
   PTZControlType_Truen                             = 150000,
   PTZControlType_Truen_TCAM_570_S22FIR             = 150001,
   PTZControlType_Truen_TN_B220CS_Shinwoo_SPT_7080B = 150002, // GOP 해병대 (mkjang-150427)
   PTZControlType_Truen_TCAM_PX220CS                = 150003,
   PTZControlType_Truen_TN_PX220CT                  = 150004,
   PTZControlType_Truen_TN_P4220CTIR                = 150005,
   PTZControlType_Truen_TN_P5230CWIR                = 150006,
   // Cellinx
   PTZControlType_Cellinx = 160000,
   // WonWooEng
   PTZControlType_WonWooEng_EWSJ_E360  = 170000,
   PTZControlType_WonWooEng_WCA_E361NR = 170001,
   PTZControlType_WonWooEng_WTK_E361   = 170002,
   PTZControlType_WonWooEng_EWSJ_363   = 170003,
   PTZControlType_WonWooEng_WTK_M202   = 170004,
   PTZControlType_WonWooEng_WMK_M202   = 170005,
   PTZControlType_WonWooEng_WMK_MM308  = 170006, // (mkjang-150615)
   PTZControlType_WonWooEng_WMK_HS308  = 170007, // (mkjang-151125)

   // IDIS
   PTZControlType_IDIS          = 180000,
   PTZControlType_IDIS_MNC221SH = 180001, // (mkjang-150120)
   // Bosch
   PTZControlType_Bosch_VIPX1 = 190000,
   // Pelco
   PTZControlType_PelcoEndura = 200000, // (xinu_bc15)
   PTZControlTYpe_Pelco_D5230 = 200001, // (mkjang-140626)
   PTZControlTYpe_Pelco_S6220 = 200002, // (qch1004-150605)
   // Xvas
   PTZControlType_Xvas_XV_4120IRD        = 210000,
   PTZControlType_Xvas_XV_4120IRD_13Byte = 210001, // Pelco D와 동일하면서 13 Byte 형식
   // Youtech
   PTZControlType_Youtech_7000WB_1000 = 220000,
   PTZControlType_Youtech_7000WB_750  = 220001,
   PTZControlType_Youtech_9000MR_750  = 220002,
   // Sony
   PTZControlType_SonyIPELA_SNC_RH164_124 = 230000,
   PTZControlType_SonyIPELA_SNC_ER580     = 230001,
   PTZControlType_SonyIPELA_SNC_RX570     = 230002,
   PTZControlType_SonyIPELA_SNC_WR630     = 230003,
   PTZControlType_SonyIPELA_SNC_ER585     = 230004, // (mkjang-150810)
   // Innodep
   PTZControlType_Innodep = 240000,
   // HIK Vision
   PTZControlType_HIKVision_NAIS_M138IR     = 250000,
   PTZControlType_HIKVision_NAIS_M138H      = 250001,
   PTZControlType_HIKVision_NAIS_DS_2DF1    = 250002,
   PTZControlType_HIKVision_NAIS_M200IR     = 250003, // (mkjang-140623)
   PTZControlType_HIKVision_DS_2DF7286_A    = 250004, // (mkjang-140819)
   PTZControlType_HIKVision_DS_2DZ2116      = 250005, // (mkjang-140822)
   PTZControlType_HIKVision_DS_2DE7186_A    = 250006, // (mkjang-150717)
   PTZControlType_HIKVision_DS_2DF8236I_AEL = 250007, // (LEEJH -160308)
   // Prosys
   PTZControlType_Prosys_PPT_350 = 260000,
   // Samsung Techwin iPOLiS
   PTZControlType_SamsungTechWin_iPOLiS                             = 270000,
   PTZControlType_SamsungTechWin_iPOLiS_SNP_3371H                   = 270001,
   PTZControlType_SamsungTechWin_iPOLiS_SNP_5300H                   = 270002,
   PTZControlType_SamsungTechWin_iPOLiS_SNP_6200H                   = 270003,
   PTZControlType_SamsungTechWin_iPOLiS_SNP_6200RH                  = 270004,
   PTZControlType_SamsungTechWin_iPOLiS_SNP_6320H                   = 270005, // (mkjang-150116)
   PTZControlType_SamsungTechWin_iPOLiS_SNP_6320RH                  = 270006, // (mkjang-150831)
   PTZControlType_SamsungTechWin_iPOLiS_SNZ_5200_Convex_CNT_20      = 270007, // GOP 서부 (S1)
   PTZControlType_SamsungTechWin_iPOLiS_SNZ_5200_Shinwoo_SPT_7080B  = 270008, // GOP 근거리 (mkjang-150507)
   PTZControlType_SamsungTechWin_iPOLiS_SNB_7004_Shinwoo_SPT_7080B  = 270009, // 감천항 원거리 (mkjang-150826)
   PTZControlType_SamsungTechWin_iPOLiS_SNB_6004_Shinwoo_SPT_7080B  = 270010, // GP (mkjang-150917)
   PTZControlType_SamsungTechWin_iPOLiS_SNB_6004_Shinwoo_SPT_8080BE = 270011, // GOP (mkjang-151012)
   PTZControlType_SamsungTechWin_iPOLiS_SNB_6005_Shinwoo_SPT_8080BE = 270012, // 감천항 (mkjang-151026)
   PTZControlType_SamsungTechWin_iPOLiS_SNZ_6320_XV_570_IRPT        = 270013,
   PTZControlType_SamsungTechWin_iPOLiS_SNZ_6320_Convex_CNT_20      = 270014, // GOP (mkjang-151105)
   PTZControlType_SamsungTechWin_iPOLiS_SNZ_6320_Shinwoo_SPT_7080B  = 270015, // GOP (mkjang-151110)
   PTZControlType_SamsungTechWin_iPOLiS_SNZ_6320_Shinwoo_SPT_8080BE = 270016, // 어장관리 (LeeJH-160609)
   PTZControlType_HanwhaTechWin_iPOLiS_SNZ_6320_CAMP                = 270017, // 주둔지경계1 (LeeJH-160629)
   PTZControlType_HanwhaTechWin_iPOLiS_SNZ_6320_CAMP2               = 270018, // 주둔지경계2 (LeeJH-160629)

   // Omnicast
   PTZControlType_Omnicast = 280000,
   // RealHub
   PTZControlType_RealHub = 290000, //미구현
   // Dahua
   PTZControlType_Dahua_NS_SD200IR     = 300000,
   PTZControlType_Dahua_NS_SD138       = 300001,
   PTZControlType_Dahua_NS_SD300IR     = 300002,
   PTZControlType_Dahua_NS_SD220IR     = 300003,
   PTZControlType_Dahua_NS_SD230IR     = 300004, // (mkjang-140418)
   PTZControlType_Dahua_NS_SD203IR     = 300005, // (mkjang-140424)
   PTZControlType_Dahua_NS_SD230IRC    = 300006, // (mkjang-140724)
   PTZControlType_Dahua_KADJ_NM2030IRA = 300007, // (mkjang-151015)
   // Win4net
   PTZControlType_Win4net_CLEBO_PM10HT = 310000,
#if defined( __SUPPORT_ROBOMEC )
   // Robomec
   PTZControlType_Robomec_RPT_30 = 320000,
   PTZControlType_Robomec_RPT_10 = 320001,
#endif
   // Support SecurityCenter
   PTZControlType_SecurityCenter = 330000,
   // Convex
   PTZControlType_Convex_CNT_20_MidDistanceCam = 340000, // GOP 중거리 카메라
   // Hitachi
   PTZControlType_Hitachi = 350000,
   // Best Digital
   PTZControlType_BestDigital_BVS_H2020R       = 360000,
   PTZControlType_BestDigital_BNC_5230HR_W     = 360001, // (mkjang-140610)
   PTZControlType_BestDigital_XV8150IRD_2020WB = 360002, // (mkjang-140812)

   // Dongyang
   PTZControlType_Dongyang_DY_IP020HD_SONY    = 380000,
   PTZControlType_Dongyang_DY_IR2020HD_SONY   = 380001,
   PTZControlType_Dongyang_DY_IR2020HD_WONWOO = 380002,
   PTZControlType_Dongyang_DY_IP020HD_WONWOO  = 380003,
   PTZControlType_Dongyang_DY_IR3030HD_WONWOO = 380004,
   PTZControlType_Dongyang_DY_IR2030HL_SONY   = 380005,
   PTZControlType_FSNetworks_FS_IR203H        = 380006,
   PTZControlType_FSNetworks_FS_IR303H        = 380007,
   PTZControlType_FSNetworks_FS_IR306H        = 380008,
   PTZControlType_FSNetworks_FS_IR307H        = 381004,

   // YoungKook Electronics
   PTZControlType_YoungKook_YSDIRMP10  = 390000,
   PTZControlType_YoungKook_YSDIRMP20S = 390001,
   PTZControlType_YoungKook_YSD_PN20MH = 390002, // (jhlee-160317)
   // Panasonic
   PTZControlType_Panasonic_WVSC385  = 400000,
   PTZControlType_Panasonic_WVNS202A = 400001, // (mkjang-140624)
   PTZControlType_Panasonic_WVSC384  = 400002, // (mkjang-140624)
   // Milestone XProtect
   PTZControlType_XProtect = 410000,
   // FlexWatch
   PTZControlType_FlexWatch_FW1177_DEF = 420000,
   // UDP
   PTZControlType_UDP_CND_20ZHO = 430000,
   // Probe Digital
   PTZControlType_ProbeDigital_PRI_H2010 = 440000,
   PTZControlType_ProbeDigital_PRI_H3000 = 440001,
   PTZControlType_ProbeDigital_PRI_H3200 = 440002,
   // Hitron IP Camera
   PTZControlType_Hitron_NFX_12053B1 = 450000,
   PTZControlType_Hitron_NFX_22053H3 = 450001,
   // Zenotec
   PTZControlType_Zenotech_MEGA_IPCAM = 460000,
   // Vision Hitech
   PTZControlType_Visionhitech_BM2TI_IR = 470000,
   // HuaWei
   PTZControlType_HuaWei_IPC6621_Z30_I = 480000,
   // Vivotek
   PTZControlType_Vivotek_SD8363E   = 490000,
   PTZControlType_Vivotek_SD9362EHL = 490001,
   // Support Onvif
   PTZControlType_Onvif = 500000,
   // 장애인 협회
   PTZControlType_EW_IRSD2020HDP = 500001,
   // Apro Http
   PTZControlType_AproHttp = 510000,
   // Dallmeier
   PTZControlType_Dallmeier_DDZ4020_YY_HS_HD = 530000,
   // Doowon
   PTZControlType_Doowon_DMH_5001IR = 540000,
   // !!! 중요 !!! : PTZ제품을 추가할 때 cpp파일에 있는 EntirePTZControlTypeList(int형 배열)에도 꼭 추가하세요.
   // !!! 중요 !!! : 추가한 PTZ제품이 Serial 전송타입일 때 는 SerialPTZControlList(int형 배열)에도 꼭 추가하세요.
};

enum ZoomModuleType {
   ZoomModuleType_HighMagnification = 0,
   ZoomModuleType_ThermalImaging    = 1,
   ZoomModuleType_WideAngle         = 2,
};

extern int SerialPTZControlList[];

const int BaudRates[] = {
   110,
   300,
   600,
   1200,
   2400,
   4800,
   9600,
   14400,
   19200,
   38400,
   56000,
   57600,
   115200,
   128000,
   256000
};

// 어떤 기능을 지원함 (Supported)
const UINT64 PTZCtrlCap_AutoFocusingByPTZCtrl                   = 0x0000000000000001; // 카메라가 자동포커스를 지원하지 않는 경우 PTZ 제어 내부알고리즘을 사용.
const UINT64 PTZCtrlCap_AttachReqAbsPosSupported                = 0x0000000000000002; // PTZ 명령을 1개 이상 동시에 보낼 수 있는 카메라. (거의 없음)
const UINT64 PTZCtrlCap_DoesNotReleasePropPanTiltSpeedOpt       = 0x0000000000000004; // 줌연동 Pan, Tilt 속도 조절을 끌 수 없는 카메라
const UINT64 PTZCtrlCap_ContPTAndAbsZoomMoveCam                 = 0x0000000000000008; // 연속Pan, Tilt 이동과 절대줌위치 이동을 동시에 할 수 있는 카메라
const UINT64 PTZCtrlCap_IRLigtControlSupported                  = 0x0000000000000010; // IR 제어가 가능한 카메라
const UINT64 PTZCtrlCap_PTZPosInitSupported                     = 0x0000000000000020; // PTZ 위치 초기화가 가능한 카메라
const UINT64 PTZCtrlCap_PTZWiperSupported                       = 0x0000000000000040; // 와이퍼 지원
const UINT64 PTZCtrlCap_PTZHeatingWireSupported                 = 0x0000000000000080; // 열선 지원
const UINT64 PTZCtrlCap_PTZThermalCamSupported                  = 0x0000000000000100; // 열화상 카메라 On/Off 지원
const UINT64 PTZCtrlCap_PTZDayAndNightModeSupported             = 0x0000000000000200;
const UINT64 PTZCtrlCap_PTZGetSetFocusAbs                       = 0x0000000000000400; // 포커스 값의 Get/Set 가능
const UINT64 PTZCtrlCap_PTZDefogSupported                       = 0x0000000000000800; // 안개제거
const UINT64 PTZCtrlCap_PTZOneShotAutoFocusSupported            = 0x0000000000001000; // 원샷 오토포커스
const UINT64 PTZCtrlCap_MultiZoomModule                         = 0x0000000000002000; // 하나의 Pan, Tilt 드라이버에
const UINT64 PTZCtrlCap_NeedExtraPTZPortNo                      = 0x0000000000004000; // 별도의 TCP 포트번호로 접속해야하는 PTZ 카메라
const UINT64 PTZCtrlCap_GotoPresetByPTZPreset                   = 0x0000000000008000; // 프리셋 이동을 PTZ 카메라의 프리셋 이동기능으로 동작해야하는 경우. (IntelliVIX는 기본적으로 절대위치 이동으로 프리셋 이동을 한다).
const UINT64 PTZCtrlCap_AutoFocusOnOff                          = 0x0000000000010000; // Auto Focus On/Off 기능
const UINT64 PTZCtrlCap_PTZFanSupported                         = 0x0000000000020000;
const UINT64 PTZCtrlCap_PanTiltCoordinateChangeable             = 0x0000000000040000; // Pan 좌표계가 변경될 소지가 있는 카메라의 경우 본 옵션을 On시켜야 한다.
const UINT64 PTZCtrlCap_ConnectionDependOnStream                = 0x0000000000080000; // 메인스트림 용 연결에 의존적인 PTZ객체인 경우 본 옵션을 On시켜야 한다.
const UINT64 PTZCtrlCap_PTZVideoStabilizationSupported          = 0x0000000000100000; // H/W 비디오 안정화(Video Stabilization) 지원 (mkjang-140616)
const UINT64 PTZCtrlCap_DoNotPeridicallyReqAbsPos               = 0x0000000000200000;
const UINT64 PTZCtrlCap_ZoomTriggeredAutoFocus                  = 0x0000000000400000; // Zoom 이동 후 자동 Auto Focus 동작 (mkjang-150918)
const UINT64 PTZCtrlCap_PresetMoveByUserSpecifiedSpeedSupported = 0x0000000000800000;

// 어떤 기능을 지원하지 않음. (NotSupported)
const UINT64 PTZCtrlCap_SinglePTZTrackNotSupported    = 0x0080000000000000; // 단일 PTZ 추적을 수행할 수 없는 카메라.
const UINT64 PTZCtrlCap_SetAbsPosNotSupported         = 0x0100000000000000;
const UINT64 PTZCtrlCap_GetAbsPosNotSupported         = 0x0200000000000000;
const UINT64 PTZCtrlCap_RealTimeGetAbsPosNotSupported = 0x0400000000000000; // 절대각을 실시간으로 얻을 수 없는 카메라.
const UINT64 PTZCtrlCap_FastAbsPosMoveNotSupported    = 0x0800000000000000; // 절대각 방식의 이동이 최대속도를 낼 수 없는 카메라.
// 보통의 돔카메라의 경우 절대각 이동은 최고 속도를 내는데 그렇지 않는 카메라도 있음.
const UINT64 PTZCtrlCap_BadAbsPosMoveMoveCam  = 0x1000000000000000; // 절대각 이동명령에 문제가 있는 카메라
const UINT64 PTZCtrlCap_DelayedABSPosMoveCam  = 0x2000000000000000;
const UINT64 PTZCtrlCap_ZoomProportionalJog   = 0x4000000000000000; // Zoom Proportional Jog 설정이 가능한 카메라 (mkjang-140425)
const UINT64 PTZCtrlCap_IRLightAreaSupported  = 0x8000000000000000; // IR 라이트 영역 // (mkjang-141211)
const UINT64 PTZCtrlCap_DoNotOpenSetupWebPage = 0x0010000000000000;

const UINT64 PTZCtrlOpt_AllAbsPosNotSupported = PTZCtrlCap_SetAbsPosNotSupported | PTZCtrlCap_GetAbsPosNotSupported | PTZCtrlCap_RealTimeGetAbsPosNotSupported;

const UINT64 g_nSupportPTZOptions_And = // 비디오서버를 통한 제어일 때 And조건일 때만 동작되는 옵션들
    PTZCtrlCap_AutoFocusingByPTZCtrl | PTZCtrlCap_DoesNotReleasePropPanTiltSpeedOpt | PTZCtrlCap_ContPTAndAbsZoomMoveCam;

const UINT64 g_nSupportPTZOptions_Or = // 비디오서버를 통한 제어일 때 Or조건일 때 동작되는 옵션들
    PTZCtrlCap_IRLigtControlSupported | PTZCtrlCap_PTZPosInitSupported | PTZCtrlCap_AttachReqAbsPosSupported | PTZCtrlCap_PTZWiperSupported | PTZCtrlCap_PTZHeatingWireSupported | PTZCtrlCap_PTZThermalCamSupported | PTZCtrlCap_PTZDayAndNightModeSupported | PTZCtrlCap_PTZGetSetFocusAbs | PTZCtrlCap_PTZDefogSupported | PTZCtrlCap_PTZOneShotAutoFocusSupported | PTZCtrlCap_MultiZoomModule | PTZCtrlCap_NeedExtraPTZPortNo | PTZCtrlCap_GotoPresetByPTZPreset | PTZCtrlCap_AutoFocusOnOff | PTZCtrlCap_PanTiltCoordinateChangeable | PTZCtrlCap_PTZVideoStabilizationSupported | PTZCtrlCap_ConnectionDependOnStream | PTZCtrlCap_ZoomProportionalJog | PTZCtrlCap_IRLightAreaSupported | PTZCtrlCap_DoNotPeridicallyReqAbsPos | PTZCtrlCap_PresetMoveByUserSpecifiedSpeedSupported;

const UINT64 g_nNotSupportPTZOptions_Or = PTZCtrlCap_SinglePTZTrackNotSupported | PTZCtrlCap_SetAbsPosNotSupported | PTZCtrlCap_GetAbsPosNotSupported | PTZCtrlCap_RealTimeGetAbsPosNotSupported | PTZCtrlCap_FastAbsPosMoveNotSupported | PTZCtrlCap_BadAbsPosMoveMoveCam | PTZCtrlCap_DelayedABSPosMoveCam | PTZCtrlCap_DoNotOpenSetupWebPage;

class PTZControlProperty {
public:
   int m_nPTZControlType;
   UINT64 m_nCapabilities;
   float m_fPredictionFactor_MSTrack;
   float m_fThermalCameraZoomStepSize;
   int m_nMaxDefogLevel; // H/W Defog의 단계 값, On/Off만 지원 시 0 (mkjang-150325)
       // m_nCapabiliteies에 PTZCtrlCap_PTZDefogSupported가 설정되어 있어야 함
public:
   PTZControlProperty();
};

/////////////////////////////////////////////////////////////////////////////////
//
// Class :  ResolutionItem
//
/////////////////////////////////////////////////////////////////////////////////

class ResolutionItem {
public:
   int m_nResolutionType;
   int m_nWidth;
   int m_nHeight;

   ResolutionItem(){};
   ResolutionItem( int nWidth, int nHeight, int nResolutionType )
   {
      m_nResolutionType = nResolutionType;
      m_nWidth          = nWidth;
      m_nHeight         = nHeight;
   }
};

/////////////////////////////////////////////////////////////////////////////////
//
// Class :  VideoResolutionList
//
/////////////////////////////////////////////////////////////////////////////////

class VideoResolutionList : public std::vector<ResolutionItem> {
public:
   int m_nVideoType;
   BOOL m_bGlobal;
   VideoResolutionList()
   {
      m_bGlobal = FALSE;
   }
};

/////////////////////////////////////////////////////////////////////////////////
//
// Class :  VideoResolutionListMgr
//
/////////////////////////////////////////////////////////////////////////////////

class VideoResolutionListMgr {
public:
   int m_nResolutionIdx_CIF; // CIF 급의 해상도의 인덱스
   std::vector<VideoResolutionList*> m_List; // 각 비디오 타입별로의 해상도 리스트

public:
   ~VideoResolutionListMgr();

   void DeleteAllList();
   void AddResolutionList( VideoResolutionList* pResolutionList ); // 특정 비디오 타입의 해상도 리스트 추가
   void GetSupportedVideoTypeList( std::vector<int>& vtVideoTypes );
   int GetVideoResolutionType( int nVideoType, int nResolutionIdx );
   ISize2D GetVideoRosolution( int nVideoType, int nResolutionIdx );
   ISize2D GetCIFVideoResolution( int nVideoType );
   VideoResolutionList* GetResolutionList( int nVideoType );
   int FindResolutionIdx( int nVideoType, int nWidth, int nHeight );
   int FindResolutionIdx( int nVideoType, int nResolutionType );
   BOOL FindVideoTypeAndResolutionIdx( int& nVideoType, int& nResolutionIdx, int nWidth, int nHeight );
};

/////////////////////////////////////////////////////////////////////////////////
//
// Class :  FrameRateList
//
/////////////////////////////////////////////////////////////////////////////////

class FrameRateList : public std::vector<float> {
public:
   int m_nVideoType;
};

/////////////////////////////////////////////////////////////////////////////////
//
// Class :  FrameRateListMgr
//
/////////////////////////////////////////////////////////////////////////////////

class FrameRateListMgr {
public:
   float m_fDefaultFrameRate;
   std::vector<FrameRateList*> m_List;

public:
   ~FrameRateListMgr();

   void Clear();
   void AddNewFrameRateList( int nVideoType, float* pFrameRateList, int nItems, float fDefaultFrameRate ); // 배열로 입력
   void AddNewFrameRateList( int nVideoType, float fMinFPS, float fMaxFPS, float fTic, float fDefaultFrameRate ); // 범위와 Tic을 입력
   void GetSupportedVideoTypeList( std::vector<int>& vtVideoTypes );
   FrameRateList* GetFrameRateList( int nVideoType );
};

/////////////////////////////////////////////////////////////////////////////////
//
// Class :  CaptureBoardProperty
//
/////////////////////////////////////////////////////////////////////////////////

class CaptureBoardProperty {
public:
   int m_nCaptureBoardType;
   int m_nChannelNum;
   int m_nBitPerPixel; // 픽셀당 비트 수를 말한다. (ex)   YUY2 : 2,   YV12 : 1.5
   int m_nColorSpace; // enum ColorSpace 참조 (RGB, YUY2, YV12)
   int m_nInputNum;
   int m_nOutputNum;
   BOOL m_bStoreImgeProperty; // 캡춰보드가 이미지 속성 값을 저장하는지?
   double m_dfMaxBandWidth; // 최대 프레임 처리양

   ImageProperty m_ImageProperty[ImagePropertyTypeNum];
   VideoResolutionListMgr m_ResolutionListMgr;

public:
   CaptureBoardProperty();
};

/////////////////////////////////////////////////////////////////////////////////
//
// Enumerstions : IP Camera
//
/////////////////////////////////////////////////////////////////////////////////

enum IPCamCaps {
   IPCamCaps_PTZControl                      = 0x00000001, // PTZ 제어가 가능한 카메라
   IPCamCaps_TestIPCam                       = 0x00000002, // IP 카메라 테스트 연결이 가능한 카메라
   IPCamCaps_GetIPCamInfo                    = 0x00000004, // IP 카메라 정보 얻기가 가능한 카메라
   IPCamCaps_DisableVideoProperties          = 0x00000008, // 비디오 속성 관련 UI를 Disable. (비디오 속성을 설정할 수 없는 카메라)
   IPCamCaps_SurpportDirectVideoStreaming    = 0x00000010, // 직접 비디오 스트리밍이 가능한 카메라.
   IPCamCaps_SetCompressionLevelNotSupported = 0x00000020, // 이미지 압축률을 설정할 수 없음.
   IPCamCaps_GetCompressionLevelNotSupported = 0x00000040, // 이미지 압축률을 설정할 수 없음.
   IPCamCaps_SetupPTZContorlInfo             = 0x00000080, // 비디오 서버를 통하여 PTZ 카메라 제어가 가능한지의 여부 (현재 iBOX, ICanTek 지원)
   IPCamCaps_SupportCustomPTZCommand         = 0x00000100, // PTZ 제어 명령을 비디오 서버가 직접 생성하여 PTZ 카메라에 보내지 않고
   // 사용자가 생성한 명령을 전달만 해주는 방식을 지원하는지의 여부. (현재 ICanTek 지원)
   IPCamCaps_SupportTalking                        = 0x00000200, // 오디오 방송 기능을 지원하는지에 대한 옵션 (IntelliVIX -->IP Camera)
   IPCamCaps_SupportAudioDataTransmit              = 0x00000400, // 오디오 데이터 전송기능을 지원하는지에 대한 옵션.
   IPCamCaps_AlarmIODependOnStream                 = 0x00000800, // 알람(디지털)IO를 제어가 스트림 연결에 종속적임. AlarmIO를 사용하는 경우에는 스트림이 항상 연결되어 있어야 한다.
   IPCamCaps_VideoAnalyticsVideoServer             = 0x00001000, // 영상분석을 하는 비디오서버.
   IPCamCaps_SupportSendEventAlarmMessage          = 0x00002000, // 이벤트 알람 정보를 비디오서버를 통하여 전송가능.
   IPCamCaps_SupportSendEventAlarmMsgByOuterServer = 0x00004000, // 별도의 외부연결을 통한 이벤틀 알람 전송가능.
   IPCamCaps_SupportPlayingRecordingVideos         = 0x00008000, // 비디오에서 재생데이터를 끌어오기가 가능한 카메라 (이노뎁)
   IPCamCaps_SupportMultiPTZControl                = 0x00010000, // 2개이상의 PTZ제어포트를 가지는 비디오 서버
   IPCamCaps_SupportCtrolPTZByOuterServer          = 0x00020000, // 외부서버에 의한 PTZ제어
   IPCamCaps_ReceiveVideoStreamByRTSP              = 0x00040000, // RTSP로 비디오를 수신받음.
   IPCamCaps_SupportSameSubModel                   = 0x00080000, // IP 카메라 API방식으로 여러 모델 지원가능한 카메라
   IPCamCaps_NeedExtraPTZPortNo                    = 0x00100000, // 별도의 PTZ제어 포트번호를 지정하는 카메라.
   IPCamCaps_ShowSameManufacturer                  = 0x00200000, // 같은 회사의 제품들만 PTZ 콤보박스에 보임 // (mkjang-140422)
   IPCamCaps_SupportOtherConnectionForPTZControl   = 0x00400000, // 비디오를 받아오기 위한 스트림과 별도로 PTZ제어 위한 연결을 독립적으로 할 수 있는 카메라
   IPCamCaps_SupportMotionDetection                = 0x00800000, // 모션감지 이벤트를 받을 수 있는 카메라.
   IPCamCaps_SupportMultiStream                    = 0x01000000, // 멀티스트림을 지원하는 카메라. 스트림 번호를 선택할 수 있는 콤보박스가 활성화됨.
   IPCamCaps_NeedStreamingTypeChoice               = 0x02000000, // Streaming Type 을 지정해주어야할 필요성이 있는 카메라
   IPCamCaps_PTZPortIsWebPort                      = 0x04000000, // PTZ제어 포트가 웹포트인 카메라 들
};

/////////////////////////////////////////////////////////////////////////////////
//
// IPCameraProperty
//
/////////////////////////////////////////////////////////////////////////////////

class IPCameraProperty {
public:
   int m_nCapabilities;
   int m_nIPCameraType;
   int m_nCompanyID;
   int m_nPTZControlType;
   int m_nColorSpace; // enum ColorSpace 참조 (RGB, YUY2, YV12)
   int m_nVideoPortNum; // 비디오 채널 수를 말함. 4채널 비디오 서버인 경우 4
   int m_nInputNum; // 센서   입력 수
   int m_nOutputNum; // 릴레이 입력 수
   int m_nDefPortNo; // 기본 영상포트
   int m_nDefPTZPortNo; // 기본 제어 포트번호
   int m_nDefZoomPortNo; // 기본 줌 포트 번호 (mkjang-140613)
   std::string m_strDefUserName;
   std::string m_strDefPassword;

   std::string m_strProdName;
   std::string m_strProdNbr[4];
   std::string m_strSetupURL;
   ImageProperty m_ImageCompression;
   ImageProperty m_ImageProperty[ImagePropertyTypeNum];

   std::vector<int> m_VideoFormatList; // 지원되는 비디오 포멧 리스트
   FrameRateListMgr m_FrameRateListMgr; // 프레임비율 리스트 관리자
   VideoResolutionListMgr m_ResolutionListMgr;

public:
   IPCameraProperty();
   BOOL IsSameProductName( const std::string& strProdName );
   void InitDigitalIO( int nInputNum, int nOutputNum );
};

/////////////////////////////////////////////////////////////////////////////////
//
// Class :  PTZControlList
//
/////////////////////////////////////////////////////////////////////////////////

class PTZControlList : public std::vector<PTZControlProperty*> {
public:
   ~PTZControlList();
};

/////////////////////////////////////////////////////////////////////////////////
//
// Class :  CaptureBoardList
//
/////////////////////////////////////////////////////////////////////////////////

class CaptureBoardList : public std::vector<CaptureBoardProperty*> {
public:
   ~CaptureBoardList();
};

/////////////////////////////////////////////////////////////////////////////////
//
// Class :  IPCameraList
//
/////////////////////////////////////////////////////////////////////////////////

class IPCameraList : public std::vector<IPCameraProperty*> {
public:
   ~IPCameraList();
};

/////////////////////////////////////////////////////////////////////////////////
//
// Functions
//
/////////////////////////////////////////////////////////////////////////////////

void InitGlobalProperties(); // 비디오 채널과 관계된 모든 전역 속성 변수들을 초기화 한다.
void InitVCMResources();

PTZControlProperty* GetPTZControlProperty( int nPTZControlType );
CaptureBoardProperty* GetCaptureBoardProporty( int nCaptureBoardType );
IPCameraProperty* GetIPCameraProperty( int nIPCaeraType );
IPCameraProperty* GetIPCameraPropertyOfSamePTZControlGroupe( int nPTZControlType );
IPCameraProperty* PopIPCameraProperty( IPCameraList& ip_cam_list, int nIPCaeraType );
BOOL PushIPCameraProperty( IPCameraList& ip_cam_list, IPCameraProperty* pProperty );
BOOL MoveIPCameraProperty( IPCameraList& list_dest, IPCameraList& list_src, int nIPCameraType );
void GetResolutionTypeString( std::string& strOutput, ResolutionItem& item );
void GetSameManufacturePTZList( int nPTZControlType, std::vector<int>& vtPTZControlTypeList );

// IP 카메라 관련
void GetIPCameraTypeList( std::vector<int>& vtIPCameraList );
void GetIPCameraTypeList( uint nIPCamCapabilities, std::vector<int>& vtIPCameraList );
void GetIPCameraTypeListByCapabilities( IPCamCaps eCaps, std::vector<int>& vtIPCameraList );
std::string GetIPCameraProductName( int nIPCameraType );
int GetIPCameraType( const std::string& strProdName );

VideoPixelFormat GetPixelFormat( int nColorSpace ); // enum ColorSpace --> AVIF_PIXEL_FORMAT
float GetBytePerPixel( int nColorSpace );

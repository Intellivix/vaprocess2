﻿#pragma once

#include <iostream>
#include <thread>
#include <chrono>

// WARNING(yhpark): vacl_panorama.h removed??
//#include "vacl_panorama.h"

#include "ICamera.h"
#include "Camera_IP.h"
#include "VCM_Define.h"
#include "CameraInfo.h"
#include "PTZCtrl.h"
#include "DayNightDetection.h"
#include "CompressImageBuffer.h"
#include "FisheyeDewarping.h"

//using namespace VACL;
//using namespace BCCL;

class CAutoFocusMgr;

const int VCThreadNum = 4;

/////////////////////////////////////////////////////////////////////////////////
//
// Enumerations
//
/////////////////////////////////////////////////////////////////////////////////

enum VC_ThreadType {
   VC_Cap  = 0,
   VC_Disp = 1,
   VC_Rec  = 2,
   VC_IP   = 3,
};

enum VCProcCallbackFuncType {
   VCProcCallbackFuncType_Display  = 0,
   VCProcCallbackFuncType_Recoding = 1,
   VCProcCallbackFuncType_ImgProc  = 2,
};

const int VCProcCallbackFuncType_Num = 3;

enum VCDecodeAndConnectionPolicy {
   VCDecodeAndConnectionPolicy_FullDecoding       = 0, // 항시 연결 & 항시 디코딩
   VCDecodeAndConnectionPolicy_AdaptiveDecoding   = 1, // 항시 연결 & 사용 시에만 디코딩
   VCDecodeAndConnectionPolicy_AdaptiveConnection = 2, // 사용 시에만 연결&디코딩
};

enum VCState {
   VCState_None                            = 0x00000000,
   VCState_Activate                        = 0x00000001, // 비디오 채널이 Activate인 상태
   VCState_SignalDeactivate                = 0x00000002, // 비디오 채널을 Deactive 시키기 위해 신호를 보내는 상태
   VCState_Deactivating                    = 0x00000004,
   VCState_ChangeSetting                   = 0x00000008,
   VCState_FailureOpenPVTFile              = 0x00000010,
   VCState_FailureOpenVCTFile              = 0x00000020,
   VCState_FailureOpenVideoFile            = 0x00000040,
   VCState_FailureOpenSerialPort           = 0x00000080,
   VCState_IncorecctPanoramSourceImageSize = 0x00000100, // 파노라마 입력 영상 크기가 맞지않음
   VCState_Talking                         = 0x00000200,
   VCState_NoiseReductorInitialized        = 0x00000400,
   VCState_AudioInOut                      = 0x00000800, // (xinu_be22)
   VCState_CanNotFindSourceVideoChannel    = 0x00001000,
   VCState_IPCamStreamVideoSizeToBeChecked = 0x00002000,
};

enum VCInnerState {
   VCInnerState_None                        = 0x00000000,
   VCInnerState_PerformVignettingCorrection = 0x00000001,
};

enum VCAlarmCheckMode {
   VCAlarmCheckMode_Default      = 0,
   VCAlarmCheckMode_CaptureBoard = 1,
};

/////////////////////////////////////////////////////////////////////////////////
//
// class CVideoChannel
//
/////////////////////////////////////////////////////////////////////////////////

class CVideoChannel : public CVideoChannelInfo, public VC_Desc {
public:
   int m_nVCIdx; // (테스트용 변수)
public:
   uint m_nState;

protected:
   uint m_nInnerState;

protected:
   int m_nCameraNum; // 비디오 채널의 실제 생성된 입력카메라 수
   int m_nThreadNum; // 비디오 채널별 스레드 수
   int m_nThreadFlag; // 현재 실행되고 있는 스레드 종류를 담는 변수, enum VC_ThreadType 참조
   int m_nCurExecThreadFlag;
   CCriticalSection m_csCurExecThreadFlag;
   UINT64 m_nCountIP;
   UINT64 m_nCountCap;
   UINT64 m_nCountRec;
   UINT64 m_nCountDisp;

   ushort m_nVideoSignalFlag; // "비디오 신호 있음" 플레그
   ushort m_nNoVideoSignalFlag; // "비디오 신호 없음" 플레그
   ushort m_nConnectingChFlag; // "연결 시도 중" 플레그
   ushort m_nConnectionFailedChFlag; // "연결 실패" 플레그
   ushort m_nAuthorizationFailedChFlag; // "연결실패 원인이 인증에 있음" 플레그

   ushort m_nPrevVideoSignalFlag;

   BOOL m_bThreadAlive[VCThreadNum];
   BOOL m_bStopThead[VCThreadNum];
   BOOL m_bNoVideoSignal[VCThreadNum];
   BOOL m_bAllSignalOK;
   BOOL m_bSetupTemp; // 비디오 채널을 임시 저장용 변수로 사용할지의 여부

public:
   byte* m_pYUY2Buff; // YUY2 포멧의 Color영상은 BArray1D에 저장되기 때문에 byte* 타입으로 얻어온다.
   BArray1D m_YUY2Buff; // YUY2 포멧의 컬러이미지 정보를 담는 BArray1D 타입의 버퍼
   CCriticalSection m_csSrcImage;
   std::vector<ICamera*> m_Cameras;
   BOOL m_bSignalToVertualPTZCameras[MAX_IP_CAMERA_STREAM_NUM];
protected:
   CCriticalSection m_csTimestamp;
   FILETIME m_ftTimestamp;
public:
   FILETIME GetTimeStamp ();

   FILETIME m_ftLastImageProcessTime;
   double m_dfNormalPlayTime;
   double m_dfImageProcessDelayTime;
   double m_dfAvgImageProcessDelayTime;
   BOOL m_bEndOfFile;

#ifdef __SUPPORT_PTZ_CAMERA
public:
   int m_nPTZControlOpenMode;
   CPTZCtrl* m_pPTZCtrl;
#endif

public:
   CCriticalSection m_csVirtualPTZCameras;
   std::deque<CVideoChannel*> m_VirtualPTZCameras;

public:
   CCriticalSection m_csCameras;
   CCriticalSection m_csVideoChannel;
#ifdef __SUPPORT_DAYNIGHT_DETECTION
   DayNightDetection m_DayNightDetector;
#endif

public:
   CEventEx m_evtVCCameraEvent;
   CVCCameraEventCallbackParamQueue m_VCCameraEventCBParamQueue; // VCM::CVideoChannel에서 상태변화 값에 대한 큐.

public:
   CDisplayImageBufferQueue m_DisplayingImageQueue;

   // Panorama 카메라 관련
protected:
#ifdef __SUPPORT_PANORAMIC_CAMERA
   ISize2D CapImgSize; // 입력(캡쳐)영상의 크기
   BArray2D CapturedImages; // 파노라마 영상을 합성하는데 사용되는 소스 영상들.
   VACL::ImageBrightnessCorrection IBCorrector; //
   PanVideoFrameCompositionTable PVFCompositor; // 실시간 파노라마 영상의 생성을 담당한다.
   LensVignettingCorrectionTable LVCorrector; // Lens Vignetting Correction을 수행한다.
#endif
   // 스레드간 동기화
protected:
   float m_fCurFrmRate[VCThreadNum];
   float m_fCurFrmRateSD[VCThreadNum];
   CMMTimer* m_pTimer[VCThreadNum]; // 타이머
   CCriticalSection m_csVCThread;
   std::thread m_VCThread[VCThreadNum];

public:
   CVideoChannel( void );
   ~CVideoChannel( void );

   // 비디오 채널 초기화 순서: Create -> Initialize -> Activate
   // 비디오 채널 해제   순서: Deactivate -> Uninitialize
protected:
   // Create 와 Destroy 는 VideoChannelManager에 의해 호출된다
   void Create( VC_Desc& desc );
   void Destroy();

public:
   int Initialize(); // 비디오 채널 정보가 변경된 경우 Initialize를 호출해주어야 한다.
   void Initialize( CVideoChannelInfo& newInfo );
   void Activate( int nThreadFlags = 0xFF ); // 실제 캡춰가 수행되도록 Activate한다.
   void Deactivate();
   void SignalDeactivate();
   void UpdateCameraFlag();

#ifdef __SUPPORT_PTZ_CAMERA
#ifdef __SUPPORT_VIRTUAL_PTZ_CAMERA
protected:
   void ResetVirtualPTZCamerasOnDeactivate();
   void ReleaseLinkedPTZControlPointers();
   void RemoveVirtualPTZCamerasOnDestroy();
   void SignalCaptureEventToVerturalPTZCameras();
   void UpdateSourceStreamIdxsForVirtualPTZCamers();
#endif // __SUPPORT_VIRTUAL_PTZ_CAMERA
#endif // __SUPPORT_PTZ_CAMERA

   void DayNightDetectProc();

   // MS
   int ActivateThread( int nThreadFlags = 0xFF );
   int DeactivateThread( int nThreadFlags = 0xFF );

public:
   ICamera* GetICamera( int nChIdx );
   CCamera_IP* GetIPCameraByStreamIdx( int nStreamIdx );

   int SetDecodingFPS( float fFPS );
   int GetCamState( int nChIdx );
   UINT64 GetImageProcessCount()
   {
      return m_nCountIP;
   }
   UINT64 GetCaptureCount()
   {
      return m_nCountCap;
   }
   float GetFrameRate( int nVCProcCallbackFuncType );
   uint GetPlayTime();
   float GetRealTimeFrameRate( int nVCProcCallbackFuncType );
   void SetFrameRate( int nVCProcCallbackFuncType, float fFrameRate );
   float GetReceiveFrameRate();
   float GetDisplayBitrate();

   void GetImage_Lock( byte** ppYUY2Buff );
   void GetImage_Unlock();
   void GetImage( byte** pYUY2Buff );
   void GetImage_Timestamp( FILETIME& ftTimestamp ); // MS: 타임 스템프도 스트림 별로 얻는 함수가 필요하다.

   ushort GetVideoSignalFlag();
   ushort GetNoVideoSignalFlag();
   ushort GetConnectingChannelFlag();
   ushort GetConnectionFailedChannelFlag();
   ushort GetAuthorizationFailedChannelFlag();

   BOOL IsAllSignalOK();
   BOOL IsAutoFocusingCamera();
   float GetCaptureFrameRate();
   BOOL IsICantekVideoServer();
   void SetSetupTemp( BOOL bSetupTemp ); // 비디오 채널이 임시 저장용 변수로 사용될 때 호출
   BOOL Read( CXMLIO* pIO, BOOL bNetwork );
   BOOL Write( CXMLIO* pIO, BOOL bNetwork );

#ifdef __SUPPORT_PTZ_CAMERA
public:
   int GetPTZControlTypeOfPTZCmd();
   CPTZCtrl* GetPTZControl();

   void SetPTZControlOpenMode( int nPTZControlOpenMode = PTZCtrlOpenMode_Normal );
   int CreatePTZControl( int nPTZControlOpenMode = PTZCtrlOpenMode_Normal );
   int DeletePTZControl( int nPTZControlOpenMode = PTZCtrlOpenMode_Normal );
   void CopyPTZInfoToPTZCtrl();
   void PTZAreaProc();
   void UpdatePTZAreaCoordinate();
#endif

protected:
   int CreateCameras();
   void DeleteCameras();
   int LoadPVTFiles();

   void Proc_Capture();
   void Proc_Display();
   void Proc_Record();
   void Proc_ImgProcess();

   void DisplayOneFrame_CaptureProc( CCamera_IP* pIPCamera_Disp, ISize2D& s2CurrImgSize_Disp, int& nCurrDispStreamIndex, ISize2D& s2PrevImgSize_Disp, int& nPrevDispStreamIndex );
   void DisplayOneFrame_DispProc( ISize2D& s2CurrImgSize_Disp, ISize2D& s2PrevImgSize_Disp );

   static UINT Thread_Capture( LPVOID pVoid );
   static UINT Thread_Display( LPVOID pVoid );
   static UINT Thread_Record( LPVOID pVoid );
   static UINT Thread_ImgProcess( LPVOID pVoid );

   void OnReceiveEncodeFrame( const VC_PARAM_ENCODE& param );
   static BOOL CallbackReceiveEncodeFrame( const VC_PARAM_ENCODE& param );
   void OnReceiveDecodeFrame( const VC_PARAM_STREAM& param );
   static BOOL CallbackReceiveDecodeFrame( const VC_PARAM_STREAM& param );

   static void CallbackCameraEvent( const VC_PARAM_CAMERA_EVENT& param );
   void AddCameraEventToQueue( const VC_PARAM_CAMERA_EVENT& param );

public:
   ImageProperty* GetImageProperties( int nCamIdx );

   int SetDefaultImageProperty( int nCamIdx, int nImagePropertyType ); // 이미지 속성값을 기본값으로 한다.
   int SetAllImageProperty( int nCamIdx );
   int SetImageProperty( int nCamIdx, int nImagePropertyType, int nValue, int nFlagAuto = FALSE );
   int GetImageProperty( int nCamIdx, int nImagePropertyType, int& nValue, int& nFlagAuto );
   void GetDefaultImageProperty( int nCamIdx, int nImagePropertyType, int& nValue );
   void GetImagePropertyRange( int nImagePropertyType, int nCamIdx, int& nMin, int& nMax, int& nTic );
   void GetSupportedImagePropertyList( int nCamIdx, std::vector<int>& vtImagePropertyType );
   void GetInputCameraDesc( int nCamIdx, std::string& strDesc ); // 입력카메라를 설명하는 스트링을 얻는다.
   UINT64 CheckVideoSignalChange();
   uint GetSensorSignalFlags();
   void SetCenterImagePos( IPoint2D p2CenterPos );
#ifdef __SUPPORT_IMAGE_ENHANCEMENT
   void SetDefog( BOOL bDefogOn );
   BOOL GetDefog();
   void SetDefogLevel( int nDefogLevel );
   void SetSoftDefog( BOOL bDefogOn );
   void SetVideoStabilization( BOOL bVideoStabilizationOn );
   BOOL GetVideoStabilization();
#endif

   // Audio
   int StartAudioInOut();
   int StopAudioInOut(); // (xinu_be22)
   int SendAudioInOut( const BYTE* pDecodeBuff, int nDecodeBuffLen );
   int StopTalking();
   int StartTalking();
   int IsTaking();
   int IsSupportAudioDataTransmit();
   int IsEnableAudioDataTransmit();

   BOOL IsSupportDirectVideoStreaming( int nStreamIdx = 0 );
   BOOL IsIVXVideoServer();
   BOOL IsVideoServerType( int nVideoServerType );
   BOOL IsVideoAnalyticsVideoServer();
   BOOL IsSupportSendEventAlarmToVideoServer();

public:
   BOOL m_bDrawPrivacyZone;
   FPTZVector m_pvTrkObjPos;
   FBox2D m_b2CurTrkBox;
   ISize2D m_s2ProcImgSize;
#ifdef __SUPPORT_PTZ_AREA
   PTZPolygonalArea* m_pCurMutualAssistanceArea;
#endif
   void SetTrkObjBox( FBox2D b2TrkBox, ISize2D s2SrcImgSize );
   void SetTrkObjPTZPos( FPTZVector pvTrkObjPos );
   void SetDrawPrivacyZone( BOOL bDraw );
   void DrawPrivacyZone();
   void GetTrackingAreaMap( GImage& trackingAreaMap, GImage& dynamicBackgroundAreaMap, BOOL bAdditionalAreas = FALSE );
   void CheckMutualAssistanceAreaProc( FBox2D b2TrkBox, ISize2D s2SrcImgSize );
   void CheckFloodLightAreaProc();

public:
   int GetMotionStatus( BOOL& bMotion );
   int GetAlarmInputStatus( int& nChIdx, BOOL& bSignal );
   int SetRelayOutputStatus( int nChIdx, BOOL bSignal );
   int GetRelayOutputStatus( int nChIdx, BOOL& bSignal );

   UINT64 GetIPCount();

public:
   friend class CVideoChannelManager;
   friend class CCamera_VirtualPTZ;

   /////////////////////////////////////////////////////////////
   // Multi Streaming
protected:
   int m_nVCDecodeAndConnectionPolicy; // 디코드 & 연결 정책. enum VCDecodeAndConnectionPolicy 참고
   BOOL m_bUseOfStreamIdxInEncodeCallBackFunc[MAX_IP_CAMERA_STREAM_NUM]; // 인코드 콜백 사용 여부
   BOOL m_bUseOfStreamIdxInDecodeCallBackFunc[MAX_IP_CAMERA_STREAM_NUM]; // 디코드 콜백 사용 여부
   BOOL m_bUseProcCallbackFunc[VCProcCallbackFuncType_Num]; // 프로세싱(분석,녹화,출력) 콜백 사용 여부
   int m_nDecodeStreamIndexOnProcCallbackFunc[VCProcCallbackFuncType_Num]; // 프로세싱 콜백에 사용할 스트림 인덱스
   BOOL m_bSignalToExecProcCallbackFunc[VCProcCallbackFuncType_Num]; // 프로세싱 콜백에 사용할 스트림 인덱스
   ISize2D m_PrevDecodeVideoSize[MAX_IP_CAMERA_STREAM_NUM];

public:
   BOOL IsVideoSignalAlive( int nStreamIdx );
   void GetVideoBuff( int nStreamIdx, byte** ppYUY2Buff, int& nWidth, int& nHeight );
   ISize2D GetVideoSize( int nStreamIdx );
   float GetVideoFrameRate( int nStreamIdx );
   void GetAvailableStreamIndex( std::vector<int>& vtAvailableStreamIndexs );

   int GetStreamIdxOfMostSimillarInSize( std::vector<int>& vtStreamIdxs, const ISize2D& imgSize, float fDispQuality = 0.5f );
   //      스트림 리스트(vtStreamIdxs) 중에서 imgSize 크기와 가까운 영상의 인덱스를 리턴한다.
   //      fDispQuality 가 1.0에 가까울수록 큰 영상의 스트림 인덱스를 얻는다.
   int GetStreamIdxOfMostSimillarInSize( const ISize2D& imgSize, float fDispQuality = 0.5f );
   //      IP카메라의 스트림들 중에서 가장 크기가 유사한 스트림 인덱스를 얻는다.
   int GetStreamIdxOfMostSimillarInSizeOnALive( const ISize2D& imgSize, float fDispQuality = 0.5f );
   //      IP카메라의 스트림들 중에서 가장 크기가 유사한 스트림 인덱스를 얻는다.
   int GetStreamIdxOfTheLargestVideoSizeOnLive();
   //      현재 연결되어 있는 스트림 중에서 가장 큰 비디오사이즈의 스트림을 얻는다.
   int GetStreamIdxOfTheLargestVideoSizeInDecoding();
   //      현재 디코딩 하고있는 스트림 중에서 가장 큰 비디오사이즈의 스트림을 얻는다.
   int GetStreamIdxOfTheSmallestVideoSizeInDecoding();
   //      현재 디코딩 하고있는 스트림 중에서 가장 작은 비디오사이즈의 스트림을 얻는다.

   // 디코딩 및 연결 정책 관련 함수
   void SetDecodeAndConnectionPolicy( int nVCDecodeAndConnectionPolicy );
   int GetDecodeAndConnectionPolicy();

   // 분석,출력,녹화 처리용 콜백 관련 함수.

   int GetStreamIdxOfProcCallbackFunc( VCProcCallbackFuncType nVCProcCallbackFuncType );
   BOOL SetStreamIdxOfProcCallbackFunc( VCProcCallbackFuncType nVCProcCallbackFuncType, int nStreamIdx );
   //   분석, 출력, 녹화 처리용 콜백 함수에서 어떤 스트림을 수신받을지 결정하는 함수 이다.
   //   이 콜백 함수는 연결 및 디코딩과는 별도로 생성한 스레드에서 호출하기 때문에 이 콜백함수에서
   //   직접 CPU부하가 걸리는 작업을 하여도 무방하다.
   //   nVCProcCallbackFuncType : 프로세싱 콜백 함수의 종류를 나타내는 값.
   //   nStreamIdx              : 해당 콜백함수에서 사용할 스트림 인덱스.

   int GetUseOfProcCallbackFunc( VCProcCallbackFuncType nVCProcCallbackFuncType, BOOL& bUseProcCallbackFunc );
   int SetUseOfProcCallbackFunc( VCProcCallbackFuncType nVCProcCallbackFuncType, BOOL bUseProcCallbackFunc );
   //   프로세싱(분석,저장,출력) 콜백 함수를 사용할지를 결정하는 함수이다.
   //   콜백 함수를 사용하지 않도록 지정하면 해당 콜백을 호출하는 스레드가 실행되지 않는다.

   int GetUseOfStreamIdxInDecodeCallBackFunc( int nStreamIdx, BOOL& bUseStreamIdx );
   int SetUseOfStreamIdxInDecodeCallBackFunc( int nStreamIdx, BOOL bUseStreamIdx );
   // 디코딩된 비디오 데이터 콜백 함수의 사용여부를 결정하는 함수.

   int GetUseOfStreamIdxInEncodeCallBackFunc( int nStreamIdx, BOOL& bUseStreamIdx );
   int SetUseOfStreamIdxInEncodeCallBackFunc( int nStreamIdx, BOOL bUseStreamIdx );
   // 인코딩 비디오 데이터 콜백 함수의 사용/여부를 결정하는 함수.

   BOOL SignalToExecProcCallBackFunction( VCProcCallbackFuncType nVCProcCallbackFuncType );

protected:
   void IPCameraDecodeAndConnectionManageProc();
   //   [중요함수] 인코딩, 디코딩 콜백 사용여부 및 프로세싱 콜백 사용여부에 따라 연결 및 디코딩을 On/Off 시키는 함수이다.

   void CheckStreamUsage( int nStreamIdx, BOOL& bUseStream, BOOL& bDecodeStream );
   //   입력 : nStreamIndex
   //   출력1: bUseStream -> 스트림 사용 여부임.
   //   출력2: bDecodeStream -> 디코딩을 해야하는지 여부.

   BOOL IsStreamIsUsedInAnyCallBackFunc( int nStreamIdx );
   BOOL IsDecodeStream( int nStreamIdx );
   uint GetThreadFlagFromProcCallbackFuncType( VCProcCallbackFuncType nVCProcCallbackFuncType );
   BOOL DetermineCopyingPrevStreamBuffToCurrStreamBuff( int nPrevStreamIndex, int nCurrStreamIndex );
   void CopyPrevStreamBuffToCurrStreamBuff( int nPrevStreamIndex, int nCurrStreamIndex );

   ///////////////////////////////////////////////////////////////////////////////
   // Fisheye Camera
public:
   ISize2D m_PrevVideoSize;
   BOOL m_bFisheyeInit;
   int m_nCylinderWidth;
   int m_nCylinderHeight;
   int m_nDPWidth;
   int m_nDPHeight;
   int m_nFisheyeTgtWidth;
   int m_nFisheyeTgtHeight;
   CCriticalSection m_csDewarp;
#ifdef __SUPPORT_IMAGE_ENHANCEMENT
   CImageEnhancement* m_pImageEnhancer;
#endif
   float m_fPan;
   float m_fTilt;

#ifdef __SUPPORT_FISHEYE_CAMERA
public:
   BOOL IsFisheyeCamera();
   BOOL IsFisheyeInit();
   void FisheyeInit( byte* pFisheyeBuffer, int nWidth, int nHeight );
   void GetPanoramaRect_Fisheye( BOOL bFitVideoBox, CRect rectBox, CRect& rectVideo );
   FisheyeViewType GetFisheyeView();

   // VPTZ
public:
   FISHEYE_INFO m_FisheyeInfo_VPTZ;
   BOOL m_bFisheyeInit_VPTZ;

public:
   void Dewarp_YUY2( byte* pSrcBuffer, int nSrcWidth, int nSrcHeight, byte* pDstBuffer, int nDstWidth, int nDstHeight );
   void SetPtzPosition_Fisheye( FPTZVector Pos, int nHeight );
   void GetPtzPosition_Fisheye( FPTZVector& Pos, int nHeight );
   void GotoAbsPos_Fisheye( FPoint2D Coord, float fZoom );
#endif

public:
   void UpdateARAreaCoordinate( int nWidth, int nHeight );

public:
   void GetVideoFrmRecvCount( int& nVideoFrmRecvCount, int& nVideoKeyFrmRecvCount );

protected:
   uint m_nCountDefog;
   uint m_nCountStabilization;

public:
   BOOL SendSetParameterCommand( char const* parameterName, char const* parameterValue );
   BOOL SendGetParameterCommand( char const* parameterName );
};

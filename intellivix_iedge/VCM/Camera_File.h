#pragma once

#ifdef __SUPPORT_VIDEO_FILE_CAMERA

#include "CameraProductAndItsProperties.h"
#include "ICamera.h"
#include "TestObject.h"
#include "AVCodec.h"

class CCamera_File : public ICamera
{
public:

   enum TYPE_ANALYZE
   {
      OFFLINE = 0,
      SINGLE_STREAMING,
   };

   CTestObject  m_TestObj[50];

   TYPE_ANALYZE     m_emType;

public:
   BOOL              m_bRepeat;
   int               m_nCurFrame;
   uint              m_nPlayTime;
   double            m_dfNormalPlayTime;
   CFileCameraInfo*  m_pInfo;
   BArray1D          m_YUY2Img;
   FFMPEG_FileReader m_FileReader;
   CAVCodec          m_VideoDecoder;
   BGRImage          m_ImgFile;
   FileCameraItem    m_FileCameraItem;   
   
public:
   LPVIDEOCHANNEL_CALLBACK_ENCODE_VIDEO_FRAME  m_pcbReceiveEncodeFrame;      // 인코드 프레임 추출 콜백 함수.

   int  SetFileCameraItem(const FileCameraItem& item);      
   BOOL IsEndAvi();
public:
   CCamera_File(TYPE_ANALYZE emType);
   virtual ~CCamera_File(void);

   // File Camera 전용
   virtual int   Initialize (CFileCameraInfo* pInfo);
               
   // 공통     
   virtual int   Uninitialize (  ) { return (DONE); }
               
   virtual int   BeginCapture (  );
   virtual int   EndCapture (  );
               
   virtual int   GetImage_Lock (byte **d_buffer);
   virtual int   GetImage_Unlock ();

   virtual int   GetCameraType (  ) { return CameraType_VideoFile; }
   virtual int   GetColorSpace (  );
   virtual int   GetImageSize ();
   virtual void  GetCameraDesc (std::string& strDesc);

   virtual BOOL  IsVideoSignalAlive ();
   virtual BOOL  CheckingVideoSignalAlive ();
};

#endif
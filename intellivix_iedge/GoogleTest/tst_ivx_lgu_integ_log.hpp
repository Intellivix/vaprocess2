﻿#ifndef TST_IVX_LGU_INTEG_LOG_HPP
#define TST_IVX_LGU_INTEG_LOG_HPP

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "Logger_LGU.h"
#include "BaseDirManager.h"
#include "Common.h"
#include "MMTimer.h"

#include <thread>

void setLogging_LGU()
{
   MakeDirectory( BaseDirManager::Get().get_base_dir() + "/Log" );
   LGU_INTEG_LOG::Logger::Get().init( BaseDirManager::Get().get_base_dir() + "/Log", "LGU", 1, "log", 5 );

   // NOTE(yhpark): test lgu+ logging.
   LGU_INTEG_LOG::Logger::log( plog::fatal, 0, LGU_INTEG_LOG::LogBody::item( LGU_INTEG_LOG::LogBody::id::IVX_PORCESS_STARTED, "" ) );
   LOGF_( LGU_INTEG_LOG::L ) << LGU_INTEG_LOG::Logger::header( 0 ) << LGU_INTEG_LOG::LogBody::item( LGU_INTEG_LOG::LogBody::id::IVX_PORCESS_STARTED, "" );
}

void do_first_logfile( bool* running )
{
   CMMTimer* pTimer_LGU = CTimerManagerSP()->GetNewTimer( "LGU+ Log" );
   pTimer_LGU->SetMilliSec( 5 * 60 * 1000 ); // 5min*60sec*1000ms
   //pTimer_LGU->SetMilliSec( 1000 );
   pTimer_LGU->StartTimer();
   LOGD << "Timer Created.";

   while( *running ) {

      LOGV << "before wait.";
      pTimer_LGU->Wait();

      LOGI << "LocalSystemProc Event_Timer_LGU_Logging. a new log file created.";
      LGU_INTEG_LOG::Logger::Get().fileAppender()->timerExpired();
      LGU_INTEG_LOG::Logger::log( plog::info, 0, LGU_INTEG_LOG::LogBody::item( LGU_INTEG_LOG::LogBody::id::IVX_LOG_NEW_FILE ) );
   }
}

TEST( ivx_lug_integ_log, main )
{
   CTimerManagerSP::Activate( CTimerManagerSP::Type_Direct );
   CTimerManagerSP TimerManager; // It must be mandatory.

   setLogging_LGU();

   bool running = true;
   std::thread thread_logfile( do_first_logfile, &running );

   int ch = 0;
   while( running ) {
      ch = getchar();
      if( ch == 'q' ) {
         running = false;
      }
   }

   if( thread_logfile.joinable() ) {
      thread_logfile.join();
   }
}

#endif // TST_IVX_LGU_INTEG_LOG_HPP

﻿#include "LogBody_LGU.h"

namespace LGU_INTEG_LOG {

std::map<LogBody::id, std::string> LogBody::names = {
   { LogBody::IVX_PORCESS_STARTED, "IVX_PORCESS_STARTED" },
   { LogBody::IVX_VA_PERFORMANCE_MONITERING, "IVX_VA_PERFORMANCE_MONITERING" },
   { LogBody::IVX_LOG_NEW_FILE, "IVX_LOG_NEW_FILE" },
};

std::string LogBody::item( LogBody::id body_id, std::string value )
{
   return "|" + names[body_id] + "=" + value;
}

void LogBody::writeItem( std::ostringstream& ss, LogBody::id body_id, std::string value )
{
   if( ss.str().size() > 0 ) {
      ss << "|";
   }
   ss << names[body_id] << "=" << value;
}

} // namespace LGU_INTEG_LOG

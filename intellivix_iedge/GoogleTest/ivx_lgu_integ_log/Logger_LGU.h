﻿#ifndef LOGGER_LGU_H
#define LOGGER_LGU_H

#include "bccl_plog.h"
#include "bccl_time.h"
#include "CommonUtil.h"
#include "LogBody_LGU.h"

#include <sstream>

//////////////////////////////////////////////////////////////////////
/// custom implementation for plog
//////////////////////////////////////////////////////////////////////
namespace plog {

////////////////////////////////////////////
/// TimeClusteringFileAppender
////////////////////////////////////////////
template <class Formatter, class Converter = UTF8Converter>
class TimeClusteringFileAppender : public IAppender {
public:
   inline TimeClusteringFileAppender(
       const util::nchar* parentDir,
       const util::nchar* filePrefixName,
       uint fileTrafficInfo,
       const util::nchar* fileExt,
       size_t rollingInterval_min = 5 )
   {
      m_timerInterval_min = rollingInterval_min;
      m_fileSize          = 0;
      m_firstWrite        = true;

      m_parentDir       = parentDir;
      m_filePrefixName  = filePrefixName;
      m_fileTrafficInfo = fileTrafficInfo;
      m_fileExt         = fileExt;
   }

   inline virtual void write( const Record& record )
   {
      util::MutexLock lock( m_mutex );
      m_file.write( Converter::convert( Formatter::format( record ) ) );
   }

   inline void timerExpired()
   {
      util::MutexLock lock( m_mutex );
      m_file.close();
      openLogFile();
      m_firstWrite = true;
   }

private:
   inline util::nstring buildLogDirAndFileName()
   {
      BCCL::Time time;
      time.GetLocalTime();

      util::nstring dirNameExt;
      {
         util::nostringstream ss;
         if( !m_parentDir.empty() ) {
            ss << m_parentDir << "/";
         }

         ss << std::setfill( PLOG_NSTR( '0' ) )
            << std::setw( 4 ) << time.Year
            << std::setw( 2 ) << time.Month
            << std::setw( 2 ) << time.Day;
         dirNameExt = ss.str();
      }

      mkdir( dirNameExt.c_str(), 0775 );

      util::nstring filename;
      {
         util::nostringstream ss;
         ss << m_filePrefixName
            << "."
            << std::setfill( PLOG_NSTR( '0' ) )
            << std::setw( 3 ) << m_fileTrafficInfo
            << "."
            << std::setw( 4 ) << time.Year
            << std::setw( 2 ) << time.Month
            << std::setw( 2 ) << time.Day
            << std::setw( 2 ) << time.Hour
            << std::setw( 2 ) << time.Minute;

         if( !m_fileExt.empty() ) {
            ss << '.' << m_fileExt;
         }
         filename = ss.str();
      }

      return dirNameExt + '/' + filename;
   }

   inline void openLogFile()
   {
      m_fileSize = m_file.open( buildLogDirAndFileName().c_str() );
      if( m_fileSize == 0 ) {
         int bytesWritten = m_file.write( Converter::header( Formatter::header() ) );
         if( bytesWritten > 0 ) {
            m_fileSize += bytesWritten;
         }
      }
   }

private:
   util::Mutex m_mutex;
   util::File m_file;
   off_t m_fileSize;
   util::nstring m_parentDir;
   util::nstring m_filePrefixName;
   uint m_fileTrafficInfo; // ex "001"
   util::nstring m_fileExt;

   size_t m_timerInterval_min;
   bool m_firstWrite;
};

////////////////////////////////////////////
/// EmptyFormatter
////////////////////////////////////////////
class EmptyFormatter {
public:
   inline static util::nstring header()
   {
      return util::nstring();
   }

   inline static util::nstring format( const Record& record )
   {
      util::nostringstream ss;
      ss << record.getMessage() << PLOG_NSTR( "\n" );
      return ss.str();
   }

private:
};

} // namespace plog

//////////////////////////////////////////////////////////////////////
/// Logger_LGU
//////////////////////////////////////////////////////////////////////

namespace LGU_INTEG_LOG {

// requirement for LGU+'s TLO log system, and also for plog's global instance.
enum en_plog_company {
   L = 1,
};

class Logger {
   ///////////////////// Plog_LG::SingleTon /////////////////////
public:
   static inline Logger& Get()
   {
      static Logger m_instance;
      return m_instance;
   }
   // delete copy and move constructors and assign operators. 옵셔널. 개발자의 실수 방지
   Logger( Logger const& ) = delete; // Copy construct
   Logger( Logger&& )      = delete; // Move construct
   Logger& operator=( Logger const& ) = delete; // Copy assign
   Logger& operator=( Logger&& ) = delete; // Move assign
   ///////////////////////////////////////////////////////////////

private:
   Logger();
   ~Logger();

public:
   void init( const std::string& parentDir,
              const plog::util::nchar* filePrefixName,
              uint trafficInfo,
              const plog::util::nchar* fileExt,
              size_t rollingInterval_min = 5 );

   static void log( plog::Severity severity, int retCode, std::string message );

   static std::string header( int retCode );

   plog::TimeClusteringFileAppender<plog::EmptyFormatter>* fileAppender()
   {
      return m_pFileAppender;
   }

private:
   plog::TimeClusteringFileAppender<plog::EmptyFormatter>* m_pFileAppender = nullptr;
};

} // namespace LGU
#endif // LOGGER_LGU_H

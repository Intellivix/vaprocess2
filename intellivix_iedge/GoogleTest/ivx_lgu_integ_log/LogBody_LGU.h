﻿#ifndef LOGBODY_LGU_H
#define LOGBODY_LGU_H

#include <string>
#include <sstream>
#include <map>

namespace LGU_INTEG_LOG {
class LogBody {
public:
   enum id {
      IVX_PORCESS_STARTED,
      IVX_VA_PERFORMANCE_MONITERING,
      IVX_LOG_NEW_FILE,
   };

   static std::string item( LogBody::id body_id, std::string value = "" );

   static void writeItem( std::ostringstream& ss, LogBody::id body_id, std::string value = "" );

private:
   static std::map<LogBody::id, std::string> names;
};

} // namespace LGU

#endif // LOGBODY_LGU_H

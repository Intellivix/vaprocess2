﻿#include "LogHeader_LGU.h"

#include <random>
#include <iomanip>

namespace LGU_INTEG_LOG {
// WARNING(yhpark): sequence should be matched with names and enum id sequence.
std::vector<std::string> LogHeader::names = {
   "SEQ_ID",
   "LOG_TIME",
   "LOG_TYPE",
   "SID",
   "RESULT_CODE",
   "REQ_TIME",
   "RSP_TIME",
   "CLIENT_IP",
   "DEV_INFO",
   "OS_INFO",
   "NW_INFO",
   "SVC_NAME",
   "DEV_MODEL",
   "CARRIER_TYPE"
};

std::string LogHeader::item( id header_id, const std::string& value )
{
   return names[header_id] + "=" + value;
}

void LogHeader::writeItem( std::ostringstream& ss, id header_id, const std::string& value )
{
   if( ss.str().size() > 0 ) {
      ss << "|";
   }
   ss << names[header_id] << "=" << value;
}

std::string LogHeader::random_uint( uint min, uint max, int digit_count )
{
   std::random_device rd;
   std::default_random_engine generator( rd() );
   std::uniform_int_distribution<uint> random_uint( min, max );

   std::stringstream ss;
   ss << std::setfill( '0' ) << std::setw( digit_count ) << random_uint( generator );

   return ss.str();
}

} // namespace LGU_INTEG_LOG

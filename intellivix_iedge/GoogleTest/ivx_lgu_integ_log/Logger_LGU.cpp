﻿#include "Logger_LGU.h"

#include "LogHeader_LGU.h"

//////////////////////////////////////////////////////////////////////
/// Logger_LGU
//////////////////////////////////////////////////////////////////////

namespace LGU_INTEG_LOG {

Logger::Logger()
{

}

Logger::~Logger()
{

}

void Logger::init( const std::string& parentDir, const plog::util::nchar* filePrefixName, uint trafficInfo, const plog::util::nchar* fileExt, size_t rollingInterval_min )
{
   MakeDirectory( parentDir );

   static plog::TimeClusteringFileAppender<plog::EmptyFormatter> lgu_fileAppender(
       parentDir.c_str(), filePrefixName, trafficInfo, fileExt, rollingInterval_min );

   m_pFileAppender = &lgu_fileAppender;
   m_pFileAppender->timerExpired();

   plog::init<L>( plog::debug, &lgu_fileAppender );

   // NOTE(yhpark): skip lgu+ console logging.
   //static plog::IvxColorConsoleAppender<plog::IvxConsoleFormatter> consoleAppender;
   //plog::get<L>()->addAppender( &consoleAppender );
}

void Logger::log( plog::Severity severity, int retCode, std::string message )
{
   LOG_( LGU_INTEG_LOG::L, severity ) << header( retCode ) << message;
}

std::string Logger::header( int retCode )
{
   struct tm time_tm;
   plog::util::Time time_plog;
   plog::util::ftime( &time_plog );
   plog::util::localtime_s( &time_tm, &time_plog.time );

   plog::util::nostringstream ss_date;
   ss_date << std::setfill( PLOG_NSTR( '0' ) )
           << std::setw( 4 ) << time_tm.tm_year + 1900
           << std::setw( 2 ) << time_tm.tm_mon + 1
           << std::setw( 2 ) << time_tm.tm_mday;
   std::string date = ss_date.str();

   plog::util::nostringstream ss_time;
   ss_time << std::setfill( PLOG_NSTR( '0' ) ) << std::setw( 2 ) << time_tm.tm_hour
           << std::setw( 2 ) << time_tm.tm_min
           << std::setw( 2 ) << time_tm.tm_sec;
   std::string time = ss_time.str();

   plog::util::nostringstream ss_millisec;
   ss_millisec << std::setw( 3 ) << time_plog.millitm;
   std::string millisec = ss_millisec.str();

   plog::util::nostringstream ss;

   LogHeader::writeItem( ss, LogHeader::id::SEQ_ID, date + time + millisec + LogHeader::random_uint( 0, 99999999, 8 ) );
   LogHeader::writeItem( ss, LogHeader::id::SID, "010xxxxxxxx" );
   LogHeader::writeItem( ss, LogHeader::id::RESULT_CODE, std::to_string( retCode ) );
   LogHeader::writeItem( ss, LogHeader::id::REQ_TIME, date + time + millisec );
   LogHeader::writeItem( ss, LogHeader::id::RSP_TIME, date + time + millisec );
   LogHeader::writeItem( ss, LogHeader::id::CLIENT_IP, "192.168.0.xxx" );
   LogHeader::writeItem( ss, LogHeader::id::OS_INFO, "Oracle Linux 4.x.x" );
   LogHeader::writeItem( ss, LogHeader::id::NW_INFO, "Wire" );
   LogHeader::writeItem( ss, LogHeader::id::SVC_NAME, "CCTV" );
   LogHeader::writeItem( ss, LogHeader::id::DEV_INFO, "XXX-XXXX" );
   LogHeader::writeItem( ss, LogHeader::id::CARRIER_TYPE, "L" );

   return ss.str();
}

} // namespace LGU_INTEG_LOG

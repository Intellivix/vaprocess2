﻿#ifndef LOGHEADER_LGU_H
#define LOGHEADER_LGU_H

#include <string>
#include <sstream>
#include <vector>

namespace LGU_INTEG_LOG {
class LogHeader {
public:
   // WARNING(yhpark): sequence should be matched with names and enum id sequence.
   enum id {
      SEQ_ID,
      LOG_TIME,
      LOG_TYPE,
      SID,
      RESULT_CODE,
      REQ_TIME,
      RSP_TIME,
      CLIENT_IP,
      DEV_INFO,
      OS_INFO,
      NW_INFO,
      SVC_NAME,
      DEV_MODEL,
      CARRIER_TYPE
   };

   static std::string item( LogHeader::id header_id, const std::string& value = "" );

   static void writeItem( std::ostringstream& ss, LogHeader::id header_id, const std::string& value = "" );

   // TODO(yhpark): To CommonUtil?
   static std::string random_uint( uint min, uint max, int digit_count );

private:
   static std::vector<std::string> names;
};

} // namespace LGU

#endif // LOGHEADER_LGU_H

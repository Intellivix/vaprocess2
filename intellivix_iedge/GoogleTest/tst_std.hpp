﻿#ifndef TST_STD_H
#define TST_STD_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include <cinttypes>
#include <bccl.h>

TEST( std, vector_resize )
{
   using byte = std::vector<unsigned char>;
   byte bData( 100 );
   EXPECT_EQ( bData.size(), 100 );

   bData.resize( 10 );
   EXPECT_EQ( bData.size(), 10 );

   bData.resize( 50 );
   EXPECT_EQ( bData.size(), 50 );
}

#include <sys/time.h>

inline void GetTickCount2()
{
   timeval tv;
   gettimeofday( &tv, NULL );
   ulong _old = static_cast<ulong>( tv.tv_sec * 1000 + tv.tv_usec / 1000 );

   struct timespec tp;
   clock_gettime( CLOCK_MONOTONIC, &tp );
   ulong _new = static_cast<ulong>( tp.tv_sec * 1000.0 + tp.tv_nsec / 1000000 );

   printf( "old[%30lu]\n"
           "new[%30lu]\n"
           "int[%30u]\n"
           "log[%30lu]\n",
           _old, _new,
           std::numeric_limits<uint>::max(),
           std::numeric_limits<ulong>::max() );
}

TEST( std, max_value_recursive )
{
   GetTickCount2();

   uint tmp = 0 - 1; //4294967295

   std::cout << tmp << std::endl;

   int size = 0;
   size     = sizeof( int );
   EXPECT_EQ( size, 4 );

   size = sizeof( long );
   EXPECT_EQ( size, 8 );
}

#include <random>

TEST( std, random )
{
   std::random_device rd;
   std::default_random_engine generator( rd() );
   
   std::uniform_int_distribution<uint> random_int( 0, 9999 );
   LOGD << "=== int Random : " << random_int;
   for( int i = 0; i < 20; i++ ) {
      LOGD << std::setfill( '0' ) << std::setw( 4 ) << random_int( generator );
   }
   
   std::uniform_real_distribution<double> random_real( -10, 10 );
   LOGD << "=== real Random : " << random_real;
   for( int i = 0; i < 20; i++ ) {
      LOGD << random_real( generator );
   }
}

#endif // TST_STD_H

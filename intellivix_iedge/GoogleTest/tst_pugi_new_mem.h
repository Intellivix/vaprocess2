#ifndef TST_PUGI_NEW_MEM_H
#define TST_PUGI_NEW_MEM_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "tst_pugi_preset.h"

using namespace testing;

static FileIO fileio;
static CXmlRoot xml_write_test;
static CXmlRoot xml_read_test;

TEST( new_pugi_memory, prepare )
{
   xml_write_test.createNodes();
   xml_read_test.createNodes();
   xml_read_test.Clear();
}

TEST( new_pugi_memory, mem_write )
{
   bool bRet = false;

   for( int i = 0; i < TEST_REPEAT; i++ ) {
#ifdef _DEBUG
      std::cout << "=== [new] FileIO Write Memory start " << i << " ===\n";
#endif

      fileio.GoToStartPos();

      CXMLIO xml_io_write;
      xml_io_write.SetFileIO( &fileio, BCCL::XMLIO_Write );
      bRet = xml_write_test.ReadWriteXML( &xml_io_write );
#ifdef _DEBUG
      std::cout << "<<< xml mem fileio.GetBuffer() size[" << fileio.GetLength() << "]\n"
           << fileio.GetBuffer() << "end >>>\n";
#else
      std::cout << "<<< xml mem fileio.GetBuffer() size[" << fileio.GetLength() << "]\n";
#endif
      EXPECT_EQ( true, bRet );
   }
}

TEST( new_pugi_memory, read_parse )
{
   bool bRet = false;

   for( int i = 0; i < TEST_REPEAT; i++ ) {
#ifdef _DEBUG
      std::cout << "=== [new] FileIO Read Memory start " << i << " ===\n";
#endif

      fileio.GoToStartPos();

      CXMLIO xml_io_read;
      xml_io_read.SetFileIO( &fileio, BCCL::XMLIO_Read );
      bRet = xml_read_test.ReadWriteXML( &xml_io_read );
      EXPECT_EQ( true, bRet );
   }
}

TEST( new_pugi_memory, verify )
{
   EXPECT_EQ( true, xml_read_test.Verity() );
}
#endif // TST_PUGI_NEW_MEM_H

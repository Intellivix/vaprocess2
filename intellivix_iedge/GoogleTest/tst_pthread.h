#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

using namespace testing;

#include <pthread.h>

class PThead {
public:
   ~PThead();
   static void* thread_loop( void* pParam )
   {
      int max = *(int*)pParam;
      for( int i = 0; i < max; i++ ) {
         printf( "threading.. %d\n", i );
      }
      return NULL;
      pthread_exit( pParam );
   }
   void startThread();

public:
   pthread_t thread_id;
};
PThead::~PThead()
{
   pthread_cancel( thread_id );
   pthread_exit( NULL );
}
void PThead::startThread()
{
   pthread_create( &this->thread_id, NULL, PThead::thread_loop, 0 );
   pthread_join( thread_id, NULL );
}

TEST( pthread, set )
{
   PThead thread;
   thread.startThread();

   pthread_exit( NULL );

   EXPECT_EQ( 1, 1 );
   ASSERT_THAT( 0, Eq( 0 ) );
}


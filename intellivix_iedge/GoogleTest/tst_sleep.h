#ifndef TST_SLEEP_H
#define TST_SLEEP_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "bccl_plog.h"

#include <ctime>
//#include "TimerCheckerManager.h"

static uint interval_ms = 1;
static timespec m_last_timespec;

void DiffTimespec( timespec* start, timespec* stop, timespec* result )
{
   if( ( stop->tv_nsec - start->tv_nsec ) < 0 ) {
      result->tv_sec  = stop->tv_sec - start->tv_sec - 1;
      result->tv_nsec = stop->tv_nsec - start->tv_nsec + 1000000000;
   } else {
      result->tv_sec  = stop->tv_sec - start->tv_sec;
      result->tv_nsec = stop->tv_nsec - start->tv_nsec;
   }
}

void DebugIntervalError()
{
   struct timespec raw_time_spec;
   clock_gettime( CLOCK_MONOTONIC, &raw_time_spec );

   struct timespec diff_time_spec;
   DiffTimespec( &m_last_timespec, &raw_time_spec, &diff_time_spec );

   double diff_ms   = diff_time_spec.tv_nsec / 1000000.0 + diff_time_spec.tv_sec * 1000;
   double errors_ms = diff_ms - interval_ms;

   printf( "interval[%3d] => measured[%7.2f] error[%+7.2f] (ms)\n",
            interval_ms, diff_ms, errors_ms );
   
   m_last_timespec.tv_sec  = raw_time_spec.tv_sec;
   m_last_timespec.tv_nsec = raw_time_spec.tv_nsec;
}

void test_func()
{
   LOGD << "Starting...";
   uint interval_us = interval_ms * 1000;
   while( true ) {
      usleep( interval_us );
      DebugIntervalError();
   }
}

TEST( sleep, main )
{
   test_func();
}

#endif // TST_SLEEP_H

#ifndef TST_PUGI_TEXT_HPP
#define TST_PUGI_TEXT_HPP

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "bccl_plog.h"
#include "bccl.h"

TEST( pugi, text )
{
   std::string xml_filename = "xml_license.xml";
   std::string strLicense = "CC6QEM-EDKZ4D-NYOS5C-RY62D4-4A22L4";
   std::string strSerialNumber = "74C74-B04ED-1D421-1B6EE-70098";
   
   CXMLIO xmlio( xml_filename, BCCL::XMLIO_Write );
   CXMLNode xml_node( &xmlio );
   if( xml_node.Start( "VAEngineLicense" ) ) {
      CXMLNode xml_node_license ( &xmlio );
      if( xml_node_license.Start( "LicenseKey" ) ) {
         xml_node_license.TextValue( TYPE_ID( string ), &strLicense );
         xml_node_license.End();
      }
      CXMLNode xml_node_serial ( &xmlio );
      if( xml_node_serial.Start( "SerialNumber" ) ) {
         xml_node_serial.TextValue( TYPE_ID( string ), &strSerialNumber );
         xml_node_serial.End();
      }
      xml_node.End();
   }
}


#endif // TST_PUGI_TEXT_HPP

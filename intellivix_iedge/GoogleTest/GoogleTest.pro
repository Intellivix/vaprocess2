GOOGLETEST_DIR = $$PWD/../../third_party/googletest
include(gtest_dependency.pri)

TARGET = GoogleTest
TEMPLATE = app
CONFIG += console c++11 thread qtc_runnable
CONFIG -= qt

include( ../env.pri )

INCLUDEPATH += ../BCCL
INCLUDEPATH += ../VACL
INCLUDEPATH += ../Common
INCLUDEPATH += ../Live555
INCLUDEPATH += ../VCM
INCLUDEPATH += ../IVCP
INCLUDEPATH += ../Intellivix

DEPENDPATH += ../BCCL
DEPENDPATH += ../VACL
DEPENDPATH += ../Common
DEPENDPATH += ../Live555
DEPENDPATH += ../VCM
DEPENDPATH += ../IVCP
DEPENDPATH += ../Intellivix

PRE_TARGETDEPS += $$DESTDIR/libBCCL.a
PRE_TARGETDEPS += $$DESTDIR/libVACL.a
PRE_TARGETDEPS += $$DESTDIR/libCommon.a
PRE_TARGETDEPS += $$DESTDIR/libLive555.a
PRE_TARGETDEPS += $$DESTDIR/libVCM.a
PRE_TARGETDEPS += $$DESTDIR/libIVCP.a

HEADERS +=  \
    tst_intellivixtestcase.h \
    tst_pugi_new_mem.h \
    tst_pugi_preset.h \
    tst_pugi_new_file.h \
    tst_pthread.h \
    tst_ivx_file_monitor.h \
    tst_samples.h \
    tst_sleep.h \
    tst_ivx_license.hpp \
    tst_pugi_text.hpp \
    tst_std.hpp \
    tst_casablanca.hpp \
    tst_sysmon_mem.hpp \
    tst_sysmon_cpu.hpp \
    tst_ivx_FILETIME.hpp \
    tst_ivx_timer.hpp \

SOURCES += \
    main_GoogleTest.cpp

INCLUDEPATH += Timer_yhpark
HEADERS += tst_timer_yhpark.hpp \
   Timer_yhpark/Timer.h
SOURCES +=  Timer_yhpark/Timer.cpp

INCLUDEPATH += ivx_lgu_integ_log
HEADERS += tst_ivx_lgu_integ_log.hpp \
   ivx_lgu_integ_log/Logger_LGU.h \
   ivx_lgu_integ_log/LogHeader_LGU.h \
   ivx_lgu_integ_log/LogBody_LGU.h
SOURCES += \
   ivx_lgu_integ_log/Logger_LGU.cpp \
   ivx_lgu_integ_log/LogHeader_LGU.cpp \
   ivx_lgu_integ_log/LogBody_LGU.cpp


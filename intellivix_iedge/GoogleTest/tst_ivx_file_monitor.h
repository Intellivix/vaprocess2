#ifndef TST_IVX_FILE_MONITOR_H
#define TST_IVX_FILE_MONITOR_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "bccl_plog.h"
#include "FileMonitoring.h"

class FileMonitoring_Custom : public FileMonitoring {
public:
   bool StartThread();

protected:
   bool Process( struct inotify_event* event ) override;
};

bool FileMonitoring_Custom::StartThread()
{
   return FileMonitoring::StartThread( this );
}

bool FileMonitoring_Custom::Process( inotify_event* event )
{
   if( FileMonitoring::Process( event ) == false ) {
      return false;
   }

   std::string filename( event->name );
   LOGI << "OVERRIDED on [" << filename << "]";
   return true;
}


///
/// \brief TEST
///
TEST( ivx_file_monitor, main )
{
   FileMonitoring_Custom fm;
   fm.SetThreadName( "FileMonitor" );
   fm.SetDir( "./recv" );
   fm.StartThread();

   int ch = 0;
   while( ch != 'q' ) {
      ch = getchar();
   }
   
}

#endif // TST_IVX_FILE_MONITOR_H

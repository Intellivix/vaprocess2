﻿#ifndef TST_TIMER_H
#define TST_TIMER_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "Timer_yhpark/Timer.h"

#include "bccl_plog.h"

TEST( Timer_yhpark, one_thread )
{
   //   Timer::get(); // no needs, coz it has auto init.

   //   Timer::get().addTimer( "1000", 1000 );

   //   Timer::get().addTimer( "1", 1 );
   //   std::this_thread::sleep_for( std::chrono::milliseconds( 250 ) );
   //   Timer::get().removeTimer( "1" );

   //   std::this_thread::sleep_for( std::chrono::seconds( 1 ) );
   //   Timer::get().addTimer( "10", 10 );
   //   std::this_thread::sleep_for( std::chrono::milliseconds( 250 ) );
   //   Timer::get().addTimer( "100", 100 );
   //   std::this_thread::sleep_for( std::chrono::milliseconds( 500 ) );
   //   Timer::get().removeTimer( "100" );
   //   std::this_thread::sleep_for( std::chrono::milliseconds( 250 ) );
   //   Timer::get().removeTimer( "10" );
   //   std::this_thread::sleep_for( std::chrono::seconds( 1 ) );

   //   Timer::get().removeTimer( "1000" );
   //   std::this_thread::sleep_for( std::chrono::seconds( 1 ) );
}

TEST( Timer_yhpark, multi_thread )
{
   plog::get()->setMaxSeverity( plog::debug );

   Timer::get(); // no needs, coz it has auto init.

   uint test_time_sec = 30;
   int test_timer_count = 500;

   bool is_running_test;
   std::thread thread_main( [&is_running_test, test_time_sec] {
      is_running_test = true;
      while( is_running_test ) {
         sleep( test_time_sec );
         is_running_test = false;
         LOGF << "Test Time finished (sec):" << test_time_sec;
      }
   } );

   std::map<std::string, double> timer_names_as_key;

   for( int i = 0; i < test_timer_count; i++ ) {
      timer_names_as_key[std::to_string( i )] = 1000.0 / 7;
   }

   for( auto& it : timer_names_as_key ) {
      Timer::get().addTimer( it.first, it.second );
   }

   std::vector<std::thread*> threads;
   for( auto& it : timer_names_as_key ) {
      std::thread* pthread = new std::thread( [&is_running_test, &it] {
         TimeWatch m_time_watch;
         Timer::Item* timer = Timer::get().getTimer( it.first );

         while( is_running_test ) {
            //LOGV << "waitting for " << it.first;
            timer->wait();

            double measured_ms = m_time_watch.elapsed_ms();
            double errors_ms   = measured_ms - it.second;
            std::ostringstream ss;
            ss << "n[" << std::setw( 4 ) << it.first << "] i[";
            ss << std::fixed << std::setprecision( 2 ) << std::setw( 6 ) << std::setfill( ' ' )
               << it.second
               << "] m[" << measured_ms
               << "] e[" << std::setw( 6 ) << errors_ms
               << "]";

            double abs_errors = fabs( errors_ms );
            if( abs_errors > 12.0 ) {
               LOGF << ss.str();
            } else if( abs_errors > 10.0 ) {
               LOGE << ss.str();
            } else if( abs_errors > 8.0 ) {
               LOGW << ss.str();
            } else if( abs_errors > 6.0 ) {
               LOGI << ss.str();
            } else if( abs_errors > 4.0 ) {
               LOGD << ss.str();
            } else if( abs_errors > 2.0 ) {
               LOGV << ss.str();
            }
         }

      } );
      threads.push_back( pthread );
   }

   for( auto& thread : threads ) {
      if( thread->joinable() ) {
         thread->join();
         delete thread;
      }
   }

   for( auto& timer : timer_names_as_key ) {
      Timer::get().removeTimer( timer.first );
   }
}

#endif // TST_TIMER_H

﻿#ifndef TST_IVX_TIMER_H
#define TST_IVX_TIMER_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "bccl.h"
#include "bccl_plog.h"
#include "MMTimer.h"
#include "TimerCheckerManager.h"
#include "WaitForMultipleObjects.h"
#include "CommonUtil.h"

static bool g_while_continue = false;
static CTimerManager* m_pTimerMgr = nullptr;

static void local()
{
   enum LocalSystemProcEvtType {
      Event_Timer_1000ms,
      Event_Timer_100ms,
      Event_Timer_10ms,
   };

   CMMTimer* pTimer_1000ms = m_pTimerMgr->GetNewTimer( "LocalSystem 1000ms" );
   CMMTimer* pTimer_100ms  = m_pTimerMgr->GetNewTimer( "LocalSystem 100ms" );
   CMMTimer* pTimer_10ms   = m_pTimerMgr->GetNewTimer( "LocalSystem 10ms" );

   pTimer_1000ms->SetFPS( 1 );
   pTimer_100ms->SetFPS( 10 );
   pTimer_10ms->SetFPS( 100 );

   pTimer_1000ms->StartTimer();
   pTimer_100ms->StartTimer();
   pTimer_10ms->StartTimer();

   CWaitForMultipleObjects wmo;
   std::vector<CEventEx*> vecEvt;
   vecEvt.push_back( &pTimer_1000ms->GetTimerEvent() );
   vecEvt.push_back( &pTimer_100ms->GetTimerEvent() );
   vecEvt.push_back( &pTimer_10ms->GetTimerEvent() );
   wmo.Init( vecEvt );
   
   CPerformanceCounter pc[3];
   pc[Event_Timer_1000ms].CheckTimeStart();
   pc[Event_Timer_100ms].CheckTimeStart();
   pc[Event_Timer_100ms].CheckTimeStart();

   LOGD << "into while";
   while( g_while_continue ) {
      
      wmo.WaitForMultipleObjects( FALSE );
      std::vector<bool> vecSignal = wmo.GetSignal();

      // 1초 주기로 처리되는 작업들
      if( vecSignal[Event_Timer_1000ms] ) {
         LOGE << TimerCheckerManager::Get().DebugMsg( pTimer_1000ms->m_szName, pTimer_1000ms->m_nPeriod );
         LOGE << std::setprecision(std::numeric_limits<double>::digits10) << pc[Event_Timer_1000ms].GetElapsedTime();
         pc[Event_Timer_1000ms].CheckTimeStart();
      }

      // 100ms 주기로 처리되는 작업들
      if( vecSignal[Event_Timer_100ms] ) {
         static int count = 0;
         count++;
         //if (count % 10)  continue;
         LOGW << TimerCheckerManager::Get().DebugMsg( pTimer_100ms->m_szName, pTimer_100ms->m_nPeriod );
         LOGW << std::setprecision(std::numeric_limits<double>::digits10) << pc[Event_Timer_100ms].GetElapsedTime();
         pc[Event_Timer_100ms].CheckTimeStart();
      }

      // 10ms 주기로 처리되는 작업들
      if( vecSignal[Event_Timer_10ms] ) {
         static int count = 0;
         count++;
         //if (count % 100)  continue;
         LOGD << TimerCheckerManager::Get().DebugMsg( pTimer_10ms->m_szName, pTimer_10ms->m_nPeriod );
         LOGD << std::setprecision(std::numeric_limits<double>::digits10) << pc[Event_Timer_10ms].GetElapsedTime();
         pc[Event_Timer_10ms].CheckTimeStart();
      }
   }
}

TEST( ivx_timer, ivx_timer_checker )
{
   TimerChecker timer( "gtest", 1000 );
   Sleep_ms( 1000 );
   LOGD << timer.DebugIntervalError();
}


TEST( ivx_timer, main )
{
   BCCL::PlogHelper::initDisplayConsole( plog::verbose );
   
   CTimerManagerSP::Activate(CTimerManagerSP::Type_Direct);
   CTimerManagerSP TimerManager;
   m_pTimerMgr = TimerManager.Get();

   g_while_continue = true;
   std::thread th_local( local );
   
   LOGD << "watting signal";
   int ch = 0;
   while( ch != 'q' ) {
      ch = getchar();
   }
   g_while_continue = false;

   if( th_local.joinable() ) {
      LOGV << "joinning";
      th_local.join();
   }
}

#endif // TST_IVX_TIMER_H

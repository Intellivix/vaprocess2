﻿#ifndef TST_SYSMON_HPP
#define TST_SYSMON_HPP

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "bccl_plog.h"

// This assumes that a digit will be found and the line ends in " Kb".
int parseLine( char* line )
{
   int i         = strlen( line );
   const char* p = line;
   while( *p < '0' || *p > '9' )
      p++;
   line[i - 3] = '\0';
   i           = atoi( p );
   return i;
}

//Note: this value is in KB!
int virtual_memory_currently_used_by_current_process_kb()
{
   FILE* file = fopen( "/proc/self/status", "r" );
   int result = -1;
   char line[128];

   while( fgets( line, 128, file ) != NULL ) {
      if( strncmp( line, "VmRSS:", 6 ) == 0 ) {
         result = parseLine( line );
         break;
      }
   }
   fclose( file );
   return result;
}

#include <sys/types.h>
#include <sys/sysinfo.h>

TEST( sysmon, memInfo )
{
   struct sysinfo memInfo;
   sysinfo( &memInfo );

   __kernel_ulong_t totalVirtualMem = memInfo.totalram;
   //Add other values in next statement to avoid int overflow on right hand side...
   totalVirtualMem += memInfo.totalswap;
   totalVirtualMem *= memInfo.mem_unit;
   logd( "%30s : %12ld", "Total Virtual Memory", totalVirtualMem );

   __kernel_ulong_t virtualMemUsed = memInfo.totalram - memInfo.freeram;
   //Add other values in next statement to avoid int overflow on right hand side...
   virtualMemUsed += memInfo.totalswap - memInfo.freeswap;
   virtualMemUsed *= memInfo.mem_unit;
   logd( "%30s : %12ld", "Virtual Memory currently used", virtualMemUsed );

   __kernel_ulong_t totalPhysMem = memInfo.totalram;
   //Multiply in next statement to avoid int overflow on right hand side...
   totalPhysMem *= memInfo.mem_unit;
   logd( "%30s : %12ld", "Total Physical Memory (RAM)", totalPhysMem );

   __kernel_ulong_t physMemUsed = memInfo.totalram - memInfo.freeram;
   //Multiply in next statement to avoid int overflow on right hand side...
   physMemUsed *= memInfo.mem_unit;
   logd( "%30s : %12ld", "Physical Memory currently used", physMemUsed );

   double free_percentage = physMemUsed / static_cast<double>( totalPhysMem ) * 100;
   logd( "%30s : %5.2f", "Pyhsical Mem %", free_percentage );

   logd( "%30s : %12ld", "Virtual MEM of this process", virtual_memory_currently_used_by_current_process_kb() );
}

#endif // TST_SYSMON_HPP

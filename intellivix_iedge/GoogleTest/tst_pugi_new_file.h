#ifndef TST_PUGI_NEW_H
#define TST_PUGI_NEW_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "tst_pugi_preset.h"

using namespace testing;

static std::string xml_filename = "xml_test_case.xml";

TEST( new_pugi_file, write )
{
   bool bRet = false;
   CXmlRoot xml_write_test;
   xml_write_test.createNodes();

   for( int i = 0; i < TEST_REPEAT; i++ ) {
#ifdef _DEBUG
      std::cout << "=== new_pugi_file.write " << i << " ===\n";
#endif
      CXMLIO xml_io_write( xml_filename, XMLIO_Write );
      bRet = xml_write_test.ReadWriteXML( &xml_io_write );
      EXPECT_EQ( true, bRet );
   }
}

TEST( new_pugi_file, read_parse )
{
   bool bRet = false;
   CXmlRoot xml_read_test;
   xml_read_test.createNodes();

   for( int i = 0; i < TEST_REPEAT; i++ ) {
#ifdef _DEBUG
      std::cout << "=== new_pugi_file.read_parse " << i << " ===\n";
#endif
      CXMLIO xml_io_read( xml_filename, BCCL::XMLIO_Read );
      bRet = xml_read_test.ReadWriteXML( &xml_io_read );
      EXPECT_EQ( true, bRet );
   }
}

#endif // TST_PUGI_NEW_H

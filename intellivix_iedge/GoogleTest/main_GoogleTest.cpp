﻿#include <gtest/gtest.h>

#ifdef _DEBUG
#include "bccl_plog.h"
#endif

//#include "tst_samples.h"
//#include "tst_intellivixtestcase.h"

//#include "tst_ivx_FILETIME.hpp"
//#include "tst_pugi_new_file.h"
//#include "tst_pugi_new_mem.h"
//#include "tst_pugi_text.hpp"

//#include "tst_std.hpp"

//#include "tst_sleep.h"
//#include "tst_ivx_timer.hpp"
//#include "tst_ivx_timerchecker.hpp"
//#include "tst_timer_yhpark.hpp"

//#include "tst_ivx_file_monitor.h"
//#include "tst_ivx_license.hpp"

//#include "tst_casablanca.hpp"

//#include "tst_sysmon_mem.hpp"
//#include "tst_sysmon_cpu.hpp"

#include "tst_ivx_lgu_integ_log.hpp"

int main( int argc, char* argv[] )
{

#ifdef _DEBUG
   BCCL::PlogHelper::initDisplayConsole( plog::verbose );
#endif // _DEBUG

   //::testing::GTEST_FLAG(filter) = "tst_samples*";
   //::testing::GTEST_FLAG(filter) = "tst_ivx*";

   ::testing::InitGoogleTest( &argc, argv );

   return RUN_ALL_TESTS();
}

#ifndef TST_SAMPLES_H
#define TST_SAMPLES_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "bccl_plog.h"

///
/// \brief The Org class
///
class Org {
public:
   virtual ~Org() = default;
   virtual bool ReadXml();
};
bool Org::ReadXml()
{
   printf( "start <Orginal>\n" );
   printf( " end  </Orginal>\n" );
   return true;
}

///
/// \brief The OrgEx class
///
class OrgEx : public Org {
public:
   ~OrgEx() = default;
   bool ReadXml() override;
};
bool OrgEx::ReadXml()
{
   printf( "start <OrginalEx>\n" );
   Org::ReadXml();
   printf( " end  </OrginalEx>\n" );
   return true;
}

///
/// \brief TEST
///
TEST( samples, Ex )
{
   Org* pOrg = new OrgEx();
   pOrg->ReadXml();
   delete pOrg;
}

///
/// \brief The Base class
///
class Base {
public:
   Base( std::string name = "[Base]" )
       : m_name( name )
   {
      LOGD << m_name << " : "
           << "Base Create.";
   }
   virtual ~Base()
   {
      LOGD << m_name << " : "
           << "Base Destory.";
   }
   virtual void work();
   std::string m_name;
};

///
/// \brief The Dog class
///
class Dog : public Base {
public:
   Dog( std::string name = "[Dog Base]" )
       : Base( name )
   {
      LOGW << m_name << " : "
           << "Dog Create.";
   }
   ~Dog()
   {
      LOGE << m_name << " : "
           << "Dog Destory.";
   }
   void work();
};

///
/// \brief The Cat class
///
class Cat : public Base {
public:
   Cat( std::string name = "[Cat Base]" )
       : Base( name )
   {
      LOGW << m_name << " : "
           << "Cat Create.";
   }
   ~Cat()
   {
      LOGE << m_name << " : "
           << "Cat Destory.";
   }
   void work() override;
};

void Cat::work()
{
   Base::work();
   LOGI << m_name << " : Cat.";
}

void Dog::work()
{
   Base::work();
   LOGI << m_name << " : Dog.";
}

void Base::work()
{
   LOGD << m_name << " : Animal.";
}

///
/// \brief TEST
///
TEST( samples, inherit )
{
   Base base;
   Cat cat;
   Dog dog;

   std::vector<Base*> vec;

   vec.push_back( &base );
   vec.push_back( &cat );
   vec.push_back( &dog );

   for( size_t i = 0; i < vec.size(); i++ ) {
      vec[i]->work();
   }
}

#endif // TST_SAMPLES_H

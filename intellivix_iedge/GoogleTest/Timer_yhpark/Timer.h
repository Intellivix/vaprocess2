﻿#ifndef TIMER_H
#define TIMER_H

#include <ctime>
#include <csignal>
#include <sys/time.h>

#include <iostream>
#include <map>

#include <thread>
#include <mutex> // std::mutex, std::unique_lock
#include <condition_variable> // std::condition_variable

class TimeWatch {
public:
   TimeWatch();
   void measure();
   double elapsed_ms();

private:
   std::chrono::high_resolution_clock::time_point m_previous_clock;
};

/**
 * @brief The UniqueLock class
 * wrapper for mutex and unique_lock.
 */
class UniqueLock {
public:
   UniqueLock();
   ~UniqueLock();
   void wait();
   void wakeup();

private:
   std::mutex m_mutex;
   std::unique_lock<std::mutex> m_locker;
   std::condition_variable m_condition_variable;
};

/**
 * @brief The Timer class
 * SingleTon.
 * Timer class has Item as a inner class.
 */
class Timer {
private:
   /**
    * A global system signal for timers. subtitutes( SIGRTMIN, SIGVTALRM, SIGUSR1 );
    */
   const static int g_signal_number = SIGVTALRM; // SIGRTMIN, SIGVTALRM, SIGUSR1
public:
   class Item; // inner forward declation.
   static double msToFps( uint fps );

public:
   /**
    * SingleTon Pattern. To access this class.
    * @return own instance.
    */
   static Timer& get();

   /**
    * Add timer into the container.
    * @param interval_ms
    * @return successs(bool).
    */
   bool addTimer( const std::string& name, double interval_ms );

   /**
    * Getter
    * @param name Timer's name as key for map.
    * @return successs(bool).
    */
   double getInterval( const std::string& name );

   /**
    * Remove timer into the container.
    * @param interval_ms
    * @return successs(bool).
    */
   bool removeTimer( const std::string& name );

   /**
    * Wait until Timer hits.
    * @param name Timer's name as key for map.
    * @return 
    */
   bool wait( const std::string& name );

   /**
   * get timer object by name.
   * @param name
   * @return 
   */
   Item* getTimer( const std::string& name );

private:
   /**
    * SingleTon Pattern. Constructor.
    * @details Initialize signal handling and threading all-in-one, so everything will be prepared.
    */
   Timer();

   /**
    * SingleTon Pattern. Destructor.
    */
   ~Timer();

   /**
    * Register system singal handling.
    */
   void registerSignalHandling();

   /**
    * Block the signal into this process.
    * we'll pass only to this thread that will be created later.
    */
   void signalBlockingInProcess();

   /**
    * Block all signals excepting the signal into this thread.
    */
   static void signalMaskingInThread();

   /**
    * Important. Keep thread for getting the only timer signal from system instead of others.
    */
   void callback_Thread();

   /**
    * When the timers expire, registered interrupt signal call this back.
    * it will wakeup it's object directly. better performance.
    * @param signal_number Unused.
    * @param sig_info Timer object information passed by system.
    * @param param Unused.
    */
   static void callback_Signal( int signal_number, siginfo_t* sig_info, void* param );

   /**
    * Utility. Calculate two timespec's diff.
    * @param start Started time.
    * @param stop Stopped stime.
    * @param result For return.
    */
   static void diffTimespec( timespec* start, timespec* stop, timespec* result );

   /**
    * This class takes care of thread.
    */
   void startThread();

   /**
    * This class takes care of thread.
    */
   void stopThread();

   /**
    * This class takes care of thread.
    */
   std::thread m_thread;

   /**
    * Container for Timer Items.
    */
   // TODO(yhpark): I will change Item* to std::shard_ptr.
   std::map<std::string, Item*> m_items;

   /**
    * wrapper for unique_lock
    */
   UniqueLock m_locker;

   //////////////////////////////////////////////
   /// Timer::Item
   //////////////////////////////////////////////

public:
   /**
    * Inner class. Used as Timer's container item.
    * @details
    * Timer has encapsulated this class, so you don't need to take care of anything.
    */
   class Item {
   public:
      /**
       * The contructor and destrocter take care of everything.
       * @param interval_ms
       */
      explicit Item( const std::string& name, double m_interval_ms );

      /**
       * The contructor and destrocter take care of everything.
       */
      ~Item();

      /**
       * @brief GetName Getter.
       * @return 
       */
      const std::string& getName() const;

      /**
       * @brief GetInterval_ms Getter.
       */
      double getInterval_ms() const;

      /**
       * @brief wait 
       * wait until timer's hit.
       */
      void wait();

      /**
       * when a timer expires, unlock the mutex.
       */
      void wakeup();

   private:
      /**
       * Timer create. Triggers by ONLY Contstrutor.
       */
      void create();

      /**
       * Timer delete. Triggers by ONLY Destructor.
       */
      void remove();

      /**
       * System's timer id. it's a key for system timer singal.
       */
      timer_t m_timer_id;

      /**
       * Timer Item's  interval value set by user.
       */
      double m_interval_ms;

      /**
       * Timer Item's name. it's a key for map.
       */
      std::string m_name;

      /**
       * Timer Item's unique_lock wrapper. Better performance than recursive_lock.
       */
      UniqueLock m_locker;

#ifdef _DEBUG_WATCH
   public:
      /**
       * to print out to logs.
       */
      std::string debugIntervalError();

      /**
       * to measure measured interval.
       */
      TimeWatch m_time_watch;
#endif // _DEBUG_WATCH
   };
};

#endif // TIMER_H

﻿#include "Timer.h"

#include "bccl_plog.h"
#include "InnerThread.h"

TimeWatch::TimeWatch()
{
   elapsed_ms();
}

void TimeWatch::measure()
{
   m_previous_clock = std::chrono::high_resolution_clock::now();
}

double TimeWatch::elapsed_ms()
{
   std::chrono::duration<double, std::milli> elapsed = std::chrono::high_resolution_clock::now() - m_previous_clock;
   measure();
   return elapsed.count();
}

UniqueLock::UniqueLock::UniqueLock()
{
   m_locker = std::unique_lock<std::mutex>( m_mutex );
}

UniqueLock::~UniqueLock()
{
   m_condition_variable.notify_all();
}

void UniqueLock::wait()
{
   m_condition_variable.wait( m_locker );
}

void UniqueLock::wakeup()
{
   m_condition_variable.notify_all();
}

double Timer::msToFps( uint fps )
{
   return 1000 / fps;
}

Timer& Timer::get()
{
   static Timer m_instance;
   return m_instance;
}

bool Timer::addTimer( const std::string& name, double interval_ms )
{
   if( name.empty() ) {
      LOGE << "AddTimer error: no name.";
      return false;
   }

   if( m_items.find( name ) != m_items.end() ) {
      LOGE << "AddTimer error: " << name << " is exist, so delete for replacing.";
      m_items.erase( name );
   }

   // insert item into map.
   LOGD << "insert item into map.";
   m_items[name] = new Item( name, interval_ms );

   return true;
}

double Timer::getInterval( const std::string& name )
{
   Item* item = getTimer( name );
   if( item == nullptr ) {
      LOGE << "Wait error: no timer: " << name;
      return 0;
   }
   return item->getInterval_ms();
}

bool Timer::removeTimer( const std::string& name )
{
   try {
      auto it = m_items.find( name );
      it->second->wakeup();
      m_items.erase( it );
      delete it->second;
   } catch( std::exception e ) {
      LOGE << "failed to remove timer: " << name << "";
      return false;
   }
   return true;
}

bool Timer::wait( const std::string& name )
{
   Timer::Item* item = getTimer( name );
   if( item == nullptr ) {
      LOGE << "Wait error: no timer: " << name;
      return false;
   }
   item->wait();
   return true;
}

Timer::Item* Timer::getTimer( const std::string& name )
{
   Item* item = nullptr;
   try {
      item = m_items.at( name );
   } catch( std::exception e ) {
      LOGE << "GetTimer error:" << name << "";
   }

   return item;
}

void Timer::callback_Signal( int signal_number, siginfo_t* sig_info, void* param )
{
   (void)signal_number;
   (void)param;

   Timer::Item* pItem = reinterpret_cast<Timer::Item*>( sig_info->si_value.sival_ptr );
   if( pItem == nullptr )
      return;

#ifdef _DEBUG
      //pItem->debugIntervalError();
#endif // _DEBUG

   pItem->wakeup();
}

void Timer::diffTimespec( timespec* start, timespec* stop, timespec* result )
{
   if( ( stop->tv_nsec - start->tv_nsec ) < 0 ) {
      result->tv_sec  = stop->tv_sec - start->tv_sec - 1;
      result->tv_nsec = stop->tv_nsec - start->tv_nsec + 1000000000;
   } else {
      result->tv_sec  = stop->tv_sec - start->tv_sec;
      result->tv_nsec = stop->tv_nsec - start->tv_nsec;
   }
}

void Timer::callback_Thread()
{
   LOGI << "Timer thread starting.";
   signalMaskingInThread();
   m_locker.wait();
   LOGI << "Timer thread finished.";
}

void Timer::signalMaskingInThread()
{
   sigset_t sig_set_masking;
   sigemptyset( &sig_set_masking );
   sigaddset( &sig_set_masking, g_signal_number );
   pthread_sigmask( SIG_UNBLOCK, &sig_set_masking, nullptr );
}

Timer::Timer()
{
   LOGI << "Timer Constroctor.";
   registerSignalHandling();
   signalBlockingInProcess();
   startThread();
}

Timer::~Timer()
{
   stopThread();
}

void Timer::registerSignalHandling()
{
   logd( "### Register signal_number = %d", g_signal_number );

   struct sigaction sig_action;
   sig_action.sa_flags     = SA_SIGINFO; // SA_RESTART; // SA_SIGINFO; // SA_INTERRUPT
   sig_action.sa_sigaction = Timer::callback_Signal;
   sigemptyset( &sig_action.sa_mask );
   if( sigaction( g_signal_number, &sig_action, nullptr ) != 0 ) {
      perror( "sigaction" );
   }
}

void Timer::signalBlockingInProcess()
{
   sigset_t siget_mask;
   sigemptyset( &siget_mask );
   sigaddset( &siget_mask, g_signal_number );
   sigprocmask( SIG_BLOCK, &siget_mask, nullptr );
}

void Timer::startThread()
{
   m_thread = std::thread( [=] {
      InnerThread::setThreadName( "Timer" );
      Timer::callback_Thread();
   } );
}

void Timer::stopThread()
{
   m_locker.wakeup();
   if( m_thread.joinable() ) {
      m_thread.join();
   }
}

//////////////////////////////////////////////
/// Timer::Item
//////////////////////////////////////////////

Timer::Item::Item( const std::string& name, double interval_ms )
{
   logd( "### Timer::Item Constructor : %7.2f", interval_ms );
   this->m_interval_ms = interval_ms;
   this->m_name        = name;
   create();
}

Timer::Item::~Item()
{
   logd( "### Timer::Item[%s] Destructor ms : %7.2f", m_name.c_str(), m_interval_ms );
   remove();
}

void Timer::Item::create()
{
   logi( "### Timer::Item[%s] Constructor ms : %7.2f", m_name.c_str(), m_interval_ms );

   sigevent sig_event;
   sig_event.sigev_notify          = SIGEV_SIGNAL; // SIGEV_THREAD
   sig_event.sigev_signo           = g_signal_number;
   sig_event.sigev_value.sival_ptr = this;
   if( timer_create( CLOCK_MONOTONIC, &sig_event, &m_timer_id ) != 0 ) { // CLOCK_MONOTONIC, CLOCK_REALTIME
      LOGE << "timer_create failed. name:" << m_name;
      return;
   }

   // set timer interval.
   itimerspec timer_spec;
   timer_spec.it_interval.tv_sec  = __time_t( m_interval_ms / 1000.0 );
   timer_spec.it_interval.tv_nsec = ( __syscall_slong_t( m_interval_ms ) % 1000 ) * 1000000;
   timer_spec.it_value            = timer_spec.it_interval;

   if( timer_settime( m_timer_id, 0, &timer_spec, nullptr ) != 0 ) {
      LOGE << "timer_settime failed . name:" << m_name;
      return;
   }
}

double Timer::Item::getInterval_ms() const
{
   return m_interval_ms;
}

const std::string& Timer::Item::getName() const
{
   return m_name;
}

void Timer::Item::remove()
{
   if( timer_delete( m_timer_id ) != 0 ) {
      LOGE << "failed to remove timer. name:" << m_name;
   } else {
      logi( "### Removed timer[%s] : %7.2f", m_name.c_str(), m_interval_ms );
   }
}

void Timer::Item::wait()
{
   m_locker.wait();
}

void Timer::Item::wakeup()
{
   m_locker.wakeup();
}

#ifdef _DEBUG_WATCH
std::string Timer::Item::debugIntervalError()
{
   double diff_ms   = m_time_watch.elapsed_ms();
   double errors_ms = diff_ms - m_interval_ms;
   //logv( "interval[%7.2f] => measured[%7.2f] error[%+7.2f] (ms)", m_interval_ms, diff_ms, errors_ms );

   std::ostringstream ss;
   ss << "n[" << std::setw( 4 ) << m_name << "] i[";
   ss << std::fixed << std::setprecision( 2 ) << std::setw( 6 ) << std::setfill( ' ' )
      << m_interval_ms
      << "] m[" << diff_ms
      << "] e[" << std::setw( 6 ) << errors_ms
      << "]";
   return ss.str();
}
#endif // _DEBUG_WATCH

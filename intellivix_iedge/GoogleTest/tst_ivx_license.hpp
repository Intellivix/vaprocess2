#ifndef TST_IVX_LICENSE_HPP
#define TST_IVX_LICENSE_HPP

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "License/KeyDecryption.h"

class CLicenseItem {
public:
   std::string strProductName;
   std::string strProductKey;
   std::string strProductCode;
   std::string strProductVer;

   std::string strLicense;
   std::string strSerial;
   std::string strHWID;

   int nChannels = 0; // result
   int nFC       = 0; // result
   
   friend std::ostream& operator<<( std::ostream& stream, const CLicenseItem& object )
   {
      stream << "\n=========================================";
      stream << "\nLicense : " << object.strLicense;
      stream << "\nSerial  : " << object.strSerial;
      stream << "\nHWID    : " << object.strHWID;
      stream << "\n=========================================";
      stream << "\nProduct name   : " << object.strProductName;
      stream << "\nProduct key    : " << object.strProductKey;
      stream << "\nProduct code   : " << object.strProductCode;
      stream << "\nProduct ver    : " << object.strProductVer;
      stream << "\n=========================================";
      stream << "\nResult Channel : " << object.nChannels;
      stream << "\nResult FC      : " << object.nFC;
      stream << "\n=========================================\n\n";
      return stream;
   }
};

TEST( ivx_license, main )
{
   CLicenseItem license;

   license.strProductName = "IEDGE ROT FSNETWORKS";
   license.strProductKey  = "294EC3A3";
   license.strProductCode = "FSN";
   license.strProductVer  = "10";

   license.strLicense = "CC6QEM-EDKZ4D-NYOS5C-RY62D4-4A22L4";
   license.strSerial  = "74C74-B04ED-1D421-1B6EE-70098";
   license.strHWID    = "653234393564";

   license.nChannels = CertifyKey(
       license.strLicense.c_str(),
       license.strSerial.c_str(),
       license.strHWID.c_str(),
       license.strProductKey.c_str(),
       license.strProductCode.c_str(),
       license.strProductVer.c_str(), 
       license.nFC );

   std::cout << license;

   EXPECT_EQ( license.nChannels, 1 );
   EXPECT_EQ( license.nFC, FCLASS_VIDEOANALYTICS );
}

#endif // TST_IVX_LICENSE_HPP

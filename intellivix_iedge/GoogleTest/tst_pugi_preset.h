#ifndef TST_PUGI_PRESET_H
#define TST_PUGI_PRESET_H

#ifdef _DEBUG
static const int ATTR_COUNT        = 3;
static const int FIRST_NODE_COUNT  = 3;
static const int SECOND_NODE_COUNT = 2;
static const int TEST_REPEAT       = 1;
#else
static const int ATTR_COUNT        = 10;
static const int FIRST_NODE_COUNT  = 1000;
static const int SECOND_NODE_COUNT = 1000;
static const int TEST_REPEAT       = 1;
#endif // _DEBUG

#include "bccl.h"

static std::string g_str_default = "default_value";

class SecondNode {
public:
   SecondNode( int parent_num )
   {
      m_node_num = parent_num;
      initAttrData();
   }
   ~SecondNode()
   {
   }
   void initAttrData()
   {
      for( int i = 0; i < ATTR_COUNT; ++i ) {
         m_attr_names[i] = "attr_" + std::to_string( i ); // std c++11
         //         std::stringstream ss;
         //         ss << i;
         //         m_attr_names[i]  = "attr_" + ss.str();
         m_attr_values[i] = i;
      }
   }
   int ReadWriteXML( CXMLIO* pIO )
   {
      CXMLNode xml_node( pIO );
      if( xml_node.Start( "Second" ) ) {
         xml_node.Attribute( TYPE_ID( int ), "id", &m_node_num /*, &g_str_default*/ );
         for( int i = 0; i < ATTR_COUNT; i++ ) {
            xml_node.Attribute( TYPE_ID( int ), m_attr_names[i].c_str(), &m_attr_values[i] /*, &g_str_default*/ );
         }
         xml_node.End();
      }
      return ( DONE );
   }
   bool Verity( int node_num )
   {
      if( m_node_num != node_num )
         return false;
      return true;
   }
   void Clear()
   {
      m_node_num = -999;
      for( int i = 0; i < ATTR_COUNT; i++ ) {
         m_attr_values[i] = -999;
      }
   }

public:
   std::string m_attr_names[ATTR_COUNT];
   int m_attr_values[ATTR_COUNT];
   int m_node_num;
};

class FirstNode {
public:
   FirstNode( int node_num )
   {
      m_node_num = node_num;
      for( int i = 0; i < SECOND_NODE_COUNT; i++ ) {
         m_nodes[i] = new SecondNode( i );
      }
      initAttrData();
   }
   ~FirstNode()
   {
      for( int i = 0; i < SECOND_NODE_COUNT; i++ ) {
         delete m_nodes[i];
      }
   }
   void initAttrData()
   {
      for( int i = 0; i < ATTR_COUNT; ++i ) {
         m_attr_names[i] = "attr_" + std::to_string( i ); // std c++11
         //         std::stringstream ss;
         //         ss << i;
         //         m_attr_names[i]  = "attr_" + ss.str();
         m_attr_values[i] = i;
      }
   }
   int ReadWriteXML( CXMLIO* pIO )
   {
      CXMLNode xml_node( pIO );
      if( xml_node.Start( "First" ) ) {
         xml_node.Attribute( TYPE_ID( int ), "id", &m_node_num /*, &g_str_default*/ );
         for( int i = 0; i < ATTR_COUNT; i++ ) {
            xml_node.Attribute( TYPE_ID( int ), m_attr_names[i].c_str(), &m_attr_values[i] /*, &g_str_default */ );
         }
         for( int i = 0; i < SECOND_NODE_COUNT; i++ ) {
            m_nodes[i]->ReadWriteXML( pIO );
         }
         xml_node.End();
      }
      return ( DONE );
   }
   bool Verity( int node_num )
   {
      if( m_node_num != node_num )
         return false;
      for( int i = 0; i < SECOND_NODE_COUNT; i++ ) {
         if( !m_nodes[i]->Verity( i ) )
            return false;
      }
      return true;
   }
   void Clear()
   {
      m_node_num = -999;
      for( int i = 0; i < ATTR_COUNT; i++ ) {
         m_attr_values[i] = -999;
      }
      for( int i = 0; i < SECOND_NODE_COUNT; i++ ) {
         m_nodes[i]->Clear();
      }
   }

public:
   SecondNode* m_nodes[SECOND_NODE_COUNT];
   std::string m_attr_names[ATTR_COUNT];
   int m_attr_values[ATTR_COUNT];
   int m_node_num;
};

class CXmlRoot {
public:
   ~CXmlRoot()
   {
      for( int i = 0; i < FIRST_NODE_COUNT; ++i ) {
         delete m_nodes[i];
      }
   }
   void createNodes()
   {
      for( int i = 0; i < FIRST_NODE_COUNT; ++i ) {
         m_nodes[i] = new FirstNode( i );
      }
   }
   bool ReadWriteXML( CXMLIO* pIO )
   {
      CXMLNode xml_node( pIO );
      if( xml_node.Start( "CXmlTest" ) ) {
         for( int i = 0; i < FIRST_NODE_COUNT; i++ ) {
            m_nodes[i]->ReadWriteXML( pIO );
         }
         xml_node.End();
      }
      return true;
   }
   bool Verity()
   {
      for( int i = 0; i < FIRST_NODE_COUNT; i++ ) {
         if( !m_nodes[i]->Verity( i ) )
            return false;
      }
      return true;
   }
   void Clear()
   {
      for( int i = 0; i < FIRST_NODE_COUNT; i++ ) {
         m_nodes[i]->Clear();
      }
   }

public:
   int m_int[ATTR_COUNT];
   FirstNode* m_nodes[FIRST_NODE_COUNT];
};

#endif // TST_PUGI_PRESET_H

﻿#ifndef TST_IVX_FILETIME_HPP
#define TST_IVX_FILETIME_HPP

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "bccl.h"

class CXmlRoot {
public:
   CXmlRoot() {}

   void Create()
   {
      m_node_num                = 1234;
      m_fileTime.dwLowDateTime  = 0;
      m_fileTime.dwHighDateTime = 0;
      BCCL::Time orgTime;
      orgTime.GetLocalTime();
      char date[128]{};
      char time[128]{};
      orgTime.GetTimeInString_YMD( date );
      orgTime.GetTimeInString_HMS( time );
      LOGI << "date:" << date << " time:" << time << " mil:" << orgTime.Millisecond;

      BCCL::Convert( orgTime, m_fileTime );
   }
   bool ReadWriteXML( CXMLIO* pIO )
   {
      CXMLNode xml_node( pIO );
      if( xml_node.Start( "CXmlTest" ) ) {
         xml_node.Attribute( TYPE_ID( int ), "id", &m_node_num );
         xml_node.Attribute( TYPE_ID( FILETIME ), "filetime", &m_fileTime );
         xml_node.End();
      }
      return true;
   }
   bool Verity()
   {
      char date[128]{};
      char time[128]{};

      BCCL::Time orgTime;
      orgTime.GetLocalTime();

      orgTime.GetTimeInString_YMD( date );
      orgTime.GetTimeInString_HMS( time );
      LOGI << "[orign]: date:" << date << " time:" << time << " mil:" << orgTime.Millisecond;

      BCCL::Convert( orgTime, m_fileTime );

      Time convertedTime;
      BCCL::Convert( m_fileTime, convertedTime );

      convertedTime.GetTimeInString_YMD( date );
      convertedTime.GetTimeInString_HMS( time );
      LOGI << "[after]: date:" << date << " time:" << time << " mil:" << convertedTime.Millisecond;

      return true;
   }
   void Clear()
   {
   }

   int m_node_num = 0;
   FILETIME m_fileTime;
};

//static CXmlRoot xml_write_test;
//static CXmlRoot xml_read_test;

TEST( ivx, filetime )
{
   usleep( 100000 );

   CXmlRoot xml_write;

   LOGI << "=== verify ===";
   xml_write.Verity();
   LOGI << "=== verify ===";

   xml_write.Create();
   LOGI << "num:" << xml_write.m_node_num
        << " low:" << xml_write.m_fileTime.dwLowDateTime
        << " high:" << xml_write.m_fileTime.dwHighDateTime;

   FileIO fileio;
   CXMLIO xml_io_write( &fileio, BCCL::XMLIO_Write );
   xml_write.ReadWriteXML( &xml_io_write );

   fileio.GoToStartPos();
   LOGI << "=== after pasing ===\n"
        << fileio.GetBuffer();

   CXmlRoot xml_read;

   LOGI << "num:" << xml_read.m_node_num
        << " low:" << xml_read.m_fileTime.dwLowDateTime
        << " high:" << xml_read.m_fileTime.dwHighDateTime;

   CXMLIO xml_io_read( &fileio, BCCL::XMLIO_Read );
   xml_read.ReadWriteXML( &xml_io_read );

   LOGI << "num:" << xml_read.m_node_num
        << " low:" << xml_read.m_fileTime.dwLowDateTime
        << " high:" << xml_read.m_fileTime.dwHighDateTime;

   //PugiXmlHelper::PrintParseResultForDebug( xml_io_read.Pugi()->GetXmlDocument() );
}

#endif // TST_IVX_FILETIME_HPP

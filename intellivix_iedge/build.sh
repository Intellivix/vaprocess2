#!/bin/bash

MODE=debug
if [ "$1" == "r" ]
then
	MODE=release
fi

which qmake
if [ $? -ne 0 ]
then 
	echo "/usr/lib64/qt5/bin/qmake ?"
	exit 1
fi

#git reset --hard
#git pull

qmake VAEngine.pro -spec linux-g++-64 CONFIG+=$MODE
make qmake_all
make -k

qmake intellivix_iedge.pro -spec linux-g++-64 CONFIG+=$MODE
make qmake_all
make -k


TEMPLATE = subdirs   # for multi projects.
CONFIG += ordered    # sequenced build.

SUBDIRS = \
   BCCL \
   VACL \

VACL.depends = BCCL

# genernal settings for projects. 
OTHER_FILES += env.pri

﻿#ifndef INNERTHREAD_H
#define INNERTHREAD_H

#include <thread>
#include <chrono>

class InnerThread {
public:
   static void setThreadName( const std::string& name );

public:
   InnerThread() = default;
   virtual ~InnerThread();

   void SeveThreadName( const std::string& thread_name );
   virtual bool StartThread( void* pInstance );
   virtual void StopThread();

protected:
   virtual void ThreadLoop() = 0;
   virtual void ThreadApplyName();
   void Sleep_ms( unsigned nMillisecs );

   std::string m_thread_name;

private:
   std::thread* m_thread = nullptr;
};

#endif // INNERTHREAD_H

#include "StdAfx.h"
#include "WaitForMultipleObjects.h"
#include "EventEx.h"

CWaitForMultipleObjects::~CWaitForMultipleObjects()
{
   ClearAllEventEx();
}

void CWaitForMultipleObjects::Init( const std::vector<CEventEx*>& vecEvent )
{
   std::lock_guard<std::mutex> mlk( m_mutex_WMO );

   m_vecEvent = vecEvent;

   for( size_t i = 0; i < m_vecEvent.size(); i++ ) {
      CEventEx* pEvt = m_vecEvent[i];
      std::lock_guard<std::mutex> lk( pEvt->m_mutex );
      pEvt->m_pWMO = this;
   }

   m_vecSignal.resize( m_vecEvent.size() );
   for( size_t i = 0; i < m_vecSignal.size(); i++ )
      m_vecSignal[i] = false;
}

bool CWaitForMultipleObjects::RemoveEventEx( CEventEx* pEvt )
{
   std::lock_guard<std::mutex> mlk( m_mutex_WMO );

   std::vector<CEventEx*>::iterator it;
   it = find( m_vecEvent.begin(), m_vecEvent.end(), pEvt );
   if( it == m_vecEvent.end() )
      return false;

   {
      std::lock_guard<std::mutex> lk( pEvt->m_mutex );
      pEvt->m_pWMO = nullptr;
   }

   m_vecEvent.erase( it );

   m_vecSignal.resize( m_vecEvent.size() );
   for( size_t i = 0; i < m_vecSignal.size(); i++ )
      m_vecSignal[i] = false;

   return true;
}

void CWaitForMultipleObjects::ClearAllEventEx()
{
   std::lock_guard<std::mutex> mlk( m_mutex_WMO );

   for( size_t i = 0; i < m_vecEvent.size(); i++ ) {
      CEventEx* pEvt = m_vecEvent[i];
      std::lock_guard<std::mutex> lk( pEvt->m_mutex );
      pEvt->m_pWMO = nullptr;
   }

   m_vecEvent.clear();
   m_vecSignal.clear();
}

size_t CWaitForMultipleObjects::CheckSignal()
{
   for( size_t i = 0; i < m_vecEvent.size(); i++ ) {
      CEventEx* pEvent = m_vecEvent[i];
      if( pEvent == nullptr ) {
         LOGF << "Timer[" << i << "] is nullptr.";
         continue;
      }

      if( pEvent->GetStatus() ) {
         pEvent->Wait_For( 0 );
         m_vecSignal[i] = true;
      }
   }
   return GetSignalCount();
}

std::vector<bool>& CWaitForMultipleObjects::GetSignal()
{
   return m_vecSignal;
}

uint CWaitForMultipleObjects::GetSignalCount()
{
   uint nCount = 0;
   for( size_t i = 0; i < m_vecSignal.size(); i++ ) {
      if( m_vecSignal[i] ) nCount++;
   }
   return nCount;
}

void CWaitForMultipleObjects::Notify()
{
   m_cv.notify_all();
}

bool CWaitForMultipleObjects::WaitForMultipleObjects( bool bWaitAll, uint32_t timeout_ms )
{
   for( size_t i = 0; i < m_vecSignal.size(); i++ )
      m_vecSignal[i] = false;

   bool bTimeOut = true;
   if( timeout_ms == INFINITE ) {
      if( bWaitAll ) {
         std::unique_lock<std::mutex> lk( m_mutex_WMO );
         m_cv.wait( lk, [this] {
            return ( CheckSignal() == m_vecEvent.size() );
         } );
      } else {
         std::unique_lock<std::mutex> lk( m_mutex_WMO );
         m_cv.wait( lk, [this] {
            return ( CheckSignal() > 0 );
         } );
      }
   } else {
      if( bWaitAll ) {
         std::unique_lock<std::mutex> lk( m_mutex_WMO );
         bTimeOut = m_cv.wait_for( lk, std::chrono::milliseconds( timeout_ms ), [this] {
            return ( CheckSignal() == m_vecEvent.size() );
         } );
      } else {
         std::unique_lock<std::mutex> lk( m_mutex_WMO );
         bTimeOut = m_cv.wait_for( lk, std::chrono::milliseconds( timeout_ms ), [this] {
            return ( CheckSignal() > 0 );
         } );
      }
   }

   return bTimeOut;
}

   //------------------------------------------------------------------------------------------

#if 0
std::mutex cv_m;
CEventEx evt_1;
CEventEx evt_2;
CEventEx evt_3;

void waits()
{
   std::cerr << "Waiting... \n";

   CWaitForMultipleObjects wmo;
   std::vector<CEventEx*> vecEvt;
   vecEvt.push_back(&evt_1);
   vecEvt.push_back(&evt_2);
   vecEvt.push_back(&evt_3);

   while (1)
   {
      bool bTimeOut = wmo.WaitForMultipleObjects(vecEvt, false, 3000);
      {
         std::vector<bool> vecSignal = wmo.GetSignal();
         std::lock_guard<std::mutex> lk(cv_m);
         std::cerr << "timeout : " << bTimeOut << " Count : " << wmo.GetSignalCount() << "\n";
         std::cerr << "Process... Index : ";
         for (int ii = 0; ii < (int)vecSignal.size(); ii++)
         {
            std::cerr << "[" << ii << " " << vecSignal[ii] << "] ";
         }
         std::cerr << " \n";
      }
   }

   std::cerr << "...finished waiting.\n";
}

void signals_1()
{
   for (int ii = 0; ii < 100; ii++)
   {
      std::this_thread::sleep_for(std::chrono::seconds(5));
      {
         std::lock_guard<std::mutex> lk(cv_m);
         std::cerr << "Notifying 0...\n";
      }
      evt_1.SetEvent(true);
   }
}

void signals_2()
{
   for (int ii = 0; ii < 100; ii++)
   {
      std::this_thread::sleep_for(std::chrono::seconds(2));
      {
         std::lock_guard<std::mutex> lk(cv_m);
         std::cerr << "Notifying 1...\n";
      }
      evt_2.SetEvent(true);
   }
}

void signals_3()
{
   for (int ii = 0; ii < 100; ii++)
   {
      std::this_thread::sleep_for(std::chrono::seconds(7));
      {
         std::lock_guard<std::mutex> lk(cv_m);
         std::cerr << "Notifying 2...\n";
      }
      evt_3.SetEvent(true);
   }
}

int main()
{
   std::thread t1(signals_1), t2(signals_2), t3(signals_3), t4(waits);
   t1.join();
   t2.join();
   t3.join();
   t4.join();

   std::cerr << "End...\n";

   getchar();

   return 0;
}
#endif

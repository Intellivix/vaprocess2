﻿#ifndef BASEDIRMANAGER_H
#define BASEDIRMANAGER_H

#include "bccl_plog.h"

#include <array>
#include <cstdlib>
#include <unistd.h>

class BaseDirManager {
public:
   static BaseDirManager& Get(); // SingleTon.

public:
   static bool is_existed_dir( const std::string& dir );

public:
   const std::string& get_multil_proces_id();
   std::string get_excutable_dir();
   const std::string& get_base_dir();
   bool set_multil_proces_id( const std::string& sub_dir );

private:
   BaseDirManager();

private:
   std::string m_multi_process_id;
   std::string m_base_dir;
};

#endif // BASEDIRMANAGER_H

#pragma once

// CSolarCalculator
// 출처: http://www.codeproject.com/KB/recipes/SolarCalculator.aspx
//       C# 코드를 C++ 코드로 이식함.

#ifdef __SUPPORT_SOLAR_CALCULATOR
class CSolarCalculator {
public:
   double m_dfLongitude;
   double m_dfLatitude;
   double m_dfLongitudeTimeZone;
   bool m_bUseSummerTime;

public:
   double m_dfLatitudeInRadians;

public:
   CSolarCalculator();
   CSolarCalculator( double dfLongitude, double dfLatitude, double dfLongitudeTimeZone, bool dfUseSummerTime );

public:
   void Initialize( double dfLongitude, double dfLatitude, double dfLongitudeTimeZone, bool dfUseSummerTime );
   BOOL Read( CXMLIO* pIO );
   BOOL Write( CXMLIO* pIO );

public:
   BCCL::Time CalculateSunRise( BCCL::Time dateTime );
   BCCL::Time CalculateSunSet( BCCL::Time dateTime );
   double CalculateMaximumSolarRadiation( BCCL::Time dateTime );
   double CalculateDeclination( int numberOfDaysSinceFirstOfJanuary );
   int ExtractDayNumber( BCCL::Time dateTime );
   BCCL::Time CreateDateTime( BCCL::Time dateTime, int timeInMinutes );
   int CalculateSunRiseInternal( double tanSunPosition, double differenceSunAndLocalTime );
   int CalculateSunSetInternal( double tanSunPosition, double differenceSunAndLocalTime );
   double CalculateTanSunPosition( double declanationOfTheSun );
   double CalculateCosSunPosition( double declanationOfTheSun );
   double CalculateSinSunPosition( double declanationOfTheSun );
   double CalculateDifferenceSunAndLocalTime( int dayNumberOfDateTime );
   double LimitTanSunPosition( double tanSunPosition );
   int LimitSunSet( int sunSet );
   int LimitSunRise( int sunRise );
   double ConvertDegreeToRadian( double degree );
   double CalculateMaximumSolarRadiationInternal( double sinSunHeight, double sunCorrection );
   int GetNumberOfMinutesThisDay( BCCL::Time dateTime, double differenceSunAndLocalTime );
};
#endif // __SUPPORT_SOLAR_CALCULATOR

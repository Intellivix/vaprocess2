﻿#pragma once

#include <map>
#include <thread>
#include <vector>

#include "EventEx.h"
#include "TimerEx.h"

class CTimerManager;

class CMMTimer {
public:
   uint m_nTimerID;
   uint m_nTickCount; // 현재 타이머 카운트
   uint m_nPeriod; // 이벤트 발생 주기 = int (1000 / m_dfFPS)
   float m_dfFPS;
   bool m_bStopTimer; // 타이머를 멈출것인가?
   CEventEx m_evtTimer;

   CTimerManager* m_pTimerManager = nullptr;

   std::string m_szName;

public:
   CMMTimer();
   ~CMMTimer();

public:
   bool Wait();
   uint GetPeriod();
   float GetFPS();
   CEventEx& GetTimerEvent();
   void SetMilliSec( uint millisec );
   void SetFPS( float fFPS );
   void StartTimer();
   void StopTimer();

private:
   void SetTimer( uint period ); // triggered by SetPeriod or SetPeriod;
};

//////////////////////////////////////////////////////////////////////////
//
// class CTimerManager
//
//////////////////////////////////////////////////////////////////////////

using timer_ex::TimerIdEx;

class CTimerManager {
public:
   enum State {
      State_Activate = 0,
      State_Signal_Deactivate,
      State_Thread_TimeProc,
      State_StartTimer,
      State_Num,
   };

private:
   std::thread m_WorkThread;
   std::vector<bool> m_Status;
   uint m_MinResolution = 1;
   uint m_TimerIdAllocate = 0;

protected:
   CCriticalSection m_csVecTimers;
   std::vector<CMMTimer*> m_vecTimers;

private:
   static int Thread_TimerProc( CTimerManager* instance );

   virtual int ProcWork();

   virtual int OnActivate();
   virtual int OnDeactivate();
   virtual int OnCreateTimer( CMMTimer* timer );
   virtual int OnDeleteTimer( CMMTimer* timer );

   void SetStatus( State index, bool value );
   void SendEvent();

protected:
   uint GetDivisor( uint nNumber );
   void TimerEvent( void* ptr );

public:
   CTimerManager();
   virtual ~CTimerManager();

   int Activate();
   int Deactivate();

   void SetMinResolution( uint ms );
   uint GetMinResolution();
   bool GetStatus( State index );

   bool StartTimer( uint id );
   bool StopTimer( uint id );
   bool DeleteTimer( uint id );
   void ClearTimer();

   CMMTimer* GetNewTimer( const std::string& name = "" );

   virtual uint OnSetPeriod( CMMTimer* timer, uint before_period );
};

//////////////////////////////////////////////////////////////////////////
//
// class CTimerManagerWmo
//
//////////////////////////////////////////////////////////////////////////

class CTimerManagerWmo : public CTimerManager {
private:
   class CTimerItem;

   std::map<uint, CTimerItem*> m_mapTimer;
   CCriticalSection m_csMapTimer;

private:
   virtual int ProcWork();

   void GetEventTimer( std::vector<CEventEx*>& vecEvt, std::vector<CTimerItem*>& vecTimer );
   void SendEvent( CTimerItem* pTimerItem );
   void CheckRefCountTimer();
   void ClearMapItem();
   CTimerItem* FindTimer( uint nPeriod, bool bDivisor );

public:
   CTimerManagerWmo();
   virtual ~CTimerManagerWmo();

   virtual uint OnSetPeriod( CMMTimer* timer, uint before_period );
};

//////////////////////////////////////////////////////////////////////////
//
// class CTimerManagerDirect
//
//////////////////////////////////////////////////////////////////////////

class CTimerManagerDirect : public CTimerManager {
private:
   class CTimerList;
   CEventEx m_evtTimerList;
   std::map<uint, CTimerList*> m_mapTimerList;

private:
   virtual int ProcWork();
   virtual int OnDeactivate();
   virtual int OnDeleteTimer( CMMTimer* timer );

   int CreateTimerList( CMMTimer* pMMTimer );
   void DeleteTimerList( CMMTimer* pMMTimer );
   void DeleteTimerList( uint id );
   void CheckRefCountTimer();
   void ClearTimerList();

public:
   CTimerManagerDirect();
   virtual ~CTimerManagerDirect();

   virtual uint OnSetPeriod( CMMTimer* timer, uint before_period );
};

//////////////////////////////////////////////////////////////////////////
//
// class CTimerManagerSP
//
//////////////////////////////////////////////////////////////////////////

class CTimerManagerSP {
private:
   class CTimerManagerImpl;

   CTimerManager* m_pTimerManager = nullptr;

public:
   enum Active_Type {
      /**
      - 기존 타이머 구동 방식
      - 1ms 로 동작하는 정확한 해상도 타이머 1개를 생성하여 콜백 함수를 등록 한다.
        등록된 콜백 함수에서는 Thread 에게 Signal 을 전송하고 Thread 에서는 Signal 수신 시에
        Count 값을 증가 시키면서 그 시간에 맞는 타이머 객체 모두에게 Signal 을 전송 한다.
      */
      Type_Original = 0,
      /**
      - 1보다 큰 최소 공약수를 갖는 타이머를 여러개 만들어서 사용 한다.
      - 타이머 객체의 생성 : FPS 를 설정 할때 결정한다.
      - 타이머 객체의 삭제 :  Ref count 가 다 사용 되었을 시에 Thread 에서 한다.
      - 타이머에 콜백 함수를 등록하여 해당 콜백 함수가 호출 될 시에 Thread 에 Signal 을 전송 한다.
        Thread 에서는 여러 타이머 콜백 함수의 Event signal 을 수신 받아 해당 타이머 객체 모두에게
        Signal 을 전송
      */
      Type_Wmo,
      /**
      - 1보다 큰 최소 공약수를 갖는 타이머를 여러개 만들어서 사용 한다.
      - 타이머 객체의 생성/삭제 : Thread 에서 관리 한다.
      - 타이머에 콜백 함수를 등록하여 해당 콜백 함수가 호출 될 시에
        최소 공약수의 주기를 갖는 타이머 객체 모두에게 Signal 을 전송 한다.
      */
      Type_Direct,
   };

   CTimerManagerSP();
   ~CTimerManagerSP();

   static void Activate( Active_Type type );

   CTimerManager* Get()
   {
      return m_pTimerManager;
   }

   CTimerManager* operator->()
   {
      return m_pTimerManager;
   }
};

#include "StdAfx.h"
#include "BitrateChecker.h"

#if !defined( ASSERT )
#define ASSERT( f )
#endif

////////////////////////////////////////////////////////////////////////
//
// class CBitrateCheckItem
//
////////////////////////////////////////////////////////////////////////

CBitrateCheckItem::CBitrateCheckItem()
{
   m_nTime = 0;
}

////////////////////////////////////////////////////////////////////////
//
// class CBitrateChecker
//
////////////////////////////////////////////////////////////////////////

CBitrateChecker::CBitrateChecker()
{
   m_nStartIdx = 0;
   m_nEndIdx   = -1;
   m_nItemNum  = 0;
   m_pItems    = NULL;
}

CBitrateChecker::~CBitrateChecker()
{
   if( m_pItems )
      delete[] m_pItems;
}

void CBitrateChecker::Initialize( float fFrameRate, float fLogPeriod )
{
   m_nFramePeriod = int( ceil( 1000.0f / fFrameRate ) );
   m_nLogPeriod   = int( ceil( fLogPeriod * 1000.0f ) );

   m_nMaxItemNum = int( ceil( fFrameRate * fLogPeriod ) );
   if( m_pItems )
      delete[] m_pItems;
   m_pItems = new CBitrateCheckItem[m_nMaxItemNum];
}

int CBitrateChecker::GetItemIdx( int nCurIdx, int nDeltaIdx )
{
   int nNextIdx = ( nCurIdx + nDeltaIdx ) % m_nMaxItemNum;
   if( nNextIdx < 0 )
      nNextIdx += m_nMaxItemNum;
   return nNextIdx;
}

void CBitrateChecker::AddItem( CBitrateCheckItem& item )
{
   if( m_pItems ) {
      m_nEndIdx = GetItemIdx( m_nEndIdx, 1 );
      if( m_nItemNum < m_nMaxItemNum ) {
         m_nItemNum++;
      } else {
         m_nStartIdx = GetItemIdx( m_nStartIdx, 1 );
      }
      item.m_nTime        = GetTickCount();
      m_pItems[m_nEndIdx] = item;
   }
}

int CBitrateChecker::FindItemIdx( int nDeltaTime )
{
   // 현재 시간으로 부터 nDeltaTime 만큼 과거에 있는 항목을 찾아라.
   int nFindIdx = -1;
   if( m_nItemNum > 0 ) {
      int nDeltaIdx = nDeltaTime / m_nFramePeriod;

      BOOL bFindOlderItem = TRUE;
      if( nDeltaIdx < m_nItemNum - 1 ) {
         nFindIdx = GetItemIdx( m_nEndIdx, -nDeltaIdx );
      } else {
         nFindIdx       = m_nStartIdx;
         bFindOlderItem = FALSE;
      }
   }

   return nFindIdx;
}

CBitrateCheckItem* CBitrateChecker::GetLastItem()
{
   CBitrateCheckItem* pItem = NULL;
   if( m_nItemNum > 0 ) {
      pItem = &m_pItems[m_nEndIdx];
   }
   return pItem;
}

float CBitrateChecker::CalcAvgBitrate( float fDeltaTimeInSec )
{
   float fAvgBitrate = 1.0f;

   double dfTotalDataSizeInByte = 0.0f;

   int i, c;

   uint32_t nCurTime   = GetTickCount();
   uint32_t nTimeLimit = nCurTime - uint32_t( fDeltaTimeInSec * 1000 );

   if( m_nItemNum == 1 ) {
      CBitrateCheckItem& sItemCurr = m_pItems[0];
      fAvgBitrate                  = float( sItemCurr.m_nDataSizeInByte );
   } else if( m_nItemNum >= 2 ) {
      for( c = 0, i = m_nEndIdx; c < m_nItemNum; c++, i-- ) {
         if( i < 0 )
            i = m_nMaxItemNum - 1;
         ASSERT( 0 <= i && i < m_nItemNum );
         CBitrateCheckItem& sItemCurr = m_pItems[i];
         if( nTimeLimit > sItemCurr.m_nTime ) {
            break;
         }
         dfTotalDataSizeInByte += sItemCurr.m_nDataSizeInByte;
      }
      if( c > 0 ) {
         fAvgBitrate = float( dfTotalDataSizeInByte / fDeltaTimeInSec ) * 8;
      }
   }
   return fAvgBitrate;
}

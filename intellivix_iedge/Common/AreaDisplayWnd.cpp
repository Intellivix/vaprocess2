﻿// DisplayWnd_Area.cpp : 구현 파일입니다.
//

#ifdef WIN32

#include "StdAfx.h"
#include "AreaDisplayWnd.h"
#include "OnscreenDisplayEx.h"

// CAreaDisplayWnd

CAreaDisplayWnd::CAreaDisplayWnd()
{
   m_nOptions = 0;
}

CAreaDisplayWnd::~CAreaDisplayWnd()
{
}

BEGIN_MESSAGE_MAP( CAreaDisplayWnd, CImgDisplayWnd )
END_MESSAGE_MAP()

BOOL CAreaDisplayWnd::Create( CRect& rect, CWnd* parent_wnd, BGRImage& s_image )

{
   if( !CImgDisplayWnd::Create( rect, parent_wnd, s_image ) )
      return FALSE;

   m_ToolTipCtrl.Create( this, TTS_ALWAYSTIP );
   m_ToolTipCtrl.AddTool( this, "" );
   return TRUE;
}

// CAreaDisplayWnd 메시지 처리기입니다.
void CAreaDisplayWnd::RenderSub( CDC* dc )

{
   m_GraphicObjectList.Draw( dc );

   if( m_GraphicObjectList.m_nDrawAreaTypes & ( ADW_OPTION_EVENT_ZONES | ADW_OPTION_PRESET_ZONES ) ) {
      //경계선과 방향 그리기
      FPoint2D scale;
      scale.X = (float)Width / REF_IMAGE_WIDTH;
      scale.Y = (float)Height / REF_IMAGE_HEIGHT;

      ISize2D size;
      size.Width  = Width;
      size.Height = Height;
      COnscreenDisplayEx od;
      od.SetScreenSize( size );

      CFont font, *old_font;
      //font.CreatePointFont (90,"Tahoma");
      old_font     = dc->SelectObject( &font );
      POSITION pos = m_GraphicObjectList.GetHeadPosition();
      while( pos ) {
         CGraphicObject* g_object = m_GraphicObjectList.GetNext( pos );
         if( m_GraphicObjectList.m_nDrawAreaTypes & ( 0x00000001 << g_object->m_nAreaType ) ) {
            if( g_object->m_nAreaType == AREA_TYPE_EVENT_ZONE ) {
               EventZone* evt_zone = (EventZone*)g_object->m_pVACPolygon;
               od.DisplayEventZone( dc, evt_zone, scale );
            }
         }
      }
      dc->SelectObject( &old_font );
   }

#ifdef __ENABLE_MOVING_POLYGON_VERTEX

   auto s_obj_ptr = m_GraphicObjectList.GetSelectedObject();
   if( s_obj_ptr && s_obj_ptr->m_nShape == SHAPE_POLYGON ) {
      auto poly_ptr = (CPolygon*)s_obj_ptr;
      poly_ptr->DrawVertices( dc );
   }

#endif // __ENABLE_MOVING_POLYGON_VERTEX

   if( IS_SUPPORT( __SUPPORT_HEAD_COUNTING ) ) {
      m_GraphicHeadList.Draw_Head( dc );

      if( m_GraphicHeadList.m_nDrawAreaTypes & ( ADW_OPTION_EVENT_ZONES ) ) {
         //경계선과 방향 그리기
         FPoint2D scale;
         scale.X = (float)Width / REF_IMAGE_WIDTH;
         scale.Y = (float)Height / REF_IMAGE_HEIGHT;

         ISize2D size;
         size.Width  = Width;
         size.Height = Height;
         COnscreenDisplayEx od;
         od.SetScreenSize( size );

         CFont font, *old_font;
         //font.CreatePointFont (90,"Tahoma");
         old_font     = dc->SelectObject( &font );
         POSITION pos = m_GraphicHeadList.GetHeadPosition();
         while( pos ) {
            CGraphicObject* g_object = m_GraphicHeadList.GetNext( pos );
            if( m_GraphicHeadList.m_nDrawAreaTypes & ( 0x00000001 << g_object->m_nAreaType ) ) {
               if( g_object->m_nAreaType == AREA_TYPE_HEAD_ZONE ) {
                  EventZone* evt_zone = (EventZone*)g_object->m_pVACPolygon;
                  od.DisplayEventZone( dc, evt_zone, scale );
               }
            }
         }
         dc->SelectObject( &old_font );
      }
   }
}

int GetLeftTopVertexIndex( IP2DArray1D& Vertices )
{
   int nLeftTopVertexIndex = 0;
   int i;
   IPoint2D* lt_point = &Vertices[0];
   for( i = 1; i < Vertices.Length; i++ ) {
      IPoint2D& point = Vertices[i];
      if( point.X <= lt_point->X && point.Y <= lt_point->Y ) {
         lt_point            = &point;
         nLeftTopVertexIndex = i;
      }
   }
   return nLeftTopVertexIndex;
}

int GetRightBottomVertexIndex( IP2DArray1D& Vertices )
{
   int nRightBottomVertexIndex = 0;
   int i;
   IPoint2D* rb_point = &Vertices[0];
   for( i = 1; i < Vertices.Length; i++ ) {
      IPoint2D& point = Vertices[i];
      if( point.X >= rb_point->X && point.Y >= rb_point->Y ) {
         rb_point                = &point;
         nRightBottomVertexIndex = i;
      }
   }
   return nRightBottomVertexIndex;
}

void CAreaDisplayWnd::AddGraphicObjectFromIVSPolygon( BCCL::Polygon* polygon, int nAreaType )
{
   int nShape;
   if( polygon->Vertices.Length != 4 || polygon->Vertices[0].Y != polygon->Vertices[1].Y || polygon->Vertices[2].Y != polygon->Vertices[3].Y || polygon->Vertices[0].X != polygon->Vertices[3].X || polygon->Vertices[1].X != polygon->Vertices[2].X ) {
      nShape = SHAPE_POLYGON;
   } else {
      nShape = SHAPE_RECTANGLE;
   }
   if( nShape == SHAPE_POLYGON ) {
      int num_points = polygon->Vertices.Length;
      CPointArray points;
      points.SetSize( num_points );
      CPoint s_point;
      for( int i = 0; i < num_points; i++ ) {
         s_point   = CPoint( polygon->Vertices[i].X, polygon->Vertices[i].Y );
         points[i] = STDMapToWnd( s_point );
      }
      CPolygon* pPolygon = new CPolygon;
      pPolygon->SetShape( nShape );
      pPolygon->SetAreaType( nAreaType );
      pPolygon->SetPoints( points );
      pPolygon->SetIVSPolygon( polygon );
      m_GraphicObjectList.AddTail( pPolygon );
      points.RemoveAll();
   } else if( nShape == SHAPE_RECTANGLE ) {
      CRect d_rect;
      int iLT         = GetLeftTopVertexIndex( polygon->Vertices );
      int iRB         = GetRightBottomVertexIndex( polygon->Vertices );
      CPoint point_lt = CPoint( polygon->Vertices[iLT].X, polygon->Vertices[iLT].Y ); //top-left
      CPoint point_rb = CPoint( polygon->Vertices[iRB].X, polygon->Vertices[iRB].Y ); //right-bottom
      d_rect.SetRect( STDMapToWnd( point_lt ), STDMapToWnd( point_rb ) );
      CRectangle* pRectangle = new CRectangle;
      pRectangle->SetShape( nShape );
      pRectangle->SetAreaType( nAreaType );
      pRectangle->SetBoundRect( d_rect );
      pRectangle->SetIVSPolygon( polygon );
      m_GraphicObjectList.AddTail( pRectangle );
   } else if( nShape == SHAPE_ELLIPSE ) {
      int num_points = polygon->Vertices.Length;
      CPointArray points;
      points.SetSize( num_points );
      CPoint s_point;
      for( int i = 0; i < num_points; i++ ) {
         s_point   = CPoint( polygon->Vertices[i].X, polygon->Vertices[i].Y );
         points[i] = STDMapToWnd( s_point );
      }
      CCircle* pCircle = new CCircle;
      pCircle->SetShape( nShape );
      pCircle->SetAreaType( nAreaType );
      pCircle->SetPoints( points );
      pCircle->SetIVSPolygon( polygon );
      m_GraphicHeadList.AddTail( pCircle );
      points.RemoveAll();
   } else
      ASSERT( FALSE );
}

void CAreaDisplayWnd::GraphicObjectToIVSPolygon( CGraphicObject* g_object )
{
   BCCL::Polygon* polygon = g_object->m_pVACPolygon;
   ASSERT( polygon );
   CPointArray d_points;
   int num_points;
   if( g_object->m_nShape == SHAPE_POLYGON ) {
      CPointArray& s_points = ( (CPolygon*)g_object )->m_Points;
      num_points            = (int)s_points.GetSize();
      d_points.SetSize( num_points );
      for( int i     = 0; i < num_points; i++ )
         d_points[i] = WndToSTDMap( s_points[i] );
   } else if( g_object->m_nShape == SHAPE_RECTANGLE ) {
      CRect& s_rect = g_object->m_OutlineRect.m_rect;
      num_points    = 4;
      d_points.SetSize( num_points );
      d_points[0] = WndToSTDMap( CPoint( s_rect.left, s_rect.top ) );
      d_points[1] = WndToSTDMap( CPoint( s_rect.right, s_rect.top ) );
      d_points[2] = WndToSTDMap( CPoint( s_rect.right, s_rect.bottom ) );
      d_points[3] = WndToSTDMap( CPoint( s_rect.left, s_rect.bottom ) );
   }
   if( g_object->m_nShape == SHAPE_ELLIPSE ) {
      CPointArray& s_points = ( (CCircle*)g_object )->m_Points;
      num_points            = (int)s_points.GetSize();
      d_points.SetSize( num_points );
      for( int i     = 0; i < num_points; i++ )
         d_points[i] = WndToSTDMap( s_points[i] );
   }
   polygon->Create( num_points );
   CopyArray1D( (byte*)d_points.GetData(), 0, num_points, sizeof( CPoint ), (byte*)(IPoint2D*)polygon->Vertices, 0 );
   polygon->UpdateCenterPosition();
   d_points.RemoveAll();
}

BOOL CAreaDisplayWnd::PreTranslateMessage( MSG* pMsg )
{
   if( pMsg->message >= WM_MOUSEFIRST && pMsg->message <= WM_MOUSELAST )
      m_ToolTipCtrl.RelayEvent( pMsg );
   return CImgDisplayWnd::PreTranslateMessage( pMsg );
}

#endif // WIN32

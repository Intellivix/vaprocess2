﻿#pragma once

#ifdef WIN32
#include "AreaDisplayWnd.h"

// Messages
#define MSG_LBUTTONDOWN                                  (WM_USER + 101)
#define MSG_RBUTTONDOWN                                  (WM_USER + 102)
#define MSG_MOUSEMOVE							               (WM_USER + 103)
#define MSG_LBUTTONUP	                                 (WM_USER + 104)
#define MSG_KEYDOWN								               (WM_USER + 105)
#define MSG_RENDER                                       (WM_USER + 106)
#define MSG_SETUP_WIZARD_CLOSED                          (WM_USER + 107)
#define MSG_UPDATE_PARAM_HEAD                            (WM_USER + 108)
#define MSG_SAVE_PARAM_HEAD                              (WM_USER + 109)

enum ASW_MODE
{
   ASW_MODE_SELECT_EDIT			= 1,
   ASW_MODE_SPECIFY_POLYGON   = 2,
   ASW_MODE_SPECIFY_RECTANGLE	= 3,
   ASW_MODE_SPECIFY_CIRCLE    = 4
};

enum ASW_STATE
{
   ASW_STATE_DRAWING	= 0x00000001,
   ASW_STATE_SIZING  = 0x00000002,
   ASW_STATE_MOVING	= 0x00000004,
};

//////////////////////////////////////////////////////////////////////////////
//
// Class : CSpecWndMessageHandler
//
//////////////////////////////////////////////////////////////////////////////

class CSpecWndMessageHandler
{
public:
   virtual void SW_OnLButtonDown  (UINT flag,CPoint point) {}
   virtual void SW_OnLButtonUp	 (UINT flag,CPoint point) {}
   virtual void SW_OnRButtonDown  (UINT flag,CPoint point) {}
   virtual void SW_OnMouseMove    (UINT flag,CPoint point) {}
   virtual void SW_OnLButtonDblClk(UINT flag,CPoint point) {}
   virtual void SW_PreRenderSub   (CDC *dc) {}
};


//////////////////////////////////////////////////////////////////////////////
//
// Class : CAreaSpecWnd_Head
//
//////////////////////////////////////////////////////////////////////////////

class CAreaSpecWnd_Head : public CAreaDisplayWnd 
{
public:
   static int Options;
public:
   int Mode;
   int AreaType;
   CSpecWndMessageHandler* m_pMsgHandler;
   CCriticalSection m_csRender;

public:
   CAreaSpecWnd_Head (   );
   virtual ~CAreaSpecWnd_Head (   );

public:
   virtual void RenderSub (CDC *dc);

protected:
   afx_msg void OnLButtonDown  (UINT flag,CPoint point);
   afx_msg void OnLButtonUp	 (UINT flag,CPoint point);
   afx_msg void OnMouseMove    (UINT flag,CPoint point);

public:
   CPoint		m_StartPoint;
   int			m_nState;
   int			m_nResizingType;
   HCURSOR		m_hCursorSizing[ST_NUM];
   HCURSOR		m_hCursorMoving;

   BOOL        m_bDisplayFlag;

public:
   CPointArray				m_ConnectedLine;
   CGraphicObject*		m_pTempObject;
   CGraphicObject*		m_pOriginalObject;
   CGraphicObject*		m_pCompletedGObj;        // 설정이 완료된 CGraphicObject

   void        SetCirclePoint(CPoint sp, CPoint ep);
   void        GetCirclePoint(CPoint& sp, CPoint& ep);
   int         GetRadius();

   DECLARE_MESSAGE_MAP (   )
};
#endif

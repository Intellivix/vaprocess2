﻿#pragma once

#include <map>

class CIniParse
{
public:
   enum FILE_TYPE
   {
      FILE_TYPE_ANSI,
      FILE_TYPE_UTF8,
      FILE_TYPE_UNICODE,
   };

private:            
   //--------------------------------------------------------------------------------
   typedef std::map<std::string, std::string>   Map_Data;       // Key = Value 모음
   typedef std::map<std::string, Map_Data*>     Map_Section;    // <세션 이름, 하위 데이터>

   typedef Map_Data::const_iterator    Map_Data_Const_iter;
   typedef Map_Data::iterator          Map_Data_iter;
   typedef Map_Section::iterator       Map_Section_iter;
   
   Map_Section      m_mapSection;
   //--------------------------------------------------------------------------------

private:

   enum LINE_TYPE
   {
      LINE_TYPE_SECTION,      // 세션
      LINE_TYPE_DATA,         // KEY = VALUE
      LINE_TYPE_REMARK,       // 주석
      LINE_TYPE_EMPTY,        // 빈줄
      LINE_TYPE_SINGLE_LINE   // 의미 없는 그냥 줄
   };
      
   LPCTSTR SkipWhiteSpace(LPCTSTR p);   
   
   LINE_TYPE GetLineType(const std::string& strLine);   
   BOOL ParseSection(const std::string& strLine, std::string& strSection);
   BOOL ParseData(const std::string& strLine, std::string& strKey, std::string& strValue);

   Map_Data* FindSection(const std::string& strSection, BOOL bMake = FALSE);
   std::string FindData(Map_Data& mapData, const std::string& strKey);      

   FILE* FileOpen(const std::string& strFile, FILE_TYPE emType);

public:
   CIniParse();
   virtual ~CIniParse();        
   
   BOOL Open(const std::string& strFile, FILE_TYPE emType = FILE_TYPE_ANSI);
   BOOL stdOpen(const std::string& strFile, FILE_TYPE emType = FILE_TYPE_ANSI);
   void Clear();

   BOOL GetValue(const std::string& strSection, const std::string& strKey, std::string& value);
   BOOL GetValue(const std::string& strSection, const std::string& strKey, int& value);
};

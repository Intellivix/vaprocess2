#ifndef EVENTEX_H
#define EVENTEX_H

#include <mutex>
#include <condition_variable>

///////////////////////////////////////////////////////////////////////
//
// class CEventEx
//
///////////////////////////////////////////////////////////////////////
class CWaitForMultipleObjects;

class CEventEx {
public:
   explicit CEventEx() = default;
   ~CEventEx()         = default;

   void Wait();
   bool Wait_For( int ms );
   void SetEvent( bool bNotifyAll = true );
   void ResetEvent();
   uint GetStatus();

protected:
   uint m_status_count = 0;
   std::mutex m_mutex;
   std::condition_variable m_cv;
   
   friend class CWaitForMultipleObjects;
   CWaitForMultipleObjects* m_pWMO = nullptr;
};

#endif // EVENTEX_H
﻿#pragma once

#ifdef WIN32

#include "ImgDisplayWnd.h"
#include "GraphicObject.h"
#include <afxcmn.h>

// CAreaDisplayWnd
#define MSG_UPDATE_GRAPHICOBJECTS                     (WM_USER + 107)
#define MSG_LBUTTON_DBLCLICKED                        (WM_USER + 108)

// enum ADW_OPTION 은   GraphicObject.h 파일의 enum AREA_TYPE과 의 각 항목과 일대일 대응관계에 있어야한다. 


enum DWA_OPTION
{
   DWA_OPTION_NOTIFY_DOUBLECLICK = 0x00000001
};

//////////////////////////////////////////////////////////////////////////////
//
// Class : CAreaDisplayWnd
//
//////////////////////////////////////////////////////////////////////////////

class CAreaDisplayWnd : public CImgDisplayWnd
{
public:
   CAreaDisplayWnd();
   virtual ~CAreaDisplayWnd();

public:
   virtual void RenderSub (CDC *dc);
   virtual BOOL Create    (CRect& rect,CWnd *parent_wnd,BGRImage &s_image);

public:
   int                  m_nOptions;
   CGraphicObjectList	m_GraphicObjectList;
   CToolTipCtrl         m_ToolTipCtrl;

   CGraphicObjectList	m_GraphicHeadList;

public:
   void AddGraphicObjectFromIVSPolygon(BCCL::Polygon* polygon, int nAreaType);
   void GraphicObjectToIVSPolygon(CGraphicObject* g_object);

protected:
   virtual BOOL PreTranslateMessage(MSG* pMsg);
   DECLARE_MESSAGE_MAP()
};

#endif // WIN32

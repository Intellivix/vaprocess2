#pragma once

#ifdef __WIN32
#include "afxtempl.h"
#include "bccl.h"
 
////////////////////////////////////////////////////////////////////////////////

template <class T>
class CTemplateList : public CTypedPtrList<CPtrList, T*>
{
public:
   virtual ~CTemplateList();

public:
	T*	  Seek(int nIndex);
	int  GetIndex(T* sItem);
	void ClearList();
};

template <class T>
CTemplateList<T>::~CTemplateList()
{
	ClearList();
}

template <class T>
void CTemplateList<T>::ClearList()
{
	POSITION pos = GetHeadPosition();
	while(pos)
	{
		T* cItem = (T*)GetNext(pos);
		delete cItem;
	}
	RemoveAll();
}

template <class T>
T* CTemplateList<T>::Seek(int nIndex)
{
	POSITION pos = FindIndex(nIndex);
	if(pos)
		return (T*)GetAt(pos);
	else
		return NULL;
}

template <class T>
int CTemplateList<T>::GetIndex(T* sItem)
{
	int index = 0;
	POSITION pos = GetHeadPosition();
	while(pos) {
		T* cItem = (T*)GetNext(pos);
		if(cItem == sItem) 
			return index;
		index++;
	}
	return -1;
}

////////////////////////////////////////////////////////////////////////////////

template <class T>
class CTemplateListEx : public CTemplateList<T>
{
public:
   virtual int  Write (FileIO& IO);
	virtual int  Read  (FileIO& IO);
   virtual int  Write (CXMLIO* pIO, LPCTSTR szNodeName);
   virtual int  Read  (CXMLIO* pIO, LPCTSTR szNodeName);
};

template <class T>
int CTemplateListEx<T>::Write (FileIO& IO)
{
   int num = GetCount();
   if (IO.Write((byte*)&num, 1, sizeof(num)))   return (1);

   POSITION pos = GetHeadPosition();
   while(pos)
   {
      T* cItem = (T*)GetNext(pos);
      if(cItem->Write(IO))
         return (2);
   }
   return (DONE);
}

template <class T>
int CTemplateListEx<T>::Read (FileIO& IO)
{
   ClearList();

   int num;
   if (IO.Read((byte*)&num, 1, sizeof(num))) return (1);

   for(int i = 0 ; i < num ; i++)
   {
      T* cItem = new T;
      if(cItem->Read(IO))
      {
         delete cItem;
         return (2);
      }
      AddTail(cItem);
   }
   return (DONE);
}


template <class T>
int CTemplateListEx<T>::Write (CXMLIO* pIO, LPCTSTR szNodeName)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start (szNodeName))
   {
      int nLen = GetCount();
      xmlNode.Attribute (TYPE_ID (int), "Len", &nLen, 1);

      POSITION pos = GetHeadPosition();
      while(pos)
      {
         T* cItem = (T*)GetNext(pos);
         cItem->Write(pIO);
      }
      xmlNode.End ();
   }
   return (DONE);
}

template <class T>
int CTemplateListEx<T>::Read (CXMLIO* pIO, LPCTSTR szNodeName)
{
   ClearList();

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start (szNodeName))
   {
      int nLen;
      xmlNode.Attribute (TYPE_ID (int), "Len", &nLen);

      for(int i = 0 ; i < nLen ; i++)
      {
         T* cItem = new T;
         if(DONE == cItem->Read(pIO))
            AddTail(cItem);
         else
            delete cItem;
      }
      xmlNode.End ();
   }
   return (DONE);
}

////////////////////////////////////////////////////////////////////////////////

template <class T>
class CTemplateArray : public CTypedPtrArray<CPtrArray, T*>
{
public:
   virtual ~CTemplateArray();

public:
	void ClearArray();
};

template <class T>
CTemplateArray<T>::~CTemplateArray()
{
	ClearArray();
}

template <class T>
void CTemplateArray<T>::ClearArray()
{
	for(int i = 0 ; i < GetCount() ; i++){
		T* cItem = (T*)GetAt(i);
		delete cItem;
	}
	RemoveAll();
}

typedef CList<int>		CIntList;
typedef CArray<int,int> CIntArray;

////////////////////////////////////////////////////////////////////////////////////

template <class T>
class CListPtrs : public CList<T*>
{
public:
	virtual ~CListPtrs();

public:
	BOOL Delete(T* cItem, BOOL bDelPtr = TRUE);
	void DeleteAll();
};

template <class T>
CListPtrs<T>::~CListPtrs()
{
	DeleteAll();
}

template <class T>
void CListPtrs<T>::DeleteAll()
{
	POSITION pos = GetHeadPosition();
	while(pos)
	{
		T* cItem = (T*)GetNext(pos);
		delete cItem;
	}
	RemoveAll();
}

template <class T>
BOOL CListPtrs<T>::Delete(T* cItem, BOOL bDelPtr)
{
	POSITION pos = Find(cItem);
	if (!pos) return FALSE;
	if (bDelPtr) delete cItem;
	RemoveAt(pos);
	return TRUE;
}

////////////////////////////////////////////////////////////////////////////////////

template <class T>
class CArrayPtrs : public CArray<T*>
{
public:
	virtual ~CArrayPtrs();

public:
	void DeleteAll();
};

template <class T>
CArrayPtrs<T>::~CArrayPtrs()
{
	DeleteAll();
}

template <class T>
void CArrayPtrs<T>::DeleteAll()
{
	for(int i = 0 ; i < GetCount() ; i++){
		T* cItem = (T*)GetAt(i);
		delete cItem;
	}
	RemoveAll();
}

#endif


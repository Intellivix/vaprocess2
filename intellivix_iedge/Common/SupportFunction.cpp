﻿
#include "StdAfx.h"
#include "SupportFunction.h"

//--------------------------------------------------------------------------------------//

BOOL IsSupport( const char* szType )
{
   CSupportFunction* pFunc = CSupportFunction::GetInstance();
   SUPPORT_ITEM sItem( szType );
   return pFunc->IsSupport( sItem );
}

BOOL IsProduct( const char* szType )
{
   CSupportFunction* pFunc = CSupportFunction::GetInstance();
   SUPPORT_ITEM sItem( szType );
   return pFunc->IsSupport( sItem );
}

int GetProdVer()
{
   CSupportFunction* pFunc = CSupportFunction::GetInstance();

   return pFunc->GetProdVer();
}

std::string GetProdCode()
{
   CSupportFunction* pFunc = CSupportFunction::GetInstance();

   return pFunc->GetProdCode();
}

//--------------------------------------------------------------------------------------//

#define MAP_ADD( type )                        \
   {                                           \
      CSupportFunction::MapAdd( _T( #type ) ); \
   }
#define MAP_ERASE( type )                        \
   {                                             \
      CSupportFunction::MapErase( _T( #type ) ); \
   }
#define SET_PROD_CODE( type )                  \
   {                                           \
      CSupportFunction::MapAdd( _T( #type ) ); \
   }

CSupportFunction* CSupportFunction::m_pThis = NULL;

CSupportFunction* CSupportFunction::GetInstance()
{
   if( NULL == m_pThis ) {
      m_pThis = new CSupportFunction;
   }

   return m_pThis;
}

void CSupportFunction::FreeInstance()
{
   if( m_pThis ) {
      delete m_pThis;
      m_pThis = NULL;
   }
}

CSupportFunction::CSupportFunction()
{
   m_iProdVer = 0;
}

CSupportFunction::~CSupportFunction()
{
}

int CSupportFunction::Initialize( const std::string& strProdCode, int iProdVer, int iProdType )
{
   CCriticalSectionSP co( m_csLock );
   m_mapFunction.clear();

   m_strProdCode = strProdCode;
   m_iProdVer    = iProdVer;

   if( ProdType_Evaluation_Edition == iProdType ) {
      MAP_ADD( __EDITION_EVALUATION );
      MAP_ADD( __EVAL_LOCK_TIMEOUT );
   }

   SupportList_Before();

   if( "B200" == strProdCode )
      SupportList_B200();
   else if( "F100" == strProdCode )
      SupportList_F100();
   else if( "F200" == strProdCode )
      SupportList_F200();
   else if( "T110" == strProdCode )
      SupportList_T110();
   else if( "TF110" == strProdCode )
      SupportList_TF110();
   else if( "G100" == strProdCode )
      SupportList_G100();
   else if( "G100 EVAL" == strProdCode )
      SupportList_G100_EVAL();
   else if( "G100 GPS" == strProdCode )
      SupportList_G100_GPS();
   else if( "G100F" == strProdCode )
      SupportList_G100F();
   else if( "G200" == strProdCode )
      SupportList_G200();
   else if( "R200" == strProdCode )
      SupportList_R200();
   else if( "G300" == strProdCode )
      SupportList_G300();
   else if( "G400" == strProdCode )
      SupportList_G400();
   else if( "M200" == strProdCode )
      SupportList_M200();
   else if( "M200F" == strProdCode )
      SupportList_M200F();
   else if( "P400" == strProdCode )
      SupportList_P400();
   else if( "P400 EVAL" == strProdCode )
      SupportList_P400();
   else if( "RAS" == strProdCode )
      SupportList_RAS();
   else if( "SRS" == strProdCode )
      SupportList_SRS();
   else if( "TOTALLINTELLIVIX" == strProdCode )
      SupportList_TOTALLINTELLIVIX();
   else
      SupportList_G100();

   SupportList_After();

   return 0;
}

BOOL CSupportFunction::IsSupport( const SUPPORT_ITEM& type )
{
   BOOL bRet = TRUE;

   CCriticalSectionSP co( m_csLock );

   if( !MapFind( type ) )
      bRet = FALSE;

   return bRet;
}

void CSupportFunction::SetProdCode( const std::string& type )
{
   m_strProdCode = type;
}

void CSupportFunction::MapAdd( const SUPPORT_ITEM& type )
{
   CCriticalSectionSP co( m_csLock );

   MapSupport_iter iter = m_mapFunction.find( type );
   int nUseCount        = 1;

   if( m_mapFunction.end() == iter ) {
      m_mapFunction[type] = nUseCount;
   } else {
      nUseCount = iter->second;
      nUseCount++;
      iter->second = nUseCount;
   }
}

void CSupportFunction::MapErase( const SUPPORT_ITEM& type )
{
   CCriticalSectionSP co( m_csLock );

   m_mapFunction.erase( type );
}

BOOL CSupportFunction::MapFind( const SUPPORT_ITEM& type )
{
   CCriticalSectionSP co( m_csLock );

   int nUseCount = 0;

   MapSupport_iter iter = m_mapFunction.find( type );

   if( m_mapFunction.end() == iter )
      return FALSE;

   nUseCount = iter->second;

   return TRUE;
}

int CSupportFunction::SupportList_Before()
{
#ifdef __SUPPORT_ISLD
   MAP_ADD( __SUPPORT_ISLD );
#endif
#ifdef __NEO_IP_WALL
   MAP_ADD( __NEO_IP_WALL );
#endif
//#ifdef __SUPPORT_MMS_BASIC
//   MAP_ADD(__SUPPORT_MMS_BASIC);
//#endif
//#ifdef __SUPPORT_MMS
//   MAP_ADD(__SUPPORT_MMS);
//#endif

// START **************************** Special Functions **************************** ~/
#ifdef __SKIP_LOGIN
#ifdef _DEBUG
   MAP_ADD( __SKIP_LOGIN );
#endif

#endif
#ifdef __SUPPORT_ONVIF_DISCOVERY
   MAP_ADD( __SUPPORT_ONVIF_DISCOVERY );
#endif
#ifdef __SUPPORT_VIDEO_ANALYTICS
   MAP_ADD( __SUPPORT_VIDEO_ANALYTICS );
#endif
#ifdef __SUPPORT_OYTO_BOARD
   MAP_ADD( __SUPPORT_OYTO_BOARD );
#endif
#ifdef __SUPPORT_SYSTEM_MONITORING
   MAP_ADD( __SUPPORT_SYSTEM_MONITORING );
#endif
#ifdef __SUPPORT_CHANNEL_VIDEOPANE
   MAP_ADD( __SUPPORT_CHANNEL_VIDEOPANE );
#endif
#ifdef __SUPPORT_SUBSYSTEM_CHANNEL
   MAP_ADD( __SUPPORT_SUBSYSTEM_CHANNEL ); // 이 옵션이 없다면 정상 작동 되지 않음
#endif
#ifdef __SUPPORT_IP_MULTIPLE_CONNECTION
   MAP_ADD( __SUPPORT_IP_MULTIPLE_CONNECTION );
#endif
#ifdef __SUPPORT_SUBSYSTEM_EMAP
   MAP_ADD( __SUPPORT_SUBSYSTEM_EMAP );
#endif
#ifdef __SUPPORT_PARKING_MONITORING
   MAP_ADD( __SUPPORT_PARKING_MONITORING );
#endif
#ifdef __SUPPORT_DAYNIGHT_IMAGE_SYNTH
   MAP_ADD( __SUPPORT_DAYNIGHT_IMAGE_SYNTH );
#endif
#ifdef __SUPPORT_PTZ_LICENSE_CHECK
   MAP_ADD( __SUPPORT_PTZ_LICENSE_CHECK );
#endif
#ifdef __SUPPORT_UNIFIED_SEARCH
   MAP_ADD( __SUPPORT_UNIFIED_SEARCH );
#endif
#ifdef __SUPPORT_XNS
   MAP_ADD( __SUPPORT_XNS );
#endif

#ifdef __SUPPORT_FISHEYE_DEWARPING
   MAP_ADD( __SUPPORT_FISHEYE_DEWARPING );
#endif

#ifdef __SUPPORT_PANORAMA_VIEW
//   MAP_ADD(__SUPPORT_PANORAMA_VIEW);
#endif

#ifdef __SUPPORT_SENSOR_MONITOR_VIEW
   MAP_ADD( __SUPPORT_SENSOR_MONITOR_VIEW );
#endif

#ifdef __SUPPORT_WARNING_LIGHT
   MAP_ADD( __SUPPORT_WARNING_LIGHT );
#endif

#ifdef __PIE_CHART_BY_TYLEE
   MAP_ADD( __PIE_CHART_BY_TYLEE );
#endif

#ifdef __SUPPORT_ACCESS_HISTORY
   MAP_ADD( __SUPPORT_ACCESS_HISTORY );
#endif

#ifdef __SUPPORT_VIDEO_SUMMARY_OBJPATH_HEATMAP
//MAP_ADD(__SUPPORT_VIDEO_SUMMARY_OBJPATH_HEATMAP);
#endif

#ifdef __SUPPORT_VIDEO_STABILIZATION
//MAP_ADD(__SUPPORT_VIDEO_STABILIZATION);
#endif

#ifdef __SUPPORT_DEFOG
//MAP_ADD(__SUPPORT_DEFOG);
#endif

#ifdef __SUPPORT_ONLY_SENSOR_MOTION_EVENT
//MAP_ADD(__SUPPORT_ONLY_SENSOR_MOTION_EVENT);
#endif

#ifdef __SUPPORT_EMERGENCY_IPEX
//MAP_ADD(__SUPPORT_EMERGENCY_IPEX);
#endif

#ifdef __SUPPORT_BIMD_COLLECTOR
   MAP_ADD( __SUPPORT_BIMD_COLLECTOR );
#endif

//MAP_ADD(__SUPPORT_VIRTUAL_PTZ_CAMERA);

#ifdef __SUPPORT_HEAD_COUNTING
   MAP_ADD( __SUPPORT_HEAD_COUNTING );
#endif
#ifdef __SUPPORT_DXVA2_DECODER
   MAP_ADD( __SUPPORT_DXVA2_DECODER );
#endif
// END **************************** Special Functions **************************** ~/

//////////////////// Cameras /////////////////////
#ifdef __SUPPORT_IDIS_CAMERA
   MAP_ADD( __SUPPORT_IDIS_CAMERA );
#endif
#ifdef __SUPPORT_PELCO_ENDURA
   MAP_ADD( __SUPPORT_PELCO_ENDURA );
#endif
#ifdef __SUPPORT_ONVIF
   MAP_ADD( __SUPPORT_ONVIF );
#endif

#ifdef __SUPPORT_HONEYWELL
//MAP_ADD(__SUPPORT_HONEYWELL)
#endif
//////////////////////////////////////////////////

//////////////////// VMS /////////////////////
#ifdef __SUPPORT_INNODEP
   MAP_ADD( __SUPPORT_INNODEP );
#endif
#ifdef __SUPPORT_REALHUB
   MAP_ADD( __SUPPORT_REALHUB );
#endif
#ifdef __SUPPORT_XPROTECT
   MAP_ADD( __SUPPORT_XPROTECT );
#endif
#ifdef __SUPPORT_OMNICAST
   MAP_ADD( __SUPPORT_OMNICAST );
#endif
#ifdef __SUPPORT_SECURITYCENTER
   MAP_ADD( __SUPPORT_SECURITYCENTER );
#endif
#ifdef __SUPPORT_VIXS3
   MAP_ADD( __SUPPORT_VIXS3 );
   MAP_ADD( __SUPPORT_XPROTECT );
   MAP_ERASE( __SKIP_LOGIN );
#endif

#ifdef __SUPPORT_VMS
   MAP_ADD( __SUPPORT_VMS );
#endif
///////////////////////////////////////////////////////

//////////////////// 기타 /////////////////////////////
#ifdef __SUPPORT_SMS
   MAP_ADD( __SUPPORT_SMS );
   MAP_ADD( __SUPPORT_JECHON_SMS );
   MAP_ADD( SMS_TYPE_DB_QUERY );
#endif

//MAP_ADD(__SUPPORT_GPS);

//MAP_ADD(__SUPPORT_AUGMENTED_REALITY);
//////////////////////////////////////////////////

#ifdef OEM_NONE
   MAP_ADD( OEM_NONE );
#endif

   return 0;
}

int CSupportFunction::SupportList_After()
{

#ifdef __PRODUCT_SDS_TAG_TEST
   MAP_ADD( __PRODUCT_SDS_TAG_TEST );
#endif

   return 0;
}

int CSupportFunction::SupportList_B200()
{
   SET_PROD_CODE( __PRODUCT_B200 );

   MAP_ADD( __SUPPORT_LOG_VIEW );
   MAP_ADD( __SUPPORT_ONLY_SENSOR_MOTION_EVENT );
   MAP_ADD( __SUPPORT_EMERGENCY_MAP );
   MAP_ADD( __SUPPORT_REMOTE_RECORDING );

   // PTZ 관련 기능.
   MAP_ADD( __SUPPORT_PRESET_TOURING );

   return 0;
}

int CSupportFunction::SupportList_T110()
{
   SET_PROD_CODE( __PRODUCT_T110 );

   MAP_ADD( __SUPPORT_OBJECT_COUNTING );
   MAP_ADD( __SUPPORT_EMERGENCY_MAP );
   MAP_ADD( __SUPPORT_REMOTE_RECORDING );
   MAP_ADD( __SUPPORT_PRESET_TOURING );
   MAP_ADD( __SUPPORT_MASTER_SLAVE_BASED_AUTONOMOUS_PTZ_TRACKING );
   MAP_ADD( __SUPPORT_SMOKE_FLAME_DETECTION );

   return 0;
}

int CSupportFunction::SupportList_TF110()
{
   SET_PROD_CODE( __PRODUCT_TF110 );

   MAP_ADD( __SUPPORT_OBJECT_COUNTING );
   MAP_ADD( __SUPPORT_EMERGENCY_MAP );
   MAP_ADD( __SUPPORT_REMOTE_RECORDING );
   MAP_ADD( __SUPPORT_PRESET_TOURING );
   MAP_ADD( __SUPPORT_MASTER_SLAVE_BASED_AUTONOMOUS_PTZ_TRACKING );
   MAP_ADD( __SUPPORT_SMOKE_FLAME_DETECTION );

   return 0;
}

int CSupportFunction::SupportList_F100()
{
   SET_PROD_CODE( __PRODUCT_F100 );

   MAP_ADD( __SUPPORT_OBJECT_COUNTING );
   MAP_ADD( __SUPPORT_EVENT_UNAUTHORIZED_DIRECTION );
   MAP_ADD( __SUPPORT_PRESET_TOURING );
   MAP_ADD( __SUPPORT_SMOKE_FLAME_DETECTION );

   return 0;
}

int CSupportFunction::SupportList_F200()
{
   SET_PROD_CODE( __PRODUCT_F200 );

   MAP_ADD( __SUPPORT_OBJECT_COUNTING );
   MAP_ADD( __SUPPORT_EMERGENCY_MAP );
   MAP_ADD( __SUPPORT_REMOTE_RECORDING );
   MAP_ADD( __SUPPORT_PRESET_TOURING );
   MAP_ADD( __SUPPORT_MASTER_SLAVE_BASED_AUTONOMOUS_PTZ_TRACKING );
   MAP_ADD( __SUPPORT_SMOKE_FLAME_DETECTION );

   return 0;
}

int CSupportFunction::SupportList_G100()
{
   SET_PROD_CODE( __PRODUCT_G100 );

   MAP_ADD( __SUPPORT_OBJECT_COUNTING );
   MAP_ADD( __SUPPORT_EMERGENCY_MAP );
   MAP_ADD( __SUPPORT_PRESET_TOURING );
   MAP_ADD( __SUPPORT_SMOKE_FLAME_DETECTION );
   MAP_ADD( __SUPPORT_VIRTUAL_PTZ_CAMERA );
   MAP_ADD( __SUPPORT_AC );
   MAP_ADD( __SUPPORT_VIDEO_SUMMARY_OBJPATH_HEATMAP );

   return 0;
}

int CSupportFunction::SupportList_G100_EVAL()
{
   SET_PROD_CODE( __PRODUCT_G100_EVAL );

   MAP_ADD( __SUPPORT_OBJECT_COUNTING );
   MAP_ADD( __SUPPORT_PRESET_TOURING );
   MAP_ADD( __SUPPORT_SMOKE_FLAME_DETECTION );
   MAP_ADD( __SUPPORT_VIRTUAL_PTZ_CAMERA );

   return 0;
}

int CSupportFunction::SupportList_G100_GPS()
{
   SET_PROD_CODE( __PRODUCT_G100_GPS );

   MAP_ADD( __SUPPORT_OBJECT_COUNTING );
   MAP_ADD( __SUPPORT_PRESET_TOURING );
   MAP_ADD( __SUPPORT_SMOKE_FLAME_DETECTION );
   MAP_ADD( __SUPPORT_GPS );

   MAP_ERASE( __SUPPORT_FISHEYE_DEWARPING );

   return 0;
}

int CSupportFunction::SupportList_G100F()
{
   SET_PROD_CODE( __PRODUCT_G100F );

   MAP_ADD( __SUPPORT_OBJECT_COUNTING );
   MAP_ADD( __SUPPORT_PRESET_TOURING );
   MAP_ADD( __SUPPORT_SMOKE_FLAME_DETECTION );

   MAP_ERASE( __SUPPORT_VIRTUAL_PTZ_CAMERA );

   MAP_ADD( __SUPPORT_FACE_RECOGNITION );

#ifdef __SUPPORT_FACE_EXTRACTION_ONLY
   MAP_ADD( __SUPPORT_FACE_EXTRACTION_ONLY );
   MAP_ERASE( __SUPPORT_PIE_CHART_FACE );
#endif

   //#if defined(__SUPPORT_USE_FACE_RE_NEOFACE)
   //   MAP_ADD(__SUPPORT_USE_FACE_RE_NEOFACE);
   //#endif
   return 0;
}

int CSupportFunction::SupportList_G200()
{
   SET_PROD_CODE( __PRODUCT_G200 );

   MAP_ADD( __SUPPORT_OBJECT_COUNTING );
   MAP_ADD( __SUPPORT_EMERGENCY_MAP );
   MAP_ADD( __SUPPORT_REMOTE_RECORDING );
   MAP_ADD( __SUPPORT_PRESET_TOURING );
   MAP_ADD( __SUPPORT_MASTER_SLAVE_BASED_AUTONOMOUS_PTZ_TRACKING );
   MAP_ADD( __SUPPORT_SMOKE_FLAME_DETECTION );
   MAP_ADD( __SUPPORT_AC );
   MAP_ADD( __SUPPORT_VIDEO_SUMMARY_OBJPATH_HEATMAP );

   return 0;
}

int CSupportFunction::SupportList_R200()
{
   SET_PROD_CODE( __PRODUCT_R200 );

   MAP_ADD( __SUPPORT_EMERGENCY_MAP );
   MAP_ADD( __SUPPORT_REMOTE_RECORDING );
   MAP_ADD( __SUPPORT_PRESET_TOURING );
   MAP_ADD( __SUPPORT_MASTER_SLAVE_BASED_AUTONOMOUS_PTZ_TRACKING );
   MAP_ADD( __SUPPORT_SMOKE_FLAME_DETECTION );

   return 0;
}

int CSupportFunction::SupportList_G300()
{
   SET_PROD_CODE( __PRODUCT_G300 );

   MAP_ADD( __SUPPORT_OBJECT_COUNTING );
   MAP_ADD( __SUPPORT_EMERGENCY_MAP );
   MAP_ADD( __SUPPORT_REMOTE_RECORDING );
   MAP_ADD( __SUPPORT_SMOKE_FLAME_DETECTION );

   // PTZ 관련 기능.
   MAP_ADD( __SUPPORT_PRESET_TOURING );
   MAP_ADD( __SUPPORT_AUTONOMOUS_PTZ_CAMERA_TRACKING );
   MAP_ADD( __SUPPORT_MASTER_SLAVE_BASED_AUTONOMOUS_PTZ_TRACKING );

   return 0;
}

int CSupportFunction::SupportList_G400()
{
   SET_PROD_CODE( __PRODUCT_G400 );

   MAP_ADD( __SUPPORT_OBJECT_COUNTING );
   MAP_ADD( __SUPPORT_EMERGENCY_MAP );
   MAP_ADD( __SUPPORT_REMOTE_RECORDING );
   MAP_ADD( __SUPPORT_SMOKE_FLAME_DETECTION );

   // PTZ 관련 기능.
   MAP_ADD( __SUPPORT_PRESET_TOURING );
   MAP_ADD( __SUPPORT_AUTONOMOUS_PTZ_CAMERA_TRACKING );
   MAP_ADD( __SUPPORT_MASTER_SLAVE_BASED_AUTONOMOUS_PTZ_TRACKING );
   MAP_ADD( __SUPPORT_HANDOFF_TRACKING );

   return 0;
}

int CSupportFunction::SupportList_M200()
{
   SET_PROD_CODE( __PRODUCT_M200 );

   MAP_ADD( __SUPPORT_OBJECT_COUNTING );
   MAP_ADD( __SUPPORT_EMERGENCY_MAP );
   MAP_ADD( __SUPPORT_REMOTE_RECORDING );
   MAP_ADD( __SUPPORT_PRESET_TOURING );
   MAP_ADD( __SUPPORT_MASTER_SLAVE_BASED_AUTONOMOUS_PTZ_TRACKING );
   MAP_ADD( __SUPPORT_SMOKE_FLAME_DETECTION );

   MAP_ADD( __SUPPORT_PLATE_DETECTION );
   MAP_ADD( __SUPPORT_PLATE_DETECTION_SEARCH );

#ifdef __SUPPORT_LPR_RUSSIA_PROJECT
   MAP_ADD( __SUPPORT_LPR_RUSSIA_PROJECT );
#elif defined __SUPPORT_LPR_COSTARICA_PROJECT
   MAP_ADD( __SUPPORT_LPR_COSTARICA_PROJECT );
#elif defined __SUPPORT_LPR_KAZAKHSTAN_PROJECT
   MAP_ADD( __SUPPORT_LPR_KAZAKHSTAN_PROJECT );
#elif defined __SUPPORT_LPR_CHINA_PROJECT
   MAP_ADD( __SUPPORT_LPR_CHINA_PROJECT );
#elif defined __SUPPORT_LPR_BRASIL_PROJECT
   MAP_ADD( __SUPPORT_LPR_BRASIL_PROJECT );
#endif

#ifdef __SUPPORT_LPR_RESULT_UTF8
   MAP_ADD( __SUPPORT_LPR_RESULT_UTF8 );
#endif

   MAP_ADD( __SUPPORT_HOT_LIST );

   return 0;
}

int CSupportFunction::SupportList_M200F()
{
   SET_PROD_CODE( __PRODUCT_M200F );

   MAP_ADD( __SUPPORT_OBJECT_COUNTING );
   MAP_ADD( __SUPPORT_EMERGENCY_MAP );
   MAP_ADD( __SUPPORT_REMOTE_RECORDING );
   MAP_ADD( __SUPPORT_PRESET_TOURING );
   MAP_ADD( __SUPPORT_MASTER_SLAVE_BASED_AUTONOMOUS_PTZ_TRACKING );
   MAP_ADD( __SUPPORT_SMOKE_FLAME_DETECTION );
   MAP_ADD( __SUPPORT_AC );
   MAP_ADD( __SUPPORT_VIDEO_SUMMARY_OBJPATH_HEATMAP );

   MAP_ADD( __SUPPORT_FACE_RECOGNITION );

#if defined( __SUPPORT_PIE_CHART_FACE ) && defined( __PIE_CHART_BY_TYLEE )
   MAP_ADD( __SUPPORT_PIE_CHART_FACE );
#endif

   //#ifdef __SUPPORT_FACE_EXTRACTION_ONLY
   //   MAP_ADD(__SUPPORT_FACE_EXTRACTION_ONLY);
   //#endif

   //#if defined(__SUPPORT_USE_FACE_RE_NEOFACE)
   //   MAP_ADD(__SUPPORT_USE_FACE_RE_NEOFACE);
   //#endif
   return 0;
}

int CSupportFunction::SupportList_P400()
{
   SET_PROD_CODE( __PRODUCT_P400 );

   MAP_ADD( __SUPPORT_OBJECT_COUNTING );
   MAP_ADD( __SUPPORT_PANORAMIC_CAMERA );
   MAP_ADD( __SUPPORT_EMERGENCY_MAP );
   MAP_ADD( __SUPPORT_REMOTE_RECORDING );
   MAP_ADD( __SUPPORT_SMOKE_FLAME_DETECTION );
   MAP_ADD( __SUPPORT_VIDEO_STABILIZATION );
   MAP_ADD( __SUPPORT_DEFOG );
   MAP_ADD( __SUPPORT_PRESET_TOURING );
   MAP_ADD( __SUPPORT_AUTONOMOUS_PTZ_CAMERA_TRACKING );
   MAP_ADD( __SUPPORT_MASTER_SLAVE_BASED_AUTONOMOUS_PTZ_TRACKING );
   MAP_ADD( __SUPPORT_HANDOFF_TRACKING );
   MAP_ADD( __SUPPORT_VIRTUAL_PTZ_CAMERA );
   MAP_ADD( __SUPPORT_VIDEO_SUMMARY_OBJPATH_HEATMAP );
   MAP_ADD( __SUPPORT_PANORAMA_VIEW );
   MAP_ADD( __SUPPORT_AC );

   return 0;
}

int CSupportFunction::SupportList_RAS()
{
#ifdef __PRODUCT_RAS_SEARCH
   MAP_ADD( __PRODUCT_RAS_SEARCH );
#endif
#ifdef __PRODUCT_SEARCH_CLIENT
   MAP_ADD( __PRODUCT_SEARCH_CLIENT );
#endif

   SET_PROD_CODE( __PRODUCT_RAS );

   MAP_ADD( __SUPPORT_OBJECT_COUNTING );
   MAP_ADD( __SUPPORT_PANORAMIC_CAMERA );
   MAP_ADD( __SUPPORT_EMERGENCY_MAP );
   MAP_ADD( __SUPPORT_REMOTE_RECORDING );
   MAP_ADD( __SUPPORT_PRESET_TOURING );
   MAP_ADD( __SUPPORT_AUTONOMOUS_PTZ_CAMERA_TRACKING );
   MAP_ADD( __SUPPORT_MASTER_SLAVE_BASED_AUTONOMOUS_PTZ_TRACKING );
   MAP_ADD( __SUPPORT_HANDOFF_TRACKING );
   MAP_ADD( __SUPPORT_VIRTUAL_PTZ_CAMERA );
   MAP_ADD( __SUPPORT_SMOKE_FLAME_DETECTION );
   MAP_ADD( __SUPPORT_PLATE_DETECTION );
   MAP_ADD( __SUPPORT_PLATE_DETECTION_SEARCH );
   MAP_ADD( __SUPPORT_PANORAMA_VIEW );
   MAP_ADD( __SUPPORT_FACE_RECOGNITION );
   MAP_ADD( __SUPPORT_AC );
   MAP_ADD( __SUPPORT_VIDEO_SUMMARY_OBJPATH_HEATMAP );
   MAP_ADD( __SUPPORT_HOT_LIST );

   return 0;
}

int CSupportFunction::SupportList_SRS()
{
   SET_PROD_CODE( __PRODUCT_SRS );

   MAP_ADD( __SUPPORT_OBJECT_COUNTING );
   MAP_ADD( __SUPPORT_PANORAMIC_CAMERA );
   MAP_ADD( __SUPPORT_EMERGENCY_MAP );
   MAP_ADD( __SUPPORT_REMOTE_RECORDING );
   MAP_ADD( __SUPPORT_PRESET_TOURING );
   MAP_ADD( __SUPPORT_AUTONOMOUS_PTZ_CAMERA_TRACKING );
   MAP_ADD( __SUPPORT_MASTER_SLAVE_BASED_AUTONOMOUS_PTZ_TRACKING );
   MAP_ADD( __SUPPORT_HANDOFF_TRACKING );
   MAP_ADD( __SUPPORT_VIRTUAL_PTZ_CAMERA );
   MAP_ADD( __SUPPORT_SMOKE_FLAME_DETECTION );
   MAP_ADD( __SUPPORT_PLATE_DETECTION );
   MAP_ADD( __SUPPORT_PLATE_DETECTION_SEARCH );
   MAP_ADD( __SUPPORT_SYSTEM_MONITORING );

   return 0;
}

int CSupportFunction::SupportList_TOTALLINTELLIVIX()
{
   SET_PROD_CODE( __PRODUCT_TOTALLINTELLIVIX );

   MAP_ADD( __SUPPORT_OBJECT_COUNTING );
   //MAP_ADD( __SUPPORT_PANORAMIC_CAMERA );
   //MAP_ADD( __SUPPORT_EMERGENCY_MAP );
   //MAP_ADD( __SUPPORT_REMOTE_RECORDING );
   MAP_ADD( __SUPPORT_SMOKE_FLAME_DETECTION );
   MAP_ADD( __SUPPORT_VIDEO_STABILIZATION );
   MAP_ADD( __SUPPORT_DEFOG );
   MAP_ADD( __SUPPORT_PRESET_TOURING );
   MAP_ADD( __SUPPORT_AUTONOMOUS_PTZ_CAMERA_TRACKING );
   //MAP_ADD(__SUPPORT_MASTER_SLAVE_BASED_AUTONOMOUS_PTZ_TRACKING);
   //MAP_ADD( __SUPPORT_HANDOFF_TRACKING );
   //MAP_ADD( __SUPPORT_VIRTUAL_PTZ_CAMERA );
   //MAP_ADD( __SUPPORT_VIDEO_SUMMARY_OBJPATH_HEATMAP );
   //MAP_ADD( __SUPPORT_PANORAMA_VIEW );

   //MAP_ADD( __SUPPORT_PLATE_DETECTION );
   //MAP_ADD( __SUPPORT_PLATE_DETECTION_SEARCH );

#ifdef __SUPPORT_LPR_RUSSIA_PROJECT
   MAP_ADD( __SUPPORT_LPR_RUSSIA_PROJECT );
#elif defined __SUPPORT_LPR_COSTARICA_PROJECT
   MAP_ADD( __SUPPORT_LPR_COSTARICA_PROJECT );
#elif defined __SUPPORT_LPR_KAZAKHSTAN_PROJECT
   MAP_ADD( __SUPPORT_LPR_KAZAKHSTAN_PROJECT );
#elif defined __SUPPORT_LPR_CHINA_PROJECT
   MAP_ADD( __SUPPORT_LPR_CHINA_PROJECT );
#elif defined __SUPPORT_LPR_BRASIL_PROJECT
   MAP_ADD( __SUPPORT_LPR_BRASIL_PROJECT );
#endif

   //MAP_ADD( __SUPPORT_LPR_OTHER_USE );

   //MAP_ADD(__SUPPORT_FACE_RECOGNITION);

   //MAP_ADD(__SUPPORT_USE_FACE_RE_NEOFACE);

   //MAP_ADD( __SUPPORT_PIE_CHART_FACE );

   //MAP_ADD( __SUPPORT_AC );
   //MAP_ADD( __SUPPORT_HOT_LIST );
   //MAP_ADD( __SUPPORT_BIMD_COLLECTOR );

   return 0;
}

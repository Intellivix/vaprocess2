#ifdef __linux

#include "FileMonitoring.h"

#include <fcntl.h>
#include <chrono>

FileMonitoring::~FileMonitoring()
{
   StopThread();
}

void FileMonitoring::SetDir( const std::string& dir )
{
   m_dir = dir;
}

bool FileMonitoring::StartThread( void* pInstance )
{
   if( pInstance == nullptr ) {
      pInstance = this;
   }

   InnerThread::StartThread( pInstance );
   return true;
}

void FileMonitoring::Close()
{
   inotify_rm_watch( m_file_descriptor, m_watch_descriptor );
   close( m_file_descriptor );
   buffer_position = 0;
   buffer_length   = 0;
   m_watch_descriptor = -1;
   m_file_descriptor  = -1;
}

void FileMonitoring::StopThread()
{
   Close();
   InnerThread::StopThread();
}

bool FileMonitoring::Init( const std::string& dir )
{
   buffer_position = 0;
   buffer_length   = 0;

   if( dir.empty() == false ) {
      SetDir( dir );
   }

   m_file_descriptor = inotify_init();
   if( m_file_descriptor < 0 ) {
      LOGE << "file_descriptor errno : " << errno;
      return false;
   }

   m_watch_descriptor = inotify_add_watch( m_file_descriptor, m_dir.c_str(), IN_MODIFY | IN_CREATE | IN_DELETE );
   if( m_watch_descriptor < 0 ) {
      LOGE << "watch_descriptor errno : " << errno;
      return false;
   }

   // set non-blocking.
   int flags = fcntl( m_file_descriptor, F_GETFL, 0 );
   if( fcntl( m_file_descriptor, F_SETFL, flags | O_NONBLOCK ) ) {
      LOGE << "fcntl errno : " << errno;
      return false;
   }

   return true;
}

bool FileMonitoring::ReadAndProcessEvent()
{
   buffer_position = 0;
   buffer_length = read( m_file_descriptor, m_buffer, BUF_LEN );
   if( buffer_length < 0 && errno == EAGAIN ) {
      // there is no data to be read
      return true;
   }

   if( buffer_length < 0 ) {
      // Some other error occurred during read.
      LOGE << "read error. errno :" << errno;
      return false;
   }

   // Otherwise, you're good to go
   while( buffer_position < buffer_length ) {
      struct inotify_event* event = reinterpret_cast<struct inotify_event*>( &m_buffer[buffer_position] );
      buffer_position += EVENT_SIZE + event->len;

      Process( event );
   }

   return true;
}

void FileMonitoring::ThreadLoop()
{
   if( Init() == false ) {
      LOGE << "init failed";
      return;
   }

   while( m_thread_running ) {
      ReadAndProcessEvent();
      Sleep_ms( 100 );
   }

   Close();
}

bool FileMonitoring::Process( inotify_event* event )
{
   if( event->len == 0 ) {
      return false;
   }
   if( event->mask & IN_ISDIR ) {
      return false;
   }

   if( event->mask & IN_CREATE ) {
      logd( "The file[%s] was created.", event->name );
   } else if( event->mask & IN_DELETE ) {
      logd( "The file[%s] was deleted.", event->name );
   } else if( event->mask & IN_MODIFY ) {
      logd( "The file[%s] was modified.", event->name );
   }

   return true;
}

#endif __linux
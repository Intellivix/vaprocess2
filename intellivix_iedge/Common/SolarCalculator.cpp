#ifdef __SUPPORT_SOLAR_CALCULATOR

#include "StdAfx.h"
#include "SolarCalculator.h"
#include "CommonUtil.h"

CSolarCalculator::CSolarCalculator()
{
   Initialize( 127.0, 37.5, 135.0, FALSE ); // 서울지역으로 초기화.
}

CSolarCalculator::CSolarCalculator( double dfLongitude, double dfLatitude, double dfLongitudeTimeZone, bool dfUseSummerTime )
{
   // 입력 값 설명
   // dfLongitude : 경도
   // dfLatitude  : 위도
   // dfLongitudeTimeZone : 표준시간대에 해당하는 경도를 말한다. 대한민국의 경우 동경 135도 이다.
   m_dfLongitude         = dfLongitude;
   m_dfLatitude          = dfLatitude;
   m_dfLatitudeInRadians = ConvertDegreeToRadian( dfLatitude );
   m_dfLongitudeTimeZone = dfLongitudeTimeZone;
   m_bUseSummerTime      = dfUseSummerTime;
}

void CSolarCalculator::Initialize( double dfLongitude, double dfLatitude, double dfLongitudeTimeZone, bool dfUseSummerTime )
{
   // 입력 값 설명
   // dfLongitude : 경도
   // dfLatitude  : 위도
   // dfLongitudeTimeZone : 표준시간대에 해당하는 경도를 말한다. 대한민국의 경우 동경 135도 이다.
   m_dfLongitude         = dfLongitude;
   m_dfLatitude          = dfLatitude;
   m_dfLatitudeInRadians = ConvertDegreeToRadian( dfLatitude );
   m_dfLongitudeTimeZone = dfLongitudeTimeZone;
   m_bUseSummerTime      = dfUseSummerTime;
}

BOOL CSolarCalculator::Read( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "SolarCalculator" ) ) {
      xmlNode.Attribute( TYPE_ID( double ), "Longitude", &m_dfLongitude );
      xmlNode.Attribute( TYPE_ID( double ), "Latitude", &m_dfLatitude );
      xmlNode.Attribute( TYPE_ID( double ), "LongitudeTimeZone", &m_dfLongitudeTimeZone );
      xmlNode.Attribute( TYPE_ID( BOOL ), "UseSummerTime", &m_bUseSummerTime );
      Initialize( 127.0, 37.5, 135.0, FALSE ); // 헌인릉 버전에서만 설정.
      xmlNode.End();
   }

   m_dfLatitudeInRadians = ConvertDegreeToRadian( m_dfLatitude );

   return TRUE;
}

BOOL CSolarCalculator::Write( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "SolarCalculator" ) ) {
      xmlNode.Attribute( TYPE_ID( double ), "Longitude", &m_dfLongitude );
      xmlNode.Attribute( TYPE_ID( double ), "Latitude", &m_dfLatitude );
      xmlNode.Attribute( TYPE_ID( double ), "LongitudeTimeZone", &m_dfLongitudeTimeZone );
      xmlNode.Attribute( TYPE_ID( BOOL ), "UseSummerTime", &m_bUseSummerTime );
      xmlNode.End();
   }
   return TRUE;
}

BCCL::Time CSolarCalculator::CalculateSunRise( BCCL::Time dateTime )
{
   int dayNumberOfDateTime          = ExtractDayNumber( dateTime );
   double differenceSunAndLocalTime = CalculateDifferenceSunAndLocalTime( dayNumberOfDateTime );
   double declanationOfTheSun       = CalculateDeclination( dayNumberOfDateTime );
   double tanSunPosition            = CalculateTanSunPosition( declanationOfTheSun );
   int sunRiseInMinutes             = CalculateSunRiseInternal( tanSunPosition, differenceSunAndLocalTime );
   return CreateDateTime( dateTime, sunRiseInMinutes );
}

BCCL::Time CSolarCalculator::CalculateSunSet( BCCL::Time dateTime )
{
   int dayNumberOfDateTime          = ExtractDayNumber( dateTime );
   double differenceSunAndLocalTime = CalculateDifferenceSunAndLocalTime( dayNumberOfDateTime );
   double declanationOfTheSun       = CalculateDeclination( dayNumberOfDateTime );
   double tanSunPosition            = CalculateTanSunPosition( declanationOfTheSun );
   int sunSetInMinutes              = CalculateSunSetInternal( tanSunPosition, differenceSunAndLocalTime );
   return CreateDateTime( dateTime, sunSetInMinutes );
}

double CSolarCalculator::CalculateMaximumSolarRadiation( BCCL::Time dateTime )
{
   int dayNumberOfDateTime          = ExtractDayNumber( dateTime );
   double differenceSunAndLocalTime = CalculateDifferenceSunAndLocalTime( dayNumberOfDateTime );
   int numberOfMinutesThisDay       = GetNumberOfMinutesThisDay( dateTime, differenceSunAndLocalTime );
   double declanationOfTheSun       = CalculateDeclination( dayNumberOfDateTime );
   double sinSunPosition            = CalculateSinSunPosition( declanationOfTheSun );
   double cosSunPosition            = CalculateCosSunPosition( declanationOfTheSun );
   double sinSunHeight              = sinSunPosition + cosSunPosition * cos( 2.0 * MC_PI * ( numberOfMinutesThisDay + 720.0 ) / 1440.0 ) + 0.08;
   double sunConstantePart          = cos( 2.0 * MC_PI * dayNumberOfDateTime );
   double sunCorrection             = 1370.0 * ( 1.0 + ( 0.033 * sunConstantePart ) );
   return CalculateMaximumSolarRadiationInternal( sinSunHeight, sunCorrection );
}

double CSolarCalculator::CalculateDeclination( int numberOfDaysSinceFirstOfJanuary )
{
   return asin( -0.39795 * cos( 2.0 * MC_PI * ( numberOfDaysSinceFirstOfJanuary + 10.0 ) / 365.0 ) );
}

int CSolarCalculator::ExtractDayNumber( BCCL::Time dateTime )
{
   int nDay = GetDaysOfYear( dateTime );
   return nDay;
}

BCCL::Time CSolarCalculator::CreateDateTime( BCCL::Time dateTime, int timeInMinutes )
{
   int hour   = timeInMinutes / 60;
   int minute = timeInMinutes - ( hour * 60 );
   BCCL::Time tm;
   tm.Year        = dateTime.Year;
   tm.Month       = dateTime.Month;
   tm.Day         = dateTime.Day;
   tm.Hour        = hour;
   tm.Minute      = minute;
   tm.Second      = 0;
   tm.Millisecond = 0;
   return tm;
}

int CSolarCalculator::CalculateSunRiseInternal( double tanSunPosition, double differenceSunAndLocalTime )
{
   int sunRise = (int)( 720.0 - 720.0 / MC_PI * acos( -tanSunPosition ) - differenceSunAndLocalTime );
   sunRise     = LimitSunRise( sunRise );
   return sunRise;
}

int CSolarCalculator::CalculateSunSetInternal( double tanSunPosition, double differenceSunAndLocalTime )
{
   int sunSet = (int)( 720.0 + 720.0 / MC_PI * acos( -tanSunPosition ) - differenceSunAndLocalTime );
   sunSet     = LimitSunSet( sunSet );
   return sunSet;
}

double CSolarCalculator::CalculateTanSunPosition( double declanationOfTheSun )
{
   double sinSunPosition = CalculateSinSunPosition( declanationOfTheSun );
   double cosSunPosition = CalculateCosSunPosition( declanationOfTheSun );
   double tanSunPosition = sinSunPosition / cosSunPosition;
   tanSunPosition        = LimitTanSunPosition( tanSunPosition );
   return tanSunPosition;
}

double CSolarCalculator::CalculateCosSunPosition( double declanationOfTheSun )
{
   return cos( m_dfLatitudeInRadians ) * cos( declanationOfTheSun );
}

double CSolarCalculator::CalculateSinSunPosition( double declanationOfTheSun )
{
   return sin( m_dfLatitudeInRadians ) * sin( declanationOfTheSun );
}

double CSolarCalculator::CalculateDifferenceSunAndLocalTime( int dayNumberOfDateTime )
{
   double ellipticalOrbitPart1 = 7.95204 * sin( ( 0.01768 * dayNumberOfDateTime ) + 3.03217 );
   double ellipticalOrbitPart2 = 9.98906 * sin( ( 0.03383 * dayNumberOfDateTime ) + 3.46870 );

   double differenceSunAndLocalTime = ellipticalOrbitPart1 + ellipticalOrbitPart2 + ( m_dfLongitude - m_dfLongitudeTimeZone ) * 4;

   if( m_bUseSummerTime )
      differenceSunAndLocalTime -= 60.0;
   return differenceSunAndLocalTime;
}

double CSolarCalculator::LimitTanSunPosition( double tanSunPosition )
{
   if( ( (int)tanSunPosition ) < -1 ) {
      tanSunPosition = -1.0;
   }
   if( ( (int)tanSunPosition ) > 1 ) {
      tanSunPosition = 1.0;
   }
   return tanSunPosition;
}

int CSolarCalculator::LimitSunSet( int sunSet )
{
   if( sunSet > 1439 ) {
      sunSet -= 1439;
   }
   return sunSet;
}

int CSolarCalculator::LimitSunRise( int sunRise )
{
   if( sunRise < 0 ) {
      sunRise += 1440;
   }
   return sunRise;
}

double CSolarCalculator::ConvertDegreeToRadian( double degree )
{
   return degree * MC_PI / 180.0;
}

double CSolarCalculator::CalculateMaximumSolarRadiationInternal( double sinSunHeight, double sunCorrection )
{
   double maximumSolarRadiation;
   if( ( sinSunHeight > 0.0 ) && fabs( 0.25 / sinSunHeight ) < 50.0 ) {
      maximumSolarRadiation = sunCorrection * sinSunHeight * exp( -0.25 / sinSunHeight );
   } else {
      maximumSolarRadiation = 0;
   }
   return maximumSolarRadiation;
}

int CSolarCalculator::GetNumberOfMinutesThisDay( BCCL::Time dateTime, double differenceSunAndLocalTime )
{
   return dateTime.Hour * 60 + dateTime.Minute + (int)differenceSunAndLocalTime;
}

#endif // __SUPPORT_SOLAR_CALCULATOR

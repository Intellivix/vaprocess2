﻿#include "StdAfx.h"
#include "VACEx.h"
#include "version.h"
#include "SupportFunction.h"

//#pragma pack(1)

///////////////////////////////////////////////////////////////////
//
// class : HeadDetectionConfig
//
///////////////////////////////////////////////////////////////////

HeadDetectionConfig::HeadDetectionConfig()
{
   HRRadius = 0;
}

int HeadDetectionConfig::ReadFile( CXMLIO* pIO )
{
   static HeadDetectionConfig dv;

   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Head" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "HRRadius", &HRRadius, &dv.HRRadius );
      xmlNode.Attribute( TYPE_ID( int ), "HeadStartPointX", &HeadStartPoint.x, &dv.HeadStartPoint.x );
      xmlNode.Attribute( TYPE_ID( int ), "HeadStartPointY", &HeadStartPoint.y, &dv.HeadStartPoint.y );
      xmlNode.Attribute( TYPE_ID( int ), "HeadEndPointX", &HeadEndPoint.x, &dv.HeadEndPoint.x );
      xmlNode.Attribute( TYPE_ID( int ), "HeadEndPointY", &HeadEndPoint.y, &dv.HeadEndPoint.y );
      xmlNode.End();
   }
   return ( DONE );
}

int HeadDetectionConfig::WriteFile( CXMLIO* pIO )
{
   if( HRRadius < 0 ) HRRadius = 0;
   static HeadDetectionConfig dv;

   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Head" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "HRRadius", &HRRadius, &dv.HRRadius );
      xmlNode.Attribute( TYPE_ID( int ), "HeadStartPointX", &HeadStartPoint.x, &dv.HeadStartPoint.x );
      xmlNode.Attribute( TYPE_ID( int ), "HeadStartPointY", &HeadStartPoint.y, &dv.HeadStartPoint.y );
      xmlNode.Attribute( TYPE_ID( int ), "HeadEndPointX", &HeadEndPoint.x, &dv.HeadEndPoint.x );
      xmlNode.Attribute( TYPE_ID( int ), "HeadEndPointY", &HeadEndPoint.y, &dv.HeadEndPoint.y );
      xmlNode.End();
   }

   return ( DONE );
}

int HeadDetectionConfig::CheckSetup( HeadDetectionConfig* hdc )

{
   int ret_flag = 0;

   FileIO buffThis, buffFrom;
   CXMLIO xmlIOThis, xmlIOFrom;
   // 변경된 아이템이 있는지 체크.
   xmlIOThis.SetFileIO( &buffThis, XMLIO_Write );
   xmlIOFrom.SetFileIO( &buffFrom, XMLIO_Write );
   WriteFile( &xmlIOThis );
   hdc->WriteFile( &xmlIOFrom );
   if( buffThis.IsDiff( buffFrom ) ) ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength( 0 );
   buffFrom.SetLength( 0 );
   if( ret_flag & CHECK_SETUP_RESULT_CHANGED ) {
      if( HRRadius != hdc->HRRadius ) ret_flag |= CHECK_SETUP_RESULT_RESTART;
   }
   return ( ret_flag );
}

///////////////////////////////////////////////////////////////////
//
// class : FaceDetectionConfig
//
///////////////////////////////////////////////////////////////////

FaceDetectionConfig::FaceDetectionConfig (   )
{
   m_fMaxPitch = 15;
   m_fMaxYaw   = 30.0;
   m_fMaxRoll  = 45.0;

   m_nObjectAnalysisMode   = FALSE;
   m_bFFVbyFrame           = FALSE;
   m_nScheduleAnalysisTime = 300;
   m_nMaxAnalysisTime      = 1000 * 60;

   m_nMinFaceSize   = 36;
   m_nMaxFaceSize   = 1000;
   m_nSWStepSize    = 8;
   m_fIPScaleFactor = 0.7f;
   m_fFDScoreThres  = 1.7f;

   m_bChkRelayOut        = FALSE;
   RelayOutputDeviceType = RelayOutputDeviceType_CaptureBoard;
   RelayOutputNo         = 0;
}


int FaceDetectionConfig::ReadFile  (CXMLIO* pIO)
{
   BOOL _bFaceEstimation = FALSE;

   static FaceDetectionConfig dv;
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Face"))
   {
      xmlNode.Attribute (TYPE_ID (int) ,   "FaceRecogType", &m_nFaceRecogType, &dv.m_nFaceRecogType);
      xmlNode.Attribute (TYPE_ID (xint),   "OAT", &m_nObjectAnalysisMode, &dv.m_nObjectAnalysisMode);

      // FaceDetection 클래스 내부에서 직접적으로 사용하는 변수
      xmlNode.Attribute (TYPE_ID (float), "MaxPitch"     , &m_fMaxPitch     , &dv.m_fMaxPitch);
      xmlNode.Attribute (TYPE_ID (float), "MaxYaw"       , &m_fMaxYaw       , &dv.m_fMaxYaw);
      xmlNode.Attribute (TYPE_ID (float), "MaxRoll"      , &m_fMaxRoll      , &dv.m_fMaxRoll);
      xmlNode.Attribute (TYPE_ID (int)  , "MinFaceSize"  , &m_nMinFaceSize  , &dv.m_nMinFaceSize);
      xmlNode.Attribute (TYPE_ID (int)  , "MaxFaceSize"  , &m_nMaxFaceSize  , &dv.m_nMaxFaceSize);
      xmlNode.Attribute (TYPE_ID (int)  , "SWStepSize"   , &m_nSWStepSize   , &dv.m_nSWStepSize);
      xmlNode.Attribute (TYPE_ID (float), "IPScaleFactor", &m_fIPScaleFactor, &dv.m_fIPScaleFactor);
      xmlNode.Attribute (TYPE_ID (float), "FDScoreThres" , &m_fFDScoreThres , &dv.m_fFDScoreThres);
      // FaceDetecion 클래스와 별도로 사용하는 변수
      xmlNode.Attribute (TYPE_ID (BOOL) , "Use_FaceEstimation" , &m_bFaceEstimation);
      xmlNode.Attribute (TYPE_ID (int)  , "FaceScheduleTime"   , &m_nScheduleAnalysisTime, &dv.m_nScheduleAnalysisTime);
      xmlNode.Attribute (TYPE_ID (int)  , "FaceAnalysisMaxTime", &m_nMaxAnalysisTime     , &dv.m_nMaxAnalysisTime);
      xmlNode.Attribute (TYPE_ID (BOOL) , "FFVByFrameFlag"     , &m_bFFVbyFrame          , &dv.m_bFFVbyFrame);
      // Relay Out
      xmlNode.Attribute (TYPE_ID (BOOL) , "RelayOutFlag"         , &m_bChkRelayOut       , &dv.m_bChkRelayOut);
      xmlNode.Attribute (TYPE_ID (int)  , "RelayOutputDeviceType", &RelayOutputDeviceType, &dv.RelayOutputDeviceType);
      xmlNode.Attribute (TYPE_ID (int)  , "RelayOutputNo"        , &RelayOutputNo        , &dv.RelayOutputNo);
      xmlNode.Attribute (TYPE_ID (char) , "RelayOutputDeviceIP"  , RelayOutPutIP         , &dv.RelayOutPutIP, sizeof(RelayOutPutIP));
      xmlNode.Attribute (TYPE_ID (int)  , "RelayOutputDevicePort", &RelayOutputPort      , &dv.RelayOutputPort);
      xmlNode.End ();
   }

   if (_bFaceEstimation) m_nObjectAnalysisMode |= ObjectAnalysisMode_FaceAgeGender;

   return (DONE);
}

int FaceDetectionConfig::WriteFile (CXMLIO* pIO)
{
   static FaceDetectionConfig dv;
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Face"))
   {
      xmlNode.Attribute (TYPE_ID (int) ,  "FaceRecogType", &m_nFaceRecogType, &dv.m_nFaceRecogType);
      xmlNode.Attribute (TYPE_ID (xint),   "OAT", &m_nObjectAnalysisMode, &dv.m_nObjectAnalysisMode);
      // FaceDetection 클래스 내부에서 직접적으로 사용하는 변수
      xmlNode.Attribute (TYPE_ID (float), "MaxPitch"     , &m_fMaxPitch     , &dv.m_fMaxPitch);
      xmlNode.Attribute (TYPE_ID (float), "MaxYaw"       , &m_fMaxYaw       , &dv.m_fMaxYaw);
      xmlNode.Attribute (TYPE_ID (float), "MaxRoll"      , &m_fMaxRoll      , &dv.m_fMaxRoll);
      xmlNode.Attribute (TYPE_ID (int)  , "MinFaceSize"  , &m_nMinFaceSize  , &dv.m_nMinFaceSize);
      xmlNode.Attribute (TYPE_ID (int)  , "MaxFaceSize"  , &m_nMaxFaceSize  , &dv.m_nMaxFaceSize);
      xmlNode.Attribute (TYPE_ID (int)  , "SWStepSize"   , &m_nSWStepSize   , &dv.m_nSWStepSize);
      xmlNode.Attribute (TYPE_ID (float), "IPScaleFactor", &m_fIPScaleFactor, &dv.m_fIPScaleFactor);
      xmlNode.Attribute (TYPE_ID (float), "FDScoreThres" , &m_fFDScoreThres , &dv.m_fFDScoreThres);
      // FaceDetecion 클래스와 별도로 사용하는 변수
      xmlNode.Attribute (TYPE_ID (BOOL) , "Use_FaceEstimation" , &m_bFaceEstimation);
      xmlNode.Attribute (TYPE_ID (int)  , "FaceScheduleTime"   , &m_nScheduleAnalysisTime, &dv.m_nScheduleAnalysisTime);
      xmlNode.Attribute (TYPE_ID (int)  , "FaceAnalysisMaxTime", &m_nMaxAnalysisTime     , &dv.m_nMaxAnalysisTime);
      xmlNode.Attribute (TYPE_ID (BOOL) , "FFVByFrameFlag"     , &m_bFFVbyFrame          , &dv.m_bFFVbyFrame);
      // Relay Out
      xmlNode.Attribute (TYPE_ID (BOOL) , "RelayOutFlag"         , &m_bChkRelayOut       , &dv.m_bChkRelayOut);
      xmlNode.Attribute (TYPE_ID (int)  , "RelayOutputDeviceType", &RelayOutputDeviceType, &dv.RelayOutputDeviceType);
      xmlNode.Attribute (TYPE_ID (int)  , "RelayOutputNo"        , &RelayOutputNo        , &dv.RelayOutputNo);
      xmlNode.Attribute (TYPE_ID (char) , "RelayOutputDeviceIP"  , RelayOutPutIP         , &dv.RelayOutPutIP, sizeof(RelayOutPutIP));
      xmlNode.Attribute (TYPE_ID (int)  , "RelayOutputDevicePort", &RelayOutputPort      , &dv.RelayOutputPort);
      xmlNode.End ();
   }
   return (DONE);
}

int FaceDetectionConfig::CheckSetup (FaceDetectionConfig* fdc)
{
   int ret_flag = 0;

   FileIO buffThis,buffFrom;
   CXMLIO xmlIOThis,xmlIOFrom;
   // 변경된 아이템이 있는지 체크.
   xmlIOThis.SetFileIO (&buffThis, XMLIO_Write);
   xmlIOFrom.SetFileIO (&buffFrom, XMLIO_Write);
   WriteFile (&xmlIOThis);
   fdc->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom)) ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength (0);
   buffFrom.SetLength (0);
   if (ret_flag & CHECK_SETUP_RESULT_CHANGED) {
      ret_flag |= CHECK_SETUP_RESULT_RESTART;
   }
   return (ret_flag);
}


///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleEx
//
///////////////////////////////////////////////////////////////////////////////

EventRuleEx::EventRuleEx()

{
   AlarmType         = ER_AT_XML;
   EmailOptions      = 0;
   RelayOutputDevice = EVT_RO_CAPTURE_BOARD;
   _tcscpy( RelayOutPutIP, "" );
   RelayOutputPort = 0;
   RelayOutputNo   = 0;
   sprintf( Name, "Event Rule #%d", ID );
   SoundFileName[0]    = 0;
   SoundFileName2[0]   = 0;
   m_EventZoneType     = 0;
   m_CameraType        = 0;
   bEnableLocalSpeaker = FALSE;
   bEnableCamSpeaker   = TRUE;
}

VOID EventRuleEx::ReplaceChar( TCHAR* pString, TCHAR FindChar, TCHAR NewChar )
{
   if( pString ) {
      INT nLen = _tcslen( pString );

      for( INT nIndex = 0; nIndex < nLen; nIndex++ ) {
         if( pString[nIndex] == FindChar )
            pString[nIndex] = NewChar;
      }
   }
}

int EventRuleEx::ReadFile( CXMLIO* pIO )
{
   static EventRuleEx dv6;

   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "EventRuleEx" ) ) {
      EventRule::ReadFile( pIO );
      xmlNode.Attribute( TYPE_ID( int ), "AlarmType", &AlarmType, &dv6.AlarmType );
      xmlNode.Attribute( TYPE_ID( int ), "RelayOutputDevice", &RelayOutputDevice, &dv6.RelayOutputDevice );
      xmlNode.Attribute( TYPE_ID( char ), "RelayOutputDeviceIP", RelayOutPutIP, &dv6.RelayOutPutIP, sizeof( RelayOutPutIP ) );
      xmlNode.Attribute( TYPE_ID( int ), "RelayOutputDevicePort", &RelayOutputPort, &dv6.RelayOutputPort );
      xmlNode.Attribute( TYPE_ID( int ), "RelayOutputNo", &RelayOutputNo, &dv6.RelayOutputNo );
      xmlNode.Attribute( TYPE_ID( char ), "SoundFileName", SoundFileName, &dv6.SoundFileName, sizeof( SoundFileName ) );
      xmlNode.Attribute( TYPE_ID( char ), "SoundFileName2", SoundFileName2, &dv6.SoundFileName2, sizeof( SoundFileName2 ) );

      xmlNode.Attribute( TYPE_ID( BOOL ), "EnableCamSpeaker", &bEnableLocalSpeaker, &dv6.bEnableLocalSpeaker );
      xmlNode.Attribute( TYPE_ID( BOOL ), "EnableCamSpeaker", &bEnableCamSpeaker, &dv6.bEnableCamSpeaker );

      xmlNode.Attribute( TYPE_ID( xint ), "EmailOptions", &EmailOptions, &dv6.EmailOptions );
      xmlNode.Attribute( TYPE_ID( int ), "EventZoneType", &m_EventZoneType, &dv6.m_EventZoneType );
      xmlNode.Attribute( TYPE_ID( int ), "CameraType", &m_CameraType, &dv6.m_CameraType );
      xmlNode.End();
   }
   // 현재 나타남 이벤트는 더이상 지원하지 않기 때문에 배회 이벤트로 대체한다.

   AlarmType |= ER_AT_EVENT_ALARM | ER_AT_EVENT_MSG | ER_AT_REALTIME_EVENT;
   return ( DONE );
}

int EventRuleEx::WriteFile( CXMLIO* pIO )
{
   static EventRuleEx dv6;

   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "EventRuleEx" ) ) {
      EventRule::WriteFile( pIO );
      xmlNode.Attribute( TYPE_ID( int ), "AlarmType", &AlarmType, &dv6.AlarmType );
      xmlNode.Attribute( TYPE_ID( int ), "RelayOutputDevice", &RelayOutputDevice, &dv6.RelayOutputDevice );
      xmlNode.Attribute( TYPE_ID( char ), "RelayOutputDeviceIP", RelayOutPutIP, &dv6.RelayOutPutIP, sizeof( RelayOutPutIP ) );
      xmlNode.Attribute( TYPE_ID( int ), "RelayOutputDevicePort", &RelayOutputPort, &dv6.RelayOutputPort );
      xmlNode.Attribute( TYPE_ID( int ), "RelayOutputNo", &RelayOutputNo, &dv6.RelayOutputNo );
      xmlNode.Attribute( TYPE_ID( char ), "SoundFileName", SoundFileName, &dv6.SoundFileName, sizeof( SoundFileName ) );
      xmlNode.Attribute( TYPE_ID( char ), "SoundFileName2", SoundFileName2, &dv6.SoundFileName2, sizeof( SoundFileName2 ) );

      xmlNode.Attribute( TYPE_ID( BOOL ), "EnableCamSpeaker", &bEnableLocalSpeaker, &dv6.bEnableLocalSpeaker );
      xmlNode.Attribute( TYPE_ID( BOOL ), "EnableCamSpeaker", &bEnableCamSpeaker, &dv6.bEnableCamSpeaker );

      xmlNode.Attribute( TYPE_ID( xint ), "EmailOptions", &EmailOptions, &dv6.EmailOptions );
      xmlNode.Attribute( TYPE_ID( int ), "EventZoneType", &m_EventZoneType, &dv6.m_EventZoneType );
      xmlNode.Attribute( TYPE_ID( int ), "CameraType", &m_CameraType, &dv6.m_CameraType );
      xmlNode.End();
   }
   return ( DONE );
}

int EventRuleEx::ReadEventCountInfo( CXMLIO* pIO )
{
   static EventRuleEx dv6;

   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "EventRule_CountInfo" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "ID", &ID );
      xmlNode.Attribute( TYPE_ID( int ), "Count", &ERC_Counting.Count, &dv6.ERC_Counting.Count );
      xmlNode.End();
   }
   return ( DONE );
}

int EventRuleEx::WriteEventCountInfo( CXMLIO* pIO )
{
   static EventRuleEx dv6;

   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "EventRule_CountInfo" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "ID", &ID );
      xmlNode.Attribute( TYPE_ID( int ), "Count", &ERC_Counting.Count, &dv6.ERC_Counting.Count );
      xmlNode.End();
   }
   return ( DONE );
}

void ReadEventCountInfo( CXMLIO* pIO, EventDetection* pEventDetector )
{
   if( NULL == pEventDetector ) return;
   EventRuleEx* pEventRuleEx = (EventRuleEx*)pEventDetector->EventRules.First();
   while( pEventRuleEx ) {
      EventRuleEx eventRuleEx;
      eventRuleEx.ReadEventCountInfo( pIO );
      if( pEventRuleEx->ID == eventRuleEx.ID ) {
         pEventRuleEx->ERC_Counting.Count = eventRuleEx.ERC_Counting.Count;
      }
      pEventRuleEx = (EventRuleEx*)pEventRuleEx->Next;
   }
}

void WriteEventCountInfo( CXMLIO* pIO, EventDetection* pEventDetector )
{
   if( NULL == pEventDetector ) return;
   EventRuleEx* pEventRuleEx = (EventRuleEx*)pEventDetector->EventRules.First();
   while( pEventRuleEx ) {
      pEventRuleEx->WriteEventCountInfo( pIO );
      pEventRuleEx = (EventRuleEx*)pEventRuleEx->Next;
   }
}

EventRule* CreateEventRuleInstance()
{
   return new EventRuleEx;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventZoneEx
//
///////////////////////////////////////////////////////////////////////////////

EventZoneEx::EventZoneEx()

{
   Priority   = 1;
   ZoomFactor = 5.0f;
   ViewingPos( 0.5f, 0.4f );
   sprintf( Name, "Event Zone #%d", ID );

   Options                      = 0;
   PresetID                     = 0;
   Flag_SlavePTZTracking        = TRUE;
   Flag_MagnifyingHumanFaceArea = TRUE;
   Flag_MagnifyingCarPlateArea  = TRUE;

   FILETIME ft;
   BCCL::Time tm;
   tm.GetLocalTime();
   BCCL::Convert( tm, ft );

   Time_Created.low_time  = ft.dwLowDateTime;
   Time_Created.high_time = ft.dwHighDateTime;

   ZoneID.id   = ID;
   ZoneID.time = Time_Created;
#ifdef __SUPPORT_EVENT_ALARM_LEVEL
   EventAlarmLevel = 0;
   int i;
   for( i = 0; i < EventAlarmLevelNum; i++ ) {
      SlaveTrackingDelayTime[i] = 0;
   }
#endif
}

EventZoneEx::~EventZoneEx()
{
}

int EventZoneEx::ReadFile( CXMLIO* pIO )
{
   static EventZoneEx dv4;

   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "EventZoneEx" ) ) {
      EventZone::ReadFile( pIO );
      ZoneID.ReadFile( pIO );
      xmlNode.Attribute( TYPE_ID( int ), "Opt", &Options, &dv4.Options );
      xmlNode.Attribute( TYPE_ID( int ), "PresetID", &PresetID, &dv4.PresetID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "CreatTime", &Time_Created, &dv4.Time_Created );
      xmlNode.Attribute( TYPE_ID( int ), "Priority", &Priority, &dv4.Priority );
      xmlNode.Attribute( TYPE_ID( float ), "ZoomFactor", &ZoomFactor, &dv4.ZoomFactor );
      xmlNode.Attribute( TYPE_ID( FPoint2D ), "ViewingPos", &ViewingPos, &dv4.ViewingPos );
      xmlNode.Attribute( TYPE_ID( BOOL ), "Flag_SlavePTZTrk", &Flag_SlavePTZTracking, &dv4.Flag_SlavePTZTracking );
      xmlNode.Attribute( TYPE_ID( BOOL ), "Flag_MagFace", &Flag_MagnifyingHumanFaceArea, &dv4.Flag_MagnifyingHumanFaceArea );
      xmlNode.Attribute( TYPE_ID( BOOL ), "Flag_MagPlate", &Flag_MagnifyingCarPlateArea, &dv4.Flag_MagnifyingCarPlateArea );
      xmlNode.Attribute( TYPE_ID( int ), "EventAlarmLevel", &EventAlarmLevel, &dv4.EventAlarmLevel );
      xmlNode.Attribute( TYPE_ID( int ), "SlaveTrackingDelayTime", SlaveTrackingDelayTime, dv4.SlaveTrackingDelayTime, 2 );
      xmlNode.End();
   }
   return ( DONE );
}

int EventZoneEx::WriteFile( CXMLIO* pIO )
{
   static EventZoneEx dv4;

   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "EventZoneEx" ) ) {
      EventZone::WriteFile( pIO );
      ZoneID.WriteFile( pIO );
      xmlNode.Attribute( TYPE_ID( int ), "Opt", &Options, &dv4.Options );
      xmlNode.Attribute( TYPE_ID( int ), "PresetID", &PresetID, &dv4.PresetID );
      xmlNode.Attribute( TYPE_ID( FILETIME ), "CreatTime", &Time_Created, &dv4.Time_Created );
      xmlNode.Attribute( TYPE_ID( int ), "Priority", &Priority, &dv4.Priority );
      xmlNode.Attribute( TYPE_ID( float ), "ZoomFactor", &ZoomFactor, &dv4.ZoomFactor );
      xmlNode.Attribute( TYPE_ID( FPoint2D ), "ViewingPos", &ViewingPos, &dv4.ViewingPos );
      xmlNode.Attribute( TYPE_ID( BOOL ), "Flag_SlavePTZTrk", &Flag_SlavePTZTracking, &dv4.Flag_SlavePTZTracking );
      xmlNode.Attribute( TYPE_ID( BOOL ), "Flag_MagFace", &Flag_MagnifyingHumanFaceArea, &dv4.Flag_MagnifyingHumanFaceArea );
      xmlNode.Attribute( TYPE_ID( BOOL ), "Flag_MagPlate", &Flag_MagnifyingCarPlateArea, &dv4.Flag_MagnifyingCarPlateArea );
      xmlNode.Attribute( TYPE_ID( int ), "EventAlarmLevel", &EventAlarmLevel, &dv4.EventAlarmLevel );
      xmlNode.Attribute( TYPE_ID( int ), "SlaveTrackingDelayTime", SlaveTrackingDelayTime, dv4.SlaveTrackingDelayTime, 2 );
      xmlNode.End();
   }
   return ( DONE );
}

EventZone* CreateEventZoneInstance()
{
   return new EventZoneEx;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectFilteringEx
//
///////////////////////////////////////////////////////////////////////////////

ObjectFilteringEx::ObjectFilteringEx()
{
}

int ObjectFilteringEx::ReadFile( CXMLIO* pIO )
{
   static ObjectFilteringEx dv5;

   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "ObjectFilteringEx" ) ) {
      ObjectFiltering::ReadFile( pIO );
      xmlNode.Attribute( TYPE_ID( int ), "MinMaxBoundingBox", &MinMaxBoundingBox, &dv5.MinMaxBoundingBox );
      xmlNode.End();
   }
   return ( DONE );
}

int ObjectFilteringEx::WriteFile( CXMLIO* pIO )
{
   static ObjectFilteringEx dv5;

   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "ObjectFilteringEx" ) ) {
      ObjectFiltering dv_ObjectFiltering;
      ObjectFiltering::WriteFile( pIO );
      xmlNode.Attribute( TYPE_ID( int ), "MinMaxBoundingBox", &MinMaxBoundingBox, &dv5.MinMaxBoundingBox );
      xmlNode.End();
   }
   return ( DONE );
}

int ObjectFilteringEx::IsTheSame( ObjectFiltering& ofx1, ObjectFiltering& ofx2 )
{
   FileIO fioThis, fioFrom;
   CXMLIO xmlThis( &fioThis, XMLIO_Write );
   CXMLIO xmlFrom( &fioFrom, XMLIO_Write );
   ofx1.ObjectFiltering::WriteFile( &xmlThis );
   ofx2.ObjectFiltering::WriteFile( &xmlFrom );
   return !fioThis.IsDiff( fioFrom );
}

ObjectFiltering* CreateObjectFilteringInstance()

{
   return new ObjectFilteringEx();
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventEx
//
///////////////////////////////////////////////////////////////////////////////

EventEx::EventEx()
    : Event( NULL, NULL, 0 )
{
}

EventEx::EventEx( EventZone* evt_zone, EventRule* evt_rule, int evt_type )
    : Event( evt_zone, evt_rule, evt_type )
{
}

EventEx& EventEx::operator=( const EventEx& evt )
{
   ID          = evt.ID;
   Status      = evt.Status;
   NumObjects  = evt.NumObjects;
   ElapsedTime = evt.ElapsedTime;

   EvtRule = evt.EvtRule;
   EvtZone = evt.EvtZone;

   ThumbBuffer = evt.ThumbBuffer;
   ThumbSize   = evt.ThumbSize;

   // EventEx
   EvtRuleID = evt.EvtRuleID;
   EvtZoneID = evt.EvtZoneID;

   Time_EventBegun = evt.Time_EventBegun;

   FrameCount    = evt.FrameCount;
   NPT_StartTime = evt.NPT_StartTime;
   NPT_EndTime   = evt.NPT_EndTime;

   return *this;
}

void EventEx::GenerateExtendedInfo()
{
   if( EvtRule ) {
      EvtRuleID = EvtRule->ID;
   }
   if( EvtZone ) {
      EvtZoneID = EvtZone->ID;
   }
}

Event* CreateEventInstance( EventZone* evt_zone, EventRule* evt_rule, int evt_type )
{
   return new EventEx( evt_zone, evt_rule, evt_type );
}

EventEx* FindEvent( EventList& eventList, int find_id )
{
   EventEx* eventEx = (EventEx*)eventList.First();
   while( eventEx ) {
      if( eventEx->ID == find_id ) {
         return eventEx;
      }
      eventEx = (EventEx*)eventEx->Next;
   }
   return NULL;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: TrackedObjectEx
//
///////////////////////////////////////////////////////////////////////////////

TrackedObjectEx::TrackedObjectEx()
{
   Flag_SlavePTZTracking        = TRUE;
   Flag_MagnifyingHumanFaceArea = TRUE;
   Flag_MagnifyingCarPlateArea  = TRUE;

   StatusEx       = 0;
   EndTime_Locked = 0;
   VAEventFlag    = 0;
   Priority       = 1;
   ZoomFactor     = 5.0f;
   ViewingPos( 0.5f, 0.5f );
   m_bCandedateTrackObject = FALSE;
}

TrackedObjectEx& TrackedObjectEx::operator=( TrackedObjectEx& to )
{
   ////////////////////////////////////////////////////////////////
   // TrackedObjectEx 추가 변수 들
   StatusEx       = to.StatusEx;
   VAEventFlag    = to.VAEventFlag;
   EndTime_Locked = to.EndTime_Locked;
   Scale          = to.Scale;

   // M/S 추적 관련 변수
   Flag_SlavePTZTracking        = to.Flag_SlavePTZTracking;
   Flag_MagnifyingHumanFaceArea = to.Flag_MagnifyingHumanFaceArea;
   Flag_MagnifyingCarPlateArea  = to.Flag_MagnifyingCarPlateArea;

   return *this;
}

int TrackedObjectEx::IsPointInsideBoundingBox( IPoint2D& s_point )
{
   int x_offset = ( Width < 20 ) ? 10 : 0;
   int y_offset = ( Height < 20 ) ? 10 : 0;
   if( ( s_point.X >= ( X - x_offset ) && s_point.X <= ( X + Width + x_offset ) ) && ( s_point.Y >= ( Y - y_offset ) && s_point.Y <= ( Y + Height + y_offset ) ) )
      return TRUE;
   else
      return FALSE;
}

BOOL TrackedObjectEx::ReadXMLForDisplay( CXMLIO* pIO )
{
   static TrackedObjectEx dv; // default value

   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "TO" ) ) {
      xmlNode.Attribute( TYPE_ID( IBox2D ), "B", (IBox2D*)this );
      xmlNode.Attribute( TYPE_ID( int ), "I", &ID );
      xmlNode.Attribute( TYPE_ID( xint ), "T", &Type );
      xmlNode.Attribute( TYPE_ID( xint ), "S", &Status, &dv.Status );
      xmlNode.Attribute( TYPE_ID( xint ), "SE", &StatusEx, &dv.StatusEx );
      xmlNode.Attribute( TYPE_ID( xint ), "ES", &EventStatus, &dv.EventStatus );
      xmlNode.Attribute( TYPE_ID( float ), "A", &Area, &dv.Area );
      xmlNode.Attribute( TYPE_ID( float ), "RW", &RealWidth, &dv.RealWidth );
      xmlNode.Attribute( TYPE_ID( float ), "RH", &RealHeight, &dv.RealHeight );
      xmlNode.Attribute( TYPE_ID( float ), "MAL", &MajorAxisLength, &dv.MajorAxisLength );
      xmlNode.Attribute( TYPE_ID( float ), "ALR", &AxisLengthRatio, &dv.AxisLengthRatio );
      xmlNode.Attribute( TYPE_ID( float ), "AR", &AspectRatio, &dv.AspectRatio );
      xmlNode.Attribute( TYPE_ID( float ), "Sp", &Speed, &dv.Speed );
      xmlNode.Attribute( TYPE_ID( float ), "NSp", &NorSpeed, &dv.NorSpeed );
      xmlNode.Attribute( TYPE_ID( float ), "RSp", &RealSpeed, &dv.RealSpeed );
      xmlNode.Attribute( TYPE_ID( float ), "RA", &RealArea, &dv.RealArea );
      xmlNode.Attribute( TYPE_ID( float ), "RMAL", &RealMajorAxisLength, &dv.RealMajorAxisLength );
      if( IS_SUPPORT( __SUPPORT_FISHEYE_DEWARPING ) ) {
         xmlNode.Attribute( TYPE_ID( float ), "MAA", &MajorAxisAngle1, &dv.MajorAxisAngle1 );
         xmlNode.Attribute( TYPE_ID( float ), "MAL", &MinorAxisLength, &dv.MinorAxisLength );
         xmlNode.Attribute( TYPE_ID( FPoint2D ), "CP", &CenterPos, &dv.CenterPos );
         xmlNode.Attribute( TYPE_ID( FPoint2D ), "MA", &MajorAxis, &dv.MajorAxis );
         xmlNode.Attribute( TYPE_ID( FPoint2D ), "MnA", &MinorAxis, &dv.MinorAxis );
         xmlNode.Attribute( TYPE_ID( FPoint2D ), "Sc", &Scale, &dv.Scale );
      }
      xmlNode.Attribute( TYPE_ID( xint ), "EF", &VAEventFlag, &dv.VAEventFlag );
      xmlNode.Attribute( TYPE_ID( float ), "TD", &Time_Dwell, &dv.Time_Dwell );
   }

   return ( TRUE );
}

BOOL TrackedObjectEx::WriteXMLForDisplay( CXMLIO* pIO )
{
   static TrackedObjectEx dv; // default value

   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "TO" ) ) {
      xmlNode.Attribute( TYPE_ID( IBox2D ), "B", (IBox2D*)this );
      xmlNode.Attribute( TYPE_ID( int ), "I", &ID );
      xmlNode.Attribute( TYPE_ID( xint ), "T", &Type );
      xmlNode.Attribute( TYPE_ID( xint ), "S", &Status, &dv.Status );
      xmlNode.Attribute( TYPE_ID( xint ), "SE", &StatusEx, &dv.StatusEx );
      xmlNode.Attribute( TYPE_ID( xint ), "ES", &EventStatus, &dv.EventStatus );
      xmlNode.Attribute( TYPE_ID( float ), "A", &Area, &dv.Area );
      xmlNode.Attribute( TYPE_ID( float ), "RW", &RealWidth, &dv.RealWidth );
      xmlNode.Attribute( TYPE_ID( float ), "RH", &RealHeight, &dv.RealHeight );
      xmlNode.Attribute( TYPE_ID( float ), "MAL", &MajorAxisLength, &dv.MajorAxisLength );
      xmlNode.Attribute( TYPE_ID( float ), "ALR", &AxisLengthRatio, &dv.AxisLengthRatio );
      xmlNode.Attribute( TYPE_ID( float ), "AR", &AspectRatio, &dv.AspectRatio );
      xmlNode.Attribute( TYPE_ID( float ), "Sp", &Speed, &dv.Speed );
      xmlNode.Attribute( TYPE_ID( float ), "NSp", &NorSpeed, &dv.NorSpeed );
      xmlNode.Attribute( TYPE_ID( float ), "RSp", &RealSpeed, &dv.RealSpeed );
      xmlNode.Attribute( TYPE_ID( float ), "RA", &RealArea, &dv.RealArea );
      xmlNode.Attribute( TYPE_ID( float ), "RMAL", &RealMajorAxisLength, &dv.RealMajorAxisLength );
      if( IS_SUPPORT( __SUPPORT_FISHEYE_DEWARPING ) ) {
         xmlNode.Attribute( TYPE_ID( float ), "MAA", &MajorAxisAngle1, &dv.MajorAxisAngle1 );
         xmlNode.Attribute( TYPE_ID( float ), "MAL", &MinorAxisLength, &dv.MinorAxisLength );
         xmlNode.Attribute( TYPE_ID( FPoint2D ), "CP", &CenterPos, &dv.CenterPos );
         xmlNode.Attribute( TYPE_ID( FPoint2D ), "MA", &MajorAxis, &dv.MajorAxis );
         xmlNode.Attribute( TYPE_ID( FPoint2D ), "MnA", &MinorAxis, &dv.MinorAxis );
         xmlNode.Attribute( TYPE_ID( FPoint2D ), "Sc", &Scale, &dv.Scale );
      }
      xmlNode.Attribute( TYPE_ID( xint ), "EF", &VAEventFlag, &dv.VAEventFlag );
      xmlNode.Attribute( TYPE_ID( float ), "TD", &Time_Dwell, &dv.Time_Dwell );
   }

   return ( TRUE );
}

void TrackedObjectEx::AddTrjectoryItem()
{
   int i;
   if( ( Count_Tracked % 3 ) == 1 ) {
      if( TrjLength >= Trajectory.Length ) {
         for( i = 1; i < Trajectory.Length; i++ )
            Trajectory[i - 1] = Trajectory[i];
         Trajectory[Trajectory.Length - 1] = CenterPos;
      } else
         Trajectory[TrjLength++] = CenterPos;
   }
}

TrackedObject* CreateTrackedObjectInstance()
{
   return new TrackedObjectEx;
}

///////////////////////////////////////////////////////////////////////////////
//
// IVAProcess Input/Output functions
//
///////////////////////////////////////////////////////////////////////////////

TrackedObjectEx* FindTrackedObject( TrackedObjectList& trackedObjectList, int find_id )
{
   TrackedObjectEx* t_object = (TrackedObjectEx*)trackedObjectList.First();
   while( t_object ) {
      if( t_object->ID == find_id ) {
         return t_object;
      }
      t_object = (TrackedObjectEx*)t_object->Next;
   }
   return NULL;
}

/////////////////////////////////////////////////////////////////////
//
// Class : Schedule
//
/////////////////////////////////////////////////////////////////////

BOOL Schedule::Read( CXMLIO* pIO )
{
   if( !Schedule_WD || !Schedule_HM || !Schedule_YMD )
      return FALSE;

   int i, n_items;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Schedule" ) ) {
      xmlNode.Attribute( TYPE_ID( xint ), "EventS_WD", Schedule_WD );

      xmlNode.Attribute( TYPE_ID( int ), "PeriodHM_Num", &n_items );
      Schedule_HM->Delete();
      for( i = 0; i < n_items; i++ ) {
         Period_HM* period_hm = new Period_HM;
         if( DONE == period_hm->ReadFile( pIO ) )
            Schedule_HM->Add( period_hm );
         else
            delete period_hm;
      }
      xmlNode.Attribute( TYPE_ID( int ), "PeriodYMD_Num", &n_items );
      Schedule_YMD->Delete();
      for( i = 0; i < n_items; i++ ) {
         Period_YMD* period_ymd = new Period_YMD;
         if( DONE == period_ymd->ReadFile( pIO ) )
            Schedule_YMD->Add( period_ymd );
         else
            delete period_ymd;
      }
      xmlNode.End();
      return TRUE;
   }
   return FALSE;
}

BOOL Schedule::Write( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Schedule" ) ) {
      xmlNode.Attribute( TYPE_ID( xint ), "EventS_WD", Schedule_WD );
      int n_items = Schedule_HM->GetNumItems();
      xmlNode.Attribute( TYPE_ID( int ), "PeriodHM_Num", &n_items );
      Period_HM* period_hm = Schedule_HM->First();
      while( period_hm ) {
         period_hm->WriteFile( pIO );
         period_hm = Schedule_HM->Next( period_hm );
      }
      n_items = Schedule_YMD->GetNumItems();
      xmlNode.Attribute( TYPE_ID( int ), "PeriodYMD_Num", &n_items );
      Period_YMD* period_ymd = Schedule_YMD->First();
      while( period_ymd ) {
         period_ymd->WriteFile( pIO );
         period_ymd = Schedule_YMD->Next( period_ymd );
      }
      xmlNode.End();
      return TRUE;
   }
   return FALSE;
}

BOOL Schedule::CheckSchedule()
{
   // TYLEE: 2015-06-12
   // ctime localtime() --> SYSTEMTIME베이스, GetLocalTime()로 바꿈 : 기존방식은 얻어진값이 오류임
   SYSTEMTIME systime;
   FILETIME filetime;
   BCCL::Time tm;
   tm.GetLocalTime();
   BCCL::Convert( tm, filetime );
   BCCL::Convert( filetime, systime );

   // 요일 설정체크.
   int mask = 0x00000001 << systime.wDayOfWeek;
   *Schedule_WD;
   if( !( ( *Schedule_WD ) & mask ) )
      return ( FALSE );

   // 날짜가 설정되어있지 않으면 모든날짜.
   if( Schedule_YMD->GetNumItems() ) {
      Period_YMD* period_ymd = Schedule_YMD->First();
      while( period_ymd != NULL ) {
         if( period_ymd->IsTimeInsidePeriod( systime.wYear, systime.wMonth, systime.wDay ) ) break;
         period_ymd = Schedule_YMD->Next( period_ymd );
      }
      if( period_ymd == NULL )
         return ( FALSE );
   }

   // 시간이 설정되어있지 않으면 모든시간
   if( Schedule_HM->GetNumItems() ) {
      Period_HM* period_hm = Schedule_HM->First();
      while( period_hm != NULL ) {
         if( period_hm->IsTimeInsidePeriod( systime.wHour, systime.wMinute ) ) break;
         period_hm = Schedule_HM->Next( period_hm );
      }
      if( period_hm == NULL )
         return ( FALSE );
   }

   return ( TRUE );
}

BOOL Schedule::IsDefaultSetting()
{
   if( NULL == Schedule_WD )
      return FALSE;
   if( *Schedule_WD != ( ER_WEEKDAY_MON | ER_WEEKDAY_TUE | ER_WEEKDAY_WED | ER_WEEKDAY_THU | ER_WEEKDAY_FRI | ER_WEEKDAY_SAT | ER_WEEKDAY_SUN ) )
      return FALSE;
   if( NULL == Schedule_HM )
      return FALSE;
   if( 0 != Schedule_HM->GetNumItems() )
      return FALSE;
   if( NULL == Schedule_YMD )
      return FALSE;
   if( 0 != Schedule_YMD->GetNumItems() )
      return FALSE;

   return TRUE;
}

//#pragma pack()

﻿/*
 * Author: jong oh [2018.07.04]
 */

#include "StdAfx.h"
#include "TimerEx.h"
#include "EventEx.h"

#include <vector>
#include <thread>

#ifdef WIN32
#include "mmiscapi2.h"
#pragma comment( lib, "winmm.lib" )
#else
#include <csignal>
#include <ctime>
#include <unistd.h>
#endif

//////////////////////////////////////////////////////////////////////////
//
// Define
//
//////////////////////////////////////////////////////////////////////////

#ifdef WIN32
typedef UINT TimerIdOs;
#elif __linux
//#define MY_TIMER_SIGNAL SIGRTMIN
#define MY_TIMER_SIGNAL SIGVTALRM
#define ONE_MSEC_TO_NSEC 1000000
#define ONE_SEC_TO_NSEC 1000000000
typedef void ( *sa_sigaction_ex )( int, siginfo_t*, void* );
typedef timer_t TimerIdOs;
#endif // __linux

//////////////////////////////////////////////////////////////////////////
//
// class CTimerInstance
//
//////////////////////////////////////////////////////////////////////////

using namespace timer_ex;

class CTimerInstance {

public:
   struct ParamTimer {
      bool used    = false;
      TimerIdOs id = 0;
      uint ms       = 0;
      std::function<void( void* ptr )> func;
      void* ptr = nullptr;
   };

private:
#ifdef WIN32
#else
   std::thread m_threadTimerSignal;
   CEventEx m_evtTimerExit;
#endif
   std::vector<ParamTimer> m_vecTimer;

private:
#ifdef WIN32
#else
   void threadTimerSignal()
   {
#ifdef SUPPORT_PTHREAD_NAME
      pthread_setname_np( pthread_self(), "TimerSignal" );
#endif

      sigset_t tSigSetMask;

      sigemptyset( &tSigSetMask );
      sigaddset( &tSigSetMask, MY_TIMER_SIGNAL );
      sigprocmask( SIG_BLOCK, &tSigSetMask, NULL );
      pthread_sigmask( SIG_UNBLOCK, &tSigSetMask, NULL );

      //sigfillset(&sa.sa_mask);
      //sigdelset(&sa.sa_mask, MY_TIMER_SIGNAL);

      while( true ) {
         m_evtTimerExit.Wait();
         break;
      }

      return;
   }

   /**
    * @brief InitSignal
    *
    * 닫혀 있는 소켓에 데이터를 전송 할 시에 SIGPIPE 시그널이 발생하면서
    * 프로그램을 종료 하는 문제를 처리 하기 위함
    * @return
    */
   int InitSignal()
   {
      struct sigaction act;
      memset( &act, 0, sizeof( act ) );

      act.sa_handler = SIG_IGN;
      //act.sa_flags = SA_RESTART;
      act.sa_flags = 0;
      if( sigaction( SIGPIPE, &act, 0 ) )
         fprintf( stderr, "sigaction" );

      return 0;
   }
#endif

#ifdef WIN32
   static void TimerCallback( UINT id, UINT msg, DWORD dwUser, DWORD dw1, DWORD dw2 )
   {
      CTimerInstance::getInstance().CallBack( (TimerIdEx)id );
   }
#else
   static void TimerCallback( int sig, siginfo_t* si, void* context )
   {
      int key = si->si_value.sival_int;
      CTimerInstance::getInstance().CallBack( (TimerIdEx)key );
   }
#endif

   void CallBack( TimerIdEx id )
   {
      ParamTimer& output = m_vecTimer[(size_t)id];
      output.func( output.ptr );
   }

   TimerIdEx CreateTimerId()
   {
      int index = 0;
      for( ParamTimer& item : m_vecTimer ) {
         if( false == item.used )
            break;
         index++;
      }

      return (TimerIdEx)index;
   }

public:
   CTimerInstance()
       : m_vecTimer( 128 )
   {
      Init();
   }

   ~CTimerInstance()
   {
      Uninit();
   }

   static CTimerInstance& getInstance()
   {
      static CTimerInstance instance;
      return instance;
   }

   int Init()
   {
#ifdef WIN32
      return 0;
#else
      // 160726 박정환 책임 추가...
      InitSignal();

      // 모든 Thread 에서 Timer signal 이벤트를 받지 않도록 한다.
      signal( MY_TIMER_SIGNAL, SIG_IGN );

      sigset_t tSigSetMask;
      sigemptyset( &tSigSetMask );
      sigaddset( &tSigSetMask, MY_TIMER_SIGNAL );
      //pthread_sigmask(SIG_SETMASK, &tSigSetMask, 0);
      pthread_sigmask( SIG_BLOCK, &tSigSetMask, 0 );

      // Timer Thread 에서만 Timer signal을 받도록 설정한다.
      // 다른 Thread 에서도 Timer signal 신호를 받게 되면 데드락 문제가 발생할 수 있다.
      m_threadTimerSignal = std::thread( std::bind( &CTimerInstance::threadTimerSignal, this ) );

      return 0;
#endif
   }

   int Uninit()
   {
      int index = 0;
      for( ParamTimer& item : m_vecTimer ) {
         if( item.used )
            DeleteTimer((TimerIdEx)index);
         index++;
      }

#ifdef WIN32
      return 0;
#else
      m_evtTimerExit.SetEvent();

      if( m_threadTimerSignal.joinable() )
         m_threadTimerSignal.join();

      return 0;
#endif
   }

   int CreateTimer( TimerIdEx& id, const ParamTimer& param )
   {
#ifdef WIN32
      timeBeginPeriod( 1 );
      MMRESULT hTimer = timeSetEvent( ms, 1, CTimerInstance::TimerCallback, this, TIME_PERIODIC );
      if( NULL == hTimer )
         return 1;

      // Save Timer Inforamtion
      id = CreateTimerId();

      ParamTimer& item = m_vecTimer[id];
      item.ms          = param.ms;
      item.func        = param.func;
      item.ptr         = param.ptr;
      item.used        = true;

      return 0;
#else
      TimerIdOs timerId = 0;

      // set up signal handler
      struct sigaction sa;
      memset( &sa, 0, sizeof( sa ) );
      sa.sa_flags     = SA_SIGINFO;
      sa.sa_sigaction = CTimerInstance::TimerCallback;
      if( sigemptyset( &sa.sa_mask ) ) {
         LOGE << "error sigemptyset";
         return 1;
      }
      if( sigaction( MY_TIMER_SIGNAL, &sa, 0 ) ) {
         LOGE << "error sigaction";
         return 2;
      }

      id = CreateTimerId();

      // create alarm
      struct sigevent sigEvt;
      memset( &sigEvt, 0x00, sizeof( sigEvt ) );
      sigEvt.sigev_notify          = SIGEV_SIGNAL;
      sigEvt.sigev_signo           = MY_TIMER_SIGNAL;
      sigEvt.sigev_value.sival_int = id;
      //sigEvt.sigev_value.sival_ptr = this;
      if( timer_create( CLOCK_REALTIME, &sigEvt, &timerId ) ) {
         LOGE << "error timer_create";
         return 3;
      }

      // Save Timer Inforamtion
      ParamTimer& item = m_vecTimer[id];
      item.id          = timerId;
      item.ms          = param.ms;
      item.func        = param.func;
      item.ptr         = param.ptr;
      item.used        = true;

      // set alarm
      struct itimerspec its;
      long nano_intv          = param.ms * ONE_MSEC_TO_NSEC;
      its.it_value.tv_sec     = nano_intv / ONE_SEC_TO_NSEC;
      its.it_value.tv_nsec    = nano_intv % ONE_SEC_TO_NSEC;
      its.it_interval.tv_sec  = its.it_value.tv_sec;
      its.it_interval.tv_nsec = its.it_value.tv_nsec;
      if( timer_settime( timerId, 0, &its, NULL ) ) {
         LOGE << "timer_settime";
         return 4;
      }

      //logi( "Create Timer   id[%d]  ms[%d]\n", id, param.ms );
      return 0;
#endif
   }

   int DeleteTimer( const TimerIdEx& id )
   {
      size_t index = (size_t)id;
      if( m_vecTimer.size() <= index )
         return 1;

      ParamTimer& item = m_vecTimer[index];

#ifdef WIN32
      timeKillEvent( (MMRESULT)output.id );
      timeEndPeriod( 1 );
#else
      timer_delete( (timer_t)item.id );
#endif

      item.used = false;
      //logi( "Delete Timer   id[%d]  ms[%d]\n", id, item.ms );

      return 0;
   }
};

//////////////////////////////////////////////////////////////////////////
//
// api
//
//////////////////////////////////////////////////////////////////////////

namespace timer_ex {
int ActiveTimer()
{
   CTimerInstance::getInstance();
   return 0;
}

int CreateTimer( TimerIdEx& id, uint ms, std::function<void( void* ptr )> func, void* ptr )
{
   CTimerInstance& instance = CTimerInstance::getInstance();

   CTimerInstance::ParamTimer param;
   param.ms   = ms;
   param.func = func;
   param.ptr  = ptr;

   return instance.CreateTimer( id, param );
}

int DeleteTimer( const TimerIdEx& id )
{
   CTimerInstance& instance = CTimerInstance::getInstance();

   return instance.DeleteTimer( id );
}
}; // namespace timer_ex

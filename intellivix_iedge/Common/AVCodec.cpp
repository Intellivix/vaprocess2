﻿#include "StdAfx.h"
#include "AVCodec.h"

#include <mutex>
//#include "vacl_vidstab.h"
//#include "vacl_defog.h"
#include "version.h"

//using namespace VACL;

BOOL g_bDebugAVCodec = FALSE;
CCriticalSection g_csAVCodec;

///////////////////////////////////////////////////////////////////////////////
//
// Struct: CAVCodecParam
//
///////////////////////////////////////////////////////////////////////////////

CAVCodecParam::CAVCodecParam()
{
   InitAVCodecParam();
}

CAVCodecParam& CAVCodecParam::operator=( CAVCodecParam& from )
{
   Set( from );
   return *this;
}

BOOL CAVCodecParam::operator==( CAVCodecParam& from )
{
   return IsEqual( from );
}

BOOL CAVCodecParam::operator!=( CAVCodecParam& from )
{
   if( *this == from ) return FALSE;
   return TRUE;
}

void CAVCodecParam::InitAVCodecParam()
{
   m_nCodecID           = VCODEC_ID_NONE;
   m_nWidth             = 0;
   m_nHeight            = 0;
   m_nBitrate           = 1000 * 2048;
   m_nPixelFormat       = VPIX_FMT_YUY2;
   m_fFrameRate         = 0;
   m_fDecodingFPS       = 0;
   m_bUseHWAcceleration = FALSE;
}

void CAVCodecParam::Set( const CAVCodecParam& from )
{
   m_nCodecID           = from.m_nCodecID;
   m_nWidth             = from.m_nWidth;
   m_nHeight            = from.m_nHeight;
   m_nPixelFormat       = from.m_nPixelFormat;
   m_fFrameRate         = from.m_fFrameRate;
   m_nBitrate           = from.m_nBitrate;
   m_fDecodingFPS       = from.m_fDecodingFPS;
   m_bUseHWAcceleration = from.m_bUseHWAcceleration;
}

BOOL CAVCodecParam::IsEqual( const CAVCodecParam& from )
{
   if( m_nCodecID != from.m_nCodecID )
      return FALSE;
   if( m_nWidth != from.m_nWidth )
      return FALSE;
   if( m_nHeight != from.m_nHeight )
      return FALSE;
   if( m_nPixelFormat != from.m_nPixelFormat )
      return FALSE;
   if( m_fFrameRate != from.m_fFrameRate )
      return FALSE;
   if( m_fDecodingFPS != from.m_fDecodingFPS )
      return FALSE;
   if( m_nBitrate != from.m_nBitrate )
      return FALSE;
   return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: IAVCoDecoder
//
///////////////////////////////////////////////////////////////////////////////

int IAVCoDecoder::OpenEncoder( CAVCodecParam& avCodecParam )
{
   CAVCodecParam& this_param = GetAVCodecParam();
   this_param                = avCodecParam;
   return ( DONE );
}

int IAVCoDecoder::OpenDecoder( CAVCodecParam& avCodecParam )
{
   CAVCodecParam& this_param = GetAVCodecParam();
   this_param                = avCodecParam;
   return ( DONE );
}

int IAVCoDecoder::OpenDecoder( AVCodecContext* codec_ctx )
{
   return ( DONE );
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CAVCoDecoder_FFMPEG
//
///////////////////////////////////////////////////////////////////////////////

CAVCoDecoder_FFMPEG::~CAVCoDecoder_FFMPEG()
{
   Close();
   m_nCount_NeedMoreFrame = 0;
}

void CAVCoDecoder_FFMPEG::Close()
{
   m_VideoCodec.Close();
}

int CAVCoDecoder_FFMPEG::OpenEncoder( CAVCodecParam& avCodecParam )
{
   Close();
   IAVCoDecoder::OpenEncoder( avCodecParam );
   //int   nGammaQuality = int (pow (float (m_nQuality), 2.0f) / pow (100.0f, 2.0f) * 100);
   int nGammaQuality             = 100;
   int nBitPerPixel              = 16;
   float fMaxEncodedImgSizeRatio = 1.0f;
   m_VideoCodec.GOPSize          = 30;
   enum AVCodecID nCodecID       = GetFFMPEGCodecID( m_nCodecID );
   m_VideoCodec.Bitrate          = avCodecParam.m_nBitrate;
   if( 0 == m_VideoCodec.Bitrate ) {
      switch( nCodecID ) {
      case AV_CODEC_ID_MPEG4: {
         fMaxEncodedImgSizeRatio = 0.125f;
         m_VideoCodec.Bitrate    = int( ( m_nWidth * m_nHeight * m_fFrameRate * nBitPerPixel ) * fMaxEncodedImgSizeRatio * ( (nGammaQuality)*1.0f / 99.0f + 0.01f ) );
      } break;
      case AV_CODEC_ID_MJPEG: {
         fMaxEncodedImgSizeRatio = 0.455f;
         m_VideoCodec.Bitrate    = int( ( m_nWidth * m_nHeight * m_fFrameRate * nBitPerPixel ) * fMaxEncodedImgSizeRatio * ( (nGammaQuality)*1.0f / 99.0f + 0.01f ) );
         // jun : 특정 값 이상이 되면 인코더 오픈에 실패함..
         if( m_VideoCodec.Bitrate > int( m_fFrameRate ) * 4 * 1000000 ) m_VideoCodec.Bitrate = int( m_fFrameRate ) * 4 * 1000000;
      } break;
      case AV_CODEC_ID_H264: {
         m_VideoCodec.Bitrate = int( ( m_nWidth * m_nHeight * m_fFrameRate * nBitPerPixel ) * fMaxEncodedImgSizeRatio * ( (nGammaQuality)*1.0f / 99.0f + 0.01f ) );
      } break;
      case AV_CODEC_ID_H263:
      case AV_CODEC_ID_H263P: {
         fMaxEncodedImgSizeRatio = 0.125f;
         m_VideoCodec.Bitrate    = int( ( m_nWidth * m_nHeight * m_fFrameRate * nBitPerPixel ) * fMaxEncodedImgSizeRatio * ( (nGammaQuality)*1.0f / 99.0f + 0.01f ) );
      } break;
      default: {
         logi( "ERROR: The specified video codec is not supported.\n" );
         return ( 1 );
      }
      }
   }

   enum AVPixelFormat nPixelFormat = GetRealPixelFormat_FFMPEG( m_nPixelFormat );
   if( nPixelFormat == AV_PIX_FMT_NONE ) {
      logi( "ERROR: The specified pixel format is not supported.\n" );
      return ( 2 );
   }
   int nErrorCode = m_VideoCodec.OpenEncoder( nCodecID, m_nWidth, m_nHeight, m_fFrameRate );
   if( nErrorCode ) {
      logi( "ERROR: Cannot open the video encoder.\n" );
      return ( 3 );
   }
   return ( DONE );
}

int CAVCoDecoder_FFMPEG::OpenDecoder( CAVCodecParam& avCodecParam )
{
   Close();

   IAVCoDecoder::OpenDecoder( avCodecParam );
   enum AVCodecID nCodecID = GetFFMPEGCodecID( m_nCodecID );
   if( nCodecID == AV_CODEC_ID_NONE ) {
      logi( "ERROR: The specified video codec is not supported.\n" );
      return ( 1 );
   }
   enum AVPixelFormat nPixelFormat = GetRealPixelFormat_FFMPEG( m_nPixelFormat );
   if( nPixelFormat == AV_PIX_FMT_NONE ) {
      logi( "ERROR: The specified pixel format is not supported.\n" );
      return ( 2 );
   }

   m_VideoCodec.PixFmt = nPixelFormat; // Output Image Format Setting
   int nErrorCode = m_VideoCodec.OpenDecoder( nCodecID, m_nWidth, m_nHeight );
   if( nErrorCode ) {
      logi( "ERROR: Cannot open the video decoder (Error code:%d).\n", nErrorCode );
      return ( 2 );
   }
   m_nCount_NeedMoreFrame    = 0;
   m_nCountInterPictuerFrame = 0;
   return ( DONE );
}

int CAVCoDecoder_FFMPEG::OpenDecoder( AVCodecContext* codec_ctx )
{
   Close();
   int nErrorCode = m_VideoCodec.OpenDecoder( codec_ctx );
   if( nErrorCode ) {
      logi( "ERROR: Cannot open the video decoder (Error code:%d).\n", nErrorCode );
      return ( 2 );
   }
   m_nCount_NeedMoreFrame    = 0;
   m_nCountInterPictuerFrame = 0;

   return ( DONE );
}

int CAVCoDecoder_FFMPEG::Compress( byte* pSrcImgBuff, byte** ppCompBuff, int& nCompBuffSize, int& nFlags )
{
   {
      *ppCompBuff   = NULL;
      nCompBuffSize = 0;
      int bKeyFrame = FALSE;
      if( nFlags & VFRM_KEYFRAME )
         bKeyFrame = TRUE;
      if( m_VideoCodec.Encode( pSrcImgBuff, ppCompBuff, nCompBuffSize, bKeyFrame ) ) {
         logi( "ERROR: Cannot compress the video frame.\n" );
         return ( 1 );
      }
      if( bKeyFrame )
         nFlags = VFRM_KEYFRAME;
      else
         nFlags = 0;
      return ( DONE );
   }
   return ( 1 );
}

int CAVCoDecoder_FFMPEG::Compress( BGRImage& s_image, byte** ppCompBuff, int& nCompBuffSize, int& nFlags )
{
   int bKeyFrame = FALSE;
   if( nFlags & VFRM_KEYFRAME ) bKeyFrame = TRUE;
   if( m_VideoCodec.Encode( s_image, ppCompBuff, nCompBuffSize, bKeyFrame ) ) {
      logi( "ERROR: Cannot compress the video frame.\n" );
      return ( 1 );
   }
   if( bKeyFrame )
      nFlags = VFRM_KEYFRAME;
   else
      nFlags = 0;
   return ( DONE );
}

int CAVCoDecoder_FFMPEG::Decompress( byte* pCompBuff, int nCompBuffSize, byte* pDstImgBuff, int nFlags )
{
   int nDecodeRet = ( AVCodecDecompressRet_Success );
   if( VFRM_ODD_EVEN_FIELD & nFlags ) {
      if( VFRM_DECODE_ODD_FILED_ONLY & nFlags ) {
         nDecodeRet = m_VideoCodec.Decode( pCompBuff, nCompBuffSize, pDstImgBuff );
      } else {
         byte* pDecodeBuff;
         nDecodeRet = m_VideoCodec.Decode( pCompBuff, nCompBuffSize, &pDecodeBuff );
         if( AVCodecDecompressRet_Success == nDecodeRet ) {
            if( m_nPixelFormat == VPIX_FMT_YUY2 ) {
               InterlacingEventOddField( pDecodeBuff, pDstImgBuff, m_nWidth, m_nHeight );
            } else {
               if( g_bDebugAVCodec ) logd( "ERROR: Cannot deinterlace the video frame.\n" );
            }
         }
      }
   } else {
      nDecodeRet = m_VideoCodec.Decode( pCompBuff, nCompBuffSize, pDstImgBuff );
   }
   m_ImageSizeOfEncodedData.Width  = m_VideoCodec.DecodedFrameSize.Width;
   m_ImageSizeOfEncodedData.Height = m_VideoCodec.DecodedFrameSize.Height;
   // jun: 디코딩을 시작한 시점부터 몇 프레임 정도는 디코딩에 실패하는 경우가 있는데 이때 에러를 리턴해서는 안된다.
   //      현재는 30프레임 정도는 기다려주는데 입력소스마다 다르다. (ex 산요:4프레임)
   // jun: 다후아 카메라의 경우 네트워크 상황이 좋지 않은 경우에는 10~20프레임 정도 에러를 리턴하는 경우가 발생한다.
   if( m_nCount_NeedMoreFrame > 5 ) {
      m_nCount_NeedMoreFrame = 0;
      return ( AVCodecDecompressRet_Failed );
   }
   if( AVCodecDecompressRet_Success == nDecodeRet ) {
      m_nCount_NeedMoreFrame = 0;
   }

   if( nDecodeRet == 4 )
      return AVCodecDecompressRet_ImageSizeIsChanged;

   if( nDecodeRet < 0 || nDecodeRet == 2 ) {
      m_nCount_NeedMoreFrame++;
      return ( AVCodecDecompressRet_NeedMoreFrame );
   }
   if( nDecodeRet == 3 ) {
      return ( AVCodecDecompressRet_ImageSizeDoesNotMatched );
   } else
      return nDecodeRet;
}

int CAVCoDecoder_FFMPEG::Resize( GImage& s_image, GImage& d_image )
{
   return m_VideoCodec.Resize( AV_PIX_FMT_GRAY8, s_image, s_image.Width, s_image.Height, d_image, d_image.Width, d_image.Height );
}

int CAVCoDecoder_FFMPEG::ConvertYUY2ToGray( byte* s_buffer, int nWidth, int nHeight, GImage& d_image )
{
   return m_VideoCodec.Convert( AV_PIX_FMT_YUYV422, s_buffer, nWidth, nHeight, AV_PIX_FMT_GRAY8, d_image, d_image.Width, d_image.Height );
}

int CAVCoDecoder_FFMPEG::Decompress( byte* pCompBuff, int nCompBuffSize, BGRImage& d_image, int nFlags )
{
   int ret = m_VideoCodec.Decode( pCompBuff, nCompBuffSize, d_image );
   if( ret == 4 )
      return AVCodecDecompressRet_ImageSizeIsChanged;

   if( ret ) {
      if( g_bDebugAVCodec ) loge( "ERROR: Cannot decompress the video frame.\n" );
      return ( AVCodecDecompressRet_Failed );
   }
   if( nFlags & VFRM_DECODE_ODD_FILED_ONLY ) {
      if( g_bDebugAVCodec ) loge( "ERROR: Cannot deinterlace the video frame.\n" );
      return ( AVCodecDecompressRet_Failed );
   }
   return ( DONE );
}

void CAVCoDecoder_FFMPEG::DeinterlaceVideoFrame_YUY2( byte* s_img_buf, int width, int height, byte* d_img_buf )
{
   int i;

   int hh = height / 2;
   int w2 = width * 2;
   int w4 = w2 * 2;
   for( i = 0; i < hh; i++ )
      memcpy( (void*)( d_img_buf + i * w4 ), (const void*)( s_img_buf + i * w2 ), w2 );
   for( i = hh; i < height; i++ )
      memcpy( (void*)( d_img_buf + ( i - hh ) * w4 + w2 ), (const void*)( s_img_buf + i * w2 ), w2 );
}

int CAVCoDecoder_FFMPEG::DecompressKeyFrame( byte* pCompBuff, int nCompBuffSize, byte* pDstImgBuff, int nFlags )
{
   m_VideoCodec.Decode( pCompBuff, nCompBuffSize, pDstImgBuff );
   return 0;
}

#ifdef __SUPPORT_MFC_CODEC // H.264 하드웨어 가속일때 실패하면 기본 코덱 라이브러리로 열어본다.
///////////////////////////////////////////////////////////////////////////////
//
// Class: CAVCoDecoder_MFC
//
///////////////////////////////////////////////////////////////////////////////
#include "../../../lib/MFCLib/inc/mfc.h"
#include "../../../lib/MFCLib/inc/common.h"

CAVCoDecoder_MFC::CAVCoDecoder_MFC()
{
   m_pMFCCodec            = nullptr;
   m_nCount_NeedMoreFrame = 0;
   m_nDecodingCount       = 0;
   m_nFirstDecoding       = 0;
   m_nActiveDecodingCount = 0;
}

CAVCoDecoder_MFC::~CAVCoDecoder_MFC()
{
   Close();
}

void CAVCoDecoder_MFC::Close()
{
   if( nullptr != m_pMFCCodec ) {
      //m_pMFCCodec->CloseMfc();
      delete m_pMFCCodec;
      m_pMFCCodec = nullptr;
   }
}

int CAVCoDecoder_MFC::OpenEncoder( CAVCodecParam& avCodecParam )
{
   return ( 1 );
}

int CAVCoDecoder_MFC::OpenDecoder( CAVCodecParam& avCodecParam )
{
   Close();
   IAVCoDecoder::OpenDecoder( avCodecParam );

   logd( "CAVCoDecoder_MFC::OpenDecoder START [Codec: %d, PX %d] [%d x %d] [FR: %02.2f] [DFPS : %02.2f]\n", m_nCodecID, m_nPixelFormat, m_nWidth, m_nHeight, m_fFrameRate, m_fDecodingFPS );

   if( nullptr == m_pMFCCodec ) {
      m_pMFCCodec = new CMFC( DEVICE_DECODE );
   }

   if( V4L2_OK != m_pMFCCodec->InitMfc( m_nCodecID, STREAM_BUFFER_CNT ) )
      return 1;

   m_pMFCCodec->MFCServiceRun();

   m_nCount_NeedMoreFrame = 0;
   m_nDecodingCount       = 0;
   m_nFirstDecoding       = 0;
   m_nActiveDecodingCount = 0;

   logd( "CAVCoDecoder_MFC::OpenDecoder END [Codec: %d]\n", m_nCodecID );

   return ( DONE );
}

int CAVCoDecoder_MFC::OpenDecoder( AVCodecContext* codec_ctx )
{
   return ( 1 );
}

int CAVCoDecoder_MFC::Compress( byte* pSrcImgBuff, byte** ppCompBuff, int& nCompBuffSize, int& nFlags )
{
   return ( 1 );
}

int CAVCoDecoder_MFC::Compress( BGRImage& s_image, byte** ppCompBuff, int& nCompBuffSize, int& nFlags )
{
   return ( 1 );
}

int CAVCoDecoder_MFC::Decompress( byte* pCompBuff, int nCompBuffSize, byte* pDstImgBuff, int nFlags )
{
   if( !m_pMFCCodec )
      return AVCodecDecompressRet_DecoderIsNotOpend;

   if( m_pMFCCodec->isServiceMode() ) {
      m_pMFCCodec->AddInputBuffer( pCompBuff, nCompBuffSize );
   }

   if( m_pMFCCodec->isFirst() ) {
      m_pMFCCodec->RunMFCDecodeThread();
   } else {
      m_nCount_NeedMoreFrame++;
      if( m_nCount_NeedMoreFrame > 5 ) {
         m_nCount_NeedMoreFrame = 0;
         return AVCodecDecompressRet_Failed;
      } else {
         return AVCodecDecompressRet_NeedMoreFrame;
      }
   }

   int nWidth  = m_pMFCCodec->GetWidth();
   int nHeight = m_pMFCCodec->GetHeight();

   if( m_nWidth != nWidth || m_nHeight != nHeight ) {
      //logi( "CAVCoDecoder_MFC::Decompress Size [Set %d x %d] [MFC %d x %d]\n", m_nWidth, m_nHeight, nWidth, nHeight );

      m_ImageSizeOfEncodedData.Width  = nWidth;
      m_ImageSizeOfEncodedData.Height = nHeight;

      //return AVCodecDecompressRet_NeedMoreFrame;
      return AVCodecDecompressRet_ImageSizeIsChanged;
   }

   Buffer DstBuffer1;
   Buffer DstBuffer2;

   //CCriticalSectionSP co(m_pMFCCodec->m_Mutex);
   m_pMFCCodec->GetDecodeData( DstBuffer1, DstBuffer2 );

   int nDstLen = DstBuffer1.Length + DstBuffer2.Length;
   if( nDstLen <= 0 ) {
      //logi("CAVCoDecoder_MFC::Decompress 1 [Len: %d]\n", nDstLen);

      return AVCodecDecompressRet_NeedMoreFrame;
   }

   ConvertPixelFormat( DstBuffer1.Address, DstBuffer2.Address, DstBuffer1.Length, DstBuffer2.Length, pDstImgBuff );

   //logi("CAVCoDecoder_MFC::Decompress 1 END [Len1: %d] [Len2: %d]\n", DstBuffer1.Length, DstBuffer2.Length);

   return AVCodecDecompressRet_Success;
}

int CAVCoDecoder_MFC::Decompress( byte* pCompBuff, int nCompBuffSize, BGRImage& d_image, int nFlags )
{
   return AVCodecDecompressRet_DecoderIsNotOpend;
}

BOOL CAVCoDecoder_MFC::ConvertPixelFormat( void* s_img_y, void* s_img_uv, int len_y, int len_uv, byte* d_img_buf )
{
   int nWidth  = m_pMFCCodec->GetWidth();
   int nHeight = m_pMFCCodec->GetHeight();
   if( 0 == nWidth || 0 == nHeight )
      return FALSE;
   if( m_nWidth != nWidth || m_nHeight != nHeight )
      return FALSE;

   BOOL bActive       = FALSE;
   float fDecodingFPS = m_fDecodingFPS;
   if( 0 == fDecodingFPS )
      bActive = TRUE;
   else if( m_nFirstDecoding < m_fFrameRate )
      bActive = TRUE;
   else {
      int nIndex = floor( m_fFrameRate / fDecodingFPS );
      if( 0 == ( m_nDecodingCount % nIndex ) )
         bActive = TRUE;
   }

   //logi("MFC Convert Pixel START Active[%d] Count[%d/%d] [%d * %d]\n", bActive, floor(m_fFrameRate / fDecodingFPS), m_nDecodingCount, m_nWidth, m_nHeight);

   if( bActive ) {
      m_nActiveDecodingCount++;
      byte* s_img_y_t  = (byte*)s_img_y;
      byte* s_img_uv_t = (byte*)s_img_uv;
      switch( m_nPixelFormat ) {
      case VPIX_FMT_YUY2:
         ConvertNV12MToYUY2( s_img_y_t, s_img_uv_t, nWidth, nHeight, d_img_buf );
         break;
      case VPIX_FMT_GRAY: {
#ifdef __SUPPORT_GRAY_IMAGE_USE
         ConvertNV12MToGray( s_img_y_t, nWidth, nHeight, d_img_buf );
#else
         uint nIndexD = 0;
         for( int y = 0; y < nHeight; y++ ) {
            for( int x = 0; x < nWidth; x += 4 ) {
               uint nIndexS = y * nWidth + x;
               byte y1      = s_img_y_t[nIndexS++];
               byte y2      = s_img_y_t[nIndexS++];
               byte y3      = s_img_y_t[nIndexS++];
               byte y4      = s_img_y_t[nIndexS];

               d_img_buf[nIndexD] = y1;
               nIndexD += 2;
               d_img_buf[nIndexD] = y2;
               nIndexD += 2;
               d_img_buf[nIndexD] = y3;
               nIndexD += 2;
               d_img_buf[nIndexD] = y4;
               nIndexD += 2;
            }
         }
#endif
      } break;
      default:
         logi( "MFC Active Decoding Error!!!  PixelFormat[%d]\n", m_nPixelFormat );
         return FALSE;
      }
   }

   m_nDecodingCount++;
   if( m_nFirstDecoding < m_fFrameRate )
      m_nFirstDecoding++;
   if( m_nDecodingCount >= m_fFrameRate ) {
      //logi("MFC Active Decoding Count[%d  %d]\n", m_nDecodingCount, m_nActiveDecodingCount);
      m_nDecodingCount       = 0;
      m_nActiveDecodingCount = 0;
   }

   return TRUE;
}

int CAVCoDecoder_MFC::DecompressKeyFrame( byte* pCompBuff, int nCompBuffSize, byte* pDstImgBuff, int nFlags )
{
   return Decompress( pCompBuff, nCompBuffSize, pDstImgBuff, nFlags );
}
#endif
///////////////////////////////////////////////////////////////////////////////
//
// Class: CAVCodec
//
///////////////////////////////////////////////////////////////////////////////

CAVCodec::CAVCodec()
{
   m_pAVCoDecoder = NULL;
   m_bOpenCodec   = FALSE;
}

CAVCodec::~CAVCodec()
{
   DestroyAVCoDecoder();
}

void CAVCodec::CreateAVCoDecoder( int nCoDecoderLibType )
{
   switch( nCoDecoderLibType ) {
   case AVCoDecoderLibType_FFMPEG:
      m_pAVCoDecoder = new CAVCoDecoder_FFMPEG;
      break;
#ifdef __SUPPORT_MFC_CODEC
   case AVCoDecoderLibType_MFC:
      m_pAVCoDecoder = new CAVCoDecoder_MFC;
      break;
#endif
   default:
      if( g_bDebugAVCodec ) logd( "ERROR: No codec found.\n" );
      break;
   }
}

void CAVCodec::CreateAVCoDecoder( uint nCodecID, int nCoDecoderLibType )
{
   switch( nCodecID ) {
   case VCODEC_ID_H264:
   case VCODEC_ID_H265:
      CreateAVCoDecoder( nCoDecoderLibType );
      break;
   case VCODEC_ID_MPEG4:
   case VCODEC_ID_MJPEG:
   case VCODEC_ID_XVID:
   case VCODEC_ID_H263:
   case VCODEC_ID_MPEG1VIDEO:
   case VCODEC_ID_MPEG2VIDEO:
      m_pAVCoDecoder = new CAVCoDecoder_FFMPEG;
      break;
   default:
      if( g_bDebugAVCodec ) logd( "ERROR: No codec found.\n" );
      break;
   }
}

void CAVCodec::DestroyAVCoDecoder()
{
   if( m_pAVCoDecoder ) {
      g_csAVCodec.Lock();
      delete m_pAVCoDecoder;
      m_pAVCoDecoder = NULL;
      g_csAVCodec.Unlock();
   }
}

void CAVCodec::Close()
{
   InitAVCodecParam();
   if( m_pAVCoDecoder ) {
      g_csAVCodec.Lock();
      m_pAVCoDecoder->Close();
      g_csAVCodec.Unlock();
      m_bOpenCodec = FALSE;
   }
#ifdef __SUPPORT_IMAGE_ENHANCEMENT
   CImageEnhancement::Close();
#endif
}

void CAVCodec::CloseImageEnhancement()
{
#ifdef __SUPPORT_IMAGE_ENHANCEMENT
   CImageEnhancement::Close();
#endif
}

#ifdef __SUPPORT_MFC_CODEC
int g_nDefaultDecoderLib = AVCoDecoderLibType_MFC;
#else
int g_nDefaultDecoderLib = AVCoDecoderLibType_FFMPEG;
#endif
int g_nDefaultEncoderLib  = AVCoDecoderLibType_FFMPEG;
BOOL g_bUseHWVideoEncoder = FALSE; // 전역 인코더 사용여부
BOOL g_bUseHWVideoDecoder = FALSE; // 전역 디코더 사용여부

BOOL CAVCodec::IsOpen()
{
   return ( m_bOpenCodec );
}

int CAVCodec::OpenEncoder( const CAVCodecParam& avOpen )
{
   int nRet = DONE;
   CCriticalSectionSP co( g_csAVCodec );
   if( !m_pAVCoDecoder || !IsEqual( avOpen ) ) {
      int nCodecLibType = g_nDefaultEncoderLib;

      int nCodecOpenTryingNum = 1;

      int i;
      for( i = 0; i < nCodecOpenTryingNum; i++ ) {
         if( 1 == i )
            nCodecLibType = g_nDefaultEncoderLib; // H.264 하드웨어 가속일때 실패하면 기본 코덱 라이브러리로 열어본다.

         // 해당 코덱 라이브러리로 생성한다.
         DestroyAVCoDecoder();
         CreateAVCoDecoder( avOpen.m_nCodecID, nCodecLibType );
         if( m_pAVCoDecoder ) {
            Set( avOpen );
            CAVCodecParam& avCodecParam = GetAVCodecParam();
            nRet                        = m_pAVCoDecoder->OpenEncoder( avCodecParam );
            if( DONE == nRet ) {
               m_bOpenCodec = TRUE;
               break;
            } else {
               Close();
            }
         }
      }
   }

   return ( nRet );
}

int CAVCodec::OpenDecoder( const CAVCodecParam& avOpen )
{
   int nRet = DONE;
   CCriticalSectionSP co( g_csAVCodec );

   // IsEqual 에 영향을 받을 필요없는 변수라서 위에서 값을 세팅  JJongO
   if( m_fDecodingFPS != avOpen.m_fDecodingFPS ) {
      logd( "CAVCodec  OpenDecode  Change DecodingFPS [%f -> %f]\n", m_fDecodingFPS, avOpen.m_fDecodingFPS );
      m_fDecodingFPS = avOpen.m_fDecodingFPS;
   }

   if( !m_pAVCoDecoder || !IsEqual( avOpen ) ) {
      int nCodecLibType = g_nDefaultDecoderLib;
      logd( "CAVCodec  OpenDecode  Codec[%d %d] FPS[%f %f] [%d x %d]\n",
            avOpen.m_nCodecID, nCodecLibType, avOpen.m_fFrameRate, avOpen.m_fDecodingFPS, avOpen.m_nWidth, avOpen.m_nHeight );

      // 해당 코덱 라이브러리로 생성한다.
      DestroyAVCoDecoder();
      CreateAVCoDecoder( avOpen.m_nCodecID, nCodecLibType );
      if( m_pAVCoDecoder ) {
         Set( avOpen );
         CAVCodecParam& avCodecParam = GetAVCodecParam();
         nRet                        = m_pAVCoDecoder->OpenDecoder( avCodecParam );
         if( DONE != nRet ) {
            LOGE << "Failed to Open Decode. [" << avOpen.m_nWidth << "x" << avOpen.m_nHeight << "]";
            Close();
         }
#ifdef __SUPPORT_IMAGE_ENHANCEMENT
         else
            CImageEnhancement::Initialize( nWidth, nHeight, 30.0f, TRUE );
#endif
      }

      // H.264 하드웨어 가속일때 실패하면 기본 코덱 라이브러리로 열어본다.
      if( ( DONE != nRet ) && ( avOpen.m_bUseHWAcceleration && g_bUseHWVideoDecoder ) && ( avOpen.m_nCodecID == VCODEC_ID_H264 ) ) {
         if( g_bDebugAVCodec ) loge( "ERROR: Can not open H.264 HW Decoder, try Default Decoder!!\n" );
         nCodecLibType = g_nDefaultDecoderLib;
         DestroyAVCoDecoder();
         CreateAVCoDecoder( avOpen.m_nCodecID, nCodecLibType );
         if( m_pAVCoDecoder ) {
            Set( avOpen );
            CAVCodecParam& avCodecParam = GetAVCodecParam();
            nRet                        = m_pAVCoDecoder->OpenDecoder( avCodecParam );
            if( DONE != nRet ) Close();
#ifdef __SUPPORT_IMAGE_ENHANCEMENT
            else
               CImageEnhancement::Initialize( nWidth, nHeight, 30.0f, TRUE );
#endif
         }
      }
   }

   return ( nRet );
}

int CAVCodec::Resize( GImage& s_image, GImage& d_image )
{
   if( !m_pAVCoDecoder ) {
      m_pAVCoDecoder = new CAVCoDecoder_FFMPEG;
   }

   return m_pAVCoDecoder->Resize( s_image, d_image );
}

int CAVCodec::ConvertYUY2ToGray( byte* s_buffer, int nWidth, int nHeight, GImage& d_image )
{
   if( !m_pAVCoDecoder ) {
      m_pAVCoDecoder = new CAVCoDecoder_FFMPEG;
   }

   return m_pAVCoDecoder->ConvertYUY2ToGray( s_buffer, nWidth, nHeight, d_image );
}

int CAVCodec::Compress( byte* pSrcImgBuff, byte** ppCompBuff, int& nCompBuffSize, int& nFlags )
{
   if( m_pAVCoDecoder )
      return ( m_pAVCoDecoder->Compress( pSrcImgBuff, ppCompBuff, nCompBuffSize, nFlags ) );
   else
      return ( AVCodecDecompressRet_Failed );
}

int CAVCodec::Compress( BGRImage& s_image, byte** ppCompBuff, int& nCompBuffSize, int& nFlags )
{
   if( m_pAVCoDecoder )
      return ( m_pAVCoDecoder->Compress( s_image, ppCompBuff, nCompBuffSize, nFlags ) );
   else
      return ( AVCodecDecompressRet_Failed );
}

int CAVCodec::Decompress( byte* pCompBuff, int nCompBuffSize, byte* pDstImgBuff, int nFlags )
{
   if( nFlags & VFRM_NULLFRAME ) return ( AVCodecDecompressRet_Success );

#ifdef __SUPPORT_IMAGE_ENHANCEMENT
   CImageEnhancement::m_pDecompressTgtBuff = pDstImgBuff;
   if( IS_SUPPORT( __SUPPORT_DEFOG ) || IS_SUPPORT( __SUPPORT_VIDEO_STABILIZATION ) ) {
      CImageEnhancement::ImageEnhancementInitProc( pDstImgBuff, pDstImgBuff );
   }
#endif
   if( m_pAVCoDecoder ) {
      int nDecompressRet = m_pAVCoDecoder->Decompress( pCompBuff, nCompBuffSize, pDstImgBuff, nFlags );
      if( DONE == nDecompressRet ) {
#ifdef __SUPPORT_IMAGE_ENHANCEMENT
         if( IS_SUPPORT( __SUPPORT_DEFOG ) || IS_SUPPORT( __SUPPORT_VIDEO_STABILIZATION ) ) {
            CImageEnhancement::ImageEnhancementMainProc();
         }
#endif
         return ( DONE );
      }
      return nDecompressRet;
   }
   return ( AVCodecDecompressRet_Failed );
}

int CAVCodec::Decompress( byte* pCompBuff, int nCompBuffSize, BGRImage& d_image, int nFlags )
{
   if( nFlags & VFRM_NULLFRAME ) return ( AVCodecDecompressRet_Success );
   if( m_pAVCoDecoder )
      return ( m_pAVCoDecoder->Decompress( pCompBuff, nCompBuffSize, d_image, nFlags ) );
   else
      return ( AVCodecDecompressRet_Failed );
}

void CAVCodec::Lock()
{
   m_csCodec.Lock();
}

void CAVCodec::Unlock()
{
   m_csCodec.Unlock();
}

int CAVCodec::DecompressKeyFrame( byte* pCompBuff, int nCompBuffSize, byte* pDstImgBuff, int nFlags )
{
   if( m_pAVCoDecoder ) return m_pAVCoDecoder->DecompressKeyFrame( pCompBuff, nCompBuffSize, pDstImgBuff, nFlags );
   return 0;
}

int CAVCodec::UpdateCurrShowFrame( byte* pDstImgBuff )
{
   if( m_pAVCoDecoder ) return m_pAVCoDecoder->UpdateCurrShowFrame( pDstImgBuff );
   return 0;
}

ISize2D CAVCodec::GetImageSizeOfEncodedData()
{
   if( m_pAVCoDecoder ) return m_pAVCoDecoder->m_ImageSizeOfEncodedData;
   return ISize2D( 0, 0 );
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

enum AVCodecID GetFFMPEGCodecID( enum VideoCodecID codec_id )
{
   enum AVCodecID ffmpeg_codec_id = AV_CODEC_ID_NONE;
   switch( codec_id ) {
   case VCODEC_ID_MPEG4:
      ffmpeg_codec_id = AV_CODEC_ID_MPEG4;
      break;
   case VCODEC_ID_MJPEG:
      ffmpeg_codec_id = AV_CODEC_ID_MJPEG;
      break;
   case VCODEC_ID_H264:
      ffmpeg_codec_id = AV_CODEC_ID_H264;
      break;
   case VCODEC_ID_H265:
      ffmpeg_codec_id = AV_CODEC_ID_H265;
      break;
   case VCODEC_ID_H263:
      ffmpeg_codec_id = AV_CODEC_ID_H263;
      break;
   case VCODEC_ID_XVID:
      ffmpeg_codec_id = AV_CODEC_ID_MPEG4;
      break;
   case VCODEC_ID_VC1:
      ffmpeg_codec_id = AV_CODEC_ID_VC1;
      break;
   default:
      break;
   }
   return ( ffmpeg_codec_id );
}

int GetHikvisionCodecID( enum VideoCodecID codec_id )
{
   int nHikvisionCodecID = 0;
#if 0
   switch (codec_id) {
   case VCODEC_ID_HIK_H264:
      nHikvisionCodecID = VIDEO_H264;
      break;
   case VCODEC_ID_HIK_MPEG4:
      nHikvisionCodecID = VIDEO_MPEG4;
      break;
   case VCODEC_ID_HIK_MJPEG:
      nHikvisionCodecID = VIDEO_MJPEG;
      break;
   case VCODEC_ID_HIK_AVC264:
      nHikvisionCodecID = VIDEO_AVC264;
      break;
   default:
      break;
   }
#endif
   return ( nHikvisionCodecID );
}

enum VideoCodecID GetVideoCodecID( enum AVCodecID codec_id )
{
   enum VideoCodecID video_codec_id = VCODEC_ID_NONE;
   switch( codec_id ) {
   case AV_CODEC_ID_MPEG4:
      video_codec_id = VCODEC_ID_MPEG4;
      break;
   case AV_CODEC_ID_MJPEG:
      video_codec_id = VCODEC_ID_MJPEG;
      break;
   case AV_CODEC_ID_H264:
      video_codec_id = VCODEC_ID_H264;
      break;
   case AV_CODEC_ID_H265:
      video_codec_id = VCODEC_ID_H265;
      break;
   case AV_CODEC_ID_H263:
   case AV_CODEC_ID_H263P:
      video_codec_id = VCODEC_ID_H263;
      break;
   case AV_CODEC_ID_MPEG1VIDEO:
      video_codec_id = VCODEC_ID_MPEG1VIDEO;
      break;
   case AV_CODEC_ID_MPEG2VIDEO:
      video_codec_id = VCODEC_ID_MPEG2VIDEO;
      break;
   case AV_CODEC_ID_VC1:
      video_codec_id = VCODEC_ID_VC1;
      break;
   default:
      break;
   }
   return ( video_codec_id );
}

enum AVPixelFormat GetRealPixelFormat_FFMPEG( enum VideoPixelFormat pixel_format )
{
   enum AVPixelFormat real_pixel_format = AV_PIX_FMT_NONE;
   switch( pixel_format ) {
   case VPIX_FMT_YV12:
      real_pixel_format = AV_PIX_FMT_YUV420P;
      break;
   case VPIX_FMT_GRAY:
      real_pixel_format = AV_PIX_FMT_GRAY8;
      break;
   case VPIX_FMT_YUY2:
      real_pixel_format = AV_PIX_FMT_YUYV422;
      break;
   case VPIX_FMT_BGR24:
      real_pixel_format = AV_PIX_FMT_BGR24;
      break;

   case VPIX_FMT_DETECTING:
      real_pixel_format = AV_PIX_FMT_YUV420P;
      break;
   case VPIX_FMT_YUVJ444P:
      real_pixel_format = AV_PIX_FMT_YUVJ444P;
      break;
   case VPIX_FMT_YUVJ422P:
      real_pixel_format = AV_PIX_FMT_YUVJ422P;
      break;
   case VPIX_FMT_YUVJ411P:
      real_pixel_format = AV_PIX_FMT_YUVJ411P;
      break;
   case VPIX_FMT_YUVJ420P:
      real_pixel_format = AV_PIX_FMT_YUVJ420P;
      break;

   default:
      break;
   }
   return ( real_pixel_format );
}

VideoPixelFormat FFmpegPixFmtToVideoPixFmt( AVPixelFormat PixFmt )
{
   VideoPixelFormat pix_fmt = VideoPixelFormat::VPIX_FMT_NONE;
   switch( PixFmt ) {
   case AV_PIX_FMT_YUYV422:
      pix_fmt = VPIX_FMT_YUY2;
      break;

   case AV_PIX_FMT_YUV420P:
      pix_fmt = VPIX_FMT_YV12;
      break;

   case AV_PIX_FMT_BGR24:
      pix_fmt = VPIX_FMT_BGR24;
      break;

   case AV_PIX_FMT_YUVJ444P:
      pix_fmt = VPIX_FMT_YUVJ444P;
      break;

   case AV_PIX_FMT_YUVJ422P:
      pix_fmt = VPIX_FMT_YUVJ422P;
      break;

   case AV_PIX_FMT_YUVJ411P:
      pix_fmt = VPIX_FMT_YUVJ411P;
      break;

   case AV_PIX_FMT_YUVJ420P:
      pix_fmt = VPIX_FMT_YUVJ420P;
      break;
   default:
      break;
   }

   return pix_fmt;
}

uint GetVFWFourCC( enum VideoCodecID codec_id )
{
   switch( codec_id ) {
   case VCODEC_ID_MPEG4:
      return mmioFOURCC( 'D', 'I', 'V', 'X' );
      break;
   case VCODEC_ID_MJPEG:
      return mmioFOURCC( 'M', 'J', 'P', 'G' );
      break;
   case VCODEC_ID_H264:
      return mmioFOURCC( 'H', '2', '6', '4' );
      break;
   case VCODEC_ID_XVID:
      return mmioFOURCC( 'X', 'V', 'I', 'D' );
      break;
   case VCODEC_ID_H263:
      return mmioFOURCC( 'H', '2', '6', '3' );
      break;
   case VCODEC_ID_H265:
      return mmioFOURCC( 'H', 'E', 'V', 'C' );
      break;
   default:
      break;
   }
   return ( VCODEC_ID_NONE );
}

BOOL IsHikVisionCodecID( enum VideoCodecID codec_id )
{
   if( VCODEC_ID_HIK_H264 == codec_id ) return TRUE;
   if( VCODEC_ID_HIK_MPEG4 == codec_id ) return TRUE;
   if( VCODEC_ID_HIK_MJPEG == codec_id ) return TRUE;
   if( VCODEC_ID_HIK_AVC264 == codec_id ) return TRUE;
   return FALSE;
}

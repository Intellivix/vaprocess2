﻿#include "InnerThread.h"

#include "bccl_plog.h"

void InnerThread::setThreadName( const std::string& name )
{
#ifdef SUPPORT_PTHREAD_NAME
   pthread_setname_np( pthread_self(), name.c_str() );
#endif // SUPPORT_PTHREAD_NAME
}

InnerThread::~InnerThread()
{
   StopThread();
}

bool InnerThread::StartThread( void* pInstance )
{
   LOGV << "Thread[" << m_thread_name << "] starting.";

   if( m_thread != nullptr ) {
      LOGE << "failed to start thread, already started.";
      return false;
   }

   m_thread = new std::thread( [pInstance]() {
      static_cast<InnerThread*>( pInstance )->ThreadApplyName();
      static_cast<InnerThread*>( pInstance )->ThreadLoop();
   } );

   if( m_thread->joinable() ) {
      LOGV << "Thread[" << m_thread_name << "] started.";
   }

   return true;
}

void InnerThread::StopThread()
{
   if( m_thread == nullptr ) {
      LOGW << "no thread to stop.";
      return;
   }

   if( m_thread->joinable() ) {
      m_thread->join();
      delete m_thread;
      m_thread = nullptr;
   }
}

void InnerThread::SeveThreadName( const std::string& thread_name )
{
   m_thread_name = thread_name;
}

void InnerThread::ThreadApplyName()
{
   if( m_thread_name.empty() == false ) {
      InnerThread::setThreadName( m_thread_name );
   }
}

void InnerThread::Sleep_ms( unsigned nMillisecs )
{
   std::this_thread::sleep_for( std::chrono::milliseconds( nMillisecs ) );
}

TARGET = Common
TEMPLATE = lib
include( ../env.pri )


INCLUDEPATH += ../BCCL
INCLUDEPATH += ../VACL

DEPENDPATH += ../BCCL
DEPENDPATH += ../VACL

PRE_TARGETDEPS += $$DESTDIR/libBCCL.a
PRE_TARGETDEPS += $$DESTDIR/libVACL.a

HEADERS = \
   AreaDisplayWnd.h \
   AreaSpecWnd.h \
   AreaSpecWnd_Head.h \
   ARFeature.h \
   AVCodec.h \
   BitrateChecker.h \
   ColorDef.h \
   Common.h \
   CommonUtil.h \
   Cryptor.h \
   DataStruct.h \
   EventEx.h \
   FisheyeDewarping.h \
   FrameRateChecker.h \
   GraphicObject.h \
   HeatMap.h \
   ImageEnhancement.h \
   ImgDisplayWnd.h \
   IniParse.h \
   MMTimer.h \
   OnscreenDisplayEx.h \
   SolarCalculator.h \
   StdAfx.h \
   SupportFunction.h \
   TestObject.h \
   VACEx.h \
   version.h \
   WaitForMultipleObjects.h \
   xSkinButton.h \
   FileMonitoring.h \
   InnerThread.h \
   TimerCheckerManager.h \
   BaseDirManager.h \
   CpuMonitoring.h \
   TimerEx.h \
    LGU/Logger.h

SOURCES = \
   AreaDisplayWnd.cpp \
   AreaSpecWnd.cpp \
   AreaSpecWnd_Head.cpp \
   ARFeature.cpp \
   AVCodec.cpp \
   BitrateChecker.cpp \
   CommonUtil.cpp \
   Cryptor.cpp \
   EventEx.cpp \
   FisheyeDewarping.cpp \
   FontBitmaps.cpp \
   FrameRateChecker.cpp \
   GraphicObject.cpp \
   HeatMap.cpp \
   ImageEnhancement.cpp \
   ImgDisplayWnd.cpp \
   IniParse.cpp \
   MMTimer.cpp \
   OnscreenDisplayEx.cpp \
   SolarCalculator.cpp \
   StdAfx.cpp \
   SupportFunction.cpp \
   TestObject.cpp \
   VACEx.cpp \
   WaitForMultipleObjects.cpp \
   xSkinButton.cpp \
   FileMonitoring.cpp \
   InnerThread.cpp \
   TimerCheckerManager.cpp \
   BaseDirManager.cpp \
   CpuMonitoring.cpp \
   TimerEx.cpp \
   LGU/Logger.cpp



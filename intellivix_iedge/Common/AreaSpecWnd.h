﻿#pragma once
#ifdef WIN32
#include "AreaDisplayWnd.h"

// Messages
#define MSG_LBUTTONDOWN                                  (WM_USER + 101)
#define MSG_RBUTTONDOWN                                  (WM_USER + 102)
#define MSG_MOUSEMOVE							               (WM_USER + 103)
#define MSG_LBUTTONUP	                                 (WM_USER + 104)
#define MSG_KEYDOWN								               (WM_USER + 105)
#define MSG_RENDER                                       (WM_USER + 106)
#define MSG_SETUP_WIZARD_CLOSED                          (WM_USER + 107)

enum ASW_MODE
{
   ASW_MODE_SELECT_EDIT			= 1,
   ASW_MODE_SPECIFY_POLYGON   = 2,
   ASW_MODE_SPECIFY_RECTANGLE	= 3
};

enum ASW_STATE
{
   ASW_STATE_DRAWING	= 0x00000001,
   ASW_STATE_SIZING  = 0x00000002,
   ASW_STATE_MOVING	= 0x00000004,
#ifdef __ENABLE_MOVING_POLYGON_VERTEX
   ASW_STATE_MOVING_POLYGON_VERTEX = 0x00000008,
#endif // __ENABLE_MOVING_POLYGON_VERTEX
};

//////////////////////////////////////////////////////////////////////////////
//
// Class : CSpecWndMessageHandler
//
//////////////////////////////////////////////////////////////////////////////

class CSpecWndMessageHandler
{
public:
   virtual void SW_OnLButtonDown  (UINT flag,CPoint point) {}
   virtual void SW_OnLButtonUp	 (UINT flag,CPoint point) {}
   virtual void SW_OnRButtonDown  (UINT flag,CPoint point) {}
   virtual void SW_OnMouseMove    (UINT flag,CPoint point) {}
   virtual void SW_OnLButtonDblClk(UINT flag,CPoint point) {}
   virtual void SW_PreRenderSub   (CDC *dc) {}
};


//////////////////////////////////////////////////////////////////////////////
//
// Class : CAreaSpecWnd
//
//////////////////////////////////////////////////////////////////////////////

class CAreaSpecWnd : public CAreaDisplayWnd 
{
public:
   static int Options;

#ifdef __ENABLE_COPY_N_PASTE_GRAPHIC_OBJECT
private:

   static const int AreaSpecWndPopupMenuCopyItemId = 10000;
   static const int AreaSpecWndPopupMenuPasteItemId = 10001;
   static const int AreaSpecWndPasteGrpObjOffset = 8;

private:

   bool              m_bCreatedPolygon;
   CGraphicObject*   m_pCopiedGrpObj;
   CPoint            m_ptLastPasted;
   CPoint            m_ptLastRBtnDown;
#endif // __ENABLE_COPY_N_PASTE_GRAPHIC_OBJECT

public:
   int Mode;
   int AreaType;
   CSpecWndMessageHandler* m_pMsgHandler;
   CCriticalSection m_csRender;

public:
   CAreaSpecWnd (   );
   virtual ~CAreaSpecWnd (   );

public:
   virtual void RenderSub (CDC *dc);

protected:
   afx_msg void OnLButtonDown  (UINT flag,CPoint point);
   afx_msg void OnLButtonUp	 (UINT flag,CPoint point);
   afx_msg void OnRButtonDown  (UINT flag,CPoint point);
   afx_msg void OnMouseMove    (UINT flag,CPoint point);
   afx_msg void OnLButtonDblClk(UINT flag,CPoint point);
#ifdef __ENABLE_COPY_N_PASTE_GRAPHIC_OBJECT
   afx_msg void OnContextMenu(CWnd* pWnd, CPoint pos);

   virtual BOOL PreTranslateMessage(MSG* pMsg);
   
   int add_grp_obj(CGraphicObject* pGrpObj);
   CGraphicObject* get_selected_grp_obj();
   CGraphicObject* get_grp_obj_by_point(CPoint* pPoint);
   bool can_copy_grp_obj(CPoint* pPoint = nullptr);
   bool can_paste_grp_obj();
   int copy_grp_obj();
   int paste_grp_obj(CPoint* pOffset = nullptr);
   int clone_grp_obj(CGraphicObject** ppDst, CGraphicObject* pSrc);
#endif


public:
   CPoint		m_StartPoint;
   int			m_nState;
   int			m_nResizingType;
   HCURSOR		m_hCursorSizing[ST_NUM];
   HCURSOR		m_hCursorMoving;
#ifdef __ENABLE_MOVING_POLYGON_VERTEX
   int         m_nLastPolygonVertexIdx;
#endif // __ENABLE_MOVING_POLYGON_VERTEX

public:
   CPointArray				m_ConnectedLine;
   CGraphicObject*		m_pTempObject;
   CGraphicObject*		m_pOriginalObject;
   CGraphicObject*		m_pCompletedGObj;        // 설정이 완료된 CGraphicObject

   DECLARE_MESSAGE_MAP (   )
};

#endif
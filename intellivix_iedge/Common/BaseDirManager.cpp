﻿#include "BaseDirManager.h"

BaseDirManager& BaseDirManager::Get()
{
   static BaseDirManager m_instance;
   return m_instance;
}

BaseDirManager::BaseDirManager()
{
   m_multi_process_id = -1;
   m_base_dir         = get_excutable_dir();
}

std::string BaseDirManager::get_excutable_dir()
{
   std::array<char, 4096> pwd;
   getcwd( pwd.data(), pwd.size() );
   //LOGI << "homedir:" << pwd.data();
   return pwd.data();
}

bool BaseDirManager::set_multil_proces_id( const std::string& sub_dir )
{
   std::string excutable_dir = get_excutable_dir();

   if( is_existed_dir( sub_dir ) == false ) {
      LOGE << "child_dir[" << sub_dir << "] has not existed.";
      return false;
   }

   m_multi_process_id = sub_dir;
   m_base_dir         = excutable_dir + "/" + sub_dir;
   LOGI << "New base dir:" << m_base_dir;
   
   return true;
}

const std::string& BaseDirManager::get_multil_proces_id()
{
   return m_multi_process_id;
}

const std::string& BaseDirManager::get_base_dir()
{
   return m_base_dir;
}

bool BaseDirManager::is_existed_dir( const std::string& dir )
{
   struct stat st;
   if( stat( dir.c_str(), &st ) != 0 ) {
      return false;
   }
   return true;
}

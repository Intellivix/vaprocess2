#include "StdAfx.h"

 CTestObject::CTestObject (   ) 

{
   m_nColor  = 0;
   m_nCount  = 0;
   m_bCreate = FALSE;
   m_nCount_ToBeCreate = 0;
}

 void CTestObject::Init (ISize2D &img_size)

{
   m_ImgSize = img_size;
}

 void CTestObject::Create (  )
 
{
   srand (GetTickCount (   ));
   int nMaxSize = 50;
   m_Size.Width  = int (float (rand ()) / RAND_MAX * nMaxSize + 8);
   m_Size.Height = int (float (rand ()) / RAND_MAX * nMaxSize + 8);
   m_LeftTop.X = 8 + (m_ImgSize.Width  - m_Size.Width  - 8) * float (rand ()) / RAND_MAX;
   m_LeftTop.Y = 8 + (m_ImgSize.Height - m_Size.Height - 8) * float (rand ()) / RAND_MAX;

   m_MovDir.X = (float (rand ()) / RAND_MAX - 0.5f) * 4.0f;
   m_MovDir.Y = (float (rand ()) / RAND_MAX - 0.5f) * 4.0f;

   m_bCreate = TRUE;
   m_nColor = rand () % 9;
}

 void CTestObject::Delete (   )
  
{
   m_bCreate = FALSE;
}

 void CTestObject::Move (   )
 
{
   m_LeftTop.X += m_MovDir.X;
   m_LeftTop.Y += m_MovDir.Y;

   if (m_LeftTop.X + m_Size.Width  - m_Size.Width  - 5 <= 0.0f) m_bCreate = FALSE;
   if (m_LeftTop.Y + m_Size.Height - m_Size.Height - 5 <= 0.0f) m_bCreate = FALSE;
   if (m_LeftTop.X + m_Size.Width  + 5 >= m_ImgSize.Width )     m_bCreate = FALSE;
   if (m_LeftTop.Y + m_Size.Height + 5 >= m_ImgSize.Height)     m_bCreate = FALSE;
   if (!m_bCreate) m_nCount_ToBeCreate = 0;
}

 void CTestObject::MainProc (byte* pImage)

{
   if (!m_bCreate) {
      if (!m_nCount_ToBeCreate) {
         if (!m_bCreate) {
            m_nCount_ToBeCreate = m_nCount + int ((float (rand ()) / RAND_MAX) * 30 * 30 + 2);
         }
      }

      if (m_nCount_ToBeCreate) {
         if (m_nCount == m_nCount_ToBeCreate) {
            Create (   );
            m_nCount_Alive = m_nCount + int ((float (rand ()) / RAND_MAX) * 30 * 30 + 60);
         }
      }
   }
   else {
      Move ();
      Draw (pImage);
   }

   m_nCount++;
}

 void CTestObject::Draw (byte* pImage)

{
   int i,j;
   
   ushort nColor = 0x8080;
   switch (m_nColor) 
   {
      case 0:
         nColor = 0x80A0;
         break;
      case 1:
         nColor = 0x2080;
         break;
      case 2:
         nColor = 0xF080;
         break;
      case 3:
         nColor = 0xF080;
         break;
      case 4:
         nColor = 0xF080;
         break;
      case 5:
         nColor = 0x1080;
         break;
      case 6:
         nColor = 0x1010;
         break;
      case 7:
         nColor = 0x601F;
         break;
      case 8:
         nColor = 0xA01F;
         break;
   }

   int sx = int (m_LeftTop.X);
   int ex = int (m_LeftTop.X + m_Size.Width);
   int sy = int (m_LeftTop.Y);
   int ey = int (m_LeftTop.Y + m_Size.Height);

   if (sx < 0) sx = 0;
   if (sy < 0) sy = 0;
   if (ex > m_ImgSize.Width)  ex = m_ImgSize.Width;
   if (ey > m_ImgSize.Height) ey = m_ImgSize.Height;

   for (i = sy; i < ey; i++) {
      ushort* p_line = (ushort*) (pImage + i * m_ImgSize.Width * 2);
      for (j = sx; j < ex; j++) {
         p_line[j] = nColor;
      }
   }
   //Fill (nColor, FPoint2D (1.0f, 1.0f), pImage, m_ImgSize.Width, m_ImgSize.Height);
}

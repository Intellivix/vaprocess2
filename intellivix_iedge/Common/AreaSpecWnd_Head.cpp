﻿#include "StdAfx.h"
#ifdef WIN32
#include "AreaSpecWnd_Head.h"
#include "vacl_event.h"
using namespace VACL;

BEGIN_MESSAGE_MAP( CAreaSpecWnd_Head, CAreaDisplayWnd )
ON_WM_LBUTTONDOWN()
ON_WM_RBUTTONDOWN()
ON_WM_MOUSEMOVE()
ON_WM_LBUTTONUP()
ON_WM_LBUTTONDBLCLK()
END_MESSAGE_MAP()

CAreaSpecWnd_Head::CAreaSpecWnd_Head()

{
   Mode                           = ASW_MODE_SELECT_EDIT;
   m_nState                       = 0;
   m_nResizingType                = -1;
   m_pTempObject                  = NULL;
   m_pOriginalObject              = NULL;
   m_pCompletedGObj               = NULL;
   m_hCursorSizing[ST_NORTH]      = AfxGetApp()->LoadStandardCursor( IDC_SIZENS );
   m_hCursorSizing[ST_SOUTH]      = AfxGetApp()->LoadStandardCursor( IDC_SIZENS );
   m_hCursorSizing[ST_WEST]       = AfxGetApp()->LoadStandardCursor( IDC_SIZEWE );
   m_hCursorSizing[ST_EAST]       = AfxGetApp()->LoadStandardCursor( IDC_SIZEWE );
   m_hCursorSizing[ST_NORTH_WEST] = AfxGetApp()->LoadStandardCursor( IDC_SIZENWSE );
   m_hCursorSizing[ST_SOUTH_EAST] = AfxGetApp()->LoadStandardCursor( IDC_SIZENWSE );
   m_hCursorSizing[ST_NORTH_EAST] = AfxGetApp()->LoadStandardCursor( IDC_SIZENESW );
   m_hCursorSizing[ST_SOUTH_WEST] = AfxGetApp()->LoadStandardCursor( IDC_SIZENESW );
   m_hCursorMoving                = AfxGetApp()->LoadStandardCursor( IDC_SIZEALL );
   m_StartPoint.SetPoint( -1, -1 );
   m_pMsgHandler = NULL;
}

CAreaSpecWnd_Head::~CAreaSpecWnd_Head()
{
   m_ConnectedLine.RemoveAll();
}

void CAreaSpecWnd_Head::OnLButtonDown( UINT flag, CPoint point )
{
   if( !m_bDisplayFlag )
      return;

   m_csRender.Lock();
   BOOL bSpecifiedObjectToBeNull = FALSE;
   if( Mode == ASW_MODE_SELECT_EDIT ) {
      //리사이징 시작
      CSizingEdge* s_edge;
      CGraphicObject* g_object;
      g_object = m_GraphicHeadList.GetSelectedObject_Head();
      if( g_object ) {
         if( s_edge = g_object->m_OutlineRect.FindEdge( point ) ) {
            switch( g_object->m_nShape ) {
            case SHAPE_ELLIPSE:
               m_pTempObject = new CCircle;
               ( (CCircle*)m_pTempObject )->SetPoints( ( (CCircle*)g_object )->m_Points );
               ( (CCircle*)m_pTempObject )->PrepareSizing( s_edge->m_nType );
               break;
            }
            m_pTempObject->SetColor( g_object->m_clrColor );
            m_pTempObject->SetBrushStyle( g_object->m_nHatchBrushStyle );
            m_nResizingType   = s_edge->m_nType;
            m_pOriginalObject = g_object;
            SetCapture();
            m_nState |= ASW_STATE_SIZING;
            Invalidate();
         }
      }
      if( !( m_nState & ASW_STATE_SIZING ) ) {
         //이동 시작
         g_object = m_GraphicHeadList.GetPointedObject_Head( point );
         if( g_object ) {
            switch( g_object->m_nShape ) {
            case SHAPE_ELLIPSE:
               m_pTempObject = new CCircle;
               ( (CCircle*)m_pTempObject )->SetPoints( ( (CCircle*)g_object )->m_Points );
               break;
            }
            m_pTempObject->SetColor( g_object->m_clrColor );
            m_pTempObject->SetBrushStyle( g_object->m_nHatchBrushStyle );
            m_StartPoint      = point;
            m_pOriginalObject = g_object;
            SetCapture();
            m_nState |= ASW_STATE_MOVING;
         }
         //선택 상태 표시
         m_GraphicHeadList.SelectPointedObject_Head( point );
         ParentWnd->SendMessage( MSG_LBUTTONDOWN );
         Invalidate();
      }
   } else if( Mode == ASW_MODE_SPECIFY_CIRCLE ) {
      if( m_GraphicHeadList.GetCount() < 1 ) {
         if( m_ConnectedLine.GetCount() == 0 )
            m_GraphicHeadList.UnSelectAllObjects();

         m_ConnectedLine.Add( point );
         // 그리기가 완료되었다.
         if( m_ConnectedLine.GetSize() == 2 ) {
            m_pCompletedGObj = new CCircle;
            ( (CCircle*)m_pCompletedGObj )->SetPoints( m_ConnectedLine );
            m_GraphicHeadList.AddTail( m_pCompletedGObj );
            m_ConnectedLine.RemoveAll();
            ParentWnd->SendMessage( MSG_LBUTTONDOWN );
            ParentWnd->SendMessage( MSG_UPDATE_PARAM_HEAD );
            bSpecifiedObjectToBeNull = TRUE;
         }
      }

      Invalidate();
   }

   if( m_pMsgHandler )
      m_pMsgHandler->SW_OnLButtonDown( flag, point );
   SetFocus();

   if( bSpecifiedObjectToBeNull ) {
      m_pCompletedGObj = NULL;
   }

   m_csRender.Unlock();
}

void CAreaSpecWnd_Head::OnLButtonUp( UINT flag, CPoint point )
{
   if( !m_bDisplayFlag )
      return;

   m_csRender.Lock();
   BOOL bSpecifiedObjectToBeNull = FALSE;
   if( Mode == ASW_MODE_SELECT_EDIT ) {
      if( m_nState & ASW_STATE_SIZING || m_nState & ASW_STATE_MOVING ) {
         CRect rect1 = m_pOriginalObject->m_OutlineRect.m_rect;
         CRect rect2 = m_pTempObject->m_OutlineRect.m_rect;
         switch( m_pTempObject->m_nShape ) {
         case SHAPE_ELLIPSE:
            ( (CCircle*)m_pOriginalObject )->SetPoints( ( (CCircle*)m_pTempObject )->m_Points );
            break;
         }
         delete m_pTempObject;
         m_pTempObject = NULL;
         if( m_pOriginalObject->m_nAreaType == AREA_TYPE_HEAD_ZONE ) {
            EventZone* e_zone = (EventZone*)m_pOriginalObject->m_pVACPolygon;
            if( m_nState & ASW_STATE_MOVING ) {
               rect1.NormalizeRect();
               rect2.NormalizeRect();

               FPoint2D scale;
               scale.X = REF_IMAGE_WIDTH / (float)Width;
               scale.Y = REF_IMAGE_HEIGHT / (float)Height;
               int dx  = (int)( ( rect2.left - rect1.left ) * scale.X );
               int dy  = (int)( ( rect2.top - rect1.top ) * scale.Y );
               e_zone->Offset( dx, dy );
               e_zone->EvtRule->ERC_PathPassing.ZoneTree.OffsetZones( dx, dy );
               e_zone->EvtRule->ERC_PathPassing.Arrows.OffsetArrows( dx, dy );
            } else {
               GraphicObjectToIVSPolygon( m_pOriginalObject );
               if( m_nState & ASW_STATE_SIZING ) {
                  e_zone->EvtRule->ERC_PathPassing.Initialize( e_zone );
                  ParentWnd->PostMessage( MSG_UPDATE_PARAM_HEAD );
               }
            }
         } else
            GraphicObjectToIVSPolygon( m_pOriginalObject );

         if( m_nState & ASW_STATE_SIZING ) {
            m_nState &= ~ASW_STATE_SIZING;
            m_nResizingType = -1;
         } else if( m_nState & ASW_STATE_MOVING ) {
            m_nState &= ~ASW_STATE_MOVING;
            m_StartPoint.SetPoint( -1, -1 );
         }
         m_pOriginalObject = NULL;
         Invalidate();
         ReleaseCapture();
      }
   }

   if( m_pMsgHandler )
      m_pMsgHandler->SW_OnLButtonUp( flag, point );

   if( bSpecifiedObjectToBeNull )
      m_pCompletedGObj = NULL;

   m_csRender.Unlock();
}

void CAreaSpecWnd_Head::OnMouseMove( UINT flag, CPoint point )
{
   if( !m_bDisplayFlag )
      return;

   m_csRender.Lock();

   CRect rc;
   GetClientRect( rc );
   if( point.x > rc.right ) point.x  = rc.right;
   if( point.y > rc.bottom ) point.y = rc.bottom;
   if( point.x < rc.left ) point.x   = rc.left;
   if( point.y < rc.top ) point.y    = rc.top;

   if( m_pTempObject ) m_pTempObject->m_nState |= GS_TEMPORARY;
   switch( Mode ) {
   case ASW_MODE_SELECT_EDIT: {
      if( m_nState & ASW_STATE_SIZING ) {
         m_pTempObject->Sizing( point, m_nResizingType );
         ::SetCursor( AfxGetApp()->LoadStandardCursor( IDC_CROSS ) );
         Invalidate();
      } else if( m_nState & ASW_STATE_MOVING ) {
         int dx = point.x - m_StartPoint.x;
         int dy = point.y - m_StartPoint.y;
         //영역내에서만 움직이게
         CRect t_rect = m_pOriginalObject->m_OutlineRect.m_rect;
         int over_x, over_y;
         over_x = ( t_rect.right + dx ) - rc.right;
         if( over_x > 0 ) dx -= over_x;
         over_x = ( t_rect.left + dx ) - rc.left;
         if( over_x < 0 ) dx -= over_x;
         over_y = ( t_rect.bottom + dy ) - rc.bottom;
         if( over_y > 0 ) dy -= over_y;
         over_y = ( t_rect.top + dy ) - rc.top;
         if( over_y < 0 ) dy -= over_y;
         m_pTempObject->Moving( m_pOriginalObject, dx, dy );
         ::SetCursor( m_hCursorMoving );
         Invalidate();
      } else {
         BOOL bCursorOnEdges = FALSE;
         CSizingEdge* s_edge;
         CGraphicObject* g_object = m_GraphicHeadList.GetSelectedObject_Head();
         if( g_object ) {
            if( s_edge = g_object->m_OutlineRect.FindEdge( point ) ) {
               ::SetCursor( m_hCursorSizing[s_edge->m_nType] );
               bCursorOnEdges = TRUE;
            }
         }
         if( !bCursorOnEdges ) {
            CGraphicObject* g_object = m_GraphicHeadList.GetPointedObject_Head( point );
            if( g_object )
               ::SetCursor( m_hCursorMoving );
            else
               ::SetCursor( AfxGetApp()->LoadStandardCursor( IDC_ARROW ) );
         }
      }
   } break;
   case ASW_MODE_SPECIFY_CIRCLE:
      break;
   }
   if( m_pMsgHandler )
      m_pMsgHandler->SW_OnMouseMove( flag, point );
   m_csRender.Unlock();
}

void CAreaSpecWnd_Head::RenderSub( CDC* dc )
{
   m_csRender.Lock();
   if( m_pMsgHandler )
      m_pMsgHandler->SW_PreRenderSub( dc );

   CAreaDisplayWnd::RenderSub( dc );

   m_ConnectedLine.DrawConnectedLine( dc );
   if( m_pTempObject )
      m_pTempObject->Draw( dc, m_GraphicHeadList.m_nDrawOptions );
   m_csRender.Unlock();
}

void CAreaSpecWnd_Head::SetCirclePoint( CPoint sp, CPoint ep )
{
   m_ConnectedLine.Add( sp );
   m_ConnectedLine.Add( ep );

   // 그리기가 완료되었다.
   if( m_ConnectedLine.GetSize() == 2 ) {
      m_pCompletedGObj = new CCircle;
      ( (CCircle*)m_pCompletedGObj )->SetPoints( m_ConnectedLine );
      m_GraphicHeadList.AddTail( m_pCompletedGObj );
      m_ConnectedLine.RemoveAll();
      ParentWnd->SendMessage( MSG_LBUTTONDOWN );
      ParentWnd->SendMessage( MSG_UPDATE_PARAM_HEAD );
   }

   Invalidate();

   SetFocus();
   m_pCompletedGObj = NULL;
}

void CAreaSpecWnd_Head::GetCirclePoint( CPoint& sp, CPoint& ep )
{
   if( m_GraphicHeadList.GetCount() == 1 ) {
      CGraphicObject* g_object = m_GraphicHeadList.GetObject();
      if( g_object ) {
         sp = ( (CCircle*)g_object )->m_Points[0];
         ep = ( (CCircle*)g_object )->m_Points[1];
      }
   }
}

int CAreaSpecWnd_Head::GetRadius()
{
   if( m_GraphicHeadList.GetCount() == 1 ) {
      CGraphicObject* g_object = m_GraphicHeadList.GetObject();
      if( g_object ) {
         return ( (CCircle*)g_object )->m_nHRRadius;
      }
   }

   return 0;
}
#endif

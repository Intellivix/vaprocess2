﻿#pragma once
#include <functional>

namespace timer_ex {

   typedef int TimerIdEx;

   int ActiveTimer();
   int CreateTimer(TimerIdEx& id, uint ms, std::function<void( void* ptr )> func, void* ptr);
   int DeleteTimer(const TimerIdEx& id);
};


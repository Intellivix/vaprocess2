#include "StdAfx.h"
#ifdef __WIN32
#include "graphicobject.h"

namespace SetupWizard {

///////////////////////////////////////////////////////////
//
// class CSizingEdge
//
///////////////////////////////////////////////////////////

#define EDGE_HSIZE 4

CSizingEdge::CSizingEdge( void )
{
   m_nType = 0;
}

CSizingEdge::~CSizingEdge( void )
{
}

void CSizingEdge::Draw( CDC* pDC )
{
   CPen pen( PS_SOLID | PS_COSMETIC, 1, RGB( 0, 0, 0 ) );
   CBrush brush( RGB( 255, 255, 255 ) );
   pDC->SelectObject( &pen );
   pDC->SelectObject( &brush );
   pDC->Rectangle( m_rect );
   pDC->SelectStockObject( NULL_PEN );
   pDC->SelectStockObject( NULL_BRUSH );
}

void CSizingEdge::SetRect( int nType, CRect& s_rect )
{
   CPoint c_pos = s_rect.CenterPoint();
   switch( nType ) {
   case ST_NORTH:
      m_rect.left   = c_pos.x - EDGE_HSIZE;
      m_rect.right  = c_pos.x + EDGE_HSIZE;
      m_rect.top    = s_rect.top - EDGE_HSIZE;
      m_rect.bottom = s_rect.top + EDGE_HSIZE;
      break;
   case ST_WEST:
      m_rect.left   = s_rect.left - EDGE_HSIZE;
      m_rect.right  = s_rect.left + EDGE_HSIZE;
      m_rect.top    = c_pos.y - EDGE_HSIZE;
      m_rect.bottom = c_pos.y + EDGE_HSIZE;
      break;
   case ST_SOUTH:
      m_rect.left   = c_pos.x - EDGE_HSIZE;
      m_rect.right  = c_pos.x + EDGE_HSIZE;
      m_rect.top    = s_rect.bottom - EDGE_HSIZE;
      m_rect.bottom = s_rect.bottom + EDGE_HSIZE;
      break;
   case ST_EAST:
      m_rect.left   = s_rect.right - EDGE_HSIZE;
      m_rect.right  = s_rect.right + EDGE_HSIZE;
      m_rect.top    = c_pos.y - EDGE_HSIZE;
      m_rect.bottom = c_pos.y + EDGE_HSIZE;
      break;
   case ST_NORTH_WEST:
      m_rect.left   = s_rect.left - EDGE_HSIZE;
      m_rect.right  = s_rect.left + EDGE_HSIZE;
      m_rect.top    = s_rect.top - EDGE_HSIZE;
      m_rect.bottom = s_rect.top + EDGE_HSIZE;
      break;
   case ST_NORTH_EAST:
      m_rect.left   = s_rect.right - EDGE_HSIZE;
      m_rect.right  = s_rect.right + EDGE_HSIZE;
      m_rect.top    = s_rect.top - EDGE_HSIZE;
      m_rect.bottom = s_rect.top + EDGE_HSIZE;
      break;
   case ST_SOUTH_WEST:
      m_rect.left   = s_rect.left - EDGE_HSIZE;
      m_rect.right  = s_rect.left + EDGE_HSIZE;
      m_rect.top    = s_rect.bottom - EDGE_HSIZE;
      m_rect.bottom = s_rect.bottom + EDGE_HSIZE;
      break;
   case ST_SOUTH_EAST:
      m_rect.left   = s_rect.right - EDGE_HSIZE;
      m_rect.right  = s_rect.right + EDGE_HSIZE;
      m_rect.top    = s_rect.bottom - EDGE_HSIZE;
      m_rect.bottom = s_rect.bottom + EDGE_HSIZE;
      break;
   }
   m_nType = nType;
}

BOOL CSizingEdge::CheckPointed( CPoint& point )
{
   return m_rect.PtInRect( point );
}

///////////////////////////////////////////////////////////
//
// class COutlineRect
//
///////////////////////////////////////////////////////////

COutlineRect::COutlineRect( void )
{
   m_rect = CRect( 0, 0, 0, 0 );
}

COutlineRect::~COutlineRect( void )
{
}

void COutlineRect::SetRect( CRect& rect )
{
   m_rect = rect;
   for( int i = 0; i < ST_NUM; i++ )
      m_Edges[i].SetRect( i, m_rect );
}

void COutlineRect::Draw( CDC* pDC )
{
   CPoint pt;
   for( int i = 0; i < ST_NUM; i++ )
      m_Edges[i].Draw( pDC );
}

CSizingEdge* COutlineRect::FindEdge( CPoint& point )
{
   for( int i = 0; i < ST_NUM; i++ ) {
      if( m_Edges[i].CheckPointed( point ) )
         return &m_Edges[i];
   }
   return NULL;
}

///////////////////////////////////////////////////////////
//
// class CGraphicObject
//
///////////////////////////////////////////////////////////

CGraphicObject::CGraphicObject()
{
   m_nState           = 0;
   m_nShape           = -1;
   m_clrColor         = RGB( 255, 255, 0 );
   m_nHatchBrushStyle = HS_BDIAGONAL;
   m_pVACPolygon      = NULL;
   m_nSpecWndWidth    = 0;
   m_nSpecWndHeight   = 0;
}

CGraphicObject::~CGraphicObject( void )
{
}

void CGraphicObject::SetBoundRect( CRect& rect )
{
   m_OutlineRect.SetRect( rect );
}

void CGraphicObject::Draw( CDC* pDC, int nDrawOptions )
{
   if( m_nAreaType == ( AREA_TYPE_EVENT_ZONE | AREA_TYPE_HEAD_ZONE ) && ( m_nState == 0 ) ) return;

   int pen_width = 1;
   COLORREF pen_color, color;
   pen_color = color = m_clrColor;
   BOOL null_brush   = FALSE;

   if( m_nState & GS_TEMPORARY ) {
      pen_color  = RGB( 128, 128, 128 );
      pen_width  = 2;
      null_brush = TRUE;
   } else {
      if( m_nHatchBrushStyle == -1 ) {
         pen_width  = 2;
         null_brush = TRUE;
      }
      if( m_nState & GS_SELECTED ) {
         pen_color = RGB( 255, 255, 255 );
         pen_width = 3;
      }
   }
   CPen pen( PS_SOLID, pen_width, pen_color );
   pDC->SelectObject( &pen );

   CBrush brush;
   if( null_brush )
      pDC->SelectStockObject( NULL_BRUSH );
   else {
      if( m_nHatchBrushStyle >= 0 )
         brush.CreateHatchBrush( m_nHatchBrushStyle, color );
      else
         brush.CreateSolidBrush( BS_SOLID );
      pDC->SelectObject( &brush );
   }
   pDC->SetBkMode( TRANSPARENT );

#ifndef __ENABLE_MOVING_POLYGON_VERTEX
   DrawObject( pDC );
#endif // __ENABLE_MOVING_POLYGON_VERTEX

   if( AREA_TYPE_AR_AREA == m_nAreaType ) {
      switch( m_nFeatureType ) {
      case 0: // AR_AREA_TYPE_POLYGON
      case 1: // AR_AREA_TYPE_POLYLINE
         if( m_nSpecWndWidth > 0 && m_nSpecWndHeight > 0 ) {
            BCCL::Polygon newPolygon;
            IBox2D box( 0, 0, m_nSpecWndWidth, m_nSpecWndHeight );
            ClipPolygon( m_pVACPolygon, box, &newPolygon );
            if( newPolygon.Vertices.Length > 0 ) {
               newPolygon.UpdateCenterPosition();

               pDC->SetTextColor( RGB( 255, 0, 0 ) );
               pDC->TextOut( newPolygon.CenterPos.X, newPolygon.CenterPos.Y, m_strAreaName );
            }
         }
         break;
      case 2: // AR_AREA_TYPE_POINT
         pDC->SetTextColor( RGB( 255, 0, 0 ) );
         pDC->TextOut( m_pVACPolygon->Vertices[0].X, m_pVACPolygon->Vertices[0].Y, m_strAreaName );
         break;
      default:
         break;
      }
   }

#ifdef __ENABLE_MOVING_POLYGON_VERTEX
   DrawObject( pDC );
#endif // __ENABLE_MOVING_POLYGON_VERTEX

   pDC->SelectStockObject( NULL_PEN );
   pDC->SelectStockObject( NULL_BRUSH );

   if( nDrawOptions & GO_DRAW_OPTION_SIZING_EDGES ) {
      if( m_nState & GS_SELECTED )
         m_OutlineRect.Draw( pDC );
   }
}

void CGraphicObject::Sizing( CPoint& point, int nResizingType )
{
   CRect rect = m_OutlineRect.m_rect;
   switch( nResizingType ) {
   case ST_NORTH:
      rect.top = point.y;
      break;
   case ST_WEST:
      rect.left = point.x;
      break;
   case ST_SOUTH:
      rect.bottom = point.y;
      break;
   case ST_EAST:
      rect.right = point.x;
      break;
   case ST_NORTH_WEST:
      rect.top  = point.y;
      rect.left = point.x;
      break;
   case ST_NORTH_EAST:
      rect.top   = point.y;
      rect.right = point.x;
      break;
   case ST_SOUTH_WEST:
      rect.bottom = point.y;
      rect.left   = point.x;
      break;
   case ST_SOUTH_EAST:
      rect.bottom = point.y;
      rect.right  = point.x;
      break;
   }
   SetBoundRect( rect );
}

void CGraphicObject::Moving( CGraphicObject* s_object, int dx, int dy )
{
   CRect s_rect = s_object->m_OutlineRect.m_rect;
   s_rect.OffsetRect( dx, dy );
   SetBoundRect( s_rect );
}

void CGraphicObject::SetAreaType( int nAreaType )
{
   m_nAreaType = nAreaType;
   switch( m_nAreaType ) {
   case AREA_TYPE_EVENT_ZONE:
   case AREA_TYPE_HEAD_ZONE:
      SetColor( COLOR_YELLOW );
      SetBrushStyle( HS_BDIAGONAL );
      break;
   case AREA_TYPE_TRACKING_AREA:
      SetColor( COLOR_RED );
      SetBrushStyle( HS_FDIAGONAL );
      break;
   case AREA_TYPE_NONDETECTION_AREA:
      SetColor( COLOR_BLACK );
      SetBrushStyle( HS_DIAGCROSS );
      break;
   case AREA_TYPE_NONHANDOFF_AREA:
      SetColor( COLOR_GREEN );
      SetBrushStyle( HS_HORIZONTAL );
      break;
   case AREA_TYPE_DYNAMIC_BACKGROUND_AREA:
      SetColor( COLOR_BLUE );
      SetBrushStyle( HS_VERTICAL );
      break;
   case AREA_TYPE_PRIVACY_AREA:
      SetColor( COLOR_BLACK );
      SetBrushStyle( -1 );
      break;
   case AREA_TYPE_MUTUAL_ASSISTANCE_AREA:
      SetColor( COLOR_BRGREY );
      SetBrushStyle( HS_HORIZONTAL );
      break;
   case AREA_TYPE_MUTUAL_IR_LIGHT_AREA:
      SetColor( COLOR_DKGREEN );
      SetBrushStyle( HS_HORIZONTAL );
      break;
   case AREA_TYPE_AR_AREA:
      SetColor( COLOR_YELLOW );
      break;
   default:
      SetColor( COLOR_BLACK );
      SetBrushStyle( HS_CROSS );
      break;
   }
}

///////////////////////////////////////////////////////////
//
// class CGraphicObjectList
//
///////////////////////////////////////////////////////////

CGraphicObjectList::CGraphicObjectList( void )
{
   m_nDrawAreaTypes     = 0;
   m_nEditableAreaTypes = 0;
   m_nDrawOptions       = GO_DRAW_OPTION_SIZING_EDGES;
}

CGraphicObjectList::~CGraphicObjectList()
{
   ClearList();
}

void CGraphicObjectList::Draw( CDC* pDC )
{
   CGraphicObject* pGObj;
   POSITION pos = GetHeadPosition();
   while( pos ) {
      pGObj = (CGraphicObject*)GetNext( pos );
      if( m_nDrawAreaTypes & ( 1 << pGObj->m_nAreaType ) )
         pGObj->Draw( pDC, m_nDrawOptions );
   }
}

void CGraphicObjectList::ClearList()
{
   CGraphicObject* d_object;
   POSITION pos = GetHeadPosition();
   while( pos ) {
      d_object = (CGraphicObject*)GetNext( pos );
      delete d_object;
      d_object = NULL;
   }
   RemoveAll();
}

BOOL CGraphicObjectList::DeleteGraphicObject( CGraphicObject* pGObj )
{
   POSITION pos = Find( pGObj );
   if( !pos ) return FALSE;
   delete pGObj;
   RemoveAt( pos );
   return TRUE;
}

BOOL CGraphicObjectList::DeleteGraphicObjects_AreaType( int nAreaType )
{
   CGraphicObject* d_object;
   POSITION ppos;
   POSITION pos = GetHeadPosition();
   while( pos ) {
      ppos     = pos;
      d_object = (CGraphicObject*)GetNext( pos );
      if( d_object->m_nAreaType == nAreaType ) {
         delete d_object;
         d_object = NULL;
         RemoveAt( ppos );
      }
   }
   return TRUE;
}

BOOL CGraphicObjectList::DeleteGraphicObjectsAll()
{
   CGraphicObject* d_object;
   POSITION ppos;
   POSITION pos = GetHeadPosition();
   while( pos ) {
      ppos     = pos;
      d_object = (CGraphicObject*)GetNext( pos );
      delete d_object;
      d_object = NULL;
      RemoveAt( ppos );
   }
   return TRUE;
}

CGraphicObject* CGraphicObjectList::GetPointedObject( CPoint& point )
{
   CGraphicObject* pGObj = NULL;
   POSITION pos          = GetTailPosition();
   while( pos ) {
      pGObj = (CGraphicObject*)GetPrev( pos );
      if( m_nEditableAreaTypes & ( 1 << pGObj->m_nAreaType ) ) {
         if( pGObj->CheckPointed( point ) )
            return pGObj;
      }
   }
   return NULL;
}

void CGraphicObjectList::SelectPointedObject( CPoint& point )
{
   CGraphicObject* pt_object = GetPointedObject( point );
   CGraphicObject* pGObj;
   POSITION pos = GetTailPosition();
   while( pos ) {
      pGObj = (CGraphicObject*)GetPrev( pos );
      if( ( m_nEditableAreaTypes & ( 1 << pGObj->m_nAreaType ) ) && pGObj == pt_object )
         pGObj->m_nState |= GS_SELECTED;
      else
         pGObj->m_nState &= ~GS_SELECTED;
   }
}

void CGraphicObjectList::UnSelectAllObjects()
{
   CGraphicObject* pGObj;
   POSITION pos = GetHeadPosition();
   while( pos ) {
      pGObj = (CGraphicObject*)GetNext( pos );
      pGObj->m_nState &= ~GS_SELECTED;
   }
}

void CGraphicObjectList::SelectObject( CGraphicObject* pGObj )
{
   CGraphicObject* go;
   POSITION pos = GetHeadPosition();
   while( pos ) {
      go = (CGraphicObject*)GetNext( pos );
      if( go == pGObj )
         go->m_nState |= GS_SELECTED;
      else
         go->m_nState &= ~GS_SELECTED;
   }
}

CGraphicObject* CGraphicObjectList::GetSelectedObject()
{
   CGraphicObject* pGObj;
   POSITION pos = GetHeadPosition();
   while( pos ) {
      pGObj = (CGraphicObject*)GetNext( pos );
      if( m_nEditableAreaTypes & ( 1 << pGObj->m_nAreaType ) ) {
         if( pGObj->m_nState & GS_SELECTED )
            return pGObj;
      }
   }
   return NULL;
}

CGraphicObject* CGraphicObjectList::GetGraphicObject_VACPolygon( BCCL::Polygon* polygon )
{
   CGraphicObject* pGObj;
   POSITION pos = GetHeadPosition();
   while( pos ) {
      pGObj = (CGraphicObject*)GetNext( pos );
      if( pGObj->m_pVACPolygon == polygon )
         return pGObj;
   }
   return NULL;
}

CGraphicObject* CGraphicObjectList::GetObject()
{
   CGraphicObject* pGObj;
   POSITION pos = GetHeadPosition();
   while( pos ) {
      pGObj = (CGraphicObject*)GetNext( pos );
      if( pGObj )
         return pGObj;
   }
   return NULL;
}

void CGraphicObjectList::Draw_Head( CDC* pDC )
{
   CGraphicObject* pGObj;
   POSITION pos = GetHeadPosition();
   while( pos ) {
      pGObj = (CGraphicObject*)GetNext( pos );
      if( pGObj ) {
         pGObj->Draw( pDC, m_nDrawOptions );
      }
   }
}

CGraphicObject* CGraphicObjectList::GetSelectedObject_Head()
{
   CGraphicObject* pGObj;
   POSITION pos = GetHeadPosition();
   while( pos ) {
      pGObj = (CGraphicObject*)GetNext( pos );
      if( pGObj ) {
         if( pGObj->m_nState & GS_SELECTED )
            return pGObj;
      }
   }
   return NULL;
}

CGraphicObject* CGraphicObjectList::GetPointedObject_Head( CPoint& point )
{
   CGraphicObject* pGObj = NULL;
   POSITION pos          = GetTailPosition();
   while( pos ) {
      pGObj = (CGraphicObject*)GetPrev( pos );
      if( pGObj ) {
         if( pGObj->CheckPointed( point ) )
            return pGObj;
      }
   }
   return NULL;
}

void CGraphicObjectList::SelectPointedObject_Head( CPoint& point )
{
   CGraphicObject* pt_object = GetPointedObject_Head( point );
   CGraphicObject* pGObj;
   POSITION pos = GetTailPosition();
   while( pos ) {
      pGObj = (CGraphicObject*)GetPrev( pos );
      if( pGObj ) {
         if( pGObj == pt_object )
            pGObj->m_nState |= GS_SELECTED;
         else
            pGObj->m_nState &= ~GS_SELECTED;
      }
   }
}

/////////////////////////////////////////////////////////////////////////
//
// CFPoint
//
/////////////////////////////////////////////////////////////////////////

CFPoint::CFPoint()
{
   fx = 0.0f;
   fy = 0.0f;
}

void CFPoint::operator=( const CFPoint& fpoint )
{
   fx = fpoint.fx;
   fy = fpoint.fy;
}

/////////////////////////////////////////////////////////////////////////
//
// CPointArray
//
/////////////////////////////////////////////////////////////////////////

CPointArray::CPointArray()
{
}

CPointArray::~CPointArray()
{
}

void CPointArray::DrawConnectedLine( CDC* pDC )
{
   int i;
   CPoint pt;
   CPen pen( PS_SOLID, 2, RGB( 128, 128, 128 ) );
   pDC->SelectObject( &pen );
   int nSize = (int)GetSize();
   for( i = 0; i < nSize; i++ ) {
      pt = GetAt( i );
      if( i == 0 )
         pDC->MoveTo( pt );
      else
         pDC->LineTo( pt );
   }
   pDC->SelectStockObject( NULL_PEN );
   pen.DeleteObject();
   pen.CreatePen( PS_SOLID, 1, RGB( 255, 255, 0 ) );
   pDC->SelectObject( &pen );
   for( i = 0; i < nSize; i++ ) {
      pt = GetAt( i );
      pDC->MoveTo( pt.x - 2, pt.y );
      pDC->LineTo( pt.x + 3, pt.y );
      pDC->MoveTo( pt.x, pt.y - 2 );
      pDC->LineTo( pt.x, pt.y + 3 );
   }
   pDC->SelectStockObject( NULL_PEN );
}

/////////////////////////////////////////////////////////////////////////
//
// CPolygon
//
/////////////////////////////////////////////////////////////////////////

CPolygon::CPolygon( void )
{
   m_nShape    = SHAPE_POLYGON;
   m_FixedAxis = CPoint( -1, -1 );
}

CPolygon::~CPolygon( void )
{
   m_Points.RemoveAll();
   m_ScalePoints.RemoveAll();
}

void CPolygon::SetPoints( CPointArray& points )
{
   int nSize = (int)points.GetSize();
   m_Points.RemoveAll();
   for( int i = 0; i < nSize; i++ )
      m_Points.Add( points[i] );

   CRgn rgn;
   CRect rect;
   rgn.CreatePolygonRgn( m_Points.GetData(), (int)m_Points.GetSize(), WINDING );
   rgn.GetRgnBox( rect );
   SetBoundRect( rect );
}

BOOL CPolygon::CheckPointed( CPoint& point )
{
   CRgn rgn;
   rgn.CreatePolygonRgn( m_Points.GetData(), (int)m_Points.GetSize(), WINDING );
   int size         = (int)m_Points.GetSize();
   BOOL bPtInRegion = rgn.PtInRegion( point );
   return bPtInRegion;
}

void CPolygon::DrawObject( CDC* pDC )
{
   //pDC->Polygon(m_Points.GetData(),(int)m_Points.GetSize());

   if( m_nHatchBrushStyle >= 0 )
      pDC->Polygon( m_Points.GetData(), (int)m_Points.GetSize() );
   else {
      CBrush brush;
      brush.CreateSolidBrush( BS_SOLID );

      CRgn rgn;
      rgn.CreatePolygonRgn( m_Points.GetData(), (int)m_Points.GetSize(), WINDING );
      pDC->FillRgn( &rgn, &brush );

      pDC->SelectStockObject( NULL_BRUSH );
   }

#ifdef __ENABLE_MOVING_POLYGON_VERTEX
   if( m_nState & GS_SELECTED )
      DrawVertices( pDC );
#endif // __ENABLE_MOVING_POLYGON_VERTEX
}

void CPolygon::Sizing( CPoint& point, int nSizingType )
{
   int i;
   CPointArray arrPoints;
   int size = (int)m_Points.GetSize();
   arrPoints.SetSize( size );
   for( i = 0; i < size; i++ ) {
      arrPoints[i].y = m_Points[i].y;
      arrPoints[i].x = m_Points[i].x;
   }
   for( i = 0; i < size; i++ ) {
      if( m_FixedAxis.y > -1 )
         arrPoints[i].y = m_FixedAxis.y - (int)( ( m_FixedAxis.y - point.y ) * m_ScalePoints[i].fy );
      if( m_FixedAxis.x > -1 )
         arrPoints[i].x = m_FixedAxis.x - (int)( ( m_FixedAxis.x - point.x ) * m_ScalePoints[i].fx );
   }
   SetPoints( arrPoints );
}

void CPolygon::PrepareSizing( int nSizingType )
{
   m_FixedAxis = CPoint( -1, -1 );
   switch( nSizingType ) {
   case ST_NORTH:
   case ST_NORTH_WEST:
   case ST_NORTH_EAST:
      m_FixedAxis.y = m_OutlineRect.m_Edges[ST_SOUTH].m_rect.CenterPoint().y;
      break;
   case ST_SOUTH:
   case ST_SOUTH_WEST:
   case ST_SOUTH_EAST:
      m_FixedAxis.y = m_OutlineRect.m_Edges[ST_NORTH].m_rect.CenterPoint().y;
      break;
   }
   switch( nSizingType ) {
   case ST_EAST:
   case ST_NORTH_EAST:
   case ST_SOUTH_EAST:
      m_FixedAxis.x = m_OutlineRect.m_Edges[ST_WEST].m_rect.CenterPoint().x;
      break;
   case ST_WEST:
   case ST_NORTH_WEST:
   case ST_SOUTH_WEST:
      m_FixedAxis.x = m_OutlineRect.m_Edges[ST_EAST].m_rect.CenterPoint().x;
      break;
   }
   int size = (int)m_Points.GetSize();
   m_ScalePoints.RemoveAll();
   m_ScalePoints.SetSize( size );
   for( int i = 0; i < size; i++ ) {
      if( m_FixedAxis.y > -1 ) {
         int height          = m_OutlineRect.m_rect.Height();
         m_ScalePoints[i].fy = abs( m_FixedAxis.y - m_Points[i].y ) / (float)height;
      }
      if( m_FixedAxis.x > -1 ) {
         int width           = m_OutlineRect.m_rect.Width();
         m_ScalePoints[i].fx = abs( m_FixedAxis.x - m_Points[i].x ) / (float)width;
      }
   }
}

#ifdef __ENABLE_MOVING_POLYGON_VERTEX

int CPolygon::SetCursor( LPCTSTR lpcszName )
{
   ::SetCursor(::AfxGetApp()->LoadStandardCursor( lpcszName ) );

   return 0;
}
int CPolygon::MakeSquareRect( CRect* pRect, CPoint* pPoint, int nSize )
{
   if( !pRect )
      return -1;

   if( !pPoint )
      return -2;

   if( nSize < 1 )
      return -3;

   int half      = (int)( ( nSize / 2 ) + 0.5f );
   pRect->left   = pPoint->x - half;
   pRect->top    = pPoint->y - half;
   pRect->right  = pPoint->x + half;
   pRect->bottom = pPoint->y + half;

   return 0;
}

int CPolygon::GetVertexIdxByPoint( CPoint* pPoint, int nSquareSize /* = VertexSqaureSize */ )
{
   if( !pPoint )
      return -1;

   if( nSquareSize < 1 )
      return -2;

   int pt_cnt = m_Points.GetSize();
   if( pt_cnt == 0 )
      return -10;

   for( int i = 0; i < pt_cnt; i++ ) {
      CRect rc;
      CPoint pt = m_Points.GetAt( i );
      MakeSquareRect( &rc, &pt, nSquareSize );

      if(::PtInRect( &rc, *pPoint ) )
         return i;
   }

   return -11;
}
int CPolygon::UpdateVertexByIdx( int nIdx, CPoint* pPoint )
{
   if( nIdx < 0 || nIdx >= m_Points.GetSize() )
      return -1;

   if( !pPoint )
      return -2;

   m_Points[nIdx] = *pPoint;

   return 0;
}
int CPolygon::DrawVertex( CDC* pDC, CPoint* pPoint, int nSquareSize /* = VertexSqaureSize */ )
{
   if( !pDC )
      return -1;

   if( !pPoint )
      return -2;

   if( nSquareSize < 1 )
      return -3;

   CRect rc;
   MakeSquareRect( &rc, pPoint, nSquareSize );

   CBrush bkgnd_br( VertexSquareBkgndColor );
   CBrush border_br( VertexSquareBorderColor );

   pDC->FillRect( &rc, &bkgnd_br );
   pDC->FrameRect( &rc, &border_br );

   return 0;
}
int CPolygon::DrawVertices( CDC* pDC, int nSquareSize /* = VertexSqaureSize */ )
{
   if( !pDC )
      return -1;

   if( nSquareSize < 1 )
      return -2;

   int pt_cnt = m_Points.GetSize();
   if( pt_cnt == 0 )
      return -10;

   for( int i = 0; i < pt_cnt; i++ ) {
      CPoint pt = m_Points.GetAt( i );
      DrawVertex( pDC, &pt, nSquareSize );
   }

   return 0;
}

#endif // __ENABLE_MOVING_POLYGON_VERTEX

void CPolygon::Moving( CGraphicObject* s_object, int dx, int dy )
{
   int i;
   CPolygon* s_polygon = (CPolygon*)s_object;
   CPointArray arrPoints;
   int size = (int)m_Points.GetSize();
   arrPoints.SetSize( size );
   for( i = 0; i < size; i++ ) {
      arrPoints[i].x = s_polygon->m_Points[i].x + dx;
      arrPoints[i].y = s_polygon->m_Points[i].y + dy;
   }
   SetPoints( arrPoints );
}

/////////////////////////////////////////////////////////////////////////
//
// CRectangle
//
/////////////////////////////////////////////////////////////////////////

CRectangle::CRectangle( void )
{
   m_nShape = SHAPE_RECTANGLE;
}

CRectangle::~CRectangle( void )
{
}

BOOL CRectangle::CheckPointed( CPoint& point )
{
   CRect rect = m_OutlineRect.m_rect;
   return rect.PtInRect( point );
}

void CRectangle::DrawObject( CDC* pDC )
{
   pDC->Rectangle( m_OutlineRect.m_rect );
}

/////////////////////////////////////////////////////////////////////////
//
// Circle
//
/////////////////////////////////////////////////////////////////////////

CCircle::CCircle( void )
{
   m_nShape    = SHAPE_ELLIPSE;
   m_FixedAxis = CPoint( -1, -1 );

   m_nHRRadius = 0;
}

CCircle::~CCircle( void )
{
   m_Points.RemoveAll();
   m_ScalePoints.RemoveAll();
}

void CCircle::SetPoints( CPointArray& points )
{
   int nSize = (int)points.GetSize();

   if( nSize != 2 )
      return;

   m_Points.RemoveAll();
   for( int i = 0; i < nSize; i++ )
      m_Points.Add( points[i] );

   CRgn rgn;
   CRect rect;

   POINT CenterPos;
   m_nHRRadius = 0;
   rect        = Calcu_Circle_Position( m_nHRRadius, CenterPos, m_Points[0], m_Points[1] );

   rgn.CreateEllipticRgn( rect.left, rect.top, rect.right, rect.bottom );

   rgn.GetRgnBox( rect );
   SetBoundRect( rect );
}

BOOL CCircle::CheckPointed( CPoint& point )
{
   CRgn rgn;
   POINT CenterPos;
   int radius = 0;
   CRect rect = Calcu_Circle_Position( radius, CenterPos, m_Points[0], m_Points[1] );
   rgn.CreateEllipticRgn( rect.left, rect.top, rect.right, rect.bottom );

   return rgn.PtInRegion( point );
}

void CCircle::DrawObject( CDC* pDC )
{
   //pDC->Polygon(m_Points.GetData(),(int)m_Points.GetSize());

   POINT CenterPos;
   int radius = 0;
   CRect rect = Calcu_Circle_Position( radius, CenterPos, m_Points[0], m_Points[1] );

   if( m_nHatchBrushStyle >= 0 )
      //pDC->Ellipse(m_Points[0].x, m_Points[0].y, m_Points[1].x, m_Points[1].y);
      pDC->Ellipse( rect );
   else {
      CBrush brush;
      brush.CreateSolidBrush( BS_SOLID );

      CRgn rgn;
      //rgn.CreatePolygonRgn (m_Points.GetData(),(int)m_Points.GetSize(), WINDING);
      //rgn.CreateEllipticRgn (m_Points[0].x, m_Points[0].y, m_Points[1].x, m_Points[1].y);

      rgn.CreateEllipticRgn( rect.left, rect.top, rect.right, rect.bottom );
      pDC->FillRgn( &rgn, &brush );
      pDC->SelectStockObject( NULL_BRUSH );
   }
}

void CCircle::Sizing( CPoint& point, int nSizingType )
{
   int i;
   CPointArray arrPoints;
   int size = (int)m_Points.GetSize();
   arrPoints.SetSize( size );
   for( i = 0; i < size; i++ ) {
      arrPoints[i].y = m_Points[i].y;
      arrPoints[i].x = m_Points[i].x;
   }
   for( i = 0; i < size; i++ ) {
      if( m_FixedAxis.y > -1 )
         arrPoints[i].y = m_FixedAxis.y - (int)( ( m_FixedAxis.y - point.y ) * m_ScalePoints[i].fy );
      if( m_FixedAxis.x > -1 )
         arrPoints[i].x = m_FixedAxis.x - (int)( ( m_FixedAxis.x - point.x ) * m_ScalePoints[i].fx );
   }
   SetPoints( arrPoints );
}

void CCircle::PrepareSizing( int nSizingType )
{
   m_FixedAxis = CPoint( -1, -1 );
   switch( nSizingType ) {
   case ST_NORTH:
   case ST_NORTH_WEST:
   case ST_NORTH_EAST:
      m_FixedAxis.y = m_OutlineRect.m_Edges[ST_SOUTH].m_rect.CenterPoint().y;
      break;
   case ST_SOUTH:
   case ST_SOUTH_WEST:
   case ST_SOUTH_EAST:
      m_FixedAxis.y = m_OutlineRect.m_Edges[ST_NORTH].m_rect.CenterPoint().y;
      break;
   }
   switch( nSizingType ) {
   case ST_EAST:
   case ST_NORTH_EAST:
   case ST_SOUTH_EAST:
      m_FixedAxis.x = m_OutlineRect.m_Edges[ST_WEST].m_rect.CenterPoint().x;
      break;
   case ST_WEST:
   case ST_NORTH_WEST:
   case ST_SOUTH_WEST:
      m_FixedAxis.x = m_OutlineRect.m_Edges[ST_EAST].m_rect.CenterPoint().x;
      break;
   }
   int size = (int)m_Points.GetSize();
   m_ScalePoints.RemoveAll();
   m_ScalePoints.SetSize( size );
   for( int i = 0; i < size; i++ ) {
      if( m_FixedAxis.y > -1 ) {
         int height          = m_OutlineRect.m_rect.Height();
         m_ScalePoints[i].fy = abs( m_FixedAxis.y - m_Points[i].y ) / (float)height;
      }
      if( m_FixedAxis.x > -1 ) {
         int width           = m_OutlineRect.m_rect.Width();
         m_ScalePoints[i].fx = abs( m_FixedAxis.x - m_Points[i].x ) / (float)width;
      }
   }
}

void CCircle::Moving( CGraphicObject* s_object, int dx, int dy )
{
   int i;
   CCircle* s_polygon = (CCircle*)s_object;
   CPointArray arrPoints;
   int size = (int)m_Points.GetSize();
   arrPoints.SetSize( size );
   for( i = 0; i < size; i++ ) {
      arrPoints[i].x = s_polygon->m_Points[i].x + dx;
      arrPoints[i].y = s_polygon->m_Points[i].y + dy;
   }
   SetPoints( arrPoints );
}

CRect CCircle::Calcu_Circle_Position( int& radius, POINT& CenterPos, POINT StartPos, POINT EndPos )
{
   int sx = StartPos.x;
   int sy = StartPos.y;
   int ex = EndPos.x;
   int ey = EndPos.y;

   CPoint Center;

   Center.x = (int)( ( sx + ex ) / 2 );
   Center.y = (int)( ( sy + ey ) / 2 );

   double dDistance = sqrt( (double)( abs( sx - Center.x ) * abs( sx - Center.x ) + abs( sy - Center.y ) * abs( sy - Center.y ) ) );

   radius = (int)( dDistance );

   CRect rect;

   rect.left   = Center.x - (int)dDistance;
   rect.top    = Center.y - (int)dDistance;
   rect.right  = Center.x + (int)dDistance;
   rect.bottom = Center.y + (int)dDistance;

   CenterPos = Center;

   return rect;
}
}
#endif

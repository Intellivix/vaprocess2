#include "StdAfx.h"
#include "ARFeature.h"

#if defined( __SUPPORT_AUGMENTED_REALITY )
///////////////////////////////////////////////////////////////////////////////
//
// Class: CARFeatureInfo
//
///////////////////////////////////////////////////////////////////////////////

CARFeatureInfo::CARFeatureInfo()
{
   static int n_polygons = 1;
   ID                    = n_polygons++;
   Type                  = 0;
   AlarmState            = 0;
   FeatureType           = 0;
   AreaName              = "";
   m_pVoid               = NULL;
   MinZoom               = 1.0f;
   MaxZoom               = 100.0f;

   FontSize  = 25;
   FontName  = _T("Arial");
   FontColor = RGB( 255, 255, 255 );
   LineColor = RGB( 255, 255, 0 );
   Prev = Next = NULL;
}

void CARFeatureInfo::Create( int n_vertices )
{
   Vertices.Create( n_vertices );
   PTZPositions.Create( n_vertices );
}

int CARFeatureInfo::CheckSetup( CARFeatureInfo* ft )
{
   int ret_flag = 0;
   FileIO buffThis, buffFrom;
   CXMLIO xmlIOThis, xmlIOFrom;

   // 변경된 아이템이 있는지 체크.
   xmlIOThis.SetFileIO( &buffThis, XMLIO_Write );
   xmlIOFrom.SetFileIO( &buffFrom, XMLIO_Write );
   WriteFile( &xmlIOThis );
   ft->WriteFile( &xmlIOFrom );

   if( buffThis.IsDiff( buffFrom ) )
      ret_flag |= CHECK_SETUP_RESULT_CHANGED;

   buffThis.SetLength( 0 );
   buffFrom.SetLength( 0 );

   if( ret_flag & CHECK_SETUP_RESULT_CHANGED ) {
      ret_flag |= CHECK_SETUP_RESULT_RESTART;
   }
   return ( ret_flag );
}

int CARFeatureInfo::ReadFile( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "CARFeatureInfo" ) ) {
      int length;
      xmlNode.Attribute( TYPE_ID( int ), "ID", &ID );
      xmlNode.Attribute( TYPE_ID( int ), "Type", &Type );
      xmlNode.Attribute( TYPE_ID( int ), "FeatureType", &FeatureType );
      xmlNode.Attribute( TYPE_ID( string ), "AreaName", &AreaName );
      xmlNode.Attribute( TYPE_ID( int ), "length", &length );
      xmlNode.Attribute( TYPE_ID( float ), "MinZoom", &MinZoom );
      xmlNode.Attribute( TYPE_ID( float ), "MaxZoom", &MaxZoom );
      xmlNode.Attribute( TYPE_ID( string ), "FontName", &FontName );
      xmlNode.Attribute( TYPE_ID( int ), "FontSize", &FontSize );
      xmlNode.Attribute( TYPE_ID( DWORD ), "FontColor", &FontColor );
      xmlNode.Attribute( TYPE_ID( DWORD ), "LineColor", &LineColor );

      Create( length );
      xmlNode.Attribute( TYPE_ID( IPoint2D ), "Vertices", (IPoint2D*)Vertices, Vertices.Length );
      xmlNode.Attribute( TYPE_ID( FPoint3D ), "PTZPositions", (FPoint3D*)PTZPositions, PTZPositions.Length );
      xmlNode.End();
   }
   return ( DONE );
}

int CARFeatureInfo::WriteFile( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "CARFeatureInfo" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "ID", &ID );
      xmlNode.Attribute( TYPE_ID( int ), "Type", &Type );
      xmlNode.Attribute( TYPE_ID( int ), "FeatureType", &FeatureType );
      xmlNode.Attribute( TYPE_ID( string ), "AreaName", &AreaName );
      xmlNode.Attribute( TYPE_ID( int ), "length", &Vertices.Length );
      xmlNode.Attribute( TYPE_ID( float ), "MinZoom", &MinZoom );
      xmlNode.Attribute( TYPE_ID( float ), "MaxZoom", &MaxZoom );
      xmlNode.Attribute( TYPE_ID( string ), "FontName", &FontName );
      xmlNode.Attribute( TYPE_ID( int ), "FontSize", &FontSize );
      xmlNode.Attribute( TYPE_ID( DWORD ), "FontColor", &FontColor );
      xmlNode.Attribute( TYPE_ID( DWORD ), "LineColor", &LineColor );
      xmlNode.Attribute( TYPE_ID( IPoint2D ), "Vertices", (IPoint2D*)Vertices, Vertices.Length );
      xmlNode.Attribute( TYPE_ID( FPoint3D ), "PTZPositions", (FPoint3D*)PTZPositions, PTZPositions.Length );
      xmlNode.End();
   }
   return ( DONE );
}

BOOL CARFeatureInfo::IsInZoomRange( float fZoom )
{
   if( fZoom >= MinZoom && fZoom <= MaxZoom )
      return TRUE;
   return FALSE;
}
#endif

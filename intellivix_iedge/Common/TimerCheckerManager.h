﻿#ifndef TIMERCHECKERMANAGER_H
#define TIMERCHECKERMANAGER_H

#include <memory>
#include <map>

#include <ctime>
#include <chrono>

#include "bccl_plog.h"

class TimerChecker {
public:
   explicit TimerChecker( const std::string& name, uint interval_ms );
   ~TimerChecker() = default;

   void Reset();
   std::string DebugIntervalError();
   double getCurrentDiff();

private:
   std::string m_name;
   std::chrono::high_resolution_clock::time_point m_last_time;
   uint m_interval_ms = 0;
   double m_diff_ms   = 0;
};

/**
 * @brief Singleton Pattern. TimerCheckerManager::Get().XXX();
 */
class TimerCheckerManager {
public:
   /// Singleton Pattern.
   static TimerCheckerManager& Get();

   // delete copy and move constructors and assign operators
   TimerCheckerManager( TimerCheckerManager const& ) = delete; // Copy construct
   TimerCheckerManager( TimerCheckerManager&& )      = delete; // Move construct
   TimerCheckerManager& operator=( TimerCheckerManager const& ) = delete; // Copy assign
   TimerCheckerManager& operator=( TimerCheckerManager&& ) = delete; // Move assign

   std::string DebugMsg( const std::string& timer_name, uint interval_ms );

   std::shared_ptr<TimerChecker> const GetTimer( const std::string& key );

protected:
   /// Singleton Pattern.
   TimerCheckerManager();
   ~TimerCheckerManager();

   /**
    * map key   : Timer's name,
    * map value : Timer's instance
    */
   std::map<std::string, std::shared_ptr<TimerChecker>> m_timers;
};

#endif // TIMERCHECKERMANAGER_H

﻿#include "CpuMonitoring.h"

bool CpuMonitoring::Start( uint interval_ms )
{
   m_interval_ms = interval_ms;
   InnerThread::SeveThreadName( "cpu monitoring" );
   m_thread_running = true;
   return InnerThread::StartThread( this );
}

void CpuMonitoring::Stop()
{
   m_thread_running = false;
   InnerThread::StopThread();
}

double CpuMonitoring::getCurrentCpuUsage()
{
   return m_current_cpu_usage;
}

std::vector<size_t> CpuMonitoring::get_cpu_times()
{
   std::ifstream proc_stat( "/proc/stat" );
   proc_stat.ignore( 5, ' ' ); // Skip the 'cpu' prefix.
   std::vector<size_t> times;
   for( size_t time; proc_stat >> time; times.push_back( time ) )
      ;
   return times;
}

bool CpuMonitoring::get_cpu_times( size_t& idle_time, size_t& total_time )
{
   const std::vector<size_t> cpu_times = get_cpu_times();
   if( cpu_times.size() < 4 )
      return false;
   idle_time  = cpu_times[3];
   total_time = size_t( std::accumulate( cpu_times.begin(), cpu_times.end(), 0 ) );
   return true;
}

void CpuMonitoring::ThreadLoop()
{
   size_t previous_idle_time = 0, previous_total_time = 0;
   size_t idle_time, total_time;

   while( m_thread_running ) {
      get_cpu_times( idle_time, total_time );
      const double idle_time_delta  = idle_time - previous_idle_time;
      const double total_time_delta = total_time - previous_total_time;
      m_current_cpu_usage           = 100.0 * ( 1.0 - idle_time_delta / total_time_delta );

      logv( "CPU_USAGE(%%): %5.1f", m_current_cpu_usage );

      previous_idle_time  = idle_time;
      previous_total_time = total_time;

      InnerThread::Sleep_ms( m_interval_ms );
   }
}

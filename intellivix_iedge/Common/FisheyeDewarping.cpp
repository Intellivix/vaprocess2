#include "StdAfx.h"
#include "FisheyeDewarping.h"

#ifdef __SUPPORT_FISHEYE_CAMERA

#pragma comment( lib, "opengl32.lib" )
#pragma comment( lib, "glu32.lib" )

///////////////////////////////////////////////////////////////////////////////
//
// Class: WarpingTable_FisheyeToCylinder
//
///////////////////////////////////////////////////////////////////////////////

int WarpingTable_FisheyeToCylinder::Initialize( int si_width, int si_height, FPoint2D& center, float radius, float up_range, float lp_range, int di_width, int di_height )

{
   int x, y;

   if( Create( si_width, si_height, di_width, di_height ) ) return ( 1 );
   int ex       = si_width - 1;
   int ey       = si_height - 1;
   float a      = 2.0f * radius / MC_PI;
   float tu     = tan( MC_DEG2RAD * up_range );
   float tl     = tan( MC_DEG2RAD * lp_range );
   float rc     = di_height / ( tu + tl );
   float yc     = rc * tu;
   float d_phi2 = MC_2_PI / di_width;
   for( y = 0; y < di_height; y++ ) {
      WTElement* _RESTRICT l_Array = Array[y];
      float phi2                   = 0.0f;
      for( x = 0; x < di_width; x++ ) {
         float theta2 = atan( ( yc - y ) / rc ) + 0.5f * MC_PI_2;
         float r      = a * theta2;
         float fx     = center.X - r * cos( phi2 );
         float fy     = center.Y - r * sin( phi2 );
         int ix       = (int)fx;
         if( 0.0f <= fx && ix < ex ) {
            int iy = (int)fy;
            if( 0.0f <= fy && iy < ey ) l_Array[x].Coord( fx, fy );
         }
         phi2 += d_phi2;
      }
   }
   GetInterpolationWeights();
   Flag_Initialized = TRUE;
   return ( DONE );
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: WarpingTable_FisheyeToSphere
//
///////////////////////////////////////////////////////////////////////////////

int WarpingTable_FisheyeToSphere::Initialize( int si_width, int si_height, FPoint2D& center, float radius, int di_width, int di_height )

{
   int x, y;

   if( Create( si_width, si_height, di_width, di_height ) ) return ( 1 );
   int ex        = si_width - 1;
   int ey        = si_height - 1;
   float a       = 2.0f * radius / MC_PI;
   float d_phi   = MC_PI / di_width;
   float d_theta = MC_PI / di_height;
   float theta   = MC_PI_2;
   for( y = 0; y < di_height; y++ ) {
      WTElement* _RESTRICT l_Array = Array[y];
      float cos_theta              = cos( theta );
      float sin_theta              = sin( theta );
      float phi                    = 0.0f;
      for( x = 0; x < di_width; x++ ) {
         float theta2 = acos( cos_theta * sin( phi ) );
         float r      = a * theta2;
         float dx     = cos_theta * cos( phi );
         float dy     = sin_theta;
         float d      = sqrt( dx * dx + dy * dy );
         if( d < MC_VSV )
            r = 0.0;
         else
            r /= d;
         float fx = center.X - r * dx;
         int ix   = (int)fx;
         if( 0.0f <= fx && ix < ex ) {
            float fy = center.Y - r * dy;
            int iy   = (int)fy;
            if( 0.0f <= fy && iy < ey ) l_Array[x].Coord( fx, fy );
         }
         phi += d_phi;
      }
      theta -= d_theta;
   }
   GetInterpolationWeights();
   Flag_Initialized = TRUE;
   return ( DONE );
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CGLVirtualPTZ
//
///////////////////////////////////////////////////////////////////////////////

CGLVirtualPTZ::CGLVirtualPTZ()

{
   PanAngle    = 0.0f;
   TiltAngle   = 0.0f;
   FocalLength = 0.0f;
   VFOV        = 90.0f;
   _Init();
}

CGLVirtualPTZ::~CGLVirtualPTZ()

{
   Close();
}

void CGLVirtualPTZ::_Init()

{
   Flag_Initialized = FALSE;
   NumSrcImgDivs    = 0;
   DC               = NULL;
   GLRC             = NULL;
}

void CGLVirtualPTZ::Close()

{
   int i;

   if( !( !Surfaces ) ) {
      for( i = 0; i < Surfaces.Length; i++ ) {
         if( Surfaces[i].TexID ) DeleteTextureObject( Surfaces[i].TexID );
      }
   }
   Surfaces.Delete();
   wglMakeCurrent( NULL, NULL );
   if( GLRC != NULL ) wglDeleteContext( GLRC );
   if( RenderWnd.GetSafeHwnd() != NULL ) RenderWnd.DestroyWindow();
   _Init();
}

void CGLVirtualPTZ::CreateFaces( int si_width, int si_height, int n_div, float s_phi, float e_phi, float s_theta, float e_theta, Array1D<_FACE>& d_array )

{
   int i, j, n;
   float a;

   int ti_width, ti_height;
   GetTexImgSize( si_width, si_height, ti_width, ti_height );
   int n_div1    = n_div - 1;
   float r       = SrcImgSize.Width / MC_PI;
   float d_phi   = ( e_phi - s_phi ) / n_div;
   float d_theta = ( e_theta - s_theta ) / n_div;
   float d_px    = 0.5f / ti_width;
   float d_py    = 0.5f / ti_height;
   float d_tx    = (float)si_width / ti_width / n_div;
   float d_ty    = (float)si_height / ti_height / n_div;
   float theta1  = s_theta;
   d_array.Create( n_div * n_div );
   for( i = n = 0; i < n_div; i++, theta1 += d_theta ) {
      float ty1 = i * d_ty;
      float ty2 = ty1 + d_ty;
      if( i == 0 )
         ty1 += d_py;
      else if( i == n_div1 )
         ty2 -= d_py;
      float phi1 = s_phi;
      for( j = 0; j < n_div; j++, phi1 += d_phi ) {
         float tx1 = j * d_tx;
         float tx2 = tx1 + d_tx;
         if( j == 0 )
            tx1 += d_px;
         else if( j == n_div1 )
            tx2 -= d_px;
         FPoint2D* tv = d_array[n].TexVertices;
         FPoint3D* v  = d_array[n].Vertices;
         n++;
         tv[0]( tx1, ty1 );
         tv[1]( tx2, ty1 );
         tv[2]( tx2, ty2 );
         tv[3]( tx1, ty2 );
         float phi2     = phi1 + d_phi;
         float theta2   = theta1 + d_theta;
         float cos_phi1 = cos( phi1 );
         float sin_phi1 = sin( phi1 );
         float cos_phi2 = cos( phi2 );
         float sin_phi2 = sin( phi2 );
         // (phi1,theta1)
         a      = r * cos( theta1 );
         v[0].X = a * cos_phi1;
         v[0].Y = r * sin( theta1 );
         v[0].Z = a * sin_phi1;
         // (phi2,theta1)
         v[1].X = a * cos_phi2;
         v[1].Y = v[0].Y;
         v[1].Z = a * sin_phi2;
         // (phi2,theta2)
         a      = r * cos( theta2 );
         v[2].X = a * cos_phi2;
         v[2].Y = r * sin( theta2 );
         v[2].Z = a * sin_phi2;
         // (phi1,theta2)
         v[3].X = a * cos_phi1;
         v[3].Y = v[2].Y;
         v[3].Z = a * sin_phi1;
      }
   }
}

int CGLVirtualPTZ::CreateRenderWindow()

{
   if( !RenderWnd.CreateEx( 0, AfxRegisterWndClass( NULL ), _T(""), WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_POPUP, CRect( 0, 0, RndImgSize.Width, RndImgSize.Height ), NULL, 0 ) ) return ( 1 );
   HWND h_wnd = RenderWnd.GetSafeHwnd();
   if( h_wnd == NULL ) return ( 2 );
   DC = GetDC( h_wnd );
   PIXELFORMATDESCRIPTOR pfd;
   memset( &pfd, 0, sizeof( PIXELFORMATDESCRIPTOR ) );
   pfd.nSize        = sizeof( pfd );
   pfd.nVersion     = 1;
   pfd.dwFlags      = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
   pfd.iPixelType   = PFD_TYPE_RGBA;
   pfd.cColorBits   = 24;
   pfd.cDepthBits   = 16;
   pfd.cStencilBits = 8;
   int pf           = ChoosePixelFormat( DC, &pfd );
   if( !pf ) return ( 3 );
   if( SetPixelFormat( DC, pf, &pfd ) == FALSE ) return ( 4 );
   GLRC = wglCreateContext( DC );
   if( GLRC == NULL ) return ( 5 );
   if( wglMakeCurrent( DC, GLRC ) == FALSE ) return ( 6 );
   return ( DONE );
}

GLuint CGLVirtualPTZ::CreateTextureObject( int si_width, int si_height )

{
   if( !wglMakeCurrent( DC, GLRC ) ) return ( 0 );
   int ti_width, ti_height;
   GetTexImgSize( si_width, si_height, ti_width, ti_height );
   BGRImage t_image( ti_width, ti_height );
   t_image.Clear();
   GLuint tex_id;
   glPixelStorei( GL_PACK_ALIGNMENT, 4 );
   glGenTextures( 1, &tex_id );
   glBindTexture( GL_TEXTURE_2D, tex_id );
   glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP );
   glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP );
   glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
   glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
   glTexImage2D( GL_TEXTURE_2D, 0, 3, ti_width, ti_height, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, (BGRPixel*)t_image );
   glBindTexture( GL_TEXTURE_2D, 0 );
   wglMakeCurrent( NULL, NULL );
   return ( tex_id );
}

int CGLVirtualPTZ::DeleteTextureObject( GLuint tex_id )

{
   if( DC == NULL || GLRC == NULL ) return ( 1 );
   if( !tex_id ) return ( 2 );
   if( !wglMakeCurrent( DC, GLRC ) ) return ( 3 );
   glDeleteTextures( 1, &tex_id );
   wglMakeCurrent( NULL, NULL );
   return ( DONE );
}

void CGLVirtualPTZ::GetTexImgSize( int si_width, int si_height, int& ti_width, int& ti_height )

{
   float a   = log( 2.0f );
   ti_width  = (int)pow( 2.0f, ceil( log( (float)si_width ) / a ) );
   ti_height = (int)pow( 2.0f, ceil( log( (float)si_height ) / a ) );
}

int CGLVirtualPTZ::Initialize( int si_width, int si_height, int di_width, int di_height, int n_si_div, int n_sf_div )

{
   int i, j, n;

   Close();
   if( ( si_width % n_si_div ) || ( si_height % n_si_div ) ) return ( 1 );
   SrcImgSize( si_width, si_height );
   RndImgSize( di_width, di_height );
   NumSrcImgDivs = n_si_div;
   SetVFOV( VFOV );
   if( CreateRenderWindow() ) return ( 2 );
   Surfaces.Create( n_si_div * n_si_div );
   int pi_width  = si_width / n_si_div;
   int pi_height = si_height / n_si_div;
   float d_theta = MC_PI / n_si_div;
   float d_phi   = MC_PI / n_si_div;
   float theta   = MC_PI_2;
   for( i = n = 0; i < n_si_div; i++, theta -= d_theta ) {
      float phi = 0.0f;
      for( j = 0; j < n_si_div; j++, phi += d_phi ) {
         CreateFaces( pi_width, pi_height, n_sf_div, phi, phi + d_phi, theta, theta - d_theta, Surfaces[n].Faces );
         GLuint tex_id = CreateTextureObject( pi_width, pi_height );
         if( !tex_id ) return ( 1 );
         Surfaces[n++].TexID = tex_id;
      }
   }
   Flag_Initialized = TRUE;
   return ( DONE );
}

int CGLVirtualPTZ::Render( BGRImage& d_image )

{
   int i, j, k;

   if( !IsInitialized() ) return ( 1 );
   wglMakeCurrent( DC, GLRC );
   glViewport( 0, 0, RndImgSize.Width, RndImgSize.Height );
   glClearColor( 0.5f, 0.0f, 0.0f, 1.0f );
   glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
   glEnable( GL_DEPTH_TEST );
   glMatrixMode( GL_PROJECTION );
   glLoadIdentity();
   glScalef( 1.0f, -1.0f, 1.0f );
   gluPerspective( VFOV, (float)RndImgSize.Width / RndImgSize.Height, 0.01f, 10000.0f );
   glMatrixMode( GL_MODELVIEW );
   glLoadIdentity();
   gluLookAt( 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0 );
   glRotatef( 90.0f - TiltAngle, 1.0f, 0.0f, 0.0f );
   glRotatef( PanAngle - 90.0f, 0.0f, 0.0f, -1.0f );
   glEnable( GL_TEXTURE_2D );
   glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );
   for( i = 0; i < Surfaces.Length; i++ ) {
      glBindTexture( GL_TEXTURE_2D, Surfaces[i].TexID );
      Array1D<_FACE>& faces = Surfaces[i].Faces;
      for( j = 0; j < faces.Length; j++ ) {
         FPoint2D* tv = faces[j].TexVertices;
         FPoint3D* v  = faces[j].Vertices;
         glBegin( GL_POLYGON );
         for( k = 0; k < 4; k++ ) {
            glTexCoord2fv( (float*)&tv[k] );
            glVertex3fv( (float*)&v[k] );
         }
         glEnd();
      }
      glBindTexture( GL_TEXTURE_2D, 0 );
   }
   glDisable( GL_TEXTURE_2D );
   glFinish();
   glReadPixels( 0, 0, d_image.Width, d_image.Height, GL_BGR_EXT, GL_UNSIGNED_BYTE, (BGRPixel*)d_image );
   SwapBuffers( DC );
   wglMakeCurrent( NULL, NULL );
   return ( DONE );
}

int CGLVirtualPTZ::SetFocalLength( float f_length )

{
   if( !IsInitialized() ) return ( 1 );
   FocalLength = f_length;
   VFOV        = 2.0f * MC_RAD2DEG * atan( 0.5f * RndImgSize.Height / f_length );
   return ( DONE );
}

int CGLVirtualPTZ::SetPanTiltPos( float pan, float tilt )

{
   PanAngle  = pan;
   TiltAngle = tilt;
   return ( DONE );
}

int CGLVirtualPTZ::SetSrcImage( BGRImage& s_image )

{
   int i, j, n;

   if( !IsInitialized() ) return ( 1 );
   if( !wglMakeCurrent( DC, GLRC ) ) return ( 2 );
   glPixelStorei( GL_PACK_ALIGNMENT, 4 );
   if( NumSrcImgDivs == 1 ) {
      glBindTexture( GL_TEXTURE_2D, Surfaces[0].TexID );
      glTexSubImage2D( GL_TEXTURE_2D, 0, 0, 0, s_image.Width, s_image.Height, GL_BGR_EXT, GL_UNSIGNED_BYTE, (BGRPixel*)s_image );
   } else {
      int pi_width  = s_image.Width / NumSrcImgDivs;
      int pi_height = s_image.Height / NumSrcImgDivs;
      int sy        = 0;
      for( i = n = 0; i < NumSrcImgDivs; i++, sy += pi_height ) {
         int sx = 0;
         for( j = 0; j < NumSrcImgDivs; j++, sx += pi_width ) {
            BGRImage p_image = s_image.Extract( sx, sy, pi_width, pi_height );
            glBindTexture( GL_TEXTURE_2D, Surfaces[n++].TexID );
            glTexSubImage2D( GL_TEXTURE_2D, 0, 0, 0, p_image.Width, p_image.Height, GL_BGR_EXT, GL_UNSIGNED_BYTE, (BGRPixel*)p_image );
            glBindTexture( GL_TEXTURE_2D, 0 );
         }
      }
   }
   wglMakeCurrent( NULL, NULL );
   return ( DONE );
}

int CGLVirtualPTZ::SetVFOV( float v_fov )

{
   if( !IsInitialized() ) return ( 1 );
   VFOV        = v_fov;
   FocalLength = 0.5f * RndImgSize.Height / tan( 0.5f * MC_DEG2RAD * v_fov );
   return ( DONE );
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: Minimization_Circumcircle
//
///////////////////////////////////////////////////////////////////////////////

class Minimization_Circumcircle : public MinimizationBase_Powell

{
protected:
   float Radius;
   IP2DArray1D* Points;

protected:
   virtual double CostFunction( Matrix& vec_p );

public:
   void GetParameters( FPoint2D& c_pos );
   void Initialize( IP2DArray1D& p_array, FPoint2D& c_pos, float radius );
};

double Minimization_Circumcircle::CostFunction( Matrix& vec_p )

{
   int i;

   IP2DArray1D& p_array = *Points;
   double sr            = Radius * vec_p( 2 ) * vec_p( 2 );
   double e             = 0.0;
   for( i = 0; i < p_array.Length; i++ ) {
      double dx = p_array[i].X - vec_p( 0 );
      double dy = p_array[i].Y - vec_p( 1 );
      double r  = sqrt( dx * dx + dy * dy );
      e += fabs( r - Radius );
   }
   return ( e );
}

void Minimization_Circumcircle::GetParameters( FPoint2D& c_pos )

{
   c_pos.X = (float)ParamVector( 0 );
   c_pos.Y = (float)ParamVector( 1 );
}

void Minimization_Circumcircle::Initialize( IP2DArray1D& p_array, FPoint2D& c_pos, float radius )

{
   Points = &p_array;
   Radius = radius;
   MinimizationBase_Powell::Initialize( 2 );
   ParamVector( 0 ) = c_pos.X;
   ParamVector( 1 ) = c_pos.Y;
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

void ConvertFisheyeToCylindricalCoord( FISHEYE_INFO& fisheye_info, int ci_width, int ci_height, FPoint2D& s_coord, FPoint2D& d_coord )

{
   float a    = 2.0f * fisheye_info.radius / MC_PI;
   float tu   = tan( MC_DEG2RAD * fisheye_info.up_range );
   float tl   = tan( MC_DEG2RAD * fisheye_info.lp_range );
   float rc   = ci_height / ( tu + tl );
   float yc   = rc * tu;
   float dx   = fisheye_info.center.X - s_coord.X;
   float dy   = fisheye_info.center.Y - s_coord.Y;
   float phi2 = atan2( dy, dx );
   if( phi2 < 0.0f ) phi2 += MC_2_PI;
   d_coord.X = ci_width * phi2 / MC_2_PI;
   float r   = sqrt( dx * dx + dy * dy );
   d_coord.Y = yc - rc * tan( r / a - 0.5f * MC_PI_2 );
}

void ConvertCylindricalToFisheyeCoord( FISHEYE_INFO& fisheye_info, int ci_width, int ci_height, FPoint2D& s_coord, FPoint2D& d_coord )

{
   float a      = 2.0f * fisheye_info.radius / MC_PI;
   float tu     = tan( MC_DEG2RAD * fisheye_info.up_range );
   float tl     = tan( MC_DEG2RAD * fisheye_info.lp_range );
   float rc     = ci_height / ( tu + tl );
   float yc     = rc * tu;
   float phi2   = MC_2_PI * s_coord.X / ci_width;
   float theta2 = atan( ( yc - s_coord.Y ) / rc ) + 0.5f * MC_PI_2;
   float r      = a * theta2;
   d_coord.X    = fisheye_info.center.X - r * cos( phi2 );
   d_coord.Y    = fisheye_info.center.Y - r * sin( phi2 );
}

void ConvertFisheyeToCylindricalImage( FISHEYE_INFO& fisheye_info, GImage& s_image, GImage& d_image )

{
   int x, y;

   int ex       = s_image.Width - 1;
   int ey       = s_image.Height - 1;
   float a      = 2.0f * fisheye_info.radius / MC_PI;
   float tu     = tan( MC_DEG2RAD * fisheye_info.up_range );
   float tl     = tan( MC_DEG2RAD * fisheye_info.lp_range );
   float rc     = d_image.Height / ( tu + tl );
   float yc     = rc * tu;
   float d_phi2 = MC_2_PI / d_image.Width;
   for( y = 0; y < d_image.Height; y++ ) {
      byte* l_d_image = d_image[y];
      float phi2      = 0.0f;
      for( x = 0; x < d_image.Width; x++ ) {
         float theta2 = atan( ( yc - y ) / rc ) + 0.5f * MC_PI_2;
         float r      = a * theta2;
         float fx     = fisheye_info.center.X - r * cos( phi2 );
         float fy     = fisheye_info.center.Y - r * sin( phi2 );
         int ix       = (int)fx;
         if( 0.0f <= fx && ix < ex ) {
            int iy = (int)fy;
            if( 0.0f <= fy && iy < ey ) {
               int ix1      = ix + 1;
               int iy1      = iy + 1;
               float rx     = fx - ix;
               float ry     = fy - iy;
               float ry1    = 1.0f - ry;
               float b1     = ry1 * s_image[iy][ix] + ry * s_image[iy1][ix];
               float b2     = ry1 * s_image[iy][ix1] + ry * s_image[iy1][ix1];
               l_d_image[x] = ( byte )( ( 1.0f - rx ) * b1 + rx * b2 );
            } else
               l_d_image[x] = PG_BLACK;
         } else
            l_d_image[x] = PG_BLACK;
         phi2 += d_phi2;
      }
   }
}

void ConvertFisheyeToPanTiltPos( FISHEYE_INFO& fisheye_info, FPoint2D& s_coord, float& d_pan, float& d_tilt )

{
   float dx     = fisheye_info.center.X - s_coord.X;
   float dy     = fisheye_info.center.Y - s_coord.Y;
   float r      = sqrt( dx * dx + dy * dy );
   float phi2   = atan2( dy, dx );
   float theta2 = ( MC_PI * r ) / ( 2.0f * fisheye_info.radius );
   d_pan        = MC_RAD2DEG * phi2;
   d_tilt       = MC_RAD2DEG * ( MC_PI_2 - theta2 );
}

void ConvertFisheyeToPlanarImage( FISHEYE_INFO& fisheye_info, GImage& s_image, float pan, float tilt, float f_length, GImage& d_image )

{
   int x, y;

   int ex  = s_image.Width - 1;
   int ey  = s_image.Height - 1;
   float a = 2.0f * fisheye_info.radius / MC_PI;
   Matrix mat_K( 3, 3 );
   mat_K.Clear();
   mat_K[0][0] = -fisheye_info.f_length;
   mat_K[1][1] = -fisheye_info.f_length;
   mat_K[2][2] = 1.0f;
   mat_K[0][2] = 0.5 * d_image.Width;
   mat_K[1][2] = 0.5 * d_image.Height;
   Matrix mat_Rp( 3, 3 );
   pan = MC_DEG2RAD * ( pan - 90.0f );
   GetRotationMatrix_Z( pan, mat_Rp );
   Matrix mat_Rt( 3, 3 );
   tilt = MC_DEG2RAD * ( tilt - 90.0f );
   GetRotationMatrix_X( tilt, mat_Rt );
   Matrix mat_T = Inv( mat_K * mat_Rt * mat_Rp );
   float xr     = (float)mat_T[0][2];
   float yr     = (float)mat_T[1][2];
   float zr     = (float)mat_T[2][2];
   float dxr    = (float)mat_T[0][1];
   float dyr    = (float)mat_T[1][1];
   float dzr    = (float)mat_T[2][1];
   float dpx    = (float)mat_T[0][0];
   float dpy    = (float)mat_T[1][0];
   float dpz    = (float)mat_T[2][0];
   for( y = 0; y < d_image.Height; y++ ) {
      byte* l_d_image = d_image[y];
      float px        = xr;
      float py        = yr;
      float pz        = zr;
      for( x = 0; x < d_image.Width; x++ ) {
         float d  = sqrt( px * px + py * py + pz * pz );
         float nx = px / d;
         float ny = py / d;
         float nz = pz / d;
         float r  = a * acos( nz );
         d        = sqrt( nx * nx + ny * ny );
         if( d < MC_VSV )
            r = 0.0;
         else
            r /= d;
         float fx = fisheye_info.center.X - r * nx;
         int ix   = (int)fx;
         if( 0.0f <= fx && ix < ex ) {
            float fy = fisheye_info.center.Y - r * ny;
            int iy   = (int)fy;
            if( 0.0f <= fy && iy < ey ) {
               int ix1      = ix + 1;
               int iy1      = iy + 1;
               float rx     = fx - ix;
               float ry     = fy - iy;
               float ry1    = 1.0f - ry;
               float b1     = ry1 * s_image[iy][ix] + ry * s_image[iy1][ix];
               float b2     = ry1 * s_image[iy][ix1] + ry * s_image[iy1][ix1];
               l_d_image[x] = ( byte )( ( 1.0f - rx ) * b1 + rx * b2 );
            } else
               l_d_image[x] = PG_BLACK;
         } else
            l_d_image[x] = PG_BLACK;
         px += dpx;
         py += dpy;
         pz += dpz;
      }
      xr += dxr;
      yr += dyr;
      zr += dzr;
   }
}

void ConvertFisheyeToSphericalImage( FISHEYE_INFO& fisheye_info, GImage& s_image, GImage& d_image )

{
   int x, y;

   int ex        = s_image.Width - 1;
   int ey        = s_image.Height - 1;
   float a       = 2.0f * fisheye_info.radius / MC_PI;
   float d_phi   = MC_PI / d_image.Width;
   float d_theta = MC_PI / d_image.Height;
   float theta   = MC_PI_2;
   for( y = 0; y < d_image.Height; y++ ) {
      byte* l_d_image = d_image[y];
      float cos_theta = cos( theta );
      float sin_theta = sin( theta );
      float phi       = 0.0f;
      for( x = 0; x < d_image.Width; x++ ) {
         float theta2 = acos( cos_theta * sin( phi ) );
         float r      = a * theta2;
         float dx     = cos_theta * cos( phi );
         float dy     = sin_theta;
         float d      = sqrt( dx * dx + dy * dy );
         if( d < MC_VSV )
            r = 0.0;
         else
            r /= d;
         float fx = fisheye_info.center.X - r * dx;
         int ix   = (int)fx;
         if( 0.0f <= fx && ix < ex ) {
            float fy = fisheye_info.center.Y - r * dy;
            int iy   = (int)fy;
            if( 0.0f <= fy && iy < ey ) {
               int ix1      = ix + 1;
               int iy1      = iy + 1;
               float rx     = fx - ix;
               float ry     = fy - iy;
               float ry1    = 1.0f - ry;
               float b1     = ry1 * s_image[iy][ix] + ry * s_image[iy1][ix];
               float b2     = ry1 * s_image[iy][ix1] + ry * s_image[iy1][ix1];
               l_d_image[x] = ( byte )( ( 1.0f - rx ) * b1 + rx * b2 );
            } else
               l_d_image[x] = PG_BLACK;
         } else
            l_d_image[x] = PG_BLACK;
         phi += d_phi;
      }
      theta -= d_theta;
   }
}

int ExtractCircumcircle( FISHEYE_INFO& fisheye_info, GImage& s_image, byte b_thrsld )

{
   int x, y, n;

   fisheye_info.radius = 0.0f;
   fisheye_info.center.Clear();
   // 입력 영상의 이진화를 수행한다.
   GImage t_image( s_image.Width, s_image.Height );
   Binarize( s_image, b_thrsld, t_image );
   GImage b_image( s_image.Width, s_image.Height );
   RemoveConnectedRegions( t_image, 0, 100, b_image );
   // 외접원의 바운딩 박스를 구한다.
   IPoint2D min_p( b_image.Width, b_image.Height );
   IPoint2D max_p( 0, 0 );
   n = 0;
   for( y = 0; y < b_image.Height; y++ ) {
      byte* l_e_image = b_image[y];
      for( x = 0; x < b_image.Width; x++ ) {
         if( l_e_image[x] ) {
            if( x < min_p.X ) min_p.X = x;
            if( x > max_p.X ) max_p.X = x;
            if( y < min_p.Y ) min_p.Y = y;
            if( y > max_p.Y ) max_p.Y = y;
            n++;
         }
      }
   }
   if( !n ) return ( 1 );
#if defined( __DEBUG )
   logd( "   Range: Left-Top = (%d,%d), Right-Bottom = (%d,%d)\n", min_p.X, min_p.Y, max_p.X, max_p.Y );
#endif
   // 외접원의 초기 반지름을 구한다.
   fisheye_info.radius = GetMaximum( 0.5f * ( max_p.X - min_p.X ), 0.5f * ( max_p.Y - min_p.Y ) );
   // 외접원의 바운더리 픽셀들을 구한다.
   n = (int)( 2.0f * MC_2_PI * fisheye_info.radius );
   IP2DArray1D bp_array1( n );
   int max_d = 0, max_y = 0;
   for( y = n = 0; y < b_image.Height; y++ ) {
      byte* l_b_image = b_image[y];
      for( x = 0; x < b_image.Width; x++ )
         if( l_b_image[x] ) break;
      if( x >= b_image.Width ) continue;
      bp_array1[n++]( x, y );
      int x1 = x;
      for( x = b_image.Width - 1; x >= 0; x-- )
         if( l_b_image[x] ) break;
      if( x < 0 ) continue;
      bp_array1[n++]( x, y );
      int d = x - x1;
      if( d > max_d ) {
         max_d = d;
         max_y = y;
      }
   }
   if( !n ) return ( 2 );
   // 외접원의 초기 중심 위치를 구한다.
   fisheye_info.center( 0.5f * ( min_p.X + max_p.X ), (float)max_y );
#if defined( __DEBUG )
   logd( "   Initial center and radius: Center = (%6.1f,%6.1f), Radius = %6.1f\n", fisheye_info.center.X, fisheye_info.center.Y, fisheye_info.radius );
#endif
   // 최적화를 통해 외접원의 최종 중심 위치와 반지름을 구한다.
   IP2DArray1D bp_array2 = bp_array1.Extract( 0, n );
   Minimization_Circumcircle min_cc;
   min_cc.Initialize( bp_array2, fisheye_info.center, fisheye_info.radius );
   min_cc.Perform();
   min_cc.GetParameters( fisheye_info.center );
#if defined( __DEBUG )
   logd( "   Final center and radius  : Center = (%6.1f,%6.1f), Radius = %6.1f\n", fisheye_info.center.X, fisheye_info.center.Y, fisheye_info.radius );
#endif
   return ( DONE );
}

int GetCylindricalImageHeight( int ci_width, float up_range, float lp_range )

{
   float r       = ci_width / MC_2_PI;
   float t1      = tan( MC_DEG2RAD * up_range );
   float t2      = tan( MC_DEG2RAD * lp_range );
   float h       = r * ( t1 + t2 );
   int ci_height = ( (int)( h + 0.5f ) + 3 ) / 4 * 4;
   return ( ci_height );
}

int GetCylindricalImageWidth( int ci_height, float up_range, float lp_range )

{
   float t1     = tan( MC_DEG2RAD * up_range );
   float t2     = tan( MC_DEG2RAD * lp_range );
   float r      = ci_height / ( t1 + t2 );
   float w      = MC_2_PI * r;
   int ci_width = ( (int)( w + 0.5f ) + 3 ) / 4 * 4;
   return ( ci_width );
}

void GetFOVs( int si_width, int si_height, float f_length, float& h_fov, float& v_fov )

{
   h_fov = 2.0f * MC_RAD2DEG * atan( 0.5f * si_width / f_length );
   v_fov = 2.0f * MC_RAD2DEG * atan( 0.5f * si_height / f_length );
}

void GetNewPanTiltPos( int si_width, int si_height, float c_pan, float c_tilt, float f_length, FPoint2D& s_pos, float& n_pan, float& n_tilt )

{
   Matrix mat_K( 3, 3 );
   mat_K.Clear();
   mat_K[0][0] = -f_length;
   mat_K[1][1] = -f_length;
   mat_K[2][2] = 1.0f;
   mat_K[0][2] = 0.5 * si_width;
   mat_K[1][2] = 0.5 * si_height;
   Matrix mat_Rp( 3, 3 );
   c_pan = MC_DEG2RAD * c_pan;
   GetRotationMatrix_Z( c_pan, mat_Rp );
   Matrix mat_Rt( 3, 3 );
   c_tilt = MC_DEG2RAD * ( c_tilt - 90.0f );
   GetRotationMatrix_X( c_tilt, mat_Rt );
   Matrix mat_T = Inv( mat_K * mat_Rt * mat_Rp );
   Matrix vec_x( 3 );
   vec_x( 0 )   = s_pos.X;
   vec_x( 1 )   = s_pos.Y;
   vec_x( 2 )   = 1.0;
   Matrix vec_X = Nor( mat_T * vec_x );
   n_pan        = (float)( MC_RAD2DEG * ( -atan2( vec_X( 0 ), vec_X( 1 ) ) ) );
   n_tilt       = (float)( MC_RAD2DEG * ( MC_PI_2 - acos( vec_X( 2 ) ) ) );
}

////////////////////////////////////////////////////////////////////////////////
void ConvertFisheyeToCylindricalImage_YUY2( FISHEYE_INFO& fisheye_info, byte* s_buffer, int s_width, int s_height, byte* d_buffer, int d_width, int d_height )

{
   int x, y;

   int ex       = s_width - 1;
   int ey       = s_height - 1;
   float a      = 2.0f * fisheye_info.radius / MC_PI;
   float tu     = tan( MC_DEG2RAD * fisheye_info.up_range );
   float tl     = tan( MC_DEG2RAD * fisheye_info.lp_range );
   float rc     = d_height / ( tu + tl );
   float yc     = rc * tu;
   float d_phi2 = MC_2_PI / d_width;
   int s_width2 = s_width * 2;
   int d_width2 = d_width * 2;
   for( y = 0; y < d_height; y++ ) {
      float phi2       = 0.0f;
      float theta2     = atan( ( yc - y ) / rc ) + 0.5f * MC_PI_2;
      float r          = a * theta2;
      byte* l_d_buffer = d_buffer + d_width2 * y;
      for( x = 0; x < d_width; x++ ) {
         int x2   = x * 2;
         float fx = fisheye_info.center.X - r * cos( phi2 );
         float fy = fisheye_info.center.Y - r * sin( phi2 );
         int ix   = (int)fx;
         if( 0.0f <= fx && ix < ex ) {
            int iy = (int)fy;
            if( 0.0f <= fy && iy < ey ) {
               int ix1           = ix + 1;
               int iy1           = iy + 1;
               int ix12          = ix * 2;
               int ix22          = ix1 * 2;
               float rx          = fx - ix;
               float ry          = fy - iy;
               float ry1         = 1.0f - ry;
               byte* l_s_buffer1 = s_buffer + s_width2 * iy;
               byte* l_s_buffer2 = s_buffer + s_width2 * iy1;
               float b1          = ry1 * l_s_buffer1[ix12] + ry * l_s_buffer2[ix12];
               float b2          = ry1 * l_s_buffer1[ix22] + ry * l_s_buffer2[ix22];
               l_d_buffer[x2]    = ( byte )( ( 1.0f - rx ) * b1 + rx * b2 );
               if( x & 0x0001 ) { // 타겟 위치의 X 좌표가 홀수인 경우: V
                  if( ix & 0x0001 ) { // 소스 위치의 X 좌표가 홀수인 경우
                     l_d_buffer[x2 + 1] = l_s_buffer1[ix12 + 1];
                  } else { //소스 위치의 X 좌표가 짝수인 경우
                     l_d_buffer[x2 + 1] = l_s_buffer1[ix12 - 1];
                  }
               } else { // 타겟 위치의 X 좌표가 짝수인 경우: U
                  if( ix & 0x0001 ) { // 소스 위치의 X 좌표가 홀수인 경우
                     l_d_buffer[x2 + 1] = l_s_buffer1[ix12 - 1];
                  } else {
                     l_d_buffer[x2 + 1] = l_s_buffer1[ix12 + 1];
                  }
               }
            } else {
               l_d_buffer[x2]     = 0x00;
               l_d_buffer[x2 + 1] = 0x80;
            }
         } else {
            l_d_buffer[x2]     = 0x00;
            l_d_buffer[x2 + 1] = 0x80;
         }
         phi2 += d_phi2;
      }
   }
}

void ConvertFisheyeToPlanarImage_YUY2( FISHEYE_INFO& fisheye_info, byte* s_buffer, int s_width, int s_height, float pan, float tilt, byte* d_buffer, int d_width, int d_height )

{
   int x, y;

   int ex  = s_width - 1;
   int ey  = s_height - 1;
   float a = 2.0f * fisheye_info.radius / MC_PI;
   Matrix mat_K( 3, 3 );
   mat_K.Clear();
   mat_K[0][0] = -fisheye_info.f_length;
   mat_K[1][1] = -fisheye_info.f_length;
   mat_K[2][2] = 1.0f;
   mat_K[0][2] = 0.5 * d_width;
   mat_K[1][2] = 0.5 * d_height;
   Matrix mat_Rp( 3, 3 );
   pan = MC_DEG2RAD * ( pan - 90.0f );
   GetRotationMatrix_Z( pan, mat_Rp );
   Matrix mat_Rt( 3, 3 );
   tilt = MC_DEG2RAD * ( tilt - 90.0f );
   GetRotationMatrix_X( tilt, mat_Rt );
   Matrix mat_T = Inv( mat_K * mat_Rt * mat_Rp );
   float xr     = (float)mat_T[0][2];
   float yr     = (float)mat_T[1][2];
   float zr     = (float)mat_T[2][2];
   float dxr    = (float)mat_T[0][1];
   float dyr    = (float)mat_T[1][1];
   float dzr    = (float)mat_T[2][1];
   float dpx    = (float)mat_T[0][0];
   float dpy    = (float)mat_T[1][0];
   float dpz    = (float)mat_T[2][0];

   int s_width2 = s_width * 2;
   int d_width2 = d_width * 2;
   for( y = 0; y < d_height; y++ ) {
      byte* l_d_buffer = d_buffer + d_width2 * y;
      float px         = xr;
      float py         = yr;
      float pz         = zr;
      for( x = 0; x < d_width; x++ ) {
         int x2   = x * 2;
         float d  = sqrt( px * px + py * py + pz * pz );
         float nx = px / d;
         float ny = py / d;
         float nz = pz / d;
         float r  = a * acos( nz );
         d        = sqrt( nx * nx + ny * ny );
         if( d < MC_VSV )
            r = 0.0;
         else
            r /= d;
         float fx = fisheye_info.center.X - r * nx;
         int ix   = (int)fx;
         if( 0.0f <= fx && ix < ex ) {
            float fy = fisheye_info.center.Y - r * ny;
            int iy   = (int)fy;
            if( 0.0f <= fy && iy < ey ) {
               int ix1           = ix + 1;
               int iy1           = iy + 1;
               int ix12          = ix * 2;
               int ix22          = ix1 * 2;
               float rx          = fx - ix;
               float ry          = fy - iy;
               float ry1         = 1.0f - ry;
               byte* l_s_buffer1 = s_buffer + s_width2 * iy;
               byte* l_s_buffer2 = s_buffer + s_width2 * iy1;
               float b1          = ry1 * l_s_buffer1[ix12] + ry * l_s_buffer2[ix12];
               float b2          = ry1 * l_s_buffer1[ix22] + ry * l_s_buffer2[ix22];
               l_d_buffer[x2]    = ( byte )( ( 1.0f - rx ) * b1 + rx * b2 );
               if( x & 0x0001 ) { // 타겟 위치의 X 좌표가 홀수인 경우: V
                  if( ix & 0x0001 ) { // 소스 위치의 X 좌표가 홀수인 경우
                     l_d_buffer[x2 + 1] = l_s_buffer1[ix12 + 1];
                  } else { //소스 위치의 X 좌표가 짝수인 경우
                     l_d_buffer[x2 + 1] = l_s_buffer1[ix12 - 1];
                  }
               } else { // 타겟 위치의 X 좌표가 짝수인 경우: U
                  if( ix & 0x0001 ) { // 소스 위치의 X 좌표가 홀수인 경우
                     l_d_buffer[x2 + 1] = l_s_buffer1[ix12 - 1];
                  } else {
                     l_d_buffer[x2 + 1] = l_s_buffer1[ix12 + 1];
                  }
               }
            } else {
               l_d_buffer[x2]     = 0x00;
               l_d_buffer[x2 + 1] = 0x80;
            }
         } else {
            l_d_buffer[x2]     = 0x00;
            l_d_buffer[x2 + 1] = 0x80;
         }
         px += dpx;
         py += dpy;
         pz += dpz;
      }
      xr += dxr;
      yr += dyr;
      zr += dzr;
   }
}

void ConvertFisheyeToSphericalImage_YUY2( FISHEYE_INFO& fisheye_info, byte* s_buffer, int s_width, int s_height, byte* d_buffer, int d_width, int d_height )

{
   int x, y;

   int ex        = s_width - 1;
   int ey        = s_height - 1;
   float a       = 2.0f * fisheye_info.radius / MC_PI;
   float d_phi   = MC_PI / d_width;
   float d_theta = MC_PI / d_height;
   float theta   = MC_PI_2;
   int s_width2  = s_width * 2;
   int d_width2  = d_width * 2;
   for( y = 0; y < d_height; y++ ) {
      byte* l_d_buffer = d_buffer + d_width2 * y;
      float cos_theta  = cos( theta );
      float sin_theta  = sin( theta );
      float phi        = 0.0f;
      for( x = 0; x < d_width; x++ ) {
         int x2       = x * 2;
         float theta2 = acos( cos_theta * sin( phi ) );
         float r      = a * theta2;
         float dx     = cos_theta * cos( phi );
         float dy     = sin_theta;
         float d      = sqrt( dx * dx + dy * dy );
         if( d < MC_VSV )
            r = 0.0;
         else
            r /= d;
         float fx = fisheye_info.center.X - r * dx;
         int ix   = (int)fx;
         if( 0.0f <= fx && ix < ex ) {
            float fy = fisheye_info.center.Y - r * dy;
            int iy   = (int)fy;
            if( 0.0f <= fy && iy < ey ) {
               int ix1           = ix + 1;
               int iy1           = iy + 1;
               int ix12          = ix * 2;
               int ix22          = ix1 * 2;
               float rx          = fx - ix;
               float ry          = fy - iy;
               float ry1         = 1.0f - ry;
               byte* l_s_buffer1 = s_buffer + s_width2 * iy;
               byte* l_s_buffer2 = s_buffer + s_width2 * iy1;
               float b1          = ry1 * l_s_buffer1[ix12] + ry * l_s_buffer2[ix12];
               float b2          = ry1 * l_s_buffer1[ix22] + ry * l_s_buffer2[ix22];
               l_d_buffer[x2]    = ( byte )( ( 1.0f - rx ) * b1 + rx * b2 );
               if( x & 0x0001 ) { // 타겟 위치의 X 좌표가 홀수인 경우: V
                  if( ix & 0x0001 ) { // 소스 위치의 X 좌표가 홀수인 경우
                     l_d_buffer[x2 + 1] = l_s_buffer1[ix12 + 1];
                  } else { //소스 위치의 X 좌표가 짝수인 경우
                     l_d_buffer[x2 + 1] = l_s_buffer1[ix12 - 1];
                  }
               } else { // 타겟 위치의 X 좌표가 짝수인 경우: U
                  if( ix & 0x0001 ) { // 소스 위치의 X 좌표가 홀수인 경우
                     l_d_buffer[x2 + 1] = l_s_buffer1[ix12 - 1];
                  } else {
                     l_d_buffer[x2 + 1] = l_s_buffer1[ix12 + 1];
                  }
               }
            } else {
               l_d_buffer[x2]     = 0x00;
               l_d_buffer[x2 + 1] = 0x80;
            }
         } else {
            l_d_buffer[x2]     = 0x00;
            l_d_buffer[x2 + 1] = 0x80;
         }
         phi += d_phi;
      }
      theta -= d_theta;
   }
}

void ConvertFisheyeToDoublePanoramaCoord( FISHEYE_INFO& fisheye_info, int dp_width, int dp_height, FPoint2D& s_coord, FPoint2D& d_coord )

{
   int c_width  = dp_width * 2;
   int c_height = dp_height / 2;
   ConvertFisheyeToCylindricalCoord( fisheye_info, c_width, c_height, s_coord, d_coord );

   if( d_coord.Y < 0 ) d_coord.Y        = 0;
   if( d_coord.Y > c_height ) d_coord.Y = (float)c_height;

   if( d_coord.X > dp_width ) {
      d_coord.X -= dp_width;
      d_coord.Y += c_height;
   }
}

float GetFocalLengthFromHFOV( int si_width, float h_fov )

{
   float f_length = 0.5f * si_width / tan( 0.5f * MC_DEG2RAD * h_fov );
   return ( f_length );
}

float GetFocalLengthFromVFOV( int si_height, float v_fov )

{
   float f_length = 0.5f * si_height / tan( 0.5f * MC_DEG2RAD * v_fov );
   return ( f_length );
}

void Make180DoublePanorama_YUY2( byte* si_buffer, int si_width, int si_height, byte* di_buffer )

{
   int i;

   int di_width  = si_width / 2;
   int di_height = si_height * 2;
   int si_pitch  = si_width * 2;
   int di_pitch  = di_width * 2;

   // 입력 영상의 좌측 절반을 출력 영상의 상단에 복사한다.
   byte* sp = si_buffer;
   byte* dp = di_buffer;
   for( i = 0; i < si_height; i++ ) {
      memcpy( dp, sp, di_pitch );
      sp += si_pitch;
      dp += di_pitch;
   }

   // 입력 영상의 우측 절반을 출력 영상의 하단에 복사한다.
   sp = si_buffer + di_pitch;
   dp = di_buffer + di_height / 2 * di_pitch;
   for( i = 0; i < si_height; i++ ) {
      memcpy( dp, sp, di_pitch );
      sp += si_pitch;
      dp += di_pitch;
   }
}

void Make180HalfPanorama_YUY2( byte* si_buffer, int si_width, int si_height, byte* di_buffer, int center_x )

{
   // 180도 파노라마 영상
   int i;

   int di_width   = si_width / 2;
   int di_width_h = si_width / 4;
   int di_height  = si_height * 2;
   int si_pitch   = si_width * 2;
   int di_pitch   = si_width;

   byte* sp = si_buffer;
   byte* dp = di_buffer;

   center_x -= di_width_h;
   if( center_x >= di_width_h && center_x <= ( si_width - di_width_h ) ) {
      // 파노라마의 중심이 90도에서 270도 사이에 있을 때
      int left_pitch = ( center_x - di_width_h ) * 2;

      sp += left_pitch;
      for( i = 0; i < si_height; i++ ) {
         memcpy( dp, sp, di_pitch );
         sp += si_pitch;
         dp += di_pitch;
      }
   } else {
      int left_pitch = 0;
      if( center_x >= -di_width_h && center_x < di_width_h ) {
         // 파노라마의 중심이 0도에서 90도사이에 있을 때
         left_pitch = di_width_h - center_x;
      } else if( center_x > ( si_width - di_width_h ) && center_x <= ( si_width + di_width_h ) ) {
         // 파노라마의 중심이 270도에서 360도 사이에 있을 때
         left_pitch = di_pitch - center_x + di_width + di_width_h;
      }

      ASSERT( left_pitch > 0 );
      int left_width  = si_pitch - left_pitch;
      int right_pitch = di_pitch - left_pitch;
      int right_width = si_pitch - left_width;

      for( i = 0; i < si_height; i++ ) {
         sp += left_width;
         memcpy( dp, sp, left_pitch );
         sp -= left_width;
         dp += left_pitch;
         memcpy( dp, sp, right_pitch );
         dp += right_pitch;
         sp += si_pitch;
      }
   }
}

#endif // __SUPPORT_FISHEYE_CAMERA

﻿#ifdef WIN32

#include "StdAfx.h"

#include "OnscreenDisplayEx.h"

#include "Win32CL/ImageView.h"

COnscreenDisplayEx::COnscreenDisplayEx()

{
   Width = Height = 0;
}

void COnscreenDisplayEx::DrawRectangle( CDC* dc, IBox2D& s_box, COLORREF color, int thickness, FPoint2D& scale )

{
   CPen pen( PS_SOLID, thickness, color );
   dc->SelectObject( &pen );
   dc->SelectStockObject( NULL_BRUSH );
   CRect rect;
   rect.left   = ( LONG )( s_box.X * scale.X );
   rect.top    = ( LONG )( s_box.Y * scale.Y );
   rect.right  = ( LONG )( ( s_box.X + s_box.Width ) * scale.X );
   rect.bottom = ( LONG )( ( s_box.Y + s_box.Height ) * scale.Y );
   dc->Rectangle( &rect );
   dc->SelectStockObject( NULL_PEN );
}

void COnscreenDisplayEx::DisplayEventZones( CDC* dc, EventZoneList& ez_list )

{
   if( !ez_list.GetNumItems() ) return;
   FPoint2D scale;
   scale.X             = (float)Width / ez_list.Width;
   scale.Y             = (float)Height / ez_list.Height;
   EventZone* evt_zone = (EventZone*)ez_list.First();
   while( evt_zone != NULL ) {
      DisplayEventZone( dc, evt_zone, scale );
      evt_zone = (EventZone*)ez_list.Next( evt_zone );
   }
}

void COnscreenDisplayEx::DisplayEventZone( CDC* dc, EventZone* evt_zone, FPoint2D& scale )

{
   DrawEventZone( dc, evt_zone, scale );
   if( evt_zone->EvtRule ) {
      switch( evt_zone->EvtRule->EventType ) {
      case ER_EVENT_DIRECTIONAL_MOTION:
         DrawEventZone_DirectionalMotion( dc, evt_zone, scale );
         break;
      case ER_EVENT_DWELL:
         DrawEventZone_Dwell( dc, evt_zone, scale, 0 );
         break;
      case ER_EVENT_PATH_PASSING:
         DrawEventZone_PathPassing( dc, evt_zone, scale );
         break;
      case ER_EVENT_WATER_LEVEL:
         DrawEventZone_WaterLevel( dc, evt_zone, scale );
         break;
      default:
         break;
      }
   }
}

void COnscreenDisplayEx::DisplayEventZone( CDC* dc, EventZone* evt_zone )

{
   FPoint2D scale;
   scale.X = (float)Width / REF_IMAGE_WIDTH;
   scale.Y = (float)Height / REF_IMAGE_HEIGHT;

   DisplayEventZone( dc, evt_zone, scale );
}

void COnscreenDisplayEx::DisplayPolygonalAreas( CDC* dc, PolygonalAreaList& pa_list, COLORREF color, int brush_style )

{
   int i;

   if( !pa_list.GetNumItems() ) return;
   float scale_x = (float)Width / pa_list.Width;
   float scale_y = (float)Height / pa_list.Height;

   int pen_width = 1;
   CBrush brush( brush_style, color );
   dc->SelectObject( &brush );

   CPen pen( PS_SOLID, pen_width, color );
   dc->SelectObject( &pen );
   dc->SetBkMode( TRANSPARENT );
   PolygonalArea* p_area = pa_list.First();
   while( p_area != NULL ) {
      IP2DArray1D& s_vertices = p_area->Vertices;
      IP2DArray1D d_vertices( s_vertices.Length );
      for( i = 0; i < s_vertices.Length; i++ ) {
         d_vertices[i].X = (int)( s_vertices[i].X * scale_x );
         d_vertices[i].Y = (int)( s_vertices[i].Y * scale_y );
      }
      dc->Polygon( (CPoint*)(IPoint2D*)d_vertices, d_vertices.Length );
      p_area = pa_list.Next( p_area );
   }
   dc->SelectStockObject( NULL_PEN );
   dc->SelectStockObject( NULL_BRUSH );
}

void COnscreenDisplayEx::DrawArrow( CDC* dc, IPoint2D& s_pos, float direction, float length, int pen_width, COLORREF color )

{
   int i;
   FPoint2D ah1[3];
   IPoint2D ah2[3];

   direction *= MC_DEG2RAD;
   float r00 = cos( direction );
   float r10 = sin( direction );
   float r01 = -r10;
   float r11 = r00;
   ah1[0]( length - 11.0f, -6.0f );
   ah1[1]( length, 0.0f );
   ah1[2]( length - 11.0f, 6.0f );
   length -= 11.0f;

   for( i = 0; i < 3; i++ ) {
      ah2[i].X = (int)( r00 * ah1[i].X + r01 * ah1[i].Y + s_pos.X );
      ah2[i].Y = (int)( r10 * ah1[i].X + r11 * ah1[i].Y + s_pos.Y );
   }
   CPen pen1( PS_SOLID, 1, color );
   CPen pen2( PS_SOLID, pen_width, color );
   CBrush brush( color );
   dc->SelectObject( &pen1 );
   dc->SelectObject( &brush );
   dc->Polygon( (LPPOINT)ah2, 3 );
   dc->SelectObject( &pen2 );
   dc->MoveTo( s_pos.X, s_pos.Y );
   int x2 = (int)( r00 * length + s_pos.X );
   int y2 = (int)( r10 * length + s_pos.Y );
   dc->LineTo( x2, y2 );
   dc->SelectStockObject( NULL_PEN );
   dc->SelectStockObject( NULL_BRUSH );
}

void COnscreenDisplayEx::DisplayCalibrationPoints( CDC* dc, FP2DArray1D& c_points, int sel_cp_no, FPoint2D& scale )

{
   int i;
   CString text;
   COLORREF color;

   CFont font;
   font.CreatePointFont( 85, "Tahoma" );
   dc->SelectObject( &font );
   dc->SetBkMode( TRANSPARENT );
   for( i = 0; i < c_points.Length; i++ ) {
      if( c_points[i].X != MC_VLV ) {
         if( i == sel_cp_no )
            color = PC_RED;
         else
            color = PC_GREEN;
         CPen pen( PS_SOLID, 1, color );
         dc->SelectObject( &pen );
         int x = (int)( c_points[i].X * scale.X );
         int y = (int)( c_points[i].Y * scale.Y );
         dc->MoveTo( x - 3, y );
         dc->LineTo( x + 4, y );
         dc->MoveTo( x, y - 3 );
         dc->LineTo( x, y + 4 );
         dc->SetTextColor( color );
         text.Format( "#%d", i + 1 );
         dc->TextOut( x + 3, y + 3, text );
         dc->SelectStockObject( NULL_PEN );
      }
   }
   dc->SelectStockObject( SYSTEM_FONT );
}

void COnscreenDisplayEx::DrawCounter( CDC* dc, EventZone* evt_zone, FPoint2D& scale )

{
   CPen pen( PS_SOLID, 1, RGB( 255, 255, 255 ) );
   CBrush brush( RGB( 100, 100, 100 ) );
   dc->SelectObject( &pen );
   dc->SelectObject( &brush );
   CString text;
   text.Format( "%d", evt_zone->EvtRule->ERC_Counting.Count );
   CRect rect;
   rect.left   = ( LONG )( evt_zone->Vertices[0].X * scale.X );
   rect.top    = ( LONG )( evt_zone->Vertices[0].Y * scale.Y );
   rect.right  = ( LONG )( rect.left + 6 * strlen( text ) + 5 );
   rect.bottom = ( LONG )( rect.top + 14 );
   dc->Rectangle( &rect );
   dc->SelectStockObject( NULL_PEN );
   dc->SelectStockObject( NULL_BRUSH );
   DrawText( dc, text, rect.left + 2, rect.top + 0, RGB( 255, 255, 255 ) );
}

void COnscreenDisplayEx::DrawCrossMark( CDC* dc, IPoint2D& pos, COLORREF crColor, int length, int pen_width, FPoint2D& scale )
{
   CPoint pt;
   pt.x = (int)( pos.X * scale.X );
   pt.y = (int)( pos.Y * scale.Y );

   CPen Pen, *pOldPen;
   Pen.CreatePen( PS_SOLID, pen_width, crColor );
   pOldPen = dc->SelectObject( &Pen );
   dc->MoveTo( pt.x - ( length - 1 ), pt.y );
   dc->LineTo( pt.x - 2, pt.y );
   dc->MoveTo( pt.x + 3, pt.y );
   dc->LineTo( pt.x + length, pt.y );
   dc->MoveTo( pt.x, pt.y - ( length - 1 ) );
   dc->LineTo( pt.x, pt.y - 2 );
   dc->MoveTo( pt.x, pt.y + 3 );
   dc->LineTo( pt.x, pt.y + length );
   dc->SelectObject( pOldPen );
}

void COnscreenDisplayEx::DrawEventZone( CDC* dc, EventZone* evt_zone, FPoint2D& scale )

{
   int i;

   IP2DArray1D vertices( evt_zone->Vertices.Length );
   for( i = 0; i < vertices.Length; i++ ) {
      vertices[i].X = (int)( evt_zone->Vertices[i].X * scale.X );
      vertices[i].Y = (int)( evt_zone->Vertices[i].Y * scale.Y );
   }
   int pen_width = 1;
#ifdef __SUPPORT_EVENT_ALARM_LEVEL
   CBrush* brush;
   if( ( (EventZoneEx*)evt_zone )->EventAlarmLevel == EventAlarmLevel_Caution )
      brush = new CBrush( HS_BDIAGONAL, PC_YELLOW );
   else if( ( (EventZoneEx*)evt_zone )->EventAlarmLevel == EventAlarmLevel_Warning )
      brush = new CBrush( HS_BDIAGONAL, PC_RED );
#else
   CBrush brush( HS_BDIAGONAL, PC_YELLOW );
#endif
#ifdef __SUPPORT_EVENT_ALARM_LEVEL
   dc->SelectObject( brush );
#else
   dc->SelectObject( &brush );
#endif

#ifdef __SUPPORT_EVENT_ALARM_LEVEL
   CPen* pen;
   if( ( (EventZoneEx*)evt_zone )->EventAlarmLevel == EventAlarmLevel_Caution )
      pen = new CPen( PS_SOLID, pen_width, PC_YELLOW );
   else if( ( (EventZoneEx*)evt_zone )->EventAlarmLevel == EventAlarmLevel_Warning )
      pen = new CPen( PS_SOLID, pen_width, PC_RED );
   dc->SelectObject( pen );
#else
   CPen pen( PS_SOLID, pen_width, PC_YELLOW );
   dc->SelectObject( &pen );
#endif
   dc->SetBkMode( TRANSPARENT );
   dc->Polygon( (CPoint*)(IPoint2D*)vertices, vertices.Length );
   dc->SelectStockObject( NULL_PEN );
   dc->SelectStockObject( NULL_BRUSH );
#ifdef __SUPPORT_EVENT_ALARM_LEVEL
   delete brush;
   delete pen;
#endif
}

void COnscreenDisplayEx::DrawEventZone_PathPassing( CDC* dc, EventZone* evt_zone, FPoint2D& scale )

{
   EventRuleConfig_PathPassing& ers_pp = evt_zone->EvtRule->ERC_PathPassing;

   COnscreenDisplayEx od;
   od.DrawPathZones( dc, &ers_pp.ZoneTree, scale );
   od.DrawPathArrowLine( dc, ers_pp.Arrows, ers_pp.Direction, scale );
}

void COnscreenDisplayEx::DrawEventZone_DirectionalMotion( CDC* dc, EventZone* evt_zone, FPoint2D& scale )

{
   IPoint2D s_pos( (int)( evt_zone->CenterPos.X * scale.X ), (int)( evt_zone->CenterPos.Y * scale.Y ) );
   EventRuleConfig_DirectionalMotion& ud = evt_zone->EvtRule->ERC_DirectionalMotion;
   float length                          = 40.0f;
   DrawArrow( dc, s_pos, ud.Direction, length, 5, PC_RED );
   DrawPie( dc, s_pos, ud.Direction, ud.Range, length * 0.8f, PC_RED );
}

void COnscreenDisplayEx::DrawEventZone_Dwell( CDC* dc, EventZone* evt_zone, FPoint2D& scale, int options )

{
   DrawEventZone( dc, evt_zone, scale );
}

void COnscreenDisplayEx::DrawEventZone_WaterLevel( CDC* dc, EventZone* evt_zone, FPoint2D& scale )
{
   DrawEventZone( dc, evt_zone, scale );
   EventRuleConfig_WaterLevel& erc = evt_zone->EvtRule->ERC_WaterLevel;
   if( erc.CrossingLine[0].X < 0 ) return;
   IPoint2D s_pos, e_pos;
   s_pos.X         = (int)( erc.CrossingLine[0].X * scale.X );
   s_pos.Y         = (int)( erc.CrossingLine[0].Y * scale.Y );
   e_pos.X         = (int)( erc.CrossingLine[1].X * scale.X );
   e_pos.Y         = (int)( erc.CrossingLine[1].Y * scale.Y );
   float length    = 20.0f;
   IPoint2D dv     = e_pos - s_pos;
   IPoint2D c_pos  = ( s_pos + e_pos ) / 2;
   float direction = MC_RAD2DEG * atan2( (float)dv.Y, (float)dv.X );
   switch( erc.Direction ) {
   case 0: // Up
      DrawArrow( dc, c_pos, direction - 90.0f, length, 5, PC_RED );
      break;
   case 1: // Down
      DrawArrow( dc, c_pos, direction + 90.0f, length, 5, PC_RED );
      break;
   default:
      break;
   }
   DrawLine( dc, s_pos, e_pos, PC_BLUE, 2 );
}

void COnscreenDisplayEx::DrawLine( CDC* dc, IPoint2D& s_pos, IPoint2D& e_pos, COLORREF color, int thickness, int pen_style )

{
   CPen pen( pen_style, thickness, color );
   dc->SelectObject( &pen );
   dc->MoveTo( s_pos.X, s_pos.Y );
   dc->LineTo( e_pos.X, e_pos.Y );
   dc->SelectStockObject( NULL_PEN );
}

void COnscreenDisplayEx::DrawLine( CDC* dc, IPoint2D& s_pos, IPoint2D& e_pos, COLORREF color, int thickness, FPoint2D& scale )

{
   IPoint2D s_p, e_p;
   s_p.X = (int)( s_pos.X * scale.X );
   s_p.Y = (int)( s_pos.Y * scale.Y );
   e_p.X = (int)( e_pos.X * scale.X );
   e_p.Y = (int)( e_pos.Y * scale.Y );

   DrawLine( dc, s_p, e_p, color, thickness, PS_SOLID );
}

void COnscreenDisplayEx::DrawPie( CDC* dc, IPoint2D& c_pos, float direction, float range, float radius, COLORREF color )

{
   range *= MC_DEG2RAD;
   direction *= MC_DEG2RAD;
   float angle1 = direction + 0.5f * range;
   float angle2 = angle1 - range;
   float r00    = cos( angle1 );
   float r10    = sin( angle1 );
   CPoint p1;
   p1.x = (int)( r00 * radius + c_pos.X );
   p1.y = (int)( r10 * radius + c_pos.Y );
   r00  = cos( angle2 );
   r10  = sin( angle2 );
   CPoint p2;
   p2.x = (int)( r00 * radius + c_pos.X );
   p2.y = (int)( r10 * radius + c_pos.Y );
   CRect rect;
   rect.left   = (int)( c_pos.X - radius );
   rect.top    = (int)( c_pos.Y - radius );
   rect.right  = (int)( c_pos.X + radius );
   rect.bottom = (int)( c_pos.Y + radius );
   CPen pen( PS_SOLID, 1, color );
   dc->SelectObject( &pen );
   dc->SelectStockObject( NULL_BRUSH );
   dc->Pie( &rect, p1, p2 );
   dc->SelectStockObject( NULL_PEN );
}

void COnscreenDisplayEx::DrawText( CDC* dc, const char* text, int x, int y, COLORREF color )

{
   dc->SetTextColor( color );
   dc->SetBkMode( TRANSPARENT );
   dc->TextOut( x, y, text, (int)strlen( text ) );
}

void COnscreenDisplayEx::GetBitmap( byte* s_buffer, ISize2D& size, CBitmap& d_bitmap )

{
   BGRImage s_image( size.Width, size.Height );
   ConvertYUY2ToBGR( s_buffer, s_image );
   CDC dc;
   g_csEntireScreenDC.Lock();
   HDC h_dc = GetDC( NULL );
   dc.Attach( h_dc );
   d_bitmap.CreateCompatibleBitmap( &dc, s_image.Width, s_image.Height );
   BITMAPINFO bmp_info;
   ClearArray1D( (byte*)&bmp_info, 1, sizeof( BITMAPINFO ) );
   bmp_info.bmiHeader.biSize        = sizeof( BITMAPINFOHEADER );
   bmp_info.bmiHeader.biWidth       = s_image.Width;
   bmp_info.bmiHeader.biHeight      = -s_image.Height;
   bmp_info.bmiHeader.biPlanes      = 1;
   bmp_info.bmiHeader.biBitCount    = 24;
   bmp_info.bmiHeader.biCompression = BI_RGB;
   SetDIBits( dc.m_hDC, d_bitmap, 0, s_image.Height, s_image, &bmp_info, DIB_RGB_COLORS );
   dc.Detach();
   ReleaseDC( NULL, h_dc );
   g_csEntireScreenDC.Unlock();
}

void COnscreenDisplayEx::GetImage( CBitmap& s_bitmap, BGRImage& d_image )

{
   BITMAP bmp;
   s_bitmap.GetBitmap( &bmp );
   if( bmp.bmWidth != d_image.Width || bmp.bmHeight != d_image.Height ) d_image.Create( bmp.bmWidth, bmp.bmHeight );
   BITMAPINFO bmp_info;
   ClearArray1D( (byte*)&bmp_info, 1, sizeof( BITMAPINFO ) );
   bmp_info.bmiHeader.biSize        = sizeof( BITMAPINFOHEADER );
   bmp_info.bmiHeader.biWidth       = d_image.Width;
   bmp_info.bmiHeader.biHeight      = -d_image.Height;
   bmp_info.bmiHeader.biPlanes      = 1;
   bmp_info.bmiHeader.biBitCount    = 24;
   bmp_info.bmiHeader.biCompression = BI_RGB;
   g_csEntireScreenDC.Lock();
   HDC h_dc = GetDC( NULL );
   GetDIBits( h_dc, s_bitmap, 0, d_image.Height, d_image, &bmp_info, DIB_RGB_COLORS );
   ReleaseDC( NULL, h_dc );
   g_csEntireScreenDC.Unlock();
}

void COnscreenDisplayEx::SetScreenSize( ISize2D& size )

{
   Width  = size.Width;
   Height = size.Height;
}

void COnscreenDisplayEx::DrawPathZone( CDC* dc, PathZone* path_zone, FPoint2D& scale )

{
   if( !path_zone ) return;
   int i;
   IP2DArray1D vertices( path_zone->Vertices.Length );
   for( i = 0; i < vertices.Length; i++ ) {
      vertices[i].X = (int)( path_zone->Vertices[i].X * scale.X );
      vertices[i].Y = (int)( path_zone->Vertices[i].Y * scale.Y );
   }
   CPen pen( PS_SOLID, 2, COLOR_BRORANGE );
   dc->SelectObject( &pen );
   dc->SelectStockObject( NULL_BRUSH );
   dc->SetBkMode( TRANSPARENT );
   dc->Polygon( (CPoint*)(IPoint2D*)vertices, vertices.Length );
   dc->SelectStockObject( NULL_PEN );
}

void COnscreenDisplayEx::DrawPathZones( CDC* dc, PathZone* path_zone, FPoint2D& scale )

{
   if( path_zone->Children.Length == 0 )
      DrawPathZone( dc, path_zone, scale );
   else {
      int i;
      for( i = 0; i < path_zone->Children.Length; i++ )
         DrawPathZones( dc, &path_zone->Children[i], scale );
   }
}

void COnscreenDisplayEx::DrawPathArrow( CDC* dc, IPoint2D& s_pos, IPoint2D& e_pos, int direction, COLORREF color, FPoint2D& scale )
{
   //윈도우 좌표계로 변환
   IPoint2D s_p( (int)( s_pos.X * scale.X ), (int)( s_pos.Y * scale.Y ) );
   IPoint2D e_p( (int)( e_pos.X * scale.X ), (int)( e_pos.Y * scale.Y ) );

   IPoint2D dv  = e_p - s_p;
   float angle  = MC_RAD2DEG * atan2( (float)dv.Y, (float)dv.X );
   float length = Mag( dv );
   if( direction == 0 ) // Forward
      DrawArrow( dc, s_p, angle, length, 3, color );
   else if( direction == 1 ) // Backward
      DrawArrow( dc, e_p, angle + 180.0f, length, 3, color );
   else if( direction == 2 ) { // Both
      DrawArrow( dc, s_p, angle, length, 3, color );
      DrawArrow( dc, e_p, angle + 180.0f, length, 3, color );
   }
}

void COnscreenDisplayEx::DrawPathArrowLine( CDC* dc, LinkedList<Arrow>& arrows, int direction, FPoint2D& scale )

{
   COLORREF color = PC_BLUE;
   Arrow* arrow   = arrows.First();
   while( arrow ) {
      if( direction == 0 ) {
         if( arrow->Next )
            DrawLine( dc, ( *arrow )[0], ( *arrow )[1], color, 3, scale );
         else
            DrawPathArrow( dc, ( *arrow )[0], ( *arrow )[1], 0, color, scale );
      } else if( direction == 1 ) {
         if( arrow->Prev )
            DrawLine( dc, ( *arrow )[0], ( *arrow )[1], color, 3, scale );
         else
            DrawPathArrow( dc, ( *arrow )[0], ( *arrow )[1], 1, color, scale );
      } else if( direction == 2 ) {
         if( arrow->Prev && arrow->Next )
            DrawLine( dc, ( *arrow )[0], ( *arrow )[1], color, 3, scale );
         else {
            if( !arrow->Prev ) DrawPathArrow( dc, ( *arrow )[0], ( *arrow )[1], 1, color, scale );
            if( !arrow->Next ) DrawPathArrow( dc, ( *arrow )[0], ( *arrow )[1], 0, color, scale );
         }
      }
      arrow = arrows.Next( arrow );
   }
}

void COnscreenDisplayEx::DrawPoint( CDC* dc, IPoint2D& pos, COLORREF crColor, int radius, FPoint2D& scale )
{
   //윈도우 좌표계로 변환
   CPoint p( (int)( pos.X * scale.X ), (int)( pos.Y * scale.Y ) );
   CRect rect( p, p );
   rect.InflateRect( radius, radius );

   CBrush brush( crColor );
   dc->SelectObject( &brush );
   CPen pen( PS_SOLID, 1, crColor );
   dc->SelectObject( &pen );
   dc->SetBkMode( TRANSPARENT );
   dc->Ellipse( rect );
   dc->SelectStockObject( NULL_PEN );
   dc->SelectStockObject( NULL_BRUSH );
}

#endif // WIN32

﻿#include <float.h>
#include "StdAfx.h"
#include "version.h"

#if defined( __linux )
// socket
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
// ~socket
#endif

BOOL g_bDebugMemory    = FALSE;
float g_bTestFPSFactor = 1.0f;
#ifdef WIN32
CCriticalSection g_csEntireScreenDC; // Screen DC 접근에 대한 임계영역 처리.
#endif

///////////////////////////////////////////////////////////////////////////////
// Math
///////////////////////////////////////////////////////////////////////////////
#if _MSC_VER == 1600
float log2( float x )
{
   return log( x ) * MC_LOG2_INV;
}
#endif

float GetAngleDiff( float fAngleDeg1, float fAngleDeg2 )
{
   fAngleDeg1          = fmod( fAngleDeg1, 360.0f );
   fAngleDeg2          = fmod( fAngleDeg2, 360.0f );
   float fAngleDiffDeg = fAngleDeg2 - fAngleDeg1;
   if( fAngleDiffDeg > 180.0f ) fAngleDiffDeg = 360.0f - fAngleDiffDeg;
   if( fAngleDiffDeg < -180.0f ) fAngleDiffDeg = 360.0f + fAngleDiffDeg;
   return fabs( fAngleDiffDeg );
}

float gfxGetAngle( const POINT& p1, const POINT& p2 )
{
   int dx = ( p2.x - p1.x );
   int dy = ( p1.y - p2.y );

   float theta = (float)( MC_RAD2DEG * atan( ( dx / (double)dy ) ) );
   if( dy < 0 ) theta += 180;
   if( dy > 0 ) theta += 360;
   if( theta >= 360.0f )
      theta -= 360.0f;
   if( theta < 0.0f ) {
      theta += 360.0f;
   }
   return theta;
}

int Get8Direction( const POINT& p1, const POINT& p2 )
{
   int direction;
   float angle = gfxGetAngle( p1, p2 );
   if( 22.5f < angle && angle <= 67.5f )
      direction = EIGHT_DIRECTION_RIGHTUP;
   else if( 67.5f < angle && angle <= 112.5f )
      direction = EIGHT_DIRECTION_RIGHT;
   else if( 112.5f < angle && angle <= 157.5f )
      direction = EIGHT_DIRECTION_RIGHTDOWN;
   else if( 157.5f < angle && angle <= 202.5f )
      direction = EIGHT_DIRECTION_DOWN;
   else if( 202.5f < angle && angle <= 247.5f )
      direction = EIGHT_DIRECTION_LEFTDOWN;
   else if( 247.5f < angle && angle <= 292.5f )
      direction = EIGHT_DIRECTION_LEFT;
   else if( 292.5f < angle && angle <= 337.5f )
      direction = EIGHT_DIRECTION_LEFTUP;
   else
      direction = EIGHT_DIRECTION_UP;
   return direction;
}

float GetOffsetAngleInDegree( float fCurAngle, float fTgtAngle )
{
   // 범위를 -180도 ~ +180도 로 변경한다.
   float fOffset = 0.0f;
   float fDiff   = fTgtAngle - fCurAngle;
   if( fDiff > 180.0f )
      fOffset = fDiff - 360.0f;
   else if( fDiff < -180.0f )
      fOffset = 360.0f + fDiff;
   else
      fOffset = fDiff;
   return fOffset;
}

BOOL IsAngleContainedSE( float fStartAngleDeg, float fEndAngleDeg, float fCurPosAngleDeg )
{
   fStartAngleDeg  = fmod( fStartAngleDeg, 360.0f );
   fEndAngleDeg    = fmod( fEndAngleDeg, 360.0f );
   fCurPosAngleDeg = fmod( fCurPosAngleDeg, 360.0f );

   if( fStartAngleDeg > 180.0f ) fStartAngleDeg = 360.0f - fStartAngleDeg;
   if( fEndAngleDeg > 180.0f ) fEndAngleDeg = 360.0f - fEndAngleDeg;
   if( fCurPosAngleDeg > 180.0f ) fCurPosAngleDeg = 360.0f - fCurPosAngleDeg;

   if( fStartAngleDeg < -180.0f ) fStartAngleDeg = 360.0f + fStartAngleDeg;
   if( fEndAngleDeg < -180.0f ) fEndAngleDeg = 360.0f + fEndAngleDeg;
   if( fCurPosAngleDeg < -180.0f ) fCurPosAngleDeg = 360.0f + fCurPosAngleDeg;

   // 부호가 같으면
   if( fStartAngleDeg * fEndAngleDeg > 0.0f ) {
      if( fStartAngleDeg <= fCurPosAngleDeg && fCurPosAngleDeg <= fEndAngleDeg ) return TRUE;
      if( fEndAngleDeg <= fCurPosAngleDeg && fCurPosAngleDeg <= fStartAngleDeg ) return TRUE;
   }
   // 부호가 다르면
   else {
      float fPosAngleDeg, fNegativeAngleDeg;
      if( fStartAngleDeg > 0.0f ) {
         fPosAngleDeg      = fStartAngleDeg;
         fNegativeAngleDeg = fEndAngleDeg;
      } else {
         fPosAngleDeg      = fEndAngleDeg;
         fNegativeAngleDeg = fStartAngleDeg;
      }
      if( fPosAngleDeg > fCurPosAngleDeg )
         return TRUE;
      if( fNegativeAngleDeg < fCurPosAngleDeg )
         return TRUE;
   }
   return FALSE;
}

BOOL IsAngleContainedCR( float fCenterAngelDeg, float fRangeAngleDeg, float fCurPosAngleDeg )
{
   float fHRangeAngleDeg = fRangeAngleDeg * 0.5f;
   if( IsAngleContainedSE( fCenterAngelDeg, fCenterAngelDeg + fHRangeAngleDeg, fCurPosAngleDeg ) )
      return TRUE;
   if( IsAngleContainedSE( fCenterAngelDeg, fCenterAngelDeg - fHRangeAngleDeg, fCurPosAngleDeg ) )
      return TRUE;
   return FALSE;
}

float GetSquaredDistance( ILine2D& line, IPoint2D& point )
{
   IPoint2D _m = line[1] - line[0]; // m : line direction
   FPoint2D m( float( _m.X ), float( _m.Y ) );
   IPoint2D _diff = point - line[0];
   FPoint2D diff( float( _diff.X ), float( _diff.Y ) );
   float t = m.X * diff.X + m.Y * diff.Y;
   if( t > 0.0f ) {
      float dot_mm = m.X * m.X + m.Y * m.Y;
      if( t < dot_mm ) {
         t    = t / dot_mm;
         diff = diff - t * m;
      } else {
         t    = 1.0f;
         diff = diff - m;
      }
   } else {
      t = 0.0f;
   }
   return diff.X * diff.X + diff.Y * diff.Y;
}

///////////////////////////////////////////////////////////////////////////////
// Drawing
///////////////////////////////////////////////////////////////////////////////

void SetColor( BGRImage& img, BGRPixel pixel )
{
   int i, j;
   for( i = 0; i < img.Height; i++ ) {
      for( j = 0; j < img.Width; j++ ) {
         img[i][j] = pixel;
      }
   }
}

void InterlacingEventOddField( BYTE* s_image, BYTE* d_image, int nWidth, int nHeight )
{
   int i;

   int hh = nHeight / 2;
   int n  = nWidth * 2;
   for( i = 0; i < hh; i++ )
      memcpy( (void*)( d_image + ( ( i * n ) * 2 ) ), (const void*)( s_image + ( i * n ) ), n );
   for( i = hh; i < nHeight; i++ )
      memcpy( (void*)( d_image + ( ( ( i - hh ) * n ) ) * 2 + n ), (const void*)( s_image + ( i * n ) ), n );
}

void ExpandOddField( BYTE* s_image, BYTE* d_image, int nWidth, int nHeight )
{
   int i;

   int hh = nHeight / 2;
   int n  = nWidth * 2;

   for( i = 0; i < hh; i++ ) {
      memcpy( (void*)( d_image + ( ( i * n ) * 2 ) ), (const void*)( s_image + ( i * n ) ), n );
      memcpy( (void*)( d_image + ( ( i * n ) * 2 + n ) ), (const void*)( s_image + ( i * n ) ), n );
   }
}

void ExpandOddField( BYTE* image, int nWidth, int nHeight )
{
   int i;

   int hh = nHeight / 2;
   int n  = nWidth * 2;
   for( i = hh - 1; i >= 0; i-- ) {
      memcpy( (void*)( image + ( ( i * n ) * 2 ) ), (const void*)( image + ( i * n ) ), n );
      memcpy( (void*)( image + ( ( i * n ) * 2 + n ) ), (void*)( image + ( ( i * n ) * 2 ) ), n );
   }
}

//y_val:0xFF(흰색) y_val:0x00(검정)
void BlackOut( BArray1D& data, BYTE y_val )
{
   for( int i = 0; i < data.Length; i++ ) {
      if( !( i % 2 ) )
         memset( &data[i], y_val, 1 );
      else
         memset( &data[i], 0x80, 1 );
   }
}

void BlackOut( byte* data, int len, BYTE y_val )
{
   for( int i = 0; i < len; i++ ) {
      if( !( i % 2 ) )
         memset( &data[i], y_val, 1 );
      else
         memset( &data[i], 0x80, 1 );
   }
}

FPoint2D GetCenterPos( FBox2D fbox2d )
{
   FPoint2D center_pos;
   center_pos.X = fbox2d.X + fbox2d.Width * 0.5f;
   center_pos.Y = fbox2d.Y + fbox2d.Height * 0.5f;
   return center_pos;
}

IPoint2D GetCenterPos( IBox2D ibox2d )
{
   IPoint2D center_pos;
   center_pos.X = int( ibox2d.X + ibox2d.Width * 0.5f + 0.5f );
   center_pos.Y = int( ibox2d.Y + ibox2d.Height * 0.5f + 0.5f );
   return center_pos;
}
#ifdef WIN32
BOOL gfxGetBitmap( byte* s_image, int width, int height, CBitmap& bitmap, int bitmap_width, int bitmap_height )
{
   CClientDC dc( NULL );
   if( !bitmap.CreateCompatibleBitmap( &dc, bitmap_width, bitmap_height ) )
      return FALSE;

   BITMAPINFO bmpInfo;
   ZeroMemory( &bmpInfo, sizeof( BITMAPINFO ) );
   bmpInfo.bmiHeader.biSize        = sizeof( BITMAPINFOHEADER );
   bmpInfo.bmiHeader.biWidth       = width;
   bmpInfo.bmiHeader.biHeight      = -height;
   bmpInfo.bmiHeader.biPlanes      = 1;
   bmpInfo.bmiHeader.biBitCount    = 24;
   bmpInfo.bmiHeader.biCompression = BI_RGB;

   if( width == bitmap_width && height == bitmap_height )
      SetDIBits( dc.m_hDC, bitmap, 0, height, s_image, &bmpInfo, DIB_RGB_COLORS );
   else {
      CDC mem_dc;
      CBitmap* pOldBitmap;
      if( !mem_dc.CreateCompatibleDC( &dc ) ) return FALSE;
      pOldBitmap = mem_dc.SelectObject( &bitmap );
      SetStretchBltMode( mem_dc.m_hDC, COLORONCOLOR );
      ::StretchDIBits( mem_dc.m_hDC, 0, 0, bitmap_width, bitmap_height, 0, 0, width, height, s_image,
                       (BITMAPINFO*)&bmpInfo, DIB_RGB_COLORS, SRCCOPY );

      mem_dc.SelectObject( pOldBitmap );
      mem_dc.DeleteDC();
   }
   return TRUE;
}

BOOL gfxGetBitmap( BGRImage& s_image, CBitmap& d_bitmap, int d_width, int d_height, LPCSTR szDescText )
{
   d_bitmap.DeleteObject();

   CClientDC dc( NULL );
   CBitmap* pOldBitmap;
   CDC memDC;
   if( !memDC.CreateCompatibleDC( &dc ) ) return FALSE;
   if( !d_bitmap.CreateCompatibleBitmap( &dc, d_width, d_height ) ) return FALSE;

   pOldBitmap = memDC.SelectObject( &d_bitmap );

   BITMAPINFO bmpInfo;
   ZeroMemory( &bmpInfo, sizeof( BITMAPINFO ) );
   bmpInfo.bmiHeader.biSize        = sizeof( BITMAPINFOHEADER );
   bmpInfo.bmiHeader.biWidth       = s_image.Width;
   bmpInfo.bmiHeader.biHeight      = -s_image.Height;
   bmpInfo.bmiHeader.biPlanes      = 1;
   bmpInfo.bmiHeader.biBitCount    = 24;
   bmpInfo.bmiHeader.biCompression = BI_RGB;

   SetStretchBltMode( memDC.m_hDC, COLORONCOLOR );
   ::StretchDIBits( memDC.m_hDC, 0, 0,
                    d_width, d_height,
                    0, 0,
                    s_image.Width,
                    s_image.Height,
                    s_image,
                    (BITMAPINFO*)&bmpInfo,
                    DIB_RGB_COLORS,
                    SRCCOPY );

   if( szDescText ) {
      CFont font, *pOldFont;
      font.CreatePointFont( d_height - 10, "Tahoma" );
      pOldFont = memDC.SelectObject( &font );
      memDC.SetBkMode( TRANSPARENT );
      memDC.SetTextColor( COLOR_RED );
      memDC.DrawText( szDescText, CRect( 0, 0, d_width, d_height ), DT_CENTER | DT_VCENTER | DT_SINGLELINE );
      memDC.SelectObject( pOldFont );
   }
   memDC.SelectObject( pOldBitmap );
   memDC.DeleteDC();

   return TRUE;
}

BOOL gfxCreateFont( LPCSTR szFontName, int nHeight, BOOL bBold, CFont& fontOut )
{
   CFont font;
   font.CreatePointFont( 100, szFontName );

   LOGFONT logfont;
   ZeroMemory( &logfont, sizeof( logfont ) );
   font.GetLogFont( &logfont );
   logfont.lfHeight = nHeight;
   if( bBold ) logfont.lfWeight = FW_BOLD;
   return fontOut.CreateFontIndirect( &logfont );
}
#endif

///////////////////////////////////////////////////////////////////////////////
// Directory & File Managument
///////////////////////////////////////////////////////////////////////////////
#ifdef WIN32
#include <io.h>
#else
#include <unistd.h>
#include <sys/stat.h>
#endif

bool IsExistFile( const std::string& szFileName )
{
#ifdef WIN32
   _finddata_t c_file;
   intptr_t hFile;
   bool result = false;

   if( ( hFile = _findfirst( szFileName.c_str(), &c_file ) ) == -1L ) {
      result = false; // 파일 없으면 거짓 반환
   } else {
      if( c_file.attrib & _A_SUBDIR )
         result = 0; // 있어도 디렉토리면 거짓
      else
         result = true; // 그밖의 경우는 "존재하는 파일"이기에 참
   }

   _findclose( hFile );

   return result;
#else
   bool result = false;

   if( -1 != access( szFileName.c_str(), 0 ) )
      result = true;

   return result;
#endif
}

bool IsExistDirectory( const std::string& szDirName )
{
#ifdef WIN32
   _finddata_t c_file;
   intptr_t hFile;
   bool result = false;

   hFile = _findfirst( szDirName.c_str(), &c_file );
   if( c_file.attrib & _A_SUBDIR ) result = true;
   _findclose( hFile );

   return result;
#else
   bool result = false;
   struct stat sb;

   if( 0 == stat( szDirName.c_str(), &sb ) && S_ISDIR( sb.st_mode ) )
      result = true;
   else
      result = false;

   return result;
#endif
}

bool MakeDirectory( const char* szDirName )
{
   std::string szTemp = szDirName;
   return MakeDirectory( szTemp );
}

bool MakeDirectory( const std::string& szDirName )
{
#ifdef WIN32
   CreateDirectory( szDirName.c_str(), NULL );
#else
   mkdir( szDirName.c_str(), 0755 );
#endif
   return true;
}

std::string GetExecutablePath()
{
#ifdef WIN32
   char szPath[256] = {
      0,
   };
   GetModuleFileName( NULL, szPath, sizeof( szPath ) );

   char* pPos = _tcsrchr( szPath, _T( '\\' ) );
   *pPos      = NULL;

   std::string strRet = szPath;
   strRet.push_back( _T( '\\' ) );

   return strRet;
#else
   std::array<char, 4096> pwd;
   getcwd( pwd.data(), pwd.size() );
   strcat( pwd.data(), "/" );
   return pwd.data();
#endif
}

bool RemoveFileEx( const std::string& szFileName )
{
#ifdef WIN32
   if( DeleteFile( szFileName.c_str() ) )
      return true;

   return false;
#else
   int nRet = remove( szFileName.c_str() );
   if( 0 == nRet )
      return true;

   return false;
#endif
}

bool MoveFileEx( const std::string& szSrc, const std::string& szDst )
{
#ifdef WIN32
   if( MoveFile( szSrc.c_str(), szDst.c_str() ) )
      return true;

   return false;
#else
   int nRet = rename( szSrc.c_str(), szDst.c_str() );
   if( 0 == nRet )
      return true;

   return false;
#endif
}

bool IsExistFile( const char* szFileName )
{
   bool result = false;

   std::string str = szFileName;
   result          = IsExistFile( str );

   return result;
}

#ifdef WIN32
CString GetDirPathFromFullPath( LPCSTR pszPathName )
{
   CString srtDirPath( pszPathName );
   int nIndex = srtDirPath.ReverseFind( '\\' );
   return srtDirPath.Left( nIndex );
}

BOOL SetCurrentDirectoryWhenEmpty( CString& strDirName )
{
   if( strDirName.IsEmpty() ) {
      char szCurDir[MAX_PATH];
      GetCurrentDirectory( MAX_PATH, szCurDir );
      strDirName = szCurDir;
      return TRUE;
   }
   return FALSE;
}
#endif

///////////////////////////////////////////////////////////////////////////////
// Time Management
///////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////

void AfxGetLocalTime( LPSYSTEMTIME lpSystemTime )
{
#ifdef WIN32
   GetLocalTime( lpSystemTime );
#else
   BCCL::Time time;
   time.GetLocalTime();

   FILETIME fm;
   BCCL::Convert( time, fm );
   BCCL::Convert( fm, *lpSystemTime );
#endif
}

void AfxGetLocalFileTime( LPFILETIME lpFileTime )
{
#ifdef WIN32
   SYSTEMTIME st;
   AfxGetLocalTime( &st );
   SystemTimeToFileTime( &st, lpFileTime );
#else
   BCCL::Time time;
   time.GetLocalTime();
   BCCL::Convert( time, *lpFileTime );
#endif
}

// Getting String from Time
void GetString( std::string& strOutput, FILETIME& ft )
{
#ifdef WIN32
   SYSTEMTIME st;
   FileTimeToSystemTime( &ft, &st );
   strOutput = sutil::sformat( "%04d/%02d/%02d %02d:%02d:%02d %03d", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds );
#else
   BCCL::Time time;
   BCCL::Convert( ft, time );
   strOutput = sutil::sformat( "%04d/%02d/%02d %02d:%02d:%02d %03d", time.Year, time.Month, time.Day, time.Hour, time.Minute, time.Second, time.Millisecond );
#endif
}
#if defined( __SUPPORT_CTIME )
void GetString( std::string& strOutput, CTime& tm )
{
   SYSTEMTIME st;
   tm.GetAsSystemTime( st );
   strOutput = sutil::sformat( "%04d/%02d/%02d %02d:%02d:%02d ", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond );
}
#endif
void GetString( std::string& strOutput, SYSTEMTIME& st )
{
   strOutput = sutil::sformat( "%04d/%02d/%02d %02d:%02d:%02d %03d %01d", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds, st.wDayOfWeek );
}

std::string GetString( FILETIME& ft )
{
   std::string strOutput;
   SYSTEMTIME st;
#ifdef WIN32
   FileTimeToSystemTime( &ft, &st );
#else
   BCCL::Convert( ft, st );
#endif
   strOutput = sutil::sformat( "%04d/%02d/%02d %02d:%02d:%02d %03d", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds );
   return strOutput;
}
#if defined( __SUPPORT_CTIME )
std::string GetString( CTime& tm )
{
   std::string strOutput;
   SYSTEMTIME st;
   tm.GetAsSystemTime( st );
   strOutput = sutil::sformat( "%04d%02d%02d %02d%02d%02d ", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond );
   return strOutput;
}
#endif
std::string GetString( SYSTEMTIME& st )
{
   std::string strOutput;
   strOutput = sutil::sformat( "%04d%02d%02d %02d%02d%02d %03d %01d", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds, st.wDayOfWeek );
   return strOutput;
}
#if defined( __SUPPORT_CTIME )
DWORD GetSecondsFromTimeSpan( CTimeSpan& ts )
{
   return DWORD( ts.GetDays() * 86400 + ts.GetHours() * 3600 + ts.GetMinutes() * 60 + ts.GetSeconds() );
}
#endif
int GetElapsedMilliSec( FILETIME& ft1, FILETIME& ft2 )
{
   INT64 nDiff   = 0;
   INT64& nFt1   = *( (INT64*)&ft1 );
   INT64& nFt2   = *( (INT64*)&ft2 );
   nDiff         = nFt2 - nFt1;
   int dwElapsed = (long)( nDiff / 10000 );
   return dwElapsed;
}

int GetElapsedMilliSec( FILETIME& ftPeriod )
{
   INT64& nPeriod = *( (INT64*)&ftPeriod );
   int dwElapsed  = (long)( nPeriod / 10000 );
   return dwElapsed;
}

int GetElapsedSec( FILETIME& ft1, FILETIME& ft2 )
{
   INT64 nDiff   = 0;
   INT64& nFt1   = *( (INT64*)&ft1 );
   INT64& nFt2   = *( (INT64*)&ft2 );
   nDiff         = nFt2 - nFt1;
   int dwElapsed = (long)( nDiff / 10000000 );
   return dwElapsed;
}

int GetElapsedSec( FILETIME& ftPeriod )
{
   INT64& nPeriod = *( (INT64*)&ftPeriod );
   int dwElapsed  = (long)( nPeriod / 10000000 );
   return dwElapsed;
}

// Add To Time Format
void AddToFileTime( FILETIME& ft, int nDays, int nHours, int nMins, int nSecs, int nMiliSecs, int nMicro )
{
   UINT64& nftTime = ( *(UINT64*)&ft );
   UINT64 n64Days, n64Hours, n64Mins, n64Secs, n64MiliSecs, n64MicroSecs;
   n64Days  = (INT64)nDays * 864000000000;
   n64Hours = (INT64)nHours * 36000000000;
   n64Mins  = (INT64)nMins * 600000000;
   n64Secs  = (INT64)nSecs * 10000000;
   // 1 milli = 1/1000 sec, 1 nano = 1/1000,000,000, 100 nano = 10,000 filtime unit
   n64MiliSecs = (INT64)nMiliSecs * 10000;
   // 1 micro = 1/1000,000 sec, 1 nano = 1/1000,000,000, 100 nano = 10,000
   n64MicroSecs = (INT64)nMicro * 10;
   nftTime += n64Days + n64Hours + n64Mins + n64Secs + n64MiliSecs + n64MicroSecs;
}
#if defined( __SUPPORT_CTIME )
void CTimeToFileTime( CTime& tm, FILETIME& ft )
{
   SYSTEMTIME st;
   tm.GetAsSystemTime( st );
   BCCL::Convert( st, ft );
}

FILETIME CTimeToFileTime( CTime& tm )
{
   FILETIME ft;
   CTimeToFileTime( tm, ft );
   return ft;
}
#endif
void SetFileTime( FILETIME& ft, WORD wYear, WORD wMonth, WORD wDay, WORD wHour, WORD wMinute, WORD wSecond, WORD wMilliseconds, WORD wMicroSeconds )
{
   SYSTEMTIME st;
   st.wYear         = wYear;
   st.wMonth        = wMonth;
   st.wDay          = wDay;
   st.wHour         = wHour;
   st.wMinute       = wMinute;
   st.wSecond       = wSecond;
   st.wMilliseconds = wMilliseconds;
#ifdef WIN32
   SystemTimeToFileTime( &st, &ft );
   INT64* nFt = (INT64*)&ft;
   ( *nFt ) += wMilliseconds;
#else
   Convert( st, ft );
#endif
}

void SetFileTimeByMilliSec( FILETIME& ft, int nMilliSec )
{
   UINT64& nftTime = ( *(UINT64*)&ft );
   nftTime         = UINT64( nMilliSec ) * 10000;
}

BOOL IsNullFileTime( FILETIME& ft )
{
   if( !ft.dwHighDateTime && !ft.dwLowDateTime )
      return TRUE;
   else
      return FALSE;
}

BOOL IsLeapYear( WORD wYear )
{
   //  Check for leap year and set the number of days in the month
   return ( ( wYear & 3 ) == 0 ) && ( ( wYear % 100 ) != 0 || ( wYear % 400 ) == 0 );
}

int64_t FrameTimeToTimeStamp( AVRational StrmTimeBase, LPFILETIME lpftFrameTime, LPFILETIME lpftBase /*= NULL*/ )
{
   int64_t frametime = 0LL;

   // 베이스가 존재하는 경우 그 값을 뺀다.
   if( lpftBase ) {
      int64_t frame_time = *(int64_t*)lpftFrameTime;
      int64_t base_time  = *(int64_t*)lpftBase;
      frametime          = frame_time - base_time;
   }

   // 한 프레임에 해당하는 시간을 구한다.
   double dbOneFrameDuration = av_q2d( StrmTimeBase );

   // 프레임 타임을 타임 베이스에 맞게 변환 한다.
   int64_t frametime_us = frametime / 10;
   int64_t frametime2   = ( int64_t )( ( frametime_us / ( dbOneFrameDuration * AV_TIME_BASE ) + 0.5f ) );

   return frametime2;
}

WORD GetDaysInMonth( SYSTEMTIME& st )
{
   static int cDaysInMonth[] = {
      31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
   };
   if( IsLeapYear( st.wYear ) && ( st.wMonth == 2 ) )
      return 29;
   else
      return cDaysInMonth[st.wMonth - 1];
}

WORD GetDaysInMonth( const BCCL::Time& tm )
{
   static int cDaysInMonth[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
   if( IsLeapYear( tm.Year ) && ( tm.Month == 2 ) )
      return 29;
   else
      return cDaysInMonth[tm.Month - 1];
}

WORD GetDaysOfYear( const BCCL::Time& tm )
{
   int nMonth = tm.Month;
   int i;
   int nTotalDays = 0;
   for( i = 1; i < nMonth; i++ ) {
      BCCL::Time _tm;
      _tm.Year   = tm.Year;
      _tm.Day    = tm.Day;
      _tm.Hour   = tm.Hour;
      _tm.Minute = tm.Minute;
      _tm.Second = tm.Second;
      nTotalDays += GetDaysInMonth( _tm );
   }
   nTotalDays += tm.Day;
   return nTotalDays;
}

// Time Compare Functions
BOOL operator<( const FILETIME& ftA, const FILETIME& ftB )
{
   UINT64& nA = ( *(UINT64*)&ftA );
   UINT64& nB = ( *(UINT64*)&ftB );
   return nA < nB;
}

BOOL operator<=( const FILETIME& ftA, const FILETIME& ftB )
{
   UINT64& nA = ( *(UINT64*)&ftA );
   UINT64& nB = ( *(UINT64*)&ftB );
   return nA <= nB;
}

BOOL operator>( const FILETIME& ftA, const FILETIME& ftB )
{
   UINT64& nA = ( *(UINT64*)&ftA );
   UINT64& nB = ( *(UINT64*)&ftB );
   return nA > nB;
}

BOOL operator>=( const FILETIME& ftA, const FILETIME& ftB )
{
   UINT64& nA = ( *(UINT64*)&ftA );
   UINT64& nB = ( *(UINT64*)&ftB );
   return nA >= nB;
}

BOOL operator==( const FILETIME& ftA, const FILETIME& ftB )
{
   UINT64& nA = ( *(UINT64*)&ftA );
   UINT64& nB = ( *(UINT64*)&ftB );
   return nA == nB;
}

BOOL operator!=( const FILETIME& ftA, const FILETIME& ftB )
{
   UINT64& nA = ( *(UINT64*)&ftA );
   UINT64& nB = ( *(UINT64*)&ftB );
   return nA != nB;
}

FILETIME operator-( const FILETIME& ftA, const FILETIME& ftB )
{
   UINT64& nA     = ( *(UINT64*)&ftA );
   UINT64& nB     = ( *(UINT64*)&ftB );
   UINT64 nResult = ( nA - nB );
   return *( (FILETIME*)&nResult );
}

FILETIME operator+( const FILETIME& ftA, const FILETIME& ftB )
{
   UINT64& nA     = ( *(UINT64*)&ftA );
   UINT64& nB     = ( *(UINT64*)&ftB );
   UINT64 nResult = ( nA + nB );
   return *( (FILETIME*)&nResult );
}

FILETIME operator+=( FILETIME& ftA, FILETIME& ftB )
{
   UINT64& nA = ( *(UINT64*)&ftA );
   UINT64& nB = ( *(UINT64*)&ftB );
   nA += nB;
   return ftA;
}

FILETIME operator-=( FILETIME& ftA, FILETIME& ftB )
{
   UINT64& nA = ( *(UINT64*)&ftA );
   UINT64& nB = ( *(UINT64*)&ftB );
   nA -= nB;
   return ftA;
}

FILETIME operator-( FILETIME& ftA )
{
   INT64& nA     = ( *(INT64*)&ftA );
   INT64 nResult = -nA;
   return *( (FILETIME*)&nResult );
}

FILETIME operator*( FILETIME& ft, float fa )
{
   INT64 nA = ( *(INT64*)&ft );
   nA       = INT64( double( nA ) * double( fa ) );
   return *( (FILETIME*)&nA );
}

FILETIME operator/( FILETIME& ft, float fa )
{
   INT64 nA = ( *(INT64*)&ft );
   nA       = INT64( double( nA ) / double( fa ) );
   return *( (FILETIME*)&nA );
}

#define NSPERSEC 10000000LL

static ULONGLONG g_ullOffsetFrom1601To1970 = 0;
void CalcOffsetFrom1601To1970()
{
   SYSTEMTIME bp;
   if ( g_ullOffsetFrom1601To1970 == 0 ) {
      //bp.wYear        = 1970;
      //bp.wMonth       = 1;
      //bp.wDay         = 1;
      //bp.wDayOfWeek   = 4;
      //bp.wHour        = 0;
      //bp.wMinute      = 0;
      //bp.wSecond      = 0;
      //bp.wMilliseconds = 0;

      //SystemTimeToFileTime(&bp, (FILETIME *)&g_ullOffsetFrom1601To1970);
      //FileTimeToLocalFileTime((FILETIME *)&g_ullOffsetFrom1601To1970, (FILETIME *)&g_ullOffsetFrom1601To1970);
      // jun: pre calcurated in windows, it does not need 
      g_ullOffsetFrom1601To1970 = 116445060000000000;
   }
}


void FileTimeToTimeval(FILETIME *pFileTime, struct timeval *pTimeval)
{
   CalcOffsetFrom1601To1970();
   ULONGLONG ul64FileTime = 0;

   ul64FileTime |= pFileTime->dwHighDateTime;
   ul64FileTime <<= 32;
   ul64FileTime |= pFileTime->dwLowDateTime;

   ul64FileTime -= g_ullOffsetFrom1601To1970;
   pTimeval->tv_sec = (long)(ul64FileTime / 10000000);
   pTimeval->tv_usec = (long)((ul64FileTime % 10000000) / 10);
}

void TimevalToFileTime(struct timeval *pTimeval, FILETIME *pFileTime)
{
   CalcOffsetFrom1601To1970();
   ULONGLONG x = pTimeval->tv_sec * NSPERSEC + pTimeval->tv_usec * 10;
   x += g_ullOffsetFrom1601To1970;
   pFileTime->dwHighDateTime = (ULONG)(x >> 32);
   pFileTime->dwLowDateTime = (ULONG)x;
}


///////////////////////////////////////////////////////////////////////////////
//
// Etc Functions
//
///////////////////////////////////////////////////////////////////////////////

BOOL IsDiff( void* val1, void* val2, int nSize )
{
   int i;
   for( i = 0; i < nSize; i++ )
      if( ( (byte*)val1 )[i] != ( (byte*)val2 )[i] ) return TRUE;
   return FALSE;
}

BOOL GetFPoint3D( FPoint3D& p3d, const std::string& strIn )
{
   char* dup = strdup( strIn.c_str() );

   BOOL bOK = TRUE;
   char *token, *next_token;
   char seps[] = _T(",; ");
   token       = _tcstok_s( dup, seps, &next_token );
   int i;
   for( i = 0; i < 3 && token; i++ ) {
      int nFieldNum = 0;
      if( i == 0 ) nFieldNum = _stscanf( token, "%G", &p3d.X );
      if( i == 1 ) nFieldNum = _stscanf( token, "%G", &p3d.Y );
      if( i == 2 ) nFieldNum = _stscanf( token, "%G", &p3d.Z );
      if( nFieldNum != 1 ) {
         break;
      }
      token = _tcstok_s( NULL, seps, &next_token );
   }
   if( i < 3 )
      bOK = FALSE;

   free( dup );
   return bOK;
}
#ifdef WIN32
std::string GetExceptionCodeString( int nExceptionCode )
{
   std::string strECode;
   if( nExceptionCode == EXCEPTION_ACCESS_VIOLATION )
      strECode = "Access Violation";
   else if( nExceptionCode == EXCEPTION_DATATYPE_MISALIGNMENT )
      strECode = "Data Type Misalignment";
   else if( nExceptionCode == EXCEPTION_BREAKPOINT )
      strECode = "Break Point";
   else if( nExceptionCode == EXCEPTION_SINGLE_STEP )
      strECode = "Single Step";
   else if( nExceptionCode == EXCEPTION_ARRAY_BOUNDS_EXCEEDED )
      strECode = "Array Bounds Exceeded";
   else if( nExceptionCode == EXCEPTION_FLT_DENORMAL_OPERAND )
      strECode = "Float Denomal Operand";
   else if( nExceptionCode == EXCEPTION_FLT_DIVIDE_BY_ZERO )
      strECode = "Float DIvide By Zero";
   else if( nExceptionCode == EXCEPTION_FLT_INEXACT_RESULT )
      strECode = "Float Inexact Result";
   else if( nExceptionCode == EXCEPTION_FLT_INVALID_OPERATION )
      strECode = "Float Invalid Operation";
   else if( nExceptionCode == EXCEPTION_FLT_OVERFLOW )
      strECode = "Float Overflow";
   else if( nExceptionCode == EXCEPTION_FLT_STACK_CHECK )
      strECode = "Float Stack Check";
   else if( nExceptionCode == EXCEPTION_FLT_UNDERFLOW )
      strECode = "Float Underflow";
   else if( nExceptionCode == EXCEPTION_INT_DIVIDE_BY_ZERO )
      strECode = "Int Divide By Zero";
   else if( nExceptionCode == EXCEPTION_INT_OVERFLOW )
      strECode = "Int Overflow";
   else if( nExceptionCode == EXCEPTION_PRIV_INSTRUCTION )
      strECode = "Privileged Instruction";
   else if( nExceptionCode == EXCEPTION_IN_PAGE_ERROR )
      strECode = "In Page Error";
   else if( nExceptionCode == EXCEPTION_ILLEGAL_INSTRUCTION )
      strECode = "Illegal Instruction";
   else if( nExceptionCode == EXCEPTION_NONCONTINUABLE_EXCEPTION )
      strECode = "Noncontiuable Exception";
   else if( nExceptionCode == EXCEPTION_STACK_OVERFLOW )
      strECode = "Stack Overflow";
   else if( nExceptionCode == EXCEPTION_INVALID_DISPOSITION )
      strECode = "Invalid Disposition";
   else if( nExceptionCode == EXCEPTION_GUARD_PAGE )
      strECode = "Guard Page";
   else if( nExceptionCode == EXCEPTION_INVALID_HANDLE )
      strECode = "Invalid Handle";
   else
      strECode = "Unknown";
   return strECode;
}
#endif
// 버퍼의 내용을 shift_size 만큼 왼쪽으로 이동시키고 버퍼크기를 조정한다.
void ShiftLeftBuff( FileIO& buff, int shift_size )
{
   int remain_size = buff.GetLength() - shift_size;
   if( remain_size > 0 ) {
      memcpy( buff.GetBuffer(), buff.GetBuffer() + shift_size, remain_size );
   }
   buff.SetLength( remain_size );
}

bool __AreaGreater( __StreamItem elem1, __StreamItem elem2 )
{
   return elem1.nSqrtArea < elem2.nSqrtArea;
}

int GetStreamIdxOfMostSimillarInSize( std::vector<__StreamItem>& vtStreamItemArr, const ISize2D& imgSize, float fDispQuality )
{
   int i;

   int nStreamIdxOfMostSimillarInSize = 0;
   int nItemArrSize                   = (int)vtStreamItemArr.size();
   if( 1 == nItemArrSize ) {
      return vtStreamItemArr[0].nStreamIdx;
   }
   if( nItemArrSize >= 2 ) {
      int nSqrtImgSize = (int)sqrt( float( imgSize.Width * imgSize.Height ) );
      std::sort( vtStreamItemArr.begin(), vtStreamItemArr.end(), __AreaGreater );

      if( nSqrtImgSize <= vtStreamItemArr[0].nSqrtArea ) {
         nStreamIdxOfMostSimillarInSize = vtStreamItemArr[0].nStreamIdx;
      } else if( nSqrtImgSize > vtStreamItemArr[nItemArrSize - 1].nSqrtArea ) {
         nStreamIdxOfMostSimillarInSize = vtStreamItemArr[nItemArrSize - 1].nStreamIdx;
      } else {
         for( i = 0; i < nItemArrSize - 1; i++ ) {
            __StreamItem& item1 = vtStreamItemArr[i];
            __StreamItem& item2 = vtStreamItemArr[i + 1];
            if( item1.nSqrtArea <= nSqrtImgSize && nSqrtImgSize < item2.nSqrtArea ) {
               if( ( item1.nSqrtArea + item2.nSqrtArea ) * ( 1.0f - fDispQuality ) > nSqrtImgSize )
                  nStreamIdxOfMostSimillarInSize = item1.nStreamIdx;
               else
                  nStreamIdxOfMostSimillarInSize = item2.nStreamIdx;
               break;
            }
         }
      }
   }
   return nStreamIdxOfMostSimillarInSize;
}

std::string GetString( FPoint3D p3d )
{
   std::string str = sutil::sformat( "%G,%G,%G", p3d.X, p3d.Y, p3d.Z );
   return str;
}

BOOL IsValidIPAddress( const std::string& strIPAddress )
{
   BOOL bRet = FALSE;
   if( strIPAddress.length() ) {
      const char* pszAddress = strIPAddress.c_str();
      unsigned long nIPAddressOut;
      nIPAddressOut = inet_addr( pszAddress );
      if( nIPAddressOut != 0 && nIPAddressOut != INADDR_NONE ) {
         bRet = TRUE;
      }
   }
   return bRet;
}
#ifdef WIN32
std::string ReadMyIP()
{
   char name[255] = {
      0,
   };
   char* ip;
   PHOSTENT host;

   BOOL bRet = FALSE;

   if( gethostname( name, sizeof( name ) ) == 0 ) {
      if( ( host = gethostbyname( name ) ) != NULL ) {
         ip = inet_ntoa( *(struct in_addr*)*host->h_addr_list );

         bRet = TRUE;
      }
   }

   std::string strRet = ip;

   return strRet;
}
#endif

#ifdef WIN32
BOOL SetCurSel_ByData( CComboBox* pComboBox, int& nData )
{
   int i;
   int nItem = pComboBox->GetCount();
   for( i = 0; i < nItem; i++ ) {
      DWORD data = (DWORD)pComboBox->GetItemData( i );
      if( data == nData ) {
         pComboBox->SetCurSel( i );
         return TRUE;
      }
   }
   if( nItem ) {
      pComboBox->SetCurSel( 0 );
      nData = (int)pComboBox->GetItemData( 0 );
   }
   if( nItem == 0 ) return FALSE;
   return TRUE;
}

BOOL SetCurSel_ByData( CComboBox* pComboBox, float& fData )
{
   int i;
   float fMax      = FLT_MAX;
   int nMinDiffIdx = -1;
   int nItem       = pComboBox->GetCount();
   if( nItem == 0 ) return FALSE;
   for( i = 0; i < nItem; i++ ) {
      uint nData = (uint)pComboBox->GetItemData( i );
      float fItemData;
      memcpy( &fItemData, &nData, sizeof( uint ) );
      if( fData == fItemData ) {
         pComboBox->SetCurSel( i );
         return TRUE;
      } else {
         if( fabs( fItemData - fData ) < fMax ) {
            fMax        = fItemData - fData;
            nMinDiffIdx = i;
         }
      }
   }
   if( nItem ) {
      if( nMinDiffIdx |= -1 ) {
         pComboBox->SetCurSel( nMinDiffIdx );
         fData = (float)pComboBox->GetItemData( nMinDiffIdx );
      } else {
         pComboBox->SetCurSel( 0 );
         fData = (float)pComboBox->GetItemData( 0 );
      }
   }
   return TRUE;
}

BOOL SetCurSel_ByIndex( CComboBox* pComboBox, int& nIdx )
{
   int nItem = pComboBox->GetCount();
   if( nItem == 0 ) return FALSE;
   if( nIdx < 0 || nIdx >= nItem ) {
      pComboBox->SetCurSel( 0 );
      nIdx = 0;
   } else
      pComboBox->SetCurSel( nIdx );
   return TRUE;
}

BOOL SetCurSel_ByText( CComboBox* pComboBox, CString& strText )
{
   CString strValue;
   int i;
   int nItem = pComboBox->GetCount();
   for( i = 0; i < nItem; i++ ) {
      pComboBox->GetLBText( i, strValue );
      if( strValue == strText ) {
         pComboBox->SetCurSel( i );
         return TRUE;
      }
   }
   return FALSE;
}

BOOL GetCurData( CComboBox* pComboBox, int& nData )
{
   if( pComboBox->GetCount() == 0 ) return FALSE;
   int nCurSelIdx = pComboBox->GetCurSel();
   nData          = (int)pComboBox->GetItemData( nCurSelIdx );
   return TRUE;
}

BOOL GetCurData( CComboBox* pComboBox, float& fData )
{
   if( pComboBox->GetCount() == 0 ) return FALSE;
   int nData = pComboBox->GetItemData( pComboBox->GetCurSel() );
   memcpy( &fData, &nData, sizeof( uint ) );
   return TRUE;
}

BOOL GetCurData( CComboBox* pComboBox, CString& strData )
{
   if( pComboBox->GetCount() == 0 ) return FALSE;
   CString* pStringData = (CString*)pComboBox->GetItemData( pComboBox->GetCurSel() );
   if( pStringData ) {
      strData = *pStringData;
      return TRUE;
   }
   return FALSE;
}

BOOL AddComboBoxItem( CComboBox* pComboBox, const CString& strItemStr, int nData )
{
   int nItemIdx = pComboBox->AddString( strItemStr );
   pComboBox->SetItemData( nItemIdx, DWORD_PTR( nData ) );
   return TRUE;
}

BOOL AddComboBoxItem( CComboBox* pComboBox, const CString& strItemStr, float fData )
{
   int nItemIdx = pComboBox->AddString( strItemStr );
   uint nData;
   memcpy( &nData, &fData, sizeof( uint ) );
   pComboBox->SetItemData( nItemIdx, DWORD_PTR( nData ) );
   return TRUE;
}

// Edit Box
void GetInt( CEdit* pEdit, int& nData )
{
   CString strText;
   pEdit->GetWindowText( strText );
   nData = atoi( strText );
}

void SetStr( CEdit* pEdit, int nData )
{
   CString strText;
   strText.Format( "%d", nData );
   pEdit->SetWindowText( strText );
}

BOOL IsShowWindow( CWnd* pWnd )
{
   BOOL bShowWnd = FALSE;
   if( pWnd ) {
      WINDOWPLACEMENT wpt;
      pWnd->GetWindowPlacement( &wpt );
      if( wpt.showCmd == SW_SHOW ) {
         bShowWnd = TRUE;
      }
   }
   return bShowWnd;
}

void GetWndRect( CRect& rect, CWnd* pParent, CWnd* pChild )
{
   // pParent 윈도우의 클라이언트 영역 좌표계 내에서
   // pChild  윈도우가 차지하는 영역의 좌표를 얻는 함수이다.
   CRect ParentWindowRect;
   CRect ParentClientRect;
   pParent->GetWindowRect( ParentWindowRect );
   pParent->GetClientRect( ParentClientRect );
   int border_size  = ( ParentWindowRect.Width() - ParentClientRect.Width() ) / 2;
   int title_height = ParentWindowRect.Height() - ParentClientRect.Height() - border_size;

   CRect ChildWindowRect;
   GetWindowRect( pChild->GetSafeHwnd(), ChildWindowRect );
   ChildWindowRect.OffsetRect( -ParentWindowRect.TopLeft() - CPoint( border_size, title_height ) );
   rect = ChildWindowRect;
}

void AFXAPI DDX_Text( CDataExchange* pDX, int nIDC, std::string& str )
{
   HWND hWndCtrl = pDX->PrepareCtrl( nIDC );
   ASSERT( hWndCtrl != NULL );

   CWnd* pWnd            = CWnd::FromHandle( hWndCtrl );
   const int MAX_LEN     = 256;
   TCHAR szTemp[MAX_LEN] = {
      0,
   };
   if( pDX->m_bSaveAndValidate ) {
      pWnd->GetWindowText( szTemp, MAX_LEN - 1 );
      str = szTemp;
   } else // initializing
   {
      pWnd->SetWindowText( str.c_str() );
   }
}

void gfxGetBoundaryRgn( CRect rcSrc, CRgn* rgn, int nThick )
{
   if( rgn->m_hObject )
      rgn->DeleteObject();

   rgn->CreateRectRgn( 0, 0, 0, 0 );

   CRgn rgnA, rgnB;
   rgnA.CreateRectRgnIndirect( rcSrc );
   CRect rc = rcSrc;
   rc.DeflateRect( nThick, nThick, nThick, nThick );
   rgnB.CreateRectRgnIndirect( rc );
   rgn->CombineRgn( &rgnA, &rgnB, RGN_XOR );
}

CRect gfxGetFittingRect( CRect rect, CSize s_size )
{
   CRect d_rect;
   float wfactor1, wfactor2, sfactor;
   wfactor1 = rect.Width() / (float)rect.Height();
   wfactor2 = s_size.cx / (float)s_size.cy;
   if( wfactor2 > wfactor1 ) {
      sfactor       = rect.Width() / (float)s_size.cx;
      int d_height  = (int)( s_size.cy * sfactor );
      d_rect.left   = rect.left;
      d_rect.right  = rect.right;
      d_rect.top    = rect.top + ( rect.Height() - d_height ) / 2;
      d_rect.bottom = d_rect.top + d_height;
   } else {
      sfactor       = rect.Height() / (float)s_size.cy;
      int d_width   = (int)( s_size.cx * sfactor );
      d_rect.top    = rect.top;
      d_rect.bottom = rect.bottom;
      d_rect.left   = rect.left + ( rect.Width() - d_width ) / 2;
      d_rect.right  = d_rect.left + d_width;
   }
   return d_rect;
}

CRect gfxGetFittingRect( CRect rect, int width, int height )
{
   return gfxGetFittingRect( rect, CSize( width, height ) );
}

void gfxGetFittingRect( CRect rect, int width, int height, CRect& d_rect )
{
   d_rect = gfxGetFittingRect( rect, width, height );
}

#endif

///////////////////////////////////////////////////////////////////////////////
// Memory Checking
///////////////////////////////////////////////////////////////////////////////
#ifdef WIN32
BOOL IsPhysMemAvail( DWORD nRequiredFreePhysMem )
{
   if( GetAvailPhysMemSize() < nRequiredFreePhysMem )
      return FALSE;
   return TRUE;
}

DWORD GetAvailPhysMemSize()
{
   MEMORYSTATUS statex;
   GlobalMemoryStatus( &statex );
   return statex.dwAvailPhys;
}

DWORD GetTotalPhysMemSize()
{
   MEMORYSTATUS statex;
   GlobalMemoryStatus( &statex );
   return statex.dwTotalPhys;
}

DWORD GetAvailVirtMemSize()
{
   MEMORYSTATUS statex;
   GlobalMemoryStatus( &statex );
   return statex.dwAvailVirtual;
}

DWORD GetTotalVirtMemSize()
{
   MEMORYSTATUS statex;
   GlobalMemoryStatus( &statex );
   return statex.dwTotalVirtual;
}

DWORD GetAvailPhysMemSizeInMB()
{
   MEMORYSTATUSEX statex;
   statex.dwLength = sizeof( statex );
   GlobalMemoryStatusEx( &statex );
   return DWORD( statex.ullAvailPhys / ( 1024 * 1024 ) );
}

DWORD GetTotalPhysMemSizeInMB()
{
   MEMORYSTATUSEX statex;
   statex.dwLength = sizeof( statex );
   GlobalMemoryStatusEx( &statex );
   return DWORD( statex.ullTotalPhys / ( 1024 * 1024 ) );
}

DWORD GetAvailVirtMemSizeInMB()
{
   MEMORYSTATUSEX statex;
   statex.dwLength = sizeof( statex );
   GlobalMemoryStatusEx( &statex );
   return DWORD( statex.ullAvailVirtual / ( 1024 * 1024 ) );
}

DWORD GetTotalVirtMemSizeInMB()
{
   MEMORYSTATUSEX statex;
   statex.dwLength = sizeof( statex );
   GlobalMemoryStatusEx( &statex );
   return DWORD( statex.ullTotalVirtual / ( 1024 * 1024 ) );
}
#endif
//////////////////////////////////////////////////////////////////////////////////////

std::string CreateGUID()
{
#ifdef WIN32
   GUID gidReference;
   HRESULT hCreateGuid = CoCreateGuid( &gidReference );
   std::string strGUID = sutil::sformat( "%08X-%04X-%04X-%02X%02X-%02X%02X%02X%02X%02X%02X",
                                         gidReference.Data1, gidReference.Data2, gidReference.Data3,
                                         gidReference.Data4[0],
                                         gidReference.Data4[1],
                                         gidReference.Data4[2],
                                         gidReference.Data4[3],
                                         gidReference.Data4[4],
                                         gidReference.Data4[5],
                                         gidReference.Data4[6],
                                         gidReference.Data4[7] );
   return strGUID;
#else

#define RANDOM( x ) ( rand() % ( x ) )
#define RANDOM_8X() ( rand() % ( 0xFFFFFFFF ) )
#define RANDOM_4X() ( rand() % ( 0x0000FFFF ) )
#define RANDOM_2X() ( rand() % ( 0x000000FF ) )

   std::string strRet = sutil::sformat( "%04X%04X-%04X-%04X-%02X%02X-%02X%02X%02X%02X%02X%02X",
                                        RANDOM_4X(), RANDOM_4X(),
                                        RANDOM_4X(),
                                        RANDOM_4X(),
                                        RANDOM_2X(), RANDOM_2X(),
                                        RANDOM_2X(), RANDOM_2X(), RANDOM_2X(), RANDOM_2X(), RANDOM_2X(), RANDOM_2X() );

   return strRet;
#endif
}

RECT GetCalcScale( const RECT& rt, float scale_x, float scale_y )
{
   RECT rtRet;
   ZeroMemory( &rtRet, sizeof( rtRet ) );

   rtRet.left   = (int)( ( rt.left * scale_x ) + 0.5 );
   rtRet.right  = (int)( ( rt.right * scale_x ) + 0.5 );
   rtRet.top    = (int)( ( rt.top * scale_y ) + 0.5 );
   rtRet.bottom = (int)( ( rt.bottom * scale_y ) + 0.5 );

   return rtRet;
}

RECT ConvertRect( const BCCL::IBox2D& box )
{
   RECT rtRet;
   ZeroMemory( &rtRet, sizeof( rtRet ) );

   rtRet.left   = box.X;
   rtRet.top    = box.Y;
   rtRet.right  = box.X + box.Width;
   rtRet.bottom = box.Y + box.Height;

   return rtRet;
}

BOOL GetIPAddress( DWORD& nIPAddressOut, const std::string& strIn )
{
   BOOL bRet = FALSE;
   if( strIn.length() ) {
      const char* pszAddress = strIn.c_str();
      nIPAddressOut          = inet_addr( pszAddress );
      if( nIPAddressOut != INADDR_NONE ) {
         bRet = TRUE;
      }
   }
   return bRet;
}

BOOL GetIPAddrString( std::string& strOut, DWORD nIPAddressIn )
{
   in_addr _nIPAddressIn;
   memcpy( &_nIPAddressIn, &nIPAddressIn, sizeof( unsigned long ) );
   char* pszAddress = inet_ntoa( _nIPAddressIn );
   if( pszAddress ) {
      strOut = sutil::sformat( "%s", pszAddress );
   } else {
      strOut = "";
      return FALSE;
   }
   return TRUE;
}

std::string GetIPAddrString( DWORD nIPAddressIn )
{
   std::string strIPAddress;
   GetIPAddrString( strIPAddress, nIPAddressIn );
   return strIPAddress;
}

BOOL GetIPAddrString( std::string& strOut, const std::string& strIn ) // (xinu_bc23)
{
   u_long inaddr;
   struct hostent* hp  = NULL;
   struct in_addr* inp = NULL;
   char** p            = NULL;

   if( strIn.empty() ) {
      strOut = "";
      return FALSE;
   }

   // inet_addr() 함수는 인자로 숫자로 이루어진 IP address를 받아들여
   // unsigned long형의 internet address로 변환시킨다.
   // url string을 숫자로 표현된 ip로 변환시켜야 한다.
   if( ( inaddr = inet_addr( strIn.c_str() ) ) == INADDR_NONE ) {
      // get host information for the given url.
      hp = gethostbyname( strIn.c_str() );
      // if hp is null, return null
      if( hp == NULL ) {
         strOut = "";
         return FALSE;
      }
      // we get the the first element from the address list.
      p                = hp->h_addr_list;
      inp              = (struct in_addr*)*p;
      char* pszAddress = inet_ntoa( *inp );
      if( pszAddress ) {
         strOut = sutil::sformat( "%s", pszAddress );
         //strOut.Format ("%s", "192.168.0.36"); // fixed (xinu_cc05)
         return TRUE;
      } else {
         strOut = "";
         return FALSE;
      }
   }
   // 만약 inet_addr()의 결과가 INADDR_NONE이 아니라면 이미
   // 인자값 자체가 ip address이므로 그대로 반환...
   else {
      strOut = strIn;
      return TRUE;
   }
}

std::string GetIPAddrString( const std::string& strIn )
{
   std::string strIPAddress;
   GetIPAddrString( strIPAddress, strIn );
   return strIPAddress;
}

DWORD GetIPAddr( const std::string& strIPAddressIn )
{
   DWORD nIPAddress = 0;
   GetIPAddress( nIPAddress, strIPAddressIn );
   return nIPAddress;
}

void ModifyState( uint& nState, uint nAdd, uint nRemove )
{
   nState &= ~nRemove;
   nState |= nAdd;
}

int Convert32BitFlagToInt( uint n32BitFlag )
{
   int i;
   for( i = 0; i < 32; i++ ) {
      int nCheckFlag = 1 << i;
      if( nCheckFlag & n32BitFlag ) {
         return i;
      }
   }
   return 0;
}

#ifdef __linux
#include <termios.h>
// TODO(jongoh): 참고 사항
// class CSetKeyboardNonBlock -> https://gist.github.com/whyrusleeping/3983293

CSetKeyboardNonBlock::CSetKeyboardNonBlock()
{
   m_setting = new struct termios;
}

CSetKeyboardNonBlock::~CSetKeyboardNonBlock()
{
   delete m_setting;
}

void CSetKeyboardNonBlock::Restore()
{
   // Not restoring the keyboard settings causes the input from the terminal to not work right
   tcsetattr(0, TCSANOW, m_setting);
}

void CSetKeyboardNonBlock::Set()
{
   struct termios new_settings;
   tcgetattr(0,m_setting);

   new_settings = *m_setting;
   new_settings.c_lflag &= ~ICANON;
   new_settings.c_lflag &= ~ECHO;
   new_settings.c_lflag &= ~ISIG;
   new_settings.c_cc[VMIN] = 0;
   new_settings.c_cc[VTIME] = 0;

   tcsetattr(0, TCSANOW, &new_settings);
}
#endif

///////////////////////////////////////////////////////////////////////////////
// std::string util
///////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <sstream>

namespace sutil {
std::string scurrent_time( const std::string& format )
{
   time_t rawtime;
   struct tm* timeinfo;
   char szTime[128];

   time( &rawtime );
   timeinfo = localtime( &rawtime );
   strftime( szTime, 127, format.c_str(), timeinfo );

   return std::string( szTime );
}

void GetSplitStringList( const std::string& strIn, char ch, std::vector<std::string>& vecOut )
{
   std::istringstream iss( strIn );
   std::string token;
   while( std::getline( iss, token, ch ) ) {
      vecOut.push_back( token );
   }
}

std::string StringToHex( const std::string& buffer )
{
   std::stringstream ss;
   char data[2];
   for( size_t i = 0; i < buffer.size(); i++ ) {
      sprintf( data, "%02x ", buffer.at( i ) );
      ss << data;
   }
   return ss.str();
}

std::string StringToHex( const uchar* data, size_t count )
{
   std::ostringstream ss;
   char buffer[2];
   for( size_t i = 0; i < count; i++ ) {
      sprintf( buffer, "%02x ", data[i] );
      ss << buffer;
   }
   return ss.str();
}

} // namespace sutil

///////////////////////////////////////////////////////////////////////////////
//
// CMapIDToString
//
///////////////////////////////////////////////////////////////////////////////

std::string CMapIDToString::GetString( int nID )
{
   int i;
   int nItem            = (int)size();
   CMapIDToString& This = *this;
   for( i = 0; i < nItem; i++ ) {
      if( This[i].m_nID == nID ) {
         return This[i].m_String;
      }
   }
   return std::string( "" );
}

int CMapIDToString::GetID( const std::string& str )
{
   int i;
   int nItem            = (int)size();
   CMapIDToString& This = *this;
   for( i = 0; i < nItem; i++ ) {
      if( This[i].m_String == str ) {
         return This[i].m_nID;
      }
   }
   return -1;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: COMMONID
//
///////////////////////////////////////////////////////////////////////////////

COMMONID::COMMONID()
{
   time.high_time = 0;
   time.low_time  = 0;
   id             = -1;
}

int COMMONID::ReadFile( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "COMMONID" ) ) {
      xmlNode.Attribute( TYPE_ID( FILETIME ), "time", &time );
      xmlNode.Attribute( TYPE_ID( int ), "id", &id );
      xmlNode.End();
   }
   return ( DONE );
}

int COMMONID::WriteFile( CXMLIO* pIO )
{
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "COMMONID" ) ) {
      xmlNode.Attribute( TYPE_ID( FILETIME ), "time", &time );
      xmlNode.Attribute( TYPE_ID( int ), "id", &id );
      xmlNode.End();
   }
   return ( DONE );
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CStreamSocketEx
//
///////////////////////////////////////////////////////////////////////////////
CStreamSocketEx::CStreamSocketEx()
{
   ErrorCode = 0;
}

int CStreamSocketEx::Create()
{
   Close();
   Socket = socket( AF_INET, SOCK_STREAM, 0 );
   if( Socket == INVALID_SOCKET ) return ( -1 );
   return ( DONE );
}

int CStreamSocketEx::Bind( const char* ip_addr, ushort port )

{
   SOCKADDR_IN address;
   address.sin_family      = AF_INET;
   address.sin_port        = 0;
   address.sin_addr.s_addr = htonl( INADDR_ANY );
   if( bind( Socket, (LPSOCKADDR)&address, sizeof( address ) ) == SOCKET_ERROR ) {
      ErrorCode = GetLastError();
      return ( -2 );
   }
   return ( DONE );
}

int CStreamSocketEx::ConnectNB( const char* ip_addr, ushort port )

{
   SOCKADDR_IN address;
   u_long flag_nbio = 1;
   ioctlsocket( Socket, FIONBIO, &flag_nbio );
   flag_nbio               = 0;
   address.sin_family      = AF_INET;
   address.sin_port        = htons( port );
   address.sin_addr.s_addr = inet_addr( ip_addr );
   ErrorCode               = DONE;
   int iRet                = connect( Socket, (LPSOCKADDR)&address, sizeof( address ) );
   if( iRet != SOCKET_ERROR ) {
      LOGD << "connect ret:" << iRet << " errno : " << ErrorCode << " strerror:" << strerror( ErrorCode );
   } else {
      ErrorCode = GetLastError();
      LOGW << "Connect Error. Ret:" << iRet << " errno:" << ErrorCode << " strerror:" << strerror( ErrorCode );
   }

   ioctlsocket( Socket, FIONBIO, &flag_nbio );
   if( ErrorCode == DONE || ErrorCode == ERR_ISCONN ) {
      if( setsockopt( Socket, SOL_SOCKET, SO_RCVTIMEO, (char*)&RcvTimeout, sizeof( timeval ) ) ) {
         //return -1;
      }
      if( setsockopt( Socket, SOL_SOCKET, SO_SNDTIMEO, (char*)&SndTimeout, sizeof( timeval ) ) ) {
         //return -2;
      }
#ifdef __linux
      int set = 1;
      if( setsockopt( Socket, SOL_SOCKET, SO_KEEPALIVE, (int*)&set, sizeof( int ) ) ) {
         //return -3;
      }
      if( setsockopt( Socket, SOL_SOCKET, SO_REUSEADDR, &set, sizeof( int ) ) ) {
         //return -4;
      }
#endif // __linux
      return ( DONE );
   } else {
      return ErrorCode;
   }
}

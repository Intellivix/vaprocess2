#pragma once

#include <mutex>
#include "bccl.h"
#include "bccl_define.h"
#include "ImageEnhancement.h"
#include "version.h"
//#include "Win32CL/DXVA2Decoder.h"

//using namespace Win32CL;
using namespace BCCL;

///////////////////////////////////////////////////////////////////////////////
//
// Enumerations
//
///////////////////////////////////////////////////////////////////////////////
// 주의: 코덱ID 값이 255를 초과하면 안된다.
//       DB에 저장할 때 코덱ID 값이 1 byte의 크기로 저장되기 때문이다.

enum AVCoDecoderLibType {
   AVCoDecoderLibType_FFMPEG    = 1,
   AVCoDecoderLibType_HikVision = 4,
   AVCoDecoderLibType_DXVA2     = 5,
   AVCoDecoderLibType_MFC       = 6,
};

enum VideoCodecID {
   VCODEC_ID_NONE       = 255,
   VCODEC_ID_MPEG4      = 0,
   VCODEC_ID_MJPEG      = 1,
   VCODEC_ID_RAW        = 2,
   VCODEC_ID_H264       = 3,
   VCODEC_ID_XVID       = 4,
   VCODEC_ID_H263       = 5,
   VCODEC_ID_MPEG1VIDEO = 6,
   VCODEC_ID_MPEG2VIDEO = 7,
   VCODEC_ID_HIK_H264   = 8,
   VCODEC_ID_HIK_MPEG4  = 9,
   VCODEC_ID_HIK_MJPEG  = 10,
   VCODEC_ID_HIK_AVC264 = 11,
   VCODEC_ID_VC1        = 12,
   VCODEC_ID_H265       = 13,
};

enum VideoPixelFormat {
   VPIX_FMT_NONE      = -1,
   VPIX_FMT_YUY2      = 0,
   VPIX_FMT_YV12      = 1,
   VPIX_FMT_BGR24     = 2,
   VPIX_FMT_GRAY      = 3,
   VPIX_FMT_DXVA2_VLD = 61,

   VPIX_FMT_DETECTING = 100, // [SGLEE:20160909FRI]
   VPIX_FMT_YUVJ444P,
   VPIX_FMT_YUVJ422P,
   VPIX_FMT_YUVJ411P,
   VPIX_FMT_YUVJ420P,
};

enum VideoFrameFlag {
   VFRM_PTZ_MOVING              = 0x00000001,
   VFRM_VIDEO_ANALYTICS_ONGOING = 0x00000002,
   VFRM_KEYFRAME                = 0x00000010, // Key Frame
   VFRM_ODD_EVEN_FIELD          = 0x00000100, // even 필드와 odd 필드가 interlaced 되어있지 않고 한 영상에 있음.
   VFRM_DECODE_ODD_FILED_ONLY   = 0x00000200, // VFRM_ODD_EVEN_FIELD 플레그가 On 되어있는 경우 even 필드만 디코딩 하기.
   VFRM_NULLFRAME               = 0x00000400, // VFRM_ODD_EVEN_FIELD 플레그가 On 되어있는 경우 even 필드만 디코딩 하기.
};

enum AVCodecDecompressRet {
   AVCodecDecompressRet_Success                 = 0,
   AVCodecDecompressRet_NeedMoreFrame           = -1, // 에러가 아님.
   AVCodecDecompressRet_Failed                  = 1, // 실패 : 이유는 모름
   AVCodecDecompressRet_DecoderIsNotOpend       = 2, // 실패 : 디코더 오픈을 하지 않음
   AVCodecDecompressRet_InputStreamDataIsFailed = 3, // 실패 : 스트림데이터를 큐(버퍼)에 추가에 실패함.
   AVCodecDecompressRet_ImageSizeDoesNotMatched = 4, // 실패 : 영상의 크기가 일치하지 않아 실패. 크기인자만 바로 잡아주면 디코딩 가능.
   AVCodecDecompressRet_ImageSizeIsChanged      = 5, // 기타 : 이미지 크기가 변경됨
};

class CFrameBuffer {
public:
   CFrameBuffer( int nOutputEncSize );
   ~CFrameBuffer();

public:
   BYTE* m_pOutputEncBuffer;
   int m_nOutputEncSize;
   int m_nOutputFlag;
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: CAVCodecParam
//
///////////////////////////////////////////////////////////////////////////////
class CAVCodecParam {
public:
   int m_nWidth;
   int m_nHeight;
   int m_nBitrate;
   float m_fFrameRate;
   float m_fDecodingFPS;
   BOOL m_bUseHWAcceleration;
   enum VideoCodecID m_nCodecID;
   enum VideoPixelFormat m_nPixelFormat;

public:
   CAVCodecParam();

public:
   CAVCodecParam& operator=( CAVCodecParam& from );
   BOOL operator==( CAVCodecParam& from );
   BOOL operator!=( CAVCodecParam& from );

public:
   CAVCodecParam& GetAVCodecParam()
   {
      return *this;
   };
   void InitAVCodecParam();
   void Set( const CAVCodecParam& from );
   BOOL IsEqual( const CAVCodecParam& from );
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: IAVCoDecoder
//
///////////////////////////////////////////////////////////////////////////////

class IAVCoDecoder : public CAVCodecParam {
public:
   ISize2D m_ImageSizeOfEncodedData; // 디코딩 결과에 영상크기 정보가 담겨있다면 본 변수에 저장한다.

public:
   IAVCoDecoder(){};
   virtual ~IAVCoDecoder(){};

public:
   virtual void Close(){};
   virtual int OpenEncoder( CAVCodecParam& avCodecParam );
   virtual int OpenDecoder( CAVCodecParam& avCodecParam );
   virtual int OpenDecoder( AVCodecContext* codec_ctx );
   virtual int Compress( byte* pSrcImgBuff, byte** ppCompBuff, int& nCompBuffSize, int& nFlags )
   {
      return ( -1 );
   };
   virtual int Compress( BGRImage& s_image, byte** ppCompBuff, int& nCompBuffSize, int& nFlags )
   {
      return ( -1 );
   };
   virtual int Decompress( byte* pCompBuff, int nCompBuffSize, byte* pDstImgBuff, int nFlags )
   {
      return ( -1 );
   };
   virtual int Decompress( byte* pCompBuff, int nCompBuffSize, BGRImage& d_image, int nFlags )
   {
      return ( -1 );
   };
   virtual int Resize( GImage& s_image, GImage& d_image )
   {
      return ( -1 );
   }
   virtual int ConvertYUY2ToGray( byte* s_buffer, int nWidth, int nHeight, GImage& d_image )
   {
      return ( -1 );
   }

   virtual int DecompressKeyFrame( byte* pCompBuff, int nCompBuffSize, byte* pDstImgBuff, int nFlags = 0 )
   {
      return 0;
   }
   virtual int UpdateCurrShowFrame( byte* pDstImgBuff )
   {
      return 0;
   }
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: CAVCoDecoder_FFMPEG
//
///////////////////////////////////////////////////////////////////////////////

class CAVCoDecoder_FFMPEG : public IAVCoDecoder {
public:
   int m_nCount_NeedMoreFrame;
   int m_nCountInterPictuerFrame;
   FFMPEG_VideoCodec m_VideoCodec;

public:
   virtual ~CAVCoDecoder_FFMPEG();

public:
   virtual void Close();
   virtual int OpenEncoder( CAVCodecParam& avCodecParam );
   virtual int OpenDecoder( CAVCodecParam& avCodecParam );
   virtual int OpenDecoder( AVCodecContext* codec_ctx );
   virtual int Compress( byte* pSrcImgBuff, byte** ppCompBuff, int& nCompBuffSize, int& nFlags );
   virtual int Compress( BGRImage& s_image, byte** ppCompBuff, int& nCompBuffSize, int& nFlags );
   virtual int Decompress( byte* pCompBuff, int nCompBuffSize, byte* pDstImgBuff, int nFlags );
   virtual int Decompress( byte* pCompBuff, int nCompBuffSize, BGRImage& d_image, int nFlags );
   virtual int Resize( GImage& s_image, GImage& d_image );
   virtual int ConvertYUY2ToGray( byte* s_buffer, int nWidth, int nHeight, GImage& d_image );

   virtual int DecompressKeyFrame( byte* pCompBuff, int nCompBuffSize, byte* pDstImgBuff, int nFlags = 0 );
   virtual int UpdateCurrShowFrame( byte* pDstImgBuff )
   {
      return 0;
   }

protected:
   void DeinterlaceVideoFrame_YUY2( byte* s_img_buf, int width, int height, byte* d_img_buf );
};

#ifdef __SUPPORT_MFC_CODEC
///////////////////////////////////////////////////////////////////////////////
//
// Class: CAVCoDecoder_MFC
//
///////////////////////////////////////////////////////////////////////////////
class CMFC;

class CAVCoDecoder_MFC : public IAVCoDecoder {
public:
   CMFC* m_pMFCCodec;
   int m_nCount_NeedMoreFrame;
   uint m_nDecodingCount;
   uint m_nFirstDecoding;
   uint m_nActiveDecodingCount;

public:
   CAVCoDecoder_MFC();
   virtual ~CAVCoDecoder_MFC();

public:
   virtual void Close();
   virtual int OpenEncoder( CAVCodecParam& avCodecParam );
   virtual int OpenDecoder( CAVCodecParam& avCodecParam );
   virtual int OpenDecoder( AVCodecContext* codec_ctx );
   virtual int Compress( byte* pSrcImgBuff, byte** ppCompBuff, int& nCompBuffSize, int& nFlags );
   virtual int Compress( BGRImage& s_image, byte** ppCompBuff, int& nCompBuffSize, int& nFlags );
   virtual int Decompress( byte* pCompBuff, int nCompBuffSize, byte* pDstImgBuff, int nFlags );
   virtual int Decompress( byte* pCompBuff, int nCompBuffSize, BGRImage& d_image, int nFlags );

   virtual int DecompressKeyFrame( byte* pCompBuff, int nCompBuffSize, byte* pDstImgBuff, int nFlags = 0 );
   virtual int UpdateCurrShowFrame( byte* pDstImgBuff )
   {
      return 0;
   }

protected:
   BOOL ConvertPixelFormat( void* s_img_y, void* s_img_uv, int len_y, int len_uv, byte* d_img_buf );
};
#endif
///////////////////////////////////////////////////////////////////////////////
//
// Class: CAVCodec
//
///////////////////////////////////////////////////////////////////////////////
class CAVCodec : public CAVCodecParam
#ifdef __SUPPORT_IMAGE_ENHANCEMENT
    ,
                 public CImageEnhancement
#endif
{
protected:
   IAVCoDecoder* m_pAVCoDecoder;
   CCriticalSection m_csCodec;
   BOOL m_bOpenCodec;

public:
   CAVCodec();
   ~CAVCodec();

protected:
   void CreateAVCoDecoder( int nCoDecoderLibType );
   void CreateAVCoDecoder( uint nCodecID, int nCoDecoderLibType );
   void DestroyAVCoDecoder();

public:
   void Close();
   BOOL IsOpen();
   int OpenEncoder( const CAVCodecParam& avOpen );
   int OpenDecoder( const CAVCodecParam& avOpen );
   int Compress( byte* pSrcImgBuff, byte** ppCompBuff, int& nCompBuffSize, int& nFlags );
   int Compress( BGRImage& s_image, byte** ppCompBuff, int& nCompBuffSize, int& nFlags );
   int Decompress( byte* pCompBuff, int nCompBuffSize, byte* pDstImgBuff, int nFlags = 0 );
   int Decompress( byte* pCompBuff, int nCompBuffSize, BGRImage& d_image, int nFlags = 0 );
   void CloseImageEnhancement();

   int DecompressKeyFrame( byte* pCompBuff, int nCompBuffSize, byte* pDstImgBuff, int nFlags = 0 );
   int UpdateCurrShowFrame( byte* pDstImgBuff );

   int Resize( GImage& s_image, GImage& d_image );
   int ConvertYUY2ToGray( byte* s_buffer, int nWidth, int nHeight, GImage& d_image );

public:
   void Lock();
   void Unlock();

public:
   ISize2D GetImageSizeOfEncodedData();

public:
   IAVCoDecoder* GetAVCoDecoder()
   {
      return m_pAVCoDecoder;
   }
};

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

enum VideoCodecID GetVideoCodecID( enum AVCodecID codec_id );
enum AVCodecID GetFFMPEGCodecID( enum VideoCodecID codec_id );
int GetHikvisionCodecID( enum VideoCodecID codec_id );
enum AVPixelFormat GetRealPixelFormat_FFMPEG( enum VideoPixelFormat pixel_format );
VideoPixelFormat FFmpegPixFmtToVideoPixFmt( AVPixelFormat PixFmt );
uint GetVFWFourCC( enum VideoCodecID codec_id );
BOOL IsHikVisionCodecID( enum VideoCodecID codec_id );

////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////

enum AudioCodecID {
   AUDIO_ID_NONE = 255,
   AUDIO_ID_G711 = 0,
};

﻿#pragma once

#include <vector>
#include <thread>
#include <chrono>
#include <mutex>
#include <sstream>

#include "bccl.h"
#include "bccl_define.h"

extern BOOL g_bDebugMemory; // 메모리 사용량을 디버깅 하기 위한 옵션임.
extern float g_bTestFPSFactor; // 위의 옵션이 TRUE일 경우 적용되는 Facter 임. 기존 FPS에 본 설정이 곱해져서 처리된다.
#ifdef WIN32
extern CCriticalSection g_csEntireScreenDC; // Screen DC 접근에 대한 임계영역 처리.
#endif

///////////////////////////////////////////////////////////////////////////////
// Math
///////////////////////////////////////////////////////////////////////////////

const float MC_LOG2_INV = 1.44269504088896f;
const float MC_LOG2     = 0.69314718055995f;

enum EIGHT_DIRECTION {
   EIGHT_DIRECTION_LEFT       = 0,
   EIGHT_DIRECTION_RIGHT      = 1,
   EIGHT_DIRECTION_UP         = 2,
   EIGHT_DIRECTION_DOWN       = 3,
   EIGHT_DIRECTION_LEFTUP     = 4,
   EIGHT_DIRECTION_LEFTDOWN   = 5,
   EIGHT_DIRECTION_RIGHTUP    = 6,
   EIGHT_DIRECTION_RIGHTDOWN  = 7,
   EIGHT_DIRECTION_ZOOM_IN    = 8,
   EIGHT_DIRECTION_ZOOM_OUT   = 9,
   EIGHT_DIRECTION_FOCUS_NEAR = 10,
   EIGHT_DIRECTION_FOCUS_FAR  = 11,
};

#if _MSC_VER == 1600
inline int round( float x )
{
   return (int)( x + 0.5f );
}
inline int round( double x )
{
   return (int)( x + 0.5 );
}
float log2( float x );
#endif

float GetAngleDiff( float fAngleDeg1, float fAngleDeg2 );
float gfxGetAngle( const POINT& p1, const POINT& p2 );
int Get8Direction( const POINT& p1, const POINT& p2 );
float GetOffsetAngleInDegree( float fCurAngle, float fTgtAngle );
BOOL IsAngleContainedSE( float fStartAngleDeg, float fEndAngleDeg, float fCurPosAngleDeg );
BOOL IsAngleContainedCR( float fCenterAngelDeg, float fRangeAngleDeg, float fCurPosAngleDeg );
float GetSquaredDistance( BCCL::ILine2D& line, BCCL::IPoint2D& point );

///////////////////////////////////////////////////////////////////////////////
// Drawing
///////////////////////////////////////////////////////////////////////////////

#define NUM_CHARACTERS 96
#define FONT_WIDTH 8
#define FONT_HEIGHT 14

#define VD_COLOR_BLACK 0x00800080
#define VD_COLOR_RED 0x4CFF4C54
#define VD_COLOR_GREEN 0x9515952B
#define VD_COLOR_BLUE 0x1D6B1DFF
#define VD_COLOR_YELLOW 0xE194E100
#define VD_COLOR_CYAN 0xB200B2AB
#define VD_COLOR_MAGENTA 0x69EA69D4
#define VD_COLOR_WHITE 0xFF80FF80

void InterlacingEventOddField( BYTE* s_image, BYTE* d_image, int nWidth, int nHeight );
void ExpandOddField( BYTE* s_image, BYTE* d_image, int nWidth, int nHeight );
void ExpandOddField( BYTE* image, int nWidth, int nHeight );
void BlackOut( BCCL::BArray1D& data, BYTE y_val );
void BlackOut( byte* data, int len, BYTE y_val );
#ifdef WIN32
BOOL gfxGetBitmap( BGRImage& s_image, CBitmap& d_bitmap, int d_width, int d_height, LPCSTR szDescText = NULL );
BOOL gfxGetBitmap( byte* s_image, int width, int height, CBitmap& bitmap, int bitmap_width, int bitmap_height );
BOOL gfxCreateFont( LPCSTR szFontName, int nHeight, BOOL bBold, CFont& fontOut );
#endif

///////////////////////////////////////////////////////////////////////////////
// Utility for Common Controls & Windows
///////////////////////////////////////////////////////////////////////////////
BCCL::FPoint2D GetCenterPos( BCCL::FBox2D fbox2d );
BCCL::IPoint2D GetCenterPos( BCCL::IBox2D ibox2d );

///////////////////////////////////////////////////////////////////////////////
// std::string util
///////////////////////////////////////////////////////////////////////////////
namespace sutil {
#if 1
template <typename... Args>
std::string sformat( const std::string& format, Args... args )
{
   size_t size = snprintf( nullptr, 0, format.c_str(), args... ) + 1; // Extra space for '\0'
   std::unique_ptr<char[]> buf( new char[size] );
   snprintf( buf.get(), size, format.c_str(), args... );
   return std::string( buf.get(), buf.get() + size - 1 ); // We don't want the '\0' inside
}
//std::string sformat(const char* format, Args ... args)
//{
//   size_t size = snprintf(nullptr, 0, format, args ...) + 1; // Extra space for '\0'
//   std::unique_ptr<char[]> buf(new char[size]);
//   snprintf(buf.get(), size, format, args ...);
//   return std::string(buf.get(), buf.get() + size - 1); // We don't want the '\0' inside
//}
//std::string sformat(const char* format, ...)
//{
//   const int MAX_BUFFER_LENGTH = 512;

//   va_list	ap;
//   char		szMsg[MAX_BUFFER_LENGTH] = { 0, };

//   va_start(ap, format);
//   vsprintf(szMsg, format, ap);
//   va_end(ap);

//   return std::string(szMsg);
//}
#else
std::string sformat( const std::string& fmt, ... )
{
   int size = ( (int)fmt.size() ) * 2 + 50; // Use a rubric appropriate for your code
   std::string str;
   va_list ap;
   while( 1 ) { // Maximum two passes on a POSIX system...
      str.resize( size );
      va_start( ap, fmt );
      int n = vsnprintf( (char*)str.data(), size, fmt.c_str(), ap );
      va_end( ap );
      if( n > -1 && n < size ) { // Everything worked
         str.resize( n );
         return str;
      }
      if( n > -1 ) // Needed size returned
         size = n + 1; // For null char
      else
         size *= 2; // Guess at a larger size (OS specific)
   }
   return str;
}
#endif
std::string scurrent_time( const std::string& format );

inline std::string sleft( std::string const& source, size_t const length )
{
   if( length >= source.size() ) {
      return source;
   }
   return source.substr( 0, length );
}

inline std::string sright( std::string const& source, size_t const length )
{
   if( length >= source.size() ) {
      return source;
   }
   return source.substr( source.size() - length );
}

#define TRIM_SPACE " \t\n\v\r"
inline std::string trim( std::string& s, const std::string& drop = TRIM_SPACE )
{
   std::string r = s.erase( s.find_last_not_of( drop ) + 1 );
   return r.erase( 0, r.find_first_not_of( drop ) );
}
inline std::string rtrim( std::string s, const std::string& drop = TRIM_SPACE )
{
   return s.erase( s.find_last_not_of( drop ) + 1 );
}
inline std::string ltrim( std::string s, const std::string& drop = TRIM_SPACE )
{
   return s.erase( 0, s.find_first_not_of( drop ) );
}

void GetSplitStringList( const std::string& strIn, char ch, std::vector<std::string>& vecOut );

std::string StringToHex( const std::string& buffer );
std::string StringToHex( const uchar* buffer, size_t count );

} // namespace sutil

///////////////////////////////////////////////////////////////////////////////
// Directory & File Managument
///////////////////////////////////////////////////////////////////////////////

bool IsExistFile( const std::string& szFileName );
bool IsExistFile( const char* szFileName );
bool IsExistDirectory( const std::string& szDirName );
bool MakeDirectory( const char* szDirName );
bool MakeDirectory( const std::string& szDirName );
bool RemoveFileEx( const std::string& szFileName );
bool MoveFileEx( const std::string& szSrc, const std::string& szDst );
std::string GetExecutablePath();

//void InitLog( const std::string& strPath );
//BOOL logi( const char* data, ... );
//BOOL logi( const char* data, ... );

#ifdef WIN32
CString GetDirPathFromFullPath( LPCSTR pszPathName );
BOOL SetCurrentDirectoryWhenEmpty( CString& strDirName );
#endif

///////////////////////////////////////////////////////////////////////////////
// Time Management
///////////////////////////////////////////////////////////////////////////////

void AfxGetLocalTime( LPSYSTEMTIME lpSystemTime );
void AfxGetLocalFileTime( LPFILETIME lpFileTime );
// Getting String from Time
int GetElapsedMilliSec( FILETIME& ft1, FILETIME& ft2 );
int GetElapsedMilliSec( FILETIME& ftPeriod );
int GetElapsedSec( FILETIME& ft1, FILETIME& ft2 );
int GetElapsedSec( FILETIME& ftPeriod );

// Add To Time Format
void AddToFileTime( FILETIME& ft, int nDays, int nHours, int nMins, int nSecs, int nMiliSecs = 0, int nMicro = 0 );
// Time Format Convertion
#if defined( __SUPPORT_CTIME )
void CTimeToFileTime( CTime& tm, FILETIME& ft );
FILETIME CTimeToFileTime( CTime& tm );
#endif
void SetFileTime( FILETIME& ft, WORD wYear, WORD wMonth, WORD wDay, WORD wHour, WORD wMinute, WORD wSecond, WORD wMilliseconds, WORD wMicroSeconds );
void SetFileTimeByMilliSec( FILETIME& ft, int nMilliSec );
BOOL IsNullFileTime( FILETIME& ft );
BOOL IsLeapYear( WORD wYear );
int64_t FrameTimeToTimeStamp( AVRational StrmTimeBase, LPFILETIME lpftFrameTime, LPFILETIME lpftBase = NULL ); // (bluecnt-20160104MON)
// Get Days Functions
WORD GetDaysInMonth( SYSTEMTIME& st );
WORD GetDaysInMonth( const BCCL::Time& tm );
WORD GetDaysOfYear( const BCCL::Time& tm );

// Time Compare Functions
BOOL operator<( const FILETIME& ftA, const FILETIME& ftB );
BOOL operator<=( const FILETIME& ftA, const FILETIME& ftB );
BOOL operator>( const FILETIME& ftA, const FILETIME& ftB );
BOOL operator>=( const FILETIME& ftA, const FILETIME& ftB );
BOOL operator==( const FILETIME& ftA, const FILETIME& ftB );
BOOL operator!=( const FILETIME& ftA, const FILETIME& ftB );
FILETIME operator-( const FILETIME& ftA, const FILETIME& ftB );
FILETIME operator+( const FILETIME& ftA, const FILETIME& ftB );
FILETIME operator+=( FILETIME& ftA, FILETIME& ftB );
FILETIME operator-=( FILETIME& ftA, FILETIME& ftB );
FILETIME operator-( FILETIME& ftA );
FILETIME operator*( FILETIME& ft, float fa );
FILETIME operator/( FILETIME& ft, float fa );

void FileTimeToTimeval(FILETIME *pFileTime, struct timeval *pTimeval);
void TimevalToFileTime(struct timeval *pTimeval, FILETIME *pFileTime);

#if WIN32
///////////////////////////////////////////////////////////////////////////////
// Utility for Common Controls & Windows
///////////////////////////////////////////////////////////////////////////////

// Combo Box
BOOL SetCurSel_ByData( CComboBox* pComboBox, int& nData );
BOOL SetCurSel_ByData( CComboBox* pComboBox, float& fData );
BOOL SetCurSel_ByIndex( CComboBox* pComboBox, int& nIdx );
BOOL SetCurSel_ByText( CComboBox* pComboBox, CString& strText );
BOOL GetCurData( CComboBox* pComboBox, int& nData );
BOOL GetCurData( CComboBox* pComboBox, float& fData );
BOOL GetCurData( CComboBox* pComboBox, CString& strData );
BOOL AddComboBoxItem( CComboBox* pComboBox, const CString& strItemStr, int nData );
BOOL AddComboBoxItem( CComboBox* pComboBox, const CString& strItemStr, float fData );
// Edit Box
void GetInt( CEdit* pEdit, int& nData );
void SetStr( CEdit* pEdit, int nData );

// Window
BOOL IsShowWindow( CWnd* pWnd );
void GetWndRect( CRect& rect, CWnd* pParent, CWnd* pChild );
void AFXAPI DDX_Text( CDataExchange* pDX, int nIDC, std::string& str );

void gfxGetBoundaryRgn( CRect rcSrc, CRgn* rgn, int nThick );
CRect gfxGetFittingRect( CRect rect, CSize s_size );
CRect gfxGetFittingRect( CRect rect, int width, int height );
void gfxGetFittingRect( CRect rect, int width, int height, CRect& d_rect );

#endif

///////////////////////////////////////////////////////////////////////////////
// Memory Checking
///////////////////////////////////////////////////////////////////////////////
#ifdef WIN32
BOOL IsPhysMemAvail( DWORD nRequiredFreePhysMem );
DWORD GetAvailPhysMemSize();
DWORD GetTotalPhysMemSize();
DWORD GetAvailVirtMemSize();
DWORD GetTotalVirtMemSize();
DWORD GetAvailPhysMemSizeInMB();
DWORD GetTotalPhysMemSizeInMB();
DWORD GetAvailVirtMemSizeInMB();
DWORD GetTotalVirtMemSizeInMB();
#endif
///////////////////////////////////////////////////////////////////////////////
//
// Etc Functions
//
///////////////////////////////////////////////////////////////////////////////

BOOL IsDiff( void* val1, void* val2, int nSize );
BOOL GetFPoint3D( BCCL::FPoint3D& p3d, const std::string& strIn );
#ifdef WIN32
std::string GetExceptionCodeString( int nExceptionCode );
#endif
void ShiftLeftBuff( BCCL::FileIO& buff, int shift_size );
BOOL IsValidIPAddress( const std::string& strIPAddress );
#ifdef WIN32
std::string ReadMyIP();
#endif
std::string CreateGUID();
RECT GetCalcScale( const RECT& rt, float scale_x, float scale_y );
RECT ConvertRect( const BCCL::IBox2D& box );

struct __StreamItem {
   int nStreamIdx;
   int nSqrtArea;
};

int GetStreamIdxOfMostSimillarInSize( std::vector<__StreamItem>& vtStreamItemArr, const BCCL::ISize2D& imgSize, float fDispQuality = 0.5f );

// get string
std::string GetString( BCCL::FPoint3D p3d );
BOOL GetIPAddress( DWORD& nIPAddressOut, const std::string& strIn );
BOOL GetIPAddrString( std::string& strOut, DWORD nIPAddressIn );
std::string GetIPAddrString( DWORD nIPAddressIn );
BOOL GetIPAddrString( std::string& strOut, const std::string& strIn ); // (xinu_ba18)
std::string GetIPAddrString( const std::string& strIn );
DWORD GetIPAddr( const std::string& strIPAddressIn );

// state...
void ModifyState( uint& nState, uint nAdd, uint nRemove );
int Convert32BitFlagToInt( uint n32BitFlag );

template <class T>
BOOL CheckStateChange( T nPrevState, T nCurState, T nFlag, int bStateON )
{
   if( bStateON ) {
      if( !( nPrevState & nFlag ) && ( nCurState & nFlag ) )
         return TRUE;
   } else {
      if( ( nPrevState & nFlag ) && !( nCurState & nFlag ) )
         return TRUE;
   }
   return FALSE;
}

template <class T>
BOOL CheckStateChange( T nPrevState, T nCurState, T nFlag )
{
   if( !( nPrevState & nFlag ) && ( nCurState & nFlag ) )
      return TRUE;
   if( ( nPrevState & nFlag ) && !( nCurState & nFlag ) )
      return TRUE;
   return FALSE;
}

#ifdef __linux
class CSetKeyboardNonBlock
{
private:
   struct termios* m_setting = nullptr;

public:
   CSetKeyboardNonBlock();
   ~CSetKeyboardNonBlock();

   void Restore();
   void Set();
};
#endif
///////////////////////////////////////////////////////////////////////////////
//
// CIDToString
//
///////////////////////////////////////////////////////////////////////////////

class CIDToString {
public:
   int m_nID;
   std::string m_String;

public:
   CIDToString();
   CIDToString( int nID, const std::string& str )
   {
      m_nID    = nID;
      m_String = str;
   };
};

///////////////////////////////////////////////////////////////////////////////
//
// CMapIDToString
//
///////////////////////////////////////////////////////////////////////////////

class CMapIDToString : public std::vector<CIDToString> {
public:
   std::string GetString( int nID );
   int GetID( const std::string& str );
};

///////////////////////////////////////////////////////////////////////////////
//
// Class : CPerformanceCounter
//
///////////////////////////////////////////////////////////////////////////////
class CPerformanceCounter {
protected:
   chrono_tp m_previous_clock;

public:
   CPerformanceCounter()
   {
      CheckTimeStart();
   }

public:
   void CheckTimeStart()
   {
      m_previous_clock = std::chrono::high_resolution_clock::now();
   }

   double GetElapsedTime()
   {
      chrono_duration_milli elapsed = std::chrono::high_resolution_clock::now() - m_previous_clock;
      return elapsed.count();
   }
};

//#pragma pack(1)

///////////////////////////////////////////////////////////////////////////////
//
// Struct: COMMONTIME
//
///////////////////////////////////////////////////////////////////////////////

struct COMMONTIME

{
   uint low_time; // low 32 bits
   uint high_time; // high 32 bits
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: COMMONID
//
///////////////////////////////////////////////////////////////////////////////

class COMMONID

{
public:
   uint id;
   COMMONTIME time;

public:
   COMMONID();

public:
   int ReadFile( BCCL::CXMLIO* pIO );
   int WriteFile( BCCL::CXMLIO* pIO );
};

//#pragma pack()

//////////////////////////////////////////////////////////////////////////

// arr의 분산을 구함
template <typename T>
float GetVariance( T& arr, float fAvg, int nWidth, int nHeight, int nCnt )
{
   float fVariance = 0.0f;
   int i, j;
   for( i = 0; i < nHeight; i++ ) {
      for( j = 0; j < nWidth; j++ ) {
         if( arr[i][j] != 0 ) {
            fVariance += ( (float)arr[i][j] - fAvg ) * ( (float)arr[i][j] - fAvg );
         }
      }
   }
   fVariance /= nCnt;

   return fVariance;
}

/////////////////////////////////////////////////////////////////////////////
//
// class CStreamSocketEx
//
/////////////////////////////////////////////////////////////////////////////

class CStreamSocketEx : public BCCL::StreamSocket {
public:
   int ErrorCode;

public:
   CStreamSocketEx();

   int Create();
   int Bind( const char* ip_addr, ushort port );
   int ConnectNB( const char* ip_addr, ushort port );
};

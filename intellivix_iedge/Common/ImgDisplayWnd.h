#pragma once
#ifdef __WIN32
#include "bccl_polygon.h"

#define SW_MSG_UPDATE_PARAMETERS   (WM_USER + 106)

//////////////////////////////////////////////////////////////////////////////
//
// Class : CSpecWndMessageHandler
//
//////////////////////////////////////////////////////////////////////////////


class CImgDisplayWnd : public CWnd
{
public:
   int      Width;
   int      Height;

protected:
   CWnd*          ParentWnd;
   CBitmap        Bitmap;

public:
   IPoint2D       STDMapToWnd(IPoint2D& p);
   CPoint         STDMapToWnd(CPoint& p);
   IPoint2D       WndToSTDMap(IPoint2D& p);
   CPoint         WndToSTDMap(CPoint& p);

public:
   virtual void RenderSub (CDC *dc) {   };
   virtual BOOL Create    (CRect& rect,CWnd *parent_wnd,BGRImage &s_image);

public:
   void DrawArrow     (CDC *dc,IPoint2D &s_pos,float direction,float length,COLORREF color);
   void DrawEllipse   (CDC *dc,int sx,int sy,int ex,int ey,COLORREF color,int thickness);
   void DrawEllipse   (CDC *dc,IBox2D &s_box,COLORREF color,int thickness);
   void DrawHeight    (CDC *dc,IBox2D &s_box,COLORREF color,int thickness);
   void DrawLine      (CDC *dc,int sx,int sy,int ex,int ey,COLORREF color,int thickness);
   void DrawLine      (CDC *dc,IPoint2D &s_pos,IPoint2D &e_pos,COLORREF color,int thickness);
   void DrawLine      (CDC *dc,ILine2D &s_line,COLORREF color,int thickness);
   void DrawPie       (CDC *dc,IPoint2D &c_pos,float direction,float range,float radius,COLORREF color);
   void DrawPoint     (CDC *dc,IPoint2D &pos,COLORREF color);
   void DrawPolygon   (CDC *dc,BCCL::Polygon *polygon,COLORREF color,int brush_style,int flag_selected = FALSE);
   void DrawRectangle (CDC *dc,int sx,int sy,int ex,int ey,COLORREF color,int thickness,int brush_style = -1);
   void DrawRectangle (CDC *dc,IBox2D &s_box,COLORREF color,int thickness,int brush_style = -1);
   void DrawText      (CDC *dc,const char *text,int x,int y,COLORREF color);
   void DrawWidth     (CDC *dc,IBox2D &s_box,COLORREF color,int thickness);
   void Render        (   );
   void Render        (CDC *dc);
   void Render        (BGRImage &s_image);

protected:
   afx_msg void OnPaint  (   );

   DECLARE_MESSAGE_MAP (   )
};
#endif

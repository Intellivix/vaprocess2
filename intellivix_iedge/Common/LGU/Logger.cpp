﻿#include "Logger.h"
#include "bccl_plog.h"
#include <cstdarg>
#include "BaseDirManager.h"

namespace LGU {

static std::string from_service_ = "";
static std::string to_service_ = "";
static std::string camera_pid_ = "";
static std::string command_ = "";
static std::string process_started_time_ = "";
static std::string process_ended_time_ = "";
static std::string Log_Write_Path_ = "";

Logger::Logger()
{
}

void Logger::initWriteToFile( plog::Severity log_level, const std::string& filename, int count_of_rolling_files )
{
   std::string logFileName;

   if( filename.empty() ) {
      logFileName = "ivx_lgu.log";
   } else {
      logFileName = filename;
   }
   
   // to write files rolling by size.
   /*
   uint a_file_size_is_one_mega_bytes = 1 * 1024 * 1024;
   static plog::RollingFileAppender<plog::MessageOnlyFormatter> fileAppende(
       logFileName.c_str(), a_file_size_is_one_mega_bytes, count_of_rolling_files );
   plog::init<L>( log_level, &fileAppende );
   */
   static plog::IvxColorConsoleAppender<plog::IvxConsoleFormatter> consoleAppender;
   uint a_file_size_is_one_mega_bytes = 1 * 1024 * 1024;
   static plog::RollingFileAppender<plog::MessageOnlyFormatter> fileAppende(
       logFileName.c_str(), a_file_size_is_one_mega_bytes, count_of_rolling_files );
   plog::init<L>( log_level, &fileAppende );

}

void Logger::initDisplayAndWriteToFile( plog::Severity log_level, const std::string& filename, int count_of_rolling_files )
{
   std::string logFileName;
         
   if( filename.empty() ) {
      logFileName = "ivx_lgu.log";
   } else {
      logFileName = filename;
   }

   static plog::IvxColorConsoleAppender<plog::IvxConsoleFormatter> consoleAppender;
   uint a_file_size_is_one_mega_bytes = 1 * 1024 * 1024;
   static plog::RollingFileAppender<plog::MessageOnlyFormatter> fileAppender(
       logFileName.c_str(), a_file_size_is_one_mega_bytes, count_of_rolling_files );

   // to use both. use addAppender() instaed of init() with consoleAppender.
   plog::init<L>( log_level, &fileAppender ).addAppender( &consoleAppender );
}

void Logger::initDisplayConsole( plog::Severity log_level )
{
   static plog::IvxColorConsoleAppender<plog::IvxConsoleFormatter> consoleAppender;
   plog::init<L>( log_level, &consoleAppender );
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-nonliteral"
std::string Logger::toStdString( std::string format, ... )
{
   va_list args, args_copy;
   va_start( args, format );
   va_copy( args_copy, args );

   int sz         = vsnprintf( nullptr, 0, format.c_str(), args );
   size_t max_len = static_cast<size_t>( sz ) + 1;
   try {
      std::string result( max_len, ' ' );
      vsnprintf( &result.front(), max_len, format.c_str(), args_copy );
      va_end( args_copy );
      va_end( args );
      return result;
   } catch( const std::bad_alloc& ) {
      va_end( args_copy );
      va_end( args );
      throw;
   }
}

std::string Logger::GetCycleLogFilePath (std::string file_path)
{
   std::string log_full_path_ = "";
   BaseDirManager& bdm = BaseDirManager::Get();
   //const std::string& cam_pid_name = bdm.get_multil_proces_id();
   // 5min save
   if (file_path.empty())
      file_path = bdm.get_excutable_dir();
   
   std::string saveFilePath = file_path + "/" + LGU::Logger::get_Current_TM(GCT_ONLY_DATE);
   if(IsExistDirectory( saveFilePath ) == false) 
      MakeDirectory( saveFilePath );
   
   //get cam cid

   log_full_path_ = saveFilePath + "/" + LGU::Logger::get_Current_TM(GCT_WITHOUT_SEC) +".log";
   
   return log_full_path_;
}

std::string Logger::GetFirstLogFilePath (int nMinCycle, std::string file_path)
{
   BaseDirManager& bdm = BaseDirManager::Get();
   //const std::string&  cam_pid_name = bdm.get_multil_proces_id();

   if (file_path.empty())
      file_path = bdm.get_excutable_dir();
   
   std::string saveFilePath = file_path + "/" + LGU::Logger::get_Current_TM(GCT_ONLY_DATE);
   if(IsExistDirectory( saveFilePath ) == false) 
      MakeDirectory( saveFilePath );
   
   struct tm time_tm;
   plog::util::Time time_plog;
   plog::util::ftime( &time_plog );
   plog::util::localtime_s( &time_tm, &time_plog.time );

   plog::util::nostringstream ss_date;
   ss_date << std::setfill( PLOG_NSTR( '0' ) )
           << std::setw( 4 ) << time_tm.tm_year + 1900
           << std::setw( 2 ) << time_tm.tm_mon + 1
           << std::setw( 2 ) << time_tm.tm_mday;
   std::string date = ss_date.str();

   int fixedMinute = (time_tm.tm_min / nMinCycle) * nMinCycle;
   
   plog::util::nostringstream ss_time;
   ss_time << std::setfill( PLOG_NSTR( '0' ) ) << std::setw( 2 ) << time_tm.tm_hour
           << std::setw( 2 ) << fixedMinute;
   std::string time = ss_time.str();
   std::string strStartTime = date + time;
   std::string log_full_path_ = saveFilePath + "/" + strStartTime +".log";
   LOGE << "full path : " << log_full_path_;
   return log_full_path_;
}

#pragma GCC diagnostic pop

//LGUPlus file save format
std::string Logger::HeaderEventLog (int header_id , EventEx* event)
{
   if (!event) return "";
   
   std::string Currentdate = get_Current_TM(GCT_ALL);
   plog::util::nostringstream ss;

   //blue box
   writeItem( ss, SEQ_ID      , Currentdate + random_uint( 0, 99999999, 8 ) );
   writeItem( ss, LOG_TIME    , Currentdate );
   writeItem( ss, LOG_TYPE    , "");
   writeItem( ss, SID         , "" );
   writeItem( ss, RESULT_CODE , std::to_string( header_id ) );
   writeItem( ss, REQ_TIME    , Currentdate );
   writeItem( ss, RSP_TIME    , Currentdate );
   writeItem( ss, CLIENT_IP   , "" );
   writeItem( ss, OS_INFO     , "" );
   writeItem( ss, NW_INFO     , "" );
   writeItem( ss, SVC_NAME    , "" );
   writeItem( ss, DEV_INFO    , "" );
   writeItem( ss, CARRIER_TYPE, "E" );

   BaseDirManager& bdm = BaseDirManager::Get();
   camera_pid_ = bdm.get_multil_proces_id();
   
   //general box
   writeItem( ss, FROM_SERVICE_NAME    , from_service_ );
   writeItem( ss, CAM_ID               , camera_pid_ );
   writeItem( ss, COMMAND              , command_ );
   writeItem( ss, RES_SERVICE_NAME     , to_service_ );
   writeItem( ss, DB_REQ_TIME          , "" );
   writeItem( ss, DB_RES_TIME          , "" );
   writeItem( ss, DB_RESULT_CODE       , "" );
   writeItem( ss, VA_PROCESS_CALL_TIME , process_started_time_ );
   writeItem( ss, VA_PROCESS_TERM_TIME , process_ended_time_ );
   writeItem( ss, EVENT_TYPE           , std::to_string(event->Type) );
   writeItem( ss, HEADER_NONE, "\n" );
   /*
   writeItem( ss, EVENT_ID     , to_service_ );
   writeItem( ss, EVENT_STATUS         , std::to_string(event->Status) );
   
   if( event->Status & EV_STATUS_BEGUN ) {
      if( IsNullFileTime( event->Time_EventBegun ) )
         AfxGetLocalFileTime(&event->Time_EventBegun);
      
      std::string start_time = sutil::sformat("%04d_%02d%02d_%02d%02d%02d_%03d", 
                                              event->StartTime.Year, event->StartTime.Month, event->StartTime.Day, event->StartTime.Hour, event->StartTime.Minute, event->StartTime.Second, event->StartTime.Millisecond);
      writeItem( ss, EVENT_STARTED_TIME   , start_time );
      writeItem( ss, EVENT_ENDED_TIME     , "" );

   } else if( event->Status & EV_STATUS_ENDED ) {
      std::string start_time = sutil::sformat("%04d_%02d%02d_%02d%02d%02d_%03d", 
                                              event->StartTime.Year, event->StartTime.Month, event->StartTime.Day, event->StartTime.Hour, event->StartTime.Minute, event->StartTime.Second, event->StartTime.Millisecond);
      writeItem( ss, EVENT_STARTED_TIME   , start_time );
      std::string end_time = sutil::sformat("%04d_%02d%02d_%02d%02d%02d_%03d", 
                                              event->EndTime.Year, event->EndTime.Month, event->EndTime.Day, event->EndTime.Hour, event->EndTime.Minute, event->EndTime.Second, event->EndTime.Millisecond);
      
      writeItem( ss, EVENT_ENDED_TIME     , end_time );
   }
   
   if(event->EvtZone){
      writeItem( ss, EVENT_ZONE_ID , std::to_string(event->EvtZone->ID) );
      writeItem( ss, EVENT_ZONE_NAME , event->EvtZone->Name );
   }
*/
   //empty value
   from_service_ = "";  
   to_service_   = "";  
   camera_pid_   = "";  
   command_      = "";  
   process_started_time_  = "";
   process_ended_time_    = "";
   
   // minus 5min
   if (!Log_Write_Path_.empty()) {  
      LOGW << "File save : " << Log_Write_Path_;
      std::fstream time_file;
      time_file.open( Log_Write_Path_ , std::fstream::in | std::fstream::out | std::fstream::app);
      time_file << ss.str();
      time_file.close();
   }
   
   return ss.str();
}

std::string Logger::header( int retCode )
{
   std::string Currentdate = get_Current_TM(GCT_ALL);
   plog::util::nostringstream ss;

   //blue box
   writeItem( ss, SEQ_ID      , Currentdate + random_uint( 0, 99999999, 8 ) );
   writeItem( ss, LOG_TIME    , Currentdate );
   writeItem( ss, LOG_TYPE    , "");
   writeItem( ss, SID         , "" );
   writeItem( ss, RESULT_CODE , std::to_string( retCode ) );
   writeItem( ss, REQ_TIME    , Currentdate );
   writeItem( ss, RSP_TIME    , Currentdate );
   writeItem( ss, CLIENT_IP   , "" );
   writeItem( ss, OS_INFO     , "" );
   writeItem( ss, NW_INFO     , "" );
   writeItem( ss, SVC_NAME    , "" );
   writeItem( ss, DEV_INFO    , "" );
   writeItem( ss, CARRIER_TYPE, "" );
   
   BaseDirManager& bdm = BaseDirManager::Get();
   camera_pid_ = bdm.get_multil_proces_id();
   
   //general box
   writeItem( ss, FROM_SERVICE_NAME    , from_service_ );
   writeItem( ss, CAM_ID               , camera_pid_ );
   writeItem( ss, COMMAND              , command_ );
   writeItem( ss, RES_SERVICE_NAME     , to_service_ );
   writeItem( ss, DB_REQ_TIME          , "" );
   writeItem( ss, DB_RES_TIME          , "" );
   writeItem( ss, DB_RESULT_CODE       , "" );
   writeItem( ss, VA_PROCESS_CALL_TIME , process_started_time_ );
   writeItem( ss, VA_PROCESS_TERM_TIME , process_ended_time_ );
   writeItem( ss, EVENT_TYPE           , "" );
   writeItem( ss, HEADER_NONE, "\n" );

   //empty value
   from_service_ = "";  
   to_service_   = "";  
   camera_pid_   = "";  
   command_      = "";  
   process_started_time_  = "";
   process_ended_time_    = "";
   
   // minus 5min
   if (!Log_Write_Path_.empty()) {  
      std::fstream time_file;
      time_file.open( Log_Write_Path_ , std::fstream::in | std::fstream::out | std::fstream::app);
      time_file << ss.str();
      time_file.close();
   }
   
   return ss.str();
}

void Logger::SetWriteLogPath (std::string file_path)
{
   Log_Write_Path_ = file_path;
}

void Logger::SetHeaderItem (int header_id, std::string value)
{
   switch(header_id) {
     case FROM_SERVICE_NAME:    from_service_ = value;   break;
     case RES_SERVICE_NAME:     to_service_   = value;   break;
     case CAM_ID:               camera_pid_   = value;   break;
     case COMMAND:              command_      = value;   break;
     case VA_PROCESS_CALL_TIME: process_started_time_  = get_Current_TM(GCT_ALL); break;
     case VA_PROCESS_TERM_TIME: process_ended_time_    = get_Current_TM(GCT_ALL); break;
     default:                   break;      
   }
}

void Logger::writeItem( std::ostringstream& ss, int header_id, std::string value )
{
   if (header_id == HEADER_NONE)   {
      ss << value; 
      return;
   }
   
   std::string header = "";
   switch(header_id) {
     case SEQ_ID:               header = "SEQ_ID";               break;
     case LOG_TIME:             header = "LOG_TIME";             break;
     case LOG_TYPE:             header = "LOG_TYPE";             break;
     case SID:                  header = "SID";                  break;
     case RESULT_CODE:          header = "RESULT_CODE";          break;
     case REQ_TIME:             header = "REQ_TIME";             break;
     case RSP_TIME:             header = "RSP_TIME";             break;
     case CLIENT_IP:            header = "CLIENT_IP";            break;
     case DEV_INFO:             header = "DEV_INFO";             break;
     case OS_INFO:              header = "OS_INFO";              break;
     case NW_INFO:              header = "NW_INFO";              break;
     case SVC_NAME:             header = "SVC_NAME";             break;
     case DEV_MODEL:            header = "DEV_MODEL";            break;
     case CARRIER_TYPE:         header = "CARRIER_TYPE";         break;
     case FROM_SERVICE_NAME:    header = "FROM_SERVICE_NAME";    break;
     case CAM_ID:               header = "CAM_ID";               break;
     case COMMAND:              header = "COMMAND";              break;
     case RES_SERVICE_NAME:     header = "RES_SERVICE_NAME";     break;
     case DB_REQ_TIME:          header = "DB_REQ_TIME";          break;
     case DB_RES_TIME:          header = "DB_RES_TIME";          break;
     case DB_RESULT_CODE:       header = "DB_RESULT_CODE";       break;
     case VA_PROCESS_CALL_TIME: header = "VA_PROCESS_CALL_TIME"; break;
     case VA_PROCESS_TERM_TIME: header = "VA_PROCESS_TERM_TIME"; break;
     case EVENT_TYPE:           header = "EVENT_TYPE";           break;
     case EVENT_ID:             header = "EVENT_ID";             break;
     case EVENT_STARTED_TIME:   header = "EVENT_STARTED_TIME";   break;
     case EVENT_ENDED_TIME:     header = "EVENT_ENDED_TIME";     break;
     case EVENT_STATUS:         header = "EVENT_STATUS";         break;
     case EVENT_ZONE_ID:        header = "EVENT_ZONE_ID";        break;
     case EVENT_ZONE_NAME:      header = "EVENT_ZONE_NAME";      break;
     default:                   header = "_ERROR_";              break;      
   }
   

   if( ss.str().size() > 0 ) {
      ss << "|";
   }
   
   ss << header << "=" << value;
}

std::string Logger::random_uint( uint min, uint max, int digit_count )
{
   std::random_device rd;
   std::default_random_engine generator( rd() );
   std::uniform_int_distribution<uint> random_uint( min, max );

   std::stringstream ss;
   ss << std::setfill( '0' ) << std::setw( digit_count ) << random_uint( generator );

   return ss.str();
}

std::string Logger::get_Current_TM( int nGCTTYpe )
{
   struct tm time_tm;
   plog::util::Time time_plog;
   plog::util::ftime( &time_plog );
   plog::util::localtime_s( &time_tm, &time_plog.time );

   plog::util::nostringstream ss_date;
   ss_date << std::setfill( PLOG_NSTR( '0' ) )
           << std::setw( 4 ) << time_tm.tm_year + 1900
           << std::setw( 2 ) << time_tm.tm_mon + 1
           << std::setw( 2 ) << time_tm.tm_mday;
   std::string date = ss_date.str();

   plog::util::nostringstream ss_time;
   ss_time << std::setfill( PLOG_NSTR( '0' ) ) << std::setw( 2 ) << time_tm.tm_hour
           << std::setw( 2 ) << time_tm.tm_min
           << std::setw( 2 ) << time_tm.tm_sec;
   std::string time = ss_time.str();
   
   plog::util::nostringstream ss_time_normal;
   ss_time_normal << std::setfill( PLOG_NSTR( '0' ) ) << std::setw( 2 ) << time_tm.tm_hour
           << std::setw( 2 ) << time_tm.tm_min;
   std::string time_without_sec = ss_time_normal.str();
   
   plog::util::nostringstream ss_millisec;
   ss_millisec << std::setw( 3 ) << time_plog.millitm;
   std::string millisec = ss_millisec.str();
   
   std::string result_date = date + time + millisec;

   if(nGCTTYpe == GCT_ONLY_DATE) {
      result_date = date;
   }
   else if(nGCTTYpe == GCT_WITHOUT_MIL_SEC) {
     result_date = date + time;
   }
   else if(nGCTTYpe == GCT_WITHOUT_SEC){
      result_date = date + time_without_sec;
   }

   return result_date;
}


} // namespace LGU

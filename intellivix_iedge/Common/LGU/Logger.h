﻿#ifndef LOGGER_H
#define LOGGER_H

#include <string>
#include <plog/Log.h>
#include <plog/Appenders/ConsoleAppender.h>
#include <plog/Formatters/FuncMessageFormatter.h>
#include <plog/Appenders/RollingFileAppender.h>
#include "VACEx.h"
#include <fstream>

enum en_plog_company {
   L = 1,
};

enum CGCTTYpe_id {
      GCT_ALL              = 0   ,
      GCT_ONLY_DATE        = 1   ,
      GCT_WITHOUT_MIL_SEC  = 2   ,
      GCT_WITHOUT_SEC      = 3
};
enum id {
      HEADER_NONE          = 0   ,
      SEQ_ID               = 1   ,
      LOG_TIME             = 2   ,
      LOG_TYPE             = 3   ,
      SID                  = 4   ,
      RESULT_CODE          = 5   ,
      REQ_TIME             = 6   ,
      RSP_TIME             = 7   ,
      CLIENT_IP            = 8   ,
      DEV_INFO             = 9   ,
      OS_INFO              = 10  ,
      NW_INFO              = 11  ,
      SVC_NAME             = 12  ,
      DEV_MODEL            = 13  ,
      CARRIER_TYPE         = 14  ,
      FROM_SERVICE_NAME    = 15  , 
      CAM_ID               = 16  ,      
      COMMAND              = 17  ,     
      RES_SERVICE_NAME     = 18  ,
      DB_REQ_TIME          = 19  ,
      DB_RES_TIME          = 20  ,           
      DB_RESULT_CODE       = 21  ,
      VA_PROCESS_CALL_TIME = 22  ,
      VA_PROCESS_TERM_TIME = 23  ,
      EVENT_TYPE           = 24  ,
      EVENT_ID             = 25  ,
      EVENT_STARTED_TIME   = 26  ,
      EVENT_ENDED_TIME     = 27  ,
      EVENT_STATUS         = 28  ,
      EVENT_ZONE_ID        = 29  ,
      EVENT_ZONE_NAME      = 30  ,
   };

namespace LGU {
class Logger {
   
public:
   Logger();

public:
   static std::string toStdString( std::string format... );
   static void initWriteToFile( plog::Severity log_level = plog::info, const std::string& filename = nullptr, int count_of_rolling_files = 10 );
   static void initDisplayAndWriteToFile( plog::Severity log_level, const std::string& filename, int count_of_rolling_files = 10 );
   void initDisplayConsole(plog::Severity log_level);
   static void SetHeaderItem (int header_id, std::string value = "");
   static std::string GetFirstLogFilePath (int nMinCycle, std::string file_path = ""); 
   static std::string GetCycleLogFilePath (std::string file_path = ""); 
   static void SetWriteLogPath (std::string file_path = "");
   
public:
   static std::string header( int retCode );
   static std::string HeaderEventLog (int header_id , EventEx* event);
   static void writeItem( std::ostringstream& ss, int header_id, std::string value = "" );
   static std::string random_uint( uint min, uint max, int digit_count );
   static std::string get_Current_TM( int nGCTTYpe );
};


} // namespace LGU

#endif // LOGGER_H

﻿#pragma once
#ifdef WIN32
#pragma warning( disable : 4099 )
#pragma warning( disable : 4204 )

#include <afxdisp.h>
#include <mmsystem.h>
#include <afxsock.h>
#include <afxmt.h>
#endif
#include "bccl.h"
#define __VER_ADVANCED

#include <map>
#include <stack>
#include <deque>
#include <list>
#include <vector>
#include <queue>
#include <algorithm>
#include <functional>

#include "AVCodec.h"
//#include "Clock.h"
#include "ColorDef.h"
#include "CommonUtil.h"
#include "TestObject.h"
#include "MMTimer.h"
//#include "WinCryptor.h"
#include "EventEx.h"

const int PresetBitmapWidth  = 96;
const int PresetBitmapHeight = 66;
const int PresetImageNum     = 100; // 중거리

#define REF_IMAGE_WIDTH 1280
#define REF_IMAGE_HEIGHT 720

enum SICode {
   //SICode_2010_KoreaExpresswayCorp_BMT  = 1,  // 2011년초 한국도로공사 BMT
   SICode_2010_ChosunRoyalTomb          = 2, // 2010년 조선왕릉 프로젝트
   SICode_2009_GangneungCityHall        = 3, // 2010년 강릉 시청 무인 방범 프로젝트.
   SICode_2011_Jeju_InPeg               = 4, // 2011년 제주도 서귀포시 무인방법, 인펙비전의 번호판인식 카메라와 연동됨.
   SICode_LGCNS_SmartIDP                = 5, // LGCNS SmartIDP 프로젝트.
   SICode_2009_Jeju                     = 6, // 2009년 제주해양경찰정
   SICode_2011_ChosunDynastyPalace      = 7, // 5대궁 - 마스터/슬레이브
   SICode_Innodep_PTZCtrlLink           = 8, // 이노뎁 PTZ제어권 외부연동
   SICode_Omnicast_PTZCtrlLink          = 9, // 옴니케스트 PTZ제어권 외부연동
   SICode_2013_Daegu_Subway_StaBuild    = 10, // 대구 지하철 역사내 리얼허브 연동// Support RealHub
   SICode_SecurityCenter_PTZCtrlLink    = 11, // 시큐리티센터 PTZ제어권 외부연동
   SICode_2013_LongMediumDistanceCamera = 12, // 2013년 중장거리 카메라 BMT
   //SICode_2013_LG_SPAO_PeopleCounting   = 13, // LGU+ SPAO
   SICode_2013_WesternPowerPlentProject = 14, // 서부발전소 프로젝트, 센서+영상분석 연동
   SICode_2013_GOP_S1_BMT               = 15, // GOP
   SICode_2013_Samsung_SDS_In_Russia    = 16, // 러시아 MGTS 프로젝트
   SICode_2013_RealHub_ArmStateAll      = 17,
   SICode_2015_Busan_Woongdong          = 18,
};

extern int g_nSICode;

#pragma once

////////////////////////////////////////////////////////////////////
//
// Class: CTestObject
//
////////////////////////////////////////////////////////////////////

 class CTestObject

{
   public:
      int      m_nColor;
      BOOL     m_bCreate;
      int      m_nCount;
      int      m_nCount_ToBeCreate;
      int      m_nCount_Alive;
      FPoint2D m_LeftTop;
      ISize2D  m_Size;
      ISize2D  m_ImgSize;
      FPoint2D m_MovDir;
      
   public:
      CTestObject (   );
      
   public:
      void Init     (ISize2D &img_size);
      void Create   (   );
      void Delete   (   );
      void Move     (   );
      void MainProc (byte* pImage);
      void Draw     (byte* pImage);
};

// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 및 프로젝트 관련 포함 파일이
// 들어 있는 포함 파일입니다.
//

#pragma once

#ifdef __WIN32

#pragma warning( disable : 4819 )
#pragma warning( disable : 4221 )
#pragma warning( disable : 4006 )
#pragma warning( disable : 4996 )

//#if _MSC_VER > 1400
//#define WINVER 0x0501 // Windows XP
//#define _WIN32_WINNT 0x0501
//#endif

#define WIN32_LEAN_AND_MEAN // 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS // 일부 CString 생성자는 명시적으로 선언됩니다.

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN // 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
#endif

#define _CRT_SECURE_NO_WARNINGS

//#include <afx.h>
//#include <afxwin.h>         // MFC core and standard components
//#include <afxmt.h>
//#include <afxpriv.h>
//#include <afxsock.h>
//#include <afxext.h>         // MFC extensions
//#include <afxdisp.h>        // MFC Automation classes

//#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
//#include <afxcmn.h>			// MFC support for Windows Common Controls

#ifndef _AFX_NO_OLE_SUPPORT
#endif // _AFX_NO_OLE_SUPPORT

#ifndef _AFX_NO_AFXCMN_SUPPORT
#endif // _AFX_NO_AFXCMN_SUPPORT

//#include "Windows.h"

#endif // __WIN32

#include "bccl_environ.h"
#include "Common.h"
#include "SupportFunction.h"

#ifndef ASSERT
#define ASSERT( x )
#endif // ASSERT

#ifndef ZeroMemory
#define ZeroMemory( Destination, Length ) memset( ( Destination ), 0, ( Length ) )
#endif // ZeroMemory

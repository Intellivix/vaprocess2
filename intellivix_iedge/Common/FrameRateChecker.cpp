#include "StdAfx.h"
#include "FrameRateChecker.h"

/////////////////////////////////////////////////////////////
//
// Class : CFrameRateChecker
//
/////////////////////////////////////////////////////////////

CFrameRateChecker::CFrameRateChecker( void )
{
   m_bDelay           = FALSE;
   m_pDelayTimeBuffer = nullptr;
}

CFrameRateChecker::~CFrameRateChecker( void )
{
   if( m_pDelayTimeBuffer )
      delete[] m_pDelayTimeBuffer;
}

void CFrameRateChecker::Initialize( float fFrameRate, int nCheckPeriodInSec, float fFrameRateUpdateRate )
{
   m_bDelay               = FALSE;
   m_nCurrIdx             = 0;
   m_nTotalDelayTime      = 0;
   m_nAverageDelayTime    = 0;
   m_nDelayTimeCount      = 0;
   m_fRealFrameRate       = fFrameRate;
   m_fFrameRateUpdateRate = fFrameRateUpdateRate;

   float fFramePeriod       = 1.0f / fFrameRate;
   m_nFramePeriod           = uint32_t( fFramePeriod * 1000 );
   m_nFrateRateUpdatePeriod = int( ( 1.0f / fFrameRateUpdateRate ) * 1000 );

   m_nDelayTimeNum = int( float( nCheckPeriodInSec ) / fFramePeriod );
   if( m_nDelayTimeNum <= 0 )
      m_nDelayTimeNum = 1;

   if( m_pDelayTimeBuffer )
      delete[] m_pDelayTimeBuffer;

   m_pDelayTimeBuffer = new int[m_nDelayTimeNum];
   memset( m_pDelayTimeBuffer, 0, sizeof( int ) * m_nDelayTimeNum );
}

void CFrameRateChecker::CheckStart()
{
   m_nCheckCount = 0;
   AfxGetLocalFileTime( &m_ftCheckStartTime );
}

int CFrameRateChecker::GetElapsedSecFromCheckStart()
{
   FILETIME ftCurrent;
   AfxGetLocalFileTime( &ftCurrent );
   FILETIME ftResult = ftCurrent - m_ftCheckStartTime;
   return GetElapsedSec( ftResult );
}

float CFrameRateChecker::CalcFrameRate()
{
   if( m_nCheckCount == 0 ) {
      m_nPrevTime       = GetTickCount() - m_nFramePeriod;
      m_nPrevUpdateTime = m_nPrevTime;
   }
   m_nCheckCount++;
   m_nDelayTimeCount++;
   if( m_nDelayTimeCount > m_nDelayTimeNum )
      m_nDelayTimeCount = m_nDelayTimeNum;

   BOOL bDelay          = FALSE;
   uint32_t nCurrTime   = GetTickCount();
   
   int nDelayInMilliSec = nCurrTime - m_nPrevTime - m_nFramePeriod;

   // test fps delay count
   //static int count = 0;
   //if(count ++ > 30) {
   //   LOGI << "nDelayInMilliSec : " << nDelayInMilliSec << "// m_nAverageDelayTime : " << m_nAverageDelayTime; 
   //   count = 0;
   //}   
   
   int nOldestIdx = m_nCurrIdx + 1;
   if( nOldestIdx >= m_nDelayTimeNum )
      nOldestIdx = 0;

   m_pDelayTimeBuffer[m_nCurrIdx] = nDelayInMilliSec;
   m_nTotalDelayTime += nDelayInMilliSec;
   m_nTotalDelayTime -= m_pDelayTimeBuffer[nOldestIdx];

   if( m_nTotalDelayTime ) {
      m_nAverageDelayTime = m_nTotalDelayTime / m_nDelayTimeCount;
   } else {
      m_nAverageDelayTime = 0;
   }

   if( nCurrTime - m_nPrevUpdateTime > m_nFrateRateUpdatePeriod ) {
      m_fRealFrameRate  = 1.0f / ( float( m_nFramePeriod + m_nAverageDelayTime ) / 1000.0f );
      m_nPrevUpdateTime = nCurrTime;
   }

   if( m_nAverageDelayTime > ( m_nFramePeriod / 5 ) )
      bDelay = TRUE;

   m_bDelay = bDelay;

   //logd( "dwDelayInMilliSec=%d, m_nAverageDelayTime=%d\n", nDelayInMilliSec, m_nAverageDelayTime );

   m_nPrevTime = nCurrTime;
   m_nCurrIdx++;
   if( m_nCurrIdx >= m_nDelayTimeNum ) m_nCurrIdx = 0;

   return m_fRealFrameRate;
}

/////////////////////////////////////////////////////////////
//
// Class : CFrameRateChecker2
//
/////////////////////////////////////////////////////////////

void CFrameRateChecker2::Initialize( float fFrameRate )
{
   m_fFrameRate     = fFrameRate;
   m_fRealFrameRate = fFrameRate * 0.5f;
}

void CFrameRateChecker2::CheckStart()
{
   m_nPrevTime         = 0;
   m_fWeightForNewData = 1.0f / ( m_fFrameRate * 5 );
   AfxGetLocalFileTime( &m_ftCheckStartTime );
}

float CFrameRateChecker2::CalcFrameRate()
{
   uint32_t nCurrTime = GetTickCount();

   if( m_nPrevTime ) {
      float fDiffTimeInMilliSec = 0.0f;
      fDiffTimeInMilliSec       = float( nCurrTime - m_nPrevTime );
      if( fDiffTimeInMilliSec < float( 2 * 1000 ) ) {
         //fDiffTimeInMilliSec = float (m_PC.GetElapsedTime (   ));
         if( fDiffTimeInMilliSec > 0 ) {
            float fNewFrameRate = 1000.0f / fDiffTimeInMilliSec;
            m_fRealFrameRate    = m_fRealFrameRate * ( 1.0f - m_fWeightForNewData ) + fNewFrameRate * m_fWeightForNewData;
         }
      }
   }

   m_nPrevTime = nCurrTime;

   return m_fRealFrameRate;
}

int CFrameRateChecker2::GetElapsedSecFromCheckStart()
{
   FILETIME ftCurrent;
   AfxGetLocalFileTime( &ftCurrent );
   FILETIME ftResult = ftCurrent - m_ftCheckStartTime;
   return GetElapsedSec( ftResult );
}

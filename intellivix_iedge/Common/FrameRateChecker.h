#pragma once

/////////////////////////////////////////////////////////////
//
// Class : CFrameRateChecke
//
/////////////////////////////////////////////////////////////

#include "bccl_define.h"

class CFrameRateChecker {
protected:
   UINT64 m_nCheckCount;

   float m_fFrameRate;
   float m_fRealFrameRate;
   float m_fFrameRateUpdateRate;

   int* m_pDelayTimeBuffer;
   int m_nDelayTimeNum;
   int m_nCurrIdx;
   uint32_t m_nTotalDelayTime;
   uint32_t m_nAverageDelayTime;
   int m_nFramePeriod;
   int m_nFrateRateUpdatePeriod;
   int m_nDelayTimeCount;
   uint32_t m_nPrevTime;
   uint32_t m_nPrevUpdateTime;
   BOOL m_bDelay;

   FILETIME m_ftCheckStartTime;

public:
   CFrameRateChecker( void );
   ~CFrameRateChecker( void );

public:
   void Initialize( float fFrameRate, int nCheckPeriodInSec = 5, float fFrameRateUpdateRate = 1.5f );
   void CheckStart();
   float CalcFrameRate();
   int GetElapsedSecFromCheckStart();
   float GetRealFrameRate()
   {
      return m_fRealFrameRate;
   }
   BOOL IsDelayed()
   {
      return m_bDelay;
   }
};

/////////////////////////////////////////////////////////////
//
// Class : CFrameRateChecker2
//
/////////////////////////////////////////////////////////////

class CFrameRateChecker2 {
public:
   float m_fFrameRate;
   float m_fRealFrameRate;
   float m_fWeightForNewData;
   uint32_t m_nPrevTime;

   FILETIME m_ftCheckStartTime;

public:
   void Initialize( float fFrameRate );
   void CheckStart();
   float CalcFrameRate();
   int GetElapsedSecFromCheckStart();
};

﻿#include "StdAfx.h"
#ifdef WIN32
#include "AreaSpecWnd.h"
#include "vacl_event.h"
using namespace VACL;

BEGIN_MESSAGE_MAP( CAreaSpecWnd, CAreaDisplayWnd )
ON_WM_LBUTTONDOWN()
ON_WM_RBUTTONDOWN()
ON_WM_MOUSEMOVE()
ON_WM_LBUTTONUP()
ON_WM_LBUTTONDBLCLK()

#ifdef __ENABLE_COPY_N_PASTE_GRAPHIC_OBJECT
ON_WM_CONTEXTMENU()
#endif

END_MESSAGE_MAP()

CAreaSpecWnd::CAreaSpecWnd()

{
   Mode                           = ASW_MODE_SELECT_EDIT;
   m_nState                       = 0;
   m_nResizingType                = -1;
   m_pTempObject                  = NULL;
   m_pOriginalObject              = NULL;
   m_pCompletedGObj               = NULL;
   m_hCursorSizing[ST_NORTH]      = AfxGetApp()->LoadStandardCursor( IDC_SIZENS );
   m_hCursorSizing[ST_SOUTH]      = AfxGetApp()->LoadStandardCursor( IDC_SIZENS );
   m_hCursorSizing[ST_WEST]       = AfxGetApp()->LoadStandardCursor( IDC_SIZEWE );
   m_hCursorSizing[ST_EAST]       = AfxGetApp()->LoadStandardCursor( IDC_SIZEWE );
   m_hCursorSizing[ST_NORTH_WEST] = AfxGetApp()->LoadStandardCursor( IDC_SIZENWSE );
   m_hCursorSizing[ST_SOUTH_EAST] = AfxGetApp()->LoadStandardCursor( IDC_SIZENWSE );
   m_hCursorSizing[ST_NORTH_EAST] = AfxGetApp()->LoadStandardCursor( IDC_SIZENESW );
   m_hCursorSizing[ST_SOUTH_WEST] = AfxGetApp()->LoadStandardCursor( IDC_SIZENESW );
   m_hCursorMoving                = AfxGetApp()->LoadStandardCursor( IDC_SIZEALL );
   m_StartPoint.SetPoint( -1, -1 );
   m_pMsgHandler = NULL;

#ifdef __ENABLE_MOVING_POLYGON_VERTEX
   m_nLastPolygonVertexIdx = -1;
#endif // __ENABLE_MOVING_POLYGON_VERTEX

#ifdef __ENABLE_COPY_N_PASTE_GRAPHIC_OBJECT
   m_bCreatedPolygon = false;
   m_pCopiedGrpObj   = nullptr;
#endif // __ENABLE_COPY_N_PASTE_GRAPHIC_OBJECT
}

CAreaSpecWnd::~CAreaSpecWnd()
{
   m_ConnectedLine.RemoveAll();

#ifdef __ENABLE_COPY_N_PASTE_GRAPHIC_OBJECT
   delete m_pCopiedGrpObj;
#endif // __ENABLE_COPY_N_PASTE_GRAPHIC_OBJECT
}

void CAreaSpecWnd::OnLButtonDown( UINT flag, CPoint point )
{
   m_csRender.Lock();
   BOOL bSpecifiedObjectToBeNull = FALSE;
   if( Mode == ASW_MODE_SELECT_EDIT ) {
      //리사이징 시작
      CSizingEdge* s_edge;
      CGraphicObject* g_object;
      g_object = m_GraphicObjectList.GetSelectedObject();
      if( g_object ) {

#ifdef __ENABLE_MOVING_POLYGON_VERTEX
         if( g_object->m_nShape == SHAPE_POLYGON ) {
            CPolygon* poly_ptr      = (CPolygon*)g_object;
            m_nLastPolygonVertexIdx = poly_ptr->GetVertexIdxByPoint( &point );
            if( m_nLastPolygonVertexIdx > -1 ) {
               m_pTempObject          = new CPolygon();
               CPolygon* tmp_poly_ptr = (CPolygon*)m_pTempObject;

               tmp_poly_ptr->SetPoints( poly_ptr->m_Points );
               tmp_poly_ptr->SetColor( poly_ptr->m_clrColor );
               tmp_poly_ptr->SetBrushStyle( poly_ptr->m_nHatchBrushStyle );

               m_pOriginalObject = poly_ptr;

               SetCapture();
               m_nState |= ASW_STATE_MOVING_POLYGON_VERTEX;
               poly_ptr->SetCursor( IDC_CROSS );
            }
         }

         if( !m_pTempObject && ( s_edge = g_object->m_OutlineRect.FindEdge( point ) ) ) {
#else
         if( s_edge = g_object->m_OutlineRect.FindEdge( point ) ) {
#endif // __ENABLE_MOVING_POLYGON_VERTEX

            switch( g_object->m_nShape ) {
            case SHAPE_RECTANGLE:
               m_pTempObject = new CRectangle;
               m_pTempObject->SetBoundRect( g_object->m_OutlineRect.m_rect );
               break;
            case SHAPE_POLYGON:
               m_pTempObject = new CPolygon;
               ( (CPolygon*)m_pTempObject )->SetPoints( ( (CPolygon*)g_object )->m_Points );
               ( (CPolygon*)m_pTempObject )->PrepareSizing( s_edge->m_nType );
               break;
            }
            m_pTempObject->SetColor( g_object->m_clrColor );
            m_pTempObject->SetBrushStyle( g_object->m_nHatchBrushStyle );
            m_nResizingType   = s_edge->m_nType;
            m_pOriginalObject = g_object;
            SetCapture();
            m_nState |= ASW_STATE_SIZING;
            Invalidate();
         }
      }
#ifdef __ENABLE_MOVING_POLYGON_VERTEX
      if( !( m_nState & ASW_STATE_SIZING ) && !( m_nState & ASW_STATE_MOVING_POLYGON_VERTEX ) ) {
#else
      if( !( m_nState & ASW_STATE_SIZING ) ) {
#endif // __ENABLE_MOVING_POLYGON_VERTEX
         //이동 시작
         g_object = m_GraphicObjectList.GetPointedObject( point );
         if( g_object ) {
            switch( g_object->m_nShape ) {
            case SHAPE_RECTANGLE:
               m_pTempObject = new CRectangle;
               m_pTempObject->SetBoundRect( g_object->m_OutlineRect.m_rect );
               break;
            case SHAPE_POLYGON:
               m_pTempObject = new CPolygon;
               ( (CPolygon*)m_pTempObject )->SetPoints( ( (CPolygon*)g_object )->m_Points );
               break;
            }
            m_pTempObject->SetColor( g_object->m_clrColor );
            m_pTempObject->SetBrushStyle( g_object->m_nHatchBrushStyle );
            m_StartPoint      = point;
            m_pOriginalObject = g_object;
            SetCapture();
            m_nState |= ASW_STATE_MOVING;
         }
         //선택 상태 표시
         m_GraphicObjectList.SelectPointedObject( point );
         ParentWnd->SendMessage( MSG_LBUTTONDOWN );
         Invalidate();
      }
   } else if( Mode == ASW_MODE_SPECIFY_POLYGON ) {
      if( m_ConnectedLine.GetCount() == 0 )
         m_GraphicObjectList.UnSelectAllObjects();

      m_ConnectedLine.Add( point );

      // 그리기가 완료되었다.
      if( m_ConnectedLine.GetSize() > 3 ) {
         CPoint dp = m_ConnectedLine[0] - point;
         float d   = (float)sqrt( (float)( dp.x * dp.x + dp.y * dp.y ) );
         if( d <= 6.0f ) {
            m_pCompletedGObj = new CPolygon;
            ( (CPolygon*)m_pCompletedGObj )->SetPoints( m_ConnectedLine );
            m_GraphicObjectList.AddTail( m_pCompletedGObj );
            m_ConnectedLine.RemoveAll();
            ParentWnd->SendMessage( MSG_LBUTTONDOWN );
            bSpecifiedObjectToBeNull = TRUE;
         }
      }
      Invalidate();
   } else if( Mode == ASW_MODE_SPECIFY_RECTANGLE ) {
      m_pTempObject = new CRectangle;
      m_StartPoint  = point;
      m_nState |= ASW_STATE_DRAWING;
      m_pTempObject->m_nState |= GS_TEMPORARY;
      SetCapture();
   }

   if( m_pMsgHandler )
      m_pMsgHandler->SW_OnLButtonDown( flag, point );
   SetFocus();

   if( bSpecifiedObjectToBeNull ) {
      m_pCompletedGObj = NULL;
   }

   m_csRender.Unlock();
}

void CAreaSpecWnd::OnLButtonUp( UINT flag, CPoint point )
{
   m_csRender.Lock();
   BOOL bSpecifiedObjectToBeNull = FALSE;
   if( Mode == ASW_MODE_SELECT_EDIT ) {
#ifdef __ENABLE_MOVING_POLYGON_VERTEX
      if( m_nState & ASW_STATE_SIZING || m_nState & ASW_STATE_MOVING || m_nState & ASW_STATE_MOVING_POLYGON_VERTEX ) {
#else
      if( m_nState & ASW_STATE_SIZING || m_nState & ASW_STATE_MOVING ) {
#endif // __ENABLE_MOVING_POLYGON_VERTEX
         CRect rect1 = m_pOriginalObject->m_OutlineRect.m_rect;
         CRect rect2 = m_pTempObject->m_OutlineRect.m_rect;
         switch( m_pTempObject->m_nShape ) {
         case SHAPE_RECTANGLE:
            m_pTempObject->m_OutlineRect.m_rect.NormalizeRect();
            m_pOriginalObject->SetBoundRect( m_pTempObject->m_OutlineRect.m_rect );
            break;
         case SHAPE_POLYGON:
            ( (CPolygon*)m_pOriginalObject )->SetPoints( ( (CPolygon*)m_pTempObject )->m_Points );
            break;
         }
         delete m_pTempObject;
         m_pTempObject = NULL;
         if( m_pOriginalObject->m_nAreaType == AREA_TYPE_EVENT_ZONE ) {
            EventZone* e_zone = (EventZone*)m_pOriginalObject->m_pVACPolygon;
            if( m_nState & ASW_STATE_MOVING ) {
               rect1.NormalizeRect();
               rect2.NormalizeRect();

               FPoint2D scale;
               scale.X = REF_IMAGE_WIDTH / (float)Width;
               scale.Y = REF_IMAGE_HEIGHT / (float)Height;
               int dx  = (int)( ( rect2.left - rect1.left ) * scale.X );
               int dy  = (int)( ( rect2.top - rect1.top ) * scale.Y );
               e_zone->Offset( dx, dy );
               e_zone->EvtRule->ERC_PathPassing.ZoneTree.OffsetZones( dx, dy );
               e_zone->EvtRule->ERC_PathPassing.Arrows.OffsetArrows( dx, dy );
            } else {
               GraphicObjectToIVSPolygon( m_pOriginalObject );
               if( m_nState & ASW_STATE_SIZING )
                  e_zone->EvtRule->ERC_PathPassing.Initialize( e_zone );
            }
         } else
            GraphicObjectToIVSPolygon( m_pOriginalObject );

         if( m_nState & ASW_STATE_SIZING ) {
            m_nState &= ~ASW_STATE_SIZING;
            m_nResizingType = -1;
         } else if( m_nState & ASW_STATE_MOVING ) {
            m_nState &= ~ASW_STATE_MOVING;
            m_StartPoint.SetPoint( -1, -1 );
         }
#ifdef __ENABLE_MOVING_POLYGON_VERTEX
         else if( m_nState & ASW_STATE_MOVING_POLYGON_VERTEX ) {
            m_nState &= ~ASW_STATE_MOVING_POLYGON_VERTEX;
            m_nLastPolygonVertexIdx = -1;
         }
#endif // __ENABLE_MOVING_POLYGON_VERTEX
         m_pOriginalObject = NULL;
         Invalidate();
         ReleaseCapture();
      }
   } else if( Mode == ASW_MODE_SPECIFY_RECTANGLE ) {
      if( m_nState & ASW_STATE_DRAWING ) {
         m_nState &= ~ASW_STATE_DRAWING;
         ReleaseCapture();
         m_StartPoint.SetPoint( -1, -1 );
         CRect rect = m_pTempObject->m_OutlineRect.m_rect;
         delete m_pTempObject;
         m_pTempObject = NULL;
         if( rect.Width() >= 16 && rect.Height() >= 16 ) {
            m_pCompletedGObj = new CRectangle;
            m_pCompletedGObj->SetBoundRect( rect );
            m_GraphicObjectList.AddTail( m_pCompletedGObj );
            ParentWnd->SendMessage( MSG_LBUTTONUP );
            bSpecifiedObjectToBeNull = TRUE;
         }
         Invalidate();
      }
   }
   if( m_pMsgHandler )
      m_pMsgHandler->SW_OnLButtonUp( flag, point );

   if( bSpecifiedObjectToBeNull )
      m_pCompletedGObj = NULL;

   m_csRender.Unlock();
}

void CAreaSpecWnd::OnMouseMove( UINT flag, CPoint point )
{
   m_csRender.Lock();

   CRect rc;
   GetClientRect( rc );
   if( point.x > rc.right ) point.x  = rc.right;
   if( point.y > rc.bottom ) point.y = rc.bottom;
   if( point.x < rc.left ) point.x   = rc.left;
   if( point.y < rc.top ) point.y    = rc.top;

   if( m_pTempObject ) m_pTempObject->m_nState |= GS_TEMPORARY;
   switch( Mode ) {
   case ASW_MODE_SELECT_EDIT: {
#ifdef __ENABLE_MOVING_POLYGON_VERTEX
      if( m_nState & ASW_STATE_MOVING_POLYGON_VERTEX ) {
         CPolygon* poly_ptr = (CPolygon*)m_pTempObject;
         if( poly_ptr ) {
            poly_ptr->UpdateVertexByIdx( m_nLastPolygonVertexIdx, &point );

            poly_ptr->SetCursor( IDC_CROSS );
            Invalidate();
         }
      }

      else if( m_nState & ASW_STATE_SIZING ) {
#else
      if( m_nState & ASW_STATE_SIZING ) {
#endif // __ENABLE_MOVING_POLYGON_VERTEX
         m_pTempObject->Sizing( point, m_nResizingType );
         ::SetCursor( AfxGetApp()->LoadStandardCursor( IDC_CROSS ) );
         Invalidate();
      } else if( m_nState & ASW_STATE_MOVING ) {
         int dx = point.x - m_StartPoint.x;
         int dy = point.y - m_StartPoint.y;
         //영역내에서만 움직이게
         CRect t_rect = m_pOriginalObject->m_OutlineRect.m_rect;
         int over_x, over_y;
         over_x = ( t_rect.right + dx ) - rc.right;
         if( over_x > 0 ) dx -= over_x;
         over_x = ( t_rect.left + dx ) - rc.left;
         if( over_x < 0 ) dx -= over_x;
         over_y = ( t_rect.bottom + dy ) - rc.bottom;
         if( over_y > 0 ) dy -= over_y;
         over_y = ( t_rect.top + dy ) - rc.top;
         if( over_y < 0 ) dy -= over_y;
         m_pTempObject->Moving( m_pOriginalObject, dx, dy );
         ::SetCursor( m_hCursorMoving );
         Invalidate();
      } else {
         BOOL bCursorOnEdges = FALSE;
         CSizingEdge* s_edge;
         CGraphicObject* g_object = m_GraphicObjectList.GetSelectedObject();
         if( g_object ) {
#ifdef __ENABLE_MOVING_POLYGON_VERTEX
            if( g_object->m_nShape == SHAPE_POLYGON ) {
               CPolygon* poly_ptr = (CPolygon*)g_object;
               int poly_vtx_idx   = poly_ptr->GetVertexIdxByPoint( &point );
               if( poly_vtx_idx > -1 ) {
                  poly_ptr->SetCursor( IDC_CROSS );

                  bCursorOnEdges = TRUE;
               }
            }

            if( !bCursorOnEdges && ( s_edge = g_object->m_OutlineRect.FindEdge( point ) ) ) {
#else
            if( s_edge = g_object->m_OutlineRect.FindEdge( point ) ) {
#endif // #ifdef __ENABLE_MOVING_POLYGON_VERTEX

               ::SetCursor( m_hCursorSizing[s_edge->m_nType] );
               bCursorOnEdges = TRUE;
            }
         }
         if( !bCursorOnEdges ) {
            CGraphicObject* g_object = m_GraphicObjectList.GetPointedObject( point );
            if( g_object )
               ::SetCursor( m_hCursorMoving );
            else
               ::SetCursor( AfxGetApp()->LoadStandardCursor( IDC_ARROW ) );
         }
      }
   } break;
   case ASW_MODE_SPECIFY_POLYGON:
      break;
   case ASW_MODE_SPECIFY_RECTANGLE:
      if( m_nState & ASW_STATE_DRAWING ) {
         CRect rectTemp;
         rectTemp.SetRect( m_StartPoint, point );
         rectTemp.NormalizeRect();
         m_pTempObject->SetBoundRect( rectTemp );
         Invalidate();
      }
   }

   if( m_pMsgHandler )
      m_pMsgHandler->SW_OnMouseMove( flag, point );
   m_csRender.Unlock();
}

void CAreaSpecWnd::OnRButtonDown( UINT flag, CPoint point )
{
   m_csRender.Lock();
   BOOL bSpecifiedObjectToBeNull = FALSE;

#ifdef __ENABLE_COPY_N_PASTE_GRAPHIC_OBJECT
   m_ptLastRBtnDown = point;
#endif // __ENABLE_COPY_N_PASTE_GRAPHIC_OBJECT

   if( m_ConnectedLine.GetSize() > 3 ) {
      bSpecifiedObjectToBeNull = TRUE;
      m_pCompletedGObj         = new CPolygon;
      ( (CPolygon*)m_pCompletedGObj )->SetPoints( m_ConnectedLine );
      m_GraphicObjectList.AddTail( m_pCompletedGObj );
      m_ConnectedLine.RemoveAll();
      ParentWnd->SendMessage( MSG_RBUTTONDOWN );

#ifdef __ENABLE_COPY_N_PASTE_GRAPHIC_OBJECT
      m_bCreatedPolygon = true;
#endif // __ENABLE_COPY_N_PASTE_GRAPHIC_OBJECT
   }

   if( m_pMsgHandler )
      m_pMsgHandler->SW_OnRButtonDown( flag, point );

   if( bSpecifiedObjectToBeNull )
      m_pCompletedGObj = NULL;
   SetFocus();
   m_csRender.Unlock();
}

void CAreaSpecWnd::OnLButtonDblClk( UINT flag, CPoint point )
{
   m_csRender.Lock();
   if( m_nOptions & DWA_OPTION_NOTIFY_DOUBLECLICK )
      ParentWnd->SendMessage( MSG_LBUTTON_DBLCLICKED, 0, (LPARAM)&point );

   if( m_nOptions & DWA_OPTION_NOTIFY_DOUBLECLICK )
      if( m_pMsgHandler )
         m_pMsgHandler->SW_OnLButtonDblClk( flag, point );

   CAreaDisplayWnd::OnLButtonDblClk( flag, point );
   m_csRender.Unlock();
}

void CAreaSpecWnd::RenderSub( CDC* dc )
{
   m_csRender.Lock();
   if( m_pMsgHandler )
      m_pMsgHandler->SW_PreRenderSub( dc );

   CAreaDisplayWnd::RenderSub( dc );

   m_ConnectedLine.DrawConnectedLine( dc );
   if( m_pTempObject )
      m_pTempObject->Draw( dc, m_GraphicObjectList.m_nDrawOptions );
   m_csRender.Unlock();
}

#ifdef __ENABLE_COPY_N_PASTE_GRAPHIC_OBJECT

void CAreaSpecWnd::OnContextMenu( CWnd* pWnd, CPoint pos )
{
   if( m_bCreatedPolygon ) {
      m_bCreatedPolygon = false;

      return;
   }

   CPoint pt = pos;
   ScreenToClient( &pt );

   CGraphicObject* pointed_obj_ptr  = m_GraphicObjectList.GetPointedObject( pt );
   CGraphicObject* selected_obj_ptr = m_GraphicObjectList.GetSelectedObject();

   if( pointed_obj_ptr ) {
      if( !selected_obj_ptr || selected_obj_ptr != pointed_obj_ptr ) {
         m_GraphicObjectList.SelectPointedObject( pt );
         Invalidate();
      }
   } else {
      m_GraphicObjectList.UnSelectAllObjects();
      Invalidate();
   }

   struct SMenuItem {
      int m_nID;
      LPCTSTR m_strText;
   } items[] = {
      { AreaSpecWndPopupMenuCopyItemId, _T("복사(&C)\tCtrl+C") },
      { AreaSpecWndPopupMenuPasteItemId, _T("붙여넣기(&P)\tCtrl+V") }
   };

   CMenu menu;
   menu.CreatePopupMenu();

   for( auto& item : items ) {
      menu.AppendMenu( MF_STRING, item.m_nID, item.m_strText );

      bool is_disabled_item = false;
      switch( item.m_nID ) {
      case AreaSpecWndPopupMenuCopyItemId:
         is_disabled_item = !can_copy_grp_obj( &pos );
         break;

      case AreaSpecWndPopupMenuPasteItemId:
         is_disabled_item = !can_paste_grp_obj();
         break;
      }

      if( is_disabled_item )
         menu.EnableMenuItem( item.m_nID, MF_DISABLED );
   }

   menu.TrackPopupMenu( TPM_LEFTALIGN | TPM_RIGHTBUTTON, pos.x, pos.y, this );
}

BOOL CAreaSpecWnd::PreTranslateMessage( MSG* pMsg )
{
   switch( pMsg->message ) {
   case WM_KEYDOWN:
      switch( pMsg->wParam ) {
      case 'C':
         if( GetKeyState( VK_CONTROL ) & 0x8000 ) {
            copy_grp_obj();

            return TRUE;
         }
         break;

      case 'V':
         if( GetKeyState( VK_CONTROL ) & 0x8000 ) {
            paste_grp_obj();
            return TRUE;
         }
         break;
      }
      break;

   case WM_COMMAND:
      switch( LOWORD( pMsg->wParam ) ) {
      case AreaSpecWndPopupMenuCopyItemId:
         copy_grp_obj();
         return TRUE;

      case AreaSpecWndPopupMenuPasteItemId: {
         CPoint off = m_ptLastRBtnDown - m_pCopiedGrpObj->m_OutlineRect.m_rect.TopLeft();
         paste_grp_obj( &off );
         return TRUE;
      }
      }
      break;
   }

   return __super ::PreTranslateMessage( pMsg );
}

int CAreaSpecWnd::add_grp_obj( CGraphicObject* pGrpObj )
{
   if( !pGrpObj )
      return -1;

   //#ifdef _DEBUG
   //   CString _s;
   //   _s.Format(_T("[%s():%4d] pGrpObj=%08X \n"),
   //      _T(__FUNCTION__), __LINE__,
   //      pGrpObj);
   //   ::OutputDebugString(_s);
   //#endif

   m_GraphicObjectList.UnSelectAllObjects();
   m_GraphicObjectList.AddTail( pGrpObj );

   m_pCompletedGObj = pGrpObj;
   ParentWnd->SendMessage( MSG_LBUTTONUP );
   m_pCompletedGObj = nullptr;

   return 0;
}
CGraphicObject* CAreaSpecWnd::get_selected_grp_obj()
{
   auto obj_ptr = m_GraphicObjectList.GetSelectedObject();
   if( obj_ptr ) {
      //#ifdef _DEBUG
      //      DWORD _dw = *(LPDWORD)obj_ptr;
      //      if (_dw == 0xDDDDDDDD || _dw == 0xFEEEFEEE)
      //         __asm int 3
      //#endif

      return obj_ptr;
   }

   return nullptr;
}
CGraphicObject* CAreaSpecWnd::get_grp_obj_by_point( CPoint* pPoint )
{
   if( !pPoint )
      return nullptr;

   auto grp_obj_ptr = m_GraphicObjectList.GetPointedObject( *pPoint );
   if( grp_obj_ptr ) {
      //

      return grp_obj_ptr;
   }

   return nullptr;
}
bool CAreaSpecWnd::can_copy_grp_obj( CPoint* pPoint /* = nullptr */ )
{
   if( get_selected_grp_obj() )
      return true;

   if( pPoint ) {
      if( get_grp_obj_by_point( pPoint ) )
         return true;
      else
         return false;
   }

   return true;
}
bool CAreaSpecWnd::can_paste_grp_obj()
{
   return m_pCopiedGrpObj != nullptr;
}
int CAreaSpecWnd::copy_grp_obj()
{
   if( !can_copy_grp_obj() )
      return -10;

   auto grp_obj = get_selected_grp_obj();
   if( clone_grp_obj( &m_pCopiedGrpObj, grp_obj ) < 0 )
      return -11;

   m_ptLastPasted.x = 0;
   m_ptLastPasted.y = 0;

   return 0;
}
int CAreaSpecWnd::paste_grp_obj( CPoint* pOffset /* = nullptr */ )
{
   if( !can_paste_grp_obj() )
      return -10;

   //#ifdef _DEBUG
   //   CGraphicObject* grp_obj_ptr = get_selected_grp_obj();
   //   CString _s;
   //   _s.Format(_T("selected_grp_obj=0x%08X \n"), grp_obj_ptr);
   //   ::OutputDebugString(_s);
   //#endif

   CGraphicObject* new_grp_obj_ptr = nullptr;
   clone_grp_obj( &new_grp_obj_ptr, m_pCopiedGrpObj );

   int x_off = 0;
   int y_off = 0;
   if( !pOffset ) {
      x_off = m_ptLastPasted.x + AreaSpecWndPasteGrpObjOffset;
      y_off = m_ptLastPasted.y + AreaSpecWndPasteGrpObjOffset;
   } else {
      x_off = pOffset->x;
      y_off = pOffset->y;
   }

   new_grp_obj_ptr->Moving( new_grp_obj_ptr, x_off, y_off );

   if( !pOffset ) {
      m_ptLastPasted.x = x_off;
      m_ptLastPasted.y = y_off;
   }

   if( add_grp_obj( new_grp_obj_ptr ) < 0 )
      return -11;

   Invalidate();

   return 0;
}
int CAreaSpecWnd::clone_grp_obj( CGraphicObject** ppDst, CGraphicObject* pSrc )
{
   if( !ppDst )
      return -1;

   if( !pSrc )
      return -2;

   if( *ppDst )
      delete *ppDst;

   //#ifdef _DEBUG
   //   DWORD _dw = *(LPDWORD)pSrc;
   //   if (_dw == 0xDDDDDDDD || _dw == 0xFEEEFEEE)
   //      __asm int 3
   //#endif

   switch( pSrc->m_nShape ) {
   case SHAPE_RECTANGLE:
      *ppDst = new CRectangle();
      ( (CRectangle*)*ppDst )->SetBoundRect( pSrc->m_OutlineRect.m_rect );
      break;

   case SHAPE_POLYGON:
      *ppDst = new CPolygon();
      ( (CPolygon*)*ppDst )->SetPoints( ( (CPolygon*)pSrc )->m_Points );
      break;
   }

   //#ifdef _DEBUG
   //   CString _s;
   //   _s.Format(_T("[%s():%4d] new_shape_ptr = 0x%08X \n"),
   //      _T(__FUNCTION__), __LINE__, *ppDst);
   //   ::OutputDebugString(_s);
   //#endif

   ( *ppDst )->m_nState |= ASW_STATE_DRAWING;

   ( *ppDst )->SetColor( pSrc->m_clrColor );
   ( *ppDst )->SetBrushStyle( pSrc->m_nHatchBrushStyle );

   return 0;
}

#endif // __ENABLE_COPY_N_PASTE_GRAPHIC_OBJECT
#endif

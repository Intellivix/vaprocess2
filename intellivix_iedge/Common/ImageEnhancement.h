#pragma once

#include "version.h"

#ifdef __SUPPORT_IMAGE_ENHANCEMENT

#include <mutex>
#include <thread>
#include "bccl.h"
//#include "VACEx.h"
#include "FisheyeDewarping.h"
#include "EventEx.h"

using namespace BCCL;

///////////////////////////////////////////////////////////////////////////////
//
// Class: CVideoStabilizationSetup
//
///////////////////////////////////////////////////////////////////////////////

class CVideoStabilizationSetup
{
public:
   // 비디오 안정화
   BOOL   m_bUseVideoStabilization;
   int    m_nVideoStabilizationFrameNum;    

public:
   BOOL   m_bPTZCameraMoving;
   BOOL   m_bUseVideoStabilizationPrev;
   int    m_nVideoStabilizationFrameNumPrev;    

public:
   CVideoStabilizationSetup (   );
   CVideoStabilizationSetup& operator=  (CVideoStabilizationSetup& from);
   BOOL                      operator== (CVideoStabilizationSetup& from);
   BOOL                      operator!= (CVideoStabilizationSetup& from);

public:
   int  ReadFile       (CXMLIO* pIO);
   int  WriteFile      (CXMLIO* pIO);

public:
   BOOL IsJustStarted (   );
   BOOL IsJustEnded (   );
   BOOL IsChanged (   );
   BOOL IsOn (   );
   void BackupToPrev (   );
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: CVideoDefoggingSetup
//
///////////////////////////////////////////////////////////////////////////////

class CVideoDefoggingSetup
{
public:
   BOOL     m_bUseDefog;
   int      m_nDefogLevel;    
   FPoint2D m_ResizeFactor;

public:
   BOOL   m_bUseDefogPrev;
   int    m_nDefogModePrev;    

public:
   CVideoDefoggingSetup (   );
   CVideoDefoggingSetup& operator=  (CVideoDefoggingSetup& from);
   BOOL                  operator== (CVideoDefoggingSetup& from);
   BOOL                  operator!= (CVideoDefoggingSetup& from);

public:
   int  ReadFile       (CXMLIO* pIO);
   int  WriteFile      (CXMLIO* pIO);

public:
   BOOL IsJustStarted (   );
   BOOL IsJustEnded (   );
   BOOL IsChanged (   );
   BOOL IsOn (   );
   void BackupToPrev (   );
};

#ifdef __SUPPORT_FISHEYE_CAMERA

///////////////////////////////////////////////////////////////////////////////
//
// Class: CFisheyeDewarpingSetup
//
///////////////////////////////////////////////////////////////////////////////

class CFisheyeDewarpingSetup
{
public:
   BOOL            m_bDefaultFisheyeInfo;
   BOOL            m_bUseDewarp;   
   FISHEYE_INFO    m_FisheyeInfo;

public:
   CFisheyeDewarpingSetup (   );
   CFisheyeDewarpingSetup& operator=  (CFisheyeDewarpingSetup& from);
   BOOL                    operator== (CFisheyeDewarpingSetup& from);
   BOOL                    operator!= (CFisheyeDewarpingSetup& from);

public:
   int  ReadFile       (CXMLIO* pIO);
   int  WriteFile      (CXMLIO* pIO);

public:
   BOOL IsOn (   );

public:
   FisheyeViewType   GetFisheyeView();
   FPoint2D          GetFisheyeCenter();
   float             GetFisheyeRadius();
   float             GetFisheyeLpRange();
   float             GetFisheyeUpRange();
   float             GetFisheyeFocalLength();
};

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Enumerations
//
///////////////////////////////////////////////////////////////////////////////

enum ImageEnhancementState
{
   ImageEnhancementState_ProcessInThread                 = 0x00000001,
   ImageEnhancementState_VideoStabilizationIsInitialized = 0x00000002,
   ImageEnhancementState_VideoDefogginIsInitialized      = 0x00000004,
   ImageEnhancementState_FisheyeDewarpIsInitialized      = 0x00000008,
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: CImageEnhancement
//
///////////////////////////////////////////////////////////////////////////////

class CImageEnhancement
{
public:
   int      m_nState;
   int      m_nWidth;
   int      m_nHeight;
   int      m_nWidthDefog;
   int      m_nHeightDefog;
   int      m_nYUY2BuffLength;
   float    m_fFrameRate;
   int      m_nVideoStabilizationMode;  // 0 : Static Camera,  1: Moving Camera
   BOOL     m_bPrevPTZCameraMoving;
   byte*    m_pSrcImgBuff;
   byte*    m_pDestImgBuff;
   byte*    m_pDecompressTgtBuff;
   byte*    m_pVideoStabilizerSrcBuff;
   byte*    m_pVideoStabilizerTgtBuff;
   byte*    m_pVideoDefoggingSrcBuff;
   byte*    m_pVideoDefoggingTgtBuff;
   byte*    m_pVideoNoiseReductionSrcBuff;
   byte*    m_pVideoNoiseReductionTgtBuff;
   BArray1D m_SrcYUY2Img;
   BArray1D m_VideoStabilizationTgtYUY2Img;
   int      m_ImageEnhancementResultImgUdateCount;
   BArray1D m_ImageEnhancementResultYUY2Img;
   std::thread m_ImageEnhancementThread;
   void*    m_pVideoStabilizer;
   void*    m_pVideoDefogger;
   void*    m_pVideoNoiseReducer;

public:
   int m_nPrevWidth;
   int m_nPrevHeight;
   int m_nPrevTgtWidth;
   int m_nPrevTgtHeight;
   int m_nTgtWidth;
   int m_nTgtHeight;
   float m_fPrevRadius;
   FPoint2D m_PrevCenterPos;

#ifdef __SUPPORT_FISHEYE_CAMERA
   FisheyeViewType m_CurFisheyeView;
   byte* m_pFisheyeDewarpSrcBuff;
   byte* m_pFisheyeDewarpTgtBuff;
   std::mutex m_csFisheyeDewarp;
   WarpingTable_FisheyeToCylinder *m_pWarpingTable_FisheyeToCylinder;
#endif

   BArray1D m_VideoDefogTgtYUY2Img;

   int m_nHalfPanoramaCenterX;

public:
   CVideoStabilizationSetup m_VideoStabilizationSetup;
   CVideoDefoggingSetup     m_VideoDefoggingSetup;
#ifdef __SUPPORT_FISHEYE_CAMERA
   CFisheyeDewarpingSetup   m_DewarpingSetup;
#endif

public:
   enum
   {
      THREAD_EVENT_FRAME_ADD = 1,
      THREAD_EVENT_STOP      = 2,
   };
   
   CEventEx                 m_evtThread;
   int                      m_nThreadEvtType;   

   CCriticalSection         m_csVideoDefogger;
   CCriticalSection         m_csVideoStabilizer;
   CCriticalSection         m_csImageBuffer;

protected:
   struct CFrameBuffer
   {
      BArray1D Buffer;
      CFrameBuffer *Prev,*Next;
   };
   Queue<CFrameBuffer>      FrameBufferInputQueue;
public:
   CCriticalSection         CS_FrameBuffer;


public:
   CImageEnhancement (   );
   ~CImageEnhancement (   );

public:
   void Initialize (int nWidth, int nHeight, float fFrameRate, BOOL bThread);
   void Close (   );
   void ImageEnhancementInitProc (byte* pSrcImgBuff, byte* pDstImgBuff);
   void ImageEnhancementMainProc (   );
   void ImageEnhancementThreadProc (   );
   BOOL IsVideoEnhancementExist (   );
   BOOL IsVideoEnhancementNotExist (   );

protected:
   void PrepareImageEnhancementBuffProc (byte* pSrcImageBuff, byte* pDstImgBuff);
   void UpdateImageEnhancementBuffPointer (byte* pSrcImageBuff, byte* pDstImgBuff, BOOL bInthread);
   void VideoStabilizationInitProc (   );
   void VideoStabilizationMainProc (   );
   void VideoDefoggingInitProc (   );
   void VideoDefoggingMainProc (   );
   void DetermineThreadStartEndProc (   );
   void StartImageEnhancementThreadProc (   );
   void StopImageEnhancementThreadProc (   );
   static UINT ThreadFuction_ImageEnhancement (LPVOID pParam);

#ifdef __SUPPORT_FISHEYE_CAMERA
   void FisheyeDewarpingInitProc (   );
   void FisheyeDewarpingMainProc (   );
#endif

public:
   void GetFisheyeDewarpingImg(byte* pDewarpBuf);
   int  GetWarpingTableWidth();
   int  GetWarpingTableHeight();
   void MoveHalfPanoramaCenterX(int center_x);
};

#endif // #ifdef __SUPPORT_IMAGE_ENHANCEMENT

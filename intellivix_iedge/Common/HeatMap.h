#pragma once

#include "CommonUtil.h"

///////////////////////////////////////////////////////////////////////////////
//
// Class:ZScore
//
///////////////////////////////////////////////////////////////////////////////

class CZScore : public FArray2D
{
public:
   float       Sum;
   float       Min;
   float       Max;
   float       Avg;
   float       Var; // Variance
   float       SD; // Standard Deviation
   
public:
   CZScore();

public:
   template <typename T> void CalcZScore(T& aValue, float fAvg, float fSD);
};

template <typename T>
void CZScore::CalcZScore(T& aValue, float fAvg, float fSD)
{
   // 표준 점수(Z-Score)를 계산
   int i,j;
   int nCnt = 0;
   for (i=0; i<Height; i++) {
      for (j=0; j<Width; j++) {
         if (aValue[i][j] != 0) {
            // z = (x - Mean) / SD
            Array[i][j] = ((float)aValue[i][j] - fAvg) / fSD;            
            if (Array[i][j] < Min)  Min = Array[i][j];
            if (Array[i][j] > Max)  Max = Array[i][j];
            Sum += Array[i][j];
            nCnt++;
         }
         else {
            Array[i][j] = 0.0f;
         }         
      }
   }  

   if (1) {
      // 2SD보다 큰 outlier 필터링
      Avg = Sum / nCnt;
      Var = GetVariance(Array, Avg, Width, Height, nCnt);
      SD  = sqrt(Var);   
      Max = SD * 2;
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// Class:CHeatMapData
//
///////////////////////////////////////////////////////////////////////////////

class CHeatMapData : public UIArray2D
{
public:
   uint        Sum;
   uint        Min;
   uint        Max;
   float       Avg;
   float       Var;
   float       SD;
   CZScore     ZScore; 
   BGRImage    Image;

public:
   void Initialize();
   int MakeHeatMapImage();

private:
   int _CalcStatistics();
   
};

///////////////////////////////////////////////////////////////////////////////
//
// Class:CHeatMap
//
///////////////////////////////////////////////////////////////////////////////

class CHeatMap
{
public:
   int m_nWidth;
   int m_nHeight;
   BOOL m_bHasHeatMap;

public:
   CHeatMapData m_HeatMapData_DwellTime;
   CHeatMapData m_HeatMapData_Count;

public:
   CHeatMap();
   virtual ~CHeatMap() {}

public:
   int Create(int nWidth, int nHeight);
   int AddData(int nSrcWidth, int nSrcHeight, int nObjBoxCount, SBox2D* ObjBox);
   void MakeHeatMap();

private:
   int _CalcAverage();   
   int _MakeHeatMapImage();
};

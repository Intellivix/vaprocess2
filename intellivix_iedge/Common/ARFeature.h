#pragma once

#if defined(__SUPPORT_AUGMENTED_REALITY)
#include "bccl_polygon.h"
using namespace VACL;

///////////////////////////////////////////////////////////////////////////////
//
// Class: CARFeatureInfo
//
///////////////////////////////////////////////////////////////////////////////

enum AR_FEATURE_TYPE
{
   AR_FEATURE_TYPE_POLYGON,   
   AR_FEATURE_TYPE_POLYLINE,
   AR_FEATURE_TYPE_POINT   
};

class CARFeatureInfo : public Polygon
{
public:
   int               AlarmState;
   int               FeatureType;
   std::string       AreaName;
   Array1D<FPoint3D> PTZPositions;
   float             MinZoom;
   float             MaxZoom;
   
   int               FontSize;
   std::string       FontName;
   COLORREF          FontColor;
   COLORREF          LineColor;

public:
   CARFeatureInfo *Prev, *Next;
   void* m_pVoid;

public:
   CARFeatureInfo (   );
   virtual ~CARFeatureInfo (   ) {   }

public:
   virtual int ReadFile   (FileIO &file) { return DONE; }
   virtual int WriteFile  (FileIO &file) { return DONE; }
   virtual int ReadFile   (CXMLIO* pIO);
   virtual int WriteFile  (CXMLIO* pIO);
   virtual int CheckSetup (CARFeatureInfo* ft);

public:
   void Create (int n_vertices);
   void ClipRect(int nWidth, int nHeight);
   BOOL IsInZoomRange(float fZoom);
};

typedef PolygonList<CARFeatureInfo> CARFeatureInfoList;
#endif

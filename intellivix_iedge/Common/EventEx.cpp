#include "StdAfx.h"
#include "EventEx.h"

#include <chrono>

#include "WaitForMultipleObjects.h"

void CEventEx::SetEvent( bool bNotifyAll )
{
   {
      std::lock_guard<std::mutex> lk( m_mutex );
      m_status_count++;
   }

   if( m_pWMO != nullptr )
      m_pWMO->Notify();

   if( bNotifyAll )
      m_cv.notify_all();
   else
      m_cv.notify_one();
}

uint CEventEx::GetStatus()
{
   return m_status_count;
}

void CEventEx::ResetEvent()
{
   std::lock_guard<std::mutex> lk( m_mutex );
   m_status_count = 0;
}

void CEventEx::Wait()
{
   std::unique_lock<std::mutex> lk( m_mutex );
   m_cv.wait( lk, [this] { return m_status_count > 0; } );
   if( m_status_count > 0 )
      --m_status_count;
}

bool CEventEx::Wait_For( int ms )
{
   bool bRet = false;
   std::unique_lock<std::mutex> lk( m_mutex );
   bRet = m_cv.wait_for( lk, std::chrono::milliseconds( ms ), [this] { return m_status_count > 0; } );
   if( bRet ) {
      if( m_status_count > 0 )
         --m_status_count;
   }

   return bRet;
}

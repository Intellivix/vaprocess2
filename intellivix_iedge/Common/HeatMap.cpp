#include "StdAfx.h"
#include "HeatMap.h"


///////////////////////////////////////////////////////////////////////////////
//
// Class:CZScore
//
///////////////////////////////////////////////////////////////////////////////

CZScore::CZScore()
{
   Sum = 0.0f;
   Min = 1.0E+10f;
   Max = 0.0f;
   Avg = 0.0f;
   Var = 0.0f;
   SD  = 0.0f;
   _Init();
}

///////////////////////////////////////////////////////////////////////////////
//
// Class:CHeatMapData
//
///////////////////////////////////////////////////////////////////////////////

void CHeatMapData::Initialize()
{
   Sum = 0;
   Min = UINT_MAX;
   Max = 0;
   Avg = 0.0f;
   Var = 0.0f;
   SD  = 0.0f;

   Image.Create( Width, Height );
   ZScore.Create( Width, Height );
   ZScore.Clear();
}

int CHeatMapData::_CalcStatistics()
{
   int nCnt = 0;
   int i, j;
   for( i = 0; i < Height; i++ ) {
      for( j = 0; j < Width; j++ ) {
         if( Array[i][j] != 0 ) {
            Sum += Array[i][j];
            if( Array[i][j] < Min ) Min = Array[i][j];
            if( Array[i][j] > Max ) Max = Array[i][j];
            nCnt++;
         }
      }
   }
   Avg = (float)Sum / nCnt;
   Var = GetVariance( Array, Avg, Width, Height, nCnt );
   SD  = sqrt( Var );

   return DONE;
}

int CHeatMapData::MakeHeatMapImage()
{
   _CalcStatistics();
   ZScore.CalcZScore( Array, Avg, SD );

   int i, j;
   unsigned char hue = 0;
   double fRange     = ZScore.Max - ZScore.Min;
   BGRPixel bgr;

   for( i = 0; i < Height; i++ ) {
      for( j = 0; j < Width; j++ ) {
         if( ZScore[i][j] != 0.0f ) {
            if( ZScore[i][j] > ZScore.Max ) ZScore[i][j] = ZScore.Max;
            hue                                          = (int)( 1.0f + ( 180.0f * ( ( ZScore[i][j] - ZScore.Min ) / fRange ) ) );
            ConvertHSVToBGR( 180 - hue, 255, 255, bgr );
            Image[i][j].R = bgr.R;
            Image[i][j].G = bgr.G;
            Image[i][j].B = bgr.B;
         } else {
            Image[i][j].R = 0;
            Image[i][j].G = 0;
            Image[i][j].B = 0;
         }
      }
   }

   return DONE;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class:CHeatMap
//
///////////////////////////////////////////////////////////////////////////////

CHeatMap::CHeatMap()
{
   m_nWidth      = 0;
   m_nHeight     = 0;
   m_bHasHeatMap = FALSE;
}

int CHeatMap::Create( int nWidth, int nHeight )
{
   m_nWidth  = nWidth;
   m_nHeight = nHeight;
   if( m_nWidth == 0 || m_nHeight == 0 )
      return -1;

   m_HeatMapData_DwellTime.Create( m_nWidth, m_nHeight );
   m_HeatMapData_DwellTime.Clear();
   m_HeatMapData_DwellTime.Initialize();
   m_HeatMapData_Count.Create( m_nWidth, m_nHeight );
   m_HeatMapData_Count.Clear();
   m_HeatMapData_Count.Initialize();

   return DONE;
}

int CHeatMap::AddData( int nSrcWidth, int nSrcHeight, int nObjBoxCount, SBox2D* ObjBox )
{
   int i, j, k;
   float fScaleX = (float)m_nWidth / nSrcWidth;
   float fScaleY = (float)m_nHeight / nSrcHeight;

   SArray2D aObjectCount( m_nWidth, m_nHeight );
   aObjectCount.Clear();

   int nBoxX, nBoxY, nBoxWidth, nBoxHeight, nScaleWidth, nScaleHeight;
   for( i = 0; i < nObjBoxCount; i++ ) {
      if( ObjBox[i].X < 0 ) {
         ObjBox[i].Width += ObjBox[i].X;
         ObjBox[i].X = 0;
      }
      if( ObjBox[i].Y < 0 ) {
         ObjBox[i].Height += ObjBox[i].Y;
         ObjBox[i].Y = 0;
      }

      // 객체 박스 크기를 스케일링
      nBoxY        = (int)( ObjBox[i].Y * fScaleY );
      nBoxX        = (int)( ObjBox[i].X * fScaleX );
      nScaleWidth  = (int)( ObjBox[i].Width * fScaleX ) + nBoxX;
      nScaleHeight = (int)( ObjBox[i].Height * fScaleY ) + nBoxY;
      nBoxWidth    = nScaleWidth > m_nWidth ? m_nHeight : nScaleWidth;
      nBoxHeight   = nScaleHeight > m_nHeight ? m_nHeight : nScaleHeight;

      for( j = nBoxY; j < nBoxHeight; j++ ) {
         for( k = nBoxX; k < nBoxWidth; k++ ) {
            if( j >= 0 && j < m_nHeight && // jun 물체 박스는 영상을 벗어날 수 있으므로 클리핑 처리를 해야함.
                k >= 0 && k < m_nWidth ) {
               aObjectCount[j][k] = 1;
               m_HeatMapData_DwellTime[j][k]++;
            }
         }
      }
   }

   for( i = 0; i < m_nHeight; i++ ) {
      for( j = 0; j < m_nWidth; j++ ) {
         m_HeatMapData_Count[i][j] += aObjectCount[i][j];
      }
   }

   return DONE;
}

void CHeatMap::MakeHeatMap()
{
   _CalcAverage();
   _MakeHeatMapImage();
}

int CHeatMap::_CalcAverage()
{
   int i, j;
   for( i = 0; i < m_nHeight; i++ ) {
      for( j = 0; j < m_nWidth; j++ ) {
         if( m_HeatMapData_Count[i][j] > 0 ) {
            m_HeatMapData_DwellTime[i][j] = ( uint )( (float)m_HeatMapData_DwellTime[i][j] / m_HeatMapData_Count[i][j] + 0.5f );
         }
      }
   }

   return DONE;
}

int CHeatMap::_MakeHeatMapImage()
{
   m_HeatMapData_DwellTime.MakeHeatMapImage();
   m_HeatMapData_Count.MakeHeatMapImage();
   m_bHasHeatMap = TRUE;

   return DONE;
}

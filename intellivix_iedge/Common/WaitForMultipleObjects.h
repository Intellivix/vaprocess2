﻿#pragma once

#include <limits>
#include <mutex>
#include <vector>
#include <condition_variable>

class CEventEx;

#ifndef WAIT_OBJECT_0
#define WAIT_OBJECT_0 0
#endif
///////////////////////////////////////////////////////////////////////
//
// class CWaitForMultipleObjects
//
///////////////////////////////////////////////////////////////////////

class CWaitForMultipleObjects {
public:
   // 이 뮤텍스의 역활 아래 condition_variable의 입력으로 사용된다.
   // 주의사항으로는 CEventEx::m_mutex 임계영역 내부에 m_mutex_WMO를 지정해서는 안된다.
   std::mutex m_mutex_WMO;
   std::condition_variable m_cv;
   std::vector<CEventEx*> m_vecEvent;
   std::vector<bool> m_vecSignal;
   bool m_bReady_WMO = false;

private:
   size_t CheckSignal();

public:
   CWaitForMultipleObjects() {}
   ~CWaitForMultipleObjects();

   void Init( const std::vector<CEventEx*>& vecEvent );
   bool RemoveEventEx( CEventEx* pEvt );
   void ClearAllEventEx();
   void Notify();
   bool WaitForMultipleObjects( bool bWaitAll, uint32_t timeout_ms = INFINITE );
   uint GetSignalCount();
   std::vector<bool> &GetSignal();

   friend class CEventEx;
};

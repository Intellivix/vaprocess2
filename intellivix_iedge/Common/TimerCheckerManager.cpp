﻿#include "TimerCheckerManager.h"

TimerChecker::TimerChecker( const std::string& name, uint interval_ms )
{
   m_name        = name;
   m_interval_ms = interval_ms;
   Reset();
}

void TimerChecker::Reset()
{
   m_last_time = std::chrono::high_resolution_clock::now();
}

std::string TimerChecker::DebugIntervalError()
{
   // currnet time
   std::chrono::high_resolution_clock::time_point current_time = std::chrono::high_resolution_clock::now();
   // diff
   std::chrono::duration<double, std::milli> diff_time_ms = current_time - m_last_time;
   // measure actual interval and error.
   m_diff_ms = diff_time_ms.count();
   double errors_ms = m_diff_ms - m_interval_ms;

   /**
    * @brief str_ret unit: ms
    * @param n : name
    * @param i : interval
    * @param m : measured
    * @param e : errors
    */
   std::string str_ret = BCCL::PlogHelper::cFormatToStdString(
       "n[%s] i[%3d] => m[%7.2f] e[%+7.2f]",
       m_name.c_str(), m_interval_ms, m_diff_ms, errors_ms );

   // update last time.
   m_last_time = current_time;

   return str_ret;
}

double TimerChecker::getCurrentDiff() {
   return m_diff_ms;
}

TimerCheckerManager& TimerCheckerManager::Get()
{
   static TimerCheckerManager s_instance;
   return s_instance;
}

std::string TimerCheckerManager::DebugMsg( const std::string& timer_name, uint interval_ms )
{
   auto& timer = m_timers[timer_name];
   if( timer == nullptr ) {
      timer                = std::make_shared<TimerChecker>( timer_name, interval_ms );
      m_timers[timer_name] = timer;
   }
   return timer->DebugIntervalError();
}

const std::shared_ptr<TimerChecker> TimerCheckerManager::GetTimer(const std::string &key)
{
   return m_timers[key];
}

TimerCheckerManager::TimerCheckerManager()
{
}

TimerCheckerManager::~TimerCheckerManager()
{
   m_timers.clear();
}

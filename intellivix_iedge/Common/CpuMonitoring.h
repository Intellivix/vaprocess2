﻿#ifndef CPUMONITORING_H
#define CPUMONITORING_H

#include "InnerThread.h"
#include "bccl_plog.h"

#include <iostream>
#include <fstream>

class CpuMonitoring : public InnerThread {
public:
   bool Start( uint interval_ms = 1000 );
   void Stop();

   double getCurrentCpuUsage();

private:
   bool m_thread_running = false;
   std::vector<size_t> get_cpu_times();
   bool get_cpu_times( size_t& idle_time, size_t& total_time );
   void ThreadLoop() override;

   double m_current_cpu_usage = 0;
   uint m_interval_ms;
};

#endif // CPUMONITORING_H

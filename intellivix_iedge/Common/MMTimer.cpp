﻿#include "StdAfx.h"
#include "MMTimer.h"
#include "WaitForMultipleObjects.h"

//////////////////////////////////////////////////////////////////////////
//
// class CMMTimer
//
//////////////////////////////////////////////////////////////////////////

CMMTimer::CMMTimer()
{
   m_nTickCount = 0;
   m_nPeriod    = 0;
   m_nTimerID   = 0;
   m_dfFPS      = 30;
   m_bStopTimer = true;
}

CMMTimer::~CMMTimer()
{
}

bool CMMTimer::Wait()
{
   if( m_pTimerManager->GetStatus( CTimerManager::State_Activate ) ) {
      if( !m_bStopTimer ) {
         m_evtTimer.Wait();
         return true;
      }
   }
   return false;
}

uint CMMTimer::GetPeriod()
{
   return m_nPeriod;
}

float CMMTimer::GetFPS()
{
   return m_dfFPS;
}

CEventEx& CMMTimer::GetTimerEvent()
{
   return m_evtTimer;
}

void CMMTimer::SetMilliSec( uint millisec )
{
   m_dfFPS = float( 1000 / double( millisec ) );
   SetTimer( millisec );
}

void CMMTimer::SetFPS( float fFPS )
{
   m_dfFPS = fFPS;
   SetTimer( uint( 1000 / fFPS ) );
}

void CMMTimer::SetTimer( uint period )
{
   uint nBeforPeriod = m_nPeriod;

   m_nPeriod = period;

   if( m_nPeriod < m_pTimerManager->GetMinResolution() )
      m_nPeriod = m_pTimerManager->GetMinResolution();

   m_pTimerManager->OnSetPeriod( this, nBeforPeriod );
}

void CMMTimer::StartTimer()
{
   m_pTimerManager->StartTimer( m_nTimerID );
}

void CMMTimer::StopTimer()
{
   m_pTimerManager->StopTimer( m_nTimerID );
}

//////////////////////////////////////////////////////////////////////////
//
// class CTimerManager
//
//////////////////////////////////////////////////////////////////////////

int CTimerManager::Thread_TimerProc( CTimerManager* instance )
{
#ifdef SUPPORT_PTHREAD_NAME
   pthread_setname_np( pthread_self(), "Timer" );
#endif

   instance->SetStatus( State_Thread_TimeProc, true );

   int ret = instance->ProcWork();

   instance->SetStatus( State_Thread_TimeProc, false );
   instance->SetStatus( State_Signal_Deactivate, false );

   return ret;
}

CTimerManager::CTimerManager()
    : m_Status( State_Num, false )
{
   timer_ex::ActiveTimer();
}

CTimerManager::~CTimerManager()
{
}

int CTimerManager::Activate()
{
   if( GetStatus( State_Activate ) )
      return 1;

   OnActivate();

   m_WorkThread = std::thread( Thread_TimerProc, this );

   SetStatus( State_Activate, true );

   return 0;
}

int CTimerManager::Deactivate()
{
   if( false == GetStatus( State_Activate ) )
      return 1;

   SetStatus( State_Signal_Deactivate, true );

   OnDeactivate();

   if( m_WorkThread.joinable() )
      m_WorkThread.join();

   ClearTimer();

   SetStatus( State_Activate, false );

   return 0;
}

void CTimerManager::SetMinResolution( uint ms )
{
   m_MinResolution = ms;
}

uint CTimerManager::GetMinResolution()
{
   return m_MinResolution;
}

void CTimerManager::SetStatus( State index, bool value )
{
   m_Status[index] = value;
}

bool CTimerManager::GetStatus( State index )
{
   return m_Status[index];
}

bool CTimerManager::StartTimer( uint id )
{
   CCriticalSectionSP co( m_csVecTimers );

   bool ret = false;
   for( CMMTimer* item : m_vecTimers ) {
      if( item->m_nTimerID == id ) {
         item->m_bStopTimer = false;
         ret                = true;
         break;
      }
   }

   return ret;
}

bool CTimerManager::StopTimer( uint id )
{
   CCriticalSectionSP co( m_csVecTimers );

   bool ret = false;
   for( CMMTimer* item : m_vecTimers ) {
      if( item->m_nTimerID == id ) {
         item->m_bStopTimer = false;
         ret                = true;
         break;
      }
   }

   return ret;
}

void CTimerManager::ClearTimer()
{
   CCriticalSectionSP co( m_csVecTimers );

   for( const CMMTimer* item : m_vecTimers ) {
      delete item;
   }

   m_vecTimers.clear();
}

bool CTimerManager::DeleteTimer( uint id )
{
   CCriticalSectionSP co( m_csVecTimers );

   bool ret  = false;
   int index = 0;

   for( CMMTimer* item : m_vecTimers ) {
      if( item->m_nTimerID == id ) {
         item->StopTimer();
         OnDeleteTimer( item );
         m_vecTimers.erase( m_vecTimers.begin() + index );
         delete item;

         ret = true;
      }
      index++;
   }

   return ret;
}

CMMTimer* CTimerManager::GetNewTimer( const std::string& name )
{
   CCriticalSectionSP co( m_csVecTimers );

   CMMTimer* pItem = new CMMTimer;
   if( name.length() ) pItem->m_szName = name;
   pItem->m_nTimerID      = m_TimerIdAllocate++;
   pItem->m_pTimerManager = this;
   m_vecTimers.push_back( pItem );

   return pItem;
}

void CTimerManager::SendEvent()
{
   CCriticalSectionSP co( m_csVecTimers );

   for( CMMTimer* item : m_vecTimers ) {
      item->m_nTickCount++;
      if( !item->m_bStopTimer ) {
         if( item->m_nTickCount >= item->m_nPeriod ) {
            item->m_evtTimer.SetEvent();
            item->m_nTickCount = 0;
         }
      }
   }
}

uint CTimerManager::GetDivisor( uint nNumber )
{
   uint ms = GetMinResolution();
   for( uint ii = GetMinResolution(); ii <= nNumber; ii++ ) {
      if( 0 == ( nNumber % ii ) ) {
         ms = ii;
         break;
      }
   }

   return ms;
}

void CTimerManager::TimerEvent( void* ptr )
{
   CEventEx* event = reinterpret_cast<CEventEx*>( ptr );
   event->SetEvent();
}

int CTimerManager::OnActivate()
{
   return 0;
}

int CTimerManager::OnDeactivate()
{
   return 0;
}

int CTimerManager::OnCreateTimer( CMMTimer* )
{
   return 0;
}

int CTimerManager::OnDeleteTimer( CMMTimer* )
{
   return 0;
}

uint CTimerManager::OnSetPeriod( CMMTimer*, uint )
{
   return 0;
}

int CTimerManager::ProcWork()
{
   TimerIdEx timer_id;
   CEventEx evtTimer_1ms;

   std::function<void( void* )> func = []( void* ptr ) {
      CEventEx* event = reinterpret_cast<CEventEx*>( ptr );
      event->SetEvent();
   };

   //auto bind_func = std::bind(&CTimerManager::TimerEvent, this,  std::placeholders::_1);
   if( DONE != timer_ex::CreateTimer( timer_id, m_MinResolution, func, &evtTimer_1ms ) )
      return 1;

   while( true ) {
      if( GetStatus( State_Signal_Deactivate ) )
         break;

      evtTimer_1ms.Wait();
      SendEvent();
   }

   timer_ex::DeleteTimer( timer_id );

   return 0;
}

//////////////////////////////////////////////////////////////////////////
//
// class CTimerManagerWmo
//
//////////////////////////////////////////////////////////////////////////

class CTimerManagerWmo::CTimerItem {
public:
   TimerIdEx nTimerID = 0;
   uint nPeroid;
   int nRefCount;
   CEventEx Event;

   CTimerItem()
   {
      nTimerID  = 0;
      nPeroid   = 0;
      nRefCount = 0;
   }
};

CTimerManagerWmo::CTimerManagerWmo()
    : CTimerManager()
{
}

CTimerManagerWmo::~CTimerManagerWmo()
{
}

void CTimerManagerWmo::GetEventTimer( std::vector<CEventEx*>& vecEvt, std::vector<CTimerItem*>& vecTimer )
{
   vecEvt.clear();
   vecTimer.clear();

   auto iter = m_mapTimer.begin();
   while( iter != m_mapTimer.end() ) {
      CTimerItem* pTimerItem = iter->second;
      vecEvt.push_back( &pTimerItem->Event );
      vecTimer.push_back( pTimerItem );
      iter++;
   }
}

CTimerManagerWmo::CTimerItem* CTimerManagerWmo::FindTimer( uint nPeriod, bool bDivisor )
{
   uint ms = nPeriod;
   if( bDivisor )
      ms = GetDivisor( nPeriod );

   CTimerItem* pTimerItem = nullptr;

   auto iter = m_mapTimer.find( ms );
   if( m_mapTimer.end() != iter )
      pTimerItem = iter->second;

   return pTimerItem;
}

void CTimerManagerWmo::SendEvent( CTimerItem* pTimerItem )
{
   CCriticalSectionSP co( m_csVecTimers );

   for( CMMTimer* item : m_vecTimers ) {
      if( 0 == item->GetPeriod() || 0 == pTimerItem->nPeroid )
         continue;
      if( item->GetPeriod() % pTimerItem->nPeroid )
         continue;

      item->m_nTickCount += pTimerItem->nPeroid;
      if( !item->m_bStopTimer ) {
         if( item->m_nTickCount >= item->m_nPeriod ) {
            item->m_evtTimer.SetEvent();
            item->m_nTickCount = 0;
         }
      }
   }
}

void CTimerManagerWmo::CheckRefCountTimer()
{
   CCriticalSectionSP co( m_csMapTimer );

   auto iter = m_mapTimer.begin();
   while( iter != m_mapTimer.end() ) {
      CTimerItem* pTimerItem = iter->second;
      if( pTimerItem->nRefCount <= 0 ) {
         timer_ex::DeleteTimer( pTimerItem->nTimerID );
         delete pTimerItem;
         m_mapTimer.erase( iter++ );
      } else
         iter++;
   }
}

void CTimerManagerWmo::ClearMapItem()
{
   CCriticalSectionSP co( m_csMapTimer );

   auto iter = m_mapTimer.begin();
   while( iter != m_mapTimer.end() ) {
      CTimerItem* pTimerItem = iter->second;
      timer_ex::DeleteTimer( pTimerItem->nTimerID );
      delete pTimerItem;
      iter++;
   }
   m_mapTimer.clear();
}

uint CTimerManagerWmo::OnSetPeriod( CMMTimer* timer, uint before_period )
{
   CCriticalSectionSP co( m_csMapTimer );

   if( before_period && before_period != timer->GetPeriod() ) {
      CTimerItem* pTimerItem = FindTimer( before_period, true );
      if( pTimerItem && pTimerItem->nRefCount > 0 )
         pTimerItem->nRefCount--;
   }

   uint ms = GetDivisor( timer->GetPeriod() );

   CTimerItem* pTimerItem = FindTimer( ms, false );
   if( pTimerItem ) {
      pTimerItem->nRefCount++;
   } else {
      TimerIdEx timer_id = 0;
      pTimerItem         = new CTimerItem;

      std::function<void( void* )> func = []( void* ptr ) {
         CEventEx* event = reinterpret_cast<CEventEx*>( ptr );
         event->SetEvent();
      };

      timer_ex::CreateTimer( timer_id, ms, func, &pTimerItem->Event );

      pTimerItem->nPeroid  = ms;
      pTimerItem->nTimerID = timer_id;
      pTimerItem->nRefCount++;
      m_mapTimer[ms] = pTimerItem;
   }

   return 0;
}

int CTimerManagerWmo::ProcWork()
{
   CWaitForMultipleObjects wmo;
   std::vector<CEventEx*> vecEvt;
   std::vector<CTimerItem*> vecTimer;

   while( true ) {
      if( GetStatus( State_Signal_Deactivate ) )
         break;

      if( m_mapTimer.empty() ) {
         Sleep_ms( GetMinResolution() );
         continue;
      }

      GetEventTimer( vecEvt, vecTimer );
      wmo.Init( vecEvt );
      wmo.WaitForMultipleObjects( FALSE, INFINITE );

      std::vector<bool> vecSignal = wmo.GetSignal();
      for( size_t ii = 0; ii < vecSignal.size(); ii++ ) {
         if( vecSignal[ii] ) {
            CTimerItem* pTimerItem = vecTimer[ii];
            if( pTimerItem )
               SendEvent( pTimerItem );
         }
      }

      CheckRefCountTimer();
   }

   wmo.ClearAllEventEx();
   ClearMapItem();

   return 0;
}

//////////////////////////////////////////////////////////////////////////
//
// class CTimerManagerDirect
//
//////////////////////////////////////////////////////////////////////////

class CTimerManagerDirect::CTimerList {
public:
   TimerIdEx nTimerID;
   uint nPeroid      = 0;
   int nRefCount     = 5;
   bool bCreateTimer = false;

   CCriticalSection csVec;
   std::vector<CMMTimer*> vecMMTimer;

   explicit CTimerList( uint ms )
   {
      nPeroid = ms;
      ResetCount();
   }

   ~CTimerList()
   {
      if( bCreateTimer )
         timer_ex::DeleteTimer( nTimerID );
   }

   void ResetCount()
   {
      nRefCount = 3000 / nPeroid; // 3 seconds count
   }

   CMMTimer* Find( uint nID )
   {
      CMMTimer* pMMTimer = nullptr;
      for( CMMTimer* item : vecMMTimer ) {
         if( nID == item->m_nTimerID ) {
            pMMTimer = item;
            break;
         }
      }

      return pMMTimer;
   }

   void Push( CMMTimer* pMMTimer )
   {
      CCriticalSectionSP co( csVec );
      vecMMTimer.push_back( pMMTimer );

      if( false == bCreateTimer ) {
         std::function<void( void* )> func = []( void* ptr ) {
            CTimerList* pTmList = reinterpret_cast<CTimerList*>( ptr );
            pTmList->SendEvent();
         };

         timer_ex::CreateTimer( nTimerID, nPeroid, func, this );
         bCreateTimer = true;
      }

      ResetCount();
   }

   void Clear( uint nID )
   {
      CCriticalSectionSP co( csVec );
      auto iter = vecMMTimer.begin();
      while( iter != vecMMTimer.end() ) {
         CMMTimer* pMMTimer = *iter;
         if( pMMTimer->m_nTimerID == nID ) {
            vecMMTimer.erase( iter );
            break;
         }
         iter++;
      }
   }

   void SendEvent()
   {
      CCriticalSectionSP co( csVec );

      if( vecMMTimer.empty() ) {
         nRefCount--;
         return;
      }

      auto iter = vecMMTimer.begin();
      while( iter != vecMMTimer.end() ) {
         CMMTimer* pMMTimer = *iter;
         if( pMMTimer->GetPeriod() % nPeroid ) {
            //iter = vecMMTimer.erase( iter );
         } else {
            pMMTimer->m_nTickCount += nPeroid;
            if( !pMMTimer->m_bStopTimer ) {
               if( pMMTimer->m_nTickCount >= pMMTimer->m_nPeriod ) {
                  pMMTimer->m_evtTimer.SetEvent();
                  pMMTimer->m_nTickCount = 0;
               }
            }
            iter++;
         }
      }
   }
};

CTimerManagerDirect::CTimerManagerDirect()
    : CTimerManager()
{
}

CTimerManagerDirect::~CTimerManagerDirect()
{
}

int CTimerManagerDirect::CreateTimerList( CMMTimer* pMMTimer )
{
   if( 0 == pMMTimer->GetPeriod() )
      return 1;

   uint ms = GetDivisor( pMMTimer->GetPeriod() );

   auto iter = m_mapTimerList.find( ms );
   if( m_mapTimerList.end() == iter ) {
      CTimerList* pTmList = new CTimerList( ms );
      pTmList->Push( pMMTimer );
      m_mapTimerList[ms] = pTmList;

   } else {
      CTimerList* pTmList = iter->second;
      pTmList->csVec.Lock();
      if( !pTmList->Find( pMMTimer->m_nTimerID ) )
         pTmList->Push( pMMTimer );
      pTmList->csVec.Unlock();
   }
   return ( DONE );
}

void CTimerManagerDirect::DeleteTimerList( uint id )
{
   auto iter = m_mapTimerList.begin();
   while( iter != m_mapTimerList.end() ) {
      CTimerList* pTmList = iter->second;
      pTmList->Clear( id );
      iter++;
   }
}

void CTimerManagerDirect::DeleteTimerList( CMMTimer* pMMTimer )
{
   DeleteTimerList( pMMTimer->m_nTimerID );
}

void CTimerManagerDirect::CheckRefCountTimer()
{
   auto iter = m_mapTimerList.begin();
   while( iter != m_mapTimerList.end() ) {
      CTimerList* pTmList = iter->second;

      if( pTmList->nRefCount <= 0 && pTmList->vecMMTimer.empty() ) {
         delete pTmList;
         m_mapTimerList.erase( iter++ );
      } else {
         iter++;
      }
   }
}

void CTimerManagerDirect::ClearTimerList()
{
   auto iter = m_mapTimerList.begin();
   while( iter != m_mapTimerList.end() ) {
      CTimerList* pTmList = iter->second;
      delete pTmList;
      iter++;
   }
   m_mapTimerList.clear();
}

int CTimerManagerDirect::OnDeleteTimer( CMMTimer* timer )
{
   DeleteTimerList( timer );
   return 0;
}

int CTimerManagerDirect::OnDeactivate()
{
   m_evtTimerList.SetEvent();
   return 0;
}

uint CTimerManagerDirect::OnSetPeriod( CMMTimer* timer, uint before_period )
{
   if( before_period && before_period != timer->GetPeriod() )
      DeleteTimerList( timer );

   m_evtTimerList.SetEvent();

   return 0;
}

int CTimerManagerDirect::ProcWork()
{
   while( true ) {
      if( GetStatus( State_Signal_Deactivate ) )
         break;

      m_evtTimerList.Wait();

      CCriticalSectionSP co( m_csVecTimers );
      for( CMMTimer* item : m_vecTimers ) {
         CreateTimerList( item ); // 최소 해상도의 타이머를 만들지 여부를 판단
      }

      CheckRefCountTimer();
   }

   ClearTimerList();

   return 0;
}

//////////////////////////////////////////////////////////////////////////
//
// class CTimerManagerImpl
//
//////////////////////////////////////////////////////////////////////////

class CTimerManagerSP::CTimerManagerImpl {
private:
   static Active_Type m_nType;
   static CCriticalSection m_csRef;
   static int m_nRef;
   static CTimerManager* m_pTimerManager;

   CTimerManagerImpl();
   ~CTimerManagerImpl();

public:
   static void Activate( Active_Type type )
   {
      m_nType = type;
   }

   static CTimerManager* getInstance()
   {
      CCriticalSectionSP co( m_csRef );
      m_nRef++;

      if( nullptr == m_pTimerManager ) {
         switch( m_nType ) {
         case Type_Direct:
            m_pTimerManager = new CTimerManagerDirect();
            m_pTimerManager->SetMinResolution( 10 );
            break;
         case Type_Wmo:
            m_pTimerManager = new CTimerManagerWmo();
            m_pTimerManager->SetMinResolution( 10 );
            break;
         case Type_Original:
            m_pTimerManager = new CTimerManager();
            m_pTimerManager->SetMinResolution( 1 );
            break;
         }

         m_pTimerManager->Activate();
      }

      return m_pTimerManager;
   }

   static void freeInstance()
   {
      CCriticalSectionSP co( m_csRef );
      m_nRef--;

      if( 0 == m_nRef && m_pTimerManager ) {
         m_pTimerManager->Deactivate();
         delete m_pTimerManager;
         m_pTimerManager = nullptr;
      }
   }
};

CTimerManager* CTimerManagerSP::CTimerManagerImpl::m_pTimerManager = nullptr;
CCriticalSection CTimerManagerSP::CTimerManagerImpl::m_csRef;
int CTimerManagerSP::CTimerManagerImpl::m_nRef                           = 0;
CTimerManagerSP::Active_Type CTimerManagerSP::CTimerManagerImpl::m_nType = Type_Original;

//////////////////////////////////////////////////////////////////////////
//
// class CTimerManagerImpl
//
//////////////////////////////////////////////////////////////////////////

CTimerManagerSP::CTimerManagerSP()
{
   m_pTimerManager = CTimerManagerImpl::getInstance();
}

CTimerManagerSP::~CTimerManagerSP()
{
   CTimerManagerImpl::freeInstance();
}

void CTimerManagerSP::Activate( Active_Type type )
{
   CTimerManagerImpl::Activate( type );
}

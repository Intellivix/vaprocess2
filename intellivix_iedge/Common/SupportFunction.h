#pragma once

#include <map>
#include <mutex>

enum ProdType
{
   ProdType_Normal = 0,
   ProdType_Evaluation_Edition,
};

typedef struct _tagSupportItem
{
   char szSupport[64];

   _tagSupportItem()
   {
      memset(szSupport, 0, sizeof(szSupport));
   }
   _tagSupportItem(const char* szData)
   {
      memset(szSupport, 0, sizeof(szSupport));
      int nLen = strlen(szData);
      strncpy(szSupport, szData, nLen>63 ? 63 : nLen);
   }

   bool operator == (const _tagSupportItem &T) const {
      int nCmp = strncmp(szSupport, T.szSupport, 63);
      return 0 == nCmp ? true : false;      
   }
   bool operator != (const _tagSupportItem &T) const {
      return !(*this == T);
   }
   bool operator < (const _tagSupportItem &T) const {
      int nCmp = strncmp(szSupport, T.szSupport, 63);
      if( 0 > nCmp )
         return true;
      return false;
   }
}
SUPPORT_ITEM;

class CSupportFunction
{
public:  // Singleton Member & Method
   static CSupportFunction*   m_pThis;
   static CSupportFunction*   GetInstance();
   void                       FreeInstance();   

public:
   typedef std::map<SUPPORT_ITEM, int>   MapSupport;
   typedef MapSupport::const_iterator    MapSupport_Const_iter;
   typedef MapSupport::iterator          MapSupport_iter;

   MapSupport        m_mapFunction;

   std::string       m_strProdCode;
   int               m_iProdVer;
   CCriticalSection  m_csLock;

public:
   CSupportFunction();
   ~CSupportFunction();   

   int   Initialize(const std::string& strProdCode, int iProdVer, int iProdType);
   BOOL  IsSupport(const SUPPORT_ITEM& type);

   int     GetProdVer()        {  return m_iProdVer;      }
   std::string GetProdCode()   {  return m_strProdCode;   }

private:   

   void  SetProdCode(const std::string& type);
   void  MapAdd(const SUPPORT_ITEM& type);
   BOOL  MapFind(const SUPPORT_ITEM& type);
   void  MapErase(const SUPPORT_ITEM& type);

   int   SupportList_After();
   int   SupportList_Before();

   int   SupportList_B200();

   int   SupportList_F100();
   int   SupportList_F200();

   int   SupportList_T110();
   int   SupportList_TF110();

   int   SupportList_G100();   
   int   SupportList_G100F();
   int   SupportList_G100_EVAL();
   int   SupportList_G100_GPS();
   int   SupportList_G200();

   int   SupportList_R200();

   int   SupportList_G300();
   int   SupportList_G400();

   int   SupportList_M200();
   int   SupportList_M200F();

   int   SupportList_P400();

   int   SupportList_RAS();

   int   SupportList_SRS();

   int   SupportList_TOTALLINTELLIVIX();
};

//--------------------------------------------------------------------------------------//

BOOL IsSupport(const char* szType);
BOOL IsProduct(const char* szType);
int  GetProdVer();
std::string GetProdCode();

#define IS_SUPPORT(type)         (IsSupport(_T(#type)))
#define IS_NOT_SUPPORT(type)     (!IsSupport(_T(#type)))

#define IS_PRODUCT(type)         (IsProduct(_T(#type)))
#define IS_NOT_PRODUCT(type)     (!IsProduct(_T(#type)))

#define PROD_VER()               (GetProdVer())
#define PROD_CODE()              (GetProdCode())

// Cryptor.h: interface for the CCryptor class.
//
//////////////////////////////////////////////////////////////////////

#pragma once
#ifdef __SUPPORT_WIN32_CRYPT
#include <Wincrypt.h>
#define BUF_SIZE 1000

class CCryptor  
{
public:
	CCryptor(LPCSTR szKeyContainerName);
	virtual ~CCryptor();

	HCRYPTPROV hProv; 
	HCRYPTKEY  hKey, hKeyXchg;
	PBYTE pbData, pbKeyBlob;
	BYTE	buffer[BUF_SIZE];
	char  m_szKeyContainerName[32];

	void Release();
	int Encrypt(LPCSTR srcText, int& nLength, ALG_ID algid = CALG_RC2);
	int Decrypt(BYTE* srcBuffer);
};

#define MAX_CRYPT_LETTER 32
class CCryption
{
public:
	CCryption(void);
	~CCryption(void);

	char	m_szEncrypted[MAX_CRYPT_LETTER];
	char	m_szDecrypted[MAX_CRYPT_LETTER];

	BOOL	Encrypt(LPCSTR szSrcText, int& nEncryptedLength);
	BOOL	Decrypt(LPCSTR szSrcText, int nSrcLength);
};

#endif

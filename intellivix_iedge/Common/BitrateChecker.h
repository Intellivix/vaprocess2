#pragma once

#include <cinttypes>

class CBitrateCheckItem
{
public:
   uint32_t m_nTime;
   uint32_t m_nDataSizeInByte;

public:
   CBitrateCheckItem (   );
};


class CBitrateChecker
{
public:
   CBitrateCheckItem* m_pItems;

public:
   int m_nStartIdx;
   int m_nEndIdx;
   int m_nItemNum;
   int m_nMaxItemNum;
   int m_nFramePeriod;
   int m_nLogPeriod;

public:
   CBitrateChecker (   );
  ~CBitrateChecker (   );

public:
   void Initialize  (float fFrameRate, float fLogPeriod);
   int  GetItemIdx  (int nCurIdx, int nDeltaIdx);
   void AddItem     (CBitrateCheckItem& item);
   int  FindItemIdx (int nDeltaTime);
   CBitrateCheckItem* GetLastItem ( );

public:
   float CalcAvgBitrate (float fDeltaTimeInSec);
};


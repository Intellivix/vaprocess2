﻿#pragma once

#ifdef WIN32

#include "vacl_event.h"
#include "vacl_pathpass.h"
using namespace VACL;

class COnscreenDisplayEx

{
public:
   int Width;
   int Height;

public:
   COnscreenDisplayEx (   );

public:
	void DisplayCalibrationPoints        (CDC *dc,FP2DArray1D &c_points,int sel_cp_no,FPoint2D& scale);
   void DisplayEventZones               (CDC *dc,EventZoneList &ez_list);
   void DisplayEventZone                (CDC *dc,EventZone* evt_zone,FPoint2D &scale);
   void DisplayEventZone                (CDC *dc,EventZone* evt_zone);
   void DisplayPolygonalAreas           (CDC *dc,PolygonalAreaList &pa_list,COLORREF color,int brush_style);
   void DrawArrow                       (CDC *dc,IPoint2D &s_pos,float direction,float length,int pen_width,COLORREF color);
   void DrawCounter                     (CDC *dc,EventZone *evt_zone,FPoint2D &scale);
   void DrawCrossMark                   (CDC *dc,IPoint2D &pos,COLORREF crColor,int length,int pen_width,FPoint2D &scale);
   void DrawEventZone                   (CDC *dc,EventZone *evt_zone,FPoint2D &scale);
   void DrawEventZone_PathPassing       (CDC *dc,EventZone *evt_zone,FPoint2D &scale);
   void DrawEventZone_DirectionalMotion (CDC *dc,EventZone *evt_zone,FPoint2D &scale);
   void DrawEventZone_Dwell             (CDC *dc,EventZone *evt_zone,FPoint2D &scale,int options);
   void DrawEventZone_WaterLevel        (CDC *dc,EventZone *evt_zone,FPoint2D &scale);
   void DrawLine                        (CDC *dc,IPoint2D &s_pos,IPoint2D &e_pos,COLORREF color,int thickness,int pen_style = PS_SOLID);
   void DrawLine                        (CDC *dc,IPoint2D &s_pos,IPoint2D &e_pos,COLORREF color,int thickness,FPoint2D &scale);
   void DrawPie                         (CDC *dc,IPoint2D &c_pos,float direction,float range,float radius,COLORREF color);
   void DrawRectangle                   (CDC *dc,IBox2D &s_box,COLORREF color,int thickness,FPoint2D &scale);
   void DrawText                        (CDC *dc,const char *text,int x,int y,COLORREF color);
   void GetBitmap                       (byte *s_buffer,ISize2D &size,CBitmap &d_bitmap);
   void GetImage                        (CBitmap &s_bitmap,BGRImage &d_image);
   void SetScreenSize                   (ISize2D &size);
   void DrawPathZone                    (CDC *dc,PathZone* path_zone,FPoint2D &scale);
   void DrawPathZones                   (CDC *dc,PathZone* path_zone,FPoint2D &scale);
   void DrawPathArrow                   (CDC *dc,IPoint2D &s_pos,IPoint2D &e_pos,int direction,COLORREF color,FPoint2D &scale);
   void DrawPathArrowLine               (CDC *dc,LinkedList<Arrow>& arrows,int direction,FPoint2D &scale);
   void DrawPoint                       (CDC *dc,IPoint2D &pos,COLORREF crColor,int radius,FPoint2D &scale);
};

#endif // WIN32

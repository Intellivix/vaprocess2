#pragma once

#ifdef __SUPPORT_FISHEYE_CAMERA

#include "vacl.h"
#include <gl/gl.h>
#include <gl/glu.h>

enum FisheyePosition 
{
   CEILING,
   GROUND,  // 미구현
   WALL     // 미구현
};

enum FisheyeViewType 
{
   DEFALUT_360,
   SINGLE_PANORAMA,
   DOUBLE_PANORAMA,
   HALF_PANORAMA
};

typedef struct tagFISHEYE_INFO 
{
   float             up_range;
   float             lp_range;
   float             f_length;
   float             radius;
   FPoint2D          center;
   FisheyePosition   pos;
   FisheyeViewType   view_type;
} FISHEYE_INFO, *LPFISHEYE_INFO;

///////////////////////////////////////////////////////////////////////////////
//
// Class: WarpingTable_FisheyeToCylinder
//
///////////////////////////////////////////////////////////////////////////////

 class WarpingTable_FisheyeToCylinder : public WarpingTable
 
{
   protected:
      int Flag_Initialized;

   public:
      int Initialize (int si_width,int si_height,FPoint2D& center,float radius,float up_range,float lp_range,int di_width,int di_height);
      int IsInitialized();
};

inline int WarpingTable_FisheyeToCylinder::IsInitialized (   )
 
{
   return (Flag_Initialized);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: WarpingTable_FisheyeToSphere
//
///////////////////////////////////////////////////////////////////////////////

 class WarpingTable_FisheyeToSphere : public WarpingTable
 
{
   protected:
      int Flag_Initialized;

   public:
      int Initialize (int si_width,int si_height,FPoint2D& center,float radius,int di_width,int di_height);
      int IsInitialized();
};

inline int WarpingTable_FisheyeToSphere::IsInitialized (   )
 
{
   return (Flag_Initialized);
}
///////////////////////////////////////////////////////////////////////////////
//
// Class: CGLVirtualPTZ
//
///////////////////////////////////////////////////////////////////////////////

 class CGLVirtualPTZ

{
   protected:
      int Flag_Initialized;
      int NumSrcImgDivs;

   protected:
      HDC   DC;
      HGLRC GLRC;
      CWnd  RenderWnd;

   protected:
      struct _FACE {
         FPoint2D TexVertices[4];
         FPoint3D Vertices[4];
      };
      struct _SURFACE {
         GLuint TexID;
         Array1D<_FACE> Faces;
      };
      Array1D<_SURFACE> Surfaces;
      
   public:
      float   PanAngle;
      float   TiltAngle;
      float   FocalLength;
      float   VFOV;
      ISize2D SrcImgSize;
      ISize2D RndImgSize;
   
   public:
      CGLVirtualPTZ (   );
      virtual ~CGLVirtualPTZ (   );
   
   protected:
      void   _Init               (   );
      void   CreateFaces         (int si_width,int si_height,int n_div,float s_phi,float e_phi,float s_theta,float e_theta,Array1D<_FACE>& d_array);
      GLuint CreateTextureObject (int si_width,int si_height);
      int    CreateRenderWindow  (   );
      int    DeleteTextureObject (GLuint tex_id);
      void   GetTexImgSize       (int si_width,int si_height,int& ti_width,int& ti_height);

   public:
      virtual void Close (   );
      
   public:
      int Initialize     (int si_width,int si_height,int di_width,int di_height,int n_si_div = 4,int n_sf_div = 30);
      int IsInitialized  (   );
      int Render         (BGRImage& d_image);
      int SetFocalLength (float f_length);
      int SetPanTiltPos  (float pan,float tilt);
      int SetSrcImage    (BGRImage& s_image);
      int SetVFOV        (float v_fov);
};

 inline int CGLVirtualPTZ::IsInitialized (   )
 
{
   return (Flag_Initialized);
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

// 어안렌즈 영상의 중심과 반지름을 구함
int ExtractCircumcircle                    (FISHEYE_INFO& fisheye_info, GImage& s_image,byte b_thrsld);
// 원통 영상의 너비로 높이를 구함
int GetCylindricalImageHeight              (int ci_width,float up_range,float lp_range);
// 원통 영상의 높이로 너비를 구함
int GetCylindricalImageWidth               (int ci_height,float up_range,float lp_range);
// FocalLength 로 HFOV, VFOV 구함
void GetFOVs                               (int si_width,int si_height,float f_length,float& h_fov,float& v_fov);
// HFOV로 FocalLength 구함
float GetFocalLengthFromHFOV               (int si_width,float h_fov);
// VFOV로 FocalLength 구함
float GetFocalLengthFromVFOV               (int si_height,float v_fov);

// VPTZ 카메라에서 절대각 이동을 위한 Pan,Tilt 각을 구함
void GetNewPanTiltPos                      (int si_width, int si_height, float c_pan,float c_tilt,float f_length, FPoint2D& s_pos, float& n_pan, float& n_tilt);
// 어안렌즈 영상의 X,Y 좌표를 원통형 영상의 X,Y좌표로 변환
void ConvertFisheyeToCylindricalCoord      (FISHEYE_INFO& fisheye_info, int ci_width, int ci_height, FPoint2D& s_coord, FPoint2D& d_coord);
// 원통형 영상의 X,Y 좌표를 어안렌즈 영상의 X,Y 좌표로 변환
void ConvertCylindricalToFisheyeCoord      (FISHEYE_INFO& fisheye_info, int ci_width,int ci_height,FPoint2D& s_coord,FPoint2D& d_coord);
// 어안렌즈 영상의 X,Y 좌표를 Pan,Tilt 각으로 변환
void ConvertFisheyeToPanTiltPos            (FISHEYE_INFO& fisheye_info, FPoint2D& s_coord, float& d_pan, float& d_tilt);
// 어안렌즈 영상의 X,Y 좌표를 더블 파노라마 영상의 X,Y 좌표로 변환
void ConvertFisheyeToDoublePanoramaCoord   (FISHEYE_INFO& fisheye_info, int dp_width,int dp_height, FPoint2D& s_coord, FPoint2D& d_coord);

// 어안렌즈 영상을 원통형 영상으로 변환
void ConvertFisheyeToCylindricalImage      (FISHEYE_INFO& fisheye_info, GImage& s_image, GImage& d_image);
void ConvertFisheyeToCylindricalImage_YUY2 (FISHEYE_INFO& fisheye_info, byte* s_buffer,int s_width, int s_height, byte* d_buffer, int d_width, int d_height);
// 원통형 영상으로 변환 된 어안렌즈 영상을 더블 파노라마 영상으로 변환
void  Make180DoublePanorama_YUY2           (byte* si_buffer, int si_width, int si_height, byte* di_buffer);
// 어안렌즈 영상을 VPTZ 카메라 출력용 평면 영상으로 변환
void ConvertFisheyeToPlanarImage           (FISHEYE_INFO& fisheye_info, GImage& s_image, float pan, float tilt, GImage& d_image);
void ConvertFisheyeToPlanarImage_YUY2      (FISHEYE_INFO& fisheye_info, byte* s_buffer,int s_width, int s_height, float pan, float tilt, byte* d_buffer, int d_width, int d_height);
// 어안렌즈 영상을 구 모양 영상으로 변환
void ConvertFisheyeToSphericalImage        (FISHEYE_INFO& fisheye_info, GImage& s_image, GImage& d_image);
void ConvertFisheyeToSphericalImage_YUY2   (FISHEYE_INFO& fisheye_info, byte* s_buffer,int s_width, int s_height, byte* d_buffer, int d_width, int d_height);

// 원통형 영상으로 변환 된 어안렌즈 영상을 하프 파노라마 영상으로 변환, offset은 파노라마의 중심 이동 픽셀
void  Make180HalfPanorama_YUY2             (byte* si_buffer, int si_width, int si_height, byte* di_buffer, int center_x = 0);

#endif // __SUPPORT_FISHEYE_CAMERA
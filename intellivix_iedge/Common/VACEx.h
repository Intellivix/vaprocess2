﻿#pragma once

#include "vacl_event.h"
#include "vacl_objtrack.h"
using namespace VACL;

#include "version.h"
#include "CommonUtil.h"
#include "version.h"

#define __SUPPORT_EVENT_ALARM_LEVEL

///////////////////////////////////////////////////////////////////////////////
//
// Definitions
//
///////////////////////////////////////////////////////////////////////////////

#define TO_TYPE_HEAD 0x00000200

#define ER_EVENT_NO_VIDEO_SIGNAL     61

#define FD_OPTION_PERFORM_FACE_BRIGHTNESS_AND_CONTRAST 0x10000000
#define FD_OPTION_ENABLE_SCHEDULE_FOR_FACE_BRIGHTNESS_AND_CONTRAST 0x20000000
#define FD_OPTION_PERFORM_FACE_HISTOGRAM_AND_EQUALIZATION 0x40000000
#define FD_OPTION_ENABLE_SCHEDULE_FOR_FACE_HISTOGRAM_AND_EQUALIZATION 0x80000000

#define ER_OBJECT_HEAD 0x00000200

//#pragma pack(1)

enum IVXXMLIOOpt {
   IVXXMLIOOpt_DoNotEnDecodeImage = 1, // 파노라마, 프리셋 정보의 경우 설정을 비교하거나 전송할 때는 인,디코딩을 할 필요가 없을 경우 이 옵션을 켜야한다.
};

///////////////////////////////////////////////////////////////////
//
// class : HeadDetectionConfig
//
///////////////////////////////////////////////////////////////////

class HeadDetectionConfig {

public:
   CPoint HeadStartPoint;
   CPoint HeadEndPoint;
   int HRRadius;

public:
   HeadDetectionConfig();

public:
   int ReadFile( CXMLIO* pIO );
   int WriteFile( CXMLIO* pIO );
   int CheckSetup( HeadDetectionConfig* hdc );
};

///////////////////////////////////////////////////////////////////
//
// class : FaceDetectionConfig
//
///////////////////////////////////////////////////////////////////

class FaceDetectionConfig
{
 public:
   int   m_nFaceRecogType;
    uint  m_nObjectAnalysisMode = 0;

   // FaceDetection 클래스 내부에서 직접적으로 사용하는 변수
   // 주목 시간 계산 용
   float m_fMaxPitch;
   float m_fMaxRoll;
   float m_fMaxYaw;
   // 얼굴 검출 관련 파라메터
   int m_nMinFaceSize;
   int m_nMaxFaceSize;
   int m_nSWStepSize; // Window step size
   float m_fIPScaleFactor; // Image pyramid scale factor
   float m_fFDScoreThres; // Face Detection Score threshold

   // FaceDetecion 클래스와 별도로 사용하는 변수
   BOOL  m_bFaceEstimation; // Age, Gender 판별 기능 사용 여부
   DWORD m_nScheduleAnalysisTime;
   DWORD m_nMaxAnalysisTime;
   BOOL  m_bFFVbyFrame;

   // Relay Out
   BOOL  m_bChkRelayOut;
   int   RelayOutputDeviceType;
   int   RelayOutputNo;           // 릴레이 출력 채널의 번호.
   char  RelayOutPutIP[32] = {0}; // ET0808 연동용 릴레이 출력 장비 IP
   int   RelayOutputPort = 0;     // ET0808 연동용 릴레이 출력 장비 Port

 public:
    FaceDetectionConfig (   );

public:
    int ReadFile  (CXMLIO* pIO);
    int WriteFile (CXMLIO* pIO);
    int CheckSetup (FaceDetectionConfig* fdc);
 };


///////////////////////////////////////////////////////////////////
//
// Enumerations
//
///////////////////////////////////////////////////////////////////

#define EV_STATUS_ALARM_ENDED 0x00000020
#define EV_STATUS_AFTER_ALARM_ENDED 0x00000040

// AlarmType 프로퍼티의 값. 복수 설정 가능.
#define ER_AT_SOUND 0x00000001
#define ER_AT_RELAY 0x00000002
#define ER_AT_EMAIL 0x00000004
#define ER_AT_XML 0x00000008
#define ER_AT_EVENT_ALARM 0x00000010
#define ER_AT_EVENT_MSG 0x00000020
#define ER_AT_REALTIME_EVENT 0x00000040
#define ER_AT_LIGHT 0x00000080

// 이메일 관련 옵션
#define ER_EM_INCLUDE_IMAGE 0x00000001
#define ER_EM_RENDER_BOUNDING_BOX 0x00000002
#define ER_EM_RENDER_TRAJECTORIES 0x00000004
#define ER_EM_RENDER_EVENT_ZONE 0x00000008

#define ER_STATUS_EVENT_BEGUN 0x10000000
#define ER_STATUS_EVENT_ONGOING 0x20000000
#define ER_STATUS_EVENT_ENDED 0x40000000

enum _EventZoneType {
   EVT_ZONE_TYPE_NONE = 0,
   EVT_ZONE_TYPE_INFLOW,
   EVT_ZONE_TYPE_SPILL,
   EVT_ZONE_TYPE_POPULATION,
   EVT_ZONE_TYPE_AREADWERLLTIME,
   EVT_ZONE_TYPE_INTERSECTION,
   EVT_ZONE_TYPE_FACE,
};

enum _EventRelayOutputDevice {
   EVT_RO_CAPTURE_BOARD = 0x0000001,
   EVT_RO_ET0808        = 0x0000002,
};

enum RelayOutputDeviceType
{
   RelayOutputDeviceType_CaptureBoard = 0x0000001,
   RelayOutputDeviceType_IPCamera_DIO = 0x0000002,
   RelayOutputDeviceType_ET0808       = 0x0000003,

};

enum Face_Type
{
   FACE_DETECTION_ONLY_MODE = 0,     // 로컬에서 얼굴 검출만 수행함
   FACE_ANALYSIS_MODE       = 1,     // 로컬에서 얼굴 나이/성별 분석을 수행함
   FACE_RECOGNITION_MODE    = 2,     // 서버에서 얼굴 인식을 수행함
   FACE_SEARCH_TYPE         = 3,     // MDB 얼굴 검색
   FACE_DETECT_TYPE         = 4,     // 얼굴 추적
};

enum ObjectAnalysisMode
{
   ObjectAnalysisMode_FaceAgeGender           = 0x00000001,
   ObjectAnalysisMode_FaceRecognition         = 0x00000002,
   ObjectAnalysisMode_PersonAttribute         = 0x00000004,
   ObjectAnalysisMode_VehicleAttribute        = 0x00000008,
   ObjectAnalysisMode_DNNObjectClassification = 0x00000010,
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleEx
//
///////////////////////////////////////////////////////////////////////////////

class EventRuleEx : public EventRule {
public:
   int AlarmType;
   int EmailOptions; // 이메일 관련 옵션
   int RelayOutputDevice; // 릴레이 출력 장비
   char RelayOutPutIP[32]; // 릴레이 출력 장비 IP
   int RelayOutputPort; // 릴레이 출력 장비 Port
   int RelayOutputNo; // 릴레이 출력 채널의 번호.
   char SoundFileName[256]; // 경보음 파일의 패스명.
   char SoundFileName2[256]; // 경보음 파일의 패스명.
   BOOL bEnableLocalSpeaker; // 본체 스피커 출력
   BOOL bEnableCamSpeaker; // 카메라 스피커 출력

   //이벤트 종류 설정
   int m_EventZoneType;
   int m_CameraType;

   // XProtect: Alarm Item
   std::string m_szEventAlarmItemData;
   std::string m_szXProtectEventName;
   std::string m_szXProtectCameraItemData;

   VOID ReplaceChar( TCHAR* pString, TCHAR FindChar, TCHAR NewChar );

public:
   EventRuleEx();
   virtual ~EventRuleEx()
   {
   }

public:
   virtual int ReadFile( CXMLIO* pIO );
   virtual int WriteFile( CXMLIO* pIO );

public:
   virtual int ReadEventCountInfo( CXMLIO* pIO );
   virtual int WriteEventCountInfo( CXMLIO* pIO );
};

void ReadEventCountInfo( CXMLIO* pIO, EventDetection* pEventDetector );
void WriteEventCountInfo( CXMLIO* pIO, EventDetection* pEventDetector );

EventRule* CreateEventRuleInstance();

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventZoneEx (IVR,iBOX용)
//
///////////////////////////////////////////////////////////////////////////////

#define EZ_OP_REFERENCE_PROPERTIES 0x00000001 // 참조 카메라의 속성들을 정의한 이벤트 존

enum _EventAlarmLevel {
   EventAlarmLevel_Caution = 0,
   EventAlarmLevel_Warning = 1,
};

const int EventAlarmLevelNum = 2;

class EventZoneEx : public EventZone {
public:
   int Flag_SlavePTZTracking; // 마스터 슬래이브 방식의 추적에서 PTZ 카메라로 추적 할지에 대한 여부 값.
   int Flag_MagnifyingHumanFaceArea; // 얼굴 영역을 좀더 확대
   int Flag_MagnifyingCarPlateArea; // 차량 번호판 영역을 좀더 확대
   int Options; // 이벤트 존 옵션
   int Priority; // PTZ 추적 시 본 EventZoneEx의 우선 순위. 값이 클수록 높은 우선 순위를 갖는다.
   float ZoomFactor; // 특정 오브젝트가 본 EventZoneEx에 들어왔을 때 배정되는 PTZ 추적 시의 줌 배율.
   ushort PresetID; // 이벤트 존이 속해있는 Preset의 ID
   FPoint2D ViewingPos; // 특정 오브젝트가 본 EventZoneEx에 들어왔을 때 배정되는 PTZ 추적 시의 오브젝트 상의 뷰 위치.
   COMMONID ZoneID; // 이벤트 히스토리에 저장되는 아이디
   COMMONTIME Time_Created; // 이벤트 존이 생성된 시간
#ifdef __SUPPORT_EVENT_ALARM_LEVEL
   int EventAlarmLevel; // 이벤트 영역 별 경보레벨
   int SlaveTrackingDelayTime[EventAlarmLevelNum]; // 슬레이브 카메라 추적까지 대기하는 시간, 단위:초
#endif
   IBox2D     WaterZoneROI;

public:
   EventZoneEx();
   virtual ~EventZoneEx();

   virtual int ReadFile( CXMLIO* pIO );
   virtual int WriteFile( CXMLIO* pIO );
};

EventZone* CreateEventZoneInstance();

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectFilteringEx
//
///////////////////////////////////////////////////////////////////////////////

class ObjectFilteringEx : public ObjectFiltering

{
public:
   IBox2D MinMaxBoundingBox[3][2][2];

public:
   ObjectFilteringEx();
   virtual ~ObjectFilteringEx(){};

public:
   virtual int ReadFile( CXMLIO* pIO );
   virtual int WriteFile( CXMLIO* pIO );

   int IsTheSame( ObjectFiltering& ofx1, ObjectFiltering& ofx2 );
};

ObjectFiltering* CreateObjectFilteringInstance();

///////////////////////////////////////////////////////////////////////////////
//
// Class: Event (IVR용)
//
///////////////////////////////////////////////////////////////////////////////

enum TO_STATUS_EX {
   TO_STATUS_EX_USER_SELECTED                 = 0x00000001, // Master에서 사용자에 의하여 선택된 물체의 경우.
   TO_STATUS_EX_OBJECT_DELETED_IN_IVA_PROCESS = 0x80000000,
};

enum EVENT_STATUS_EX {
   EVENT_STATUS_EX_EVENT_DELETED_IN_IVA_PROCESS = 0x00000001,
};

class EventEx : public Event {
public:
   int EvtRuleID;
   int EvtZoneID;

public:
   int StatusEx;

public:
   FILETIME Time_EventBegun;
   int FrameCount;
   double NPT_StartTime; // Play Normal Time을 기준으로 시작한 시간, 단위:초
   double NPT_EndTime; // Play Normal Time을 기준으로 시작한 시간, 단위:초

public:
   EventEx();
   EventEx( EventZone* evt_zone, EventRule* evt_rule, int evt_type );
   virtual ~EventEx(){};

public:
   EventEx& operator=( const EventEx& evt );

public:
   void GenerateExtendedInfo();
};

Event* CreateEventInstance( EventZone* evt_zone, EventRule* evt_rule, int evt_type );

///////////////////////////////////////////////////////////////////////////////
//
// Class: TrackedObjectEx
//
///////////////////////////////////////////////////////////////////////////////

class TrackedObjectEx : public TrackedObject {
public:
   // 화면 출력에 사용되는 변수
   int StatusEx;
   uint VAEventFlag; // IntelliVIX에서 정의한 이벤트 타입(EID_XXX)을 플레그 형태로 표현하는 변수
   FPoint2D Scale;

public:
   FILETIME Time_ObjectBegun;
   uint EndTime_Locked;

public:
   // M/S 추적 관련 변수
   int Priority; // 객체 우선 순위
   float ZoomFactor; // 자동 PTZ 추적 시 줌 배율
   FPoint2D ViewingPos; //

   int Flag_SlavePTZTracking; // 마스터 슬래이브 방식의 추적에서 PTZ 카메라로 추적 할지에 대한 여부 값.
   int Flag_MagnifyingHumanFaceArea; // 얼굴 영역을 좀더 확대
   int Flag_MagnifyingCarPlateArea; // 차량 번호판 영역을 좀더 확대

public:
   // DB저장용 정보
   std::deque<SBox2D> TrkObjBoxList;
   FILETIME TrkObjBoxStartTime;
   std::deque<int> TrkObjBoxElapsedTimeList;

public:
   // 얼굴인식 관련 변수
   int FaceID;
   int Gender;
   int Age;
   int BlackList;
   uint nStartTime; // 얼굴 인식 서버에 요청 딜레이 시간을 위한 설정
   int nFaceStatus;
   CPoint leftEye;
   CPoint righteye;
   int iRoll;
   int iYaw;
   float fMatchingRate;

public:
   // CVideoBox에서 UI용
   BOOL m_bCandedateTrackObject;

public:
   TrackedObjectEx();
   virtual ~TrackedObjectEx(){};

public:
   TrackedObjectEx& operator=( TrackedObjectEx& to );

public:
   int IsPointInsideBoundingBox( IPoint2D& s_point );

public:
   BOOL ReadXMLForDisplay( CXMLIO* pIO );
   BOOL WriteXMLForDisplay( CXMLIO* pIO );

public:
   void AddTrjectoryItem();
};

TrackedObject* CreateTrackedObjectInstance();

/////////////////////////////////////////////////////////////////////
//
// Class : Schedule
//
/////////////////////////////////////////////////////////////////////

//Sensor Event Schedule
class Schedule {
public:
   Schedule()
   {
      Schedule_WD  = NULL;
      Schedule_HM  = NULL;
      Schedule_YMD = NULL;
   }

   int* Schedule_WD;
   LinkedList<Period_HM>* Schedule_HM;
   LinkedList<Period_YMD>* Schedule_YMD;

public:
   BOOL Read( CXMLIO* pIO );
   BOOL Write( CXMLIO* pIO );
   BOOL CheckSchedule();
   BOOL IsDefaultSetting();
};

//#pragma pack()

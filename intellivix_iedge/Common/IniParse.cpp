﻿#include "StdAfx.h"
#include "IniParse.h"
#include "CommonUtil.h"

#define _UNICODE

CIniParse::CIniParse()
{
}

CIniParse::~CIniParse()
{
   Clear();
}

#include <iostream>
#include <fstream>

FILE* CIniParse::FileOpen( const std::string& strFile, FILE_TYPE emType )
{
   TCHAR szType[32] = {
      0,
   };
   switch( emType ) {
   case FILE_TYPE_UTF8:
      _tcscpy( szType, _T("rb, ccs= UTF-8") );
      break;
   case FILE_TYPE_UNICODE:
      _tcscpy( szType, _T("rb, ccs= UNICODE") );
      break;
   case FILE_TYPE_ANSI:
   default:
      _tcscpy( szType, _T("rb") );
      break;
   }

   // fopen 참고
   // http://msdn.microsoft.com/ko-kr/library/yeby3zcb.aspx
   return _tfopen( strFile.c_str(), szType );
}

BOOL CIniParse::Open( const std::string& strFile, FILE_TYPE emType )
{
   FILE* pFile = FileOpen( strFile, emType );
   if( NULL == pFile )
      return FALSE;

   const int iBufSize    = 512;
   TCHAR szBuf[iBufSize] = {
      0,
   };

   Map_Data* pCurMapData = NULL;
   std::string strSection, strKey, strValue;

   while( !feof( pFile ) ) {
      ZeroMemory( szBuf, sizeof( TCHAR ) * iBufSize );

      if( !fgets( szBuf, iBufSize, pFile ) )
         break;

      int iLen = _tcslen( szBuf );
      if( iLen < 1 )
         continue;

      std::string strLine = szBuf;
      strLine             = sutil::trim( strLine );

      LINE_TYPE emType = GetLineType( strLine );
      switch( emType ) {
      case LINE_TYPE_SECTION:
         if( ParseSection( strLine, strSection ) )
            pCurMapData = FindSection( strSection, TRUE );
         break;
      case LINE_TYPE_DATA:
         if( pCurMapData && ParseData( strLine, strKey, strValue ) )
            ( *pCurMapData )[strKey] = strValue;
         break;
      default:
         break;
      }
   }

   fclose( pFile );

   return TRUE;
}

BOOL CIniParse::stdOpen( const std::string& strFile, FILE_TYPE emType )
{
   std::ifstream fs;
   fs.open( strFile, std::ios::in );

   const int iBufSize    = 512;
   TCHAR szBuf[iBufSize] = {
      0,
   };

   Map_Data* pCurMapData = NULL;
   std::string strSection, strKey, strValue;

   while( fs.getline( szBuf, iBufSize ) ) {
      int iLen = _tcslen( szBuf );
      if( iLen < 1 )
         continue;

      std::string strLine = szBuf;
      strLine             = sutil::trim( strLine );

      LINE_TYPE emType = GetLineType( strLine );
      switch( emType ) {
      case LINE_TYPE_SECTION:
         if( ParseSection( strLine, strSection ) )
            pCurMapData = FindSection( strSection, TRUE );
         break;
      case LINE_TYPE_DATA:
         if( pCurMapData && ParseData( strLine, strKey, strValue ) )
            ( *pCurMapData )[strKey] = strValue;
         break;
      default:
         break;
      }

      ZeroMemory( szBuf, sizeof( TCHAR ) * iBufSize );
   }

   fs.close();

   return TRUE;
}

void CIniParse::Clear()
{
   Map_Section_iter iterSec;

   for( iterSec = m_mapSection.begin(); iterSec != m_mapSection.end(); iterSec++ ) {
      Map_Data* mapData = iterSec->second;
      mapData->clear();
      delete mapData;
   }

   m_mapSection.clear();
}

LPCTSTR CIniParse::SkipWhiteSpace( LPCTSTR p )
{
   while( *p ) {
      if( *p == _T( ' ' ) || *p == _T( '\t' ) )
         p++;
      else
         break;
   }

   return p;
}

BOOL CIniParse::GetValue( const std::string& strSection, const std::string& strKey, std::string& value )
{
   Map_Data* mapData = FindSection( strSection );
   if( !mapData )
      return FALSE;

   value = FindData( *mapData, strKey );
   if( value.empty() )
      return FALSE;

   return TRUE;
}

BOOL CIniParse::GetValue( const std::string& strSection, const std::string& strKey, int& value )
{
   std::string strValue;

   if( !GetValue( strSection, strKey, strValue ) )
      return FALSE;

   //value = std::stoi( strValue );
   value = atoi( strValue.c_str() );

   return TRUE;
}

std::string CIniParse::FindData( Map_Data& mapData, const std::string& strKey )
{
   std::string strRet    = _T("");
   Map_Data_iter iterMap = mapData.find( strKey );

   if( mapData.end() == iterMap ) {
      return strRet;
   }

   strRet = iterMap->second;

   return strRet;
}

CIniParse::Map_Data* CIniParse::FindSection( const std::string& strSection, BOOL bMake /*FALSE*/ )
{
   Map_Data* mapDataRet = NULL;

   Map_Section_iter iterSec = m_mapSection.find( strSection );

   if( m_mapSection.end() == iterSec ) {
      if( bMake ) {
         Map_Data* mapDataN       = new Map_Data;
         m_mapSection[strSection] = mapDataN;

         return mapDataN;
      } else
         return NULL;
   }

   mapDataRet = iterSec->second;

   return mapDataRet;
}

CIniParse::LINE_TYPE CIniParse::GetLineType( const std::string& strLine )
{
   LINE_TYPE emType = LINE_TYPE_SINGLE_LINE;

   if( 0 == strLine.length() )
      return LINE_TYPE_EMPTY;

   size_t iStPos = strLine.find( _T( '[' ) );
   size_t iEdPos = strLine.rfind( _T( ']' ) );
   if( std::string::npos != iStPos && iStPos < 3 && iStPos < iEdPos )
      return LINE_TYPE_SECTION;

   std::string strRemark = sutil::sleft( strLine, 1 );
   if( _T(";") == strRemark )
      return LINE_TYPE_REMARK;

   size_t pos = strLine.find( '=' );
   if( pos >= 3 )
      return LINE_TYPE_DATA;

   return emType;
}

BOOL CIniParse::ParseSection( const std::string& strLine, std::string& strSection )
{
   const TCHAR* p = strLine.c_str();

   size_t iStPos = strLine.find( _T( '[' ) );
   size_t iEdPos = strLine.rfind( _T( ']' ) );

   if( -1 == iStPos || -1 == iEdPos )
      return FALSE;

   size_t iLen = iEdPos - iStPos;
   if( iLen < 3 )
      return FALSE;

   std::string strRet;
   strRet = strLine.substr( iStPos + 1, iLen - 1 );

   if( strRet.empty() )
      return FALSE;

   strSection = sutil::trim( strRet );

   return TRUE;
}

/// 아래와 같은 형태도 파싱 가능하다.
///   1) HELLO=WORLD
///   2) "HELLO"="WORLD"
///   3) HELLO=WORLD ; REMARK
///   4) 'HELLO' = 'WORLD'
///   5) HELLO = 'ABC"DEF'
///   6) HELLO = "ABC"DEF" ( Value 파싱 X)
BOOL CIniParse::ParseData( const std::string& strLine, std::string& strKey, std::string& strValue )
{
   const TCHAR* p = strLine.c_str();
   TCHAR chQuot;

   strKey.clear();
   strValue.clear();

   // LEFT 파싱
   chQuot = ( *p == '"' || *p == '\'' ) ? *p : '\0';
   if( chQuot ) p++;
   while( *p ) {
      if( *p == chQuot ) {
         p++;
         break;
      } // "로 끝났다.
      if( !chQuot && *p == '=' ) break; // =를 찾았다.
      strKey += *p;
      p++;
   }
   if( *p == 0 ) {
      return FALSE;
   } // 파싱 실패

   if( !chQuot ) // 따옴표가 없으면
   {
      strKey = sutil::trim( strKey );
   }

   if( strKey.empty() ) {
      return FALSE;
   } // 왼쪽이 공백이다! 에러

   // "aa" = "b" 에서 = 이전의 공백 건너 띄기
   p = SkipWhiteSpace( p );
   if( *p != '=' ) {
      return FALSE;
   } // = 가 없다?
   p++; // = 건너 띄기

   // = 다음의 공백 건너 띄기
   p = SkipWhiteSpace( p );

   // RIGHT 파싱
   chQuot = ( *p == '"' || *p == '\'' ) ? *p : '\0';
   if( chQuot ) p++;

   TCHAR chPrev = 0;
   while( *p ) {
      if( *p == chQuot ) break; // "로 끝났다.
      if( !chQuot && *p == ';' ) break; // ;를 찾았다.

      if( *p == '\\' && *( p + 1 ) == 'n' ) {
         strValue += '\n';
         p += 2;
         continue;
      } else if( *p == '\\' && *( p + 1 ) == 't' ) {
         strValue += '\t';
         p += 2;
         continue;
      } else if( *p == '\\' && *( p + 1 ) == 'r' ) {
         strValue += '\r';
         p += 2;
         continue;
      }

      strValue += *p;
      p++;
   }

   if( !chQuot ) // 따옴표가 없으면
   {
      strValue = sutil::trim( strValue );
   }

   return TRUE;
}

#pragma once
#ifdef WIN32
#include <afxcmn.h>
const int WM_BUTTON_UP   = WM_USER + 0x102;
const int WM_BUTTON_DOWN = WM_USER + 0x103;

#define WM_CXSHADE_RADIO WM_USER+0x100

/////////////////////////////////////////////////////////////////////////////
class CxSkinButton : public CButton
{
public:
   CxSkinButton();

public:
   public:
   virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
protected:
   virtual void PreSubclassWindow();
public:
   void     SetToolTipText(CString s);   // 주의(jun): 본 함수는 사용하지 말것!! 버튼이 많아지면 시스템 정체현상이 나타남.
   COLORREF SetTextColor(COLORREF new_color);
   void     SetSkin(UINT normal,UINT down, UINT over=0, UINT disabled=0, UINT focus=0,UINT mask=0,short drawmode=1,short border=1,short margin=4);
   void     SetDrawText(BOOL bDraw) {  m_bDrawText = bDraw; }
   virtual ~CxSkinButton();
protected:
   bool         m_Checked;             //radio & check buttons
   DWORD        m_Style;               //radio & check buttons
   bool         m_tracking;
   bool         m_button_down;
   CToolTipCtrl m_tooltip;
   CBitmap      m_bNormal,m_bDown,m_bDisabled,m_bMask,m_bOver,m_bFocus; //skin bitmaps
   short        m_FocusRectMargin;   //dotted margin offset
   COLORREF     m_TextColor;         //button text color
   HRGN         hClipRgn;            //clipping region
   BOOL         m_Border;            //0=flat; 1=3D;
   short        m_DrawMode;          //0=normal; 1=stretch; 2=tiled;
   BOOL         m_bDrawText;

protected:
   void  RelayEvent(UINT message, WPARAM wParam, LPARAM lParam);
   HRGN  CreateRgnFromBitmap(HBITMAP hBmp, COLORREF color);
   void  FillWithBitmap(CDC* dc, HBITMAP hbmp, RECT r);
   void  DrawBitmap(CDC* dc, HBITMAP hbmp, RECT r, int DrawMode);
   int   GetBitmapWidth (HBITMAP hBitmap);
   int   GetBitmapHeight (HBITMAP hBitmap);

protected:
   afx_msg BOOL    OnEraseBkgnd(CDC* pDC);
   afx_msg void    OnLButtonDown(UINT nFlags, CPoint point);
   afx_msg void    OnLButtonUp(UINT nFlags, CPoint point);
   afx_msg void    OnMouseMove(UINT nFlags, CPoint point);
   afx_msg void    OnLButtonDblClk(UINT nFlags, CPoint point);
   afx_msg void    OnKillFocus(CWnd* pNewWnd);
   afx_msg BOOL    OnClicked();
   afx_msg void    OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
   afx_msg LRESULT OnMouseLeave(WPARAM, LPARAM);
   afx_msg LRESULT OnRadioInfo(WPARAM, LPARAM);
   afx_msg LRESULT OnBMSetCheck(WPARAM, LPARAM);
   afx_msg LRESULT OnBMGetCheck(WPARAM, LPARAM);
   DECLARE_MESSAGE_MAP()
};

#endif

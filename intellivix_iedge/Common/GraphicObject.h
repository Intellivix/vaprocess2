#pragma once
 
#ifdef WIN32
#include "Common.h"
#include "bccl_polygon.h"
#include "DataStruct.h"

#define GS_SELECTED        0x00000001
#define GS_TEMPORARY       0x00000002

#define SHAPE_POLYGON      1
#define SHAPE_RECTANGLE    2
#define SHAPE_ELLIPSE      3

 namespace SetupWizard 

{

enum SIZING_TYPE
{
   ST_NORTH    = 0,
   ST_WEST        ,
   ST_SOUTH       ,
   ST_EAST        ,
   ST_NORTH_WEST  ,
   ST_NORTH_EAST  ,
   ST_SOUTH_WEST  ,
   ST_SOUTH_EAST  ,
   ST_NUM,
};

// enum AREA_TYPE 은   AreaDisplayWnd.h 파일의 enum ADW_OPTION 의 각 항목과 일대일 대응관계에 있어야한다. 

enum AREA_TYPE 
{
   AREA_TYPE_TRACKING_AREA                    = 0,
   AREA_TYPE_NONDETECTION_AREA                   ,
   AREA_TYPE_DYNAMIC_BACKGROUND_AREA             ,
   AREA_TYPE_NONHANDOFF_AREA                     ,
   AREA_TYPE_EVENT_ZONE                          ,
   AREA_TYPE_PRIVACY_AREA                        ,
   AREA_TYPE_MUTUAL_ASSISTANCE_AREA              ,
   AREA_TYPE_MUTUAL_IR_LIGHT_AREA                ,
   AREA_TYPE_HEAD_ZONE                           ,
   AREA_TYPE_AR_AREA                             ,
};

enum ADW_OPTION
{
   ADW_OPTION_TRACKING_AREAS                    = 0x00000001,
   ADW_OPTION_NONDETECTION_AREAS                = 0x00000002,
   ADW_OPTION_DYNAMIC_BACKGROUND_AREAS          = 0x00000004,
   ADW_OPTION_NONHANDOFF_AREAS                  = 0x00000008,
   ADW_OPTION_EVENT_ZONES                       = 0x00000010,
   ADW_OPTION_PRESET_ZONES                      = 0x00000020,
   ADW_OPTION_PRIVACY_AREAS                     = 0x00000040,
   ADW_OPTION_MUTUAL_ASSISTANCE_AREA            = 0x00000080,
   ADW_OPTION_MUTUAL_IR_LIGHT_AREA              = 0x00000100,
   ADW_OPTION_HEAD_ZONES                        = 0x00000200,
   ADW_OPTION_AR_AREA                           = 0x00000400,
};


class CSizingEdge
{
public:
   CSizingEdge(void);
   ~CSizingEdge(void);
   CRect m_rect;
   int   m_nType;

public:
   void SetRect(int pos, CRect& s_rect);
   void Draw(CDC* pDC);
   BOOL CheckPointed(CPoint& point);
};

class COutlineRect
{
public:
   COutlineRect(void);
   ~COutlineRect(void);
   CRect m_rect;
   CSizingEdge m_Edges[8];

public:
   void SetRect(CRect& rect);
   void Draw(CDC* pDC);
   BOOL CheckPointed(CPoint& point);
   CSizingEdge* FindEdge(CPoint& point);
};

class CGraphicObject
{
public:
   CGraphicObject();
   virtual ~CGraphicObject(void);

   int            m_nState;
   int            m_nShape;
   int            m_nAreaType;
   COutlineRect   m_OutlineRect;
   COLORREF       m_clrColor;
   int            m_nBrushStyle;
   int            m_nHatchBrushStyle;
   BCCL::Polygon* m_pVACPolygon;
   
   // AR
   int            m_nSpecWndWidth;
   int            m_nSpecWndHeight;
   int            m_nFeatureType;
   CString        m_strAreaName;

public:
   void SetShape        (int shape)                {m_nShape = shape;}
   void SetColor        (COLORREF color)           {m_clrColor = color;}   
   void SetBrushStyle   (int style)                {m_nHatchBrushStyle = style;}
   void SetAreaType     (int area_type);
   void SetBoundRect    (CRect& rect);
   void SetIVSPolygon   (BCCL::Polygon* polygon)   {m_pVACPolygon = polygon;}
   void SetAreaName     (CString strAreaName)      {m_strAreaName = strAreaName;};
   void SetFeatureType  (int nFeatureType)         {m_nFeatureType = nFeatureType;};
   void SetSize         (int nWidth, int nHeight)  {m_nSpecWndWidth = nWidth; m_nSpecWndHeight = nHeight; }
   void Select          (   )                      {m_nState |= GS_SELECTED;}
   void Draw            (CDC* pDC, int nDrawOptions = 0);

public:
   virtual void DrawObject    (CDC* pDC) = 0;
   virtual BOOL CheckPointed  (CPoint& point) = 0;
   virtual void Sizing        (CPoint& point, int nResizingType);
   virtual void Moving        (CGraphicObject* d_object, int dx, int dy);
};


#define GO_DRAW_OPTION_SIZING_EDGES          0x00000001

class CGraphicObjectList : public CListPtrs<CGraphicObject>
{
public:
   CGraphicObjectList(void);
   ~CGraphicObjectList();

public:
   int m_nDrawAreaTypes;
   int m_nEditableAreaTypes;
   int m_nDrawOptions;
   
public:
   void SetDrawAreaTypes(int types) { m_nDrawAreaTypes = types;}
   void SetEditableAreaTypes(int types) { m_nEditableAreaTypes = types;}
   void Draw(CDC* pDC);
   void ClearList();
   void SelectObject(CGraphicObject* pGObj);
   void SelectPointedObject(CPoint& point);
   void UnSelectAllObjects();
   BOOL DeleteGraphicObject(CGraphicObject* pGObj);
   BOOL DeleteGraphicObjects_AreaType(int nAreaType);
   BOOL DeleteGraphicObjectsAll();
   CGraphicObject* GetPointedObject(CPoint& point);
   CGraphicObject* GetSelectedObject();
   CGraphicObject* GetGraphicObject_VACPolygon (BCCL::Polygon *polygon);

   CGraphicObject* GetObject();
   void Draw_Head(CDC* pDC);
   CGraphicObject* GetSelectedObject_Head();
   void SelectPointedObject_Head(CPoint& point);
   CGraphicObject* GetPointedObject_Head(CPoint& point);

};

class CFPoint
{
public:
   float fx;
   float fy;
public:
   CFPoint();
public:
   void operator=(const CFPoint& f);
};
typedef CArray<CFPoint> CFPointArray;

class CPointArray : public CArray <CPoint>
{
public:
   CPointArray();
   ~CPointArray();
   
public:
   void DrawConnectedLine(CDC* pDC);
};

class CPolygon : public CGraphicObject
{
#ifdef __ENABLE_MOVING_POLYGON_VERTEX
private:

   static const int VertexSqaureSize = 8;
   static const COLORREF VertexSquareBorderColor = RGB(255,255,255);
   static const COLORREF VertexSquareBkgndColor = RGB(0,0,0);
#endif // __ENABLE_MOVING_POLYGON_VERTEX

public:
   CPolygon(void);
   virtual ~CPolygon(void);

   CPointArray  m_Points;
   CFPointArray m_ScalePoints;
   CPoint       m_FixedAxis;

public:
   void  SetPoints(CPointArray& points);
   void  PrepareSizing(int nSizingType);

#ifdef __ENABLE_MOVING_POLYGON_VERTEX

   int SetCursor(LPCTSTR lpcszName);
   int MakeSquareRect(CRect* pRect, CPoint* pPoint, int nSize);

   int GetVertexIdxByPoint(CPoint* pPoint, int nSquareSize = VertexSqaureSize);
   int UpdateVertexByIdx(int nIdx, CPoint* pPoint);
   int DrawVertex(CDC* pDC, CPoint* pPoint, int nSquareSize = VertexSqaureSize);
   int DrawVertices(CDC* pDC, int nSquareSize = VertexSqaureSize);

#endif // __ENABLE_MOVING_POLYGON_VERTEX
   
public:
   virtual BOOL CheckPointed(CPoint& point);
   virtual void DrawObject(CDC* pDC);
   virtual void Sizing(CPoint& point, int nSizingType);
   virtual void Moving(CGraphicObject* s_object, int dx, int dy);
};

class CRectangle : public CGraphicObject
{
public:
   CRectangle(void);
   virtual ~CRectangle(void);

public:
   virtual void DrawObject(CDC* pDC);
   virtual BOOL CheckPointed(CPoint& point);
};

class CCircle : public CGraphicObject
{
public:
   CCircle(void);
   virtual ~CCircle(void);

   CPointArray  m_Points;
   CFPointArray m_ScalePoints;
   CPoint       m_FixedAxis;
   int          m_nHRRadius;

public:
   void  SetPoints(CPointArray& points);
   void  PrepareSizing(int nSizingType);

   CRect Calcu_Circle_Position(int& radius, POINT& CenterPos, POINT StartPos, POINT EndPos);

public:
   virtual BOOL CheckPointed(CPoint& point);
   virtual void DrawObject(CDC* pDC);
   virtual void Sizing(CPoint& point, int nSizingType);
   virtual void Moving(CGraphicObject* s_object, int dx, int dy);
};

}
using namespace SetupWizard;

#endif
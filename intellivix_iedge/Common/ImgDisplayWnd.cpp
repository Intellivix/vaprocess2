#include "StdAfx.h"
#ifdef __WIN32
#include "ImgDisplayWnd.h"

BEGIN_MESSAGE_MAP( CImgDisplayWnd, CWnd )
ON_WM_PAINT()
END_MESSAGE_MAP()

BOOL CImgDisplayWnd::Create( CRect& rect, CWnd* parent_wnd, BGRImage& s_image )
{
   if( !CWnd::Create( NULL, NULL, WS_CHILD | WS_VISIBLE, rect, parent_wnd, 0 ) )
      return FALSE;

   Width     = rect.Width();
   Height    = rect.Height();
   ParentWnd = parent_wnd;
   gfxGetBitmap( s_image, Bitmap, Width, Height );
   return TRUE;
}

void CImgDisplayWnd::DrawArrow( CDC* dc, IPoint2D& s_pos, float direction, float length, COLORREF color )
{
   IPoint2D s_tpos = STDMapToWnd( s_pos ); //SJM

   int i;
   FPoint2D ah1[3];
   IPoint2D ah2[3];

   direction *= MC_DEG2RAD;
   float r00 = cos( direction );
   float r10 = sin( direction );
   float r01 = -r10;
   float r11 = r00;
   ah1[0]( length, -6.0f );
   ah1[1]( length + 11.0f, 0.0f );
   ah1[2]( length, 6.0f );
   for( i = 0; i < 3; i++ ) {
      ah2[i].X = (int)( r00 * ah1[i].X + r01 * ah1[i].Y + s_tpos.X );
      ah2[i].Y = (int)( r10 * ah1[i].X + r11 * ah1[i].Y + s_tpos.Y );
   }
   CPen pen1( PS_SOLID, 1, color );
   CPen pen2( PS_SOLID, 5, color );
   CBrush brush( color );
   dc->SelectObject( &pen1 );
   dc->SelectObject( &brush );
   dc->Polygon( (LPPOINT)ah2, 3 );
   dc->SelectObject( &pen2 );
   dc->MoveTo( s_tpos.X, s_tpos.Y );
   int x2 = (int)( r00 * length + s_tpos.X );
   int y2 = (int)( r10 * length + s_tpos.Y );
   dc->LineTo( x2, y2 );
   dc->SelectStockObject( NULL_PEN );
   dc->SelectStockObject( NULL_BRUSH );
}

void CImgDisplayWnd::DrawEllipse( CDC* dc, int sx, int sy, int ex, int ey, COLORREF color, int thickness )
{
   CPoint s_p( sx, sy );
   CPoint e_p( ex, ey );
   s_p = STDMapToWnd( s_p );
   e_p = STDMapToWnd( e_p );

   CPen pen( PS_SOLID, thickness, color );
   dc->SelectObject( &pen );
   dc->SelectStockObject( NULL_BRUSH );
   dc->Ellipse( s_p.x, s_p.y, e_p.x, e_p.y );
   dc->SelectStockObject( NULL_PEN );
}

void CImgDisplayWnd::DrawEllipse( CDC* dc, IBox2D& s_box, COLORREF color, int thickness )
{
   int ex = s_box.X + s_box.Width - 1;
   int ey = s_box.Y + s_box.Height - 1;
   DrawEllipse( dc, s_box.X, s_box.Y, ex, ey, color, thickness );
}

void CImgDisplayWnd::DrawLine( CDC* dc, int sx, int sy, int ex, int ey, COLORREF color, int thickness )
{
   CPoint s_pos, e_pos;
   s_pos = STDMapToWnd( CPoint( sx, sy ) );
   e_pos = STDMapToWnd( CPoint( ex, ey ) );

   CPen pen( PS_SOLID, thickness, color );
   dc->SelectObject( &pen );
   dc->MoveTo( s_pos );
   dc->LineTo( e_pos );
   dc->SelectStockObject( NULL_PEN );
}

void CImgDisplayWnd::DrawLine( CDC* dc, IPoint2D& s_pos, IPoint2D& e_pos, COLORREF color, int thickness )
{
   DrawLine( dc, s_pos.X, s_pos.Y, e_pos.X, e_pos.Y, color, thickness );
}

void CImgDisplayWnd::DrawLine( CDC* dc, ILine2D& s_line, COLORREF color, int thickness )
{
   DrawLine( dc, s_line[0], s_line[1], color, thickness );
}

void CImgDisplayWnd::DrawWidth( CDC* dc, IBox2D& s_box, COLORREF color, int thickness )
{
   int x1   = s_box.X;
   int x2   = s_box.X + s_box.Width;
   int y    = s_box.Y;
   int size = 8;
   DrawLine( dc, x1, y, x2, y, color, thickness );
   DrawLine( dc, x1, y, x1 + size, y - size, color, thickness );
   DrawLine( dc, x1, y, x1 + size, y + size, color, thickness );
   DrawLine( dc, x2, y, x2 - size, y - size, color, thickness );
   DrawLine( dc, x2, y, x2 - size, y + size, color, thickness );

   DrawLine( dc, x1, y - size, x1, y + size + 1, color, thickness + 1 );
   DrawLine( dc, x2, y - size, x2, y + size + 1, color, thickness + 1 );
}

void CImgDisplayWnd::DrawHeight( CDC* dc, IBox2D& s_box, COLORREF color, int thickness )
{
   int y1   = s_box.Y;
   int y2   = s_box.Y + s_box.Height;
   int x    = s_box.X;
   int size = 8;
   DrawLine( dc, x, y1, x, y2, color, thickness );
   DrawLine( dc, x, y1, x - size, y1 + size, color, thickness );
   DrawLine( dc, x, y1, x + size, y1 + size, color, thickness );
   DrawLine( dc, x, y2, x - size, y2 - size, color, thickness );
   DrawLine( dc, x, y2, x + size, y2 - size, color, thickness );

   DrawLine( dc, x - size, y1, x + size + 1, y1, color, thickness + 1 );
   DrawLine( dc, x - size, y2, x + size + 1, y2, color, thickness + 1 );
}

void CImgDisplayWnd::DrawPie( CDC* dc, IPoint2D& c_pos, float direction, float range, float radius, COLORREF color )
{
   IPoint2D tpos = STDMapToWnd( c_pos );

   range *= MC_DEG2RAD;
   direction *= MC_DEG2RAD;
   float angle1 = direction + 0.5f * range;
   float angle2 = angle1 - range;
   float r00    = cos( angle1 );
   float r10    = sin( angle1 );
   CPoint p1;
   p1.x = (int)( r00 * radius + tpos.X );
   p1.y = (int)( r10 * radius + tpos.Y );
   r00  = cos( angle2 );
   r10  = sin( angle2 );
   CPoint p2;
   p2.x = (int)( r00 * radius + tpos.X );
   p2.y = (int)( r10 * radius + tpos.Y );
   CRect rect;
   rect.left   = (int)( tpos.X - radius );
   rect.top    = (int)( tpos.Y - radius );
   rect.right  = (int)( tpos.X + radius );
   rect.bottom = (int)( tpos.Y + radius );
   CPen pen( PS_SOLID, 1, color );
   dc->SelectObject( &pen );
   dc->SelectStockObject( NULL_BRUSH );
   dc->Pie( &rect, p1, p2 );
   dc->SelectStockObject( NULL_PEN );
}

void CImgDisplayWnd::DrawPoint( CDC* dc, IPoint2D& pos, COLORREF color )
{
   IPoint2D tpos = STDMapToWnd( pos );
   CPen pen( PS_SOLID, 1, color );
   dc->SelectObject( &pen );
   dc->MoveTo( tpos.X - 2, tpos.Y );
   dc->LineTo( tpos.X + 3, tpos.Y );
   dc->MoveTo( tpos.X, tpos.Y - 2 );
   dc->LineTo( tpos.X, tpos.Y + 3 );
   dc->SelectStockObject( NULL_PEN );
}

void CImgDisplayWnd::DrawPolygon( CDC* dc, BCCL::Polygon* polygon, COLORREF color, int brush_style, int flag_selected )
{
   int pen_width      = 1;
   COLORREF pen_color = color;
   if( flag_selected ) {
      pen_width = 3;
      pen_color = RGB( 255, 255, 255 );
   }
   CPen pen( PS_SOLID, pen_width, pen_color );
   dc->SelectObject( &pen );

   CBrush* pBrush = NULL;

   if( brush_style == -1 ) {
      dc->SelectStockObject( NULL_BRUSH );
   } else {
      pBrush = new CBrush( brush_style, color );
      dc->SelectObject( &pBrush );
   }

   dc->SelectObject( &pen );
   dc->SelectObject( pBrush );

   dc->SetBkMode( TRANSPARENT );

   IP2DArray1D Vertices;
   Vertices.Create( polygon->Vertices.Length );
   for( int i     = 0; i < Vertices.Length; i++ )
      Vertices[i] = STDMapToWnd( polygon->Vertices[i] );
   dc->Polygon( ( LPPOINT )(IPoint2D*)Vertices, Vertices.Length );
   dc->SelectStockObject( NULL_PEN );
   dc->SelectStockObject( NULL_BRUSH );
   if( pBrush ) delete pBrush;
}

void CImgDisplayWnd::DrawRectangle( CDC* dc, int sx, int sy, int ex, int ey, COLORREF color, int thickness, int brush_style )
{
   CPoint s_p( sx, sy );
   CPoint e_p( ex, ey );
   s_p = STDMapToWnd( s_p );
   e_p = STDMapToWnd( e_p );

   CBrush brush;
   CPen pen( PS_SOLID, thickness, color );
   if( brush_style > 0 ) {
      brush.CreateHatchBrush( brush_style, RGB( 255, 255, 255 ) );
      dc->SelectObject( &brush );
   } else
      dc->SelectStockObject( NULL_BRUSH );

   dc->SelectObject( &pen );
   dc->SetBkMode( TRANSPARENT );
   dc->Rectangle( s_p.x, s_p.y, e_p.x, e_p.y );
   dc->SelectStockObject( NULL_PEN );
   dc->SelectStockObject( NULL_BRUSH );
}

void CImgDisplayWnd::DrawRectangle( CDC* dc, IBox2D& s_box, COLORREF color, int thickness, int brush_style )
{
   int ex = s_box.X + s_box.Width - 1;
   int ey = s_box.Y + s_box.Height - 1;
   DrawRectangle( dc, s_box.X, s_box.Y, ex, ey, color, thickness, brush_style );
}

void CImgDisplayWnd::DrawText( CDC* dc, const char* text, int x, int y, COLORREF color )
{
   CPoint p( x, y );
   p = STDMapToWnd( p );

   CFont font;
   font.CreatePointFont( 90, "Tahoma" );
   dc->SelectObject( &font );
   dc->SetTextColor( color );
   dc->SetBkMode( TRANSPARENT );
   dc->TextOut( p.x, p.y, text, (int)strlen( text ) );
   dc->SelectStockObject( SYSTEM_FONT );
}

void CImgDisplayWnd::OnPaint()

{
   CPaintDC dc( this );
   Render( &dc );
}

void CImgDisplayWnd::Render()
{
   CClientDC dc( this );
   Render( &dc );
}

void CImgDisplayWnd::Render( CDC* dc )
{
   CBitmap d_bitmap;
   d_bitmap.CreateCompatibleBitmap( dc, Width, Height );
   CDC s_mem_dc, d_mem_dc;
   s_mem_dc.CreateCompatibleDC( dc );
   d_mem_dc.CreateCompatibleDC( dc );
   CBitmap* old_bitmap1 = s_mem_dc.SelectObject( &Bitmap );
   CBitmap* old_bitmap2 = d_mem_dc.SelectObject( &d_bitmap );
   d_mem_dc.BitBlt( 0, 0, Width, Height, &s_mem_dc, 0, 0, SRCCOPY );
   s_mem_dc.SelectObject( old_bitmap1 );
   RenderSub( &d_mem_dc );
   dc->BitBlt( 0, 0, Width, Height, &d_mem_dc, 0, 0, SRCCOPY );
   d_mem_dc.SelectObject( old_bitmap2 );
}

void CImgDisplayWnd::Render( BGRImage& s_image )
{
   gfxGetBitmap( s_image, Bitmap, Width, Height );
   CClientDC dc( this );
   Render( &dc );
}

IPoint2D CImgDisplayWnd::WndToSTDMap( IPoint2D& p )
{
   IPoint2D point;
   point.X = (int)( p.X * ( REF_IMAGE_WIDTH / (float)Width ) );
   point.Y = (int)( p.Y * ( REF_IMAGE_HEIGHT / (float)Height ) );

   if( point.X >= REF_IMAGE_WIDTH ) point.X  = REF_IMAGE_WIDTH - 1;
   if( point.Y >= REF_IMAGE_HEIGHT ) point.Y = REF_IMAGE_HEIGHT - 1;
   if( point.X < 0 ) point.X                 = 0;
   if( point.Y < 0 ) point.Y                 = 0;
   return point;
}

IPoint2D CImgDisplayWnd::STDMapToWnd( IPoint2D& p )
{
   IPoint2D point;
   point.X = (int)( p.X * ( Width / (float)REF_IMAGE_WIDTH ) );
   point.Y = (int)( p.Y * ( Height / (float)REF_IMAGE_HEIGHT ) );
   return point;
}

CPoint CImgDisplayWnd::WndToSTDMap( CPoint& p )
{
   IPoint2D point = WndToSTDMap( IPoint2D( p.x, p.y ) );
   return CPoint( point.X, point.Y );
}

CPoint CImgDisplayWnd::STDMapToWnd( CPoint& p )
{
   IPoint2D point = STDMapToWnd( IPoint2D( p.x, p.y ) );
   return CPoint( point.X, point.Y );
}
#endif

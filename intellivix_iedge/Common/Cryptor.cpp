// Cryptor.cpp: implementation of the CCryptor class.
//
//////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "Cryptor.h"

#ifdef __SUPPORT_WIN32_CRYPT

/////////////////////////////////////////////////////////////////////////////////////
//
// class CCryptor
//
/////////////////////////////////////////////////////////////////////////////////////

CCryptor::CCryptor( LPCSTR szKeyContainerName )
{
   pbKeyBlob = pbData = NULL;
   hKeyXchg = hKey = NULL;
   hProv           = NULL;
   strcpy( m_szKeyContainerName, szKeyContainerName );
}

CCryptor::~CCryptor()
{
   Release();
}

void CCryptor::Release()
{
   if( pbKeyBlob ) {
      delete[] pbKeyBlob;
      pbKeyBlob = NULL;
   }
   if( pbData ) {
      delete[] pbData;
      pbData = NULL;
   }
   if( hKeyXchg ) {
      CryptDestroyKey( hKeyXchg );
      hKeyXchg = NULL;
   }
   if( hKey ) {
      CryptDestroyKey( hKey );
      hKey = NULL;
   }
   if( hProv ) {
      CryptReleaseContext( hProv, 0 );
      hProv = NULL;
   }
}

int CCryptor::Encrypt( LPCSTR srcText, int& nLength, ALG_ID algid /*=CALG_RC2*/ )
{
   Release();

   DWORD dwKeyBlobLen = 0;
   DWORD dwDataLen    = 0;

   if( !CryptAcquireContext( &hProv, m_szKeyContainerName, NULL, PROV_RSA_FULL, 0 ) ) {
      if( GetLastError() == NTE_BAD_KEYSET ) {
         if( !CryptAcquireContext( &hProv, m_szKeyContainerName, NULL, PROV_RSA_FULL, CRYPT_NEWKEYSET ) ) {
            Release();
            return ( 1 );
         }
      } else {
         Release();
         return ( 2 );
      }
   }

   if( !CryptGenKey( hProv, algid, CRYPT_EXPORTABLE, &hKey ) ) {
      Release();
      return ( 3 );
   }

   if( !CryptGetUserKey( hProv, AT_KEYEXCHANGE, &hKeyXchg ) ) {
      if( NTE_NO_KEY != GetLastError() ) {
         Release();
         return ( 4 );
      }

      if( !CryptGenKey( hProv, AT_KEYEXCHANGE, 0, &hKeyXchg ) ) {
         Release();
         return ( 5 );
      }
   }

   if( !CryptExportKey( hKey, hKeyXchg, SIMPLEBLOB, 0, NULL, &dwKeyBlobLen ) ) {
      Release();
      return ( 6 );
   }

   pbKeyBlob = new BYTE[dwKeyBlobLen];
   if( !CryptExportKey( hKey, hKeyXchg, SIMPLEBLOB, 0, pbKeyBlob, &dwKeyBlobLen ) ) {
      Release();
      return ( 7 );
   }

   dwDataLen = strlen( srcText );
   if( !CryptEncrypt( hKey, 0, TRUE, 0, NULL, &dwDataLen, BUF_SIZE ) ) {
      Release();
      return ( 8 );
   }

   pbData = new BYTE[dwDataLen];
   memcpy( pbData, srcText, dwDataLen );

   dwDataLen = strlen( srcText );
   if( !CryptEncrypt( hKey, 0, TRUE, 0, pbData, &dwDataLen, BUF_SIZE ) ) {
      Release();
      return ( 9 );
   }

   nLength = sizeof( dwKeyBlobLen ) + dwKeyBlobLen + sizeof( dwDataLen ) + dwDataLen;

   ZeroMemory( buffer, sizeof( buffer ) );
   BYTE* pdst = buffer;
   memcpy( pdst, &dwKeyBlobLen, sizeof( dwKeyBlobLen ) );
   pdst += sizeof( dwKeyBlobLen );
   memcpy( pdst, pbKeyBlob, dwKeyBlobLen );
   pdst += dwKeyBlobLen;
   memcpy( pdst, &dwDataLen, sizeof( dwDataLen ) );
   pdst += sizeof( dwDataLen );
   memcpy( pdst, pbData, dwDataLen );
   pdst += dwDataLen;

   Release();
   return ( 0 );
}

int CCryptor::Decrypt( BYTE* srcBuffer )
{
   Release();

   DWORD dwKeyBlobLen = 0;
   DWORD dwDataLen    = 0;

   if( !CryptAcquireContext( &hProv, m_szKeyContainerName, NULL, PROV_RSA_FULL, 0 ) ) {
      if( GetLastError() == NTE_BAD_KEYSET ) {
         if( !CryptAcquireContext( &hProv, m_szKeyContainerName, NULL, PROV_RSA_FULL, CRYPT_NEWKEYSET ) ) {
            Release();
            return ( 1 );
         }
      } else {
         Release();
         return ( 2 );
      }
   }
   memcpy( &dwKeyBlobLen, srcBuffer, sizeof( dwKeyBlobLen ) );
   srcBuffer += sizeof( dwKeyBlobLen );

   pbKeyBlob = new BYTE[dwKeyBlobLen];
   memcpy( pbKeyBlob, srcBuffer, dwKeyBlobLen );
   srcBuffer += dwKeyBlobLen;
   if( !CryptImportKey( hProv, pbKeyBlob, dwKeyBlobLen, 0, 0, &hKey ) ) {
      Release();
      return ( 3 );
   }

   memcpy( &dwDataLen, srcBuffer, sizeof( dwDataLen ) );
   srcBuffer += sizeof( dwDataLen );

   pbData = new BYTE[dwDataLen];
   ZeroMemory( pbData, dwDataLen );
   memcpy( pbData, srcBuffer, dwDataLen );
   if( !CryptDecrypt( hKey, 0, TRUE, 0, pbData, &dwDataLen ) ) {
      Release();
      return ( 4 );
   }
   ZeroMemory( buffer, sizeof( buffer ) );
   memcpy( buffer, pbData, dwDataLen );

   Release();
   return ( 0 );
}

/////////////////////////////////////////////////////////////////////////////////////
//
// class CCryption
//
/////////////////////////////////////////////////////////////////////////////////////

CCryption::CCryption( void )
{
   ZeroMemory( m_szEncrypted, sizeof( m_szEncrypted ) );
   ZeroMemory( m_szDecrypted, sizeof( m_szDecrypted ) );
}

CCryption::~CCryption( void )
{
}

BOOL CCryption::Encrypt( LPCSTR szSrcText, int& nEncryptedLength )
{
   int nSrcLength = (int)strlen( szSrcText );
   if( !nSrcLength || nSrcLength > MAX_CRYPT_LETTER )
      return FALSE;

   int i;
   char letter;
   char szEncrypt1[MAX_CRYPT_LETTER];
   char szEncrypt2[MAX_CRYPT_LETTER];
   ZeroMemory( szEncrypt1, sizeof( szEncrypt1 ) );
   ZeroMemory( szEncrypt2, sizeof( szEncrypt2 ) );

   for( i = 0; i < nSrcLength; i++ ) {
      letter        = ( szSrcText[i] + ( i + 1 ) );
      szEncrypt1[i] = letter;
   }
   for( i                            = 0; i < nSrcLength; i++ )
      szEncrypt2[nSrcLength - 1 - i] = szEncrypt1[i];

   strcpy( m_szEncrypted, szEncrypt2 );
   nEncryptedLength = (int)strlen( m_szEncrypted );
   return TRUE;
}

BOOL CCryption::Decrypt( LPCSTR szSrcText, int nSrcLength )
{
   if( !nSrcLength || nSrcLength > MAX_CRYPT_LETTER )
      return FALSE;

   int i;
   char letter;
   char szDecrypt1[MAX_CRYPT_LETTER];
   char szDecrypt2[MAX_CRYPT_LETTER];
   ZeroMemory( szDecrypt1, sizeof( szDecrypt1 ) );
   ZeroMemory( szDecrypt2, sizeof( szDecrypt2 ) );

   for( i                            = 0; i < nSrcLength; i++ )
      szDecrypt1[nSrcLength - 1 - i] = szSrcText[i];

   for( i = 0; i < nSrcLength; i++ ) {
      letter        = ( szDecrypt1[i] - ( i + 1 ) );
      szDecrypt2[i] = letter;
   }
   strcpy( m_szDecrypted, szDecrypt2 );
   return TRUE;
}
#endif

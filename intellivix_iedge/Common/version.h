﻿#pragma once

#define PRODUCT_VERSION "10"
#define INTELLIVIX_VERSION "1.0"
#define INTELLIVIX_BUILD_NO "100"
#define INTELLIVIX_BUILD_NO_ 100
#define INTELLIVIX_BUILD_DATE "425"
#define INTELLIVIX_BUILD_DATE_ 425

/*********************************************************************************/
/*                                                                               */
/*                                   Product                                     */
/*                                                                               */
/*********************************************************************************/

///////////////////////////////////////////////////////////////////////////////////
// Linux Appliance
///////////////////////////////////////////////////////////////////////////////////
#define __SUPPORT_VIDEO_ANALYTICS
#define __SUPPORT_HEAD_COUNTING
//#define __SUPPORT_VIDEO_FILE_CAMERA
//#define __SUPPORT_PTZ_CAMERA
//#define __SUPPORT_PTZ_AREA
#define __SUPPORT_FISHEYE_DEWARPING
//#define __SUPPORT_PANORAMIC_CAMERA
//#define __SUPPORT_IMAGE_ENHANCEMENT
//#define __SUPPORT_MASTER_SLAVE_BASED_AUTONOMOUS_PTZ_TRACKING
//#define __SUPPORT_WIN32_CRYPT
//#define __SUPPORT_SOLAR_CALCULATOR
//#define __SUPPORT_AUGMENTED_REALITY
//#define __SUPPORT_DAYNIGHT_DETECTION
//#define __SUPPORT_VIRTUAL_PTZ_CAMERA
#define __SUPPORT_AVCODEC_IMAGE_UTIL


///////////////////////////////////////////////////////////////////////////////////
/// License
///////////////////////////////////////////////////////////////////////////////////
//#define __SUPPORT_LICENSE_CERTIFY
#ifdef __linux
//#define __SUPPORT_LICENSE_SDCARD ( "/sys/class/mmc_host/mmc1/mmc1:aaaa/serial" ) // cellinx.
//#define __SUPPORT_LICENSE_SDCARD ( "/sys/class/mmc_host/mmc0/mmc0:0001/serial" ) // yhpark. I don't know which company.
#endif // __linux

//#define __SUPPORT_GRAY_IMAGE_USE
//#define __SUPPORT_MFC_CODEC
//#define __SUPPORT_ONVIF

#ifdef __linux
//#define SUPPORT_PTHREAD_NAME // use thread's name.
#endif // __linux


/*********************************************************************************/
/*                                                                               */
/*                                     OEM                                       */
/*                                                                               */
/*********************************************************************************/

#define OEM_NONE // IntelliVIX

#if defined( OEM_NONE )
#define __PRODUCT_NAME "IntelliVIX"
#define __COPYRIGHT_STRING "Copyright (C) 2015 illisis Inc. All rights reserved."
#endif

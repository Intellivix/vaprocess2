﻿#ifndef FILEMONITORING_H
#define FILEMONITORING_H

#ifdef __linux

#include <cstdio>
#include <cstdlib>
#include <cerrno>
#include <thread>

#include <sys/inotify.h>

#include "bccl_plog.h"
#include "InnerThread.h"

///
/// \brief The FileMonitoring class
///
class FileMonitoring : public InnerThread {
public:
   FileMonitoring() = default;
   virtual ~FileMonitoring();

   void SetDir( const std::string& dir );

   bool StartThread( void* pInstance = nullptr ) override;
   void StopThread() override;

   bool Init( const std::string& dir = "" );
   bool ReadAndProcessEvent();
   void Close();

protected:
   static const uint EVENT_SIZE = sizeof( struct inotify_event );
   static const uint BUF_LEN    = 128 * ( EVENT_SIZE + 16 );

   virtual bool Process( struct inotify_event* event );

   bool m_thread_running = false;
   char m_buffer[BUF_LEN]{};
   int m_file_descriptor   = -1;
   int m_watch_descriptor  = -1;
   ssize_t buffer_position = 0;
   ssize_t buffer_length   = 0;
   std::string m_dir;

private:
   void ThreadLoop() override;
};

#endif // __linux
#endif // FILEMONITORING_H

#include "StdAfx.h"
#include "version.h"
#include "ImageEnhancement.h"

#ifdef __SUPPORT_IMAGE_ENHANCEMENT

#include "vacl_vidstab.h"
#include "vacl_defog.h"
#include "vacl_videnhnc.h"
#include "SupportFunction.h"

using namespace VACL;

///////////////////////////////////////////////////////////////////////////////
//
// Class: CVideoStabilizationSetup
//
///////////////////////////////////////////////////////////////////////////////

CVideoStabilizationSetup::CVideoStabilizationSetup()
{
   m_bUseVideoStabilization      = FALSE;
   m_nVideoStabilizationFrameNum = 5;

   m_bPTZCameraMoving = FALSE;

   BackupToPrev();
}

CVideoStabilizationSetup& CVideoStabilizationSetup::operator=( CVideoStabilizationSetup& from )
{
   m_bUseVideoStabilization      = from.m_bUseVideoStabilization;
   m_nVideoStabilizationFrameNum = from.m_nVideoStabilizationFrameNum;
   m_bPTZCameraMoving            = from.m_bPTZCameraMoving;
   return ( *this );
}

BOOL CVideoStabilizationSetup::operator==( CVideoStabilizationSetup& from )
{
   if( m_bUseVideoStabilization != from.m_bUseVideoStabilization ) return FALSE;
   if( m_nVideoStabilizationFrameNum != from.m_nVideoStabilizationFrameNum ) return FALSE;
   if( m_bPTZCameraMoving != from.m_bPTZCameraMoving ) return FALSE;
   return true;
}

BOOL CVideoStabilizationSetup::operator!=( CVideoStabilizationSetup& from )
{
   return !( *this == from );
}

int CVideoStabilizationSetup::ReadFile( CXMLIO* pIO )
{
   CVideoStabilizationSetup dv;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "VSS" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "UseVS", &m_bUseVideoStabilization, &dv.m_bUseVideoStabilization );
      xmlNode.Attribute( TYPE_ID( int ), "VSFN", &m_nVideoStabilizationFrameNum, &dv.m_nVideoStabilizationFrameNum );
   }

   if( m_nVideoStabilizationFrameNum > 10 )
      m_nVideoStabilizationFrameNum = 10;

   return ( DONE );
}

int CVideoStabilizationSetup::WriteFile( CXMLIO* pIO )
{
   if( m_nVideoStabilizationFrameNum > 10 )
      m_nVideoStabilizationFrameNum = 10;

   CVideoStabilizationSetup dv;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "VSS" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "UseVS", &m_bUseVideoStabilization, &dv.m_bUseVideoStabilization );
      xmlNode.Attribute( TYPE_ID( int ), "VSFN", &m_nVideoStabilizationFrameNum, &dv.m_nVideoStabilizationFrameNum );
   }
   return ( DONE );
}

BOOL CVideoStabilizationSetup::IsJustStarted()
{
   if( !m_bUseVideoStabilizationPrev && m_bUseVideoStabilization )
      return TRUE;
   return FALSE;
}

BOOL CVideoStabilizationSetup::IsJustEnded()
{
   if( m_bUseVideoStabilizationPrev && !m_bUseVideoStabilization )
      return TRUE;
   return FALSE;
}

BOOL CVideoStabilizationSetup::IsChanged()
{
   if( m_bUseVideoStabilization != m_bUseVideoStabilizationPrev ) return TRUE;
   if( m_nVideoStabilizationFrameNum != m_nVideoStabilizationFrameNumPrev ) return TRUE;
   return FALSE;
}

BOOL CVideoStabilizationSetup::IsOn()
{
   return m_bUseVideoStabilization;
}

void CVideoStabilizationSetup::BackupToPrev()
{
   m_bUseVideoStabilizationPrev      = m_bUseVideoStabilization;
   m_nVideoStabilizationFrameNumPrev = m_nVideoStabilizationFrameNum;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CVideoDefoggingSetup
//
///////////////////////////////////////////////////////////////////////////////

CVideoDefoggingSetup::CVideoDefoggingSetup()
{
   m_bUseDefog   = FALSE;
   m_nDefogLevel = 5;
   m_ResizeFactor( 0.75f, 0.5f );

   BackupToPrev();
}

CVideoDefoggingSetup& CVideoDefoggingSetup::operator=( CVideoDefoggingSetup& from )
{
   m_bUseDefog   = from.m_bUseDefog;
   m_nDefogLevel = from.m_nDefogLevel;
   return ( *this );
}

BOOL CVideoDefoggingSetup::operator==( CVideoDefoggingSetup& from )
{
   if( m_bUseDefog != from.m_bUseDefog ) return FALSE;
   if( m_nDefogLevel != from.m_nDefogLevel ) return FALSE;
   return true;
}

BOOL CVideoDefoggingSetup::operator!=( CVideoDefoggingSetup& from )
{
   return !( *this == from );
}

int CVideoDefoggingSetup::ReadFile( CXMLIO* pIO )
{
   static CVideoDefoggingSetup dv;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "VDS" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "UseDF", &m_bUseDefog, &dv.m_bUseDefog );
      xmlNode.Attribute( TYPE_ID( int ), "DFLevel", &m_nDefogLevel, &dv.m_nDefogLevel );
   }
   return ( DONE );
}

int CVideoDefoggingSetup::WriteFile( CXMLIO* pIO )
{
   static CVideoDefoggingSetup dv;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "VDS" ) ) {
      xmlNode.Attribute( TYPE_ID( int ), "UseDF", &m_bUseDefog, &dv.m_bUseDefog );
      xmlNode.Attribute( TYPE_ID( int ), "DFLevel", &m_nDefogLevel, &dv.m_nDefogLevel );
   }
   return ( DONE );
}

BOOL CVideoDefoggingSetup::IsJustStarted()
{
   if( !m_bUseDefogPrev && m_bUseDefog )
      return TRUE;
   return FALSE;
}

BOOL CVideoDefoggingSetup::IsJustEnded()
{
   if( m_bUseDefogPrev && !m_bUseDefog )
      return TRUE;
   return FALSE;
}

BOOL CVideoDefoggingSetup::IsChanged()
{
   if( m_bUseDefog != m_bUseDefogPrev ) return TRUE;
   if( m_nDefogLevel != m_nDefogModePrev ) return TRUE;
   return FALSE;
}

BOOL CVideoDefoggingSetup::IsOn()
{
   return m_bUseDefog;
}

void CVideoDefoggingSetup::BackupToPrev()
{
   m_bUseDefogPrev  = m_bUseDefog;
   m_nDefogModePrev = m_nDefogLevel;
}

#ifdef __SUPPORT_FISHEYE_CAMERA

///////////////////////////////////////////////////////////////////////////////
//
// Class: CFisheyeDewarpingSetup
//
///////////////////////////////////////////////////////////////////////////////

CFisheyeDewarpingSetup::CFisheyeDewarpingSetup()
{
   m_bDefaultFisheyeInfo   = TRUE;
   m_bUseDewarp            = FALSE;
   m_FisheyeInfo.pos       = CEILING;
   m_FisheyeInfo.view_type = DEFALUT_360;
   m_FisheyeInfo.f_length  = 0.0f;
   m_FisheyeInfo.radius    = 0.0f;
   m_FisheyeInfo.lp_range  = 35.0;
   m_FisheyeInfo.up_range  = 35.0;
   m_FisheyeInfo.center.X  = 0.0;
   m_FisheyeInfo.center.Y  = 0.0;
}

CFisheyeDewarpingSetup& CFisheyeDewarpingSetup::operator=( CFisheyeDewarpingSetup& from )
{
   m_bDefaultFisheyeInfo   = from.m_bDefaultFisheyeInfo;
   m_bUseDewarp            = from.m_bUseDewarp;
   m_FisheyeInfo.pos       = from.m_FisheyeInfo.pos;
   m_FisheyeInfo.view_type = from.m_FisheyeInfo.view_type;
   m_FisheyeInfo.f_length  = from.m_FisheyeInfo.f_length;
   m_FisheyeInfo.radius    = from.m_FisheyeInfo.radius;
   m_FisheyeInfo.lp_range  = from.m_FisheyeInfo.lp_range;
   m_FisheyeInfo.up_range  = from.m_FisheyeInfo.up_range;
   m_FisheyeInfo.center    = from.m_FisheyeInfo.center;

   return ( *this );
}

BOOL CFisheyeDewarpingSetup::operator==( CFisheyeDewarpingSetup& from )
{
   if( m_bDefaultFisheyeInfo != from.m_bDefaultFisheyeInfo ) return FALSE;
   if( m_bUseDewarp != from.m_bUseDewarp ) return FALSE;
   if( m_FisheyeInfo.pos != from.m_FisheyeInfo.pos ) return FALSE;
   if( m_FisheyeInfo.view_type != from.m_FisheyeInfo.view_type ) return FALSE;
   if( m_FisheyeInfo.f_length != from.m_FisheyeInfo.f_length ) return FALSE;
   if( m_FisheyeInfo.radius != from.m_FisheyeInfo.radius ) return FALSE;
   if( m_FisheyeInfo.lp_range != from.m_FisheyeInfo.lp_range ) return FALSE;
   if( m_FisheyeInfo.up_range != from.m_FisheyeInfo.up_range ) return FALSE;
   if( m_FisheyeInfo.center != from.m_FisheyeInfo.center ) return FALSE;
   return TRUE;
}

BOOL CFisheyeDewarpingSetup::operator!=( CFisheyeDewarpingSetup& from )
{
   return !( *this == from );
}

int CFisheyeDewarpingSetup::ReadFile( CXMLIO* pIO )
{
   CFisheyeDewarpingSetup dv;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Fisheye" ) ) {
      xmlNode.Attribute( TYPE_ID( BOOL ), "DefaultFisheyeInfo", &m_bDefaultFisheyeInfo, &dv.m_bDefaultFisheyeInfo );
      xmlNode.Attribute( TYPE_ID( BOOL ), "UseDewarp", &m_bUseDewarp, &dv.m_bUseDewarp );
      xmlNode.Attribute( TYPE_ID( int ), "Position", &m_FisheyeInfo.pos, &dv.m_FisheyeInfo.pos );
      xmlNode.Attribute( TYPE_ID( int ), "ViewType", &m_FisheyeInfo.view_type, &dv.m_FisheyeInfo.view_type );
      xmlNode.Attribute( TYPE_ID( float ), "FocalLength", &m_FisheyeInfo.f_length, &dv.m_FisheyeInfo.f_length );
      xmlNode.Attribute( TYPE_ID( float ), "LP_Range", &m_FisheyeInfo.lp_range, &dv.m_FisheyeInfo.lp_range );
      xmlNode.Attribute( TYPE_ID( float ), "UP_Range", &m_FisheyeInfo.up_range, &dv.m_FisheyeInfo.up_range );
      xmlNode.Attribute( TYPE_ID( float ), "CenterX", &m_FisheyeInfo.center.X, &dv.m_FisheyeInfo.center.X );
      xmlNode.Attribute( TYPE_ID( float ), "CenterY", &m_FisheyeInfo.center.Y, &dv.m_FisheyeInfo.center.Y );
      xmlNode.Attribute( TYPE_ID( float ), "Radius", &m_FisheyeInfo.radius, &dv.m_FisheyeInfo.radius );
   }
   return DONE;
}

int CFisheyeDewarpingSetup::WriteFile( CXMLIO* pIO )
{
   CFisheyeDewarpingSetup dv;
   CXMLNode xmlNode( pIO );
   if( xmlNode.Start( "Fisheye" ) ) {
      xmlNode.Attribute( TYPE_ID( BOOL ), "DefaultFisheyeInfo", &m_bDefaultFisheyeInfo, &dv.m_bDefaultFisheyeInfo );
      xmlNode.Attribute( TYPE_ID( BOOL ), "UseDewarp", &m_bUseDewarp, &dv.m_bUseDewarp );
      xmlNode.Attribute( TYPE_ID( int ), "Position", &m_FisheyeInfo.pos, &dv.m_FisheyeInfo.pos );
      xmlNode.Attribute( TYPE_ID( int ), "ViewType", &m_FisheyeInfo.view_type, &dv.m_FisheyeInfo.view_type );
      xmlNode.Attribute( TYPE_ID( float ), "FocalLength", &m_FisheyeInfo.f_length, &dv.m_FisheyeInfo.f_length );
      xmlNode.Attribute( TYPE_ID( float ), "LP_Range", &m_FisheyeInfo.lp_range, &dv.m_FisheyeInfo.lp_range );
      xmlNode.Attribute( TYPE_ID( float ), "UP_Range", &m_FisheyeInfo.up_range, &dv.m_FisheyeInfo.up_range );
      xmlNode.Attribute( TYPE_ID( float ), "CenterX", &m_FisheyeInfo.center.X, &dv.m_FisheyeInfo.center.X );
      xmlNode.Attribute( TYPE_ID( float ), "CenterY", &m_FisheyeInfo.center.Y, &dv.m_FisheyeInfo.center.Y );
      xmlNode.Attribute( TYPE_ID( float ), "Radius", &m_FisheyeInfo.radius, &dv.m_FisheyeInfo.radius );
   }
   return DONE;
}

FisheyeViewType CFisheyeDewarpingSetup::GetFisheyeView()
{
   return m_FisheyeInfo.view_type;
}

FPoint2D CFisheyeDewarpingSetup::GetFisheyeCenter()
{
   return m_FisheyeInfo.center;
}

float CFisheyeDewarpingSetup::GetFisheyeRadius()
{
   return m_FisheyeInfo.radius;
}

float CFisheyeDewarpingSetup::GetFisheyeLpRange()
{
   return m_FisheyeInfo.lp_range;
}

float CFisheyeDewarpingSetup::GetFisheyeUpRange()
{
   return m_FisheyeInfo.up_range;
}

float CFisheyeDewarpingSetup::GetFisheyeFocalLength()
{
   return m_FisheyeInfo.f_length;
}

BOOL CFisheyeDewarpingSetup::IsOn()
{
   if( m_bUseDewarp )
      return TRUE;
   else
      return FALSE;
}

#endif //  __SUPPORT_FISHEYE_CAMERA

///////////////////////////////////////////////////////////////////////////////
//
// Class: CImageEnhancement
//
///////////////////////////////////////////////////////////////////////////////

CImageEnhancement::CImageEnhancement()
{
   m_nState                      = 0;
   m_nWidth                      = 0;
   m_nHeight                     = 0;
   m_nYUY2BuffLength             = 0;
   m_fFrameRate                  = 0.0f;
   m_nVideoStabilizationMode     = 0;
   m_bPrevPTZCameraMoving        = -1;
   m_pSrcImgBuff                 = NULL;
   m_pDestImgBuff                = NULL;
   m_pDecompressTgtBuff          = NULL;
   m_pVideoStabilizerSrcBuff     = NULL;
   m_pVideoStabilizerTgtBuff     = NULL;
   m_pVideoDefoggingSrcBuff      = NULL;
   m_pVideoDefoggingTgtBuff      = NULL;
   m_pVideoNoiseReductionSrcBuff = NULL;
   m_pVideoNoiseReductionTgtBuff = NULL;
   m_pVideoDefogger              = new VideoDefogging;
   m_pVideoStabilizer            = NULL;
   m_pVideoNoiseReducer          = new VideoNoiseReduction;

   m_nTgtWidth      = 0;
   m_nTgtHeight     = 0;
   m_nPrevWidth     = 0;
   m_nPrevHeight    = 0;
   m_nPrevTgtWidth  = 0;
   m_nPrevTgtHeight = 0;
   m_fPrevRadius    = 0.0f;
   m_PrevCenterPos( -1.0f, -1.0f );
#ifdef __SUPPORT_FISHEYE_CAMERA
   m_pFisheyeDewarpSrcBuff           = NULL;
   m_pFisheyeDewarpTgtBuff           = NULL;
   m_pWarpingTable_FisheyeToCylinder = NULL;
#endif

   m_nHalfPanoramaCenterX = 0;
   m_nThreadEvtType       = 0;
}

CImageEnhancement::~CImageEnhancement()
{
   Close();
   if( m_pVideoDefogger ) {
      VideoDefogging* pVideoDefogger = (VideoDefogging*)m_pVideoDefogger;
      delete pVideoDefogger;
   }
   if( m_pVideoStabilizer ) {
      VideoStabilizationBase* pVideoStabilization = (VideoStabilizationBase*)m_pVideoStabilizer;
      delete pVideoStabilization;
   }
   if( m_pVideoNoiseReducer ) {
      VideoNoiseReduction* pVideoNoiseReducer = (VideoNoiseReduction*)m_pVideoNoiseReducer;
      delete pVideoNoiseReducer;
   }
#ifdef __SUPPORT_FISHEYE_CAMERA
   if( m_pWarpingTable_FisheyeToCylinder ) {
      delete m_pWarpingTable_FisheyeToCylinder;
      m_pWarpingTable_FisheyeToCylinder = NULL;
   }
#endif
}

void CImageEnhancement::Initialize( int nWidth, int nHeight, float fFrameRate, BOOL bThread )
{
   Close();
   m_fFrameRate      = fFrameRate;
   m_nWidth          = nWidth;
   m_nHeight         = nHeight;
   m_nYUY2BuffLength = GetYUY2ImageLength( m_nHeight, m_nWidth );
   if( bThread )
      m_nState |= ImageEnhancementState_ProcessInThread;
}

void CImageEnhancement::Close()
{
   StopImageEnhancementThreadProc();

   m_VideoStabilizationSetup.m_bUseVideoStabilizationPrev = FALSE;
   m_VideoDefoggingSetup.m_bUseDefogPrev                  = FALSE;

   m_nState &= ~ImageEnhancementState_VideoStabilizationIsInitialized;
   m_nState &= ~ImageEnhancementState_VideoDefogginIsInitialized;
   m_nState &= ~ImageEnhancementState_FisheyeDewarpIsInitialized;

   m_SrcYUY2Img.Delete();
   m_VideoStabilizationTgtYUY2Img.Delete();
   FrameBufferInputQueue.Delete();
   m_nWidth = m_nHeight = m_nYUY2BuffLength = 0;
}

void CImageEnhancement::PrepareImageEnhancementBuffProc( byte* pSrcImageBuff, byte* pDstImgBuff )
{
   CCriticalSectionSP co( m_csImageBuffer );
   // 영상개선을 해야하고 원본과 대상 버퍼가 같다면 원본영상을 별도로 생성한다.
   if( IsVideoEnhancementExist() && ( pSrcImageBuff == pDstImgBuff ) ) {
      if( m_nYUY2BuffLength != m_SrcYUY2Img.Length ) {
         // jun: 압축을 푸는 대상버퍼의 크기가 조금 커야 오류가 발생하지 않는다.
         m_SrcYUY2Img.Create( m_nYUY2BuffLength + m_nWidth * 2 );
         m_SrcYUY2Img.Length = m_nYUY2BuffLength;
      }
   } else {
      if( m_SrcYUY2Img.Length )
         m_SrcYUY2Img.Delete();
   }

   // 별도로 생성된 원본영상에 압축을 해제할 수 있도록 포인터를 업데이트한다.
   if( IsVideoEnhancementExist() && ( pSrcImageBuff == pDstImgBuff ) ) {
      m_pDecompressTgtBuff = m_SrcYUY2Img;
   } else {
      m_pDecompressTgtBuff = pDstImgBuff;
   }

   // 안개 제거 이전에 안정화를 해야한다면 별도의 안정화 버퍼를 생성한다.
   if( m_VideoDefoggingSetup.IsOn() && m_VideoStabilizationSetup.IsOn() ) {
      if( m_nYUY2BuffLength != m_VideoStabilizationTgtYUY2Img.Length )
         m_VideoStabilizationTgtYUY2Img.Create( m_nYUY2BuffLength );
   } else {
      m_VideoStabilizationTgtYUY2Img.Delete();
   }
}

void CImageEnhancement::UpdateImageEnhancementBuffPointer( byte* pSrcImageBuff, byte* pDstImgBuff, BOOL bInthread )
{
   CCriticalSectionSP co( m_csImageBuffer );
   if( IsVideoEnhancementExist() && ( pSrcImageBuff == pDstImgBuff ) ) {
      pSrcImageBuff = m_SrcYUY2Img;
   }

   byte* pResultImagebuffer;
   if( !bInthread )
      pResultImagebuffer = m_pDestImgBuff;
   else
      pResultImagebuffer = m_ImageEnhancementResultYUY2Img;

#ifdef __SUPPORT_FISHEYE_CAMERA
   if( IS_SUPPORT( __SUPPORT_FISHEYE_DEWARPING ) ) {
      if( m_DewarpingSetup.IsOn() ) {
         m_pFisheyeDewarpSrcBuff = pSrcImageBuff;
         m_pFisheyeDewarpTgtBuff = pDstImgBuff;
      }
   }
#endif

   if( m_VideoStabilizationSetup.IsOn() ) {
      if( m_VideoDefoggingSetup.IsOn() ) {
         m_pVideoStabilizerSrcBuff = pSrcImageBuff;
         m_pVideoStabilizerTgtBuff = m_VideoStabilizationTgtYUY2Img;
         m_pVideoDefoggingSrcBuff  = m_pVideoStabilizerTgtBuff;
         m_pVideoDefoggingTgtBuff  = pResultImagebuffer;
      } else {
         m_pVideoStabilizerSrcBuff = pSrcImageBuff;
         m_pVideoStabilizerTgtBuff = pResultImagebuffer;
         m_pVideoDefoggingSrcBuff  = NULL;
         m_pVideoDefoggingTgtBuff  = NULL;
      }
   } else {
      if( m_VideoDefoggingSetup.IsOn() ) {
         m_pVideoStabilizerSrcBuff = NULL;
         m_pVideoStabilizerTgtBuff = NULL;
         m_pVideoDefoggingSrcBuff  = pSrcImageBuff;
         m_pVideoDefoggingTgtBuff  = pResultImagebuffer;
      } else {
         m_pVideoStabilizerTgtBuff = NULL;
         m_pVideoDefoggingSrcBuff  = NULL;
         m_pVideoDefoggingTgtBuff  = NULL;
      }
   }
}

// 비디오 안정화
void CImageEnhancement::VideoStabilizationInitProc()
{
   CCriticalSectionSP co( m_csVideoStabilizer );
   VideoStabilizationBase* pVideoStabilizer = (VideoStabilizationBase*)m_pVideoStabilizer;
// 비디오 안정화 시작
#define __SUPPORT_VIDEO_STABILIZATION_IN_STATIC_CAM
   int nVideoStabilizationMode = 0; // 1: Static Camera
   BOOL bChangedPTZMovingState = FALSE;
#if defined( __SUPPORT_VIDEO_STABILIZATION_IN_STATIC_CAM )
   if( m_bPrevPTZCameraMoving != m_VideoStabilizationSetup.m_bPTZCameraMoving ) {
      m_nState &= ~ImageEnhancementState_VideoStabilizationIsInitialized;

      bChangedPTZMovingState = TRUE;
      if( m_VideoStabilizationSetup.m_bPTZCameraMoving )
         nVideoStabilizationMode = 1; // 1: Moving Camera
      m_bPrevPTZCameraMoving     = m_VideoStabilizationSetup.m_bPTZCameraMoving;
   }
#else
   nVideoStabilizationMode = 1; // 1: Moving Camera
#endif

   if( m_VideoStabilizationSetup.IsOn() && ( m_VideoStabilizationSetup.IsJustStarted() || m_VideoStabilizationSetup.IsChanged() || bChangedPTZMovingState ) ) {
      if( pVideoStabilizer ) {
         pVideoStabilizer->Close();
         delete pVideoStabilizer;
      }
      if( 0 == nVideoStabilizationMode ) {
         m_pVideoStabilizer                             = new VideoStabilization_StaticCam;
         VideoStabilization_StaticCam* pVideoStabilizer = (VideoStabilization_StaticCam*)m_pVideoStabilizer;
         ISize2D s2d( m_nWidth, m_nHeight );
         pVideoStabilizer->Initialize( s2d, m_fFrameRate );
      } else {
         m_pVideoStabilizer                             = new VideoStabilization_MovingCam;
         VideoStabilization_MovingCam* pVideoStabilizer = (VideoStabilization_MovingCam*)m_pVideoStabilizer;
         ISize2D s2d( m_nWidth, m_nHeight );
         pVideoStabilizer->Initialize( s2d, m_VideoStabilizationSetup.m_nVideoStabilizationFrameNum );
      }

      m_nState |= ImageEnhancementState_VideoStabilizationIsInitialized;
   }
   // 비디오 안정화 종료
   if( m_VideoStabilizationSetup.IsJustEnded() ) {
      pVideoStabilizer->Close();
      m_nState &= ~ImageEnhancementState_VideoStabilizationIsInitialized;
   }
   m_VideoStabilizationSetup.BackupToPrev();
}

void CImageEnhancement::VideoStabilizationMainProc()
{
   CCriticalSectionSP co( m_csVideoStabilizer );
   if( !m_pVideoStabilizer ) return;
   VideoStabilizationBase* pVideoStabilizer = (VideoStabilizationBase*)m_pVideoStabilizer;
   if( m_VideoStabilizationSetup.IsOn() && ( m_nState & ImageEnhancementState_VideoStabilizationIsInitialized ) ) {
      m_csImageBuffer.lock();
      if( m_pVideoStabilizerSrcBuff && m_pVideoStabilizerTgtBuff ) {
         pVideoStabilizer->Perform_YUY2( m_pVideoStabilizerSrcBuff, m_pVideoStabilizerTgtBuff );
      }
      m_pVideoDefoggingSrcBuff = m_pVideoStabilizerTgtBuff;
      m_csImageBuffer.unlock();
   }
}

// 안개 개선
void CImageEnhancement::VideoDefoggingInitProc()
{
   CCriticalSectionSP co( m_csVideoDefogger );
   if( !m_pVideoDefogger ) return;
   VideoDefogging* pVideoDefogger = (VideoDefogging*)m_pVideoDefogger;

   // 안개제거 시작
   if( m_VideoDefoggingSetup.IsJustStarted() || ( m_VideoDefoggingSetup.IsOn() && m_VideoDefoggingSetup.IsChanged() ) ) {
      pVideoDefogger->Close();
      int nDefogLevel = 10 - m_VideoDefoggingSetup.m_nDefogLevel + 1;
      if( 1 <= nDefogLevel && nDefogLevel <= 3 ) {
         pVideoDefogger->GammaCorrectionParam = 1.0f + ( nDefogLevel - 1 ) * 0.15f;
         pVideoDefogger->MinTransmission      = 0.2f + ( nDefogLevel - 1 ) * 0.05f;
      } else if( nDefogLevel >= 4 ) {
         pVideoDefogger->GammaCorrectionParam = 1.3f;
         pVideoDefogger->MinTransmission      = nDefogLevel * 0.1f;
      }
      if( m_nWidth * m_nHeight > 720 * 480 ) {
         m_nWidthDefog  = (int)( m_nWidth * m_VideoDefoggingSetup.m_ResizeFactor.X + 1 ) / 4 * 4;
         m_nHeightDefog = (int)( m_nHeight * m_VideoDefoggingSetup.m_ResizeFactor.Y + 1 ) / 4 * 4;
      } else {
         m_nWidthDefog  = m_nWidth;
         m_nHeightDefog = m_nHeight;
      }
      pVideoDefogger->Initialize( m_nWidthDefog, m_nHeightDefog, m_fFrameRate );
      m_nState |= ImageEnhancementState_VideoDefogginIsInitialized;
   }
   // 안개제거 종료
   if( m_VideoDefoggingSetup.IsJustEnded() ) {
      pVideoDefogger->Close();
      m_nState &= ~ImageEnhancementState_VideoDefogginIsInitialized;
   }
   m_VideoDefoggingSetup.BackupToPrev();
}

void CImageEnhancement::VideoDefoggingMainProc()
{
   CCriticalSectionSP co( m_csVideoDefogger );
   if( !m_pVideoDefogger ) return;
   VideoDefogging* pVideoDefogger = (VideoDefogging*)m_pVideoDefogger;
   if( m_VideoDefoggingSetup.IsOn() && ( m_nState & ImageEnhancementState_VideoDefogginIsInitialized ) ) {
      m_csImageBuffer.lock();
      if( m_pVideoDefoggingSrcBuff && m_pVideoDefoggingTgtBuff ) {
         if( m_nWidth != m_nWidthDefog || m_nHeight != m_nHeightDefog ) {
            BArray1D tmpResizedOrg( m_nWidthDefog * m_nHeightDefog * 2 );
            BArray1D tmpResizedDefog( tmpResizedOrg.Length );
            Resize_YUY2( m_pVideoDefoggingSrcBuff, m_nWidth, m_nHeight, tmpResizedOrg, m_nWidthDefog, m_nHeightDefog );
            pVideoDefogger->Perform_YUY2( tmpResizedOrg, tmpResizedDefog );
            Resize_YUY2( tmpResizedDefog, m_nWidthDefog, m_nHeightDefog, m_pVideoDefoggingTgtBuff, m_nWidth, m_nHeight );
         } else {
            pVideoDefogger->Perform_YUY2( m_pVideoDefoggingSrcBuff, m_pVideoDefoggingTgtBuff );
         }

#ifdef __SUPPORT_FISHEYE_CAMERA
         if( IS_SUPPORT( __SUPPORT_FISHEYE_DEWARPING ) )
            m_pFisheyeDewarpSrcBuff = m_pVideoDefoggingTgtBuff;
#endif
      }
      m_csImageBuffer.unlock();
   }
}

#ifdef __SUPPORT_FISHEYE_CAMERA
void CImageEnhancement::FisheyeDewarpingInitProc()
{
   if( !m_nTgtWidth || !m_nTgtHeight ) return;

   if( m_DewarpingSetup.m_bUseDewarp ) {
      FisheyeViewType CurFisheyeView = m_DewarpingSetup.GetFisheyeView();

      if( !m_pWarpingTable_FisheyeToCylinder ) {
         m_pWarpingTable_FisheyeToCylinder = new WarpingTable_FisheyeToCylinder;
      }

      BOOL bCenterPosChanged = FALSE;
      if( m_PrevCenterPos != m_DewarpingSetup.GetFisheyeCenter() )
         bCenterPosChanged = TRUE;

      BOOL bRadiusChanged = FALSE;
      if( m_fPrevRadius != m_DewarpingSetup.GetFisheyeRadius() )
         bRadiusChanged = TRUE;

      BOOL bSizeChanaged = FALSE;
      if( m_nPrevWidth != m_nWidth || m_nPrevHeight != m_nHeight )
         bSizeChanaged = TRUE;

      BOOL bTgtSizeChanaged = FALSE;
      if( m_nPrevTgtWidth != m_nTgtWidth || m_nPrevTgtHeight != m_nTgtHeight )
         bTgtSizeChanaged = TRUE;

      BOOL bFisheyeViewChanged = FALSE;
      if( m_CurFisheyeView != CurFisheyeView )
         bFisheyeViewChanged = TRUE;

      if( bSizeChanaged || bTgtSizeChanaged || bFisheyeViewChanged || bCenterPosChanged || bRadiusChanged ) {
         int nTgtWidth  = m_nTgtWidth;
         int nTgtHeight = m_nTgtHeight;
         if( DOUBLE_PANORAMA == CurFisheyeView ) {
            nTgtWidth  = nTgtWidth * 2;
            nTgtHeight = nTgtHeight / 2;
         } else if( HALF_PANORAMA == CurFisheyeView ) {
            nTgtWidth = nTgtWidth * 2;
         }
         m_pWarpingTable_FisheyeToCylinder->Initialize( m_nWidth, m_nHeight, m_DewarpingSetup.GetFisheyeCenter(), m_DewarpingSetup.GetFisheyeRadius(),
                                                        m_DewarpingSetup.GetFisheyeUpRange(), m_DewarpingSetup.GetFisheyeLpRange(), nTgtWidth, nTgtHeight );

         m_nPrevWidth     = m_nWidth;
         m_nPrevHeight    = m_nHeight;
         m_nPrevTgtWidth  = m_nTgtWidth;
         m_nPrevTgtHeight = m_nTgtHeight;
         m_fPrevRadius    = m_DewarpingSetup.GetFisheyeRadius();
         m_PrevCenterPos  = m_DewarpingSetup.GetFisheyeCenter();
      }

      m_CurFisheyeView = CurFisheyeView;
      m_nState |= ImageEnhancementState_FisheyeDewarpIsInitialized;
   }
}

void CImageEnhancement::FisheyeDewarpingMainProc()
{
   if( m_DewarpingSetup.IsOn() && m_nState & ImageEnhancementState_FisheyeDewarpIsInitialized ) {
      if( !m_pFisheyeDewarpTgtBuff ) return;
      m_csFisheyeDewarp.lock();
      if( SINGLE_PANORAMA == m_CurFisheyeView ) {
         m_pWarpingTable_FisheyeToCylinder->Warp_YUY2( m_pFisheyeDewarpSrcBuff, m_pFisheyeDewarpTgtBuff );
      } else {
         if( DOUBLE_PANORAMA == m_CurFisheyeView ) {
            int tmpWidth  = m_nTgtWidth * 2;
            int tmpHeight = m_nTgtHeight / 2;
            BArray1D tmpImg( tmpWidth * tmpHeight * 2 );
            m_pWarpingTable_FisheyeToCylinder->Warp_YUY2( m_pFisheyeDewarpSrcBuff, tmpImg );
            Make180DoublePanorama_YUY2( tmpImg, tmpWidth, tmpHeight, m_pFisheyeDewarpTgtBuff );
         } else if( HALF_PANORAMA == m_CurFisheyeView ) {
            int tmpWidth  = m_nTgtWidth * 2;
            int tmpHeight = m_nTgtHeight;
            BArray1D tmpImg( tmpWidth * tmpHeight * 2 );
            m_pWarpingTable_FisheyeToCylinder->Warp_YUY2( m_pFisheyeDewarpSrcBuff, tmpImg );
            Make180HalfPanorama_YUY2( tmpImg, tmpWidth, tmpHeight, m_pFisheyeDewarpTgtBuff, m_nHalfPanoramaCenterX );
         }
      }
      m_csFisheyeDewarp.unlock();
   }
}

int CImageEnhancement::GetWarpingTableWidth()
{
   if( m_pWarpingTable_FisheyeToCylinder )
      return m_pWarpingTable_FisheyeToCylinder->Width;
   else
      return -1;
}

int CImageEnhancement::GetWarpingTableHeight()
{
   if( m_pWarpingTable_FisheyeToCylinder )
      return m_pWarpingTable_FisheyeToCylinder->Height;
   else
      return -1;
}

void CImageEnhancement::GetFisheyeDewarpingImg( byte* pDewarpBuf )
{
   if( m_pFisheyeDewarpTgtBuff )
      pDewarpBuf = (byte*)m_pFisheyeDewarpTgtBuff;
}
#endif

void CImageEnhancement::StartImageEnhancementThreadProc()
{
   if( !m_ImageEnhancementThread.joinable() ) return;
   if( !m_VideoDefoggingSetup.IsOn() && !m_VideoStabilizationSetup.IsOn()
#ifdef __SUPPORT_FISHEYE_CAMERA
       && ( IS_SUPPORT( __SUPPORT_FISHEYE_DEWARPING ) && !m_DewarpingSetup.IsOn() )
#endif
           ) {
      return;
   }

   m_ImageEnhancementThread = std::thread( ThreadFuction_ImageEnhancement, this );
}

void CImageEnhancement::StopImageEnhancementThreadProc()
{
   if( !m_ImageEnhancementThread.joinable() ) return;

   {
      std::lock_guard<std::mutex> lk( m_evtThread.GetMutex() );
      m_nThreadEvtType = THREAD_EVENT_STOP;
      m_evtThread.SetEvent();
   }

   m_ImageEnhancementThread.join();
}

void CImageEnhancement::ImageEnhancementInitProc( byte* pSrcImgBuff, byte* pDstImgBuff )
{
   m_csImageBuffer.lock();
   m_pSrcImgBuff  = pSrcImgBuff;
   m_pDestImgBuff = pDstImgBuff;
   PrepareImageEnhancementBuffProc( pSrcImgBuff, pDstImgBuff );
   if( !m_ImageEnhancementThread.joinable() )
      UpdateImageEnhancementBuffPointer( pSrcImgBuff, pDstImgBuff, FALSE );
   m_csImageBuffer.unlock();
   VideoStabilizationInitProc();
   VideoDefoggingInitProc();
#ifdef __SUPPORT_FISHEYE_CAMERA
   if( IS_SUPPORT( __SUPPORT_FISHEYE_DEWARPING ) )
      FisheyeDewarpingInitProc();
#endif
   DetermineThreadStartEndProc();
}

BOOL CImageEnhancement::IsVideoEnhancementExist()
{
   if( m_VideoDefoggingSetup.IsOn() ) return TRUE;
   if( m_VideoStabilizationSetup.IsOn() ) return TRUE;
#ifdef __SUPPORT_FISHEYE_CAMERA
   if( IS_SUPPORT( __SUPPORT_FISHEYE_DEWARPING ) ) {
      if( m_DewarpingSetup.IsOn() ) return TRUE;
   }
#endif

   return FALSE;
}

BOOL CImageEnhancement::IsVideoEnhancementNotExist()
{
   if( m_VideoDefoggingSetup.IsOn() ) return FALSE;
   if( m_VideoStabilizationSetup.IsOn() ) return FALSE;
#ifdef __SUPPORT_FISHEYE_CAMERA
   if( IS_SUPPORT( __SUPPORT_FISHEYE_DEWARPING ) && m_DewarpingSetup.IsOn() )
      return FALSE;
#endif

   return TRUE;
}

void CImageEnhancement::DetermineThreadStartEndProc()
{
   //
   if( 1 ) {
      if( m_ImageEnhancementThread.joinable() ) {
         if( IsVideoEnhancementNotExist() ) {
            StopImageEnhancementThreadProc();
         }
      } else {
         if( m_nState & ImageEnhancementState_ProcessInThread ) {
            if( IsVideoEnhancementExist() ) {
               StartImageEnhancementThreadProc();
            }
         }
      }
   }
}

void CImageEnhancement::ImageEnhancementMainProc()
{
   if( m_ImageEnhancementThread.joinable() ) {
      if( m_SrcYUY2Img.Length ) {
         CFrameBuffer* frame_buffer = new CFrameBuffer;
         frame_buffer->Buffer.Import( m_SrcYUY2Img, m_SrcYUY2Img.Length );
         m_SrcYUY2Img.Import( NULL, 0 );
         CS_FrameBuffer.lock();
         FrameBufferInputQueue.Add( frame_buffer );
         CS_FrameBuffer.unlock();
         {
            std::lock_guard<std::mutex> lk( m_evtThread.GetMutex() );
            m_nThreadEvtType = THREAD_EVENT_FRAME_ADD;
            m_evtThread.SetEvent();
         }
         m_csImageBuffer.lock();
         if( m_pDestImgBuff && m_ImageEnhancementResultYUY2Img.Length == m_nYUY2BuffLength )
            memcpy( m_pDestImgBuff, (byte*)m_ImageEnhancementResultYUY2Img, m_nYUY2BuffLength );
         m_csImageBuffer.unlock();
      }
   } else {
      if( IS_SUPPORT( __SUPPORT_VIDEO_STABILIZATION ) ) {
         VideoStabilizationMainProc();
      }
      if( IS_SUPPORT( __SUPPORT_DEFOG ) ) {
         VideoDefoggingMainProc();
      }
#ifdef __SUPPORT_FISHEYE_CAMERA
      if( IS_SUPPORT( __SUPPORT_FISHEYE_DEWARPING ) ) {
         FisheyeDewarpingMainProc();
      }
#endif
   }
}

void CImageEnhancement::ImageEnhancementThreadProc()
{
   int i;
   while( 1 ) {
      m_evtThread.Wait();
      if( THREAD_EVENT_STOP == m_nThreadEvtType )
         break;

      CS_FrameBuffer.lock();
      int frame_num = FrameBufferInputQueue.GetNumItems();
      if( frame_num >= 2 ) {
         for( i = 1; i < frame_num; i++ ) {
            CFrameBuffer* frame_buffer = FrameBufferInputQueue.Retrieve();
            delete frame_buffer;
         }
      }
      CFrameBuffer* frame_buffer = FrameBufferInputQueue.Retrieve();
      CS_FrameBuffer.unlock();
      if( frame_buffer ) {
         // 스레드에서 영상개선이 수행되어야 하는 경우 별도의 결과 버퍼를 생성한다.
         m_csImageBuffer.lock();
         if( m_ImageEnhancementThread.joinable() ) {
            if( m_nYUY2BuffLength != m_ImageEnhancementResultYUY2Img.Length ) {
               m_ImageEnhancementResultYUY2Img.Create( m_nYUY2BuffLength );
               m_ImageEnhancementResultYUY2Img.Length = m_nYUY2BuffLength;
               if( m_pDecompressTgtBuff )
                  memcpy( (byte*)m_ImageEnhancementResultYUY2Img, m_pDecompressTgtBuff, m_nYUY2BuffLength );
            }
            if( IsVideoEnhancementExist() ) {
               m_pVideoStabilizerTgtBuff = m_ImageEnhancementResultYUY2Img;
               m_pVideoDefoggingTgtBuff  = m_ImageEnhancementResultYUY2Img;
            } else {
               m_ImageEnhancementResultYUY2Img.Delete();
            }
         }
         UpdateImageEnhancementBuffPointer( frame_buffer->Buffer, m_ImageEnhancementResultYUY2Img, TRUE );
         VideoStabilizationMainProc();
         VideoDefoggingMainProc();
#ifdef __SUPPORT_FISHEYE_CAMERA
         if( IS_SUPPORT( __SUPPORT_FISHEYE_DEWARPING ) )
            FisheyeDewarpingMainProc();
#endif
         m_csImageBuffer.unlock();

         delete frame_buffer;
      }
   }
}

UINT CImageEnhancement::ThreadFuction_ImageEnhancement( LPVOID pParam )
{
   CImageEnhancement* pImageEnhacement = (CImageEnhancement*)pParam;
   pImageEnhacement->ImageEnhancementThreadProc();
   return ( DONE );
}

#ifdef __SUPPORT_FISHEYE_CAMERA

void CImageEnhancement::MoveHalfPanoramaCenterX( int center_x )
{
   if( HALF_PANORAMA == m_CurFisheyeView ) {
      int max_center_x = m_nTgtWidth * 3;

      center_x *= -1;
      int rem = center_x % 4;
      center_x -= rem;

      m_nHalfPanoramaCenterX += center_x;
      if( m_nHalfPanoramaCenterX >= max_center_x ) {
         m_nHalfPanoramaCenterX -= max_center_x;
      } else if( m_nHalfPanoramaCenterX <= 0 ) {
         m_nHalfPanoramaCenterX += max_center_x;
      }
   }
}

#endif

#endif // #ifdef __SUPPORT_IMAGE_ENHANCEMENT

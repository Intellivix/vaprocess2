#if !defined(__BCCL_LIST_H)
#define __BCCL_LIST_H

#include "bccl_string.h"
#include "bccl_xml.h"

 namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class Template: List<T>
//
///////////////////////////////////////////////////////////////////////////////

 template<class T>
 class List

{
   protected:
      T *Items;

   public:
      int NItems;

   public:
      List (   );
      List (int n_items);
      List (const List &s_list);
      List (List *s_list);

   public:
      virtual ~List (   );

   public:
      List& operator =     (const List &s_list);
        T&  operator []    (int i) const;
        int operator !     (   ) const;
            operator T*    (   ) const;
            operator void* (   ) const;

   public:
      virtual void Clear  (   );
      virtual void Create (int n_items);
      virtual void Delete (   );
      virtual void Import (T *s_list,int n_items);

   public:
      void Copy (const List &s_list,int si,int n_items,int di);
      void Set  (int si,int n_items,T value);
};

 template<class T>
 inline T& List<T>::operator [] (int i) const

{
   return (Items[i]);
}

 template<class T>
 inline int List<T>::operator ! (   ) const
 
{
   if (Items) return (FALSE);
   else return (TRUE);
}

 template<class T>
 inline List<T>::operator T* (   ) const

{
   return (Items);
}

 template<class T>
 inline List<T>::operator void* (   ) const

{
   return ((void *)Items);
}

 template<class T>
 List<T>::List (   )

{
   NItems = 0;
   Items  = NULL;
}

 template<class T>
 List<T>::List (int n_items)

{
   Items = NULL;
   Create (n_items);
}

 template<class T>
 List<T>::List (const List &s_list)
 
{
   Items = NULL;
   Create (s_list.NItems);
   Copy (s_list,0,NItems + 1,0);
}

 template<class T>
 List<T>::List (List *s_list)

{
   NItems = s_list->NItems;
   Items   = s_list->Items;
   s_list->NItems = 0;
   s_list->Items  = NULL;
}

 template<class T>
 List<T>::~List (   )

{
   Delete (   );
}

 template<class T>
 List<T>& List<T>::operator = (const List &s_list)

{
   if (NItems != s_list.NItems) Create (s_list.NItems);
   Copy (s_list,0,NItems + 1,0);
   return (*this);
}
 
 template<class T>
 void List<T>::Clear (   )

{
   ClearArray1D ((byte *)Items,NItems + 1,sizeof(T));
}

 template<class T>
 void List<T>::Copy (const List &s_list,int si,int n_items,int di)

{
   int i,j;
 
   int ei = si + n_items;
   for (i = si,j = di; i < ei; i++,j++) Items[j] = s_list[i];
}

 template<class T>
 void List<T>::Create (int n_items) 

{
   if (Items != NULL) delete [] Items;
   Items = new T[n_items + 1];   
   NItems = n_items;
}

 template<class T>
 void List<T>::Delete (   )

{
   if (Items == NULL) return;
   delete [] Items;
   NItems = 0;
   Items = NULL;
}

 template<class T>
 void List<T>::Import (T *list,int n_items)

{
   Items  = list;
   NItems = n_items;
}

 template<class T>
 void List<T>::Set (int si,int n_items,T value)

{
   int i;

   int ei = si + n_items;
   for (i = si; i < ei; i++) Items[i] = value;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class Template: LinkedList<T>
//
///////////////////////////////////////////////////////////////////////////////

#define LL_FIRST   0
#define LL_LAST    1

 template<class T>
 class LinkedList

{
   protected:
      int NItems;
      T  *Items;
      T  *LastItem;

   public:
      LinkedList (   );
      LinkedList (const LinkedList &s_list);

   public:
      virtual ~LinkedList (   );

   public:
      LinkedList& operator =  (LinkedList &s_list);
               T* operator [] (int x);

   public:
      virtual void Delete (   );
   
   public:
      void Add          (T *s_item,int option = LL_LAST);
      T   *First        (   );
      void Copy         (LinkedList &s_list);
      T   *Get          (int n);
      int  GetNumItems  (   );
      void InsertAfter  (T *s_item,T *p_item);
      void InsertBefore (T *s_item,T *n_item);
      T   *Last         (   );
      void Move         (LinkedList &s_list,int option = LL_LAST);
      T   *Next         (T *s_item);
      T   *Previous     (T *s_item);
      void Remove       (T *s_item);
      void RemoveAll    (   );
      void Swap         (T *s_item, T *e_item);
      int  Where        (T *s_item);
};

 template<class T>
 LinkedList<T>::LinkedList (   )

{
   NItems = 0;
   Items = LastItem = NULL;
}

 template<class T>
 LinkedList<T>::LinkedList (const LinkedList &s_list)
 
{
   Copy (s_list);
}

 template<class T>
 LinkedList<T>::~LinkedList (   )

{
   Delete (   );
}

 template<class T>
 LinkedList<T>& LinkedList<T>::operator = (LinkedList &s_list)
 
{
   Copy (s_list);
   return (*this);
}

 template<class T>
 T *LinkedList<T>::operator [] (int n)

{
   return (T*)Get (n);
}

 template<class T>
 void LinkedList<T>::Add (T *s_item,int option)

{
   if (option == LL_FIRST) {
      s_item->Prev = NULL;
      if (Items == NULL) {
         Items = LastItem = s_item;
         s_item->Next = NULL;
     }
     else {
        s_item->Next = Items;
        Items = s_item->Next->Prev = s_item;
      }
   }
   else {
      s_item->Prev = LastItem;
      s_item->Next = NULL;
      if (LastItem == NULL) Items = s_item;
      else LastItem->Next = s_item;
      LastItem = s_item;
   }
   NItems++;
}
 
 template<class T>
 void LinkedList<T>::Copy (LinkedList &s_list)
 
{
   Delete (   );
   T *c_item = s_list.First (   );
   while (c_item != NULL) {
      T *n_item = new T;
      *n_item = *c_item;
      Add (n_item);
      c_item = s_list.Next (c_item);
   }
}

 template<class T>
 void LinkedList<T>::Delete (   )

{
   T *c_item = Items;
   while (c_item != NULL) {
      T *p_item = c_item;
      c_item = c_item->Next;
      delete p_item;
   }
   NItems = 0;
   Items = LastItem = NULL;
}

 template<class T>
 T *LinkedList<T>::First (   )

{
   return (Items);
}
 
 template<class T>
 T *LinkedList<T>::Get (int n)

{
   int i;

   T *c_item = First (   );
   for (i = 0;   ; i++) {
      if (c_item == NULL) return (NULL);
      if (i == n) return (c_item);
      c_item = Next (c_item);
   }
}

 template<class T>
 int LinkedList<T>::GetNumItems (   )

{
   return (NItems);
}

 template<class T>
 void LinkedList<T>::InsertAfter (T *s_item,T *p_item)

{
   s_item->Prev = p_item;
   if (p_item == NULL) {
      if (Items == NULL) {
         s_item->Next = NULL;
         LastItem = s_item;
      }
      else {
         s_item->Next = Items;
         s_item->Next->Prev = s_item;
      }
      Items = s_item;
   }
   else {
      s_item->Next = p_item->Next;
      p_item->Next = s_item;
      if (s_item->Next == NULL) LastItem = s_item;
      else s_item->Next->Prev = s_item;
   }
   NItems++;
}

 template<class T>
 void LinkedList<T>::InsertBefore (T *s_item,T *n_item)

{
   s_item->Next = n_item;
   s_item->Prev = n_item->Prev;
   n_item->Prev = s_item;
   if (s_item->Prev == NULL) Items = s_item;
   else s_item->Prev->Next = s_item;
   NItems++;
}

 template<class T>
 T *LinkedList<T>::Last (   )

{
   return (LastItem);
}

 template<class T>
 void LinkedList<T>::Move (LinkedList &s_list,int option)

{
   int n_items = s_list.GetNumItems (   );
   if (!n_items) return;
   T *f_item = s_list.First (   );
   T *l_item = s_list.Last  (   );
   if (option == LL_FIRST) {
      if (Items == NULL) LastItem = l_item;
      else {
         l_item->Next = Items;
         Items->Prev  = l_item;
      }
      Items = f_item;
   }
   else {
      if (LastItem == NULL) Items = f_item;
      else {
         LastItem->Next = f_item;
         f_item->Prev   = LastItem;
      }
      LastItem = l_item;
   }
   NItems += n_items;
   s_list.NItems = 0;
   s_list.Items = s_list.LastItem = NULL;
}

 template<class T>
 T *LinkedList<T>::Next (T *s_item)

{
   if (s_item == NULL) return (NULL);
   else return (s_item->Next);
}

 template<class T>
 T *LinkedList<T>::Previous (T *s_item)

{
   if (s_item == NULL) return (NULL);
   else return (s_item->Prev);
}

 template<class T>
 void LinkedList<T>::Remove (T *s_item)

{
   if (s_item->Prev == NULL) {
      Items = s_item->Next;
      if (s_item->Next == NULL) LastItem = NULL;
      else s_item->Next->Prev = NULL;
   }
   else {
      s_item->Prev->Next = s_item->Next;
      if (s_item->Next == NULL) LastItem = s_item->Prev;
      else s_item->Next->Prev = s_item->Prev;
   }
   NItems--;
}

 template<class T>
 void LinkedList<T>::RemoveAll (   )

{
   NItems = 0;
   Items = LastItem = NULL;
}

// jun : IntelliVIX 내부에 LinkedList를 많이 사용하는데 Swap 기능이 필요하여 구현 하였습니다.
 template<class T>
 void LinkedList<T>::Swap (T *s_item, T *e_item)

 {
    if (First (   ) == s_item) 
       Items = e_item;
    else if (First (   ) == e_item) 
       Items = s_item;

    if (s_item->Next == e_item) {
       T* s_item_prev = s_item->Prev;
       s_item->Prev = e_item;
       s_item->Next = e_item->Next;
       e_item->Prev = s_item_prev;
       e_item->Next = s_item;
       if (s_item->Next) s_item->Next->Prev = s_item;
       if (e_item->Prev) e_item->Prev->Next = e_item;
       if (e_item == LastItem)
          LastItem = s_item;
    }
    else if (e_item->Next == s_item) {
       T* e_item_prev = e_item->Prev;
       e_item->Prev = s_item;
       e_item->Next = s_item->Next;
       s_item->Prev = e_item_prev;
       s_item->Next = e_item;
       if (s_item->Prev) 
          s_item->Prev->Next = s_item;
       if (e_item->Next) 
          e_item->Next->Prev = e_item;
       if (s_item == LastItem)
          LastItem = e_item;
    }
    else {
       T* e_item_Prev = e_item->Prev;
       T* e_item_Next = e_item->Next;
       e_item->Prev = s_item->Prev;
       e_item->Next = s_item->Next;
       s_item->Prev = e_item_Prev;
       s_item->Next = e_item_Next;
       if (s_item->Next) s_item->Next->Prev = s_item;
       if (e_item->Prev) e_item->Prev->Next = e_item;
       if (s_item->Prev) s_item->Prev->Next = s_item;
       if (e_item->Next) e_item->Next->Prev = e_item;
       if (e_item == LastItem)
          LastItem = s_item;
       if (s_item == LastItem)
          LastItem = e_item;
    }
 }

 template<class T>
 int LinkedList<T>::Where (T *s_item)

{
   int i;

   T *c_item = First (   );
   for (i = 0;   ; i++) {
      if (c_item == NULL  ) return (-1);
      if (c_item == s_item) return (i);
      c_item = Next (c_item);
   }
}

 template<class T>
 int ReadFile (LinkedList<T>& llist, CXMLIO* pIO, const char* szNodeName)

{
   int i,n_items;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start (szNodeName))
   {
      llist.Delete (   );
      xmlNode.Attribute (TYPE_ID (int) , "ItemNum" , &n_items);
      if (n_items != llist.GetNumItems ()) {
         llist.Delete (   );
         for (i = 0; i < n_items; i++) {
            T *item = new T;
            if (DONE == item->ReadFile (pIO)) 
               llist.Add (item);
            else
               delete item;
         }
      }
      else {
         T *item = llist.First (   );
         while (item != NULL) {
            item->ReadFile (pIO);
            item = llist.Next (item);
         }
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

 template<class T>
 int WriteFile (LinkedList<T>& llist, CXMLIO* pIO, const char* szNodeName)

{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start (szNodeName))
   {
      int n_items = llist.GetNumItems (   );
      xmlNode.Attribute (TYPE_ID (int) , "ItemNum" , &n_items);
      T *item = llist.First (   );
      while (item != NULL) {
         item->WriteFile (pIO);
         item = llist.Next (item);
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class Template: Queue<T>
//
///////////////////////////////////////////////////////////////////////////////

 template<class T>
 class Queue : public LinkedList<T>
 
{
   public:
      int QueueLength;
      
   public:
      Queue (   ) { QueueLength = 100; };
      virtual ~Queue (   ) {   };
      
   public:
      int  Add      (T *s_item);
      void Clear    (   );
      T*   Retrieve (   );
};

 template<class T>
 int Queue<T>::Add (T *s_item)
 
{
   if (LinkedList<T>::GetNumItems (   ) >= QueueLength) return (1);
   LinkedList<T>::Add (s_item,LL_LAST);
   return (DONE);
}

 template<class T>
 void Queue<T>::Clear (   )
 
{
   LinkedList<T>::Delete (   );
}

 template <class T>
 T* Queue<T>::Retrieve (   )
 
{
   T* item = LinkedList<T>::First (   );
   if (item != NULL) this->Remove (item);
   return (item);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: Parameter
//
///////////////////////////////////////////////////////////////////////////////

#define MAX_PARAM_NAME_LENGTH   128
#define MAX_PARAM_DIMENSION     16

 class Parameter
 
{
   public:
      int     Dimension;                   // 파라미터 값의 갯수
      StringA Name;                        // 파라미터 이름
      StringA Values[MAX_PARAM_DIMENSION]; // 파라미터 값들을 저장하는 배열
      
   public:
      Parameter *Prev,*Next;
      
   public:
      Parameter (   ) { Dimension = 0; };
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: ParameterList
//
///////////////////////////////////////////////////////////////////////////////
//
// 파라미터 문자열을 파싱하여 파라미터 목록을 얻는다.
// 파라미터 문자열의 예: "Position=50,20.5,37.2 & Speed=20.3 & IsHuman=false"
//

 class ParameterList : public LinkedList<Parameter>
 
{
   protected:
      void GetParameterName  (const char *s_string,int si,int ei,Parameter *param);
      int  GetParameterValue (const char *s_string,int si,int ei,Parameter *param);

   public:
      // 주어진 파라미터 문자열을 파싱하여 파라미터 목록을 얻는다.
      // s_string : 파라미터 문자열
      // flag_init: TRUE이면 기존 파라미터 목록은 삭제한다.
      //            FALSE이면 기존 파라미터 목록에 추가한다.
      int  Parse (const char* s_string,int flag_init = TRUE);
      void Print (   );
};

}

#endif

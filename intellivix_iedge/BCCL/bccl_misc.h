#if !defined(__BCCL_MISC_H)
#define __BCCL_MISC_H

#include "bccl_image.h"

 namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// Structure: ObjectScore
//
///////////////////////////////////////////////////////////////////////////////

 struct ObjectScore

{
   int    IndexNo;
   double Score;
};

 typedef Array1D<ObjectScore> OSArray1D;
 
///////////////////////////////////////////////////////////////////////////////
//
// Function Templates
//
///////////////////////////////////////////////////////////////////////////////

 template <class T>
 void Binarize (Array2D<T> &s_array,T thrsld,GImage &d_image)

{
   int i,j;

   int sw = s_array.Width;
   int sh = s_array.Height;
   for (i = 0; i < sh; i++) {
      T    *_RESTRICT l_s_array = s_array[i];
      byte *_RESTRICT l_d_image = d_image[i];
      for (j = 0; j < sw; j++)
         l_d_image[j] = (l_s_array[j] > thrsld) ? PG_WHITE : PG_BLACK;
   }
}

 template <class T>
 inline T GetMaximum (T a,T b)

{
   if (a > b) return (a);
   else return (b);
}

 template <class T>
 inline T GetMinimum (T a,T b)

{
   if (a < b) return (a);
   else return (b);
}

 template <class T>
 inline void Swap (T& a,T& b)

{
   T t = a;
   a   = b;
   b   = t;
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

extern "C" {

int Compare_Byte_Ascending         (const void *a,const void *b);
int Compare_Byte_Descending        (const void *a,const void *b);
int Compare_Double_Ascending       (const void *a,const void *b);
int Compare_Double_Descending      (const void *a,const void *b);
int Compare_Float_Ascending        (const void *a,const void *b);
int Compare_Float_Descending       (const void *a,const void *b);
int Compare_Int_Ascending          (const void *a,const void *b);
int Compare_Int_Descending         (const void *a,const void *b);
int Compare_ObjectScore_Ascending  (const void *a,const void *b);
int Compare_ObjectScore_Descending (const void *a,const void *b);
int Compare_Short_Ascending        (const void *a,const void *b);
int Compare_Short_Descending       (const void *a,const void *b);
int Compare_String_Ascending       (const void *a,const void *b);
int Compare_String_Descending      (const void *a,const void *b);
int Compare_UShort_Ascending       (const void *a,const void *b);
int Compare_UShort_Descending      (const void *a,const void *b);

}

byte FindMedian (byte *s_buffer,int n);

}

#endif

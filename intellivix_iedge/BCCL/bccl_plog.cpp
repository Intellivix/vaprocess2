﻿#include "bccl_plog.h"

////////////////////////////////
/// PlogHelper
////////////////////////////////

namespace BCCL {

const std::string PlogHelper::m_logFileName = "intellivix.log";

void PlogHelper::removeLastEndLine( std::string& str )
{
   if( str.size() > 2 ) {
      if( str[str.size() - 2] == '\n' ) {
         str.pop_back(); // extra space for '\0'
         str.pop_back(); // if contexts ends with '\n'
      }
   }
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-nonliteral"
std::string PlogHelper::cFormatToStdString( std::string format, ... )
{
   va_list args, args_copy;
   va_start( args, format );
   va_copy( args_copy, args );

   int sz         = vsnprintf( nullptr, 0, format.c_str(), args );
   size_t max_len = static_cast<size_t>( sz ) + 1;
   try {
      std::string result( max_len, ' ' );
      vsnprintf( &result.front(), max_len, format.c_str(), args_copy );
      va_end( args_copy );
      va_end( args );
      removeLastEndLine( result );
      return result;
   } catch( const std::bad_alloc& ) {
      va_end( args_copy );
      va_end( args );
      throw;
   }
}
#pragma GCC diagnostic pop

void PlogHelper::initDisplayConsole( plog::Severity log_level )
{
   static plog::IvxColorConsoleAppender<plog::IvxConsoleFormatter> consoleAppender;
   plog::init( log_level, &consoleAppender );
}

void PlogHelper::initWriteToFile( plog::Severity log_level, const std::string &filename, int count_of_rolling_files )
{
   std::string logFileName;
   if( filename.empty() ) {
      logFileName = m_logFileName;
   } else {
      logFileName = filename;
   }

   // to write files rolling by size.
   uint a_file_size_is_one_mega_bytes = 1 * 1024 * 1024;
   static plog::RollingFileAppender<plog::TxtFormatter> fileAppende(
       logFileName.c_str(), a_file_size_is_one_mega_bytes, count_of_rolling_files );
   plog::init( log_level, &fileAppende );
}

void PlogHelper::initDisplayAndWriteToFile( plog::Severity log_level, const std::string &filename, int count_of_rolling_files )
{
   std::string logFileName;
   if( filename.empty() ) {
      logFileName = m_logFileName;
   } else {
      logFileName = filename;
   }
   
   static plog::IvxColorConsoleAppender<plog::IvxConsoleFormatter> consoleAppender;
   uint a_file_size_is_one_mega_bytes = 1 * 1024 * 1024;
   static plog::RollingFileAppender<plog::TxtFormatter> fileAppende(
       logFileName.c_str(), a_file_size_is_one_mega_bytes, count_of_rolling_files );

   // to use both. use addAppender() instaed of init() with consoleAppender.
   plog::init( log_level, &fileAppende ).addAppender( &consoleAppender );
}

} // namespace BCCL

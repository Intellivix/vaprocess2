﻿#include "bccl_xmlcvtfn.h"
#include "bccl_plog.h"

#define DECLARE_CONVFUNC( funcName ) BOOL funcName( std::string& str, void* pInOutMem, void* pDefValue, char* szFormat, int nMem )
#define DECLARE_CONVFUNC_PAIR( funcStr2Type, funcType2Str ) \
   DECLARE_CONVFUNC( funcStr2Type );                        \
   DECLARE_CONVFUNC( funcType2Str );

#if defined( __WIN32 )
#define FORMAT_I64d ( "%I64d" )
#define FORMAT_I64u ( "%I64u" )
#define FORMAT_I64x ( "%I64x" )
#define FORMAT_I32d ( "%I32d" )
#define FORMAT_I32u ( "%I32u" )
#define FORMAT_I32x ( "%I32x" )
#define FORMAT_I32dx2 ( "(%I32d,%I32d)" )
#define FORMAT_I32dx3 ( "(%I32d,%I32d,%I32d)" )
#define FORMAT_I32dx4 ( "(%I32d,%I32d,%I32d,%I32d)" )
#else
#define FORMAT_I64d ( "%lld" )
#define FORMAT_I64u ( "%llu" )
#define FORMAT_I64x ( "%llx" )
#define FORMAT_I32d ( "%d" )
#define FORMAT_I32u ( "%u" )
#define FORMAT_I32x ( "%x" )
#define FORMAT_I32dx2 ( "(%d,%d)" )
#define FORMAT_I32dx3 ( "(%d,%d,%d)" )
#define FORMAT_I32dx4 ( "(%d,%d,%d,%d)" )
#endif

namespace BCCL {

typedef BOOL ( *CONVFUNCTYPE )( std::string& str, void* pInOutMem, void* pDefValue, char* szFormat, int nMem );
struct Str2VarCvtFunc {
   int m_nType; // 변수 타입
   CONVFUNCTYPE m_pStr2Var; // 변환함수 포인터 (스트링 --> 변수  )
   CONVFUNCTYPE m_pVar2Str; // 변환함수 포인터 (변수   --> 스트링)
};

DECLARE_CONVFUNC_PAIR( Str2CBOOL, BOOL2Str );
DECLARE_CONVFUNC_PAIR( Str2CHAR, CHAR2Str );
DECLARE_CONVFUNC_PAIR( Str2ICHAR, ICHAR2Str );
DECLARE_CONVFUNC_PAIR( Str2BYTE, BYTE2Str );
DECLARE_CONVFUNC_PAIR( Str2XBYTE, XBYTE2Str );
DECLARE_CONVFUNC_PAIR( Str2b64byte, b64byte2Str );
DECLARE_CONVFUNC_PAIR( Str2Short, Short2Str );
DECLARE_CONVFUNC_PAIR( Str2UShort, UShort2Str );
DECLARE_CONVFUNC_PAIR( Str2XShort, XShort2Str );
DECLARE_CONVFUNC_PAIR( Str2Int, Int2Str );
DECLARE_CONVFUNC_PAIR( Str2UInt, UInt2Str );
DECLARE_CONVFUNC_PAIR( Str2XInt, XInt2Str );
DECLARE_CONVFUNC_PAIR( Str2LONG, LONG2Str );
DECLARE_CONVFUNC_PAIR( Str2ULONG, ULONG2Str );
DECLARE_CONVFUNC_PAIR( Str2XLONG, XLONG2Str );
DECLARE_CONVFUNC_PAIR( Str2Int64, Int642Str );
DECLARE_CONVFUNC_PAIR( Str2UInt64, UInt642Str );
DECLARE_CONVFUNC_PAIR( Str2XInt64, XInt642Str );
DECLARE_CONVFUNC_PAIR( Str2Float, Float2Str );
DECLARE_CONVFUNC_PAIR( Str2Double, Double2Str );
DECLARE_CONVFUNC_PAIR( Str2StringA, StringA2Str );
DECLARE_CONVFUNC_PAIR( Str2IPoint2D, IPoint2D2Str );
DECLARE_CONVFUNC_PAIR( Str2FPoint2D, FPoint2D2Str );
DECLARE_CONVFUNC_PAIR( Str2FPoint3D, FPoint3D2Str );
DECLARE_CONVFUNC_PAIR( Str2ISize2D, ISize2D2Str );
DECLARE_CONVFUNC_PAIR( Str2ILine2D, ILine2D2Str );
DECLARE_CONVFUNC_PAIR( Str2IBox2D, IBox2D2Str );
DECLARE_CONVFUNC_PAIR( Str2FBox2D, FBox2D2Str );
DECLARE_CONVFUNC_PAIR( Str2BGRColor, BGRColor2Str );
DECLARE_CONVFUNC_PAIR( Str2string, string2Str );
DECLARE_CONVFUNC_PAIR( Str2SYSTEMTIME, SYSTEMTIME2Str );
DECLARE_CONVFUNC_PAIR( Str2FILETIME, FILETIME2Str );
DECLARE_CONVFUNC_PAIR( Str2IPADDR, IPADDR2Str );
DECLARE_CONVFUNC_PAIR( Str2CRect, CRect2Str );
DECLARE_CONVFUNC_PAIR( Str2CSize, CSize2Str );
DECLARE_CONVFUNC_PAIR( Str2CPoint, CPoint2Str );
DECLARE_CONVFUNC_PAIR( Str2CTime, CTime2Str );
DECLARE_CONVFUNC_PAIR( Str2CStringA, CStringA2Str );

// string member to variable conversion function table
Str2VarCvtFunc Str2VarConvFuncsTbl[] = {
   { TYPE_ID( BOOL ), Str2CBOOL, BOOL2Str },
   { TYPE_ID( char ), Str2CHAR, CHAR2Str },
   { TYPE_ID( ICHAR ), Str2ICHAR, ICHAR2Str },
   { TYPE_ID( BYTE ), Str2BYTE, BYTE2Str },
   { TYPE_ID( XBYTE ), Str2XBYTE, XBYTE2Str },
   { TYPE_ID( b64byte ), Str2b64byte, b64byte2Str },
   { TYPE_ID( short ), Str2Short, Short2Str },
   { TYPE_ID( ushort ), Str2UShort, UShort2Str },
   { TYPE_ID( XSHORT ), Str2XShort, XShort2Str },
   { TYPE_ID( int ), Str2Int, Int2Str },
   { TYPE_ID( uint ), Str2UInt, UInt2Str },
   { TYPE_ID( XINT ), Str2XInt, XInt2Str },
   { TYPE_ID( LONG ), Str2LONG, LONG2Str },
   { TYPE_ID( ULONG ), Str2ULONG, ULONG2Str },
   { TYPE_ID( XLONG ), Str2XLONG, XLONG2Str },
   { TYPE_ID( INT64 ), Str2Int64, Int642Str },
   { TYPE_ID( UINT64 ), Str2UInt64, UInt642Str },
   { TYPE_ID( XINT64 ), Str2XInt64, XInt642Str },
   { TYPE_ID( float ), Str2Float, Float2Str },
   { TYPE_ID( double ), Str2Double, Double2Str },
   { TYPE_ID( StringA ), Str2StringA, StringA2Str },
   { TYPE_ID( IPoint2D ), Str2IPoint2D, IPoint2D2Str },
   { TYPE_ID( FPoint2D ), Str2FPoint2D, FPoint2D2Str },
   { TYPE_ID( FPoint3D ), Str2FPoint3D, FPoint3D2Str },
   { TYPE_ID( ISize2D ), Str2ISize2D, ISize2D2Str },
   { TYPE_ID( ILine2D ), Str2ILine2D, ILine2D2Str },
   { TYPE_ID( IBox2D ), Str2IBox2D, IBox2D2Str },
   { TYPE_ID( FBox2D ), Str2FBox2D, FBox2D2Str },
   { TYPE_ID( BGRColor ), Str2BGRColor, BGRColor2Str },
   { TYPE_ID( string ), Str2string, string2Str },
   { TYPE_ID( SYSTEMTIME ), Str2SYSTEMTIME, SYSTEMTIME2Str },
   { TYPE_ID( FILETIME ), Str2FILETIME, FILETIME2Str },
   { TYPE_ID( IPADDR ), Str2IPADDR, IPADDR2Str },
   { TYPE_ID( CRect ), Str2CRect, CRect2Str },
   { TYPE_ID( CSize ), Str2CSize, CSize2Str },
   { TYPE_ID( CPoint ), Str2CPoint, CPoint2Str },
   { TYPE_ID( CTime ), Str2CTime, CTime2Str },
   { TYPE_ID( CStringA ), Str2CStringA, CStringA2Str },
   { 0, NULL, NULL }
};

typedef std::map<int, Str2VarCvtFunc*> MapType2CvtFunc;
typedef std::map<int, Str2VarCvtFunc*>::iterator MapType2CvtFuncIter;
typedef std::map<int, Str2VarCvtFunc*>::value_type MapType2CvtFuncValuePair;

static MapType2CvtFunc g_MapType2CvtFunc;
static BOOL g_bUseDefValue_XMLIO = FALSE;

void SetXMLUseDefaultValue( BOOL bUseDefaultValue )
{
   g_bUseDefValue_XMLIO = bUseDefaultValue;
}

void InsertExtConvFuncs( Str2VarCvtFunc* tbl )
{
   for( int i = 0; tbl[i].m_nType; i++ ) {
      g_MapType2CvtFunc.insert( MapType2CvtFuncValuePair( tbl[i].m_nType, &tbl[i] ) );
   }
}

void InitType2ConvertFuncTable()
{
   for( int i = 0; Str2VarConvFuncsTbl[i].m_nType; i++ ) {
      g_MapType2CvtFunc.insert( MapType2CvtFuncValuePair( Str2VarConvFuncsTbl[i].m_nType, &Str2VarConvFuncsTbl[i] ) );
   }
}

BOOL IsSameVar( int nTypeSize, void* pVar, void* pDefVar, int nVarNum )
{
   int i;

   if( FALSE == g_bUseDefValue_XMLIO ) return FALSE;

   if( nTypeSize == sizeof( BYTE ) ) {
      if( pDefVar ) {
         BYTE* pByte    = (BYTE*)pVar;
         BYTE* pByteDef = (BYTE*)pDefVar;
         if( nVarNum == 1 ) {
            if( *pByte == *pByteDef ) return TRUE;
         } else if( nVarNum > 1 ) {
            BOOL bSame = TRUE;
            for( i = 0; i < nVarNum; i++ ) {
               if( pByte[i] != pByteDef[i] ) {
                  bSame = FALSE;
                  break;
               }
            }
            if( bSame ) return TRUE;
         }
      }
   } else if( nTypeSize == sizeof( ushort ) ) {
      if( pDefVar ) {
         ushort* pUshort    = (ushort*)pVar;
         ushort* pUshortDef = (ushort*)pDefVar;
         if( nVarNum == 1 ) {
            if( *pUshort == *pUshortDef ) return TRUE;
         } else if( nVarNum > 1 ) {
            BOOL bSame = TRUE;
            for( i = 0; i < nVarNum; i++ ) {
               if( pUshort[i] != pUshortDef[i] ) {
                  bSame = FALSE;
                  break;
               }
            }
            if( bSame ) return TRUE;
         }
      }
   } else if( nTypeSize == sizeof( ushort ) ) {
      if( pDefVar ) {
         ushort* pUshort    = (ushort*)pVar;
         ushort* pUshortDef = (ushort*)pDefVar;
         if( nVarNum == 1 ) {
            if( *pUshort == *pUshortDef ) return TRUE;
         } else if( nVarNum > 1 ) {
            BOOL bSame = TRUE;
            for( i = 0; i < nVarNum; i++ ) {
               if( pUshort[i] != pUshortDef[i] ) {
                  bSame = FALSE;
                  break;
               }
            }
            if( bSame ) return TRUE;
         }
      }
   } else if( nTypeSize == sizeof( uint ) ) {
      if( pDefVar ) {
         uint* pUInt    = (uint*)pVar;
         uint* pUIntDef = (uint*)pDefVar;
         if( nVarNum == 1 ) {
            if( *pUInt == *pUIntDef ) return TRUE;
         } else if( nVarNum > 1 ) {
            BOOL bSame = TRUE;
            for( i = 0; i < nVarNum; i++ ) {
               if( pUInt[i] != pUIntDef[i] ) {
                  bSame = FALSE;
                  break;
               }
            }
            if( bSame ) return TRUE;
         }
      }
   } else if( nTypeSize == sizeof( UINT64 ) ) {
      if( pDefVar ) {
         UINT64* pUInt    = (UINT64*)pVar;
         UINT64* pUIntDef = (UINT64*)pDefVar;
         if( nVarNum == 1 ) {
            if( *pUInt == *pUIntDef ) return TRUE;
         } else if( nVarNum > 1 ) {
            BOOL bSame = TRUE;
            for( i = 0; i < nVarNum; i++ ) {
               if( pUInt[i] != pUIntDef[i] ) {
                  bSame = FALSE;
                  break;
               }
            }
            if( bSame ) return TRUE;
         }
      }
   }
   return FALSE;
}

BOOL CopyFromDefaultValue( int nTypeSize, void* pVar, void* pDefVar, int nVarNum )
{
   if( pDefVar ) {
      BYTE* pByte    = (BYTE*)pVar;
      BYTE* pByteDef = (BYTE*)pDefVar;
      memcpy( pByte, pByteDef, nTypeSize * nVarNum );
      return TRUE;
   }
   return FALSE;
}

char* strDup( char const* str )
{
   if( str == NULL ) return NULL;
   size_t len = strlen( str ) + 1;
   char* copy = new char[len];

   if( copy != NULL ) {
      memcpy( copy, str, len );
   }
   return copy;
}

char* strDupSize( char const* str )
{
   if( str == NULL ) return NULL;
   size_t len = strlen( str ) + 1;
   char* copy = new char[len];

   return copy;
}

static char base64DecodeTable[256];

static void initBase64DecodeTable()
{
   int i;
   for( i = 0; i < 256; ++i )
      base64DecodeTable[i] = (char)0x80;
   // default value: invalid

   for( i = 'A'; i <= 'Z'; ++i )
      base64DecodeTable[i] = 0 + ( i - 'A' );
   for( i = 'a'; i <= 'z'; ++i )
      base64DecodeTable[i] = 26 + ( i - 'a' );
   for( i = '0'; i <= '9'; ++i )
      base64DecodeTable[i] = 52 + ( i - '0' );
   base64DecodeTable[(unsigned char)'+'] = 62;
   base64DecodeTable[(unsigned char)'/'] = 63;
   base64DecodeTable[(unsigned char)'='] = 0;
}

void base64Decode( char const* in, unsigned inSize, unsigned& resultSize, bool trimTrailingZeros, byte* resultBuffer )
{
   static bool haveInitializedBase64DecodeTable = false;
   if( !haveInitializedBase64DecodeTable ) {
      initBase64DecodeTable();
      haveInitializedBase64DecodeTable = true;
   }

   unsigned char* out = (unsigned char*)strDupSize( in ); // ensures we have enough space
   int k              = 0;
   int paddingCount   = 0;
   int const jMax     = inSize - 3;
   // in case "inSize" is not a multiple of 4 (although it should be)
   for( int j = 0; j < jMax; j += 4 ) {
      char inTmp[4], outTmp[4];
      for( int i = 0; i < 4; ++i ) {
         inTmp[i] = in[i + j];
         if( inTmp[i] == '=' ) ++paddingCount;
         outTmp[i] = base64DecodeTable[(unsigned char)inTmp[i]];
         if( ( outTmp[i] & 0x80 ) != 0 ) outTmp[i] = 0; // this happens only if there was an invalid character; pretend that it was 'A'
      }

      out[k++] = ( outTmp[0] << 2 ) | ( outTmp[1] >> 4 );
      out[k++] = ( outTmp[1] << 4 ) | ( outTmp[2] >> 2 );
      out[k++] = ( outTmp[2] << 6 ) | outTmp[3];
   }

   if( trimTrailingZeros ) {
      while( paddingCount > 0 && k > 0 && out[k - 1] == '\0' ) {
         --k;
         --paddingCount;
      }
   }
   resultSize = k;
   //unsigned char* result = new unsigned char[resultSize];
   memmove( resultBuffer, out, resultSize );
   delete[] out;
}

void base64Decode( char const* in, unsigned& resultSize, bool trimTrailingZeros, byte* resultBuffer )
{
   if( in == NULL ) return; // sanity check
   base64Decode( in, (uint)strlen( in ), resultSize, trimTrailingZeros, resultBuffer );
}

static const char base64Char[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

void base64Encode( char const* origSigned, unsigned origLength, std::string& strResult )
{
   unsigned char const* orig = (unsigned char const*)origSigned; // in case any input bytes have the MSB set
   if( orig == NULL ) return;

   unsigned const numOrig24BitValues = origLength / 3;
   bool havePadding                  = origLength > numOrig24BitValues * 3;
   bool havePadding2                 = origLength == numOrig24BitValues * 3 + 2;

   char tempResult4byte[5];
   tempResult4byte[4] = 0;
   // Map each full group of 3 input bytes into 4 output base-64 characters:
   unsigned i;
   for( i = 0; i < numOrig24BitValues; ++i ) {
      tempResult4byte[0] = base64Char[( orig[3 * i] >> 2 ) & 0x3F];
      tempResult4byte[1] = base64Char[( ( ( orig[3 * i] & 0x3 ) << 4 ) | ( orig[3 * i + 1] >> 4 ) ) & 0x3F];
      tempResult4byte[2] = base64Char[( ( orig[3 * i + 1] << 2 ) | ( orig[3 * i + 2] >> 6 ) ) & 0x3F];
      tempResult4byte[3] = base64Char[orig[3 * i + 2] & 0x3F];
      strResult += tempResult4byte;
   }

   // Now, take padding into account.  (Note: i == numOrig24BitValues)
   if( havePadding ) {
      tempResult4byte[0] = base64Char[( orig[3 * i] >> 2 ) & 0x3F];
      if( havePadding2 ) {
         tempResult4byte[1] = base64Char[( ( ( orig[3 * i] & 0x3 ) << 4 ) | ( orig[3 * i + 1] >> 4 ) ) & 0x3F];
         tempResult4byte[2] = base64Char[( orig[3 * i + 1] << 2 ) & 0x3F];
      } else {
         tempResult4byte[1] = base64Char[( ( orig[3 * i] & 0x3 ) << 4 ) & 0x3F];
         tempResult4byte[2] = '=';
      }
      tempResult4byte[3] = '=';
      strResult += tempResult4byte;
   }
}

//-----------------------------------------------------------------------------

BOOL Str2CBOOL( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( BOOL ), pVar, pDefVar, nVarNum ) ) return TRUE;
   BOOL* pBool  = (BOOL*)pVar;
   int nItemNum = int( str.size() );
   if( nItemNum > nVarNum )
      nItemNum = nVarNum;
   for( int i = 0; i < nItemNum; i++ ) {
      char c;
      c = str[i];
      if( c == 't' || c == 'T' )
         pBool[i] = TRUE;
      if( c == 'f' || c == 'F' )
         pBool[i] = FALSE;
   }
   return TRUE;
}

BOOL BOOL2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   int i;
   if( IsSameVar( sizeof( BOOL ), pVar, pDefVar, nVarNum ) ) return FALSE;
   BOOL* pBool = (BOOL*)pVar;
   for( i = 0; i < nVarNum; i++ ) {
      std::string strAppand;
      if( pBool[i] == TRUE )
         strAppand = "T";
      else
         strAppand = "F";
      str.append( strAppand );
   }

   return TRUE;
}

//-----------------------------------------------------------------------------

BOOL Str2CHAR( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( char ), pVar, pDefVar, nVarNum ) ) return TRUE;
   char* pChar = (char*)pVar;
   int nStrLen = (int)str.size();
   int nCopy   = nStrLen;
   if( nCopy > 0 ) {
      if( nVarNum == 1 )
         pChar[0] = str.c_str()[0];
      else {
         if( nCopy > nVarNum - 1 ) nCopy = nVarNum - 1; // jun : 오류로 때문에 기존 코드위치를 옮김.
         strncpy( pChar, str.c_str(), nCopy );
         pChar[nCopy] = 0;
      }
   } else {
      if( nVarNum > 0 ) pChar[0] = 0;
   }
   return TRUE;
}

BOOL CHAR2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( char ), pVar, pDefVar, nVarNum ) ) return FALSE;
   char* pChar = (char*)pVar;
   if( pChar[0] != 0 ) {
      if( nVarNum == 1 ) {
         char szIntStr[32];
         sprintf( szIntStr, "%c", pChar[0] );
         str.append( szIntStr );
      } else {
         str = pChar;
      }
   } else {
      str = "";
   }
   return TRUE;
}

//-----------------------------------------------------------------------------

BOOL Str2ICHAR( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( char ), pVar, pDefVar, nVarNum ) ) return TRUE;
   char* pChar = (char*)pVar;
   char* token;
   char seps[] = ",; ";
   token       = strtok( (char*)str.c_str(), seps );
   for( int i = 0; i < nVarNum && token; i++ ) {
      pChar[i] = (char)atoi( token );
      token    = strtok( NULL, seps );
   }

   return TRUE;
}

BOOL ICHAR2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( char ), pVar, pDefVar, nVarNum ) ) return FALSE;
   char* pChar = (char*)pVar;
   char szIntStr[32];
   for( int i = 0; i < nVarNum; i++ ) {
      sprintf( szIntStr, "%d", pChar[i] );
      str.append( szIntStr );
      if( nVarNum > 1 && i < ( nVarNum - 1 ) )
         str.append( " " );
   }
   return TRUE;
}

//-----------------------------------------------------------------------------

BOOL Str2BYTE( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( BYTE ), pVar, pDefVar, nVarNum ) ) return TRUE;
   BYTE* pByte = (BYTE*)pVar;
   char* token;
   char seps[] = ",; ";
   token       = strtok( (char*)str.c_str(), seps );
   for( int i = 0; i < nVarNum && token; i++ ) {
      pByte[i] = atoi( token );
      token    = strtok( NULL, seps );
   }
   return TRUE;
}

BOOL BYTE2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( BYTE ), pVar, pDefVar, nVarNum ) ) return FALSE;
   BYTE* pByte = (BYTE*)pVar;
   char szIntStr[64];
   for( int i = 0; i < nVarNum; i++ ) {
      sprintf( szIntStr, "%d", pByte[i] );
      str.append( szIntStr );
      if( nVarNum > 1 && i < ( nVarNum - 1 ) )
         str.append( " " );
   }
   return TRUE;
}

//-----------------------------------------------------------------------------

BOOL Str2XBYTE( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( BYTE ), pVar, pDefVar, nVarNum ) ) return TRUE;
   BYTE* pByte      = (BYTE*)pVar;
   const char* pStr = str.c_str();
   int nByteLen     = (int)str.length() / 2;
   int i;
   // jun : 변환과정에서의 속도향상을 위해 sscanf를 사용하지 않았음.
   for( i = 0; i < nByteLen; i++ ) {
      const char& high_char = pStr[i * 2];
      const char& low_char  = pStr[i * 2 + 1];
      int nHigh             = high_char - '0';
      if( nHigh > 9 ) nHigh = high_char - 'A' + 10;
      if( nHigh > 15 ) nHigh = high_char - 'a' + 10;
      int nLow = low_char - '0';
      if( nLow > 9 ) nLow = low_char - 'A' + 10;
      if( nLow > 15 ) nLow = low_char - 'a' + 10;
      pByte[i] = ( nHigh << 4 ) + nLow;
   }
   return TRUE;
}

BOOL XBYTE2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( BYTE ), pVar, pDefVar, nVarNum ) ) return FALSE;
   BYTE* pByte = (BYTE*)pVar;
   char szIntStr[64];
   for( int i = 0; i < nVarNum; i++ ) {
      sprintf( szIntStr, "%02X", pByte[i] );
      str.append( szIntStr );
   }
   return TRUE;
}

//-----------------------------------------------------------------------------

BOOL Str2b64byte( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( BYTE ), pVar, pDefVar, nVarNum ) ) return TRUE;
   byte* pByte              = (byte*)pVar;
   const char* pStr         = str.c_str();
   unsigned int nResultSize = 0;
   base64Decode( pStr, nResultSize, true, pByte );

   return TRUE;
}

BOOL b64byte2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( BYTE ), pVar, pDefVar, nVarNum ) ) return FALSE;
   BYTE* pByte = (BYTE*)pVar;

   base64Encode( (char*)pByte, nVarNum, str );
   return TRUE;
}

//-----------------------------------------------------------------------------

BOOL Str2Short( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( short ), pVar, pDefVar, nVarNum ) ) return TRUE;
   short* pShort = (short*)pVar;
   char* token;
   char seps[] = ",; ";
   token       = strtok( (char*)str.c_str(), seps );
   for( int i = 0; i < nVarNum && token; i++ ) {
      sscanf( token, "%hi", &pShort[i] );
      token = strtok( NULL, seps );
   }
   return TRUE;
}

BOOL Short2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( short ), pVar, pDefVar, nVarNum ) ) return FALSE;
   short* pShort = (short*)pVar;
   char szWordStr[64];
   for( int i = 0; i < nVarNum; i++ ) {
      sprintf( szWordStr, "%hi", pShort[i] );
      str.append( szWordStr );
      if( nVarNum > 1 || i < ( nVarNum - 1 ) )
         str.append( " " );
   }
   return TRUE;
}

//-----------------------------------------------------------------------------

const int nUShortBitNum = sizeof( unsigned short ) * 8;

BOOL Str2UShort( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( unsigned short ), pVar, pDefVar, nVarNum ) ) return TRUE;
   unsigned short* pUShort = (unsigned short*)pVar;
   char* token;
   char seps[] = ",; ";
   token       = strtok( (char*)str.c_str(), seps );
   for( int i = 0; i < nVarNum && token; i++ ) {
      sscanf( token, "%hu", &pUShort[i] );
      token = strtok( NULL, seps );
   }
   return TRUE;
}

BOOL UShort2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( unsigned short ), pVar, pDefVar, nVarNum ) ) return FALSE;
   unsigned short* pUShort = (unsigned short*)pVar;
   char szUShortStr[64];
   for( int i = 0; i < nVarNum; i++ ) {
      sprintf( szUShortStr, "%hu", pUShort[i] );
      str.append( szUShortStr );
      if( nVarNum > 1 || i < ( nVarNum - 1 ) )
         str.append( " " );
   }
   return TRUE;
}

//---------------------------------------------------------------------------

BOOL Str2XShort( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( unsigned short ), pVar, pDefVar, nVarNum ) ) return TRUE;
   unsigned short* pUShort = (unsigned short*)pVar;
   char* token;
   char seps[] = ",; ";
   token       = strtok( (char*)str.c_str(), seps );
   for( int i = 0; i < nVarNum && token; i++ ) {
      sscanf( token, "%hx", &pUShort[i] );
      token = strtok( NULL, seps );
   }
   return TRUE;
}

BOOL XShort2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( unsigned short ), pVar, pDefVar, nVarNum ) ) return FALSE;
   unsigned short* pUShort = (unsigned short*)pVar;
   char szUShortStr[64];
   for( int i = 0; i < nVarNum; i++ ) {
      sprintf( szUShortStr, "%hx", pUShort[i] );
      str.append( szUShortStr );
      if( nVarNum > 1 || i < ( nVarNum - 1 ) )
         str.append( " " );
   }
   return TRUE;
}

//-----------------------------------------------------------------------------

BOOL Str2Int( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( int ), pVar, pDefVar, nVarNum ) ) return TRUE;
   int* pInt = (int*)pVar;
   char* token;
   char seps[] = ",; ";
   token       = strtok( (char*)str.c_str(), seps );
   for( int i = 0; i < nVarNum && token; i++ ) {
      sscanf( token, FORMAT_I32d, &pInt[i] );
      token = strtok( NULL, seps );
   }
   return TRUE;
}

BOOL Int2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( int ), pVar, pDefVar, nVarNum ) ) return FALSE;
   int* pInt = (int*)pVar;
   char szIntStr[64];
   for( int i = 0; i < nVarNum; i++ ) {
      sprintf( szIntStr, FORMAT_I32d, pInt[i] );
      str.append( szIntStr );
      if( nVarNum > 1 || i < ( nVarNum - 1 ) )
         str.append( " " );
   }
   return TRUE;
}

//-----------------------------------------------------------------------------

BOOL Str2UInt( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( uint ), pVar, pDefVar, nVarNum ) ) return TRUE;
   uint* pInt = (uint*)pVar;
   char* token;
   char seps[] = ",; ";
   token       = strtok( (char*)str.c_str(), seps );
   for( int i = 0; i < nVarNum && token; i++ ) {
      sscanf( token, FORMAT_I32u, &pInt[i] );
      token = strtok( NULL, seps );
   }
   return TRUE;
}

BOOL UInt2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( uint ), pVar, pDefVar, nVarNum ) ) return FALSE;
   uint* pInt = (uint*)pVar;
   char szIntStr[64];
   for( int i = 0; i < nVarNum; i++ ) {
      sprintf( szIntStr, FORMAT_I32u, pInt[i] );
      str.append( szIntStr );
      if( nVarNum > 1 || i < ( nVarNum - 1 ) )
         str.append( " " );
   }
   return TRUE;
}

//-----------------------------------------------------------------------------

BOOL Str2XInt( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( uint ), pVar, pDefVar, nVarNum ) ) return TRUE;
   uint* pInt = (uint*)pVar;
   char* token;
   char seps[] = ",; ";
   token       = strtok( (char*)str.c_str(), seps );
   for( int i = 0; i < nVarNum && token; i++ ) {
      sscanf( token, FORMAT_I32x, &pInt[i] );
      token = strtok( NULL, seps );
   }
   return TRUE;
}

BOOL XInt2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( uint ), pVar, pDefVar, nVarNum ) ) return FALSE;
   uint* pInt = (uint*)pVar;
   char szIntStr[64];
   for( int i = 0; i < nVarNum; i++ ) {
      sprintf( szIntStr, FORMAT_I32x, pInt[i] );
      str.append( szIntStr );
      if( nVarNum > 1 || i < ( nVarNum - 1 ) )
         str.append( " " );
   }
   return TRUE;
}

//-----------------------------------------------------------------------------

BOOL Str2LONG( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( long ), pVar, pDefVar, nVarNum ) ) return TRUE;
   long* pInt = (long*)pVar;
   char* token;
   char seps[] = ",; ";
   token       = strtok( (char*)str.c_str(), seps );
   for( int i = 0; i < nVarNum && token; i++ ) {
      sscanf( token, "%ld", &pInt[i] );
      token = strtok( NULL, seps );
   }
   return TRUE;
}

BOOL LONG2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( long ), pVar, pDefVar, nVarNum ) ) return FALSE;
   long* pInt = (long*)pVar;
   char szIntStr[64];
   for( int i = 0; i < nVarNum; i++ ) {
      sprintf( szIntStr, "%ld", pInt[i] );
      str.append( szIntStr );
      if( nVarNum > 1 || i < ( nVarNum - 1 ) )
         str.append( " " );
   }
   return TRUE;
}

//-----------------------------------------------------------------------------

BOOL Str2ULONG( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( ULONG ), pVar, pDefVar, nVarNum ) ) return TRUE;
   ULONG* pInt = (ULONG*)pVar;
   char* token;
   char seps[] = ",; ";
   token       = strtok( (char*)str.c_str(), seps );
   for( int i = 0; i < nVarNum && token; i++ ) {
      sscanf( token, "%lu", &pInt[i] );
      token = strtok( NULL, seps );
   }
   return TRUE;
}

BOOL ULONG2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( ULONG ), pVar, pDefVar, nVarNum ) ) return FALSE;
   ULONG* pInt = (ULONG*)pVar;
   char szIntStr[64];
   for( int i = 0; i < nVarNum; i++ ) {
      sprintf( szIntStr, "%lu", pInt[i] );
      str.append( szIntStr );
      if( nVarNum > 1 || i < ( nVarNum - 1 ) )
         str.append( " " );
   }
   return TRUE;
}

//------------------------------------------------------------------------

BOOL Str2XLONG( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( ULONG ), pVar, pDefVar, nVarNum ) ) return TRUE;
   ULONG* pInt = (ULONG*)pVar;
   char* token;
   char seps[] = ",; ";
   token       = strtok( (char*)str.c_str(), seps );
   for( int i = 0; i < nVarNum && token; i++ ) {
      sscanf( token, "%lx", &pInt[i] );
      token = strtok( NULL, seps );
   }
   return TRUE;
}

BOOL XLONG2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( ULONG ), pVar, pDefVar, nVarNum ) ) return FALSE;
   ULONG* pInt = (ULONG*)pVar;
   char szIntStr[64];
   for( int i = 0; i < nVarNum; i++ ) {
      sprintf( szIntStr, "%lx", pInt[i] );
      str.append( szIntStr );
      if( nVarNum > 1 || i < ( nVarNum - 1 ) )
         str.append( " " );
   }
   return TRUE;
}

//------------------------------------------------------------------------

BOOL Str2Int64( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( INT64 ), pVar, pDefVar, nVarNum ) ) return TRUE;
   INT64* pInt = (INT64*)pVar;
   char* token;
   char seps[] = ",; ";
   token       = strtok( (char*)str.c_str(), seps );
   for( int i = 0; i < nVarNum && token; i++ ) {
      sscanf( token, FORMAT_I64d, &pInt[i] );
      token = strtok( NULL, seps );
   }
   return TRUE;
}

BOOL Int642Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( INT64 ), pVar, pDefVar, nVarNum ) ) return FALSE;
   INT64* pInt = (INT64*)pVar;
   char szIntStr[64];
   for( int i = 0; i < nVarNum; i++ ) {
      sprintf( szIntStr, FORMAT_I64d, pInt[i] );
      str.append( szIntStr );
      if( nVarNum > 1 || i < ( nVarNum - 1 ) )
         str.append( " " );
   }
   return TRUE;
}

//---------------------------------------------------------------------------

BOOL Str2UInt64( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( UINT64 ), pVar, pDefVar, nVarNum ) ) return TRUE;
   UINT64* pInt = (UINT64*)pVar;
   char* token;
   char seps[] = ",; ";
   token       = strtok( (char*)str.c_str(), seps );
   for( int i = 0; i < nVarNum && token; i++ ) {
      sscanf( token, FORMAT_I64u, &pInt[i] );
      token = strtok( NULL, seps );
   }
   return TRUE;
}

BOOL UInt642Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( UINT64 ), pVar, pDefVar, nVarNum ) ) return FALSE;
   UINT64* pInt = (UINT64*)pVar;
   char szIntStr[64];
   for( int i = 0; i < nVarNum; i++ ) {
      sprintf( szIntStr, FORMAT_I64u, pInt[i] );
      str.append( szIntStr );
      if( nVarNum > 1 || i < ( nVarNum - 1 ) )
         str.append( " " );
   }
   return TRUE;
}

//-----------------------------------------------------------------------------

BOOL Str2XInt64( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( UINT64 ), pVar, pDefVar, nVarNum ) ) return TRUE;
   UINT64* pInt = (UINT64*)pVar;
   char* token;
   char seps[] = ",; ";
   token       = strtok( (char*)str.c_str(), seps );
   for( int i = 0; i < nVarNum && token; i++ ) {
      sscanf( token, FORMAT_I64x, &pInt[i] );
      token = strtok( NULL, seps );
   }
   return TRUE;
}

BOOL XInt642Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( UINT64 ), pVar, pDefVar, nVarNum ) ) return FALSE;
   UINT64* pInt = (UINT64*)pVar;
   char szIntStr[64];
   for( int i = 0; i < nVarNum; i++ ) {
      sprintf( szIntStr, FORMAT_I64x, pInt[i] );
      str.append( szIntStr );
      if( nVarNum > 1 || i < ( nVarNum - 1 ) )
         str.append( " " );
   }
   return TRUE;
}

//-----------------------------------------------------------------------------

BOOL Str2Float( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( float ), pVar, pDefVar, nVarNum ) ) return TRUE;
   float* pFloat = (float*)pVar;
   char* token;
   char seps[] = ",; ";
   token       = strtok( (char*)str.c_str(), seps );
   for( int i = 0; i < nVarNum && token; i++ ) {
      int ret = sscanf( token, "%G", &pFloat[i] );
      if( ret == EOF || ret != 1 )
         break;
      token = strtok( NULL, seps );
   }

   return TRUE;
}

BOOL Float2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( float ), pVar, pDefVar, nVarNum ) ) return FALSE;
   float* pFloat = (float*)pVar;
   char szFloatStr[64];
   for( int i = 0; i < nVarNum; i++ ) {
      if( szFormat )
         sprintf( szFloatStr, szFormat, pFloat[i] );
      else
         sprintf( szFloatStr, "%.3f", pFloat[i] );
      std::string _str = szFloatStr;
      str.append( szFloatStr );
      if( nVarNum > 1 || i < ( nVarNum - 1 ) )
         str.append( " " );
   }
   return TRUE;
}

//-----------------------------------------------------------------------------

BOOL Str2Double( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( double ), pVar, pDefVar, nVarNum ) ) return TRUE;
   double* pDouble = (double*)pVar;
   char* token;
   char seps[] = ",; ";
   token       = strtok( (char*)str.c_str(), seps );
   for( int i = 0; i < nVarNum && token; i++ ) {
      pDouble[i] = (double)atof( token );
      token      = strtok( NULL, seps );
   }

   return TRUE;
}

BOOL Double2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( double ), pVar, pDefVar, nVarNum ) ) return FALSE;
   double* pDouble = (double*)pVar;
   char szDoubleStr[64];
   for( int i = 0; i < nVarNum; i++ ) {
      if( szFormat )
         sprintf( szDoubleStr, szFormat, pDouble[i] );
      else
         sprintf( szDoubleStr, "%.5f", pDouble[i] );
      str.append( szDoubleStr );
      if( nVarNum > 1 || i < ( nVarNum - 1 ) )
         str.append( " " );
   }
   return TRUE;
}

//-----------------------------------------------------------------------------

BOOL Str2string( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   int i;

   if( pDefVar ) {
      std::string* pDefString = (std::string*)pDefVar;
      std::string* pString    = (std::string*)pVar;
      for( i = 0; i < nVarNum; i++ ) {
         pDefString[i] = pString[i];
      }
      return TRUE;
   }
   std::string* pString = (std::string*)pVar;
   char* token;
   char seps[] = " ";
   token       = strtok( (char*)str.c_str(), seps );
   for( i = 0; i < nVarNum && token; i++ ) {
      pString[i] = token;
      token      = strtok( NULL, seps );
   }

   return TRUE;
}
BOOL string2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   std::string* pString = (std::string*)pVar;
   char szStr[512];
   for( int i = 0; i < nVarNum; i++ ) {
      sprintf( szStr, "%s", pString[i].c_str() );
      str.append( szStr );
      if( nVarNum > 1 || i < ( nVarNum - 1 ) )
         str.append( " " );
   }

   return TRUE;
}

//-----------------------------------------------------------------------------

BOOL Str2StringA( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( pDefVar ) {
      StringA& s_string_def = *( (StringA*)pDefVar );
      StringA& s_string     = *( (StringA*)pVar );
      s_string              = s_string_def;
   } else {
      StringA& s_string = *( (StringA*)pVar );
      s_string          = str.c_str();
   }
   return ( TRUE );
}

BOOL StringA2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   StringA* pString = (StringA*)pVar;
   if( pDefVar ) {
      if( nVarNum == 1 ) {
         StringA* pStringDef = (StringA*)pDefVar;
         if( *pString == *pStringDef ) return ( FALSE );
      }
   }
   str = (char*)( *pString );
   return ( TRUE );
}

//-----------------------------------------------------------------------------

BOOL Str2FPoint2D( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( FPoint2D ), pVar, pDefVar, nVarNum ) ) return TRUE;
   FPoint2D* pFPoint2D = (FPoint2D*)pVar;
   char* token;
   char seps[] = " ";
   token       = strtok( (char*)str.c_str(), seps );
   for( int i = 0; i < nVarNum && token; i++ ) {
      FPoint2D& pnt2d = pFPoint2D[i];
      int ret         = sscanf( token, "(%G,%G)", &pnt2d.X, &pnt2d.Y );
      if( ret == EOF || ret != 2 )
         break;
      token = strtok( NULL, seps );
   }

   return TRUE;
}

BOOL FPoint2D2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( float ), pVar, pDefVar, nVarNum * 2 ) ) return FALSE;
   FPoint2D* pFPoint2D = (FPoint2D*)pVar;
   char szBuffer[256];
   for( int i = 0; i < nVarNum; i++ ) {
      FPoint2D& pnt2d = pFPoint2D[i];
      sprintf( szBuffer, "(%G,%G)", pnt2d.X, pnt2d.Y );
      str.append( szBuffer );
      if( nVarNum > 1 && i < ( nVarNum - 1 ) )
         str.append( " " );
   }
   return TRUE;
}

//-----------------------------------------------------------------------------

BOOL Str2FPoint3D( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( FPoint3D ), pVar, pDefVar, nVarNum ) ) return TRUE;
   FPoint3D* pFPoint3D = (FPoint3D*)pVar;
   char* token;
   char seps[] = " ";
   token       = strtok( (char*)str.c_str(), seps );
   for( int i = 0; i < nVarNum && token; i++ ) {
      FPoint3D& pnt3d = pFPoint3D[i];
      int ret         = sscanf( token, "(%G,%G,%G)", &pnt3d.X, &pnt3d.Y, &pnt3d.Z );
      if( ret == EOF || ret != 3 )
         break;
      token = strtok( NULL, seps );
   }

   return TRUE;
}

BOOL FPoint3D2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( float ), pVar, pDefVar, nVarNum * 3 ) ) return FALSE;
   FPoint3D* pFPoint3D = (FPoint3D*)pVar;
   char szBuffer[256];
   for( int i = 0; i < nVarNum; i++ ) {
      FPoint3D& pnt3d = pFPoint3D[i];
      sprintf( szBuffer, "(%G,%G,%G)", pnt3d.X, pnt3d.Y, pnt3d.Z );
      str.append( szBuffer );
      if( nVarNum > 1 && i < ( nVarNum - 1 ) )
         str.append( " " );
   }
   return TRUE;
}

//-----------------------------------------------------------------------------

BOOL Str2ISize2D( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( ISize2D ), pVar, pDefVar, nVarNum ) ) return TRUE;
   ISize2D* pISize2D = (ISize2D*)pVar;
   char* token;
   char seps[] = " ";
   token       = strtok( (char*)str.c_str(), seps );
   for( int i = 0; i < nVarNum && token; i++ ) {
      ISize2D& size2d = pISize2D[i];
      int ret         = sscanf( token, FORMAT_I32dx2, &size2d.Width, &size2d.Height );
      if( ret == EOF || ret != 2 )
         break;
      token = strtok( NULL, seps );
   }

   return TRUE;
}

BOOL ISize2D2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( int ), pVar, pDefVar, nVarNum * 2 ) ) return FALSE;
   ISize2D* pISize2D = (ISize2D*)pVar;
   char szBuffer[256];
   for( int i = 0; i < nVarNum; i++ ) {
      ISize2D& size2d = pISize2D[i];
      sprintf( szBuffer, FORMAT_I32dx2, size2d.Width, size2d.Height );
      str.append( szBuffer );
      if( nVarNum > 1 && i < ( nVarNum - 1 ) )
         str.append( " " );
   }
   return TRUE;
}

//-----------------------------------------------------------------------------

BOOL Str2ILine2D( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( ILine2D ), pVar, pDefVar, nVarNum ) ) return TRUE;
   ILine2D* pILine2D = (ILine2D*)pVar;
   char* token;
   char seps[] = " ";
   token       = strtok( (char*)str.c_str(), seps );
   for( int i = 0; i < nVarNum && token; i++ ) {
      ILine2D& line = pILine2D[i];
      int ret       = sscanf( token, FORMAT_I32dx4, &line[0].X, &line[0].Y, &line[1].X, &line[1].Y );
      if( ret == EOF || ret != 4 )
         break;
      token = strtok( NULL, seps );
   }
   return TRUE;
}

BOOL ILine2D2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( int ), pVar, pDefVar, nVarNum * 4 ) ) return FALSE;
   ILine2D* pILine2D = (ILine2D*)pVar;
   char szBuffer[256];
   for( int i = 0; i < nVarNum; i++ ) {
      ILine2D& line = pILine2D[i];
      sprintf( szBuffer, FORMAT_I32dx4, line[0].X, line[0].Y, line[1].X, line[1].Y );
      str.append( szBuffer );
      if( nVarNum > 1 && i < ( nVarNum - 1 ) )
         str.append( " " );
   }
   return TRUE;
}

//-----------------------------------------------------------------------------

BOOL Str2IPoint2D( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( IPoint2D ), pVar, pDefVar, nVarNum ) ) return TRUE;
   IPoint2D* pIPoint2D = (IPoint2D*)pVar;
   char* token;
   char seps[] = " ";
   token       = strtok( (char*)str.c_str(), seps );
   for( int i = 0; i < nVarNum && token; i++ ) {
      IPoint2D& point = pIPoint2D[i];
      int ret         = sscanf( token, FORMAT_I32dx2, &point.X, &point.Y );
      if( ret == EOF || ret != 2 )
         break;
      token = strtok( NULL, seps );
   }
   return TRUE;
}

BOOL IPoint2D2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( int ), pVar, pDefVar, nVarNum * 2 ) ) return FALSE;
   IPoint2D* pIPoint2D = (IPoint2D*)pVar;
   char szBuffer[256];
   for( int i = 0; i < nVarNum; i++ ) {
      IPoint2D& point = pIPoint2D[i];
      sprintf( szBuffer, FORMAT_I32dx2, point.X, point.Y );
      str.append( szBuffer );
      if( nVarNum > 1 && i < ( nVarNum - 1 ) )
         str.append( " " );
   }
   return TRUE;
}

//-----------------------------------------------------------------------------

BOOL Str2IBox2D( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( IBox2D ), pVar, pDefVar, nVarNum ) ) return TRUE;
   IBox2D* pIBox2D = (IBox2D*)pVar;
   char* token;
   char seps[] = " ";
   token       = strtok( (char*)str.c_str(), seps );
   for( int i = 0; i < nVarNum && token; i++ ) {
      IBox2D& box = pIBox2D[i];
      int ret     = sscanf( token, FORMAT_I32dx4, &box.X, &box.Y, &box.Width, &box.Height );
      if( ret == EOF || ret != 4 )
         break;
      token = strtok( NULL, seps );
   }
   return TRUE;
}

BOOL IBox2D2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( int ), pVar, pDefVar, nVarNum * 4 ) ) return FALSE;
   IBox2D* pIBox2D = (IBox2D*)pVar;
   char szBuffer[256];
   for( int i = 0; i < nVarNum; i++ ) {
      IBox2D& box = pIBox2D[i];
      sprintf( szBuffer, FORMAT_I32dx4, box.X, box.Y, box.Width, box.Height );
      str.append( szBuffer );
      if( nVarNum > 1 && i < ( nVarNum - 1 ) )
         str.append( " " );
   }
   return TRUE;
}

//-----------------------------------------------------------------------------

BOOL Str2FBox2D( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( FBox2D ), pVar, pDefVar, nVarNum ) ) return TRUE;
   FBox2D* pFBox2D = (FBox2D*)pVar;
   char* token;
   char seps[] = " ";
   token       = strtok( (char*)str.c_str(), seps );
   for( int i = 0; i < nVarNum && token; i++ ) {
      FBox2D& box = pFBox2D[i];
      int ret     = sscanf( token, "(%G,%G,%G,%G)", &box.X, &box.Y, &box.Width, &box.Height );
      if( ret == EOF || ret != 4 ) break;
      token = strtok( NULL, seps );
   }
   return TRUE;
}

BOOL FBox2D2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( int ), pVar, pDefVar, nVarNum * 4 ) ) return FALSE;
   FBox2D* pFBox2D = (FBox2D*)pVar;
   char szBuffer[256];
   for( int i = 0; i < nVarNum; i++ ) {
      FBox2D& box = pFBox2D[i];
      sprintf( szBuffer, "(%.3f,%.3f,%.3f,%.3f)", box.X, box.Y, box.Width, box.Height );
      str.append( szBuffer );
      if( nVarNum > 1 && i < ( nVarNum - 1 ) )
         str.append( " " );
   }
   return TRUE;
}

//-----------------------------------------------------------------------------

BOOL Str2SYSTEMTIME( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( SYSTEMTIME ), pVar, pDefVar, nVarNum ) ) return TRUE;
   SYSTEMTIME* pSYSTEMTIME = (SYSTEMTIME*)pVar;
   char* token;
   char seps[] = ",; ";
   token       = strtok( (char*)str.c_str(), seps );
   for( int i = 0; i < nVarNum && token; i++ ) {
      //                      012345678901234567890
      // Time String Format : 2005_0105_101010_0000
      char szYear[8];
      char szMonth[4];
      char szDay[4];
      char szHour[4];
      char szMin[4];
      char szSec[4];
      char szMilliSec[8];
      strncpy( szYear, token, 4 );
      szYear[4] = 0;
      strncpy( szMonth, token + 5, 2 );
      szMonth[2] = 0;
      strncpy( szDay, token + 7, 2 );
      szDay[2] = 0;
      strncpy( szHour, token + 10, 2 );
      szHour[2] = 0;
      strncpy( szMin, token + 12, 2 );
      szMin[2] = 0;
      strncpy( szSec, token + 14, 2 );
      szSec[2] = 0;
      strncpy( szMilliSec, token + 17, 4 );
      szMilliSec[4] = 0;
      SYSTEMTIME st;
      st.wYear         = atoi( szYear );
      st.wMonth        = atoi( szMonth );
      st.wDay          = atoi( szDay );
      st.wHour         = atoi( szHour );
      st.wMinute       = atoi( szMin );
      st.wSecond       = atoi( szSec );
      st.wMilliseconds = atoi( szMilliSec );
      if( st.wDay >= 1 && st.wDay <= 31 && st.wMonth >= 1 && st.wMonth <= 12 && st.wYear >= 1900 ) {
         CTime tm( st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond );
         st.wDayOfWeek  = tm.GetDayOfWeek();
         pSYSTEMTIME[i] = st;
      }
      token = strtok( NULL, seps );
   }
   return TRUE;
}

BOOL SYSTEMTIME2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( uint ), pVar, pDefVar, nVarNum * 4 ) ) return FALSE;
   SYSTEMTIME* pSYSTEMTIME = (SYSTEMTIME*)pVar;
   char szStr[512];
   for( int i = 0; i < nVarNum; i++ ) {
      SYSTEMTIME& tm = pSYSTEMTIME[i];
      sprintf( szStr, "%04d_%02d%02d_%02d%02d%02d_%04d", tm.wYear, tm.wMonth, tm.wDay, tm.wHour, tm.wMinute, tm.wSecond, tm.wMilliseconds );
      str.append( szStr );
      if( nVarNum > 1 && i < ( nVarNum - 1 ) )
         str.append( " " );
   }
   return TRUE;
}

//-----------------------------------------------------------------------------

BOOL Str2FILETIME( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
#if defined( __WIN32 )
{
   if( CopyFromDefaultValue( sizeof( FILETIME ), pVar, pDefVar, nVarNum ) ) return TRUE;
   FILETIME* pFILETIME     = (FILETIME*)pVar;
   SYSTEMTIME* pSystemTime = new SYSTEMTIME[nVarNum];
   Str2SYSTEMTIME( str, pSystemTime, pDefVar, szFormat, nVarNum );
   for( int i = 0; i < nVarNum; i++ ) {
      SystemTimeToFileTime( &pSystemTime[i], &pFILETIME[i] );
   }
   delete[] pSystemTime;
   return TRUE;
}
#else
{
   if( CopyFromDefaultValue( sizeof( FILETIME ), pVar, pDefVar, nVarNum ) ) return TRUE;
   FILETIME* pFILETIME = (FILETIME*)pVar;
   char* token;
   char seps[] = ( ",; " );
   token       = strtok( (char*)str.c_str(), seps );
   for( int i = 0; i < nVarNum && token; i++ ) {
      //                      012345678901234567890
      // Time String Format : 2005_0105_101010_0000
      char szYear[8];
      char szMonth[4];
      char szDay[4];
      char szHour[4];
      char szMin[4];
      char szSec[4];
      char szMilliSec[8];
      strncpy( szYear, token, 4 );
      szYear[4] = 0;
      strncpy( szMonth, token + 5, 2 );
      szMonth[2] = 0;
      strncpy( szDay, token + 7, 2 );
      szDay[2] = 0;
      strncpy( szHour, token + 10, 2 );
      szHour[2] = 0;
      strncpy( szMin, token + 12, 2 );
      szMin[2] = 0;
      strncpy( szSec, token + 14, 2 );
      szSec[2] = 0;
      strncpy( szMilliSec, token + 17, 4 );
      szMilliSec[4] = 0;
      
      // yhpark bugfix time converting.
      BCCL::Time bccl_time;
      bccl_time.Year        = _ttoi( szYear );
      bccl_time.Month       = _ttoi( szMonth );
      bccl_time.Day         = _ttoi( szDay );
      bccl_time.Hour        = _ttoi( szHour );
      bccl_time.Minute      = _ttoi( szMin );
      bccl_time.Second      = _ttoi( szSec );
      bccl_time.Millisecond = _ttoi( szMilliSec );

      BCCL::Convert( bccl_time, *pFILETIME );
   }
   return TRUE;
}
#endif

BOOL FILETIME2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( FILETIME ), pVar, pDefVar, nVarNum ) ) return FALSE;
   FILETIME* pFILETIME     = (FILETIME*)pVar;
   SYSTEMTIME* pSystemTime = new SYSTEMTIME[nVarNum];
   for( int i = 0; i < nVarNum; i++ ) {
#if defined( __WIN32 )
      FileTimeToSystemTime( &pFILETIME[i], &pSystemTime[i] );
#else
      BCCL::Convert( pFILETIME[i], pSystemTime[i] );
#endif
   }
   SYSTEMTIME2Str( str, pSystemTime, pDefVar, szFormat, nVarNum );
   delete[] pSystemTime;
   return TRUE;
}

//-----------------------------------------------------------------------------

BOOL Str2IPADDR( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( ULONG ), pVar, pDefVar, nVarNum ) ) return TRUE;
   ULONG* pIPAddress = (ULONG*)pVar;
   char* token;
   char seps[] = " ";
   token       = strtok( (char*)str.c_str(), seps );
   for( int i = 0; i < nVarNum && token; i++ ) {
      if( strlen( token ) > 0 ) {
         pIPAddress[i] = inet_addr( token );
      }
      token = strtok( NULL, seps );
   }
   return TRUE;
}

BOOL IPADDR2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( ULONG ), pVar, pDefVar, nVarNum ) ) return FALSE;
   ULONG* pIPAddress = (ULONG*)pVar;
   char szStr[256];
   for( int i = 0; i < nVarNum; i++ ) {
      in_addr _nIPAddressIn;
      memcpy( &_nIPAddressIn, &pIPAddress[i], sizeof( unsigned long ) );
      char* pszAddress = inet_ntoa( _nIPAddressIn );
      if( pszAddress ) {
         sprintf( szStr, "%s", pszAddress );
         str.append( szStr );
      }
      if( nVarNum > 1 || i < ( nVarNum - 1 ) )
         str.append( " " );
   }

   return TRUE;
}

//-----------------------------------------------------------------------------

BOOL Str2CStringA( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( pDefVar ) {
      CStringA& cstring_def = *( (CStringA*)pDefVar );
      CStringA& cstring     = *( (CStringA*)pVar );
      cstring               = cstring_def;
      return TRUE;
   }
   CStringA& cstring = *( (CStringA*)pVar );
   cstring           = str.c_str();
   return TRUE;
}

BOOL CStringA2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   CStringA* pString = (CStringA*)pVar;
   if( pDefVar ) {
      if( nVarNum == 1 ) {
         CStringA* pStringDef = (CStringA*)pDefVar;
         if( *pString == *pStringDef ) return FALSE;
      }
   }
   str = pString->GetBuffer( 0 );
   return TRUE;
}

//-----------------------------------------------------------------------------

BOOL Str2CRect( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( CRect ), pVar, pDefVar, nVarNum ) ) return TRUE;
   CRect* pRect = (CRect*)pVar;
   char* token;
   char seps[] = " ";
   token       = strtok( (char*)str.c_str(), seps );
   for( int i = 0; i < nVarNum && token; i++ ) {
      CRect& rect = pRect[i];
      int ret     = sscanf( token, "%d,%d,%d,%d", (int*)&rect.left, (int*)&rect.top, (int*)&rect.right, (int*)&rect.bottom );
      if( ret == EOF || ret != 4 ) break;
      token = strtok( NULL, seps );
   }
   return TRUE;
}

BOOL CRect2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( LONG ), pVar, pDefVar, nVarNum * 4 ) ) return FALSE;
   CRect* pRect = (CRect*)pVar;
   char szBuffer[256];
   sprintf( szBuffer, "%d,%d,%d,%d", (int)pRect->left, (int)pRect->top, (int)pRect->right, (int)pRect->bottom );
   str = szBuffer;
   return TRUE;
}

//-----------------------------------------------------------------------------

BOOL Str2CSize( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( CSize ), pVar, pDefVar, nVarNum ) ) return TRUE;
   CSize* pSize = (CSize*)pVar;
   char* token;
   char seps[] = " ";
   token       = strtok( (char*)str.c_str(), seps );
   for( int i = 0; i < nVarNum && token; i++ ) {
      CSize& pnt = pSize[i];
      int ret    = sscanf( token, "(%d,%d)", (int*)&pnt.cx, (int*)&pnt.cy );
      if( ret == EOF || ret != 2 )
         break;
      token = strtok( NULL, seps );
   }
   return TRUE;
}

BOOL CSize2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( CSize ), pVar, pDefVar, nVarNum ) ) return FALSE;
   CSize* pSize = (CSize*)pVar;
   char szBuffer[256];
   for( int i = 0; i < nVarNum; i++ ) {
      CSize& size = pSize[i];
      sprintf( szBuffer, "(%d,%d)", (int)size.cx, (int)size.cy );
      str.append( szBuffer );
      if( nVarNum > 1 && i < ( nVarNum - 1 ) )
         str.append( " " );
   }
   return TRUE;
}

//-----------------------------------------------------------------------------

BOOL Str2CPoint( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( CPoint ), pVar, pDefVar, nVarNum ) ) return TRUE;
   CPoint* pPoint = (CPoint*)pVar;
   char* token;
   char seps[] = " ";
   token       = strtok( (char*)str.c_str(), seps );
   for( int i = 0; i < nVarNum && token; i++ ) {
      CPoint& pnt = pPoint[i];
      int ret     = sscanf( token, "(%d,%d)", (int*)&pnt.x, (int*)&pnt.y );
      if( ret == EOF || ret != 2 )
         break;
      token = strtok( NULL, seps );
   }
   return TRUE;
}

BOOL CPoint2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( CPoint ), pVar, pDefVar, nVarNum ) ) return FALSE;
   CPoint* pPoint = (CPoint*)pVar;
   char szBuffer[256];
   for( int i = 0; i < nVarNum; i++ ) {
      CPoint& point = pPoint[i];
      sprintf( szBuffer, "(%d,%d)", (int)point.x, (int)point.y );
      str.append( szBuffer );
      if( nVarNum > 1 && i < ( nVarNum - 1 ) )
         str.append( " " );
   }
   return TRUE;
}

//-----------------------------------------------------------------------------

BOOL Str2CTime( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( CTime ), pVar, pDefVar, nVarNum ) ) return TRUE;
   CTime* pCTime = (CTime*)pVar;
   char* token;
   char seps[] = ",; ";
   token       = strtok( (char*)str.c_str(), seps );
   for( int i = 0; i < nVarNum && token; i++ ) {
      //                      0123456789012345
      // Time String Format : 2005_0105_101010
      char szYear[8];
      char szMonth[4];
      char szDay[4];
      char szHour[4];
      char szMin[4];
      char szSec[4];
      strncpy( szYear, token, 4 );
      szYear[4] = 0;
      strncpy( szMonth, token + 5, 2 );
      szMonth[2] = 0;
      strncpy( szDay, token + 7, 2 );
      szDay[2] = 0;
      strncpy( szHour, token + 10, 2 );
      szHour[2] = 0;
      strncpy( szMin, token + 12, 2 );
      szMin[2] = 0;
      strncpy( szSec, token + 14, 2 );
      szSec[2] = 0;
      SYSTEMTIME st;
      st.wYear   = atoi( szYear );
      st.wMonth  = atoi( szMonth );
      st.wDay    = atoi( szDay );
      st.wHour   = atoi( szHour );
      st.wMinute = atoi( szMin );
      st.wSecond = atoi( szSec );

      if( st.wDay >= 1 && st.wDay <= 31 && st.wMonth >= 1 && st.wMonth <= 12 && st.wYear > 1970 ) {
         CTime tt  = CTime( (int)st.wYear, (int)st.wMonth, (int)st.wDay, (int)st.wHour, (int)st.wMinute, (int)st.wSecond, -1 );
         pCTime[i] = tt;
      }
      token = strtok( NULL, seps );
   }
   return TRUE;
}

BOOL CTime2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( CTime ), pVar, pDefVar, nVarNum ) ) return FALSE;
   CTime* pCTime = (CTime*)pVar;
   char szStr[512];
   for( int i = 0; i < nVarNum; i++ ) {
      CTime& tm = pCTime[i];
      sprintf( szStr, "%04d_%02d%02d_%02d%02d%02d", tm.GetYear(), tm.GetMonth(), tm.GetDay(), tm.GetHour(), tm.GetMinute(), tm.GetSecond() );
      str.append( szStr );
      if( nVarNum > 1 && i < ( nVarNum - 1 ) )
         str.append( " " );
   }
   return TRUE;
}

//-----------------------------------------------------------------------------

BOOL Str2BGRColor( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( CopyFromDefaultValue( sizeof( BGRColor ), pVar, pDefVar, nVarNum ) ) return TRUE;
   BGRColor* pBGRColor = (BGRColor*)pVar;
   char* token;
   char seps[] = " ";
   token       = strtok( (char*)str.c_str(), seps );
   for( int i = 0; i < nVarNum && token; i++ ) {
      int b = 0, g = 0, r = 0;
      int ret = sscanf( token, FORMAT_I32dx3, &b, &g, &r );
      pBGRColor[i]( (byte)b, (byte)g, (byte)r );
      if( ret == EOF || ret != 3 ) break;
      token = strtok( NULL, seps );
   }

   return TRUE;
}

BOOL BGRColor2Str( std::string& str, void* pVar, void* pDefVar, char* szFormat, int nVarNum )
{
   if( IsSameVar( sizeof( float ), pVar, pDefVar, nVarNum * 3 ) ) return FALSE;
   BGRColor* pBGRColor = (BGRColor*)pVar;
   char szBuffer[256];
   for( int i = 0; i < nVarNum; i++ ) {
      BGRColor& pnt3d = pBGRColor[i];
      sprintf( szBuffer, FORMAT_I32dx3, (int)pnt3d.B, (int)pnt3d.G, (int)pnt3d.R );
      str.append( szBuffer );
      if( nVarNum > 1 && i < ( nVarNum - 1 ) )
         str.append( " " );
   }
   return TRUE;
}

BOOL ConvertValueTypeToString( int nTypeID, std::string& output_string, void* input_value, void* default_string, char* szFormat, int nNum )
{
   MapType2CvtFuncIter iterType2CvrFunc = g_MapType2CvtFunc.find( nTypeID );
   if( iterType2CvrFunc == g_MapType2CvtFunc.end() ) {
      //LOGF << "not found nTypeID : " << nTypeID;
      return ( FALSE );
   }
   Str2VarCvtFunc* find_cvt_func = ( *iterType2CvrFunc ).second;
   CONVFUNCTYPE pVar2StrFunc     = CONVFUNCTYPE( find_cvt_func->m_pVar2Str );
   if( !pVar2StrFunc( output_string, input_value, default_string, szFormat, nNum ) ) {
      //LOGE << "failed to convert. type:" << nTypeID;
      return ( FALSE );
   }

   return ( TRUE );
}

/**
	* @brief default_value
	* @param nTypeID
	* @param input_value
	* @param output_value
	* @param default_value SHOULD be always nullptr, otherwise it updates as default_value.
	* @param szFormat
	* @param nNum
	* @return
	*/
BOOL ConvertStringToValueType( int nTypeID, const char* input_value, void* output_value, void* default_value, char* szFormat, int nNum )
{
   (void)default_value; // not used.
   MapType2CvtFuncIter iterType2CvrFunc = g_MapType2CvtFunc.find( nTypeID );
   if( iterType2CvrFunc == g_MapType2CvtFunc.end() ) {
      LOGF << "not found nTypeID : " << nTypeID;
      return ( FALSE );
   }
   Str2VarCvtFunc* find_cvt_func = (Str2VarCvtFunc*)( *iterType2CvrFunc ).second;
   CONVFUNCTYPE pStr2VarFunc     = CONVFUNCTYPE( find_cvt_func->m_pStr2Var );
   std::string strValue( input_value );
   if( !pStr2VarFunc( strValue, output_value, nullptr, szFormat, nNum ) ) {
      LOGE << "failed to convert. type:" << nTypeID << " inputStr:" << input_value;
      return ( FALSE );
   }

   return ( TRUE );
}

//////////////////////////////////
/// Initializing
//////////////////////////////////

struct C2XMLInit {
   C2XMLInit()
   {
      InitType2ConvertFuncTable();
   }
};
static C2XMLInit __InitC2XML;

} // namespace BCCL

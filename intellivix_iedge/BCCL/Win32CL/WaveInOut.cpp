#include "Win32CL/WaveInOut.h"

#pragma comment(lib,"Winmm.lib")

 namespace Win32CL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: CWaveData
//
///////////////////////////////////////////////////////////////////////////////

 CWaveData::CWaveData (   )
 
{
   memset ((WAVEHDR*)this,0,sizeof(WAVEHDR));
}

 CWaveData::CWaveData (int buf_size)
 
{
   memset ((WAVEHDR*)this,0,sizeof(WAVEHDR));
   Create (buf_size);
}

 void CWaveData::Create (int buf_size)
 
{
   if (Buffer.Length == buf_size) return;
   Buffer.Create (buf_size);
   lpData         = (LPSTR)(byte*)Buffer;
   dwBufferLength = buf_size;
}

 void CWaveData::Delete (   )
 
{
   Buffer.Delete (   );
   lpData         = NULL;
   dwBufferLength = 0;
}

 void CWaveData::Set (const byte *s_buffer,int s_buf_size)
 
{
   Create (s_buf_size);
   memcpy ((byte*)Buffer,s_buffer,s_buf_size);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CWaveIn
//
///////////////////////////////////////////////////////////////////////////////

 CWaveIn::CWaveIn (   )
 
{
   Flag_Started  = FALSE;
   HndWaveIn     = NULL;
   BitsPerSample = 0;
   BufferSize    = 0;
   NumChannels   = 0;
   SamplingRate  = 0;
   memset (&WaveDataFormat,0,sizeof(WAVEFORMATEX));
}

 CWaveIn::~CWaveIn (   )
 
{
   Close (   );
}

 int CWaveIn::AddBuffer (CWaveData *wave_data)
 
{
   if (HndWaveIn == NULL) return (1);
   if (waveInPrepareHeader (HndWaveIn,(LPWAVEHDR)wave_data,sizeof(WAVEHDR)) != MMSYSERR_NOERROR) return (2);
   if (waveInAddBuffer (HndWaveIn,(LPWAVEHDR)wave_data,sizeof(WAVEHDR)) != MMSYSERR_NOERROR) return (3);
   return (DONE);
}

 void CWaveIn::Callback_WaveIn (HWAVEIN hwi,UINT uMsg,DWORD_PTR dwInstance,DWORD_PTR dwParam1,DWORD_PTR dwParam2)
 
{
   if (uMsg != WIM_DATA) return;
   CWaveIn *wave_in = (CWaveIn*)dwInstance;
   if (!wave_in->Flag_Started) return;
   wave_in->OnWaveInBufferEnd (   );
}

 void CWaveIn::Close (   )
 
{
   if (HndWaveIn == NULL) return;
   CS_WaveDataQueue.Lock (   );
   waveInReset (HndWaveIn);
   CWaveData *wave_data = WaveDataQueue.First (   );
   while (wave_data) {
      waveInUnprepareHeader (HndWaveIn,(LPWAVEHDR)wave_data,sizeof(WAVEHDR));
      wave_data = WaveDataQueue.Next (wave_data);
   }
   waveInClose (HndWaveIn);
   HndWaveIn = NULL;
   WaveDataQueue.Delete (   );
   CS_WaveDataQueue.Unlock (   );
}

 CWaveData* CWaveIn::GetWaveData (   )
 
{
   int i;

   CS_WaveDataQueue.Lock (   );
   CWaveData *wave_data = WaveDataQueue.First (   );
   if (wave_data == NULL || !(wave_data->dwFlags & WHDR_DONE)) {
      CS_WaveDataQueue.Unlock (   );
      return (NULL);
   }
   WaveDataQueue.Remove  (wave_data);
   waveInUnprepareHeader (HndWaveIn,(LPWAVEHDR)wave_data,sizeof(WAVEHDR));
   // 버퍼수가 줄어들 경우 새로 넣어준다.
   int n = WaveDataQueue.GetNumItems();
   if (n < 2) {
      for (i = n; i < 2; i++) {
         CWaveData *wave_data = new CWaveData(BufferSize);
         if (AddBuffer (wave_data)) delete wave_data;
         else WaveDataQueue.Add (wave_data);
      }
   }
   CS_WaveDataQueue.Unlock (   );
   return (wave_data);
}
 
 int CWaveIn::Open (int n_channels,int sampling_rate,int bits_per_sample,int buf_size_in_msec)

{
   Close (   );
   int n_samples = (int)(buf_size_in_msec / 1000.0f * sampling_rate);
   NumChannels   = n_channels;
   SamplingRate  = sampling_rate;
   BitsPerSample = bits_per_sample;
   BufferSize    = n_samples * n_channels * bits_per_sample / 8;
   memset (&WaveDataFormat,0,sizeof(WAVEFORMATEX));
   WaveDataFormat.wFormatTag      = WAVE_FORMAT_PCM;
   WaveDataFormat.nChannels       = n_channels;
   WaveDataFormat.nSamplesPerSec  = sampling_rate;
   WaveDataFormat.wBitsPerSample  = bits_per_sample;
   WaveDataFormat.nBlockAlign     = WaveDataFormat.nChannels * WaveDataFormat.wBitsPerSample / 8;
   WaveDataFormat.nAvgBytesPerSec = WaveDataFormat.nSamplesPerSec * WaveDataFormat.nBlockAlign;
   if (waveInOpen (&HndWaveIn,WAVE_MAPPER,&WaveDataFormat,(DWORD_PTR)&Callback_WaveIn,(DWORD_PTR)this,CALLBACK_FUNCTION) != MMSYSERR_NOERROR) return (1);
   return (DONE);
}

 int CWaveIn::Start (   )
 
{
   int i,n;
   
   if (Flag_Started) return (DONE);
   if (HndWaveIn == NULL) return (1);
   CS_WaveDataQueue.Lock (   );
   CWaveData *wave_data = WaveDataQueue.First (   );
   for (n = 0; wave_data != NULL;   ) {
      if (!(wave_data->dwFlags & WHDR_DONE)) n++;
      wave_data = WaveDataQueue.Next (wave_data);
   }
   if (n < 2) n = 2 - n;
   for (i = 0; i < n; i++) {
      wave_data = new CWaveData(BufferSize);
      if (AddBuffer (wave_data)) {
         delete wave_data;
         CS_WaveDataQueue.Unlock (   );
         return (2);
      }
      else WaveDataQueue.Add (wave_data);
   }
   if (waveInStart (HndWaveIn) != MMSYSERR_NOERROR) return (3);
   CS_WaveDataQueue.Unlock (   );
   Flag_Started = TRUE;
   return (DONE);
}

 int CWaveIn::Stop (   )
 
{
   if (!Flag_Started) return (DONE);
   if (HndWaveIn == NULL) return (1);
   if (waveInStop (HndWaveIn) != MMSYSERR_NOERROR) return (2);
   Flag_Started = FALSE;
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CWaveOut
//
///////////////////////////////////////////////////////////////////////////////

 CWaveOut::CWaveOut (   )
 
{
   Flag_Started  = FALSE;
   HndWaveOut    = NULL;
   BitsPerSample = 0;
   NumChannels   = 0;
   SamplingRate  = 0;
   memset(&WaveDataFormat,0,sizeof(WAVEFORMATEX));
}

 CWaveOut::~CWaveOut (   )
 
{
   Close (   );
}

 int CWaveOut::Add (const byte *s_buffer,int s_buf_size)
 
{
   if (HndWaveOut == NULL) return (1);
   if (s_buf_size == 0) return (DONE);
   // 플레이 완료된 것들은 지운다.
   CS_WaveDataQueue.Lock (   );
   while (TRUE) {
      CWaveData *wave_data = WaveDataQueue.First (   );
      if (wave_data != NULL && (wave_data->dwFlags & WHDR_DONE)) {
         WaveDataQueue.Remove   (wave_data);
         waveOutUnprepareHeader (HndWaveOut,(LPWAVEHDR)wave_data,sizeof(WAVEHDR));
         delete wave_data;
      }
      else break;
   }
   CWaveData *wave_data = new CWaveData;
   wave_data->Set (s_buffer,s_buf_size);
   if (AddBuffer (wave_data)) {
      delete wave_data;
      CS_WaveDataQueue.Unlock (   );
      return (2);
   }
   WaveDataQueue.Add (wave_data);
   CS_WaveDataQueue.Unlock (   );
   return (DONE);
}

 int CWaveOut::AddBuffer (CWaveData *wave_data)
 
{
   if (HndWaveOut == NULL) return (1);   
   if (waveOutPrepareHeader (HndWaveOut,(LPWAVEHDR)wave_data,sizeof(WAVEHDR)) != MMSYSERR_NOERROR) return (2);
   if (waveOutWrite (HndWaveOut,(LPWAVEHDR)wave_data,sizeof(WAVEHDR)) != MMSYSERR_NOERROR) return (3);
   return (DONE);
}

 void CWaveOut::Callback_WaveOut (HWAVEOUT hwo,UINT uMsg,DWORD_PTR dwInstance,DWORD_PTR dwParam1,DWORD_PTR dwParam2)
 
{
   if (uMsg != WOM_DONE) return;
   CWaveOut *wave_out = (CWaveOut*)dwInstance;
   if (!wave_out->Flag_Started) return;
   wave_out->OnWaveOutBufferEnd (   );
}

 void CWaveOut::Close (   )
 
{
   if (HndWaveOut == NULL) return;
   CS_WaveDataQueue.Lock (   );
   waveOutReset (HndWaveOut);
   CWaveData *wave_data = WaveDataQueue.First (   );
   while (wave_data) {
      waveOutUnprepareHeader (HndWaveOut,(LPWAVEHDR)wave_data,sizeof(WAVEHDR));
      wave_data = WaveDataQueue.Next (wave_data);
   }
   waveOutClose (HndWaveOut);
   HndWaveOut = NULL;
   WaveDataQueue.Delete (   );
   CS_WaveDataQueue.Unlock (   );
}

 int CWaveOut::Open (int n_channels,int sampling_rate,int bits_per_sample)
 
{
   Close (   );
   NumChannels   = n_channels;
   SamplingRate  = sampling_rate;
   BitsPerSample = bits_per_sample;
   memset (&WaveDataFormat,0,sizeof(WAVEFORMATEX));
   WaveDataFormat.wFormatTag      = WAVE_FORMAT_PCM;
   WaveDataFormat.nChannels       = n_channels;
   WaveDataFormat.nSamplesPerSec  = sampling_rate;
   WaveDataFormat.wBitsPerSample  = bits_per_sample;
   WaveDataFormat.nBlockAlign     = WaveDataFormat.nChannels * WaveDataFormat.wBitsPerSample / 8;
   WaveDataFormat.nAvgBytesPerSec = WaveDataFormat.nSamplesPerSec * WaveDataFormat.nBlockAlign;
   WaveDataFormat.cbSize          = 0;
   if (waveOutOpen (&HndWaveOut,WAVE_MAPPER,&WaveDataFormat,(DWORD_PTR)&Callback_WaveOut,(DWORD_PTR)this,CALLBACK_FUNCTION) != MMSYSERR_NOERROR) return (1);
   Flag_Started = TRUE;
   if (Pause (   )) return (2);
   return (DONE);
}

 int CWaveOut::Pause (   )
 
{
   if (!Flag_Started) return (DONE);
   if (HndWaveOut == NULL) return (1);
   if (waveOutPause (HndWaveOut) != MMSYSERR_NOERROR) return (2);
   Flag_Started = FALSE;
   return (DONE);
}

 int CWaveOut::Start (   )
 
{
   if (Flag_Started) return (DONE);
   if (HndWaveOut == NULL) return (1);
   if (waveOutRestart (HndWaveOut) != MMSYSERR_NOERROR) return (2);
   Flag_Started = TRUE;
   return (DONE);
}

 int CWaveOut::Stop (   )
 
{
   if (HndWaveOut == NULL) return (1);
   if (waveOutReset (HndWaveOut) != MMSYSERR_NOERROR) return (2);
   Flag_Started = FALSE;
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CWaveFile
//
///////////////////////////////////////////////////////////////////////////////

 CWaveFile::CWaveFile (   )
 
{
   Flag_Writing = FALSE;
}

 CWaveFile::~CWaveFile (   )
 
{
   Close (   );
}

 void CWaveFile::Close (   )
 
{
   if (File == NULL) return;
   if (Flag_Writing) {
      int length = FileIO::Where (   );
      if (length < 0) return;
      length -= 8;
      if (FileIO::Seek (-(DataSize + 4),FIO_SEEK_CURRENT)) return;
      if (FileIO::Write ((byte*)&DataSize,1,sizeof(int))) return;
      if (FileIO::Seek (4,FIO_SEEK_BEGIN)) return;
      if (FileIO::Write ((byte*)&length,1,sizeof(int))) return;
      Flag_Writing = FALSE;
   }
   FileIO::Close (   );
}

 int CWaveFile::GoToStartPos (   )
 
{
   if (File == NULL) return (1);
   if (Seek (StartPos,SEEK_SET)) return (2);
   return (DONE);
}

 int CWaveFile::Create (const char *file_name,int n_channels,int sampling_rate,int bits_per_sample,int format)
 
{
   uint size;
   
   NumChannels   = (short)n_channels;
   SamplingRate  = (uint )sampling_rate;
   BitsPerSample = (short)bits_per_sample;
   BlockAlign    = (short)(n_channels * bits_per_sample / 8);
   ByteRate      = (uint )(sampling_rate * BlockAlign);
   Format        = (short)format;
   if (FileIO::Open  (file_name,FIO_BINARY,FIO_CREATE)) return (1);
   if (FileIO::Write ((byte*)"RIFF",1,4)) return (2);
   size = 0;
   if (FileIO::Write ((byte*)&size,1,sizeof(uint))) return (3);
   if (FileIO::Write ((byte*)"WAVE",1,4)) return (4);
   if (FileIO::Write ((byte*)"fmt ",1,4)) return (5);
   size = 16;
   if (FileIO::Write ((byte*)&size,1,sizeof(uint))) return (6);
   if (FileIO::Write ((byte*)&Format,1,sizeof(short))) return (7);
   if (FileIO::Write ((byte*)&NumChannels,1,sizeof(short))) return (8);
   if (FileIO::Write ((byte*)&SamplingRate,1,sizeof(uint))) return (9);
   if (FileIO::Write ((byte*)&ByteRate,1,sizeof(uint))) return (10);
   if (FileIO::Write ((byte*)&BlockAlign,1,sizeof(short))) return (11);
   if (FileIO::Write ((byte*)&BitsPerSample,1,sizeof(short))) return (12);
   if (FileIO::Write ((byte*)"data",1,4)) return (13);
   size = 0;
   if (FileIO::Write ((byte*)&size,1,sizeof(uint))) return (14);
   Flag_Writing = TRUE;
   DataSize     = 0;
   return (DONE);
}

 int CWaveFile::Open (const char *file_name)
 
{
   byte buffer[4];
   uint size;
   
   if (FileIO::Open (file_name,FIO_BINARY,FIO_READ)) return (1);
   if (FileIO::Read (buffer,4,1)) return (2);
   if (strncmp ((char*)buffer,"RIFF",4)) return (3);
   if (FileIO::Read ((byte*)&size,1,sizeof(uint))) return (4);
   if (FileIO::Read (buffer,4,1)) return (5);
   if (strncmp ((char*)buffer,"WAVE",4)) return (6);
   if (FileIO::Read (buffer,4,1)) return (7);
   if (strncmp ((char*)buffer,"fmt ",4)) return (8);
   if (FileIO::Read ((byte*)&size,1,sizeof(uint))) return (9);
   if (FileIO::Read ((byte*)&Format,1,sizeof(short))) return (10);
   if (FileIO::Read ((byte*)&NumChannels,1,sizeof(short))) return (11);
   if (FileIO::Read ((byte*)&SamplingRate,1,sizeof(uint))) return (12);
   if (FileIO::Read ((byte*)&ByteRate,1,sizeof(uint))) return (13);
   if (FileIO::Read ((byte*)&BlockAlign,1,sizeof(short))) return (14);
   if (FileIO::Read ((byte*)&BitsPerSample,1,sizeof(short))) return (15);
   if (GetLength (   ) < 0) return (16);
   StartPos = Where (   );
   DataSize = 0;
   return (DONE);
}

 int CWaveFile::Read (byte *d_buffer,int d_buf_size)
 
{
   byte buffer[4];
   uint size;
   
   if (File == NULL) return (0);
   int n_bytes_read = 0;
   while (TRUE) {
      if (d_buf_size == n_bytes_read) break;
      if (!DataSize) {
         int c_pos = Where (   );
         if (c_pos >= Length) break;
         if (FileIO::Read (buffer,4,1)) break;
         if (FileIO::Read ((byte*)&size,1,sizeof(uint))) break;
         if (strncmp ((char*)buffer,"data",4)) {
            if (Seek (size,SEEK_CUR)) break;
         }
         else DataSize = size;
      }
      else {
         int n_bytes_remain = d_buf_size - n_bytes_read;
         int n = GetMinimum (DataSize,n_bytes_remain);
         if (FileIO::Read (d_buffer + n_bytes_read,n,1)) break;
         DataSize -= n;
         n_bytes_read += n;
      }
   }
   return (n_bytes_read);
}

 int CWaveFile::Write (byte *s_buffer,int s_buf_size)
 
{
   if (!Flag_Writing) return (1);
   if (FileIO::Write (s_buffer,s_buf_size,sizeof(byte))) return (2);
   DataSize += s_buf_size;
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 int ConvertTo2CHWaveData (byte *s_buffer,int n_channels,int n_samples,int bps,byte *d_buffer)
 
{
   int i;
   
   int smp_size = (bps + 7) / 8;
   if (n_channels < 1) return (1);
   if (smp_size < 1 || smp_size > 2) return (2);
   switch (n_channels) {
      case 1: {
         if (smp_size == 1) {
            byte *src = s_buffer;
            byte *dst = d_buffer;
            for (i = 0; i < n_samples; i++) {
               *dst++ = *src;
               *dst++ = *src++;
            }
         }
         else {
            ushort *src = (ushort*)s_buffer;
            ushort *dst = (ushort*)d_buffer;
            for (i = 0; i < n_samples; i++) {
               *dst++ = *src;
               *dst++ = *src++;
            }
         }
      } break;
      case 2: {
         memcpy (d_buffer,s_buffer,n_channels * n_samples * smp_size);
      } break;
      default: {
         int skip = n_channels - 2;
         if (smp_size == 1) {
            byte *src = s_buffer;
            byte *dst = s_buffer;
            for (i = 0; i < n_samples; i++) {
               *dst++ = *src++;
               *dst++ = *src++;
               src += skip;
            }
         }
         else {
            ushort *src = (ushort*)s_buffer;
            ushort *dst = (ushort*)d_buffer;
            for (i = 0; i < n_samples; i++) {
               *dst++ = *src++;
               *dst++ = *src++;
               src += skip;
            }
         }
      } break;
   }
   return (DONE);
}

} // namespace Win32CL

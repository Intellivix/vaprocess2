#pragma once

#include "Win32CLDef.h"
#include <vfw.h>

 namespace Win32CL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: CAVIFile
//
///////////////////////////////////////////////////////////////////////////////
 
 class CAVIFile

{
   protected:
      int Flag_CoInitialize;
      int Flag_AVIFileInit;
      int NextFramePos;
   
   protected:
      BArray1D   FrameBuffer;
      BArray1D   VideoFormat;
      PAVIFILE   File;
      PAVISTREAM Stream;

   public: // Real-Only
      int   NumFrames;
      int   StartFramePos;
      int   EndFramePos;
      int   FrameWidth;
      int   FrameHeight;
      uint  CodecID;
      float FrameRate;

   public:
      int   Flag_Repeat;

   public:
      CAVIFile (   );
      virtual ~CAVIFile (   );

   public:
      int operator ! (   ) const;

   public:
      virtual void Close (   );

   protected:
      void GetVideoFormat (uint codec_fcc,int frm_width,int frm_height,BITMAPINFOHEADER *video_format);
      void InvertFrame    (const byte *s_buffer,int bpl,int height,byte *d_buffer);
   
   public:
      int  AddEncodedFrame     (const byte *s_buffer,int s_buf_size,int flag_key_frame);
      int  ConvertToTimePos    (int frame_pos);
      int  Create              (LPCTSTR file_name,uint codec_id,int frm_width,int frm_height,float frame_rate);
      int  ExtractEncodedFrame (byte **p_d_buffer,int &d_buf_size,int &flag_key_frame);
      int  FindKeyFrame        (int s_pos,int &d_pos,int flag_prev = TRUE);
      void GetCodecFCC         (char *codec_fcc);
      void GetTimeCode         (int frame_pos,char *time_code);
      int  GetNextFramePos     (   );
      void GoToFirstFrame      (   );
      void GoToLastFrame       (   );
      int  Open                (LPCTSTR file_name);
      int  SetAbsPlaybackPos   (int pos,int flag_time_based = TRUE);
      void SetNextFramePos     (int frame_pos);
      int  SetRelPlaybackPos   (int pos,int flag_time_based = TRUE);
};

 inline int CAVIFile::operator ! (   ) const

{
   if (FrameWidth == 0 || FrameHeight == 0) return (TRUE);
   else return (FALSE);
}

 inline int CAVIFile::GetNextFramePos (   )
 
{
   return (NextFramePos);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CAVIFile_FFMPEG
//
///////////////////////////////////////////////////////////////////////////////

 class CAVIFile_FFMPEG : public CAVIFile
 
{
   public:
      FFMPEG_VideoCodec VideoCodec;
   
   public:
      virtual ~CAVIFile_FFMPEG (   );
   
   public:
      virtual void Close (   );
      
   public:
      int AddFrame          (const byte *s_buffer);
      int AddFrame          (BGRImage &s_image);
      int Create            (LPCTSTR file_name,enum AVCodecID codec_id,int frm_width,int frm_height,float frame_rate,enum AVPixelFormat in_pixel_format = AV_PIX_FMT_YUYV422);
      int ExtractFrame      (byte **p_d_buffer);
      int ExtractFrame      (byte *d_buffer);
      int ExtractFrame      (BGRImage &d_image);
         // 주의: ExtractFrame() 함수의 리턴 값이 -1인 경우는 디코딩 오류는 없으나 영상 데이터는 얻지 못했음을 의미한다.
      int FlushCodecBuffers (   );
      int Open              (LPCTSTR file_name,enum AVPixelFormat out_pixel_format = AV_PIX_FMT_YUYV422);
};

}

#pragma once

#include "Win32CLDef.h"
#include <mmsystem.h>

 namespace Win32CL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Class: CWaveData
//
///////////////////////////////////////////////////////////////////////////////

 class CWaveData : public WAVEHDR
 
{
   public:
      BArray1D Buffer;
   
   public:
      CWaveData *Prev;
      CWaveData *Next;
      
   public:
      CWaveData (   );
      CWaveData (int buf_size);
   
   public:
      void Create              (int buf_size);
      void Delete              (   );
      int  GetRecordedDataSize (   );
      void Set                 (const byte *s_buffer,int s_buf_size);
};

 inline int CWaveData::GetRecordedDataSize (   )
 
{
   return ((int)dwBytesRecorded);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CWaveIn
//
///////////////////////////////////////////////////////////////////////////////

 class CWaveIn
 
{
   protected:
      int     Flag_Started;
      HWAVEIN HndWaveIn;
   
   protected:
      CCriticalSection      CS_WaveDataQueue;
      LinkedList<CWaveData> WaveDataQueue;
      
   public:
      int BitsPerSample;
      int BufferSize;
      int NumChannels;
      int SamplingRate;
      
   public:
      WAVEFORMATEX WaveDataFormat;
   
   public:
      CWaveIn (   );
      virtual ~CWaveIn (   );
     
   protected:
      void static CALLBACK Callback_WaveIn (HWAVEIN hwi,UINT uMsg,DWORD_PTR dwInstance,DWORD_PTR dwParam1,DWORD_PTR dwParam2);
      
   protected:
      int AddBuffer (CWaveData *audio_data);
   
   public:
      virtual void OnWaveInBufferEnd (   ) {   };
   
   public:
      void Close             (   );
      int  GetQueueLength    (   );
      CWaveData* GetWaveData (   );
      int  Open              (int n_channels,int sampling_rate,int bits_per_sample,int buf_size_in_msec = 1000);
      int  Start             (   );
      int  Stop              (   );
};

 inline int CWaveIn::GetQueueLength (   )
 
{
   return (WaveDataQueue.GetNumItems (   ));
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CWaveOut
//
///////////////////////////////////////////////////////////////////////////////

 class CWaveOut
 
{
   protected:
      int      Flag_Started;
      HWAVEOUT HndWaveOut;
      
   protected:
      CCriticalSection      CS_WaveDataQueue;
      LinkedList<CWaveData> WaveDataQueue;
   
   public:
      int BitsPerSample;
      int NumChannels;
      int SamplingRate;
   
   public:
      WAVEFORMATEX WaveDataFormat;
   
   public:
      CWaveOut (   );
      virtual ~CWaveOut (   );
   
   protected:
      void static CALLBACK Callback_WaveOut (HWAVEOUT hwo,UINT uMsg,DWORD_PTR dwInstance,DWORD_PTR dwParam1,DWORD_PTR dwParam2);

   protected:
      int AddBuffer (CWaveData *audio_data);

   public:
      virtual void OnWaveOutBufferEnd (   ) {   };

   public:
      int  Add            (const byte *s_buffer,int s_buf_size);
      void Close          (   );
      int  GetQueueLength (   );
      int  Open           (int n_channels,int sampling_rate,int bits_per_sample);
      int  Pause          (   );
      int  Start          (   );
      int  Stop           (   );
};

 inline int CWaveOut::GetQueueLength (   )
 
{
   return (WaveDataQueue.GetNumItems (   ));
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CWaveFile
//
///////////////////////////////////////////////////////////////////////////////

 class CWaveFile : public FileIO
 
{
   protected:
      int  DataSize;
      int  StartPos;
      bool Flag_Writing;
   
   public:
      uint  ByteRate;
      uint  SamplingRate;
      short BitsPerSample;
      short BlockAlign;
      short Format;
      short NumChannels;
   
   public:
      CWaveFile (   );
      virtual ~CWaveFile (   );
      
   public:
      void Close        (   );
      int  Create       (const char *file_name,int n_channels,int sampling_rate,int bits_per_sample,int format = WAVE_FORMAT_PCM);
      int  GoToStartPos (   );
      int  Open         (const char *file_name);
      int  Read         (byte *d_buffer,int d_buf_size);
      int  Write        (byte *s_buffer,int s_buf_size);
};

}

#include "Win32CL/Utilities.h"
#include <Iphlpapi.h>

#pragma comment(lib,"iphlpapi.lib")

 namespace Win32CL
 
{

// String Conversion

 void ConvertString (const char* s_string,CString& d_string)
 
{
   #if defined(_UNICODE)
   int n = MultiByteToWideChar (CP_ACP,0,s_string,-1,NULL,0);
   Array1D<wchar_t> t_string(n);
   MultiByteToWideChar (CP_ACP,0,s_string,-1,t_string,n);
   d_string = (wchar_t*)t_string;
   #else
   d_string = s_string;
   #endif
}

 void ConvertString (CString& s_string,StringA& d_string)
 
{
   #if defined(_UNICODE)
   int n = WideCharToMultiByte (CP_ACP,0,s_string,-1,NULL,0,NULL,NULL);
   d_string.Create (n);
   WideCharToMultiByte (CP_ACP,0,s_string,-1,d_string,n,NULL,NULL);
   #else
   d_string = (LPCSTR)s_string;
   #endif
}

 void ConvertStringUTF8 (const char* s_string,CString& d_string)
 
{
   int n = MultiByteToWideChar (CP_UTF8,0,s_string,-1,NULL,0);
   Array1D<wchar_t> t_string(n);
   MultiByteToWideChar (CP_UTF8,0,s_string,-1,t_string,n);
   #if defined(_UNICODE)
   d_string = (wchar_t*)t_string;
   #else
   n = WideCharToMultiByte (CP_ACP,0,t_string,-1,NULL,0,NULL,NULL);
   StringA t_string2(n);
   WideCharToMultiByte (CP_ACP,0,t_string,-1,t_string2,n,NULL,NULL);
   d_string = (char*)t_string2;
   #endif
}

 void ConvertStringUTF8 (CString& s_string,StringA& d_string)
 
{
   #if defined(_UNICODE)
   int n = WideCharToMultiByte (CP_UTF8,0,s_string,-1,NULL,0,NULL,NULL);
   d_string.Create (n);
   WideCharToMultiByte (CP_UTF8,0,s_string,-1,(char*)d_string,n,NULL,NULL);
   #else
   int n = MultiByteToWideChar (CP_ACP,0,s_string,-1,NULL,0);
   Array1D<wchar_t> t_string(n);
   MultiByteToWideChar (CP_ACP,0,s_string,-1,t_string,n);
   n = WideCharToMultiByte (CP_UTF8,0,t_string,-1,NULL,0,NULL,NULL);
   d_string.Create (n);
   WideCharToMultiByte (CP_UTF8,0,t_string,-1,d_string,n,NULL,NULL);
   #endif
}

// Drawing

 void DrawGraph (CImageView *iv,FArray1D &s_array,int x,int y,int height,COLORREF color,int flag_bg_erase,int flag_rotate,float threshold)

{
   int i;
   
   int offset = 3;
   int x0 = x + 1;
   int y0 = y + height;
   int width = s_array.Length + 2;
   if (flag_bg_erase) {
      if (flag_rotate) iv->DrawFilledRectangle (x,y,height,width,PC_BLACK);
      else iv->DrawFilledRectangle (x,y,width,height,PC_BLACK);
   }
   int   min_i,max_i;
   float min_v,max_v;
   FindMinMax (s_array,min_v,max_v,min_i,max_i);
   if (threshold != MC_VLV) {
      if (threshold < min_v) min_v = threshold;
      if (threshold > max_v) max_v = threshold;
   }
   float a = 0.0f;
   if (min_v != max_v) a = (2.0f * offset - height) / (max_v - min_v);
   float b  = -a * min_v - offset + 0.5f;
   int x1 = x0;
   int y1 = y0 + (int)(a * s_array[0] + b);
   int dx = y + x + height;
   int dy = y - x;
   int x2,y2;
   for (i = 0; i < s_array.Length; i++) {
      x2 = x0 + i;
      y2 = y0 + (int)(a * s_array[i] + b);
      if (flag_rotate) iv->DrawLine (dx - y1,x1 + dy,dx - y2,x2 + dy,color);
      else iv->DrawLine (x1,y1,x2,y2,color);
      x1 = x2, y1 = y2;
   }
   x1 = x0;
   x2 = x0 + s_array.Length - 1;
   y1 = y2 = y0 + (int)(a * threshold + b);
   if (flag_rotate) {
      if (threshold != MC_VLV) iv->DrawLine (dx - y1,x1 + dy,dx - y2,x2 + dy,PC_RED);
      iv->DrawRectangle (x,y,height,width,PC_WHITE);
   }
   else {
      if (threshold != MC_VLV) iv->DrawLine (x1,y1,x2,y2,PC_RED);
      iv->DrawRectangle (x,y,width,height,PC_WHITE);
   }
}

// MAC Address

 int GetMACAddress (StringA& mac_addr)

{
   ULONG buf_size = sizeof(IP_ADAPTER_INFO);
   BArray1D adapter_info(buf_size);
   if (GetAdaptersInfo ((IP_ADAPTER_INFO*)(byte*)adapter_info,&buf_size) == ERROR_BUFFER_OVERFLOW) adapter_info.Create (buf_size);
   if (GetAdaptersInfo ((IP_ADAPTER_INFO*)(byte*)adapter_info,&buf_size) == ERROR_SUCCESS) {
      BYTE* addr = ((IP_ADAPTER_INFO*)(byte*)adapter_info)->Address;
      mac_addr.Format ("%02X-%02X-%02X-%02X-%02X-%02X",addr[0],addr[1],addr[2],addr[3],addr[4],addr[5]);
      return (DONE);
   }
   return (1);
}

// Buffer Loading & Saving

 int LoadFileIntoBuffer (LPCTSTR file_name,BArray1D& d_buffer,int flag_add_null)

{
   d_buffer.Delete (   );
   HANDLE h_file = CreateFile (file_name,FILE_GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
   if (h_file == INVALID_HANDLE_VALUE) return (1);
   LARGE_INTEGER file_size;
   if (!GetFileSizeEx (h_file,&file_size)) return (2);
   if (!file_size.LowPart) return (3);
   int buf_size = (int)file_size.LowPart;
   if (flag_add_null) buf_size++;
   d_buffer.Create (buf_size);
   int r_code = DONE;
   DWORD bytes_read;
   if (!ReadFile (h_file,d_buffer,file_size.LowPart,&bytes_read,NULL)) r_code = 4;
   else {
      if (flag_add_null) d_buffer[file_size.LowPart] = 0;
   }
   CloseHandle (h_file);
   return (r_code);
}

 int SaveBufferIntoFile (LPCTSTR file_name,void* s_buffer,int s_buf_size)
 
{
   HANDLE h_file = CreateFile (file_name,GENERIC_WRITE,0,NULL,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL);
   if (h_file == INVALID_HANDLE_VALUE) return (1);
   int r_code = DONE;
   DWORD n_bytes_written;
   if (!WriteFile (h_file,s_buffer,(DWORD)s_buf_size,&n_bytes_written,NULL)) r_code = 2;
   CloseHandle (h_file);
   return (r_code);
}

// Bitmap Handling

 void GetBitmap (CDC* dc,BGRImage& s_image,CBitmap& d_bitmap)

{
   d_bitmap.DeleteObject (   );
   d_bitmap.CreateCompatibleBitmap (dc,s_image.Width,s_image.Height);
   BITMAPINFO bmp_info;
   memset (&bmp_info,0,sizeof(BITMAPINFO));
   bmp_info.bmiHeader.biSize        = sizeof(BITMAPINFOHEADER);
   bmp_info.bmiHeader.biWidth       = s_image.Width + s_image.Margin;
   bmp_info.bmiHeader.biHeight      = -s_image.Height;
   bmp_info.bmiHeader.biPlanes      = 1;
   bmp_info.bmiHeader.biBitCount    = 24;
   bmp_info.bmiHeader.biCompression = BI_RGB;
   SetDIBits (dc->m_hDC,d_bitmap,0,s_image.Height,s_image,&bmp_info,DIB_RGB_COLORS);
}

 void GetBitmap (CDC* dc,BGRImage& s_image,ISize2D& bm_size,CBitmap& d_bitmap)

{
   if      (s_image.Width == bm_size.Width && s_image.Height == bm_size.Height) GetBitmap (dc,s_image,d_bitmap);
   else if (bm_size.Width <= s_image.Width && bm_size.Height <= s_image.Height) {
      BGRImage t_image(bm_size.Width,bm_size.Height);
      Reduce (s_image,t_image);
      GetBitmap (dc,t_image,d_bitmap);
   }
   else {
      BGRImage t_image(bm_size.Width,bm_size.Height);
      Resize (s_image,t_image);
      GetBitmap (dc,t_image,d_bitmap);
   }
}

}

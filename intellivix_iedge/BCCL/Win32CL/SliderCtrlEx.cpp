#include "Win32CL/SliderCtrlEx.h"

 namespace Win32CL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Class: CSliderCtrlEx
//
///////////////////////////////////////////////////////////////////////////////

IMPLEMENT_DYNAMIC(CSliderCtrlEx,CSliderCtrl)

BEGIN_MESSAGE_MAP(CSliderCtrlEx,CSliderCtrl)
   ON_WM_LBUTTONDOWN (   )
   ON_WM_SETFOCUS    (   )
END_MESSAGE_MAP(   )

 void CSliderCtrlEx::OnLButtonDown (UINT flags,CPoint point)

{
   CRect ch_rect,th_rect;
   GetChannelRect (ch_rect);
   GetThumbRect   (th_rect);
   int length = ch_rect.Width (   ) - th_rect.Width (   );
   int range  = GetRangeMax (   ) - GetRangeMin (   );
   int pos    = (int)((point.x - ch_rect.left) * (range / (float)length));
   SetPos (pos);
   CSliderCtrl::OnLButtonDown (flags,point);
}

 void CSliderCtrlEx::OnSetFocus (CWnd *old_wnd)
 
{
   CWnd *parent = GetParent (   );
   if (parent != NULL) parent->SetFocus (   );
}

}

///////////////////////////////////////////////////////////////////////////////
//
// Win32CL (Win32 Component Library) 
//
// Created by Jeong-Hun Jang
// E-mail: jeong.hun.jang@gmail.com
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Win32CL/Direct3D.h"
extern "C"
{
#include "libavcodec/dxva2.h"
#include "libavutil/imgutils.h"
};

 namespace Win32CL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Class: CDXVA2VideoDecoder
//
///////////////////////////////////////////////////////////////////////////////

 class CDXVA2VideoDecoder

{
   protected:
      struct DXVA2Mode {
         const GUID* Guid;
         AVCodecID   CodecID;
      };
      static DXVA2Mode DXVA2Modes[];

   protected:
      int            Flag_InnerCodecCtx;
      int            Flag_Opened;
      int            Flag_CreatedMemory = false;
      AVPixelFormat  IntPixFmt;
      AVCodecContext BackupCodecCtx;
      BArray1D       SrcBuffer;
      AVFrame* DestFrame  = nullptr;
      byte*    CasheBlock = nullptr;

   protected:
      IDirect3DDevice9*            D3DDevice;
      IDirectXVideoDecoderService* VideoService;
      IDirectXVideoDecoder*        VideoDecoder;

   protected:
      GUID                      DecoderGUID;
      D3DFORMAT                 DecoderFormat;
      DXVA2_ConfigPictureDecode DecoderConfig;     
      dxva_context              DXVAContext;
 
   protected:
      struct SurfaceInfo {
         int Flag_Used;
         int Age;
      };
      struct SurfaceParam {
         CDXVA2VideoDecoder* Decoder;
         IDirect3DSurface9*  Surface;
      };
      int SurfaceWidth;
      int SurfaceHeight;
      int SurfaceAge;
      Array1D<IDirect3DSurface9*> Surfaces;
      Array1D<SurfaceInfo> SurfaceInfos;
   
   public:
      int             FrameWidth;
      int             FrameHeight;
      AVCodecID       CodecID;
      AVPixelFormat   PixFmt;
      AVCodecContext* CodecCtx;

   public:
      ISize2D DecodedFrameSize;     // jun: 디코딩 한 영상의 실제 크기 

   public:
      CDXVA2VideoDecoder (   );
      virtual ~CDXVA2VideoDecoder (   );
      
   protected:
      void _Init                  (   );
      int  CopyFrame              (AVFrame* s_frame,AVFrame* d_frame);
      int  CreateD3DDevice        (   );
      void CreateFrameNV12        (int fr_width,int fr_height,int pitch,AVFrame* d_frame);
      int  CreateRenderTarget     (   );
      int  CreateVideoDecoder     (   );
      int  CreateVideoService     (   );
      int  GetDecoderDevice       (   );
      int  GetDecoderConfig       (   );
      int  GetNumSurfacesRequired (   );
      int  OpenAVCodec            (   );
      int  OpenDecoder            (   );
      void SetHWAccelContext      (   );
   
   protected:
      void CopyFrameNV12       (const byte* s_buffer,byte* d_buffer_y,byte* d_buffer_uv,int s_height,int d_height,int pitch);
      void CopyFrameNV12_Basic (const byte* s_buffer,byte* d_buffer_y,byte* d_buffer_uv,int s_height,int d_height,int pitch);
      void CopyFrameNV12_SSE4  (const byte* s_buffer,byte* d_buffer_y,byte* d_buffer_uv,int s_height,int d_height,int pitch);
      void CopyMemory_Basic    (const void* s_buffer,uint s_buf_size,void* d_buffer);
      void CopyMemory_SSE4     (const void* s_buffer,uint s_buf_size,void* d_buffer);

   protected:
      static void          Callback_FreeAlignedMem (void* opaque,uint8_t* data);
      static int           Callback_GetBuffer      (AVCodecContext* codec_ctx,AVFrame* frame,int flags);
      static AVPixelFormat Callback_GetFormat      (AVCodecContext* codec_ctx,const AVPixelFormat* pix_fmts);   
      static void          Callback_ReleaseBuffer  (void* opaque,uint8_t* data);
   
   public:
      int operator ! (   ) const;
   
   public:
      virtual void Close (   );
   
   public:
      int OpenDecoder (AVCodecID codec_id,int frm_width,int frm_height);
      int OpenDecoder (AVCodecContext* codec_ctx);
      int Decode      (const byte* s_buffer,int s_buf_size,AVFrame* d_frame);
      int Decode      (const byte* s_buffer,int s_buf_size,byte* d_buffer,const int* pitches = NULL,int n_planes = 1);
      int Decode      (const byte *s_buffer,int s_buf_size,BGRImage &d_image);
      int Flush       (   );
};

 inline int CDXVA2VideoDecoder::operator ! (   ) const

{
   if (Flag_Opened) return (FALSE);
   else return (TRUE);
}

}

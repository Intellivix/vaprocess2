#include "Win32CL/Timer.h"

#pragma comment(lib,"Winmm.lib")

 namespace Win32CL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: CPerformanceTimer
//
///////////////////////////////////////////////////////////////////////////////

 CPerformanceTimer::CPerformanceTimer (   )
 
{
   QueryPerformanceFrequency ((_LARGE_INTEGER *)&Frequency);
   Reset (   );
}

 double CPerformanceTimer::GetElapsedTime (   )
 
{
   UINT64 end_time;
   
   QueryPerformanceCounter ((_LARGE_INTEGER *)&end_time);
   double e_time = (double)(end_time - StartTime) / Frequency * 1000.0;
   return (e_time);
}
 
 void CPerformanceTimer::Reset (   )
 
{
   QueryPerformanceCounter ((_LARGE_INTEGER *)&StartTime);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CTimer
//
///////////////////////////////////////////////////////////////////////////////

 CTimer::CTimer (   )
 
{
   Period  = 0;
   TimerID = 0;
};

 int CTimer::CheckTimerEvent (uint blocking_time)

{
   if (WaitForSingleObject (Event_Timer,blocking_time) == WAIT_OBJECT_0) return (TRUE);
   else return (FALSE);
}

 int CTimer::StartTimer (uint period,int flag_periodic)

{
   uint flags = TIME_CALLBACK_EVENT_SET;
   if (flag_periodic) flags |= TIME_PERIODIC;
   else flags |= TIME_ONESHOT;
   StopTimer (   );
   Event_Timer.ResetEvent (   );
   if (timeBeginPeriod (1) != TIMERR_NOERROR) return (1);
   Period  = period;
   TimerID = timeSetEvent (Period,1,(LPTIMECALLBACK)(HANDLE)Event_Timer,NULL,flags);
   if (!TimerID) return (2);
   else return (DONE);
}

 int CTimer::StopTimer (   )
 
{
   if (!TimerID) return (1);
   MMRESULT e_code = timeKillEvent (TimerID);
   TimerID = 0;
   if (e_code != TIMERR_NOERROR) return (2);
   if (timeEndPeriod (1) != TIMERR_NOERROR) return (3);
   return (DONE);
}

}

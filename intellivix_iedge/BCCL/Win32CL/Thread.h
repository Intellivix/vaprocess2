#pragma once

#include "Win32CLDef.h"

 namespace Win32CL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Class: CLock
//
///////////////////////////////////////////////////////////////////////////////

 class CLock
 
{
   protected:
      int Flag_Lock;
      CCriticalSection* CriticalSection;
   
   public:
      CLock (CCriticalSection& cs,int flag_lock = TRUE);
     ~CLock (   );
     
   public:
      void Lock   (   );
      void Unlock (   );
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: CThread
//
///////////////////////////////////////////////////////////////////////////////

class CThread;

typedef void(*CALLBACK_ThreadProc)(CThread* thread,void* param);

 class CThread
 
{
   protected:
      int    Flag_ThreadRunning;
      HANDLE ThreadHnd;

   protected:
      void* CBParam_ThreadProc;
      CALLBACK_ThreadProc Callback_ThreadProc;
 
   public:
      CEvent Event_Stop;

   public:
      uint WaitingTime; // 쓰레드가 종료될 때까지 기다리는 시간

   public:
      CThread (   );
      virtual ~CThread (   );
   
   protected:
      static uint _ThreadProc (void* param);
      
   public:    
      void ExecuteThreadProc (   );
 
   protected:
      virtual void ThreadProc (   );

   public:
      int  CheckStopEvent         (uint timeout = 0);
      int  IsThreadRunning        (   );
      void SetCallback_ThreadProc (CALLBACK_ThreadProc cb_func,void* cb_param);
      int  StartThread            (   );
      void StopThread             (   );
      void WaitForThreadEnd       (uint timeout = INFINITE);
};

}

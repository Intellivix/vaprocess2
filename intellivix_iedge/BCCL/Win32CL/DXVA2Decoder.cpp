///////////////////////////////////////////////////////////////////////////////
//
// Win32CL (Win32 Component Library) 
//
// Created by Jeong-Hun Jang
// E-mail: jeong.hun.jang@gmail.com
//
///////////////////////////////////////////////////////////////////////////////

#pragma comment(lib,"dxva2.lib")
#pragma comment(lib,"avutil.lib")

#include <smmintrin.h>
#include "Win32CL/DXVA2Decoder.h"

#include <initguid.h>
DEFINE_GUID(IID_IDirectXVideoDecoderService, 0xfc51a551,0xd5e7,0x11d9,0xaf,0x55,0x00,0x05,0x4e,0x43,0xff,0x02);
DEFINE_GUID(DXVA2_ModeMPEG2_VLD,             0xee27417f,0x5e28,0x4e65,0xbe,0xea,0x1d,0x26,0xb5,0x08,0xad,0xc9);
DEFINE_GUID(DXVA2_ModeMPEG2and1_VLD,         0x86695f12,0x340e,0x4f04,0x9f,0xd3,0x92,0x53,0xdd,0x32,0x74,0x60);
DEFINE_GUID(DXVA2_ModeH264_E,                0x1b81be68,0xa0c7,0x11d3,0xb9,0x84,0x00,0xc0,0x4f,0x2e,0x73,0xc5);
DEFINE_GUID(DXVA2_ModeH264_F,                0x1b81be69,0xa0c7,0x11d3,0xb9,0x84,0x00,0xc0,0x4f,0x2e,0x73,0xc5);
DEFINE_GUID(DXVADDI_Intel_ModeH264_E,        0x604F8E68,0x4951,0x4C54,0x88,0xFE,0xAB,0xD2,0x5C,0x15,0xB3,0xD6);
DEFINE_GUID(DXVA2_ModeVC1_D,                 0x1b81beA3,0xa0c7,0x11d3,0xb9,0x84,0x00,0xc0,0x4f,0x2e,0x73,0xc5);
DEFINE_GUID(DXVA2_ModeVC1_D2010,             0x1b81beA4,0xa0c7,0x11d3,0xb9,0x84,0x00,0xc0,0x4f,0x2e,0x73,0xc5);
DEFINE_GUID(DXVA2_NoEncrypt,                 0x1b81beD0,0xa0c7,0x11d3,0xb9,0x84,0x00,0xc0,0x4f,0x2e,0x73,0xc5);
DEFINE_GUID(GUID_NULL,                       0x00000000,0x0000,0x0000,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00);

 namespace Win32CL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// External Variables
//
///////////////////////////////////////////////////////////////////////////////

extern LPDIRECT3D9 _Direct3D;

///////////////////////////////////////////////////////////////////////////////
//
// Class: CDXVA2VideoDecoder
//
///////////////////////////////////////////////////////////////////////////

 CDXVA2VideoDecoder::DXVA2Mode CDXVA2VideoDecoder::DXVA2Modes[] =
 
{
   // MPEG-2
   { &DXVA2_ModeMPEG2_VLD,      AV_CODEC_ID_MPEG2VIDEO },
   { &DXVA2_ModeMPEG2and1_VLD,  AV_CODEC_ID_MPEG2VIDEO },
   // H.264
   { &DXVA2_ModeH264_F,         AV_CODEC_ID_H264 },
   { &DXVA2_ModeH264_E,         AV_CODEC_ID_H264 },
   // Intel specific H.264 mode
   { &DXVADDI_Intel_ModeH264_E, AV_CODEC_ID_H264 },
   // VC-1 / WMV3
   { &DXVA2_ModeVC1_D2010,      AV_CODEC_ID_VC1  },
   { &DXVA2_ModeVC1_D2010,      AV_CODEC_ID_WMV3 },
   { &DXVA2_ModeVC1_D,          AV_CODEC_ID_VC1  },
   { &DXVA2_ModeVC1_D,          AV_CODEC_ID_WMV3 },
   // END
   { NULL,                      AV_CODEC_ID_NONE }
};

 CDXVA2VideoDecoder::CDXVA2VideoDecoder (   )

{
   PixFmt = FFMPEG_PIXFMT_YUY2;
   _Init (   );
}

 CDXVA2VideoDecoder::~CDXVA2VideoDecoder (   )
 
{
   Close (   );   
}

 void CDXVA2VideoDecoder::_Init (   )  
 
{
   Flag_InnerCodecCtx = FALSE;
   Flag_Opened        = FALSE;
   IntPixFmt          = FFMPEG_PIXFMT_NONE;
   DecoderFormat      = D3DFMT_UNKNOWN;
   D3DDevice          = NULL;
   VideoService       = NULL;
   VideoDecoder       = NULL;
   SurfaceWidth       = 0;
   SurfaceHeight      = 0;
   SurfaceAge         = 0;
   
   FrameWidth         = 0;
   FrameHeight        = 0;
   CodecID            = AV_CODEC_ID_NONE;
   CodecCtx           = NULL;

   memset (&DecoderGUID,0,sizeof(DecoderGUID));
   memset (&DecoderConfig,0,sizeof(DecoderConfig));
   memset (&DXVAContext,0,sizeof(DXVAContext));
}

 void CDXVA2VideoDecoder::Callback_FreeAlignedMem (void* opaque,uint8_t* data)
 
{
   _aligned_free ((void*)data);   
}

 int CDXVA2VideoDecoder::Callback_GetBuffer (AVCodecContext* codec_ctx,AVFrame* frame,int flags)
 
{
   int i;
   
   if (frame->format != AV_PIX_FMT_DXVA2_VLD) {
      loge( "[ERROR] Incorrect pixel format!\n");
      return (AVERROR_UNKNOWN);
   }
   CDXVA2VideoDecoder* decoder = (CDXVA2VideoDecoder*)codec_ctx->opaque;
   Array1D<IDirect3DSurface9*>& surfaces = decoder->Surfaces;
   Array1D<SurfaceInfo>&  surface_infos  = decoder->SurfaceInfos;
   int old_unused = -1;
   for (i = 0; i < surfaces.Length; i++) {
      SurfaceInfo& info = surface_infos[i];
      if (!info.Flag_Used && (old_unused == -1 || info.Age < surface_infos[old_unused].Age)) old_unused = i;
   }
   if (old_unused == -1) {
      loge( "[ERROR] No free DXVA2 surface!\n");
      return (AVERROR(ENOMEM));
   }
   i = old_unused;
   IDirect3DSurface9* surface = surfaces[i];
   SurfaceParam* param = new SurfaceParam;
   param->Decoder = decoder;
   param->Surface = surface;
   frame->buf[0]  = av_buffer_create ((uint8_t*)surface,0,Callback_ReleaseBuffer,param,AV_BUFFER_FLAG_READONLY);
   if (frame->buf[0] == NULL) {
      delete param;
      loge( "[ERROR] No memory!\n");
      return (AVERROR(ENOMEM));
   }
   surface_infos[i].Flag_Used = TRUE;
   surface_infos[i].Age       = decoder->SurfaceAge++;
   frame->data[3] = (uint8_t*)surface;
   return (0);
}

 AVPixelFormat CDXVA2VideoDecoder::Callback_GetFormat (AVCodecContext* codec_ctx,const AVPixelFormat* pix_fmts)
 
{
   int i;
   
   for (i = 0; pix_fmts[i] != AV_PIX_FMT_NONE; i++) {
      if (pix_fmts[i] == AV_PIX_FMT_DXVA2_VLD) {
         return (AV_PIX_FMT_DXVA2_VLD);
      }
   }
   return (AV_PIX_FMT_NONE);
}

 void CDXVA2VideoDecoder::Callback_ReleaseBuffer (void* opaque,uint8_t* data)
 
{
   int i;

   SurfaceParam* param = (SurfaceParam*)opaque;
   Array1D<IDirect3DSurface9*>& surfaces = param->Decoder->Surfaces;
   Array1D<SurfaceInfo>& surface_infos   = param->Decoder->SurfaceInfos;
   for (i = 0; i < surfaces.Length; i++) {
      if (surfaces[i] == param->Surface) {
         surface_infos[i].Flag_Used = FALSE;
         break;
      }
   }
   delete param;
}

 void CDXVA2VideoDecoder::Close (   )
 
{
   int i;

   FFMPEG_CS_Lock (   );
   if (CodecCtx) {
      if (DestFrame) {
         av_frame_free (&DestFrame);
         DestFrame = nullptr;
      }
      if(CasheBlock) {
         _aligned_free ((void*)CasheBlock);
         CasheBlock = nullptr;
      }

      if (FrameWidth && FrameHeight) avcodec_close (CodecCtx);
      if (Flag_InnerCodecCtx) av_free (CodecCtx);
      else {
         CodecCtx->thread_count = BackupCodecCtx.thread_count;
         CodecCtx->opaque       = BackupCodecCtx.opaque;
         CodecCtx->get_buffer2  = BackupCodecCtx.get_buffer2;
         CodecCtx->get_format   = BackupCodecCtx.get_format;
         CodecCtx->codec_id     = BackupCodecCtx.codec_id;
         CodecCtx->pix_fmt      = BackupCodecCtx.pix_fmt;
      }
   }
   if (!(!Surfaces)) {
      for (i = 0; i < Surfaces.Length; i++) {
         if (Surfaces[i]) {
            Surfaces[i]->Release (   );
            Surfaces[i] = NULL;
         }
      }
   }
   if (VideoDecoder) VideoDecoder->Release (   );
   if (VideoService) VideoService->Release (   );
   if (D3DDevice   ) D3DDevice->Release    (   );
   Surfaces.Delete     (   );
   SurfaceInfos.Delete (   );
   SrcBuffer.Delete    (   );
   _Init (   );
   FFMPEG_CS_Unlock (   );
}

 int CDXVA2VideoDecoder::CopyFrame (AVFrame* s_frame,AVFrame* d_frame)
// GPU 메모리에 있는 디코딩 된 프레임 데이터를 CPU 메모리로 복사한다.
// 주의: d_frame에 복사되는 프레임 데이터의 픽셀 포맷은 PixFmt 값에 상관 없이 항상 NV12 포맷이다.
{
   HRESULT hr;
   
   if (s_frame->format != AV_PIX_FMT_DXVA2_VLD) return (1);
   if (s_frame->data[3] == NULL) return (2);
   IDirect3DSurface9* surface = (IDirect3DSurface9*)s_frame->data[3];
   D3DSURFACE_DESC surface_desc;
   hr = surface->GetDesc (&surface_desc);
   if (FAILED(hr)) return (3);
   D3DLOCKED_RECT l_rect;
   hr = surface->LockRect (&l_rect,NULL,D3DLOCK_READONLY);
   if (FAILED(hr)) {
      loge( "[ERROR] Unable to lock the DXVA2 surface.\n");
      return (4);
   }
   if (FALSE == Flag_CreatedMemory) 
   {
      CreateFrameNV12 (FrameWidth,FrameHeight,l_rect.Pitch,d_frame);
      Flag_CreatedMemory = TRUE;
   }
   CopyFrameNV12   ((byte*)l_rect.pBits,d_frame->data[0],d_frame->data[1],surface_desc.Height,FrameHeight,l_rect.Pitch);
   surface->UnlockRect (   );
   return (DONE);
}

 void CDXVA2VideoDecoder::CreateFrameNV12 (int fr_width,int fr_height,int pitch,AVFrame* d_frame)
 
{
   memset (d_frame->buf     ,0,sizeof(d_frame->buf));
   memset (d_frame->data    ,0,sizeof(d_frame->data));
   memset (d_frame->linesize,0,sizeof(d_frame->linesize));
   d_frame->width   = fr_width;
   d_frame->height  = fr_height;
   d_frame->format  = FFMPEG_PIXFMT_NV12;
   int buf_size     = fr_height * pitch;
   int buf_size_y   = buf_size + FF_INPUT_BUFFER_PADDING_SIZE;
   int buf_size_uv  = buf_size / 2 + FF_INPUT_BUFFER_PADDING_SIZE;
   byte* buffer_y   = (byte*)_aligned_malloc (buf_size_y ,64);
   byte* buffer_uv  = (byte*)_aligned_malloc (buf_size_uv,64);
   d_frame->buf[0]  = av_buffer_create (buffer_y,buf_size_y,Callback_FreeAlignedMem,this,0);
   d_frame->data[0] = buffer_y;
   d_frame->linesize[0] = pitch;
   d_frame->buf[1]  = av_buffer_create (buffer_uv,buf_size_uv,Callback_FreeAlignedMem,this,0);
   d_frame->data[1] = buffer_uv;
   d_frame->linesize[1] = pitch;
}

 void CDXVA2VideoDecoder::CopyFrameNV12 (const byte* s_buffer,byte* d_buffer_y,byte* d_buffer_uv,int s_height,int d_height,int pitch)
 
{
   int cpu_flags = av_get_cpu_flags (   );
   if (cpu_flags & AV_CPU_FLAG_SSE4) CopyFrameNV12_SSE4 (s_buffer,d_buffer_y,d_buffer_uv,s_height,d_height,pitch);
   else CopyFrameNV12_Basic (s_buffer,d_buffer_y,d_buffer_uv,s_height,d_height,pitch);
}

 void CDXVA2VideoDecoder::CopyFrameNV12_Basic (const byte* s_buffer,byte* d_buffer_y,byte* d_buffer_uv,int s_height,int d_height,int pitch)
 
{
   uint s_buf_size = d_height * pitch;
   CopyMemory_Basic (s_buffer,s_buf_size,d_buffer_y);
   CopyMemory_Basic (s_buffer + s_height * pitch,s_buf_size / 2,d_buffer_uv);
}

 //  CopyFrame( )
 //
 //  COPIES VIDEO FRAMES FROM USWC MEMORY TO WB SYSTEM MEMORY VIA CACHED BUFFER
 //    ASSUMES PITCH IS A MULTIPLE OF 64B CACHE LINE SIZE, WIDTH MAY NOT BE

 typedef		unsigned int		UINT;
#define		CACHED_BUFFER_SIZE	4096	

 void	_CopyFrame(void * pSrc,void * pDest,void * pCacheBlock,UINT width,UINT height,UINT pitch)
 {
    __m128i		x0,x1,x2,x3;
    __m128i		*pLoad;
    __m128i		*pStore;
    __m128i		*pCache;
    UINT		x,y,yLoad,yStore;
    UINT		rowsPerBlock;
    UINT		width64;
    UINT		extraPitch;


    rowsPerBlock = CACHED_BUFFER_SIZE / pitch;
    width64 = (width + 63) & ~0x03f;
    extraPitch = (pitch - width64) / 16;

    pLoad = (__m128i *)pSrc;
    pStore = (__m128i *)pDest;

    //  COPY THROUGH 4KB CACHED BUFFER
    for(y = 0; y < height; y += rowsPerBlock)
    {
       //  ROWS LEFT TO COPY AT END
       if(y + rowsPerBlock > height)
          rowsPerBlock = height - y;

       pCache = (__m128i *)pCacheBlock;

       _mm_mfence();

       // LOAD ROWS OF PITCH WIDTH INTO CACHED BLOCK
       for(yLoad = 0; yLoad < rowsPerBlock; yLoad++)
       {
          // COPY A ROW, CACHE LINE AT A TIME
          for(x = 0; x < pitch; x += 64)
          {
             x0 = _mm_stream_load_si128(pLoad + 0);
             x1 = _mm_stream_load_si128(pLoad + 1);
             x2 = _mm_stream_load_si128(pLoad + 2);
             x3 = _mm_stream_load_si128(pLoad + 3);

             _mm_store_si128(pCache + 0,x0);
             _mm_store_si128(pCache + 1,x1);
             _mm_store_si128(pCache + 2,x2);
             _mm_store_si128(pCache + 3,x3);

             pCache += 4;
             pLoad += 4;
          }
       }

       _mm_mfence();

       pCache = (__m128i *)pCacheBlock;

       // STORE ROWS OF FRAME WIDTH FROM CACHED BLOCK
       for(yStore = 0; yStore < rowsPerBlock; yStore++)
       {
          // copy lign row, cache line at a time
          for(x = 0; x < width64; x += 64)
          {
             x0 = _mm_load_si128(pCache);
             x1 = _mm_load_si128(pCache + 1);
             x2 = _mm_load_si128(pCache + 2);
             x3 = _mm_load_si128(pCache + 3);

             _mm_stream_si128(pStore,x0);
             _mm_stream_si128(pStore + 1,x1);
             _mm_stream_si128(pStore + 2,x2);
             _mm_stream_si128(pStore + 3,x3);

             pCache += 4;
             pStore += 4;
          }

          pCache += extraPitch;
          pStore += extraPitch;
       }
    }
 }

 void CDXVA2VideoDecoder::CopyFrameNV12_SSE4 (const byte* s_buffer,byte* d_buffer_y,byte* d_buffer_uv,int s_height,int d_height,int pitch)

{
   uint s_buf_size = d_height * pitch;

   if (1) {
      if (!CasheBlock)
         CasheBlock = (byte*)_aligned_malloc (CACHED_BUFFER_SIZE,64);
      _CopyFrame ((void*)s_buffer,d_buffer_y,CasheBlock,pitch,s_height,pitch);
      _CopyFrame ((void*)(s_buffer + s_height * pitch),d_buffer_uv,CasheBlock,pitch / 2,s_height,pitch / 2);
   }
   else {
      CopyMemory_SSE4 (s_buffer,s_buf_size,d_buffer_y);
      CopyMemory_SSE4 (s_buffer + s_height * pitch,s_buf_size / 2,d_buffer_uv);
   }
}

 void CDXVA2VideoDecoder::CopyMemory_Basic (const void* s_buffer,uint s_buf_size,void* d_buffer)
 
{
   memcpy (d_buffer,s_buffer,s_buf_size);
}

 void CDXVA2VideoDecoder::CopyMemory_SSE4 (const void* s_buffer,uint s_buf_size,void* d_buffer)
 
{
   uint i;
   
   static const size_t regsInLoop = sizeof(size_t) * 2;
   if (s_buffer == NULL || d_buffer == NULL) return;
   // If memory is not aligned, use memcpy.
   bool isAligned = (((size_t)(s_buffer) | (size_t)(d_buffer)) & 0xF) == 0;
   if (!isAligned) {
      memcpy (d_buffer,s_buffer,s_buf_size);
      return;
   }
   __m128i xmm0, xmm1, xmm2, xmm3, xmm4, xmm5, xmm6, xmm7;
   #ifdef _M_X64
   __m128i xmm8, xmm9, xmm10, xmm11, xmm12, xmm13, xmm14, xmm15;
   #endif
   size_t reminder = s_buf_size & (regsInLoop * sizeof(xmm0) - 1); // Copy 128 or 256 bytes every loop
   size_t end = 0;
   __m128i* pTrg = (__m128i*)d_buffer;
   __m128i* pTrgEnd = pTrg + ((s_buf_size - reminder) >> 4);
   __m128i* pSrc = (__m128i*)s_buffer;
   // Make sure source is synced - doesn't hurt if not needed.
   _mm_sfence();
   while (pTrg < pTrgEnd) {
      // _mm_stream_load_si128 emits the Streaming SIMD Extensions 4 (SSE4.1) instruction MOVNTDQA
      // Fastest method for copying GPU RAM. Available since Penryn (45nm Core 2 Duo/Quad)
      xmm0  = _mm_stream_load_si128(pSrc);
      xmm1  = _mm_stream_load_si128(pSrc + 1);
      xmm2  = _mm_stream_load_si128(pSrc + 2);
      xmm3  = _mm_stream_load_si128(pSrc + 3);
      xmm4  = _mm_stream_load_si128(pSrc + 4);
      xmm5  = _mm_stream_load_si128(pSrc + 5);
      xmm6  = _mm_stream_load_si128(pSrc + 6);
      xmm7  = _mm_stream_load_si128(pSrc + 7);
      #ifdef _M_X64 // Use all 16 xmm registers
      xmm8  = _mm_stream_load_si128(pSrc + 8);
      xmm9  = _mm_stream_load_si128(pSrc + 9);
      xmm10 = _mm_stream_load_si128(pSrc + 10);
      xmm11 = _mm_stream_load_si128(pSrc + 11);
      xmm12 = _mm_stream_load_si128(pSrc + 12);
      xmm13 = _mm_stream_load_si128(pSrc + 13);
      xmm14 = _mm_stream_load_si128(pSrc + 14);
      xmm15 = _mm_stream_load_si128(pSrc + 15);
      #endif
      pSrc += regsInLoop;
      // _mm_store_si128 emit the SSE2 intruction MOVDQA (aligned store)
      _mm_store_si128(pTrg     , xmm0);
      _mm_store_si128(pTrg +  1, xmm1);
      _mm_store_si128(pTrg +  2, xmm2);
      _mm_store_si128(pTrg +  3, xmm3);
      _mm_store_si128(pTrg +  4, xmm4);
      _mm_store_si128(pTrg +  5, xmm5);
      _mm_store_si128(pTrg +  6, xmm6);
      _mm_store_si128(pTrg +  7, xmm7);
      #ifdef _M_X64 // Use all 16 xmm registers
      _mm_store_si128(pTrg +  8, xmm8);
      _mm_store_si128(pTrg +  9, xmm9);
      _mm_store_si128(pTrg + 10, xmm10);
      _mm_store_si128(pTrg + 11, xmm11);
      _mm_store_si128(pTrg + 12, xmm12);
      _mm_store_si128(pTrg + 13, xmm13);
      _mm_store_si128(pTrg + 14, xmm14);
      _mm_store_si128(pTrg + 15, xmm15);
      #endif
      pTrg += regsInLoop;
   }
   // Copy in 16 byte steps.
   if (reminder >= 16) {
      s_buf_size = reminder;
      reminder = s_buf_size & 15;
      end = s_buf_size >> 4;
      for (i = 0; i < end; ++i)
         pTrg[i] = _mm_stream_load_si128 (pSrc + i);
   }
   // Copy last bytes - shouldn't happen as strides are modulu 16
   if (reminder) {
      __m128i temp = _mm_stream_load_si128(pSrc + end);
      char* ps = (char*)(&temp);
      char* pt = (char*)(pTrg + end);
      for (i = 0; i < reminder; ++i) pt[i] = ps[i];
   }
}

 int CDXVA2VideoDecoder::CreateD3DDevice (   )
// D3DDevice 값을 얻는다. 
{
   if (_Direct3D == NULL) return (1);
   D3DDISPLAYMODE dm = {0};
   if (_Direct3D->GetAdapterDisplayMode (D3DADAPTER_DEFAULT,&dm) != D3D_OK) return (2);
   D3DPRESENT_PARAMETERS params;
   memset (&params,0,sizeof(D3DPRESENT_PARAMETERS));
   params.Windowed         = TRUE;
   params.BackBufferWidth  = 640;
   params.BackBufferHeight = 480;
   params.BackBufferCount  = 0;
   params.BackBufferFormat = dm.Format;
   params.SwapEffect       = D3DSWAPEFFECT_DISCARD;
   params.Flags            = D3DPRESENTFLAG_VIDEO;
   if (_Direct3D->CreateDevice (D3DADAPTER_DEFAULT,D3DDEVTYPE_HAL,GetShellWindow(),D3DCREATE_SOFTWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED | D3DCREATE_FPU_PRESERVE,&params,&D3DDevice) != D3D_OK) return (3);
   return (DONE);
}

 int CDXVA2VideoDecoder::CreateRenderTarget (   )
// SurfaceWidth, SurfaceHeight, Surfaces 값을 얻는다. 
{
   int n_surfaces = GetNumSurfacesRequired (   );
   Surfaces.Create (n_surfaces);
   Surfaces.Clear  (   );
   SurfaceInfos.Create (n_surfaces);
   SurfaceInfos.Clear  (   );
   SurfaceWidth   = FFALIGN(FrameWidth ,64);
   SurfaceHeight  = FFALIGN(FrameHeight,32);
   HRESULT hr = VideoService->CreateSurface (SurfaceWidth,SurfaceHeight,n_surfaces - 1,DecoderFormat,D3DPOOL_DEFAULT,0,DXVA2_VideoDecoderRenderTarget,Surfaces,NULL);
   if (FAILED(hr)) return (1);
   return (DONE);   
}

 int CDXVA2VideoDecoder::CreateVideoDecoder (   )
// VideoDecoder 값을 얻는다. 
{
   DXVA2_VideoDesc desc;
   memset (&desc,0,sizeof(desc));
   desc.SampleWidth  = FrameWidth;
   desc.SampleHeight = FrameHeight;
   desc.Format       = DecoderFormat;
   HRESULT hr = VideoService->CreateVideoDecoder (DecoderGUID,&desc,&DecoderConfig,Surfaces,Surfaces.Length,&VideoDecoder);
   if (FAILED(hr)) return (1);
   return (DONE);
}

 int CDXVA2VideoDecoder::CreateVideoService (   )
// VideoService 값을 얻는다. 
{
   HRESULT hr = DXVA2CreateVideoService (D3DDevice,IID_IDirectXVideoDecoderService,(void**)&VideoService);
   if (FAILED(hr)) return (1);
   return (DONE);
}

 int CDXVA2VideoDecoder::Decode (const byte* s_buffer,int s_buf_size,AVFrame* d_frame)
// d_frame: 외부에서 av_frame_alloc()에 의해 할당되어야 한다.
// 주의: d_frame에 복사되는 프레임 데이터의 픽셀 포맷은 PixFmt 값에 상관 없이 항상 NV12 포맷이다.
{
   if (!Flag_Opened) return (1);
   int r_code  = DONE;
   int sb_size = s_buf_size + FFMPEG_BUFFER_PADDING_SIZE;
   if (SrcBuffer.Length < sb_size) SrcBuffer.Create (sb_size);
   memcpy ((byte*)SrcBuffer,s_buffer,s_buf_size);
   memset ((byte*)SrcBuffer + s_buf_size,0,FFMPEG_BUFFER_PADDING_SIZE);
   AVPacket avpkt;
   av_init_packet (&avpkt);
   avpkt.data = (byte*)SrcBuffer;
   avpkt.size = s_buf_size;
   AVFrame* s_frame = av_frame_alloc (   );
   int got_picture = FALSE;
   CodecCtx->refcounted_frames = 1;
   int n_read = avcodec_decode_video2 (CodecCtx,s_frame,&got_picture,&avpkt);
   if (n_read < 0) {
      r_code = 2;
      goto end;
   }
   DecodedFrameSize (s_frame->width, s_frame->height); // jun: 디코딩 영상크기 저장.
   if (s_frame->width != FrameWidth || s_frame->height != FrameHeight) // jun : 디코딩한 영상의 크기가 다르면 리턴
      return (3);
   if (got_picture) {
      if (CopyFrame (s_frame,d_frame)) {
         r_code = 3;
         goto end;
      }
   }
   else r_code = - 1;
   end:;
   av_frame_free (&s_frame);
   return (r_code);
}

 int CDXVA2VideoDecoder::Decode (const byte* s_buffer,int s_buf_size,byte* d_buffer,const int* pitches,int n_planes)

{
   int i;
   int r_code          = DONE;
   //AVFrame* s_frame   = NULL;
   AVFrame*& s_frame = DestFrame;
   AVFrame* d_frame    = NULL;
   SwsContext* sws_ctx = NULL;
   if (!s_frame)
   s_frame = av_frame_alloc (   );
   r_code = Decode (s_buffer,s_buf_size,s_frame);
   if (r_code == DONE) {
      sws_ctx = sws_getContext (FrameWidth,FrameHeight,IntPixFmt,FrameWidth,FrameHeight,PixFmt,SWS_FAST_BILINEAR,NULL,NULL,NULL);
      if (sws_ctx == NULL) {
         r_code = 4;
         goto end;
      }
      d_frame = av_frame_alloc (   );
      avpicture_fill ((AVPicture*)d_frame,d_buffer,PixFmt,FrameWidth,FrameHeight);
      if (pitches) {
         if (n_planes > 4) n_planes = 4;
         for (i = 0; i < n_planes; i++) d_frame->linesize[i] = pitches[i];
      }
      sws_scale (sws_ctx,s_frame->data,s_frame->linesize,0,FrameHeight,d_frame->data,d_frame->linesize);
      sws_freeContext (sws_ctx);
      av_free (d_frame);
   }
   end:;
   //av_frame_free (&s_frame);
   return (r_code);
}

 int CDXVA2VideoDecoder::Decode( const byte *s_buffer,int s_buf_size,BGRImage &d_image )
 {
   if (!Flag_Opened) return (1);
   if (d_image.Width != FrameWidth || d_image.Height != FrameHeight) d_image.Create (FrameWidth,FrameHeight);
   byte *d_buffer = NULL;
   int e_code = Decode (s_buffer,s_buf_size,d_buffer);
   if (e_code == DONE) {
      switch (PixFmt) {
         case AV_PIX_FMT_YUV420P:
            ConvertYV12ToBGR (d_buffer,d_image,YV12_FORMAT_STANDARD);
            break;
         case AV_PIX_FMT_YUYV422:
            ConvertYUY2ToBGR (d_buffer,d_image);
            break;
         case AV_PIX_FMT_BGR24:
            memcpy ((BGRPixel*)d_image,d_buffer,d_image.GetLineLength() * d_image.Height);
            break;
         default:
            d_image.Clear (   );
            return (3);
      }
   }
   else if (e_code < 0) return (-1);
   else return (2);
   return (DONE);
 }

 int CDXVA2VideoDecoder::Flush (   )
 
{
   if (CodecCtx == NULL) return (1);
   avcodec_flush_buffers (CodecCtx);
   return (DONE);
}

 int CDXVA2VideoDecoder::GetDecoderDevice (   )
// DecoderGUID와 DecoderFormat의 값을 얻는다. 
{
   int i,j;
   HRESULT hr;

   int r_code     = DONE;
   UINT n_dds     = 0;
   GUID* dds      = NULL;
   UINT n_rts     = 0;
   D3DFORMAT* rts = NULL; 
   // 디코더 디바이스 목록을 얻는다.
   hr = VideoService->GetDecoderDeviceGuids (&n_dds,&dds);
   if (FAILED(hr)) {
      r_code = 1;
      goto end;
   }
   DecoderFormat = D3DFMT_UNKNOWN;
   for (i = 0; DXVA2Modes[i].Guid; i++) {
      DXVA2Mode &mode = DXVA2Modes[i];
      // 지정된 코덱에 해당하는 모드를 찾는다.
      if (mode.CodecID != CodecID) continue;
      // 해당 모드를 지원하는 디코더 디바이스를 찾는다.
      for (j = 0; j < (int)n_dds; j++)
         if (IsEqualGUID (*mode.Guid,dds[j])) break;
      if (j == (int)n_dds) continue;
      DecoderGUID = *mode.Guid;
      // 찾은 디코더 디바이스의 렌더 타겟(픽셀 포맷) 목록을 얻는다.
      hr = VideoService->GetDecoderRenderTargets (*mode.Guid,&n_rts,&rts);
      if (FAILED(hr)) {
         r_code = 2;
         goto end;
      }
      // 지원되는 픽셀 포맷을 찾는다.
      DecoderFormat = D3DFMT_UNKNOWN;
      IntPixFmt     = AV_PIX_FMT_NONE;
      for (j = 0; j < (int)n_rts; j++) {
         if (rts[j] == (D3DFORMAT)MAKEFOURCC('N','V','1','2')) {
            DecoderFormat = rts[j];
            IntPixFmt = FFMPEG_PIXFMT_NV12;
            goto end;
         }
      }
   }
   end:
   if (r_code == DONE && DecoderFormat == D3DFMT_UNKNOWN) r_code = -1;
   if (dds != NULL) CoTaskMemFree (dds);
   if (rts != NULL) CoTaskMemFree (rts);
   return (r_code);
}

 int CDXVA2VideoDecoder::GetDecoderConfig (   )
// DecoderConfig 값을 얻는다. 
{
   int i;
   HRESULT hr;
   
   UINT n_cfgs = 0;
   DXVA2_ConfigPictureDecode* cfgs = NULL;
   DXVA2_VideoDesc desc;
   memset (&desc,0,sizeof(desc));
   desc.SampleWidth  = FrameWidth;
   desc.SampleHeight = FrameHeight;
   desc.Format       = DecoderFormat;
   hr = VideoService->GetDecoderConfigurations (DecoderGUID,&desc,NULL,&n_cfgs,&cfgs);
   if (FAILED(hr)) return (1);
   int best_score = 0;
   for (i = 0; i < (int)n_cfgs; i++) {
      DXVA2_ConfigPictureDecode& cfg = cfgs[i];
      int score = 0;
      if (cfg.ConfigBitstreamRaw == 1) score = 1;
      else if (CodecID == AV_CODEC_ID_H264 && cfg.ConfigBitstreamRaw == 2) score = 2;
      else continue;
      if (IsEqualGUID (cfg.guidConfigBitstreamEncryption,DXVA2_NoEncrypt)) score += 16;
      if (score > best_score) {
         best_score    = score;
         DecoderConfig = cfg;
      }
   }
   if (cfgs != NULL) CoTaskMemFree (cfgs);
   if (best_score <= 0) return (2);
   return (DONE); 
}

 int CDXVA2VideoDecoder::GetNumSurfacesRequired (   )
 
{
   int n_surfaces = 4;
   if (CodecID == AV_CODEC_ID_H264) n_surfaces += 8;
   else n_surfaces += 2;
   return (n_surfaces);
}

 int CDXVA2VideoDecoder::OpenAVCodec (   )
 
{
   // 주의: thread_count 값이 1보다 크면 avcodec_decode_video2() 함수 호출 시 다운된다.
   CodecCtx->thread_count = 1;
   CodecCtx->opaque       = this;
   CodecCtx->get_buffer2  = Callback_GetBuffer;
   CodecCtx->get_format   = Callback_GetFormat;
   AVCodec* codec = avcodec_find_decoder (CodecID);
   if (codec == NULL) return (1);
   if (avcodec_open2 (CodecCtx,codec,NULL) < 0) return (2);
   FrameWidth  = CodecCtx->width;
   FrameHeight = CodecCtx->height;
   if (CodecID == AV_CODEC_ID_H264 && CodecCtx->pix_fmt != FFMPEG_PIXFMT_YV12) return (3);
   return (DONE);
}

 int CDXVA2VideoDecoder::OpenDecoder (AVCodecID codec_id,int frm_width,int frm_height)
 
{
   Close (   );
   FFMPEG_CS_Lock (   );
   CodecCtx = avcodec_alloc_context3 (NULL);
   if (CodecCtx == NULL) {
      FFMPEG_CS_Unlock (   );
      Close (   );
      return (1);
   }
   Flag_InnerCodecCtx = TRUE;
   CodecID            = codec_id;
   CodecCtx->width    = frm_width;
   CodecCtx->height   = frm_height;
   CodecCtx->pix_fmt  = FFMPEG_PIXFMT_YV12;
   int e_code = OpenDecoder (   );
   FFMPEG_CS_Unlock (   );
   if (e_code != DONE) {
      Close (   );
      return (e_code + 1);
   }
   return (DONE);
}

 int CDXVA2VideoDecoder::OpenDecoder (AVCodecContext* codec_ctx)
 
{
   Close (   );
   BackupCodecCtx.thread_count = codec_ctx->thread_count;
   BackupCodecCtx.opaque       = codec_ctx->opaque;
   BackupCodecCtx.get_buffer2  = codec_ctx->get_buffer2;
   BackupCodecCtx.get_format   = codec_ctx->get_format;
   BackupCodecCtx.codec_id     = codec_ctx->codec_id;
   BackupCodecCtx.pix_fmt      = codec_ctx->pix_fmt;
   Flag_InnerCodecCtx = FALSE;
   CodecID  = codec_ctx->codec_id;
   CodecCtx = codec_ctx;
   FFMPEG_CS_Lock (   );
   int e_code = OpenDecoder (   );
   FFMPEG_CS_Unlock  (   );
   if (e_code) Close (   );
   return (e_code);
}

 int CDXVA2VideoDecoder::OpenDecoder (   )
 
{
   int cpu_flags = av_get_cpu_flags (   );
   if (!(cpu_flags & AV_CPU_FLAG_SSE4)) logw("[WARNING] SSE4 not supported.\n");
   if (OpenAVCodec        (   )) return (1);
   if (CreateD3DDevice    (   )) return (2);
   if (CreateVideoService (   )) return (3);
   if (GetDecoderDevice   (   )) return (4);
   if (GetDecoderConfig   (   )) return (5);
   if (CreateRenderTarget (   )) return (6);
   if (CreateVideoDecoder (   )) return (7);
   SetHWAccelContext (   );
   Flag_Opened = TRUE;
   return (DONE);
}

 void CDXVA2VideoDecoder::SetHWAccelContext (   )
 
{
   memset (&DXVAContext,0,sizeof(DXVAContext));
   CodecCtx->hwaccel_context = &DXVAContext;
   DXVAContext.cfg           = &DecoderConfig;
   DXVAContext.decoder       = VideoDecoder;
   DXVAContext.surface       = Surfaces;
   DXVAContext.surface_count = Surfaces.Length;
}

}

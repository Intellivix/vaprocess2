#include "Win32CL/Utilities.h"
#include "bccl.h"
#include <afxcmn.h>

 namespace Win32CL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: CPauseDialog
//
///////////////////////////////////////////////////////////////////////////////

 class CPauseDialog : public CDialog

{
   public:
      const static int FORM_WIDTH;
      const static int FORM_MARGIN_X;
      const static int FORM_MARGIN_Y;
      const static int FORM_BUTTON_HEIGHT;
      const static int FORM_TITLEBAR_HEIGHT;

   protected:
      int WidthDBU;
      int HeightDBU;
      int *Flag_Cont;

   protected:
      CButton CBContinue;

   public:
      CPauseDialog (int width_dbu,int height_dbu,int *flag_cont);

   protected:
      virtual void OnCancel      (   );
      virtual BOOL OnInitDialog  (   );
      virtual void PostNcDestroy (   );
};

 const int CPauseDialog::FORM_WIDTH           = 60;
 const int CPauseDialog::FORM_MARGIN_X        = 8;
 const int CPauseDialog::FORM_MARGIN_Y        = 8;
 const int CPauseDialog::FORM_BUTTON_HEIGHT   = 16;
 const int CPauseDialog::FORM_TITLEBAR_HEIGHT = 11;

 CPauseDialog::CPauseDialog (int width_dbu,int height_dbu,int *flag_cont)

{
   WidthDBU   = width_dbu;
   HeightDBU  = height_dbu;
   Flag_Cont  = flag_cont;
   *Flag_Cont = FALSE;
}

 void CPauseDialog::OnCancel (   )

{
   DestroyWindow (   );
}

 BOOL CPauseDialog::OnInitDialog (   )

{
   int dlg_width,dlg_height;
   int margin_x,margin_y,bt_height;
   float factor_x,factor_y;
   CRect rect;

   GetWindowRect (&rect);
   dlg_width  = rect.right - rect.left;
   dlg_height = rect.bottom - rect.top;
   factor_x   = (float)dlg_width  / WidthDBU;
   factor_y   = (float)dlg_height / HeightDBU;
   GetClientRect (&rect);
   dlg_width = rect.right;
   margin_x  = (int)(FORM_MARGIN_X * factor_x);
   margin_y  = (int)(FORM_MARGIN_Y * factor_y);
   bt_height = (int)(FORM_BUTTON_HEIGHT * factor_y);
   rect.left   = margin_x;
   rect.top    = margin_y;
   rect.right  = dlg_width - margin_x;
   rect.bottom = rect.top + bt_height;
   CBContinue.Create (_T("Resume"),WS_CHILD | WS_VISIBLE | WS_TABSTOP | BS_DEFPUSHBUTTON,rect,this,IDCANCEL);
   CDialog::OnInitDialog (   );
   return (TRUE);
}

 void CPauseDialog::PostNcDestroy (   )

{
   *Flag_Cont = TRUE;
   delete this;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CTextInputDialog
//
///////////////////////////////////////////////////////////////////////////////

 class CTextInputDialog : public CDialog

{
   public:
      const static int   FORM_WIDTH;
      const static int   FORM_MARGIN_X;
      const static int   FORM_MARGIN_Y;
      const static int   FORM_MARGIN_B;
      const static float FORM_CHAR_WIDTH;
      const static int   FORM_CHAR_HEIGHT;
      const static int   FORM_EDIT_HEIGHT;
      const static int   FORM_BUTTON_WIDTH;
      const static int   FORM_BUTTON_HEIGHT;
      const static int   FORM_TITLEBAR_HEIGHT;
      const static int   IDC_EDIT_INPUT;

   protected:
      int Length;
      int NLines;
      int WidthDBU;
      int HeightDBU;
      LPTSTR  DstString;
      LPCTSTR Message;
      LPCTSTR SrcString;

   protected:
      CEdit   Edit_Input;
      CButton Button_OK;
      CButton Button_Cancel;
      CStatic Static_Message;

   public:
      CTextInputDialog (int width_dbu,int height_dbu,LPCTSTR message,int n_lines,LPCTSTR s_string,LPTSTR d_string,int length);
   
   protected:
      virtual void OnCancel     (   );
      virtual BOOL OnInitDialog (   );
      virtual void OnOK         (   );
};
 
 const int   CTextInputDialog::FORM_WIDTH           = 230;
 const int   CTextInputDialog::FORM_MARGIN_X        = 8;
 const int   CTextInputDialog::FORM_MARGIN_Y        = 6;
 const int   CTextInputDialog::FORM_MARGIN_B        = 6;
 const float CTextInputDialog::FORM_CHAR_WIDTH      = 3.8f;
 const int   CTextInputDialog::FORM_CHAR_HEIGHT     = 10;
 const int   CTextInputDialog::FORM_EDIT_HEIGHT     = 13;
 const int   CTextInputDialog::FORM_BUTTON_WIDTH    = 48;
 const int   CTextInputDialog::FORM_BUTTON_HEIGHT   = 14;
 const int   CTextInputDialog::FORM_TITLEBAR_HEIGHT = 11;
 const int   CTextInputDialog::IDC_EDIT_INPUT       = 32001;
 
 int FormatString (LPCTSTR s_string,LPTSTR d_string);

 CTextInputDialog::CTextInputDialog (int width_dbu,int height_dbu,LPCTSTR message,int n_lines,LPCTSTR s_string,LPTSTR d_string,int length)

{
   WidthDBU  = width_dbu;
   HeightDBU = height_dbu;
   Message   = message;
   NLines    = n_lines;
   SrcString = s_string;
   DstString = d_string;
   Length    = length;
}
 
 void CTextInputDialog::OnCancel (   )

{
   DstString[0] = 0;
   CDialog::OnCancel (   );
}

 BOOL CTextInputDialog::OnInitDialog (   )

{
   int bt_width,bt_height;
   int dg_width,dg_height;
   int ch_height,ed_height;
   int margin_x,margin_y,margin_b;
   float factor_x,factor_y;
   CRect rect;

   GetWindowRect (&rect);
   dg_width  = rect.right - rect.left;
   dg_height = rect.bottom - rect.top;
   factor_x  = (float)dg_width  / WidthDBU;
   factor_y  = (float)dg_height / HeightDBU;
   GetClientRect (&rect);
   dg_width  = rect.right;
   margin_x  = (int)(FORM_MARGIN_X * factor_x);
   margin_y  = (int)(FORM_MARGIN_Y * factor_y);
   margin_b  = (int)(FORM_MARGIN_B * factor_x);
   ch_height = (int)(FORM_CHAR_HEIGHT * factor_y);
   ed_height = (int)(FORM_EDIT_HEIGHT * factor_y);
   bt_width  = (int)(FORM_BUTTON_WIDTH  * factor_x);
   bt_height = (int)(FORM_BUTTON_HEIGHT * factor_y);
   rect.left   = margin_x;
   rect.top    = margin_y;
   rect.right  = dg_width - margin_x;
   rect.bottom = rect.top + NLines * ch_height;
   Static_Message.Create (Message,WS_CHILD|WS_VISIBLE|SS_LEFTNOWORDWRAP,rect,this);
   rect.top    = rect.bottom + margin_y;
   rect.bottom = rect.top    + ed_height;
   Edit_Input.CreateEx (WS_EX_CLIENTEDGE,_T("EDIT"),SrcString,WS_CHILD | WS_VISIBLE | WS_TABSTOP | ES_AUTOHSCROLL,
      rect.left,rect.top,dg_width - margin_x * 2,ed_height,m_hWnd,(HMENU)IDC_EDIT_INPUT);
   rect.left   = dg_width / 2 - (bt_width + margin_b);
   rect.top    = rect.bottom + margin_y;
   rect.right  = rect.left   + bt_width;
   rect.bottom = rect.top    + bt_height;
   Button_OK.Create (_T("Enter"),WS_CHILD|WS_VISIBLE|WS_TABSTOP|BS_DEFPUSHBUTTON,rect,this,IDOK);
   rect.left   = dg_width / 2 + margin_b;
   rect.right  = rect.left + bt_width;
   Button_Cancel.Create (_T("Cancel"),WS_CHILD|WS_VISIBLE|WS_TABSTOP|BS_PUSHBUTTON,rect,this,IDCANCEL);
   CDialog::OnInitDialog (   );
   return (TRUE);
}

 void CTextInputDialog::OnOK (   )

{
   Edit_Input.GetWindowText (DstString,Length);
   CDialog::OnOK (   );
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 int FormatString (LPCTSTR s_string,LPTSTR d_string)

{
   int i,j,n;
   int bi,bj;
   
   int width  = (int)((float)(CTextInputDialog::FORM_WIDTH - 2 * CTextInputDialog::FORM_MARGIN_X) / CTextInputDialog::FORM_CHAR_WIDTH) - 1;
   int height = 1;
   for (i = j = n = 0,bi = -1;   ;   ) {
      TCHAR c = d_string[j] = s_string[i];
      if (!c) break;
      else if (c == _T(' ')) bi = i, bj = j, n++;
      else if (c == _T('\n')) height++, n = 0;
      else if (n >= width) {
         if (bi == -1) d_string[++j] = _T('\n');
         else {
            i = bi, j = bj;
            d_string[j] = _T('\n');
            bi = -1;
         }
         height++;
         n = 0;
      }
      else n++;
      i++, j++;
   }
   return (height);
}

 void OpenMessageBox (LPCTSTR format,...)
 
{
   TCHAR   message[MAX_BUFFER_SIZE];
   va_list arg_list;

   va_start   (arg_list,format);
   _vstprintf (message,format,arg_list);
   va_end     (arg_list);
   AfxMessageBox (message,MB_OK|MB_ICONEXCLAMATION);
}

 void OpenPauseBox (   )

{
   int flag_cont;
   MSG message;
   struct {
      DLGTEMPLATE Main;
      WORD Menu;
      WORD Class;
      WORD Title[6];
   } dlg_template;

   dlg_template.Main.style = DS_CENTER | WS_POPUP | WS_VISIBLE | WS_CAPTION;
   dlg_template.Main.dwExtendedStyle = 0;
   dlg_template.Main.cdit = 0;
   dlg_template.Main.x    = 0;
   dlg_template.Main.y    = 0;
   dlg_template.Main.cx   = CPauseDialog::FORM_WIDTH;
   dlg_template.Main.cy   = 2 * CPauseDialog::FORM_MARGIN_Y + CPauseDialog::FORM_BUTTON_HEIGHT + CPauseDialog::FORM_TITLEBAR_HEIGHT;
   dlg_template.Menu      = 0;
   dlg_template.Class     = 0;
   MultiByteToWideChar (CP_ACP,0,"Pause",6,(LPWSTR)dlg_template.Title,sizeof(dlg_template.Title));
   CPauseDialog *dlg = new CPauseDialog (dlg_template.Main.cx,dlg_template.Main.cy,&flag_cont);
   dlg->CreateIndirect (&dlg_template);
   for (   ;   ;   ) {
      if (flag_cont == TRUE) break;
      while (::PeekMessage (&message,NULL,0,0,PM_NOREMOVE)) AfxGetThread (   )->PumpMessage (   );
   }
}

 int OpenTextInputBox (LPCTSTR message,LPCTSTR s_string,LPTSTR d_string,int buffer_size)

{
   int   n_lines;
   TCHAR fm_message[MAX_BUFFER_SIZE];
   struct {
      DLGTEMPLATE Main;
      WORD Menu;
      WORD Class;
      WORD Title[15];
   } dlg_template;
   
   n_lines = FormatString (message,fm_message);
   dlg_template.Main.style = DS_MODALFRAME | WS_POPUP | WS_VISIBLE | WS_CAPTION | WS_SYSMENU;
   dlg_template.Main.dwExtendedStyle = 0;
   dlg_template.Main.cdit = 0;
   dlg_template.Main.x    = 0;
   dlg_template.Main.y    = 0;
   dlg_template.Main.cx   = CTextInputDialog::FORM_WIDTH;
   dlg_template.Main.cy   = (int)(4.65f * CTextInputDialog::FORM_MARGIN_Y + n_lines * CTextInputDialog::FORM_CHAR_HEIGHT + CTextInputDialog::FORM_EDIT_HEIGHT + CTextInputDialog::FORM_BUTTON_HEIGHT + CTextInputDialog::FORM_TITLEBAR_HEIGHT);
   dlg_template.Menu      = 0;
   dlg_template.Class     = 0;
   MultiByteToWideChar (CP_ACP,0,"Text Input Box",15,(LPWSTR)dlg_template.Title,sizeof(dlg_template.Title));
   CTextInputDialog dialog(dlg_template.Main.cx,dlg_template.Main.cy,fm_message,n_lines,s_string,d_string,buffer_size);
   dialog.InitModalIndirect (&dlg_template,AfxGetApp (   )->m_pMainWnd);
   if (dialog.DoModal (   ) == IDCANCEL) return (1);
   else return (0);
}

}

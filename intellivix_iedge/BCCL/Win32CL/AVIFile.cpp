#include "Win32CL/AVIFile.h"

#pragma comment(lib,"VFW32.lib")
#pragma comment(lib,"Winmm.lib")

 namespace Win32CL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: CAVIFile
//
///////////////////////////////////////////////////////////////////////////////

 CAVIFile::CAVIFile (   )

{
   Flag_CoInitialize = FALSE;
   Flag_AVIFileInit  = FALSE;
   NextFramePos      = 0;
   File              = NULL;
   Stream            = NULL;
   NumFrames         = 0;
   StartFramePos     = 0;
   EndFramePos       = 0;
   FrameWidth        = 0;
   FrameHeight       = 0;
   CodecID           = 0;
   FrameRate         = 0.0f;
   Flag_Repeat       = TRUE;
}

 CAVIFile::~CAVIFile (   )

{
   Close (   );
}

 int CAVIFile::AddEncodedFrame (const byte *s_buffer,int s_buf_size,int flag_key_frame)
 
{
   DWORD flags = 0;

   if (Stream == NULL) return (1);
   if (flag_key_frame) flags = AVIIF_KEYFRAME;
   if (AVIStreamWrite (Stream,NextFramePos,1,(byte *)s_buffer,s_buf_size,flags,NULL,NULL)) return (2);
   NextFramePos++;
   return (DONE);
}

 void CAVIFile::Close (   )

{
   if (Stream != NULL) {
      AVIStreamRelease (Stream);
      Stream = NULL;
   }
   if (File != NULL) {
      AVIFileRelease (File);
      File = NULL;
   }
   if (Flag_AVIFileInit) {
      AVIFileExit (   );
      Flag_AVIFileInit = FALSE;
   }
   if (Flag_CoInitialize) {
      CoUninitialize (   );
      Flag_CoInitialize = FALSE;
   }
   FrameBuffer.Delete (   );
   VideoFormat.Delete (   );
   NumFrames     = 0;
   StartFramePos = 0;
   EndFramePos   = 0;
   NextFramePos  = 0;
   FrameWidth    = 0;
   FrameHeight   = 0;
   CodecID       = 0;
   FrameRate     = 0.0f;
}

 int CAVIFile::ConvertToTimePos (int frame_pos)
 
{
   int time_pos = (int)(frame_pos / FrameRate * 1000.0f);
   return (time_pos);
}
 
 int CAVIFile::Create (LPCTSTR file_name,uint codec_id,int frm_width,int frm_height,float frame_rate)

{
   Close (   );
   CoInitialize (NULL);
   Flag_CoInitialize = TRUE;
   AVIFileInit (   );
   Flag_AVIFileInit = TRUE;
   if (AVIFileOpen (&File,file_name,OF_CREATE|OF_WRITE,NULL)) {
      File = NULL;
      Close (   );
      return (1);
   }
   AVISTREAMINFO si;
   ZeroMemory (&si,sizeof(AVISTREAMINFO));
   si.fccType    = streamtypeVIDEO;
   si.fccHandler = codec_id;
   si.dwScale    = 100;
   si.dwRate     = (int)(frame_rate * 100.0f);
   if (AVIFileCreateStream (File,&Stream,&si)) {
      Stream = NULL;
      Close (   );
      return (2);
   }
   VideoFormat.Create (sizeof(BITMAPINFOHEADER));
   GetVideoFormat (codec_id,frm_width,frm_height,(BITMAPINFOHEADER*)(byte*)VideoFormat);
   if (AVIStreamSetFormat (Stream,0,(byte*)VideoFormat,sizeof(BITMAPINFOHEADER))) {
      Close (   );
      return (4);
   }
   CodecID      = codec_id;
   FrameWidth   = frm_width;
   FrameHeight  = frm_height;
   FrameRate    = frame_rate;
   NextFramePos = 0;
   return (DONE);
}

 int CAVIFile::ExtractEncodedFrame (byte **p_d_buffer,int &d_buf_size,int &flag_key_frame)
 
{
   LONG buf_size;
   
   if (Stream == NULL) return (1);
   int e_code = AVIStreamRead (Stream,NextFramePos,1,(byte *)FrameBuffer,FrameBuffer.Length,&buf_size,NULL);
   if (e_code) return (2);
   if (AVIStreamIsKeyFrame (Stream,NextFramePos)) flag_key_frame = TRUE;
   else flag_key_frame = FALSE;
   *p_d_buffer = FrameBuffer;
   d_buf_size  = (int)buf_size;
   NextFramePos++;
   if (NextFramePos > EndFramePos) {
      if (Flag_Repeat) NextFramePos = StartFramePos;
      else NextFramePos = EndFramePos;
   }
   return (DONE);
}

 int CAVIFile::FindKeyFrame (int s_pos,int &d_pos,int flag_prev)
 
{
   d_pos = 0;
   if (Stream == NULL) return (1);
   if (s_pos < StartFramePos) s_pos = StartFramePos;
   else if (s_pos > EndFramePos) s_pos = EndFramePos;
   if (AVIStreamIsKeyFrame (Stream,s_pos)) {
      d_pos = s_pos;
      return (DONE);
   }
   if (flag_prev) {
      for (   ; s_pos >= StartFramePos; s_pos--) {
         if (AVIStreamIsKeyFrame (Stream,s_pos)) {
            d_pos = s_pos;
            return (DONE);
         }
      }
      d_pos = StartFramePos;
      return (2);
   }
   else {
      for (   ; s_pos <= EndFramePos; s_pos++) {
         if (AVIStreamIsKeyFrame (Stream,s_pos)) {
            d_pos = s_pos;
            return (DONE);
         }
      }
      d_pos = EndFramePos;
      return (3);
   }
}

 void CAVIFile::GetCodecFCC (char *codec_fcc)
 
{
   codec_fcc[4] = 0;
   memcpy (codec_fcc,&CodecID,4);
}

 void CAVIFile::GetTimeCode (int frame_pos,char *time_code)
 
{
   int time_pos = ConvertToTimePos (frame_pos);
   Time time;
   time.SetTimeInSecond     (time_pos / 1000);
   time.GetTimeInString_HMS (time_code);
}

 void CAVIFile::GetVideoFormat (uint codec_fcc,int frm_width,int frm_height,BITMAPINFOHEADER *video_format)

{
   ZeroMemory ((byte *)video_format,sizeof(BITMAPINFOHEADER));
   video_format->biSize        = sizeof(BITMAPINFOHEADER);
   video_format->biWidth       = frm_width;
   video_format->biHeight      = frm_height;
   video_format->biPlanes      = 1;
   video_format->biBitCount    = 24;
   video_format->biSizeImage   = frm_width * frm_height * 4;
   video_format->biCompression = codec_fcc;
}

 void CAVIFile::GoToFirstFrame (   )
 
{
   NextFramePos = StartFramePos;
}

 void CAVIFile::GoToLastFrame (   )
 
{
   NextFramePos = EndFramePos;
}

 void CAVIFile::InvertFrame (const byte *s_buffer,int bpl,int height,byte *d_buffer)

{
   int i;
   
   byte *s_image = (byte*)s_buffer;
   byte *d_image = d_buffer + (height - 1) * bpl;
   for (i = 0; i < height; i++) {
      CopyMemory (d_image,s_image,bpl);
      s_image += bpl;
      d_image -= bpl;
   }
}

 int CAVIFile::Open (LPCTSTR file_name)

{
   Close (   );
   CoInitialize (NULL);
   Flag_CoInitialize = TRUE;
   AVIFileInit (   );
   Flag_AVIFileInit  = TRUE;
   if (AVIFileOpen (&File,file_name,OF_READ|OF_SHARE_DENY_WRITE,NULL)) {
      File = NULL;
      return (1);
   }
   if (AVIFileGetStream (File,&Stream,streamtypeVIDEO,0)) {
      Stream = NULL;
      Close (   );
      return (2);
   }
   StartFramePos = NextFramePos = AVIStreamStart (Stream);
   EndFramePos   = AVIStreamEnd (Stream) - 1;
   NumFrames     = EndFramePos - StartFramePos + 1;
   AVISTREAMINFO si;
   if (AVIStreamInfo (Stream,&si,sizeof(AVISTREAMINFO))) {
      Close (   );
      return (3);
   }
   long vf_size;
   AVIStreamReadFormat (Stream,0,NULL,&vf_size);
   VideoFormat.Create (vf_size);
   if (AVIStreamReadFormat (Stream,0,(byte*)VideoFormat,&vf_size)) {
      Close (   );
      return (4);
   }
   BITMAPINFOHEADER *vf = (BITMAPINFOHEADER*)(byte*)VideoFormat;
   if (si.fccHandler) CodecID = si.fccHandler;
   else CodecID = vf->biCompression;
   FrameWidth  = vf->biWidth;
   FrameHeight = vf->biHeight;
   FrameRate   = (float)si.dwRate / si.dwScale;
   FrameBuffer.Create (FrameWidth * FrameHeight * 4);
   return (DONE);
}

 void CAVIFile::SetNextFramePos (int frame_pos)
 
{
   NextFramePos = frame_pos;
   if      (NextFramePos < StartFramePos) NextFramePos = StartFramePos;
   else if (NextFramePos > EndFramePos  ) NextFramePos = EndFramePos;
}

 int CAVIFile::SetAbsPlaybackPos (int pos,int flag_time_based)
 
{
   int abs_pos;
   if (flag_time_based) abs_pos = (int)(pos * FrameRate / 1000.0f);
   else abs_pos = pos;
   if      (abs_pos < StartFramePos) abs_pos = StartFramePos;
   else if (abs_pos > EndFramePos  ) abs_pos = EndFramePos;
   int flag_prev = TRUE;
   if (abs_pos > NextFramePos) flag_prev = FALSE;
   int e_code = FindKeyFrame (abs_pos,NextFramePos,flag_prev);
   return (e_code);
}

 int CAVIFile::SetRelPlaybackPos (int pos,int flag_time_based)
 
{
   int rel_pos;
   if (flag_time_based) rel_pos = (int)(pos * FrameRate / 1000.0f);
   else rel_pos = pos;
   int abs_pos = NextFramePos + rel_pos;
   if      (abs_pos < StartFramePos) abs_pos = StartFramePos;
   else if (abs_pos > EndFramePos  ) abs_pos = EndFramePos;
   int flag_prev = TRUE;
   if (pos > 0) flag_prev = FALSE;
   int e_code = FindKeyFrame (abs_pos,NextFramePos,flag_prev);
   return (e_code);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CAVIFile_FFMPEG
//
///////////////////////////////////////////////////////////////////////////////

 CAVIFile_FFMPEG::~CAVIFile_FFMPEG (   )
 
{
   Close (   );
}

 int CAVIFile_FFMPEG::AddFrame (const byte *s_buffer)
 
{
   int e_code;
   int d_buf_size;
   int flag_key_frame = FALSE;
   byte *d_buffer;

   e_code = VideoCodec.Encode (s_buffer,&d_buffer,d_buf_size,flag_key_frame);
   if (e_code) return (1);
   e_code = AddEncodedFrame (d_buffer,d_buf_size,flag_key_frame);
   if (e_code) return (2);
   return (DONE);
}

 int CAVIFile_FFMPEG::AddFrame (BGRImage &s_image)

{
   int e_code;
   
   if (s_image.Width != FrameWidth || s_image.Height != FrameHeight) return (1);
   switch (VideoCodec.PixFmt) {
      case FFMPEG_PIXFMT_BGR24: {
         InvertFrame ((byte *)(BGRPixel *)s_image,s_image.GetLineLength (   ),s_image.Height,FrameBuffer);
         e_code = AddFrame (FrameBuffer);
         if (e_code) return (2);
      }  break;
      case FFMPEG_PIXFMT_YUY2: {
         ConvertBGRToYUY2 (s_image,FrameBuffer);
         e_code = AddFrame (FrameBuffer);
         if (e_code) return (3);
      }  break;
      case FFMPEG_PIXFMT_YV12: {
         ConvertBGRToYV12 (s_image,FrameBuffer,YV12_FORMAT_STANDARD);
         e_code = AddFrame (FrameBuffer);
         if (e_code) return (4);
      }  break;
      default:
         return (5);
   }
   return (DONE);
}

 void CAVIFile_FFMPEG::Close (   )
 
{
   VideoCodec.Close (   );
   CAVIFile::Close  (   );
}

 int CAVIFile_FFMPEG::Create (LPCTSTR file_name,enum AVCodecID codec_id,int frm_width,int frm_height,float frame_rate,enum AVPixelFormat in_pixel_format)
 
{
   uint codec_fcc = 0;
   switch (codec_id) {
      case AV_CODEC_ID_MJPEG:
         codec_fcc = mmioFOURCC ('M','J','P','G');
         break;
      case AV_CODEC_ID_MPEG4:
         codec_fcc = mmioFOURCC ('X','V','I','D');
         break;
      case AV_CODEC_ID_H263:
         codec_fcc = mmioFOURCC ('H','2','6','3');
         break;
      case AV_CODEC_ID_H264:
         codec_fcc = mmioFOURCC ('X','2','6','4');
         break;
      default:
         break;
   }
   if (!codec_fcc) {
      Close (   );
      return (1);
   }
   int e_code = CAVIFile::Create (file_name,codec_fcc,frm_width,frm_height,frame_rate);
   if (e_code) {
      Close (   );
      return (2);
   }
   VideoCodec.PixFmt = in_pixel_format;
   e_code = VideoCodec.OpenEncoder (codec_id,frm_width,frm_height,frame_rate);
   if (e_code) {
      Close (   );
      return (3);
   }
   FrameBuffer.Create (FrameWidth * FrameHeight * 4);
   return (DONE);
}

 int CAVIFile_FFMPEG::ExtractFrame (byte **p_d_buffer)

{
   int s_buf_size;
   int flag_key_frame;
   byte *s_buffer;
   
   if (ExtractEncodedFrame (&s_buffer,s_buf_size,flag_key_frame)) return (1);
   int e_code = VideoCodec.Decode (s_buffer,s_buf_size,p_d_buffer);
   if (e_code > 0) return (2);
   else if (e_code < 0) return (-1);
   else return (DONE);
}

 int CAVIFile_FFMPEG::ExtractFrame (byte *d_buffer)
 
{
   int s_buf_size;
   int flag_key_frame;
   byte *s_buffer;
   
   int e_code = ExtractEncodedFrame (&s_buffer,s_buf_size,flag_key_frame);
   if (e_code) return (1);
   e_code = VideoCodec.Decode (s_buffer,s_buf_size,d_buffer);
   if (e_code > 0) return (2);
   else if (e_code < 0) return (-1);
   else return (DONE);
}

 int CAVIFile_FFMPEG::ExtractFrame (BGRImage &d_image)

{
   byte *s_buffer;
   
   if (d_image.Width != FrameWidth || d_image.Height != FrameHeight) d_image.Create (FrameWidth,FrameHeight);
   int e_code = ExtractFrame (&s_buffer);
   if (e_code == DONE) {
      switch (VideoCodec.PixFmt) {
         case FFMPEG_PIXFMT_BGR24:
            InvertFrame (s_buffer,d_image.GetLineLength (   ),d_image.Height,(byte*)(BGRPixel*)d_image);
            break;
         case FFMPEG_PIXFMT_YUY2:
            ConvertYUY2ToBGR (s_buffer,d_image);
            break;
         case FFMPEG_PIXFMT_YV12:
            ConvertYV12ToBGR (s_buffer,d_image,YV12_FORMAT_STANDARD);
            break;
         default:
            return (3);
      }
   }
   else return (e_code);
   return (DONE);
}

 int CAVIFile_FFMPEG::FlushCodecBuffers (   )
 
{
   return (VideoCodec.Flush (   ));
}

 int CAVIFile_FFMPEG::Open (LPCTSTR file_name,enum AVPixelFormat out_pixel_format)
 
{
   enum AVCodecID codec_id = AV_CODEC_ID_NONE;
   int e_code = CAVIFile::Open (file_name);
   if (e_code) {
      Close (   );
      return (1);
   }
// char* id = (char*)&CodecID;
// logd ("%c%c%c%c\n",id[0],id[1],id[2],id[3]);
   switch (CodecID) {
      case mmioFOURCC('M','J','P','G'):
      case mmioFOURCC('m','j','p','g'):
         codec_id = AV_CODEC_ID_MJPEG;
         break;
      case mmioFOURCC('D','I','V','X'):
      case mmioFOURCC('d','i','v','x'):
      case mmioFOURCC('D','V','X','1'):
      case mmioFOURCC('d','v','x','1'):
      case mmioFOURCC('D','X','5','0'):
      case mmioFOURCC('d','x','5','0'):
      case mmioFOURCC('M','P','4','V'):
      case mmioFOURCC('m','p','4','v'):
      case mmioFOURCC('X','V','I','D'):
      case mmioFOURCC('x','v','i','d'):
         codec_id = AV_CODEC_ID_MPEG4;
         break;
      case mmioFOURCC('M','P','4','1'):
      case mmioFOURCC('m','p','4','1'):
         codec_id = AV_CODEC_ID_MSMPEG4V1;
         break;
      case mmioFOURCC('M','P','4','2'):
      case mmioFOURCC('m','p','4','2'):
         codec_id = AV_CODEC_ID_MSMPEG4V2;
         break;
      case mmioFOURCC('M','P','4','3'):
      case mmioFOURCC('m','p','4','3'):
         codec_id = AV_CODEC_ID_MSMPEG4V3;
         break;
      case mmioFOURCC('H','2','6','3'):
      case mmioFOURCC('h','2','6','3'):
         codec_id = AV_CODEC_ID_H263;
         break;
      case mmioFOURCC('H','2','6','4'):
      case mmioFOURCC('h','2','6','4'):
      case mmioFOURCC('X','2','6','4'):
      case mmioFOURCC('x','2','6','4'):
         codec_id = AV_CODEC_ID_H264;
         break;
   }
   if (codec_id == AV_CODEC_ID_NONE) {
      Close (   );
      return (2);
   }
   VideoCodec.PixFmt = out_pixel_format;
   if (VideoCodec.OpenDecoder (codec_id,FrameWidth,FrameHeight)) {
      Close (   );
      return (3);
   }
   return (DONE);
}

}

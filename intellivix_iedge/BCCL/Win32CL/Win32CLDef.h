#pragma once

#include "bccl.h"
#include <afxcmn.h>
#include <afxext.h>

#define FT_ANSI_FIXED       0
#define FT_ANSI_VAR         1
#define FT_DEVICE_DEFAULT   2
#define FT_OEM_FIXED        3
#define FT_SYSTEM           4
#define FT_SYSTEM_FIXED     5
#define FT_ARIAL            6
#define FT_TAHOMA           7
#define FT_VERDANA          8
 
#define PT_DOT              0
#define PT_BOX              1
#define PT_CROSS            2
#define PT_O                3
#define PT_X                4

#define PC_BLACK            RGB(0  ,  0,  0)
#define PC_WHITE            RGB(255,255,255)
#define PC_RED              RGB(255,  0,  0)
#define PC_PINK             RGB(255, 50,100)
#define PC_GREEN            RGB(  0,255,  0)
#define PC_BLUE             RGB(  0,  0,255)
#define PC_YELLOW           RGB(255,255,  0)
#define PC_MAGENTA          RGB(255,  0,255)
#define PC_CYAN             RGB(  0,255,255)
#define PC_DARK_RED         RGB(128,  0,  0)
#define PC_DARK_GREEN       RGB(  0,128,  0)
#define PC_DARK_BLUE        RGB(  0,  0,128)
#define PC_DARK_YELLOW      RGB(128,128,  0)
#define PC_DARK_MAGENTA     RGB(128,  0,128)
#define PC_DARK_CYAN        RGB(  0,128,128)

#pragma once

#include "Win32CL/Thread.h"

 namespace Win32CL

{

///////////////////////////////////////////////////////////////////////////////
//
// Definitions
//
///////////////////////////////////////////////////////////////////////////////

#define TS_NORMAL      0x00000001
#define TS_BOLD        0x00000002
#define TS_ITALIC      0x00000004
#define TS_UNDERLINE   0x00000008

///////////////////////////////////////////////////////////////////////////////
//
// Class: 
//
///////////////////////////////////////////////////////////////////////////////

 class CTextItem

{
   public:
      COLORREF Color;
      int      Style;
      CString  Text;

   public:
      CTextItem* Prev;
      CTextItem* Next;
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: CTextOutCtrl
//
///////////////////////////////////////////////////////////////////////////////

 class CTextOutCtrl : public CRichEditCtrl, public CThread

{
   protected:
      int        NumLines;
      CMenu      Menu;
      CHARFORMAT TextFormat;

   protected:
      Queue<CTextItem> TextItemQueue;
      CCriticalSection CS_TextItemQueue;
      CEvent           Event_NewTextItem;
   
   public:
      CTextOutCtrl (   );

   protected:
      int AddToTextItemQueue               (CTextItem* s_item);
      CTextItem* RetrieveFromTextItemQueue (   );
   
   protected:
      virtual void ThreadProc (   );
   
   public:
      void Clear           (   );
      void InitContextMenu (   );
      void Print           (LPCTSTR format,...);
      void Print           (COLORREF color,int style,LPCTSTR text);
      void SetMaxNumLines  (int n_lines);
      void SetTextColor    (COLORREF color);
      void SetTextFont     (LPCTSTR font_name);
      void SetTextSize     (int size);
      void SetTextStyle    (int style,int activate = TRUE);

   public:
      // 여러 쓰레드에서 동시 다발적으로 호출해도 안전함.
      // (입력된 텍스트를 큐에 쌓아 놓고 순차적으로 처리함)
      int Print2 (COLORREF color,int style,LPCTSTR format,...);

   protected:
      virtual BOOL PreCreateWindow (CREATESTRUCT &);

   protected:
      afx_msg int  OnCreate      (LPCREATESTRUCT cs);
      afx_msg void OnDestroy     (   );
      afx_msg void OnRButtonDown (UINT,CPoint);
      afx_msg void OnCmd_Clear   (   );
      afx_msg void OnCmd_Copy    (   );

   DECLARE_MESSAGE_MAP (   )
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: CTextView
//
///////////////////////////////////////////////////////////////////////////////
 
 class CTextView : public CView

{
   DECLARE_DYNCREATE (CTextView)

   public:
      CTextOutCtrl TextOutCtrl;

   protected:
      virtual void OnDraw (CDC *dc);

   public:
      void Clear          (   );
      void Print          (LPCTSTR format,...);
      void SetMaxNumLines (int n_lines);
      void SetTextColor   (COLORREF color);
      void SetTextFont    (LPCTSTR font_name);
      void SetTextSize    (int size);
      void SetTextStyle   (int style,int activate = TRUE);

   protected:
      afx_msg int  OnCreate (LPCREATESTRUCT cs);
      afx_msg void OnSize   (UINT type,int width,int height);

   DECLARE_MESSAGE_MAP (   )
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: CTextViewWnd
//
///////////////////////////////////////////////////////////////////////////////

 class CTextViewWnd : public CFrameWnd

{
   public:
      CTextView *TextView;

   public:
      CTextViewWnd (   );

   protected:
      afx_msg int OnCreate (LPCREATESTRUCT cs);

   DECLARE_MESSAGE_MAP (   );
};

}

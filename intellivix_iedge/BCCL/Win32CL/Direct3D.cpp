#include "Win32CL/Direct3D.h"

#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"d3dx9.lib")

 namespace Win32CL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Global Variables
//
///////////////////////////////////////////////////////////////////////////////

LPDIRECT3D9 _Direct3D = NULL;

///////////////////////////////////////////////////////////////////////////////
//
// Class: CD3DDevice
//
///////////////////////////////////////////////////////////////////////////////

 CD3DDevice::CD3DDevice (   )
 
{
   Device = NULL;
}
 
 CD3DDevice::~CD3DDevice (   )
 
{
   Delete (   );
}

 int CD3DDevice::Clear (D3DCOLOR color)
 
{
   int i,n;
   
   if (Device == NULL) return (1);
   if (PresentParams.SwapEffect == D3DSWAPEFFECT_COPY) n = 1;
   else n = PresentParams.BackBufferCount + 1;
   for (i = 0; i < n; i++) {
      CD3DSurface b_buffer;
      if (b_buffer.GetBackBuffer (Device)) return (2);
      if (b_buffer.Clear (color)) return (3);
      Present (   );
   }
   return (DONE);
}

 int CD3DDevice::Create (HWND h_wnd,int width,int height,D3DSWAPEFFECT swap_effect)
 
{
   Delete (   );
   if (_Direct3D == NULL) return (1);
   D3DDISPLAYMODE dm;
   if (_Direct3D->GetAdapterDisplayMode (D3DADAPTER_DEFAULT,&dm) != D3D_OK) return (2);
   memset (&PresentParams,0,sizeof(D3DPRESENT_PARAMETERS));
   PresentParams.BackBufferWidth  = width;
   PresentParams.BackBufferHeight = height;
   PresentParams.BackBufferFormat = dm.Format;
   PresentParams.BackBufferCount  = 1;
   PresentParams.Flags           |= D3DPRESENTFLAG_LOCKABLE_BACKBUFFER;
   PresentParams.MultiSampleType  = D3DMULTISAMPLE_NONE;
   PresentParams.SwapEffect       = swap_effect;
   PresentParams.hDeviceWindow    = h_wnd;
   PresentParams.Windowed         = TRUE;
   if (_Direct3D->CreateDevice (D3DADAPTER_DEFAULT,D3DDEVTYPE_HAL,h_wnd,D3DCREATE_SOFTWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED,&PresentParams,&Device) != D3D_OK) {
      loge( "[ERROR](Direct3D): Cannot create a device.\n");
      return (3);
   }
   return (DONE);
}

 void CD3DDevice::Delete (   )
 
{
   if (Device != NULL) {
      Device->Release (   );
      Device = NULL;
   }
}

 D3DFORMAT CD3DDevice::GetPixelFormat (   )
 
{
   return (PresentParams.BackBufferFormat);
}

 int CD3DDevice::GetSize (int &width,int &height)
 
{
   width = height = 0;
   if (Device == NULL) return (1);
   width  = PresentParams.BackBufferWidth;
   height = PresentParams.BackBufferHeight;
   return (DONE);
}

 int CD3DDevice::Present (const RECT *s_rect,const RECT *d_rect)
 
{
   if (Device == NULL) return (1);
   if (Device->Present (s_rect,d_rect,NULL,NULL) != D3D_OK) return (2);
   return (DONE);
}

 int CD3DDevice::Reset (int width,int height)
 
{
   if (Device == NULL) return (1);
   D3DDISPLAYMODE dm;
   _Direct3D->GetAdapterDisplayMode (D3DADAPTER_DEFAULT,&dm);
   PresentParams.BackBufferWidth  = width;
   PresentParams.BackBufferHeight = height;
   PresentParams.BackBufferFormat = dm.Format;
   if (Device->Reset (&PresentParams) != D3D_OK) return (2);
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CD3DSurface
//
///////////////////////////////////////////////////////////////////////////////

 CD3DSurface::CD3DSurface (   )
 
{
   Device   = NULL;
   Surface  = NULL;
   DC.m_hDC = NULL;
}

 CD3DSurface::~CD3DSurface (   )
 
{
   Delete (   );
}
 
 int CD3DSurface::Blit (LPDIRECT3DSURFACE9 s_surface,LPRECT s_rect,LPRECT d_rect)
 
{
   if (s_surface == NULL || Surface == NULL) return (1);
   D3DSURFACE_DESC ss_desc;
   s_surface->GetDesc (&ss_desc);
   if (_Direct3D->CheckDeviceFormatConversion (D3DADAPTER_DEFAULT,D3DDEVTYPE_HAL,ss_desc.Format,GetPixelFormat()) != D3D_OK) {
      loge( "[ERROR](Direct3D): Cannot convert the pixel format of the surface in the blitting operation.\n");
      return (2);
   }
   if (Device->StretchRect (s_surface,s_rect,Surface,d_rect,D3DTEXF_LINEAR) != D3D_OK) {
      loge( "[ERROR](Direct3D): Cannot blit the surface.\n");
      return (3);
   }
   return (DONE);
}

 int CD3DSurface::Clear (D3DCOLOR color)
 
{
   return (FillRect (NULL,color));
}

 int CD3DSurface::CreateOffscreenSurface (LPDIRECT3DDEVICE9 device,int width,int height,D3DFORMAT pixel_format)
 
{
   if (!device) return (1);
   Device = device;
   if (device->CreateOffscreenPlainSurface (width,height,pixel_format,D3DPOOL_DEFAULT,&Surface,NULL) != D3D_OK) {
      loge( "[ERROR](Direct3D): Cannot create an offscreen surface.\n");
      return (1);
   }
   return (DONE);
} 
 
 void CD3DSurface::Delete (   )
 
{
   if (DC.m_hDC != NULL) ReleaseDC (   );
   if (Surface != NULL) {
      Surface->Release (   );
      Surface = NULL;
      Device  = NULL;
   }
}

 int CD3DSurface::FillRect (LPRECT s_rect,D3DCOLOR color)
 
{
   if (Surface == NULL) return (1);
   if (Device->ColorFill (Surface,s_rect,color) != D3D_OK) {
      loge( "[ERROR](Direct3D): Cannot fill the rectangle.\n");
      return (2);
   }
   return (DONE);
}

 int CD3DSurface::GetBackBuffer (LPDIRECT3DDEVICE9 device,int bb_no)
 
{
   if (!device) return (1);
   Device = device;
   if (device->GetBackBuffer (0,bb_no,D3DBACKBUFFER_TYPE_MONO,&Surface) != D3D_OK) {
      loge( "[ERROR](Direct3D): Cannot get the back buffer.\n");
      return (2);
   }
   return (DONE);
}

 CDC* CD3DSurface::GetDC (   )
 
{
   if (Surface == NULL) return (NULL);
   if (DC.m_hDC == NULL) {
      HDC h_dc;
      if (Surface->GetDC (&h_dc) != D3D_OK) {
         loge( "[ERROR](Direct3D): Cannot get a DC.\n");
         return (NULL);
      }
      DC.Attach (h_dc);
   }
   return (&DC);
}

 int CD3DSurface::GetImage_YUY2 (byte *d_buffer)
 
{
   int i;

   if (Surface == NULL) return (1);
   if (GetPixelFormat (   ) != D3DFMT_YUY2) return (2);
   int width,height;
   GetSize (width,height);
   D3DLOCKED_RECT rect;
   if (Surface->LockRect (&rect,NULL,0) != D3D_OK) {
      loge( "[ERROR](Direct3D): Cannot lock the surface.\n");
      return (3);
   }
   int s_bpl = rect.Pitch;
   int d_bpl = width * 2;
   byte *s_buffer = (byte*)rect.pBits;
   for (i = 0; i < height; i++) {
      CopyMemory (d_buffer,s_buffer,d_bpl);
      s_buffer += s_bpl;
      d_buffer += d_bpl;
   }
   Surface->UnlockRect (   );
   return (DONE);
}

 D3DFORMAT CD3DSurface::GetPixelFormat (   )
 
{
   if (Surface == NULL) return (D3DFMT_UNKNOWN);
   D3DSURFACE_DESC desc;
   Surface->GetDesc (&desc);
   return (desc.Format);
}

 int CD3DSurface::GetSize (int &width,int &height)

{
   width = height = 0;
   if (Surface == NULL) return (1);
   D3DSURFACE_DESC desc;
   Surface->GetDesc (&desc);
   width  = desc.Width;
   height = desc.Height;
   return (DONE);
}

 int CD3DSurface::DrawImage (BGRImage &s_image)
 
{
   int i;

   if (Surface == NULL) return (1);
   if (GetPixelFormat (   ) != D3DFMT_R8G8B8) return (2);
   int width,height;
   GetSize (width,height);
   if (s_image.Width != width || s_image.Height != height) return (3);
   D3DLOCKED_RECT rect;
   if (Surface->LockRect (&rect,NULL,0) != D3D_OK) {
      loge( "[ERROR](Direct3D): Cannot lock the surface.\n");
      return (4);
   }
   int s_bpl = s_image.GetLineLength (   );
   int d_bpl = rect.Pitch;
   byte *s_buffer = (byte*)(BGRPixel*)s_image;
   byte *d_buffer = (byte*)rect.pBits;
   for (i = 0; i < height; i++) {
      CopyMemory (d_buffer,s_buffer,s_bpl);
      s_buffer += s_bpl;
      d_buffer += d_bpl;
   }
   Surface->UnlockRect (   );
   return (DONE);
}

 int CD3DSurface::PutImage_YUY2 (byte *s_buffer)
 
{
   int i;

   if (Surface == NULL) return (1);
   if (GetPixelFormat (   ) != D3DFMT_YUY2) return (2);
   int width,height;
   GetSize (width,height);
   D3DLOCKED_RECT rect;
   if (Surface->LockRect (&rect,NULL,0) != D3D_OK) {
      loge( "[ERROR](Direct3D): Cannot lock the surface.\n");
      return (3);
   }
   int s_bpl = width * 2;
   int d_bpl = rect.Pitch;
   byte *d_buffer = (byte*)rect.pBits;
   for (i = 0; i < height; i++) {
      CopyMemory (d_buffer,s_buffer,s_bpl);
      s_buffer += s_bpl;
      d_buffer += d_bpl;
   }
   Surface->UnlockRect (   );
   return (DONE);
}

 void CD3DSurface::ReleaseDC (   )
 
{
   if (Surface == NULL || DC.m_hDC == NULL) return;
   Surface->ReleaseDC (DC.m_hDC);
   DC.Detach (   );
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 void CloseDirect3D (   )
 
{
   if (_Direct3D != NULL) {
      _Direct3D->Release (   );
      _Direct3D = NULL;
   }
}

 int InitDirect3D (   )
 
{
   if (_Direct3D != NULL) return (DONE);
   _Direct3D = Direct3DCreate9 (D3D_SDK_VERSION);
   if (_Direct3D == NULL) {
      loge( "[ERROR](Direct3D): Cannot initialize Direct3D.\n");
      return (1);
   }
   return (DONE);
}

}

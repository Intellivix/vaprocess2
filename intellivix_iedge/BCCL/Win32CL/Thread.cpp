#include "Win32CL/Thread.h"

 namespace Win32CL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Class: CLock
//
///////////////////////////////////////////////////////////////////////////////

 CLock::CLock (CCriticalSection& cs,int flag_lock)
 
{
   Flag_Lock = FALSE;
   CriticalSection = &cs;
   if (flag_lock) Lock (   );
}

 CLock::~CLock (   )
 
{
   Unlock (   );
}

 void CLock::Lock (   )
 
{
   if (Flag_Lock) return;
   CriticalSection->Lock (   );
   Flag_Lock = TRUE;
}

 void CLock::Unlock (   )
 
{
   if (!Flag_Lock) return;
   CriticalSection->Unlock (   );
   Flag_Lock = FALSE;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CThread
//
///////////////////////////////////////////////////////////////////////////////

 CThread::CThread (   )

{
   Flag_ThreadRunning = FALSE;
   ThreadHnd           = NULL;
   CBParam_ThreadProc  = NULL;
   Callback_ThreadProc = NULL;

   WaitingTime         = INFINITE;
}

 CThread::~CThread (   )
 
{
   StopThread (   );
}

 int CThread::CheckStopEvent (uint timeout)

{
   if (WaitForSingleObject (Event_Stop,timeout) == WAIT_OBJECT_0) return (TRUE);
   else return (FALSE);
}

 void CThread::ExecuteThreadProc (   )

{
   Event_Stop.ResetEvent (   );
   Flag_ThreadRunning = TRUE;
   ThreadProc (   );
   Flag_ThreadRunning = FALSE;
   ThreadHnd = NULL;
}

 int CThread::IsThreadRunning (   )

{
   return (Flag_ThreadRunning);
}

 uint CThread::_ThreadProc (void* param)
 
{
   CThread *thread = (CThread*)param;
   thread->ExecuteThreadProc (   );
   return (DONE);
}

 void CThread::ThreadProc (   )

{
   if (Callback_ThreadProc != NULL) Callback_ThreadProc(this,CBParam_ThreadProc);
}

 void CThread::SetCallback_ThreadProc (CALLBACK_ThreadProc cb_func,void* cb_param)

{
   Callback_ThreadProc = cb_func;
   CBParam_ThreadProc  = cb_param;
}

 int CThread::StartThread (   )

{
   if (ThreadHnd != NULL) return (1);
   CWinThread *thread = AfxBeginThread (_ThreadProc,this,THREAD_PRIORITY_NORMAL,0,CREATE_SUSPENDED);
   ThreadHnd = thread->m_hThread;
   thread->ResumeThread (   );
   return (DONE);
}

 void CThread::StopThread (   )

{
   if (!IsThreadRunning (   )) return;
   Event_Stop.SetEvent (   );
   if (ThreadHnd != NULL) WaitForSingleObject (ThreadHnd,WaitingTime);
}

 void CThread::WaitForThreadEnd (uint timeout)

{
   if (!IsThreadRunning (   )) return;
   if (ThreadHnd != NULL) WaitForSingleObject (ThreadHnd,timeout);
}

}

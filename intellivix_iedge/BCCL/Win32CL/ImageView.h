#pragma once

#include "Win32CLDef.h"

 namespace Win32CL

{
 
///////////////////////////////////////////////////////////////////////////////
//
// Class: CImageView
//
///////////////////////////////////////////////////////////////////////////////
 
 #define IV_DM_DIRECT            0
 #define IV_DM_INDIRECT          1
 
 #define IDM_IV_ZOOM_X1          31001
 #define IDM_IV_ZOOM_X2          31002
 #define IDM_IV_ZOOM_X3          31003
 #define IDM_IV_ZOOM_X4          31004
 #define IDM_IV_ZOOM_X5          31005
 #define IDM_IV_ZOOM_X6          31006
 #define IDM_IV_ZOOM_X7          31007
 #define IDM_IV_ZOOM_X8          31008
 #define IDM_IV_ZOOM_X9          31009
 #define IDM_IV_CAPTURE_START    31101
 #define IDM_IV_CAPTURE_CANCEL   31102
 #define IDM_IV_CAPTURE_COPY     31103
 #define IDM_IV_CAPTURE_SAVE     31104

 #define BITMAP_INFO_SIZE        1064
   
 class CImageView : public CScrollView

{
   DECLARE_DYNCREATE (CImageView)

   protected:
      static int     NBitsPerPixel;
      static byte    BitmapInfo[BITMAP_INFO_SIZE];
      static HCURSOR CrossCursor;
      static LPCTSTR ClassName;

   protected:
      int DrawMode;
      int ZoomFactor;
      int CanvasWidth;
      int CanvasHeight;
      int Flag_Dragging;
      int Flag_CaptureEnd;
      int Flag_CaptureStart;
      
   protected:
      CMenu   Menu;
      CPoint  StartPos;
      CPoint  EndPos;
      CBitmap Canvas;

   public:
      CImageView (   );

   protected:
      virtual void OnDraw (CDC *dc);
      virtual BOOL PreCreateWindow (CREATESTRUCT &cs);

   protected:
      void ClearCanvas        (   );
      void ClearWindow        (   );
      void CreateCanvas       (int width,int height);
      void DrawSelectionBox   (CDC *dc);
      void EraseSelectionBox  (   );
      void InitBitmapInfo     (   );
      void InitContextMenu    (   );
      void InvertSelectionBox (   );

   public:
      void GetBitmap     (GImage &s_image,CBitmap &d_bitmap);
      void GetBitmap     (BGRImage &s_image,CBitmap &d_bitmap);
      void GetCanvasSize (int &bm_width,int &bm_height);
      void GetImage      (int x,int y,BGRImage &d_image);
      COLORREF GetPixel  (int x,int y);
      int  GetZoomFactor (   );
      void SetCanvasSize (int width,int height);
      void SetDrawMode   (int mode);
      void Zoom          (int zoom_factor);

   public:
      void Clear               (   );
      void DrawArrow           (int cx,int cy,float direction,float length,COLORREF color);
      void DrawCircle          (int cx,int cy,int radius,COLORREF pen_color,int pen_width = 1,int pen_style = PS_SOLID);
      void DrawFilledCircle    (int cx,int cy,int radius,COLORREF color);
      void DrawFilledPie       (int cx,int cy,int radius,float s_angle,float e_angle,COLORREF color);
      void DrawFilledRectangle (int x,int y,int width,int height,COLORREF color);
      void DrawImage           (CBitmap &s_bitmap,int x,int y,int width,int height);
      void DrawImage           (DArray2D &s_array,int x,int y,int flag_scaling = TRUE);
      void DrawImage           (FArray2D &s_array,int x,int y,int flag_scaling = TRUE);
      void DrawImage           (IArray2D &s_array,int x,int y,int flag_scaling = TRUE);
      void DrawImage           (SArray2D &s_array,int x,int y,int flag_scaling = TRUE);
      void DrawImage           (USArray2D &s_array,int x,int y,int flag_scaling = TRUE);
      void DrawImage           (GImage &s_image,int x,int y);
      void DrawImage           (BGRImage &s_image,int x,int y);
      void DrawLine            (int x1,int y1,int x2,int y2,COLORREF pen_color,int pen_width = 1,int pen_style = PS_SOLID);
      void DrawPie             (int cx,int cy,int radius,float s_angle,float e_angle,COLORREF pen_color,int pen_width = 1,int pen_style = PS_SOLID);
      void DrawPixel           (int x,int y,COLORREF color);
      void DrawPoint           (int x,int y,COLORREF color,int shape = PT_BOX);
      void DrawPolygon         (IPoint2D *vertices,int n_vertices,COLORREF pen_color,int pen_width = 1,int pen_style = PS_SOLID);
      void DrawRectangle       (int x,int y,int width,int height,COLORREF pen_color,int pen_width = 1,int pen_style = PS_SOLID);
      void DrawText            (LPCTSTR text,int x,int y,COLORREF color,int bk_mode = TRANSPARENT,int font_type = FT_SYSTEM,int font_size = 100);

   protected:
      afx_msg int  OnCreate      (LPCREATESTRUCT cs);
      afx_msg void OnKeyDown     (UINT key_code,UINT,UINT);
      afx_msg void OnLButtonDown (UINT,CPoint point);
      afx_msg void OnLButtonUp   (UINT,CPoint point);
      afx_msg void OnMouseMove   (UINT,CPoint point);
      afx_msg BOOL OnMouseWheel  (UINT flags,short delta,CPoint point);
      afx_msg void OnRButtonDown (UINT,CPoint point);
      afx_msg BOOL OnSetCursor   (CWnd *window,UINT hit_test,UINT message);
      afx_msg void OnSize        (UINT,int w_width,int w_height);
   
   protected:
      afx_msg void OnCmd_Capture_Cancel (   );
      afx_msg void OnCmd_Capture_Copy   (   );
      afx_msg void OnCmd_Capture_Save   (   );
      afx_msg void OnCmd_Capture_Start  (   );
      afx_msg void OnCmd_Zoom           (UINT id);

   DECLARE_MESSAGE_MAP (   )
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: CImageViewWnd
//
///////////////////////////////////////////////////////////////////////////////

 class CImageViewWnd : public CWnd

{
   protected:
      static LPCTSTR ClassName;
   
   public:
      CImageView *ImageView;

   public:
      CImageViewWnd (   );

   protected:
      virtual void PostNcDestroy (   );

   public:
      BOOL Create (LPCTSTR lpszWindowName,const RECT &rect);
   
   protected:
      afx_msg int  OnCreate (LPCREATESTRUCT lpcs);
      afx_msg void OnSize   (UINT nType,int cx,int cy);

   DECLARE_MESSAGE_MAP (   );
};

}


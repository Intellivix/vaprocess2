#include "Win32CL/ImageView.h"
#include "Win32CL/Utilities.h"

 namespace Win32CL

{

#define DEF_CANVAS_WIDTH    1000
#define DEF_CANVAS_HEIGHT   1000
#define DEF_ZOOM_FACTOR     1

///////////////////////////////////////////////////////////////////////////////
//
// Class: CImageView
//
///////////////////////////////////////////////////////////////////////////////

int     CImageView::NBitsPerPixel = 0;
byte    CImageView::BitmapInfo[BITMAP_INFO_SIZE];
HCURSOR CImageView::CrossCursor   = NULL;
LPCTSTR CImageView::ClassName     = NULL;

IMPLEMENT_DYNCREATE (CImageView,CScrollView)

BEGIN_MESSAGE_MAP (CImageView,CScrollView)
   ON_WM_CREATE      (   )
   ON_WM_KEYDOWN     (   )
   ON_WM_LBUTTONDOWN (   )
   ON_WM_LBUTTONUP   (   )
   ON_WM_MOUSEMOVE   (   )
   ON_WM_MOUSEWHEEL  (   )
   ON_WM_RBUTTONDOWN (   )
   ON_WM_SETCURSOR   (   )
   ON_WM_SIZE        (   )
   ON_COMMAND        (IDM_IV_CAPTURE_CANCEL,OnCmd_Capture_Cancel)
   ON_COMMAND        (IDM_IV_CAPTURE_START ,OnCmd_Capture_Start )
   ON_COMMAND        (IDM_IV_CAPTURE_COPY  ,OnCmd_Capture_Copy  )
   ON_COMMAND        (IDM_IV_CAPTURE_SAVE  ,OnCmd_Capture_Save  ) 
   ON_COMMAND_RANGE  (IDM_IV_ZOOM_X1,IDM_IV_ZOOM_X9,OnCmd_Zoom  )
END_MESSAGE_MAP (   )

 CImageView::CImageView (   )

{
   ZoomFactor   = DEF_ZOOM_FACTOR;
   DrawMode     = IV_DM_DIRECT;
   CanvasWidth  = DEF_CANVAS_WIDTH;
   CanvasHeight = DEF_CANVAS_HEIGHT;
   Flag_CaptureStart = Flag_CaptureEnd = Flag_Dragging = FALSE;
}

 void CImageView::Clear (   )

{
   ClearCanvas (   );
   if (DrawMode == IV_DM_DIRECT) ClearWindow (   );
}

 void CImageView::ClearCanvas (   )
 
{
   CDC mem_dc;

   CClientDC dc(this);
   mem_dc.CreateCompatibleDC (&dc);
   CBitmap *bitmap = mem_dc.SelectObject (&Canvas);
   mem_dc.FillSolidRect (0,0,CanvasWidth,CanvasHeight,PC_BLACK);
   mem_dc.SelectObject  (bitmap);
}

 void CImageView::ClearWindow (   )

{
   CRect rect;

   CClientDC dc(this);
   GetClientRect (&rect);
   dc.FillSolidRect (rect.left,rect.top,rect.right,rect.bottom,PC_BLACK);
   if (Flag_CaptureEnd && DrawMode == IV_DM_DIRECT) DrawSelectionBox (&dc);
}

 void CImageView::CreateCanvas (int width,int height)

{
   CClientDC dc(this);
   CanvasWidth  = width;
   CanvasHeight = height;
   Canvas.CreateCompatibleBitmap (&dc,CanvasWidth,CanvasHeight);
}

 void CImageView::DrawArrow (int cx,int cy,float direction,float length,COLORREF color)

{
   int i,sx,sy;
   int s_width,s_height;
   CDC mem_dc;
   FPoint2D ah1[3];
   IPoint2D ah2[3];

   direction *= MC_DEG2RAD;
   float r00 = (float)cos (direction);
   float r10 = (float)sin (direction);
   float r01 = -r10;
   float r11 =  r00;
   ah1[0](length,-6.0f);
   ah1[1](length + 11.0f,0.0f);
   ah1[2](length, 6.0f);
   for (i = 0; i < 3; i++) {
      ah2[i].X = (int)(r00 * ah1[i].X + r01 * ah1[i].Y + cx);
      ah2[i].Y = (int)(r10 * ah1[i].X + r11 * ah1[i].Y + cy);
   }
   CClientDC dc(this);
   mem_dc.CreateCompatibleDC (&dc);
   CBitmap *bitmap = mem_dc.SelectObject (&Canvas);
   CPen pen1(PS_SOLID,1,color);
   CPen pen2(PS_SOLID,5,color);
   CBrush brush(color);
   mem_dc.SelectObject (&pen1);
   mem_dc.SelectObject (&brush);
   mem_dc.Polygon ((LPPOINT)ah2,3);
   mem_dc.SelectObject (&pen2);
   mem_dc.MoveTo (cx,cy);
   int x2 = (int)(r00 * length + cx);
   int y2 = (int)(r10 * length + cy);
   mem_dc.LineTo (x2,y2);
   mem_dc.SelectStockObject (NULL_PEN);
   mem_dc.SelectStockObject (NULL_BRUSH);
   if (DrawMode == IV_DM_DIRECT) {
      OnPrepareDC (&dc);
      if (ZoomFactor == 1) {
         dc.SelectObject (&pen1);
         dc.SelectObject (&brush);
         dc.Polygon ((LPPOINT)ah2,3);
         dc.SelectObject (&pen2);
         dc.MoveTo (cx,cy);
         dc.LineTo (x2,y2);
         dc.SelectStockObject (NULL_PEN);
         dc.SelectStockObject (NULL_BRUSH);
      }
      else {
         IPoint2D min_p,max_p;
         IPoint2D min_x,max_x;
         IP2DArray1D v_array(4);
         for (i = 0; i < 3; i++) v_array[i] = ah2[i];
         v_array[3](cx,cy);
         FindMinMax (v_array,min_p,max_p,min_x,max_x);
         sx = min_p.X, sy = min_p.Y;
         s_width  = max_p.X - min_p.X + 1;
         s_height = max_p.Y - min_p.Y + 1;
         dc.StretchBlt (sx * ZoomFactor,sy * ZoomFactor,s_width * ZoomFactor,s_height * ZoomFactor,&mem_dc,sx,sy,s_width,s_height,SRCCOPY);
      }
   }
   mem_dc.SelectObject (bitmap);
   if (Flag_CaptureEnd && DrawMode == IV_DM_DIRECT) DrawSelectionBox (&dc);
}

 void CImageView::DrawCircle (int cx,int cy,int radius,COLORREF pen_color,int pen_width,int pen_style)

{
   int sx,sy;
   int s_width,s_height;
   CDC mem_dc;

   CClientDC dc(this);
   mem_dc.CreateCompatibleDC (&dc);
   CBitmap *bitmap = mem_dc.SelectObject (&Canvas);
   CPen pen(pen_style,pen_width,pen_color);
   mem_dc.SelectObject (&pen);
   mem_dc.SelectStockObject (NULL_BRUSH);
   int x1 = cx - radius;
   int y1 = cy - radius;
   int x2 = cx + radius;
   int y2 = cy + radius;
   mem_dc.Ellipse (x1,y1,x2,y2);
   mem_dc.SelectStockObject (NULL_PEN);
   if (DrawMode == IV_DM_DIRECT) {
      OnPrepareDC (&dc);
      if (ZoomFactor == 1) {
         dc.SelectObject (&pen);
         dc.SelectStockObject (NULL_BRUSH);
         dc.Ellipse (x1,y1,x2,y2);
         dc.SelectStockObject (NULL_PEN);
      }
      else {
         sx = x1;
         sy = y1;
         s_width  = x2 - x1 + 1;
         s_height = y2 - y1 + 1;
         dc.StretchBlt (sx * ZoomFactor,sy * ZoomFactor,s_width * ZoomFactor,s_height * ZoomFactor,&mem_dc,sx,sy,s_width,s_height,SRCCOPY);
      }
   }
   mem_dc.SelectObject (bitmap);
   if (Flag_CaptureEnd && DrawMode == IV_DM_DIRECT) DrawSelectionBox (&dc);
}

 void CImageView::DrawFilledCircle (int cx,int cy,int radius,COLORREF color)

{
   int sx,sy;
   int s_width,s_height;
   CDC mem_dc;

   CClientDC dc(this);
   mem_dc.CreateCompatibleDC (&dc);
   CBitmap *bitmap = mem_dc.SelectObject (&Canvas);
   CPen pen(PS_SOLID,1,color);
   mem_dc.SelectObject (&pen);
   CBrush brush(color);
   mem_dc.SelectObject (&brush);
   int x1 = cx - radius;
   int y1 = cy - radius;
   int x2 = cx + radius;
   int y2 = cy + radius;
   mem_dc.Ellipse (x1,y1,x2,y2);
   mem_dc.SelectStockObject (NULL_PEN);
   mem_dc.SelectStockObject (NULL_BRUSH);
   if (DrawMode == IV_DM_DIRECT) {
      OnPrepareDC (&dc);
      if (ZoomFactor == 1) {
         dc.SelectObject (&pen);
         dc.SelectObject (&brush);
         dc.Ellipse (x1,y1,x2,y2);
         dc.SelectStockObject (NULL_PEN);
         dc.SelectStockObject (NULL_BRUSH);
      }
      else {
         sx = x1;
         sy = y1;
         s_width  = x2 - x1 + 1;
         s_height = y2 - y1 + 1;
         dc.StretchBlt (sx * ZoomFactor,sy * ZoomFactor,s_width * ZoomFactor,s_height * ZoomFactor,&mem_dc,sx,sy,s_width,s_height,SRCCOPY);
      }
   }
   mem_dc.SelectObject (bitmap);
   if (Flag_CaptureEnd && DrawMode == IV_DM_DIRECT) DrawSelectionBox (&dc);
}

 void CImageView::DrawFilledPie (int cx,int cy,int radius,float s_angle,float e_angle,COLORREF color)

{
   int sx,sy;
   int s_width,s_height;
   CDC mem_dc;

   CClientDC dc(this);
   mem_dc.CreateCompatibleDC (&dc);
   CBitmap *bitmap = mem_dc.SelectObject (&Canvas);
   CPen pen(PS_SOLID,1,color);
   mem_dc.SelectObject (&pen);
   CBrush brush(color);
   mem_dc.SelectObject (&brush);
   s_angle *= MC_DEG2RAD;
   e_angle *= MC_DEG2RAD;
   int x1 = cx - radius;
   int y1 = cy - radius;
   int x2 = cx + radius;
   int y2 = cy + radius;
   int x3 = (int)(radius * cos (s_angle) + cx + 0.5f);
   int y3 = (int)(radius * sin (s_angle) + cy + 0.5f);
   int x4 = (int)(radius * cos (e_angle) + cx + 0.5f);
   int y4 = (int)(radius * sin (e_angle) + cy + 0.5f);
   mem_dc.Pie (x1,y1,x2,y2,x3,y3,x4,y4);
   mem_dc.SelectStockObject (NULL_PEN);
   mem_dc.SelectStockObject (NULL_BRUSH);
   if (DrawMode == IV_DM_DIRECT) {
      OnPrepareDC (&dc);
      if (ZoomFactor == 1) {
         dc.SelectObject (&pen);
         dc.SelectObject (&brush);
         dc.Pie (x1,y1,x2,y2,x3,y3,x4,y4);
         dc.SelectStockObject (NULL_PEN);
         dc.SelectStockObject (NULL_BRUSH);
      }
      else {
         sx = x1;
         sy = y1;
         s_width  = x2 - x1 + 1;
         s_height = y2 - y1 + 1;
         dc.StretchBlt (sx * ZoomFactor,sy * ZoomFactor,s_width * ZoomFactor,s_height * ZoomFactor,&mem_dc,sx,sy,s_width,s_height,SRCCOPY);
      }
   }
   mem_dc.SelectObject (bitmap);
   if (Flag_CaptureEnd && DrawMode == IV_DM_DIRECT) DrawSelectionBox (&dc);
}

 void CImageView::DrawFilledRectangle (int x,int y,int width,int height,COLORREF color)

{
   CDC mem_dc;

   CClientDC dc(this);
   mem_dc.CreateCompatibleDC (&dc);
   CBitmap *bitmap = mem_dc.SelectObject (&Canvas);
   mem_dc.FillSolidRect (x,y,width,height,color);
   if (DrawMode == IV_DM_DIRECT) {   
      OnPrepareDC (&dc);
      if (ZoomFactor == 1) dc.FillSolidRect (x,y,width,height,color);
      else dc.StretchBlt (x * ZoomFactor,y * ZoomFactor,width * ZoomFactor,height * ZoomFactor,&mem_dc,x,y,width,height,SRCCOPY);
   }
   mem_dc.SelectObject (bitmap);
   if (Flag_CaptureEnd && DrawMode == IV_DM_DIRECT) DrawSelectionBox (&dc);
}

 void CImageView::DrawImage (CBitmap &s_bitmap,int x,int y,int width,int height)

{
   CDC s_mem_dc,d_mem_dc;

   CClientDC dc(this);
   s_mem_dc.CreateCompatibleDC (&dc);
   d_mem_dc.CreateCompatibleDC (&dc);
   CBitmap *bitmap1 = s_mem_dc.SelectObject (&s_bitmap);
   CBitmap *bitmap2 = d_mem_dc.SelectObject (&Canvas);
   d_mem_dc.BitBlt (x,y,width,height,&s_mem_dc,0,0,SRCCOPY);
   if (DrawMode == IV_DM_DIRECT) {
      OnPrepareDC (&dc);
      if (ZoomFactor == 1) dc.BitBlt (x,y,width,height,&s_mem_dc,0,0,SRCCOPY);
      else dc.StretchBlt (x * ZoomFactor,y * ZoomFactor,width * ZoomFactor,height * ZoomFactor,&s_mem_dc,0,0,width,height,SRCCOPY);
   }
   s_mem_dc.SelectObject (bitmap1);
   d_mem_dc.SelectObject (bitmap2);
   if (Flag_CaptureEnd && DrawMode == IV_DM_DIRECT) DrawSelectionBox (&dc);
}

 void CImageView::DrawImage (DArray2D &s_array,int x,int y,int flag_scaling)

{
   GImage d_image(s_array.Width,s_array.Height);
   Convert  (s_array,d_image,flag_scaling);
   DrawImage (d_image,x,y);
}

 void CImageView::DrawImage (FArray2D &s_array,int x,int y,int flag_scaling)

{
   GImage d_image(s_array.Width,s_array.Height);
   Convert  (s_array,d_image,flag_scaling);
   DrawImage (d_image,x,y);
}

 void CImageView::DrawImage (IArray2D &s_array,int x,int y,int flag_scaling)
 
{
   GImage d_image(s_array.Width,s_array.Height);
   Convert  (s_array,d_image,flag_scaling);
   DrawImage (d_image,x,y);
}

 void CImageView::DrawImage (SArray2D &s_array,int x,int y,int flag_scaling)
 
{
   GImage d_image(s_array.Width,s_array.Height);
   Convert  (s_array,d_image,flag_scaling);
   DrawImage (d_image,x,y);
}

 void CImageView::DrawImage (USArray2D &s_array,int x,int y,int flag_scaling)

{
   GImage d_image(s_array.Width,s_array.Height);
   Convert  (s_array,d_image,flag_scaling);
   DrawImage (d_image,x,y);
}

 void CImageView::DrawImage (GImage &s_image,int x,int y)

{
   CBitmap d_bitmap;

   GetBitmap (s_image,d_bitmap);
   DrawImage  (d_bitmap,x,y,s_image.Width,s_image.Height);
}

 void CImageView::DrawImage (BGRImage &s_image,int x,int y)

{
   CBitmap d_bitmap;

   GetBitmap (s_image,d_bitmap);
   DrawImage  (d_bitmap,x,y,s_image.Width,s_image.Height);
}

 void CImageView::DrawLine (int x1,int y1,int x2,int y2,COLORREF pen_color,int pen_width,int pen_style)

{
   int sx,sy;
   int s_width,s_height;
   CDC mem_dc;

   CClientDC dc(this);
   mem_dc.CreateCompatibleDC (&dc);
   CBitmap *bitmap = mem_dc.SelectObject (&Canvas);
   CPen pen(pen_style,pen_width,pen_color);
   mem_dc.SelectObject (&pen);
   mem_dc.SetBkMode (TRANSPARENT);
   mem_dc.MoveTo    (x1,y1);
   mem_dc.LineTo    (x2,y2);
   mem_dc.SetPixel  (x2,y2,pen_color);
   mem_dc.SelectStockObject (NULL_PEN);
   if (DrawMode == IV_DM_DIRECT) {
      OnPrepareDC (&dc);
      if (ZoomFactor == 1) {
        dc.SelectObject (&pen);
        dc.SetBkMode (TRANSPARENT);
        dc.MoveTo    (x1,y1);
        dc.LineTo    (x2,y2);
        dc.SetPixel  (x2,y2,pen_color);
        dc.SelectStockObject (NULL_PEN);
      }
      else {
         sx = x1;
         sy = y1;
         s_width  = x2 - x1 + 1;
         s_height = y2 - y1 + 1;
         dc.StretchBlt (sx * ZoomFactor,sy * ZoomFactor,s_width * ZoomFactor,s_height * ZoomFactor,&mem_dc,sx,sy,s_width,s_height,SRCCOPY);
      }
   }
   mem_dc.SelectObject (bitmap);
   if (Flag_CaptureEnd && DrawMode == IV_DM_DIRECT) DrawSelectionBox (&dc);
}

 void CImageView::DrawPie (int cx,int cy,int radius,float s_angle,float e_angle,COLORREF pen_color,int pen_width,int pen_style)
 
{
   int sx,sy;
   int s_width,s_height;
   CDC mem_dc;

   CClientDC dc(this);
   mem_dc.CreateCompatibleDC (&dc);
   CBitmap *bitmap = mem_dc.SelectObject (&Canvas);
   CPen pen(pen_style,pen_width,pen_color);
   mem_dc.SelectObject (&pen);
   mem_dc.SetBkMode (TRANSPARENT);
   s_angle *= MC_DEG2RAD;
   e_angle *= MC_DEG2RAD;
   int x1 = cx - radius;
   int y1 = cy - radius;
   int x2 = cx + radius;
   int y2 = cy + radius;
   int x3 = (int)(radius * cos (s_angle) + cx + 0.5f);
   int y3 = (int)(radius * sin (s_angle) + cy + 0.5f);
   int x4 = (int)(radius * cos (e_angle) + cx + 0.5f);
   int y4 = (int)(radius * sin (e_angle) + cy + 0.5f);
   mem_dc.Pie (x1,y1,x2,y2,x3,y3,x4,y4);
   mem_dc.SelectStockObject (NULL_PEN);
   if (DrawMode == IV_DM_DIRECT) {
      OnPrepareDC (&dc);
      if (ZoomFactor == 1) {
         dc.SelectObject (&pen);
         dc.SetBkMode (TRANSPARENT);
         dc.Pie (x1,y1,x2,y2,x3,y3,x4,y4);
         dc.SelectStockObject (NULL_PEN);
      }
      else {
         sx = x1;
         sy = y1;
         s_width  = x2 - x1 + 1;
         s_height = y2 - y1 + 1;
         dc.StretchBlt (sx * ZoomFactor,sy * ZoomFactor,s_width * ZoomFactor,s_height * ZoomFactor,&mem_dc,sx,sy,s_width,s_height,SRCCOPY);
      }
   }
   mem_dc.SelectObject (bitmap);
   if (Flag_CaptureEnd && DrawMode == IV_DM_DIRECT) DrawSelectionBox (&dc);
}

 void CImageView::DrawPixel (int x,int y,COLORREF color)

{
   CDC mem_dc;

   CClientDC dc(this);
   mem_dc.CreateCompatibleDC (&dc);
   CBitmap *bitmap = mem_dc.SelectObject (&Canvas);
   mem_dc.SetPixel (x,y,color);
   if (DrawMode == IV_DM_DIRECT) {
      OnPrepareDC (&dc);
      if (ZoomFactor == 1) dc.SetPixel (x,y,color);
      else dc.StretchBlt  (x * ZoomFactor,y * ZoomFactor,ZoomFactor,ZoomFactor,&mem_dc,x,y,1,1,SRCCOPY);
   }
   mem_dc.SelectObject (bitmap);
   if (Flag_CaptureEnd && DrawMode == IV_DM_DIRECT) DrawSelectionBox (&dc);
}

 void CImageView::DrawPoint (int x,int y,COLORREF color,int shape)

{
   int s_width,s_height;
   int x1,x2,y1,y2,sx,sy;
   CDC mem_dc;

   CClientDC dc(this);
   mem_dc.CreateCompatibleDC (&dc);
   CBitmap *bitmap = mem_dc.SelectObject (&Canvas);
   CPen pen(PS_SOLID,0,color);
   mem_dc.SelectObject (&pen);
   x1 = x - 1, x2 = x + 1;
   y1 = y - 1, y2 = y + 1;
   switch (shape) {
      case PT_DOT :
         mem_dc.SetPixel (x,y,color);
         break;
      case PT_BOX :
         mem_dc.MoveTo (x1,y1);
         mem_dc.LineTo (x2,y1);
         mem_dc.LineTo (x2,y2);
         mem_dc.LineTo (x1,y2);
         mem_dc.LineTo (x1,y1);
         break;
      case PT_CROSS :
         mem_dc.SetPixel (x1,y,color);
         mem_dc.SetPixel (x,y,color);
         mem_dc.SetPixel (x2,y,color);
         mem_dc.SetPixel (x,y1,color);
         mem_dc.SetPixel (x,y2,color);
         break;
      case PT_O :
         mem_dc.SetPixel (x1,y,color);
         mem_dc.SetPixel (x,y1,color);
         mem_dc.SetPixel (x2,y,color);
         mem_dc.SetPixel (x,y2,color);
         break;
      case PT_X :
         mem_dc.SetPixel (x1,y1,color);
         mem_dc.SetPixel (x,y,color);
         mem_dc.SetPixel (x2,y2,color);
         mem_dc.SetPixel (x1,y2,color);
         mem_dc.SetPixel (x2,y1,color);
         break;
      default :
         break;
   }
   mem_dc.SelectStockObject (NULL_PEN);
   if (DrawMode == IV_DM_DIRECT) {
      OnPrepareDC (&dc);
      if (ZoomFactor == 1) {
         dc.SelectObject (&pen);
         switch (shape) {
            case PT_DOT :
               dc.SetPixel (x,y,color);
               break;
            case PT_BOX :
               dc.MoveTo (x1,y1);
               dc.LineTo (x2,y1);
               dc.LineTo (x2,y2);
               dc.LineTo (x1,y2);
               dc.LineTo (x1,y1);
               break;
            case PT_CROSS :
               dc.SetPixel (x1,y,color);
               dc.SetPixel (x,y,color);
               dc.SetPixel (x1,y,color);
               dc.SetPixel (x,y1,color);
               dc.SetPixel (x,y2,color);
               break;
            case PT_O :
               dc.SetPixel (x1,y,color);
               dc.SetPixel (x,y1,color);
               dc.SetPixel (x2,y,color);
               dc.SetPixel (x,y2,color);
               break;
            case PT_X :
               dc.SetPixel (x1,y1,color);
               dc.SetPixel (x,y,color);
               dc.SetPixel (x2,y2,color);
               dc.SetPixel (x1,y2,color);
               dc.SetPixel (x2,y1,color);
               break;
            default :
               break;
         }
         dc.SelectStockObject (NULL_PEN);
      }
      else {
         sx = x1;
         sy = y1;
         s_width  = x2 - x1 + 1;
         s_height = y2 - y1 + 1;
         dc.StretchBlt (sx * ZoomFactor,sy * ZoomFactor,s_width * ZoomFactor,s_height * ZoomFactor,&mem_dc,sx,sy,s_width,s_height,SRCCOPY);
      }
   }
   mem_dc.SelectObject (bitmap);
   if (Flag_CaptureEnd && DrawMode == IV_DM_DIRECT) DrawSelectionBox (&dc);
}

 void CImageView::DrawPolygon (IPoint2D *vertices,int n_vertices,COLORREF pen_color,int pen_width,int pen_style)

{
   int sx,sy;
   int s_width,s_height;
   CDC mem_dc;
   
   CClientDC dc(this);
   mem_dc.CreateCompatibleDC (&dc);
   CBitmap *bitmap = mem_dc.SelectObject (&Canvas);
   CPen pen(pen_style,pen_width,pen_color);
   mem_dc.SelectObject (&pen);
   mem_dc.SelectStockObject (NULL_BRUSH);
   mem_dc.SetBkMode (TRANSPARENT);
   mem_dc.Polygon ((POINT*)vertices,n_vertices);
   mem_dc.SelectStockObject (NULL_PEN);
   if (DrawMode == IV_DM_DIRECT) {
      OnPrepareDC (&dc);
      if (ZoomFactor == 1) {
        dc.SelectObject (&pen);
        dc.SelectStockObject (NULL_BRUSH);  
        dc.SetBkMode (TRANSPARENT);
        dc.Polygon ((POINT*)vertices,n_vertices);
        dc.SelectStockObject (NULL_PEN);
      }
      else {
         IPoint2D min_p,max_p;
         IPoint2D min_x,max_x;
         IP2DArray1D v_array;
         v_array.Import (vertices,n_vertices);
         FindMinMax (v_array,min_p,max_p,min_x,max_x);
         v_array.Import (NULL,0);
         sx = min_p.X;
         sy = min_p.Y;
         s_width  = max_p.X - min_p.X + 1;
         s_height = max_p.Y - min_p.Y + 1;
         dc.StretchBlt (sx * ZoomFactor,sy * ZoomFactor,s_width * ZoomFactor,s_height * ZoomFactor,&mem_dc,sx,sy,s_width,s_height,SRCCOPY);
      }
   }
   mem_dc.SelectObject (bitmap);
   if (Flag_CaptureEnd && DrawMode == IV_DM_DIRECT) DrawSelectionBox (&dc);
}

 void CImageView::DrawSelectionBox (CDC *dc)

{
   int x1,y1,x2,y2;

   CPen pen(PS_DOT,1,PC_RED);
   dc->SelectObject (&pen);
   x1 = StartPos.x * ZoomFactor;
   y1 = StartPos.y * ZoomFactor;
   x2 = EndPos.x   * ZoomFactor;
   y2 = EndPos.y   * ZoomFactor;
   dc->MoveTo (x1,y1);
   dc->LineTo (x2,y1);
   dc->LineTo (x2,y2);
   dc->LineTo (x1,y2);
   dc->LineTo (x1,y1);
   dc->SelectStockObject (NULL_PEN);
}

 void CImageView::DrawRectangle (int x,int y,int width,int height,COLORREF pen_color,int pen_width,int pen_style)

{
   int sx,sy;
   int s_width,s_height;
   CDC mem_dc;
   
   int x2 = x + width  - 1;
   int y2 = y + height - 1;
   CClientDC dc(this);
   mem_dc.CreateCompatibleDC (&dc);
   CBitmap *bitmap = mem_dc.SelectObject (&Canvas);
   CPen pen(pen_style,pen_width,pen_color);
   mem_dc.SelectObject (&pen);
   mem_dc.SelectStockObject (NULL_BRUSH);
   mem_dc.Rectangle (x,y,x2,y2);
   mem_dc.SelectStockObject (NULL_PEN);
   if (DrawMode == IV_DM_DIRECT) {
      OnPrepareDC (&dc);
      if (ZoomFactor == 1) {
         dc.SelectObject (&pen);
         dc.SelectStockObject (NULL_BRUSH);
         dc.Rectangle (x,y,x2,y2);
         dc.SelectStockObject (NULL_PEN);
      }
      else {
         sx = x;
         sy = y;
         s_width  = x2 - x + 1;
         s_height = y2 - y + 1;
         dc.StretchBlt (sx * ZoomFactor,sy * ZoomFactor,s_width * ZoomFactor,s_height * ZoomFactor,&mem_dc,sx,sy,s_width,s_height,SRCCOPY);
      }
   }
   mem_dc.SelectObject (bitmap);
   if (Flag_CaptureEnd && DrawMode == IV_DM_DIRECT) DrawSelectionBox (&dc);
}

 void CImageView::DrawText (LPCTSTR text,int x,int y,COLORREF color,int bk_mode,int font_type,int font_size)

{
   CDC mem_dc;
   CFont font;
   
   int flag_stock_font = FALSE;
   if (font_type <= FT_SYSTEM_FIXED) {
      flag_stock_font = TRUE;
      switch (font_type) {
         case FT_ANSI_FIXED:
            font_type = ANSI_FIXED_FONT;
            break;
         case FT_ANSI_VAR:
            font_type = ANSI_VAR_FONT;
            break;
         case FT_DEVICE_DEFAULT:
            font_type = DEVICE_DEFAULT_FONT;
            break;
         case FT_OEM_FIXED:
            font_type = OEM_FIXED_FONT;
            break;
         case FT_SYSTEM:
            font_type = SYSTEM_FONT;
            break;
         case FT_SYSTEM_FIXED:
            font_type = SYSTEM_FIXED_FONT;
            break;
         default:
            font_type = SYSTEM_FONT;
            break;
      }
   }
   else {
      switch (font_type) {
         case FT_ARIAL:
            font.CreateFont (
               font_size,           // nHeight
               0,                   // nWidth
               0,                   // nEscapement
               0,                   // nOrientation
               FW_EXTRABOLD,        // nWeight
               FALSE,               // bItalic
               FALSE,               // bUnderline
               0,                   // cStrikeOut
               ANSI_CHARSET,        // nCharSet
               OUT_DEFAULT_PRECIS,  // nOutPrecision
               CLIP_DEFAULT_PRECIS, // nClipPrecision
               DEFAULT_QUALITY,     // nQuality
               DEFAULT_PITCH,       // nPitchAndFamily
               _T("Arial")          // lpszFacename
            );
            break;
         case FT_TAHOMA:
            font.CreateFont (
               font_size,           // nHeight
               0,                   // nWidth
               0,                   // nEscapement
               0,                   // nOrientation
               FW_EXTRABOLD,        // nWeight
               FALSE,               // bItalic
               FALSE,               // bUnderline
               0,                   // cStrikeOut
               ANSI_CHARSET,        // nCharSet
               OUT_DEFAULT_PRECIS,  // nOutPrecision
               CLIP_DEFAULT_PRECIS, // nClipPrecision
               DEFAULT_QUALITY,     // nQuality
               DEFAULT_PITCH,       // nPitchAndFamily
               _T("Tahoma")         // lpszFacename
            );
            break;
         case FT_VERDANA:
            font.CreateFont (
               font_size,           // nHeight
               0,                   // nWidth
               0,                   // nEscapement
               0,                   // nOrientation
               FW_EXTRABOLD,        // nWeight
               FALSE,               // bItalic
               FALSE,               // bUnderline
               0,                   // cStrikeOut
               ANSI_CHARSET,        // nCharSet
               OUT_DEFAULT_PRECIS,  // nOutPrecision
               CLIP_DEFAULT_PRECIS, // nClipPrecision
               DEFAULT_QUALITY,     // nQuality
               DEFAULT_PITCH,       // nPitchAndFamily
               _T("Verdana")        // lpszFacename
            );
            break;
         default:
            break;
      }
   }
   CClientDC dc(this);
   mem_dc.CreateCompatibleDC (&dc);
   CBitmap *bitmap = mem_dc.SelectObject (&Canvas);
   int t_len = (int)_tcslen (text);
   mem_dc.SetTextColor (color);
   mem_dc.SetBkMode (bk_mode);
   if (flag_stock_font) mem_dc.SelectStockObject (font_type);
   else mem_dc.SelectObject (&font);
   mem_dc.TextOut (x,y,text,t_len);
   mem_dc.SelectStockObject (SYSTEM_FONT);
   if (DrawMode == IV_DM_DIRECT) {
      OnPrepareDC (&dc);
      if (ZoomFactor == 1) {
         dc.SetTextColor (color);
         dc.SetBkMode  (bk_mode);
         if (flag_stock_font) dc.SelectStockObject (font_type);
         else dc.SelectObject (&font);
         dc.TextOut (x,y,text,t_len);
         dc.SelectStockObject (SYSTEM_FONT);
      }
      else {
         TEXTMETRIC tm;
         mem_dc.GetTextMetrics (&tm);
         int width  = tm.tmMaxCharWidth * t_len;
         int height = tm.tmHeight;
         dc.StretchBlt (x * ZoomFactor,y * ZoomFactor,width * ZoomFactor,height * ZoomFactor,&mem_dc,x,y,width,height,SRCCOPY);
      }
   }
   mem_dc.SelectObject (bitmap);
   if (Flag_CaptureEnd && DrawMode == IV_DM_DIRECT) DrawSelectionBox (&dc);
}

 void CImageView::EraseSelectionBox (   )

{
   int sx,sy;
   int s_width,s_height;
   CDC mem_dc;
   
   CClientDC dc(this);
   OnPrepareDC (&dc);
   mem_dc.CreateCompatibleDC (&dc);
   CBitmap *bitmap = mem_dc.SelectObject (&Canvas);
   sx = StartPos.x - 1;
   sy = StartPos.y - 1;
   s_width  = EndPos.x - StartPos.x + 3;
   s_height = EndPos.y - StartPos.y + 3;
   dc.StretchBlt (sx * ZoomFactor,sy * ZoomFactor,s_width * ZoomFactor,s_height * ZoomFactor,&mem_dc,sx,sy,s_width,s_height,SRCCOPY);
   mem_dc.SelectObject (bitmap);
}

 void CImageView::GetBitmap (GImage &s_image,CBitmap &d_bitmap)

{
   CClientDC dc(this);
   d_bitmap.CreateCompatibleBitmap (&dc,s_image.Width,s_image.Height);
   BITMAPINFO *bm_info = (BITMAPINFO *)BitmapInfo;
   bm_info->bmiHeader.biWidth  =  s_image.Width + s_image.Margin;
   bm_info->bmiHeader.biHeight = -s_image.Height;
   SetDIBits (dc.m_hDC,d_bitmap,0,s_image.Height,s_image,bm_info,DIB_RGB_COLORS);
}

 void CImageView::GetBitmap (BGRImage &s_image,CBitmap &d_bitmap)

{
   CClientDC dc(this);
   d_bitmap.CreateCompatibleBitmap (&dc,s_image.Width,s_image.Height);
   BITMAPINFO bmp_info;
   memset (&bmp_info,0,sizeof(BITMAPINFO));
   bmp_info.bmiHeader.biSize        = sizeof(BITMAPINFOHEADER);
   bmp_info.bmiHeader.biWidth       = s_image.Width + s_image.Margin;
   bmp_info.bmiHeader.biHeight      = -s_image.Height;
   bmp_info.bmiHeader.biPlanes      = 1;
   bmp_info.bmiHeader.biBitCount    = 24;
   bmp_info.bmiHeader.biCompression = BI_RGB;
   SetDIBits (dc.m_hDC,d_bitmap,0,s_image.Height,s_image,&bmp_info,DIB_RGB_COLORS);
}

 void CImageView::GetCanvasSize (int &width,int &height)

{
   width  = CanvasWidth;
   height = CanvasHeight;
}
 
 void CImageView::GetImage (int x,int y,BGRImage &d_image)

{
   CClientDC dc(this);
   CDC s_mem_dc;
   s_mem_dc.CreateCompatibleDC (&dc);
   CDC d_mem_dc;
   d_mem_dc.CreateCompatibleDC (&dc);
   CBitmap d_bitmap;
   d_bitmap.CreateCompatibleBitmap (&dc,d_image.Width,d_image.Height);
   CBitmap *old_bitmap1 = s_mem_dc.SelectObject (&Canvas);
   CBitmap *old_bitmap2 = d_mem_dc.SelectObject (&d_bitmap);
   d_mem_dc.BitBlt (0,0,d_image.Width,d_image.Height,&s_mem_dc,x,y,SRCCOPY);
   BITMAPINFO di_info;
   memset (&di_info,0,sizeof(BITMAPINFO));
   di_info.bmiHeader.biSize        = sizeof(BITMAPINFOHEADER);
   di_info.bmiHeader.biWidth       = d_image.Width;
   di_info.bmiHeader.biHeight      = -d_image.Height;
   di_info.bmiHeader.biPlanes      = 1;
   di_info.bmiHeader.biBitCount    = 24;
   di_info.bmiHeader.biCompression = BI_RGB;
   GetDIBits (d_mem_dc.m_hDC,d_bitmap,0,d_image.Height,d_image,&di_info,DIB_RGB_COLORS);
   s_mem_dc.SelectObject (old_bitmap1);
   d_mem_dc.SelectObject (old_bitmap2);
}

 COLORREF CImageView::GetPixel (int x,int y)

{
   CDC mem_dc;
   
   CClientDC dc(this);
   mem_dc.CreateCompatibleDC (&dc);
   CBitmap *bitmap = mem_dc.SelectObject (&Canvas);
   COLORREF color = mem_dc.GetPixel (x,y);
   mem_dc.SelectObject (bitmap);
   return (color);
}

 int CImageView::GetZoomFactor (   )

{
   return (ZoomFactor);
}

 void CImageView::InitBitmapInfo (   )

{
   int i;
   BITMAPINFO *bmp_info;

   CClientDC dc(this);
   NBitsPerPixel = dc.GetDeviceCaps (BITSPIXEL);
   bmp_info = (BITMAPINFO *)BitmapInfo;
   bmp_info->bmiHeader.biSize        = sizeof(BITMAPINFOHEADER);
   bmp_info->bmiHeader.biPlanes      = 1;
   bmp_info->bmiHeader.biBitCount    = 8;
   bmp_info->bmiHeader.biCompression = BI_RGB;
   for (i = 0; i < 256; i++) {
      bmp_info->bmiColors[i].rgbBlue     = i;
      bmp_info->bmiColors[i].rgbGreen    = i;
      bmp_info->bmiColors[i].rgbRed      = i;
      bmp_info->bmiColors[i].rgbReserved = 0;
   }
}

 void CImageView::InitContextMenu (   )

{
   Menu.CreatePopupMenu (   );
   CMenu smenu1;
   smenu1.CreatePopupMenu (   );
   smenu1.AppendMenu (MF_STRING,IDM_IV_ZOOM_X1,_T("x &1"));
   smenu1.AppendMenu (MF_STRING,IDM_IV_ZOOM_X2,_T("x &2"));
   smenu1.AppendMenu (MF_STRING,IDM_IV_ZOOM_X3,_T("x &3"));
   smenu1.AppendMenu (MF_STRING,IDM_IV_ZOOM_X4,_T("x &4"));
   smenu1.AppendMenu (MF_STRING,IDM_IV_ZOOM_X5,_T("x &5"));
   smenu1.AppendMenu (MF_STRING,IDM_IV_ZOOM_X6,_T("x &6"));
   smenu1.AppendMenu (MF_STRING,IDM_IV_ZOOM_X7,_T("x &7"));
   smenu1.AppendMenu (MF_STRING,IDM_IV_ZOOM_X8,_T("x &8"));
   smenu1.AppendMenu (MF_STRING,IDM_IV_ZOOM_X9,_T("x &9"));
   Menu.AppendMenu   (MF_POPUP,(UINT_PTR)smenu1.Detach (   ),_T("&Zoom"));
   CMenu smenu2;
   smenu2.CreatePopupMenu (   );
   smenu2.AppendMenu (MF_STRING,IDM_IV_CAPTURE_START ,_T("&Start"));
   smenu2.AppendMenu (MF_STRING,IDM_IV_CAPTURE_CANCEL,_T("&Cancel"));
   smenu2.AppendMenu (MF_STRING,IDM_IV_CAPTURE_COPY  ,_T("Co&py"));
   smenu2.AppendMenu (MF_STRING,IDM_IV_CAPTURE_SAVE  ,_T("Sa&ve"));
   Menu.AppendMenu   (MF_POPUP,(UINT_PTR)smenu2.Detach (   ),_T("&Capture"));
   Menu.CheckMenuItem (ZoomFactor + IDM_IV_ZOOM_X1 - 1,MF_CHECKED);
}

 void CImageView::InvertSelectionBox (   )

{
   CClientDC dc(this);
   dc.SetROP2 (R2_NOT);
   dc.MoveTo (StartPos);
   dc.LineTo (EndPos.x,StartPos.y);
   dc.LineTo (EndPos);
   dc.LineTo (StartPos.x,EndPos.y);
   dc.LineTo (StartPos);
}

 void CImageView::OnCmd_Capture_Cancel (   )

{
   Flag_CaptureStart = FALSE;
   if (Flag_CaptureEnd == TRUE) EraseSelectionBox (   );
   Flag_CaptureEnd = FALSE;
}

 void CImageView::OnCmd_Capture_Copy (   )

{
   MessageBox (_T("Sorry! Not implemented yet..."),_T("Error"),MB_OK | MB_ICONERROR);
}

 void CImageView::OnCmd_Capture_Save (   )

{
   BGRImage d_image(EndPos.x - StartPos.x + 1,EndPos.y - StartPos.y + 1);
   GetImage (StartPos.x,StartPos.y,d_image);
   CFileDialog fdlg (FALSE,_T("pcx"),NULL,OFN_HIDEREADONLY,
      _T("BMP Files (*.bmp)|*.bmp|PCX Files (*.pcx)|*.pcx|PGM Files (*.pgm)|*.pgm|RAS Files (*.ras)|*.ras|RAW Files (*.raw)|*.raw|All Files (*.*)|*.*||"),this);
   if (fdlg.DoModal (   ) != IDOK) return;
   StringA path_name;
   ConvertString (fdlg.GetPathName(),path_name);
   int  error = d_image.WriteFile (path_name);
   if (error) {
      if (error < 0) MessageBox (_T("Unsupported file type !"),_T("Error"),MB_OK | MB_ICONERROR);
      else MessageBox (_T("Writing failure!"),_T("Error"),MB_OK | MB_ICONERROR);
   }
   else OnCmd_Capture_Cancel (   );
}

 void CImageView::OnCmd_Capture_Start (   )

{
   Flag_CaptureStart = TRUE;
   Flag_CaptureEnd   = FALSE;
}

 void CImageView::OnCmd_Zoom (UINT id)

{
   Menu.CheckMenuItem (ZoomFactor + IDM_IV_ZOOM_X1 - 1,MF_UNCHECKED);
   Zoom (id - IDM_IV_ZOOM_X1 + 1);
   Menu.CheckMenuItem (ZoomFactor + IDM_IV_ZOOM_X1 - 1,MF_CHECKED);
}

 int CImageView::OnCreate (LPCREATESTRUCT cs)

{
   if (CScrollView::OnCreate (cs) == -1) return (-1);
   if (!NBitsPerPixel) {
      InitBitmapInfo (   );
      CrossCursor = AfxGetApp (   )->LoadStandardCursor (IDC_CROSS);
   }
   InitContextMenu (   );
   CreateCanvas (CanvasWidth,CanvasHeight);
   ClearCanvas  (   );
   return (0);
}

 void CImageView::OnDraw (CDC *dc)

{
   int sx,sy;
   int s_width,s_height;
   CDC mem_dc;
   CRect rect;

   mem_dc.CreateCompatibleDC (dc);
   CBitmap *bitmap = mem_dc.SelectObject (&Canvas);
   dc->GetClipBox (&rect);
   sx = rect.left / ZoomFactor;
   sy = rect.top  / ZoomFactor;
   s_width  = rect.right  / ZoomFactor - sx + 1;
   s_height = rect.bottom / ZoomFactor - sy + 1;
   dc->StretchBlt (sx * ZoomFactor,sy * ZoomFactor,s_width * ZoomFactor,s_height * ZoomFactor,&mem_dc,sx,sy,s_width,s_height,SRCCOPY);
   mem_dc.SelectObject (bitmap);
   if (Flag_CaptureEnd) DrawSelectionBox (dc);
}

 void CImageView::OnKeyDown (UINT key_code,UINT,UINT)

{
   switch (key_code) {
      case VK_HOME :
         OnVScroll (SB_TOP,0,NULL);
         OnHScroll (SB_LEFT,0,NULL);
         break;
      case VK_END :
         OnVScroll (SB_BOTTOM,0,NULL);
         OnHScroll (SB_LEFT,0,NULL);
         break;
      case VK_UP :
         OnVScroll (SB_LINEUP,0,NULL);
         break;
      case VK_DOWN :
         OnVScroll (SB_LINEDOWN,0,NULL);
         break;
      case VK_PRIOR :
         OnVScroll (SB_PAGEUP,0,NULL);
         break;
      case VK_NEXT :
         OnVScroll (SB_PAGEDOWN,0,NULL);
         break;
      case VK_LEFT :
         OnHScroll (SB_LINELEFT,0,NULL);
         break;
      case VK_RIGHT :
         OnHScroll (SB_LINERIGHT,0,NULL);
         break;
      default :
         break;
   }
}

 void CImageView::OnLButtonDown (UINT,CPoint point)

{
   if (Flag_CaptureStart == TRUE) {
      Flag_Dragging = TRUE;
      StartPos = EndPos = point;
      SetCapture (   );
   }
   else if (Flag_CaptureEnd == TRUE) {
      EraseSelectionBox (   );
      Flag_CaptureStart = TRUE;
      Flag_CaptureEnd = FALSE;
      Flag_Dragging = TRUE;
      StartPos = EndPos = point;
      SetCapture (   );
   }
}

 void CImageView::OnLButtonUp (UINT,CPoint point)

{
   int x1,y1,x2,y2;
   
   if (Flag_CaptureStart == TRUE) {
      if (StartPos != point) {
         Flag_CaptureStart = FALSE;
         Flag_CaptureEnd = TRUE;
         EndPos = point;
         InvertSelectionBox (   );
         CPoint base_point = GetScrollPosition (   );
         x1 = (StartPos.x + base_point.x) / ZoomFactor;
         y1 = (StartPos.y + base_point.y) / ZoomFactor;
         x2 = (EndPos.x   + base_point.x) / ZoomFactor;
         y2 = (EndPos.y   + base_point.y) / ZoomFactor;
         if (x1 < x2) StartPos.x = x1;
         else StartPos.x = x2;
         if (y1 < y2) StartPos.y = y1;
         else StartPos.y = y2;
         EndPos.x = StartPos.x + abs (x1 - x2);
         EndPos.y = StartPos.y + abs (y1 - y2);
         CClientDC dc(this);
         OnPrepareDC (&dc);
         DrawSelectionBox (&dc);
      }
      Flag_Dragging = FALSE;
      if (GetCapture (   ) == this) ReleaseCapture (   );
   }
}

 void CImageView::OnMouseMove (UINT,CPoint point)

{
   if (Flag_CaptureStart == TRUE && Flag_Dragging == TRUE) {
      InvertSelectionBox (   );
      EndPos = point;
      InvertSelectionBox (   );
   }
}

 BOOL CImageView::OnMouseWheel (UINT flags,short delta,CPoint point)

{
   if (delta < 0) OnVScroll (SB_LINEDOWN,0,NULL);
   else OnVScroll (SB_LINEUP,0,NULL);
   return (CScrollView::OnMouseWheel (flags,delta,point));
}

 void CImageView::OnRButtonDown (UINT,CPoint point)

{
   if (Flag_CaptureStart == TRUE) {
      Menu.EnableMenuItem (IDM_IV_CAPTURE_START ,MF_GRAYED);
      Menu.EnableMenuItem (IDM_IV_CAPTURE_CANCEL,MF_ENABLED);
      Menu.EnableMenuItem (IDM_IV_CAPTURE_COPY  ,MF_GRAYED);
      Menu.EnableMenuItem (IDM_IV_CAPTURE_SAVE  ,MF_GRAYED);
   }
   else if (Flag_CaptureEnd == TRUE) {
      Menu.EnableMenuItem (IDM_IV_CAPTURE_START ,MF_GRAYED);
      Menu.EnableMenuItem (IDM_IV_CAPTURE_CANCEL,MF_ENABLED);
      Menu.EnableMenuItem (IDM_IV_CAPTURE_COPY  ,MF_ENABLED);
      Menu.EnableMenuItem (IDM_IV_CAPTURE_SAVE  ,MF_ENABLED);
   }
   else {
      Menu.EnableMenuItem (IDM_IV_CAPTURE_START ,MF_ENABLED);
      Menu.EnableMenuItem (IDM_IV_CAPTURE_CANCEL,MF_GRAYED);
      Menu.EnableMenuItem (IDM_IV_CAPTURE_COPY  ,MF_GRAYED);
      Menu.EnableMenuItem (IDM_IV_CAPTURE_SAVE  ,MF_GRAYED);
   }
   ClientToScreen (&point);
   Menu.TrackPopupMenu (TPM_LEFTALIGN | TPM_LEFTBUTTON | TPM_RIGHTBUTTON,point.x,point.y,this);
}

 BOOL CImageView::OnSetCursor (CWnd *window,UINT hit_test,UINT message)

{
   if (hit_test == HTCLIENT) {
      if (Flag_CaptureStart == TRUE || Flag_CaptureEnd == TRUE) {
         ::SetCursor (CrossCursor);
         return (TRUE);
      }
   }
   return CScrollView::OnSetCursor (window,hit_test,message);
}

 void CImageView::OnSize (UINT,int w_width,int w_height)

{
   SetScrollSizes (MM_TEXT,CSize(CanvasWidth * ZoomFactor,CanvasHeight * ZoomFactor),
      CSize(w_width,w_height),CSize(ZoomFactor * 20,ZoomFactor * 20));
}

 BOOL CImageView::PreCreateWindow (CREATESTRUCT &cs)

{
   if (ClassName == NULL) {
      ClassName = AfxRegisterWndClass (CS_HREDRAW | CS_VREDRAW,
         AfxGetApp (   )->LoadStandardCursor (IDC_ARROW),
         (HBRUSH)::GetStockObject (BLACK_BRUSH),
         AfxGetApp (   )->LoadStandardIcon (IDI_APPLICATION));
   }
   cs.lpszClass = ClassName;
   return (CScrollView::PreCreateWindow (cs));
}

 void CImageView::SetCanvasSize (int width,int height)

{
   CRect rect;

   Canvas.DeleteObject (   );
   CreateCanvas (width,height);
   ClearCanvas  (   );
   ClearWindow  (   );
   GetClientRect (&rect);
   OnSize (0,rect.right,rect.bottom);
}

 void CImageView::SetDrawMode (int mode)
 
{
   DrawMode = mode;
}

 void CImageView::Zoom (int zoom_factor)

{ 
   CRect rect;

   CPoint sp = GetScrollPosition (   );
   sp.x = sp.x / ZoomFactor * zoom_factor;
   sp.y = sp.y / ZoomFactor * zoom_factor;
   ZoomFactor = zoom_factor;
   GetClientRect (&rect);
   OnSize (0,rect.right,rect.bottom);
   ScrollToPosition (sp);
   Invalidate (   );
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CImageViewWnd
//
///////////////////////////////////////////////////////////////////////////////

LPCTSTR CImageViewWnd::ClassName = NULL;

BEGIN_MESSAGE_MAP (CImageViewWnd,CWnd)
   ON_WM_CREATE (   )
   ON_WM_SIZE   (   )
END_MESSAGE_MAP (   )

 CImageViewWnd::CImageViewWnd (   )

{
   ImageView = NULL;
}

 BOOL CImageViewWnd::Create (LPCTSTR lpszWindowName,const RECT &rect)
 
{
   if (ClassName == NULL) {
      ClassName = AfxRegisterWndClass (CS_HREDRAW | CS_VREDRAW,
         AfxGetApp (   )->LoadStandardCursor (IDC_ARROW),
         (HBRUSH)::GetStockObject (BLACK_BRUSH),
         AfxGetApp (   )->LoadStandardIcon (IDI_APPLICATION));
   }
   return (CreateEx (WS_EX_OVERLAPPEDWINDOW,ClassName,lpszWindowName,WS_OVERLAPPEDWINDOW|WS_VISIBLE,rect,NULL,0));
}

 int CImageViewWnd::OnCreate (LPCREATESTRUCT lpcs)

{
   if (CWnd::OnCreate (lpcs) == -1) return (-1);
   ImageView = new CImageView;
   ImageView->Create (NULL,NULL,WS_CHILD|WS_VISIBLE,CRect(0,0,0,0),this,0);
   return (0);
}

 void CImageViewWnd::OnSize (UINT nType,int cx,int cy)
 
{
   ImageView->MoveWindow (0,0,cx,cy);
   CWnd::OnSize (nType,cx,cy);
}

 void CImageViewWnd::PostNcDestroy (   )
 
{
   CWnd::PostNcDestroy (   );
   delete this;
}

}

#include "Win32CL/TextView.h"

 namespace Win32CL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: CTextOutCtrl
//
///////////////////////////////////////////////////////////////////////////////

#define DEF_MAX_NUM_LINES      1000
#define TEXT_BUFFER_SIZE       512
#define TEXT_ITEM_QUEUE_SIZE   1000
#define IDM_TOC_CLEAR          31001
#define IDM_TOC_COPY           31002
 
BEGIN_MESSAGE_MAP (CTextOutCtrl,CRichEditCtrl)
   ON_WM_CREATE      (   )
   ON_WM_DESTROY     (   )
   ON_WM_RBUTTONDOWN (   )
   ON_COMMAND        (IDM_TOC_CLEAR,OnCmd_Clear)
   ON_COMMAND        (IDM_TOC_COPY ,OnCmd_Copy)
END_MESSAGE_MAP (   ) 

 CTextOutCtrl::CTextOutCtrl (   )

{
   NumLines                   = DEF_MAX_NUM_LINES;
   TextFormat.cbSize          = sizeof(CHARFORMAT);
   TextFormat.dwMask          = CFM_BOLD | CFM_ITALIC | CFM_UNDERLINE;
   TextFormat.dwEffects       = 0;
   TextFormat.bCharSet        = ANSI_CHARSET;
   TextFormat.bPitchAndFamily = DEFAULT_PITCH;
   TextItemQueue.QueueLength  = TEXT_ITEM_QUEUE_SIZE;
}

 int CTextOutCtrl::AddToTextItemQueue (CTextItem* s_item)

{
   CLock lock(CS_TextItemQueue);
   if (TextItemQueue.Add (s_item)) return (1);
   Event_NewTextItem.SetEvent (   );
   return (DONE);
}

 void CTextOutCtrl::Clear (   )

{
   SetWindowText (_T(""));
}

 void CTextOutCtrl::InitContextMenu (   )

{
   Menu.CreatePopupMenu (   );
   Menu.AppendMenu (MF_STRING,IDM_TOC_CLEAR,_T("&Clear"));
   Menu.AppendMenu (MF_STRING,IDM_TOC_COPY ,_T("Co&py"));
}

 void CTextOutCtrl::OnCmd_Clear (   )

{
   Clear (   );
}

 void CTextOutCtrl::OnCmd_Copy (   )

{
   Copy (   );
}
 
 int CTextOutCtrl::OnCreate (LPCREATESTRUCT cs)

{
   if (CRichEditCtrl::OnCreate (cs) == -1) return (-1);
   InitContextMenu (   );
   StartThread (   );
   return (0);
}

 void CTextOutCtrl::OnDestroy (   )

{
   StopThread (   );
   CRichEditCtrl::OnDestroy (   );
}

 void CTextOutCtrl::OnRButtonDown (UINT,CPoint point)

{
   long start,end;

   GetSel (start,end);
   if (start == end) Menu.EnableMenuItem (IDM_TOC_COPY,MF_GRAYED);
   else Menu.EnableMenuItem (IDM_TOC_COPY,MF_ENABLED);
   ClientToScreen (&point);
   Menu.TrackPopupMenu (TPM_LEFTALIGN | TPM_LEFTBUTTON | TPM_RIGHTBUTTON,point.x,point.y,this);
}

 BOOL CTextOutCtrl::PreCreateWindow (CREATESTRUCT &cs)

{
   cs.style |= WS_CHILD | WS_VISIBLE | WS_HSCROLL | WS_VSCROLL | ES_MULTILINE | ES_AUTOHSCROLL | ES_AUTOVSCROLL | ES_READONLY;
   return (CRichEditCtrl::PreCreateWindow (cs));
}
 
 void CTextOutCtrl::Print (LPCTSTR format,...)

{
   int i,j;
   TCHAR s_buffer[TEXT_BUFFER_SIZE];
   TCHAR d_buffer[TEXT_BUFFER_SIZE];
   va_list arg_list;
   va_start (arg_list,format);
   #if defined(_UNICODE)
   vswprintf (s_buffer,sizeof(s_buffer) / sizeof(TCHAR),format,arg_list);
   #else
   vsprintf (s_buffer,format,arg_list);
   #endif
   va_end  (arg_list);
   int n_chars = GetTextLength (   );
   SetSel (n_chars,n_chars);
   SetSelectionCharFormat (TextFormat);
   for (i = j = 0; s_buffer[i]; i++) {
      if (s_buffer[i] == _T('\n')) {
         d_buffer[j++] = _T('\r');
         d_buffer[j++] = _T('\n');
      }
      else d_buffer[j++] = s_buffer[i];
   }
   d_buffer[j] = 0;
   ReplaceSel (d_buffer);
   int n_lines = GetLineCount (   ) - NumLines;
   if (n_lines > 0) {
      n_chars = LineIndex (n_lines);
      if (n_chars > 0) {
         SetSel (0,n_chars);
         SetReadOnly (FALSE);
         CRichEditCtrl::Clear (   );
         SetReadOnly (TRUE);
         n_chars = GetTextLength (   );
         SetSel (n_chars,n_chars);
      }
   }
   // 마지막 라인이 항상 보이도록 스크롤한다.
   CRect rect;
   GetClientRect (&rect);
   int n1 = LineFromChar (CharFromPos (CPoint(0,rect.bottom))); // 마지막으로 보이는 라인의 위치
   int n2 = GetLineCount (   );
   LineScroll (n2 - n1 - 1);
}

 void CTextOutCtrl::Print (COLORREF color,int style,LPCTSTR text)

{
   SetTextColor (color);
   SetTextStyle (style,TRUE);
   Print (text);
   SetTextColor (RGB(0,0,0));
   SetTextStyle (style,FALSE);
}

 int CTextOutCtrl::Print2 (COLORREF color,int style,LPCTSTR format,...)
 
{
   TCHAR s_buffer[TEXT_BUFFER_SIZE];
   va_list arg_list;
   va_start (arg_list,format);
   #if defined(_UNICODE)
   vswprintf (s_buffer,sizeof(s_buffer) / sizeof(TCHAR),format,arg_list);
   #else
   vsprintf (s_buffer,format,arg_list);
   #endif
   va_end (arg_list);
   CTextItem* t_item = new CTextItem;
   t_item->Color = color;
   t_item->Style = style;
   t_item->Text  = s_buffer;
   if (AddToTextItemQueue (t_item)) {
      delete t_item;
      return (1);
   }
   return (DONE);
}

 CTextItem* CTextOutCtrl::RetrieveFromTextItemQueue (   )

{
   CLock lock(CS_TextItemQueue);
   return (TextItemQueue.Retrieve (   ));
}

 void CTextOutCtrl::SetMaxNumLines (int n_lines)

{
   Clear (   );
   NumLines = n_lines;
}

 void CTextOutCtrl::SetTextColor (COLORREF color)

{
   TextFormat.dwMask     |= CFM_COLOR;
   TextFormat.crTextColor = color;
}

 void CTextOutCtrl::SetTextFont (LPCTSTR font_name)

{
   TextFormat.dwMask  |= CFM_FACE;
   _tcscpy (TextFormat.szFaceName,font_name);
}

 void CTextOutCtrl::SetTextSize (int size)

{
   TextFormat.dwMask |= CFM_SIZE;
   TextFormat.yHeight = size * 20;
}

 void CTextOutCtrl::SetTextStyle (int style,int activate)

{
   int effects;
    
   effects = 0;
   TextFormat.dwEffects &= ~(CFE_BOLD | CFE_ITALIC | CFE_UNDERLINE);
   if (style & TS_NORMAL   ) return;
   if (style & TS_BOLD     ) effects |= CFE_BOLD;
   if (style & TS_ITALIC   ) effects |= CFE_ITALIC;
   if (style & TS_UNDERLINE) effects |= CFE_UNDERLINE;
   if (activate == TRUE) TextFormat.dwEffects |= effects;
   else TextFormat.dwEffects &= ~effects;
}

 void CTextOutCtrl::ThreadProc (   )

{
   while (TRUE) {
      if (CheckStopEvent (   )) break;
      WaitForSingleObject (Event_NewTextItem,300);
      while (TRUE) {
         CTextItem* t_item = RetrieveFromTextItemQueue (   );
         if (t_item == NULL) break;
         Print (t_item->Color,t_item->Style,t_item->Text);
         delete t_item;
      }
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CTextView
//
///////////////////////////////////////////////////////////////////////////////

IMPLEMENT_DYNCREATE (CTextView,CView)

BEGIN_MESSAGE_MAP (CTextView,CView)
   ON_WM_CREATE (   )
   ON_WM_SIZE   (   )
END_MESSAGE_MAP (   )

 void CTextView::Clear (   )

{
   TextOutCtrl.Clear (   );
}
 
 int CTextView::OnCreate (LPCREATESTRUCT cs)
 
{
   if (CView::OnCreate (cs) == -1) return (-1);
   TextOutCtrl.Create (0,CRect (0,0,0,0),this,0);
   return (0);
}

 void CTextView::OnDraw (CDC *dc)

{

}

 void CTextView::OnSize (UINT type,int width,int height)

{
   TextOutCtrl.MoveWindow (0,0,width,height);
}

 void CTextView::Print (LPCTSTR format,...)

{
   TCHAR buffer[MAX_BUFFER_SIZE];

   va_list arg_list;
   va_start   (arg_list,format);
   _vstprintf (buffer,format,arg_list);
   TextOutCtrl.Print (buffer);
}

 void CTextView::SetMaxNumLines (int n_lines)

{
   TextOutCtrl.SetMaxNumLines (n_lines);
}

 void CTextView::SetTextColor (COLORREF color)

{
   TextOutCtrl.SetTextColor (color);
}

 void CTextView::SetTextFont (LPCTSTR font_name)

{
   TextOutCtrl.SetTextFont (font_name);
}

 void CTextView::SetTextSize (int size)

{
   TextOutCtrl.SetTextSize (size);
}

 void CTextView::SetTextStyle (int style,int activate)

{
   TextOutCtrl.SetTextStyle (style,activate);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CTextViewWnd
//
///////////////////////////////////////////////////////////////////////////////

BEGIN_MESSAGE_MAP (CTextViewWnd,CFrameWnd)
   ON_WM_CREATE (   )
END_MESSAGE_MAP (   )
 
 CTextViewWnd::CTextViewWnd (   )
 
{
   TextView = NULL;
}

 int CTextViewWnd::OnCreate (LPCREATESTRUCT cs)
 
{
   if (CFrameWnd::OnCreate (cs) == -1) return (-1);
   TextView = new CTextView;
   TextView->Create (NULL,NULL,WS_CHILD|WS_VISIBLE,CRect(0,0,0,0),this,AFX_IDW_PANE_FIRST,NULL);
   return (0);
}

}

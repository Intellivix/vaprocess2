#pragma once

#include "Win32CL/ImageView.h"

 namespace Win32CL
 
{

// String Conversion
void ConvertString     (const char* s_string,CString& d_string);
void ConvertString     (CString& s_string,StringA& d_string);
void ConvertStringUTF8 (const char* s_string,CString& d_string);
void ConvertStringUTF8 (CString& s_string,StringA& d_string);

// Drawing
void DrawGraph (CImageView *iv,FArray1D& s_array,int x,int y,int height = 100,COLORREF color = RGB(0,255,0),int flag_bg_erase = TRUE,int flag_rotate = FALSE,float threshold = MC_VLV);
void GetBitmap (CDC* dc,BGRImage& s_image,CBitmap& d_bitmap);

// Useful Dialog Boxes
void OpenPauseBox     (   );
void OpenMessageBox   (LPCTSTR format,...);
int  OpenTextInputBox (LPCTSTR message,LPCTSTR s_string,LPTSTR d_string,int buffer_size = 256);

// Mac Address
int GetMACAddress (StringA& mac_addr);

// Buffer Loading & Saving
int LoadFileIntoBuffer (LPCTSTR file_name,BArray1D& d_buffer,int flag_add_null);
int SaveBufferIntoFile (LPCTSTR file_name,void* s_buffer,int s_buf_size);

// Bitmap Handling
void GetBitmap (CDC* dc,BGRImage& s_image,CBitmap& d_bitmap);
void GetBitmap (CDC* dc,BGRImage& s_image,ISize2D& bm_size,CBitmap& d_bitmap);

}

#pragma once
 
#include "Win32CLDef.h"
#include <vector>
#include <mmsystem.h>

 namespace Win32CL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: CPerformanceTimer
//
///////////////////////////////////////////////////////////////////////////////

 class CPerformanceTimer
 
{
   protected:
      UINT64 Frequency;
      UINT64 StartTime;
      
   public:
      CPerformanceTimer (   );
      
   public:
      double GetElapsedTime (   );
      void   Reset          (   );
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: CTimer
//
///////////////////////////////////////////////////////////////////////////////

 class CTimer
 
{
   protected:
      CEvent   Event_Timer;
      MMRESULT TimerID;
      
   public: // Input Parameters
      uint Period; // Period in msec
   
   public:
      CTimer (   );
      
   public:
      int CheckTimerEvent (uint blocking_time = INFINITE);
      int StartTimer      (uint period,int flag_periodic = TRUE);
      int StopTimer       (   );
};

}

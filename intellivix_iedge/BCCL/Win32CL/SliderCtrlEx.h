#pragma once

#include "Win32CLDef.h"

 namespace Win32CL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Class: CSliderCtrlEx
//
///////////////////////////////////////////////////////////////////////////////

 class CSliderCtrlEx : public CSliderCtrl

{
   DECLARE_DYNAMIC(CSliderCtrlEx)

   protected:
      afx_msg void OnLButtonDown (UINT flags,CPoint point);
      afx_msg void OnSetFocus    (CWnd *old_wnd);

   DECLARE_MESSAGE_MAP (   )
};

}

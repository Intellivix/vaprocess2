#pragma once

#include "bccl.h"
#include <d3d9.h>
#if _MSC_VER < 1600
#include <d3dx9.h> // jun : vc2010에서는 컴파일 시 에러 발생
#endif

 namespace Win32CL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Class: CD3DDevice
//
///////////////////////////////////////////////////////////////////////////////

 class CD3DDevice
 
{
   protected:
      LPDIRECT3DDEVICE9 Device;
      
   public:
      D3DPRESENT_PARAMETERS PresentParams;
   
   public:
       CD3DDevice (   );
      ~CD3DDevice (   );
   
   public:
      int operator !                 (   ) const;
          operator LPDIRECT3DDEVICE9 (   ) const;
   
   public:
      int  Clear               (D3DCOLOR color);
      int  Create              (HWND h_wnd,int width,int height,D3DSWAPEFFECT swap_effect = D3DSWAPEFFECT_FLIP);
      void Delete              (   );
      D3DFORMAT GetPixelFormat (   );
      int  GetSize             (int &width,int &height);
      int  Present             (const RECT *s_rect = NULL,const RECT *d_rect = NULL);
      int  Reset               (int width,int height);
};

 inline int CD3DDevice::operator ! (   ) const
 
{
   if (Device) return (FALSE);
   else return (TRUE);
}

 inline CD3DDevice::operator LPDIRECT3DDEVICE9 (   ) const
 
{
   return (Device);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CD3DSurface
//
///////////////////////////////////////////////////////////////////////////////

#define D3D_PIXELFORMAT_BGR24   D3DFMT_R8G8B8
#define D3D_PIXELFORMAT_YUY2    D3DFMT_YUY2
 
 class CD3DSurface
 
{
   protected:
      CDC DC;
      LPDIRECT3DDEVICE9  Device;
      LPDIRECT3DSURFACE9 Surface;
      
   public:
      CD3DSurface (   );
     ~CD3DSurface (   );
     
   public:
      int operator !                  (   ) const;
          operator LPDIRECT3DSURFACE9 (   ) const;
      
   public:
      int  Blit                   (LPDIRECT3DSURFACE9 s_surface,LPRECT s_rect = NULL,LPRECT d_rect = NULL);
      void Delete                 (   );
      int  Clear                  (D3DCOLOR color);
      int  CreateOffscreenSurface (LPDIRECT3DDEVICE9 device,int width,int height,D3DFORMAT pixel_format);
      int  FillRect               (LPRECT s_rect,D3DCOLOR color);
      CDC* GetDC                  (   );
      int  GetBackBuffer          (LPDIRECT3DDEVICE9 device,int bb_no = 0);
      int  GetImage_YUY2          (byte *_buffer);
      D3DFORMAT GetPixelFormat    (   );
      int  GetSize                (int &width,int &height);
      int  DrawImage               (BGRImage &s_image);
      int  PutImage_YUY2          (byte *s_buffer);
      void ReleaseDC              (   );
};

 inline int CD3DSurface::operator ! (   ) const
 
{
   if (Surface) return (FALSE);
   return (TRUE);
}

 inline CD3DSurface::operator LPDIRECT3DSURFACE9 (   ) const
 
{
   return (Surface);
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

void CloseDirect3D (   );
int  InitDirect3D  (   );

}

#if !defined(__bccl_websock_client_H)
#define __bccl_websock_client_H

#include "bccl_define.h"

#if defined(__LIB_WEBSOCKETS)

#include "libwebsockets.h"

typedef libwebsocket_context          WebSocketContext;
typedef libwebsocket                  WebSocket;
typedef libwebsocket_callback_reasons WebSocketEventID;
typedef int(*WebSocketEventHandler)(WebSocketContext* context,WebSocket* socket,WebSocketEventID event_id,void* s_buffer,int s_buf_size,void* user_data);

 namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: WebSocket_Client
//
///////////////////////////////////////////////////////////////////////////////

 class WebSocket_Client

{
   protected:
      libwebsocket_protocols Protocols[2];
      libwebsocket_context*  SocketCtx;
      libwebsocket*          Socket;

   protected:
      WebSocketEventHandler EventHandler;
      void*                 UserData;
      
   protected:
      static int Callback_Event (WebSocketContext* context,WebSocket* socket,WebSocketEventID event_id,void* user,void* s_buffer,size_t s_buf_size);

   protected:
      virtual int OnEvent (WebSocketEventID event_id,void* s_buffer,int s_buf_size);

   public:
      WebSocket_Client (   );
      virtual ~WebSocket_Client (   );
   
   public:
      virtual void Close (   );

   public:
      // Connects to a server via WebSocket.
      // ws_path       : Websocket path on server (ex: url = ws://192.168.0.100/wsapi => path = /wsapi)
      // ssl_connection: 0 = ws://, 1 = wss:// encrypted, 2 = wss:// allow self-signed certs
      // protocol_name : Comma-separated list of protocols being asked for from the server. The server will pick the one it likes best.
      // port_no       : if port_no == -1, default port number will be used. (ws:// => 80, wss:// => 443)
      int Open (const char* ip_address,const char* ws_path = NULL,int ssl_connection = 0,const char* protocol_name = NULL,int port_no = -1);
      // Asks for an "LWS_CALLBACK_CLIENT_WRITABLE" event to be issued when the socket becomes able to be written to without blocking.
      int RequestWritableEvent (   );
      // Processes events. It should be called periodically in a main loop.
      // timeout: time-out period in msec.
      int Run (int timeout = 10);
      // Sets an event handler.
      // user_data: User data to be passed to the handler.
      void SetEventHandler (WebSocketEventHandler handler,void* user_data);
      // Writes data.
      // Return value: >= 0: Length of data written
      //               <  0: Error
      int Write (void* s_buffer,int s_buf_size,int flag_binary = FALSE);
};

}

#endif

#endif

﻿#if !defined(__BCCL_COLOR_H)
#define __BCCL_COLOR_H

#include "bccl_image.h"

 namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// Definitions
//
///////////////////////////////////////////////////////////////////////////////

#define YV12_FORMAT_STANDARD    0x00000000
#define YV12_FORMAT_OYTO        0x00000001
#define YV12_FORMAT_HIKVISION   0x00000002

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////
uint GetYUY2ImageLength(int width, int heigth);
void ConvertNV12MToYUY2(byte* s_buffer_y, byte* s_buffer_uv, int width, int height, byte* d_buffer);
void ConvertNV12MToGray(byte* s_buffer_y, int width, int height, byte* d_buffer);

void ClearImageBuffer_YUY2 (byte* s_buffer,int width,int height);
void ConvertBGRToHSV       (BGRPixel& cp,int& ch,int& cs,int& cv);
void ConvertBGRToYUV       (BGRPixel& cp,byte& cy,byte& cu,byte& cv);
void ConvertBGRToYUV       (const BGRImage& s_cimage,YUVImage2& d_cimage);
void ConvertBGRToYUY2      (const BGRImage& s_cimage,byte* d_buffer);
void ConvertBGRToYV12      (BGRImage& s_cimage,byte* d_buffer,int yv12_format);
void ConvertGrayToYUY2     (GImage& s_image,byte* d_buffer);
void ConvertHSVToBGR       (byte ch,byte cs,byte cv,BGRPixel& cp);
void ConvertYUVToBGR       (byte cy,byte cu,byte cv,BGRPixel& cp);
void ConvertYUY2ToBGR      (byte* s_buffer,BGRImage& d_cimage); // 주의: 영상의 너비가 4의 배수여야 한다.
void ConvertYUY2ToGray     (byte* s_buffer,GImage& d_image);
void ConvertYUY2ToYUV      (byte* s_buffer,YUVImage& d_cimage);
void ConvertYV12ToGray     (byte* s_buffer,GImage& d_image,int yv12_format = YV12_FORMAT_STANDARD);
void ConvertYUY2ToYV12     (byte* s_buffer,int width,int height,byte* d_buff_y,byte* d_buff_u,byte* d_buff_v);
void ConvertYUY2ToYV12     (byte* s_buffer,int width,int height,byte* d_buff_y,int yv12_format);
void ConvertYV12ToBGR      (byte* s_buffer,BGRImage& d_cimage,int yv12_format);
void ConvertYV12ToYUV2     (byte* s_buff_y,byte* s_buff_u,byte* s_buff_v,int width,int height,int stride_y,int stride_uv,byte* d_buffer);
void ConvertYV12ToYUY2     (byte *s_buffer,int width,int height,int stride_y,int stride_uv,byte *d_buffer,int yv12_format = YV12_FORMAT_STANDARD);
void ConvertYV12ToYUY2     (byte* s_buffer,int width,int height,byte* d_buffer,int yv12_format = YV12_FORMAT_STANDARD);
void ConvertUYVYToYUY2     (byte* s_buffer,int width,int height,byte* d_buffer);
int  GetSaturation         (BGRColor& color);
void ReplaceYComponents_YUY2 (byte* s_buffer,GImage& s_image);

}

#endif

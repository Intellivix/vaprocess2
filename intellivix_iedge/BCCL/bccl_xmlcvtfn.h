#ifndef BCCL_XMLCVTFN_H
#define BCCL_XMLCVTFN_H

#include "bccl_cpoint.h"
#include "bccl_cstring.h"
#include "bccl_ctime.h"
#include "bccl_image.h"
#include "bccl_string.h"
#include "bccl_time.h"

#include <map>

#if defined( __WIN32 )
#pragma warning( disable : 4786 )
#pragma warning( disable : 4099 )
#else
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#endif

#define DEFINE_TYPE_ID( type, nId ) \
   const int type_##type = nId;

#define TYPE_ID( type ) \
   type_##type

namespace BCCL {

// bool
DEFINE_TYPE_ID( bool, 1001 )
DEFINE_TYPE_ID( BOOL, 1001 )

// char
DEFINE_TYPE_ID( char, 1002 )
DEFINE_TYPE_ID( CHAR, 1002 )
DEFINE_TYPE_ID( ichar, 1003 )
DEFINE_TYPE_ID( ICHAR, 1003 )
DEFINE_TYPE_ID( byte, 1004 )
DEFINE_TYPE_ID( BYTE, 1004 )
DEFINE_TYPE_ID( xchar, 1005 )
DEFINE_TYPE_ID( xbyte, 1005 )
DEFINE_TYPE_ID( XCHAR, 1005 )
DEFINE_TYPE_ID( XBYTE, 1005 )
DEFINE_TYPE_ID( b64byte, 1030 )
DEFINE_TYPE_ID( B64BYTE, 1031 )

// short
DEFINE_TYPE_ID( short, 1006 )
DEFINE_TYPE_ID( SHORT, 1006 )
DEFINE_TYPE_ID( ushort, 1007 )
DEFINE_TYPE_ID( WORD, 1007 )
DEFINE_TYPE_ID( xshort, 1008 )
DEFINE_TYPE_ID( XSHORT, 1008 )

// int
DEFINE_TYPE_ID( int, 1009 )
DEFINE_TYPE_ID( INT, 1009 )
DEFINE_TYPE_ID( uint, 1010 )
DEFINE_TYPE_ID( UINT, 1010 )
DEFINE_TYPE_ID( xint, 1011 )
DEFINE_TYPE_ID( XINT, 1011 )

// long
DEFINE_TYPE_ID( long, 1012 )
DEFINE_TYPE_ID( LONG, 1012 )
DEFINE_TYPE_ID( ULONG, 1013 )
DEFINE_TYPE_ID( DWORD, 1013 )
DEFINE_TYPE_ID( xlong, 1014 )
DEFINE_TYPE_ID( XLONG, 1014 )

// int64
DEFINE_TYPE_ID( INT64, 1015 )
DEFINE_TYPE_ID( UINT64, 1016 )
DEFINE_TYPE_ID( xint64, 1017 )
DEFINE_TYPE_ID( XINT64, 1017 )

// float
DEFINE_TYPE_ID( float, 1018 )
DEFINE_TYPE_ID( FLOAT, 1018 )

// double
DEFINE_TYPE_ID( double, 1019 )
DEFINE_TYPE_ID( DOUBLE, 1019 )

// etc
DEFINE_TYPE_ID( StringA, 2001 )
DEFINE_TYPE_ID( ILine2D, 2002 )
DEFINE_TYPE_ID( IPoint2D, 2003 )
DEFINE_TYPE_ID( FPoint2D, 2004 )
DEFINE_TYPE_ID( FPoint3D, 2005 )
DEFINE_TYPE_ID( ISize2D, 2006 )
DEFINE_TYPE_ID( IBox2D, 2007 )
DEFINE_TYPE_ID( FBox2D, 2008 )
DEFINE_TYPE_ID( BGRColor, 2009 )

DEFINE_TYPE_ID( string, 3001 )
DEFINE_TYPE_ID( SYSTEMTIME, 3002 )
DEFINE_TYPE_ID( FILETIME, 3003 )
DEFINE_TYPE_ID( IPADDR, 3004 )

DEFINE_TYPE_ID( CRect, 3101 )
DEFINE_TYPE_ID( CSize, 3102 )
DEFINE_TYPE_ID( CPoint, 3103 )
DEFINE_TYPE_ID( CTime, 3104 )
DEFINE_TYPE_ID( CStringA, 3105 )

BOOL ConvertValueTypeToString( int nTypeID, std::string& output_string, void* input_value, void* default_string, char* szFormat, int nNum );
BOOL ConvertStringToValueType( int nTypeID, const char* input_value, void* output_value, void* default_value, char* szFormat, int nNum );

} // namespace BCCL

#if defined( __WIN32 )
#else
#pragma GCC diagnostic pop
#endif

#endif // BCCL_XMLCVTFN_H

#include "bccl_drawing.h"

 namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 int ClipLine (int si_width,int si_height,int &x1,int &y1,int &x2,int &y2)
 
{
   int right  = si_width  - 1;
   int bottom = si_height - 1;
   if (si_width <= 0 || si_height <= 0) return (1);
   int c1 = (x1 < 0) + (x1 > right) * 2 + (y1 < 0) * 4 + (y1 > bottom) * 8;
   int c2 = (x2 < 0) + (x2 > right) * 2 + (y2 < 0) * 4 + (y2 > bottom) * 8;
   if ((c1 & c2) == 0 && (c1 | c2) != 0 ) {
      int a;
      if (c1 & 12) {
         a = c1 < 8 ? 0 : bottom;
         x1 +=  (a - y1) * (x2 - x1) / (y2 - y1);
         y1 = a;
         c1 = (x1 < 0) + (x1 > right) * 2;
      }
      if (c2 & 12) {
         a = c2 < 8 ? 0 : bottom;
         x2 += (a - y2) * (x2 - x1) / (y2 - y1);
         y2 = a;
         c2 = (x2 < 0) + (x2 > right) * 2;
      }
      if ((c1 & c2) == 0 && (c1 | c2) != 0) {
         if (c1) {
            a = c1 == 1 ? 0 : right;
            y1 += (a - x1) * (y2 - y1) / (x2 - x1);
            x1 = a;
            c1 = 0;
         }
         if (c2) {
            a = c2 == 1 ? 0 : right;
            y2 += (a - x2) * (y2 - y1) / (x2 - x1);
            x2 = a;
            c2 = 0;
         }
      }
    }
    if ((c1 | c2) == 0) return (DONE);
    else return (2);
}

 extern "C"
 int Compare_PolygonEdge_Ascending (const void *a,const void *b)

{
   if      (((PolygonEdge *)a)->MinY < ((PolygonEdge *)b)->MinY) return (-1);
   else if (((PolygonEdge *)a)->MinY > ((PolygonEdge *)b)->MinY) return ( 1);
   else return (0);
}

}

#if !defined( __BCCL_FILE_H )
#define __BCCL_FILE_H

#include "bccl_array.h"

#include <string>

namespace BCCL {

///////////////////////////////////////////////////////////////////////////////
//
// Definitions
//
///////////////////////////////////////////////////////////////////////////////

// FileIO Status/Options
#define FIO_BINARY 0
#define FIO_TEXT 1

#define FIO_CREATE 0
#define FIO_READ 1
#define FIO_WRITE 2
#define FIO_APPEND 3

#define FIO_SEEK_BEGIN 0
#define FIO_SEEK_CURRENT 1
#define FIO_SEEK_END 2

enum FIO_STORAGE {
   FIO_STORAGE_MEMORY = 0,
   FIO_STORAGE_FILE   = 1,
};

#define FIO_INCLUDE_NEWLINE 0
#define FIO_REMOVE_NEWLINE 1

// File Access Mode
#define FAC_EXISTENCE 0
#define FAC_READ_ONLY 4
#define FAC_WRITE_ONLY 6
#define FAC_READ_WRITE 8

// Image File Type
#define IFT_NONE 0
#define IFT_BMP 1
#define IFT_GIF 2
#define IFT_JPG 3
#define IFT_PCX 4
#define IFT_PGM 5
#define IFT_PNG 6
#define IFT_RAS 7
#define IFT_SGI 8
#define IFT_TIF 9

///////////////////////////////////////////////////////////////////////////////
//
// Class: FileIO
//
///////////////////////////////////////////////////////////////////////////////

class FileIO

{
protected:
   int Attribute;
   int Mode;
   int Length;
   int CurPos;
   int BufExpSize;
   int BufferSize;
   int Flag_Attached;
   FILE* File;
   byte* Buffer;
   std::string m_filename;

public:
   FileIO();
   FileIO( const char* file_name, int attribute, int mode );
   FileIO( byte* s_buffer, int length );
   virtual ~FileIO();

protected:
   void _Init();
   void ExpandBuffer( int new_size );
   int IsDiff( byte* buff_a, byte* buff_b, int length );

public:
   int operator!() const;
   operator void*() const;
   operator FILE*() const;
   operator byte*() const;

public:
   void Attach( byte* s_buffer, int length );
   void Close();
   byte* Detach();
   int GetAttribute();
   byte* GetBuffer( int pos = 0 );
   const char* GetFilename();
   int GetLength();
   int GetMode();
   FIO_STORAGE GetStorageType();
   int GoToEndPos();
   int GoToStartPos();
   int IsDiff( FileIO& fio );
   int Open( const char* file_name, const char* mode );
   int Open( const char* file_name, int attribute, int mode );
   int Print( const char* format, ... );
   int Read( byte* d_buffer, int n_items, int item_size );
   int ReadByte( byte& value );
   int ReadChar( char& value );
   int ReadLine( char* d_buffer, int buf_size );
   int ReadString( char* d_buffer, int buf_size, int option = FIO_REMOVE_NEWLINE );
   int Scan( const char* format, ... );
   int Seek( int offset, int origin );
   int SetBuffExpSize( int exp_size );
   void SetLength( int length );
   int Where();
   int Write( byte* s_buffer, int n_items, int item_size );
   int WriteByte( byte value );
   int WriteChar( char value );
   int WriteString( const char* s_buffer );
};

inline int FileIO::operator!() const

{
   if( File == NULL && Buffer == NULL )
      return ( TRUE );
   else
      return ( FALSE );
}

inline FileIO::operator void*() const

{
   if( File == NULL && Buffer == NULL )
      return ( (void*)FALSE );
   else
      return ( (void*)TRUE );
}

inline FileIO::operator FILE*() const

{
   return ( File );
}

inline FileIO::operator byte*() const

{
   return ( Buffer );
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

int AccessFile( const char* file_name, int option = 0 );
void ChangeFileExtension( const char* s_file_name, const char* new_ext, char* d_file_name );
void ExtractDirName( const char* s_path_name, char* d_dir_name );
void ExtractFileName( const char* s_path_name, char* d_file_name, int flag_add_ext = TRUE );
int FindFiles( const char* mask, CArray2D& fns, int fn_length );
int GetFileLength( const char* file_name );
int IdentifyImageFile( const char* file_name );
int LoadFileToBuffer( const char* file_name, BArray1D& d_buffer, int flag_add_null = FALSE );
int SaveBufferToFile( const char* file_name, void* s_buffer, int s_buf_size );
void SplitPathName( const char* s_path, char* d_drive, char* d_dir, char* d_fname, char* d_ext );
}

#endif

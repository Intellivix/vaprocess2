#include "bccl_color.h"
#include "bccl_image.h"
#include "bccl_file.h"
#include "bccl_string.h"

 namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: GImage
//
///////////////////////////////////////////////////////////////////////////////

 GImage::GImage (   )

{
   _Init (   );
}

 GImage::GImage (int width,int height,int margin)

{
   _Init (   );
   Create (width,height,margin);
}

 GImage::GImage (byte *s_buffer,int width,int height,int margin)
 
{
   _Init (   );
   Import (s_buffer,width,height,margin);
}

 GImage::GImage (byte **s_array,int width,int height,int margin)

{
   _Init (   );
   Import (s_array,width,height,margin);
}

 GImage::GImage (const BGRImage& s_cimage)

{
   _Init (   );
   Create  (s_cimage.Width,s_cimage.Height,s_cimage.Margin);
   Convert (s_cimage,*this);
}

 GImage::GImage (const GImage &s_image)

{
   _Init (   );
   Create (s_image.Width,s_image.Height,s_image.Margin);
   CopyArray1D (s_image,0,GetLineLength (   ) * Height,sizeof(byte),Buffer,0);
}

 GImage::GImage (GImage *s_image)

{
   Width                 = s_image->Width;
   Height                = s_image->Height;
   Margin                = s_image->Margin;
   Array                 = s_image->Array;
   Buffer                = s_image->Buffer;
   AlphaChannel          = s_image->AlphaChannel;
   s_image->Width        = 0;
   s_image->Height       = 0;
   s_image->Margin       = 0;
   s_image->Array        = NULL;
   s_image->Buffer       = NULL;
   s_image->AlphaChannel = NULL;
}

 GImage::~GImage (   )

{
   Delete (   );
}

 GImage& GImage::operator = (const GImage &s_image)

{
   if (Width != s_image.Width || Height != s_image.Height || Margin != s_image.Margin) Create (s_image.Width,s_image.Height,s_image.Margin);
   CopyArray1D (s_image,0,GetLineLength (   ) * Height,sizeof(byte),Array[0],0);
   if (s_image.AlphaChannel != NULL) {
      if (AlphaChannel == NULL) CreateAlphaChannel (   );
      *AlphaChannel = *s_image.AlphaChannel;
   }
   return (*this);
}

 GImage& GImage::operator = (const BGRImage &s_image)

{
   if (Width != s_image.Width || Height != s_image.Height || Margin != s_image.Margin) Create (s_image.Width,s_image.Height,s_image.Margin);
   Convert (s_image,*this);
   if (s_image.AlphaChannel != NULL) {
      if (AlphaChannel == NULL) CreateAlphaChannel (   );
      *AlphaChannel = *s_image.AlphaChannel;
   }   
   return (*this);
}

 GImage GImage::operator ~ (   )

{
   int i,j;
   
   GImage d_image(Width,Height);
   for (i = 0; i < Height; i++) {
      uint *_RESTRICT l_Array   = (uint *)Array[i];
      uint *_RESTRICT l_d_image = (uint *)d_image[i];
      for (j = 0; j < Width; j += sizeof(uint)) {
         *l_d_image = ~(*l_Array);
         l_Array++;
         l_d_image++;
      }
   }   
   return (GImage (&d_image));
}

 GImage GImage::operator | (const GImage &s_image)

{
   int i,j;
   
   GImage d_image(Width,Height);
   for (i = 0; i < Height; i++) {
      uint *_RESTRICT l_Array   = (uint *)Array[i];
      uint *_RESTRICT l_s_image = (uint *)s_image[i];
      uint *_RESTRICT l_d_image = (uint *)d_image[i];
      for (j = 0; j < Width; j += sizeof(uint)) {
         *l_d_image = *l_Array | *l_s_image;
         l_Array++;
         l_s_image++;
         l_d_image++;
      }
   }   
   return (GImage (&d_image));
}

 GImage GImage::operator | (byte p)

{
   int i,j;
   
   uint q = (uint)p | ((uint)p << 8) | ((uint)p << 16) | ((uint)p << 24);
   GImage d_image(Width,Height);
   for (i = 0; i < Height; i++) {
      uint *_RESTRICT l_Array   = (uint *)Array[i];
      uint *_RESTRICT l_d_image = (uint *)d_image[i];
      for (j = 0; j < Width; j += sizeof(uint)) {
         *l_d_image = *l_Array | q;
         l_Array++;
         l_d_image++;
      }
   }   
   return (GImage (&d_image));
}

 GImage operator | (byte p,const GImage &s_image)

{
   int i,j;
   
   uint q = (uint)p | ((uint)p << 8) | ((uint)p << 16) | ((uint)p << 24);
   GImage d_image(s_image.Width,s_image.Height);
   for (i = 0; i < s_image.Height; i++) {
      uint *_RESTRICT l_s_image = (uint *)s_image[i];
      uint *_RESTRICT l_d_image = (uint *)d_image[i];
      for (j = 0; j < s_image.Width; j += sizeof(uint)) {
         *l_d_image = q | *l_s_image;
         l_s_image++;
         l_d_image++;
      }
   }   
   return (GImage (&d_image));
}

 GImage GImage::operator & (const GImage &s_image)

{
   int i,j;
   
   GImage d_image(Width,Height);
   for (i = 0; i < Height; i++) {
      uint *_RESTRICT l_Array   = (uint *)Array[i];
      uint *_RESTRICT l_s_image = (uint *)s_image[i];
      uint *_RESTRICT l_d_image = (uint *)d_image[i];
      for (j = 0; j < Width; j += sizeof(uint)) {
         *l_d_image = *l_Array & *l_s_image;
         l_Array++;
         l_s_image++;
         l_d_image++;
      }
   }   
   return (GImage (&d_image));
}

 GImage GImage::operator & (byte p)

{
   int i,j;
   
   uint q = (uint)p | ((uint)p << 8) | ((uint)p << 16) | ((uint)p << 24);
   GImage d_image(Width,Height);
   for (i = 0; i < Height; i++) {
      uint *_RESTRICT l_Array   = (uint *)Array[i];
      uint *_RESTRICT l_d_image = (uint *)d_image[i];
      for (j = 0; j < Width; j += sizeof(uint)) {
         *l_d_image = *l_Array & q;
         l_Array++;
         l_d_image++;
      }
   }   
   return (GImage (&d_image));
}

 GImage operator & (byte p,const GImage &s_image)

{
   int i,j;
   
   uint q = (uint)p | ((uint)p << 8) | ((uint)p << 16) | ((uint)p << 24);
   GImage d_image(s_image.Width,s_image.Height);
   for (i = 0; i < s_image.Height; i++) {
      uint *_RESTRICT l_s_image = (uint *)s_image[i];
      uint *_RESTRICT l_d_image = (uint *)d_image[i];
      for (j = 0; j < s_image.Width; j += sizeof(uint)) {
         *l_d_image = q & *l_s_image;
         l_s_image++;
         l_d_image++;
      }
   }   
   return (GImage (&d_image));
}

 GImage GImage::operator ^ (const GImage &s_image)

{
   int i,j;
   
   GImage d_image(Width,Height);
   for (i = 0; i < Height; i++) {
      uint *_RESTRICT l_Array   = (uint *)Array[i];
      uint *_RESTRICT l_s_image = (uint *)s_image[i];
      uint *_RESTRICT l_d_image = (uint *)d_image[i];
      for (j = 0; j < Width; j += sizeof(uint)) {
         *l_d_image = *l_Array ^ *l_s_image;
         l_Array++;
         l_s_image++;
         l_d_image++;
      }
   }   
   return (GImage (&d_image));
}

 GImage GImage::operator ^ (byte p)

{
   int i,j;
   
   uint q = (uint)p | ((uint)p << 8) | ((uint)p << 16) | ((uint)p << 24);
   GImage d_image(Width,Height);
   for (i = 0; i < Height; i++) {
      uint *_RESTRICT l_Array   = (uint *)Array[i];
      uint *_RESTRICT l_d_image = (uint *)d_image[i];
      for (j = 0; j < Width; j += sizeof(uint)) {
         *l_d_image = *l_Array ^ q;
         l_Array++;
         l_d_image++;
      }
   }   
   return (GImage (&d_image));
}

 GImage operator ^ (byte p,const GImage &s_image)

{
   int i,j;
   
   uint q = (uint)p | ((uint)p << 8) | ((uint)p << 16) | ((uint)p << 24);
   GImage d_image(s_image.Width,s_image.Height);
   for (i = 0; i < s_image.Height; i++) {
      uint *_RESTRICT l_s_image = (uint *)s_image[i];
      uint *_RESTRICT l_d_image = (uint *)d_image[i];
      for (j = 0; j < s_image.Width; j += sizeof(uint)) {
         *l_d_image = q ^ *l_s_image;
         l_s_image++;
         l_d_image++;
      }
   }   
   return (GImage (&d_image));
}

 GImage& GImage::operator |= (const GImage &s_image)

{
   int i,j;
   
   for (i = 0; i < Height; i++) {
      uint *_RESTRICT l_Array   = (uint *)Array[i];
      uint *_RESTRICT l_s_image = (uint *)s_image[i];
      for (j = 0; j < Width; j += sizeof(uint)) {
         *l_Array |= *l_s_image;
         l_Array++;
         l_s_image++;
      }
   }   
   return (*this);
}
 
 GImage& GImage::operator |= (byte p)

{
   int i,j;
   
   uint q = (uint)p | ((uint)p << 8) | ((uint)p << 16) | ((uint)p << 24);
   for (i = 0; i < Height; i++) {
      uint *_RESTRICT l_Array = (uint *)Array[i];
      for (j = 0; j < Width; j += sizeof(uint)) {
         *l_Array |= q;
         l_Array++;
      }
   }   
   return (*this);
}

 GImage& GImage::operator &= (const GImage &s_image)

{
   int i,j;

   for (i = 0; i < Height; i++) {
      uint *_RESTRICT l_Array   = (uint *)Array[i];
      uint *_RESTRICT l_s_image = (uint *)s_image[i];
      for (j = 0; j < Width; j += sizeof(uint)) {
         *l_Array &= *l_s_image;
         l_Array++;
         l_s_image++;
      }
   }   
   return (*this);
}

 GImage& GImage::operator &= (byte p)

{
   int i,j;

   uint q = (uint)p | ((uint)p << 8) | ((uint)p << 16) | ((uint)p << 24);
   for (i = 0; i < Height; i++) {
      uint *_RESTRICT l_Array = (uint *)Array[i];
      for (j = 0; j < Width; j += sizeof(uint)) {
         *l_Array &= q;
         l_Array++;
      }
   }   
   return (*this);
}

 GImage& GImage::operator ^= (const GImage &s_image)

{
   int i,j;
   
   for (i = 0; i < Height; i++) {
      uint *_RESTRICT l_Array   = (uint *)Array[i];
      uint *_RESTRICT l_s_image = (uint *)s_image[i];
      for (j = 0; j < Width; j += sizeof(uint)) {
         *l_Array ^= *l_s_image;
         l_Array++;
         l_s_image++;
      }
   }   
   return (*this);
}
 
 GImage& GImage::operator ^= (byte p)

{
   int i,j;
   
   uint q = (uint)p | ((uint)p << 8) | ((uint)p << 16) | ((uint)p << 24);
   for (i = 0; i < Height; i++) {
      uint *_RESTRICT l_Array = (uint *)Array[i];
      for (j = 0; j < Width; j += sizeof(uint)) {
         *l_Array ^= q;
         l_Array++;
      }
   }   
   return (*this);
}

 GImage::operator BGRImage (   ) const

{
   BGRImage d_image(Width,Height);
   Convert (*this,d_image);
   return (BGRImage(&d_image));
}

 void GImage::_Init (   )
 
{
   BArray2D::_Init (   );
   Margin       = 0;
   AlphaChannel = NULL;
}
 
 void GImage::Clear (   )

{
   if (Buffer) ClearArray1D (Buffer,GetLineLength (   ) * Height,sizeof(byte));
}

 int GImage::Create (int width,int height)

{
   int x,y;

   if (Width == width && Height == height) return (DONE);
   if (Array  != NULL) DeleteArray1D ((byte*)Array);
   if (Buffer != NULL) DeleteArray1D ((byte*)Buffer);
   DeleteAlphaChannel (   );
   Array  = NULL;
   Buffer = NULL;
   Width  = width;
   Height = height;
   if (Width && Height) {
      Array = (byte**)CreateArray1D (Height,sizeof(byte*));
      if (Array == NULL)  return (1);
      int line_len = GetLineLength (   );
      Buffer = CreateArray1D (line_len * Height,sizeof(byte));
      if (Buffer == NULL) return (2);
      for (x = y = 0; y < Height; y++,x += line_len) Array[y] = Buffer + x;
   }
   return (DONE);
}

 int GImage::Create (int width,int height,int margin)

{
   Margin = margin;
   Width  = Height = 0;
   return (Create (width,height));
}

 int GImage::CreateAlphaChannel (   )
 
{
   DeleteAlphaChannel (   );
   AlphaChannel = new GImage(Width,Height,Margin);
   if (AlphaChannel == NULL) return (1);
   return (DONE);
}

 void GImage::Delete (   )

{
   if (Array  != NULL) DeleteArray1D ((byte*)Array);
   if (Buffer != NULL) DeleteArray1D ((byte*)Buffer);
   DeleteAlphaChannel (   );
   _Init (   );
}

 void GImage::DeleteAlphaChannel (   )
 
{
   if (AlphaChannel == NULL) return;
   delete AlphaChannel;
   AlphaChannel = NULL;
}

 GImage GImage::Extract (int sx,int sy,int width,int height)

{
   GImage d_image(width,height);
   d_image.Copy (*this,sx,sy,width,height,0,0);
   return (GImage(&d_image));
}

 GImage GImage::Extract (IBox2D &s_box)

{
   GImage d_image(s_box.Width,s_box.Height);
   d_image.Copy (*this,s_box.X,s_box.Y,s_box.Width,s_box.Height,0,0);
   return (GImage(&d_image));
}

 int GImage::GetLineLength (   ) const

{
   return ((Width + Margin + 3) / 4 * 4);
}

 int GImage::Import (byte* s_buffer,int width,int height)
 
{
   int x,y;

   if (s_buffer == NULL) {
      if (Array != NULL) {
         DeleteArray1D ((byte*)Array);
         Array = NULL;
      }      
   }   
   DeleteAlphaChannel (   );
   Buffer = s_buffer;
   Width  = width;
   Height = height;
   if (Buffer != NULL && Width && Height) {
      Array = (byte**)CreateArray1D (Height,sizeof(byte*));
      if (Array == NULL) return (1);
      int line_len = GetLineLength (   );
      for (x = y = 0; y < Height; y++,x += line_len) Array[y] = Buffer + x;
   }
   return (DONE);
}

 int GImage::Import (byte **s_array,int width,int height)
 
{
   DeleteAlphaChannel (   );
   return (BArray2D::Import (s_array,width,height));
}

 int GImage::Import (byte *s_buffer,int width,int height,int margin)
 
{
   Margin = margin;
   return (Import (s_buffer,width,height));
}

 int GImage::Import (byte **s_array,int width,int height,int margin)
 
{
   Margin = margin;
   return (Import (s_array,width,height));
}

 int GImage::ReadFile (const char *file_name)

{
   switch (IdentifyImageFile (file_name)) {
      case IFT_BMP:
         return (ReadBMPFile (file_name));
      case IFT_JPG :
         return (ReadJPGFile (file_name));
      case IFT_PCX:
         return (ReadPCXFile (file_name));
      case IFT_PGM:
         return (ReadPGMFile (file_name));
      case IFT_PNG:
         return (ReadPNGFile (file_name));
      case IFT_RAS:
         return (ReadRASFile (file_name));
      default:
         return (-1);
   }
}

 void GImage::SetMask (byte m_color)
 
{
   int x,y;
	
	if (AlphaChannel == NULL) CreateAlphaChannel (   );
   GImage &m_image = *AlphaChannel;
	for (y = 0; y < Height; y++) {
	   byte *l_Array   = Array[y];
	   byte *l_m_image = m_image[y];
	   for (x = 0; x < Width; x++) {
		   if (l_Array[x] == m_color) l_m_image[x] = PG_BLACK;
			else l_m_image[x] = PG_WHITE;
		}
	}
}

 int GImage::WriteFile (const char *file_name)

{
   int r_value;
   
   StringA f_name = file_name;
   f_name.ToLower (   );
   if      (strstr ((char*)f_name,".bmp")) r_value = WriteBMPFile (file_name);
   else if (strstr ((char*)f_name,".jpg")) r_value = WriteJPGFile (file_name);
   else if (strstr ((char*)f_name,".pcx")) r_value = WritePCXFile (file_name);
   else if (strstr ((char*)f_name,".pgm")) r_value = WritePGMFile (file_name);
   else if (strstr ((char*)f_name,".png")) r_value = WritePNGFile (file_name);
   else if (strstr ((char*)f_name,".ras")) r_value = WriteRASFile (file_name);
   else if (strstr ((char*)f_name,".raw")) r_value = WriteRAWFile (file_name);
   else r_value = -1;
   return (r_value);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: BGRPixel
//
///////////////////////////////////////////////////////////////////////////////

 BGRPixel::BGRPixel (   )

{
   B = G = R = 0;
}

 BGRPixel::BGRPixel (byte b,byte g,byte r)

{
   B = b, G = g, R = r;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: BGRImage
//
///////////////////////////////////////////////////////////////////////////////

 BGRImage::BGRImage (   )

{
   _Init (   );
}

 BGRImage::BGRImage (int width,int height,int margin)

{
   _Init (   );
   Create (width,height,margin);
}

 BGRImage::BGRImage (BGRPixel *s_buffer,int width,int height,int margin)
 
{
   _Init (   );
   Import (s_buffer,width,height,margin);
}

 BGRImage::BGRImage (BGRPixel **s_array,int width,int height,int margin)

{
   _Init (   );
   Import (s_array,width,height,margin);
}

 BGRImage::BGRImage (const BGRImage &s_image)

{
   _Init (   );
   Create (s_image.Width,s_image.Height,s_image.Margin);
   CopyArray1D ((byte *)(BGRPixel *)s_image,0,GetLineLength (   ) * Height,sizeof(byte),(byte *)Array[0],0);
   if (s_image.AlphaChannel != NULL) {
      CreateAlphaChannel (   );
      *AlphaChannel = *s_image.AlphaChannel;
   }
}

 BGRImage::BGRImage (const GImage &s_image)

{
   _Init (   );
   Create  (s_image.Width,s_image.Height,s_image.Margin);
   Convert (s_image,*this);
   if (s_image.AlphaChannel != NULL) {
      CreateAlphaChannel (   );
      *AlphaChannel = *s_image.AlphaChannel;
   }
}

 BGRImage::BGRImage (BGRImage *s_image)

{
   Width                 = s_image->Width;
   Height                = s_image->Height;
   Margin                = s_image->Margin;
   Array                 = s_image->Array;
   Buffer                = s_image->Buffer;
   AlphaChannel          = s_image->AlphaChannel;
   s_image->Width        = 0;
   s_image->Height       = 0;
   s_image->Margin       = 0;
   s_image->Array        = NULL;
   s_image->Buffer       = NULL;
   s_image->AlphaChannel = NULL;
}

 BGRImage::~BGRImage (   )

{
   Delete (   );
}

 BGRImage& BGRImage::operator = (const BGRImage &s_image)

{
   if (Width != s_image.Width || Height != s_image.Height || Margin != s_image.Margin) Create (s_image.Width,s_image.Height,s_image.Margin);
   CopyArray1D ((byte *)(BGRPixel *)s_image,0,GetLineLength (   ) * Height,sizeof(byte),(byte *)Array[0],0);
   if (s_image.AlphaChannel != NULL) {
      if (AlphaChannel == NULL) CreateAlphaChannel (   );
      *AlphaChannel = *s_image.AlphaChannel;
   }
   return (*this);
}

 BGRImage& BGRImage::operator = (const GImage &s_image)

{
   if (Width != s_image.Width || Height != s_image.Height || Margin != s_image.Margin) Create (s_image.Width,s_image.Height,s_image.Margin);
   Convert (s_image,*this);
   if (s_image.AlphaChannel != NULL) {
      if (AlphaChannel == NULL) CreateAlphaChannel (   );
      *AlphaChannel = *s_image.AlphaChannel;
   }
   return  (*this);
}

 BGRImage BGRImage::operator ~ (   )

{
   int x,y;
   
   BGRImage d_image(Width,Height);
   for (y = 0; y < Height; y++) {
      BGRPixel* l_Array   = Array[y];
      BGRPixel* l_d_image = d_image[y];
      for (x = 0; x < Width; x++) {
         l_d_image->R = ~l_Array->R;
         l_d_image->G = ~l_Array->G;
         l_d_image->B = ~l_Array->B;
         l_Array++;
         l_d_image++;
      }
   }
   return (BGRImage (&d_image));
}

 BGRImage BGRImage::operator | (const BGRImage &s_image)

{
   int x,y;

   BGRImage d_image(Width,Height);
   for (y = 0; y < Height; y++) {
      BGRPixel* l_Array   = Array[y];
      BGRPixel* l_s_image = s_image[y];
      BGRPixel* l_d_image = d_image[y];
      for (x = 0; x < Width; x++) {
         l_d_image->R = l_Array->R | l_s_image->R;
         l_d_image->G = l_Array->G | l_s_image->G;
         l_d_image->B = l_Array->B | l_s_image->B;
         l_Array++;
         l_s_image++;
         l_d_image++;
      }
   }
   return (BGRImage (&d_image));
}

 BGRImage BGRImage::operator | (const GImage &s_image)

{
   int x,y;

   BGRImage d_image(Width,Height);
   for (y = 0; y < Height; y++) {
      BGRPixel* l_Array   = Array[y];
      byte*     l_s_image = s_image[y];
      BGRPixel* l_d_image = d_image[y];
      for (x = 0; x < Width; x++) {
         l_d_image->R = l_Array->R | *l_s_image;
         l_d_image->G = l_Array->G | *l_s_image;
         l_d_image->B = l_Array->B | *l_s_image;
         l_Array++;
         l_s_image++;
         l_d_image++;
      }
   }
   return (BGRImage (&d_image));
}

 BGRImage operator | (const GImage &s_image1,const BGRImage &s_image2)

{
   int x,y;

   BGRImage d_image(s_image1.Width,s_image1.Height);
   for (y = 0; y < d_image.Height; y++) {
      byte*     l_s_image1 = s_image1[y];
      BGRPixel* l_s_image2 = s_image2[y];
      BGRPixel* l_d_image  = d_image[y];
      for (x = 0; x < d_image.Width; x++) {
         l_d_image->R = *l_s_image1 | l_s_image2->R;
         l_d_image->G = *l_s_image1 | l_s_image2->G;
         l_d_image->B = *l_s_image1 | l_s_image2->B;
         l_s_image1++;
         l_s_image2++;
         l_d_image++;
      }
   }
   return (BGRImage (&d_image));
}

 BGRImage BGRImage::operator | (const BGRPixel &p)

{
   int x,y;

   BGRImage d_image(Width,Height);
   for (y = 0; y < Height; y++) {
      BGRPixel* l_Array   = Array[y];
      BGRPixel* l_d_image = d_image[y];
      for (x = 0; x < Width; x++) {
         l_d_image->R = l_Array->R | p.R;
         l_d_image->G = l_Array->G | p.G;
         l_d_image->B = l_Array->B | p.B;
         l_Array++;
         l_d_image++;
      }
   }
   return (BGRImage (&d_image));
}

 BGRImage operator | (const BGRPixel &p,const BGRImage &s_image)

{
   int x,y;
 
   BGRImage d_image(s_image.Width,s_image.Height);
   for (y = 0; y < s_image.Height; y++) {
      BGRPixel* l_s_image = s_image[y];
      BGRPixel* l_d_image = d_image[y];
      for (x = 0; x < s_image.Width; x++) {
         l_d_image->R = p.R | l_s_image->R;
         l_d_image->G = p.G | l_s_image->G;
         l_d_image->B = p.B | l_s_image->B;
         l_s_image++;
         l_d_image++;
      }
   }
   return (BGRImage (&d_image));
}

 BGRImage BGRImage::operator & (const BGRImage &s_image)

{
   int x,y;

   BGRImage d_image(Width,Height);
   for (y = 0; y < Height; y++) {
      BGRPixel* l_Array   = Array[y];
      BGRPixel* l_s_image = s_image[y];
      BGRPixel* l_d_image = d_image[y];
      for (x = 0; x < Width; x++) {
         l_d_image->R = l_Array->R & l_s_image->R;
         l_d_image->G = l_Array->G & l_s_image->G;
         l_d_image->B = l_Array->B & l_s_image->B;
         l_Array++;
         l_s_image++;
         l_d_image++;
      }
   }
   return (BGRImage (&d_image));
}

 BGRImage BGRImage::operator & (const GImage &s_image)

{
   int x,y;

   BGRImage d_image(Width,Height);
   for (y = 0; y < Height; y++) {
      BGRPixel* l_Array   = Array[y];
      byte*     l_s_image = s_image[y];
      BGRPixel* l_d_image = d_image[y];
      for (x = 0; x < Width; x++) {
         l_d_image->R = l_Array->R & *l_s_image;
         l_d_image->G = l_Array->G & *l_s_image;
         l_d_image->B = l_Array->B & *l_s_image;
         l_Array++;
         l_s_image++;
         l_d_image++;
      }
   }
   return (BGRImage (&d_image));
}

 BGRImage operator & (const GImage &s_image1,const BGRImage &s_image2)

{
   int x,y;

   BGRImage d_image(s_image1.Width,s_image1.Height);
   for (y = 0; y < d_image.Height; y++) {
      byte*     l_s_image1 = s_image1[y];
      BGRPixel* l_s_image2 = s_image2[y];
      BGRPixel* l_d_image  = d_image[y];
      for (x = 0; x < d_image.Width; x++) {
         l_d_image->R = *l_s_image1 & l_s_image2->R;
         l_d_image->G = *l_s_image1 & l_s_image2->G;
         l_d_image->B = *l_s_image1 & l_s_image2->B;
         l_s_image1++;
         l_s_image2++;
         l_d_image++;
      }
   }
   return (BGRImage (&d_image));
}

 BGRImage BGRImage::operator & (const BGRPixel &p)

{
   int x,y;

   BGRImage d_image(Width,Height);
   for (y = 0; y < Height; y++) {
      BGRPixel* l_Array   = Array[y];
      BGRPixel* l_d_image = d_image[y];
      for (x = 0; x < Width; x++) {
         l_d_image->R = l_Array->R & p.R;
         l_d_image->G = l_Array->G & p.G;
         l_d_image->B = l_Array->B & p.B;
         l_Array++;
         l_d_image++;
      }
   }
   return (BGRImage (&d_image));
}

 BGRImage operator & (const BGRPixel &p,const BGRImage &s_image)

{
   int x,y;
 
   BGRImage d_image(s_image.Width,s_image.Height);
   for (y = 0; y < s_image.Height; y++) {
      BGRPixel* l_s_image = s_image[y];
      BGRPixel* l_d_image = d_image[y];
      for (x = 0; x < s_image.Width; x++) {
         l_d_image->R = p.R & l_s_image->R;
         l_d_image->G = p.G & l_s_image->G;
         l_d_image->B = p.B & l_s_image->B;
         l_s_image++;
         l_d_image++;
      }
   }
   return (BGRImage (&d_image));
}

 BGRImage BGRImage::operator ^ (const BGRImage &s_image)

{
   int x,y;

   BGRImage d_image(Width,Height);
   for (y = 0; y < Height; y++) {
      BGRPixel* l_Array   = Array[y];
      BGRPixel* l_s_image = s_image[y];
      BGRPixel* l_d_image = d_image[y];
      for (x = 0; x < Width; x++) {
         l_d_image->R = l_Array->R ^ l_s_image->R;
         l_d_image->G = l_Array->G ^ l_s_image->G;
         l_d_image->B = l_Array->B ^ l_s_image->B;
         l_Array++;
         l_s_image++;
         l_d_image++;
      }
   }
   return (BGRImage (&d_image));
}

 BGRImage BGRImage::operator ^ (const GImage &s_image)

{
   int x,y;

   BGRImage d_image(Width,Height);
   for (y = 0; y < Height; y++) {
      BGRPixel* l_Array   = Array[y];
      byte*     l_s_image = s_image[y];
      BGRPixel* l_d_image = d_image[y];
      for (x = 0; x < Width; x++) {
         l_d_image->R = l_Array->R ^ *l_s_image;
         l_d_image->G = l_Array->G ^ *l_s_image;
         l_d_image->B = l_Array->B ^ *l_s_image;
         l_Array++;
         l_s_image++;
         l_d_image++;
      }
   }
   return (BGRImage (&d_image));
}

 BGRImage operator ^ (const GImage &s_image1,const BGRImage &s_image2)

{
   int x,y;

   BGRImage d_image(s_image1.Width,s_image1.Height);
   for (y = 0; y < d_image.Height; y++) {
      byte*     l_s_image1 = s_image1[y];
      BGRPixel* l_s_image2 = s_image2[y];
      BGRPixel* l_d_image  = d_image[y];
      for (x = 0; x < d_image.Width; x++) {
         l_d_image->R = *l_s_image1 ^ l_s_image2->R;
         l_d_image->G = *l_s_image1 ^ l_s_image2->G;
         l_d_image->B = *l_s_image1 ^ l_s_image2->B;
         l_s_image1++;
         l_s_image2++;
         l_d_image++;
      }
   }
   return (BGRImage (&d_image));
}

 BGRImage BGRImage::operator ^ (const BGRPixel &p)

{
   int x,y;

   BGRImage d_image(Width,Height);
   for (y = 0; y < Height; y++) {
      BGRPixel* l_Array   = Array[y];
      BGRPixel* l_d_image = d_image[y];
      for (x = 0; x < Width; x++) {
         l_d_image->R = l_Array->R ^ p.R;
         l_d_image->G = l_Array->G ^ p.G;
         l_d_image->B = l_Array->B ^ p.B;
         l_Array++;
         l_d_image++;
      }
   }
   return (BGRImage (&d_image));
}

 BGRImage operator ^ (const BGRPixel &p,const BGRImage &s_image)

{
   int x,y;
 
   BGRImage d_image(s_image.Width,s_image.Height);
   for (y = 0; y < s_image.Height; y++) {
      BGRPixel* l_s_image = s_image[y];
      BGRPixel* l_d_image = d_image[y];
      for (x = 0; x < s_image.Width; x++) {
         l_d_image->R = p.R ^ l_s_image->R;
         l_d_image->G = p.G ^ l_s_image->G;
         l_d_image->B = p.B ^ l_s_image->B;
         l_s_image++;
         l_d_image++;
      }
   }
   return (BGRImage (&d_image));
}

 BGRImage& BGRImage::operator |= (const BGRImage &s_image)

{
   int x,y;
   
   for (y = 0; y < Height; y++) {
      BGRPixel* l_s_image = s_image[y];
      BGRPixel* l_Array   = Array[y];
      for (x = 0; x < Width; x++) {
         l_Array->R |= l_s_image->R;
         l_Array->G |= l_s_image->G;
         l_Array->B |= l_s_image->B;
         l_s_image++;
         l_Array++;
      }
   }
   return (*this);
}

 BGRImage& BGRImage::operator |= (const GImage &s_image)

{
   int x,y;
   
   for (y = 0; y < Height; y++) {
      byte*     l_s_image = s_image[y];
      BGRPixel* l_Array   = Array[y];
      for (x = 0; x < Width; x++) {
         l_Array->R |= *l_s_image;
         l_Array->G |= *l_s_image;
         l_Array->B |= *l_s_image;
         l_s_image++;
         l_Array++;
      }
   }
   return (*this);
}

 BGRImage& BGRImage::operator |= (const BGRPixel &p)

{
   int x,y;
  
   for (y = 0; y < Height; y++) {
      BGRPixel *l_Array = Array[y];
      for (x = 0; x < Width; x++) {
         l_Array->R |= p.R;
         l_Array->G |= p.G;
         l_Array->B |= p.B;
         l_Array++;
      }
   }
   return (*this);
}

 BGRImage& BGRImage::operator &= (const BGRImage &s_image)

{
   int x,y;
   
   for (y = 0; y < Height; y++) {
      BGRPixel *l_s_image = s_image[y];
      BGRPixel *l_Array   = Array[y];
      for (x = 0; x < Width; x++) {
         l_Array->R &= l_s_image->R;
         l_Array->G &= l_s_image->G;
         l_Array->B &= l_s_image->B;
         l_s_image++;
         l_Array++;
      }
   }
   return (*this);
}

 BGRImage& BGRImage::operator &= (const GImage &s_image)

{
   int x,y;
   
   for (y = 0; y < Height; y++) {
      byte   *l_s_image = s_image[y];
      BGRPixel *l_Array   = Array[y];
      for (x = 0; x < Width; x++) {
         l_Array->R &= *l_s_image;
         l_Array->G &= *l_s_image;
         l_Array->B &= *l_s_image;
         l_s_image++;
         l_Array++;
      }
   }
   return (*this);
}

 BGRImage& BGRImage::operator &= (const BGRPixel &p)

{
   int x,y;
   
   for (y = 0; y < Height; y++) {
      BGRPixel *l_Array = Array[y];
      for (x = 0; x < Width; x++) {
         l_Array->R &= p.R;
         l_Array->G &= p.G;
         l_Array->B &= p.B;
         l_Array++;
      }
   }
   return (*this);
}

 BGRImage& BGRImage::operator ^= (const BGRImage &s_image)

{
   int x,y;
   
   for (y = 0; y < Height; y++) {
      BGRPixel *l_s_image = s_image[y];
      BGRPixel *l_Array   = Array[y];
      for (x = 0; x < Width; x++) {
         l_Array->R ^= l_s_image->R;
         l_Array->G ^= l_s_image->G;
         l_Array->B ^= l_s_image->B;
         l_s_image++;
         l_Array++;
      }
   }
   return (*this);
}

 BGRImage& BGRImage::operator ^= (const GImage &s_image)

{
   int x,y;
   
   for (y = 0; y < Height; y++) {
      byte   *l_s_image = s_image[y];
      BGRPixel *l_Array   = Array[y];
      for (x = 0; x < Width; x++) {
         l_Array->R ^= *l_s_image;
         l_Array->G ^= *l_s_image;
         l_Array->B ^= *l_s_image;
         l_s_image++;
         l_Array++;
      }
   }
   return (*this);
}

 BGRImage& BGRImage::operator ^= (const BGRPixel &p)

{
   int x,y;
   
   for (y = 0; y < Height; y++) {
      BGRPixel *l_Array = Array[y];
      for (x = 0; x < Width; x++) {
         l_Array->R ^= p.R;
         l_Array->G ^= p.G;
         l_Array->B ^= p.B;
         l_Array++;
      }
   }
   return (*this);
}

 BGRImage::operator GImage (   ) const
 
{
   GImage d_image(Width,Height);
   Convert (*this,d_image);
   return (GImage(&d_image));
}

 void BGRImage::_Init (   )
 
{
   Array2D<BGRPixel>::_Init (   );
   Margin       = 0;
   AlphaChannel = NULL;
}

 void BGRImage::Clear (   )

{
   ClearArray1D ((byte *)Array[0],GetLineLength (   ) * Height,sizeof(byte));
}

 int BGRImage::Create (int width,int height)

{
   int x,y;
   
   if (Width == width && Height == height) return (DONE);
   if (Array  != NULL) DeleteArray1D ((byte*)Array);
   if (Buffer != NULL) DeleteArray1D ((byte*)Buffer);
   DeleteAlphaChannel (   );
   Array  = NULL;
   Buffer = NULL;
   Width  = width;
   Height = height;
   if (Width && Height) {
      Array = (BGRPixel**)CreateArray1D (Height,sizeof(BGRPixel*));
      if (Array == NULL)  return (1);
      int line_len = GetLineLength (   );
      Buffer = (BGRPixel*)CreateArray1D (line_len * Height,sizeof(byte));
      if (Buffer == NULL) return (2);
      for (x = y = 0; y < Height; y++,x += line_len) Array[y] = (BGRPixel*)((byte*)Buffer + x);
   }
   return (DONE);
}

 int BGRImage::Create (int width,int height,int margin)

{
   Margin = margin;
   Width  = Height = 0;
   return (Create (width,height));
}

 int BGRImage::CreateAlphaChannel (   )

{
   DeleteAlphaChannel (   );
   AlphaChannel = new GImage(Width,Height,Margin);
   if (AlphaChannel == NULL) return (1);
   return (DONE);
}

 void BGRImage::Delete (   )

{
   if (Array  != NULL) DeleteArray1D ((byte*)Array);
   if (Buffer != NULL) DeleteArray1D ((byte*)Buffer);
   DeleteAlphaChannel (   );
   _Init (   );
}

 void BGRImage::DeleteAlphaChannel (   )

{
   if (AlphaChannel == NULL) return;
   delete AlphaChannel;
   AlphaChannel = NULL;
}

 BGRImage BGRImage::Extract (int sx,int sy,int width,int height)

{
   BGRImage d_image(width,height);
   d_image.Copy (*this,sx,sy,width,height,0,0);
   return (BGRImage(&d_image));
}

 BGRImage BGRImage::Extract (IBox2D &s_box)

{
   BGRImage d_image(s_box.Width,s_box.Height);
   d_image.Copy (*this,s_box.X,s_box.Y,s_box.Width,s_box.Height,0,0);
   return (BGRImage(&d_image));
}

 int BGRImage::GetLineLength (   ) const

{
   return ((Width * 3 + Margin + 3) / 4 * 4);
}

 void BGRImage::GetPlane (int n,GImage &d_image)

{
   int x,y;

   for (y = 0; y < Height; y++) {
      BGRPixel* l_Array   = Array[y];
      byte*     l_d_image = d_image[y];
      for (x = 0; x < Width; x++)
         l_d_image[x] = l_Array[x][n];
   }
}

 void BGRImage::GetPlanes (GImage& d_image_r,GImage& d_image_g,GImage& d_image_b)

{
   int x,y;

   for (y = 0; y < Height; y++) {
      BGRPixel* sp          = Array[y];
      byte*     l_d_image_r = d_image_r[y];
      byte*     l_d_image_g = d_image_g[y];
      byte*     l_d_image_b = d_image_b[y];
      for (x = 0; x < Width; x++) {
         l_d_image_r[x] = sp->R;
         l_d_image_g[x] = sp->G;
         l_d_image_b[x] = sp->B;
      }
   }
}

 int BGRImage::Import (BGRPixel *s_buffer,int width,int height)
 
{
   int x,y;

   if (s_buffer == NULL) {
      if (Array != NULL) {
         DeleteArray1D ((byte*)Array);
         Array = NULL;
      }
   }
   Buffer = s_buffer;
   Width  = width;
   Height = height;
   DeleteAlphaChannel (   );
   if (Buffer != NULL && Width && Height) {
      Array = (BGRPixel**)CreateArray1D (Height,sizeof(BGRPixel*));
      if (Array == NULL) return (1);
      int line_len = GetLineLength (   );
      for (x = y = 0; y < Height; y++,x += line_len) Array[y] = (BGRPixel*)((byte*)Buffer + x);
   }
   return (DONE);
}

 int BGRImage::Import (BGRPixel **s_array,int width,int height)
 
{
   DeleteAlphaChannel (   );
   return (Array2D<BGRPixel>::Import (s_array,width,height));
}

 int BGRImage::Import (BGRPixel *s_buffer,int width,int height,int margin)
 
{
   Margin = margin;
   return (Import (s_buffer,width,height));
}

 int BGRImage::Import (BGRPixel **s_array,int width,int height,int margin)
 
{
   Margin = margin;
   return (Import (s_array,width,height));
}

 int BGRImage::ReadFile (const char *file_name)

{
   switch (IdentifyImageFile (file_name)) {
      case IFT_BMP:
         return (ReadBMPFile (file_name));
      case IFT_JPG:
         return (ReadJPGFile (file_name));
      case IFT_PCX:
         return (ReadPCXFile (file_name));
      case IFT_PGM:
         return (ReadPGMFile (file_name));
      case IFT_PNG:
         return (ReadPNGFile (file_name));
      case IFT_RAS:
         return (ReadRASFile (file_name));
      default:
         return (-1);
   }
}
 
 void BGRImage::SetMask (BGRPixel &m_color)

{
   int x,y;
   
	if (AlphaChannel == NULL) CreateAlphaChannel (   );
   GImage &m_image = *AlphaChannel;
	for (y = 0; y < Height; y++) {
	   BGRPixel *l_Array = Array[y];
	   byte *l_m_image   = m_image[y];
	   for (x = 0; x < Width; x++) {
	      BGRPixel *p = l_Array + x;
		   if (p->R == m_color.R && p->G == m_color.G && p->B == m_color.B) l_m_image[x] = PG_BLACK;
			else l_m_image[x] = PG_WHITE;
		}
	}
}

 void BGRImage::SetPlane (GImage &s_image,int n)

{
   int x,y;

   for (y = 0; y < Height; y++) {
      BGRPixel* l_Array   = Array[y];
      byte*     l_s_image = s_image[y];
      for (x = 0; x < Width; x++)
         l_Array[x][n] = l_s_image[x];
   }
}

 int BGRImage::WriteFile (const char *file_name)

{
   int r_value;
   
   StringA f_name = file_name;
   f_name.ToLower (   );
   if      (strstr ((char*)f_name,".bmp")) r_value = WriteBMPFile (file_name);
   else if (strstr ((char*)f_name,".jpg")) r_value = WriteJPGFile (file_name);
   else if (strstr ((char*)f_name,".pcx")) r_value = WritePCXFile (file_name);
   else if (strstr ((char*)f_name,".pgm")) r_value = WritePGMFile (file_name);
   else if (strstr ((char*)f_name,".png")) r_value = WritePNGFile (file_name);
   else if (strstr ((char*)f_name,".ras")) r_value = WriteRASFile (file_name);
   else if (strstr ((char*)f_name,".raw")) r_value = WriteRAWFile (file_name);
   else r_value = -1;
   return (r_value);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: YUVImage
//
///////////////////////////////////////////////////////////////////////////////

 void YUVImage::GetPlane (int n,GImage& d_image)

{
   int x,y;

   for (y = 0; y < Height; y++) {
      YUVPixel* l_Array   = Array[y];
      byte*     l_d_image = d_image[y];
      for (x = 0; x < Width; x++)
         l_d_image[x] = l_Array[x][n];
   }
}

 void YUVImage::SetPlane (GImage& s_image,int n)

{
   int x,y;

   for (y = 0; y < Height; y++) {
      byte*     l_s_image = s_image[y];
      YUVPixel* l_Array   = Array[y];
      for (x = 0; x < Width; x++)
         l_Array[x][n] = l_s_image[x];
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: YUVImage2
//
///////////////////////////////////////////////////////////////////////////////

 YUVImage2::YUVImage2 (   )

{
   Width  = 0;
   Height = 0;
   Margin = 0;
}

 YUVImage2::YUVImage2 (int width,int height,int margin)

{
   Create (width,height,margin);
}

 YUVImage2::YUVImage2 (const BGRImage& s_cimage)

{
   Create (s_cimage.Width,s_cimage.Height);
   ConvertBGRToYUV (s_cimage,*this);
}

 YUVImage2::~YUVImage2 (   )

{
   Delete (   );
}

 YUVImage2& YUVImage2::operator = (const BGRImage& s_cimage)

{
   if (Width != s_cimage.Width || Height != s_cimage.Height) Create (s_cimage.Width,s_cimage.Height);
   ConvertBGRToYUV (s_cimage,*this);
   return (*this);
}

 void YUVImage2::Clear (   )

{
   int i;

   for (i = 0; i < Length; i++) 
      Array[i].Clear (   );
}

 int YUVImage2::Create (int width,int height,int margin)

{
   int i;
   
   Width  = width;
   Height = height;
   Margin = margin;
   Array1D<GImage>::Create (3);
   for (i = 0; i < Length; i++)
      Array[i].Create (width,height,margin);
   return (DONE);
}

 void YUVImage2::Delete (   )

{
   Array1D<GImage>::Delete (   );
   Width = Height = Margin = 0;
}

 int YUVImage2::GetLineLength (   ) const

{
   return (Array[0].GetLineLength (   ));
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 void And (GImage &s_image1,GImage &s_image2,GImage &d_image)

{
   int x,y;
   
   for (y = 0; y < d_image.Height; y++) {
      uint* sp1 = (uint*)s_image1[y];
      uint* sp2 = (uint*)s_image2[y];
      uint* dp  = (uint*)d_image[y];
      for (x = 0; x < d_image.Width; x += sizeof(uint)) {
         *dp = *sp1 & *sp2;
         sp1++, sp2++, dp++;
      }
   }   
}

 void Convert (const BGRImage &s_image,GImage &d_image)

{
   int x,y;

   for (y = 0; y < s_image.Height; y++)  {
      BGRPixel* l_s_image = s_image[y];
      byte*     l_d_image = d_image[y];
      for (x = 0; x < s_image.Width; x++) {
         BGRPixel &p  = l_s_image[x];
         l_d_image[x] = (byte)(((ushort)p.R + p.G + p.B) / 3);
      }
   }
}

 void Convert (const GImage &s_image,BGRImage &d_image)

{
   int x,y;

   for (y = 0; y < s_image.Height; y++) {
      byte*     l_s_image = s_image[y];
      BGRPixel* l_d_image = d_image[y];
      for (x = 0; x < s_image.Width; x++) {
         BGRPixel& p = l_d_image[x];
         p.R = p.G = p.B = l_s_image[x];
      }
   }
}

 void CopyPixels (GImage &s_image,byte s_pv,GImage &d_image,byte d_pv)

{
   int x,y;

   for (y = 0; y < s_image.Height; y++) {
      byte* l_s_image = s_image[y];
      byte* l_d_image = d_image[y];
      for (x = 0; x < s_image.Width; x++) {
         if (l_s_image[x] == s_pv) l_d_image[x] = d_pv;
      }
   }
}

 void ExtractAndResize_YUY2 (byte* si_buffer,ISize2D& si_size,IBox2D& e_rgn,byte* di_buffer,ISize2D& di_size)
// [??] X ?? Width ?? ???? ??.
{
   int   x,y;
   float fx,fy;

   si_buffer += 2 * (e_rgn.Y * si_size.Width + e_rgn.X);
   int sex = e_rgn.Width  - 1;
   int sey = e_rgn.Height - 1;
   int s_stride = 2 * si_size.Width;
   int d_stride = 2 * di_size.Width;
   float dx = (float)e_rgn.Width  / di_size.Width;
   float dy = (float)e_rgn.Height / di_size.Height;
   for (y = 0,fy = 0.0f; y < di_size.Height; y++,fy += dy) {
      int iy1 = (int)fy;
      int iy2 = iy1 + 1;
      if (iy2 > sey) iy2 = sey;
      uint iry2 = (uint)(256.0f * (fy - iy1));
      uint iry1 = 256 - iry2;
      byte *l_si_buffer1 = si_buffer + iy1 * s_stride;
      byte *l_si_buffer2 = si_buffer + iy2 * s_stride;
      byte *l_di_buffer  = di_buffer + y   * d_stride;
      for (x = 0,fx = 0.0f; x < di_size.Width; x++,fx += dx) {
         int ix1 = (int)fx;
         int ix2 = ix1 + 1;
         if (ix2 > sex) ix2 = sex;
         int ix12 = 2 * ix1;
         int ix22 = 2 * ix2;
         int x2   = 2 * x;
         uint irx2 = (uint)(256.0f * (fx - ix1));
         uint irx1 = 256 - irx2;
         uint b1 = iry1 * l_si_buffer1[ix12] + iry2 * l_si_buffer2[ix12];
         uint b2 = iry1 * l_si_buffer1[ix22] + iry2 * l_si_buffer2[ix22];
         l_di_buffer[x2] = (byte)((irx1 * b1 + irx2 * b2) >> 16);
         if (x & 0x0001) { // ?? ??? X ??? ??? ??: V
            if (ix1 & 0x0001) { // ?? ??? X ??? ??? ??
               l_di_buffer[x2 + 1] = l_si_buffer1[ix12 + 1];
            }
            else { //?? ??? X ??? ??? ??
               l_di_buffer[x2 + 1] = l_si_buffer1[ix12 - 1];
            }
         }
         else { // ?? ??? X ??? ??? ??: U
            if (ix1 & 0x0001) { // ?? ??? X ??? ??? ??
               l_di_buffer[x2 + 1] = l_si_buffer1[ix12 - 1];
            }
            else {
               l_di_buffer[x2 + 1] = l_si_buffer1[ix12 + 1];
            }
         }
      }
   }
}
 
 void GetBlendedImage (BGRImage& s_cimage,GImage& m_image,BGRColor& color,float alpha,BGRImage& d_cimage)

{
   int x,y;

   int b  = (int)(256 * (1.0f - alpha));
   int cr = (int)(alpha * color.R * 256);
   int cg = (int)(alpha * color.G * 256);
   int cb = (int)(alpha * color.B * 256);
   for (y = 0; y < s_cimage.Height; y++) {
      BGRPixel* l_s_cimage = s_cimage[y];
      BGRPixel* l_d_cimage = d_cimage[y];
      byte* l_m_image = m_image[y];
      for (x = 0; x < s_cimage.Width; x++) {
         if (l_m_image[x]) {
            BGRPixel& sp = l_s_cimage[x];
            BGRPixel& dp = l_d_cimage[x];
            dp.B = (byte)((b * sp.B + cb) >> 8);
            dp.G = (byte)((b * sp.G + cg) >> 8);
            dp.R = (byte)((b * sp.R + cr) >> 8);
         }
      }
   }
}

 void GetEvenField (GImage &s_image,GImage &d_image)

{
   int i,j;

   for (i = j = 0; j < d_image.Height; i += 2,j++)
      CopyArray1D ((byte*)s_image[i],0,s_image.Width,sizeof(byte),(byte *)d_image[j],0);
}

 void GetEvenField (BGRImage &s_image,BGRImage &d_image)

{
   int i,j;

   for (i = j = 0; j < d_image.Height; i += 2,j++)
      CopyArray1D ((byte*)s_image[i],0,s_image.Width,sizeof(BGRPixel),(byte *)d_image[j],0);
}

 void GetOddField (GImage &s_image,GImage &d_image)

{
   int i,j;

   for (i = 1,j = 0; j < d_image.Height; i += 2,j++)
      CopyArray1D ((byte*)s_image[i],0,s_image.Width,sizeof(byte),(byte *)d_image[j],0);
}

 void GetOddField (BGRImage &s_image,BGRImage &d_image)

{
   int i,j;

   for (i = 1,j = 0; j < d_image.Height; i += 2,j++)
      CopyArray1D ((byte*)s_image[i],0,s_image.Width,sizeof(BGRPixel),(byte *)d_image[j],0);
}

 int64 GetSumPixels (GImage& s_image,int sx,int sy,int width,int height)

{
   int x,y;
   
   int ex = sx + width;
   int ey = sy + height;
   int64 sum = 0;
   for (y = sy; y < ey; y++) {
      byte* l_s_image = s_image[y];
      for (x = sx; x < ex; x++) {
         sum += l_s_image[x];
      }
   }
   return (sum);
}

 void InterlaceEvenOddFields (byte *s_img_buf,int width,int height,byte *d_img_buf)

{
   int i;

   int hh = height / 2;
   int w2 = width * 2;
   int w4 = w2 * 2;
   for (i = 0; i < hh; i++)
      memcpy ((void*)(d_img_buf + i * w4),(const void*)(s_img_buf + i * w2),w2);
   for (i = hh; i < height; i++)
      memcpy ((void*)(d_img_buf + (i - hh) * w4 + w2),(const void*)(s_img_buf + i * w2),w2);
}

 void Or (GImage &s_image1,GImage &s_image2,GImage &d_image)

{
   int x,y;
   
   for (y = 0; y < d_image.Height; y++) {
      uint* sp1 = (uint*)s_image1[y];
      uint* sp2 = (uint*)s_image2[y];
      uint* dp  = (uint*)d_image[y];
      for (x = 0; x < d_image.Width; x += sizeof(uint)) {
         *dp = *sp1 | *sp2;
         sp1++,sp2++,dp++;
      }
   }   
}

 void Reduce (GImage &s_image,GImage &d_image)

{
   int i,j,k;

   FArray2D t_array(d_image.Width,s_image.Height);
   float scale = (float)s_image.Width / d_image.Width;
   for (i = 0; i < s_image.Height; i++) {
      float sum = 0.0f;
      float x   = 0.0f;
      byte*  l_s_image = s_image[i];
      float* l_t_array = t_array[i];
      for (j = k = 0; j < d_image.Width; j++) {
         x += scale;
         float y = x - k;
         while (y > 1.0f) {
            sum += l_s_image[k];
            y -= 1.0f;
            k++;
         }
         if (k >= s_image.Width) k = s_image.Width - 1;
         sum += y * l_s_image[k];
         l_t_array[j] = sum / scale;
         sum = (1.0f - y) * l_s_image[k];
         k++;
      }
   }
   scale = (float)s_image.Height / d_image.Height;
   for (j = 0; j < d_image.Width; j++) {
      float sum = 0.0f;
      float x   = 0.0f;
      for (i = k = 0; i < d_image.Height; i++) {
         x += scale;
         float y =  x - k;
         while (y > 1.0f) {
            sum += t_array[k][j];
            y -= 1.0f;
            k++;
         }
         if (k >= s_image.Height) k = s_image.Height - 1;
         sum += y * t_array[k][j];
         d_image[i][j] = (byte)(sum / scale);
         sum = (1.0f - y) * t_array[k][j];
         k++;
      }
   }
}

 void Reduce (BGRImage &s_image,BGRImage &d_image)

{
   int i,j,k;
   BGRPixel* p;

   FArray2D r_array(d_image.Width,s_image.Height);
   FArray2D g_array(d_image.Width,s_image.Height);
   FArray2D b_array(d_image.Width,s_image.Height);
   float scale = (float)s_image.Width / d_image.Width;
   for (i = 0; i < s_image.Height; i++) {
      float sum_r = 0.0f;
      float sum_g = 0.0f;
      float sum_b = 0.0f;
      float x     = 0.0f;
      BGRPixel* l_s_image = s_image[i];
      float* l_r_array = r_array[i];
      float* l_g_array = g_array[i];
      float* l_b_array = b_array[i];
      for (j = k = 0; j < d_image.Width; j++) {
         x += scale;
         float y = x - k;
         while (y > 1.0f) {
            p = l_s_image + k;
            sum_r += p->R;
            sum_g += p->G;
            sum_b += p->B;
            y -= 1.0f;
            k++;
         }
         if (k >= s_image.Width) k = s_image.Width - 1;
         p = l_s_image + k;
         float y1 = 1.0f - y;
         sum_r += y * p->R;
         l_r_array[j] = sum_r / scale;
         sum_r = y1 * p->R;
         sum_g += y * p->G;
         l_g_array[j] = sum_g / scale;
         sum_g = y1 * p->G;
         sum_b += y * p->B;
         l_b_array[j] = sum_b / scale;
         sum_b = y1 * p->B;
         k++;
      }
   }
   scale = (float)s_image.Height / d_image.Height;
   for (j = 0; j < d_image.Width; j++) {
      float sum_r = 0.0f;
      float sum_g = 0.0f;
      float sum_b = 0.0f;
      float x     = 0.0f;
      for (i = k = 0; i < d_image.Height; i++) {
         x += scale;
         float y = x - k;
         while (y > 1.0f) {
	         sum_r += r_array[k][j];
	         sum_g += g_array[k][j];
	         sum_b += b_array[k][j];
            y -= 1.0f;
            k++;
         }
         if (k >= s_image.Height) k = s_image.Height - 1;
         float y1 = 1.0f - y;
         p = &d_image[i][j];
         sum_r += y * r_array[k][j];
         p->R = (byte)(sum_r / scale);
         sum_r = y1 * r_array[k][j];
         sum_g += y * g_array[k][j];
         p->G = (byte)(sum_g / scale);
         sum_g = y1 * g_array[k][j];
         sum_b += y * b_array[k][j];
         p->B = (byte)(sum_b / scale);
         sum_b = y1 * b_array[k][j];
         k++;
      }
   }
}

 void Resize (GImage &s_image,GImage &d_image)

{
   int x,y;

   int sex  = s_image.Width  - 1;
   int sey  = s_image.Height - 1;
   float dx = (float)s_image.Width  / d_image.Width;
   float dy = (float)s_image.Height / d_image.Height;
   float fy = 0.0f;
   for (y = 0; y < d_image.Height; y++,fy += dy) {
      int iy  = (int)fy;
      int iy1 = iy + 1;
      if (iy1 > sey) iy1 = sey;
      float ry2 = fy - iy;
      float ry1 = 1.0f - ry2;
      float fx  = 0.0f;
      byte* l_s_image  = s_image[iy];
      byte* l_s_image1 = s_image[iy1];
      byte* l_d_image  = d_image[y];
      for (x = 0; x < d_image.Width; x++,fx += dx) {
         int ix  = (int)fx;
         int ix1 = ix + 1;
         if (ix1 > sex) ix1 = sex;
         float rx = fx - ix;
         float b1 = ry1 * l_s_image[ix ] + ry2 * l_s_image1[ix ];
         float b2 = ry1 * l_s_image[ix1] + ry2 * l_s_image1[ix1];
         l_d_image[x] = (byte)((1.0f - rx) * b1 + rx * b2);
      }
   }
}

 void Resize (BGRImage &s_image,BGRImage &d_image)

{
   int x,y;

   int sex  = s_image.Width  - 1;
   int sey  = s_image.Height - 1;
   float dx = (float)s_image.Width  / d_image.Width;
   float dy = (float)s_image.Height / d_image.Height;
   float fy = 0.0f;
   for (y = 0; y < d_image.Height; y++,fy += dy) {
      int iy  = (int)fy;
      int iy1 = iy + 1;
      if (iy1 > sey) iy1 = sey;
      float ry2 = fy - iy;
      float ry1 = 1.0f - ry2;
      float fx  = 0.0f;
      BGRPixel* l_s_image  = s_image[iy];
      BGRPixel* l_s_image1 = s_image[iy1];
      BGRPixel* l_d_image  = d_image[y];
      for (x = 0; x < d_image.Width; x++,fx += dx) {
         int ix  = (int)fx;
         int ix1 = ix + 1;
         if (ix1 > sex) ix1 = sex;
         float rx2 = fx - ix;
         float rx1 = 1.0f - rx2;
         BGRPixel *sp00 = l_s_image  + ix;
         BGRPixel *sp01 = l_s_image  + ix1;
         BGRPixel *sp10 = l_s_image1 + ix;
         BGRPixel *sp11 = l_s_image1 + ix1;
         BGRPixel *dp   = l_d_image  + x;
         float b1 = ry1 * sp00->R + ry2 * sp10->R;
         float b2 = ry1 * sp01->R + ry2 * sp11->R;
         dp->R = (byte)(rx1 * b1 + rx2 * b2);
         b1 = ry1 * sp00->G + ry2 * sp10->G;
         b2 = ry1 * sp01->G + ry2 * sp11->G;
         dp->G = (byte)(rx1 * b1 + rx2 * b2);
         b1 = ry1 * sp00->B + ry2 * sp10->B;
         b2 = ry1 * sp01->B + ry2 * sp11->B;
         dp->B = (byte)(rx1 * b1 + rx2 * b2);
      }
   }
}

 void Resize_YUY2 (byte *si_buffer,int si_width,int si_height,byte *di_buffer,int di_width,int di_height)
 
{
   int   x,y;
   float fx,fy;

   int sex = si_width  - 1;
   int sey = si_height - 1;
   int s_stride = 2 * si_width;
   int d_stride = 2 * di_width;
   float dx = (float)si_width  / di_width;
   float dy = (float)si_height / di_height;
   for (y = 0,fy = 0.0f; y < di_height; y++,fy += dy) {
      int iy1 = (int)fy;
      int iy2 = iy1 + 1;
      if (iy2 > sey) iy2 = sey;
      uint iry2 = (uint)(256.0f * (fy - iy1));
      uint iry1 = 256 - iry2;
      byte *l_si_buffer1 = si_buffer + iy1 * s_stride;
      byte *l_si_buffer2 = si_buffer + iy2 * s_stride;
      byte *l_di_buffer  = di_buffer + y   * d_stride;
      for (x = 0,fx = 0.0f; x < di_width; x++,fx += dx) {
         int ix1 = (int)fx;
         int ix2 = ix1 + 1;
         if (ix2 > sex) ix2 = sex;
         int ix12 = 2 * ix1;
         int ix22 = 2 * ix2;
         int x2   = 2 * x;
         uint irx2 = (uint)(256.0f * (fx - ix1));
         uint irx1 = 256 - irx2;
         uint b1 = iry1 * l_si_buffer1[ix12] + iry2 * l_si_buffer2[ix12];
         uint b2 = iry1 * l_si_buffer1[ix22] + iry2 * l_si_buffer2[ix22];
         l_di_buffer[x2] = (byte)((irx1 * b1 + irx2 * b2) >> 16);
         if (x & 0x0001) { // 타겟 위치의 X 좌표가 홀수인 경우: V
            if (ix1 & 0x0001) { // 소스 위치의 X 좌표가 홀수인 경우
               l_di_buffer[x2 + 1] = l_si_buffer1[ix12 + 1];
            }
            else { //소스 위치의 X 좌표가 짝수인 경우
               l_di_buffer[x2 + 1] = l_si_buffer1[ix12 - 1];
            }
         }
         else { // 타겟 위치의 X 좌표가 짝수인 경우: U
            if (ix1 & 0x0001) { // 소스 위치의 X 좌표가 홀수인 경우
               l_di_buffer[x2 + 1] = l_si_buffer1[ix12 - 1];
            }
            else {
               l_di_buffer[x2 + 1] = l_si_buffer1[ix12 + 1];
            }
         }
      }
   }
}

 void Resize_YV12 (byte *si_buffer,int si_width,int si_height,byte *di_buffer,int di_width,int di_height)
 
{
   int   x,y;
   float fx,fy;

   int sex = si_width  - 1;
   int sey = si_height - 1;
   float dx = (float)si_width  / di_width;
   float dy = (float)si_height / di_height;
   for (y = 0,fy = 0.0f; y < di_height; y++,fy += dy) {
      int iy  = (int)fy;
      int iy1 = iy + 1;
      if (iy1 > sey) iy1 = sey;
      uint iry2 = (uint)(256.0f * (fy - iy));
      uint iry1 = 256 - iry2;
      byte *l_si_buffer_y  = si_buffer + iy  * si_width;
      byte *l_si_buffer1_y = si_buffer + iy1 * si_width;
      byte *l_di_buffer_y  = di_buffer + y   * di_width;
      for (x = 0,fx = 0.0f; x < di_width; x++,fx += dx) {
         int ix  = (int)fx;
         int ix1 = ix + 1;
         if (ix1 > sex) ix1 = sex;
         uint irx2 = (uint)(256.0f * (fx - ix));
         uint irx1 = 256 - irx2;
         uint b1 = iry1 * l_si_buffer_y[ix ] + iry2 * l_si_buffer1_y[ix ];
         uint b2 = iry1 * l_si_buffer_y[ix1] + iry2 * l_si_buffer1_y[ix1];
         l_di_buffer_y[x] = (byte)((irx1 * b1 + irx2 * b2) >> 16);
      }
   }
   float dx2 = 2.0f * dx;
   float dy2 = 2.0f * dy;
   int si_width2  = si_width  / 2;
   int si_height2 = si_height / 2;
   int di_width2  = di_width  / 2;
   int di_height2 = di_height / 2;
   byte *si_buffer_u = si_buffer   + si_width  * si_height;
   byte *si_buffer_v = si_buffer_u + si_width2 * si_height2;
   byte *di_buffer_u = di_buffer   + di_width  * di_height;
   byte *di_buffer_v = di_buffer_u + di_width2 * di_height2;
   for (y = 0,fy = 0.0f; y < di_height2; y++,fy += dy2) {
      int dwy = di_width2 * y;
      int iy2 = ((int)fy) / 2;
      int swy = si_width2 * iy2;
      byte *l_si_buffer_u = si_buffer_u + swy;
      byte *l_si_buffer_v = si_buffer_v + swy;
      byte *l_di_buffer_u = di_buffer_u + dwy;
      byte *l_di_buffer_v = di_buffer_v + dwy;
      for (x = 0,fx = 0.0f; x < di_width2; x++,fx += dx2) {
         int ix2 = ((int)fx) / 2;
         l_di_buffer_u[x] = l_si_buffer_u[ix2];
         l_di_buffer_v[x] = l_si_buffer_v[ix2];
      }
   }
}

 void Rotate90Degrees (GImage &s_image,GImage &d_image)

{
   int x,y;
   
   int ey = s_image.Height - 1;
   for (y = 0; y < s_image.Height; y++) {
      byte* l_s_image = s_image[y];
      for (x = 0; x < s_image.Width; x++)
         d_image[x][ey - y] = l_s_image[x];
   }
}

 void Rotate90Degrees (BGRImage &s_image,BGRImage &d_image)

{
   int x,y;
   
   int ey = s_image.Height - 1;
   for (y = 0; y < s_image.Height; y++) {
      BGRPixel* l_s_image = s_image[y];
      for (x = 0; x < s_image.Width; x++)
         d_image[x][ey - y] = l_s_image[x];
   }
}

 void Rotate180Degrees (GImage &s_image,GImage &d_image)

{
   int x,y;
   
   int ey = s_image.Height - 1;
   int ex = s_image.Width  - 1;
   for (y = 0; y < s_image.Height; y++) {
      int y2 = ey - y;
      byte* l_s_image = s_image[y];
      for (x = 0; x < s_image.Width; x++)
         d_image[y2][ex - x] = l_s_image[x];
   }
}

 void Rotate180Degrees (BGRImage &s_image,BGRImage &d_image)

{
   int x,y;
   
   int ey = s_image.Height - 1;
   int ex = s_image.Width  - 1;
   for (y = 0; y < s_image.Height; y++) {
      int y2 = ey - y;
      BGRPixel* l_s_image = s_image[y];
      for (x = 0; x < s_image.Width; x++)
         d_image[y2][ex - x] = l_s_image[x];
   }
}

 void Rotate270Degrees (GImage &s_image,GImage &d_image)

{
   int x,y;

   int ex = s_image.Width - 1;
   for (y = 0; y < s_image.Height; y++) {
      byte* l_s_image = s_image[y];
      for (x = 0; x < s_image.Width; x++)
         d_image[ex - x][y] = l_s_image[x];
   }
}

 void Rotate270Degrees (BGRImage &s_image,BGRImage &d_image)

{
   int x,y;

   int ex = s_image.Width - 1;
   for (y = 0; y < s_image.Height; y++) {
      BGRPixel* l_s_image = s_image[y];
      for (x = 0; x < s_image.Width; x++)
         d_image[ex - x][y] = l_s_image[x];
   }
}
 
}

#if !defined(__BCCL_STRING_H)
#define __BCCL_STRING_H

#include "bccl_array.h"

 namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: StringA
//
///////////////////////////////////////////////////////////////////////////////

class StringA;
typedef Array1D<StringA> STRArray1DA;

 class StringA : public CArray1D
 
{
   public:
      StringA (   );
      StringA (int length);
      StringA (char  c);
      StringA (const char *s_buffer);
      StringA (StringA *s_string);
   
   public:
             StringA& operator =  (char c);
             StringA& operator =  (const char *s_buffer);
             StringA  operator +  (const char *s_buffer);
      friend StringA  operator +  (const char *s_buffer,const StringA &s_string);
             StringA  operator +  (char c);
      friend StringA  operator +  (char c,const StringA &s_string);
             StringA& operator += (const char *s_buffer);
             StringA& operator += (char c);
             int      operator == (const char *s_buffer);

   public:
      virtual int Create (int length);
   
   public:
      void    CopyFrom         (const char *s_buffer,int s_pos,int length,int d_pos);
      void    CopyTo           (char *d_buffer);
      void    CopyTo           (int s_pos,int length,char *d_buffer,int d_pos);
      int     Create           (char c);
      int     Create           (const char *s_buffer);
      StringA Extract          (int s_pos,int length);
      int     Format           (const char *format,...);
      int     GetStringLength  (   ) const;
      void    ToLower          (   );
      void    ToUpper          (   );
};

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

// 주어진 두 문자열을 대소문자 구분 없이 비교한다.
// 리턴 값:  0: s_string1 == s_string2
//          -1: s_string1 <  s_string2
//           1: s_string1 >  s_string2
int   CompareStrings (const char *s_string1,const char *s_string2);
// 16진수 형태로 주어진 문자열을 short 형 숫자로 바꾼다.
short ConvertStringToNumber_HexShort (const char *s_string);
// 16진수 형태로 주어진 문자열을 unsigned short 형 숫자로 바꾼다.
ushort ConvertStringToNumber_HexUShort (const char *s_string);
// s_string이 가리키는 문자열을 d_string이 가리키는 버퍼에 복사한다.
// 이때 C <= ' ' 또는 C == '"'인 문자(C)는 제외하고 문자열을 복사한다.
// 단 " "로 묶여 있는 문자열은 그대로 복사한다.
// s_string이 가리키는 문자열은 s_buf_size 길이 만큼 처리된다.
// s_string이 가리키는r 문자열에 null 문자가 포함되어 있으면 null 문자 직전까지 처리된다.
// s_buf_size를 0보다 작은 값으로 설정하면 s_string 문자열은 null-terminated 문자열이어야 한다.
// d_string으로 복사된 문자열은 null-terminated 문자열이다.
// 리턴값: 문자열을 정상적으로 복사하면 0을 리턴한다.
//         복사할 문자열의 최종 길이가 (d_buf_size - 1)보다 크면 중간에 복사를 멈추고 0이 아닌 값을 리턴한다.
int CopyString (const char *s_string,int s_buf_size,char *d_string,int d_buf_size);
// 주어진 문자열을 단어들로 분리한다.
// 단어 사이의 구분은 공백 문자(열)에 의해 이루어진다.
// 리턴값: 정상 동작하면 DONE을 리턴한다.
int SplitIntoWords (const char* s_string,STRArray1DA &d_array);

}

#endif

#include "bccl_console.h"
#include "bccl_string.h"

 namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: StringA
//
///////////////////////////////////////////////////////////////////////////////

 StringA::StringA (   )
 
{
   _Init (   );
}

 StringA::StringA (int length)
 
{
   _Init (  );
   Create (length);
}

 StringA::StringA (char c)

{
   _Init (  );
   Create (c);
}

 StringA::StringA (const char *s_buffer)
 
{
   _Init (  );
   Create (s_buffer);
}

 StringA::StringA (StringA *s_string)
 
{
   Length = s_string->Length;
   Array = s_string->Array;
   s_string->Array = NULL;
   s_string->Length = 0;
}

 StringA& StringA::operator = (char c)

{
   Create (c);
   return (*this);
}

 StringA& StringA::operator = (const char *s_buffer)
 
{
   Create (s_buffer);
   return (*this);   
}

 StringA StringA::operator + (const char *s_buffer)
 
{
   StringA d_string;
   if (Array == NULL) d_string = s_buffer;
   else {
      if (!s_buffer) d_string = *this;
      else {
         int sl1 = GetStringLength (   );
         int sl2 = (int)strlen (s_buffer);
         int dl  = sl1 + sl2;
         d_string.Create (dl + 1);
         d_string.CopyFrom (Array,0,sl1,0);
         d_string.CopyFrom (s_buffer,0,sl2,sl1);
         d_string[dl] = 0;
      }
   }
   return (StringA(&d_string));
}

 StringA operator + (const char *s_buffer,const StringA &s_string)
 
{
   StringA d_string;
   if (s_buffer == NULL) d_string = s_string;
   else {
      if (!s_string) d_string = s_buffer;
      else {
         int sl1 = (int)strlen (s_buffer);
         int sl2 = s_string.GetStringLength (   );
         int dl  = sl1 + sl2;
         d_string.Create (dl + 1);
         d_string.CopyFrom (s_buffer,0,sl1,0);
         d_string.CopyFrom (s_string,0,sl2,sl1);
         d_string[dl] = 0;
      }
   }
   return (StringA(&d_string));
}

 StringA StringA::operator + (char c)
 
{
   StringA d_string;
   if (Array == NULL) d_string = c;
   else {
      int sl = GetStringLength (   );
      d_string.Create (sl + 2);
      d_string.CopyFrom (Array,0,sl,0);
      d_string[sl] = c;
      d_string[sl + 1] = 0;
   }
   return (StringA(&d_string));
}

 StringA operator + (char c,const StringA &s_string)
 
{
   StringA d_string;
   if (!s_string) d_string = c;
   else {
      int sl = s_string.GetStringLength (   );
      d_string.Create (sl + 2);
      d_string[0] = c;
      d_string.CopyFrom (s_string,0,sl,1);
      d_string[sl + 1] = 0;
   }
   return (StringA(&d_string));   
}

 StringA& StringA::operator += (const char *s_buffer)
 
{
   *this = *this + s_buffer;
   return (*this);
}

 StringA& StringA::operator += (char c)
 
{
   *this = *this + c;
   return (*this);
}

 int StringA::operator == (const char *s_buffer)
 
{
   if (!strcmp (Array,s_buffer)) return (TRUE);
   else return (FALSE);
}

 void StringA::CopyFrom (const char *s_buffer,int s_pos,int length,int d_pos)
 
{
   int i,j;
   
   int ei = s_pos + length;
   int ej = d_pos + length;
   if (ej > Length) ei -= ej - Length;
   for (i = s_pos,j = d_pos; i < ei; i++,j++)
      Array[j] = s_buffer[i];
}

 void StringA::CopyTo (char *d_buffer)
 
{
   int i;
   
   for (i = 0; Array[i]; i++) d_buffer[i] = Array[i];
   d_buffer[i] = 0;
}

 void StringA::CopyTo (int s_pos,int length,char *d_buffer,int d_pos)
 
{
   int i,j;
   
   int ei = s_pos + length;
   if (ei > Length) ei = Length;
   for (i = s_pos,j = d_pos; i < ei; i++,j++)
      d_buffer[j] = Array[i];
}

 int StringA::Create (int length)
 
{
   if (CArray1D::Create (length) == DONE) {
      if (length > 0) Array[0] = 0;
      return (DONE);
   }
   else return (1);
}

 int StringA::Create (char c)
 
{
   if (Create (2) == DONE) {
      Array[0] = c;
      Array[1] = 0;
      return (DONE);
   }
   else return (1);
}

 int StringA::Create (const char *s_buffer)
 
{
   if (s_buffer == NULL) {
      Delete (   );
      return (DONE);
   }
   else {
      if (Create ((int)strlen (s_buffer) + 1) == DONE) {
         strcpy (Array,s_buffer);
         return (DONE);
      }
      else return (1);
   }
}

 StringA StringA::Extract (int s_pos,int length)
 
{
   StringA d_string(length + 1);
   d_string.CopyFrom (Array,s_pos,length,0);
   d_string[length] = 0;
   return (StringA(&d_string));
}

 int StringA::Format (const char *format,...)
 
{
   char buffer[MAX_BUFFER_SIZE];
   
   va_list arg_list;
   va_start (arg_list,format);
   if (vsprintf (buffer,format,arg_list) < 0) {
      Delete (   );
      return (1);
   }
   va_end (arg_list);
   Create (buffer);
   return (DONE);
}

 int StringA::GetStringLength (   ) const
 
{
   if (Array == NULL) return (0);
   return ((int)strlen (Array));
}

 void StringA::ToLower (   )
 
{
   int i;
   
   if (Array == NULL) return;
   for (i = 0; Array[i]; i++)
      Array[i] = (char)tolower (Array[i]);
}

 void StringA::ToUpper (   )
 
{
   int i;
   
   if (Array == NULL) return;
   for (i = 0; Array[i]; i++)
      Array[i] = (char)toupper (Array[i]);
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 int CompareStrings (const char *s_string1,const char *s_string2)

{
   int i = 0;
   while (TRUE) {
      int c1 = tolower (s_string1[i]);
      int c2 = tolower (s_string2[i]);
      if      (c1 < c2) return (-1);
      else if (c1 > c2) return ( 1);
      else if (!c1 && !c2) return (0);
      i++;
   }
}

 short ConvertStringToNumber_HexShort (const char *s_string)
 
{
   return ((short)strtol (s_string,NULL,16));
}

 ushort ConvertStringToNumber_HexUShort (const char *s_string)
 
{
   return ((ushort)strtol (s_string,NULL,16));
}

 int CopyString (const char *s_string,int s_buf_size,char *d_string,int d_buf_size)
 
{
   int i,n;
   int r_code  = DONE;
   int flag_qm = FALSE;
   d_buf_size--;
   for (i = n = 0; s_buf_size < 0 || n < s_buf_size; n++) {
     if (i >= d_buf_size) {
        r_code = 1;
        break;
      }
      char c = s_string[n];
      if (!c) break;
      if (c == '"') flag_qm = 1 - flag_qm;
      else if (c <= ' ') { // 특수문자 또는 스페이스인 경우
         if (flag_qm) d_string[i++] = c; // " " 안에 있는 경우에만 복사한다.
      }
      else d_string[i++] = c;
   }
   if (d_buf_size) d_string[i] = 0;
   return (r_code);
}

 int SplitIntoWords (const char* s_string,STRArray1DA &d_array)
 
{
   int i;
   
   d_array.Delete (   );
   if (s_string == NULL) return (1);
   int n_words  = 0;
   int word_len = 0;
   for (i = 0; s_string[i]; i++) {
      char c = s_string[i];
      if (c < ' ') continue;
      else if (c == ' ') {
         if (word_len) {
            n_words++;
            word_len = 0;
         }
      }
      else word_len++;
   }
   if (word_len) n_words++;
   if (!n_words) return (2);
   d_array.Create (n_words);
   n_words  = 0;
   word_len = 0;
   char* word_sp = NULL;
   for (i = 0; s_string[i]; i++) {
      char c = s_string[i];
      if (c < ' ') continue;
      else if (c == ' ') {
         if (word_sp) {
            d_array[n_words].Create (word_len + 1);
            memcpy ((char*)d_array[n_words],word_sp,word_len * sizeof(char));
            d_array[n_words][word_len] = 0;
            n_words++;
            word_sp  = NULL;
            word_len = 0;
         }
      }
      else {
         if (word_sp == NULL) word_sp = (char*)s_string + i;
         word_len++;
      }
   }
   if (word_sp) {
      d_array[n_words].Create (word_len + 1);
      memcpy ((char*)d_array[n_words],word_sp,word_len * sizeof(char));
      d_array[n_words][word_len] = 0;
   }
   return (DONE);
}

}

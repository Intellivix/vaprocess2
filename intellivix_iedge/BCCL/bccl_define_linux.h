﻿#ifndef __BCCL_DEFINE_LINUX_H
#define __BCCL_DEFINE_LINUX_H

#if !defined( _MSC_VER )

#include <cstdint>

typedef int64_t int64;
typedef uint64_t uint64;
typedef int BOOL;
typedef char CHAR;
typedef unsigned char BYTE;
typedef short SHORT;
typedef unsigned short WORD;
typedef int INT;
typedef unsigned int UINT;
//typedef unsigned long DWORD;
//typedef unsigned int UINT32;
using DWORD = uint32_t;
using UINT32 = uint32_t;
typedef int64 INT64;
typedef uint64 UINT64;
typedef int64_t LONGLONG;
typedef uint64_t ULONGLONG;
typedef long LONG;
typedef unsigned long ULONG;
typedef float FLOAT;
typedef double DOUBLE;
typedef unsigned char UCHAR;
typedef unsigned short USHORT;
typedef const char* LPCSTR;
typedef const char* PCSTR;
typedef const char* LPCTSTR;
typedef void* LPVOID;
typedef void* PVOID;
typedef void VOID;
typedef PVOID HANDLE;
typedef HANDLE HWND;
typedef DWORD COLORREF;
typedef char TCHAR;

#define _atoi64( s ) atoll( s )
#define _tcslen( s ) strlen( s )
#define _tcsncpy( d, s, n ) strncpy( d, s, n )
#define _tcscpy( d, s ) strcpy( d, s )
#define _tcscmp( s1, s2 ) strcmp( s1, s2 )
#define _tcsncmp( s1, s2, n ) strncmp( s1, s2, n )
#define _tcstol( n, e, b ) strtol( n, e, b )
#define _tcstod( n, e ) strtod( n, e )
#define _tcstok( s, d ) strtok( s, d )
#define _itot( v, s, r ) sprintf( s, "%d", v )
#define _ltot( v, s, r ) sprintf( s, "%ld", v )
#define _T( s ) s
#define _istalnum( c ) isalnum( c )
#define _stprintf sprintf
#define _ttoi atoi
#define _stscanf sscanf
#define _tcscspn strcspn
#define _tstof atof
#define _tcstok_s( s, d, p ) strtok( s, d )
#define _tmain main
#define _tfopen fopen
#define _tfopen_s fopen_s

#define MAX_PATH 256
#define E_INVALIDARG

#define RGB( r, g, b ) ( ( COLORREF )( ( ( BYTE )( r ) | ( ( WORD )( ( BYTE )( g ) ) << 8 ) ) | ( ( ( DWORD )( BYTE )( b ) ) << 16 ) ) )
#define ASSERT( f )
#define ZeroMemory( Destination, Length ) memset( ( Destination ), 0, ( Length ) )
#define CopyMemory( Destination, Source, Length ) memcpy( ( Destination ), ( Source ), ( Length ) )
#define ATLENSURE( expr )
#define ATLASSUME( expr )
#define AtlThrow( x )
#define AfxMessageBox( x )

/////////////////////////////////////////////////////////////////////////////

#define _In_
#define _Out_
#define _Inout_
#define _In_z_

typedef unsigned int UINT_PTR;
typedef long LONG_PTR;

/* Types use for passing & returning polymorphic values */
typedef UINT_PTR WPARAM;
typedef LONG_PTR LPARAM;
typedef LONG_PTR LRESULT;

#define WINAPI
#define DWORD_PTR DWORD
#define CONST const

#define MAKEWORD( a, b ) ( ( WORD )( ( ( BYTE )( ( ( DWORD_PTR )( a ) ) & 0xff ) ) | ( ( WORD )( ( BYTE )( ( ( DWORD_PTR )( b ) ) & 0xff ) ) ) << 8 ) )
#define MAKELONG( a, b ) ( ( LONG )( ( ( WORD )( ( ( DWORD_PTR )( a ) ) & 0xffff ) ) | ( ( DWORD )( ( WORD )( ( ( DWORD_PTR )( b ) ) & 0xffff ) ) ) << 16 ) )
#define LOWORD( l ) ( ( WORD )( ( ( DWORD_PTR )( l ) ) & 0xffff ) )
#define HIWORD( l ) ( ( WORD )( ( ( ( DWORD_PTR )( l ) ) >> 16 ) & 0xffff ) )
#define LOBYTE( w ) ( ( BYTE )( ( ( DWORD_PTR )( w ) ) & 0xff ) )
#define HIBYTE( w ) ( ( BYTE )( ( ( ( DWORD_PTR )( w ) ) >> 8 ) & 0xff ) )

/////////////////////////////////////////////////////////////////////////////////////

#include <cerrno>
#include <unistd.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>

#define SOCKET int
#define INVALID_SOCKET ( SOCKET )( ~0 )
#define SD_BOTH ( 2 )
#define closesocket( x ) ( shutdown( x, SD_BOTH ), close( x ) )
#define ioctlsocket ioctl
#define SOCKET_ERROR ( -1 )

typedef void* HANDLE;

typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr* LPSOCKADDR;

typedef struct _in_addr_x {
   union {
      struct {
         UCHAR s_b1, s_b2, s_b3, s_b4;
      } S_un_b;
      struct {
         USHORT s_w1, s_w2;
      } S_un_w;
      ULONG S_addr;
   } S_un;
#define s_addr_x S_un.S_addr /* can be used for most tcp & ip code */
#define s_host_x S_un.S_un_b.s_b2 // host on imp
#define s_net_x S_un.S_un_b.s_b1 // network
#define s_imp_x S_un.S_un_w.s_w2 // imp
#define s_impno_x S_un.S_un_b.s_b4 // imp #
#define s_lh_x S_un.S_un_b.s_b3 // logical host
} in_addr_x;

/////////////////////////////////////////////////////////////////////////////////////

#if defined( __CPP11 )
#include <mutex>
class CCriticalSection : public std::recursive_mutex {
public:
   void Lock()
   {
      this->lock();
   }
   void Unlock()
   {
      this->unlock();
   }
};
#elif defined( _POSIX_THREADS ) // __CPP11
class CCriticalSection {
protected:
   pthread_mutex_t Mutex;

public:
   CCriticalSection();
   ~CCriticalSection();

public:
   void Lock();
   void Unlock();
};
#endif // _POSIX_THREADS

/////////////////////////////////////////////////////////////////////////////////////

#include <semaphore.h>
#include <climits>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-variable"
const static DWORD STATUS_WAIT_0 = 0x00000000L;
const static DWORD WAIT_FAILED   = 0xFFFFFFFF;
const static DWORD WAIT_OBJECT_0 = STATUS_WAIT_0 + 0;
const static DWORD WAIT_TIMEOUT  = 258L;
#pragma GCC diagnostic pop

class CEvent {
protected:
   sem_t Handle;

public:
   CEvent();
   ~CEvent();

   void SetEvent();
   void ResetEvent();
   operator HANDLE() const;
};
const static uint32_t INFINITE = UINT32_MAX;
DWORD WaitForSingleObject( HANDLE pSem, uint32_t nTimeout );
/////////////////////////////////////////////////////////////////////////////////////
#define CREATE_SUSPENDED ( 0x00000004 )
#define THREAD_PRIORITY_NORMAL ( 0 )
#define THREAD_PRIORITY_HIGHEST ( 2 )

typedef UINT ( *AFX_THREADPROC )( LPVOID );
typedef LPVOID ( *AFX_THREADPROC_LINUX )( LPVOID );

class CWinThread {
public:
   HANDLE m_hThread;
   pthread_t m_hRealThread;

   CEvent m_evtThread;

   LPVOID m_pThreadParams;
   AFX_THREADPROC m_pfnThreadProc;

public:
   CWinThread();
   virtual ~CWinThread();

   DWORD ResumeThread();

   static LPVOID ThreadProc( LPVOID pVoid );
};

CWinThread* AfxBeginThread( AFX_THREADPROC pfnThreadProc, LPVOID pParam, int nPriority = THREAD_PRIORITY_NORMAL, UINT nStackSize = 0, DWORD dwCreateFlags = 0 );
/////////////////////////////////////////////////////////////////////////////////////
#if defined( __CPP11 )
#include <thread>
inline void Sleep_ms( unsigned nMillisecs )
{
   std::this_thread::sleep_for( std::chrono::milliseconds( nMillisecs ) );
}
inline void Sleep_sec( unsigned seconds )
{
   std::this_thread::sleep_for( std::chrono::seconds( seconds ) );
}
#else // __CPP11
inline void Sleep_ms( DWORD nMillisecs )
{
   usleep( (int)( nMillisecs * 1000 ) );
}
#endif // NOT __CPP11
/////////////////////////////////////////////////////////////////////////////////////

typedef struct _SYSTEMTIME {
   WORD wYear;
   WORD wMonth;
   WORD wDayOfWeek;
   WORD wDay;
   WORD wHour;
   WORD wMinute;
   WORD wSecond;
   WORD wMilliseconds;
} SYSTEMTIME, *PSYSTEMTIME, *LPSYSTEMTIME;

typedef struct _FILETIME {
   DWORD dwLowDateTime;
   DWORD dwHighDateTime;
} FILETIME, *PFILETIME, *LPFILETIME;

#include <ctime>
inline uint32_t GetTickCount()
{
   struct timespec now;
   clock_gettime( CLOCK_MONOTONIC, &now );
   time_t time_ms        = ( now.tv_sec * 1000 ) + ( now.tv_nsec / 1000000 );
   uint32_t uint_time_ms = static_cast<uint32_t>( time_ms );
   return uint_time_ms;
}

/////////////////////////////////////////////////////////////////////////////

typedef struct tagRECT {
   LONG left;
   LONG top;
   LONG right;
   LONG bottom;
} RECT, *PRECT, *LPRECT;

typedef struct tagPOINT {
   LONG x;
   LONG y;
} POINT, *PPOINT, *LPPOINT;

typedef struct tagSIZE {
   LONG cx;
   LONG cy;
} SIZE, *PSIZE, *LPSIZE;

typedef const RECT* LPCRECT;

//////////////////////////////////////////////////////////////////////////////////////////////////////////

typedef struct tagDBTIMESTAMP {
   SHORT year;
   USHORT month;
   USHORT day;
   USHORT hour;
   USHORT minute;
   USHORT second;
   ULONG fraction;
} DBTIMESTAMP;

#endif // !defined( _MSC_VER )

#endif // __BCCL_DEFINE_LINUX_H

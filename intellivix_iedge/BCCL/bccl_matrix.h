#if !defined(__BCCL_MATRIX_H)
#define __BCCL_MATRIX_H
 
#include "bccl_array.h"
#include "bccl_xml.h"

 namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: Matrix
//
///////////////////////////////////////////////////////////////////////////////

 class Matrix

{
   protected:
      double **Array;
      double *Buffer;

   public:
      int NRows,NCols;

   public:
      Matrix (   );
      Matrix (int n_rows);
      Matrix (int n_rows,int n_cols);
      Matrix (double *s_buffer,int n_rows,int n_cols);
      Matrix (double **s_array,int n_rows,int n_cols);
      Matrix (const FPoint2D &s_point);
      Matrix (const FPoint3D &s_point);
      Matrix (const Matrix &mat_S);
      Matrix (Matrix *mat_S);

   public:
      virtual ~Matrix (   );

   public:
             Matrix& operator =        (const FPoint2D &s_point);
             Matrix& operator =        (const FPoint3D &s_point);
             Matrix& operator =        (const Matrix &mat_S);
             Matrix  operator +        (const Matrix &mat_S);
             double  operator +        (double a);
      friend double  operator +        (double a,const Matrix &mat_S);
             Matrix  operator -        (   );
             Matrix  operator -        (const Matrix &mat_S);
             double  operator -        (double a);
      friend double  operator -        (double a,const Matrix &mat_S);
             Matrix  operator *        (const Matrix &mat_S);
             Matrix  operator *        (double a);
      friend Matrix  operator *        (double a,const Matrix &mat_S);
             Matrix  operator /        (double a);
             Matrix& operator +=       (const Matrix &mat_S);
             Matrix& operator -=       (const Matrix &mat_S);
             Matrix& operator *=       (const Matrix &mat_S);
             Matrix& operator *=       (double a);
             Matrix& operator /=       (double a);
             double* operator []       (int i) const;
             double* operator []       (byte i) const;
             double* operator []       (char i) const;
             double* operator []       (uint i) const;
             double* operator []       (short i) const;
             double* operator []       (ushort i) const;
             double& operator ()       (int i) const;
             double& operator ()       (byte i) const;
             double& operator ()       (char i) const;
             double& operator ()       (uint i) const;
             double& operator ()       (short i) const;
             double& operator ()       (ushort i) const;
             int     operator !        (   ) const;
                     operator double   (   ) const;
                     operator double*  (   ) const;
                     operator double** (   ) const;
                     operator void*    (   ) const;
                     operator FPoint2D (   ) const;
                     operator FPoint3D (   ) const;

   protected:
      void _Init (   );   

   public:
      void    Clear           (   );
      void    Copy            (const Matrix &mat_S,int si,int sj,int n_rows,int n_cols,int di,int dj);
      int     Create          (int n_rows);
      int     Create          (int n_rows,int n_cols);
      void    Delete          (   );
      Matrix  Extract         (int si,int sj,int n_rows,int n_cols);
      Matrix  GetColumnVector (int col);
      Matrix  GetMatrix       (int n_rows);
      Matrix  GetRowVector    (int row);
      Matrix  GetVector       (   );
      Matrix& Identity        (   );
      int     Import          (double *s_buffer,int n_rows,int n_cols);
      int     Import          (double **s_array,int n_rows,int n_cols);
      void    Print           (const char *name);
      int     ReadFile        (const char *file_name);
      int     ReadFile        (CXMLIO* pIO);
      void    Set             (int si,int sj,int n_rows,int n_cols,double value);
      void    SetColumnVector (Matrix &vec_s,int col);
      void    SetRowVector    (Matrix &vec_s,int row);
      int     WriteFile       (const char *file_name);
      int     WriteFile       (CXMLIO* pIO);
};

 inline double* Matrix::operator [] (int i) const

{
   return (Array[i]);
}

 inline double* Matrix::operator [] (byte i) const

{
   return (Array[i]);
}

 inline double* Matrix::operator [] (char i) const

{
   return (Array[(int)i]);
}

 inline double* Matrix::operator [] (uint i) const

{
   return (Array[i]);
}

 inline double* Matrix::operator [] (short i) const

{
   return (Array[i]);
}

 inline double* Matrix::operator [] (ushort i) const

{
   return (Array[i]);
}

 inline double& Matrix::operator () (int i) const

{
   return (Buffer[i]);
}

 inline double& Matrix::operator () (byte i) const

{
   return (Buffer[i]);
}

 inline double& Matrix::operator () (char i) const

{
   return (Buffer[(int)i]);
}

 inline double& Matrix::operator () (short i) const

{
   return (Buffer[i]);
}

 inline double& Matrix::operator () (ushort i) const

{
   return (Buffer[i]);
}

 inline int Matrix::operator ! (   ) const

{
   if (Array) return (FALSE);
   else return (TRUE);
}

 inline Matrix::operator double (   ) const

{
   return (Array[0][0]);
}

 inline Matrix::operator double* (   ) const

{
   return (Buffer);
}

 inline Matrix::operator double** (   ) const

{
   return (Array);
}

 inline Matrix::operator void* (   ) const

{
   return ((void *)Buffer);
}

///////////////////////////////////////////////////////////////////////////////
//
// Function Templates
//
///////////////////////////////////////////////////////////////////////////////

 template <class T>
 void Convert (const Array2D<T> &s_array,Matrix &mat_D)

{
   int i,j;

   int sw = s_array.Width;
   int sh = s_array.Height;
   for (i = 0; i < sh; i++) {
      double *_RESTRICT l_mat_D   = mat_D[i];
      double *_RESTRICT l_s_array = s_array[i];
      for (j = 0; j < sw; j++)
         l_mat_D[j] = (double)l_s_array[j];
   }
}

 template <class T>
 void Convert (const Matrix &mat_S,Array2D<T> &d_array)

{
   int i,j;

   for (i = 0; i < mat_S.NRows; i++) {
      double *_RESTRICT l_mat_S   = mat_S[i];
      double *_RESTRICT l_d_array = d_array[i];
      for (j = 0; j < mat_S.NCols; j++)
         l_d_array[j] = (T)l_mat_S[j];
   }
}

 template<class T>
 Matrix GetHomogeneousVector (const Point2D<T> &s_point)

{
   Matrix vec_d(3);
   vec_d(0) = s_point.X;
   vec_d(1) = s_point.Y;
   vec_d(2) = 1.0;
   return (Matrix (&vec_d));
}

 template<class T>
 void GetInhomogeneousCoordinates (const Matrix &vec_s,Point2D<T> &d_point)

{
   d_point.X = (T)(vec_s(0) / vec_s(2));
   d_point.Y = (T)(vec_s(1) / vec_s(2));
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

Matrix CP          (const Matrix &vec_s1,const Matrix &vec_s2);
Matrix CPM         (const Matrix &vec_s);
double Det         (const Matrix &mat_S);
Matrix Diag        (const Matrix &vec_s);
void   EigenDecomp (const Matrix &mat_S,Matrix &vec_e,Matrix &mat_E);
Matrix IM          (int n_rows);
Matrix Inv         (const Matrix &mat_S);
double IP          (const Matrix &vec_s1,const Matrix &vec_s2);
double Mag         (const Matrix &vec_s);
Matrix Nor         (const Matrix &vec_s);
double SquaredMag  (const Matrix &vec_s);
Matrix Tr          (const Matrix &mat_S);
double Trace       (const Matrix &mat_s);

}

#endif

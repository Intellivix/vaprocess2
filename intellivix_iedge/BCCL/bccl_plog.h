﻿#ifndef BCCL_PLOG_H
#define BCCL_PLOG_H

#ifdef _DEBUG
#define PLOG_CAPTURE_FILE
#endif

#include <plog/Log.h>
#include <plog/Appenders/ConsoleAppender.h>
#include <plog/Formatters/FuncMessageFormatter.h>
#include <plog/Appenders/RollingFileAppender.h>
#include <plog/Formatters/MessageOnlyFormatter.h>
#include <plog/WinApi.h>

#include <sys/stat.h> // make dir.
#include <cstdarg>
#include <string>

// converting std's printf to std++'s operator<<
#define logv( ... ) LOG( plog::verbose ) << BCCL::PlogHelper::cFormatToStdString( __VA_ARGS__ )
#define logd( ... ) LOG( plog::debug )   << BCCL::PlogHelper::cFormatToStdString( __VA_ARGS__ )
#define logi( ... ) LOG( plog::info )    << BCCL::PlogHelper::cFormatToStdString( __VA_ARGS__ )
#define logw( ... ) LOG( plog::warning ) << BCCL::PlogHelper::cFormatToStdString( __VA_ARGS__ )
#define loge( ... ) LOG( plog::error )   << BCCL::PlogHelper::cFormatToStdString( __VA_ARGS__ )
#define logf( ... ) LOG( plog::fatal )   << BCCL::PlogHelper::cFormatToStdString( __VA_ARGS__ )
#define logn( ... ) LOG( plog::none )    << BCCL::PlogHelper::cFormatToStdString( __VA_ARGS__ )

////////////////////////////////
/// Override plog libarary.
////////////////////////////////

namespace plog {

inline const char* severityToStringShort( Severity severity )
{
   switch( severity ) {
   case fatal:
      return "F";
   case error:
      return "E";
   case warning:
      return "W";
   case info:
      return "I";
   case debug:
      return "D";
   case verbose:
      return "V";
   default:
      return "N";
   }
}

class IvxConsoleFormatter {
public:
   static util::nstring header()
   {
      return util::nstring();
   }

   static util::nstring format( const Record& record )
   {
      util::nostringstream ss;
      ss << PLOG_NSTR( "[" )
         << std::setw( 1 ) << std::left << severityToStringShort( record.getSeverity() ) << PLOG_NSTR( " " )
         << record.getTid() << PLOG_NSTR( " " );
#ifdef PLOG_CAPTURE_FILE
      util::nchar* filename = const_cast<util::nchar*>( record.getFile() );
      util::nchar* slash    = const_cast<util::nchar*>( std::strrchr( filename, '/' ) );
      if( slash ) {
         filename = slash + 1;
      }
      ss << filename << PLOG_NSTR( ":" ) << record.getLine() << PLOG_NSTR( " " )
         << record.getFunc()
#else
      //ss << record.getFunc() << PLOG_NSTR( "@" ) << record.getLine()
      ss
#endif
         << PLOG_NSTR( "] " )
         << record.getMessage() << PLOG_NSTR( "\n" );
      return ss.str();
   }
};

template <class Formatter>
class IvxColorConsoleAppender : public ConsoleAppender<Formatter> {
public:
#ifdef _WIN32
   IvxColorConsoleAppender()
       : m_originalAttr()
   {
      if( this->m_isatty ) {
         CONSOLE_SCREEN_BUFFER_INFO csbiInfo;
         GetConsoleScreenBufferInfo( this->m_stdoutHandle, &csbiInfo );

         m_originalAttr = csbiInfo.wAttributes;
      }
   }
#else
   IvxColorConsoleAppender() = default;
#endif

   void write( const Record& record ) override
   {
      util::nstring str = Formatter::format( record );
      util::MutexLock lock( this->m_mutex );

      setColor( record.getSeverity() );
      this->writestr( str );
      resetColor();
   }

private:
   void setColor( Severity severity )
   {
      if( this->m_isatty ) {
         switch( severity ) {
#ifdef _WIN32
         case fatal:
            SetConsoleTextAttribute( this->m_stdoutHandle, foreground::kRed | foreground::kGreen | foreground::kBlue | foreground::kIntensity | background::kRed ); // white on red background
            break;
         case error:
            SetConsoleTextAttribute( this->m_stdoutHandle, static_cast<WORD>( foreground::kRed | foreground::kIntensity | ( m_originalAttr & 0xf0 ) ) ); // red
            break;
         case warning:
            SetConsoleTextAttribute( this->m_stdoutHandle, static_cast<WORD>( foreground::kRed | foreground::kGreen | foreground::kIntensity | ( m_originalAttr & 0xf0 ) ) ); // yellow
            break;
         case info:
            SetConsoleTextAttribute( this->m_stdoutHandle, static_cast<WORD>( foreground::kGreen | foreground::kIntensity | ( m_originalAttr & 0xf0 ) ) ); // green
            break;
         case debug:
            SetConsoleTextAttribute( this->m_stdoutHandle, static_cast<WORD>( foreground::kRed | foreground::kGreen | foreground::kBlue | foreground::kIntensity | ( m_originalAttr & 0xf0 ) ) ); // white
            break;
         case verbose:
            SetConsoleTextAttribute( this->m_stdoutHandle, static_cast<WORD>( foreground::kIntensity | ( m_originalAttr & 0xf0 ) ) ); // gray
            break;
#else
         case fatal:
            std::cerr << "\x1B[97m\x1B[41m"; // white on red background
            break;
         case error:
            std::cerr << "\x1B[91m"; // red
            break;
         case warning:
            std::cout << "\x1B[93m"; // yellow
            break;
         case info:
            std::cout << "\x1B[92m"; // green
            break;
         case debug:
            std::cout << "\x1B[0m"; // white
            break;
         case verbose:
            std::cout << "\x1B[90m"; // gray
            break;
#endif
         default:
            break;
         }
      }
   }

   void resetColor()
   {
      if( this->m_isatty ) {
#ifdef _WIN32
         SetConsoleTextAttribute( this->m_stdoutHandle, m_originalAttr );
#else
         std::cout << "\x1B[0m\x1B[0K";
#endif
      }
   }

private:
#ifdef _WIN32
   WORD m_originalAttr;
#endif
};
} // namespace plog

namespace BCCL {

class PlogHelper {
public:
   static std::string cFormatToStdString( std::string format, ... );
   static void initDisplayConsole( plog::Severity log_level = plog::debug );
   static void initWriteToFile(plog::Severity log_level = plog::info, const std::string &filename = nullptr, int count_of_rolling_files = 10 );
   static void initDisplayAndWriteToFile( plog::Severity log_level = plog::info, const std::string& filename = nullptr, int count_of_rolling_files = 10 );

private:
   static void removeLastEndLine( std::string& str );
   const static std::string m_logFileName;
};
} // namespace BCCL

#endif // BCCL_PLOG_H

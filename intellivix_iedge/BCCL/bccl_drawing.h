#if !defined(__BCCL_DRAWING_H)
#define __BCCL_DRAWING_H

#include "bccl_misc.h"

 namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// Structure: PolygonEdge
//
///////////////////////////////////////////////////////////////////////////////

 struct PolygonEdge

{
   int MinY,MaxY;
   int A,B,D,F,I,X;
   PolygonEdge *Prev;
   PolygonEdge *Next;
};

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

int ClipLine (int si_width,int si_height,int &x1,int &y1,int &x2,int &y2);
extern "C" int Compare_PolygonEdge_Ascending (const void *a,const void *b);

///////////////////////////////////////////////////////////////////////////////
//
// Function Templates
//
///////////////////////////////////////////////////////////////////////////////

 template<class T>
 void DrawFilledCircle (int cx,int cy,int diameter,T color,Array2D<T> &d_array)
 
{
   int x = 0; 
   int y = diameter >> 1;
   int d = 3 - diameter;
   while (x <= y) {
      DrawHorizontalLine (cx - y,cx + y,cy + x,color,d_array); 
      if (x > 0) DrawHorizontalLine (cx - y,cx + y,cy - x ,color,d_array);
      if (d < 0) d += (x << 2) + 6; 
      else {
         d += ((x - y) << 2) + 10; 
         if (x != y) {
            DrawHorizontalLine (cx - x,cx + x,cy - y,color,d_array); 
            DrawHorizontalLine (cx - x,cx + x,cy + y,color,d_array); 
         }
         y--;
      } 
      x++;
   }
}

 template<class T>
 void DrawFilledPolygon (IPoint2D *s_points,int n_points,T color,Array2D<T> &d_array)
 
{
   int i,j,n,x,y;

   IP2DArray1D vertices(n_points + 1);
   for (i = 0; i < vertices.Length; i++) {
      j = i % n_points;
      vertices[i].X = s_points[j].X;
      vertices[i].Y = s_points[j].Y;
   }
   // Constructing edge elements.
   int min_y = vertices[0].Y;
   int max_y = min_y;
   Array1D<PolygonEdge> edges(n_points);
   for (i = 0; i < n_points; i++) {
      int x1 = vertices[i].X;
      int y1 = vertices[i].Y;
      int x2 = vertices[i + 1].X;
      int y2 = vertices[i + 1].Y;
      if (min_y > y1) min_y = y1;
      else if (max_y < y1) max_y = y1;
      if (y1 == y2) {
         edges[i].MinY = y1;
         edges[i].D    = 0;
      }
      else {
         if (y1 < y2) {
            edges[i].MinY = y1;
            edges[i].MaxY = y2;
            edges[i].X    = x1;
            edges[i].D    = y2 - y1;
            n             = x2 - x1;
         }
         else {
            edges[i].MinY = y2;
            edges[i].MaxY = y1;
            edges[i].X    = x2;
            edges[i].D    = y1 - y2;
            n             = x1 - x2;
         }
         edges[i].A = n / edges[i].D;
         edges[i].B = n - edges[i].A * edges[i].D;
         edges[i].I = 0;
         if (n < 0) {
            edges[i].B = -edges[i].B;
            edges[i].F = -1;
         }
         else if (n > 0) edges[i].F = 1;
         else edges[i].F = 0;
      }
   }
   // Building an ET (Edge Table).
   qsort (edges,edges.Length,sizeof(PolygonEdge),Compare_PolygonEdge_Ascending);
   PolygonEdge *l_edge = NULL;
   Array1D<PolygonEdge*> e_table(n_points);
   e_table.Clear (   );
   for (i = j = 0; i < edges.Length;   ) {
      if (edges[i].D) {
         if (e_table[j] == NULL) {
            e_table[j] = (PolygonEdge*)edges + i;
            l_edge = (PolygonEdge*)edges + i;
            l_edge->Prev = l_edge->Next = NULL;
         }
         else {
            if (l_edge->MinY == edges[i].MinY) {
               edges[i].Prev = l_edge;
               edges[i].Next = l_edge->Next;
               l_edge->Next  = (PolygonEdge*)edges + i;
               l_edge        = (PolygonEdge*)edges + i;
            }
            else {
               j++;
               continue;
            }
         }
      }
      i++;
   }
   PolygonEdge *ae_table = e_table[0];
   if (!ae_table) return;
   ae_table->Prev = NULL;
   int ex = d_array.Width  - 1;
   int ey = d_array.Height - 1;
   IArray1D ix(n_points);
   for (y = min_y,j = 1; y <= max_y; y++) {
      // Searching for intersections of edges with a scan line.
      PolygonEdge *c_edge = ae_table;
      for (n = 0;   ;   ) {
         ix[n++] = c_edge->X;
         if (c_edge->Next == NULL) break;
         else c_edge = c_edge->Next;
      }
      qsort (ix,n,sizeof(int),Compare_Int_Ascending);
      n--;
      // Filling one line.
      T *_RESTRICT l_d_array = d_array[y];
      if (0 <= y && y <= ey) {
         int flag_fill = FALSE;
         for (i = 0; i < n; i++) {
            flag_fill = 1 - flag_fill;
            int x1 = ix[i];
            int x2 = ix[i + 1];
            if (flag_fill) {
               if (x1 < 0 ) x1 = 0;
               if (x2 > ex) x2 = ex;
               for (x = x1; x <= x2; x++) l_d_array[x] = color;
            }
         }
      }
      // Adding new edge elements to the AET (Active Edge Table).
      if (e_table[j] != NULL) {
         if (y == e_table[j]->MinY) {
            l_edge = ae_table;
            for (   ;   ;   ) {
               if (l_edge->Next == NULL) break;
               else l_edge = l_edge->Next;
            }
            e_table[j]->Prev = l_edge;
            l_edge->Next = e_table[j];
            j++;
         }
      }
      // Eleminating obsolete edge elements from the AET.
      c_edge = ae_table;
      for (   ;   ;   ) {
         if (c_edge->MaxY == y) {
            if (c_edge->Next == NULL) {
               if (c_edge->Prev == NULL) {
                  ae_table = NULL;
                  break;
               }
               else c_edge->Prev->Next = NULL;
            }
            else if (c_edge->Prev == NULL) {
               ae_table = c_edge->Next;
               ae_table->Prev = NULL;
            }
            else {
               c_edge->Prev->Next = c_edge->Next;
               c_edge->Next->Prev = c_edge->Prev;
            }
         }
         if (c_edge->Next == NULL) break;
         else c_edge = c_edge->Next;
      }
      // Updating x's.
      c_edge = ae_table;
      if (c_edge == NULL) break;
      for (   ;   ;   ) {
         c_edge->X += c_edge->A;
         c_edge->I += c_edge->B;
         if (c_edge->I > c_edge->D) {
            c_edge->X += c_edge->F;
            c_edge->I -= c_edge->D;
         }
         if (c_edge->Next == NULL) break;
         else c_edge = c_edge->Next;
      }
   }
}

 template<class T>
 void DrawFilledRectangle (IBox2D &s_rgn,T color,Array2D<T> &d_array)
 
{
   int x,y;
   
   int sx = s_rgn.X;
   int sy = s_rgn.Y;
   int ex = sx + s_rgn.Width;
   int ey = sy + s_rgn.Height;
   if (sx >= d_array.Width || sy >= d_array.Height) return;
   if (ex < 0 || ey < 0) return;
   if (sx < 0) sx = 0;
   if (sy < 0) sy = 0;
   if (ex > d_array.Width ) ex = d_array.Width;
   if (ey > d_array.Height) ey = d_array.Height;
   for (y = sy; y < ey; y++) {
      T *_RESTRICT l_d_array = d_array[y];
      for (x = sx; x < ex; x++) l_d_array[x] = color;
   }
}

 template<class T>
 void DrawHorizontalLine (int x1,int x2,int y,T color,Array2D<T> &d_array)

{
   int x;
   
   if (y < 0 || y >= d_array.Height) return;
   if (x1 > x2) Swap (x1,x2);
   if (x2 < 0 || x1 >= d_array.Width) return;
   if (x1 < 0) x1 = 0;
   if (x2 >= d_array.Width) x2 = d_array.Width - 1;
   T *_RESTRICT l_d_array = d_array[y];
   for (x = x1; x <= x2; x++) l_d_array[x] = color;
}

 template<class T>
 void DrawLine (T *si_buffer,int si_width,int si_height,int nb_scanline,int x1,int y1,int x2,int y2,T color)  

{  
   int i,x,y;
   int dx,dy,dx1,dy1;
   int px,py,xe,ye;
   
   if (ClipLine (si_width,si_height,x1,y1,x2,y2)) return;
   dx  = x2 - x1;
   dy  = y2 - y1;  
   dx1 = abs (dx);  
   dy1 = abs (dy);
   px  = 2 * dy1 - dx1;  
   py  = 2 * dx1 - dy1;  
   if (dy1 <= dx1) {
      if (dx >= 0) {
         x  = x1;
         y  = y1;
         xe = x2;
      }
      else {
         x  = x2;
         y  = y2;
         xe = x1;
      }
      *((T*)((byte*)si_buffer + y * nb_scanline) + x) = color;
      for (i = 0; x < xe; i++) {
         x = x + 1;
         if (px < 0) {
            px = px + 2 * dy1;
         }  
         else {
            if ((dx < 0 && dy < 0) || (dx > 0 && dy > 0)) {
               y = y + 1;
            }  
            else {
               y = y - 1;
            }  
            px = px + 2 * (dy1 - dx1);
         }  
         *((T*)((byte*)si_buffer + y * nb_scanline) + x) = color;
      }  
   }  
   else {
      if (dy >= 0) {
         x  = x1;
         y  = y1;  
         ye = y2;  
      }  
      else {
         x  = x2;
         y  = y2;
         ye = y1;
      }  
      *((T*)((byte*)si_buffer + y * nb_scanline) + x) = color;
      for (i = 0; y < ye; i++) {
         y = y + 1;
         if (py <= 0) {
            py = py + 2 * dx1;
         }  
         else {
            if ((dx <0 && dy < 0) || (dx > 0 && dy > 0)) {
               x = x + 1;
            }
            else {
               x = x - 1;  
            }  
            py = py + 2 * (dx1 - dy1);
         }  
         *((T*)((byte*)si_buffer + y * nb_scanline) + x) = color;
      }  
   }  
} 

 template<class T>
 void DrawRectangle (IBox2D &s_rgn,T color,Array2D<T> &d_array)
 
{
   int x,y;
   int flag_left   = TRUE;
   int flag_right  = TRUE;
   int flag_top    = TRUE;
   int flag_bottom = TRUE;
   int sx = s_rgn.X;
   int sy = s_rgn.Y;
   int ex = sx + s_rgn.Width  - 1;
   int ey = sy + s_rgn.Height - 1;
   if (sx >= d_array.Width) return;
   else if (sx < 0) {
      sx = 0;
      flag_left = FALSE;
   }
   if (sy >= d_array.Height) return;
   else if (sy < 0) {
      sy = 0;
      flag_top = FALSE;
   }
   if (ex < 0) return;
   else if (ex >= d_array.Width) {
      ex = d_array.Width - 1;
      flag_right = FALSE;
   }
   if (ey < 0) return;
   else if (ey >= d_array.Height) {
      ey = d_array.Height - 1;
      flag_bottom = FALSE;
   }
   if (flag_left) {
      for (y = sy; y <= ey; y++)
         d_array[y][sx] = color;
   }
   if (flag_right) {
      for (y = sy; y <= ey; y++)
         d_array[y][ex] = color;
   }
   if (flag_top) {
      T* l_d_array = d_array[sy];
      for (x = sx; x <= ex; x++)
         l_d_array[x] = color;
   }
   if (flag_bottom) {
      T* l_d_array = d_array[ey];
      for (x = sx; x <= ex; x++)
         l_d_array[x] = color;
   }
}

 template<class T>
 void DrawVerticalLine (int x,int y1,int y2,T color,Array2D<T> &d_array)

{
   int y;
   
   if (x < 0 || x >= d_array.Width) return;
   if (y1 > y2) Swap (y1,y2);
   if (y2 < 0 || y1 >= d_array.Height) return;
   if (y1 < 0) y1 = 0;
   if (y2 >= d_array.Height) y2 = d_array.Height - 1;
   for (y = y1; y <= y2; y++) d_array[y][x] = color;
}

}

#endif

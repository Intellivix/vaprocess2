﻿#include "bccl_define_linux.h"

#ifndef _MSC_VER

/////////////////////////////////////////////////////////////////////////////////////
#if defined( __CPP11 )
#elif defined( _POSIX_THREADS )

#include <pthread.h>

CCriticalSection::CCriticalSection()
{
   // recursive
   pthread_mutexattr_t attr;
   pthread_mutexattr_init( &attr );
   pthread_mutexattr_settype( &attr, PTHREAD_MUTEX_RECURSIVE_NP );
   pthread_mutex_init( &Mutex, &attr );
   // Not recursive
   //pthread_mutex_init (&Mutex,NULL);
}

CCriticalSection::~CCriticalSection()
{
   pthread_mutex_destroy( &Mutex );
}

void CCriticalSection::Lock()
{
   pthread_mutex_lock( &Mutex );
}

void CCriticalSection::Unlock()
{
   pthread_mutex_unlock( &Mutex );
}
#else
#error neither c++11 nor pthread
#endif // _POSIX_THREADS

/////////////////////////////////////////////////////////////////////////////////////
CEvent::CEvent()
{
   sem_t* HandlePtr = &Handle;
   sem_init( HandlePtr, 0, 0 );
}
CEvent::~CEvent()
{
   sem_destroy( &Handle );
}

void CEvent::SetEvent()
{
   sem_t* HandlePtr = &Handle;
   sem_post( HandlePtr );
}

void CEvent::ResetEvent()
{
   sem_destroy( &Handle );
   sem_init( &Handle, 0, 0 );
}

CEvent::operator HANDLE() const
{
   return (HANDLE)&Handle;
}
/////////////////////////////////////////////////////////////////////////////////////
DWORD WaitForSingleObject( HANDLE pSem, uint32_t nTimeout )
{
   sem_t* HandlePtr = (sem_t*)pSem;
   if( nTimeout == UINT_MAX ) {
      if( sem_wait( HandlePtr ) != 0 )
         return WAIT_FAILED;
   } else {
      if( nTimeout == 0 ) nTimeout = 1;

      struct timespec timeOut;
      ::clock_gettime( CLOCK_REALTIME, &timeOut );
      timeOut.tv_sec += nTimeout / 1000;
      timeOut.tv_nsec += ( nTimeout % 1000 ) * 1000000;
      if( timeOut.tv_nsec > 1000000000 ) {
         timeOut.tv_sec++;
         timeOut.tv_nsec -= 1000000000;
      }
      if( sem_timedwait( HandlePtr, &timeOut ) != 0 ) {
         if( errno == ETIMEDOUT )
            return WAIT_TIMEOUT;
         else
            return WAIT_FAILED;
      }
   }
   return WAIT_OBJECT_0;
}
/////////////////////////////////////////////////////////////////////////////////////
CWinThread::CWinThread()
{
   m_hThread       = NULL;
   m_pfnThreadProc = NULL;
   m_pThreadParams = NULL;
   m_hRealThread   = 0;
}

CWinThread::~CWinThread()
{
}

DWORD CWinThread::ResumeThread()
{
   int thr_id = pthread_create( &m_hRealThread, NULL, ThreadProc, (LPVOID)this );

   if( thr_id < 0 )
      return 1;

   return ( 0 );
}

LPVOID CWinThread::ThreadProc( LPVOID pVoid )
{
   CWinThread* pThread = (CWinThread*)pVoid;
   pThread->m_pfnThreadProc( pThread->m_pThreadParams );
   pThread->m_evtThread.SetEvent();
   delete pThread;
   return (LPVOID)0;
}

CWinThread* AfxBeginThread( AFX_THREADPROC pfnThreadProc, LPVOID pParam, int nPriority, UINT nStackSize, DWORD dwCreateFlags )
{
   CWinThread* pThread = new CWinThread();
   if( pThread ) {
      pThread->m_pfnThreadProc = pfnThreadProc;
      pThread->m_pThreadParams = pParam;
      pThread->m_hThread       = (HANDLE)pThread->m_evtThread;
   }
   return pThread;
}

#endif // ! _MSC_VER

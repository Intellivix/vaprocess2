#include "bccl_console.h"
#include "bccl_file.h"
#include "bccl_nrc.h"

 namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: Matrix
//
///////////////////////////////////////////////////////////////////////////////
 
 Matrix::Matrix (   )

{
   _Init (   );
}

 Matrix::Matrix (int n_rows)

{
   _Init (   );
   Create (n_rows);
}

 Matrix::Matrix (int n_rows,int n_cols)

{
   _Init (   );
   Create (n_rows,n_cols);
}

 Matrix::Matrix (double *s_buffer,int n_rows,int n_cols)

{
   _Init (   );
   Import (s_buffer,n_rows,n_cols);
}

 Matrix::Matrix (double **s_array,int n_rows,int n_cols)

{
   _Init (   );
   Import (s_array,n_rows,n_cols);
}

 Matrix::Matrix (const FPoint2D &s_point)

{
   _Init (   );
   Create (2);
   Array[0][0] = s_point.X;
   Array[1][0] = s_point.Y;
}

 Matrix::Matrix (const FPoint3D &s_point)

{
   _Init (   );
   Create (3);
   Array[0][0] = s_point.X;
   Array[1][0] = s_point.Y;
   Array[2][0] = s_point.Z;
}

 Matrix::Matrix (const Matrix &mat_S)

{
   _Init (   );
   Create (mat_S.NRows,mat_S.NCols);
   Copy (mat_S,0,0,NRows,NCols,0,0);
}

 Matrix::Matrix (Matrix *mat_S)

{
   NRows  = mat_S->NRows;
   NCols  = mat_S->NCols;
   Array  = mat_S->Array;
   Buffer = mat_S->Buffer;
   mat_S->Array  = NULL;
   mat_S->Buffer = NULL;
   mat_S->NRows  = mat_S->NCols = 0;
}

 Matrix::~Matrix (   )

{
   Delete (   );
}

 Matrix& Matrix::operator = (const FPoint2D &s_point)

{
   if (NRows != 2 || NCols != 1) Create (2);
   Array[0][0] = s_point.X;
   Array[1][0] = s_point.Y;
   return (*this);   
}

 Matrix& Matrix::operator = (const FPoint3D &s_point)

{
   if (NRows != 3 || NCols != 1) Create (3);
   Array[0][0] = s_point.X;
   Array[1][0] = s_point.Y;
   Array[2][0] = s_point.Z;
   return (*this);   
}

 Matrix& Matrix::operator = (const Matrix &mat_S)

{
   if (NRows != mat_S.NRows || NCols != mat_S.NCols) Create (mat_S.NRows,mat_S.NCols);
   Copy (mat_S,0,0,NRows,NCols,0,0);
   return (*this);
}

 Matrix Matrix::operator + (const Matrix &mat_S)

{
   int i,j;

   Matrix mat_D(NRows,NCols);
   for (i = 0; i < NRows; i++) {
      double *_RESTRICT l_Array = Array[i];
      double *_RESTRICT l_mat_S = mat_S[i];
      double *_RESTRICT l_mat_D = mat_D[i];
      for (j = 0; j < NCols; j++)
         l_mat_D[j] = l_Array[j] + l_mat_S[j];
   }   
   return (Matrix (&mat_D));
}

 double Matrix::operator + (double a)

{
   return (Array[0][0] + a);
}

 double operator + (double a,const Matrix &mat_S)

{
   return (a + mat_S[0][0]);
}

 Matrix Matrix::operator - (   )

{
   int i,j;

   Matrix mat_D(NRows,NCols);
   for (i = 0; i < NRows; i++) {
      double *_RESTRICT l_Array = Array[i];
      double *_RESTRICT l_mat_D = mat_D[i];
      for (j = 0; j < NCols; j++)
         l_mat_D[j] = -l_Array[j];
   }   
   return (Matrix (&mat_D));
}

 Matrix Matrix::operator - (const Matrix &mat_S)

{
   int i,j;

   Matrix mat_D(NRows,NCols);
   for (i = 0; i < NRows; i++) {
      double *_RESTRICT l_Array = Array[i];
      double *_RESTRICT l_mat_S = mat_S[i];
      double *_RESTRICT l_mat_D = mat_D[i];
      for (j = 0; j < NCols; j++)
         l_mat_D[j] = l_Array[j] - l_mat_S[j];
   }   
   return (Matrix (&mat_D));
}

 double Matrix::operator - (double a)

{
   return (Array[0][0] - a);
}

 double operator - (double a,const Matrix &mat_S)

{
   return (a - mat_S[0][0]);
}

 Matrix Matrix::operator * (const Matrix &mat_S)

{
   int i,j,k;
   double sum;

   Matrix mat_D;
   if (NRows == 1 && NCols == 1) {
      mat_D.Create (mat_S.NRows,mat_S.NCols);
      double a = Array[0][0];
      for (i = 0; i < mat_D.NRows; i++) {
         double *_RESTRICT l_mat_S = mat_S[i];
         double *_RESTRICT l_mat_D = mat_D[i];      
         for (j = 0; j < mat_D.NCols; j++)
            l_mat_D[j] = a * l_mat_S[j];
      }   
   }
   else if (mat_S.NRows == 1 && mat_S.NCols == 1) {
      mat_D.Create (NRows,NCols);
      double a = mat_S[0][0];
      for (i = 0; i < mat_D.NRows; i++) {
         double *_RESTRICT l_Array = Array[i];
         double *_RESTRICT l_mat_D = mat_D[i];
         for (j = 0; j < mat_D.NCols; j++)
            l_mat_D[j] = a * l_Array[j];
      }   
   }
   else {
      mat_D.Create (NRows,mat_S.NCols);
      for (k = 0; k < mat_D.NRows; k++) {
         double *_RESTRICT l_Array = Array[k];
         double *_RESTRICT l_mat_D = mat_D[k];
         for (j = 0; j < mat_D.NCols; j++) {
            for (i = 0,sum = 0.0; i < NCols; i++) sum += l_Array[i] * mat_S[i][j];
            l_mat_D[j] = sum;
         }
      }
   }
   return (Matrix (&mat_D));
}

 Matrix Matrix::operator * (double a)

{
   int i,j;

   Matrix mat_D(NRows,NCols);
   for (i = 0; i < NRows; i++) {
      double *_RESTRICT l_Array = Array[i];
      double *_RESTRICT l_mat_D = mat_D[i];
      for (j = 0; j < NCols; j++)
         l_mat_D[j] = l_Array[j] * a;
   }   
   return (Matrix (&mat_D));
}

 Matrix operator * (double a,const Matrix &mat_S)

{
   int i,j;

   Matrix mat_D(mat_S.NRows,mat_S.NCols);
   for (i = 0; i < mat_S.NRows; i++) {
      double *_RESTRICT l_mat_S = mat_S[i];
      double *_RESTRICT l_mat_D = mat_D[i];
      for (j = 0; j < mat_S.NCols; j++)
         l_mat_D[j] = l_mat_S[j] * a;
   }   
   return (Matrix (&mat_D));
}

 Matrix Matrix::operator / (double a)

{
   int i,j;

   Matrix mat_D(NRows,NCols);
   for (i = 0; i < NRows; i++) {
      double *_RESTRICT l_Array = Array[i];
      double *_RESTRICT l_mat_D = mat_D[i];
      for (j = 0; j < NCols; j++)
         l_mat_D[j] = l_Array[j] / a;
   }   
   return (Matrix (&mat_D));
}

 Matrix& Matrix::operator += (const Matrix &mat_S)

{
   int i,j;

   for (i = 0; i < NRows; i++) {
      double *_RESTRICT l_Array = Array[i];
      double *_RESTRICT l_mat_S = mat_S[i];
      for (j = 0; j < NCols; j++)
         l_Array[j] += l_mat_S[j];
   }   
   return (*this);
}

 Matrix& Matrix::operator -= (const Matrix &mat_S)

{
   int i,j;

   for (i = 0; i < NRows; i++) {
      double *_RESTRICT l_Array = Array[i];
      double *_RESTRICT l_mat_S = mat_S[i];
      for (j = 0; j < NCols; j++)
         l_Array[j] -= l_mat_S[j];
   }   
   return (*this);
}

 Matrix& Matrix::operator *= (const Matrix &mat_S)

{
   *this = *this * mat_S;
   return (*this);
}

 Matrix& Matrix::operator *= (double a)

{
   int i,j;

   for (i = 0; i < NRows; i++) {
      double *_RESTRICT l_Array = Array[i];
      for (j = 0; j < NCols; j++)
         l_Array[j] *= a;
   }   
   return (*this);
}

 Matrix& Matrix::operator /= (double a)

{
   int i,j;

   for (i = 0; i < NRows; i++) {
      double *_RESTRICT l_Array = Array[i];
      for (j = 0; j < NCols; j++)
         l_Array[j] /= a;
   }   
   return (*this);
}

 Matrix::operator FPoint2D (   ) const

{
   return (FPoint2D ((float)Array[0][0],(float)Array[1][0]));
}

 Matrix::operator FPoint3D (   ) const

{
   return (FPoint3D ((float)Array[0][0],(float)Array[1][0],(float)Array[2][0]));
}

 void Matrix::_Init (   )
 
{
   Array  = NULL;
   Buffer = NULL;
   NRows  = 0;
   NCols  = 0;
}

 void Matrix::Clear (   )

{
   ClearArray2D ((byte **)Array,NCols,NRows,sizeof(double));
}

 void Matrix::Copy (const Matrix &mat_S,int si,int sj,int n_rows,int n_cols,int di,int dj)

{
   int i,j,k,l,ei,ej;

   ei = si + n_rows;
   ej = sj + n_cols;
   for (i = si,k = di; i < ei; i++,k++) {
      double *_RESTRICT l_Array = Array[k];
      double *_RESTRICT l_mat_S = mat_S[i];
      for (j = sj,l = dj; j < ej; j++,l++)
         l_Array[l] = l_mat_S[j];
   }  
}

 int Matrix::Create (int n_rows)

{
   return (Create (n_rows,1));
}
 
 int Matrix::Create (int n_rows,int n_cols)

{
   int i,j;

   if (NRows == n_rows && NCols == n_cols) return (DONE);
   if (Array  != NULL) DeleteArray1D ((byte*)Array);
   if (Buffer != NULL) DeleteArray1D ((byte*)Buffer);
   Array  = NULL;
   Buffer = NULL;
   NRows  = n_rows;
   NCols  = n_cols;
   if (NRows && NCols) {
      Array = (double**)CreateArray1D (NRows,sizeof(double*));
      if (Array == NULL)  return (1);
      Buffer = (double*)CreateArray1D (NRows * NCols,sizeof(double));
      if (Buffer == NULL) return (2);
      for (i = j = 0; i < NRows; i++,j += NCols) Array[i] = Buffer + j;
   }
   return (DONE);
}

 void Matrix::Delete (   )

{
   if (Array  != NULL) DeleteArray1D ((byte*)Array);
   if (Buffer != NULL) DeleteArray1D ((byte*)Buffer);
   _Init (   );
}

 Matrix Matrix::Extract (int si,int sj,int n_rows,int n_cols)

{
   Matrix mat_D(n_rows,n_cols);
   mat_D.Copy (*this,si,sj,n_rows,n_cols,0,0);
   return (Matrix (&mat_D));
}

 Matrix Matrix::GetColumnVector (int col)

{
   int i;

   Matrix vec_d(NRows);
   for (i = 0; i < NRows; i++) vec_d(i) = Array[i][col];
   return (Matrix (&vec_d));
}

 Matrix Matrix::GetMatrix (int n_rows)

{
   int i;
   
   int length = NRows * NCols;
   Matrix mat_D(n_rows,length / n_rows);
   double *_RESTRICT l_Array = Array[0];
   for (i = 0; i < length; i++) mat_D(i) = l_Array[i];
   return (Matrix (&mat_D));
}

 Matrix Matrix::GetRowVector (int row)

{
   int i;

   Matrix vec_d(NCols);
   double *_RESTRICT l_Array = Array[row];   
   for (i = 0; i < NCols; i++) vec_d(i) = l_Array[i];
   return (Matrix (&vec_d));
}

 Matrix Matrix::GetVector (   )

{
   int i;

   int length = NRows * NCols;
   Matrix vec_d(length);
   double *_RESTRICT l_Array = Array[0];
   for (i = 0; i < length; i++) vec_d(i) = l_Array[i];
   return (Matrix (&vec_d));
}

 Matrix& Matrix::Identity (   )

{
   int i;

   Clear (   );
   for (i = 0; i < NRows; i++) Array[i][i] = 1.0;
   return (*this);
}
 
 int Matrix::Import (double* s_buffer,int n_rows,int n_cols)
 
{
   int i,j;

   if (s_buffer == NULL) {
      if (Array != NULL) {
         DeleteArray1D ((byte*)Array);
         Array = NULL;
      }      
   }   
   Buffer = s_buffer;
   NRows  = n_rows;
   NCols  = n_cols;
   if (Buffer != NULL && NRows && NCols) {
      Array = (double**)CreateArray1D (NRows,sizeof(double*));
      if (Array == NULL) return (1);
      for (i = j = 0; i < NRows; i++,j += NCols) Array[i] = Buffer + j;
   }
   return (DONE);
}
 
 int Matrix::Import (double **s_array,int n_rows,int n_cols)

{
   Array  = s_array;
   Buffer = NULL;
   NRows  = n_rows;
   NCols  = n_cols;
   if (Array != NULL) Buffer = Array[0];
   return (DONE);
}

 void Matrix::Print (const char *name)

{
   int i,j;
   
   logd ("Matrix: %s (NRows = %d, NCols = %d)\n",name,NRows,NCols);
   for (i = 0; i < NRows; i++) {
      double *_RESTRICT l_Array = Array[i];
      for (j = 0; j < NCols; j++) if (l_Array[j] >= 0) logd (" %E ",l_Array[j]);
      else logd ("%E ",l_Array[j]);
      logd ("\n");
   }
}

 int Matrix::ReadFile (const char *file_name)

{
   int i,j;
   int n_cols;
   byte c;
   double v;
   
   FileIO file(file_name,FIO_BINARY,FIO_READ);
   if (!file) return (1);
   for (i = 0;   ;   ) {
      if (file.ReadByte (c)) break;
      else if (c == 0x0A) break;
      else i++;
   }
   file.Close (   );
   file.Open  (file_name,FIO_BINARY,FIO_READ);
   for (n_cols = 0;   ;   ) {
      if (file.Scan ("%E",&v)) break;
      else if (file.Where (   ) > i) break;
      n_cols++;
   }
   file.Close (   );
   file.Open  (file_name,FIO_TEXT,FIO_READ);
   for (i = 0;   ;   ) {
      if (file.Scan ("%E",&v)) break;
      else i++;
   }
   if (i % n_cols) return (2);
   file.Close (   );
   file.Open (file_name,FIO_TEXT,FIO_READ);
   Create (i / n_cols,n_cols);
   for (i = 0; i < NRows; i++) {
      for (j = 0; j < NCols; j++) {
         file.Scan ("%E",&v);
         Array[i][j] = v;
      }
   }
   return (DONE);
}

 int Matrix::ReadFile (CXMLIO* pIO)

{
   int i;
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Matrix"))
   {
      xmlNode.Attribute (TYPE_ID (int), "NRows", &NRows);
      xmlNode.Attribute (TYPE_ID (int), "NCols", &NCols);
      int array_length = NRows * NCols;
      double* array = new double[array_length];
      xmlNode.Attribute (TYPE_ID (double), "Array", array, array_length);
      for (i = 0; i < NRows; i++) {
         memcpy (Array[i], array + NCols * i, sizeof(double)*NCols);
      }
      delete [] array;
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

 void Matrix::Set (int si,int sj,int n_rows,int n_cols,double value)

{
   int i,j,ei,ej;

   ei = si + n_rows;
   ej = sj + n_cols;
   for (i = si; i < ei; i++) {
      double *_RESTRICT l_Array = Array[i];
      for (j = sj; j < ej; j++)
         l_Array[j] = value;
   }  
}

 void Matrix::SetColumnVector (Matrix &vec_s,int col)

{
   int i;
   
   for (i = 0; i < NRows; i++) Array[i][col] = vec_s(i);
}

 void Matrix::SetRowVector (Matrix &vec_s,int row)

{
   int i;

   double *_RESTRICT l_Array = Array[row];
   for (i = 0; i < NCols; i++) l_Array[i] = vec_s(i);
}

 int Matrix::WriteFile (const char *file_name)

{
   int i,j;

   FileIO file(file_name,FIO_TEXT,FIO_CREATE);
   if (!file) return (1);
   for (i = 0; i < NRows; i++) {
      for (j = 0; j < NCols; j++) if (file.Print ("%14E ",Array[i][j])) return (2);
      if (file.Print ("\n")) return (2);
   }
   return (DONE);
}

 int Matrix::WriteFile (CXMLIO* pIO)

{
   int i;
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Matrix"))
   {
      xmlNode.Attribute (TYPE_ID (int), "NRows", &NRows);
      xmlNode.Attribute (TYPE_ID (int), "NCols", &NCols);
      int array_length = NRows * NCols;
      double* array = new double[array_length];
      for (i = 0; i < NRows; i++) {
         memcpy (array + NCols * i, Array[i], sizeof(double)*NCols);
      }
      xmlNode.Attribute (TYPE_ID (double), "Array", array, array_length);
      delete [] array;
      xmlNode.End ();
      return (DONE);
   }   
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 Matrix CP (const Matrix &vec_s1,const Matrix &vec_s2)
 
{
   Matrix vec_d(3);
   vec_d(0) = vec_s1(1) * vec_s2(2) - vec_s1(2) * vec_s2(1);
   vec_d(1) = vec_s1(2) * vec_s2(0) - vec_s1(0) * vec_s2(2);
   vec_d(2) = vec_s1(0) * vec_s2(1) - vec_s1(1) * vec_s2(0);
   return (Matrix (&vec_d));
}

 Matrix CPM (const Matrix &vec_s)

{
   Matrix mat_D(3,3);
   mat_D[0][0] = mat_D[1][1] = mat_D[2][2] = 0;
   mat_D[0][1] = -vec_s(2);
   mat_D[0][2] =  vec_s(1);
   mat_D[1][2] = -vec_s(0);
   mat_D[1][0] =  vec_s(2);
   mat_D[2][0] = -vec_s(1);
   mat_D[2][1] =  vec_s(0);
   return (Matrix (&mat_D));
}

 double Det (const Matrix &mat_S)
 
{
   int i;
   double d = (double)0.0;

   if (mat_S.NRows == 2) d = mat_S[0][0] * mat_S[1][1] - mat_S[0][1] * mat_S[1][0];
   else {
      Matrix a = mat_S;
      IArray1D indx(mat_S.NRows);
      ludcmp (a,indx,d);
      for (i = 0; i < mat_S.NRows; i++) d *= a[i][i];
   }
   return (d);
}

 Matrix Diag (const Matrix &vec_s)

{
   int i;
   
   Matrix mat_D(vec_s.NRows,vec_s.NRows);
   mat_D.Clear (   );
   for (i = 0; i < vec_s.NRows; i++) mat_D[i][i] = vec_s(i);
   return (Matrix (&mat_D));
}

 void EigenDecomp (const Matrix &mat_S,Matrix &vec_e,Matrix &mat_E)
 
{
   int nrot;
   
   Matrix a = mat_S;
   jacobi (a,vec_e,mat_E,nrot);
   eigsrt (vec_e,mat_E);
}

 Matrix IM (int n_rows)

{
   int i;
   
   Matrix mat_D(n_rows,n_rows);
   mat_D.Clear (   );
   for (i = 0; i < n_rows; i++) mat_D[i][i] = (double)1;
   return (Matrix (&mat_D));
}

 Matrix Inv (const Matrix &mat_S)

{
   int i,j;
   double d;

   Matrix mat_D(mat_S.NRows,mat_S.NRows);
   if (mat_S.NRows == 2) {
      double det = Det(mat_S);
      if (det == (double)0.0) {
         mat_D.Clear (   );
         logd ("Inv( ): Singular matrix.\n");
      }
      else {
         mat_D[0][0] =  mat_S[1][1] / det;
         mat_D[0][1] = -mat_S[0][1] / det;
         mat_D[1][0] = -mat_S[1][0] / det;
         mat_D[1][1] =  mat_S[0][0] / det;
      }
   }
   else {
      Matrix a = mat_S;
      Matrix col(mat_S.NRows);
      IArray1D indx(mat_S.NRows);
      ludcmp (a,indx,d);
      for (j = 0; j < mat_S.NRows; j++) {
         for (i = 0; i < mat_S.NRows; i++) col(i) = (double)0.0;
         col(j) = (double)1.0;
         lubksb (a,indx,col);
         for (i = 0; i < mat_S.NRows; i++) mat_D[i][j] = col(i);
      }
   }
   return (Matrix (&mat_D));
}

 double IP (const Matrix &vec_s1,const Matrix &vec_s2)

{
   int i,j;

   double ip = 0.0;
   for (i = 0; i < vec_s1.NRows; i++) {
      double *_RESTRICT l_vec_s1 = vec_s1[i];
      double *_RESTRICT l_vec_s2 = vec_s2[i];
      for (j = 0; j < vec_s1.NCols; j++) 
         ip += l_vec_s1[j] * l_vec_s2[j];
   }   
   return (ip);
}

 double Mag (const Matrix &vec_s)

{
   int i,j;

   double sm = 0.0;
   for (i = 0; i < vec_s.NRows; i++) {
      double *_RESTRICT l_vec_s = vec_s[i];
      for (j = 0; j < vec_s.NCols; j++)
         sm += l_vec_s[j] * l_vec_s[j];
   }   
   return (sqrt (sm));
}

 Matrix Nor (const Matrix &vec_s)

{
   int i,j;

   Matrix vec_d(vec_s.NRows,vec_s.NCols);
   double mag = Mag(vec_s);
   for (i = 0; i < vec_s.NRows; i++) {
      double *_RESTRICT l_vec_s = vec_s[i];
      double *_RESTRICT l_vec_d = vec_d[i];
      for (j = 0; j < vec_s.NCols; j++)
         l_vec_d[j] = l_vec_s[j] / mag;
   }   
   return (Matrix (&vec_d));
}

 double SquaredMag (const Matrix &vec_s)

{
   int i,j;

   double sm = 0.0;
   for (i = 0; i < vec_s.NRows; i++) {
      double *_RESTRICT l_vec_s = vec_s[i];
      for (j = 0; j < vec_s.NCols; j++)
         sm += l_vec_s[j] * l_vec_s[j];
   }   
   return (sm);
}

 Matrix Tr (const Matrix &mat_S)

{
   int i,j;

   Matrix mat_D(mat_S.NCols,mat_S.NRows);
   for (i = 0; i < mat_S.NRows; i++) {
      double *_RESTRICT l_mat_S = mat_S[i];
      for (j = 0; j < mat_S.NCols; j++)
         mat_D[j][i] = l_mat_S[j];
   }   
   return (Matrix (&mat_D));
}

 double Trace (const Matrix &mat_S)

{
   int i;
   double trace;
   
   for (i = 0,trace = 0.0; i < mat_S.NRows; i++) trace += mat_S[i][i];
   return (trace);
}

}

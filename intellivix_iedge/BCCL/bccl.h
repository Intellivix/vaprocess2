#ifndef __BCCL_H
#define __BCCL_H

#include "bccl_array.h"
#include "bccl_color.h"
#include "bccl_console.h"
#include "bccl_cpoint.h"
#include "bccl_cstring.h"
#include "bccl_ctime.h"
#include "bccl_curl.h"
#include "bccl_define.h"
#include "bccl_drawing.h"
#include "bccl_environ.h"
#include "bccl_ffmpeg.h"
#include "bccl_file.h"
#include "bccl_geomobj.h"
#include "bccl_image.h"
#include "bccl_list.h"
#include "bccl_matrix.h"
#include "bccl_mem.h"
#include "bccl_misc.h"
#include "bccl_nrc.h"
#include "bccl_polygon.h"
#include "bccl_socket.h"
#include "bccl_string.h"
#include "bccl_time.h"
#include "bccl_thread.h"
#include "bccl_xml.h"
#include "bccl_pugixml.h"
#include "bccl_zlib.h"
#include "bccl_plog.h"

using namespace BCCL;

#endif

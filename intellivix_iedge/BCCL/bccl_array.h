﻿#if !defined(__BCCL_ARRAY_H)
#define __BCCL_ARRAY_H

#include "bccl_mem.h"
#include "bccl_geomobj.h"

 namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class Template: Array1D<T>
//
///////////////////////////////////////////////////////////////////////////////

 template<class T>
 class Array1D

{
   protected:
      T *Array;

   public:
      int Length;

   public:
      Array1D (   );
      Array1D (int length);
      Array1D (T *s_array,int length);
      Array1D (const Array1D &s_array);
      Array1D (Array1D *s_array);

   public:
      virtual ~Array1D (   );

   public:
      Array1D&  operator =     (const Array1D &s_array);
            T&  operator []    (char   x) const;
            T&  operator []    (byte   x) const;
            T&  operator []    (short  x) const;
            T&  operator []    (ushort x) const;
            T&  operator []    (int    x) const;
            T&  operator []    (uint   x) const;
            T&  operator []    (long   x) const;
            T&  operator []    (ulong  x) const;
            T&  operator ()    (char   i) const;
            T&  operator ()    (byte   i) const;
            T&  operator ()    (short  i) const;
            T&  operator ()    (ushort i) const;
            T&  operator ()    (int    i) const;
            T&  operator ()    (uint   i) const;
            T&  operator ()    (long   i) const;
            T&  operator ()    (ulong  i) const;
            int operator !        (   ) const;
                operator T*       (   ) const;
                operator const T* (   ) const;
                operator void*    (   ) const;

   protected:
      void _Init (   );
   
   public:
      virtual void Clear  (   );
      virtual int  Create (int length);
      virtual void Delete (   );
      virtual int  Import (T *s_array,int length);
   
   public:
      void    Copy    (const Array1D &s_array,int sx,int length,int dx);
      Array1D Extract (int sx,int length);
      void    Set     (int sx,int length,T value);
};

 template<class T>
 Array1D<T>& Array1D<T>::operator = (const Array1D &s_array)

{
   if (Length != s_array.Length) Create (s_array.Length);
   Copy (s_array,0,Length,0);
   return (*this);
}

 template<class T>
 inline T& Array1D<T>::operator [] (char x) const

{
   return (Array[x]);
}

 template<class T>
 inline T& Array1D<T>::operator [] (byte x) const

{
   return (Array[x]);
}

 template<class T>
 inline T& Array1D<T>::operator [] (short x) const

{
   return (Array[x]);
}

 template<class T>
 inline T& Array1D<T>::operator [] (ushort x) const

{
   return (Array[x]);
}

 template<class T>
 inline T& Array1D<T>::operator [] (int x) const

{
   return (Array[x]);
}

 template<class T>
 inline T& Array1D<T>::operator [] (uint x) const

{
   return (Array[x]);
}

 template<class T>
 inline T& Array1D<T>::operator [] (long x) const

{
   return (Array[x]);
}

 template<class T>
 inline T& Array1D<T>::operator [] (ulong x) const

{
   return (Array[x]);
}

 template<class T>
 inline T& Array1D<T>::operator () (char i) const

{
   return (Array[i]);
}

 template<class T>
 inline T& Array1D<T>::operator () (byte i) const

{
   return (Array[i]);
}

 template<class T>
 inline T& Array1D<T>::operator () (short i) const

{
   return (Array[i]);
}

 template<class T>
 inline T& Array1D<T>::operator () (ushort i) const

{
   return (Array[i]);
}

 template<class T>
 inline T& Array1D<T>::operator () (int i) const

{
   return (Array[i]);
}

 template<class T>
 inline T& Array1D<T>::operator () (uint i) const

{
   return (Array[i]);
}

 template<class T>
 inline T& Array1D<T>::operator () (long i) const

{
   return (Array[i]);
}

 template<class T>
 inline T& Array1D<T>::operator () (ulong i) const

{
   return (Array[i]);
}

 template<class T>
 inline int Array1D<T>::operator ! (   ) const
 
{
   if (Array) return (FALSE);
   else return (TRUE);
}

 template<class T>
 inline Array1D<T>::operator T* (   ) const

{
   return (Array);
}

 template<class T>
 inline Array1D<T>::operator const T* (   ) const
 
{
   return (Array);
}

 template<class T>
 inline Array1D<T>::operator void* (   ) const

{
   return ((void *)Array);
}

 template<class T>
 Array1D<T>::Array1D (   )

{
   _Init (   );
}

 template<class T>
 Array1D<T>::Array1D (int length)

{
   _Init (   );
   Create (length);
}
 
 template<class T>
 Array1D<T>::Array1D (T *s_array,int length)

{
   _Init (   );
   Import (s_array,length);
}

 template<class T>
 Array1D<T>::Array1D (const Array1D &s_array)

{
   _Init (   );
   Create (s_array.Length);
   Copy (s_array,0,Length,0);
}
 
 template<class T>
 Array1D<T>::Array1D (Array1D *s_array)

{
   Length = s_array->Length;
   Array  = s_array->Array;
   s_array->Length = 0;
   s_array->Array  = NULL;
}

 template<class T>
 Array1D<T>::~Array1D (   )

{
   Delete (   );
}

 template<class T>
 void Array1D<T>::_Init (   )
 
{
   Array  = NULL;
   Length = 0;
}

 template<class T>
 void Array1D<T>::Clear (   )

{
   ClearArray1D ((byte *)Array,Length,sizeof(T));
}

 template<class T>
 void Array1D<T>::Copy (const Array1D &s_array,int sx,int length,int dx)

{
   int x1,x2;
 
   int ex = sx + length;
   for (x1 = sx,x2 = dx; x1 < ex; x1++,x2++) Array[x2] = s_array[x1];
}

 template<class T>
 int Array1D<T>::Create (int length)

{
   if (Array != NULL) delete [] Array;
   Array  = NULL;
   Length = length;
   if (Length) {
      Array = new T[Length];
      if (Array == NULL) return (1);
   }
   return (DONE);
}

 template<class T>
 void Array1D<T>::Delete (   )

{
   if (Array == NULL) return;
   delete [] Array;
   _Init (   );
}

 template<class T>
 Array1D<T> Array1D<T>::Extract (int sx,int length)

{
   Array1D<T> d_array(length);
   d_array.Copy (*this,sx,length,0);
   return (Array1D<T>(&d_array));
}

 template<class T>
 int Array1D<T>::Import (T *s_array,int length)

{
   Array  = s_array;
   Length = length;
   return (DONE);
}
 
 template<class T>
 void Array1D<T>::Set (int sx,int length,T value)

{
   int x;
  
   int ex = sx + length;
   for (x = sx; x < ex; x++) Array[x] = value;
}

///////////////////////////////////////////////////////////////////////////////
//
// Function Templates
//
///////////////////////////////////////////////////////////////////////////////

 template <class T,class U>
 void Convert (const Array1D<T> &s_array,Array1D<U> &d_array)

{
   int x;

   for (x = 0; x < s_array.Length; x++) d_array[x] = (U)s_array[x];
}

 template<class T>
 int FindMaximum (const Array1D<T> &s_array,T &max,int &max_x)

{
   int x;

   max   = 0;
   max_x = 0;
   if (!s_array) return (1);
   max = s_array[0];
   for (x = 1; x < s_array.Length; x++)
      if (s_array[x] > max) max = s_array[x], max_x = x;
   return (DONE);
}

 template<class T>
 int FindMinimum (const Array1D<T> &s_array,T &min,int &min_x)

{
   int x;

   min   = 0;
   min_x = 0;
   if (!s_array) return (1);
   min = s_array[0];
   for (x = 1; x < s_array.Length; x++)
      if (s_array[x] < min) min = s_array[x], min_x = x;
   return (DONE);
}

 template<class T>
 int FindMinMax (const Array1D<T> &s_array,T &min,T &max,int &min_x,int &max_x)

{
   int x;

   min   = max   = 0;
   min_x = max_x = 0;
   if (!s_array) return (1);
   min = max = s_array[0];
   for (x = 1; x < s_array.Length; x++) {
      if      (s_array[x] < min) min = s_array[x], min_x = x;
      else if (s_array[x] > max) max = s_array[x], max_x = x;
   }
   return (DONE);
}

 template<class T>
 float GetMean (const Array1D<T> &s_array)
 
{
   int x;
   
   if (!s_array) return (0.0f);
   float sum = 0.0f;
   for (x = 0; x < s_array.Length; x++)
      sum += (float)s_array[x];
   return (sum / s_array.Length);
}

 template<class T>
 float GetMean (const Array1D<T> &s_array,int sx,int length)

{
   int x;
   
   if (!s_array) return (0.0f);
   float sum = 0.0f;
   int ex = sx + length; 
   for (x = sx; x < ex; x++)
      sum += (float)s_array[x];
   return (sum / length);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class Template: Array2D<T>
//
///////////////////////////////////////////////////////////////////////////////

 template<class T>
 class Array2D

{
   protected:
      T **Array;
      T *Buffer;

   public:
      int Width,Height;

   public:
      Array2D (   );
      Array2D (int width,int height);
      Array2D (T *s_buffer,int width,int height);
      Array2D (T **s_array,int width,int height);
      Array2D (const Array2D &s_array);
      Array2D (Array2D *s_array);

   public:
      virtual ~Array2D (   );

   public:
      Array2D&  operator =     (const Array2D &s_array);
            T*  operator []    (char   y) const;
            T*  operator []    (byte   y) const;
            T*  operator []    (short  y) const;
            T*  operator []    (ushort y) const;
            T*  operator []    (int    y) const;
            T*  operator []    (uint   y) const;
            T*  operator []    (long   y) const;
            T*  operator []    (ulong  y) const;
            T&  operator ()    (char   i) const;
            T&  operator ()    (byte   i) const;
            T&  operator ()    (short  i) const;
            T&  operator ()    (ushort i) const;
            T&  operator ()    (int    i) const;
            T&  operator ()    (uint   i) const;
            T&  operator ()    (long   i) const;
            T&  operator ()    (ulong  i) const;
            int operator !     (   ) const;
                operator T*    (   ) const;
                operator T**   (   ) const;
                operator void* (   ) const;

   protected:
      void _Init (   );

   public:
      virtual void Clear  (   );
      virtual int  Create (int width,int height);
      virtual void Delete (   );
      virtual int  Import (T *s_buffer,int width,int height);
      virtual int  Import (T **s_array,int width,int height);

   public:
      void    Copy         (const Array2D &s_array,int sx,int sy,int width,int height,int dx,int dy);
      void    Copy         (const Array2D &s_array,IBox2D &s_box,IPoint2D &d_pos);
      Array2D Extract      (int sx,int sy,int width,int height);
      Array2D Extract      (IBox2D &s_box);
      void    FillBoundary (int thickness);
      void    Set          (int sx,int sy,int width,int height,T value);
      void    Set          (IBox2D &s_box,T value);
      void    SetBoundary  (int thickness,T value);
};

 template<class T>
 Array2D<T>& Array2D<T>::operator = (const Array2D &s_array)

{
   if (Width != s_array.Width || Height != s_array.Height) Create (s_array.Width,s_array.Height);
   Copy (s_array,0,0,Width,Height,0,0);
   return (*this);
}

 template<class T>
 inline T* Array2D<T>::operator [] (char y) const

{
   return (Array[y]);
}

 template<class T>
 inline T* Array2D<T>::operator [] (byte y) const

{
   return (Array[y]);
}

 template<class T>
 inline T* Array2D<T>::operator [] (short y) const

{
   return (Array[y]);
}

 template<class T>
 inline T* Array2D<T>::operator [] (ushort y) const
 
{
   return (Array[y]);
}

 template<class T>
 inline T* Array2D<T>::operator [] (int y) const
 
{
   return (Array[y]);
}

 template<class T>
 inline T* Array2D<T>::operator [] (uint y) const

{
   return (Array[y]);
}

 template<class T>
 inline T* Array2D<T>::operator [] (long y) const

{
   return (Array[y]);
}

 template<class T>
 inline T* Array2D<T>::operator [] (ulong y) const

{
   return (Array[y]);
}

 template<class T>
 inline T& Array2D<T>::operator () (char i) const

{
   return (Buffer[i]);
}

 template<class T>
 inline T& Array2D<T>::operator () (byte i) const

{
   return (Buffer[i]);
}

 template<class T>
 inline T& Array2D<T>::operator () (short i) const

{
   return (Buffer[i]);
}

 template<class T>
 inline T& Array2D<T>::operator () (ushort i) const
 
{
   return (Buffer[i]);
}

 template<class T>
 inline T& Array2D<T>::operator () (int i) const
 
{
   return (Buffer[i]);
}

 template<class T>
 inline T& Array2D<T>::operator () (uint i) const

{
   return (Buffer[i]);
}

 template<class T>
 inline T& Array2D<T>::operator () (long i) const

{
   return (Buffer[i]);
}

 template<class T>
 inline T& Array2D<T>::operator () (ulong i) const

{
   return (Buffer[i]);
}

 template<class T>
 inline int Array2D<T>::operator ! (   ) const
 
{
   if (Array) return (FALSE);
   else return (TRUE);
}

 template<class T>
 inline Array2D<T>::operator T* (   ) const

{
   return (Buffer);
}

 template<class T>
 inline Array2D<T>::operator T** (   ) const

{
   return (Array);
}

 template<class T>
 inline Array2D<T>::operator void* (   ) const

{
   return ((void *)Buffer);
}

 template<class T>
 Array2D<T>::Array2D (   )

{
   _Init (   );
}

 template<class T>
 Array2D<T>::Array2D (int width,int height)

{
   _Init  (   );
   Create (width,height);
}

 template<class T>
 Array2D<T>::Array2D (T *s_buffer,int width,int height)
 
{
   _Init  (   );
   Import (s_buffer,width,height);
}

 template<class T>
 Array2D<T>::Array2D (T **s_array,int width,int height)

{
   _Init (   );
   Import (s_array,width,height);
}

 template<class T>
 Array2D<T>::Array2D (const Array2D &s_array)

{
   _Init  (   );
   Create (s_array.Width,s_array.Height);
   Copy   (s_array,0,0,Width,Height,0,0);
}

 template<class T>
 Array2D<T>::Array2D (Array2D *s_array)

{
   Width  = s_array->Width;
   Height = s_array->Height;
   Array  = s_array->Array;
   Buffer = s_array->Buffer;
   s_array->Width  = s_array->Height = 0;
   s_array->Array  = NULL;
   s_array->Buffer = NULL;
}

 template<class T>
 Array2D<T>::~Array2D (   )
 
{
   Delete (   );
}

 template<class T>
 void Array2D<T>::_Init (   )
 
{
   Array  = NULL;
   Buffer = NULL;
   Width  = 0;
   Height = 0;
} 

 template<class T>
 void Array2D<T>::Clear (   )

{
   ClearArray2D ((byte **)Array,Width,Height,sizeof(T));
}

 template<class T>
 void Array2D<T>::Copy (const Array2D &s_array,int sx,int sy,int width,int height,int dx,int dy)

{
   int x1,y1,x2,y2;

   int ex = sx + width;
   int ey = sy + height;
   for (y1 = sy,y2 = dy; y1 < ey; y1++,y2++) {
      T *_RESTRICT l_s_array = s_array[y1];
      T *_RESTRICT l_d_array = Array[y2];
      for (x1 = sx,x2 = dx; x1 < ex; x1++,x2++)
         l_d_array[x2] = l_s_array[x1];
   }
}

 template<class T>
 void Array2D<T>::Copy (const Array2D &s_array,IBox2D &s_box,IPoint2D &d_pos)

{
   Copy (s_array,s_box.X,s_box.Y,s_box.Width,s_box.Height,d_pos.X,d_pos.Y);
}

 template<class T>
 int Array2D<T>::Create (int width,int height)

{
   int x,y;

   if (Array  != NULL) delete [] Array;
   if (Buffer != NULL) delete [] Buffer;
   Array  = NULL;
   Buffer = NULL;
   Width  = width;
   Height = height;
   if (Width && Height) {
      Array  = new T*[Height];
      if (Array == NULL)  return (1);
      Buffer = new T[Width * Height];
      if (Buffer == NULL) return (2);
      for (x = y = 0; y < Height; y++,x += Width) Array[y] = Buffer + x;
   }
   return (DONE);
}

 template<class T>
 void Array2D<T>::Delete (   )

{
   if (Array  != NULL) delete [] Array;
   if (Buffer != NULL) delete [] Buffer;
   _Init (   );
}
 
 template<class T>
 Array2D<T> Array2D<T>::Extract (int sx,int sy,int width,int height)

{
   Array2D<T> d_array(width,height);
   d_array.Copy (*this,sx,sy,width,height,0,0);
   return (Array2D<T>(&d_array));
}

 template<class T>
 Array2D<T> Array2D<T>::Extract (IBox2D &s_box)

{
   Array2D<T> d_array(s_box.Width,s_box.Height);
   IPoint2D s_point(0,0);
   d_array.Copy (*this,s_box,s_point);
   return (Array2D<T>(&d_array));
}

 template<class T>
 void Array2D<T>::FillBoundary (int thickness)

{
   int x,y;

   for (   ; thickness > 0; thickness--) {
      int sy = thickness;
      int sx = thickness;
      int ey = Height - thickness - 1;
      int ex = Width  - thickness - 1;
      int sy1 = sy - 1;
      int sx1 = sx - 1;
      int ey1 = ey + 1;
      int ex1 = ex + 1;
      for (y = sy; y <= ey; y++) {
         T *_RESTRICT l_array  = Array[y];
         l_array[sx1] = l_array[sx];
         l_array[ex1] = l_array[ex];
      }
      T *_RESTRICT l_array_s0 = Array[sy];
      T *_RESTRICT l_array_s1 = Array[sy1];
      T *_RESTRICT l_array_e0 = Array[ey];
      T *_RESTRICT l_array_e1 = Array[ey1];
      for (x = sx; x <= ex; x++) {
         l_array_s1[x] = l_array_s0[x];
         l_array_e1[x] = l_array_e0[x];
      }
      l_array_s1[sx1] = l_array_s0[sx];
      l_array_s1[ex1] = l_array_s0[ex];
      l_array_e1[sx1] = l_array_e0[sx];
      l_array_e1[ex1] = l_array_e0[ex];
   }
}

 template<class T>
 float GetMean (const Array2D<T> &s_array)

{
   int x,y;

   float mean = 0.0f;
   for (y = 0; y < s_array.Height; y++) {
      T* l_s_array = s_array[y];
      for (x = 0; x < s_array.Width; x++)
         mean += (float)l_s_array[x];
   }
   mean /= s_array.Width * s_array.Height;
   return (mean);
}

 template<class T>
 float GetMean (const Array2D<T> &s_array,int sx,int sy,int width,int height)

{
   int x,y;

   float mean = 0.0f;
   int ex = sx + width;
   int ey = sy + height;
   for (y = sy; y < ey; y++) {
      T* l_s_array = s_array[y];
      for (x = sx; x < ex; x++)
         mean += (float)l_s_array[x];
   }
   mean /= width * height;
   return (mean);
}

 template<class T>
 int Array2D<T>::Import (T *s_buffer,int width,int height)
 
{
   int x,y;

   if (s_buffer == NULL) {
      if (Array != NULL) {
         delete [] Array;
         Array = NULL;
      }      
   }   
   Buffer = s_buffer;
   Width  = width;
   Height = height;
   if (Buffer != NULL && Width && Height) {
      Array = new T*[Height];
      if (Array == NULL) return (1);
      for (x = y = 0; y < Height; y++,x += Width) Array[y] = Buffer + x;
   }
   return (DONE);
}

 template<class T>
 int Array2D<T>::Import (T **s_array,int width,int height)

{
   Array  = s_array;
   Buffer = NULL;
   Width  = width;
   Height = height;
   if (Array != NULL) Buffer = Array[0];
   return (DONE);
}

 template<class T>
 void Array2D<T>::Set (int sx,int sy,int width,int height,T value)

{
   int x,y;
   
   int ex = sx + width;
   int ey = sy + height;
   for (y = sy; y < ey; y++) {
      T *_RESTRICT l_d_array = Array[y];
      for (x = sx; x < ex; x++)
         l_d_array[x] = value;
   }
}

 template<class T>
 void Array2D<T>::Set (IBox2D &s_box,T value)

{
   Set (s_box.X,s_box.Y,s_box.Width,s_box.Height,value);
}

 template<class T>
 void Array2D<T>::SetBoundary (int thickness,T value)

{
   int x,y;

   int by = Height - thickness;
   int bx = Width  - thickness;
   for (y = 0; y < thickness; y++) {
      T *_RESTRICT l_array_s = Array[y];
      T *_RESTRICT l_array_t = Array[by + y];
      for (x = 0; x < Width; x++)
         l_array_s[x] = l_array_t[x] = value;
   }
   for (y = 0; y < Height; y++) {
      T *_RESTRICT l_array = Array[y];
      for (x = 0; x < thickness; x++)
         l_array[x] = l_array[bx + x] = value;
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// Function Templates
//
///////////////////////////////////////////////////////////////////////////////

 template <class T,class U>
 void Convert (const Array2D<T> &s_array,Array2D<U> &d_array)

{
   int x,y;

   for (y = 0; y < s_array.Height; y++) {
      T *_RESTRICT l_s_array = s_array[y];
      U *_RESTRICT l_d_array = d_array[y];
      for (x = 0; x < s_array.Width; x++)
         l_d_array[x] = (U)l_s_array[x];
   }
}

 template<class T>
 int FindMaximum (const Array2D<T> &s_array,T &max,int &max_x,int &max_y)

{
   int x,y;

   max = 0;
   max_x = max_y = 0;
   if (!s_array) return (1);
   max = s_array[0][0];
   for (y = 0; y < s_array.Height; y++) {
      T *_RESTRICT l_s_array = s_array[y];
      for (x = 0; x < s_array.Width; x++) {
         if (l_s_array[x] > max) {
            max   = l_s_array[x];
            max_x = x;
            max_y = y;
         }
      }
   }
   return (DONE);
}

 template<class T>
 int FindMinimum (const Array2D<T> &s_array,T &min,int &min_x,int &min_y)

{
   int x,y;

   min = 0;
   min_x = min_y = 0;
   if (!s_array) return (1);
   min = s_array[0][0];
   for (y = 0; y < s_array.Height; y++) {
      T *_RESTRICT l_s_array = s_array[y];
      for (x = 0; x < s_array.Width; x++) {
         if (l_s_array[x] < min) {
            min   = l_s_array[x];
            min_x = x;
            min_y = y;
         }
      }
   }
   return (DONE);
}

 template<class T>
 int FindMinMax (const Array2D<T> &s_array,T &min,T &max,int &min_x,int &min_y,int &max_x,int &max_y)

{
   int x,y;

   min = max = 0;
   min_x = min_y = max_x = max_y = 0;
   if (!s_array) return (1);
   min = max = s_array[0][0];
   for (y = 0; y < s_array.Height; y++) {
      T *_RESTRICT l_s_array = s_array[y];
      for (x = 0; x < s_array.Width; x++) {
         if (l_s_array[x] < min) {
             min   = l_s_array[x];
             min_x = x;
             min_y = y;
         }
         else if (l_s_array[x] > max) {
            max   = l_s_array[x];
            max_x = x;
            max_y = y;
         }
      }
   }
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class Template: Array3D<T>
//
///////////////////////////////////////////////////////////////////////////////

 template<class T>
 class Array3D

{
   protected:
      T ***Array;
      T *Buffer;

   public:
      int Width,Height,Depth;

   public:
      Array3D (   );
      Array3D (int width,int height,int depth);
      Array3D (T *s_buffer ,int width,int height,int depth);
      Array3D (T ***s_array,int width,int height,int depth);
      Array3D (const Array3D &s_array);
      Array3D (Array3D *s_array);

   public:
      virtual ~Array3D (   );

   public:
      Array3D&  operator =     (const Array3D &s_array);
            T** operator []    (char   z) const;
            T** operator []    (byte   z) const;
            T** operator []    (short  z) const;
            T** operator []    (ushort z) const;
            T** operator []    (int    z) const;
            T** operator []    (uint   z) const;
            T** operator []    (long   z) const;
            T** operator []    (ulong  z) const;
            T&  operator ()    (char   i) const;
            T&  operator ()    (byte   i) const;
            T&  operator ()    (short  i) const;
            T&  operator ()    (ushort i) const;
            T&  operator ()    (int    i) const;
            T&  operator ()    (uint   i) const;
            T&  operator ()    (long   i) const;
            T&  operator ()    (ulong  i) const;
            int operator !     (   ) const;
                operator T*    (   ) const;
                operator T***  (   ) const;
                operator void* (   ) const;

   protected:
      void _Init (   );
   
   public:
      virtual int  Create (int width,int height,int depth);
      virtual void Delete (   );
      virtual int  Import (T *s_buffer ,int width,int height,int depth);
      virtual int  Import (T ***s_array,int width,int height,int depth);

   public:
      void    Clear   (   );
      void    Copy    (const Array3D &s_array,int sx,int sy,int sz,int width,int height,int depth,int dx,int dy,int dz);
      void    Copy    (const Array3D &s_array,IBox3D &s_box,IPoint3D &d_pos);
      Array3D Extract (int sx,int sy,int sz,int width,int height,int depth);
      Array3D Extract (IBox3D &s_box);
      void    Set     (int sx,int sy,int sz,int width,int height,int depth,T value);
      void    Set     (IBox3D &s_box,T value);
};

 template<class T>
 Array3D<T>& Array3D<T>::operator = (const Array3D &s_array)

{
   if (Width != s_array.Width || Height != s_array.Height || Depth != s_array.Depth) Create (s_array.Width,s_array.Height,s_array.Depth);
   Copy (s_array,0,0,0,Width,Height,Depth,0,0,0);
   return (*this);
}

 template<class T>
 inline T** Array3D<T>::operator [] (char z) const

{
   return (Array[z]);
}

 template<class T>
 inline T** Array3D<T>::operator [] (byte z) const

{
   return (Array[z]);
}

 template<class T>
 inline T** Array3D<T>::operator [] (short z) const
 
{
   return (Array[z]);
}

 template<class T>
 inline T** Array3D<T>::operator [] (ushort z) const

{
   return (Array[z]);
}

 template<class T>
 inline T** Array3D<T>::operator [] (int z) const

{
   return (Array[z]);
}

 template<class T>
 inline T** Array3D<T>::operator [] (uint z) const

{
   return (Array[z]);
}

 template<class T>
 inline T** Array3D<T>::operator [] (long z) const

{
   return (Array[z]);
}

 template<class T>
 inline T** Array3D<T>::operator [] (ulong z) const

{
   return (Array[z]);
}

 template<class T>
 inline T& Array3D<T>::operator () (char i) const

{
   return (Buffer[i]);
}

 template<class T>
 inline T& Array3D<T>::operator () (byte i) const

{
   return (Buffer[i]);
}

 template<class T>
 inline T& Array3D<T>::operator () (short i) const

{
   return (Buffer[i]);
}

 template<class T>
 inline T& Array3D<T>::operator () (ushort i) const
 
{
   return (Buffer[i]);
}

 template<class T>
 inline T& Array3D<T>::operator () (int i) const
 
{
   return (Buffer[i]);
}

 template<class T>
 inline T& Array3D<T>::operator () (uint i) const

{
   return (Buffer[i]);
}

 template<class T>
 inline T& Array3D<T>::operator () (long i) const
 
{
   return (Buffer[i]);
}

 template<class T>
 inline T& Array3D<T>::operator () (ulong i) const

{
   return (Buffer[i]);
}

 template<class T>
 inline int Array3D<T>::operator ! (   ) const
 
{
   if (Array) return (FALSE);
   else return (TRUE);
}

 template<class T>
 inline Array3D<T>::operator T* (   ) const

{
   return (Buffer);
}

 template<class T>
 inline Array3D<T>::operator T*** (   ) const

{
   return (Array);
}

 template<class T>
 inline Array3D<T>::operator void* (   ) const

{
   return ((void *)Buffer);
}

 template<class T>
 Array3D<T>::Array3D (   )

{
   _Init (   );
}

 template<class T>
 Array3D<T>::Array3D (int width,int height,int depth)

{
   _Init (   );
   Create (width,height,depth);
}
 
 template<class T>
 Array3D<T>::Array3D (T ***s_array,int width,int height,int depth)

{
   _Init (   );
   Import (s_array,width,height,depth);
}

 template<class T>
 Array3D<T>::Array3D (const Array3D &s_array)

{
   _Init (   );
   Create (s_array.Width,s_array.Height,s_array.Depth);
   Copy (s_array,0,0,0,Width,Height,Depth,0,0,0);
}
 
 template<class T>
 Array3D<T>::Array3D (Array3D *s_array)

{
   Width  = s_array->Width;
   Height = s_array->Height;
   Depth  = s_array->Depth;
   Array  = s_array->Array;
   Buffer = s_array->Buffer;
   s_array->Width = s_array->Height = s_array->Depth = 0;
   s_array->Array  = NULL;
   s_array->Buffer = NULL;
}

 template<class T>
 Array3D<T>::~Array3D (   )

{
   Delete (   );
}

 template<class T>
 void Array3D<T>::_Init (   )
 
{
   Array  = NULL;
   Buffer = NULL;
   Width  = 0;
   Height = 0;
   Depth  = 0;
}

 template<class T>
 void Array3D<T>::Clear (   )

{
   ClearArray3D ((byte ***)Array,Width,Height,Depth,sizeof(T));
}

 template<class T>
 void Array3D<T>::Copy (const Array3D &s_array,int sx,int sy,int sz,int width,int height,int depth,int dx,int dy,int dz)

{
   int x1,y1,z1,x2,y2,z2;
   
   int ex = sx + width;
   int ey = sy + height;
   int ez = sz + depth;
   for (z1 = sz,z2 = dz; z1 < ez; z1++,z2++) {
      T **_RESTRICT l_s_array = s_array[z1];
      T **_RESTRICT l_Array   = Array[z2];
      for (y1 = sy,y2 = dy; y1 < ey; y1++,y2++) {
         T *_RESTRICT ll_s_array = l_s_array[y1];
         T *_RESTRICT ll_Array   = l_Array[y2];
         for (x1 = sx,x2 = dx; x1 < ex; x1++,x2++) {
            ll_Array[x2] = ll_s_array[x1];
         }
      }
   } 
}

 template<class T>
 void Array3D<T>::Copy (const Array3D &s_array,IBox3D &s_box,IPoint3D &d_pos)

{
   Copy (s_array,s_box.X,s_box.Y,s_box.Z,s_box.Width,s_box.Height,s_box.Depth,d_pos.X,d_pos.Y,d_pos.Z);
}

 template<class T>
 int Array3D<T>::Create (int width,int height,int depth)

{
   int x,y,z;
   
   if (Array != NULL) {
      if (Array[0] != NULL) delete [] Array[0];
      delete Array;
   }
   if (Buffer != NULL) delete [] Buffer;
   Array  = NULL;
   Buffer = NULL;
   Width  = width;
   Height = height;
   Depth  = depth;
   if (Width && Height && Depth) {
      Array = new T**[Depth];
      if (Array == NULL) return (1);
      T **array2d = new T*[Height * Depth];
      if (array2d == NULL) return (2);
      for (z = y = 0; z < Depth; z++,y += Height) Array[z] = array2d + y;
      Buffer = new T[Width * Height * Depth];
      if (Buffer == NULL) return (3);
      for (z = x = 0; z < Depth; z++) {
         T **l_Array = Array[z];
         for (y = 0; y < Height; y++) {
            l_Array[y] = Buffer + x;
            x += width;
         }
      }
   }
   return (DONE);
}

 template<class T>
 void Array3D<T>::Delete (   )

{
   if (Array != NULL) {
      if (Array[0] != NULL) delete [] Array[0];
      delete [] Array;
   }
   if (Buffer != NULL) delete [] Buffer;
   _Init (   );
}

 template<class T>
 Array3D<T> Array3D<T>::Extract (int sx,int sy,int sz,int width,int height,int depth)

{
   Array3D<T> d_array(width,height,depth);
   d_array.Copy (*this,sx,sy,sz,width,height,depth,0,0,0);
   return (Array3D<T>(&d_array));
}

 template<class T>
 Array3D<T> Array3D<T>::Extract (IBox3D &s_box)

{
   Array3D<T> d_array(s_box.Width,s_box.Height,s_box.Depth);
   IPoint3D s_point(0,0,0);
   d_array.Copy (*this,s_box,s_point);
   return (Array3D<T>(&d_array));
}

 template<class T>
 int Array3D<T>::Import (T *s_buffer,int width,int height,int depth)
 
{
   int x,y,z;
   
   if (s_buffer == NULL) {
      if (Array != NULL) {
         if (Array[0] != NULL) delete [] Array[0];
         if (Array != NULL) delete [] Array;
         Array = NULL;
      }      
   }   
   Buffer = s_buffer;
   Width  = width;
   Height = height;
   Depth  = depth;
   if (Buffer != NULL && Width && Height && Depth) {
      Array = new T**[Depth];
      if (Array == NULL) return (1);
      T **array2d = new T*[Height * Depth];
      if (array2d == NULL) return (2);
      for (z = y = 0; z < Depth; z++,y += Height) Array[z] = array2d + y;
      for (z = x = 0; z < Depth; z++) {
         T **l_Array = Array[z];
         for (y = 0; y < Height; y++) {
            l_Array[y] = Buffer + x;
            x += width;
         }
      }
   }
   return (DONE);
}

 template<class T>
 int Array3D<T>::Import (T ***s_array,int width,int height,int depth)

{
   Array  = s_array;
   Buffer = NULL;
   Width  = width;
   Height = height;
   Depth  = depth;
   if (Array != NULL) {
      if (Array[0] != NULL) Buffer = Array[0][0];
   }
   return (DONE);
}

 template<class T>
 void Array3D<T>::Set (int sx,int sy,int sz,int width,int height,int depth,T value)

{
   int x,y,z;
   
   int ex = sx + width;
   int ey = sy + height;
   int ez = sz + depth;
   for (z = sz; z < ez; z++) {
      T **_RESTRICT l_Array = Array[z];
      for (y = sy; y < ey; y++) {
         T *_RESTRICT ll_Array = l_Array[y];
         for (x = sx; x < ex; x++) {
            ll_Array[x] = value;
         }
      }
   }
}

 template<class T>
 void Array3D<T>::Set (IBox3D &s_box,T value)

{
   Set (s_box.X,s_box.Y,s_box.Z,s_box.Width,s_box.Height,s_box.Depth,value);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class Template: PointerArray<T>
//
///////////////////////////////////////////////////////////////////////////////

 template<class T>
 class PointerArray : public Array1D<T*>
 
{
   public:
      void Add    (T *s_item);
      int  Find   (T *s_item);
      void Remove (T *s_item);
};

 template<class T>
 void PointerArray<T>::Add (T *s_item)
 
{
   int i;
   
   if (this->Array == NULL) {
	  this->Create (1);
	  this->Array[0] = s_item;
      return;
   }
   for (i = 0; i < this->Length; i++) if (this->Array[i] == s_item) return;
   Array1D<T*> t_array(this->Length + 1);
   t_array.Copy (*this,0,this->Length,0);
   t_array[this->Length] = s_item;
   this->Delete (   );
   Import ((T**)t_array,t_array.Length);
   t_array.Import (NULL,0);
}

 template<class T>
 int PointerArray<T>::Find (T *s_item)
 
{
   int i;
   
   if (this->Array == NULL) return (-1);
   for (i = 0; i < this->Length; i++) if (this->Array[i] == s_item) return (i);
   return (-1);
}

 template<class T>
 void PointerArray<T>::Remove (T *s_item)
 
{
   int pos = this->Find (s_item);
   if (pos < 0) return;
   if (this->Length == 1) {
	   this->Delete (   );
      return;
   }
   Array1D<T*> t_array(this->Length - 1);
   if (pos == 0) {
      t_array.Copy (*this,1,t_array.Length,0);
   }
   else if (pos == this->Length - 1) {
      t_array.Copy (*this,0,t_array.Length,0);
   }
   else {
      t_array.Copy (*this,0,pos,0);
      t_array.Copy (*this,pos + 1,this->Length - pos - 1,pos);
   }
   this->Delete (   );
   Import ((T**)t_array,t_array.Length);
   t_array.Import (NULL,0);
}


///////////////////////////////////////////////////////////////////////////////
//
// Type Redefinitions
//
///////////////////////////////////////////////////////////////////////////////

typedef Array1D<byte>      BArray1D;
typedef Array1D<char>      CArray1D;
typedef Array1D<double>    DArray1D;
typedef Array1D<float>     FArray1D;
typedef Array1D<int>       IArray1D;
typedef Array1D<long>      LArray1D;
typedef Array1D<short>     SArray1D;
typedef Array1D<uint>      UIArray1D;
typedef Array1D<ulong>     ULArray1D;
typedef Array1D<ushort>    USArray1D;
typedef Array2D<int64>     I64Array2D;

typedef Array1D<DPoint2D>  DP2DArray1D;
typedef Array1D<FPoint2D>  FP2DArray1D;
typedef Array1D<IPoint2D>  IP2DArray1D;
typedef Array1D<SPoint2D>  SP2DArray1D;
typedef Array1D<USPoint2D> USP2DArray1D;

typedef Array1D<DPoint3D>  DP3DArray1D;
typedef Array1D<FPoint3D>  FP3DArray1D;
typedef Array1D<IPoint3D>  IP3DArray1D;
typedef Array1D<SPoint3D>  SP3DArray1D;
typedef Array1D<USPoint3D> USP3DArray1D;

typedef Array1D<DSize2D>   DS2DArray1D;
typedef Array1D<FSize2D>   FS2DArray1D;
typedef Array1D<ISize2D>   IS2DArray1D;
typedef Array1D<SSize2D>   SS2DArray1D;
typedef Array1D<USSize2D>  USS2DArray1D;

typedef Array1D<DSize3D>   DS3DArray1D;
typedef Array1D<FSize3D>   FS3DArray1D;
typedef Array1D<ISize3D>   IS3DArray1D;
typedef Array1D<SSize3D>   SS3DArray1D;
typedef Array1D<USSize3D>  USS3DArray1D;

typedef Array1D<DBox2D>    DB2DArray1D;
typedef Array1D<FBox2D>    FB2DArray1D;
typedef Array1D<IBox2D>    IB2DArray1D;
typedef Array1D<SBox2D>    SB2DArray1D;
typedef Array1D<USBox2D>   USB2DArray1D;

typedef Array1D<DBox3D>    DB3DArray1D;
typedef Array1D<FBox3D>    FB3DArray1D;
typedef Array1D<IBox3D>    IB3DArray1D;
typedef Array1D<SBox3D>    SB3DArray1D;
typedef Array1D<USBox3D>   USB3DArray1D;

typedef Array1D<DLine2D>   DL2DArray1D;
typedef Array1D<FLine2D>   FL2DArray1D;
typedef Array1D<ILine2D>   IL2DArray1D;
typedef Array1D<SLine2D>   SL2DArray1D;
typedef Array1D<USLine2D>  USL2DArray1D;

typedef Array1D<DLine3D>   DL3DArray1D;
typedef Array1D<FLine3D>   FL3DArray1D;
typedef Array1D<ILine3D>   IL3DArray1D;
typedef Array1D<SLine3D>   SL3DArray1D;
typedef Array1D<USLine3D>  USL3DArray1D;

typedef Array2D<byte>      BArray2D;
typedef Array2D<char>      CArray2D;
typedef Array2D<double>    DArray2D;
typedef Array2D<float>     FArray2D;
typedef Array2D<int>       IArray2D;
typedef Array2D<long>      LArray2D;
typedef Array2D<short>     SArray2D;
typedef Array2D<uint>      UIArray2D;
typedef Array2D<ulong>     ULArray2D;
typedef Array2D<ushort>    USArray2D;

typedef Array2D<DPoint2D>  DP2DArray2D;
typedef Array2D<FPoint2D>  FP2DArray2D;
typedef Array2D<IPoint2D>  IP2DArray2D;
typedef Array2D<SPoint2D>  SP2DArray2D;
typedef Array2D<USPoint2D> USP2DArray2D;

typedef Array2D<DPoint3D>  DP3DArray2D;
typedef Array2D<FPoint3D>  FP3DArray2D;
typedef Array2D<IPoint3D>  IP3DArray2D;
typedef Array2D<SPoint3D>  SP3DArray2D;
typedef Array2D<USPoint3D> USP3DArray2D;

typedef Array2D<DSize2D>   DS2DArray2D;
typedef Array2D<FSize2D>   FS2DArray2D;
typedef Array2D<ISize2D>   IS2DArray2D;
typedef Array2D<SSize2D>   SS2DArray2D;
typedef Array2D<USSize2D>  USS2DArray2D;

typedef Array2D<DSize3D>   DS3DArray2D;
typedef Array2D<FSize3D>   FS3DArray2D;
typedef Array2D<ISize3D>   IS3DArray2D;
typedef Array2D<SSize3D>   SS3DArray2D;
typedef Array2D<USSize3D>  USS3DArray2D;

typedef Array2D<DBox2D>    DB2DArray2D;
typedef Array2D<FBox2D>    FB2DArray2D;
typedef Array2D<IBox2D>    IB2DArray2D;
typedef Array2D<SBox2D>    SB2DArray2D;
typedef Array2D<USBox2D>   USB2DArray2D;

typedef Array2D<DBox3D>    DB3DArray2D;
typedef Array2D<FBox3D>    FB3DArray2D;
typedef Array2D<IBox3D>    IB3DArray2D;
typedef Array2D<SBox3D>    SB3DArray2D;
typedef Array2D<USBox3D>   USB3DArray2D;

typedef Array2D<DLine2D>   DL2DArray2D;
typedef Array2D<FLine2D>   FL2DArray2D;
typedef Array2D<ILine2D>   IL2DArray2D;
typedef Array2D<SLine2D>   SL2DArray2D;
typedef Array2D<USLine2D>  USL2DArray2D;

typedef Array2D<DLine3D>   DL3DArray2D;
typedef Array2D<FLine3D>   FL3DArray2D;
typedef Array2D<ILine3D>   IL3DArray2D;
typedef Array2D<SLine3D>   SL3DArray2D;
typedef Array2D<USLine3D>  USL3DArray2D;

typedef Array3D<byte>      BArray3D;
typedef Array3D<char>      CArray3D;
typedef Array3D<double>    DArray3D;
typedef Array3D<float>     FArray3D;
typedef Array3D<int>       IArray3D;
typedef Array3D<long>      LArray3D;
typedef Array3D<short>     SArray3D;
typedef Array3D<uint>      UIArray3D;
typedef Array3D<ulong>     ULArray3D;
typedef Array3D<ushort>    USArray3D;

typedef Array3D<DPoint2D>  DP2DArray3D;
typedef Array3D<FPoint2D>  FP2DArray3D;
typedef Array3D<IPoint2D>  IP2DArray3D;
typedef Array3D<SPoint2D>  SP2DArray3D;
typedef Array3D<USPoint2D> USP2DArray3D;

typedef Array3D<DPoint3D>  DP3DArray3D;
typedef Array3D<FPoint3D>  FP3DArray3D;
typedef Array3D<IPoint3D>  IP3DArray3D;
typedef Array3D<SPoint3D>  SP3DArray3D;
typedef Array3D<USPoint3D> USP3DArray3D;

typedef Array3D<DSize2D>   DS2DArray3D;
typedef Array3D<FSize2D>   FS2DArray3D;
typedef Array3D<ISize2D>   IS2DArray3D;
typedef Array3D<SSize2D>   SS2DArray3D;
typedef Array3D<USSize2D>  USS2DArray3D;

typedef Array3D<DSize3D>   DS3DArray3D;
typedef Array3D<FSize3D>   FS3DArray3D;
typedef Array3D<ISize3D>   IS3DArray3D;
typedef Array3D<SSize3D>   SS3DArray3D;
typedef Array3D<USSize3D>  USS3DArray3D;

typedef Array3D<DBox2D>    DB2DArray3D;
typedef Array3D<FBox2D>    FB2DArray3D;
typedef Array3D<IBox2D>    IB2DArray3D;
typedef Array3D<SBox2D>    SB2DArray3D;
typedef Array3D<USBox2D>   USB2DArray3D;

typedef Array3D<DBox3D>    DB3DArray3D;
typedef Array3D<FBox3D>    FB3DArray3D;
typedef Array3D<IBox3D>    IB3DArray3D;
typedef Array3D<SBox3D>    SB3DArray3D;
typedef Array3D<USBox3D>   USB3DArray3D;

typedef Array3D<DLine2D>   DL2DArray3D;
typedef Array3D<FLine2D>   FL2DArray3D;
typedef Array3D<ILine2D>   IL2DArray3D;
typedef Array3D<SLine2D>   SL2DArray3D;
typedef Array3D<USLine2D>  USL2DArray3D;

typedef Array3D<DLine3D>   DL3DArray3D;
typedef Array3D<FLine3D>   FL3DArray3D;
typedef Array3D<ILine3D>   IL3DArray3D;
typedef Array3D<SLine3D>   SL3DArray3D;
typedef Array3D<USLine3D>  USL3DArray3D;

///////////////////////////////////////////////////////////////////////////////
//
// Function Templates
//
///////////////////////////////////////////////////////////////////////////////

 template <class T,class U>
 void Convert (const Array3D<T> &s_array,Array3D<U> &d_array)

{
   int x,y,z;

   for (z = 0; z < s_array.Depth; z++) {
      T **_RESTRICT l_s_array = s_array[z];
      U **_RESTRICT l_d_array = d_array[z];
      for (y = 0; y < s_array.Height; y++) {
         T *_RESTRICT ll_s_array = l_s_array[y];
         U *_RESTRICT ll_d_array = l_d_array[y];
         for (x = 0; x < s_array.Width; x++) {
            ll_d_array[x] = (U)ll_s_array[x];
         }
      }
   }
}

 template<class T>
 void FindMaximum (const Array3D<T> &s_array,T &max,int &max_x,int &max_y,int &max_z)

{
   int x,y,z;
   
   max = s_array[0][0][0];
   max_x = max_y = max_z = 0;
   for (z = 0; z < s_array.Depth; z++) {
      T **_RESTRICT l_s_array = s_array[z];
      for (y = 0; y < s_array.Height; y++) {
         T *_RESTRICT ll_s_array = l_s_array[y];
         for (x = 0; x < s_array.Width; x++) {
            if (ll_s_array[x] > max) {
               max   = ll_s_array[x];
               max_x = x;
               max_y = y;
               max_z = z;
            }
         }
      }
   }
}

 template<class T>
 void FindMinimum (const Array3D<T> &s_array,T &min,int &min_x,int &min_y,int &min_z)

{
   int x,y,z;
   
   min = s_array[0][0][0];
   min_x = min_y = min_z = 0;
   for (z = 0; z < s_array.Depth; z++) {
      T **_RESTRICT l_s_array = s_array[z];
      for (y = 0; y < s_array.Height; y++) {
         T *_RESTRICT ll_s_array = l_s_array[y];
         for (x = 0; x < s_array.Width; x++) {
            if (ll_s_array[x] < min) {
               min   = ll_s_array[x];
               min_x = x;
               min_y = y;
               min_z = z;
            }
         }
      }
   }
}

 template<class T>
 void FindMinMax (const Array3D<T> &s_array,T &min,T &max,int &min_x,int &min_y,int &min_z,int &max_x,int &max_y,int &max_z)

{
   int x,y,z;

   min = max = s_array[0][0][0];
   min_x = min_y = min_z = max_x = max_y = max_z = 0;
   for (z = 0; z < s_array.Depth; z++) {
      T **_RESTRICT l_s_array = s_array[z];
      for (y = 0; y < s_array.Height; y++) {
         T *_RESTRICT ll_s_array = l_s_array[y];
         for (x = 0; x < s_array.Width; x++) {
            if (ll_s_array[x] < min) {
               min   = ll_s_array[x];
               min_x = x;
               min_y = y;
               min_z = z;
            }
            else if (ll_s_array[x] > max) {
               max   = ll_s_array[x];
               max_x = x;
               max_y = y;
               max_z = z;
            }
         }
      }
   }
}

 template<class T>
 void FindMaxima (Array1D< Point2D<T> > &s_array,Point2D<T> &max,IPoint2D &max_x)

{
   int x;

   max = s_array[0];
   max_x.Clear (   );
   for (x = 1; x < s_array.Length; x++) {
      if (s_array[x].X > max.X) {
         max.X   = s_array[x].X;
         max_x.X = x;
      }
      if (s_array[x].Y > max.Y) {
         max.Y   = s_array[x].Y;
         max_x.Y = x;
      }
   }
}

 template<class T>
 void FindMinima (Array1D< Point2D<T> > &s_array,Point2D<T> &min,IPoint2D &min_x)

{
   int x;

   min = s_array[0];
   min_x.Clear (   );
   for (x = 1; x < s_array.Length; x++) {
      if (s_array[x].X < min.X) {
         min.X   = s_array[x].X;
         min_x.X = x;
      }
      if (s_array[x].Y < min.Y) {
         min.Y   = s_array[x].Y;
         min_x.Y = x;
      }
   } 
}   

 template<class T>
 void FindMinMax (Array1D< Point2D<T> > &s_array,Point2D<T> &min,Point2D<T> &max,IPoint2D &min_x,IPoint2D &max_x)

{
   int x;

   min = max = s_array[0];
   min_x.Clear (   );
   max_x.Clear (   );
   for (x = 1; x < s_array.Length; x++) {
      if (s_array[x].X < min.X) {
         min.X   = s_array[x].X;
         min_x.X = x;
      }
      else if (s_array[x].X > max.X) {
         max.X   = s_array[x].X;
         max_x.X = x;
      }
      if (s_array[x].Y < min.Y) {
         min.Y   = s_array[x].Y;
         min_x.Y = x;
      }
      else if (s_array[x].Y > max.Y) {
         max.Y   = s_array[x].Y;
         max_x.Y = x;
      }
   }
}

 template<class T>
 void FindMaxima (Array1D< Point3D<T> > &s_array,Point3D<T> &max,IPoint3D &max_x)

{
   int x;

   max = s_array[0];
   max_x.Clear (   );
   for (x = 1; x < s_array.Length; x++) {
      if (s_array[x].X > max.X) {
         max.X   = s_array[x].X;
         max_x.X = x;
      }
      if (s_array[x].Y > max.Y) {
         max.Y   = s_array[x].Y;
         max_x.Y = x;
      }
      if (s_array[x].Z > max.Z) {
         max.Z   = s_array[x].Z;
         max_x.Z = x;
      }
   }
}

 template<class T>
 void FindMinima (Array1D< Point3D<T> > &s_array,Point3D<T> &min,IPoint3D &min_x)

{
   int x;

   min = s_array[0];
   min_x.Clear (   );
   for (x = 1; x < s_array.Length; x++) {
      if (s_array[x].X < min.X) {
         min.X   = s_array[x].X;
         min_x.X = x;
      }
      if (s_array[x].Y < min.Y) {
         min.Y   = s_array[x].Y;
         min_x.Y = x;
      }
      if (s_array[x].Z < min.Z) {
         min.Z   = s_array[x].Z;
         min_x.Z = x;
      }
   }
}

 template<class T>
 void FindMinMax (Array1D< Point3D<T> > &s_array,Point3D<T> &min,Point3D<T> &max,IPoint3D &min_x,IPoint3D &max_x)

{
   int x;

   min = max = s_array[0];
   min_x.Clear (   );
   max_x.Clear (   );
   for (x = 1; x < s_array.Length; x++) {
      if (s_array[x].X < min.X) {
         min.X   = s_array[x].X;
         min_x.X = x;
      }
      else if (s_array[x].X > max.X) {
         max.X   = s_array[x].X;
         max_x.X = x;
      }
      if (s_array[x].Y < min.Y) {
         min.Y   = s_array[x].Y;
         min_x.Y = x;
      }
      else if (s_array[x].Y > max.Y) {
         max.Y   = s_array[x].Y;
         max_x.Y = x;
      }
      if (s_array[x].Z < min.Z) {
         min.Z   = s_array[x].Z;
         min_x.Z = x;
      }
      else if (s_array[x].Z > max.Z) {
         max.Z   = s_array[x].Z;
         max_x.Z = x;
      }
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

FArray1D Abs   (const FArray1D &s_array);
FArray1D Acos  (const FArray1D &s_array);
FArray1D Asin  (const FArray1D &s_array);
FArray1D Atan  (const FArray1D &s_array);
FArray1D Atan2 (const FArray1D &s_array1,const FArray1D &s_array2);
FArray1D Cos   (const FArray1D &s_array);
FArray1D Dx    (const FArray1D &s_array);
FArray1D Dxx   (const FArray1D &s_array);
FArray1D Exp   (const FArray1D &s_array);
FArray1D Log   (const FArray1D &s_array);
FArray1D Log10 (const FArray1D &s_array);
FArray1D Pow   (const FArray1D &s_array1,const FArray1D &s_array2);
FArray1D Pow   (const FArray1D &s_array,float a);
FArray1D Pow   (float a,const FArray1D &s_array);
FArray1D Sin   (const FArray1D &s_array);
FArray1D Sqr   (const FArray1D &s_array);
FArray1D Sqrt  (const FArray1D &s_array);
FArray1D Tan   (const FArray1D &s_array);

}

#endif

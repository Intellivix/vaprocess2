#include "bccl_geomobj.h"
#include "bccl_misc.h"

 namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 FPoint3D CP (const FPoint3D &s_point1,const FPoint3D &s_point2)

{
   FPoint3D d_point;
   d_point.X = s_point1.Y * s_point2.Z - s_point1.Z * s_point2.Y;
   d_point.Y = s_point1.Z * s_point2.X - s_point1.X * s_point2.Z;
   d_point.Z = s_point1.X * s_point2.Y - s_point1.Y * s_point2.X;
   return (d_point);
}

 void CropRegion (int width,int height,int& sx,int& sy,int& ex,int& ey)

{
   if (sx < 0) sx = 0;
   else if (sx >= width ) sx = width  - 1;
   if (sy < 0) sy = 0;
   else if (sy >= height) sy = height - 1;
   if (ex > width ) ex = width;
   else if (ex < 0) ex = 0;
   if (ey > height) ey = height;
   else if (ey < 0) ey = 0;
}

 void CropRegion (ISize2D& c_size,IBox2D& d_rgn)

{
   int sx = d_rgn.X;
   int sy = d_rgn.Y;
   int ex = sx + d_rgn.Width;
   int ey = sy + d_rgn.Height;
   if (sx < 0) sx = 0;
   else if (sx >= c_size.Width ) sx = c_size.Width  - 1;
   if (sy < 0) sy = 0;
   else if (sy >= c_size.Height) sy = c_size.Height - 1;
   if (ex > c_size.Width ) ex = c_size.Width;
   else if (ex < 0) ex = 0;
   if (ey > c_size.Height) ey = c_size.Height;
   else if (ey < 0) ey = 0;
   d_rgn(sx,sy,ex - sx,ey - sy); 
}

 float GetIOU (IBox2D& s_rgn1,IBox2D& s_rgn2)

{
   int   boa = GetOverlapArea (s_rgn1,s_rgn2);
   int   ba1 = s_rgn1.Width * s_rgn1.Height;
   int   ba2 = s_rgn2.Width * s_rgn2.Height;
   float iou = (float)boa / (ba1 + ba2 - boa);
   return (iou);
}

 float GetMaxOverlapRatio (IBox2D& s_rgn1,IBox2D& s_rgn2)

{
   int boa = GetOverlapArea (s_rgn1,s_rgn2);
   int ba1 = s_rgn1.Width * s_rgn1.Height;
   int ba2 = s_rgn2.Width * s_rgn2.Height;
   float or1 = (float)boa / ba1;
   float or2 = (float)boa / ba2;
   return (GetMaximum (or1,or2));
}

 float GetMinOverlapRatio (IBox2D& s_rgn1,IBox2D& s_rgn2)

{
   int boa = GetOverlapArea (s_rgn1,s_rgn2);
   int ba1 = s_rgn1.Width * s_rgn1.Height;
   int ba2 = s_rgn2.Width * s_rgn2.Height;
   float or1 = (float)boa / ba1;
   float or2 = (float)boa / ba2;
   return (GetMinimum (or1,or2));
}

 int GetOverlapArea (IBox2D& s_rgn1,IBox2D& s_rgn2)

{
   int sx1 = s_rgn1.X;
   int sy1 = s_rgn1.Y;
   int ex1 = sx1 + s_rgn1.Width  - 1;
   int ey1 = sy1 + s_rgn1.Height - 1;
   int sx2 = s_rgn2.X;
   int sy2 = s_rgn2.Y;
   int ex2 = sx2 + s_rgn2.Width  - 1;
   int ey2 = sy2 + s_rgn2.Height - 1;
   if (sx1 < sx2) sx1 = sx2;
   if (sy1 < sy2) sy1 = sy2;
   if (ex1 > ex2) ex1 = ex2;
   if (ey1 > ey2) ey1 = ey2;
   int w = ex1 - sx1 + 1;
   int h = ey1 - sy1 + 1;
   if (w <= 0 || h <= 0) return (0);
   return (w * h);
}

 float GetOverlapRatio (IBox2D& s_rgn1,IBox2D& s_rgn2)

{
   int boa = GetOverlapArea (s_rgn1,s_rgn2);
   int ba  = s_rgn2.Width * s_rgn2.Height;
   return ((float)boa / ba);
}

 float GetSimilarity (IBox2D& s_rgn1,IBox2D& s_rgn2)

{
   int w = GetMinimum (s_rgn1.Width ,s_rgn2.Width);
   int h = GetMinimum (s_rgn1.Height,s_rgn2.Height);
   int a = GetMaximum (s_rgn1.Width * s_rgn1.Height,s_rgn2.Width * s_rgn2.Height);
   float similarity = (float)(w * h) / a;
   return (similarity);
}

 IBox2D GetUnion (IBox2D& s_rgn1,IBox2D& s_rgn2)

{
   IP2DArray1D cp_array(8);
   s_rgn1.GetCornerPoints (&cp_array[0]);
   s_rgn2.GetCornerPoints (&cp_array[4]);
   IPoint2D min_p,max_p,min_x,max_x;
   FindMinMax (cp_array,min_p,max_p,min_x,max_x);
   IBox2D d_rgn;
   d_rgn.X      = min_p.X;
   d_rgn.Y      = min_p.Y;
   d_rgn.Width  = max_p.X - min_p.X + 1;
   d_rgn.Height = max_p.Y - min_p.Y + 1;
   return (d_rgn);
}

 float IP (const FPoint2D &s_point1,const FPoint2D &s_point2)

{
   return (s_point1.X * s_point2.X + s_point1.Y * s_point2.Y);
}

 int IP (const IPoint2D &s_point1,const IPoint2D &s_point2)

{
   return (s_point1.X * s_point2.X + s_point1.Y * s_point2.Y);
}

 float IP (const FPoint3D &s_point1,const FPoint3D &s_point2)

{
   return (s_point1.X * s_point2.X + s_point1.Y * s_point2.Y + s_point1.Z * s_point2.Z);
}

 float Mag (const FPoint2D &s_point)

{
   return ((float)sqrt (SquaredMag (s_point)));
}

 float Mag (const IPoint2D &s_point)

{
   return ((float)sqrt ((double)SquaredMag (s_point)));
}

 float Mag (const FPoint3D &s_point)

{
   return ((float)sqrt (SquaredMag (s_point)));
}

 FPoint2D Nor (const FPoint2D &s_point)

{
   float m = Mag(s_point);
   FPoint2D d_point;
   d_point.X = s_point.X / m;
   d_point.Y = s_point.Y / m;
   return (d_point);
}

 FPoint3D Nor (const FPoint3D &s_point)

{
   float m = Mag(s_point);
   FPoint3D d_point;
   d_point.X = s_point.X / m;
   d_point.Y = s_point.Y / m;
   d_point.Z = s_point.Z / m;
   return (d_point);
}

 float SquaredMag (const FPoint2D &s_point)

{
   return (s_point.X * s_point.X + s_point.Y * s_point.Y);
}

 int SquaredMag (const IPoint2D &s_point)

{
   return (s_point.X * s_point.X + s_point.Y * s_point.Y);
}

 float SquaredMag (const FPoint3D &s_point)

{
   return (s_point.X * s_point.X + s_point.Y * s_point.Y + s_point.Z * s_point.Z);
}
   
}

#include "bccl_array.h"

 namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 FArray1D Abs (const FArray1D &s_array)
 
{
   int i;

   FArray1D d_array(s_array.Length);
   for (i = 0; i < s_array.Length; i++)
      d_array[i] = (float)fabs (s_array[i]);
   return (FArray1D (&d_array));
}

 FArray1D Acos (const FArray1D &s_array)
 
{
   int i;

   FArray1D d_array(s_array.Length);
   for (i = 0; i < s_array.Length; i++)
      d_array[i] = (float)acos (s_array[i]);
   return (FArray1D (&d_array));
}
 
 FArray1D Asin (const FArray1D &s_array)
 
{
   int i;

   FArray1D d_array(s_array.Length);
   for (i = 0; i < s_array.Length; i++)
      d_array[i] = (float)asin (s_array[i]);
   return (FArray1D (&d_array));
}

 FArray1D Atan (const FArray1D &s_array)
 
{
   int i;

   FArray1D d_array(s_array.Length);
   for (i = 0; i < s_array.Length; i++)
      d_array[i] = (float)atan (s_array[i]);
   return (FArray1D (&d_array));
}

 FArray1D Atan2 (const FArray1D &s_array1,const FArray1D &s_array2)

{
   int i;

   FArray1D d_array(s_array1.Length);
   for (i = 0; i < s_array1.Length; i++)
      d_array[i] = (float)atan2 (s_array1[i],s_array2[i]);
   return (FArray1D (&d_array));
}

 FArray1D Cos (const FArray1D &s_array)
 
{
   int i;

   FArray1D d_array(s_array.Length);
   for (i = 0; i < s_array.Length; i++)
      d_array[i] = (float)cos (s_array[i]);
   return (FArray1D (&d_array));
}

 FArray1D Dx (const FArray1D &s_array)

{
   int i;

   FArray1D d_array(s_array.Length);
   int ei = s_array.Length - 1;
   for (i = 1; i < ei; i++) d_array[i] = 0.5f * (s_array[i + 1] - s_array[i - 1]);
   d_array[ 0] = d_array[1];
   d_array[ei] = d_array[ei - 1];
   return (FArray1D (&d_array));
}

 FArray1D Dxx (const FArray1D &s_array)

{
   int i;

   FArray1D d_array(s_array.Length);
   int ei = s_array.Length - 1;
   for (i = 1; i < ei; i++) d_array[i] = s_array[i + 1] - 2.0f * s_array[i] + s_array[i - 1];
   return (FArray1D (&d_array));
}

 FArray1D Exp (const FArray1D &s_array)

{
   int i;

   FArray1D d_array(s_array.Length);
   for (i = 0; i < s_array.Length; i++)
      d_array[i] = (float)exp (s_array[i]);
   return (FArray1D (&d_array));
}

 FArray1D Log (const FArray1D &s_array)

{
   int i;

   FArray1D d_array(s_array.Length);
   for (i = 0; i < s_array.Length; i++)
      d_array[i] = (float)log (s_array[i]);
   return (FArray1D (&d_array));
}

 FArray1D Log10 (const FArray1D &s_array)

{
   int i;

   FArray1D d_array(s_array.Length);
   for (i = 0; i < s_array.Length; i++)
      d_array[i] = (float)log10 (s_array[i]);
   return (FArray1D (&d_array));
}

 FArray1D Pow (const FArray1D &s_array1,const FArray1D &s_array2)

{
   int i;

   FArray1D d_array(s_array1.Length);
   for (i = 0; i < s_array1.Length; i++)
      d_array[i] = (float)pow (s_array1[i],s_array2[i]);
   return (FArray1D (&d_array));
}

 FArray1D Pow (const FArray1D &s_array,float a)

{
   int i;

   FArray1D d_array(s_array.Length);
   for (i = 0; i < s_array.Length; i++)
      d_array[i] = (float)pow (s_array[i],a);
   return (FArray1D (&d_array));
}

 FArray1D Pow (float a,const FArray1D &s_array)

{
   int i;

   FArray1D d_array(s_array.Length);
   for (i = 0; i < s_array.Length; i++)
      d_array[i] = (float)pow (a,s_array[i]);
   return (FArray1D (&d_array));
}

 FArray1D Sin (const FArray1D &s_array)
 
{
   int i;

   FArray1D d_array(s_array.Length);
   for (i = 0; i < s_array.Length; i++)
      d_array[i] = (float)sin (s_array[i]);
   return (FArray1D (&d_array));
}

 FArray1D Sqr (const FArray1D &s_array)
 
{
   int i;

   FArray1D d_array(s_array.Length);
   for (i = 0; i < s_array.Length; i++)
      d_array[i] = s_array[i] * s_array[i];
   return (FArray1D (&d_array));
}

 FArray1D Sqrt (const FArray1D &s_array)
 
{
   int i;

   FArray1D d_array(s_array.Length);
   for (i = 0; i < s_array.Length; i++)
      d_array[i] = (float)sqrt (s_array[i]);
   return (FArray1D (&d_array));
}

 FArray1D Tan (const FArray1D &s_array)
 
{
   int i;

   FArray1D d_array(s_array.Length);
   for (i = 0; i < s_array.Length; i++)
      d_array[i] = (float)tan (s_array[i]);
   return (FArray1D (&d_array));
}

}

#if !defined(__BCCL_CURL_H)
#define __BCCL_CURL_H

#include "bccl_define.h"

#if defined(__LIB_CURL)

#include "bccl_string.h"
#include "bccl_list.h"
extern "C" {
#include "curl/curl.h"
};

 namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: CURL_HTTPClient
//
///////////////////////////////////////////////////////////////////////////////

#define CURL_HTTPREQ_GET    0
#define CURL_HTTPREQ_POST   1
#define CURL_HTTPREQ_PUT    2
 
 class CURL_HTTPClient
 
{
   protected:
      int         Flag_Initialized;
      int         BodySize;
      int         HeaderSize;
      int         UploadBufSize;
      int         UploadDataSize;
      CURLcode    ErrorCode;
      CURL*       Handle;
      byte*       UploadBuffer;
      curl_slist* ReqHeader;
      StringA     RcvBuffer_Body;
      StringA     RcvBuffer_Header;

   public:
      int Timeout; // Timeout interval in second.
      
   public:
      CURL_HTTPClient (   );
      virtual ~CURL_HTTPClient (   );
      
   protected:
      static size_t Callback_ReceiveBody   (char* s_buffer,size_t size,size_t n_items,void* user_data);
      static size_t Callback_ReceiveHeader (char* s_buffer,size_t size,size_t n_items,void* user_data);
      static size_t Callback_SendData      (char* s_buffer,size_t size,size_t n_items,void* user_data);

   protected:
      int    ExtractResCode (const char *s_string);
      size_t ReceiveBody    (char* s_buffer,size_t size,size_t n_items);
      size_t ReceiveHeader  (char* s_buffer,size_t size,size_t n_items);
      size_t SendData       (char* s_buffer,size_t size,size_t n_items);

   public:
      int operator ! (   ) const;
   
   public:
      virtual void Close (   );
   
   public:
      void     AddToReqHeader   (const char* s_string);
      void     ClearReqHeader   (   );
      CURLcode GetErrorCode     (   );
      char*    GetResBody       (   );
      int      GetResCode       (   );
      char*    GetResHeader     (   );
      int      Initialize       (   );
      int      IsInitialized    (   );
      int      SendRequest      (int option = CURL_HTTPREQ_GET);
      int      SetPostField     (const char* s_string);
      int      SetPostFieldSize (int size);
      int      SetUploadBuffer  (byte* s_buffer,int s_buf_size);
      int      SetURL           (const char* s_string);
};

 inline int CURL_HTTPClient::operator ! (   ) const

{
   return (!Flag_Initialized);
}

 inline CURLcode CURL_HTTPClient::GetErrorCode (   )

{
   return (ErrorCode);
}

 inline int CURL_HTTPClient::IsInitialized (   )

{
   return (Flag_Initialized);
}

///////////////////////////////////////////////////////////////////////////////
//
// Funcitons
//
///////////////////////////////////////////////////////////////////////////////

void CloseCURL (   );
int  InitCURL  (   );

}

#endif

#endif

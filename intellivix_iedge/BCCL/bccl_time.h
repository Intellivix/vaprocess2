﻿#if !defined( __BCCL_TIME_H )
#define __BCCL_TIME_H

#include "bccl_define.h"

#include <cstdint>

namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: Time
//
///////////////////////////////////////////////////////////////////////////////

class Time

{
public:
   int Year; // 1601 ~ 30827
   int Month; // 1 ~ 12
   int Day; // 1 ~ 31
   int Weekday; // 0 ~ 6, 0: Sunday
   int Hour; // 0 ~ 23
   int Minute; // 0 ~ 59
   int Second; // 0 ~ 59
   int Millisecond; // 0 ~ 999

public:
   Time();

public:
   // 모든 멤버 변수들의 값을 0으로 설정한다.
   void Clear();
   void FromTimeval(::timeval *tv);
   // 현재 시각을 얻는다.
   int GetLocalTime();
   // 저장된 시간 값을 초(second)로 리턴한다.
   // [주의] Day/Hour/Minute/Second 값만 고려한다.
   int GetTimeInSecond();
   // 저장된 시간 값을 문자열로 리턴한다.
   // 문자열 포맷: "YYYY:MM:DD:HH:MM:SS:MMM"
   void GetTimeInString( char* d_string );
   // 저장된 Hour/Minute/Second 값에 대한 문자열을 얻는다.
   // 문자열 포맷: "HH:MM:SS"
   void GetTimeInString_HMS( char* d_string );
   // 저장된 Year/Month/Day 값에 대한 문자열을 얻는다.
   // 문자열 포맷: "YYYY:MM:DD"
   void GetTimeInString_YMD( char* d_string );
   // 주어진 초(second) 값으로부터 시간을 설정한다.
   // [주의] Day/Hour/Minute/Second 값만 고려한다.
   void SetTimeInSecond( int second );
   // 주어진 문자열로부터 시간을 설정한다.
   // 문자열 포맷: "YYYY:MM:DD:HH:MM:SS:MMM"
   // 주어진 문자열의 포맷이 잘못되었으면 0이 아닌 값을 리턴한다.
   int SetTimeInString( const char* s_string );
   // 주어진 문자열로부터 Hour/Minute/Second 값을 설정한다.
   // 문자열 포맷: "HH:MM:SS"
   // 주어진 문자열의 포맷이 잘못되었으면 0이 아닌 값을 리턴한다.
   int SetTimeInString_HMS( const char* s_string );
   // 주어진 문자열로부터 Year/Month/Day 값을 설정한다.
   // 문자열 포맷: "YYYY:MM:DD"
   // 주어진 문자열의 포맷이 잘못되었으면 0이 아닌 값을 리턴한다.
   int SetTimeInString_YMD( const char* s_string );
};

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

void Convert( Time& s_time, FILETIME& d_time );
void Convert( FILETIME& s_time, Time& d_time );
void Convert( SYSTEMTIME& s_time, FILETIME& d_time );
void Convert( FILETIME& s_time, SYSTEMTIME& d_time );
uint32_t GetTimerValue();
}

#endif

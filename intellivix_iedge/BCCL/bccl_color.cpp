﻿#include "bccl_color.h"
#include "bccl_ffmpeg.h"
#include "bccl_misc.h"

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__WIN32) && defined(__SIMD_MMX)
extern "C" void __cdecl mmx_YUY2toRGB24 (const byte *src,byte *dst,const byte *src_end,int stride);
#endif
 
 namespace BCCL

{
 uint GetYUY2ImageLength(int width, int heigth)

{
   return width * heigth * 2;
}

 uint GetNV12MImageLength(int width, int heigth)

{
   uint len = width * heigth;
   return len + (len / 2);
}

 void ClearImageBuffer_YUY2 (byte* s_buffer,int width,int height)

{
   int x,y;

   ushort* p = (ushort*)s_buffer;
   for (y = 0; y < height; y++) {
      for (x = 0; x < width; x++) {
         #if defined(__CPU_LITTLE_ENDIAN)
         *p++ = 0x8000;
         #else
         *p++ = 0x0080;
         #endif
      }
   }
}

 void ConvertBGRToHSV (BGRPixel& cp,int& ch,int& cs,int& cv)
// ch: 0 ~ 359, cs: 0 ~ 255, cv: 0 ~ 255 
{
   float h = 0.0f;
   int min_c = (int)GetMinimum (GetMinimum (cp.R,cp.G),cp.B);
   int max_c = (int)GetMaximum (GetMaximum (cp.R,cp.G),cp.B);
   int dm = max_c - min_c;
   if (max_c) cs = dm * 255 / max_c;
   else cs = 0;
   cv = max_c;
   if (dm) {
      if (cp.R == max_c) {
         h = (float)((int)cp.G - cp.B) / dm;
         if (h < 0.0f) h += 6.0f;
      }
      else if (cp.G == max_c) {
         h = (float)((int)cp.B - cp.R) / dm + 2.0f;
      }
      else {
         h = (float)((int)cp.R - cp.G) / dm + 4.0f;
      }
   }
   ch = (int)(60.0f * h + 0.5f);
   if (ch == 360) ch = 0;
}

 void ConvertBGRToYUV (BGRPixel &cp,byte &cy,byte &cu,byte &cv)
 
{
   // Y     = 0.299R + 0.587G + 0.114B
   // U(Cb) = 0.565(B - Y) + 128    = -0.169R - 0.332G + 0.501B + 128
   // V(Cr) = 0.713 * (R - Y) + 128 =  0.5R   - 0.419G - 0.081B + 128
   int ir = (int)cp.R, ig = (int)cp.G, ib = (int)cp.B;
   cy = (byte)((77 * ir + 150 * ig + 29 * ib) >> 8);
   cu = (byte)((32768 - 43  * ir - 85  * ig + 128 * ib) >> 8);
   cv = (byte)((32768 + 128 * ir - 107 * ig - 21  * ib) >> 8);
}

 void ConvertBGRToYUV (const BGRImage& s_cimage,YUVImage2& d_cimage)

{
   int x,y;
   
   BArray1D t_buffer(s_cimage.Width * s_cimage.Height * 2);
   ConvertBGRToYUY2 (s_cimage,t_buffer);
   byte* sp = t_buffer;
   for (y = 0; y < d_cimage.Height; y++) {
      byte* dp_y = d_cimage[0][y];
      byte* dp_u = d_cimage[1][y];
      byte* dp_v = d_cimage[2][y];
      for (x = 0; x < d_cimage.Width; x += 2) {
         dp_y[0] = sp[0];
         dp_y[1] = sp[2];
         dp_u[0] = sp[1];
         dp_u[1] = sp[1];
         dp_v[0] = sp[3];
         dp_v[1] = sp[3];
         sp   += 4;
         dp_y += 2;
         dp_u += 2;
         dp_v += 2;
      }
   }
}

 void ConvertBGRToYUY2 (const BGRImage& s_cimage,byte *d_buffer)
#if defined(__WIN32) && defined(__SIMD_MMX)
{
    #define RGB24     1
    #define DUPL      0
    #define SRC       eax
    #define DST       edi
    #define MATRIX    esi
    #define RGBOFFSET ecx
    #define YUVOFFSET edx

   __declspec(align(8)) static const __int64 cybgr_64[4] = { 0x000020DE40870C88,
                                                             0x0000175C4EA507ED,
                                                             0x000026464B230E97,
                                                             0x00001B335B92093B };

   __declspec(align(8)) static const __int64 fpix_mul[4] = { 0x0000503300003F74,
                                                             0x0000476400003C97,
                                                             0x00005AF1000047F4,
                                                             0x000050F3000044B4 };
   __declspec(align(8)) static const __int64 rb_mask       = 0x0000ffff0000ffff;
   __declspec(align(8)) static const __int64 fpix_add      = 0x0080800000808000;
   __declspec(align(8)) static const __int64 chroma_mask2  = 0xffff0000ffff0000;
   static const int y1y2_mult[4] = { 0x00004A85,
                                     0x00004A85,
                                     0x00004000,
                                     0x00004000 };
   static const int fraction[4]  = { 0x00084000,
                                     0x00084000,
                                     0x00004000,
                                     0x00004000 };
   static const int sub_32[4]    = { 0x0000FFE0,
                                     0x0000FFE0,
                                     0x00000000,
                                     0x00000000 };

   byte *src = (byte*)(BGRPixel*)s_cimage;
   byte *dst = (byte*)d_buffer;
   int src_pitch = s_cimage.GetLineLength (   );
   int dst_pitch = s_cimage.Width * 2;
   int w = s_cimage.Width;
   int h = s_cimage.Height;
   int matrix = 2;

#if RGB24
   int lwidth_bytes = w*3;    // Width in bytes
#else
   int lwidth_bytes = w<<2;   // Width in bytes
#endif
   for (int y = 0; y < h; y++) {
   __asm {
        mov         SRC,src
      mov         DST,dst
      mov         MATRIX,matrix
      mov         RGBOFFSET,0
      mov         YUVOFFSET,0
        
      movd        mm0,[fraction+MATRIX*4]
      movq        mm7,[cybgr_64+MATRIX*8]
      movd        mm5,[y1y2_mult+MATRIX*4]

      movq        mm2,[SRC+RGBOFFSET] ; mm2= XXR2 G2B2 XXR1 G1B1
      cmp         RGBOFFSET,[lwidth_bytes]
      punpcklbw   mm1,mm2             ; mm1= XXxx R1xx G1xx B1xx
#if RGB24
      psllq       mm2,8               ; Compensate for RGB24
#endif

      align 16
re_enter:
      punpckhbw   mm2,mm0             ; mm2= 00XX 00R2 00G2 00B2 
      psrlw       mm1,8               ; mm1= 00XX 00R1 00G1 00B1
      jge         outloop             ; Jump out of loop if true (width==0)

      movq        mm6,mm1             ; mm6= 00XX 00R1 00G1 00B1
      pmaddwd     mm1,mm7             ; mm1= v2v2 v2v2 v1v1 v1v1   y1 //(cyb*rgb[0] + cyg*rgb[1] + cyr*rgb[2] + 0x108000)
#if DUPL
      paddw       mm6,mm6             ; mm6 = accumulated RGB values (for b_y and r_y)
#else
      paddw       mm6,mm2             ; mm6 = accumulated RGB values (for b_y and r_y)
#endif
      pmaddwd     mm2,mm7             ; mm2= w2w2 w2w2 w1w1 w1w1   y2 //(cyb*rgbnext[0] + cyg*rgbnext[1] + cyr*rgbnext[2] + 0x108000)
      paddd       mm1,mm0             ; Add rounding fraction (16.5)<<15 to lower dword only
      paddd       mm2,mm0             ; Add rounding fraction (16.5)<<15 to lower dword only
      movq        mm3,mm1
      movq        mm4,mm2
      psrlq       mm3,32
      pand        mm6,[rb_mask]       ; Clear out accumulated G-value mm6= 0000 RRRR 0000 BBBB
      psrlq       mm4,32
      paddd       mm1,mm3
      paddd       mm2,mm4
      psrld       mm1,15              ; mm1= xxxx xxxx 0000 00y1 final value
      movd        mm3,[sub_32+MATRIX*4]; mm3 = -32
      psrld       mm2,15              ; mm2= xxxx xxxx 0000 00y2 final value
      paddw       mm3,mm1
      pslld       mm6,14              ; Shift up accumulated R and B values (<<15 in C)
#if DUPL
      paddw       mm3,mm1             ; mm3 = y1+y1-32
#else
      paddw       mm3,mm2             ; mm3 = y1+y2-32
#endif
      psllq       mm2,16              ; mm2 Y2 shifted up (to clear fraction) mm2 ready
      pmaddwd     mm3,mm5             ; mm3=scaled_y (latency 2 cycles)
      por         mm1,mm2             ; mm1 = 0000 0000 00Y2 00Y1
      punpckldq   mm3,mm3             ; Move scaled_y to upper dword mm3=SCAL ED_Y SCAL ED_Y 
      movq        mm2,[fpix_mul+MATRIX*8]
      psubd       mm6,mm3             ; mm6 = b_y and r_y
      movq        mm4,[fpix_add]
      psrad       mm6,14              ; Shift down b_y and r_y (>>10 in C-code) 
      movq        mm3,[chroma_mask2]
      pmaddwd     mm6,mm2             ; Mult b_y and r_y 
      add         YUVOFFSET,4         ; Two pixels (packed)
      paddd       mm6,mm4             ; Add 0x808000 to r_y and b_y 
#if RGB24
      add         RGBOFFSET,6
#else
      add         RGBOFFSET,8
#endif
      pand        mm6,mm3             ; Clear out fractions
      movq        mm2,[SRC+RGBOFFSET] ; mm2= XXR2 G2B2 XXR1 G1B1
      packuswb    mm6,mm6             ; mm6 = VV00 UU00 VV00 UU00
      cmp         RGBOFFSET,[lwidth_bytes]
      por         mm6,mm1             ; Or luma and chroma together         
      punpcklbw   mm1,mm2             ; mm1= XXxx R1xx G1xx B1xx
      movd        [DST+YUVOFFSET-4],mm6 ; Store final pixel                  
#if RGB24
      psllq       mm2,8               ; Compensate for RGB24
#endif
      jmp         re_enter            ; loop if true

      align 16
outloop:
      }
      src += src_pitch;
      dst += dst_pitch;
   }
   __asm {
      emms
   }

#undef SRC
#undef DST
#undef RGBOFFSET
#undef YUVOFFSET
#undef RGB24
#undef DUPL

}
#elif defined(__LIB_FFMPEG)
{
   int fr_width  = s_cimage.Width;
   int fr_height = s_cimage.Height;
   SwsContext* sws_ctx = sws_getContext (fr_width,fr_height,FFMPEG_PIXFMT_BGR24,
      fr_width,fr_height,FFMPEG_PIXFMT_YUY2,SWS_FAST_BILINEAR,NULL,NULL,NULL);
   if (sws_ctx == NULL) return;
   AVFrame* s_frame = av_frame_alloc (   );
   AVFrame* d_frame = av_frame_alloc (   );
   avpicture_fill ((AVPicture*)s_frame,(byte*)(BGRPixel*)s_cimage,FFMPEG_PIXFMT_BGR24,fr_width,fr_height);
   avpicture_fill ((AVPicture*)d_frame,d_buffer,FFMPEG_PIXFMT_YUY2,fr_width,fr_height);
   sws_scale (sws_ctx,s_frame->data,s_frame->linesize,0,fr_height,d_frame->data,d_frame->linesize);
   av_free (s_frame);
   av_free (d_frame);
   sws_freeContext (sws_ctx);
}
#else
{

}
#endif

 void ConvertBGRToYV12 (BGRImage &s_cimage,byte *d_buffer,int yv12_format)
 
{
   BArray1D t_buffer(s_cimage.Width * s_cimage.Height * 2);
   ConvertBGRToYUY2  (s_cimage,t_buffer);
   ConvertYUY2ToYV12 (t_buffer,s_cimage.Width,s_cimage.Height,d_buffer,yv12_format);
}

 void ConvertGrayToYUY2 (GImage& s_image,byte* d_buffer)

{
   int i,x,y;

   for (y = i = 0; y < s_image.Height; y++) {
      byte* l_s_image = s_image[y];
      for (x = 0; x < s_image.Width; x++) {
         d_buffer[i++] = l_s_image[x];
         d_buffer[i++] = 128;
      }
   }
}

 void ConvertHSVToBGR(byte ch,byte cs,byte cv,BGRPixel& cp)

{
    if (cs == 0) {
        cp.R = cv;
        cp.G = cv;
        cp.B = cv;        
    }
    byte region    = ch / 43;
    byte remainder = (ch - (region * 43)) * 6; 
    byte p = (byte)(((ushort)cv * (255 - cs)) >> 8);
    byte q = (byte)((cv * (255 - (((ushort)cs * remainder) >> 8))) >> 8);
    byte t = (byte)((cv * (255 - (((ushort)cs * (255 - remainder)) >> 8))) >> 8);
    switch (region) {
        case 0:
            cp.R = cv; cp.G = t; cp.B = p;
            break;
        case 1:
            cp.R = q; cp.G = cv; cp.B = p;
            break;
        case 2:
            cp.R = p; cp.G = cv; cp.B = t;
            break;
        case 3:
            cp.R = p; cp.G = q; cp.B = cv;
            break;
        case 4:
            cp.R = t; cp.G = p; cp.B = cv;
            break;
        default:
            cp.R = cv; cp.G = p; cp.B = q;
            break;
    }
}

 void ConvertYUVToBGR (byte cy,byte cu,byte cv,BGRPixel &cp)
  
{
   // U'= U - 128
   // V'= V - 128
   // R = Y + 1.403V'
   // G = Y - 0.344U' - 0.714V'
   // B = Y + 1.770U'
   int iy = (int)cy * 256;
   int iu = (int)cu - 128;
   int iv = (int)cv - 128;
   int ir = (iy + 359 * iv) >> 8;
   int ig = (iy - 88  * iu - 183 * iv) >> 8;
   int ib = (iy + 453 * iu) >> 8;
   if (ir < 0) cp.R = 0;
   else if (ir > 255) cp.R = 255;
   else cp.R = (byte)ir;
   if (ig < 0) cp.G = 0;
   else if (ig > 255) cp.G = 255;
   else cp.G = (byte)ig;
   if (ib < 0) cp.B = 0;
   else if (ib > 255) cp.B = 255;
   else cp.B = (byte)ib;
}

 void ConvertYUY2ToBGR (byte* s_buffer,BGRImage &d_cimage)
#if defined(__WIN32) && defined(__SIMD_MMX)
{
   const byte *src = (const byte*)s_buffer;
   byte *dst = (byte*)(BGRPixel*)d_cimage;
   int stride = d_cimage.Width * 2;
   const byte *src_end = src + stride * d_cimage.Height;
   mmx_YUY2toRGB24 (src,dst,src_end,stride);
}
#elif defined(__LIB_FFMPEG)
{
   int fr_width  = d_cimage.Width;
   int fr_height = d_cimage.Height;
   SwsContext* sws_ctx = sws_getContext (fr_width,fr_height,FFMPEG_PIXFMT_YUY2,
      fr_width,fr_height,FFMPEG_PIXFMT_BGR24,SWS_FAST_BILINEAR,NULL,NULL,NULL);
   if (sws_ctx == NULL) return;
   AVFrame* s_frame = av_frame_alloc (   );
   AVFrame* d_frame = av_frame_alloc (   );
   avpicture_fill ((AVPicture*)s_frame,s_buffer,FFMPEG_PIXFMT_YUY2,fr_width,fr_height);
   avpicture_fill ((AVPicture*)d_frame,(byte*)(BGRPixel*)d_cimage,FFMPEG_PIXFMT_BGR24,fr_width,fr_height);
   sws_scale (sws_ctx,s_frame->data,s_frame->linesize,0,fr_height,d_frame->data,d_frame->linesize);
   av_free (s_frame);
   av_free (d_frame);
   sws_freeContext (sws_ctx);
}
#else
{ 

}
#endif

 void ConvertYUY2ToGray (byte* s_buffer,GImage& d_image)

{
   int x,y;

   for (y = 0; y < d_image.Height; y++) {
      byte* dp = d_image[y];
      for (x = 0; x < d_image.Width; x++) {
         *dp++ = *s_buffer++;
         s_buffer++;
      }
   }
}

 void ConvertYUY2ToYUV (byte* s_buffer,YUVImage& d_cimage)
 
{
   int x,y;
   
   int ej = d_cimage.Width / 2;
   YUVPixel *d_buffer = d_cimage;
   for (y = 0; y < d_cimage.Height; y++) {
      for (x = 0; x < ej; x++) {
         d_buffer[0].Y = *s_buffer++;
         d_buffer[0].U = *s_buffer++;
         d_buffer[1].Y = *s_buffer++;
         d_buffer[1].V = *s_buffer++;
         d_buffer[0].V = d_buffer[1].V;
         d_buffer[1].U = d_buffer[0].U;
         d_buffer += 2;
      }
   }
}

 void ConvertYV12ToGray (byte* s_buffer,GImage& d_image,int yv12_format)

{
   int x,y;
   byte* s_buff_y;

   switch (yv12_format) {
      case YV12_FORMAT_OYTO:
         s_buff_y = s_buffer + 0x80000;
         break;
      case YV12_FORMAT_STANDARD:
      default:
         s_buff_y = s_buffer;
         break;
   }
   for (y = 0; y < d_image.Height; y++) {
      byte* dp = d_image[y];
      for (x = 0; x < d_image.Width; x++)
         *dp++ = *s_buff_y++;
   }
}

 void ConvertYUY2ToYV12 (byte* s_buffer,int width,int height,byte* d_buff_y,byte* d_buff_u,byte* d_buff_v)
 
{
   int x,y;
   
   int ej = width / 2;
   for (y = 0; y < height; y++) {
      if (y % 2) {
         for (x = 0; x < ej; x++) {
            *d_buff_y++ = *s_buffer++;
            *d_buff_u++ = *s_buffer++;
            *d_buff_y++ = *s_buffer++;
            *d_buff_v++ = *s_buffer++;
         }
      }
      else {
         for (x = 0; x < ej; x++) {
            *d_buff_y++ = *s_buffer;
            s_buffer += 2;
            *d_buff_y++ = *s_buffer;
            s_buffer += 2;
         }
      }
   }
}

 void ConvertYUY2ToYV12 (byte* s_buffer,int width,int height,byte* d_buffer,int yv12_format)
 
{
   byte *d_buff_u,*d_buff_v,*d_buff_y;
   switch (yv12_format) {
      case YV12_FORMAT_OYTO:
         d_buff_u = d_buffer;
         d_buff_v = d_buffer + 0x40000;
         d_buff_y = d_buffer + 0x80000;
         break;
      case YV12_FORMAT_HIKVISION:
         d_buff_y = d_buffer;
         d_buff_v = d_buff_y + width * height;
         d_buff_u = d_buff_v + width * height / 4;
         break;   
      case YV12_FORMAT_STANDARD:
      default:
         d_buff_y = d_buffer;
         d_buff_u = d_buff_y + width * height;
         d_buff_v = d_buff_u + width * height / 4;
         break;
   }
   ConvertYUY2ToYV12 (s_buffer,width,height,d_buff_y,d_buff_u,d_buff_v);
}

 void ConvertYV12ToBGR (byte* s_buffer,BGRImage& d_cimage,int yv12_format)
#if defined(__WIN32) && defined(__SIMD_MMX)
{
   #define MAXIMUM_Y_WIDTH 2048
   #define uint8_t  unsigned char
   #define uint64_t unsigned __int64

   static uint64_t mmw_mult_Y   = 0x2568256825682568;
   static uint64_t mmw_mult_U_G = 0xf36ef36ef36ef36e;
   static uint64_t mmw_mult_U_B = 0x40cf40cf40cf40cf;
   static uint64_t mmw_mult_V_R = 0x3343334333433343;
   static uint64_t mmw_mult_V_G = 0xe5e2e5e2e5e2e5e2;
   static uint64_t mmb_0x10     = 0x1010101010101010;
   static uint64_t mmw_0x0080   = 0x0080008000800080;
   static uint64_t mmw_0x00ff   = 0x00ff00ff00ff00ff;

   int y;
   int width       = d_cimage.Width;
   int height      = d_cimage.Height;
   int stride_y    = width;
   int stride_uv   = width >> 1;
   int stride_out  = width * 3;
   int horiz_count = -(width >> 3);
   byte *puc_u,*puc_v,*puc_y;
   switch (yv12_format) {
      case YV12_FORMAT_OYTO:
         puc_u = s_buffer;
         puc_v = s_buffer + 0x40000;
         puc_y = s_buffer + 0x80000;
         break;
      case YV12_FORMAT_STANDARD:
      default:
         puc_y = s_buffer;
         puc_u = puc_y + d_cimage.Width * d_cimage.Height;
         puc_v = puc_u + d_cimage.Width * d_cimage.Height / 4;
         break;
   }
   byte *puc_out = (byte*)(BGRPixel*)d_cimage;
   byte *puc_out_remembered;
   if (height < 0) {
      // we are flipping our output upside-down
      height    = -height;
      puc_y    += (height   - 1) * stride_y ;
      puc_u    += (height/2 - 1) * stride_uv;
      puc_v    += (height/2 - 1) * stride_uv;
      stride_y  = -stride_y;
      stride_uv = -stride_uv;
   }
   for (y = 0; y < height; y++) {
      if (y == height - 1) {
         // this is the last output line - we need to be careful not to overrun the end of this line
         uint8_t temp_buff[3 * MAXIMUM_Y_WIDTH + 1];
         puc_out_remembered = puc_out;
         puc_out = temp_buff; // write the RGB to a temporary store
      }
      _asm {
         push eax
         push ebx
         push ecx
         push edx
         push edi

         mov eax, puc_out       
         mov ebx, puc_y       
         mov ecx, puc_u       
         mov edx, puc_v
         mov edi, horiz_count
         
      horiz_loop:

         movd mm2, [ecx]
         pxor mm7, mm7

         movd mm3, [edx]
         punpcklbw mm2, mm7       ; mm2 = __u3__u2__u1__u0

         movq mm0, [ebx]          ; mm0 = y7y6y5y4y3y2y1y0  
         punpcklbw mm3, mm7       ; mm3 = __v3__v2__v1__v0

         movq mm1, mmw_0x00ff     ; mm1 = 00ff00ff00ff00ff 

         psubusb mm0, mmb_0x10    ; mm0 -= 16

         psubw mm2, mmw_0x0080    ; mm2 -= 128
         pand mm1, mm0            ; mm1 = __y6__y4__y2__y0

         psubw mm3, mmw_0x0080    ; mm3 -= 128
         psllw mm1, 3             ; mm1 *= 8

         psrlw mm0, 8             ; mm0 = __y7__y5__y3__y1
         psllw mm2, 3             ; mm2 *= 8

         pmulhw mm1, mmw_mult_Y   ; mm1 *= luma coeff 
         psllw mm0, 3             ; mm0 *= 8

         psllw mm3, 3             ; mm3 *= 8
         movq mm5, mm3            ; mm5 = mm3 = v

         pmulhw mm5, mmw_mult_V_R ; mm5 = red chroma
         movq mm4, mm2            ; mm4 = mm2 = u

         pmulhw mm0, mmw_mult_Y   ; mm0 *= luma coeff 
         movq mm7, mm1            ; even luma part

         pmulhw mm2, mmw_mult_U_G ; mm2 *= u green coeff 
         paddsw mm7, mm5          ; mm7 = luma + chroma    __r6__r4__r2__r0

         pmulhw mm3, mmw_mult_V_G ; mm3 *= v green coeff  
         packuswb mm7, mm7        ; mm7 = r6r4r2r0r6r4r2r0

         pmulhw mm4, mmw_mult_U_B ; mm4 = blue chroma
         paddsw mm5, mm0          ; mm5 = luma + chroma    __r7__r5__r3__r1

         packuswb mm5, mm5        ; mm6 = r7r5r3r1r7r5r3r1
         paddsw mm2, mm3          ; mm2 = green chroma

         movq mm3, mm1            ; mm3 = __y6__y4__y2__y0
         movq mm6, mm1            ; mm6 = __y6__y4__y2__y0

         paddsw mm3, mm4          ; mm3 = luma + chroma    __b6__b4__b2__b0
         paddsw mm6, mm2          ; mm6 = luma + chroma    __g6__g4__g2__g0
         
         punpcklbw mm7, mm5       ; mm7 = r7r6r5r4r3r2r1r0
         paddsw mm2, mm0          ; odd luma part plus chroma part    __g7__g5__g3__g1

         packuswb mm6, mm6        ; mm2 = g6g4g2g0g6g4g2g0
         packuswb mm2, mm2        ; mm2 = g7g5g3g1g7g5g3g1

         packuswb mm3, mm3        ; mm3 = b6b4b2b0b6b4b2b0
         paddsw mm4, mm0          ; odd luma part plus chroma part    __b7__b5__b3__b1

         packuswb mm4, mm4        ; mm4 = b7b5b3b1b7b5b3b1
         punpcklbw mm6, mm2       ; mm6 = g7g6g5g4g3g2g1g0

         punpcklbw mm3, mm4       ; mm3 = b7b6b5b4b3b2b1b0

         // 32-bit shuffle....
         pxor mm0, mm0            ; is this needed?

         movq mm1, mm6            ; mm1 = g7g6g5g4g3g2g1g0
         punpcklbw mm1, mm0       ; mm1 = __g3__g2__g1__g0

         movq mm0, mm3            ; mm0 = b7b6b5b4b3b2b1b0
         punpcklbw mm0, mm7       ; mm0 = r3b3r2b2r1b1r0b0

         movq mm2, mm0            ; mm2 = r3b3r2b2r1b1r0b0

         punpcklbw mm0, mm1       ; mm0 = __r1g1b1__r0g0b0
         punpckhbw mm2, mm1       ; mm2 = __r3g3b3__r2g2b2

         // 24-bit shuffle and save...
         movd   [eax], mm0        ; eax[0] = __r0g0b0
         psrlq mm0, 32            ; mm0 = __r1g1b1

         movd  3[eax], mm0        ; eax[3] = __r1g1b1

         movd  6[eax], mm2        ; eax[6] = __r2g2b2
         

         psrlq mm2, 32            ; mm2 = __r3g3b3
   
         movd  9[eax], mm2        ; eax[9] = __r3g3b3

         // 32-bit shuffle....
         pxor mm0, mm0            ; is this needed?

         movq mm1, mm6            ; mm1 = g7g6g5g4g3g2g1g0
         punpckhbw mm1, mm0       ; mm1 = __g7__g6__g5__g4

         movq mm0, mm3            ; mm0 = b7b6b5b4b3b2b1b0
         punpckhbw mm0, mm7       ; mm0 = r7b7r6b6r5b5r4b4

         movq mm2, mm0            ; mm2 = r7b7r6b6r5b5r4b4

         punpcklbw mm0, mm1       ; mm0 = __r5g5b5__r4g4b4
         punpckhbw mm2, mm1       ; mm2 = __r7g7b7__r6g6b6

         // 24-bit shuffle and save...
         movd 12[eax], mm0        ; eax[12] = __r4g4b4
         psrlq mm0, 32            ; mm0 = __r5g5b5
         
         movd 15[eax], mm0        ; eax[15] = __r5g5b5
         add ebx, 8               ; puc_y   += 8;

         movd 18[eax], mm2        ; eax[18] = __r6g6b6
         psrlq mm2, 32            ; mm2 = __r7g7b7
         
         add ecx, 4               ; puc_u   += 4;
         add edx, 4               ; puc_v   += 4;

         movd 21[eax], mm2        ; eax[21] = __r7g7b7
         add eax, 24              ; puc_out += 24

         inc edi
         jne horiz_loop         

         pop edi 
         pop edx 
         pop ecx
         pop ebx 
         pop eax

         emms
      }
      if (y == height - 1) {
         // last line of output - we have used the temp_buff and need to copy...
         int x = 3 * width;                     // interation counter
         uint8_t *ps = puc_out;                 // source pointer (temporary line store)
         uint8_t *pd = puc_out_remembered;      // dest pointer
         while (x--) *(pd++) = *(ps++);         // copy the line
      }
      puc_y += stride_y;
      if (y % 2) {
         puc_u += stride_uv;
         puc_v += stride_uv;
      }
      puc_out += stride_out; 
   }
}
#else
{
   BArray1D t_buffer(d_cimage.Width * d_cimage.Height * 2);
   ConvertYV12ToYUY2 (s_buffer,d_cimage.Width,d_cimage.Height,t_buffer,yv12_format);
   ConvertYUY2ToBGR  (t_buffer,d_cimage);
}
#endif

 void ConvertYV12ToYUV2 (byte* s_buff_y,byte* s_buff_u,byte* s_buff_v,int width,int height,int stride_y,int stride_uv,byte* d_buffer)

{
   int x,y;
   
   int ex = width / 2;
   for (y = 0; y < height; y++) {
      for (x = 0; x < ex; x++) {
         *d_buffer++ = *s_buff_y++;
         *d_buffer++ = *s_buff_u++;
         *d_buffer++ = *s_buff_y++;
         *d_buffer++ = *s_buff_v++;
      }
      if (!(y % 2)) {
         s_buff_u -= ex;
         s_buff_v -= ex;
      }
      else {
         s_buff_u += stride_uv - ex;
         s_buff_v += stride_uv - ex;
      }
      s_buff_y += stride_y  - width;
   }
}

 void ConvertYV12ToYUY2 (byte *s_buffer,int width,int height,int stride_y,int stride_uv,byte *d_buffer,int yv12_format)

{
   byte *s_buff_u,*s_buff_v,*s_buff_y;
   switch (yv12_format) {
      case YV12_FORMAT_OYTO:
         s_buff_u = s_buffer;
         s_buff_v = s_buffer + 0x40000;
         s_buff_y = s_buffer + 0x80000;
         break;
      case YV12_FORMAT_HIKVISION:
         s_buff_y = s_buffer;
         s_buff_v = s_buff_y + stride_y * height;
         s_buff_u = s_buff_v + stride_y * height / 4;
         break;
      case YV12_FORMAT_STANDARD:
      default:
         s_buff_y = s_buffer;
         s_buff_u = s_buff_y + stride_y * height;
         s_buff_v = s_buff_u + stride_y * height / 4;
         break;
   }
   ConvertYV12ToYUV2 (s_buff_y,s_buff_u,s_buff_v,width,height,stride_y,stride_uv,d_buffer);
}

 void ConvertYV12ToYUY2 (byte *s_buffer,int width,int height,byte *d_buffer,int yv12_format)

{
   ConvertYV12ToYUY2 (s_buffer,width,height,width,width / 2,d_buffer,yv12_format);
}

 void ConvertUYVYToYUY2  (byte *s_buffer,int width,int height,byte *d_buffer)

{
   int i,j;

   for (i = 0; i < height; i++) {
      byte* src_line = s_buffer + i * width * 2;
      byte* dst_line = d_buffer + i * width * 2;
      for (j = 0; j < width; j++) {
         dst_line[0] = src_line[1];
         dst_line[1] = src_line[0];
         src_line += 2;
         dst_line += 2;
      }
   }
}

 void ConvertNV12MToYUY2(byte* s_buffer_y, byte* s_buffer_uv, int width, int height, byte* d_buffer)

{
   int nIndex = 0;
   for (int y = 0; y < height; y++)
   {
      for (int x = 0; x < width; x += 2)
      {
         byte y1 = s_buffer_y [y * width + x];
         byte y2 = s_buffer_y [y * width + x + 1];
         byte u  = s_buffer_uv[(y / 2 * width + x)];
         byte v  = s_buffer_uv[(y / 2 * width + x + 1)];

         d_buffer[nIndex++] = y1;
         d_buffer[nIndex++] = u;
         d_buffer[nIndex++] = y2;
         d_buffer[nIndex++] = v;
      }
   }
}

 void ConvertNV12MToGray(byte* s_buffer_y, int width, int height, byte* d_buffer)

{
   memcpy(d_buffer, s_buffer_y, width * height);
}

 int GetSaturation (BGRColor& color)

{
   int sat;

   int min_c = (int)GetMinimum (GetMinimum (color.B,color.G),color.R);
   int max_c = (int)GetMaximum (GetMaximum (color.B,color.G),color.R);
   if (max_c) sat = (max_c - min_c) * 255 / max_c;
   else sat = 0;
   return (sat);
}

 void ReplaceYComponents_YUY2 (byte* s_buffer,GImage& s_image)

{
   int x,y;

   for (y = 0; y < s_image.Height; y++) {
      byte* l_s_image = s_image[y];
      for (x = 0; x < s_image.Width; x++) {
         *s_buffer = l_s_image[x];
         s_buffer += 2;
      }
   }
}

}

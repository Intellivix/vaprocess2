#include "bccl_ctime.h"

#if !defined(__WIN32)

BOOL FileTimeToLocalFileTime(CONST FILETIME *lpFileTime, LPFILETIME lpLocalFileTime)
{
   return FALSE;
}

BOOL FileTimeToSystemTime(CONST FILETIME *lpFileTime, LPSYSTEMTIME lpSystemTime)
{
   return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// CTimeSpan
/////////////////////////////////////////////////////////////////////////////

 CTimeSpan::CTimeSpan() throw() :
   m_timeSpan(0)
 {
 }

 CTimeSpan::CTimeSpan(_In_ time_t time) throw() :
   m_timeSpan( time )
 {
 }

 CTimeSpan::CTimeSpan(
   _In_ LONG lDays,
   _In_ int nHours,
   _In_ int nMins,
   _In_ int nSecs) throw()
 {
   m_timeSpan = nSecs + 60* (nMins + 60* (nHours + 24* lDays));
 }

 LONG CTimeSpan::GetDays() const throw()
 {
   return( m_timeSpan/(24*3600) );
 }

 LONG CTimeSpan::GetTotalHours() const throw()
 {
   return( m_timeSpan/3600 );
 }

 LONG CTimeSpan::GetHours() const throw()
 {
   return( LONG( GetTotalHours()-(GetDays()*24) ) );
 }

 LONG CTimeSpan::GetTotalMinutes() const throw()
 {
   return( m_timeSpan/60 );
 }

 LONG CTimeSpan::GetMinutes() const throw()
 {
   return( LONG( GetTotalMinutes()-(GetTotalHours()*60) ) );
 }

 LONG CTimeSpan::GetTotalSeconds() const throw()
 {
   return( m_timeSpan );
 }

 LONG CTimeSpan::GetSeconds() const throw()
 {
   return( LONG( GetTotalSeconds()-(GetTotalMinutes()*60) ) );
 }

 time_t CTimeSpan::GetTimeSpan() const throw()
 {
   return( m_timeSpan );
 }

 CTimeSpan CTimeSpan::operator+(_In_ CTimeSpan span) const throw()
 {
   return( CTimeSpan( m_timeSpan+span.m_timeSpan ) );
 }

 CTimeSpan CTimeSpan::operator-(_In_ CTimeSpan span) const throw()
 {
   return( CTimeSpan( m_timeSpan-span.m_timeSpan ) );
 }

 CTimeSpan& CTimeSpan::operator+=(_In_ CTimeSpan span) throw()
 {
   m_timeSpan += span.m_timeSpan;
   return( *this );
 }

 CTimeSpan& CTimeSpan::operator-=(_In_ CTimeSpan span) throw()
 {
   m_timeSpan -= span.m_timeSpan;
   return( *this );
 }

 bool CTimeSpan::operator==(_In_ CTimeSpan span) const throw()
 {
   return( m_timeSpan == span.m_timeSpan );
 }

 bool CTimeSpan::operator!=(_In_ CTimeSpan span) const throw()
 {
   return( m_timeSpan != span.m_timeSpan );
 }

 bool CTimeSpan::operator<(_In_ CTimeSpan span) const throw()
 {
   return( m_timeSpan < span.m_timeSpan );
 }

 bool CTimeSpan::operator>(_In_ CTimeSpan span) const throw()
 {
   return( m_timeSpan > span.m_timeSpan );
 }

 bool CTimeSpan::operator<=(_In_ CTimeSpan span) const throw()
 {
   return( m_timeSpan <= span.m_timeSpan );
 }

 bool CTimeSpan::operator>=(_In_ CTimeSpan span) const throw()
 {
   return( m_timeSpan >= span.m_timeSpan );
 }

/////////////////////////////////////////////////////////////////////////////
// CTime
/////////////////////////////////////////////////////////////////////////////

 CTime CTime::GetCurrentTime() throw()
 {
   return( CTime( ::time( NULL ) ) );
 }

 BOOL CTime::IsValidFILETIME(_In_ const FILETIME& fileTime) throw()
 {
   FILETIME localTime;
   if (!FileTimeToLocalFileTime(&fileTime, &localTime))
   {
      return FALSE;
   }

   // then convert that time to system time
   SYSTEMTIME sysTime;
   if (!FileTimeToSystemTime(&localTime, &sysTime))
   {
      return FALSE;
   }

   return TRUE;
 }

 CTime::CTime() throw() :
   m_time(0)
 {
 }

 CTime::CTime(_In_ time_t time)  throw():
   m_time( time )
 {
 }

 CTime::CTime(
   _In_ int nYear,
   _In_ int nMonth,
   _In_ int nDay,
   _In_ int nHour,
   _In_ int nMin,
   _In_ int nSec,
   _In_ int nDST)
 {
 
   ATLENSURE( nYear >= 1900 );
   ATLENSURE( nMonth >= 1 && nMonth <= 12 );
   ATLENSURE( nDay >= 1 && nDay <= 31 );
   ATLENSURE( nHour >= 0 && nHour <= 23 );
   ATLENSURE( nMin >= 0 && nMin <= 59 );
   ATLENSURE( nSec >= 0 && nSec <= 59 );

   struct tm atm;

   atm.tm_sec = nSec;
   atm.tm_min = nMin;
   atm.tm_hour = nHour;
   atm.tm_mday = nDay;
   atm.tm_mon = nMonth - 1;        // tm_mon is 0 based
   atm.tm_year = nYear - 1900;     // tm_year is 1900 based
   atm.tm_isdst = nDST;

   m_time = mktime(&atm);
   ATLASSUME(m_time != -1);       // indicates an illegal input time
   if(m_time == -1)
   {
      AtlThrow(E_INVALIDARG);
   }
 }

 CTime::CTime(
   _In_ WORD wDosDate,
   _In_ WORD wDosTime,
   _In_ int nDST)
 {
   struct tm atm;
   atm.tm_sec = (wDosTime & ~0xFFE0) << 1;
   atm.tm_min = (wDosTime & ~0xF800) >> 5;
   atm.tm_hour = wDosTime >> 11;

   atm.tm_mday = wDosDate & ~0xFFE0;
   atm.tm_mon = ((wDosDate & ~0xFE00) >> 5) - 1;
   atm.tm_year = (wDosDate >> 9) + 80;
   atm.tm_isdst = nDST;
   m_time = mktime(&atm);
   ATLASSUME(m_time != -1);       // indicates an illegal input time

   if(m_time == -1)
      AtlThrow(E_INVALIDARG);

 }

 CTime::CTime(
   _In_ const SYSTEMTIME& sysTime,
   _In_ int nDST)
 {
   if (sysTime.wYear < 1900)
   {
      time_t time0 = 0L;
      CTime timeT(time0);
      *this = timeT;
   }
   else
   {
      CTime timeT(
         (int)sysTime.wYear, (int)sysTime.wMonth, (int)sysTime.wDay,
         (int)sysTime.wHour, (int)sysTime.wMinute, (int)sysTime.wSecond,
         nDST);
      *this = timeT;
   }
 }

 CTime::CTime(
   _In_ const FILETIME& fileTime,
   _In_ int nDST)
 {
   // first convert file time (UTC time) to local time
   FILETIME localTime;
   if (!FileTimeToLocalFileTime(&fileTime, &localTime))
   {
      m_time = 0;
      AtlThrow(E_INVALIDARG);
      return;
   }

   // then convert that time to system time
   SYSTEMTIME sysTime;
   if (!FileTimeToSystemTime(&localTime, &sysTime))
   {
      m_time = 0;
      AtlThrow(E_INVALIDARG);
      return;
   }

   // then convert the system time to a time_t (C-runtime local time)
   CTime timeT(sysTime, nDST);
   *this = timeT;
 }

 CTime& CTime::operator=(_In_ time_t time) throw()
 {
   m_time = time;

   return( *this );
 }

 CTime& CTime::operator+=(_In_ CTimeSpan span) throw()
 {
   m_time += span.GetTimeSpan();

   return( *this );
 }

 CTime& CTime::operator-=(_In_ CTimeSpan span) throw()
 {
   m_time -= span.GetTimeSpan();

   return( *this );
 }

 CTimeSpan CTime::operator-(_In_ CTime time) const throw()
 {
   return( CTimeSpan( m_time-time.m_time ) );
 }

 CTime CTime::operator-(_In_ CTimeSpan span) const throw()
 {
   return( CTime( m_time-span.GetTimeSpan() ) );
 }

 CTime CTime::operator+(_In_ CTimeSpan span) const throw()
 {
   return( CTime( m_time+span.GetTimeSpan() ) );
 }

 bool CTime::operator==(_In_ CTime time) const throw()
 {
   return( m_time == time.m_time );
 }

 bool CTime::operator!=(_In_ CTime time) const throw()
 {
   return( m_time != time.m_time );
 }

 bool CTime::operator<(_In_ CTime time) const throw()
 {
   return( m_time < time.m_time );
 }

 bool CTime::operator>(_In_ CTime time) const throw()
 {
   return( m_time > time.m_time );
 }

 bool CTime::operator<=(_In_ CTime time) const throw()
 {
   return( m_time <= time.m_time );
 }

 bool CTime::operator>=(_In_ CTime time) const throw()
 {
   return( m_time >= time.m_time );
 }

 struct tm* CTime::GetGmtTm(_Out_ struct tm* ptm) const
 {
   // Ensure ptm is valid
   ATLENSURE( ptm != NULL );

   if (ptm != NULL)
   {
      struct tm* ptmTemp = gmtime(&m_time);
      *ptm = *ptmTemp;
      return ptm;
   }

   return NULL;
 }

 struct tm* CTime::GetLocalTm(_Out_ struct tm* ptm) const
 {
   // Ensure ptm is valid
   ATLENSURE( ptm != NULL );

   if (ptm != NULL)
   {
      struct tm *ptmTemp = localtime (&m_time);
      *ptm = *ptmTemp;
      return ptm;
   }

   return NULL;
 }

 bool CTime::GetAsSystemTime(_Out_ SYSTEMTIME& timeDest) const throw()
 {
   struct tm ttm;
   struct tm* ptm;

   ptm = GetLocalTm(&ttm);
   if(!ptm)
   {
      return false;
   }

   timeDest.wYear = (WORD) (1900 + ptm->tm_year);
   timeDest.wMonth = (WORD) (1 + ptm->tm_mon);
   timeDest.wDayOfWeek = (WORD) ptm->tm_wday;
   timeDest.wDay = (WORD) ptm->tm_mday;
   timeDest.wHour = (WORD) ptm->tm_hour;
   timeDest.wMinute = (WORD) ptm->tm_min;
   timeDest.wSecond = (WORD) ptm->tm_sec;
   timeDest.wMilliseconds = 0;

   return true;
 }

 time_t CTime::GetTime() const throw()
 {
   return( m_time );
 }

 int CTime::GetYear() const throw()
 {
   struct tm ttm;
   struct tm * ptm;

   ptm = GetLocalTm(&ttm);
   return ptm ? (ptm->tm_year) + 1900 : 0 ;
 }

 int CTime::GetMonth() const throw()
 {
   struct tm ttm;
   struct tm * ptm;

   ptm = GetLocalTm(&ttm);
   return ptm ? ptm->tm_mon + 1 : 0;
 }

 int CTime::GetDay() const throw()
 {
   struct tm ttm;
   struct tm * ptm;

   ptm = GetLocalTm(&ttm);
   return ptm ? ptm->tm_mday : 0 ;
 }

 int CTime::GetHour() const throw()
 {
   struct tm ttm;
   struct tm * ptm;

   ptm = GetLocalTm(&ttm);
   return ptm ? ptm->tm_hour : -1 ;
 }

 int CTime::GetMinute() const throw()
 {
   struct tm ttm;
   struct tm * ptm;

   ptm = GetLocalTm(&ttm);
   return ptm ? ptm->tm_min : -1 ;
 }

 int CTime::GetSecond() const throw()
 {
   struct tm ttm;
   struct tm * ptm;

   ptm = GetLocalTm(&ttm);
   return ptm ? ptm->tm_sec : -1 ;
 }

 int CTime::GetDayOfWeek() const throw()
 {
   struct tm ttm;
   struct tm * ptm;

   ptm = GetLocalTm(&ttm);
   return ptm ? ptm->tm_wday + 1 : 0 ;
 }

#endif

﻿#include "bccl_xml.h"

#include "bccl_console.h"
#include "bccl_geomobj.h"

#include <iostream>
#include <cstring>
#include <utility>
#include <map>

namespace BCCL {

////////////////////////////////////////////////////////////////////////
/// Class : CXMLIO
////////////////////////////////////////////////////////////////////////

CXMLIO::CXMLIO( std::string filename, XMLIOMode enMode )
{
   ResetFile( std::move( filename ), enMode );
}

CXMLIO::CXMLIO( FileIO* pFileIO, XMLIOMode enMode )
{
   SetFileIO( pFileIO, enMode );
}

PugiXmlHelper* CXMLIO::Pugi()
{
   return &m_pugiXmlHelper;
}

bool CXMLIO::ResetFile( std::string filename, XMLIOMode enMode )
{
   bool bRet = false;
   m_pFileIO = nullptr;

   m_mode     = enMode;
   m_filename = std::move( filename );

   Pugi()->Reset();

   // new way.
   if( m_mode == XMLIO_Read ) {
      bRet = Pugi()->ParseFromFile( m_filename.c_str() );
   } else if( m_mode == XMLIO_Write ) {
      bRet = true;
   }
   Pugi()->PushParent( Pugi()->GetXmlDocument() );

   return bRet;
}

bool CXMLIO::ResetMemory( FileIO* pFileIO, XMLIOMode enMode )
{
   assert( pFileIO );

   bool bRet = false;
   m_filename.clear();
   m_pFileIO = pFileIO;
   m_mode    = enMode;

   Pugi()->Reset();

   if( m_mode == XMLIO_Read ) {
      Pugi()->ParseFromMemory( reinterpret_cast<char*>( pFileIO->GetBuffer() ), static_cast<uint>( pFileIO->GetLength() ) );
   } else if( m_mode == XMLIO_Write ) {
      bRet = true;
   }

   Pugi()->PushParent( Pugi()->GetXmlDocument() );

   return bRet;
}

// use SetFileIO only for memory(FIO_STORAGE_MEMORY) mode.
bool CXMLIO::SetFileIO( FileIO* pFileIO, XMLIOMode enMode )
{
   if( pFileIO == nullptr ) {
      return false;
   }

   if( pFileIO->GetStorageType() == FIO_STORAGE_FILE ) {
      assert( false );
   }

   return ResetMemory( pFileIO, enMode );
}

FIO_STORAGE CXMLIO::GetStorageType()
{
   if( m_pFileIO == nullptr ) {
      // This is direct to file mode, so return FIO_STORAGE_FILE.
      return FIO_STORAGE_FILE;
   }

   return m_pFileIO->GetStorageType(); // FIO_STORAGE_MEMORY
}

////////////////////////////////////////////////////////////////////////
/// Class : CXMLNode
////////////////////////////////////////////////////////////////////////

CXMLNode::CXMLNode( CXMLIO* pXMLIO )
{
   m_nState = 0;
   m_pXMLIO = pXMLIO;
   SetXMLIO( pXMLIO );
}

CXMLNode::~CXMLNode()
{
   End();
}

void CXMLNode::SetXMLIO( CXMLIO* pXMLIO )
{
   if( m_pXMLIO != nullptr && m_pXMLIO != pXMLIO ) {
      LOGW << "Are you sure to change m_pXMLIO before END()?";
      End();
   }
   if( pXMLIO != nullptr ) {
      m_pXMLIO = pXMLIO;
   }
   m_nState = 0;
}

bool CXMLNode::Start( const char* node_name )
{
   if( m_pXMLIO->m_mode == XMLIO_Read ) {
      if( ReadStart( node_name ) ) {
         return true;
      }
      ReadEnd();
   } else if( m_pXMLIO->m_mode == XMLIO_Write ) {
      if( WriteStart( node_name ) ) {
         return true;
      }
      WriteEnd();
   }
   return false;
}

void CXMLNode::End()
{
   if( m_pXMLIO != nullptr ) {
      if( m_pXMLIO->m_mode == XMLIO_Read ) {
         ReadEnd();
      } else if( m_pXMLIO->m_mode == XMLIO_Write ) {
         WriteEnd();
      }
   }
   m_node_current.~xml_node();
}

bool CXMLNode::ReadStart( const char* szNodeName )
{
   if( ( m_nState & XMLNodeState_ReadStart ) != 0 ) {
      //LOGE << "was already XMLNodeState_ReadStart. looking:" << szNodeName;
      return false;
   }

   std::string child_name( szNodeName );

   m_node_current = Pugi()->GetParent().GetChildUnRead( child_name );
   if( m_node_current.empty() ) {
		// TODO(yhpark): xml tmp log off.
      //LOGW << "not found child node[" << szNodeName << "] in [" << Pugi()->GetParent().path() << "]";
      return false;
   }

   Pugi()->GetParent().SetChildReadDone( child_name, m_node_current );
   Pugi()->PushParent( m_node_current );
   m_nState |= XMLNodeState_ReadStart;
   return true;
}

void CXMLNode::ReadEnd()
{
   if( ( m_nState & XMLNodeState_ReadStart ) != 0 ) {
      if( m_node_current.empty() == false ) {
         Pugi()->PopParent();
      }
      m_nState &= ~( XMLNodeState_ReadStart );
   }
}

bool CXMLNode::WriteStart( const char* szNodeName )
{
   if( ( m_nState & XMLNodeState_WriteStart ) != 0 ) {
      return false;
   }
   m_node_current = Pugi()->GetParent().append_child( szNodeName );
   if( m_node_current.empty() ) {
      return false;
   }

   Pugi()->PushParent( m_node_current );
   m_nState |= XMLNodeState_WriteStart;
   return true;
}

void CXMLNode::WriteEnd()
{
   if( ( m_nState & XMLNodeState_WriteStart ) == 0 ) {
      return;
   }

   if( m_node_current.empty() == false ) {
      Pugi()->PopParent();

      // pugi::node_document is root node, it menas the node's depth(level) is 0.
      if( Pugi()->GetParent().type() == pugi::node_document ) {
         if( m_pXMLIO->m_mode == XMLIO_Write ) {
            if( m_pXMLIO->m_filename.empty() == false ) {
               Pugi()->WriteToFile( m_pXMLIO->m_filename.c_str() );
            } else if( m_pXMLIO->m_pFileIO != nullptr ) {
               Pugi()->WriteToMemory( m_pXMLIO->m_pFileIO );
            } else {
               assert( false );
            }
         }
      }
   }

   m_nState &= ~( XMLNodeState_WriteStart );
}

bool CXMLNode::Attribute( int nTypeID, const char* szAttrName, void* pVar, int nNum )
{
   if( m_pXMLIO->m_mode == XMLIO_Read ) {
      return ReadAttr( nTypeID, szAttrName, pVar, nullptr, nullptr, nNum );
   }
   if( m_pXMLIO->m_mode == XMLIO_Write ) {
      return WriteAttr( nTypeID, szAttrName, pVar, nullptr, nullptr, nNum );
   }
   return false;
}

bool CXMLNode::Attribute( int nTypeID, const char* szAttrName, void* pVar, void* pDefVar, int nNum )
{
   if( m_pXMLIO->m_mode == XMLIO_Read ) {
      return ReadAttr( nTypeID, szAttrName, pVar, pDefVar, nullptr, nNum );
   }
   if( m_pXMLIO->m_mode == XMLIO_Write ) {
      return WriteAttr( nTypeID, szAttrName, pVar, pDefVar, nullptr, nNum );
   }
   return false;
}

bool CXMLNode::Attribute( int nTypeID, const char* szAttrName, void* pVar, void* pDefVar, char* szFormat, int nNum )
{
   if( m_pXMLIO->m_mode == XMLIO_Read ) {
      return ReadAttr( nTypeID, szAttrName, pVar, pDefVar, nullptr, nNum ); // 일단 읽기에서는 Format을 사용하지 않는다.
   }
   if( m_pXMLIO->m_mode == XMLIO_Write ) {
      return WriteAttr( nTypeID, szAttrName, pVar, pDefVar, szFormat, nNum );
   }
   return false;
}

bool CXMLNode::TextValue( int nTypeID, void* pVar, int nNum )
{
   if( m_pXMLIO->m_mode == XMLIO_Read ) {
      return ReadNodeValue( nTypeID, pVar, nullptr, nullptr, nNum );
   }
   if( m_pXMLIO->m_mode == XMLIO_Write ) {
      return WriteNodeValue( nTypeID, pVar, nullptr, nullptr, nNum );
   }
   return false;
}

bool CXMLNode::TextValue( int nTypeID, void* pVar, void* pDefVar, int nNum )
{
   if( m_pXMLIO->m_mode == XMLIO_Read ) {
      return ReadNodeValue( nTypeID, pVar, pDefVar, nullptr, nNum );
   }
   if( m_pXMLIO->m_mode == XMLIO_Write ) {
      return WriteNodeValue( nTypeID, pVar, pDefVar, nullptr, nNum );
   }
   return false;
}

bool CXMLNode::TextValue( int nTypeID, void* pVar, void* pDefVar, char* szFormat, int nNum )
{
   if( m_pXMLIO->m_mode == XMLIO_Read ) {
      return ReadNodeValue( nTypeID, pVar, pDefVar, nullptr, nNum ); // 일단 읽기에서는 Format을 사용하지 않는다.
   }
   if( m_pXMLIO->m_mode == XMLIO_Write ) {
      return WriteNodeValue( nTypeID, pVar, pDefVar, szFormat, nNum );
   }
   return false;
}

bool CXMLNode::ReadAttr( int nTypeID, const char* szAttrName, void* pVar, void* pDefVar, char* szFormat, int nNum )
{
   if( m_node_current.empty() ) {
      LOGE << "current node is empty. path: " << m_node_current.path();
      return false;
   }

   pugi::xml_attribute attr_cur = m_node_current.attribute( szAttrName );
   if( attr_cur.empty() ) {
      return false;
   }

   return ConvertStringToValueType( nTypeID, attr_cur.value(), pVar, pDefVar, szFormat, nNum );
}

bool CXMLNode::ReadNodeValue( int nTypeID, void* pVar, void* pDefVar, char* szFormat, int nNum )
{
   if( m_node_current.empty() ) {
      LOGE << "current node is empty. path: " << m_node_current.path();
      return false;
   }

   pugi::xml_text xml_text = m_node_current.text();
   if( xml_text.empty() ) {
      LOGW << "no pcdata(text) in node[" << m_node_current.name() << "]";
   }

   return ConvertStringToValueType( nTypeID, xml_text.get(), pVar, pDefVar, szFormat, nNum );
}

bool CXMLNode::WriteAttr( int nTypeID, const char* szAttrName, void* pVar, void* pDefVar, char* szFormat, int nNum )
{
   if( m_node_current.empty() ) {
      LOGE << "current node is empty. path: " << m_node_current.path();
      return false;
   }

   pugi::xml_attribute attribute = m_node_current.append_attribute( szAttrName );
   if( attribute.empty() ) {
      LOGE << "pugi failed to make attribute[" << szAttrName << "in node[" << m_node_current.name() << "]";
      return false;
   }

   std::string strValue;
   ConvertValueTypeToString( nTypeID, strValue, pVar, pDefVar, szFormat, nNum );
   if( attribute.set_value( strValue.c_str() ) == false ) {
      LOGE << "pugi failed to make attribute" << szAttrName << "]'s value in " << m_node_current.path();
      return false;
   }

   return true;
}

bool CXMLNode::WriteNodeValue( int nTypeID, void* pVar, void* pDefVar, char* szFormat, int nNum )
{
   if( m_node_current.empty() ) {
      LOGE << "current node is empty. path: " << m_node_current.path();
      return false;
   }

   std::string strValue;
   ConvertValueTypeToString( nTypeID, strValue, pVar, pDefVar, szFormat, nNum );
   if( m_node_current.text().set( strValue.c_str() ) == false ) {
      LOGE << "pugi failed to make pcdata[" << strValue << "]. in " << m_node_current.path();
      return false;
   }

   return true;
}

PugiXmlHelper* CXMLNode::Pugi()
{
   return &m_pXMLIO->m_pugiXmlHelper;
}

} // namespace BCCL

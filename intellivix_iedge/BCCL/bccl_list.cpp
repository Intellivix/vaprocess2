#include "bccl_list.h"
#include "bccl_console.h"

 namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: ParameterList
//
///////////////////////////////////////////////////////////////////////////////

 void ParameterList::GetParameterName (const char *s_string,int si,int ei,Parameter *param)
 
{
   int n = ei - si + 1;
   param->Name.Create (n);
   CopyString (s_string + si,n - 1,param->Name,n);
}

 int ParameterList::GetParameterValue (const char *s_string,int si,int ei,Parameter *param)
 
{
   int i = param->Dimension;
   if (i >= MAX_PARAM_DIMENSION) return (1);
   int n = ei - si + 1;
   param->Values[i].Create (n);
   CopyString (s_string + si,n - 1,param->Values[i],n);
   param->Dimension++;
   return (DONE);
}

 int ParameterList::Parse (const char *s_string,int flag_init)
 
{
   int i,si;
   int r_code = DONE;
   
   if (flag_init) Delete (   );
   if (s_string == NULL) return (1);
   Parameter *param = new Parameter;
   int flag_number = FALSE;
   for (i = si = 0;   ; i++) {
      char c = s_string[i];
      if (!c) {
         if (flag_number) {
            if (GetParameterValue (s_string,si,i,param)) r_code = 3;
            this->Add (param);
         }
         else delete param;
         break;
      }
      else if (c == '=') {
         if (!flag_number) {
            GetParameterName (s_string,si,i,param);
            si = i + 1;
            flag_number = TRUE;
         }
      }
      else if (c == ',') {
         if (flag_number) {
            if (GetParameterValue (s_string,si,i,param)) r_code = 3;
            si = i + 1;
         }
      }
      else if (c == '&' || c == '\r' || c == '\n') {
         if (flag_number) {
            if (GetParameterValue (s_string,si,i,param)) r_code = 3;
            si = i + 1;
            this->Add (param);
            param = new Parameter;
            flag_number = FALSE;
         }
      }
   }
   if (!this->GetNumItems (   )) return (2);
   else return (r_code);
}

 void ParameterList::Print (   )
 
{
   int i;
   
   logd ("[Parameter List (%d parameter(s))]\n",GetNumItems());
   Parameter *param = this->First (   );
   while (param != NULL) {
      logd ("%20s = ",(char*)param->Name);
      int ei = param->Dimension - 1;
      for (i = 0; i < param->Dimension; i++) {
         logd ("%6s",(char*)param->Values[i]);
         if (i != ei) logd (",");
      }
      logd ("\n");
      param = this->Next (param);
   }
}

}

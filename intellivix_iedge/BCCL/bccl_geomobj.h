#if !defined(__BCCL_GEOMOBJ_H)
#define __BCCL_GEOMOBJ_H

#include "bccl_define.h"

 namespace BCCL

{

//
// WARNING: Don't add any virtual member function to the following classes.
//          Adding virtual member functions causes the change of class instance size.
//

///////////////////////////////////////////////////////////////////////////////
//
// Class Template: Point2D<T>
//
///////////////////////////////////////////////////////////////////////////////

 template<class T>
 class Point2D

{
   public:
      T X,Y;

   public:
      Point2D (   );
      Point2D (T x,T y);

   public:
      Point2D  operator +  (const Point2D &s_point);
      Point2D  operator -  (   );
      Point2D  operator -  (const Point2D &s_point);
      Point2D  operator *  (const Point2D &s_point);
      Point2D  operator *  (T a);
      Point2D  operator /  (const Point2D &s_point);
      Point2D  operator /  (T a);
      Point2D& operator += (const Point2D &s_point);
      Point2D& operator -= (const Point2D &s_point);
      Point2D& operator *= (const Point2D &s_point);
      Point2D& operator *= (T a);
      Point2D& operator /= (const Point2D &s_point);
      Point2D& operator /= (T a);
            T& operator [] (int i) const;
         void  operator () (T x,T y);
         int   operator == (const Point2D &s_point);
         int   operator != (const Point2D &s_point);

   public:
      friend Point2D operator * (T a,const Point2D &s_point)
     {
         Point2D<T> d_point;
         d_point.X = a * s_point.X;
         d_point.Y = a * s_point.Y;
         return (d_point);
     };

   public:
      void Clear (   );
};

typedef Point2D<double> DPoint2D;
typedef Point2D<float>  FPoint2D;
typedef Point2D<int>    IPoint2D;
typedef Point2D<short>  SPoint2D;
typedef Point2D<ushort> USPoint2D;

 template<class T>
 Point2D<T> Point2D<T>::operator + (const Point2D &s_point)

{
   Point2D d_point;
   d_point.X = X + s_point.X;
   d_point.Y = Y + s_point.Y;
   return (d_point);
}

 template<class T>
 Point2D<T> Point2D<T>::operator - (   )

{
   Point2D d_point;
   d_point.X = -X;
   d_point.Y = -Y;
   return (d_point);
}

 template<class T>
 Point2D<T> Point2D<T>::operator - (const Point2D &s_point)

{
   Point2D d_point;
   d_point.X = X - s_point.X;
   d_point.Y = Y - s_point.Y;
   return (d_point);
}

 template<class T>
 Point2D<T> Point2D<T>::operator * (const Point2D &s_point)

{
   Point2D d_point;
   d_point.X = X * s_point.X;
   d_point.Y = Y * s_point.Y;
   return (d_point);
}

 template<class T>
 Point2D<T> Point2D<T>::operator * (T a)

{
   Point2D d_point;
   d_point.X = X * a;
   d_point.Y = Y * a;
   return (d_point);
}

 template<class T>
 Point2D<T> Point2D<T>::operator / (const Point2D &s_point)

{
   Point2D d_point;
   d_point.X = X / s_point.X;
   d_point.Y = Y / s_point.Y;
   return (d_point);
}

 template<class T>
 Point2D<T> Point2D<T>::operator / (T a)

{
   Point2D d_point;
   d_point.X = X / a;
   d_point.Y = Y / a;
   return (d_point);
}

 template<class T>
 Point2D<T>& Point2D<T>::operator += (const Point2D &s_point)

{
   X += s_point.X;
   Y += s_point.Y;
   return (*this);
}

 template<class T>
 Point2D<T>& Point2D<T>::operator -= (const Point2D &s_point)

{
   X -= s_point.X;
   Y -= s_point.Y;
   return (*this);
}

 template<class T>
 Point2D<T>& Point2D<T>::operator *= (const Point2D &s_point)

{
   X *= s_point.X;
   Y *= s_point.Y;
   return (*this);
}

 template<class T>
 Point2D<T>& Point2D<T>::operator *= (T a)

{
   X *= a;
   Y *= a;
   return (*this);
}

 template<class T>
 Point2D<T>& Point2D<T>::operator /= (const Point2D &s_point)

{
   X /= s_point.X;
   Y /= s_point.Y;
   return (*this);
}

 template<class T>
 Point2D<T>& Point2D<T>::operator /= (T a)

{
   X /= a;
   Y /= a;
   return (*this);
}

 template<class T>
 inline T& Point2D<T>::operator [] (int i) const

{
   return (((T*)this)[i]);
}

 template<class T>
 inline void Point2D<T>::operator () (T x,T y)

{
   X = x;
   Y = y;
}

 template<class T>
 inline int Point2D<T>::operator == (const Point2D &s_point)

{
   return !(*this != s_point);
}

 template<class T>
 inline int Point2D<T>::operator != (const Point2D &s_point)

{
   if (X != s_point.X) return (1);
   if (Y != s_point.Y) return (1);
   return (0);
}

 template<class T>
 Point2D<T>::Point2D (   )

{
   Clear (   );
}

 template<class T>
 Point2D<T>::Point2D (T x,T y)

{
   X = x;
   Y = y;
}

 template<class T>
 inline void Point2D<T>::Clear (   )

{
   X = Y = (T)0;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class Template: Point3D<T>
//
///////////////////////////////////////////////////////////////////////////////

 template<class T>
 class Point3D

{
   public:
      T X,Y,Z;

   public:
      Point3D (   );
      Point3D (T x,T y,T z);

   public:
      Point3D  operator +  (const Point3D &s_point);
      Point3D  operator -  (   );
      Point3D  operator -  (const Point3D &s_point);
      Point3D  operator *  (const Point3D &s_point);
      Point3D  operator *  (T a);
      Point3D  operator /  (const Point3D &s_point);
      Point3D  operator /  (T a);
      Point3D& operator += (const Point3D &s_point);
      Point3D& operator -= (const Point3D &s_point);
      Point3D& operator *= (const Point3D &s_point);
      Point3D& operator *= (T a);
      Point3D& operator /= (const Point3D &s_point);
      Point3D& operator /= (T a);
           T&  operator [] (int i) const;
         void  operator () (T x,T y,T z);

   public:
      friend Point3D operator * (T a,const Point3D &s_point)
     {
         Point3D<T> d_point;
         d_point.X = a * s_point.X;
         d_point.Y = a * s_point.Y;
         d_point.Z = a * s_point.Z;
         return (d_point);
     };

   public:
       void Clear (   );
};

typedef Point3D<double> DPoint3D;
typedef Point3D<float>  FPoint3D;
typedef Point3D<int>    IPoint3D;
typedef Point3D<short>  SPoint3D;
typedef Point3D<ushort> USPoint3D;

 template<class T>
 Point3D<T> Point3D<T>::operator + (const Point3D &s_point)

{
   Point3D d_point;
   d_point.X = X + s_point.X;
   d_point.Y = Y + s_point.Y;
   d_point.Z = Z + s_point.Z;
   return (d_point);
}

 template<class T>
 Point3D<T> Point3D<T>::operator - (   )

{
   Point3D d_point;
   d_point.X = -X;
   d_point.Y = -Y;
   d_point.Z = -Z;
   return (d_point);
}

 template<class T>
 Point3D<T> Point3D<T>::operator - (const Point3D &s_point)

{
   Point3D d_point;
   d_point.X = X - s_point.X;
   d_point.Y = Y - s_point.Y;
   d_point.Z = Z - s_point.Z;
   return (d_point);
}

 template<class T>
 Point3D<T> Point3D<T>::operator * (const Point3D &s_point)

{
   Point3D d_point;
   d_point.X = X * s_point.X;
   d_point.Y = Y * s_point.Y;
   d_point.Z = Z * s_point.Z;
   return (d_point);
}

 template<class T>
 Point3D<T> Point3D<T>::operator * (T a)

{
   Point3D d_point;
   d_point.X = X * a;
   d_point.Y = Y * a;
   d_point.Z = Z * a;
   return (d_point);
}

 template<class T>
 Point3D<T> Point3D<T>::operator / (const Point3D &s_point)

{
   Point3D d_point;
   d_point.X = X / s_point.X;
   d_point.Y = Y / s_point.Y;
   d_point.Z = Z / s_point.Z;
   return (d_point);
}

 template<class T>
 Point3D<T> Point3D<T>::operator / (T a)

{
   Point3D d_point;
   d_point.X = X / a;
   d_point.Y = Y / a;
   d_point.Z = Z / a;
   return (d_point);
}

 template<class T>
 Point3D<T>& Point3D<T>::operator += (const Point3D &s_point)

{
   X += s_point.X;
   Y += s_point.Y;
   Z += s_point.Z;
   return (*this);
}

 template<class T>
 Point3D<T>& Point3D<T>::operator -= (const Point3D &s_point)

{
   X -= s_point.X;
   Y -= s_point.Y;
   Z -= s_point.Z;
   return (*this);
}

 template<class T>
 Point3D<T>& Point3D<T>::operator *= (const Point3D &s_point)

{
   X *= s_point.X;
   Y *= s_point.Y;
   Z *= s_point.Z;
   return (*this);
}

 template<class T>
 Point3D<T>& Point3D<T>::operator *= (T a)

{
   X *= a;
   Y *= a;
   Z *= a;
   return (*this);
}

 template<class T>
 Point3D<T>& Point3D<T>::operator /= (const Point3D &s_point)

{
   X /= s_point.X;
   Y /= s_point.Y;
   Z /= s_point.Z;
   return (*this);
}

 template<class T>
 Point3D<T>& Point3D<T>::operator /= (T a)

{
   X /= a;
   Y /= a;
   Z /= a;
   return (*this);
}

 template<class T>
 inline T& Point3D<T>::operator [] (int i) const

{
   return (((T*)this)[i]);
}

 template<class T>
 inline void Point3D<T>::operator () (T x,T y,T z)

{
   X = x;
   Y = y;
   Z = z;
}

 template<class T>
 Point3D<T>::Point3D (   )

{
   Clear (   );
}

 template<class T>
 Point3D<T>::Point3D (T x,T y,T z)

{
   X = x;
   Y = y;
   Z = z;
}

 template<class T>
 inline void Point3D<T>::Clear (   )

{
   X = Y = Z = (T)0;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class Template: Size2D<T>
//
///////////////////////////////////////////////////////////////////////////////

 template<class T>
 class Size2D

{
   public:
      T Width,Height;

   public:
      Size2D (   );
      Size2D (T width,T height);

   public:
      Size2D  operator +  (const Size2D &s_size);
      Size2D  operator -  (   );
      Size2D  operator -  (const Size2D &s_size);
      Size2D  operator *  (const Size2D &s_size);
      Size2D  operator *  (T a);
      Size2D  operator /  (const Size2D &s_size);
      Size2D  operator /  (T a);
      Size2D& operator += (const Size2D &s_size);
      Size2D& operator -= (const Size2D &s_size);
      Size2D& operator *= (const Size2D &s_size);
      Size2D& operator *= (T a);
      Size2D& operator /= (const Size2D &s_size);
      Size2D& operator /= (T a);
           T& operator [] (int i) const;
        void  operator () (T width,T height);

   public:
      friend Size2D operator * (T a,const Size2D &s_size)
     {
        Size2D<T> d_size;
        d_size.Width  = a * s_size.Width;
        d_size.Height = a * s_size.Height;
        return (d_size);
     };

   public:
      void Clear (   );
};

typedef Size2D<double> DSize2D;
typedef Size2D<float>  FSize2D;
typedef Size2D<int>    ISize2D;
typedef Size2D<short>  SSize2D;
typedef Size2D<ushort> USSize2D;

 template<class T>
 Size2D<T> Size2D<T>::operator + (const Size2D &s_size)

{
   Size2D d_size;
   d_size.Width  = Width  + s_size.Width;
   d_size.Height = Height + s_size.Height;
   return (d_size);
}

 template<class T>
 Size2D<T> Size2D<T>::operator - (   )

{
   Size2D d_size;
   d_size.Width  = -Width;
   d_size.Height = -Height;
   return (d_size);
}

 template<class T>
 Size2D<T> Size2D<T>::operator - (const Size2D &s_size)

{
   Size2D d_size;
   d_size.Width  = Width  - s_size.Width;
   d_size.Height = Height - s_size.Height;
   return (d_size);
}

 template<class T>
 Size2D<T> Size2D<T>::operator * (const Size2D &s_size)

{
   Size2D d_size;
   d_size.Width  = Width  * s_size.Width;
   d_size.Height = Height * s_size.Height;
   return (d_size);
}

 template<class T>
 Size2D<T> Size2D<T>::operator * (T a)

{
   Size2D d_size;
   d_size.Width  = Width  * a;
   d_size.Height = Height * a;
   return (d_size);
}

 template<class T>
 Size2D<T> Size2D<T>::operator / (const Size2D &s_size)

{
   Size2D d_size;
   d_size.Width  = Width  / s_size.Width;
   d_size.Height = Height / s_size.Height;
   return (d_size);
}

 template<class T>
 Size2D<T> Size2D<T>::operator / (T a)

{
   Size2D d_size;
   d_size.Width  = Width  / a;
   d_size.Height = Height / a;
   return (d_size);
}

 template<class T>
 Size2D<T>& Size2D<T>::operator += (const Size2D &s_size)

{
   Width  += s_size.Width;
   Height += s_size.Height;
   return (*this);
}

 template<class T>
 Size2D<T>& Size2D<T>::operator -= (const Size2D &s_size)

{
   Width  -= s_size.Width;
   Height -= s_size.Height;
   return (*this);
}

 template<class T>
 Size2D<T>& Size2D<T>::operator *= (const Size2D &s_size)

{
   Width  *= s_size.Width;
   Height *= s_size.Height;
   return (*this);
}

 template<class T>
 Size2D<T>& Size2D<T>::operator *= (T a)

{
   Width  *= a;
   Height *= a;
   return (*this);
}

 template<class T>
 Size2D<T>& Size2D<T>::operator /= (const Size2D &s_size)

{
   Width  /= s_size.Width;
   Height /= s_size.Height;
   return (*this);
}

 template<class T>
 Size2D<T>& Size2D<T>::operator /= (T a)

{
   Width  /= a;
   Height /= a;
   return (*this);
}

 template<class T>
 inline T& Size2D<T>::operator [] (int i) const

{
   return (((T*)this)[i]);
}

 template<class T>
 inline void Size2D<T>::operator () (T width,T height)

{
   Width  = width;
   Height = height;
}

 template<class T>
 Size2D<T>::Size2D (   )

{
   Clear (   );
}

 template<class T>
 Size2D<T>::Size2D (T width,T height)

{
   Width  = width;
   Height = height;
}

 template<class T>
 inline void Size2D<T>::Clear (   )

{
   Width = Height = (T)0;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class Template: Size3D<T>
//
///////////////////////////////////////////////////////////////////////////////

 template<class T>
 class Size3D

{
   public:
      T Width,Height,Depth;

   public:
      Size3D (   );
      Size3D (T width,T height,T depth);

   public:
      Size3D  operator +  (const Size3D &s_size);
      Size3D  operator -  (   );
      Size3D  operator -  (const Size3D &s_size);
      Size3D  operator *  (const Size3D &s_size);
      Size3D  operator *  (T a);
      Size3D  operator /  (const Size3D &s_size);
      Size3D  operator /  (T a);
      Size3D& operator += (const Size3D &s_size);
      Size3D& operator -= (const Size3D &s_size);
      Size3D& operator *= (const Size3D &s_size);
      Size3D& operator *= (T a);
      Size3D& operator /= (const Size3D &s_size);
      Size3D& operator /= (T a);
           T& operator [] (int i) const;
        void  operator () (T width,T height,T depth);

   public:
      friend Size3D operator * (T a,const Size3D &s_size)
     {
        Size3D<T> d_size;
        d_size.Width  = a * s_size.Width;
        d_size.Height = a * s_size.Height;
        d_size.Depth  = a * s_size.Depth;
        return (d_size);
     };

   public:
      void Clear (   );
};

typedef Size3D<double> DSize3D;
typedef Size3D<float>  FSize3D;
typedef Size3D<int>    ISize3D;
typedef Size3D<short>  SSize3D;
typedef Size3D<ushort> USSize3D;

 template<class T>
 Size3D<T> Size3D<T>::operator + (const Size3D &s_size)

{
   Size3D d_size;
   d_size.Width  = Width  + s_size.Width;
   d_size.Height = Height + s_size.Height;
   d_size.Depth  = Depth  + s_size.Depth;
   return (d_size);
}

 template<class T>
 Size3D<T> Size3D<T>::operator - (   )

{
   Size3D d_size;
   d_size.Width  = -Width;
   d_size.Height = -Height;
   d_size.Depth  = -Depth;
   return (d_size);
}

 template<class T>
 Size3D<T> Size3D<T>::operator - (const Size3D &s_size)

{
   Size3D d_size;
   d_size.Width  = Width  - s_size.Width;
   d_size.Height = Height - s_size.Height;
   d_size.Depth  = Depth  - s_size.Depth;
   return (d_size);
}

 template<class T>
 Size3D<T> Size3D<T>::operator * (const Size3D &s_size)

{
   Size3D d_size;
   d_size.Width  = Width  * s_size.Width;
   d_size.Height = Height * s_size.Height;
   d_size.Depth  = Depth  * s_size.Depth;
   return (d_size);
}

 template<class T>
 Size3D<T> Size3D<T>::operator * (T a)

{
   Size3D d_size;
   d_size.Width  = Width  * a;
   d_size.Height = Height * a;
   d_size.Depth  = Depth  * a;
   return (d_size);
}

 template<class T>
 Size3D<T> Size3D<T>::operator / (const Size3D &s_size)

{
   Size3D d_size;
   d_size.Width  = Width  / s_size.Width;
   d_size.Height = Height / s_size.Height;
   d_size.Depth  = Depth  / s_size.Depth;
   return (d_size);
}

 template<class T>
 Size3D<T> Size3D<T>::operator / (T a)

{
   Size3D d_size;
   d_size.Width  = Width  / a;
   d_size.Height = Height / a;
   d_size.Depth  = Depth  / a;
   return (d_size);
}

 template<class T>
 Size3D<T>& Size3D<T>::operator += (const Size3D &s_size)

{
   Width  += s_size.Width;
   Height += s_size.Height;
   Depth  += s_size.Depth;
   return (*this);
}

 template<class T>
 Size3D<T>& Size3D<T>::operator -= (const Size3D &s_size)

{
   Width  -= s_size.Width;
   Height -= s_size.Height;
   Depth  -= s_size.Depth;
   return (*this);
}

 template<class T>
 Size3D<T>& Size3D<T>::operator *= (const Size3D &s_size)

{
   Width  *= s_size.Width;
   Height *= s_size.Height;
   Depth  *= s_size.Depth;
   return (*this);
}

 template<class T>
 Size3D<T>& Size3D<T>::operator *= (T a)

{
   Width  *= a;
   Height *= a;
   Depth  *= a;
   return (*this);
}

 template<class T>
 Size3D<T>& Size3D<T>::operator /= (const Size3D &s_size)

{
   Width  /= s_size.Width;
   Height /= s_size.Height;
   Depth  /= s_size.Depth;
   return (*this);
}

 template<class T>
 Size3D<T>& Size3D<T>::operator /= (T a)

{
   Width  /= a;
   Height /= a;
   Depth  /= a;
   return (*this);
}

 template<class T>
 inline T& Size3D<T>::operator [] (int i) const

{
   return (((T*)this)[i]);
}

 template<class T>
 inline void Size3D<T>::operator () (T width,T height,T depth)

{
   Width  = width;
   Height = height;
   Depth  = depth;
}

 template<class T>
 Size3D<T>::Size3D (   )

{
   Clear (   );
}

 template<class T>
 Size3D<T>::Size3D (T width,T height,T depth)

{
   Width  = width;
   Height = height;
   Depth  = depth;
}

 template<class T>
 inline void Size3D<T>::Clear (   )

{
   Width = Height = Depth = (T)0;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class Template: Box2D<T>
//
///////////////////////////////////////////////////////////////////////////////

 template<class T>
 class Box2D

{
   public:
      T X,Y;
      T Width,Height;

   public:
      Box2D (   );
      Box2D (T x,T y,T width,T height);
      Box2D (const Box2D& s_box);

   public:
      void operator () (T x,T y,T width,T height);
      void operator () (const Box2D& s_box);
      int  operator == (const Box2D& s_box);
      int  operator != (const Box2D& s_box);

   public:
      void Clear (   );

   public:
      Box2D    Clip              (T x,T y,T width,T height);
      FPoint2D GetCenterPosition (   );
      void     GetCornerPoints   (Point2D<T> *d_points,float scale = 1.0f);
      FPoint2D GetPosition       (float rx,float ry);
      Box2D    Inflate           (FPoint2D &scale);
      int      IsPointInsideBox  (FPoint2D &s_point);
      Box2D    Scale             (FPoint2D &scale);
      void     Set               (Box2D& s_box);
};

typedef Box2D<double> DBox2D;
typedef Box2D<float>  FBox2D;
typedef Box2D<int>    IBox2D;
typedef Box2D<short>  SBox2D;
typedef Box2D<ushort> USBox2D;

 template<class T>
 inline void Box2D<T>::operator () (T x,T y,T width,T height)

{
   X = x;
   Y = y;
   Width  = width;
   Height = height;
}

 template<class T>
 inline void Box2D<T>::operator () (const Box2D& s_box)

{
   X      = s_box.X;
   Y      = s_box.Y;
   Width  = s_box.Width;
   Height = s_box.Height;
}

 template<class T>
 inline int Box2D<T>::operator == (const Box2D& s_box)

{
   return !(*this != s_box);
}

 template<class T>
 inline int Box2D<T>::operator != (const Box2D& s_box)

{
   if (X      != s_box.X     ) return (1);
   if (Y      != s_box.Y     ) return (1);
   if (Width  != s_box.Width ) return (1);
   if (Height != s_box.Height) return (1);
   return (0);
}

 template<class T>
 Box2D<T>::Box2D (   )

{
   Clear (   );
}

 template<class T>
 Box2D<T>::Box2D (T x,T y,T width,T height)

{
   X = x;
   Y = y;
   Width  = width;
   Height = height;
}

 template<class T>
 Box2D<T>::Box2D (const Box2D& s_box)

{
   X      = s_box.X;
   Y      = s_box.Y;
   Width  = s_box.Width;
   Height = s_box.Height;
}

 template<class T>
 inline void Box2D<T>::Clear (   )

{
   X = Y = Width = Height = (T)0;
}
 
 template<class T>
 Box2D<T> Box2D<T>::Clip (T x,T y,T width,T height)
 
{
   T sx1 = X;
   T sy1 = Y;
   T ex1 = X + Width;
   T ey1 = Y + Height;
   T ex2 = x + width;
   T ey2 = y + height;
   if (sx1 < x  ) sx1 = x;
   if (sy1 < y  ) sy1 = y;
   if (ex1 > ex2) ex1 = ex2;
   if (ey1 > ey2) ey1 = ey2;
   Box2D<T> d_box;
   d_box.X      = sx1;
   d_box.Y      = sy1;
   d_box.Width  = ex1 - sx1;
   d_box.Height = ey1 - sy1;
   return (d_box);
} 

 template<class T>
 FPoint2D Box2D<T>::GetCenterPosition (   )

{
   FPoint2D pos;
   pos.X = (float)(X + 0.5f * Width );
   pos.Y = (float)(Y + 0.5f * Height);
   return (pos);
}

 template<class T>
 void Box2D<T>::GetCornerPoints (Point2D<T> *d_points,float scale)
 
{
   double cx = X + 0.5 * Width;
   double cy = Y + 0.5 * Height;
   double hw = 0.5 * scale * Width;
   double hh = 0.5 * scale * Height;
   d_points[0].X = (T)(cx - hw);
   d_points[0].Y = (T)(cy - hh);
   d_points[1].X = (T)(cx + hw);
   d_points[1].Y = (T)(cy - hh);
   d_points[2].X = (T)(cx + hw);
   d_points[2].Y = (T)(cy + hh);
   d_points[3].X = (T)(cx - hw);
   d_points[3].Y = (T)(cy + hh);
}

 template<class T>
 FPoint2D Box2D<T>::GetPosition (float rx,float ry)

{
   FPoint2D pos;
   pos.X = (float)(X + rx * Width );
   pos.Y = (float)(Y + ry * Height);
   return (pos);
}

 template<class T>
 Box2D<T> Box2D<T>::Inflate (FPoint2D &scale)

{
   Box2D<T> d_box;
   double cx = X + 0.5 * (double)Width;
   double cy = Y + 0.5 * (double)Height;
   double nw = scale.X * (double)Width;
   double nh = scale.Y * (double)Height;
   d_box.X      = (T)(cx - 0.5 * nw);
   d_box.Y      = (T)(cy - 0.5 * nh);
   d_box.Width  = (T)nw;
   d_box.Height = (T)nh;
   return (d_box);
}
 
 template<class T>
 int Box2D<T>::IsPointInsideBox (FPoint2D &s_point)
 
{
   float sx = (float)X;
   float sy = (float)Y;
   float ex = (float)(sx + Width  - 1.0f);
   float ey = (float)(sy + Height - 1.0f);
   if (sx <= s_point.X && s_point.X <= ex && sy <= s_point.Y && s_point.Y <= ey) return (TRUE);
   else return (FALSE);
}

 template<class T>
 Box2D<T> Box2D<T>::Scale (FPoint2D &scale)
 
{
   Box2D<T> d_box;
   d_box.X      = (T)(X * scale.X);
   d_box.Y      = (T)(Y * scale.Y);
   d_box.Width  = (T)(Width  * scale.X);
   d_box.Height = (T)(Height * scale.Y);
   return (d_box);
}

 template<class T>
 void Box2D<T>::Set (Box2D& s_box)

{
   X      = s_box.X;
   Y      = s_box.Y;
   Width  = s_box.Width;
   Height = s_box.Height;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class Template: Box3D<T>
//
///////////////////////////////////////////////////////////////////////////////

 template<class T>
 class Box3D

{
   public:
      T X,Y,Z;
      T Width,Height,Depth;

   public:
      Box3D (   );
      Box3D (T x,T y,T z,T width,T height,T depth);

   public:
      void operator () (T x,T y,T z,T width,T height,T depth);

   public:
      void Clear (   );

   public:
      Box3D    Clip              (T x,T y,T z,T width,T height,T depth);
      FPoint3D GetCenterPosition (   );
      int      IsPointInsideBox  (FPoint3D &s_point);
      Box3D    Scale             (FPoint3D &scale);
      
};

typedef Box3D<double> DBox3D;
typedef Box3D<float>  FBox3D;
typedef Box3D<int>    IBox3D;
typedef Box3D<short>  SBox3D;
typedef Box3D<ushort> USBox3D;

 template<class T>
 inline void Box3D<T>::operator () (T x,T y,T z,T width,T height,T depth)

{
   X = x;
   Y = y;
   Z = z;
   Width  = width;
   Height = height;
   Depth  = depth;
}

 template<class T>
 Box3D<T>::Box3D (   )

{
   Clear (   );
}

 template<class T>
 Box3D<T>::Box3D (T x,T y,T z,T width,T height,T depth)

{
   X = x;
   Y = y;
   Z = z;
   Width  = width;
   Height = height;
   Depth  = depth;
}

 template<class T>
 inline void Box3D<T>::Clear (   )
 
{
   X = Y = Z = Width = Height = Depth = (T)0;
}

 template<class T>
 Box3D<T> Box3D<T>::Clip (T x,T y,T z,T width,T height,T depth)
 
{
   T sx1 = X;
   T sy1 = Y;
   T sz1 = Z;
   T ex1 = X + Width;
   T ey1 = Y + Height;
   T ez1 = Z + Depth;
   T ex2 = x + width;
   T ey2 = y + height;
   T ez2 = z + depth;
   if (sx1 < x  ) sx1 = x;
   if (sy1 < y  ) sy1 = y;
   if (sz1 < z  ) sz1 = z;
   if (ex1 > ex2) ex1 = ex2;
   if (ey1 > ey2) ey1 = ey2;
   if (ez1 > ez2) ez1 = ez2;
   Box3D<T> d_box;
   d_box.X      = sx1;
   d_box.Y      = sy1;
   d_box.Z      = sz1;
   d_box.Width  = ex1 - sx1;
   d_box.Height = ey1 - sy1;
   d_box.Depth  = ez1 - sz1;
   return (d_box);
} 

 template<class T>
 FPoint3D Box3D<T>::GetCenterPosition (   )

{
   FPoint3D cp;
   cp.X = (float)(X + 0.5f * Width);
   cp.Y = (float)(Y + 0.5f * Height);
   cp.Z = (float)(Z + 0.5f * Depth);
   return (cp);
}

 template<class T>
 int Box3D<T>::IsPointInsideBox (FPoint3D &s_point)
 
{
   float sx = (float)X;
   float sy = (float)Y;
   float sz = (float)Z;
   float ex = (float)(sx + Width  - 1.0f);
   float ey = (float)(sy + Height - 1.0f);
   float ez = (float)(sz + Depth  - 1.0f);
   if (sx <= s_point.X && s_point.X <= ex && sy <= s_point.Y && s_point.Y <= ey && sz <= s_point.Z && s_point.Z <= ez) return (TRUE);
   else return (FALSE);
}

 template<class T>
 Box3D<T> Box3D<T>::Scale (FPoint3D &scale)
 
{
   Box3D<T> d_box;
   d_box.X      = (T)(X * scale.X);
   d_box.Y      = (T)(Y * scale.Y);
   d_box.Z      = (T)(Z * scale.Z);
   d_box.Width  = (T)(Width  * scale.X);
   d_box.Height = (T)(Height * scale.Y);
   d_box.Depth  = (T)(Depth  * scale.Z);
   return (d_box);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class Template: Line2D<T>
//
///////////////////////////////////////////////////////////////////////////////

 template<class T>
 class Line2D

{
   protected:
      Point2D<T> Points[2];

   public:
      Line2D (   );
      Line2D (T x1,T y1,T x2,T y2);
      Line2D (Point2D<T> &s_point1,Point2D<T> &s_point2);

   public:
      void        operator () (T x1,T y1,T x2,T y2);
      void        operator () (Point2D<T> &s_point1,Point2D<T> &s_point2);
      Point2D<T>& operator [] (int i);
      void        operator = (Line2D<T> &s_line);

   public:
      void Clear (   );

   public:
      FPoint2D GetCenterPosition (   );
      float    GetLength         (   );
      void     GetLineEquation   (float &a,float &b,float &c);
      float    GetSquaredLength  (   );
};

typedef Line2D<double> DLine2D;
typedef Line2D<float>  FLine2D;
typedef Line2D<int>    ILine2D;
typedef Line2D<short>  SLine2D;
typedef Line2D<ushort> USLine2D;

 template<class T>
 inline void Line2D<T>::operator () (T x1,T y1,T x2,T y2)

{
   Points[0](x1,y1);
   Points[1](x2,y2);
}

 template<class T>
 inline void Line2D<T>::operator () (Point2D<T> &s_point1,Point2D<T> &s_point2)

{
   Points[0] = s_point1;
   Points[1] = s_point2;
}

 template<class T>
 inline Point2D<T>& Line2D<T>::operator [] (int i)

{
   return (Points[i]);
}

 template<class T>
 inline void Line2D<T>::operator = (Line2D<T> &s_line)

{
   Points[0] = s_line[0];
   Points[1] = s_line[1];
}

 template<class T>
 Line2D<T>::Line2D (   )

{
   Clear (   );
}

 template<class T>
 Line2D<T>::Line2D (T x1,T y1,T x2,T y2)

{
   Points[0](x1,y1);
   Points[1](x2,y2);
}

 template<class T>
 Line2D<T>::Line2D (Point2D<T> &s_point1,Point2D<T> &s_point2)

{
   Points[0] = s_point1;
   Points[1] = s_point2;
}

 template<class T>
 inline void Line2D<T>::Clear (   )

{
   Points[0]((T)0,(T)0);
   Points[1]((T)0,(T)0);
}

 template<class T>
 FPoint2D Line2D<T>::GetCenterPosition (   )

{
   FPoint2D pc;
   pc.X = (float)(0.5f * ((float)Points[0].X + Points[1].X));
   pc.Y = (float)(0.5f * ((float)Points[0].Y + Points[1].Y));
   return (pc);
}

 template<class T>
 float Line2D<T>::GetLength (   )

{
   float dx = (float)((float)Points[1].X - Points[0].X);
   float dy = (float)((float)Points[1].Y - Points[0].Y);
   return ((float)sqrt (dx * dx + dy * dy));
}

 template<class T>
 void Line2D<T>::GetLineEquation (float &a,float &b,float &c)

{
   a = (float)((float)Points[1].Y - Points[0].Y);
   b = (float)((float)Points[0].X - Points[1].X);
   c = (float)(a * Points[0].X + b * Points[0].Y);
}

 template<class T>
 float Line2D<T>::GetSquaredLength (   )

{
   float dx = (float)((float)Points[1].X - Points[0].X);
   float dy = (float)((float)Points[1].Y - Points[0].Y);
   return (dx * dx + dy * dy);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class Template: Line3D<T>
//
///////////////////////////////////////////////////////////////////////////////

 template<class T>
 class Line3D

{
   protected:
      Point3D<T> Points[2];

   public:
      Line3D (   );
      Line3D (T x1,T y1,T z1,T x2,T y2,T z2);
      Line3D (Point3D<T> &s_point1,Point3D<T> &s_point2);

   public:
      void        operator () (T x1,T y1,T z1,T x2,T y2,T z2);
      void        operator () (Point3D<T> &s_point1,Point3D<T> &s_point2);
      Point3D<T>& operator [] (int i);

   public:
      void Clear (   );

   public:
      FPoint3D GetCenterPosition (   );
      float    GetLength         (   );
      float    GetSquaredLength  (   );
};

typedef Line3D<double> DLine3D;
typedef Line3D<float>  FLine3D;
typedef Line3D<int>    ILine3D;
typedef Line3D<short>  SLine3D;
typedef Line3D<ushort> USLine3D;

 template<class T>
 inline void Line3D<T>::operator () (T x1,T y1,T z1,T x2,T y2,T z2)

{
   Points[0](x1,y1,z1);
   Points[1](x2,y2,z2);
}

 template<class T>
 inline void Line3D<T>::operator () (Point3D<T> &s_point1,Point3D<T> &s_point2)

{
   Points[0] = s_point1;
   Points[1] = s_point2;
}

 template<class T>
 inline Point3D<T>& Line3D<T>::operator [] (int i)

{
   return (Points[i]);
}

 template<class T>
 Line3D<T>::Line3D (   )

{
   Clear (   );
}

 template<class T>
 Line3D<T>::Line3D (T x1,T y1,T z1,T x2,T y2,T z2)

{
   Points[0](x1,y1,z1);
   Points[1](x2,y2,z2);
}

 template<class T>
 Line3D<T>::Line3D (Point3D<T> &s_point1,Point3D<T> &s_point2)

{
   Points[0] = s_point1;
   Points[1] = s_point2;
}

 template<class T>
 inline void Line3D<T>::Clear (   )

{
   Points[0]((T)0,(T)0,(T)0);
   Points[1]((T)0,(T)0,(T)0);
}

 template<class T>
 FPoint3D Line3D<T>::GetCenterPosition (   )

{
   FPoint3D pc;
   pc.X = (float)(0.5f * ((float)Points[0].X + Points[1].X));
   pc.Y = (float)(0.5f * ((float)Points[0].Y + Points[1].Y));
   pc.Z = (float)(0.5f * ((float)Points[0].Z + Points[1].Z));
   return (pc);
}

 template<class T>
 float Line3D<T>::GetLength (   )

{
   float dx = (float)((float)Points[1].X - Points[0].X);
   float dy = (float)((float)Points[1].Y - Points[0].Y);
   float dz = (float)((float)Points[1].Z - Points[0].Z);
   return ((float)sqrt (dx * dx + dy * dy + dz * dz));
}

 template<class T>
 float Line3D<T>::GetSquaredLength (   )

{
   float dx = (float)((float)Points[1].X - Points[0].X);
   float dy = (float)((float)Points[1].Y - Points[0].Y);
   float dz = (float)((float)Points[1].Z - Points[0].Z);
   return (dx * dx + dy * dy + dz * dz);
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

FPoint3D CP                 (const FPoint3D &s_point1,const FPoint3D &s_point2);
void     CropRegion         (int width,int height,int& sx,int& sy,int& ex,int& ey);
void     CropRegion         (ISize2D& c_size,IBox2D& d_rgn);
float    GetIOU             (IBox2D& s_rgn1,IBox2D& s_rgn2);
float    GetMaxOverlapRatio (IBox2D& s_rgn1,IBox2D& s_rgn2);
float    GetMinOverlapRatio (IBox2D& s_rgn1,IBox2D& s_rgn2);
int      GetOverlapArea     (IBox2D& s_rgn1,IBox2D& s_rgn2);
float    GetOverlapRatio    (IBox2D& s_rgn1,IBox2D& s_rgn2);
float    GetSimilarity      (IBox2D& s_rgn1,IBox2D& s_rgn2);
IBox2D   GetUnion           (IBox2D& s_rgn1,IBox2D& s_rgn2);
float    IP                 (const FPoint2D &s_point1,const FPoint2D &s_point2);
int      IP                 (const IPoint2D &s_point1,const IPoint2D &s_point2);
float    IP                 (const FPoint3D &s_point1,const FPoint3D &s_point2);
float    Mag                (const FPoint2D &s_point);
float    Mag                (const IPoint2D &s_point);
float    Mag                (const FPoint3D &s_point);
FPoint2D Nor                (const FPoint2D &s_point);
FPoint3D Nor                (const FPoint3D &s_point);
float    SquaredMag         (const FPoint2D &s_point);
int      SquaredMag         (const IPoint2D &s_point);
float    SquaredMag         (const FPoint3D &s_point);

}

#endif

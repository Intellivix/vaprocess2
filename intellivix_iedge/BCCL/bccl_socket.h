#if !defined(__BCCL_SOCKET_H)
#define __BCCL_SOCKET_H

#include "bccl_define.h"

#if defined(__WIN32)
#include <afxsock.h>
#endif

///////////////////////////////////////////////////////////////////////////////
//
// Definitions
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__WIN32)
#define ERR_WOULDBLOCK   WSAEWOULDBLOCK
#define ERR_ISCONN       WSAEISCONN
#else
#define ERR_WOULDBLOCK   EWOULDBLOCK
#define ERR_ISCONN       EISCONN
#endif

 namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: StreamSocket
//
///////////////////////////////////////////////////////////////////////////////

 class StreamSocket

{
   protected:
      SOCKET  Socket;
      timeval RcvTimeout;
      timeval SndTimeout;

   public:
      StreamSocket (   );
      virtual ~StreamSocket (   );

   public:
      int operator !      (   ) const;
          operator SOCKET (   ) const;

   protected:
      // 주어진 소켓 핸들을 본 스트림 소켓 객체의 붙인다.
      void AttachHandle (SOCKET h_socket);
      // 소켓을 생성한다.
      int  Create (   );
      // AttachHandle() 함수에 의해 붙여진 소켓 핸들을 떼어낸다.
      void DetachHandle (   );
   
   // 서버로 동작하는 경우 사용
   public:
      // 클라이언트 접속을 기다린다.
      // 접속이 되면 내부 소켓 핸들을 접속된 소켓의 핸들로 교체한다.
      // flag_block: TRUE이면 클라이언트가 접속할 때까지 계속 기다린다.
      //             FALSE이면 바로 리턴한다.
      int Accept (int flag_block);
      // 클라이언트 접속을 기다린다.
      // 클라이언트 접속이 되면 접속된 소켓을 리턴한다.
      // (참고) Listen() (1회 호출) => Accept() (반복 호출)
      // flag_block: TRUE이면 클라이언트가 접속할 때까지 계속 기다린다.
      //             FALSE이면 바로 리턴한다.
      int Accept (StreamSocket *d_socket,int flag_block);
      // 본 소켓에 의해 연결된 클라이언트의 IP 주소와 접속 포트 번호를 리턴한다.
      int GetPeerName (char *ip_addr,ushort &port);
      // 신규 소켓을 생성하고 지정된 포트에 대한 Listen을 수행한다.
      int Listen (ushort port);
   
   // 클라이언트로 동작하는 경우 사용
   public:
      // 신규 소켓을 생성하고 서버 접속을 시도한다. (Non-blocking 함수)
      // 리턴값: 접속에 성공하면 DONE을 리턴한다.
      int Connect (const char *ip_addr,ushort port);

   // 서버/클라이언트 공통으로 사용
   public:
      // 새로운 데이터가 도착했는지 검사한다. (Non-blocking 함수)
      int AreNewDataAvailable (   );
      // 소켓을 닫는다. (연결 종료)
      void Close (   );
      // 가장 최근에 발생한 오류에 대한 오류 코드를 리턴한다.
      int GetLastError (   );
      // 데이터를 받는다.
      // flag_block: TRUE이면 지정된 길이(d_buf_size)의 데이터를 모두 받을 때까지 기다린다.
      //             (대기 시간에 제한을 가하려면 SetRcvTimeout() 함수를 통해 타임아웃 기간 설정)
      //             FALSE이면 현재 받을 수 있는 데이터를 모두 받은 후 리턴한다.
      // 리턴값: 받은 데이터의 길이를 리턴한다.
      //         받은 데이터가 없거나 오류가 발생하면 0보다 작은 값을 리턴한다.
      int ReceiveData  (byte *d_buffer,int d_buf_size,int flag_block);
      // 데이터를 보낸다.
      // flag_block: TRUE이면 지정된 길이(s_buf_size)의 데이터를 모두 보낼 때까지 기다린다.
      //             (대기 시간에 제한을 가하려면 SetSndTimeout() 함수를 통해 타임아웃 기간 설정)
      //             FALSE이면 데이터를 더 이상 보낼 수 없을 때 바로 리턴한다.
      // 리턴값: 보낸 데이터의 길이를 리턴한다.
      //         오류가 발생하면 0보다 작은 값을 리턴한다.
      int SendData (byte *s_buffer,int s_buf_size,int flag_block);
      // 데이터 수신 타임아웃을 설정한다.
      int SetRcvTimeout (int t_sec,int t_usec = 0);
      // 데이터 전송 타임아웃을 설정한다.
      int SetSndTimeout (int t_sec,int t_usec = 0);
};

 inline int StreamSocket::operator ! (   ) const

{
   if (Socket == INVALID_SOCKET) return (TRUE);
   else return (FALSE);
}

 inline StreamSocket::operator SOCKET (   ) const

{
   return (Socket);
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

void CloseWinsock         (   );
int  GetHostIPAddress     (const char* host_name,char *ip_addr);
int  GetLostHostIPAddress (char *ip_addr);
int  IsIPAddressValid     (const char* ip_addr);
int  InitWinsock          (   );

}

#endif

#if defined(__LIB_WEBSOCKETS)
#include "bccl_array.h"
#include "bccl_console.h"
#include "bccl_websock.h"

#if defined(__OS_WINDOWS)
#pragma comment(lib,"websockets.lib")
#endif

 namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: WebSocket_Client
//
///////////////////////////////////////////////////////////////////////////////

 WebSocket_Client::WebSocket_Client (   )

{
   SocketCtx    = NULL;
   Socket       = NULL;
   EventHandler = NULL;
   UserData     = NULL;
}

 WebSocket_Client::~WebSocket_Client (   )

{
   Close (   );
}

 int WebSocket_Client::Callback_Event (WebSocketContext* context,WebSocket* socket,WebSocketEventID event_id,void* user,void* s_buffer,size_t s_buf_size)

{
   void* user_data = libwebsocket_context_user (context);
   return (((WebSocket_Client*)user_data)->OnEvent (event_id,s_buffer,s_buf_size));
}

 void WebSocket_Client::Close (   )

{
   if (SocketCtx != NULL) {
      libwebsocket_context_destroy (SocketCtx);
      SocketCtx = NULL;
      Socket    = NULL;
   }
}

 int WebSocket_Client::OnEvent (WebSocketEventID event_id,void* s_buffer,int s_buf_size)

{
   if (EventHandler != NULL) {
      return (EventHandler(SocketCtx,Socket,event_id,s_buffer,s_buf_size,UserData));
   }
   return (0);
}

 int WebSocket_Client::Open (const char* ip_address,const char* ws_path,int ssl_connection,const char* protocol_name,int port_no)

{
   static char null_str[] = "";
   Close (   );
   memset (Protocols,0,sizeof(Protocols));
   if (protocol_name == NULL) Protocols[0].name = null_str;
   else Protocols[0].name = protocol_name;
   Protocols[0].callback       = Callback_Event;
   Protocols[0].rx_buffer_size = 100000;
   lws_context_creation_info cc_info;
   memset (&cc_info,0,sizeof(cc_info));
   cc_info.port       = CONTEXT_PORT_NO_LISTEN;
   cc_info.protocols  = Protocols;
   cc_info.extensions = libwebsocket_get_internal_extensions (   );
   cc_info.gid        = -1;
   cc_info.uid        = -1;
   cc_info.user       = this;
   SocketCtx = libwebsocket_create_context (&cc_info);
   if (SocketCtx == NULL) {
      Close (   );
      return (1);
   }
   if (port_no == -1) {
      if (ssl_connection) port_no = 443;
      else port_no = 80;
   }
   if (ws_path == NULL) ws_path = null_str;
   const char* hostname = libwebsocket_canonical_hostname (SocketCtx);
   Socket = libwebsocket_client_connect (SocketCtx,ip_address,port_no,ssl_connection,ws_path,hostname,ip_address,Protocols[0].name,-1);
   if (Socket == NULL) {
      Close (   );
      return (2);
   }
   return (DONE);
}

 int WebSocket_Client::RequestWritableEvent (   )

{
   if (SocketCtx == NULL || Socket == NULL) return (1);
   libwebsocket_callback_on_writable (SocketCtx,Socket);
   return (DONE);
}

 void WebSocket_Client::SetEventHandler (WebSocketEventHandler handler,void* user_data)

{
   EventHandler = handler;
   UserData     = user_data;
}

 int WebSocket_Client::Run (int timeout)

{
   if (SocketCtx == NULL) return (1);
   if (libwebsocket_service (SocketCtx,0) < 0) return (2);
   return (DONE);
}

 int WebSocket_Client::Write (void* s_buffer,int s_buf_size,int flag_binary)

{
   if (Socket == NULL) return (-1);
   BArray1D t_buffer(LWS_SEND_BUFFER_PRE_PADDING + s_buf_size + LWS_SEND_BUFFER_POST_PADDING);
   memcpy (&t_buffer[LWS_SEND_BUFFER_PRE_PADDING],s_buffer,s_buf_size);
   libwebsocket_write_protocol protocol = LWS_WRITE_TEXT;
   if (flag_binary) protocol = LWS_WRITE_BINARY;
   int n_bytes_written = libwebsocket_write (Socket,&t_buffer[LWS_SEND_BUFFER_PRE_PADDING],s_buf_size,protocol);
   return (n_bytes_written);
}

}

#endif


#ifndef BCCL_PUGIXML_H
#define BCCL_PUGIXML_H

#include <map>
#include <stack>
#include <sstream>

#include <pugixml.hpp>

#include "bccl_file.h"

namespace BCCL {

class PugiXmlNodeCache : public pugi::xml_node {
public:
   PugiXmlNodeCache( pugi::xml_node& xml_node_from );

   // for preventing from previously read done.
   pugi::xml_node GetChildUnRead( const std::string& child_name );
   void SetChildReadDone( const std::string& child_name, pugi::xml_node& node );

private:
   /**
    * @brief 
    * key  (std::string)    : looking for sibling's node name.
    * value(pugi::xml_node) : next sibling's xml_node to read.
    */
   std::map<std::string, pugi::xml_node> m_cache_nodes;
};

class PugiXmlHelper {
protected:
public:
   PugiXmlHelper();

   void AddDecaration();

   pugi::xml_document& GetXmlDocument();

   bool ParseFromFile( const char* filename,
                       uint parse_options          = pugi::parse_default,
                       pugi::xml_encoding encoding = pugi::encoding_utf8 );
   bool ParseFromMemory( const char* source, uint size,
                         uint parse_options          = pugi::parse_default,
                         pugi::xml_encoding encoding = pugi::encoding_utf8 );
   bool WriteToFile( const char* filename,
                     const char* indent          = " ",
                     uint format_flags           = pugi::format_default,
                     pugi::xml_encoding encoding = pugi::encoding_utf8 );

   bool WriteToMemory( FileIO* pFile,
                       const char* indent          = " ",
                       uint format_flags           = pugi::format_default,
                       pugi::xml_encoding encoding = pugi::encoding_utf8 );

   // for CXMLNode's Start() End().
   PugiXmlNodeCache& GetParent();
   void PushParent( pugi::xml_node& xml_node );
   void PopParent();

   void Reset();

   // for debugging print.
   static void PrintParseResultForDebug( pugi::xml_node& xml_node );
   struct pugi_override_walker : pugi::xml_tree_walker {
      std::stringstream log_buffer;
      virtual bool for_each( pugi::xml_node& node );
   };

   // for pugi to write to FileIO when memory mode.
   struct pugi_override_writer : pugi::xml_writer {
      pugi_override_writer( FileIO* pFileIO );
      virtual void write( const void* data, size_t size );
      ~pugi_override_writer();
      FileIO* st_pFileIO;
   };

protected:
   static const char* g_pugi_node_types[]; // for debugging print.
   pugi::xml_document m_xmlDocument;
   std::stack<PugiXmlNodeCache> m_parent_node; // to remember perent's node.
   char* m_pugi_buffer = nullptr;
};

} // namespace BCCL

#endif // BCCL_PUGIXML_H

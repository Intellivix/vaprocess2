TARGET = BCCL
TEMPLATE = lib
include( ../env.pri )

HEADERS = \
   bccl.h \
   bccl_array.h \
   bccl_color.h \
   bccl_console.h \
   bccl_cpoint.h \
   bccl_cstring.h \
   bccl_ctime.h \
   bccl_curl.h \
   bccl_define.h \
   bccl_define_linux.h \
   bccl_drawing.h \
   bccl_environ.h \
   bccl_ffmpeg.h \
   bccl_file.h \
   bccl_geomobj.h \
   bccl_image.h \
   bccl_list.h \
   bccl_matrix.h \
   bccl_mem.h \
   bccl_misc.h \
   bccl_nrc.h \
   bccl_polygon.h \
   bccl_socket.h \
   bccl_string.h \
   bccl_time.h \
   bccl_xml.h \
   bccl_xmlcvtfn.h \
   bccl_zlib.h \
   bccl_plog.h \
   bccl_pugixml.h \
    bccl_thread.h

SOURCES = \
   bccl_array.cpp \
   bccl_color.cpp \
   bccl_cpoint.cpp \
   bccl_ctime.cpp \
   bccl_curl.cpp \
   bccl_define.cpp \
   bccl_define_linux.cpp \
   bccl_drawing.cpp \
   bccl_ffmpeg.cpp \
   bccl_file.cpp \
   bccl_geomobj.cpp \
   bccl_image.cpp \
   bccl_imgfile.cpp \
   bccl_list.cpp \
   bccl_matrix.cpp \
   bccl_mem.cpp \
   bccl_misc.cpp \
   bccl_nrc.cpp \
   bccl_polygon.cpp \
   bccl_socket.cpp \
   bccl_string.cpp \
   bccl_time.cpp \
   bccl_xml.cpp \
   bccl_xmlcvtfn.cpp \
   bccl_zlib.cpp \
   bccl_plog.cpp \
   bccl_pugixml.cpp \
    bccl_thread.cpp
   

#include "bccl_misc.h"

 namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

extern "C" {

 int Compare_Byte_Ascending (const void *a,const void *b)

{
   if      (*(byte *)a < *(byte *)b) return (-1);
   else if (*(byte *)a > *(byte *)b) return ( 1);
   else return (0);
}

 int Compare_Byte_Descending (const void *a,const void *b)

{
   if      (*(byte *)a < *(byte *)b) return ( 1);
   else if (*(byte *)a > *(byte *)b) return (-1);
   else return (0);
}

 int Compare_Double_Ascending (const void *a,const void *b)

{
   if      (*(double *)a < *(double *)b) return (-1);
   else if (*(double *)a > *(double *)b) return ( 1);
   else return (0);
}

 int Compare_Double_Descending (const void *a,const void *b)

{
   if      (*(double *)a < *(double *)b) return ( 1);
   else if (*(double *)a > *(double *)b) return (-1);
   else return (0);
}

 int Compare_Float_Ascending (const void *a,const void *b)

{
   if      (*(float *)a < *(float *)b) return (-1);
   else if (*(float *)a > *(float *)b) return ( 1);
   else return (0);
}

 int Compare_Float_Descending (const void *a,const void *b)

{
   if      (*(float *)a < *(float *)b) return ( 1);
   else if (*(float *)a > *(float *)b) return (-1);
   else return (0);
}

 int Compare_Int_Ascending (const void *a,const void *b)

{
   if      (*(int *)a < *(int *)b) return (-1);
   else if (*(int *)a > *(int *)b) return ( 1);
   else return (0);
}

 int Compare_Int_Descending (const void *a,const void *b)

{
   if      (*(int *)a < *(int *)b) return ( 1);
   else if (*(int *)a > *(int *)b) return (-1);
   else return (0);
}

 int Compare_ObjectScore_Ascending (const void *a,const void *b)

{
   if      (((ObjectScore *)a)->Score < ((ObjectScore *)b)->Score) return (-1);
   else if (((ObjectScore *)a)->Score > ((ObjectScore *)b)->Score) return ( 1);
   else return (0);
}

 int Compare_ObjectScore_Descending (const void *a,const void *b)

{
   if      (((ObjectScore *)a)->Score < ((ObjectScore *)b)->Score) return ( 1);
   else if (((ObjectScore *)a)->Score > ((ObjectScore *)b)->Score) return (-1);
   else return (0);
}

 int Compare_Short_Ascending (const void *a,const void *b)

{
   if      (*(short *)a < *(short *)b) return (-1);
   else if (*(short *)a > *(short *)b) return ( 1);
   else return (0);
}

 int Compare_Short_Descending (const void *a,const void *b)

{
   if      (*(short *)a < *(short *)b) return ( 1);
   else if (*(short *)a > *(short *)b) return (-1);
   else return (0);
}

 int Compare_String_Ascending (const void *a,const void *b)

{
   return (strcmp ((char *)a,(char *)b));
}

 int Compare_String_Descending (const void *a,const void *b)

{
   return (-strcmp ((char *)a,(char *)b));
}

 int Compare_UShort_Ascending (const void *a,const void *b)

{
   if      (*(ushort *)a < *(ushort *)b) return (-1);
   else if (*(ushort *)a > *(ushort *)b) return ( 1);
   else return (0);
}

 int Compare_UShort_Descending (const void *a,const void *b)

{
   if      (*(ushort *)a < *(ushort *)b) return ( 1);
   else if (*(ushort *)a > *(ushort *)b) return (-1);
   else return (0);
}

}

#define ELEM_SWAP(a,b) { byte t = (a); (a) = (b); (b) = t; }

 byte FindMedian (byte *s_buffer,int n)
 
{
   int low,high ;
   int median;
   int middle,ll,hh;
   
   low = 0; high = n-1; median = (low + high) / 2;
   for (   ;   ;   ) {
      if (high <= low) /* One element only */
         return s_buffer[median] ;
      if (high == low + 1) { /* Two elements only */
         if (s_buffer[low] > s_buffer[high])
            ELEM_SWAP(s_buffer[low], s_buffer[high]) ;
         return s_buffer[median] ;
      }
      /* Find median of low, middle and high items; swap into position low */
      middle = (low + high) / 2;
      if (s_buffer[middle] > s_buffer[high]) ELEM_SWAP(s_buffer[middle], s_buffer[high]) ;
      if (s_buffer[low] > s_buffer[high]) ELEM_SWAP(s_buffer[low], s_buffer[high]) ;
      if (s_buffer[middle] > s_buffer[low]) ELEM_SWAP(s_buffer[middle], s_buffer[low]) ;
      /* Swap low item (now in position middle) into position (low+1) */
      ELEM_SWAP(s_buffer[middle], s_buffer[low+1]) ;
      /* Nibble from each end towards middle, swapping items when stuck */
      ll = low + 1;
      hh = high;
      for (   ;   ;   ) {
         do ll++; while (s_buffer[low] > s_buffer[ll]) ;
         do hh--; while (s_buffer[hh] > s_buffer[low]) ;
         if (hh < ll) break;
         ELEM_SWAP(s_buffer[ll], s_buffer[hh]) ;
      }
      /* Swap middle item (in position low) back into correct position */
      ELEM_SWAP(s_buffer[low], s_buffer[hh]) ;
      /* Re-set active partition */
      if (hh <= median) low = ll;
      if (hh >= median) high = hh - 1;
   }
}

#undef ELEM_SWAP

}



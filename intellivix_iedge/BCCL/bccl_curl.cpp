#include "bccl_console.h"
#include "bccl_curl.h"

#if defined(__LIB_CURL)

#if defined(__WIN32)
#pragma comment(lib,"libcurl.dll.a")
#endif

 namespace BCCL

{

#define MAX_BODY_LENGTH     1000000
#define MAX_HEADER_LENGTH   10000

///////////////////////////////////////////////////////////////////////////////
//
// Class: CURL_HTTPClient
//
///////////////////////////////////////////////////////////////////////////////

 CURL_HTTPClient::CURL_HTTPClient (   )

{
   Flag_Initialized = FALSE;
   BodySize         = 0;
   HeaderSize       = 0;
   UploadBufSize    = 0;
   UploadDataSize   = 0;
   ErrorCode        = CURLE_OK;
   Handle           = NULL;
   UploadBuffer     = NULL;
   ReqHeader        = NULL;

   Timeout          = 10;
}

 CURL_HTTPClient::~CURL_HTTPClient (   )

{
   Close (   );
}

 void CURL_HTTPClient::AddToReqHeader (const char* s_string)

{
   ReqHeader = curl_slist_append (ReqHeader,s_string);
}

 size_t CURL_HTTPClient::Callback_ReceiveBody (char* s_buffer,size_t size,size_t n_items,void* user_data)

{
   return (((CURL_HTTPClient*)user_data)->ReceiveBody (s_buffer,size,n_items));
}

 size_t CURL_HTTPClient::Callback_ReceiveHeader (char* s_buffer,size_t size,size_t n_items,void* user_data)

{
   return (((CURL_HTTPClient*)user_data)->ReceiveHeader (s_buffer,size,n_items));
}

 size_t CURL_HTTPClient::Callback_SendData (char* s_buffer,size_t size,size_t n_items,void* user_data)

{
   return (((CURL_HTTPClient*)user_data)->SendData (s_buffer,size,n_items));
}

 void CURL_HTTPClient::ClearReqHeader (   )

{
   if (ReqHeader == NULL) return;
   curl_slist_free_all (ReqHeader);
   ReqHeader = NULL;
}

 void CURL_HTTPClient::Close (   )

{
   if (Handle) {
      curl_easy_cleanup (Handle);
      Handle = NULL;
   }
   if (ReqHeader) {
      curl_slist_free_all (ReqHeader);
      ReqHeader = NULL;
   }
   RcvBuffer_Header.Delete (   );
   RcvBuffer_Body.Delete (   );
   Flag_Initialized = FALSE;
}

 int CURL_HTTPClient::ExtractResCode (const char *s_string)

{
   if (strlen (s_string) < 12) return (-1);
   char header[6];
   strncpy (header,s_string,5);
   header[5] = 0;
   if (CompareStrings (header,"HTTP/")) return (-2);
   char code[4];
   strncpy (code,s_string + 9,3);
   code[3] = 0;
   return (atoi (code));
}

 char* CURL_HTTPClient::GetResBody (   )

{
   return ((char*)RcvBuffer_Body);
}

 int CURL_HTTPClient::GetResCode (   )

{
   return (ExtractResCode (RcvBuffer_Header));
}

 char* CURL_HTTPClient::GetResHeader (   )

{
   return ((char*)RcvBuffer_Header);
}

 int CURL_HTTPClient::Initialize (   )

{
   Close (   );
   Handle = curl_easy_init (   );
   if (Handle == NULL) return (1);
   if (curl_easy_setopt (Handle,CURLOPT_TIMEOUT       ,Timeout               )) return (2);
   if (curl_easy_setopt (Handle,CURLOPT_HEADERFUNCTION,Callback_ReceiveHeader)) return (3);
   if (curl_easy_setopt (Handle,CURLOPT_HEADERDATA    ,this                  )) return (4);
   if (curl_easy_setopt (Handle,CURLOPT_READFUNCTION  ,Callback_SendData     )) return (5);
   if (curl_easy_setopt (Handle,CURLOPT_READDATA      ,this                  )) return (6);
   if (curl_easy_setopt (Handle,CURLOPT_WRITEFUNCTION ,Callback_ReceiveBody  )) return (7);
   if (curl_easy_setopt (Handle,CURLOPT_WRITEDATA     ,this                  )) return (8);
   if (curl_easy_setopt (Handle,CURLOPT_SSL_VERIFYPEER,0                     )) return (9);
   if (curl_easy_setopt (Handle,CURLOPT_SSL_VERIFYHOST,0                     )) return (10);
   RcvBuffer_Header.Create (MAX_HEADER_LENGTH);
   RcvBuffer_Body.Create   (MAX_BODY_LENGTH);
   Flag_Initialized = TRUE;
   return (DONE);
}

 size_t CURL_HTTPClient::ReceiveBody (char* s_buffer,size_t size,size_t n_items)

{
   int length = (int)(size * n_items);
   int space_left = RcvBuffer_Body.Length - BodySize;
   if (length > space_left) return (0);
   memcpy ((char*)RcvBuffer_Body + BodySize,s_buffer,length);
   BodySize += length;
   return (length);
}

 size_t CURL_HTTPClient::ReceiveHeader (char* s_buffer,size_t size,size_t n_items)

{
   int length = (int)(size * n_items);
   int space_left = RcvBuffer_Header.Length - HeaderSize;
   if (length > space_left) return (0);
   memcpy ((char*)RcvBuffer_Header + HeaderSize,s_buffer,length);
   HeaderSize += length;
   return (length);
}

 size_t CURL_HTTPClient::SendData (char* s_buffer,size_t size,size_t n_items)

{
   if (UploadBuffer == NULL || !UploadBufSize) return (0);
   int length = (int)(size * n_items);
   int space_left = UploadBufSize - UploadDataSize;
   if (!space_left) return (0);
   if (length > space_left) length = space_left;
   memcpy (s_buffer,UploadBuffer + UploadDataSize,length);
   UploadDataSize += length;
   return (length);
}

 int CURL_HTTPClient::SendRequest (int option)

{
   if (Handle == NULL) return (1);
   if (ReqHeader != NULL) curl_easy_setopt (Handle,CURLOPT_HTTPHEADER,ReqHeader);
   switch (option) {
      case CURL_HTTPREQ_GET:
         curl_easy_setopt (Handle,CURLOPT_HTTPGET,1);
         break;
      case CURL_HTTPREQ_POST:
         curl_easy_setopt (Handle,CURLOPT_HTTPGET,0);
         break;
      case CURL_HTTPREQ_PUT:
         curl_easy_setopt (Handle,CURLOPT_UPLOAD,1);
         curl_easy_setopt (Handle,CURLOPT_INFILESIZE,UploadBufSize);
         break;
      default:
         break;
   }
   BodySize = HeaderSize = UploadDataSize = 0;
   ErrorCode = curl_easy_perform (Handle);
   if (ErrorCode) {
      Print ("[ERROR] curl_easy_perform() (Error Code: %d, Error Message: %s)\n",ErrorCode,curl_easy_strerror(ErrorCode));
      return (2);
   }
   RcvBuffer_Header[HeaderSize++] = 0;
   RcvBuffer_Body[BodySize++] = 0;
   return (DONE);
}

 int CURL_HTTPClient::SetPostField (const char *s_string)

{
   if (Handle == NULL) return (1);
   if (curl_easy_setopt (Handle,CURLOPT_POSTFIELDS,s_string)) return (2);
   return (DONE);
}

 int CURL_HTTPClient::SetPostFieldSize (int size)

{
   if (Handle == NULL) return (1);
   if (curl_easy_setopt (Handle,CURLOPT_POSTFIELDSIZE,size)) return (2);
   return (DONE);
}

 int CURL_HTTPClient::SetUploadBuffer (byte* s_buffer,int s_buf_size)

{
   if (Handle == NULL) return (1);
   UploadBuffer  = s_buffer;
   UploadBufSize = s_buf_size;
   return (DONE);
}

 int CURL_HTTPClient::SetURL (const char *s_string)

{
   if (Handle == NULL) return (1);
   if (curl_easy_setopt (Handle,CURLOPT_URL,s_string)) return (2);
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 void CloseCURL (   )

{
   curl_global_cleanup (   );
}

 int InitCURL (   )

{
   if (curl_global_init (CURL_GLOBAL_DEFAULT)) return (1);
   else return (DONE);
}

}

#endif

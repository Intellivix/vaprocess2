#if !defined(__BCCL_NRC_H)
#define __BCCL_NRC_H

#include "bccl_matrix.h"

 namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

void eigsrt (Matrix &d,Matrix &v);
void jacobi (Matrix &a,Matrix &d,Matrix &v,int &nrot);
void lubksb (Matrix &a,IArray1D &indx,Matrix &b);
void ludcmp (Matrix &a,IArray1D &indx,double &d);
void savgol (Matrix &c,const int np,const int nl,const int nr,const int ld,const int m);

}

#endif

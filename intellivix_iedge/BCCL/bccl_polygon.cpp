#include "bccl_drawing.h"
#include "bccl_image.h"
#include "bccl_polygon.h"

 namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: Polygon
//
///////////////////////////////////////////////////////////////////////////////

 Polygon::Polygon (   )

{
   static int n_polygons = 1;
   ID   = n_polygons++;
   Type = 0;
}

 void Polygon::Create (int n_vertices)
 
{
   Vertices.Create (n_vertices);
}

 void Polygon::Delete (   )
 
{
   Vertices.Delete (   );
}

 void Polygon::Fill (byte value,FPoint2D &scale,GImage &d_image)

{
   int i;

   IP2DArray1D vertices(Vertices.Length);
   // 대상영상의 스케일과 동일한 폴리곤을 생성한다. 
   Polygon polygon;
   polygon.Create (Vertices.Length);
   for (i = 0; i < Vertices.Length; i++) {
      IPoint2D pnt = Vertices[i];
      polygon.Vertices[i].X = (int)(pnt.X * scale.X);
      polygon.Vertices[i].Y = (int)(pnt.Y * scale.Y);
   }
   // Polygon Clipping을 수행한다. 
   IBox2D  c_box (0,0,d_image.Width,d_image.Height);  
   Polygon c_polygon;
   ClipPolygon (&polygon,c_box,&c_polygon);
   // 채워진 폴리곤을 그린다.
   if (c_polygon.Vertices.Length >= 3)
       DrawFilledPolygon (c_polygon.Vertices,c_polygon.Vertices.Length,value,d_image);
}

 void Polygon::Fill (ushort value,FPoint2D &scale,byte* d_buff,int width,int height)

{
   int i;

   // 대상영상의 스케일과 동일한 폴리곤을 생성한다. 
   Polygon polygon;
   polygon.Create (Vertices.Length);
   for (i = 0; i < Vertices.Length; i++) {
      IPoint2D pnt = Vertices[i];
      polygon.Vertices[i].X = (int)(pnt.X * scale.X);
      polygon.Vertices[i].Y = (int)(pnt.Y * scale.Y);
   }
   // Polygon Clipping을 수행한다. 
   IBox2D  c_box (0,0,width,height);  
   Polygon c_polygon;
   ClipPolygon (&polygon,c_box,&c_polygon);
   USArray2D d_array;
   d_array.Import ((ushort*)d_buff,width,height); 
   // 채워진 폴리곤을 그린다.
   if (c_polygon.Vertices.Length >= 3)
      DrawFilledPolygon (c_polygon.Vertices,c_polygon.Vertices.Length,value,d_array);
   d_array.Import ((ushort*)NULL,0,0);
}

 void Polygon::GetBoundingBox (IBox2D& d_box)

{
   IPoint2D min_p,max_p,min_x,max_x;
   FindMinMax (Vertices,min_p,max_p,min_x,max_x);
   d_box.X      = min_p.X;
   d_box.Y      = min_p.Y;
   d_box.Width  = max_p.X - min_p.X + 1;
   d_box.Height = max_p.Y - min_p.Y + 1;
}

 int Polygon::IsPointInsidePolygon (IPoint2D &s_point)

{
   int i,j;
   
   int flag_inside = FALSE;
   for (i = 0,j = Vertices.Length - 1; i < Vertices.Length; j = i++) {
      if (((Vertices[i].Y <= s_point.Y && s_point.Y < Vertices[j].Y) || (Vertices[j].Y <= s_point.Y && s_point.Y < Vertices[i].Y))
         && (s_point.X < (Vertices[j].X - Vertices[i].X) * (s_point.Y - Vertices[i].Y) / (Vertices[j].Y - Vertices[i].Y) + Vertices[i].X)) flag_inside = 1 - flag_inside;
   }
   return (flag_inside);
}

 int Polygon::IsPointInsidePolygon (FPoint2D &s_point)

{
   int i,j;
   
   int flag_inside = FALSE;
   for (i = 0,j = Vertices.Length - 1; i < Vertices.Length; j = i++) {
      if (((Vertices[i].Y <= s_point.Y && s_point.Y < Vertices[j].Y) || (Vertices[j].Y <= s_point.Y && s_point.Y < Vertices[i].Y))
         && (s_point.X < (Vertices[j].X - Vertices[i].X) * (s_point.Y - Vertices[i].Y) / (Vertices[j].Y - Vertices[i].Y) + Vertices[i].X)) flag_inside = 1 - flag_inside;
   }
   return (flag_inside);
}

 void Polygon::Offset (int dx,int dy) 

{
   int i;
   for (i = 0 ; i < Vertices.Length ; i++) {
      Vertices[i].X += dx;
      Vertices[i].Y += dy;
   }
   UpdateCenterPosition (   );
}

 int Polygon::ReadFile (FileIO &file)

{
   int length;
   
   if (file.Read ((byte*)&ID,1,sizeof(int))) return (1);
   if (file.Read ((byte*)&Type,1,sizeof(int))) return (1);
   if (file.Read ((byte*)&CenterPos,1,sizeof(IPoint2D))) return (1);
   if (file.Read ((byte*)&length,1,sizeof(int))) return (1);
   Vertices.Create (length);
   if (file.Read ((byte*)(IPoint2D*)Vertices,Vertices.Length,sizeof(IPoint2D))) return (1);
   return (DONE);
}

 void Polygon::UpdateCenterPosition (   )

{
   int i;

   float sum_area = 0.0f;
   float sum_cx   = 0.0f;
   float sum_cy   = 0.0f;
   for (i = 0 ; i < Vertices.Length ; i++) {
      int   i1 = (i + 1) % Vertices.Length;
      float x1 = (float)Vertices[i].X;
      float y1 = (float)Vertices[i].Y;
      float x2 = (float)Vertices[i1].X;
      float y2 = (float)Vertices[i1].Y;
      sum_area += (x1 * y2 - x2 * y1);
      sum_cx   += (x1 + x2) * (x1 * y2 - x2 * y1);
      sum_cy   += (y1 + y2) * (x1 * y2 - x2 * y1);
   }
   sum_area *= 3.0f;
   CenterPos.X = (int)(sum_cx / sum_area);
   CenterPos.Y = (int)(sum_cy / sum_area);
}

 int Polygon::WriteFile (FileIO &file)

{
   if (file.Write ((byte*)&ID,1,sizeof(int))) return (1);
   if (file.Write ((byte*)&Type,1,sizeof(int))) return (1);
   if (file.Write ((byte*)&CenterPos,1,sizeof(IPoint2D))) return (1);
   if (file.Write ((byte*)&Vertices.Length,1,sizeof(int))) return (1);
   if (file.Write ((byte*)(IPoint2D *)Vertices,Vertices.Length,sizeof(IPoint2D))) return (1);
   return (DONE);
}

 int Polygon::CheckSetup (Polygon* pg)
 
{
   int ret_flag = 0;

   FileIO buffThis, buffFrom;   
   CXMLIO xmlIOThis, xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis, XMLIO_Write);
   xmlIOFrom.SetFileIO (&buffFrom, XMLIO_Write);
   WriteFile (&xmlIOThis);
   pg->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom))
      ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength (0); buffFrom.SetLength (0);
   if (ret_flag & CHECK_SETUP_RESULT_CHANGED) ret_flag |= CHECK_SETUP_RESULT_RESTART;
   return (ret_flag);
}

 int Polygon::ReadFile (CXMLIO* pIO)
 
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Polygon"))
   {
      int length = 0,length2 = 0;
      xmlNode.Attribute (TYPE_ID(int)     ,"ID"       ,&ID);
      xmlNode.Attribute (TYPE_ID(int)     ,"Type"     ,&Type);
      xmlNode.Attribute (TYPE_ID(IPoint2D),"CenterPos",&CenterPos);
      xmlNode.Attribute (TYPE_ID(int)     ,"Length"   ,&length);
      xmlNode.Attribute (TYPE_ID(int)     ,"length"   ,&length2);
      if (length2 > 0) length = length2;
      Vertices.Create (length);
      xmlNode.Attribute (TYPE_ID(IPoint2D),"Vertices" ,(IPoint2D*)Vertices,Vertices.Length);
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}

 int Polygon::WriteFile (CXMLIO* pIO)
 
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Polygon"))
   {
      xmlNode.Attribute (TYPE_ID(int)     ,"ID"       ,&ID);
      xmlNode.Attribute (TYPE_ID(int)     ,"Type"     ,&Type);
      xmlNode.Attribute (TYPE_ID(IPoint2D),"CenterPos",&CenterPos);
      xmlNode.Attribute (TYPE_ID(int)     ,"Length"   ,&Vertices.Length);
      xmlNode.Attribute (TYPE_ID(IPoint2D),"Vertices" ,(IPoint2D*)Vertices,Vertices.Length);
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

#define POLYGON_VERTEX_ORDER_CLOCKWISE          0
#define POLYGON_VERTEX_ORDER_COUNTERCLOCKWISE   1

int  AddPoint                  (IP2DArray1D &vertices,IPoint2D &input_pnt,int input_vertex_idx);
int  GetPolygonVerticeOrder    (Polygon *polygon);
void Intersect                 (IPoint2D &p1,IPoint2D &p2,IBox2D &c_box,int edge_type,IPoint2D &ip);
void InvertPolygonVertexOrder (Polygon *polygon);
int  IsInsideEdge              (IPoint2D &pnt,IBox2D &c_box,int edge_type);
int  IsInsideBox               (IBox2D &box,IPoint2D &pnt);

 int AddPoint (IP2DArray1D& vertices,IPoint2D &input_pnt,int input_vertex_idx)

{
   vertices[input_vertex_idx] = input_pnt;
   return (1);
}

 int GetPolygonVerticeOrder (Polygon* polygon)

{
   int i,j,k;
   int count = 0;
   double z;

   IP2DArray1D& vertices = polygon->Vertices;
   int n_vertices = vertices.Length;
   if (n_vertices < 3)
      return (-1);

   // 각 에지를 구성하는 포인트의 cross product를 누적합을 구한다. 
   for (i = 0; i < n_vertices; i++) {
      j = (i + 1) % n_vertices;
      k = (i + 2) % n_vertices;
      z =  (vertices[j].X - vertices[i].X) * (vertices[k].Y - vertices[j].Y);
      z -= (vertices[j].Y - vertices[i].Y) * (vertices[k].X - vertices[j].X);
      if (z < 0)
         count--;
      else if (z > 0)
         count++;
   }
   if (count > 0)
      return (POLYGON_VERTEX_ORDER_CLOCKWISE);
   else if (count < 0)
      return (POLYGON_VERTEX_ORDER_COUNTERCLOCKWISE);
   else
      return (-1);
}

 void Intersect (IPoint2D &p1,IPoint2D &p2,IBox2D &c_box,int edge_type,IPoint2D &ip)

{
   switch (edge_type) 
   {
      case 0: // left
      {
         IPoint2D d = p2 - p1;
         float y_slope = float (d.Y) / d.X;
         ip.X = c_box.X;
         ip.Y = int (p1.Y + y_slope * (ip.X - p1.X) + 0.5f);
      }
      break;
   case 1: // bottom
      {
         IPoint2D d = p2 - p1;
         float x_slope = float (d.X) / d.Y;
         ip.Y = c_box.Y + c_box.Height;
         ip.X = int (p1.X + x_slope * (ip.Y - p1.Y) + 0.5f);
      }
      break;
   case 2: // right
      {
         IPoint2D d = p2 - p1;
         float y_slope = float (d.Y) / d.X;
         ip.X = c_box.X + c_box.Width;
         ip.Y = int (p1.Y + y_slope * (ip.X - p1.X) + 0.5f);
      }
      break;
   case 3: // top
      {
         IPoint2D d = p2 - p1;
         float x_slope = float (d.X) / d.Y;
         ip.Y = c_box.Y;
         ip.X = int (p1.X + x_slope * (ip.Y - p1.Y) + 0.5f);
      }
      break;
   }
}

 void InvertPolygonVertexOrder (Polygon* polygon)

{
   int i;

   IP2DArray1D& vertices_org = polygon->Vertices;
   IP2DArray1D  vertices_tmp = polygon->Vertices;
   int n_vertices = vertices_org.Length;
   for (i = 0; i < vertices_org.Length; i++) {
      vertices_org[i] = vertices_tmp[n_vertices - i - 1];
   }
}

 int IsInsideBox (IBox2D &box,IPoint2D &pnt) 

{
   if (box.X        > pnt.X) return (0);
   if (box.Y        > pnt.Y) return (0);
   if (box.Width-1  < pnt.X) return (0);
   if (box.Height-1 < pnt.Y) return (0);
   return (1);
}

 int IsInsideEdge (IPoint2D &pnt,IBox2D &c_box,int edge_type)

{
   switch (edge_type) 
   {
      case 0: // left
      {
         if   (pnt.X < c_box.X) return (0);
         else                   return (1);
      }
      case 1: // bottom
      {
         int bottom = c_box.Y + c_box.Height - 1;
         if   (pnt.Y > bottom) return (0);
         else                  return (1);
      }
      case 2: // right
      {
         int right = c_box.X + c_box.Width - 1;
         if   (pnt.X > right) return (0);
         else                 return (1);
      }
      case 3: // top
      {
         if   (pnt.Y < c_box.Y) return (0);
         else                   return (1);
      }
   }
   return (0);
}

 void ClipPolygon (Polygon *s_polygon,IBox2D &c_box,Polygon *d_polygon)

{
   // Computer Graphics Principles and Practice 2nd (Addison Wesley) 124 page
   // 3.14.1 The Sutherland-Hodgman Polygon-Clipping Algorithm을 참조함. 
   IP2DArray1D vertices_in = s_polygon->Vertices;
   IP2DArray1D vertices_out;
   int max_vertex_num = vertices_in.Length * 3;
   vertices_out.Create (max_vertex_num);

   int i, j;
   int output_vertex_num = 0;
   int n_in_vertex = vertices_in.Length;
   IPoint2D p, s, ip;

   // c_box의 4개의 면마다 루프
   for (i = 0; i < 4; i++) 
   {
      if (n_in_vertex >= 3)  // 클립핑 할 폴리곤이 있는 경우에만. 
      {
         s = vertices_in[n_in_vertex - 1];
         for (j = 0; j < n_in_vertex; j++) 
         {
            p = vertices_in[j];
            // Case 1 and 4 
            if (IsInsideEdge (p, c_box, i)) {         
               // Case 1 (In -> In)
               if (IsInsideEdge (s, c_box, i)) {      
                  AddPoint (vertices_out, p, output_vertex_num++);
               }
               // Case 4 (In -> Out)
               else                                    
               {
                  Intersect (s, p, c_box, i, ip);
                  AddPoint (vertices_out, ip, output_vertex_num++);
                  AddPoint (vertices_out, p, output_vertex_num++);
               }
            }
            // Case 2 and 3 
            else 
            {
               // Case 3 (Out -> Out) : No 

               // Case 2 (Out -> In)
               if (IsInsideEdge (s, c_box, i)) {      // Case 1 and 4 
                  Intersect (s, p, c_box, i, ip);
                  AddPoint (vertices_out, ip, output_vertex_num++);
               }
            }
            s = p;
         }
      }
      else {
         // 클리핑 한 결과가 폴리곤을 구성할 수 없으면 모두 클립되었음으로 처리.
         output_vertex_num = 0;
         break;
      }
      if (i < 3) {
         n_in_vertex = output_vertex_num;
         vertices_in.Create (n_in_vertex);
         for (j = 0; j < n_in_vertex; j++) 
            vertices_in[j] = vertices_out[j];
         output_vertex_num = 0; 
      }
   }

   IP2DArray1D& vertices_clip = d_polygon->Vertices;
   vertices_clip.Create (output_vertex_num);
   for (j = 0; j < output_vertex_num; j++)
      vertices_clip[j] = vertices_out[j];
}

}

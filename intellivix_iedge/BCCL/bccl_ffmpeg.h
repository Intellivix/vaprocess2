﻿#ifndef __BCCL_FFMPEG_H
#define __BCCL_FFMPEG_H

#if defined(__LIB_FFMPEG) && !defined(__SYSBIOS)

#include "bccl_image.h"
#include "bccl_string.h"

extern "C" 
{
#define __STDC_CONSTANT_MACROS
//#include "stdint.h"
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libswresample/swresample.h>
#include <libavutil/avstring.h>
#include <libavutil/opt.h>
};

 namespace BCCL

{

#define FFMPEG_BUFFER_PADDING_SIZE   64

///////////////////////////////////////////////////////////////////////////////
//
// Class: FFMPEG_AudioCodec
//
///////////////////////////////////////////////////////////////////////////////
//
// 오디오 데이터를 인코딩/디코딩한다.
//

 class FFMPEG_AudioCodec
 
{
   protected:
      int         Flag_Decoding;
      int         Flag_Encoding;
      int         Flag_InnerCodecCtx;
      BArray1D    SrcBuffer;
      BArray1D    DstBuffer;
      AVFrame*    Frame;
      SwrContext* SwrCtx;

   // Read-Only 변수들
   // OpenDecoder() / OpenEncoder() 함수를 호출한 후 사용 가능하다.   
   public:
      int             BitsPerSample;    // 샘플당 비트 수.
      int             NumChannels;      // 오디오 채널 수.
      int             SampleRate;       // 한 채널에서의 초당 샘플 수.
                                        // (참고) 오디오 데이터의 초당 바이트 수 = NumChannels * SampleRate * BitsPerSample / 8
      int             Flag_FloatSample; // 샘플이 실수형이면 TRUE가 된다.
      uint64_t        ChannelLayout;    // 채널 레이아웃. (AV_CH_LAYOUT_XXX 정의문 참조)
      AVCodecID       CodecID;          // 설정된 코덱의 ID.
      AVSampleFormat  SampleFormat;     // 샘플 포맷. (AV_SAMPLE_FMT_XXX 정의문 참조)
                                        // [주의] 항상 Planar Sample Format 값을 가진다.
      AVCodecContext* CodecCtx;         // 코덱 컨텍스트.

   // 사용자 설정 변수들
   // OpenEncoder() / OpenDecoder() 함수를 호출하기 전에 설정해야 효과가 있다.
   public:
      int Bitrate;       // 인코딩 시의 비트레이트. 손실 압축 방식일 때만 유효하다.
                         // 기본 설정값: 128000
      int OutSampleRate; // 디코딩 시 출력 오디오 데이터의 초당 샘플 수.
                         // 값이 0이면 출력 오디오 데이터의 SampleRate 값은 입력 오디오 데이터의 SampleRate와 동일한 값을 갖는다.
                         // 기본 설정값: 0

   public:
      FFMPEG_AudioCodec (   );
      virtual ~FFMPEG_AudioCodec (   );
     
   protected:
      int OpenDecoder (   );
      int OpenEncoder (   );
   
   public:
      // 오디오 코덱이 열려 있지 않으면 TRUE를 리턴한다.
      int operator ! (   ) const;
   
   public:
      // 오디오 디코더/인코더를 닫는다.
      virtual void Close (   );
         
   public:
      static AVSampleFormat GetPackedSampleFormat (AVSampleFormat sample_fmt);
      static AVSampleFormat GetPlanarSampleFormat (AVSampleFormat sample_fmt);
      static int IsPlanarSampleFormat             (AVSampleFormat sample_fmt);

   protected:
      void ConvertPackedToPlanarSampleFormat (const byte *s_buffer,int s_buf_size,byte *d_buffer,int &d_buf_size);
      void ConvertPlanarToPackedSampleFormat (AVFrame* s_frame,byte *d_buffer,int &d_buf_size);

   public:
      // 인코딩되어 있는 오디오 데이터를 디코딩한다.
      // s_buffer[in]         : 인코딩된 오디오 데이터(입력 오디오 데이터)가 저장되어 있는 버퍼의 포인터
      // s_buf_size[in]       : 입력 오디오 데이터의 길이
      // p_d_buffer[out]      : 디코딩된 오디오 데이터가 저장되어 있는 버퍼의 포인터 값을 저장할 변수의 포인터
      // d_buf_size[out]      : 디코딩된 오디오 데이터의 길이
      // 리턴 값              : 오류가 없으면 0을 리턴한다.
      int Decode (const byte *s_buffer,int s_buf_size,byte **p_d_buffer,int &d_buf_size);
      // 오디오 데이터를 인코딩한다.
      // s_buffer[in]         : 인코딩할 오디오 데이터(입력 오디오 데이터)가 저장되어 있는 버퍼의 포인터
      //                        [주의] 인코딩할 오디오 데이터의 길이를 임의로 결정해서는 안된다.
      //                               반드시 GetFrameSize() 함수가 제공하는 프레임 크기로 맞춰야 한다.
      // p_d_buffer[out]      : 인코딩된 오디오 데이터가 저장되는 있는 버퍼의 포인터 값을 저장할 변수의 포인터
      // d_buf_size[out]      : 인코딩된 오디오 데이터의 길이
      // 리턴 값              : 오류가 없으면 0을 리턴한다.
      int Encode (const byte *s_buffer,byte **p_d_buffer,int &d_buf_size);
      int Flush  (   );
      // 현재 설정되어 있는 코덱이 지원하는 오디오 데이터 프레임의 길이(바이트 수)를 얻는다.
      // "프레임"은 오디오 데이터를 인코딩할 때 최소 단위가 되는 데이터 덩어리를 나타내며, 프레임의 길이는 사용하는 코덱마다 다르다.
      // 따라서 본 함수를 호출하기 전에 OpenDecoder() 함수 또는 OpenEncoder() 함수를 먼저 호출해야 한다.
      // 프레임의 크기 = 프레임 당 샘플 수 * 채널 수 * 샘플 당 바이트 수
      // (참고) 코덱이 PCM 계열이면 프레임 당 샘플 수는 100ms 기간 동안의 샘플 수로 설정된다.
      // 리턴 값              : = 0: 오류 발생
      //                      : > 0: 프레임 크기
      int GetFrameSize (   );
      // 오디오 디코더를 연다.
      // codec_id[in]         : 오디오 인코더의 코덱 ID. (AV_CODEC_ID_XXX 참조)
      // n_channels[in]       : 오디오 채널 수. 모노: 1, 스테레오: 2
      // sampling_rate[in]    : 한 채널에서의 초당 샘플 수.
      // 리턴 값              : 오류가 없으면 0을 리턴한다.
      int OpenDecoder (AVCodecID codec_id,int n_channels,int sampling_rate);
      int OpenDecoder (AVCodecContext *codec_ctx);
      // 오디오 인코더를 연다.
      // codec_id[in]         : 오디오 인코더의 코덱 ID. (AV_CODEC_ID_XXX 참조)
      // n_channels[in]       : 오디오 채널 수.
      // sample_rate[in]      : 한 채널에서의 초당 샘플 수.
      // sample_fmt[in]       : 샘플 포맷. (AV_SAMPLE_FMT_XXX 참조)
      // channel_layout[in]   : 채널 레이아웃. (AV_CH_LAYOUT_XXX 참조)
      //                        0이면 n_channels 값에 기반한 디폴트 채널 레이아웃이 설정된다.
      // 리턴 값              : 오류가 없으면 0을 리턴한다.
      int OpenEncoder (AVCodecID codec_id,int n_channels,int sample_rate,AVSampleFormat sample_fmt,uint64_t channel_layout = 0);
      int OpenEncoder (AVCodecContext *codec_ctx);
};

 inline int FFMPEG_AudioCodec::operator ! (   ) const

{
   if (Flag_Decoding || Flag_Encoding) return (FALSE);
   else return (TRUE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: FFMPEG_SubtitleCodec
//
///////////////////////////////////////////////////////////////////////////////
//
// 자막 데이터를 인코딩/디코딩한다.
//

 class FFMPEG_SubtitleCodec
 
{
   protected:
      int Flag_Decoding;
      int Flag_Encoding;
      int Flag_InnerCodecCtx;
      
   public:
      AVCodecID       CodecID;  // 설정된 코덱의 ID
      AVCodecContext* CodecCtx; // 코덱 컨텍스트
      
   public:
      FFMPEG_SubtitleCodec (   );
      virtual ~FFMPEG_SubtitleCodec (   );
      
   protected:
      int OpenDecoder (   );
      
   public:
      int operator ! (   ) const;

   public:
      virtual void Close (   );
      
   public:
      int Flush       (   );
      // 인코딩되어 있는 자막 데이터를 디코딩한다.
      // s_buffer[in]   : 인코딩된 자막 데이터(입력 자막 데이터)가 저장되어 있는 버퍼의 포인터
      // s_buf_size[in] : 입력 자막 데이터의 길이
      // d_subtitle[out]: 디코딩된 자막 데이터가 저장되어 있는 AVSubtitle 객체
      //                  디코딩된 자막 데이터는 UTF8 문자열(끝에 0 포함)로 주어진다.
      //                  사용이 완료된 AVSubtitle 객체는 avsubtitle_free() 함수를 통해 제거한다.
      // time_pos[out]  : 자막이 출력되는 시작 시간(위치)
      // duration[out]  : 자막이 출력되는 기간
      // 리턴 값        :   0: 디코딩 오류가 없고 디코딩된 자막 데이터가 있는 경우
      //                  < 0: 디코딩 오류는 없으나 디코딩된 자막 데이터는 없는 경우
      //                  > 0: 디코딩 오류가 발생한 경우
      int Decode      (const byte *s_buffer,int s_buf_size,AVSubtitle& d_subtitle,int &time_pos,int &duration);
      int OpenDecoder (AVCodecContext *codec_ctx);
};

 inline int FFMPEG_SubtitleCodec::operator ! (   ) const

{
   if (Flag_Decoding || Flag_Encoding) return (FALSE);
   else return (TRUE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: FFMPEG_VideoCodec
//
///////////////////////////////////////////////////////////////////////////////
//
// 비디오 데이터를 인코딩/디코딩한다.
//

#define FFMPEG_PIXFMT_NONE     AV_PIX_FMT_NONE
#define FFMPEG_PIXFMT_GRAY8    AV_PIX_FMT_GRAY8
#define FFMPEG_PIXFMT_BGR24    AV_PIX_FMT_BGR24
#define FFMPEG_PIXFMT_BGRA32   AV_PIX_FMT_BGRA
#define FFMPEG_PIXFMT_YUY2     AV_PIX_FMT_YUYV422
#define FFMPEG_PIXFMT_YV12     AV_PIX_FMT_YUV420P
#define FFMPEG_PIXFMT_NV12     AV_PIX_FMT_NV12

 class FFMPEG_VideoCodec
 
{
   protected:
      int           Flag_Decoding;
      int           Flag_Encoding;
      int           Flag_InnerCodecCtx;
      BArray1D      SrcBuffer;
      BArray1D      DstBuffer;
      SwsContext*   SwsCtx;
      AVDictionary* Options;
      
   // Read-Only 변수들
   // OpenDecoder() / OpenEncoder() 함수를 호출한 후 사용 가능하다.
   public:
      int             FrameWidth;  // 비디오 프레임의 너비
      int             FrameHeight; // 비디오 프레임의 높이
      float           FrameRate;   // 프레임 레이트
      AVCodecID       CodecID;     // 설정된 코덱의 ID
      AVPixelFormat   PixFmt;      // 비디오 프레임의 픽셀 포맷 (인코딩 시에는 입력 영상의 픽셀 포맷을 의미하고, 디코딩 시에는 출력 영상의 픽셀 포맷을 의미함)
      AVCodecContext* CodecCtx;    // 코덱 컨텍스트

      int             FrameWidth2;  // 비디오 프레임의 너비
      int             FrameHeight2; // 비디오 프레임의 높이
      AVPixelFormat   PixFmt2;
  
   // Read-Only 변수들
   // Decode() / Encode() 함수를 호출한 후 사용 가능하다.
   public:
      AVFrame* SrcFrame;
      AVFrame* DstFrame;
 
   // 사용자 설정 변수들
   public:
      // 비디오 인코딩 시의 비트레이트
      // 손실 압축 코덱을 사용할 경우에만 유효한 의미를 갖는다.   
      int Bitrate; 
      // 비디오 인코딩 시의 GOP(Group of Pictures) 크기
      // 손실 압축 코덱을 사용할 경우에만 유효한 의미를 갖는다.
      // 키 프레임(I 프레임)이 생성되는 주기를 조절하는데 사용된다.
      int GOPSize;
      // jun: 디코딩 한 영상의 실제 크기 
      ISize2D DecodedFrameSize;

   public:
      FFMPEG_VideoCodec (   );
      virtual ~FFMPEG_VideoCodec (   );
   
   protected:
      int OpenDecoder (   );
      int OpenEncoder (   );

   public:
      int operator ! (   ) const;

   public:
      // 비디오 디코더/인코더를 닫는다.
      virtual void Close (   );

   public:
      int Resize  (AVPixelFormat nPixFmt, const byte* s_buffer, int s_width, int s_height, byte* d_buffer, int d_width, int d_height);
      int Convert (AVPixelFormat s_nPixFmt, const byte* s_buffer, int s_width, int s_height, AVPixelFormat d_nPixFmt, byte* d_buffer, int d_width, int d_height);

      // 주어진 (인코딩되어 있는) 비디오 프레임 데이터를 디코딩한다.
      int Decode (const byte *s_buffer,int s_buf_size,AVFrame* d_frame);
      // 주어진 (인코딩되어 있는) 비디오 프레임 데이터를 디코딩한다.
      // 본 함수를 최초로 호출하기 전에 OpenDecoder() 함수를 먼저 한번 호출해야 한다.
      // 본 함수 호출 후 GetDecodedFrameData() 함수를 호출하여 디코딩된 프레임 데이터를 얻는다.
      // s_buffer[in]  : 인코딩되어 있는 프레임 데이터가 저장되어 있는 버퍼의 포인터
      // s_buf_size[in]: 인코딩되어 있는 프레임 데이터의 길이
      // d_fr_size[out]: 프레임의 크기
      // 리턴 값       :   0: 디코딩 오류가 없고 디코딩된 영상 데이터가 있는 경우
      //               :  -1: 디코딩 오류는 없으나 디코딩된 영상 데이터는 없는 경우
      //               :  -2: 디코딩 된 프레임의 크기가 코덱 오픈 시 지정된 프레임의 크기와 다른 경우
      //               : > 0: 디코딩 오류가 발생한 경우
      int Decode (const byte *s_buffer,int s_buf_size,ISize2D &d_fr_size);      
      // 주어진 (인코딩되어 있는) 비디오 프레임 데이터를 디코딩한다.
      // 본 함수를 최초로 호출하기 전에 OpenDecoder() 함수를 먼저 한번 호출해야 한다.
      // s_buffer[in]   : 인코딩되어 있는 프레임 데이터가 저장되어 있는 버퍼의 포인터
      // s_buf_size[in] : 인코딩되어 있는 프레임 데이터의 길이
      // d_buffer[out]  : 디코딩된 프레임 데이터를 저장할 버퍼의 포인터
      //                : 버퍼는 외부에서 할당해야 한다.
      // pitches[in]    : 프레임의 평면 별 피치(한 행(row)의 바이트 수) 값이 담긴 배열의 포인터
      //                : 본 값이 NULL이면 기본 피치 값을 사용한다.
      // n_planes[in]   : 프레임를 구성하는 평면 수
      //                : 본 값이 4보다 크면 안된다.
      // 리턴 값        :   0: 디코딩 오류가 없고 디코딩된 영상 데이터가 있는 경우
      //                :  -1: 디코딩 오류는 없으나 디코딩된 영상 데이터는 없는 경우
      //                : > 0: 디코딩 오류가 발생한 경우
      int Decode (const byte *s_buffer,int s_buf_size,byte *d_buffer,const int* pitches = NULL,int n_planes = 1);
      // 주어진 (인코딩되어 있는) 비디오 프레임 데이터를 디코딩한다.
      // 본 함수를 최초로 호출하기 전에 OpenDecoder() 함수를 먼저 한번 호출해야 한다.
      // s_buffer[in]   : 인코딩되어 있는 프레임 데이터가 저장되어 있는 버퍼의 포인터
      // s_buf_size[in] : 인코딩되어 있는 프레임 데이터의 길이
      // p_d_buffer[out]: 디코딩된 프레임 데이터가 저장되어 있는 버퍼의 포인터 값을 저장할 변수의 포인터
      //                : 버퍼는 내부에서 할당된다.
      // pitches[in]    : 프레임의 평면 별 피치(한 행(row)의 바이트 수) 값이 담긴 배열의 포인터
      //                : 본 값이 NULL이면 기본 피치 값을 사용한다.
      // n_planes[in]   : 프레임를 구성하는 평면 수
      //                : 본 값이 4보다 크면 안된다.
      // 리턴 값        :   0: 디코딩 오류가 없고 디코딩된 영상 데이터가 있는 경우
      //                :  -1: 디코딩 오류는 없으나 디코딩된 영상 데이터는 없는 경우
      //                : > 0: 디코딩 오류가 발생한 경우
      int Decode (const byte *s_buffer,int s_buf_size,byte **p_d_buffer,const int* pitches = NULL,int n_planes = 1);
      // 인코딩되어 있는 비디오 프레임 데이터를 디코딩한다.
      // 본 함수를 최초로 호출하기 전에 OpenDecoder() 함수를 먼저 한번 호출해야 한다.
      // s_buffer[in]  : 인코딩되어 있는 프레임(또는 비디오 스트림) 데이터가 저장되어 있는 버퍼의 포인터
      // s_buf_size[in]: 인코딩되어 있는 프레임(또는 비디오 스트림) 데이터의 길이
      // d_image[out]  : 디코딩된 프레임 데이터가 저장될 버퍼
      //               : 디코딩된 프레임 데이터가 저장될 버퍼를 외부에서 할당하여 주어야 한다.
      // 리턴 값       : 0  : 오류 없이 정상적으로 디코딩된 영상을 얻은 경우
      //               : -1 : 오류는 없으나 디코딩된 영상을 얻지 못한 경우
      //               : > 0: 디코딩 오류가 발생한 경우
      int Decode (const byte *s_buffer,int s_buf_size,BGRImage &d_image);
      // 주어진 비디오 프레임 데이터를 인코딩한다.
      // 본 함수를 최초로 호출하기 전에 OpenEncoder() 함수를 먼저 한번 호출해야 한다.
      // s_buffer[in]       : 인코딩할 프레임 데이터가 저장되어 있는 버퍼의 포인터
      // p_d_buffer[out]    : 인코딩된 프레임 데이터가 저장되어 있는 버퍼의 포인터 값을 담을 변수의 포인터
      //                    : 버퍼는 내부에서 할당된다.
      // d_buf_size[out]    : 인코딩된 프레임 데이터의 길이
      // flag_key_frame[out]: 인코딩된 프레임이 키 프레임(I 프레임)인지 여부를 나타낸다.
      // 리턴 값            : 오류가 없으면 0을 리턴한다.
      int Encode (const byte *s_buffer,byte **p_d_buffer,int &d_buf_size,int &flag_key_frame);
      // 주어진 비디오 프레임 데이터를 인코딩한다.
      // 본 함수를 최초로 호출하기 전에 OpenEncoder() 함수를 먼저 한번 호출해야 한다.
      // s_buffer[in]       : 인코딩할 프레임 데이터가 저장되어 있는 BGRImage
      // p_d_buffer[out]    : 인코딩된 프레임 데이터가 저장되어 있는 버퍼의 포인터 값을 담을 변수의 포인터
      //                    : 버퍼는 내부에서 할당된다.
      // d_buf_size[out]    : 인코딩된 프레임 데이터의 길이
      // flag_key_frame[out]: 인코딩된 프레임이 키 프레임(I 프레임)인지 여부를 나타낸다.
      // 리턴 값            : 오류가 없으면 0을 리턴한다.
      int Encode (BGRImage &s_image,byte **p_d_buffer,int &d_buf_size,int &flag_key_frame);

      // 디코딩된 프레임 데이터를 얻는다.
      // 본 함수를 호출하기 전에 Decode(const byte *s_buffer,int s_buf_size) 함수를 먼저 호출해야 한다.
      // d_buffer[out]: 디코딩된 프레임 데이터를 저장할 버퍼의 포인터
      //              : 버퍼는 외부에서 할당해야 한다.
      // pitches[in]  : 프레임의 평면 별 피치(한 행(row)의 바이트 수) 값이 담긴 배열의 포인터
      //              : 본 값이 NULL이면 기본 피치 값을 사용한다.
      // n_planes[in] : 프레임를 구성하는 평면 수
      //              : 본 값이 4보다 크면 안된다.
      // 리턴 값      : 오류가 없으면 0을 리턴한다.
      int GetDecodedFrameData (byte* d_buffer,const int* pitches = NULL,int n_planes = 1);
      // 설정된 픽셀 포맷과 프레임 너비 및 높이 값을 바탕으로 프레임 데이터의 길이를 계산한다.
      // 리턴 값: 프레임 데이터의 길이
      int GetFrameSize (   );
      int Flush (   );
      // 비디오 디코더를 연다.
      // codec_id[in]  : 비디오 디코더의 코덱 ID. (AV_CODEC_ID_XXX 참조)
      // frm_width[in] : 비디오 프레임의 너비
      // frm_height[in]: 비디오 프레임의 높이
      // 리턴 값       : 오류가 없으면 0을 리턴한다.
      int OpenDecoder (AVCodecID codec_id,int frm_width,int frm_height,
         AVPixelFormat nPixFmt = AV_PIX_FMT_YUV420P);
      // 비디오 디코더를 연다.
      // codec_ctx[in] : 디코딩할 비디오의 코덱 컨텍스트.
      int OpenDecoder (AVCodecContext *codec_ctx);
      // 비디오 인코더를 연다.
      // codec_id[in]  : 비디오 인코더의 코덱 ID. (AV_CODEC_ID_XXX 참조)
      // frm_width[in] : 비디오 프레임의 너비
      // frm_height[in]: 비디오 프레임의 높이
      // frm_rate[in]  : 비디오 프레임 레이트
      // 리턴 값       : 오류가 없으면 0을 리턴한다.
      int OpenEncoder (AVCodecID codec_id,int frm_width,int frm_height,float frm_rate);
      int OpenEncoder (AVCodecContext* codec_ctx);
};

 inline int FFMPEG_VideoCodec::operator ! (   ) const

{
   if (Flag_Decoding || Flag_Encoding) return (FALSE);
   else return (TRUE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: FFMPEG_FileReader
//
///////////////////////////////////////////////////////////////////////////////

#define FFMPEG_KEY_FRAME       0x00000001
#define FFMPEG_MDT_AUDIO       0x00000002
#define FFMPEG_MDT_SUBTITLE    0x00000004
#define FFMPEG_MDT_VIDEO       0x00000008

 class FFMPEG_FileReader
 
{
   protected:
      int          FileHandle;
      byte*        IOBuffer;
      AVPacket     Packet;
      AVIOContext* AVIOCtx;

   public:
      struct TrackInfo {
         int     StreamIndex;
         StringA HandlerName; // UTF8
         StringA Language;    // UTF8
         StringA Title;       // UTF8
      };
   
   // 미디어 파일 정보 (Read-Only)
   public:
      int                Duration;          // 미디어 데이터 길이 (msec 단위)
      AVFormatContext*   FormatCtx;         // 미디어 파일 포맷 정보
      Array1D<TrackInfo> AudioTrackList;    // 미디어 파일에 포함된 오디오 트랙 목록
      Array1D<TrackInfo> SubtitleTrackList; // 미디어 파일에 포함된 자막 트랙 목록 
      Array1D<TrackInfo> VideoTrackList;    // 미디어 파일에 포함된 비디오 트랙 목록
 
   // 선택된 비디오 트랙 정보
   public:
      int                VideoTrackNo;      // 선택된 비디오 트랙 번호 (-1이면 선택된 비디오 트랙이 없음을 의미함)
      int                FrameWidth;        // 비디오 프레임의 너비
      int                FrameHeight;       // 비디오 프레임의 높이
      int                NumFrames;         // 총 비디오 프레임 수
      float              FrameRate;         // 프레임 레이트
      AVCodecID          VideoCodecID;      // 비디오 코덱 ID (AV_CODEC_ID_XXX 참조)
      AVStream*          VideoStream;       // 비디오 스트림 정보
      AVCodecContext*    VideoCodecCtx;     // 비디오 코덱 정보
      StringA            VideoTrackName;    // 비디오 트랙 제목 (UTF8)

   // 선택된 오디오 트랙 정보
   public:
      int                AudioTrackNo;      // 선택된 오디오 트랙 번호 (-1이면 선택된 오디오 트랙이 없음을 의미함)
      int                NumChannels;       // 채널 수
      int                SampleRate;        // 초당 샘플 수
      int                BitsPerSample;     // 샘플당 비트 수
      int                Flag_FloatSample;     // 샘플 값이 실수형인지 여부
      uint64_t           ChannelLayout;     // 채널 레이아웃
      AVCodecID          AudioCodecID;      // 오디오 코덱 ID (AV_CODEC_ID_XXX 참조)
      AVSampleFormat     SampleFormat;      // 샘플 포맷 (AV_SAMPLE_FMT_XXX 참조)
      AVStream*          AudioStream;       // 오디오 스트림 정보
      AVCodecContext*    AudioCodecCtx;     // 오디오 코덱 정보
      StringA            AudioTrackLng;     // 오디오 트랙 언어 (UTF8)     
      StringA            AudioTrackName;    // 오디오 트랙 이름 (UTF8)

   // 선택된 자막 트랙 정보
   public:
      int                SubtitleTrackNo;   // 선택된 자막 트랙 번호 (-1이면 선택된 자막 트랙이 없음을 의미함)
      double             SubtitleTimeBase;  // 자막 타임 베이스 (sec 단위)
      AVCodecID          SubtitleCodecID;   // 자막 코덱 ID (AV_CODEC_ID_XXX 참조)
      AVStream*          SubtitleStream;    // 자막 스트림 정보
      AVCodecContext*    SubtitleCodecCtx;  // 자막 코덱 정보
      StringA            SubtitleHeader;    // 자막 헤더 (특히 MKV 포맷에서 ASS 자막의 헤더가 포함됨)
      StringA            SubtitleTrackLng;  // 자막 트랙 언어 (UTF8)
      StringA            SubtitleTrackName; // 자막 트랙 이름 (UTF8)

   public:
      FFMPEG_FileReader (   );
      virtual ~FFMPEG_FileReader (   );

   protected:
      void _Init                  (   );
      void GetTrackDescription    (AVDictionary* metadata,TrackInfo& ti);
      void ResetAudioTrackInfo    (   );
      void ResetSubtitleTrackInfo (   );
      void ResetVideoTrackInfo    (   );
   
   public:
      int operator ! (   ) const;
   
   public:
      virtual void Close (   );
   
   public: 
      int Open              (   );
      int Open              (const char *file_name);
      int Read              (byte **p_d_buffer,int &d_buf_size,int &time_pos,int &duration,int &flags);
      int ReadAudioTrack    (byte **p_d_buffer,int &d_buf_size,int &time_pos,int &duration);
      int ReadSubtitleTrack (byte **p_d_buffer,int &d_buf_size,int &time_pos,int &duration);
      int ReadVideoTrack    (byte **p_d_buffer,int &d_buf_size,int &time_pos,int &duration,int &flag_key_frame);
      int Seek              (int time_pos);
      int SetAudioTrack     (int track_no);
      int SetSubtitleTrack  (int track_no);
      int SetVideoTrack     (int track_no);

   // time_pos: msec 단위. 값이 -1이면 시간 위치를 못 읽어 오는 것을 의미한다.
};

 inline int FFMPEG_FileReader::operator ! (   ) const

{
   if (FormatCtx) return (FALSE);
   else return (TRUE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: FFMPEG_FileWriter
//
///////////////////////////////////////////////////////////////////////////////

 class FFMPEG_FileWriter
 
{
   protected:
      int FileHandle;
      int Flag_HeaderWritten;
      byte* IOBuffer;
   
   public:
      AVFormatContext *FormatCtx;

   public:
      FFMPEG_FileWriter (   );
      virtual ~FFMPEG_FileWriter (   );
   
   protected:
      void _Init (   );
   
   public:
      int operator ! (   ) const;
   
   public:
      virtual void Close (   );
   
   // 함수 호출 순서: Open() => AddVideoTrack()/AddAudioTrack() => WriteHeader() => Write() => Close()
   public:
      // 오디오 트랙을 추가한다.
      // 리턴값: 정상 동작 시 트랙의 인덱스 번호를 리턴한다.
      //         오류가 발생하면 0보다 작은 값을 리턴한다.
      int AddAudioTrack (AVCodecID codec_id,int n_channels,int sampling_rate,AVSampleFormat sample_format,uint64_t channel_layout = 0);
      // 비디오 트랙을 추가한다.
      // 리턴값: 정상 동작 시 트랙의 인덱스 번호를 리턴한다.
      //         오류가 발생하면 0보다 작은 값을 리턴한다.
      int AddVideoTrack (AVCodecID codec_id,int frm_width,int frm_height,float frm_rate);
      // 지정된 트랙에 해당하는 AVCodecContext 객체의 포인터를 얻는다.
      // track_idx: 트랙의 인덱스 번호.
      // 리턴값: 지정된 트랙에 해당하는 AVCodecContext 객체의 포인터.
      AVCodecContext* GetAVCodecContext (int track_idx);
      // 지정된 비디오 트랙의 시간 위치에 해당하는 프래임 위치를 리턴한다.
      // time_pos: 시간 위치. (millisecond 단위)
      // 리턴값  : 정상 동작 시 프래임 위치를 리턴한다.
      //           오류가 발생하면 0보다 작은 값을 리턴한다.
      double GetCurTimePos (int track_idx);
      int GetFramePos (int track_idx,int time_pos);
      // 헤더를 기록했으면 TRUE를 리턴한다.
      int IsHeaderWritten (   );      
      // 미디어 데이터를 쓰기 위한 파일을 연다.
      int Open (const char *file_name);
      // 파일에 헤더를 기록한다.
      int WriteHeader (   );
      // 미디어 데이터를 파일에 저장한다.
      // - frame_pos: int(32비트)에서 int64_t(64비트)로 변경하여, 타임 스탬프 값을 설정할 수 있도록 변경
      int Write (int track_idx,byte *s_buffer,int s_buf_size,int flag_key_frame = FALSE,int64_t frame_pos = 0LL);

};

 inline int FFMPEG_FileWriter::operator ! (   ) const

{
   if (FormatCtx) return (FALSE);
   else return (TRUE);
}

 inline int FFMPEG_FileWriter::IsHeaderWritten (   )
 
{
   return (Flag_HeaderWritten);
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

// FFMPEG 라이브러리를 닫는다.
void CloseFFMPEG (   );
// FFMPEG 라이브러리를 초기화한다.
// FFMPEG 라이브러리를 사용하기 전에 반드시 한 번 호출해야 한다.
void InitFFMPEG (   );

// Critical Section Lock/Unlock 함수
int  FFMPEG_CS_Lock   (   );
int  FFMPEG_CS_Unlock (   );

// 로그 출력의 활성화/비활성화 설정을 수행한다.
void FFMPEG_EnableLogOutput (int flag_enable);

}

#endif // defined(__LIB_FFMPEG) && !defined(__SYSBIOS)

#endif // __BCCL_FFMPEG_H

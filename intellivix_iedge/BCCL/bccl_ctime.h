#if !defined(__BCCL_CTIME_H)
#define __BCCL_CTIME_H

#include "bccl_define.h"

#if !defined(__WIN32)

/////////////////////////////////////////////////////////////////////////////////////////////
// CTimeSpan

 class CTimeSpan
 {
 public:
   CTimeSpan() throw();
   CTimeSpan(_In_ time_t time) throw();
   CTimeSpan(
      _In_ LONG lDays,
      _In_ int nHours,
      _In_ int nMins,
      _In_ int nSecs) throw();

   LONG GetDays() const throw();
   LONG GetTotalHours() const throw();
   LONG GetHours() const throw();
   LONG GetTotalMinutes() const throw();
   LONG GetMinutes() const throw();
   LONG GetTotalSeconds() const throw();
   LONG GetSeconds() const throw();

   time_t GetTimeSpan() const throw();

   CTimeSpan operator+(_In_ CTimeSpan span) const throw();
   CTimeSpan operator-(_In_ CTimeSpan span) const throw();
   CTimeSpan& operator+=(_In_ CTimeSpan span) throw();
   CTimeSpan& operator-=(_In_ CTimeSpan span) throw();
   bool operator==(_In_ CTimeSpan span) const throw();
   bool operator!=(_In_ CTimeSpan span) const throw();
   bool operator<(_In_ CTimeSpan span) const throw();
   bool operator>(_In_ CTimeSpan span) const throw();
   bool operator<=(_In_ CTimeSpan span) const throw();
   bool operator>=(_In_ CTimeSpan span) const throw();

 private:
   time_t m_timeSpan;
 };

/////////////////////////////////////////////////////////////////////////////////////////////
// CTime

 class CTime
 {
 public:
   static CTime GetCurrentTime() throw();
   static BOOL  IsValidFILETIME(_In_ const FILETIME& ft) throw();

   CTime() throw();
   CTime(_In_ time_t time) throw();
   CTime(
      _In_ int nYear,
      _In_ int nMonth,
      _In_ int nDay,
      _In_ int nHour,
      _In_ int nMin,
      _In_ int nSec,
      _In_ int nDST = -1);
   CTime(
      _In_ WORD wDosDate,
      _In_ WORD wDosTime,
      _In_ int nDST = -1);
   CTime(
      _In_ const SYSTEMTIME& st,
      _In_ int nDST = -1);
   CTime(
      _In_ const FILETIME& ft,
      _In_ int nDST = -1);
   CTime(
      _In_ const DBTIMESTAMP& dbts,
      _In_ int nDST = -1) throw();

   CTime& operator=(_In_ time_t time) throw();

   CTime& operator+=(_In_ CTimeSpan span) throw();
   CTime& operator-=(_In_ CTimeSpan span) throw();

   CTimeSpan operator-(_In_ CTime time) const throw();
   CTime operator-(_In_ CTimeSpan span) const throw();
   CTime operator+(_In_ CTimeSpan span) const throw();

   bool operator==(_In_ CTime time) const throw();
   bool operator!=(_In_ CTime time) const throw();
   bool operator<(_In_ CTime time) const throw();
   bool operator>(_In_ CTime time) const throw();
   bool operator<=(_In_ CTime time) const throw();
   bool operator>=(_In_ CTime time) const throw();

   struct tm* GetGmtTm(_Out_ struct tm* ptm) const;
   struct tm* GetLocalTm(_Out_ struct tm* ptm) const;

   bool GetAsSystemTime(_Out_ SYSTEMTIME& st) const throw();

   time_t GetTime() const throw();

   int GetYear() const throw();
   int GetMonth() const throw();
   int GetDay() const throw();
   int GetHour() const throw();
   int GetMinute() const throw();
   int GetSecond() const throw();
   int GetDayOfWeek() const throw();
 
 private:
   time_t m_time;
};

#endif // #if !defined(__WIN32)

#endif

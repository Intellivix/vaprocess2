﻿#ifndef __BCCL_DEFINE_H
#define __BCCL_DEFINE_H

#include "bccl_environ.h"

#if defined( __WIN32 )
#include <afxwin.h>
#include <afxmt.h>
#include <tchar.h>
#include <windowsx.h>
#else
#include "bccl_define_linux.h"
#include "bccl_cstring.h"
typedef CStdString CString;
typedef CStdStringA CStringA;
#endif

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdarg>
#include <cctype>
#include <ctime>
#include <cmath>

#include <chrono>

///////////////////////////////////////////////////////////////////////////////
//
// Type Redefinitions
//
///////////////////////////////////////////////////////////////////////////////

typedef unsigned int uint;
typedef unsigned char byte;
typedef unsigned char uchar;
typedef unsigned short ushort;
typedef unsigned long ulong;
typedef uint IPADDR;

#if defined( __WIN32 )
typedef wchar_t wchar;
typedef __int64 int64;
typedef unsigned __int64 uint64;
#endif

using chrono_tp = std::chrono::high_resolution_clock::time_point;
using chrono_duration_micro = std::chrono::duration<double, std::micro>;
using chrono_duration_milli = std::chrono::duration<double, std::milli>;

///////////////////////////////////////////////////////////////////////////////
//
// Definitions
//
///////////////////////////////////////////////////////////////////////////////

#if !defined( TRUE )
#define TRUE 1
#endif
#if !defined( FALSE )
#define FALSE 0
#endif
#if !defined( NULL )
#define NULL 0
#endif
#if !defined( DONE )
extern const int DONE;
#endif

#define MC_PI 3.141592653589f
#define MC_PI_2 1.570796326794f
#define MC_PI_4 0.785398163397f
#define MC_2_PI 6.283185307178f
#define MC_R_2_PI 2.506628274631f
#define MC_DEG2RAD 0.017453292520f
#define MC_RAD2DEG 57.29577951310f
#define MC_VSV (float)1E-20
#define MC_VLV (float)1E+20

#define DEF_BUFFER_SIZE 0x0200
#define MAX_BUFFER_SIZE 0x0800

#define REF_IMAGE_WIDTH 1280
#define REF_IMAGE_HEIGHT 720

#define _RESTRICT

///////////////////////////////////////////////////////////////////////////////
//
// Enumerations
//
///////////////////////////////////////////////////////////////////////////////

enum CHECK_SETUP_RESULT {
   CHECK_SETUP_RESULT_CHANGED     = 0x00000001,
   CHECK_SETUP_RESULT_RESTART     = 0x00000002,
   CHECK_SETUP_RESULT_RESTART_ALL = 0x00000004
};

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

#if !defined( MAKEFOURCC )
#define MAKEFOURCC( ch0, ch1, ch2, ch3 ) \
   ( ( DWORD )( BYTE )( ch0 ) | ( ( DWORD )( BYTE )( ch1 ) << 8 ) | ( ( DWORD )( BYTE )( ch2 ) << 16 ) | ( ( DWORD )( BYTE )( ch3 ) << 24 ) )
#endif
#if !defined( mmioFOURCC )
#define mmioFOURCC( ch0, ch1, ch2, ch3 ) MAKEFOURCC( ch0, ch1, ch2, ch3 )
#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class
//
///////////////////////////////////////////////////////////////////////////////

class CCriticalSectionSP {
public:
   CCriticalSection& m_pCS;
   CCriticalSectionSP( CCriticalSection& cs )
       : m_pCS( cs )
   {
      m_pCS.Lock();
   }
   ~CCriticalSectionSP()
   {
      m_pCS.Unlock();
   }
};

#endif

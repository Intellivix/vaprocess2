#include "bccl_color.h"
#include "bccl_image.h"
#include "bccl_file.h"

#if defined( __LIB_JPEG )
extern "C" {
#include <jpeglib.h>
};
#if defined( __WIN32 )
#if defined( _DEBUG )
#pragma comment( lib, "libjpeg_DS.lib" )
#else
#pragma comment( lib, "libjpeg_RS.lib" )
#endif
#endif
#endif

#if defined( __LIB_PNG )
extern "C" {
#include <png.h>
}
#if defined( __WIN32 )
#if defined( _DEBUG )
#pragma comment( lib, "libpng_DS.lib" )
#pragma comment( lib, "zlib_DS.lib" )
#else
#pragma comment( lib, "libpng_RS.lib" )
#pragma comment( lib, "zlib_RS.lib" )
#endif
#endif
#endif

namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// BMP File Reading/Writing Routines
//
///////////////////////////////////////////////////////////////////////////////

#if !defined( BI_RGB )
#define BI_RGB 0
#endif
#if !defined( BI_RLE8 )
#define BI_RLE8 1
#endif
#if !defined( BI_RLE4 )
#define BI_RLE4 2
#endif
#if !defined( BI_BITFIELDS )
#define BI_BITFIELDS 3
#endif

#define BMP_TP_OS2 0
#define BMP_TP_WINDOWS 1

struct BMPFileHeader

{
   byte Type[2];
   byte Size[4];
   byte Reserved1[2];
   byte Reserved2[2];
   byte OffBits[4];
};

struct BMPCoreHeader

{
   byte Width[2];
   byte Height[2];
   byte Planes[2];
   byte BitCount[2];
};

struct BMPInfoHeader

{
   byte Width[4];
   byte Height[4];
   byte Planes[2];
   byte BitCount[2];
   byte Compression[4];
   byte SizeImage[4];
   byte XPelsPerMeter[4];
   byte YPelsPerMeter[4];
   byte ClrUsed[4];
   byte ClrImportant[4];
};

class BMPFile

{
protected:
   int LineCounter;
   BArray2D Buffer;

public:
   int Type;
   int Width;
   int Height;
   int BPP;
   int LBS;
   int Compression;
   int NColors;
   int BottomUp;
   BArray2D Palette;

public:
   BMPFile();

public:
   void ConvertLine1BPP( byte* s_array, BGRPixel* d_array );
   void ConvertLine4BPP( byte* s_array, BGRPixel* d_array );
   void ConvertLine8BPP( byte* s_array, BGRPixel* d_array );
   void ConvertLine16BPP( byte* s_array, BGRPixel* d_array );
   void ConvertLine24BPP( byte* s_array, BGRPixel* d_array );
   void ConvertLine32BPP( byte* s_array, BGRPixel* d_array );
   int DecodeData( FileIO& file );
   int DecodeDataRLE4( FileIO& file );
   int DecodeDataRLE8( FileIO& file );
   int ReadHeader( FileIO& file );
   int ReadLine( BGRPixel* d_array );
   int ReadPalette( FileIO& file );
   int WriteHeader( FileIO& file );
   int WriteLine( FileIO& file, byte* s_array );
   int WriteLine( FileIO& file, BGRPixel* s_array );
   int WritePalette( FileIO& file );
};

BMPFile::BMPFile()

{
   LineCounter = 0;
}

void BMPFile::ConvertLine1BPP( byte* s_array, BGRPixel* d_array )

{
   int i, j, k;

   BGRPixel* p    = d_array;
   byte* palette0 = Palette[0];
   byte* palette1 = Palette[1];
   for( i = k = 0; i < LBS; i++ ) {
      byte c = s_array[i];
      for( j = 0; j < 8; j++ ) {
         if( k >= Width ) return;
         if( c & 0x80 ) {
            p->B = palette1[0];
            p->G = palette1[1];
            p->R = palette1[2];
         } else {
            p->B = palette0[0];
            p->G = palette0[1];
            p->R = palette0[2];
         }
         p++;
         k++;
         c <<= 1;
      }
   }
}

void BMPFile::ConvertLine4BPP( byte* s_array, BGRPixel* d_array )

{
   int i, k;

   BGRPixel* p = d_array;
   for( i = k = 0; i < LBS; i++ ) {
      byte c = s_array[i];
      if( k >= Width ) return;
      byte* palette = Palette[(int)( c >> 4 )];
      p->B          = palette[0];
      p->G          = palette[1];
      p->R          = palette[2];
      p++;
      k++;
      if( k >= Width ) return;
      palette = Palette[(int)( c & 0x0F )];
      p->B    = palette[0];
      p->G    = palette[1];
      p->R    = palette[2];
      p++;
      k++;
   }
}

void BMPFile::ConvertLine8BPP( byte* s_array, BGRPixel* d_array )

{
   int i, k;

   BGRPixel* p = d_array;
   for( i = k = 0; i < Width; i++ ) {
      byte* palette = Palette[(int)s_array[i]];
      p->B          = palette[0];
      p->G          = palette[1];
      p->R          = palette[2];
      p++;
      k++;
   }
}

void BMPFile::ConvertLine16BPP( byte* s_array, BGRPixel* d_array )

{
   int i, k;

   BGRPixel* p = d_array;
   for( i = k = 0; k < Width; k++, i += 2 ) {
#if defined( __CPU_LITTLE_ENDIAN )
      ushort rgb = *(ushort*)&s_array[i];
#else
      ushort rgb = ( (ushort)s_array[i + 1] << 8 ) | s_array[i];
#endif
      p->B = ( rgb & 0x7C00 ) >> 8;
      p->G = ( rgb & 0x03E0 ) >> 2;
      p->R = ( rgb & 0x001F ) << 3;
      p++;
   }
}

void BMPFile::ConvertLine24BPP( byte* s_array, BGRPixel* d_array )

{
   int i, k;

   BGRPixel* p = d_array;
   for( i = k = 0; k < Width; k++ ) {
      p->B = s_array[i++];
      p->G = s_array[i++];
      p->R = s_array[i++];
      p++;
   }
}

void BMPFile::ConvertLine32BPP( byte* s_array, BGRPixel* d_array )

{
   int i, k;

   BGRPixel* p = d_array;
   for( i = k = 0; k < Width; k++ ) {
      p->B = s_array[i++];
      p->G = s_array[i++];
      p->R = s_array[i++];
      p++;
      i++;
   }
}

int BMPFile::DecodeData( FileIO& file )

{
   switch( Compression ) {
   case BI_RGB:
      Buffer.Create( LBS, Height );
      if( file.Read( Buffer, LBS * Height, sizeof( byte ) ) ) return ( 1 );
      break;
   case BI_RLE4:
      if( DecodeDataRLE4( file ) ) return ( 1 );
      break;
   case BI_RLE8:
      if( DecodeDataRLE8( file ) ) return ( 1 );
      break;
   default:
      return ( 2 );
   }
   return ( DONE );
}

int BMPFile::DecodeDataRLE4( FileIO& file )

{
   int i, j, k;
   byte c, p;

   BPP = 8;
   LBS *= 2;
   Compression = BI_RLE8;
   Buffer.Create( LBS, Height );
   for( i = j = 0; i < Height; ) {
      if( file.ReadByte( c ) ) return ( 1 );
      if( c ) {
         if( file.ReadByte( p ) ) return ( 1 );
         byte p1 = p >> 4;
         byte p2 = p & 0x0F;
         int ej  = j + c;
         for( ;; ) {
            if( j >= ej ) break;
            Buffer[i][j++] = p1;
            if( j >= ej ) break;
            Buffer[i][j++] = p2;
         }
      } else {
         if( file.ReadByte( c ) ) return ( 1 );
         if( c > 2 ) {
            int n  = ( c + 1 ) / 2;
            int ej = j + c;
            for( k = 0; k < n; k++ ) {
               if( file.ReadByte( p ) ) return ( 1 );
               Buffer[i][j++] = p >> 4;
               if( j >= ej ) break;
               Buffer[i][j++] = p & 0x0F;
            }
            if( n % 2 )
               if( file.ReadByte( p ) ) return ( 1 );
         } else {
            if( c == 0 )
               i++, j = 0;
            else if( c == 1 )
               break;
            else {
               if( file.ReadByte( p ) ) return ( 1 );
               j += p;
               if( file.ReadByte( p ) ) return ( 1 );
               i += p;
            }
         }
      }
   }
   return ( DONE );
}

int BMPFile::DecodeDataRLE8( FileIO& file )

{
   int i, j, k;
   byte c, p;

   Buffer.Create( LBS, Height );
   for( i = j = 0; i < Height; ) {
      if( file.ReadByte( c ) ) return ( 1 );
      if( c ) {
         if( file.ReadByte( p ) ) return ( 1 );
         for( k            = 0; k < c; k++ )
            Buffer[i][j++] = p;
      } else {
         if( file.ReadByte( c ) ) return ( 1 );
         if( c > 2 ) {
            for( k = 0; k < c; k++ ) {
               if( file.ReadByte( p ) ) return ( 1 );
               Buffer[i][j++] = p;
            }
            if( c % 2 )
               if( file.ReadByte( p ) ) return ( 1 );
         } else {
            if( c == 0 )
               i++, j = 0;
            else if( c == 1 )
               break;
            else {
               if( file.ReadByte( p ) ) return ( 1 );
               j += p;
               if( file.ReadByte( p ) ) return ( 1 );
               i += p;
            }
         }
      }
   }
   return ( DONE );
}

int BMPFile::ReadHeader( FileIO& file )

{
   int size;
   byte buffer[4];
   BMPFileHeader file_header;
   BMPCoreHeader core_header;
   BMPInfoHeader info_header;

   if( file.Read( (byte*)&file_header, 14, sizeof( byte ) ) ) return ( 1 );
   if( file_header.Type[0] != (byte)'B' || file_header.Type[1] != (byte)'M' ) return ( 2 );
   if( file.Read( buffer, 4, sizeof( byte ) ) ) return ( 1 );
#if defined( __CPU_LITTLE_ENDIAN )
   CopyArray1D( buffer, 0, 1, 4, (byte*)&size, 0 );
#else
   CopyArray1D( buffer, 0, 1, 4, (byte*)&size, 0, CP_REVERSE );
#endif
   Width = Height = BPP = 0;
   if( size == 12 ) {
      Type = BMP_TP_OS2;
      if( file.Read( (byte*)&core_header, 8, sizeof( byte ) ) ) return ( 1 );
#if defined( __CPU_LITTLE_ENDIAN )
      CopyArray1D( core_header.Width, 0, 1, 2, (byte*)&Width, 0 );
      CopyArray1D( core_header.Height, 0, 1, 2, (byte*)&Height, 0 );
      CopyArray1D( core_header.BitCount, 0, 1, 2, (byte*)&BPP, 0 );
#else
      CopyArray1D( core_header.Width, 0, 1, 2, (byte*)&Width, 1, CP_REVERSE );
      CopyArray1D( core_header.Height, 0, 1, 2, (byte*)&Height, 1, CP_REVERSE );
      CopyArray1D( core_header.BitCount, 0, 1, 2, (byte*)&BPP, 1, CP_REVERSE );
#endif
      Compression = BI_RGB;
      NColors     = 0;
   } else if( size == 40 ) {
      Type = BMP_TP_WINDOWS;
      if( file.Read( (byte*)&info_header, 36, sizeof( byte ) ) ) return ( 1 );
#if defined( __CPU_LITTLE_ENDIAN )
      CopyArray1D( info_header.Width, 0, 1, 4, (byte*)&Width, 0 );
      CopyArray1D( info_header.Height, 0, 1, 4, (byte*)&Height, 0 );
      CopyArray1D( info_header.BitCount, 0, 1, 2, (byte*)&BPP, 0 );
      CopyArray1D( info_header.Compression, 0, 1, 4, (byte*)&Compression, 0 );
      CopyArray1D( info_header.ClrUsed, 0, 1, 4, (byte*)&NColors, 0 );
#else
      CopyArray1D( info_header.Width, 0, 1, 4, (byte*)&Width, 0, CP_REVERSE );
      CopyArray1D( info_header.Height, 0, 1, 4, (byte*)&Height, 0, CP_REVERSE );
      CopyArray1D( info_header.BitCount, 0, 1, 2, (byte*)&BPP, 1, CP_REVERSE );
      CopyArray1D( info_header.Compression, 0, 1, 4, (byte*)&Compression, 0, CP_REVERSE );
      CopyArray1D( info_header.ClrUsed, 0, 1, 4, (byte*)&NColors, 0, CP_REVERSE );
#endif
   } else
      return ( 3 );
   if( Height > 0 )
      BottomUp = TRUE;
   else {
      BottomUp = FALSE;
      Height   = -Height;
   }
   if( Compression != BI_RGB && Compression != BI_RLE4 && Compression != BI_RLE8 ) return ( 4 );
   switch( BPP ) {
   case 1:
      LBS = ( Width + 7 ) / 8;
      break;
   case 4:
      LBS = ( Width + 1 ) / 2;
      break;
   case 8:
      LBS = Width;
      break;
   case 16:
      LBS = Width * 2;
      break;
   case 24:
      LBS = Width * 3;
      break;
   case 32:
      LBS = Width * 4;
      break;
   default:
      return ( 6 );
   }
   LBS = ( LBS + 3 ) / 4 * 4;
   return ( DONE );
}

int BMPFile::ReadPalette( FileIO& file )

{
   if( Type == BMP_TP_OS2 ) {
      switch( BPP ) {
      case 1:
         Palette.Create( 3, 2 );
         break;
      case 4:
         Palette.Create( 3, 16 );
         break;
      case 8:
         Palette.Create( 3, 256 );
      case 24:
         return ( DONE );
      default:
         return ( 1 );
      }
   } else {
      if( NColors )
         Palette.Create( 4, NColors );
      else {
         switch( BPP ) {
         case 1:
            Palette.Create( 4, 2 );
            break;
         case 4:
            Palette.Create( 4, 16 );
            break;
         case 8:
            Palette.Create( 4, 256 );
            break;
         case 16:
         case 24:
         case 32:
            return ( DONE );
         default:
            return ( 1 );
         }
      }
   }
   if( file.Read( Palette, Palette.Width * Palette.Height, sizeof( byte ) ) )
      return ( 2 );
   else
      return ( DONE );
}

int BMPFile::ReadLine( BGRPixel* d_array )

{
   switch( BPP ) {
   case 1:
      ConvertLine1BPP( Buffer[LineCounter], d_array );
      break;
   case 4:
      ConvertLine4BPP( Buffer[LineCounter], d_array );
      break;
   case 8:
      ConvertLine8BPP( Buffer[LineCounter], d_array );
      break;
   case 16:
      ConvertLine16BPP( Buffer[LineCounter], d_array );
      break;
   case 24:
      ConvertLine24BPP( Buffer[LineCounter], d_array );
      break;
   case 32:
      ConvertLine32BPP( Buffer[LineCounter], d_array );
      break;
   default:
      return ( 1 );
   }
   LineCounter++;
   return ( DONE );
}

int BMPFile::WriteHeader( FileIO& file )

{
   int offset, ppm = 3846;
   byte buffer[4] = { 40, 0, 0, 0 };
   BMPFileHeader file_header;
   BMPInfoHeader info_header;

   if( BPP == 8 )
      offset = 1078;
   else if( BPP == 24 )
      offset = 54;
   else
      return ( 1 );
   int size = offset + LBS * Height;
   ClearArray1D( (byte*)&file_header, 1, sizeof( BMPFileHeader ) );
   ClearArray1D( (byte*)&info_header, 1, sizeof( BMPInfoHeader ) );
   file_header.Type[0]        = 'B';
   file_header.Type[1]        = 'M';
   info_header.Planes[0]      = 1;
   info_header.BitCount[0]    = (byte)BPP;
   info_header.Compression[0] = BI_RGB;
#if defined( __CPU_LITTLE_ENDIAN )
   CopyArray1D( (byte*)&size, 0, 1, 4, file_header.Size, 0 );
   CopyArray1D( (byte*)&offset, 0, 1, 4, file_header.OffBits, 0 );
   CopyArray1D( (byte*)&Width, 0, 1, 4, info_header.Width, 0 );
   CopyArray1D( (byte*)&Height, 0, 1, 4, info_header.Height, 0 );
   CopyArray1D( (byte*)&ppm, 0, 1, 4, info_header.XPelsPerMeter, 0 );
   CopyArray1D( (byte*)&ppm, 0, 1, 4, info_header.YPelsPerMeter, 0 );
#else
   CopyArray1D( (byte*)&size, 0, 1, 4, file_header.Size, 0, CP_REVERSE );
   CopyArray1D( (byte*)&offset, 0, 1, 4, file_header.OffBits, 0, CP_REVERSE );
   CopyArray1D( (byte*)&Width, 0, 1, 4, info_header.Width, 0, CP_REVERSE );
   CopyArray1D( (byte*)&Height, 0, 1, 4, info_header.Height, 0, CP_REVERSE );
   CopyArray1D( (byte*)&ppm, 0, 1, 4, info_header.XPelsPerMeter, 0, CP_REVERSE );
   CopyArray1D( (byte*)&ppm, 0, 1, 4, info_header.YPelsPerMeter, 0, CP_REVERSE );
#endif
   if( file.Write( (byte*)&file_header, 14, sizeof( byte ) ) ) return ( 2 );
   if( file.Write( buffer, 4, sizeof( byte ) ) ) return ( 2 );
   if( file.Write( (byte*)&info_header, 36, sizeof( byte ) ) ) return ( 2 );
   return ( DONE );
}

int BMPFile::WriteLine( FileIO& file, byte* s_array )

{
   if( file.Write( s_array, LBS, sizeof( byte ) ) ) return ( 1 );
   return ( DONE );
}

int BMPFile::WriteLine( FileIO& file, BGRPixel* s_array )

{
   if( file.Write( (byte*)s_array, LBS, sizeof( byte ) ) ) return ( 1 );
   return ( DONE );
}

int BMPFile::WritePalette( FileIO& file )

{
   int i;
   byte rgb[4];

   rgb[3] = 0;
   for( i = 0; i < 256; i++ ) {
      rgb[0] = rgb[1] = rgb[2] = (byte)i;
      if( file.Write( rgb, 1, sizeof( rgb ) ) ) return ( 1 );
   }
   return ( DONE );
}

int GImage::ReadBMPFile( const char* file_name )

{
   int i, j;

   FileIO file( file_name, FIO_BINARY, FIO_READ );
   if( !file ) return ( 1 );
   BMPFile bmp;
   if( bmp.ReadHeader( file ) ) return ( 2 );
   if( bmp.ReadPalette( file ) ) return ( 3 );
   if( bmp.DecodeData( file ) ) return ( 4 );
   Create( bmp.Width, bmp.Height );
   Array1D<BGRPixel> buffer( bmp.Width );
   if( bmp.BottomUp ) {
      for( i = bmp.Height - 1; i >= 0; i-- ) {
         if( bmp.ReadLine( buffer ) ) {
            Delete();
            return ( 5 );
         }
         byte* _RESTRICT l_Array = Array[i];
         BGRPixel* p             = buffer;
         for( j = 0; j < bmp.Width; j++ ) {
            l_Array[j] = ( byte )( ( 76 * (ushort)p->R + 150 * (ushort)p->G + 29 * (ushort)p->B ) >> 8 );
            p++;
         }
      }
   } else {
      for( i = 0; i < bmp.Height; i++ ) {
         if( bmp.ReadLine( buffer ) ) {
            Delete();
            return ( 5 );
         }
         byte* _RESTRICT l_Array = Array[i];
         BGRPixel* p             = buffer;
         for( j = 0; j < bmp.Width; j++ ) {
            l_Array[j] = ( byte )( ( 76 * (ushort)p->R + 150 * (ushort)p->G + 29 * (ushort)p->B ) >> 8 );
            p++;
         }
      }
   }
   return ( DONE );
}

int GImage::WriteBMPFile( const char* file_name )

{
   int i;

   FileIO file( file_name, FIO_BINARY, FIO_CREATE );
   if( !file ) return ( 1 );
   BMPFile bmp;
   bmp.Width  = Width;
   bmp.Height = Height;
   bmp.BPP    = 8;
   bmp.LBS    = ( Width + 3 ) / 4 * 4;
   if( bmp.WriteHeader( file ) ) return ( 2 );
   if( bmp.WritePalette( file ) ) return ( 2 );
   for( i = Height - 1; i >= 0; i-- )
      if( bmp.WriteLine( file, Array[i] ) ) return ( 2 );
   return ( DONE );
}

int BGRImage::ReadBMPFile( const char* file_name )

{
   int i;

   FileIO file( file_name, FIO_BINARY, FIO_READ );
   if( !file ) return ( 1 );
   BMPFile bmp;
   if( bmp.ReadHeader( file ) ) return ( 2 );
   if( bmp.ReadPalette( file ) ) return ( 3 );
   if( bmp.DecodeData( file ) ) return ( 4 );
   Create( bmp.Width, bmp.Height );
   if( bmp.BottomUp ) {
      for( i = bmp.Height - 1; i >= 0; i-- ) {
         if( bmp.ReadLine( Array[i] ) ) {
            Delete();
            return ( 5 );
         }
      }
   } else {
      for( i = 0; i < bmp.Height; i++ ) {
         if( bmp.ReadLine( Array[i] ) ) {
            Delete();
            return ( 5 );
         }
      }
   }
   return ( DONE );
}

int BGRImage::WriteBMPFile( const char* file_name )

{
   int i;

   FileIO file( file_name, FIO_BINARY, FIO_CREATE );
   if( !file ) return ( 1 );
   BMPFile bmp;
   bmp.Width  = Width;
   bmp.Height = Height;
   bmp.BPP    = 24;
   bmp.LBS    = ( Width * 3 + 3 ) / 4 * 4;
   if( bmp.WriteHeader( file ) ) return ( 2 );
   for( i = Height - 1; i >= 0; i-- )
      if( bmp.WriteLine( file, Array[i] ) ) return ( 2 );
   return ( DONE );
}

///////////////////////////////////////////////////////////////////////////////
//
// JPEG File Reading/Writing Routines
//
///////////////////////////////////////////////////////////////////////////////

int GImage::ReadJPGFile( const char* file_name )
#if defined( __LIB_JPEG )
{
   int i, j, k;
   jpeg_error_mgr jerr;
   jpeg_decompress_struct cinfo;

   FileIO file( file_name, FIO_BINARY, FIO_READ );
   if( !file ) return ( 1 );
   cinfo.err = jpeg_std_error( &jerr );
   jpeg_create_decompress( &cinfo );
   jpeg_stdio_src( &cinfo, file );
   jpeg_read_header( &cinfo, TRUE );
   jpeg_start_decompress( &cinfo );
   Create( cinfo.output_width, cinfo.output_height );
   if( cinfo.out_color_components == 1 ) {
      for( i = 0; i < Height; i++ ) {
         if( jpeg_read_scanlines( &cinfo, Array + i, 1 ) != 1 ) {
            jpeg_destroy_decompress( &cinfo );
            Delete();
            return ( 2 );
         }
      }
   } else {
      BArray2D buffer( cinfo.output_width * cinfo.out_color_components, 1 );
      byte* _RESTRICT s_buffer = buffer[0];
      for( i = 0; i < Height; i++ ) {
         if( jpeg_read_scanlines( &cinfo, buffer, 1 ) != 1 ) {
            jpeg_destroy_decompress( &cinfo );
            Delete();
            return ( 2 );
         }
         byte* _RESTRICT l_Array = Array[i];
         for( j = k    = 0; j < Width; j++, k += 3 )
            l_Array[j] = ( byte )( 0.299f * s_buffer[k] + 0.587f * s_buffer[k + 1] + 0.114f * s_buffer[k + 2] + 0.5f );
      }
   }
   jpeg_finish_decompress( &cinfo );
   jpeg_destroy_decompress( &cinfo );
   return ( DONE );
}
#else
{
   return ( -1 );
}
#endif

int GImage::WriteJPGFile( const char* file_name, int quality )
#if defined( __LIB_JPEG )
{
   int i;
   jpeg_error_mgr jerr;
   jpeg_compress_struct cinfo;

   FileIO file( file_name, FIO_BINARY, FIO_CREATE );
   if( !file ) return ( 1 );
   cinfo.err = jpeg_std_error( &jerr );
   jpeg_create_compress( &cinfo );
   jpeg_stdio_dest( &cinfo, file );
   cinfo.image_width      = Width;
   cinfo.image_height     = Height;
   cinfo.input_components = 1;
   cinfo.in_color_space   = JCS_GRAYSCALE;
   jpeg_set_defaults( &cinfo );
   jpeg_set_quality( &cinfo, quality, TRUE );
   jpeg_start_compress( &cinfo, TRUE );
   for( i = 0; i < Height; i++ ) {
      if( jpeg_write_scanlines( &cinfo, Array + i, 1 ) != 1 ) {
         jpeg_destroy_compress( &cinfo );
         return ( 2 );
      }
   }
   jpeg_finish_compress( &cinfo );
   jpeg_destroy_compress( &cinfo );
   return ( DONE );
}
#else
{
   return ( -1 );
}
#endif

int BGRImage::ReadJPGFile( const char* file_name )
#if defined( __LIB_JPEG )
{
   int i, j, k;
   jpeg_error_mgr jerr;
   jpeg_decompress_struct cinfo;

   FileIO file( file_name, FIO_BINARY, FIO_READ );
   if( !file ) return ( 1 );
   cinfo.err = jpeg_std_error( &jerr );
   jpeg_create_decompress( &cinfo );
   jpeg_stdio_src( &cinfo, file );
   jpeg_read_header( &cinfo, TRUE );
   jpeg_start_decompress( &cinfo );
   BArray2D buffer( cinfo.output_width * cinfo.out_color_components, 1 );
   byte* _RESTRICT s_buffer = buffer[0];
   Create( cinfo.output_width, cinfo.output_height );
   for( i = 0; i < Height; i++ ) {
      if( jpeg_read_scanlines( &cinfo, buffer, 1 ) != 1 ) {
         jpeg_destroy_decompress( &cinfo );
         Delete();
         return ( 2 );
      }
      BGRPixel* p = Array[i];
      if( cinfo.out_color_components == 1 ) {
         for( j = 0; j < Width; j++ ) {
            p->R = p->G = p->B = s_buffer[j];
            p++;
         }
      } else {
         for( j = k = 0; j < Width; j++ ) {
            p->R = s_buffer[k++];
            p->G = s_buffer[k++];
            p->B = s_buffer[k++];
            p++;
         }
      }
   }
   jpeg_finish_decompress( &cinfo );
   jpeg_destroy_decompress( &cinfo );
   return ( DONE );
}
#else
{
   return ( -1 );
}
#endif

int BGRImage::ReadJPGFile( FileIO& file )
#if defined( __LIB_JPEG )
{
   return ReadJPGFile( file.GetBuffer(), file.GetLength() );
}
#else
{
   return ( -1 );
}
#endif

int BGRImage::ReadJPGFile( byte* s_buffer, int s_length )
#if defined( __LIB_JPEG )
{
   int i, j, k;
   jpeg_error_mgr jerr;
   jpeg_decompress_struct cinfo;

   cinfo.err = jpeg_std_error( &jerr );
   jpeg_create_decompress( &cinfo );
   jpeg_mem_src( &cinfo, s_buffer, s_length );
   jpeg_read_header( &cinfo, TRUE );
   jpeg_start_decompress( &cinfo );
   BArray2D buffer( cinfo.output_width * cinfo.out_color_components, 1 );
   byte* _RESTRICT t_buffer = buffer[0];
   Create( cinfo.output_width, cinfo.output_height );
   for( i = 0; i < Height; i++ ) {
      if( jpeg_read_scanlines( &cinfo, buffer, 1 ) != 1 ) {
         jpeg_destroy_decompress( &cinfo );
         Delete();
         return ( 2 );
      }
      BGRPixel* p = Array[i];
      if( cinfo.out_color_components == 1 ) {
         for( j = 0; j < Width; j++ ) {
            p->R = p->G = p->B = t_buffer[j];
            p++;
         }
      } else {
         for( j = k = 0; j < Width; j++ ) {
            p->R = t_buffer[k++];
            p->G = t_buffer[k++];
            p->B = t_buffer[k++];
            p++;
         }
      }
   }
   jpeg_finish_decompress( &cinfo );
   jpeg_destroy_decompress( &cinfo );
   return ( DONE );
}
#else
{
   return ( -1 );
}
#endif

int BGRImage::WriteJPGFile( const char* file_name, int quality )
#if defined( __LIB_JPEG )
{
   int i, j, k;
   jpeg_error_mgr jerr;
   jpeg_compress_struct cinfo;

   FileIO file( file_name, FIO_BINARY, FIO_CREATE );
   if( !file ) return ( 1 );
   cinfo.err = jpeg_std_error( &jerr );
   jpeg_create_compress( &cinfo );
   jpeg_stdio_dest( &cinfo, file );
   cinfo.image_width      = Width;
   cinfo.image_height     = Height;
   cinfo.input_components = 3;
   cinfo.in_color_space   = JCS_RGB;
   jpeg_set_defaults( &cinfo );
   jpeg_set_quality( &cinfo, quality, TRUE );
   jpeg_start_compress( &cinfo, TRUE );
   BArray2D buffer( Width * 3, 1 );
   byte* _RESTRICT d_buffer = buffer[0];
   for( i = 0; i < Height; i++ ) {
      BGRPixel* p = Array[i];
      for( j = k = 0; j < Width; j++ ) {
         d_buffer[k++] = p->R;
         d_buffer[k++] = p->G;
         d_buffer[k++] = p->B;
         p++;
      }
      if( jpeg_write_scanlines( &cinfo, buffer, 1 ) != 1 ) {
         jpeg_destroy_compress( &cinfo );
         return ( 2 );
      }
   }
   jpeg_finish_compress( &cinfo );
   jpeg_destroy_compress( &cinfo );
   return ( DONE );
}
#else
{
   return ( -1 );
}
#endif

int BGRImage::WriteJPGFile( FileIO& file, int quality )
#if defined( __LIB_JPEG )
{
   ulong jpg_buff_size = 0;
   byte* tmp_buff      = new byte[0xFFFFF];
   if( WriteJPGFile( tmp_buff, jpg_buff_size, quality ) ) {
      delete[] tmp_buff;
      return ( 1 );
   }
   file.Write( tmp_buff, 1, jpg_buff_size );
   delete[] tmp_buff;
   return ( DONE );
}
#else
{
   return ( -1 );
}
#endif

int BGRImage::WriteJPGFile( byte* d_buffer, ulong& d_length, int quality )
#if defined( __LIB_JPEG )
{
   int i, j, k;
   jpeg_error_mgr jerr;
   jpeg_compress_struct cinfo;
   d_length = 0;

   cinfo.err = jpeg_std_error( &jerr );
   jpeg_create_compress( &cinfo );
   jpeg_mem_dest( &cinfo, (byte**)&d_buffer, &d_length );
   cinfo.image_width      = Width;
   cinfo.image_height     = Height;
   cinfo.input_components = 3;
   cinfo.in_color_space   = JCS_RGB;
   jpeg_set_defaults( &cinfo );
   jpeg_set_quality( &cinfo, quality, TRUE );
   jpeg_start_compress( &cinfo, TRUE );
   BArray2D buffer( Width * 3, 1 );
   byte* _RESTRICT t_buffer = buffer[0];
   for( i = 0; i < Height; i++ ) {
      BGRPixel* p = Array[i];
      for( j = k = 0; j < Width; j++ ) {
         t_buffer[k++] = p->R;
         t_buffer[k++] = p->G;
         t_buffer[k++] = p->B;
         p++;
      }
      if( jpeg_write_scanlines( &cinfo, buffer, 1 ) != 1 ) {
         jpeg_destroy_compress( &cinfo );
         return ( 1 );
      }
   }
   jpeg_finish_compress( &cinfo );
   jpeg_destroy_compress( &cinfo );
   return ( DONE );
}
#else
{
   return ( -1 );
}
#endif

///////////////////////////////////////////////////////////////////////////////
//
// PCX File Reading/Writing Routines
//
///////////////////////////////////////////////////////////////////////////////

struct PCXHeader

{
   byte Manufacturer;
   byte Version;
   byte Encoding;
   byte Bits;
   byte XMin[2];
   byte YMin[2];
   byte XMax[2];
   byte YMax[2];
   byte HRes[2];
   byte VRes[2];
   byte Palette[48];
   byte Reserved;
   byte Planes;
   byte BPL[2];
   byte PalType[2];
   byte Filler[58];
};

class PCXFile

{
public:
   int Version;
   int Width;
   int Height;
   int Planes;
   int BPL;
   int BPP;
   int LBS;
   BArray2D Palette;

public:
   void ConvertLine1BPP( BArray1D& s_array, BGRPixel* d_array );
   void ConvertLine4BPP1( BArray1D& s_array, BGRPixel* d_array );
   void ConvertLine4BPP2( BArray1D& s_array, BGRPixel* d_array );
   void ConvertLine8BPP( BArray1D& s_array, BGRPixel* d_array );
   void ConvertLine24BPP( BArray1D& s_array, BGRPixel* d_array );
   int DecodeLine( FileIO& file, BArray1D& d_array );
   int EncodeLine( FileIO& file, byte* s_array );
   int ReadHeader( FileIO& file );
   int ReadLine( FileIO& file, BGRPixel* d_array );
   int ReadPalette( FileIO& file );
   int WriteHeader( FileIO& file );
   int WritePalette( FileIO& file );
};

void PCXFile::ConvertLine1BPP( BArray1D& s_array, BGRPixel* d_array )

{
   int i, j, k;

   BGRPixel* p = d_array;
   for( i = k = 0; i < LBS; i++ ) {
      byte c = s_array[i];
      for( j = 0; j < 8; j++ ) {
         if( k >= Width ) return;
         if( c & 0x80 )
            p->R = p->G = p->B = PG_WHITE;
         else
            p->R = p->G = p->B = PG_BLACK;
         p++;
         k++;
         c <<= 1;
      }
   }
}

void PCXFile::ConvertLine4BPP1( BArray1D& s_array, BGRPixel* d_array )

{
   int i, k;

   BGRPixel* p = d_array;
   for( i = k = 0; i < LBS; i++ ) {
      byte c = s_array[i];
      if( k >= Width ) return;
      byte* palette = Palette[(int)( c >> 4 )];
      p->R          = palette[0];
      p->G          = palette[1];
      p->B          = palette[2];
      p++;
      k++;
      if( k >= Width ) return;
      palette = Palette[(int)( c & 0x0F )];
      p->R    = palette[0];
      p->G    = palette[1];
      p->B    = palette[2];
      p++;
      k++;
   }
}

void PCXFile::ConvertLine4BPP2( BArray1D& s_array, BGRPixel* d_array )

{
   int j, k;
   int i1, i2, i3, i4;

   BGRPixel* p = d_array;
   for( i1 = k = 0, i2 = BPL, i3 = BPL * 2, i4 = BPL * 3; i1 < BPL; i1++, i2++, i3++, i4++ ) {
      byte c1 = s_array[i1];
      byte c2 = s_array[i2];
      byte c3 = s_array[i3];
      byte c4 = s_array[i4];
      for( j = 0; j < 8; j++ ) {
         if( k >= Width ) return;
         byte p1       = ( c1 & 0x80 ) >> 7;
         byte p2       = ( c2 & 0x80 ) >> 6;
         byte p3       = ( c3 & 0x80 ) >> 5;
         byte p4       = ( c4 & 0x80 ) >> 4;
         byte* palette = Palette[(int)( p1 | p2 | p3 | p4 )];
         p->R          = palette[0];
         p->G          = palette[1];
         p->B          = palette[2];
         p++;
         k++;
         c1 <<= 1;
         c2 <<= 1;
         c3 <<= 1;
         c4 <<= 1;
      }
   }
}

void PCXFile::ConvertLine8BPP( BArray1D& s_array, BGRPixel* d_array )

{
   int i, k;

   BGRPixel* p = d_array;
   for( i = k = 0; i < Width; i++ ) {
      byte* palette = Palette[s_array[i]];
      p->R          = palette[0];
      p->G          = palette[1];
      p->B          = palette[2];
      p++;
      k++;
   }
}

void PCXFile::ConvertLine24BPP( BArray1D& s_array, BGRPixel* d_array )

{
   int i1, i2, i3, k;

   BGRPixel* p = d_array;
   for( i1 = k = 0, i2 = BPL, i3 = BPL * 2; i1 < Width; i1++, i2++, i3++ ) {
      p->R = s_array[i1];
      p->G = s_array[i2];
      p->B = s_array[i3];
      p++;
      k++;
   }
}

int PCXFile::DecodeLine( FileIO& file, BArray1D& d_array )

{
   int i;
   byte c;

   for( i = 0; i < LBS; ) {
      if( file.ReadByte( c ) ) return ( 1 );
      if( ( c & 0xC0 ) == 0xC0 ) {
         int n = c & 0x3F;
         if( file.ReadByte( c ) ) return ( 1 );
         if( i + n > LBS )
            return ( 2 );
         else
            while( n-- )
               d_array[i++] = (byte)c;
      } else
         d_array[i++] = (byte)c;
   }
   return ( DONE );
}

int PCXFile::EncodeLine( FileIO& file, byte* s_array )

{
   int i, j, t;

   i = j = t = 0;
   do {
      i = 0;
      while( ( s_array[t + i] == s_array[t + i + 1] ) && ( ( t + i ) < BPL ) && ( i < 63 ) )
         ++i;
      if( i > 0 ) {
         if( file.WriteByte( i | 0xC0 ) ) return ( 1 );
         if( file.WriteByte( s_array[t] ) ) return ( 1 );
         t += i;
         j += 2;
      } else {
         if( ( ( s_array[t] ) & 0xC0 ) == 0xC0 ) {
            if( file.WriteByte( 0xC1 ) ) return ( 1 );
            ++j;
         }
         if( file.WriteByte( s_array[t++] ) ) return ( 1 );
         ++j;
      }
   } while( t < BPL );
   return ( DONE );
}

int PCXFile::ReadHeader( FileIO& file )

{
   PCXHeader pcx_header;
   if( file.Read( (byte*)&pcx_header, 128, sizeof( byte ) ) ) return ( 1 );
   if( pcx_header.Manufacturer != 10 ) return ( 2 );
   BPL      = 0;
   int xmin = 0, ymin = 0, xmax = 0, ymax = 0;
#if defined( __CPU_LITTLE_ENDIAN )
   CopyArray1D( pcx_header.XMin, 0, 1, 2, (byte*)&xmin, 0 );
   CopyArray1D( pcx_header.YMin, 0, 1, 2, (byte*)&ymin, 0 );
   CopyArray1D( pcx_header.XMax, 0, 1, 2, (byte*)&xmax, 0 );
   CopyArray1D( pcx_header.YMax, 0, 1, 2, (byte*)&ymax, 0 );
   CopyArray1D( pcx_header.BPL, 0, 1, 2, (byte*)&BPL, 0 );
#else
   CopyArray1D( pcx_header.XMin, 0, 1, 2, (byte*)&xmin, 1, CP_REVERSE );
   CopyArray1D( pcx_header.YMin, 0, 1, 2, (byte*)&ymin, 1, CP_REVERSE );
   CopyArray1D( pcx_header.XMax, 0, 1, 2, (byte*)&xmax, 1, CP_REVERSE );
   CopyArray1D( pcx_header.YMax, 0, 1, 2, (byte*)&ymax, 1, CP_REVERSE );
   CopyArray1D( pcx_header.BPL, 0, 1, 2, (byte*)&BPL, 1, CP_REVERSE );
#endif
   Version = (int)pcx_header.Version;
   Width   = xmax - xmin + 1;
   Height  = ymax - ymin + 1;
   Planes  = (int)pcx_header.Planes;
   BPP     = (int)pcx_header.Bits * pcx_header.Planes;
   switch( BPP ) {
   case 1:
   case 8:
      LBS = BPL;
      break;
   case 4:
      if( pcx_header.Planes == 1 )
         LBS = BPL;
      else
         LBS = BPL * BPP;
      Palette.Create( 3, 16 );
      CopyArray1D( pcx_header.Palette, 0, 48, sizeof( byte ), Palette, 0 );
      break;
   case 24:
      LBS = BPL * 3;
      break;
   default:
      return ( 3 );
   }
   return ( DONE );
}

int PCXFile::ReadLine( FileIO& file, BGRPixel* d_array )

{
   BArray1D s_array( LBS );
   if( DecodeLine( file, s_array ) ) return ( 1 );
   switch( BPP ) {
   case 1:
      ConvertLine1BPP( s_array, d_array );
      break;
   case 4:
      if( Planes == 1 )
         ConvertLine4BPP1( s_array, d_array );
      else
         ConvertLine4BPP2( s_array, d_array );
      break;
   case 8:
      ConvertLine8BPP( s_array, d_array );
      break;
   case 24:
      ConvertLine24BPP( s_array, d_array );
      break;
   default:
      return ( 2 );
   }
   return ( DONE );
}

int PCXFile::ReadPalette( FileIO& file )

{
   byte c;

   if( BPP == 8 ) {
      int cur_pos = file.Where();
      if( file.Seek( -769, FIO_SEEK_END ) ) return ( 1 );
      file.ReadByte( c );
      if( c != 0x0C ) return ( 2 );
      Palette.Create( 3, 256 );
      file.Read( Palette, 768, sizeof( byte ) );
      file.Seek( cur_pos, FIO_SEEK_BEGIN );
   }
   return ( DONE );
}

int PCXFile::WriteHeader( FileIO& file )

{
   PCXHeader pcx_header;
   ClearArray1D( (byte*)&pcx_header, 1, sizeof( PCXHeader ) );
   pcx_header.Manufacturer = 10;
   pcx_header.Version      = (byte)Version;
   pcx_header.Encoding     = 1;
   pcx_header.Bits         = ( byte )( BPP / Planes );
   pcx_header.Planes       = (byte)Planes;
   pcx_header.PalType[0]   = 2;
   int xmax                = Width - 1;
   int ymax                = Height - 1;
#if defined( __CPU_LITTLE_ENDIAN )
   CopyArray1D( (byte*)&xmax, 0, 1, 2, pcx_header.XMax, 0 );
   CopyArray1D( (byte*)&ymax, 0, 1, 2, pcx_header.YMax, 0 );
   CopyArray1D( (byte*)&BPL, 0, 1, 2, pcx_header.BPL, 0 );
#else
   CopyArray1D( (byte*)&xmax, 1, 1, 2, pcx_header.XMax, 0, CP_REVERSE );
   CopyArray1D( (byte*)&ymax, 1, 1, 2, pcx_header.YMax, 0, CP_REVERSE );
   CopyArray1D( (byte*)&BPL, 1, 1, 2, pcx_header.BPL, 0, CP_REVERSE );
#endif
   if( file.Write( (byte*)&pcx_header, 1, sizeof( PCXHeader ) ) ) return ( 1 );
   return ( DONE );
}

int PCXFile::WritePalette( FileIO& file )

{
   int i;
   byte rgb[3];

   if( file.WriteByte( 0x0C ) ) return ( 1 );
   for( i = 0; i < 256; i++ ) {
      rgb[0] = rgb[1] = rgb[2] = (byte)i;
      if( file.Write( rgb, 1, sizeof( rgb ) ) ) return ( 1 );
   }
   return ( DONE );
}

int GImage::ReadPCXFile( const char* file_name )

{
   int i, j;

   FileIO file( file_name, FIO_BINARY, FIO_READ );
   if( !file ) return ( 1 );
   PCXFile pcx;
   if( pcx.ReadHeader( file ) ) return ( 2 );
   if( pcx.ReadPalette( file ) ) return ( 3 );
   Create( pcx.Width, pcx.Height );
   Array1D<BGRPixel> d_array( pcx.Width );
   for( i = 0; i < pcx.Height; i++ ) {
      if( pcx.ReadLine( file, d_array ) ) {
         Delete();
         return ( 3 );
      }
      BGRPixel* p   = d_array;
      byte* l_Array = Array[i];
      for( j = 0; j < pcx.Width; j++ ) {
         l_Array[j] = ( byte )( ( 76 * (ushort)p->R + 150 * (ushort)p->G + 29 * (ushort)p->B ) >> 8 );
         p++;
      }
   }
   return ( DONE );
}

int GImage::WritePCXFile( const char* file_name )

{
   int i;

   FileIO file( file_name, FIO_BINARY, FIO_CREATE );
   if( !file ) return ( 1 );
   PCXFile pcx;
   pcx.Version = 5;
   pcx.Width   = Width;
   pcx.Height  = Height;
   pcx.Planes  = 1;
   pcx.BPL     = Width;
   pcx.BPP     = 8;
   pcx.LBS     = Width;
   if( pcx.WriteHeader( file ) ) return ( 2 );
   for( i = 0; i < Height; i++ )
      if( pcx.EncodeLine( file, Array[i] ) ) return ( 3 );
   if( pcx.WritePalette( file ) ) return ( 4 );
   return ( DONE );
}

int BGRImage::ReadPCXFile( const char* file_name )

{
   int i;

   FileIO file( file_name, FIO_BINARY, FIO_READ );
   if( !file ) return ( 1 );
   PCXFile pcx;
   if( pcx.ReadHeader( file ) ) return ( 2 );
   if( pcx.ReadPalette( file ) ) return ( 3 );
   Create( pcx.Width, pcx.Height );
   for( i = 0; i < pcx.Height; i++ ) {
      if( pcx.ReadLine( file, Array[i] ) ) {
         Delete();
         return ( 4 );
      }
   }
   return ( DONE );
}

int BGRImage::WritePCXFile( const char* file_name )

{
   int i, j;

   FileIO file( file_name, FIO_BINARY, FIO_CREATE );
   if( !file ) return ( 1 );
   PCXFile pcx;
   pcx.Version = 5;
   pcx.Width   = Width;
   pcx.Height  = Height;
   pcx.Planes  = 3;
   pcx.BPL     = Width;
   pcx.BPP     = 24;
   pcx.LBS     = Width * 3;
   if( pcx.WriteHeader( file ) ) return ( 2 );
   BArray1D buffer( Width );
   for( i = 0; i < Height; i++ ) {
      BGRPixel* _RESTRICT l_Array = Array[i];
      for( j       = 0; j < Width; j++ )
         buffer[j] = l_Array[j].R;
      if( pcx.EncodeLine( file, buffer ) ) return ( 3 );
      for( j       = 0; j < Width; j++ )
         buffer[j] = l_Array[j].G;
      if( pcx.EncodeLine( file, buffer ) ) return ( 3 );
      for( j       = 0; j < Width; j++ )
         buffer[j] = l_Array[j].B;
      if( pcx.EncodeLine( file, buffer ) ) return ( 3 );
   }
   return ( DONE );
}

///////////////////////////////////////////////////////////////////////////////
//
// PGM File Reading/Writing Routines
//
///////////////////////////////////////////////////////////////////////////////

class PGMFile

{
public:
   int Width;
   int Height;
   int MaxValue;
   int DataType;

public:
   int ReadHeader( FileIO& file );
   int ReadLine( FileIO& file, byte* d_array );
   int WriteHeader( FileIO& file );
   int WriteLine( FileIO& file, byte* s_array );
   int WriteLine( FileIO& file, BGRPixel* s_array );
};

int PGMFile::ReadHeader( FileIO& file )

{
   byte lf;
   char c, magic[2];

   if( file.Read( (byte*)magic, 2, sizeof( char ) ) ) return ( 1 );
   if( magic[0] != 'P' )
      return ( 2 );
   else {
      switch( magic[1] ) {
      case '2':
         DataType = PGM_TEXT;
         break;
      case '5':
         DataType = PGM_BIN;
         break;
      default:
         return ( 3 );
      }
   }
   file.Scan( "%1s", &c );
   if( c == '#' ) {
      for( ;; ) {
         if( file.ReadByte( lf ) ) return ( 4 );
         if( lf == 0x0A ) break;
      }
   } else
      file.Seek( -1, FIO_SEEK_CURRENT );
   if( file.Scan( "%d %d %d", &Width, &Height, &MaxValue ) ) return ( 5 );
   if( DataType == PGM_BIN && MaxValue > PG_WHITE ) return ( 6 );
   if( file.Scan( "%1s", &c ) ) return ( 7 );
   file.Seek( -1, FIO_SEEK_CURRENT );
   return ( DONE );
}

int PGMFile::ReadLine( FileIO& file, byte* d_array )

{
   int i, p;

   if( DataType == PGM_TEXT ) {
      if( MaxValue == PG_WHITE ) {
         for( i = 0; i < Width; i++ ) {
            if( file.Scan( "%d", &p ) ) return ( 1 );
            d_array[i] = (byte)p;
         }
      } else {
         uint factor = ( uint )( (float)PG_WHITE / MaxValue * 65536.0f );
         for( i = 0; i < Width; i++ ) {
            if( file.Scan( "%d", &p ) ) return ( 1 );
            d_array[i] = ( byte )( ( factor * p ) >> 16 );
         }
      }
   } else {
      if( file.Read( d_array, Width, sizeof( byte ) ) ) return ( 2 );
      if( MaxValue < PG_WHITE ) {
         ushort factor = ( ushort )( (float)PG_WHITE / MaxValue * 256.0f );
         for( i        = 0; i < Width; i++ )
            d_array[i] = ( byte )( ( factor * d_array[i] ) >> 8 );
      }
   }
   return ( DONE );
}

int PGMFile::WriteHeader( FileIO& file )

{
   if( DataType == PGM_TEXT ) {
      if( file.Print( "P2\n" ) ) return ( 1 );
   } else {
      if( file.Print( "P5\n" ) ) return ( 1 );
   }
   if( file.Print( "# Portable Gray Map\n" ) ) return ( 2 );
   if( file.Print( "%d %d\n", Width, Height ) ) return ( 3 );
   if( file.Print( "%d\n", MaxValue ) ) return ( 4 );
   return ( DONE );
}

int PGMFile::WriteLine( FileIO& file, byte* s_array )

{
   int i;

   if( DataType == PGM_TEXT ) {
      for( i = 0; i < Width; i++ )
         if( file.Print( "%03d ", (int)s_array[i] ) ) return ( 1 );
      if( file.Print( "\n" ) ) return ( 1 );
   } else {
      if( file.Write( s_array, Width, sizeof( byte ) ) ) return ( 2 );
   }
   return ( DONE );
}

int PGMFile::WriteLine( FileIO& file, BGRPixel* s_array )

{
   int i;

   BGRPixel* p = s_array;
   BArray1D d_buffer( Width );
   for( i = 0; i < Width; i++ ) {
      d_buffer[i] = ( byte )( ( 76 * (ushort)p->R + 150 * (ushort)p->G + 29 * (ushort)p->B ) >> 8 );
      p++;
   }
   WriteLine( file, d_buffer );
   return ( DONE );
}

int GImage::ReadPGMFile( const char* file_name )

{
   int i;

   FileIO file( file_name, FIO_BINARY, FIO_READ );
   if( !file ) return ( 1 );
   PGMFile pgm;
   if( pgm.ReadHeader( file ) ) return ( 2 );
   Create( pgm.Width, pgm.Height );
   for( i = 0; i < Height; i++ ) {
      if( pgm.ReadLine( file, Array[i] ) ) {
         Delete();
         return ( 3 );
      }
   }
   return ( DONE );
}

int GImage::WritePGMFile( const char* file_name, int option )

{
   int i;

   FileIO file( file_name, FIO_BINARY, FIO_CREATE );
   if( !file ) return ( 1 );
   PGMFile pgm;
   pgm.Width    = Width;
   pgm.Height   = Height;
   pgm.MaxValue = PG_WHITE;
   pgm.DataType = option;
   if( pgm.WriteHeader( file ) ) return ( 2 );
   for( i = 0; i < Height; i++ )
      if( pgm.WriteLine( file, Array[i] ) ) return ( 3 );
   return ( DONE );
}

int BGRImage::ReadPGMFile( const char* file_name )

{
   int i, j;

   FileIO file( file_name, FIO_BINARY, FIO_READ );
   if( !file ) return ( 1 );
   PGMFile pgm;
   if( pgm.ReadHeader( file ) ) return ( 2 );
   Create( pgm.Width, pgm.Height );
   BArray1D buffer( pgm.Width );
   for( i = 0; i < Height; i++ ) {
      if( pgm.ReadLine( file, buffer ) ) {
         Delete();
         return ( 3 );
      }
      BGRPixel* p = Array[i];
      for( j = 0; j < buffer.Length; j++ ) {
         p->R = p->G = p->B = buffer[j];
         p++;
      }
   }
   return ( DONE );
}

int BGRImage::WritePGMFile( const char* file_name, int option )

{
   int i;

   FileIO file( file_name, FIO_BINARY, FIO_CREATE );
   if( !file ) return ( 1 );
   PGMFile pgm;
   pgm.Width    = Width;
   pgm.Height   = Height;
   pgm.MaxValue = PG_WHITE;
   pgm.DataType = option;
   if( pgm.WriteHeader( file ) ) return ( 2 );
   for( i = 0; i < Height; i++ )
      if( pgm.WriteLine( file, Array[i] ) ) return ( 3 );
   return ( DONE );
}

///////////////////////////////////////////////////////////////////////////////
//
// PNG File Reading/Writing Routines
//
///////////////////////////////////////////////////////////////////////////////

int GImage::ReadPNGFile( const char* file_name )
#if defined( __LIB_PNG )
{
   int i, j, n_bytes;
   int bit_depth, color_type;
   byte signature[8];
   ulong width, height;

   FileIO file( file_name, FIO_BINARY, FIO_READ );
   if( !file ) return ( 1 );
   if( file.Read( signature, 1, sizeof( signature ) ) ) return ( 2 );
   if( png_sig_cmp( signature, 0, 8 ) ) return ( 3 );
   png_structp png_ptr = png_create_read_struct( PNG_LIBPNG_VER_STRING, NULL, NULL, NULL );
   if( !png_ptr ) return ( 4 );
   png_infop info_ptr = png_create_info_struct( png_ptr );
   if( !info_ptr ) {
      png_destroy_read_struct( &png_ptr, NULL, NULL );
      return ( 5 );
   }
   png_init_io( png_ptr, file );
   png_set_sig_bytes( png_ptr, 8 );
   png_read_info( png_ptr, info_ptr );
   png_get_IHDR( png_ptr, info_ptr, &width, &height, &bit_depth, &color_type, NULL, NULL, NULL );
   Create( width, height );
   if( bit_depth == 16 ) png_set_strip_16( png_ptr );
   if( color_type & PNG_COLOR_MASK_ALPHA ) png_set_strip_alpha( png_ptr );
   if( color_type == PNG_COLOR_TYPE_PALETTE || bit_depth < 8 ) png_set_expand( png_ptr );
   png_read_update_info( png_ptr, info_ptr );
   color_type = png_get_color_type( png_ptr, info_ptr );
   if( color_type & PNG_COLOR_MASK_COLOR ) {
      if( color_type & PNG_COLOR_MASK_ALPHA )
         n_bytes = 4;
      else
         n_bytes = 3;
      BArray2D buffer( Width * n_bytes, Height );
      png_read_image( png_ptr, buffer );
      for( i = 0; i < Height; i++ ) {
         byte* p1 = buffer[i];
         byte* p2 = Array[i];
         for( j = 0; j < Width; j++ ) {
            *p2++ = ( byte )( 0.299f * p1[0] + 0.587f * p1[1] + 0.114f * p1[2] + 0.5f );
            p1 += n_bytes;
         }
      }
   } else
      png_read_image( png_ptr, Array );
   png_read_end( png_ptr, NULL );
   png_destroy_read_struct( &png_ptr, &info_ptr, NULL );
   return ( DONE );
}
#else
{
   return ( -1 );
}
#endif

int GImage::WritePNGFile( const char* file_name )
#if defined( __LIB_PNG )
{
   FileIO file( file_name, FIO_BINARY, FIO_CREATE );
   if( !file ) return ( 1 );
   png_structp png_ptr = png_create_write_struct( PNG_LIBPNG_VER_STRING, NULL, NULL, NULL );
   if( !png_ptr ) return ( 2 );
   png_infop info_ptr = png_create_info_struct( png_ptr );
   if( !info_ptr ) {
      png_destroy_write_struct( &png_ptr, (png_infopp)NULL );
      return ( 3 );
   }
   png_init_io( png_ptr, file );
   png_set_IHDR( png_ptr, info_ptr, Width, Height, 8, PNG_COLOR_TYPE_GRAY, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT );
   png_write_info( png_ptr, info_ptr );
   png_write_image( png_ptr, Array );
   png_write_end( png_ptr, info_ptr );
   png_destroy_write_struct( &png_ptr, &info_ptr );
   return ( DONE );
}
#else
{
   return ( -1 );
}
#endif

int BGRImage::ReadPNGFile( const char* file_name )
#if defined( __LIB_PNG )
{
   int i, j, n_bytes;
   int bit_depth, color_type;
   byte signature[8];
   ulong width, height;

   FileIO file( file_name, FIO_BINARY, FIO_READ );
   if( !file ) return ( 1 );
   if( file.Read( signature, 1, sizeof( signature ) ) ) return ( 2 );
   if( png_sig_cmp( signature, 0, 8 ) ) return ( 3 );
   png_structp png_ptr = png_create_read_struct( PNG_LIBPNG_VER_STRING, NULL, NULL, NULL );
   if( !png_ptr ) return ( 4 );
   png_infop info_ptr = png_create_info_struct( png_ptr );
   if( !info_ptr ) {
      png_destroy_read_struct( &png_ptr, NULL, NULL );
      return ( 5 );
   }
   png_init_io( png_ptr, file );
   png_set_sig_bytes( png_ptr, 8 );
   png_read_info( png_ptr, info_ptr );
   png_get_IHDR( png_ptr, info_ptr, &width, &height, &bit_depth, &color_type, NULL, NULL, NULL );
   Create( width, height );
   if( bit_depth == 16 ) png_set_strip_16( png_ptr );
   if( color_type & PNG_COLOR_MASK_ALPHA ) png_set_strip_alpha( png_ptr );
   if( color_type == PNG_COLOR_TYPE_PALETTE || bit_depth < 8 ) png_set_expand( png_ptr );
   if( color_type == PNG_COLOR_TYPE_GRAY || color_type == PNG_COLOR_TYPE_GRAY_ALPHA ) png_set_gray_to_rgb( png_ptr );
   png_read_update_info( png_ptr, info_ptr );
   color_type = png_get_color_type( png_ptr, info_ptr );
   if( color_type & PNG_COLOR_MASK_ALPHA )
      n_bytes = 4;
   else
      n_bytes = 3;
   BArray2D buffer( Width * n_bytes, Height );
   png_read_image( png_ptr, buffer );
   for( i = 0; i < Height; i++ ) {
      BGRPixel* p1 = Array[i];
      byte* p2     = buffer[i];
      for( j = 0; j < Width; j++ ) {
         p1->R = p2[0];
         p1->G = p2[1];
         p1->B = p2[2];
         p1++;
         p2 += n_bytes;
      }
   }
   png_read_end( png_ptr, NULL );
   png_destroy_read_struct( &png_ptr, &info_ptr, NULL );
   return ( DONE );
}
#else
{
   return ( -1 );
}
#endif

int BGRImage::WritePNGFile( const char* file_name )
#if defined( __LIB_PNG )
{
   int i, j;

   FileIO file( file_name, FIO_BINARY, FIO_CREATE );
   if( !file ) return ( 1 );
   png_structp png_ptr = png_create_write_struct( PNG_LIBPNG_VER_STRING, NULL, NULL, NULL );
   if( !png_ptr ) return ( 2 );
   png_infop info_ptr = png_create_info_struct( png_ptr );
   if( !info_ptr ) {
      png_destroy_write_struct( &png_ptr, (png_infopp)NULL );
      return ( 3 );
   }
   png_init_io( png_ptr, file );
   png_set_IHDR( png_ptr, info_ptr, Width, Height, 8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT );
   png_write_info( png_ptr, info_ptr );
   BArray1D buffer( Width * 3 );
   for( i = 0; i < Height; i++ ) {
      BGRPixel* p1 = Array[i];
      byte* p2     = buffer;
      for( j = 0; j < Width; j++ ) {
         *p2++ = p1->R;
         *p2++ = p1->G;
         *p2++ = p1->B;
         p1++;
      }
      png_write_row( png_ptr, buffer );
   }
   png_write_end( png_ptr, info_ptr );
   png_destroy_write_struct( &png_ptr, &info_ptr );
   return ( DONE );
}
#else
{
   return ( -1 );
}
#endif

///////////////////////////////////////////////////////////////////////////////
//
// RAS File Reading/Writing Routines
//
///////////////////////////////////////////////////////////////////////////////

#define RAS_MAGIC 0x59A66A95
#define RT_OLD 0
#define RT_STANDARD 1
#define RT_BYTE_ENCODED 2
#define RMT_NONE 0
#define RMT_EQUAL_RGB 1
#define RMT_RAW 2

struct RASHeader

{
   byte Magic[4];
   byte Width[4];
   byte Height[4];
   byte Depth[4];
   byte Length[4];
   byte Type[4];
   byte MapType[4];
   byte MapLength[4];
};

class RASFile

{
protected:
   int LineCounter;
   BArray2D Buffer;

public:
   int Width;
   int Height;
   int BPP;
   int LBS;
   int DataType;
   int DataLength;
   int PalType;
   int PalLength;
   BArray2D Palette;

public:
   RASFile();

public:
   void ConvertLine1BPP( byte* s_array, BGRPixel* d_array );
   void ConvertLine8BPP( byte* s_array, BGRPixel* d_array );
   void ConvertLine24BPP( byte* s_array, BGRPixel* d_array );
   int DecodeData( FileIO& file );
   int ReadHeader( FileIO& file );
   int ReadLine( BGRPixel* d_array );
   int ReadPalette( FileIO& file );
   int WriteHeader( FileIO& file );
   int WriteLine( FileIO& file, byte* s_array );
   int WriteLine( FileIO& file, BGRPixel* s_array );
   int WritePalette( FileIO& file );
};

RASFile::RASFile()

{
   LineCounter = 0;
}

void RASFile::ConvertLine1BPP( byte* s_array, BGRPixel* d_array )

{
   int i, j, k;

   BGRPixel* p = d_array;
   for( i = k = 0; i < LBS; i++ ) {
      byte c = s_array[i];
      for( j = 0; j < 8; j++ ) {
         if( k >= Width ) return;
         if( c & 0x80 )
            p->R = p->G = p->B = PG_BLACK;
         else
            p->R = p->G = p->B = PG_WHITE;
         p++;
         k++;
         c <<= 1;
      }
   }
}

void RASFile::ConvertLine8BPP( byte* s_array, BGRPixel* d_array )

{
   int i;

   BGRPixel* p = d_array;
   for( i = 0; i < Width; i++ ) {
      byte* palette = Palette[(int)s_array[i]];
      p->R          = palette[0];
      p->G          = palette[1];
      p->B          = palette[2];
   }
}

void RASFile::ConvertLine24BPP( byte* s_array, BGRPixel* d_array )

{
   int i, j;

   BGRPixel* p = d_array;
   if( DataType == RT_STANDARD ) {
      for( i = j = 0; i < Width; i++ ) {
         p->B = s_array[j++];
         p->G = s_array[j++];
         p->R = s_array[j++];
         p++;
      }
   } else {
      for( i = j = 0; i < Width; i++ ) {
         p->R = s_array[j++];
         p->G = s_array[j++];
         p->B = s_array[j++];
         p++;
      }
   }
}

int RASFile::DecodeData( FileIO& file )

{
   int i, counter;
   byte c, n;

   Buffer.Create( LBS, Height );
   byte* buffer = Buffer;
   int n_bytes  = LBS * Height;
   if( DataType == RT_BYTE_ENCODED ) {
      for( counter = 0; counter < n_bytes; ) {
         if( file.ReadByte( c ) ) return ( 1 );
         if( c == 128 ) {
            file.ReadByte( n );
            if( !n )
               buffer[counter++] = 128;
            else {
               file.ReadByte( c );
               for( i               = (int)n; i >= 0; i-- )
                  buffer[counter++] = c;
            }
         } else
            buffer[counter++] = c;
      }
   } else if( file.Read( buffer, n_bytes, sizeof( byte ) ) )
      return ( 2 );
   return ( DONE );
}

int RASFile::ReadHeader( FileIO& file )

{
   RASHeader ras_header;
   if( file.Read( (byte*)&ras_header, 32, sizeof( byte ) ) ) return ( 1 );
   int magic = 0;
   Width = Height = BPP = DataLength = DataType = PalType = PalLength = 0;
#if defined( __CPU_LITTLE_ENDIAN )
   CopyArray1D( ras_header.Magic, 0, 1, 4, (byte*)&magic, 0, CP_REVERSE );
   CopyArray1D( ras_header.Width, 0, 1, 4, (byte*)&Width, 0, CP_REVERSE );
   CopyArray1D( ras_header.Height, 0, 1, 4, (byte*)&Height, 0, CP_REVERSE );
   CopyArray1D( ras_header.Depth, 0, 1, 4, (byte*)&BPP, 0, CP_REVERSE );
   CopyArray1D( ras_header.Length, 0, 1, 4, (byte*)&DataLength, 0, CP_REVERSE );
   CopyArray1D( ras_header.Type, 0, 1, 4, (byte*)&DataType, 0, CP_REVERSE );
   CopyArray1D( ras_header.MapType, 0, 1, 4, (byte*)&PalType, 0, CP_REVERSE );
   CopyArray1D( ras_header.MapLength, 0, 1, 4, (byte*)&PalLength, 0, CP_REVERSE );
#else
   CopyArray1D( ras_header.Magic, 0, 1, 4, (byte*)&magic, 0 );
   CopyArray1D( ras_header.Width, 0, 1, 4, (byte*)&Width, 0 );
   CopyArray1D( ras_header.Height, 0, 1, 4, (byte*)&Height, 0 );
   CopyArray1D( ras_header.Depth, 0, 1, 4, (byte*)&BPP, 0 );
   CopyArray1D( ras_header.Length, 0, 1, 4, (byte*)&DataLength, 0 );
   CopyArray1D( ras_header.Type, 0, 1, 4, (byte*)&DataType, 0 );
   CopyArray1D( ras_header.MapType, 0, 1, 4, (byte*)&PalType, 0 );
   CopyArray1D( ras_header.MapLength, 0, 1, 4, (byte*)&PalLength, 0 );
#endif
   if( magic != RAS_MAGIC ) return ( 2 );
   switch( BPP ) {
   case 1:
   case 8:
      LBS = ( Width * BPP + 15 ) / 16 * 2;
      break;
   case 24:
      LBS = Width * 3;
      break;
   default:
      return ( 3 );
   }
   return ( DONE );
}

int RASFile::ReadLine( BGRPixel* d_array )

{
   switch( BPP ) {
   case 1:
      ConvertLine1BPP( Buffer[LineCounter], d_array );
      break;
   case 8:
      ConvertLine8BPP( Buffer[LineCounter], d_array );
      break;
   case 24:
      ConvertLine24BPP( Buffer[LineCounter], d_array );
      break;
   default:
      return ( 1 );
   }
   LineCounter++;
   return ( DONE );
}

int RASFile::ReadPalette( FileIO& file )

{
   int i, j, k;

   switch( PalType ) {
   case RMT_NONE:
      if( BPP == 8 ) {
         Palette.Create( 3, 256 );
         for( i           = 0; i < 256; i++ )
            Palette[i][0] = Palette[i][1] = Palette[i][2] = (byte)i;
      }
      break;
   case RMT_EQUAL_RGB:
      if( BPP == 8 ) {
         BArray1D palette( PalLength );
         if( file.Read( palette, PalLength, sizeof( byte ) ) ) return ( 1 );
         int n_entries = PalLength / 3;
         Palette.Create( 3, n_entries );
         for( i = k = 0; i < 3; i++ )
            for( j           = 0; j < n_entries; j++ )
               Palette[j][i] = palette[k++];
      } else
         return ( 2 );
      break;
   case RMT_RAW:
      if( file.Seek( PalLength, FIO_SEEK_CURRENT ) ) return ( 3 );
      break;
   default:
      return ( 4 );
   }
   return ( DONE );
}

int RASFile::WriteHeader( FileIO& file )

{
   int magic = RAS_MAGIC;
   RASHeader ras_header;

#if defined( __CPU_LITTLE_ENDIAN )
   CopyArray1D( (byte*)&magic, 0, 1, 4, ras_header.Magic, 0, CP_REVERSE );
   CopyArray1D( (byte*)&Width, 0, 1, 4, ras_header.Width, 0, CP_REVERSE );
   CopyArray1D( (byte*)&Height, 0, 1, 4, ras_header.Height, 0, CP_REVERSE );
   CopyArray1D( (byte*)&BPP, 0, 1, 4, ras_header.Depth, 0, CP_REVERSE );
   CopyArray1D( (byte*)&DataType, 0, 1, 4, ras_header.Type, 0, CP_REVERSE );
   CopyArray1D( (byte*)&DataLength, 0, 1, 4, ras_header.Length, 0, CP_REVERSE );
   CopyArray1D( (byte*)&PalType, 0, 1, 4, ras_header.MapType, 0, CP_REVERSE );
   CopyArray1D( (byte*)&PalLength, 0, 1, 4, ras_header.MapLength, 0, CP_REVERSE );
#else
   CopyArray1D( (byte*)&magic, 0, 1, 4, ras_header.Magic, 0 );
   CopyArray1D( (byte*)&Width, 0, 1, 4, ras_header.Width, 0 );
   CopyArray1D( (byte*)&Height, 0, 1, 4, ras_header.Height, 0 );
   CopyArray1D( (byte*)&BPP, 0, 1, 4, ras_header.Depth, 0 );
   CopyArray1D( (byte*)&DataType, 0, 1, 4, ras_header.Type, 0 );
   CopyArray1D( (byte*)&DataLength, 0, 1, 4, ras_header.Length, 0 );
   CopyArray1D( (byte*)&PalType, 0, 1, 4, ras_header.MapType, 0 );
   CopyArray1D( (byte*)&PalLength, 0, 1, 4, ras_header.MapLength, 0 );
#endif
   if( file.Write( (byte*)&ras_header, 1, sizeof( RASHeader ) ) ) return ( 1 );
   return ( DONE );
}

int RASFile::WriteLine( FileIO& file, byte* s_array )

{
   if( file.Write( s_array, LBS, sizeof( byte ) ) ) return ( 1 );
   return ( DONE );
}

int RASFile::WriteLine( FileIO& file, BGRPixel* s_array )

{
   if( file.Write( (byte*)s_array, LBS, sizeof( byte ) ) ) return ( 1 );
   return ( DONE );
}

int RASFile::WritePalette( FileIO& file )

{
   int i;

   BArray1D buffer( 256 );
   for( i       = 0; i < 256; i++ )
      buffer[i] = (byte)i;
   for( i = 0; i < 3; i++ )
      if( file.Write( buffer, 256, sizeof( byte ) ) ) return ( 1 );
   return ( DONE );
}

int GImage::ReadRASFile( const char* file_name )

{
   int i, j;

   FileIO file( file_name, FIO_BINARY, FIO_READ );
   if( !file ) return ( 1 );
   RASFile ras;
   if( ras.ReadHeader( file ) ) return ( 2 );
   if( ras.ReadPalette( file ) ) return ( 3 );
   if( ras.DecodeData( file ) ) return ( 4 );
   Create( ras.Width, ras.Height );
   Array1D<BGRPixel> d_array( ras.Width );
   for( i = 0; i < ras.Height; i++ ) {
      if( ras.ReadLine( d_array ) ) {
         Delete();
         return ( 5 );
      }
      for( j         = 0; j < ras.Width; j++ )
         Array[i][j] = ( byte )( ( 76 * (ushort)d_array[j].R + 150 * (ushort)d_array[j].G + 29 * (ushort)d_array[j].B ) >> 8 );
   }
   return ( DONE );
}

int GImage::WriteRASFile( const char* file_name )

{
   int i;

   FileIO file( file_name, FIO_BINARY, FIO_CREATE );
   if( !file ) return ( 1 );
   RASFile ras;
   ras.Width      = Width;
   ras.Height     = Height;
   ras.BPP        = 8;
   ras.LBS        = ( ras.Width * ras.BPP + 15 ) / 16 * 2;
   ras.DataType   = RT_STANDARD;
   ras.DataLength = ras.LBS * ras.Height;
   ras.PalType    = RMT_EQUAL_RGB;
   ras.PalLength  = 256 * 3;
   if( ras.WriteHeader( file ) ) return ( 1 );
   if( ras.WritePalette( file ) ) return ( 2 );
   for( i = 0; i < Height; i++ )
      if( ras.WriteLine( file, Array[i] ) ) return ( 3 );
   return ( DONE );
}

int BGRImage::ReadRASFile( const char* file_name )

{
   int i;

   FileIO file( file_name, FIO_BINARY, FIO_READ );
   if( !file ) return ( 1 );
   RASFile ras;
   if( ras.ReadHeader( file ) ) return ( 2 );
   if( ras.ReadPalette( file ) ) return ( 3 );
   if( ras.DecodeData( file ) ) return ( 4 );
   Create( ras.Width, ras.Height );
   for( i = 0; i < ras.Height; i++ ) {
      if( ras.ReadLine( Array[i] ) ) {
         Delete();
         return ( 5 );
      }
   }
   return ( DONE );
}

int BGRImage::WriteRASFile( const char* file_name )

{
   int i;

   FileIO file( file_name, FIO_BINARY, FIO_CREATE );
   if( !file ) return ( 1 );
   RASFile ras;
   ras.Width      = Width;
   ras.Height     = Height;
   ras.BPP        = 24;
   ras.LBS        = ras.Width * 3;
   ras.DataType   = RT_STANDARD;
   ras.DataLength = ras.LBS * ras.Height;
   ras.PalType    = RMT_NONE;
   ras.PalLength  = 0;
   if( ras.WriteHeader( file ) ) return ( 1 );
   for( i = 0; i < Height; i++ )
      if( ras.WriteLine( file, Array[i] ) ) return ( 2 );
   return ( DONE );
}

///////////////////////////////////////////////////////////////////////////////
//
// RAW File Reading/Writing Routines
//
///////////////////////////////////////////////////////////////////////////////

int GImage::ReadRAWFile( const char* file_name, int skip_bytes, int line_order )

{
   int i;

   FileIO file( file_name, FIO_BINARY, FIO_READ );
   if( !file ) return ( 1 );
   if( skip_bytes )
      if( file.Seek( skip_bytes, FIO_SEEK_BEGIN ) ) return ( 2 );
   if( line_order == LO_NORMAL ) {
      for( i = 0; i < Height; i++ )
         if( file.Read( Array[i], Width, sizeof( byte ) ) ) return ( 3 );
   } else {
      for( i = Height - 1; i >= 0; i-- )
         if( file.Read( Array[i], Width, sizeof( byte ) ) ) return ( 3 );
   }
   return ( DONE );
}

int GImage::WriteRAWFile( const char* file_name, int skip_bytes, int line_order )

{
   int i;

   FileIO file( file_name, FIO_BINARY, FIO_CREATE );
   if( !file ) return ( 1 );
   if( skip_bytes )
      if( file.Seek( skip_bytes, FIO_SEEK_BEGIN ) ) return ( 2 );
   if( line_order == LO_NORMAL ) {
      for( i = 0; i < Height; i++ )
         if( file.Write( Array[i], Width, sizeof( byte ) ) ) return ( 3 );
   } else {
      for( i = Height - 1; i >= 0; i-- )
         if( file.Write( Array[i], Width, sizeof( byte ) ) ) return ( 3 );
   }
   return ( DONE );
}

int BGRImage::ReadRAWFile( const char* file_name, int skip_bytes, int line_order, int cc_order )

{
   int i, j, k, l;

   FileIO file( file_name, FIO_BINARY, FIO_READ );
   if( !file ) return ( 1 );
   if( skip_bytes )
      if( file.Seek( skip_bytes, FIO_SEEK_BEGIN ) ) return ( 2 );
   int height1 = Height - 1;
   BArray1D buffer;
   switch( cc_order ) {
   case CCO_RGBRGB: {
      buffer.Create( Width * 3 );
      for( i = 0; i < Height; i++ ) {
         if( file.Read( buffer, buffer.Length, sizeof( byte ) ) ) return ( 3 );
         if( line_order == LO_NORMAL )
            j = i;
         else
            j        = height1 - i;
         BGRPixel* p = Array[j];
         for( k = l = 0; k < Width; k++ ) {
            p->R = buffer[l++];
            p->G = buffer[l++];
            p->B = buffer[l++];
            p++;
         }
      }
   } break;
   case CCO_BGRBGR: {
      int bpl = Width * 3;
      for( i = 0; i < Height; i++ ) {
         if( line_order == LO_NORMAL )
            j = i;
         else
            j = height1 - i;
         if( file.Read( (byte*)Array[j], bpl, sizeof( byte ) ) ) return ( 3 );
      }
   } break;
   case CCO_RRGGBB: {
      buffer.Create( Width );
      for( i = 0; i < 3; i++ ) {
         for( j = 0; j < Height; j++ ) {
            if( file.Read( buffer, Width, sizeof( byte ) ) ) return ( 3 );
            if( line_order == LO_NORMAL )
               k = j;
            else
               k                        = height1 - j;
            BGRPixel* _RESTRICT l_Array = Array[k];
            switch( i ) {
            case 0:
               for( l          = 0; l < Width; l++ )
                  l_Array[l].R = buffer[l];
               break;
            case 1:
               for( l          = 0; l < Width; l++ )
                  l_Array[l].G = buffer[l];
               break;
            case 2:
               for( l          = 0; l < Width; l++ )
                  l_Array[l].B = buffer[l];
               break;
            default:
               break;
            }
         }
      }
   } break;
   case CCO_BBGGRR: {
      buffer.Create( Width );
      for( i = 0; i < 3; i++ ) {
         for( j = 0; j < Height; j++ ) {
            if( file.Read( buffer, Width, sizeof( byte ) ) ) return ( 3 );
            if( line_order == LO_NORMAL )
               k = j;
            else
               k                        = height1 - j;
            BGRPixel* _RESTRICT l_Array = Array[k];
            switch( i ) {
            case 0:
               for( l          = 0; l < Width; l++ )
                  l_Array[l].B = buffer[l];
               break;
            case 1:
               for( l          = 0; l < Width; l++ )
                  l_Array[l].G = buffer[l];
               break;
            case 2:
               for( l          = 0; l < Width; l++ )
                  l_Array[l].R = buffer[l];
               break;
            default:
               break;
            }
         }
      }
   } break;
   default:
      break;
   }
   return ( DONE );
}

int BGRImage::WriteRAWFile( const char* file_name, int skip_bytes, int line_order, int cc_order )

{
   int i, j, k, l;

   FileIO file( file_name, FIO_BINARY, FIO_CREATE );
   if( !file ) return ( 1 );
   if( skip_bytes )
      if( file.Seek( skip_bytes, FIO_SEEK_BEGIN ) ) return ( 2 );
   int height1 = Height - 1;
   BArray1D buffer;
   switch( cc_order ) {
   case CCO_RGBRGB: {
      buffer.Create( Width * 3 );
      for( i = 0; i < Height; i++ ) {
         if( line_order == LO_NORMAL )
            j = i;
         else
            j        = height1 - i;
         BGRPixel* p = Array[j];
         for( k = l = 0; k < Width; k++ ) {
            buffer[l++] = p->R;
            buffer[l++] = p->G;
            buffer[l++] = p->B;
            p++;
         }
         if( file.Write( buffer, buffer.Length, sizeof( byte ) ) ) return ( 3 );
      }
   } break;
   case CCO_BGRBGR: {
      int bpl = Width * 3;
      for( i = 0; i < Height; i++ ) {
         if( line_order == LO_NORMAL )
            j = i;
         else
            j = height1 - i;
         if( file.Write( (byte*)Array[j], bpl, sizeof( byte ) ) ) return ( 3 );
      }
   } break;
   case CCO_RRGGBB: {
      buffer.Create( Width );
      for( i = 0; i < 3; i++ ) {
         for( j = 0; j < Height; j++ ) {
            if( line_order == LO_NORMAL )
               k = j;
            else
               k                        = height1 - j;
            BGRPixel* _RESTRICT l_Array = Array[k];
            switch( i ) {
            case 0:
               for( l       = 0; l < Width; l++ )
                  buffer[l] = l_Array[l].R;
               break;
            case 1:
               for( l       = 0; l < Width; l++ )
                  buffer[l] = l_Array[l].G;
               break;
            case 2:
               for( l       = 0; l < Width; l++ )
                  buffer[l] = l_Array[l].B;
               break;
            default:
               break;
            }
            if( file.Write( buffer, buffer.Length, sizeof( byte ) ) ) return ( 3 );
         }
      }
   } break;
   case CCO_BBGGRR: {
      buffer.Create( Width );
      for( i = 0; i < 3; i++ ) {
         for( j = 0; j < Height; j++ ) {
            if( line_order == LO_NORMAL )
               k = j;
            else
               k                        = height1 - j;
            BGRPixel* _RESTRICT l_Array = Array[k];
            switch( i ) {
            case 0:
               for( l       = 0; l < Width; l++ )
                  buffer[l] = l_Array[l].B;
               break;
            case 1:
               for( l       = 0; l < Width; l++ )
                  buffer[l] = l_Array[l].G;
               break;
            case 2:
               for( l       = 0; l < Width; l++ )
                  buffer[l] = l_Array[l].R;
               break;
            default:
               break;
            }
            if( file.Write( buffer, Width, sizeof( byte ) ) ) return ( 3 );
         }
      }
   } break;
   default:
      break;
   }
   return ( DONE );
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

int WriteImageFile_YUY2( const char* file_name, byte* s_img_buf, ISize2D& si_size )

{
   BGRImage s_image( si_size.Width, si_size.Height );
   ConvertYUY2ToBGR( s_img_buf, s_image );
   return ( s_image.WriteFile( file_name ) );
}
}

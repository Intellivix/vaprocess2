#if !defined(__BCCL_POLYGON_H)
#define __BCCL_POLYGON_H

#include "bccl_list.h"

 namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: Polygon
//
///////////////////////////////////////////////////////////////////////////////

 class Polygon

{
   public:
      int         ID;
      int         Type;
      IPoint2D    CenterPos;
      IP2DArray1D Vertices;
   
   public:
      Polygon (   );
      virtual ~Polygon (   ) {   };

   public:
      virtual int ReadFile   (FileIO &file);
      virtual int ReadFile   (CXMLIO* pIO);
      virtual int WriteFile  (FileIO &file);
      virtual int WriteFile  (CXMLIO* pIO);
      virtual int CheckSetup (Polygon* pg);

   public:
      void Create               (int n_vertices);
      void Delete               (   );
      void Fill                 (byte value,FPoint2D& scale,GImage &d_image);
      void Fill                 (ushort value,FPoint2D& scale,byte* d_buff,int width,int height);
      void GetBoundingBox       (IBox2D& d_box);
      int  IsPointInsidePolygon (IPoint2D &s_point);
      int  IsPointInsidePolygon (FPoint2D &s_point);
      void Offset               (int dx,int dy);
      void UpdateCenterPosition (   );
};

///////////////////////////////////////////////////////////////////////////////
//
// Class Template: PolygonList<T>
//
///////////////////////////////////////////////////////////////////////////////

 template<class T>
 class PolygonList : public LinkedList<T>

{
   public:
      int Width;
      int Height;
      
   public:
      PolygonList (   );

   public:
      virtual int ReadFile   (FileIO &file);
      virtual int ReadFile   (CXMLIO* pIO, const char* szNodeName);
      virtual int WriteFile  (FileIO &file);
      virtual int WriteFile  (CXMLIO* pIO, const char* szNodeName);
      virtual int CheckSetup (PolygonList<T>* pl);
   
   public:
      void DeletePolygon   (int id);
      void DeletePolygons  (FPoint2D &s_point);
      T*   FindPolygon     (int id);
      T*   FindPolygon     (IPoint2D &s_point,T *p_polygon = NULL);
      void RenderAreaMap   (GImage &d_image,byte p_value = PG_WHITE);
      void SetRefImageSize (ISize2D& ri_size);
};

 template<class T>
 PolygonList<T>::PolygonList (   )
 
{
   Width  = 640;
   Height = 480;
}

 template<class T>
 int PolygonList<T>::CheckSetup (PolygonList<T>* pl)

{
   int ret_flag = 0;

   FileIO buffThis, buffFrom;   
   CXMLIO xmlIOThis, xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis, XMLIO_Write);
   xmlIOFrom.SetFileIO (&buffFrom, XMLIO_Write);
   WriteFile (&xmlIOThis, "PL");
   pl->WriteFile (&xmlIOFrom, "PL");
   if (buffThis.IsDiff (buffFrom))
      ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength (0); buffFrom.SetLength (0);
   if (ret_flag & CHECK_SETUP_RESULT_CHANGED) {
      if (LinkedList<T>::GetNumItems () == pl->GetNumItems ()) {
         T* polygon_this = LinkedList<T>::First (   );
         T* polygon_from = pl->LinkedList<T>::First (   );
         while (polygon_this != NULL) {
            ret_flag |= polygon_this->CheckSetup (polygon_from);
            polygon_this = LinkedList<T>::Next (polygon_this);
            polygon_from = LinkedList<T>::Next (polygon_from);
         }
      }
   }
   return (ret_flag);
}

 template<class T>
 void PolygonList<T>::DeletePolygon (int id)

{
   T *polygon = LinkedList<T>::First (   );
   while (polygon != NULL) {
      if (polygon->ID == id) {
         LinkedList<T>::Remove (polygon);
         delete polygon;
         return;
      }
      polygon = LinkedList<T>::Next (polygon);
   }
}

 template<class T>
 void PolygonList<T>::DeletePolygons (FPoint2D &s_point)

{
   T *polygon = LinkedList<T>::First (   );
   while (polygon != NULL) {
      Polygon *n_polygon = LinkedList<T>::Next (n_polygon);
      if (polygon->IsPointInsidePolygon (s_point)) {
         LinkedList<T>::Remove (polygon);
         delete (polygon);
      }
      polygon = n_polygon;
   }
}

 template<class T>
 T* PolygonList<T>::FindPolygon (int id)

{
   Polygon *polygon = LinkedList<T>::First (   );
   while (polygon != NULL) {
      if (polygon->ID == id) return (polygon);
      polygon = LinkedList<T>::Next (polygon);
   }
   return (NULL);
}

 template<class T>
 T* PolygonList<T>::FindPolygon (IPoint2D &s_point,T *p_polygon)
 
{
   T *polygon;
   
   if (p_polygon == NULL) polygon = LinkedList<T>::First (   );
   else polygon = p_polygon->Next;
   while (polygon != NULL) {
      if (polygon->IsPointInsidePolygon (s_point)) return (polygon);
      polygon = LinkedList<T>::Next (polygon);
   }
   return (NULL);
}

 template<class T>
 int PolygonList<T>::ReadFile (FileIO &file)

{
   int i,n_items;

   if (file.Read ((byte*)&Width  ,1,sizeof(int))) return (1);
   if (file.Read ((byte*)&Height ,1,sizeof(int))) return (1);
   if (file.Read ((byte*)&n_items,1,sizeof(int))) return (1);
   if (n_items != LinkedList<T>::GetNumItems ()) {
      LinkedList<T>::Delete (   );
      for (i = 0; i < n_items; i++) {
         T *polygon = new T;
         if (DONE == polygon->ReadFile (file)) LinkedList<T>::Add (polygon);
         else delete polygon;
      }
   }
   else {
      T *polygon = LinkedList<T>::First (   );
      while (polygon != NULL) {
         polygon->ReadFile (file);
         polygon = LinkedList<T>::Next (polygon);
      }
   }
   return (DONE);
}

 template<class T>
 int PolygonList<T>::ReadFile (CXMLIO* pIO, const char* szNodeName)

{
   int i,n_items;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start (szNodeName))
   {
      xmlNode.Attribute (TYPE_ID(int),"Width"  ,&Width);
      xmlNode.Attribute (TYPE_ID(int),"Height" ,&Height);
      xmlNode.Attribute (TYPE_ID(int),"ItemNum",&n_items);
      if (n_items != LinkedList<T>::GetNumItems ()) {
         LinkedList<T>::Delete (   );
         for (i = 0; i < n_items; i++) {
            T *polygon = new T;
            if (DONE == polygon->ReadFile (pIO)) LinkedList<T>::Add (polygon);
            else delete polygon;
         }
      }
      else {
         T *polygon = LinkedList<T>::First (   );
         while (polygon != NULL) {
            polygon->ReadFile (pIO);
            polygon = LinkedList<T>::Next (polygon);
         }
      }
      xmlNode.End (   );
   }
   else {
      LinkedList<T>::Delete (   );
   }
   return (DONE);
 }

 template<class T>
 void PolygonList<T>::RenderAreaMap (GImage &d_image,byte p_value)

{
   FPoint2D scale;
   scale.X = (float)d_image.Width  / Width;
   scale.Y = (float)d_image.Height / Height;
   T *polygon = LinkedList<T>::First (   );
   while (polygon != NULL) {
      polygon->Fill (p_value,scale,d_image);
      polygon = LinkedList<T>::Next (polygon);
   }
}

 template<class T>
 void PolygonList<T>::SetRefImageSize (ISize2D& ri_size)
 
{
   Width  = ri_size.Width;
   Height = ri_size.Height;
}

 template<class T>
 int PolygonList<T>::WriteFile (FileIO &file)

{
   int n_items = LinkedList<T>::GetNumItems (   );
   if (file.Write ((byte*)&Width  ,1,sizeof(int))) return (1);
   if (file.Write ((byte*)&Height ,1,sizeof(int))) return (1);
   if (file.Write ((byte*)&n_items,1,sizeof(int))) return (1);
   T *polygon = LinkedList<T>::First (   );
   while (polygon != NULL) {
      if (polygon->WriteFile (file)) return (2);
      polygon = LinkedList<T>::Next (polygon);
   }
   return (DONE);
}

 template<class T>
 int PolygonList<T>::WriteFile (CXMLIO* pIO, const char* szNodeName)

{
   int n_items = LinkedList<T>::GetNumItems (   );
   if (n_items == 0) return (DONE);

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start (szNodeName))
   {
      xmlNode.Attribute (TYPE_ID(int),"Width"  ,&Width);
      xmlNode.Attribute (TYPE_ID(int),"Height" ,&Height);
      xmlNode.Attribute (TYPE_ID(int),"ItemNum",&n_items);
      T *polygon = LinkedList<T>::First (   );
      while (polygon != NULL) {
         polygon->WriteFile (pIO);
         polygon = LinkedList<T>::Next (polygon);
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: PolygonalArea
//
///////////////////////////////////////////////////////////////////////////////

 class PolygonalArea : public Polygon

{
   public:
      PolygonalArea *Prev,*Next;

   public:
      virtual ~PolygonalArea (   ) {   };
};

typedef PolygonList<PolygonalArea> PolygonalAreaList;

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

void ClipPolygon (Polygon *s_polygon,IBox2D &clip_box,Polygon *d_polygon);

}

#endif

// FIXME: This file has already broken encoding.

#include "bccl_socket.h"

 namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// Definitions
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__WIN32)
#define ERR_WOULDBLOCK   WSAEWOULDBLOCK
#define ERR_ISCONN       WSAEISCONN
#else
#define ERR_WOULDBLOCK   EWOULDBLOCK
#define ERR_ISCONN       EISCONN
#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: StreamSocket
//
///////////////////////////////////////////////////////////////////////////////

 StreamSocket::StreamSocket (   )

{
   Socket             = INVALID_SOCKET;
   RcvTimeout.tv_sec  = 0x7FFFFFFF;
   RcvTimeout.tv_usec = 0;
   SndTimeout.tv_sec  = 0x7FFFFFFF;
   SndTimeout.tv_usec = 0;
}

 StreamSocket::~StreamSocket (   )

{
   Close (   );
}

 int StreamSocket::Accept (int flag_block)

{
   ulong arg;
   int r_code = DONE;
   if (Socket == INVALID_SOCKET) return (-1);
   if (!flag_block) {
      arg = TRUE;
      ioctlsocket (Socket,FIONBIO,&arg);
   }
   SOCKET h_socket = accept (Socket,NULL,NULL);
   if (h_socket == INVALID_SOCKET) {
      int e_code = GetLastError (   );
      if (e_code == ERR_WOULDBLOCK) r_code = 1;
      else r_code = -2;
   }
   else {
      closesocket (Socket);
      Socket = h_socket;
      linger opt_ling = { 0,0 };
      setsockopt (Socket,SOL_SOCKET,SO_LINGER  ,(char*)&opt_ling  ,sizeof(linger ));
      setsockopt (Socket,SOL_SOCKET,SO_RCVTIMEO,(char*)&RcvTimeout,sizeof(timeval));
      setsockopt (Socket,SOL_SOCKET,SO_SNDTIMEO,(char*)&SndTimeout,sizeof(timeval));
   }
   if (!flag_block) {
      arg = FALSE;
      ioctlsocket (Socket,FIONBIO,&arg);
   }
   return (r_code);
}

 int StreamSocket::Accept (StreamSocket *d_socket,int flag_block)
 
{
   ulong arg;
   int r_code = DONE;
   if (Socket == INVALID_SOCKET) return (-1);
   if (!flag_block) {
      arg  = TRUE;
      ioctlsocket (Socket,FIONBIO,&arg);
   }
   SOCKET h_socket = accept (Socket,NULL,NULL);
   if (h_socket == INVALID_SOCKET) {
      int e_code = GetLastError (   );
      if (e_code == ERR_WOULDBLOCK) r_code = 1;
      else r_code = -2;
   }
   else {
      d_socket->AttachHandle (h_socket);
      linger opt_ling = { 0,0 };
      setsockopt (h_socket,SOL_SOCKET,SO_LINGER  ,(char*)&opt_ling  ,sizeof(linger ));
      setsockopt (h_socket,SOL_SOCKET,SO_RCVTIMEO,(char*)&RcvTimeout,sizeof(timeval));
      setsockopt (h_socket,SOL_SOCKET,SO_SNDTIMEO,(char*)&SndTimeout,sizeof(timeval));
   }
   if (!flag_block) {
      arg = FALSE;
      ioctlsocket (Socket,FIONBIO,&arg);
   }
   return (r_code);
}

 void StreamSocket::AttachHandle (SOCKET h_socket)
 
{
   Socket = h_socket;
}

 int StreamSocket::AreNewDataAvailable (   )

{
   byte temp;
   if (Socket == INVALID_SOCKET) return (-1);
   int r_value = TRUE;
   ulong arg   = TRUE;
   ioctlsocket (Socket,FIONBIO,&arg);
   if (recv (Socket,(char*)&temp,sizeof(temp),MSG_PEEK) == SOCKET_ERROR) {
      int e_code = GetLastError (   );
      if (e_code == ERR_WOULDBLOCK) r_value = FALSE;
      else r_value = -2;
   }
   arg = FALSE;
   ioctlsocket (Socket,FIONBIO,&arg);
   return (r_value);
}
 
 int StreamSocket::Connect (const char *ip_addr,ushort port)

{
   if (Create (   )) return (-1);
   SOCKADDR_IN address;
   address.sin_family      = AF_INET;
   address.sin_port        = 0;
   address.sin_addr.s_addr = htonl(INADDR_ANY);
   if (bind (Socket,(LPSOCKADDR)&address,sizeof(address)) == SOCKET_ERROR) return (-2);
   address.sin_family      = AF_INET;
   address.sin_port        = htons (port);
   address.sin_addr.s_addr = inet_addr(ip_addr);
/*
   ulong arg   = TRUE;
   ioctlsocket (Socket,FIONBIO,&arg);
*/   
   int e_code = connect (Socket,(LPSOCKADDR)&address,sizeof(address));
/*   
   arg = FALSE;
   ioctlsocket (Socket,FIONBIO,&arg);
*/   
   if (e_code == SOCKET_ERROR) return (-3);
   linger opt_ling = { 0,0 };
   if (setsockopt(Socket,SOL_SOCKET,SO_LINGER  ,(char*)&opt_ling  ,sizeof(linger )) == SOCKET_ERROR) return (-4);
   if (setsockopt(Socket,SOL_SOCKET,SO_RCVTIMEO,(char*)&RcvTimeout,sizeof(timeval)) == SOCKET_ERROR) return (-5);
   if (setsockopt(Socket,SOL_SOCKET,SO_SNDTIMEO,(char*)&SndTimeout,sizeof(timeval)) == SOCKET_ERROR) return (-6);
   return (DONE);
}

 void StreamSocket::Close (   )

{
   if (Socket != INVALID_SOCKET) {
      shutdown (Socket,SD_BOTH);
      closesocket (Socket);
      Socket = INVALID_SOCKET;
   }
}

 int StreamSocket::Create (   )

{
   Close (   );
   Socket = socket (AF_INET,SOCK_STREAM,0);
   if (Socket == INVALID_SOCKET) return (-1);
   return (DONE);
}

 void StreamSocket::DetachHandle (   )
 
{
   Socket = INVALID_SOCKET;
}

 int StreamSocket::GetLastError (   )
#if defined(__WIN32)
{
   return (WSAGetLastError (   ));
}
#elif defined(__linux)
{
   return (errno);
}
#endif

 int StreamSocket::GetPeerName (char *ip_addr,ushort &port)
 
{
   if (Socket == INVALID_SOCKET) return (-1);
   SOCKADDR_IN name;
   socklen_t size = sizeof(SOCKADDR_IN);
   if (getpeername (Socket,(sockaddr*)&name,&size) == SOCKET_ERROR) return (-2);
   port = name.sin_port;
   #if defined(__WIN32)
   sprintf (ip_addr,"%d.%d.%d.%d",name.sin_addr.s_net,name.sin_addr.s_host,name.sin_addr.s_lh,name.sin_addr.s_impno);
   #elif defined(__linux)
   in_addr_x s_addr;
   s_addr.s_addr_x = name.sin_addr.s_addr;
   sprintf (ip_addr,"%d.%d.%d.%d",s_addr.s_net_x,s_addr.s_host_x,s_addr.s_lh_x,s_addr.s_impno_x);
   #endif
   return (DONE);
}   

 int StreamSocket::Listen (ushort port)

{
   if (Create (   )) return (-1);
   SOCKADDR_IN address;
   address.sin_family      = AF_INET;
   address.sin_port        = htons (port);
   address.sin_addr.s_addr = htonl (INADDR_ANY);
   if (bind (Socket,(LPSOCKADDR)&address,sizeof(address)) == SOCKET_ERROR) return (-2);
   if (listen (Socket,SOMAXCONN) == SOCKET_ERROR) return (-3);
   return (DONE);
}

 int StreamSocket::ReceiveData (byte *d_buffer,int d_buf_size,int flag_block)

{
   if (d_buffer == NULL || !d_buf_size) return (0);
   if (Socket == INVALID_SOCKET) return (-1);
   int n_rcv_bytes = 0;
   if (flag_block) {
      while (d_buf_size) {
         int n_bytes_read = recv (Socket,(char*)d_buffer,d_buf_size,0);
         if (n_bytes_read == SOCKET_ERROR || !n_bytes_read) {
            n_rcv_bytes = -2;
            break;
         }
         n_rcv_bytes += n_bytes_read;
         d_buf_size  -= n_bytes_read;
         d_buffer    += n_bytes_read;
      }
   }
   else {
      ulong arg = TRUE;
      ioctlsocket (Socket,FIONBIO,&arg); // Non-blocking Mode占쏙옙 占쏙옙占쏙옙
      while (d_buf_size) {
         int n_bytes_read = recv (Socket,(char*)d_buffer,d_buf_size,0);
         if (n_bytes_read == SOCKET_ERROR || !n_bytes_read) {
            int e_code = GetLastError (   );
            if (e_code != ERR_WOULDBLOCK) n_rcv_bytes = -2;
               // e_code 占쏙옙占쏙옙 ERR_WOULDBLOCK占싱몌옙 占쏙옙 占싱삼옙 占쏙옙占쏙옙 占쏙옙占쏙옙占싶곤옙 占쏙옙占쏙옙占쏙옙 占실뱄옙占싼댐옙.
               // e_code占쏙옙 ERR_WOULDBLOCK 占싱울옙占쏙옙 占쏙옙占싱몌옙 占쏙옙占쏙옙占쏙옙 占쌩삼옙占쏙옙占쏙옙占쏙옙 占실뱄옙占싼댐옙.
            break;
         }
         n_rcv_bytes += n_bytes_read;
         d_buf_size  -= n_bytes_read;
         d_buffer    += n_bytes_read;
      }
      arg = FALSE;
      ioctlsocket (Socket,FIONBIO,&arg); // Blocking Mode占쏙옙 占쏙옙占쏙옙
   }
   return (n_rcv_bytes);
}

 int StreamSocket::SendData (byte *s_buffer,int s_buf_size,int flag_block)

{
   if (s_buffer == NULL || !s_buf_size) return (0);
   if (Socket == INVALID_SOCKET) return (-1);
   int n_snd_bytes = 0;
   if (flag_block) {
      while (s_buf_size) {
         int n_bytes_sent = send (Socket,(char*)s_buffer,s_buf_size,0);
         if (n_bytes_sent == SOCKET_ERROR) {
            n_snd_bytes = -2;
            break;
         }
         n_snd_bytes += n_bytes_sent;
         s_buf_size  -= n_bytes_sent;
         s_buffer    += n_bytes_sent;
      }
   }
   else {
      ulong arg = TRUE;
      ioctlsocket (Socket,FIONBIO,&arg); // Non-blocking Mode占쏙옙 占쏙옙占쏙옙
      while (s_buf_size) {
         int n_bytes_sent = send (Socket,(char*)s_buffer,s_buf_size,0);
         if (n_bytes_sent == SOCKET_ERROR) {
            int e_code = GetLastError (   );
            if (e_code != ERR_WOULDBLOCK) n_snd_bytes = -2;
            break;
         }
         n_snd_bytes += n_bytes_sent;
         s_buf_size  -= n_bytes_sent;
         s_buffer    += n_bytes_sent;
      }
      arg = FALSE;
      ioctlsocket (Socket,FIONBIO,&arg); // Blocking Mode占쏙옙 占쏙옙占쏙옙
   }
   return (n_snd_bytes);
}

 int StreamSocket::SetRcvTimeout (int t_sec,int t_usec)
 
{
   RcvTimeout.tv_sec  = t_sec;
   RcvTimeout.tv_usec = t_usec;
   if (Socket != INVALID_SOCKET) {
      if (setsockopt(Socket,SOL_SOCKET,SO_RCVTIMEO,(char*)&RcvTimeout,sizeof(timeval)) == SOCKET_ERROR) return (-1);
   }
   return (DONE);
}

 int StreamSocket::SetSndTimeout (int t_sec,int t_usec)
 
{
   RcvTimeout.tv_sec  = t_sec;
   RcvTimeout.tv_usec = t_usec;
   if (Socket != INVALID_SOCKET) {
      if (setsockopt(Socket,SOL_SOCKET,SO_SNDTIMEO,(char*)&SndTimeout,sizeof(timeval)) == SOCKET_ERROR) return (-1);
   }
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 void CloseWinsock (   )
#if defined(__WIN32) 
{
   WSACleanup (   );
}
#else
{

}
#endif

 int GetHostIPAddress (const char* host_name,char *ip_addr)
 
{
   hostent *he = gethostbyname (host_name);
   if (he == NULL) return (1);
   if (he->h_addr_list[0] == NULL) return (2);
   in_addr addr;
   memcpy (&addr,he->h_addr_list[0],sizeof(in_addr));
   #if defined(__WIN32)
   sprintf (ip_addr,"%d.%d.%d.%d",addr.s_net,addr.s_host,addr.s_lh,addr.s_impno);
   #elif defined(__linux)
   in_addr_x s_addr;
   s_addr.s_addr_x = addr.s_addr;
   sprintf (ip_addr,"%d.%d.%d.%d",s_addr.s_net_x,s_addr.s_host_x,s_addr.s_lh_x,s_addr.s_impno_x);
   #endif
   return (DONE);
}

 int GetLocalHostIPAddress (char *ip_addr)
 
{
   char host_name[DEF_BUFFER_SIZE];
   if (gethostname (host_name,sizeof(host_name)) == SOCKET_ERROR) return (1);
   return (GetHostIPAddress (host_name,ip_addr));
}

 int InitWinsock (   )
#if defined(__WIN32) 
{
   WSADATA wsa_data;
   if (WSAStartup (MAKEWORD(2,2),&wsa_data)) return (-1);
   return (DONE);
}
#else
{
   return (DONE);
}
#endif

 int IsIPAddressValid (const char *ip_addr)

{ 
   int  i,n;
   char buffer[4];
   
   if (ip_addr == NULL) return (FALSE);
   int len = (int)strlen (ip_addr); 
   if (len < 7 || len > 15) return (FALSE); 
   int nNumCount = 0; 
   int nDotCount = 0; 
   for (i = 0; i < len; i++) {
      if (ip_addr[i] < '0' || ip_addr[i] > '9') {
         if (ip_addr[i] == '.') {
            buffer[nNumCount] = 0;
            n = atoi (buffer);
            if (n < 0 || n > 255) return (FALSE);
            nDotCount++;
            nNumCount = 0;
         } 
         else return (FALSE); 
      } 
      else {
         if (nNumCount > 3) return (FALSE); 
         buffer[nNumCount] = ip_addr[i];
         nNumCount++;
      } 
   } 
   buffer[nNumCount] = 0;
   n = atoi (buffer);
   if (n < 0 || n > 255) return (FALSE);
   if (nDotCount != 3) return (FALSE); 
   return (TRUE); 
} 

}

#if !defined(__BCCL_IMAGE_H)
#define __BCCL_IMAGE_H

#include "bccl_array.h"
#include "bccl_file.h"

 namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// Definitions
//
///////////////////////////////////////////////////////////////////////////////

#define CCO_RRGGBB    0
#define CCO_BBGGRR    1
#define CCO_RGBRGB    2
#define CCO_BGRBGR    3

#define JPG_QUALITY   75

#define LO_NORMAL     0
#define LO_REVERSE    1

#define PG_BLACK      0
#define PG_WHITE      255

#define PGM_TEXT      0
#define PGM_BIN       1

///////////////////////////////////////////////////////////////////////////////
//
// Class Declarations
//
///////////////////////////////////////////////////////////////////////////////

class BGRImage;

///////////////////////////////////////////////////////////////////////////////
//
// Class: GImage
//
///////////////////////////////////////////////////////////////////////////////

 class GImage : public BArray2D

{
   public:
      int     Margin;
      GImage* AlphaChannel;

   public:
      GImage (   );
      GImage (int width,int height,int margin = 0);
      GImage (byte* s_buffer,int width,int height,int margin = 0);
      GImage (byte** s_array,int width,int height,int margin = 0);
      GImage (const BGRImage& s_cimage);
      GImage (const GImage& s_image);
      GImage (GImage* s_image);

   public:
      virtual ~GImage (   );

   public:
             GImage& operator =  (const GImage& s_image);
             GImage& operator =  (const BGRImage& s_cimage);
             GImage  operator ~  (   );
             GImage  operator |  (const GImage& s_image);
             GImage  operator |  (byte p);
      friend GImage  operator |  (byte,const GImage& s_image);
             GImage  operator &  (const GImage& s_image);
             GImage  operator &  (byte p);
      friend GImage  operator &  (byte p,const GImage& s_image);
             GImage  operator ^  (const GImage& s_image);
             GImage  operator ^  (byte p);
      friend GImage  operator ^  (byte p,const GImage& s_image);
             GImage& operator |= (const GImage& s_image);
             GImage& operator |= (byte p);
             GImage& operator &= (const GImage& s_image);
             GImage& operator &= (byte p);
             GImage& operator ^= (const GImage& s_image);
             GImage& operator ^= (byte p);
                     operator BGRImage (   ) const;

   protected:
      void _Init (   );

   public:
      virtual void Clear  (   );
      virtual int  Create (int width,int height);
      virtual void Delete (   );
      virtual int  Import (byte*  s_buffer,int width,int height);
      virtual int  Import (byte** s_array ,int width,int height);

   public:
      int    Create             (int width,int height,int margin);
      int    CreateAlphaChannel (   );
      void   DeleteAlphaChannel (   );
      GImage Extract            (int sx,int sy,int width,int height);
      GImage Extract            (IBox2D &s_box);
      int    GetLineLength      (   ) const;
      int    Import             (byte *s_buffer,int width,int height,int margin);
      int    Import             (byte **s_array,int width,int height,int margin);
      int    ReadBMPFile        (const char *file_name);
      int    ReadFile           (const char *file_name);
      int    ReadJPGFile        (const char *file_name);
      int    ReadPCXFile        (const char *file_name);
      int    ReadPGMFile        (const char *file_name);
      int    ReadPNGFile        (const char *file_name);
      int    ReadRASFile        (const char *file_name);
      int    ReadRAWFile        (const char *file_name,int skip_bytes = 0,int line_order = LO_NORMAL);
      void   SetMask            (byte m_color);
      int    WriteBMPFile       (const char *file_name);
      int    WriteFile          (const char *file_name);
      int    WriteJPGFile       (const char *file_name,int quality = JPG_QUALITY);
      int    WritePCXFile       (const char *file_name);
      int    WritePGMFile       (const char *file_name,int option = PGM_BIN);
      int    WritePNGFile       (const char *file_name);
      int    WriteRASFile       (const char *file_name);
      int    WriteRAWFile       (const char *file_name,int skip_bytes = 0,int line_order = LO_NORMAL);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: BGRPixel
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__WIN32) || defined(__linux)
#pragma pack(1)
#endif

 class BGRPixel

{
   public:
      byte B,G,R;

   public:
      BGRPixel (   );
      BGRPixel (byte b,byte g,byte r);

   public:
      byte& operator [] (int i) const;
      void  operator () (byte b,byte g,byte r);

   public:
      void Clear (   );
};

#if defined(__WIN32) || defined(__linux)
#pragma pack()
#endif

typedef BGRPixel BGRColor;

 inline byte& BGRPixel::operator [] (int i) const

{
   return (((byte*)this)[i]);
}

 inline void BGRPixel::operator () (byte b,byte g,byte r)

{
   B = b, G = g, R = r;
}

 inline void BGRPixel::Clear (   )

{
   B = G = R = 0;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: BGRImage
//
///////////////////////////////////////////////////////////////////////////////

 class BGRImage : public Array2D<BGRPixel>

{
   public:
      int     Margin;
      GImage* AlphaChannel;

   public:
      BGRImage (   );
      BGRImage (int width,int height,int margin = 0);
      BGRImage (BGRPixel *s_buffer,int width,int height,int margin = 0);
      BGRImage (BGRPixel **s_array,int width,int height,int margin = 0);
      BGRImage (const BGRImage &s_image);
      BGRImage (const GImage &s_image);
      BGRImage (BGRImage *s_image);

   public:
      virtual ~BGRImage (   );

   public:
             BGRImage& operator =  (const BGRImage &s_image);
             BGRImage& operator =  (const GImage &s_image);
             BGRImage  operator ~  (   );
             BGRImage  operator |  (const BGRImage &s_image);
             BGRImage  operator |  (const GImage &s_image);
      friend BGRImage  operator |  (const GImage &s_image1,const BGRImage &s_image2);
             BGRImage  operator |  (const BGRPixel &p);
      friend BGRImage  operator |  (const BGRPixel &p,const BGRImage &s_image);
             BGRImage  operator &  (const BGRImage &s_image);
             BGRImage  operator &  (const GImage &s_image);
      friend BGRImage  operator &  (const GImage &s_image1,const BGRImage &s_image2);
             BGRImage  operator &  (const BGRPixel &p);
      friend BGRImage  operator &  (const BGRPixel &p,const BGRImage &s_image);
             BGRImage  operator ^  (const BGRImage &s_image);
             BGRImage  operator ^  (const GImage &s_image);
      friend BGRImage  operator ^  (const GImage &s_image1,const BGRImage &s_image2);
             BGRImage  operator ^  (const BGRPixel &p);
      friend BGRImage  operator ^  (const BGRPixel &p,const BGRImage &s_image);
             BGRImage& operator |= (const BGRImage &s_image);
             BGRImage& operator |= (const GImage &s_image);
             BGRImage& operator |= (const BGRPixel &p);
             BGRImage& operator &= (const BGRImage &s_image);
             BGRImage& operator &= (const GImage &s_image);
             BGRImage& operator &= (const BGRPixel &p);
             BGRImage& operator ^= (const BGRImage &s_image);
             BGRImage& operator ^= (const GImage &s_image);
             BGRImage& operator ^= (const BGRPixel &p);
                       operator GImage (   ) const;

   protected:
      void _Init (   );

   public:
      virtual void Clear  (   );
      virtual int  Create (int width,int height);
      virtual void Delete (   );
      virtual int  Import (BGRPixel *s_buffer,int width,int height);
      virtual int  Import (BGRPixel **s_array,int width,int height);

   public:
      int      Create             (int width,int height,int margin);
      int      CreateAlphaChannel (   );
      void     DeleteAlphaChannel (   );
      BGRImage Extract            (int sx,int sy,int width,int height);
      BGRImage Extract            (IBox2D &s_box);
      int      GetLineLength      (   ) const;
      void     GetPlane           (int n,GImage &d_image);
      void     GetPlanes          (GImage& d_image_r,GImage& d_image_g,GImage& d_image_b);
      int      Import             (BGRPixel *s_buffer,int width,int height,int margin);
      int      Import             (BGRPixel **s_array,int width,int height,int margin);
      int      ReadBMPFile        (const char *file_name);
      int      ReadFile           (const char *file_name);
      int      ReadJPGFile        (const char *file_name);
      int      ReadJPGFile        (FileIO& file);
      int      ReadJPGFile        (byte *s_buffer,int s_length);
      int      ReadPCXFile        (const char *file_name);
      int      ReadPGMFile        (const char *file_name);
      int      ReadPNGFile        (const char *file_name);
      int      ReadRASFile        (const char *file_name);
      int      ReadRAWFile        (const char *file_name,int skip_bytes = 0,int line_order = LO_NORMAL,int cc_order = CCO_BGRBGR);
      void     SetMask            (BGRPixel &m_color);
      void     SetPlane           (GImage &s_image,int n);
      int      WriteBMPFile       (const char *file_name);
      int      WriteFile          (const char *file_name);
      int      WriteJPGFile       (const char *file_name,int quality = JPG_QUALITY);
      int      WriteJPGFile       (FileIO& file,int quality = JPG_QUALITY);
      int      WriteJPGFile       (byte *d_buffer, ulong &d_length, int quality = JPG_QUALITY);
      int      WritePCXFile       (const char *file_name);
      int      WritePGMFile       (const char *file_name,int option = PGM_BIN);
      int      WritePNGFile       (const char *file_name);
      int      WriteRASFile       (const char *file_name);
      int      WriteRAWFile       (const char *file_name,int skip_bytes = 0,int line_order = LO_NORMAL,int cc_order = CCO_BGRBGR);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: YUVPixel
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__WIN32) || defined(__linux)
#pragma pack(1)
#endif

 class YUVPixel

{
   public:
      byte Y,U,V;

   public:
      YUVPixel (   ) {   };
      YUVPixel (byte y,byte u,byte v);

   public:
      byte& operator [] (int i) const;
      void  operator () (byte y,byte u,byte v);

   public:
      void Clear (   );
};

#if defined(__WIN32) || defined(__linux)
#pragma pack()
#endif

 inline byte& YUVPixel::operator [] (int i) const

{
   return (((byte*)this)[i]);
}

 inline void YUVPixel::operator () (byte y,byte u,byte v)

{
   Y = y, U = u, V = v;
}

 inline void YUVPixel::Clear (   )

{
   Y = U = V = 0;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: YUVImage
//
///////////////////////////////////////////////////////////////////////////////

 class YUVImage : public Array2D<YUVPixel>

{
   public:
      void GetPlane (int n,GImage& d_image);
      void SetPlane (GImage& s_image,int n);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: YUVImage2
//
///////////////////////////////////////////////////////////////////////////////

 class YUVImage2 : public Array1D<GImage>

{
   public:
      int Width;
      int Height;
      int Margin;

   public:
      YUVImage2 (   );
      YUVImage2 (int width,int height,int margin = 0);
      YUVImage2 (const BGRImage& s_cimage);

   public:
      virtual ~YUVImage2 (   );

   public:
      YUVImage2& operator = (const BGRImage& s_cimage);

   public:
      virtual void Clear  (   );
      virtual void Delete (   );

   public:
      int Create        (int width,int height,int margin = 0);
      int GetLineLength (   ) const;
};

///////////////////////////////////////////////////////////////////////////////
//
// Function Templates
//
///////////////////////////////////////////////////////////////////////////////

 template <class T>
 void Convert (const Array2D<T> &s_array,GImage &d_image,int flag_scaling = FALSE)

{
  int   i,j;
  float sf;

  int sw = s_array.Width;
  int sh = s_array.Height;
  if (flag_scaling == TRUE) {
     T min1,max1;
     int min_x,min_y,max_x,max_y;
     FindMinMax (s_array,min1,max1,min_x,min_y,max_x,max_y);
     float min2 = (float)min1;
     float max2 = (float)max1;
     if (max1 == min1) sf = 0.0f;
     else sf = 255.0f / (max2 - min2);
     for (i = 0; i < sh; i++) {
        byte *_RESTRICT l_d_image = d_image[i];
        T    *_RESTRICT l_s_array = s_array[i];
        for (j = 0; j < sw; j++)
           l_d_image[j] = (byte)((l_s_array[j] - min2) * sf);
     }
  }
  else {
     for (i = 0; i < sh; i++) {
        byte *_RESTRICT l_d_image = d_image[i];
        T    *_RESTRICT l_s_array = s_array[i];
        for (j = 0; j < sw; j++) {
           if (l_s_array[j] < (T)0) l_d_image[j] = 0;
           else if (l_s_array[j] > (T)255) l_d_image[j] = 255;
           else l_d_image[j] = (byte)l_s_array[j];
         }
      }
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

void  And                    (GImage &s_image1,GImage &s_image2,GImage &d_image);
void  Convert                (const BGRImage &s_image,GImage &d_image);
void  Convert                (const GImage &s_image,BGRImage &d_image);
void  CopyPixels             (GImage &s_image,byte s_pv,GImage &d_image,byte d_pv);
// [주의] X 값과 Width 값은 짝수여야 한다.
void  ExtractAndResize_YUY2  (byte* si_buffer,ISize2D& si_size,IBox2D& e_rgn,byte* di_buffer,ISize2D& di_size);
void  GetBlendedImage        (BGRImage& s_cimage,GImage& m_image,BGRColor& color,float alpha,BGRImage& d_cimage);
void  GetEvenField           (GImage &s_image,GImage &d_image);
void  GetEvenField           (BGRImage &s_image,BGRImage &d_image);
void  GetOddField            (GImage &s_image,GImage &d_image);
void  GetOddField            (BGRImage &s_image,BGRImage &d_image);
int64 GetSumPixels           (GImage& s_image,int sx,int sy,int width,int height);
void  InterlaceEvenOddFields (byte* s_img_buf,int width,int height,byte *d_img_buf);
void  Or                     (GImage &s_image1,GImage &s_image2,GImage &d_image);
void  Reduce                 (GImage &s_image,GImage &d_image);
void  Reduce                 (BGRImage &s_image,BGRImage &d_image);
void  Resize                 (GImage &s_image,GImage &d_image);
void  Resize                 (BGRImage &s_image,BGRImage &d_image);
void  Resize_YUY2            (byte *si_buffer,int si_width,int si_height,byte *di_buffer,int di_width,int di_height);
void  Resize_YV12            (byte *si_buffer,int si_width,int si_height,byte *di_buffer,int di_width,int di_height);
void  Rotate90Degrees        (GImage &s_image,GImage &d_image);
void  Rotate90Degrees        (BGRImage &s_image,BGRImage &d_image);
void  Rotate180Degrees       (GImage &s_image,GImage &d_image);
void  Rotate180Degrees       (BGRImage &s_image,BGRImage &d_image);
void  Rotate270Degrees       (GImage &s_image,GImage &d_image);
void  Rotate270Degrees       (BGRImage &s_image,BGRImage &d_image);

}

#endif

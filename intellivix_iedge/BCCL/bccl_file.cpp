#include "bccl_console.h"
#include "bccl_file.h"
#include "bccl_misc.h"
#include "bccl_string.h"
#if defined( __WIN32 )
#include <io.h>
#else
#include <unistd.h>
#endif

namespace BCCL {

///////////////////////////////////////////////////////////////////////////////
//
// Class: FileIO
//
///////////////////////////////////////////////////////////////////////////////

FileIO::FileIO()

{
   _Init();
}

FileIO::FileIO( const char* file_name, int attribute, int mode )

{
   _Init();
   Open( file_name, attribute, mode );
}

FileIO::FileIO( byte* s_buffer, int length )

{
   _Init();
   Attach( s_buffer, length );
}

FileIO::~FileIO()

{
   Close();
}

void FileIO::_Init()

{
   Attribute     = 0;
   Mode          = 0;
   Length        = 0;
   CurPos        = 0;
   BufExpSize    = 4096;
   BufferSize    = 0;
   Buffer        = NULL;
   File          = NULL;
   Flag_Attached = FALSE;
}

void FileIO::Attach( byte* s_buffer, int length )

{
   Close();
   Buffer        = s_buffer;
   Length        = length;
   BufferSize    = length;
   Flag_Attached = TRUE;
}

void FileIO::Close()

{
   if( File != NULL ) {
      fclose( File );
      File = NULL;
   }
   if( Buffer ) {
      if( !Flag_Attached ) // jun: 외부 버퍼를 잠시 Attach 하여 사용한 경우에는 해제해서는 안됨.
         free( Buffer );
      Buffer        = NULL;
      Flag_Attached = FALSE;
   }
   CurPos     = 0;
   Length     = 0;
   BufferSize = 0;
   m_filename.clear();
}

byte* FileIO::Detach()

{
   byte* temp = Buffer;
   CurPos     = 0;
   Length     = 0;
   BufferSize = 0;
   Buffer     = NULL;
   return ( temp );
}

void FileIO::ExpandBuffer( int n_buf_size )

{
   if( n_buf_size <= BufferSize )
      return;
   int buf_size = BufferSize;
   while( buf_size < n_buf_size )
      buf_size += BufExpSize;
   byte* buffer;
   if( Buffer == NULL )
      buffer = (byte*)malloc( buf_size );
   else
      buffer = (byte*)realloc( Buffer, buf_size );
   Buffer     = buffer;
   BufferSize = n_buf_size;
}

int FileIO::GetAttribute()

{
   return ( Attribute );
}

byte* FileIO::GetBuffer( int pos )

{
   return ( &Buffer[pos] );
}

const char* FileIO::GetFilename()
{
   return m_filename.c_str();
}

int FileIO::GetLength()

{
   if( File != NULL ) {
      CurPos = Where();
      if( Seek( 0, SEEK_END ) )
         return ( -1 );
      Length = ftell( File );
      if( Seek( CurPos, SEEK_SET ) )
         return ( -2 );
   }
   return ( Length );
}

FIO_STORAGE FileIO::GetStorageType()

{
   if( File != NULL )
      return ( FIO_STORAGE_FILE );
   else
      return ( FIO_STORAGE_MEMORY );
}

int FileIO::GetMode()

{
   return ( Mode );
}

int FileIO::GoToEndPos()

{
   return ( Seek( -1, FIO_SEEK_END ) );
}

int FileIO::GoToStartPos()

{
   return ( Seek( 0, FIO_SEEK_BEGIN ) );
}

int FileIO::IsDiff( FileIO& fio )

{
   int length_fio;
   Length     = GetLength();
   length_fio = fio.GetLength();
   if( Length != length_fio )
      return ( TRUE );
   if( Length > 0 ) {
      if( File != NULL ) {
         if( fio.GetStorageType() != FIO_STORAGE_FILE )
            return ( FALSE );
         byte* buff_this = (byte*)malloc( Length );
         byte* buff_fio  = (byte*)malloc( length_fio );
         Read( buff_this, 1, Length );
         Read( buff_fio, 1, Length );
         int flag_diff = IsDiff( buff_this, buff_fio, Length );
         free( buff_this );
         free( buff_fio );
         return ( flag_diff );
      } else {
         if( fio.GetStorageType() != FIO_STORAGE_MEMORY )
            return ( FALSE );
         byte* buff_fio = fio.GetBuffer();
         return ( IsDiff( Buffer, buff_fio, Length ) );
      }
   }
   return TRUE;
}

int FileIO::IsDiff( byte* buff_a, byte* buff_b, int length )

{
   int i, n_start = 0;

   if( length >= 8 ) {
      int* _RESTRICT _buff_a = (int*)buff_a;
      int* _RESTRICT _buff_b = (int*)buff_b;
      int _length            = length / sizeof( int );
      for( i = 0; i < _length; i++ )
         if( _buff_a[i] != _buff_b[i] )
            return ( TRUE );
      n_start = _length * sizeof( int );
   }
   for( i = n_start; i < length; i++ )
      if( buff_a[i] != buff_b[i] )
         return ( TRUE );
   return ( FALSE );
}

int FileIO::Open( const char* file_name, const char* mode )

{
   Close();
   File = fopen( file_name, mode );
   if( File == NULL )
      return ( 1 );
   return ( DONE );
}

int FileIO::Open( const char* file_name, int attribute, int mode )

{
   char attr, option[4];

   Close();
   option[0] = 0;
   Attribute = attribute;
   Mode      = mode;
   switch( attribute ) {
   case FIO_BINARY:
      attr = 'b';
      break;
   case FIO_TEXT:
      attr = 't';
      break;
   default:
      attr = 'b';
      break;
   }
   switch( mode ) {
   case FIO_CREATE:
      sprintf( option, "w+%c", attr );
      break;
   case FIO_READ:
      sprintf( option, "r%c", attr );
      break;
   case FIO_WRITE:
      sprintf( option, "r+%c", attr );
      break;
   case FIO_APPEND:
      sprintf( option, "a%c", attr );
      break;
   default:
      File = NULL;
      break;
   }
   if( option[0] )
      File = fopen( file_name, option );
   if( File == NULL )
      return ( 1 );
   return ( DONE );
}

int FileIO::Print( const char* format, ... )

{
   char buffer[MAX_BUFFER_SIZE];

   va_list arg_list;
   if( File != NULL ) {
      va_start( arg_list, format );
      int r_value = vfprintf( File, format, arg_list );
      va_end( arg_list );
      if( r_value < 0 )
         return ( 1 );
      else
         return ( DONE );
   } else {
      va_start( arg_list, format );
      vsprintf( buffer, format, arg_list );
      va_end( arg_list );
      int buf_size = (int)strlen( buffer );
      return ( Write( (byte*)buffer, 1, buf_size ) );
   }
}

int FileIO::Read( byte* d_buffer, int n_items, int item_size )

{
   if( !n_items || !item_size )
      return ( DONE );
   if( File != NULL ) {
      int n_items_read = (int)fread( d_buffer, item_size, n_items, File );
      if( n_items_read == n_items )
         return ( DONE );
      else
         return ( 1 );
   } else {
      if( CurPos >= Length )
         return ( -1 );
      int n_read_bytes;
      int length = n_items * item_size;
      if( CurPos + length > Length )
         n_read_bytes = Length - CurPos;
      else
         n_read_bytes = length;
      memcpy( (byte*)d_buffer, (byte*)Buffer + CurPos, n_read_bytes );
      CurPos += n_read_bytes;
      return ( DONE );
   }
}

int FileIO::ReadByte( byte& value )

{
   if( File != NULL ) {
      int c = fgetc( File );
      if( c == EOF )
         return ( -1 );
      else
         value = (byte)c;
   } else {
      if( CurPos >= Length )
         return ( -1 );
      else
         value = Buffer[CurPos++];
   }
   return ( DONE );
}

int FileIO::ReadChar( char& value )

{
   if( File != NULL ) {
      int c = fgetc( File );
      if( c == EOF )
         return ( -1 );
      else
         value = (char)c;
   } else {
      if( CurPos >= Length )
         return ( -1 );
      else
         value = (char)Buffer[CurPos++];
   }
   return ( DONE );
}

int FileIO::ReadLine( char* d_buffer, int buf_size )

{
   int i;

   if( File != NULL ) {
      d_buffer[0] = 0;
      int ei      = buf_size - 1;
      for( i = 0; i < ei; i++ ) {
         int c = fgetc( File );
         if( c == EOF ) {
            d_buffer[i] = 0;
            return ( -1 );
         } else if( c == 0x0D )
            i--;
         else if( c == 0x0A ) {
            d_buffer[i] = 0;
            return ( DONE );
         } else
            d_buffer[i] = (char)c;
      }
      d_buffer[ei] = 0;
   } else {
      d_buffer[0] = 0;
      int ei      = buf_size - 1;
      for( i = 0; i < ei; i++ ) {
         if( CurPos >= Length ) {
            d_buffer[i] = 0;
            return ( -1 );
         }
         byte c = Buffer[CurPos++];
         if( c == 0x0D )
            i--;
         else if( c == 0x0A ) {
            d_buffer[i] = 0;
            return ( DONE );
         } else
            d_buffer[i] = c;
      }
      d_buffer[ei] = 0;
   }
   return ( 1 );
}

int FileIO::ReadString( char* d_buffer, int d_buf_size, int option )

{
   int i;

   if( File != NULL ) {
      if( fgets( d_buffer, d_buf_size, File ) == NULL ) {
         if( feof( File ) )
            return ( -1 );
         else
            return ( 1 );
      }
      if( option == FIO_REMOVE_NEWLINE ) {
         for( i = 0; d_buffer[i]; i++ ) {
            if( d_buffer[i] == '\n' ) {
               d_buffer[i] = 0;
               break;
            }
         }
      }
   } else {
      for( i = 0; i < d_buf_size; i++ ) {
         if( CurPos >= Length ) {
            d_buffer[i] = 0;
            return ( -1 );
         }
         byte c = Buffer[CurPos++];
         if( c == '\n' && option == FIO_REMOVE_NEWLINE ) {
            d_buffer[i] = 0;
            return ( DONE );
         } else if( c == ' ' ) {
            d_buffer[i] = 0;
            return ( DONE );
         } else
            d_buffer[i] = c;
      }
      d_buffer[d_buf_size - 1] = 0;
   }
   return ( DONE );
}

int FileIO::Scan( const char* format, ... )

{
   int i, n;
   int loc[20], len[20];
   char fm[20];
   va_list arg_list;

   if( GetLength() <= 0 )
      return ( -1 );
   for( i = n = 0; format[i]; i++ )
      if( format[i] == '%' )
         loc[n++] = i;
   loc[n] = i;
   for( i = 0; i < n; i++ )
      len[i] = loc[i + 1] - loc[i];
   va_start( arg_list, format );
   for( i = 0; i < n; i++ ) {
      memcpy( fm, format + loc[i], len[i] );
      fm[len[i]] = 0;
      if( File != NULL ) {
         if( fscanf( File, fm, va_arg( arg_list, void* ) ) == EOF )
            return ( -1 );
      } else {
         if( CurPos >= Length )
            return ( 1 );
         if( sscanf( (char*)( Buffer + CurPos ), fm, va_arg( arg_list, void* ) ) == EOF )
            return ( -1 );
         for( ;; ) {
            if( CurPos >= Length || !Buffer[CurPos] ) {
               if( i == n - 1 )
                  return ( DONE );
               else
                  return ( 1 );
            }
            char c = Buffer[CurPos++];
            if( c == ' ' || c == '\b' || c == '\t' || c == '\r' || c == '\n' )
               break;
         }
      }
   }
   va_end( arg_list );
   return ( DONE );
}

int FileIO::Seek( int offset, int origin )

{
   if( File != NULL ) {
      if( fseek( File, offset, origin ) )
         return ( 1 );
   } else {
      switch( origin ) {
      case FIO_SEEK_BEGIN:
         CurPos = offset;
         break;
      case FIO_SEEK_CURRENT:
         CurPos += offset;
         break;
      case FIO_SEEK_END:
         CurPos = Length + offset;
         break;
      default:
         break;
      }
      if( CurPos < 0 ) {
         CurPos = 0;
         return ( 1 );
      }
      if( CurPos > Length ) {
         CurPos = Length;
         return ( 2 );
      }
   }
   return ( DONE );
}

int FileIO::SetBuffExpSize( int exp_size )

{
   return BufExpSize = exp_size;
}

void FileIO::SetLength( int new_length )

{
   if( File != NULL ) {
      CurPos = Where();
      if( fseek( File, new_length, SEEK_SET ) )
         return;
      if( fseek( File, CurPos, SEEK_SET ) )
         return;
      Length = new_length;
   } else {
      if( new_length > BufferSize )
         ExpandBuffer( new_length );
      if( new_length < CurPos )
         CurPos = new_length;
      Length = new_length;
   }
}

int FileIO::Where()

{
   if( File != NULL )
      return ( (int)ftell( File ) );
   else
      return ( CurPos );
}

int FileIO::Write( byte* s_buffer, int n_items, int item_size )

{
   if( !n_items || !item_size )
      return ( DONE );
   if( File != NULL ) {
      int n_items_written = (int)fwrite( s_buffer, item_size, n_items, File );
      if( n_items_written != n_items )
         return ( 1 );
   } else {
      int data_len = n_items * item_size;
      if( BufExpSize > 0 ) {
         if( CurPos + data_len > BufferSize )
            ExpandBuffer( CurPos + data_len );
      } else if( BufExpSize == 0 ) {
         if( CurPos + data_len > Length )
            data_len = Length - CurPos;
      }
      memcpy( (byte*)Buffer + CurPos, (byte*)s_buffer, data_len );
      CurPos += data_len;
      if( CurPos > Length )
         Length = CurPos;
   }
   return ( DONE );
}

int FileIO::WriteByte( byte value )

{
   if( File != NULL ) {
      if( fputc( (int)value, File ) == EOF )
         return ( 1 );
      else
         return ( DONE );
   } else
      return ( Write( &value, 1, sizeof( byte ) ) );
}

int FileIO::WriteChar( char value )

{
   if( File != NULL ) {
      if( fputc( (int)value, File ) == EOF )
         return ( 1 );
      else
         return ( DONE );
   } else
      return ( Write( (byte*)&value, 1, sizeof( char ) ) );
}

int FileIO::WriteString( const char* s_buffer )

{
   if( File != NULL ) {
      if( fputs( s_buffer, File ) == EOF )
         return ( 1 );
      else
         return ( DONE );
   } else {
      int s_buf_size = (int)strlen( s_buffer );
      return Write( (byte*)s_buffer, s_buf_size, sizeof( char ) );
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

void ChangeFileExtension( const char* s_file_name, const char* new_ext, char* d_file_name )

{
   StringA file_name = s_file_name;
   char* end         = strrchr( (char*)file_name, '.' ); // 마지막으로 나오는 '.'를 찾는다.
   if( end != NULL )
      *end = 0;
   sprintf( d_file_name, "%s.%s", (char*)file_name, new_ext );
}

int GetFileLength( const char* file_name )

{
   FileIO file( file_name, FIO_BINARY, FIO_READ );
   if( !file )
      return ( -1 );
   file.Seek( 0, FIO_SEEK_END );
   return ( file.Where() );
}

int IdentifyImageFile( const char* file_name )

{
   byte signature[4];

   FileIO file( file_name, FIO_BINARY, FIO_READ );
   if( !file )
      return ( -1 );
   if( file.Read( signature, 4, sizeof( byte ) ) )
      return ( -2 );
   int ift = IFT_NONE;
   if( signature[0] == 'B' && signature[1] == 'M' )
      ift = IFT_BMP;
   else if( signature[0] == 0xFF && signature[1] == 0xD8 && signature[2] == 0xFF )
      ift = IFT_JPG;
   else if( signature[0] == 0x0A )
      ift = IFT_PCX;
   else if( signature[0] == 'P' && ( signature[1] == '2' || signature[1] == '5' ) )
      ift = IFT_PGM;
   else if( signature[0] == 0x89 && signature[1] == 0x50 && signature[2] == 0x4E && signature[3] == 0x47 )
      ift = IFT_PNG;
   else if( signature[0] == 0x59 && signature[1] == 0xA6 && signature[2] == 0x6A && signature[3] == 0x95 )
      ift = IFT_RAS;
   else if( ( signature[0] == 'I' && signature[1] == 'I' ) || ( signature[0] == 'M' && signature[1] == 'M' ) )
      ift = IFT_TIF;
   return ( ift );
}

int LoadFileToBuffer( const char* file_name, BArray1D& d_buffer, int flag_add_null )

{
   d_buffer.Delete();
   int length = GetFileLength( file_name );
   if( length <= 0 )
      return ( 1 );
   if( flag_add_null )
      d_buffer.Create( length + 1 );
   else
      d_buffer.Create( length );
   FileIO file( file_name, FIO_BINARY, FIO_READ );
   if( !file )
      return ( 2 );
   if( file.Read( d_buffer, length, 1 ) )
      return ( 3 );
   if( flag_add_null )
      d_buffer[length] = 0;
   return ( DONE );
}

int SaveBufferToFile( const char* file_name, void* s_buffer, int s_buf_size )

{
   FileIO file( file_name, FIO_BINARY, FIO_CREATE );
   if( !file )
      return ( 2 );
   if( file.Write( (byte*)s_buffer, s_buf_size, 1 ) )
      return ( 3 );
   return ( DONE );
}

int AccessFile( const char* file_name, int option )
#if defined( __WIN32 )
{
   return ( _access( file_name, option ) );
}
#else
{
   return ( access( file_name, option ) );
}
#endif

void ExtractDirName( const char* s_path_name, char* d_dir_name )

{
   int len = strlen( s_path_name ) + 1;
   StringA d_drive( len );
   StringA d_dir( len );
   SplitPathName( s_path_name, (char*)d_drive, (char*)d_dir, NULL, NULL );
   sprintf( d_dir_name, "%s%s", (char*)d_drive, (char*)d_dir );
}

void ExtractFileName( const char* s_path_name, char* d_file_name, int flag_add_ext )

{
   int len = strlen( s_path_name ) + 1;
   StringA d_ext( len );
   StringA d_fname( len );
   SplitPathName( s_path_name, NULL, NULL, (char*)d_fname, (char*)d_ext );
   if( flag_add_ext )
      sprintf( d_file_name, "%s%s", (char*)d_fname, (char*)d_ext );
   else
      sprintf( d_file_name, "%s", (char*)d_fname );
}

int FindFiles( const char* mask, CArray2D& fns, int fn_length )
// fns: Created inside the function.
#if defined( __WIN32 )
{
   int i, n_fns;
   HANDLE h_file;
   WIN32_FIND_DATAA wfd;

   n_fns  = 0;
   h_file = FindFirstFileA( mask, &wfd );
   if( h_file == INVALID_HANDLE_VALUE )
      return ( 1 );
   else
      n_fns++;
   for( ; FindNextFileA( h_file, &wfd ); n_fns++ )
      ;
   FindClose( h_file );
   fns.Create( fn_length, n_fns );
   h_file = FindFirstFileA( mask, &wfd );
   strcpy( fns[0], wfd.cFileName );
   for( i = 1; i < n_fns; i++ ) {
      FindNextFileA( h_file, &wfd );
      strcpy( fns[i], wfd.cFileName );
   }
   FindClose( h_file );
   qsort( (char*)fns, n_fns, fn_length, Compare_String_Ascending );
   return ( DONE );
}
#else
{
   LOGF << "Not implemented yet.";
   fns.Delete();
   return ( -1 );
}
#endif

void SplitPathName( const char* s_path, char* d_drive, char* d_dir, char* d_fname, char* d_ext )
#if defined( __WIN32 )
{
   _splitpath( s_path, d_drive, d_dir, d_fname, d_ext );
}
#else
{
   int n1, n2;

   int p1 = -1, p2 = -1;
   for( n1 = 0; s_path[n1]; n1++ ) {
      char c = s_path[n1];
      if( c == '/' || c == '\\' )
         p1 = n1;
      else if( c == '.' )
         p2 = n1;
   }
   int p3 = p2 + 1;
   if( s_path[p3] == '/' || s_path[p3] == '\\' )
      p2 = -1;
   if( d_drive != NULL )
      d_drive[0] = 0;
   p1++;
   if( d_dir != NULL ) {
      if( p1 > 0 )
         strncpy( d_dir, s_path, p1 );
      d_dir[p1] = 0;
   }
   if( d_fname != NULL ) {
      if( p2 == -1 )
         n2 = n1 - p1;
      else
         n2 = p2 - p1;
      strncpy( d_fname, s_path + p1, n2 );
      d_fname[n2] = 0;
   }
   if( d_ext != NULL ) {
      if( p2 == -1 )
         d_ext[0] = 0;
      else {
         n2 = n1 - p2;
         strncpy( d_ext, s_path + p2, n2 );
         d_ext[n2] = 0;
      }
   }
}
#endif

} // namespace BCCL

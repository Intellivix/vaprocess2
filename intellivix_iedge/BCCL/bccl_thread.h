#if !defined(__BCCL_THREAD_H)
#define __BCCL_THREAD_H

#include "bccl_array.h"

#if defined(__OS_WINDOWS_T)

#elif defined(__LIB_PTHREAD)
#include "pthread.h"
#endif

 namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: CriticalSection
//
///////////////////////////////////////////////////////////////////////////////

 class CriticalSection

{
   protected:
      #if defined(__LIB_PTHREAD)
      pthread_mutex_t Mutex;
      #elif defined(__OS_WINDOWS)
      CCriticalSection CS;
      #endif

   public:
      CriticalSection (   );
      virtual ~CriticalSection (   );

   public:
      int Lock   (   );
      int Unlock (   );
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: ScopedLock
//
///////////////////////////////////////////////////////////////////////////////

 class ScopedLock

{
   protected:
      int Flag_Locked;
      CriticalSection* CS;
   
   public:
      ScopedLock (CriticalSection& cs);
      virtual ~ScopedLock (   );

   public:
      int Lock   (   );
      int Unlock (   );
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: SingleEvent
//
///////////////////////////////////////////////////////////////////////////////

#define TIME_INFINITE   0x0FFFFFFF

 class SingleEvent

{
   protected:
      #if defined(__LIB_PTHREAD)
      int EventFlag;
      pthread_mutex_t Mutex;
      pthread_cond_t  Cond;
      #elif defined(__OS_WINDOWS)
      CEvent Event;
      #endif

   public:
      SingleEvent (   );
      virtual ~SingleEvent (   );

   public:
      int ResetEvent (   );
      int SetEvent   (   );

   public:
      friend int WaitForSingleEvent (SingleEvent& event,uint timeout = TIME_INFINITE);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: MultiEvents
//
///////////////////////////////////////////////////////////////////////////////

 class MultiEvents

{
   protected:
      #if defined(__LIB_PTHREAD)
      BArray1D        EventFlags;
      pthread_mutex_t Mutex;
      pthread_cond_t  Cond;
      #elif defined(__OS_WINDOWS)
      Array1D<CEvent> Events;
      #endif

   public:
      MultiEvents (   );
      virtual ~MultiEvents (   );
   
   public:
      int Create         (int n_events);
      int GetNumEvents   (   );
      int ResetAllEvents (   );
      int ResetEvent     (int evt_idx);
      int SetEvent       (int evt_idx);

   public:
      friend int WaitForMultiEvents (MultiEvents& events,int flag_wait_all,int& evt_idx,uint timeout = TIME_INFINITE);
};

 inline int MultiEvents::GetNumEvents (   )
#if defined(__LIB_PTHREAD)
{
   return (EventFlags.Length);
}
#elif defined(__OS_WINDOWS)
{
   return (Events.Length);
}
#else
{
   return (0);
}
#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: Thread
//
///////////////////////////////////////////////////////////////////////////////

class Thread;
typedef void(*CBF_ThreadProc)(Thread* thread,void* param);

 class Thread

{
   protected:
      int Flag_ThreadRunning;
      #if defined(__LIB_PTHREAD)
      pthread_t* ThreadHnd;
      #elif defined(__OS_WINDOWS)
      HANDLE ThreadHnd;
      #endif

   protected:
      void* CBParam_ThreadProc;
      CBF_ThreadProc CBFunc_ThreadProc;

   public:
      SingleEvent Event_Stop;

   public:
      Thread (   );
      virtual ~Thread (   );

   protected:
      #if defined(__LIB_PTHREAD)
      static void* _ThreadProc (void* param);
      #elif defined(__OS_WINDOWS)
      static uint  _ThreadProc (void* param);
      #endif
      void ExecuteThreadProc (   );

   protected:
      virtual void ThreadProc (   );

   public:
      int  CheckStopEvent         (uint timeout = 0);
      int  IsThreadRunning        (   );
      void SetCallback_ThreadProc (CBF_ThreadProc cb_func,void* cb_param);
      int  StartThread            (   );
      void StopThread             (   );
};

 inline int Thread::IsThreadRunning (   )

{
   return (Flag_ThreadRunning);
}

}

#endif

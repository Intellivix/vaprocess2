﻿#include "bccl_time.h"

#ifdef __linux
#include <sys/time.h>
#endif // __linux

namespace BCCL {

///////////////////////////////////////////////////////////////////////////////
//
// Class: Time
//
///////////////////////////////////////////////////////////////////////////////

Time::Time()
{
   Clear();
}

void Time::Clear()
{
   Year = Month = Day = Weekday = Hour = Minute = Second = Millisecond = 0;
}

int Time::GetLocalTime()
#if defined( __WIN32 )
{
   SYSTEMTIME l_time;
   ::GetLocalTime( &l_time );
   Year        = (int)l_time.wYear;
   Month       = (int)l_time.wMonth;
   Day         = (int)l_time.wDay;
   Weekday     = (int)l_time.wDayOfWeek;
   Hour        = (int)l_time.wHour;
   Minute      = (int)l_time.wMinute;
   Second      = (int)l_time.wSecond;
   Millisecond = (int)l_time.wMilliseconds;
   return ( DONE );
}
#else
{
   struct timeval tv;
   gettimeofday( &tv, nullptr );
   FromTimeval( &tv );
   return ( 0 );
}
#endif

void Time::FromTimeval(::timeval* tv )
{
   gettimeofday( tv, nullptr );

   tm c_time;
   localtime_r( &tv->tv_sec, &c_time );
   Year        = c_time.tm_year + 1900;
   Month       = c_time.tm_mon + 1;
   Day         = c_time.tm_mday;
   Weekday     = c_time.tm_wday;
   Hour        = c_time.tm_hour;
   Minute      = c_time.tm_min;
   Second      = c_time.tm_sec;
   Millisecond = static_cast<int>( tv->tv_usec / 1000 );
}

int Time::GetTimeInSecond()
{
   int second = 86400 * Day + 3600 * Hour + 60 * Minute + Second;
   return ( second );
}

void Time::GetTimeInString( char* d_string )
{
   sprintf( d_string, "%04d:%02d:%02d:%02d:%02d:%02d:%03d", Year, Month, Day, Hour, Minute, Second, Millisecond );
}

void Time::GetTimeInString_HMS( char* d_string )
{
   sprintf( d_string, "%02d:%02d:%02d", Hour, Minute, Second );
}

void Time::GetTimeInString_YMD( char* d_string )
{
   sprintf( d_string, "%04d:%02d:%02d", Year, Month, Day );
}

void Time::SetTimeInSecond( int second )
{
   Year        = 0;
   Month       = 0;
   Weekday     = 0;
   Millisecond = 0;
   Day         = second / 86400;
   second -= Day * 86400;
   Hour = second / 3600;
   second -= Hour * 3600;
   Minute = second / 60;
   second -= Minute * 60;
   Second = second;
}

int Time::SetTimeInString( const char* s_string )
{
   char* p1 = const_cast<char*>( s_string );
   // Year
   char* p2 = (char*)strstr( p1, ":" );
   if( p2 == NULL ) return ( 2 );
   *p2  = 0;
   Year = atoi( p1 );
   p1   = p2 + 1;
   // Month
   p2 = (char*)strstr( p1, ":" );
   if( p2 == NULL ) return ( 3 );
   *p2   = 0;
   Month = atoi( p1 );
   p1    = p2 + 1;
   // Day
   p2 = (char*)strstr( p1, ":" );
   if( p2 == NULL ) return ( 4 );
   *p2 = 0;
   Day = atoi( p1 );
   p1  = p2 + 1;
   // Weekday
   Weekday = 0;
   // Hour
   p2 = (char*)strstr( p1, ":" );
   if( p2 == NULL ) return ( 5 );
   *p2  = 0;
   Hour = atoi( p1 );
   p1   = p2 + 1;
   // Minute
   p2 = (char*)strstr( p1, ":" );
   if( p2 == NULL ) return ( 6 );
   *p2    = 0;
   Minute = atoi( p1 );
   p1     = p2 + 1;
   // Second
   Second = atoi( p1 );
   return ( DONE );
}

int Time::SetTimeInString_HMS( const char* s_string )
{
   char* p1 = const_cast<char*>( s_string );
   // Hour
   char* p2 = (char*)strstr( p1, ":" );
   if( p2 == NULL ) return ( 1 );
   *p2  = 0;
   Hour = atoi( p1 );
   p1   = p2 + 1;
   // Minute
   p2 = (char*)strstr( p1, ":" );
   if( p2 == NULL ) return ( 2 );
   *p2    = 0;
   Minute = atoi( p1 );
   p1     = p2 + 1;
   // Second
   Second = atoi( p1 );
   return ( DONE );
}

int Time::SetTimeInString_YMD( const char* s_string )
{
   char* p1 = const_cast<char*>( s_string );
   // Year
   char* p2 = (char*)strstr( p1, ":" );
   if( p2 == NULL ) return ( 1 );
   *p2  = 0;
   Year = atoi( p1 );
   p1   = p2 + 1;
   // Month
   p2 = (char*)strstr( p1, ":" );
   if( p2 == NULL ) return ( 2 );
   *p2   = 0;
   Month = atoi( p1 );
   p1    = p2 + 1;
   // Day
   Day = atoi( p1 );
   return ( DONE );
}

   ///////////////////////////////////////////////////////////////////////////////
   // Functions
   ///////////////////////////////////////////////////////////////////////////////

#define WINDOWS_TICK_SEC 10000000
#define WINDOWS_TICK_MS 10000
#define EPOCH_DIFFERENCE_SECOND 11644473600LL

void Convert( Time& s_time, FILETIME& d_time )
#if defined( __WIN32 )
{
   SYSTEMTIME t_time;
   t_time.wYear         = (WORD)s_time.Year;
   t_time.wMonth        = (WORD)s_time.Month;
   t_time.wDay          = (WORD)s_time.Day;
   t_time.wDayOfWeek    = (WORD)s_time.Weekday;
   t_time.wHour         = (WORD)s_time.Hour;
   t_time.wMinute       = (WORD)s_time.Minute;
   t_time.wSecond       = (WORD)s_time.Second;
   t_time.wMilliseconds = (WORD)s_time.Millisecond;
   ::SystemTimeToFileTime( &t_time, &d_time );
}
#else
{
   // yhpark. bugfix. converting SYSTEMTIME to TIME;
   struct tm unix_tm;
   unix_tm.tm_year = s_time.Year - 1900;
   unix_tm.tm_mon  = s_time.Month - 1;
   unix_tm.tm_mday = s_time.Day;
   unix_tm.tm_hour = s_time.Hour;
   unix_tm.tm_min  = s_time.Minute;
   unix_tm.tm_sec  = s_time.Second;

   time_t unix_time_t    = mktime( &unix_tm ) + EPOCH_DIFFERENCE_SECOND;
   uint64_t win_filetime = static_cast<uint64_t>( unix_time_t ) * WINDOWS_TICK_SEC;
   win_filetime += static_cast<uint64_t>( s_time.Millisecond * WINDOWS_TICK_MS ); // Millisecond

   d_time.dwHighDateTime = static_cast<DWORD>( win_filetime >> 32 );
   d_time.dwLowDateTime  = static_cast<DWORD>( win_filetime & 0xFFFFFFFF );
}
#endif

void Convert( FILETIME& s_time, Time& d_time )
#if defined( __WIN32 )
{
   SYSTEMTIME t_time;
   ::FileTimeToSystemTime( &s_time, &t_time );
   d_time.Year        = t_time.wYear;
   d_time.Month       = t_time.wMonth;
   d_time.Day         = t_time.wDay;
   d_time.Weekday     = t_time.wDayOfWeek;
   d_time.Hour        = t_time.wHour;
   d_time.Minute      = t_time.wMinute;
   d_time.Second      = t_time.wSecond;
   d_time.Millisecond = t_time.wMilliseconds;
}
#else
{
   // yhpark. bugfix. converting TIME to SYSTEMTIME;
   uint64_t win_filetime = ( static_cast<uint64_t>( s_time.dwHighDateTime ) << 32 ) | static_cast<uint64_t>( s_time.dwLowDateTime );
   time_t unix_time_t    = static_cast<time_t>( static_cast<uint64_t>( win_filetime / WINDOWS_TICK_SEC ) ) - EPOCH_DIFFERENCE_SECOND;

   struct tm unix_tm;
   localtime_r( &unix_time_t, &unix_tm );
   d_time.Year    = unix_tm.tm_year + 1900;
   d_time.Month   = unix_tm.tm_mon + 1;
   d_time.Day     = unix_tm.tm_mday;
   d_time.Hour    = unix_tm.tm_hour;
   d_time.Minute  = unix_tm.tm_min;
   d_time.Second  = unix_tm.tm_sec;
   d_time.Weekday = unix_tm.tm_wday;

   // Millisecond
   d_time.Millisecond = static_cast<int>( win_filetime % WINDOWS_TICK_SEC / WINDOWS_TICK_MS );
}
#endif

void Convert( SYSTEMTIME& s_time, FILETIME& d_time )
#if defined( __WIN32 )
{
   SystemTimeToFileTime( &s_time, &d_time );
}
#else
{
   // yhpark. bugfix. converting SYSTEMTIME to FILETIME;
   struct tm unix_tm;
   unix_tm.tm_year = s_time.wYear - 1900;
   unix_tm.tm_mon  = s_time.wMonth - 1;
   unix_tm.tm_mday = s_time.wDay;
   unix_tm.tm_hour = s_time.wHour;
   unix_tm.tm_min  = s_time.wMinute;
   unix_tm.tm_sec  = s_time.wSecond;

   time_t unix_time_t  = mktime( &unix_tm );
   uint64 win_filetime = static_cast<uint64>( unix_time_t * WINDOWS_TICK_SEC ) + EPOCH_DIFFERENCE_SECOND;
   win_filetime += s_time.wMilliseconds * WINDOWS_TICK_MS; // Millisecond

   d_time.dwHighDateTime = static_cast<DWORD>( win_filetime >> 32 );
   d_time.dwLowDateTime  = static_cast<DWORD>( win_filetime & 0xFFFFFFFF );
}
#endif

void Convert( FILETIME& s_time, SYSTEMTIME& d_time )
#if defined( __WIN32 )
{
   FileTimeToSystemTime( &s_time, &d_time );
}
#else
{
   // yhpark. bugfix. converting FILETIME to SYSTEMTIME;
   uint64_t win_filetime = ( static_cast<uint64_t>( s_time.dwHighDateTime ) << 32 ) | static_cast<uint64_t>( s_time.dwLowDateTime );
   time_t unix_time_t    = static_cast<time_t>( win_filetime / WINDOWS_TICK_SEC ) - EPOCH_DIFFERENCE_SECOND;

   struct tm unix_tm;
   localtime_r( &unix_time_t, &unix_tm );
   d_time.wYear      = static_cast<WORD>( unix_tm.tm_year + 1900 );
   d_time.wMonth     = static_cast<WORD>( unix_tm.tm_mon + 1 );
   d_time.wDay       = static_cast<WORD>( unix_tm.tm_mday );
   d_time.wHour      = static_cast<WORD>( unix_tm.tm_hour );
   d_time.wMinute    = static_cast<WORD>( unix_tm.tm_min );
   d_time.wSecond    = static_cast<WORD>( unix_tm.tm_sec );
   d_time.wDayOfWeek = static_cast<WORD>( unix_tm.tm_wday );

   // Millisecond
   d_time.wMilliseconds = static_cast<WORD>( ( win_filetime % WINDOWS_TICK_SEC ) / WINDOWS_TICK_MS );
}
#endif

uint32_t GetTimerValue()
#if defined( __WIN32 )
{
   return ( GetTickCount() );
}
#else
{
   return GetTickCount();
}
#endif
} // namespace BCCL

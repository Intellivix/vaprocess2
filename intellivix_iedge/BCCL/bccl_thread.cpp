#include "bccl_console.h"

#include "bccl_thread.h"

#if defined(__OS_WINDOWS) && defined(__LIB_PTHREAD)
#pragma comment(lib,"pthreadVC2.lib")
#endif

 namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: CriticalSection
//
///////////////////////////////////////////////////////////////////////////////

 CriticalSection::CriticalSection (   )

{
   #if defined(__LIB_PTHREAD)
   Mutex = NULL;
   pthread_mutexattr_t attr;
   pthread_mutexattr_init (&attr);
   pthread_mutexattr_settype (&attr,PTHREAD_MUTEX_RECURSIVE);
   if (pthread_mutex_init (&Mutex,&attr)) Mutex = NULL;
   pthread_mutexattr_destroy (&attr);
   #endif
}

 CriticalSection::~CriticalSection (   )

{
   #if defined(__LIB_PTHREAD)
   if (Mutex != NULL) pthread_mutex_destroy (&Mutex);
   #endif
}

 int CriticalSection::Lock (   )
#if defined(__LIB_PTHREAD)
{
   if (Mutex == NULL) return (1);
   if (pthread_mutex_lock (&Mutex)) return (2);
   return (DONE);
}
#elif defined(__OS_WINDOWS)
{
   if (CS.Lock (   )) return (DONE);
   return (1);
}
#else
{
   return (-1);
}
#endif

 int CriticalSection::Unlock (   )
#if defined(__LIB_PTHREAD)
{
   if (Mutex == NULL) return (1);
   if (pthread_mutex_unlock (&Mutex)) return (2);
   return (DONE);
}
#elif defined(__OS_WINDOWS)
{
   if (CS.Unlock (   )) return (DONE);
   return (1);
}
#else
{
   return (-1);
}
#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: ScopedLock
//
///////////////////////////////////////////////////////////////////////////////

 ScopedLock::ScopedLock (CriticalSection& cs)

{
   CS = &cs;
   CS->Lock (   );
   Flag_Locked = TRUE;
}

 ScopedLock::~ScopedLock (   )

{
   if (Flag_Locked) CS->Unlock (   );
}

 int ScopedLock::Lock (   )

{
   if (Flag_Locked) return (DONE);
   int e_code = CS->Lock (   );
   if (e_code) return (e_code);
   Flag_Locked = TRUE;
   return (DONE);
}

 int ScopedLock::Unlock (   )

{
   if (!Flag_Locked) return (DONE);
   int e_code = CS->Unlock (   );
   if (e_code) return (e_code);
   Flag_Locked = FALSE;
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: SingleEvent
//
///////////////////////////////////////////////////////////////////////////////

 SingleEvent::SingleEvent (   )

{
   #if defined(__LIB_PTHREAD)
   EventFlag = FALSE;
   Mutex = NULL;
   pthread_mutexattr_t attr;
   pthread_mutexattr_init (&attr);
   pthread_mutexattr_settype (&attr,PTHREAD_MUTEX_RECURSIVE);
   if (pthread_mutex_init (&Mutex,&attr)) Mutex = NULL;
   pthread_mutexattr_destroy (&attr);
   Cond = NULL;
   if (pthread_cond_init (&Cond,NULL )) Cond = NULL;
   #endif
}

 SingleEvent::~SingleEvent (   )

{
   #if defined(__LIB_PTHREAD)
   if (Mutex != NULL) pthread_mutex_destroy (&Mutex);
   if (Cond  != NULL) pthread_cond_destroy  (&Cond);
   #endif
}

 int SingleEvent::ResetEvent (   )
#if defined(__LIB_PTHREAD)
{
   if (Mutex == NULL || Cond == NULL) return (1);
   pthread_mutex_lock (&Mutex);
   EventFlag = FALSE;
   pthread_mutex_unlock (&Mutex);
   return (DONE);
}
#elif defined(__OS_WINDOWS)
{
   if (Event.ResetEvent (   )) return (DONE);
}
#else
{
   return (-1);
}
#endif

 int SingleEvent::SetEvent (   )
#if defined(__LIB_PTHREAD)
{
   if (Mutex == NULL || Cond == NULL) return (1);
   pthread_mutex_lock (&Mutex);
   EventFlag = TRUE;
   pthread_cond_signal  (&Cond);
   pthread_mutex_unlock (&Mutex);
   return (DONE);
}
#elif defined(__OS_WINDOWS)
{
   if (Event.SetEvent (   )) return (DONE);
   return (1);
}
#else
{
   return (-1);
}
#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: MultiEvents
//
///////////////////////////////////////////////////////////////////////////////

 MultiEvents::MultiEvents (   )

{
   #if defined(__LIB_PTHREAD)
   Mutex = NULL;
   pthread_mutexattr_t attr;
   pthread_mutexattr_init (&attr);
   pthread_mutexattr_settype (&attr,PTHREAD_MUTEX_RECURSIVE);
   if (pthread_mutex_init (&Mutex,&attr)) Mutex = NULL;
   pthread_mutexattr_destroy (&attr);
   Cond = NULL;
   if (pthread_cond_init (&Cond,NULL )) Cond = NULL;
   #endif
}

 MultiEvents::~MultiEvents (   )

{
   #if defined(__LIB_PTHREAD)
   if (Mutex != NULL) pthread_mutex_destroy (&Mutex);
   if (Cond  != NULL) pthread_cond_destroy  (&Cond);
   #endif
}

 int MultiEvents::Create (int n_events)
#if defined(__LIB_PTHREAD)
{
   EventFlags.Create (n_events);
   EventFlags.Clear  (   );
   return (DONE);
}
#elif defined(__OS_WINDOWS)
{
   Events.Create (n_events);
   return (DONE);
}
#else
{
   return (-1);
}
#endif

 int MultiEvents::ResetAllEvents (   )
#if defined(__LIB_PTHREAD)
{
   if (Mutex == NULL || Cond == NULL) return (1);
   if (!EventFlags) return (2);
   pthread_mutex_lock (&Mutex);
   EventFlags.Clear (   );
   pthread_mutex_unlock (&Mutex);
   return (DONE);
}
#elif defined(__OS_WINDOWS)
{
   if (!Events) return (1);
   for (int i = 0; i < Events.Length; i++) Events[i].ResetEvent (   );
   return (DONE);
}
#else
{
   return (-1);
}
#endif

 int MultiEvents::ResetEvent (int evt_idx)
#if defined(__LIB_PTHREAD)
{
   if (Mutex == NULL || Cond == NULL) return (1);
   if (!EventFlags) return (2);
   if (evt_idx >= EventFlags.Length) return (3);
   pthread_mutex_lock (&Mutex);
   EventFlags[evt_idx] = FALSE;
   pthread_mutex_unlock (&Mutex);
   return (DONE);
}
#elif defined(__OS_WINDOWS)
{
   if (!Events) return (1);
   if (evt_idx >= Events.Length) return (2);
   Events[evt_idx].ResetEvent (   );
   return (DONE);
}
#else
{
   return (-1);
}
#endif

 int MultiEvents::SetEvent (int evt_idx)
#if defined(__LIB_PTHREAD)
{
   if (Mutex == NULL || Cond == NULL) return (1);
   if (!EventFlags) return (2);
   if (evt_idx >= EventFlags.Length) return (3);
   pthread_mutex_lock (&Mutex);
   EventFlags[evt_idx] = TRUE;
   pthread_cond_signal  (&Cond);
   pthread_mutex_unlock (&Mutex);
   return (DONE);
}
#elif defined(__OS_WINDOWS)
{
   if (!Events) return (1);
   if (evt_idx >= Events.Length) return (2);
   Events[evt_idx].SetEvent (   );
}
#else
{
   return (-1);
}
#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: Thread
//
///////////////////////////////////////////////////////////////////////////////

 Thread::Thread (   )

{
   Flag_ThreadRunning = FALSE;
   #if defined(__LIB_PTHREAD) || defined(__OS_WINDOWS)
   ThreadHnd = NULL;
   #endif
   CBParam_ThreadProc = NULL;
   CBFunc_ThreadProc  = NULL;
}

 Thread::~Thread (   )

{
   StopThread (   );
}

 int Thread::CheckStopEvent (uint timeout)

{
   if (WaitForSingleEvent (Event_Stop,timeout) == DONE) return (TRUE);
   else return (FALSE);
}

 void Thread::ExecuteThreadProc (   )

{
   Event_Stop.ResetEvent (   );
   Flag_ThreadRunning = TRUE;
   ThreadProc (   );
   Flag_ThreadRunning = FALSE;
}

#if defined(__LIB_PTHREAD)
 void* Thread::_ThreadProc (void* param)

{
   Thread* thread = (Thread*)param;
   thread->ExecuteThreadProc (   );
   return (NULL);
}
#elif defined(__OS_WINDOWS)
 uint Thread::_ThreadProc (void* param)
 
{
   Thread* thread = (Thread*)param;
   thread->ExecuteThreadProc (   );
   return (DONE);
}
#endif

 void Thread::ThreadProc (   )

{
   if (CBFunc_ThreadProc != NULL) CBFunc_ThreadProc (this,CBParam_ThreadProc);
}

 void Thread::SetCallback_ThreadProc (CBF_ThreadProc cb_func,void* cb_param)

{
   CBFunc_ThreadProc  = cb_func;
   CBParam_ThreadProc = cb_param;
}

 int Thread::StartThread (   )
#if defined(__LIB_PTHREAD)
{
   if (ThreadHnd) return (1);
   ThreadHnd = new pthread_t;
   if (pthread_create (ThreadHnd,NULL,_ThreadProc,this) < 0) {
      delete ThreadHnd;
      ThreadHnd = NULL;
      return (2);
   }
   return (DONE);
}
#elif defined(__OS_WINDOWS)
{
   if (ThreadHnd) return (1);
   CWinThread* thread = AfxBeginThread (_ThreadProc,this,THREAD_PRIORITY_NORMAL,0,CREATE_SUSPENDED);
   ThreadHnd = thread->m_hThread;
   thread->ResumeThread (   );
   return (DONE);
}
#else
{
   return (-1);
}
#endif

 void Thread::StopThread (   )
#if defined(__LIB_PTHREAD)
{
   if (!ThreadHnd || !IsThreadRunning (   )) return;
   Event_Stop.SetEvent (   );
   void* ret;
   pthread_join (*ThreadHnd,&ret);
   delete ThreadHnd;
   ThreadHnd = NULL;
}
#elif defined(__OS_WINDOWS)
{
   if (!ThreadHnd || !IsThreadRunning (   )) return;
   Event_Stop.SetEvent (   );
   WaitForSingleObject (ThreadHnd,INFINITE);
   ThreadHnd = NULL;
}
#else
{
   return ;
}
#endif

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 int WaitForSingleEvent (SingleEvent& event,uint timeout)
#if defined(__LIB_PTHREAD)
{
   if (event.Mutex == NULL || event.Cond == NULL) return (1);
   uint timeout_sec  = timeout / 1000;
   uint timeout_nsec = 1000000 * (timeout - timeout / 1000 * 1000);
   pthread_mutex_lock (&event.Mutex);
   int r_code = DONE;
   while (!event.EventFlag) {
      timespec c_time;
      timespec_get (&c_time,TIME_UTC);
      timespec t_time;
      t_time.tv_sec  = c_time.tv_sec  + (time_t)timeout_sec;
      t_time.tv_nsec = c_time.tv_nsec + (long)timeout_nsec;
      if (t_time.tv_nsec > 1000000000L) {
         t_time.tv_sec++;
         t_time.tv_nsec -= 1000000000L;
      }
      if (pthread_cond_timedwait (&event.Cond,&event.Mutex,&t_time) == ETIMEDOUT) {
         r_code = 2;
         break;
      }
   }
   event.EventFlag = FALSE;
   pthread_mutex_unlock (&event.Mutex);
   return (r_code);
}
#elif defined(__OS_WINDOWS)
{
   DWORD r_code = WaitForSingleObject (event,timeout);
   if (r_code == WAIT_OBJECT_0) return (DONE);
   if (r_code == WAIT_TIMEOUT ) return (2);
   return (1);
}
#else
{
   return (-1);
}
#endif

 int WaitForMultiEvents (MultiEvents& events,int flag_wait_all,int& evt_idx,uint timeout)
#if defined(__LIB_PTHREAD)
{
   int i;
   
   evt_idx = -1;
   if (events.Mutex == NULL || events.Cond == NULL) return (1);
   int n_events = events.GetNumEvents (   );
   if (!n_events) return (2);
   uint timeout_sec  = timeout / 1000;
   uint timeout_nsec = 1000000 * (timeout - timeout / 1000 * 1000);
   pthread_mutex_lock (&events.Mutex);
   int r_code = DONE;
   while (TRUE) {
      int count = 0;
      for (i = 0; i < n_events; i++) {
         if (events.EventFlags[i]) {
            if (evt_idx < 0) evt_idx = i;
            count++;
         }
      }
      if (flag_wait_all) {
         if (count == n_events) break;
      }
      else {
         if (count > 0) break;
      }
      timespec c_time;
      timespec_get (&c_time,TIME_UTC);
      timespec t_time;
      t_time.tv_sec  = c_time.tv_sec  + (time_t)timeout_sec;
      t_time.tv_nsec = c_time.tv_nsec + (long)timeout_nsec;
      if (t_time.tv_nsec > 1000000000L) {
         t_time.tv_sec++;
         t_time.tv_nsec -= 1000000000L;
      }
      if (pthread_cond_timedwait (&events.Cond,&events.Mutex,&t_time) == ETIMEDOUT) {
         r_code = 3;
         break;
      }
   }
   events.EventFlags.Clear (   );
   pthread_mutex_unlock (&events.Mutex);
   return (r_code);
}
#elif defined(__OS_WINDOWS)
{
   int i;

   int n_events = event.GetNumEvents (   );
   if (!n_events) return (1);
   Array1D<HANDLE> events(n_events);
   for (i = 0; i < n_events; i++) events[i] = Events[i];
   DWORD r_code = WaitForMultipleObjects (n_events,events,flag_wait_all,timeout);
   if (WAIT_OBJECT_0 <= r_code && r_code < (WAIT_OBJECT_0 + n_events)) return (DONE);
   if (r_code == WAIT_TIMEOUT) return (2);
   return (1);
}
#else
{
   return (-1);
}
#endif

}

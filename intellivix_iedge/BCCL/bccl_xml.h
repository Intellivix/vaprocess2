#ifndef BCCL_XML_H
#define BCCL_XML_H

#include <string>
#include <map>

#include "bccl_pugixml.h"
#include "bccl_xmlcvtfn.h"

namespace BCCL {

///////////////////////////////////////////////////////////////////////////////
/// Enumerations
///////////////////////////////////////////////////////////////////////////////
enum XMLIOMode {
   XMLIO_None  = 0,
   XMLIO_Read  = 1,
   XMLIO_Write = 2,
};

class CXMLNode;

////////////////////////////////////////////////////////////////////////
/// Class : CXMLIO
////////////////////////////////////////////////////////////////////////

class CXMLIO {
public:
   CXMLIO()          = default;
   virtual ~CXMLIO() = default;

   CXMLIO( std::string filename, XMLIOMode enMode ); // new way : Pugi directly reads/writes to a file.
   CXMLIO( FileIO* pFileIO, XMLIOMode enMode );      // old way : only for FileIO is FIO_STORAGE_MEMORY.

   PugiXmlHelper* Pugi();

   // NOTE(yhpark): use SetFileIO only for memory(FIO_STORAGE_MEMORY) mode.
   bool SetFileIO( FileIO* pFileIO, XMLIOMode enMode );      // compatibility for old way.
   bool ResetFile( std::string filename, XMLIOMode enMode ); // new way : pugi directly writes to a file.
   bool ResetMemory( FileIO* pFileIO, XMLIOMode enMode );    // old way : only for FileIO is FIO_STORAGE_MEMORY.

   FIO_STORAGE GetStorageType();

   // WARNING(yhpark): recommend deprecate custum options. Please solve it outside.
   int m_nCustOpt = 0; 

protected:
   XMLIOMode m_mode = XMLIO_None;

   // m_filename and m_pFileIO determine new/old modes.
   std::string m_filename;       // new way : Pugi directly reads/writes to a file.
   FileIO* m_pFileIO = nullptr;  // old way : only for FileIO is FIO_STORAGE_MEMORY.

   PugiXmlHelper m_pugiXmlHelper; // pugixml's implementation.

   friend CXMLNode;
};

////////////////////////////////////////////////////////////////////////
/// Class : CXMLNode
////////////////////////////////////////////////////////////////////////

class CXMLNode {
public:
   enum XMLNodeState {
      XMLNodeState_ReadStart  = 0x00000001,
      XMLNodeState_WriteStart = 0x00000002
   };

   explicit CXMLNode( CXMLIO* pXMLIO = nullptr );
   virtual ~CXMLNode();

public:
   void SetXMLIO( CXMLIO* pXMLIO );

   bool Start( const char* node_name );
   void End();

   bool Attribute( int nTypeID, const char* szAttrName, void* pVar, int nNum = 1 );
   bool Attribute( int nTypeID, const char* szAttrName, void* pVar, void* pDefVar, int nNum = 1 );
   bool Attribute( int nTypeID, const char* szAttrName, void* pVar, void* pDefVar, char* szFormat, int nNum = 1 );
   bool TextValue( int nTypeID, void* pVar, int nNum = 1 );
   bool TextValue( int nTypeID, void* pVar, void* pDefVar, int nNum = 1 );
   bool TextValue( int nTypeID, void* pVar, void* pDefVar, char* szFormat, int nNum = 1 );

   // NOTE(yhpark): removed : const char* PeekUnLoadChildName();

protected:
   bool ReadStart( const char* szNodeName );
   void ReadEnd();

   bool WriteStart( const char* szNodeName );
   void WriteEnd();

   bool ReadAttr( int nTypeID, const char* szAttrName, void* pVar, void* pDefVar, char* szFormat, int nNum = 1 );
   bool ReadNodeValue( int nTypeID, void* pVar, void* pDefVar, char* szFormat, int nNum = 1 );

   bool WriteAttr( int nTypeID, const char* szAttrName, void* pVar, void* pDefVar, char* szFormat, int nNum = 1 );
   bool WriteNodeValue( int nTypeID, void* pVar, void* pDefVar, char* szFormat, int nNum = 1 );

   PugiXmlHelper* Pugi();

protected:
   int m_nState     = 0; // bit masking by XMLNodeState;
   CXMLIO* m_pXMLIO = nullptr;
   pugi::xml_node m_node_current; // inside a node, from Start() to End().
};
} // namespace BCCL

#endif // BCCL_XML_H

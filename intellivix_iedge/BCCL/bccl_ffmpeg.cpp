﻿#ifdef __LIB_FFMPEG

#include "bccl_console.h"
#include "bccl_ffmpeg.h"
#include "bccl_color.h"

#include <fcntl.h>
#include <sys/stat.h>

#if defined(__WIN32)
#pragma comment(lib,"avcodec.lib")
#pragma comment(lib,"avformat.lib")
#pragma comment(lib,"avutil.lib")
#pragma comment(lib,"swscale.lib")
#pragma comment(lib,"swresample.lib")
#include <afxmt.h>
#include <io.h>
#endif

 namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// Definitions
//
///////////////////////////////////////////////////////////////////////////////

#define DEF_AUDIO_BUFFER_SIZE   1000000

///////////////////////////////////////////////////////////////////////////////
//
// Function Declarations
//
///////////////////////////////////////////////////////////////////////////////

static void _FFMPEG_Log (void* ptr,int level,const char* fmt,va_list vl);

///////////////////////////////////////////////////////////////////////////////
//
// Global Variables
//
///////////////////////////////////////////////////////////////////////////////

CCriticalSection *_CS_FFMPEG = NULL;
int _FFMPEG_LogOutput = FALSE;

///////////////////////////////////////////////////////////////////////////////
//
// Class: FFMPEG_AudioCodec
//
///////////////////////////////////////////////////////////////////////////////

 FFMPEG_AudioCodec::FFMPEG_AudioCodec (   )
 
{
   Flag_Decoding      = FALSE;
   Flag_Encoding      = FALSE;
   Flag_InnerCodecCtx = FALSE;
   Frame              = NULL;
   SwrCtx             = NULL;
   
   BitsPerSample      = 0;
   NumChannels        = 0;
   SampleRate         = 0;
   Flag_FloatSample   = FALSE;
   ChannelLayout      = 0;
   CodecID            = AV_CODEC_ID_NONE;
   SampleFormat       = AV_SAMPLE_FMT_NONE;
   CodecCtx           = NULL;

   Bitrate            = 128000;
   OutSampleRate      = 0;
}

 FFMPEG_AudioCodec::~FFMPEG_AudioCodec (   )
 
{
   Close (   );
}

 void FFMPEG_AudioCodec::Close (   )
 
{
   FFMPEG_CS_Lock (   );
   if (CodecCtx) {
      if (Flag_Encoding || Flag_Decoding) avcodec_close (CodecCtx);
      if (Flag_InnerCodecCtx) av_free (CodecCtx);
      CodecCtx = NULL;
   }
   if (Frame) {
      av_frame_free (&Frame);
      Frame = NULL;
   }
   if (SwrCtx) {
      swr_free (&SwrCtx);
      SwrCtx = NULL;
   }
   Flag_Encoding = FALSE;
   Flag_Decoding = FALSE;
   SrcBuffer.Delete (   );
   DstBuffer.Delete (   );
   FFMPEG_CS_Unlock (   );
}

 void FFMPEG_AudioCodec::ConvertPackedToPlanarSampleFormat (const byte* s_buffer,int s_buf_size,byte* d_buffer,int &d_buf_size)
 
{
   uint8_t* s_data[8];
   uint8_t* d_data[8];
   s_data[0] = (uint8_t*)s_buffer;
   int bps     = BitsPerSample / 8;                // 샘플 당 바이트 수
   int s_count = (s_buf_size / NumChannels) / bps; // 채널 당 샘플 수
   int d_count = s_count;
   if (OutSampleRate && SampleRate != OutSampleRate) d_count = (int)((float)OutSampleRate / SampleRate * s_count);
   av_samples_fill_arrays ((uint8_t**)d_data,NULL,d_buffer,NumChannels,d_count,SampleFormat,0);
   swr_convert (SwrCtx,(uint8_t**)d_data,d_count,(const uint8_t**)s_data,s_count);
   d_buf_size = d_count * NumChannels * bps;
}

 void FFMPEG_AudioCodec::ConvertPlanarToPackedSampleFormat (AVFrame* s_frame,byte *d_buffer,int &d_buf_size)
 
{
   uint8_t* d_data[8];
   memset (d_data,0,sizeof(d_data));
   int bps     = BitsPerSample / 8;   // 샘플 당 바이트 수
   int s_count = s_frame->nb_samples; // 채널 당 샘플 수 
   d_data[0]   = d_buffer;
   int d_count = s_count;
   if (OutSampleRate && SampleRate != OutSampleRate) d_count = (int)((float)OutSampleRate / SampleRate * s_count);
   swr_convert (SwrCtx,(uint8_t**)d_data,d_count,(const uint8_t**)s_frame->data,s_count);
   d_buf_size = d_count * NumChannels * bps;
}

 int FFMPEG_AudioCodec::Decode (const byte *s_buffer,int s_buf_size,byte **p_d_buffer,int &d_buf_size)
 
{
   *p_d_buffer = NULL;
   d_buf_size  = 0;
   if (!Flag_Decoding) return (1);
   int sb_size = s_buf_size + FFMPEG_BUFFER_PADDING_SIZE;
   if (SrcBuffer.Length < sb_size) SrcBuffer.Create (sb_size);
   memcpy (SrcBuffer,s_buffer,s_buf_size);
   memset ((byte*)SrcBuffer + s_buf_size,0,FFMPEG_BUFFER_PADDING_SIZE);
   int db_size = sb_size;
   if (db_size < DEF_AUDIO_BUFFER_SIZE) db_size = DEF_AUDIO_BUFFER_SIZE;
   if (DstBuffer.Length < db_size) DstBuffer.Create (db_size);
   DstBuffer.Clear (   );
   byte* in_buf     = SrcBuffer;
   byte* out_buf    = DstBuffer;
   int in_buf_size  = s_buf_size;
   int out_buf_size = DstBuffer.Length;
   while (in_buf_size > 0 && out_buf_size > 0) {
      AVPacket avpkt;
      av_init_packet (&avpkt);
      avpkt.size    = in_buf_size;
      avpkt.data    = in_buf;
      int got_frame = 0;
      int nb_read   = avcodec_decode_audio4 (CodecCtx,Frame,&got_frame,&avpkt);
      if (nb_read < 0) return (2);
      int nb_written = 0; 
      if (got_frame) ConvertPlanarToPackedSampleFormat (Frame,out_buf,nb_written);
      in_buf       += nb_read;
      out_buf      += nb_written;
      in_buf_size  -= nb_read;
      out_buf_size -= nb_written;
   }
   *p_d_buffer = DstBuffer;
   d_buf_size  = DstBuffer.Length - out_buf_size;
   return (DONE);
}

 int FFMPEG_AudioCodec::Encode (const byte *s_buffer,byte **p_d_buffer,int &d_buf_size)

{
   *p_d_buffer = NULL;
   d_buf_size  = 0;
   if (!Flag_Encoding) return (1);
   int fr_size = GetFrameSize (   );
   if (SrcBuffer.Length < fr_size) SrcBuffer.Create (fr_size);
   int s_buf_size;
   ConvertPackedToPlanarSampleFormat (s_buffer,fr_size,SrcBuffer,s_buf_size);
   avcodec_fill_audio_frame (Frame,NumChannels,SampleFormat,SrcBuffer,fr_size,0);
   AVPacket avpkt;
   av_init_packet (&avpkt);
   // avpkt.data과 avpkt.size 값은 av_init_packet()에 의해 초기화되지 않기 때문에 별도 초기화가 필요하다.
   avpkt.data = NULL; // 패킷 데이터를 저장할 버퍼는 인코더에 의해 할당된다.
   avpkt.size = 0;
   int got_output = 0;
   if (avcodec_encode_audio2 (CodecCtx,&avpkt,Frame,&got_output) < 0) return (2);
   if (got_output) {
      DstBuffer.Create (avpkt.size);
      memcpy (DstBuffer,avpkt.data,avpkt.size);
      av_free_packet (&avpkt);
      *p_d_buffer = DstBuffer;
      d_buf_size  = DstBuffer.Length;
      return (DONE);
   }
   else return (-1);
}

 int FFMPEG_AudioCodec::Flush (   )
 
{
   if (CodecCtx == NULL) return (1);
   avcodec_flush_buffers (CodecCtx);
   return (DONE);
}

 AVSampleFormat FFMPEG_AudioCodec::GetPackedSampleFormat (AVSampleFormat sample_fmt)
 
{
   return (av_get_packed_sample_fmt (sample_fmt));
}

 AVSampleFormat FFMPEG_AudioCodec::GetPlanarSampleFormat (AVSampleFormat sample_fmt)
 
{
   return (av_get_planar_sample_fmt (sample_fmt));
}

 int FFMPEG_AudioCodec::GetFrameSize (   )
 
{
   int n_samples;
   if (CodecCtx == NULL) return (0);
   if (CodecCtx->frame_size) n_samples = CodecCtx->frame_size;
   else n_samples = SampleRate / 10; // 코덱이 PCM 계열이면 100ms 기간 동안의 샘플 수로 설정된다.
   return (av_samples_get_buffer_size (NULL,NumChannels,n_samples,SampleFormat,0));
}

 int FFMPEG_AudioCodec::IsPlanarSampleFormat (AVSampleFormat sample_fmt)
 
{
   return (av_sample_fmt_is_planar (sample_fmt));
}

 int FFMPEG_AudioCodec::OpenDecoder (   )

{
   AVCodec *decoder = avcodec_find_decoder (CodecID);
   if (decoder == NULL) return (1);
   if (avcodec_open2 (CodecCtx,decoder,NULL) < 0) return (2);
   Frame = av_frame_alloc (   );
   if (Frame == NULL) return (3);
   SampleFormat  = CodecCtx->sample_fmt;
   BitsPerSample = av_get_bytes_per_sample (SampleFormat) * 8;
   AVSampleFormat pck_smp_fmt = av_get_packed_sample_fmt (SampleFormat);
   if (pck_smp_fmt == AV_SAMPLE_FMT_FLT || pck_smp_fmt == AV_SAMPLE_FMT_DBL) Flag_FloatSample = TRUE;
   else Flag_FloatSample = FALSE;
   AVSampleFormat out_smp_fmt = pck_smp_fmt; // 출력 샘플 포맷은 항상 Packed Sample Format이다.
   int out_smp_rate = SampleRate;
   if (OutSampleRate) out_smp_rate = OutSampleRate; 
   SwrCtx = swr_alloc (   );
   av_opt_set_int        (SwrCtx,"in_channel_count"  ,NumChannels             ,0);
   av_opt_set_int        (SwrCtx,"out_channel_count" ,NumChannels             ,0);
   av_opt_set_int        (SwrCtx,"in_channel_layout" ,CodecCtx->channel_layout,0);
   av_opt_set_int        (SwrCtx,"out_channel_layout",CodecCtx->channel_layout,0);
   av_opt_set_int        (SwrCtx,"in_sample_rate"    ,SampleRate              ,0);
   av_opt_set_int        (SwrCtx,"out_sample_rate"   ,out_smp_rate            ,0);
   av_opt_set_sample_fmt (SwrCtx,"in_sample_fmt"     ,SampleFormat            ,0);
   av_opt_set_sample_fmt (SwrCtx,"out_sample_fmt"    ,out_smp_fmt             ,0);
   if (swr_init (SwrCtx)) return (4);
   Flag_Decoding = TRUE;
   return (DONE);
}

 int FFMPEG_AudioCodec::OpenDecoder (AVCodecID codec_id,int n_channels,int sample_rate)
 
{
   Close (   );
   FFMPEG_CS_Lock (   );
   AVCodec* decoder = avcodec_find_decoder (codec_id);
   if (decoder == NULL) {
      FFMPEG_CS_Unlock (   );
      Close (   );
      return (1);
   }
   CodecCtx = avcodec_alloc_context3 (decoder);
   Flag_InnerCodecCtx = TRUE;
   if (CodecCtx == NULL) {
      FFMPEG_CS_Unlock (   );
      Close (   );
      return (2);
   }
   CodecID               = codec_id;
   NumChannels           = n_channels;
   SampleRate            = sample_rate;
   CodecCtx->channels    = NumChannels;
   CodecCtx->sample_rate = SampleRate;
   int e_code = OpenDecoder (   );
   FFMPEG_CS_Unlock (   );
   if (e_code != DONE) {
      Close (   );
      return (e_code + 2);
   }
   return (DONE);
} 

 int FFMPEG_AudioCodec::OpenDecoder (AVCodecContext *codec_ctx)
 
{
   Close (   );
   FFMPEG_CS_Lock (   );
   CodecID      = codec_ctx->codec_id;
   NumChannels  = codec_ctx->channels;
   SampleRate   = codec_ctx->sample_rate;
   SampleFormat = codec_ctx->sample_fmt;
   CodecCtx     = codec_ctx;
   Flag_InnerCodecCtx = FALSE;
   int e_code = OpenDecoder (   );
    FFMPEG_CS_Unlock (   );
   if (e_code != DONE) Close (   );
   return (e_code);
}

 int FFMPEG_AudioCodec::OpenEncoder (   )
 
{
   AVCodec *encoder = avcodec_find_encoder (CodecID);
   if (encoder == NULL) return (1);
   if (avcodec_open2 (CodecCtx,encoder,NULL) < 0) return (2);
   Frame = av_frame_alloc (   );
   if (Frame == NULL) return (3);
   BitsPerSample = av_get_bytes_per_sample (SampleFormat) * 8;
   AVSampleFormat pck_smp_fmt = av_get_packed_sample_fmt (SampleFormat);
   if (pck_smp_fmt == AV_SAMPLE_FMT_FLT || pck_smp_fmt == AV_SAMPLE_FMT_DBL) Flag_FloatSample = TRUE;
   else Flag_FloatSample = FALSE;
   AVSampleFormat in_smp_fmt = pck_smp_fmt; // 입력 샘플 포맷은 항상 Packed Sample Format이다.
   SwrCtx = swr_alloc (   );
   av_opt_set_int        (SwrCtx,"in_channel_count"  ,NumChannels             ,0);
   av_opt_set_int        (SwrCtx,"out_channel_count" ,NumChannels             ,0);
   av_opt_set_int        (SwrCtx,"in_channel_layout" ,CodecCtx->channel_layout,0);
   av_opt_set_int        (SwrCtx,"out_channel_layout",CodecCtx->channel_layout,0);
   av_opt_set_int        (SwrCtx,"in_sample_rate"    ,SampleRate              ,0);
   av_opt_set_int        (SwrCtx,"out_sample_rate"   ,SampleRate              ,0);
   av_opt_set_sample_fmt (SwrCtx,"in_sample_fmt"     ,in_smp_fmt              ,0);
   av_opt_set_sample_fmt (SwrCtx,"out_sample_fmt"    ,SampleFormat            ,0);
   if (swr_init (SwrCtx)) return (4);
   Flag_Encoding = TRUE;
   return (DONE);
}

 int FFMPEG_AudioCodec::OpenEncoder (AVCodecID codec_id,int n_channels,int sample_rate,AVSampleFormat sample_fmt,uint64_t channel_layout)
 
{
   Close (   );
   FFMPEG_CS_Lock (   );
   AVCodec* encoder = avcodec_find_encoder (codec_id);
   if (encoder == NULL) {
      FFMPEG_CS_Unlock (   );
      Close (   );
      return (1);
   }
   CodecCtx = avcodec_alloc_context3 (encoder);
   Flag_InnerCodecCtx = TRUE;
   if (CodecCtx == NULL) {
      FFMPEG_CS_Unlock (   );
      Close (   );
      return (2);
   }
   CodecID      = codec_id;
   NumChannels  = n_channels;
   SampleRate   = sample_rate;
   SampleFormat = av_get_planar_sample_fmt (sample_fmt);
   if (channel_layout) ChannelLayout = channel_layout;
   else ChannelLayout = av_get_default_channel_layout (n_channels);
   CodecCtx->bit_rate       = Bitrate;
   CodecCtx->channels       = NumChannels;
   CodecCtx->sample_rate    = SampleRate;
   CodecCtx->sample_fmt     = SampleFormat;
   CodecCtx->channel_layout = ChannelLayout;
   int e_code = OpenEncoder (   );
   FFMPEG_CS_Unlock (   );
   if (e_code != DONE) {
      Close (   );
      return (e_code + 2);
   }
   return (DONE);
}

 int FFMPEG_AudioCodec::OpenEncoder (AVCodecContext *codec_ctx)
 
{
   Close (   );
   FFMPEG_CS_Lock (   );
   CodecID       = codec_ctx->codec_id;
   NumChannels   = codec_ctx->channels;
   SampleRate    = codec_ctx->sample_rate;
   SampleFormat  = codec_ctx->sample_fmt;
   ChannelLayout = codec_ctx->channel_layout;
   CodecCtx      = codec_ctx;
   CodecCtx->bit_rate = Bitrate;
   Flag_InnerCodecCtx = FALSE;
   int e_code = OpenEncoder (   );
   FFMPEG_CS_Unlock (   );
   if (e_code != DONE) Close (   );
   return (e_code);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: FFMPEG_SubtitleCodec
//
///////////////////////////////////////////////////////////////////////////////

 FFMPEG_SubtitleCodec::FFMPEG_SubtitleCodec (   )
 
{
   Flag_Decoding      = FALSE;
   Flag_Encoding      = FALSE;
   Flag_InnerCodecCtx = FALSE;
   CodecCtx           = NULL;
   CodecID            = AV_CODEC_ID_NONE;
}

 FFMPEG_SubtitleCodec::~FFMPEG_SubtitleCodec (   )
 
{
   Close (   );
}

 void FFMPEG_SubtitleCodec::Close (   )

{
   FFMPEG_CS_Lock (   );
   if (CodecCtx) {
      if (Flag_Decoding || Flag_Encoding) avcodec_close (CodecCtx);
      if (Flag_InnerCodecCtx) av_free  (CodecCtx);
      CodecCtx = NULL;
   }
   Flag_Decoding = FALSE;
   FFMPEG_CS_Unlock (   );
}

 int FFMPEG_SubtitleCodec::Decode (const byte* s_buffer,int s_buf_size,AVSubtitle& d_subtitle,int& time_pos,int& duration)
 
{
   memset (&d_subtitle,0,sizeof(AVSubtitle));
   if (!Flag_Decoding) return (1);
   if (CodecID == AV_CODEC_ID_TEXT) {
      d_subtitle.format    = 1;
      d_subtitle.num_rects = 1;
      d_subtitle.rects = (AVSubtitleRect**)av_malloc (sizeof(AVSubtitleRect*));
      AVSubtitleRect* rect = (AVSubtitleRect*)av_malloc (sizeof(AVSubtitleRect));
      d_subtitle.rects[0] = rect;
      rect->type = SUBTITLE_TEXT;
      rect->text = (char*)av_malloc (s_buf_size + 1);
      memcpy (rect->text,s_buffer,s_buf_size);
      rect->text[s_buf_size] = 0;
   }
   else {
      AVPacket avpkt;
      av_init_packet (&avpkt);
      avpkt.data = (uint8_t*)s_buffer;
      avpkt.size = s_buf_size;
      int got_sub;
      int n_read = avcodec_decode_subtitle2 (CodecCtx,&d_subtitle,&got_sub,&avpkt);
      if (n_read < 0) return (2);
      if (got_sub) {
         int d_time = d_subtitle.end_display_time - d_subtitle.start_display_time;
         if (d_time > 0) {
            time_pos += d_subtitle.start_display_time;
            duration = d_time;
         }
      }
   }
   return (DONE);
}

 int FFMPEG_SubtitleCodec::Flush (   )
 
{
   if (CodecCtx == NULL) return (1);
   if (CodecID != AV_CODEC_ID_TEXT) avcodec_flush_buffers (CodecCtx);
   return (DONE);
}

 int FFMPEG_SubtitleCodec::OpenDecoder (   )
 
{
   if (CodecID != AV_CODEC_ID_TEXT) {
      AVCodec *decoder = avcodec_find_decoder (CodecID);
      if (decoder == NULL) return (1);
      if (avcodec_open2 (CodecCtx,decoder,NULL) < 0) return (2);
   }
   Flag_Decoding = TRUE;
   return (DONE);
}

 int FFMPEG_SubtitleCodec::OpenDecoder (AVCodecContext *codec_ctx)
 
{
   Close (   );
   FFMPEG_CS_Lock (   );
   CodecCtx = codec_ctx;
   CodecID  = codec_ctx->codec_id;
   Flag_InnerCodecCtx = FALSE;
   int e_code = OpenDecoder (   );
   FFMPEG_CS_Unlock (   );
   if (e_code) Close (   );
   return (e_code);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: FFMPEG_VideoCodec
//
///////////////////////////////////////////////////////////////////////////////

 FFMPEG_VideoCodec::FFMPEG_VideoCodec (   )
 
{
   Flag_Decoding      = FALSE;
   Flag_Encoding      = FALSE;
   Flag_InnerCodecCtx = FALSE;
   SrcFrame           = NULL;
   DstFrame           = NULL;
   SwsCtx             = NULL;
   Options            = NULL;
   CodecCtx           = NULL;
   FrameWidth         = 0;
   FrameHeight        = 0;
   FrameWidth2        = 0;
   FrameHeight2       = 0;
   FrameRate          = 0.0f;
   Bitrate            = 1000 * 1024;
   GOPSize            = 15;
   CodecID            = AV_CODEC_ID_NONE;
   PixFmt             = FFMPEG_PIXFMT_YUY2;
}

 FFMPEG_VideoCodec::~FFMPEG_VideoCodec (   )
 
{
   Close (   );
}

 void FFMPEG_VideoCodec::Close (   )
 
{
   FFMPEG_CS_Lock (   );
   Options = NULL;
   if (CodecCtx) {
      if (Flag_Encoding || Flag_Decoding) avcodec_close (CodecCtx);
      if (Flag_InnerCodecCtx) av_free (CodecCtx);
      CodecCtx = NULL;
   }
   Flag_Encoding = FALSE;
   Flag_Decoding = FALSE;
   if (SrcFrame) {
      av_free (SrcFrame);
      SrcFrame = NULL;
   }
   if (DstFrame) {
      av_free (DstFrame);
      DstFrame = NULL;
   }
   if (SwsCtx) {
      sws_freeContext (SwsCtx);
      SwsCtx = NULL;
   }
   SrcBuffer.Delete (   );
   DstBuffer.Delete (   );
   FFMPEG_CS_Unlock (   );
}

 int FFMPEG_VideoCodec::Resize (AVPixelFormat nPixFmt, const byte* s_buffer, int s_width, int s_height, byte* d_buffer, int d_width, int d_height)

 {             
    if (!SwsCtx ||
       (PixFmt != nPixFmt) ||
       (FrameWidth  != s_width || FrameHeight  != s_height) ||
       (FrameWidth2 != d_width || FrameHeight2 != d_height))
    {
       Close (   );
       
       SrcFrame = av_frame_alloc (   );
       DstFrame = av_frame_alloc (   );

       PixFmt       = nPixFmt;
       FrameWidth   = s_width;
       FrameHeight  = s_height;
       FrameWidth2  = d_width;
       FrameHeight2 = d_height;

       SwsCtx = sws_getContext(s_width, s_height, PixFmt, d_width, d_height, PixFmt, SWS_BICUBIC, NULL, NULL, NULL);
    }

    avpicture_fill((AVPicture*)SrcFrame, s_buffer, PixFmt, s_width, s_height);                    
    avpicture_fill((AVPicture*)DstFrame, d_buffer, PixFmt, d_width, d_height);

    int n_read = sws_scale(SwsCtx, SrcFrame->data, SrcFrame->linesize, 0, s_height, DstFrame->data, DstFrame->linesize);
    if (n_read != FrameHeight2)
       return 1;

    return (DONE);
 }

 int FFMPEG_VideoCodec::Convert (AVPixelFormat s_nPixFmt, const byte* s_buffer, int s_width, int s_height, AVPixelFormat d_nPixFmt, byte* d_buffer, int d_width, int d_height)

 {
    if (!SwsCtx ||
       (PixFmt  != s_nPixFmt) ||
       (PixFmt2 != d_nPixFmt) ||
       (FrameWidth  != s_width || FrameHeight  != s_height) ||
       (FrameWidth2 != d_width || FrameHeight2 != d_height))
    {
       Close ();

       SrcFrame = av_frame_alloc ();
       DstFrame = av_frame_alloc ();

       PixFmt       = s_nPixFmt;
       PixFmt2      = d_nPixFmt;
       FrameWidth   = s_width;
       FrameHeight  = s_height;
       FrameWidth2  = d_width;
       FrameHeight2 = d_height;
       
       SwsCtx = sws_getContext(s_width, s_height, s_nPixFmt, d_width, d_height, d_nPixFmt, SWS_BICUBIC, NULL, NULL, NULL);
    }

    avpicture_fill((AVPicture*)SrcFrame, s_buffer, s_nPixFmt, s_width, s_height);
    avpicture_fill((AVPicture*)DstFrame, d_buffer, d_nPixFmt, d_width, d_height);

    int n_read = sws_scale(SwsCtx, SrcFrame->data, SrcFrame->linesize, 0, s_height, DstFrame->data, DstFrame->linesize);
    if (n_read != FrameHeight2)
       return 1;

    return (DONE);
 }

 int FFMPEG_VideoCodec::Decode (const byte* s_buffer,int s_buf_size,AVFrame* d_frame)
 
{
   if (!Flag_Decoding) return (1);
   int sb_size = s_buf_size + FFMPEG_BUFFER_PADDING_SIZE;
   if (SrcBuffer.Length < sb_size) SrcBuffer.Create (sb_size);
   memcpy ((byte*)SrcBuffer,s_buffer,s_buf_size);
   memset ((byte*)SrcBuffer + s_buf_size,0,FFMPEG_BUFFER_PADDING_SIZE);
   AVPacket avpkt;
   av_init_packet (&avpkt);
   avpkt.data = (byte*)SrcBuffer;
   avpkt.size = s_buf_size;
   int got_picture = FALSE;
   CodecCtx->refcounted_frames = 1;
   int n_read = avcodec_decode_video2 (CodecCtx,d_frame,&got_picture,&avpkt);
   if (n_read < 0) return (2);
   if (got_picture) return (DONE);
   else return (-1);
}

 int FFMPEG_VideoCodec::Decode (const byte* s_buffer,int s_buf_size,ISize2D &d_fr_size)
 
{
   if (!Flag_Decoding) return (1);
   int sb_size = s_buf_size + FFMPEG_BUFFER_PADDING_SIZE;
   if (SrcBuffer.Length < sb_size) SrcBuffer.Create (sb_size);
   memcpy ((byte*)SrcBuffer,s_buffer,s_buf_size);
   memset ((byte*)SrcBuffer + s_buf_size,0,FFMPEG_BUFFER_PADDING_SIZE);
   AVPacket avpkt;
   av_init_packet (&avpkt);
   avpkt.data = (byte*)SrcBuffer;
   avpkt.size = s_buf_size;
   int got_picture = FALSE;
   CodecCtx->refcounted_frames = 0;
   int n_read = avcodec_decode_video2 (CodecCtx,SrcFrame,&got_picture,&avpkt);
   if (n_read < 0)   return (2);   

   if (!got_picture)
      return (-1);

   DecodedFrameSize (SrcFrame->width, SrcFrame->height); // jun: 디코딩 영상크기 저장.

   if (CodecID == AV_CODEC_ID_MJPEG && CodecCtx->pix_fmt == AV_PIX_FMT_YUV420P) {
      OpenDecoder ();
      Decode (s_buffer, s_buf_size, d_fr_size);
   }   
   if (SrcFrame->width != FrameWidth || SrcFrame->height != FrameHeight) // jun : 디코딩한 영상의 크기가 다르면 리턴
      return (3);
   d_fr_size(FrameWidth, FrameHeight);
   if (CodecCtx->width != FrameWidth || CodecCtx->height != FrameHeight) {
      d_fr_size(CodecCtx->width, CodecCtx->height);
      return (-2);
   }

   return (DONE);
}

 int FFMPEG_VideoCodec::Decode (const byte* s_buffer,int s_buf_size,byte *d_buffer,const int* pitches,int n_planes)
 
{
   ISize2D fr_size;
   int e_code = Decode (s_buffer,s_buf_size,fr_size);
   if (e_code == DONE) GetDecodedFrameData (d_buffer,pitches,n_planes);
   return (e_code);
}

 int FFMPEG_VideoCodec::Decode (const byte* s_buffer,int s_buf_size,byte **p_d_buffer,const int* pitches,int n_planes)
 
{
   int i;
   
   int buf_size = 0;
   if (pitches == NULL) buf_size = FrameWidth * FrameHeight * 4;
   else for (i = 0; i < n_planes; i++) buf_size += pitches[i] * FrameHeight;
   buf_size += FFMPEG_BUFFER_PADDING_SIZE;
   if (DstBuffer.Length < buf_size) DstBuffer.Create (buf_size);
   *p_d_buffer = DstBuffer;
   return (Decode (s_buffer,s_buf_size,DstBuffer,pitches,n_planes));
}

 int FFMPEG_VideoCodec::Decode( const byte *s_buffer,int s_buf_size,BGRImage &d_image )
 {
   if (!Flag_Decoding) return (1);
   if (d_image.Width != FrameWidth || d_image.Height != FrameHeight) d_image.Create (FrameWidth,FrameHeight);
   byte *d_buffer;
   int e_code = Decode (s_buffer,s_buf_size,&d_buffer);
   if (e_code == DONE) {
      switch (PixFmt) {
         case AV_PIX_FMT_YUV420P:
            ConvertYV12ToBGR (d_buffer,d_image,YV12_FORMAT_STANDARD);
            break;
         case AV_PIX_FMT_YUYV422:
            ConvertYUY2ToBGR (d_buffer,d_image);
            break;
         case AV_PIX_FMT_BGR24:
            memcpy ((BGRPixel*)d_image,d_buffer,d_image.GetLineLength() * d_image.Height);
            break;
         default:
            d_image.Clear (   );
            return (3);
      }
   }
   else if (e_code < 0) return (-1);
   else if (e_code == 4) return 4;
   else return (2);
   return (DONE);
 }

 int FFMPEG_VideoCodec::Encode (const byte *s_buffer,byte **p_d_buffer,int &d_buf_size,int &flag_key_frame)
 
{
   *p_d_buffer = NULL;
   d_buf_size  = 0;
   flag_key_frame = FALSE;
   if (!Flag_Encoding) return (1);
   avpicture_fill ((AVPicture*)SrcFrame,(byte*)s_buffer,PixFmt,FrameWidth,FrameHeight);
   AVFrame* s_frame = SrcFrame;
   int buf_size = avpicture_get_size (CodecCtx->pix_fmt,FrameWidth,FrameHeight);
   if (CodecCtx->pix_fmt != PixFmt) {
      if (SrcBuffer.Length < buf_size) SrcBuffer.Create (buf_size);
      avpicture_fill ((AVPicture*)DstFrame,SrcBuffer,CodecCtx->pix_fmt,FrameWidth,FrameHeight);
      sws_scale (SwsCtx,SrcFrame->data,SrcFrame->linesize,0,FrameHeight,DstFrame->data,DstFrame->linesize);
      s_frame = DstFrame;
   }
   AVPacket avpkt;
   av_init_packet (&avpkt);
   avpkt.data = NULL;
   avpkt.size = 0;
   int got_output = 0;
   if (avcodec_encode_video2 (CodecCtx,&avpkt,s_frame,&got_output) < 0) return (2);
   if (got_output) {
      DstBuffer.Create (avpkt.size);
      memcpy (DstBuffer,avpkt.data,avpkt.size);
      if (avpkt.flags & AV_PKT_FLAG_KEY) flag_key_frame = TRUE;
      av_free_packet (&avpkt);
      *p_d_buffer = DstBuffer;
      d_buf_size  = DstBuffer.Length;
      return (DONE);
   }
   else return (-1);
}

 int FFMPEG_VideoCodec::Encode( BGRImage &s_image,byte **p_d_buffer,int &d_buf_size,int &flag_key_frame )
 {
   int e_code;
   
   switch (PixFmt) {
      case AV_PIX_FMT_YUV420P: {
         BArray1D s_buffer(s_image.Width * s_image.Height * 3 / 2);
         ConvertBGRToYV12 (s_image,s_buffer,YV12_FORMAT_STANDARD);
         e_code = Encode (s_buffer,p_d_buffer,d_buf_size,flag_key_frame);
      }  break;
      case AV_PIX_FMT_YUYV422: {
         BArray1D s_buffer(s_image.Width * s_image.Height * 2);
         ConvertBGRToYUY2 (s_image,s_buffer);
         e_code = Encode (s_buffer,p_d_buffer,d_buf_size,flag_key_frame);
      }  break;
      case AV_PIX_FMT_BGR24: {
         e_code = Encode ((byte*)(BGRPixel*)s_image,p_d_buffer,d_buf_size,flag_key_frame);
      }  break;
      default:
         e_code = 3;
         break;
   }
   return (e_code);
 }


 int FFMPEG_VideoCodec::Flush (   )
 
{
   if (CodecCtx == NULL) return (1);
   avcodec_flush_buffers (CodecCtx);
   return (DONE);
}

 int FFMPEG_VideoCodec::GetDecodedFrameData (byte* d_buffer,const int* pitches,int n_planes)
 
{
   if (!Flag_Decoding) return (1);

   // NOTE(yhpark): 영상 디코딩 이후, 기본 디폴트 포멧으로 변환하도록 짜져있었음. 
   // RTSP 영상일 경우, IPCamStream_RTSP.cpp에 기본 픽셀포멧이 박혀있음.
   // 칼라일 경우: AV_PIX_FMT_YUYV422 
   // 흑백일 경우: AV_PIX_FMT_GRAY8
   avpicture_fill ((AVPicture*)DstFrame,d_buffer,PixFmt,FrameWidth,FrameHeight);
   if (pitches) {
      if (n_planes > 4) n_planes = 4;
      for (int i = 0; i < n_planes; i++) DstFrame->linesize[i] = pitches[i];
   }
   sws_scale (SwsCtx,SrcFrame->data,SrcFrame->linesize,0,FrameHeight,DstFrame->data,DstFrame->linesize);
   return (DONE);
}

 int FFMPEG_VideoCodec::GetFrameSize (   )
 
{
   int buf_size = avpicture_get_size (PixFmt,FrameWidth,FrameHeight);
   return (buf_size);
}

 int FFMPEG_VideoCodec::OpenDecoder (   )

{
   SrcFrame = av_frame_alloc (   );
   if (SrcFrame == NULL) return (1);
   DstFrame = av_frame_alloc (   );
   if (DstFrame == NULL) return (2);
   AVCodec *decoder;
   if (Flag_InnerCodecCtx) decoder = avcodec_find_decoder (CodecID);
   else decoder = avcodec_find_decoder (CodecCtx->codec_id);
   if (decoder == NULL) return (3);
   if (avcodec_open2 (CodecCtx,decoder,NULL) < 0) return (4);
   Flag_Decoding = TRUE;
   FrameWidth    = CodecCtx->width;
   FrameHeight   = CodecCtx->height;

   // NOTE(yhpark): 영상 디코딩 이후, 기본 디폴트 포멧으로 변환하도록 짜져있었음. 
   // RTSP 영상일 경우, IPCamStream_RTSP.cpp에 기본 픽셀포멧이 박혀있음.
   // 칼라일 경우: AV_PIX_FMT_YUYV422 
   // 흑백일 경우: AV_PIX_FMT_GRAY8
   SwsCtx = sws_getContext (
            FrameWidth,FrameHeight,CodecCtx->pix_fmt,
            FrameWidth,FrameHeight,PixFmt,
            SWS_FAST_BILINEAR,NULL,NULL,NULL);
   if (SwsCtx == NULL) return (5);
   return (DONE);
}

 int FFMPEG_VideoCodec::OpenDecoder (AVCodecID codec_id,int frm_width,int frm_height,
    AVPixelFormat nPixFmt /* AV_PIX_FMT_YUV420P */) 
{
   Close (   );
   FFMPEG_CS_Lock (   );
   AVCodec* decoder = avcodec_find_decoder (codec_id);
   if (decoder == NULL) {
      FFMPEG_CS_Unlock (   );
      return (1);
   }
   CodecCtx = avcodec_alloc_context3 (NULL);
   Flag_InnerCodecCtx = TRUE;
   if (CodecCtx == NULL) {
      FFMPEG_CS_Unlock (   );
      Close (   );
      return (2);
   }

   CodecID           = codec_id;
   CodecCtx->width   = frm_width;
   CodecCtx->height  = frm_height;
   CodecCtx->pix_fmt = nPixFmt;   

   int e_code = OpenDecoder (   );
   FFMPEG_CS_Unlock (   );
   if (e_code != DONE) {
      Close (   );
      return (e_code + 1);
   }
   else return (DONE);
}

 int FFMPEG_VideoCodec::OpenDecoder (AVCodecContext *codec_ctx)

{
   Close (   );
   FFMPEG_CS_Lock (   );
   Flag_InnerCodecCtx = FALSE;
   CodecCtx = codec_ctx;
   int e_code = OpenDecoder (   );
   FFMPEG_CS_Unlock  (   );
   if (e_code) Close (   );
   return (e_code);
}

 int FFMPEG_VideoCodec::OpenEncoder (   )
 
{
   SrcFrame = av_frame_alloc (   );
   if (SrcFrame == NULL) return (1);
   DstFrame = av_frame_alloc (   );
   if (DstFrame == NULL) return (2);
   AVCodec *encoder = avcodec_find_encoder (CodecID);
   if (encoder == NULL)  return (3);
   if (Options == NULL) {
      if (avcodec_open2 (CodecCtx,encoder,NULL) < 0) return (4);
   }
   else {
      if (avcodec_open2 (CodecCtx,encoder,&Options) < 0) return (5);
   }
   Flag_Encoding = TRUE;
   SwsCtx = sws_getContext (FrameWidth,FrameHeight,PixFmt,FrameWidth,FrameHeight,CodecCtx->pix_fmt,SWS_FAST_BILINEAR,NULL,NULL,NULL);
   if (SwsCtx == NULL) return (6);
   return (DONE);
}

 int FFMPEG_VideoCodec::OpenEncoder (AVCodecID codec_id,int frm_width,int frm_height,float frm_rate)
 
{
   Close (   );
   FFMPEG_CS_Lock (   );
   switch (codec_id) {
      case AV_CODEC_ID_H263:
      case AV_CODEC_ID_H263I:
         codec_id = AV_CODEC_ID_H263P;
         break;
      default:
         break;
   }
   AVCodec* encoder = avcodec_find_encoder (codec_id);
   if (encoder == NULL) {
      return (1);
   }
   CodecCtx = avcodec_alloc_context3 (encoder);
   Flag_InnerCodecCtx = TRUE;
   if (CodecCtx == NULL) {
      Close (   );
      return (2);
   }
   CodecID                 = codec_id;
   FrameWidth              = frm_width;
   FrameHeight             = frm_height;
   FrameRate               = frm_rate;
   CodecCtx->bit_rate      = Bitrate;
   CodecCtx->width         = FrameWidth;
   CodecCtx->height        = FrameHeight;
   CodecCtx->gop_size      = GOPSize;
   CodecCtx->pix_fmt       = AV_PIX_FMT_YUV420P;
   CodecCtx->time_base.num = 100;
   CodecCtx->time_base.den = (int)(FrameRate * 100.0f);
   switch (codec_id) {
      case AV_CODEC_ID_H264: {
         // 프로파일을 설정한다.
         av_dict_set (&Options,"vprofile","baseline",0);
         // 프리셋을 설정한다.
         av_dict_set (&Options,"preset","ultrafast",0);
         // 인코딩 레이턴시가 최대한 없도록 설정한다.
         // 본 설정을 해야 밑의 비트레이트 설정이 제대로 동작한다.
         av_dict_set (&Options,"tune","zerolatency",0);
         char bitrate[16];
         sprintf (bitrate,"%d",Bitrate);
         // 인코딩 비트레이트를 설정한다.
         av_dict_set (&Options,"b",bitrate,0);
         // VBR로 인코딩하려면 crf (constant rate factor),
         // cqp (constant quantization parameter rate control)
         // 설정?
      }  break;
      case AV_CODEC_ID_MJPEG: {
         CodecCtx->pix_fmt = AV_PIX_FMT_YUVJ420P;
      }  break;
      default:
         break;
   }
   int e_code = OpenEncoder (   );
   FFMPEG_CS_Unlock (   );
   if (e_code != DONE) {
      Close (   );
      return (e_code + 2);
   }
   return (DONE);
}

 int FFMPEG_VideoCodec::OpenEncoder (AVCodecContext* codec_ctx)
 
{
   Close (   );
   FFMPEG_CS_Lock (   );
   CodecID     = codec_ctx->codec_id;
   FrameWidth  = codec_ctx->width;
   FrameHeight = codec_ctx->height;
   FrameRate   = (float)codec_ctx->time_base.den / codec_ctx->time_base.num;
   CodecCtx    = codec_ctx;
   CodecCtx->pix_fmt  = AV_PIX_FMT_YUV420P;
   CodecCtx->bit_rate = Bitrate;
   CodecCtx->gop_size = GOPSize;
   Flag_InnerCodecCtx = FALSE;
   switch (CodecID) {
      case AV_CODEC_ID_H264: {
         // 프로파일을 설정한다.
         av_dict_set (&Options,"vprofile","baseline",0);
         // 프리셋을 설정한다.
         // 본 설정을 해야 빠른 인코딩이 수행된다.
         av_dict_set (&Options,"preset","ultrafast",0);
         // 인코딩 레이턴시가 최대한 없도록 설정한다.
         // 본 설정을 해야 밑의 비트레이트 설정이 제대로 동작한다.
         av_dict_set (&Options,"tune","zerolatency",0);
         char bitrate[16];
         sprintf (bitrate,"%d",Bitrate);
         // 인코딩 비트레이트를 설정한다.
         av_dict_set (&Options,"b",bitrate,0);
      }  break;
      case AV_CODEC_ID_MJPEG: {
         CodecCtx->pix_fmt = AV_PIX_FMT_YUVJ420P;
      }  break;
      default:
         break;
   }
   int e_code = OpenEncoder (   );
   FFMPEG_CS_Unlock (   );
   if (e_code != DONE) Close (   );
   return (e_code);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: FFMPEG_FileReader
//
///////////////////////////////////////////////////////////////////////////////

 FFMPEG_FileReader::FFMPEG_FileReader (   ) 
 
{
   _Init (   );
}

 FFMPEG_FileReader::~FFMPEG_FileReader (   )
 
{
   Close (   );
}

 void FFMPEG_FileReader::_Init (   )
 
{
   FileHandle = -1;
   IOBuffer   = NULL;
   AVIOCtx    = NULL;
   Duration   = 0;
   FormatCtx  = NULL;
   memset (&Packet,0,sizeof(AVPacket));
   AudioTrackList.Delete    (   );
   SubtitleTrackList.Delete (   );
   VideoTrackList.Delete    (   );
   ResetAudioTrackInfo      (   );
   ResetSubtitleTrackInfo   (   );
   ResetVideoTrackInfo      (   );
}

 void FFMPEG_FileReader::Close (   )
 
{
   if (FormatCtx) avformat_close_input (&FormatCtx);
   //if (FileHandle != -1) _close (FileHandle);
   if (Packet.data) av_free_packet (&Packet);
   _Init (   );
}

 void FFMPEG_FileReader::GetTrackDescription (AVDictionary* metadata,TrackInfo& ti)
 
{
   AVDictionaryEntry *de;
   de = av_dict_get (metadata,"handler_name",NULL,AV_DICT_IGNORE_SUFFIX);
   if (de != NULL && de->value != NULL) ti.HandlerName = de->value;
   else ti.HandlerName.Delete (   );
   de = av_dict_get (metadata,"language",NULL,AV_DICT_IGNORE_SUFFIX);
   if (de != NULL && de->value != NULL) ti.Language = de->value;
   else ti.Language.Delete (   );
   de = av_dict_get (metadata,"title",NULL,AV_DICT_IGNORE_SUFFIX);
   if (de != NULL && de->value != NULL) ti.Title = de->value;
   else ti.Title.Delete (   );

   //logd ("------------------------------\n");
   AVDictionaryEntry* t = NULL;
   while (t = av_dict_get(metadata,"",t,AV_DICT_IGNORE_SUFFIX)) {
      logd ("[%s][%s]\n",t->key,t->value);
   }

}

 int FFMPEG_FileReader::Open (   )
 
{
   int i;
   
   #if defined(__FFMPEG_DEBUG)
   logd ("========== FFMPEG_FileReader::Open() ==========\n");
   #endif
   if (avformat_find_stream_info (FormatCtx,NULL) < 0) {
      Close (   );
      return (1);
   }
   Duration = (int)(FormatCtx->duration * 1000 / AV_TIME_BASE);
   int n  = (int)FormatCtx->nb_streams;
   int na = 0, ns = 0, nv = 0;
   AVStream** streams = FormatCtx->streams;
   #if defined(__FFMPEG_DEBUG)
   logd ("nb_streams = %d\n",FormatCtx->nb_streams);
   #endif
   for (i = 0; i < n; i++) {
      #if defined(__FFMPEG_DEBUG)
      logd ("Stream #%d --------------------\n",i);
      logd ("codec_type = %X\n",streams[i]->codec->codec_type);
      if (streams[i]->start_time == AV_NOPTS_VALUE) logd ("start_time = AV_NOPTS_VALUE\n");
      else {
         int start_time = (int)(streams[i]->start_time * streams[i]->time_base.num * 1000 / streams[i]->time_base.den);
         logd ("start_time = %d\n",start_time);
      }
      #endif
      switch (streams[i]->codec->codec_type) {
         case AVMEDIA_TYPE_AUDIO: {
            na++;
         }  break;
         case AVMEDIA_TYPE_SUBTITLE: {
            ns++;
         }  break;
         case AVMEDIA_TYPE_VIDEO: {
            if (!(streams[i]->disposition & AV_DISPOSITION_ATTACHED_PIC)) nv++;
         }  break;
         default:
            break;
      }
   }
   if (na) AudioTrackList.Create    (na);
   if (ns) SubtitleTrackList.Create (ns);
   if (nv) VideoTrackList.Create    (nv);
   na = ns = nv = 0;
   for (i = 0; i < n; i++) {
      switch (streams[i]->codec->codec_type) {
         case AVMEDIA_TYPE_AUDIO: {
            AudioTrackList[na].StreamIndex = i;
            GetTrackDescription (streams[i]->metadata,AudioTrackList[na]);
            na++;
         }  break;
         case AVMEDIA_TYPE_SUBTITLE: {
            SubtitleTrackList[ns].StreamIndex = i;
            GetTrackDescription (streams[i]->metadata,SubtitleTrackList[ns]);
            ns++;
         }  break;
         case AVMEDIA_TYPE_VIDEO: {
            if (!(streams[i]->disposition & AV_DISPOSITION_ATTACHED_PIC)) {
               VideoTrackList[nv].StreamIndex = i;
               GetTrackDescription (streams[i]->metadata,VideoTrackList[nv]);
               nv++;
            }
         }  break;
         default:
            break;
      }
   }
   if (na) SetAudioTrack    (0);
   else SetAudioTrack       (-1);
   if (ns) SetSubtitleTrack (0);
   else SetSubtitleTrack    (-1);
   if (nv) SetVideoTrack    (0);
   else SetVideoTrack       (-1);
   return (DONE);
}

 int FFMPEG_FileReader::Open (const char *file_name)
 
{
   Close (   );
   if (avformat_open_input (&FormatCtx,file_name,NULL,NULL)) {
      Close (   );
      return (1);
   }
   int e_code = Open (   );
   if (e_code != DONE) return (e_code + 1);
   return (DONE);
}

 int FFMPEG_FileReader::Read (byte **p_d_buffer,int &d_buf_size,int &time_pos,int &duration,int &flags)
 
{
   *p_d_buffer = NULL;
   d_buf_size  = 0;
   time_pos    = -1;
   duration    = 0;
   flags       = 0;
   if (FormatCtx == NULL) return (1);
   if (Packet.data != NULL) av_free_packet (&Packet);
   Packet.data = NULL;
   if (av_read_frame (FormatCtx,&Packet) < 0) return (2);
   *p_d_buffer = (byte*)Packet.data;
   d_buf_size  = Packet.size;
   int64_t ts  = Packet.dts;
   if (AudioStream != NULL && Packet.stream_index == AudioTrackList[AudioTrackNo].StreamIndex) {
      flags |= FFMPEG_MDT_AUDIO;
      if (ts != AV_NOPTS_VALUE) {
         if (AudioStream->start_time != AV_NOPTS_VALUE) {
            ts -= AudioStream->start_time;
            if (ts < 0) ts = 0;   
         }
         time_pos = (int)(ts * AudioStream->time_base.num * 1000 / AudioStream->time_base.den);
      }
      if (Packet.convergence_duration != AV_NOPTS_VALUE) duration = (int)(Packet.convergence_duration * AudioStream->time_base.num * 1000 / AudioStream->time_base.den);
   }
   if (SubtitleStream != NULL && Packet.stream_index == SubtitleTrackList[SubtitleTrackNo].StreamIndex) {
      flags |= FFMPEG_MDT_SUBTITLE;
      if (ts != AV_NOPTS_VALUE) {
         if (SubtitleStream->start_time != AV_NOPTS_VALUE) {
            ts -= SubtitleStream->start_time;
            if (ts < 0) ts = 0;   
         }
         time_pos = (int)(ts * SubtitleStream->time_base.num * 1000 / SubtitleStream->time_base.den);
      }
      if (Packet.convergence_duration != AV_NOPTS_VALUE) duration = (int)(Packet.convergence_duration * SubtitleStream->time_base.num * 1000 / SubtitleStream->time_base.den);
   }
   if (VideoStream != NULL && Packet.stream_index == VideoTrackList[VideoTrackNo].StreamIndex) {
      flags |= FFMPEG_MDT_VIDEO;
      if (ts != AV_NOPTS_VALUE) {
         if (VideoStream->start_time != AV_NOPTS_VALUE) {
            ts -= VideoStream->start_time;
            if (ts < 0) ts = 0;   
         }
         time_pos = (int)(ts * VideoStream->time_base.num * 1000 / VideoStream->time_base.den);
      }
      if (Packet.convergence_duration != AV_NOPTS_VALUE) duration = (int)(Packet.convergence_duration * VideoStream->time_base.num * 1000 / VideoStream->time_base.den);
      if (Packet.flags & AV_PKT_FLAG_KEY) flags |= FFMPEG_KEY_FRAME;
   }
   return (DONE);
}

 int FFMPEG_FileReader::ReadAudioTrack (byte **p_d_buffer,int &d_buf_size,int &time_pos,int &duration)
 
{
   *p_d_buffer = NULL;
   d_buf_size  = 0;
   time_pos    = -1;
   duration    = 0;
   if (FormatCtx   == NULL) return (1);
   if (AudioStream == NULL) return (2);
   if (Packet.data != NULL) av_free_packet (&Packet);
   while (TRUE) {
      Packet.data = NULL;
      if (av_read_frame (FormatCtx,&Packet) < 0) return (3); 
      if (Packet.stream_index == AudioTrackList[AudioTrackNo].StreamIndex) break;
      av_free_packet (&Packet);
   }
   *p_d_buffer = (byte*)Packet.data;
   d_buf_size  = Packet.size;
   if (Packet.dts      != AV_NOPTS_VALUE) time_pos = (int)(Packet.dts * AudioStream->time_base.num * 1000 / AudioStream->time_base.den);
   if (Packet.duration != AV_NOPTS_VALUE) duration = (int)(Packet.convergence_duration * AudioStream->time_base.num * 1000 / AudioStream->time_base.den);
   return (DONE);
}

 int FFMPEG_FileReader::ReadSubtitleTrack (byte **p_d_buffer,int &d_buf_size,int &time_pos,int &duration)
 
{
   *p_d_buffer = NULL;
   d_buf_size  = 0;
   time_pos    = -1;
   duration    = 0;
   if (FormatCtx   == NULL) return (1);
   if (SubtitleStream == NULL) return (2);
   if (Packet.data != NULL) av_free_packet (&Packet);
   while (TRUE) {
      Packet.data = NULL;
      if (av_read_frame (FormatCtx,&Packet) < 0) return (3); 
      if (Packet.stream_index == SubtitleTrackList[SubtitleTrackNo].StreamIndex) break;
      av_free_packet (&Packet);
   }
   *p_d_buffer = (byte*)Packet.data;
   d_buf_size  = Packet.size;
   if (Packet.dts      != AV_NOPTS_VALUE) time_pos = (int)(Packet.dts * SubtitleStream->time_base.num * 1000 / SubtitleStream->time_base.den);
   if (Packet.duration != AV_NOPTS_VALUE) duration = (int)(Packet.convergence_duration * SubtitleStream->time_base.num * 1000 / SubtitleStream->time_base.den);
   return (DONE);
}

 int FFMPEG_FileReader::ReadVideoTrack (byte **p_d_buffer,int &d_buf_size,int &time_pos,int &duration,int &flag_key_frame)
 
{
   *p_d_buffer    = NULL;
   d_buf_size     = 0;
   time_pos       = -1;
   duration       = 0;
   flag_key_frame = FALSE;
   if (FormatCtx   == NULL) return (1);
   if (VideoStream == NULL) return (2);
   if (Packet.data != NULL) av_free_packet (&Packet);
   while (TRUE) {
      Packet.data = NULL;
      if (av_read_frame (FormatCtx,&Packet) < 0) return (3);
      if (Packet.stream_index == VideoTrackList[VideoTrackNo].StreamIndex) break;
      av_free_packet (&Packet);
   }
   *p_d_buffer = (byte*)Packet.data;
   d_buf_size  = Packet.size;
   if (Packet.dts != AV_NOPTS_VALUE)
      time_pos = (int)(Packet.dts * VideoStream->time_base.num * 1000 / VideoStream->time_base.den);
   if (Packet.duration != AV_NOPTS_VALUE) duration = (int)(Packet.convergence_duration * VideoStream->time_base.num * 1000 / VideoStream->time_base.den);
   if (Packet.flags & AV_PKT_FLAG_KEY) flag_key_frame = TRUE;
   return (DONE);
}

 void FFMPEG_FileReader::ResetAudioTrackInfo (   )
 
{
   AudioTrackNo  = -1;
   NumChannels   = 0;
   SampleRate    = 0;
   BitsPerSample = 0;
   Flag_FloatSample = FALSE;
   ChannelLayout = 0;
   AudioCodecID  = AV_CODEC_ID_NONE;
   SampleFormat  = AV_SAMPLE_FMT_NONE;
   AudioStream   = NULL;
   AudioCodecCtx = NULL;
   AudioTrackLng.Delete  (   );
   AudioTrackName.Delete (   );
}

 void FFMPEG_FileReader::ResetSubtitleTrackInfo (   )
 
{
   SubtitleTrackNo  = -1;
   SubtitleTimeBase = 0.0;
   SubtitleCodecID  = AV_CODEC_ID_NONE;
   SubtitleStream   = NULL;
   SubtitleCodecCtx = NULL;
   SubtitleTrackLng.Delete  (   );
   SubtitleTrackName.Delete (   );
}

 void FFMPEG_FileReader::ResetVideoTrackInfo (   )
 
{
   VideoTrackNo  = -1;
   FrameWidth    = 0;
   FrameHeight   = 0;
   NumFrames     = 0;
   FrameRate     = 0.0f;
   VideoCodecID  = AV_CODEC_ID_NONE;
   VideoStream   = NULL;
   VideoCodecCtx = NULL;
   VideoTrackName.Delete (   );
}

 int FFMPEG_FileReader::Seek (int time_pos)
 
{
   if (FormatCtx == NULL) return (1);
   int64_t ts = (int64_t)time_pos * AV_TIME_BASE / 1000;
   if (FormatCtx->start_time != AV_NOPTS_VALUE) ts += FormatCtx->start_time;
   if (av_seek_frame (FormatCtx,-1,ts,0) < 0) return (2);
   return (DONE);
}

 int FFMPEG_FileReader::SetAudioTrack (int track_no)
 
{
   if (track_no < 0) {
      ResetAudioTrackInfo (   );
      return (DONE);
   }
   if (FormatCtx == NULL) return (1);
   if (track_no >= AudioTrackList.Length) return (2);
   TrackInfo& ti  = AudioTrackList[track_no];
   AudioTrackNo   = track_no;
   AudioStream    = FormatCtx->streams[ti.StreamIndex];
   AudioCodecCtx  = AudioStream->codec;
   AudioCodecID   = AudioCodecCtx->codec_id;
   NumChannels    = AudioCodecCtx->channels;
   SampleRate     = AudioCodecCtx->sample_rate;
   SampleFormat   = AudioCodecCtx->sample_fmt;
   ChannelLayout  = AudioCodecCtx->channel_layout;
   BitsPerSample  = av_get_bytes_per_sample (SampleFormat) * 8;
   AVSampleFormat smp_fmt = av_get_packed_sample_fmt (SampleFormat);
   if (smp_fmt == AV_SAMPLE_FMT_FLT || smp_fmt == AV_SAMPLE_FMT_DBL) Flag_FloatSample = TRUE;
   else Flag_FloatSample = FALSE;
   if      ((char*)ti.Language    != NULL) AudioTrackLng  = ti.Language;
   if      ((char*)ti.Title       != NULL) AudioTrackName = ti.Title;
   else if ((char*)ti.HandlerName != NULL) AudioTrackName = ti.HandlerName;
   return (DONE);
}

 int FFMPEG_FileReader::SetSubtitleTrack (int track_no)
 
{
   if (track_no < 0) {
      ResetSubtitleTrackInfo (   );
      return (DONE);
   }
   if (FormatCtx == NULL) return (1);
   if (track_no >= SubtitleTrackList.Length) return (2);
   TrackInfo& ti    = SubtitleTrackList[track_no];
   SubtitleTrackNo  = track_no;
   SubtitleStream   = FormatCtx->streams[ti.StreamIndex];
   SubtitleCodecCtx = SubtitleStream->codec;
   SubtitleCodecID  = SubtitleCodecCtx->codec_id;
   SubtitleTimeBase = (double)SubtitleStream->time_base.num / SubtitleStream->time_base.den;
   if (SubtitleCodecCtx->subtitle_header_size) {
      SubtitleHeader.Create (SubtitleCodecCtx->subtitle_header_size + 1);
      memcpy ((char*)SubtitleHeader,SubtitleCodecCtx->subtitle_header,SubtitleCodecCtx->subtitle_header_size);
      SubtitleHeader[SubtitleHeader.Length - 1] = 0;
   }
   else {
      SubtitleHeader.Delete (   );
   }
   if      ((char*)ti.Language    != NULL) SubtitleTrackLng  = ti.Language;
   if      ((char*)ti.Title       != NULL) SubtitleTrackName = ti.Title;
   else if ((char*)ti.HandlerName != NULL) SubtitleTrackName = ti.HandlerName;
   return (DONE);
}

 int FFMPEG_FileReader::SetVideoTrack (int track_no)
 
{
   if (track_no < 0) {
      ResetVideoTrackInfo (   );
      return (DONE);
   }
   if (FormatCtx == NULL) return (1);
   if (track_no >= VideoTrackList.Length) return (2);
   TrackInfo& ti = VideoTrackList[track_no];
   VideoTrackNo  = track_no;
   VideoStream   = FormatCtx->streams[ti.StreamIndex];
   VideoCodecCtx = VideoStream->codec;
   VideoCodecID  = VideoCodecCtx->codec_id;
   FrameWidth    = VideoCodecCtx->width;
   FrameHeight   = VideoCodecCtx->height;
   NumFrames     = (int)VideoStream->nb_frames;
   //FrameRate     = (float)VideoStream->r_frame_rate.num / VideoStream->r_frame_rate.den;
   AVRational fr = av_guess_frame_rate(FormatCtx, VideoStream, NULL);
   FrameRate = (float)av_q2d(fr);
   if      ((char*)ti.Title       != NULL) VideoTrackName = ti.Title;
   else if ((char*)ti.HandlerName != NULL) VideoTrackName = ti.HandlerName;
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: FFMPEG_FileWriter
//
///////////////////////////////////////////////////////////////////////////////

 FFMPEG_FileWriter::FFMPEG_FileWriter (   )
 
{
   _Init (   );
}

 FFMPEG_FileWriter::~FFMPEG_FileWriter (   )
 
{
   Close (   );
}

 void FFMPEG_FileWriter::_Init (   )
  
{
   FileHandle       = -1;
   IOBuffer         = NULL;
   Flag_HeaderWritten = FALSE;
   FormatCtx        = NULL;
}

 int FFMPEG_FileWriter::AddAudioTrack (AVCodecID codec_id,int n_channels,int sample_rate,AVSampleFormat sample_fmt,uint64_t channel_layout)
 
{
   if (FormatCtx == NULL) return (-1);
   AVStream *stream = avformat_new_stream (FormatCtx,NULL);
   if (stream == NULL) return (-2);
   AVCodecContext *codec_ctx = stream->codec;
   codec_ctx->codec_type  = AVMEDIA_TYPE_AUDIO;
   codec_ctx->codec_id    = codec_id;
   codec_ctx->channels    = n_channels;
   codec_ctx->sample_rate = sample_rate;
   codec_ctx->sample_fmt  = sample_fmt;
   if (channel_layout) codec_ctx->channel_layout = channel_layout;
   else codec_ctx->channel_layout = av_get_default_channel_layout (n_channels);
   if (FormatCtx->oformat->flags & AVFMT_GLOBALHEADER) codec_ctx->flags |= CODEC_FLAG_GLOBAL_HEADER;
   return (stream->index);
}

 int FFMPEG_FileWriter::AddVideoTrack (AVCodecID codec_id,int frm_width,int frm_height,float frm_rate)
 
{
   if (FormatCtx == NULL) return (-1);
   AVStream *stream = avformat_new_stream (FormatCtx,NULL);
   if (stream == NULL) return (-2);
   AVCodecContext *codec_ctx = stream->codec;
   codec_ctx->codec_type = AVMEDIA_TYPE_VIDEO;
   codec_ctx->codec_id   = codec_id;
   codec_ctx->width      = frm_width;
   codec_ctx->height     = frm_height;
   if (!strcmp (FormatCtx->oformat->name,"avi")) {
      codec_ctx->time_base.num = 100;
      codec_ctx->time_base.den = (int)(frm_rate * 100.0f);
   }
   else {
      // AVI 파일 포맷에서는 time_base.num이 1이 아니어도 상관 없으나, 다른 파일 포맷에서는
      // time_base.num이 1이 아니면 Frame Rate가 제대로 기록되지 않는 경우가 있다.
      codec_ctx->time_base.num  = 1;
      codec_ctx->time_base.den  = (int)(frm_rate + 0.5f);
   }
   if (FormatCtx->oformat->flags & AVFMT_GLOBALHEADER) codec_ctx->flags |= CODEC_FLAG_GLOBAL_HEADER;
   return (stream->index);
}

 void FFMPEG_FileWriter::Close (   )

{
   if (FormatCtx) {
      if (Flag_HeaderWritten) av_write_trailer (FormatCtx);
      if (FormatCtx->pb) {
         if (FileHandle != -1) av_free (FormatCtx->pb);
         else avio_close (FormatCtx->pb);
      }
   }
   //if (FileHandle != -1) _close (FileHandle);
   if (IOBuffer) av_free (IOBuffer);
   _Init (   );
}

 AVCodecContext* FFMPEG_FileWriter::GetAVCodecContext (int track_idx)
 
{
   if (track_idx < 0 || track_idx >= (int)FormatCtx->nb_streams) return (NULL);
   AVStream *stream = FormatCtx->streams[track_idx];
   return (stream->codec);
}

 double FFMPEG_FileWriter::GetCurTimePos (int track_idx)
 
{
   if (FormatCtx == NULL) return (-1.0);
   if (track_idx < 0 || track_idx >= (int)FormatCtx->nb_streams) return (-1.0);
   AVStream* stream = FormatCtx->streams[track_idx];
   return (stream->pts.val * av_q2d(stream->time_base));
}

 int FFMPEG_FileWriter::GetFramePos (int track_idx,int time_pos)
 
{
   if (track_idx < 0 || track_idx >= (int)FormatCtx->nb_streams) return (-1);
   AVStream *stream = FormatCtx->streams[track_idx];
   AVCodecContext *codec_ctx = stream->codec;
   int frame_pos = (int)((int64_t)time_pos * codec_ctx->time_base.den / (codec_ctx->time_base.num * 1000));
   return (frame_pos);
}

 int FFMPEG_FileWriter::Open (const char *file_name)
 
{
   Close (   );
   FormatCtx = avformat_alloc_context (   );
   if (FormatCtx == NULL) return (1);
   AVOutputFormat *fmt = av_guess_format (NULL,file_name,NULL);
   if (fmt == NULL || (fmt->flags & AVFMT_NOFILE)) {
      Close (   );
      return (2);
   }
   FormatCtx->oformat = fmt;
   if (avio_open (&FormatCtx->pb,file_name,AVIO_FLAG_WRITE) < 0) {
      Close (   );
      return (3);
   }
   return (DONE);
}

 int FFMPEG_FileWriter::WriteHeader (   )
 
{
   if (!FormatCtx) return (1);
   if (!FormatCtx->pb) return (2);
   if (Flag_HeaderWritten) return (3);
   if (avformat_write_header (FormatCtx,NULL) < 0) {
      Close (   );
      return (4);
   }
   Flag_HeaderWritten = TRUE;
   return (DONE);
}

 int FFMPEG_FileWriter::Write (int track_idx,byte *s_buffer,int s_buf_size,int flag_key_frame,int64_t frame_pos)
 
{
   if (!Flag_HeaderWritten) return (1);
   if (track_idx >= (int)FormatCtx->nb_streams) return (2);
   if (!s_buf_size) return (DONE);
   AVPacket pkt;
   av_init_packet (&pkt);
   pkt.stream_index = track_idx;
   pkt.data         = s_buffer;
   pkt.size         = s_buf_size;
   pkt.pts          = frame_pos;
   if (flag_key_frame) pkt.flags |= AV_PKT_FLAG_KEY;
   if (av_interleaved_write_frame (FormatCtx,&pkt) < 0) return (3);
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 static void _FFMPEG_Log (void* ptr,int level,const char* fmt,va_list vl)

{
   if (!_FFMPEG_LogOutput) return;
   static int print_prefix = 1;
   char line[512];
   av_log_format_line (ptr,level,fmt,vl,line,sizeof(line),&print_prefix);
   logd (line);
}

 void CloseFFMPEG (   )

{
   if (_CS_FFMPEG != NULL) {
      delete _CS_FFMPEG;
      _CS_FFMPEG = NULL;
   }
}
 
 void InitFFMPEG (   )
 
{
   if (_CS_FFMPEG == NULL) _CS_FFMPEG = new CCriticalSection;
   FFMPEG_CS_Lock (   );
   av_log_set_callback (_FFMPEG_Log);
   av_register_all      (   );
   avcodec_register_all (   );
   FFMPEG_CS_Unlock (   );
}

 int FFMPEG_CS_Lock (   )
 
{
   if (_CS_FFMPEG == NULL) return (1);
   _CS_FFMPEG->Lock (   );
   return (DONE);   
}

 int FFMPEG_CS_Unlock (   )
 
{
   if (_CS_FFMPEG == NULL) return (1);
   _CS_FFMPEG->Unlock (   );
   return (DONE);
}

 void FFMPEG_EnableLogOutput (int flag_enable)

{
   _FFMPEG_LogOutput = flag_enable;
}

}

#endif // __LIB_FFMPEG

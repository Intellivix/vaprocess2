#include "bccl_mem.h"

 namespace BCCL

{

void (*__MemAllocErrorHandler)(   ) = NULL;

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 void ClearArray1D (byte *s_array,int length,int item_size)

{
   int n_bytes;
   
   n_bytes = length * item_size;
   if (n_bytes) memset (s_array,0,n_bytes);
}

 void ClearArray2D (byte **s_array,int width,int height,int item_size)

{
   int n_bytes;

   n_bytes = width * height * item_size;
   if (n_bytes) memset (s_array[0],0,n_bytes);
}

 void ClearArray3D (byte ***s_array,int width,int height,int depth,int item_size)

{
   int n_bytes;

   n_bytes = width * height * depth * item_size;
   if (n_bytes) memset (s_array[0][0],0,n_bytes);
}

 void CopyArray1D (byte *s_array,int sx,int length,int item_size,byte *d_array,int dx,int option)

{
   int i,j,k;
   
   sx *= item_size;
   dx *= item_size;
   length *= item_size;
   if (option == CP_NORMAL) memcpy (d_array + dx,s_array + sx,length);
   else {
      int ei = sx + length;
      for (i = sx,j = dx + item_size - 1; i < ei; j += item_size) {
         int end = i + item_size;
         for (k = j; i < end; i++,k--) d_array[k] = s_array[i];
      }
   }
}

 void CopyArray2D (byte **s_array,int sx,int sy,int width,int height,int item_size,byte **d_array,int dx,int dy,int option)

{
   int i,j,k,l,m;

   sx *= item_size;
   dx *= item_size;
   width *= item_size;
   int ei = sy + height;
   int ej = sx + width; 
   if (option == CP_NORMAL) {
      for (i = sy,k = dy; i < ei; i++,k++) {
         byte *_RESTRICT l_s_array = s_array[i];
         byte *_RESTRICT l_d_array = d_array[k];
         for (j = sx,l = dx; j < ej; j++,l++) 
            l_d_array[l] = l_s_array[j];
      }
   }
   else {
      for (i = sy,k = dy; i < ei; i++,k++) {
         byte *_RESTRICT l_s_array = s_array[i];
         byte *_RESTRICT l_d_array = d_array[k];
         for (j = sx,l = dx + item_size - 1; j < ej; l += item_size) {
            int end = j + item_size;
            for (m = l; j < end; j++,m--) l_d_array[m] = l_s_array[j];
         }
      }
   }
}

 void CopyArray3D (byte ***s_array,int sx,int sy,int sz,int width,int height,int depth,int item_size,byte ***d_array,int dx,int dy,int dz,int option)

{
   int i,j,k,l,m,n,o;

   sx *= item_size;
   dx *= item_size;
   width *= item_size;
   int ei = sz + depth;
   int ej = sy + height;
   int ek = sx + width;
   if (option == CP_NORMAL) {
      for (i = sz,l = dz; i < ei; i++,l++)
         for (j = sy,m = dy; j < ej; j++,m++)
            for (k = sx,n = dx; k < ek; k++,n++)
               d_array[l][m][n] = s_array[i][j][k];
   }
   else {
      for (i = sz,l = dz; i < ei; i++,l++) {
        for (j = sy,m = dy; j < ej; j++,m++) {
          for (k = sx,n = dx + item_size - 1; k < ek; n += item_size) {
               int end = k + item_size;
               for (o = n; k < end; k++,o--) d_array[l][m][o] = s_array[i][j][k];
            }
         }
      }
   }
}

 byte *CreateArray1D (int length,int item_size)

{
   if (length == 0) return (NULL);
   int n_bytes = length * item_size + 3;
   byte *array = (byte *)malloc (n_bytes);
   return (array);
}

 byte **CreateArray2D (int width,int height,int item_size)

{
   int  i;

   if (width == 0 || height == 0) return (NULL);
   byte **array2d = (byte **)CreateArray1D (height,sizeof(byte*));
   byte * array1d = (byte  *)CreateArray1D (width * height,item_size);
   int step = width * item_size;
   for (i = 0; i < height; i++,array1d += step) array2d[i] = array1d;
   return (array2d);
}
   
 byte **CreateArray2D (byte *array1d,int width,int height,int item_size)
 
{
   int i;

   if (width == 0 || height == 0) return (NULL);
   byte **array2d = (byte **)CreateArray1D (height,sizeof(byte*));
   int step = width * item_size;
   for (i = 0; i < height; i++,array1d += step) array2d[i] = array1d;
   return (array2d);
}

 byte ***CreateArray3D (int width,int height,int depth,int item_size)

{
   int  i,j;
   
   if (width == 0 || height == 0 || depth == 0) return (NULL);   
   byte ***array3d = (byte ***)CreateArray1D (depth,sizeof(byte**));
   byte ** array2d = (byte  **)CreateArray1D (depth * height,sizeof(byte*));
   byte *  array1d = (byte   *)CreateArray1D (depth * height * width,item_size);
   for (i = 0; i < depth; i++,array2d += height) array3d[i] = array2d;
   int step = width * item_size;
   for (i = 0; i < depth; i++) {
      for (j = 0; j < height; j++) {
         array3d[i][j] = array1d;
         array1d += step;
      }
   }
   return (array3d);
}

 byte ***CreateArray3D (byte *array1d,int width,int height,int depth,int item_size)
 
{
   int i,j;
   
   if (width == 0 || height == 0 || depth == 0) return (NULL);
   byte ***array3d = (byte ***)CreateArray1D (depth,sizeof(byte **));
   byte ** array2d = (byte  **)CreateArray1D (depth * height,sizeof(byte *));
   for (i = 0; i < depth; i++,array2d += height) array3d[i] = array2d;
   int step = width * item_size;
   for (i = 0; i < depth; i++) {
      for (j = 0; j < height; j++) {
         array3d[i][j] = array1d;
         array1d += step;
      }
   }
   return (array3d);
}

 void DeleteArray1D (byte *s_array)

{
   if (s_array == NULL) return;
   free (s_array);
}

 void DeleteArray2D (byte **s_array)

{
   if (s_array == NULL) return;
   DeleteArray1D (s_array[0]);
   DeleteArray1D ((byte *)s_array);
}

 void DeleteArray3D (byte ***s_array)

{
   if (s_array == NULL) return;
   DeleteArray1D (s_array[0][0]);
   DeleteArray1D ((byte *)(s_array[0]));
   DeleteArray1D ((byte *)s_array);
}

}

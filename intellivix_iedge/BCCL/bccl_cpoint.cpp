#include "bccl_cpoint.h"

#if !defined(__WIN32)

#ifndef GET_X_LPARAM
#define GET_X_LPARAM(lp)                        ((int)(short)LOWORD(lp))
#endif
#ifndef GET_Y_LPARAM
#define GET_Y_LPARAM(lp)                        ((int)(short)HIWORD(lp))
#endif

BOOL CopyRect(LPRECT lprcDst, CONST RECT *lprcSrc)
{
   lprcDst->left   = lprcSrc->left;
   lprcDst->top    = lprcSrc->top;
   lprcDst->right  = lprcSrc->right;
   lprcDst->bottom = lprcSrc->bottom;
   
   return TRUE;
}

BOOL IsRectEmpty(CONST RECT *lprc)
{
   if (0 == lprc->left && 0 == lprc->top && 0 == lprc->right && 0 == lprc->bottom)
      return TRUE;
   
   return FALSE;
}

BOOL PtInRect(CONST RECT *lprc, POINT pt)
{
   return FALSE;
}

BOOL SetRect(LPRECT lprc, int xLeft, int yTop, int xRight, int yBottom)
{
   lprc->left   = xLeft;
   lprc->top    = yTop;
   lprc->right  = xRight;
   lprc->bottom = yBottom;
   
   return TRUE;
}

BOOL SetRectEmpty(LPRECT lprc)
{
   lprc->left   = 0;
   lprc->top    = 0;
   lprc->right  = 0;
   lprc->bottom = 0;
   
   return TRUE;
}

BOOL EqualRect(CONST RECT *lprc1, CONST RECT *lprc2)
{
   return FALSE;
}

BOOL InflateRect(LPRECT lprc, int dx, int dy)
{
   return FALSE;
}

BOOL OffsetRect(LPRECT lprc, int dx, int dy)
{
   return FALSE;
}

BOOL IntersectRect(LPRECT lprcDst, CONST RECT *lprcSrc1, CONST RECT *lprcSrc2)
{
   return FALSE;
}

BOOL UnionRect(LPRECT lprcDst, CONST RECT *lprcSrc1, CONST RECT *lprcSrc2)
{
   return FALSE;
}

BOOL SubtractRect(LPRECT lprcDst, CONST RECT *lprcSrc1, CONST RECT *lprcSrc2)
{
   return FALSE;
}

int  MulDiv(int nNumber, int nNumerator, int nDenominator)
{
   return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// CPoint
/////////////////////////////////////////////////////////////////////////////

CPoint::CPoint() throw()
{
   x = 0;
   y = 0;
}

CPoint::CPoint(
   _In_ int initX,
   _In_ int initY) throw()
{
   x = initX;
   y = initY;
}

CPoint::CPoint(_In_ POINT initPt) throw()
{
   *(POINT*)this = initPt;
}

CPoint::CPoint(_In_ SIZE initSize) throw()
{
   *(SIZE*)this = initSize;
}

CPoint::CPoint(_In_ LPARAM dwPoint) throw()
{
   x = (short)GET_X_LPARAM(dwPoint);
   y = (short)GET_Y_LPARAM(dwPoint);
}

void CPoint::Offset(
   _In_ int xOffset,
   _In_ int yOffset) throw()
{
   x += xOffset;
   y += yOffset;
}

void CPoint::Offset(_In_ POINT point) throw()
{
   x += point.x;
   y += point.y;
}

void CPoint::Offset(_In_ SIZE size) throw()
{
   x += size.cx;
   y += size.cy;
}

void CPoint::SetPoint(
   _In_ int X,
   _In_ int Y) throw()
{
   x = X;
   y = Y;
}

BOOL CPoint::operator==(_In_ POINT point) const throw()
{
   return (x == point.x && y == point.y);
}

BOOL CPoint::operator!=(_In_ POINT point) const throw()
{
   return (x != point.x || y != point.y);
}

void CPoint::operator+=(_In_ SIZE size) throw()
{
   x += size.cx;
   y += size.cy;
}

void CPoint::operator-=(_In_ SIZE size) throw()
{
   x -= size.cx;
   y -= size.cy;
}

void CPoint::operator+=(_In_ POINT point) throw()
{
   x += point.x;
   y += point.y;
}

void CPoint::operator-=(_In_ POINT point) throw()
{
   x -= point.x;
   y -= point.y;
}

CPoint CPoint::operator+(_In_ SIZE size) const throw()
{
   return CPoint(x + size.cx, y + size.cy);
}

CPoint CPoint::operator-(_In_ SIZE size) const throw()
{
   return CPoint(x - size.cx, y - size.cy);
}

CPoint CPoint::operator-() const throw()
{
   return CPoint(-x, -y);
}

CPoint CPoint::operator+(_In_ POINT point) const throw()
{
   return CPoint(x + point.x, y + point.y);
}

CSize CPoint::operator-(_In_ POINT point) const throw()
{
   return CSize(x - point.x, y - point.y);
}

CRect CPoint::operator+(_In_ const RECT* lpRect) const throw()
{
   return CRect(lpRect) + *this;
}

CRect CPoint::operator-(_In_ const RECT* lpRect) const throw()
{
   return CRect(lpRect) - *this;
}

/////////////////////////////////////////////////////////////////////////////
// CRect
/////////////////////////////////////////////////////////////////////////////

CRect::CRect() throw()
{
   left = 0;
   top = 0;
   right = 0;
   bottom = 0;
}

CRect::CRect(
   _In_ int l,
   _In_ int t,
   _In_ int r,
   _In_ int b) throw()
{
   left = l;
   top = t;
   right = r;
   bottom = b;
}

CRect::CRect(_In_ const RECT& srcRect) throw()
{
   ::CopyRect(this, &srcRect);
}

CRect::CRect(_In_ LPCRECT lpSrcRect) throw()
{
   ::CopyRect(this, lpSrcRect);
}

CRect::CRect(
   _In_ POINT point,
   _In_ SIZE size) throw()
{
   right = (left = point.x) + size.cx;
   bottom = (top = point.y) + size.cy;
}

CRect::CRect(
   _In_ POINT topLeft,
   _In_ POINT bottomRight) throw()
{
   left = topLeft.x;
   top = topLeft.y;
   right = bottomRight.x;
   bottom = bottomRight.y;
}

int CRect::Width() const throw()
{
   return right - left;
}

int CRect::Height() const throw()
{
   return bottom - top;
}

CSize CRect::Size() const throw()
{
   return CSize(right - left, bottom - top);
}

CPoint& CRect::TopLeft() throw()
{
   return *((CPoint*)this);
}

CPoint& CRect::BottomRight() throw()
{
   return *((CPoint*)this+1);
}

const CPoint& CRect::TopLeft() const throw()
{
   return *((CPoint*)this);
}

const CPoint& CRect::BottomRight() const throw()
{
   return *((CPoint*)this+1);
}

CPoint CRect::CenterPoint() const throw()
{
   return CPoint((left+right)/2, (top+bottom)/2);
}

void CRect::SwapLeftRight() throw()
{
   SwapLeftRight(LPRECT(this));
}

void WINAPI CRect::SwapLeftRight(_Inout_ LPRECT lpRect) throw()
{
   LONG temp = lpRect->left;
   lpRect->left = lpRect->right;
   lpRect->right = temp;
}

CRect::operator LPRECT() throw()
{
   return this;
}

CRect::operator LPCRECT() const throw()
{
   return this;
}

BOOL CRect::IsRectEmpty() const throw()
{
   return ::IsRectEmpty(this);
}

BOOL CRect::IsRectNull() const throw()
{
   return (left == 0 && right == 0 && top == 0 && bottom == 0);
}

BOOL CRect::PtInRect(_In_ POINT point) const throw()
{
   return ::PtInRect(this, point);
}

void CRect::SetRect(
   _In_ int x1,
   _In_ int y1,
   _In_ int x2,
   _In_ int y2) throw()
{
   ::SetRect(this, x1, y1, x2, y2);
}

void CRect::SetRect(
   _In_ POINT topLeft,
   _In_ POINT bottomRight) throw()
{
   ::SetRect(this, topLeft.x, topLeft.y, bottomRight.x, bottomRight.y);
}

void CRect::SetRectEmpty() throw()
{
   ::SetRectEmpty(this);
}

void CRect::CopyRect(_In_ LPCRECT lpSrcRect) throw()
{
   ::CopyRect(this, lpSrcRect);
}

BOOL CRect::EqualRect(_In_ LPCRECT lpRect) const throw()
{
   return ::EqualRect(this, lpRect);
}

void CRect::InflateRect(
   _In_ int x,
   _In_ int y) throw()
{
   ::InflateRect(this, x, y);
}

void CRect::InflateRect(SIZE size) throw()
{
   ::InflateRect(this, size.cx, size.cy);
}

void CRect::DeflateRect(
   _In_ int x,
   _In_ int y) throw()
{
   ::InflateRect(this, -x, -y);
}

void CRect::DeflateRect(_In_ SIZE size) throw()
{
   ::InflateRect(this, -size.cx, -size.cy);
}

void CRect::OffsetRect(
   _In_ int x,
   _In_ int y) throw()
{
   ::OffsetRect(this, x, y);
}

void CRect::OffsetRect(_In_ POINT point) throw()
{
   ::OffsetRect(this, point.x, point.y);
}

void CRect::OffsetRect(_In_ SIZE size) throw()
{
   ::OffsetRect(this, size.cx, size.cy);
}

void CRect::MoveToY(_In_ int y) throw()
{
   bottom = Height() + y;
   top = y;
}

void CRect::MoveToX(_In_ int x) throw()
{
   right = Width() + x;
   left = x;
}

void CRect::MoveToXY(
   _In_ int x,
   _In_ int y) throw()
{
   MoveToX(x);
   MoveToY(y);
}

void CRect::MoveToXY(_In_ POINT pt) throw()
{
   MoveToX(pt.x);
   MoveToY(pt.y);
}

BOOL CRect::IntersectRect(
   _In_ LPCRECT lpRect1,
   _In_ LPCRECT lpRect2) throw()
{
   return ::IntersectRect(this, lpRect1, lpRect2);
}

BOOL CRect::UnionRect(
   _In_ LPCRECT lpRect1,
   _In_ LPCRECT lpRect2) throw()
{
   return ::UnionRect(this, lpRect1, lpRect2);
}

void CRect::operator=(_In_ const RECT& srcRect) throw()
{
   ::CopyRect(this, &srcRect);
}

BOOL CRect::operator==(_In_ const RECT& rect) const throw()
{
   return ::EqualRect(this, &rect);
}

BOOL CRect::operator!=(_In_ const RECT& rect) const throw()
{
   return !::EqualRect(this, &rect);
}

void CRect::operator+=(_In_ POINT point) throw()
{
   ::OffsetRect(this, point.x, point.y);
}

void CRect::operator+=(_In_ SIZE size) throw()
{
   ::OffsetRect(this, size.cx, size.cy);
}

void CRect::operator+=(_In_ LPCRECT lpRect) throw()
{
   InflateRect(lpRect);
}

void CRect::operator-=(_In_ POINT point) throw()
{
   ::OffsetRect(this, -point.x, -point.y);
}

void CRect::operator-=(_In_ SIZE size) throw()
{
   ::OffsetRect(this, -size.cx, -size.cy);
}

void CRect::operator-=(_In_ LPCRECT lpRect) throw()
{
   DeflateRect(lpRect);
}

void CRect::operator&=(_In_ const RECT& rect) throw()
{
   ::IntersectRect(this, this, &rect);
}

void CRect::operator|=(_In_ const RECT& rect) throw()
{
   ::UnionRect(this, this, &rect);
}

CRect CRect::operator+(_In_ POINT pt) const throw()
{
   CRect rect(*this);
   ::OffsetRect(&rect, pt.x, pt.y);
   return rect;
}

CRect CRect::operator-(_In_ POINT pt) const throw()
{
   CRect rect(*this);
   ::OffsetRect(&rect, -pt.x, -pt.y);
   return rect;
}

CRect CRect::operator+(_In_ SIZE size) const throw()
{
   CRect rect(*this);
   ::OffsetRect(&rect, size.cx, size.cy);
   return rect;
}

CRect CRect::operator-(_In_ SIZE size) const throw()
{
   CRect rect(*this);
   ::OffsetRect(&rect, -size.cx, -size.cy);
   return rect;
}

CRect CRect::operator+(_In_ LPCRECT lpRect) const throw()
{
   CRect rect(this);
   rect.InflateRect(lpRect);
   return rect;
}

CRect CRect::operator-(_In_ LPCRECT lpRect) const throw()
{
   CRect rect(this);
   rect.DeflateRect(lpRect);
   return rect;
}

CRect CRect::operator&(_In_ const RECT& rect2) const throw()
{
   CRect rect;
   ::IntersectRect(&rect, this, &rect2);
   return rect;
}

CRect CRect::operator|(_In_ const RECT& rect2) const throw()
{
   CRect rect;
   ::UnionRect(&rect, this, &rect2);
   return rect;
}

BOOL CRect::SubtractRect(
   _In_ LPCRECT lpRectSrc1,
   _In_ LPCRECT lpRectSrc2) throw()
{
   return ::SubtractRect(this, lpRectSrc1, lpRectSrc2);
}

void CRect::NormalizeRect() throw()
{
   int nTemp;
   if (left > right)
   {
      nTemp = left;
      left = right;
      right = nTemp;
   }
   if (top > bottom)
   {
      nTemp = top;
      top = bottom;
      bottom = nTemp;
   }
}

void CRect::InflateRect(_In_ LPCRECT lpRect) throw()
{
   left -= lpRect->left;
   top -= lpRect->top;
   right += lpRect->right;
   bottom += lpRect->bottom;
}

void CRect::InflateRect(
   _In_ int l,
   _In_ int t,
   _In_ int r,
   _In_ int b) throw()
{
   left -= l;
   top -= t;
   right += r;
   bottom += b;
}

void CRect::DeflateRect(_In_ LPCRECT lpRect) throw()
{
   left += lpRect->left;
   top += lpRect->top;
   right -= lpRect->right;
   bottom -= lpRect->bottom;
}

void CRect::DeflateRect(
   _In_ int l,
   _In_ int t,
   _In_ int r,
   _In_ int b) throw()
{
   left += l;
   top += t;
   right -= r;
   bottom -= b;
}

CRect CRect::MulDiv(
   _In_ int nMultiplier,
   _In_ int nDivisor) const throw()
{
   return CRect(
      ::MulDiv(left, nMultiplier, nDivisor),
      ::MulDiv(top, nMultiplier, nDivisor),
      ::MulDiv(right, nMultiplier, nDivisor),
      ::MulDiv(bottom, nMultiplier, nDivisor));
}

/////////////////////////////////////////////////////////////////////////////
// CSize
/////////////////////////////////////////////////////////////////////////////

CSize::CSize() throw()
{
   cx = 0;
   cy = 0;
}

CSize::CSize(
   _In_ int initCX,
   _In_ int initCY) throw()
{
   cx = initCX;
   cy = initCY;
}

CSize::CSize(_In_ SIZE initSize) throw()
{
   *(SIZE*)this = initSize;
}

CSize::CSize(_In_ POINT initPt) throw()
{
   *(POINT*)this = initPt;
}

CSize::CSize(_In_ DWORD dwSize) throw()
{
      cx = (short)LOWORD(dwSize);
      cy = (short)HIWORD(dwSize);
}

BOOL CSize::operator==(_In_ SIZE size) const throw()
{
   return (cx == size.cx && cy == size.cy);
}

BOOL CSize::operator!=(_In_ SIZE size) const throw()
{
   return (cx != size.cx || cy != size.cy);
}

void CSize::operator+=(_In_ SIZE size) throw()
{
   cx += size.cx;
   cy += size.cy;
}

void CSize::operator-=(_In_ SIZE size) throw()
{
   cx -= size.cx;
   cy -= size.cy;
}

void CSize::SetSize(
   _In_ int CX,
   _In_ int CY) throw()
{
   cx = CX;
   cy = CY;
}

CSize CSize::operator+(_In_ SIZE size) const throw()
{
   return CSize(cx + size.cx, cy + size.cy);
}

CSize CSize::operator-(_In_ SIZE size) const throw()
{
   return CSize(cx - size.cx, cy - size.cy);
}

CSize CSize::operator-() const throw()
{
   return CSize(-cx, -cy);
}

CPoint CSize::operator+(_In_ POINT point) const throw()
{
   return CPoint(cx + point.x, cy + point.y);
}

CPoint CSize::operator-(_In_ POINT point) const throw()
{
   return CPoint(cx - point.x, cy - point.y);
}

CRect CSize::operator+(_In_ const RECT* lpRect) const throw()
{
   return CRect(lpRect) + *this;
}

CRect CSize::operator-(_In_ const RECT* lpRect) const throw()
{
   return CRect(lpRect) - *this;
}

#endif

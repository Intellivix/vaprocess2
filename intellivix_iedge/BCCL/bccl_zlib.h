#if !defined(__BCCL_ZLIB_H)
#define __BCCL_ZLIB_H

#include "bccl_define.h"

#if defined(__LIB_ZLIB)

#include "bccl_define.h"
extern "C" {
#include "zlib.h"
};

 namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: ZL_Compression
//
///////////////////////////////////////////////////////////////////////////////

 class ZL_Compression

{
   protected:
      int      Flag_Initialized;
      z_stream Stream;
      
    public:
      int CompressionLevel; // Range: 0 ~ 9 (0: No compression)
      
   public:
      ZL_Compression (   );
     ~ZL_Compression (   );
   
   protected:
      void _Init (   );
   
   public:
      void Close           (   );
      int  Initialize      (   );
      int  Perform         (byte *d_buffer,int d_buf_size,int &d_buf_size_used,int flag_finish = TRUE);
      void SetSourceBuffer (byte *s_buffer,int s_buf_size);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: ZL_Decompression
//
///////////////////////////////////////////////////////////////////////////////

 class ZL_Decompression
 
{
   protected:
      int      Flag_Initialized;
      z_stream Stream;
      
   public:
      ZL_Decompression (   );
     ~ZL_Decompression (   );
     
   protected:
      void _Init (   );
   
   public:
      void Close           (   );
      int  Initialize      (   );
      int  Perform         (byte *d_buffer,int d_buf_size,int &size_used);
      void SetSourceBuffer (byte *s_buffer,int s_buf_size);
};

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

int ZL_Compress   (byte *s_buffer,int s_buf_size,byte *d_buffer,int d_buf_size);
int ZL_Decompress (byte *s_buffer,int s_buf_size,byte *d_buffer,int d_buf_size);

}

#endif

#endif


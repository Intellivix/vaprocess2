#if !defined(__BCCL_MEM_H)
#define __BCCL_MEM_H

#include "bccl_define.h"

 namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// Definitions
//
///////////////////////////////////////////////////////////////////////////////

#define CP_NORMAL    0
#define CP_REVERSE   1

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

void ClearArray1D     (byte *s_array,int length,int item_size);
void ClearArray2D     (byte **s_array,int width,int height,int item_size);
void ClearArray3D     (byte ***s_array,int width,int height,int depth,int item_size);
void CopyArray1D      (byte *s_array,int sx,int length,int item_size,byte *d_array,int dx,int option = CP_NORMAL);
void CopyArray2D      (byte **s_array,int sx,int sy,int width,int height,int item_size,byte **d_array,int dx,int dy,int option = CP_NORMAL);
void CopyArray3D      (byte ***s_array,int sx,int sy,int sz,int width,int height,int depth,int item_size,byte ***d_array,int dx,int dy,int dz,int option = CP_NORMAL);
byte *CreateArray1D   (int length,int item_size);
byte **CreateArray2D  (int width,int height,int item_size);
byte **CreateArray2D  (byte *array1d,int width,int height,int item_size);
byte ***CreateArray3D (int width,int height,int depth,int item_size);
byte ***CreateArray3D (byte *array1d,int width,int height,int depth,int item_size);
void DeleteArray1D    (byte *s_array);
void DeleteArray2D    (byte **s_array);
void DeleteArray3D    (byte ***s_array);

}

#endif

#include "bccl_pugixml.h"

#include "bccl_plog.h"

namespace BCCL {

const char* PugiXmlHelper::g_pugi_node_types[] = { "null", "document", "element", "pcdata", "cdata", "comment", "pi", "declaration" };

//////////////////////////////////////////////////
/// PugiXmlNodeCache
//////////////////////////////////////////////////

PugiXmlNodeCache::PugiXmlNodeCache( pugi::xml_node& xml_node_from )
{
   pugi::xml_node::operator=( xml_node_from );
}

pugi::xml_node PugiXmlNodeCache::GetChildUnRead( const std::string& child_name )
{
   auto it = m_cache_nodes.find( child_name );
   if( it == m_cache_nodes.end() ) {
      // Not found, means first time iteration for children nodes.
      pugi::xml_node child      = pugi::xml_node::child( child_name.c_str() );
      m_cache_nodes[child_name] = child;
      return child;
   }
   // Found. previously sat by SetChildReadDone().
   return it->second;
}

void PugiXmlNodeCache::SetChildReadDone( const std::string& child_name, pugi::xml_node& node )
{
   // First, find the next sibling by node's name, and then, set the xml_node into m_cache_nodes.
   // The xml_node could be empty one, which means no more to read when next GetChildUnRead() called.
   m_cache_nodes[child_name] = node.next_sibling( child_name.c_str() );
}

//////////////////////////////////////////////////
/// PugiXmlHelper
//////////////////////////////////////////////////

PugiXmlHelper::PugiXmlHelper()
{
   AddDecaration();
}

void PugiXmlHelper::PushParent( pugi::xml_node& xml_node )
{
   m_parent_node.push( xml_node );
}

void PugiXmlHelper::PopParent()
{
   m_parent_node.pop();
}

PugiXmlNodeCache& PugiXmlHelper::GetParent()
{
   return m_parent_node.top();
}

void PugiXmlHelper::AddDecaration()
{
   pugi::xml_node decl                 = m_xmlDocument.prepend_child( pugi::node_declaration );
   decl.append_attribute( "version" )  = "1.0";
   decl.append_attribute( "encoding" ) = "UTF-8";
}

bool PugiXmlHelper::ParseFromFile( const char* filename, uint parse_options, pugi::xml_encoding encoding )
{
   pugi::xml_parse_result result = m_xmlDocument.load_file( filename, parse_options, encoding );
   if( result.status != pugi::status_ok ) {
      LOGE << "pugi doc failed loaded and parsed (directly) from file[" << filename
           << "] #pugi_errCode[" << result.status
           << "] desc[" << result.description() << "]";
      return false;
   }
   return false;
}

bool PugiXmlHelper::ParseFromMemory( const char* source, uint size, uint parse_options, pugi::xml_encoding encoding )
{
   // Pugi takes care of m_pugi_buffer and delete it, when m_xmlDocument's life cycles ends.
   m_pugi_buffer = static_cast<char*>( pugi::get_memory_allocation_function()( size ) );
   memcpy( m_pugi_buffer, source, size );

   pugi::xml_parse_result result = m_xmlDocument.load_buffer_inplace_own( m_pugi_buffer, size, parse_options, encoding );
   if( result.status != pugi::status_ok ) {
      LOGE << "pugi doc failed to load and parse from mem(fileIO). #code[" << result.status
           << "] #desc[" << result.description()
           << "] <<< source start >>>\n"
           << source << "\n<<< source end >>>";
      return false;
   }
#ifdef _DEBUG
   //PrintParseResultForDebug( m_xmlDocument );
#endif
   return true;
}

bool PugiXmlHelper::WriteToFile( const char* filename, const char* indent, uint format_flags, pugi::xml_encoding encoding )
{
   AddDecaration();
   if( m_xmlDocument.save_file( filename, indent, format_flags, encoding ) ) {
      LOGD << "pugi doc wrote (directly) to file[" << filename << "]";
      return true;
   }
   return false;
}

bool PugiXmlHelper::WriteToMemory( FileIO* pFile, const char* indent, uint format_flags, pugi::xml_encoding encoding )
{
   AddDecaration();
   pugi_override_writer writer( pFile );
   GetXmlDocument().print( writer, indent, format_flags, encoding );

   if( pFile->GetLength() > 0 ) {
      return true;
   }
   return false;
}

pugi::xml_document& PugiXmlHelper::GetXmlDocument()
{
   return m_xmlDocument;
}

void PugiXmlHelper::PrintParseResultForDebug( pugi::xml_node& xml_node )
{
   PugiXmlHelper::pugi_override_walker walker;
   xml_node.traverse( walker );
   LOGV << "\n<<< debugging pugixml contents start >>>\n"
        << walker.log_buffer.str()
        << "debugging pugixml contents end >>>";
}

void PugiXmlHelper::Reset()
{
   m_xmlDocument.reset();
}

//////////////////////////////////////////////////
/// PugiXmlHelper::pugi_override_walker
//////////////////////////////////////////////////
bool PugiXmlHelper::pugi_override_walker::for_each( pugi::xml_node& node )
{
   // indentation
   for( int i = 0; i < depth(); ++i ) {
      log_buffer << "--";
   }

   // node
   log_buffer << g_pugi_node_types[node.type()] << ":"
              << " name='" << node.name() << "'"
              << " value='" << node.value() << "'";
   // attr
   if( node.type() == pugi::node_element ) {
      log_buffer << " attr=[";
      for( pugi::xml_attribute attr = node.first_attribute(); attr != nullptr; attr = attr.next_attribute() ) {
         log_buffer << " " << attr.name() << "='" << attr.value() << "'";
      }
      log_buffer << "]";
   }
   log_buffer << "\n";
   return true; // continue traversal
}

//////////////////////////////////////////////////
/// PugiXmlHelper::pugi_override_writer
//////////////////////////////////////////////////
PugiXmlHelper::pugi_override_writer::pugi_override_writer( FileIO* pFileIO )
{
   st_pFileIO = pFileIO;
}

void PugiXmlHelper::pugi_override_writer::write( const void* data, size_t size )
{
   if( st_pFileIO == nullptr ) {
      LOGF << "m_pFileIO is null";
      return;
   }
   st_pFileIO->Write( const_cast<byte*>( reinterpret_cast<const byte*>( data ) ), 1, static_cast<int>( size ) );
}

PugiXmlHelper::pugi_override_writer::~pugi_override_writer()
{
   st_pFileIO->WriteChar( '\0' );
}

} // namespace BCCL

#include "bccl_console.h"
#include "bccl_misc.h"
#include "bccl_nrc.h"

 namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// Inline Functions
//
///////////////////////////////////////////////////////////////////////////////

 inline void rot (Matrix &a,const double s,const double tau,const int i,const int j,const int k,const int l)
 
{
   double g,h;

   g=a[i][j];
   h=a[k][l];
   a[i][j]=g-s*(h+g*tau);
   a[k][l]=h+s*(g-h*tau);
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 void eigsrt (Matrix &d,Matrix &v)

{
   int i,j,k;
   double p;

   int n=d.NRows;
   for (i=0;i<n-1;i++) {
      p=d(k=i);
      for (j=i;j<n;j++)
         if (d(j) >= p) p=d(k=j);
      if (k != i) {
         d(k)=d(i);
         d(i)=p;
         for (j=0;j<n;j++) {
            p=v[j][i];
            v[j][i]=v[j][k];
            v[j][k]=p;
         }
      }
   }
}

 void jacobi (Matrix &a,Matrix &d,Matrix &v,int &nrot)

{
   int i,j,ip,iq;
   double tresh,theta,tau,t,sm,s,h,g,c;

   int n=d.NRows;
   Matrix b(n),z(n);
   for (ip=0;ip<n;ip++) {
      double *_RESTRICT l_v = v[ip];
      for (iq=0;iq<n;iq++) l_v[iq]=(double)0.0;
      l_v[ip]=(double)1.0;
   }
   for (ip=0;ip<n;ip++) {
      b(ip)=d(ip)=a[ip][ip];
      z(ip)=(double)0.0;
   }
   nrot=0;
   for (i=1;i<=50;i++) {
      sm=(double)0.0;
      for (ip=0;ip<n-1;ip++) {
         double *_RESTRICT l_a = a[ip];
         for (iq=ip+1;iq<n;iq++)
            sm += (double)fabs(l_a[iq]);
      }
      if (sm == (double)0.0)
         return;
      if (i < 4)
         tresh=0.2f*sm/(n*n);
      else
         tresh=(double)0.0;
      for (ip=0;ip<n-1;ip++) {
         double *_RESTRICT l_a = a[ip];
         for (iq=ip+1;iq<n;iq++) {
            g=100.0f*(double)fabs(l_a[iq]);
            if (i > 4 && (fabs(d(ip))+g) == fabs(d(ip))
               && (fabs(d(iq))+g) == fabs(d(iq)))
                  l_a[iq]=(double)0.0;
            else if (fabs(l_a[iq]) > tresh) {
               h=d(iq)-d(ip);
               if ((fabs(h)+g) == fabs(h))
                  t=(l_a[iq])/h;
               else {
                  theta=0.5f*h/(l_a[iq]);
                  t=1.0f/(double)(fabs(theta)+sqrt(1.0f+theta*theta));
                  if (theta < 0.0) t = -t;
               }
               c=1.0f/(double)sqrt(1+t*t);
               s=t*c;
               tau=s/((double)1.0+c);
               h=t*l_a[iq];
               z(ip) -= h;
               z(iq) += h;
               d(ip) -= h;
               d(iq) += h;
               l_a[iq]=(double)0.0;
               for (j=0;j<ip;j++)
                  rot(a,s,tau,j,ip,j,iq);
               for (j=ip+1;j<iq;j++)
                  rot(a,s,tau,ip,j,j,iq);
               for (j=iq+1;j<n;j++)
                  rot(a,s,tau,ip,j,iq,j);
               for (j=0;j<n;j++)
                  rot(v,s,tau,j,ip,j,iq);
               ++nrot;
            }
         }
      }
      for (ip=0;ip<n;ip++) {
         b(ip) += z(ip);
         d(ip)=b(ip);
         z(ip)=(double)0.0;
      }
   }
   // logd ("jacobi(): Too many iterations.\n");
}

 void lubksb (Matrix &a,IArray1D &indx,Matrix &b)

{
   int i,ii=0,ip,j;
   double sum;

   int n=a.NRows;
   for (i=0;i<n;i++) {
      ip=indx[i];
      sum=b(ip);
      b(ip)=b(i);
      if (ii != 0) {
         double* a_i = a[i];
         for (j=ii-1;j<i;j++) 
            sum -= a_i[j]*b(j);
      }
      else if (sum != 0.0)
         ii=i+1;
      b(i)=sum;
   }
   for (i=n-1;i>=0;i--) {
      sum=b(i);
      double* a_i = a[i];
      for (j=i+1;j<n;j++) sum -= a_i[j]*b(j);
      b(i)=sum/a_i[i];
   }
}

 void ludcmp (Matrix &a,IArray1D &indx,double &d)

{
   int i,imax,j,k;
   double big,dum,sum,temp;
   const double TINY=(double)1.0e-20;

   int n=a.NRows;
   Matrix vv(n);
   d=(double)1.0;
   for (i=0;i<n;i++) {
      big=(double)0.0;
      double* a_i = a[i];
      for (j=0;j<n;j++)
         if ((temp=(double)fabs(a_i[j])) > big) big=temp;
      if (big == 0.0) {
         // logd ("ludcmp(): Singular matrix.\n");
         big = MC_VSV; 
      }
      vv(i)=(double)1.0/big;
   }
   for (j=0;j<n;j++) {
      for (i=0;i<j;i++) {
         double* a_i = a[i];
         sum=a_i[j];
         for (k=0;k<i;k++) sum -= a_i[k]*a[k][j];
         a_i[j]=sum;
      }
      big  = (double)0.0;
      imax = 0;
      for (i=j;i<n;i++) {
         double* a_i = a[i];
         sum=a_i[j];
         for (k=0;k<j;k++) sum -= a_i[k]*a[k][j];
         a_i[j]=sum;
         if ((dum=vv(i)*(double)fabs(sum)) >= big) {
            big=dum;
            imax=i;
         }
      }
      if (j != imax) {
         for (k=0;k<n;k++) {
            dum=a[imax][k];
            a[imax][k]=a[j][k];
            a[j][k]=dum;
         }
         d = -d;
         vv(imax)=vv(j);
      }
      indx[j]=imax;
      if (a[j][j] == 0.0) a[j][j]=TINY;
      if (j != n-1) {
         dum=(double)1.0/(a[j][j]);
         for (i=j+1;i<n;i++) a[i][j] *= dum;
      }
   }
}

 void savgol (Matrix &c,const int np,const int nl,const int nr,const int ld,const int m)
 
{
   int j,k,imj,ipj,kk,mm;
   double d,fac,sum;
   
   if (np < nl+nr+1 || nl < 0 || nr < 0 || ld > m || nl+nr < m) logd ("bad args in savgol");
   IArray1D indx(m+1);
   Matrix a(m+1,m+1);
   Matrix b(m+1);
   for (ipj=0;ipj<=(m << 1); ipj++) {
      sum=(ipj ? 0.0:1.0);
      for (k=1;k<=nr;k++) sum += pow((double)k,(double)ipj);
      for (k=1;k<=nl;k++) sum += pow((double)-k,(double)ipj);
      mm=GetMinimum (ipj,2*m-ipj);
      for (imj=-mm;imj<=mm;imj+=2) a[(ipj+imj)/2][(ipj-imj)/2]=sum;
   }
   ludcmp(a,indx,d);
   for (j=0;j<m+1;j++) b(j)=0.0;
   b(ld)=1.0;
   lubksb(a,indx,b);
   for (kk=0;kk<np;kk++) c(kk)=(double)0.0;
   for (k=-nl;k<=nr;k++) {
      sum=b(0);
      fac=1.0;
      for (mm=1;mm<=m;mm++) sum += b(mm)*(fac *= k);
      kk=(np-k) % np;
      c(kk)=sum;
   }
}

}

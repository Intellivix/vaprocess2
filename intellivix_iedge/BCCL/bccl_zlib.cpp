#include "bccl_zlib.h"

#if defined(__LIB_ZLIB)

#if defined(__WIN32)
   #if defined(_DEBUG)
      #pragma comment(lib,"zlib_DS.lib")
   #else
      #pragma comment(lib,"zlib_RS.lib")
   #endif
#endif

 namespace BCCL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: ZL_Compression
//
///////////////////////////////////////////////////////////////////////////////

 ZL_Compression::ZL_Compression (   )
 
{
   CompressionLevel = 6;
   _Init (   );
}

 ZL_Compression::~ZL_Compression (   )
 
{
   Close (   );
}

 void ZL_Compression::_Init (   )
 
{
   memset (&Stream,0,sizeof(z_stream));
   Flag_Initialized = FALSE;
}

 void ZL_Compression::Close (   )
 
{
   if (!Flag_Initialized) return;
   deflateEnd (&Stream);
   _Init (   );
}

 int ZL_Compression::Initialize (   )
 
{
   Close (   );
   if (deflateInit (&Stream,CompressionLevel) != Z_OK) return (1);
   Flag_Initialized = TRUE;
   return (DONE);
}

 int ZL_Compression::Perform (byte *d_buffer,int d_buf_size,int &d_buf_size_used,int flag_finish)
 
{
   int flush;
   
   if (!Flag_Initialized || Stream.next_in == NULL) return (1);
   if (flag_finish) flush = Z_FINISH;
   else flush = Z_NO_FLUSH;
   Stream.avail_out = d_buf_size;
   Stream.next_out  = d_buffer;
   int r_code = deflate (&Stream,flush);
   if (r_code == Z_STREAM_ERROR) return (2);
   d_buf_size_used = d_buf_size - Stream.avail_out;
   if (Stream.avail_out) return (DONE);
   else {
      if (Stream.avail_in) return (-1);
      else return (DONE);
   }
}

 void ZL_Compression::SetSourceBuffer (byte *s_buffer,int s_buf_size)
 
{
   Stream.avail_in = s_buf_size;
   Stream.next_in  = s_buffer;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: ZL_Decompression
//
///////////////////////////////////////////////////////////////////////////////

 ZL_Decompression::ZL_Decompression (   )
 
{
   _Init (   );
}

 ZL_Decompression::~ZL_Decompression (   )
 
{
   Close (   );
}

 void ZL_Decompression::_Init (   )
 
{
   Flag_Initialized = FALSE;
   memset (&Stream,0,sizeof(z_stream));
}

 void ZL_Decompression::Close (   )
 
{
   if (!Flag_Initialized) return;
   inflateEnd (&Stream);
   _Init (   );
}

 int ZL_Decompression::Initialize (   )
 
{
   Close (   );
   if (inflateInit (&Stream) != Z_OK) return (1);
   Flag_Initialized = TRUE;
   return (DONE);
}

 int ZL_Decompression::Perform (byte *d_buffer,int d_buf_size,int &d_buf_size_used)
 
{
   if (!Flag_Initialized || Stream.next_in == NULL) return (1);
   Stream.avail_out = d_buf_size;
   Stream.next_out  = d_buffer;
   int r_code = inflate (&Stream,Z_NO_FLUSH);
   if (r_code == Z_NEED_DICT || r_code == Z_DATA_ERROR || r_code == Z_MEM_ERROR || r_code == Z_STREAM_ERROR) return (2);
   d_buf_size_used = d_buf_size - Stream.avail_out;
   if (Stream.avail_out) return (DONE);
   else {
      if (Stream.avail_in) return (-1);
      else return (DONE);
   }
}

 void ZL_Decompression::SetSourceBuffer (byte *s_buffer,int s_buf_size)
 
{
   Stream.avail_in = s_buf_size;
   Stream.next_in  = s_buffer;
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 int ZL_Compress (byte *s_buffer,int s_buf_size,byte *d_buffer,int d_buf_size)
 
{
   int d_buf_size_used;
   
   ZL_Compression comp;
   if (comp.Initialize (   )) return (-1);
   comp.SetSourceBuffer (s_buffer,s_buf_size);
   int e_code = comp.Perform (d_buffer,d_buf_size,d_buf_size_used);
   if (e_code != DONE) return (-2);
   return (d_buf_size_used);
}

 int ZL_Decompress (byte *s_buffer,int s_buf_size,byte *d_buffer,int d_buf_size)
 
{
   int d_buf_size_used;
   
   ZL_Decompression decomp;
   if (decomp.Initialize (   )) return (-1);
   decomp.SetSourceBuffer (s_buffer,s_buf_size);
   int e_code = decomp.Perform (d_buffer,d_buf_size,d_buf_size_used);
   if (e_code != DONE) return (-2);
   return (d_buf_size_used);
}

}

#endif

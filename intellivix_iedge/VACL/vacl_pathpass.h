#if !defined(__VACL_PATHPASS_H)
#define __VACL_PATHPASS_H

#include "vacl_define.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: Arrow
//
///////////////////////////////////////////////////////////////////////////////

 class Arrow : public ILine2D

{
   public:
      int SplitZoneID;

   public:
      Arrow *Prev,*Next;

   public:
      Arrow (   );

   public:
      int ReadFile  (FileIO &file);
      int ReadFile  (CXMLIO* pIO);
      int WriteFile (FileIO &file);
      int WriteFile (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: ArrowList
//
///////////////////////////////////////////////////////////////////////////////

 class ArrowList : public LinkedList<Arrow>

{
   public:
      int ReadFile  (FileIO &file);
      int ReadFile  (CXMLIO* pIO);
      int WriteFile (FileIO &file);
      int WriteFile (CXMLIO* pIO);

   public:
      void OffsetArrows (int dx,int dy);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: PathZonePtr
//
///////////////////////////////////////////////////////////////////////////////

class PathZone;
 
 class PathZonePtr

{
   public:
      PathZone* Zone;
      
   public:
      PathZonePtr *Prev,*Next;

   public:
      PathZonePtr (PathZone* zone);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: PathZone
//
///////////////////////////////////////////////////////////////////////////////

class PathZoneRoot;
 
 class PathZone : public Polygon

{
   public:
      FPoint2D          Scale;
      PathZoneRoot     *Root;
      Array1D<PathZone> Children;

   public:
      PathZone (   );

   public:
      int       CheckRegionInsideZone (IBox2D &b_box); // [주의] 분석 영상 해상도 기준
      PathZone* FindPathZone          (int id);
      PathZone* FindPathZone          (IBox2D &b_box); // [주의] 분석 영상 해상도 기준
      PathZone* FindPathZone          (IPoint2D &s_point);
      char      GetIDLetter           (   );
      void      GetLeafZonePtrs       (LinkedList<PathZonePtr> &pz_ptrs);
      void      MergeZones            (   );
      void      OffsetZones           (int dx,int dy);
      int       SplitZone             (Arrow &arrow);
      int       SplitZone             (IPoint2D &s_pos,IPoint2D &e_pos,ILine2D *partition = NULL);
      void      UpdateAreaMaps        (FPoint2D &scale);

   public:
      virtual int ReadFile  (FileIO &file);
      virtual int ReadFile  (CXMLIO* pIO);
      virtual int WriteFile (FileIO &file);
      virtual int WriteFile (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: PathZoneRoot
//
///////////////////////////////////////////////////////////////////////////////

 class PathZoneRoot : public PathZone

{
   public:
      int NumChildren;

   public:
      PathZoneRoot (   );
      
   public:
      virtual int ReadFile  (FileIO &file);
      virtual int ReadFile  (CXMLIO* pIO);
      virtual int WriteFile (FileIO &file);
      virtual int WriteFile (CXMLIO* pIO);
};
 
///////////////////////////////////////////////////////////////////////////////
//
// Class: Path
//
///////////////////////////////////////////////////////////////////////////////

class EventZone;
 
 class Path

{
   public:
      int          DirectionToPass;
      float        Time_Elapsed;
      PathZonePtr *CurZonePtr;
      EventZone   *EvtZone;

   public:
      Path *Prev;
      Path *Next;

   public:
      Path (   );
};

typedef LinkedList<Path> PathList;

///////////////////////////////////////////////////////////////////////////////
//
// Class: Edge
//
///////////////////////////////////////////////////////////////////////////////

 class Edge : public FLine2D

{
   public:
     FPoint2D IP;

   public:
      Edge *Prev,*Next;

   public:
      Edge (   );
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: Segment
//
///////////////////////////////////////////////////////////////////////////////

 class Segment : public FLine2D

{
   public:
      Segment *Prev,*Next;
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: PolygonSplitter
//
///////////////////////////////////////////////////////////////////////////////

 class PolygonSplitter : public Polygon
 
{
   public:
      PolygonSplitter (Polygon *s_polygon);

   public:
      int  SplitPolygon (IPoint2D &s_pos,IPoint2D &e_pos,Array1D<Polygon> &d_polygons,ILine2D &d_partition);

   protected:
      int  CheckPointIsOnSegment                (FPoint2D &pos,Segment &segment);
      int  GetEdges                             (LinkedList<Edge> &d_edges);
      int  GetIntersectionPoints                (IPoint2D &s_pos,IPoint2D &e_pos,LinkedList<Edge> &edges,FP2DArray1D &d_ips);
      int  GetIntersectionSegments              (LinkedList<Edge> &edges,FP2DArray1D &ips,LinkedList<Segment> &d_segments);
      int  GetNumberOfEdgesIntersectedBySegment (LinkedList<Edge> &edges,Segment &segment);
      int  GetSplitPolygons                     (LinkedList<Edge> &edges,Segment &segment,Array1D<Polygon> &d_polygons);
};

}

#endif



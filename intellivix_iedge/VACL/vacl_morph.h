#if !defined(__VACL_MORPH_H)
#define __VACL_MORPH_H

#include "vacl_define.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

void BinClosing         (GImage& s_image,GImage& s_element,GImage& d_image);
void BinDilation        (GImage& s_image,GImage& s_element,GImage& d_image);
void BinErosion         (GImage& s_image,GImage& s_element,GImage& d_image);
void BinHoleFilling     (int min_area,int max_area,GImage& d_image);
void BinHRLFiltering    (GImage& s_image,int gap_size,GImage& d_image);
void BinOpening         (GImage& s_image,GImage& s_element,GImage& d_image);
void BinVRLFiltering    (GImage& s_image,int gap_size,GImage& d_image);
void CRDilation         (IArray2D& s_cr_map,GImage& s_element,IArray2D& d_cr_map);
void CRErosion          (IArray2D& s_cr_map,GImage& s_element,IArray2D& d_cr_map);
void CRHoleFilling      (int min_area,int max_area,IArray2D& d_cr_map);
void CRHRLFiltering     (IArray2D& s_cr_map,int gap_size,IArray2D& d_cr_map);
void CRVRLFiltering     (IArray2D& s_cr_map,int gap_size,IArray2D& d_cr_map);
void GetBinStrElement   (GImage &s_element);
void GetBinStrElement   (int se_radius,GImage &s_element);
void PerformBinClosing  (GImage& s_image,int se_radius,GImage& d_image,int rd = 1);
void PerformBinClosing2 (GImage& s_image,int se_radius,GImage& d_image,int rd = 1);
void PerformBinDilation (GImage& s_image,int se_radius,GImage& d_image);
void PerformBinErosion  (GImage& s_image,int se_radius,GImage& d_image);
void PerformBinOpening  (GImage& s_image,int se_radius,GImage& d_image);
void PerformCRClosing   (IArray2D& s_cr_map,int se_radius,IArray2D& d_cr_map,int rd = 1);
void PerformCRDilation  (IArray2D& s_cr_map,int se_radius,IArray2D& d_cr_map);
void PerformCRErosion   (IArray2D& s_cr_map,int se_radius,IArray2D& d_cr_map);
void PerformCROpening   (IArray2D& s_cr_map,int se_radius,IArray2D& d_cr_map);

}

#endif

#if !defined(__VACL_OBJFILTER_H)
#define __VACL_OBJFILTER_H
 
#include "vacl_objtrack.h"

 namespace VACL

{
 
///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectFiltering
//
///////////////////////////////////////////////////////////////////////////////

#define OBF_OPTION_ENABLE                  0x00000001
#define OBF_OPTION_USE_REAL_SIZE_VALUES    0x00000002
#define OBF_OPTION_USE_WIDTH               0x00000004
#define OBF_OPTION_USE_HEIGHT              0x00000008
#define OBF_OPTION_USE_AREA                0x00000010
#define OBF_OPTION_USE_ASPECT_RATIO        0x00000020
#define OBF_OPTION_USE_AXIS_LENGTH_RATIO   0x00000040
#define OBF_OPTION_USE_MAJOR_AXIS_LENGTH   0x00000080
#define OBF_OPTION_USE_MINOR_AXIS_LENGTH   0x00000100
#define OBF_OPTION_USE_NORMALIZED_SPEED    0x00000200
#define OBF_OPTION_USE_SPEED               0x00000400
#define OBF_OPTION_USE_REAL_DISTANCE       0x00000800
#define OBF_OPTION_USE_TARGET_COLOR        0x00001000

 class ObjectFiltering
 
{
   protected:
      int   Flag_Enabled;
      float MaxOverlap;

   public:
      int   Options;
      float MaxTime_Tracked;

   public:
      int   MinWidth;
      int   MaxWidth;
      int   MinHeight;
      int   MaxHeight;
      float MinArea;
      float MaxArea;
      float MinAspectRatio;
      float MaxAspectRatio;
      float MinAxisLengthRatio;
      float MaxAxisLengthRatio;
      float MinMajorAxisLength;
      float MaxMajorAxisLength;
      float MinMinorAxisLength;
      float MaxMinorAxisLength;
      float MinNorSpeed;
      float MaxNorSpeed;
      float MinSpeed;
      float MaxSpeed;
      float MinRealArea;
      float MaxRealArea;
      float MinRealDistance;
      float MaxRealDistance;
      float MinRealWidth;
      float MaxRealWidth;
      float MinRealHeight;
      float MaxRealHeight;
      float MinRealMajorAxisLength;
      float MaxRealMajorAxisLength;
      float MinRealMinorAxisLength;
      float MaxRealMinorAxisLength;
      float MinRealSpeed;
      float MaxRealSpeed;

   public:
      int      CDS;      // Color Detection Sensitivity
      BGRColor TgtColor; // Target Color
      
   public:
      ISize2D SrcImgSize;

   public:
      ObjectFiltering (   );
      virtual ~ObjectFiltering (   ) {   };

   protected:
      void FilterObjects   (ObjectTracking& obj_tracker);
      void SuppressOverlap (ObjectTracking& obj_tracker);

   public:
      virtual int CheckSetup (ObjectFiltering* of);
      virtual int ReadFile   (FileIO& file);
      virtual int ReadFile   (CXMLIO* pIO);
      virtual int WriteFile  (FileIO& file);
      virtual int WriteFile  (CXMLIO* pIO);
   
   public:
      void Enable     (int flag_enable);
      int  Filter     (TrackedBlob* t_blob);
      int  Filter     (TrackedObject* t_object);
      void InitParams (EventDetection& evt_detector);
      int  IsEnabled  (   );
      int  Perform    (ObjectTracking& obj_tracker);
      void ResetCDS   (   );
};

 inline void ObjectFiltering::Enable (int flag_enable)

{
   Flag_Enabled = flag_enable;
}

 inline int ObjectFiltering::IsEnabled (   )

{
   return (Flag_Enabled);
}

}

#endif

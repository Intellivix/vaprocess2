#if !defined(__VACL_HOUGH_H)
#define __VACL_HOUGH_H

#include "vacl_opencv.h"

 namespace VACL

{

#if defined(__LIB_OPENCV)
int OpenCV_DetectBrightCircles_HCT (GImage &s_image,GImage &e_image,float dp,float min_dist,int acc_threshold,int min_radius,int max_radius,FP3DArray1D &d_array);
int OpenCV_DetectCircles_HCT       (GImage &s_image,GImage &e_image,float dp,float min_dist,int acc_threshold,int min_radius,int max_radius,FP3DArray1D &d_array);
int OpenCV_DetectDarkCircles_HCT   (GImage &s_image,GImage &e_image,float dp,float min_dist,int acc_threshold,int min_radius,int max_radius,FP3DArray1D &d_array);
#endif

}

#endif

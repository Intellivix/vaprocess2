#include "vacl_evtzone.h"
#include "vacl_event.h"

 namespace VACL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Global Variables
//
///////////////////////////////////////////////////////////////////////////////

EventZoneListManager* __EZLM = NULL;
EventZone* (*__CreationFunction_EventZone)(   ) = NULL;

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventZone
//
///////////////////////////////////////////////////////////////////////////////

 EventZone::EventZone (   )
 
{
   BasePos(0.5f,0.5f);
   Scale(1.0f,1.0f);
   if (__EZLM) ID = __EZLM->GetNewID (   );
   else ID = 0;
   EvtRule               = NULL;
   CurCrowdCount         = -1;
   CurDwellObjectCount   = 0;
   TotalDwellObjectCount = 0;
   AverageDwellTime      = 0.0f;
   sprintf (Name,"Event Zone #%d",ID);
   if (__EZLM) __EZLM->Add (this);
}

 EventZone::~EventZone (   ) 
 
{
   EvtRule = NULL;
   if (__EZLM) __EZLM->Remove (this);
}

 int EventZone::CheckObjectInsideZone (TrackedObject* t_object)
// 객체 바운딩 박스의 네 꼭지점이 모두 이벤트 존에 진입했을 때 객체가 이벤트 존에 진입한 것으로 판단한다.
// 객체 바운딩 박스의 네 꼭지점이 모두 이벤트 존을 벗어났을 때 객체가 이벤트 존을 벗어난 것으로 판단한다.
{
   int i,n;
   
   // 객체가 이미 이벤트 존 내에 들어와 있는지 체크한다.
   int flag_entered = FALSE;
   Event* event = t_object->Events.First (   );
   while (event != NULL) {
      if (event->EvtZone == this) {
         flag_entered = TRUE;
         break;
      }
      event = t_object->Events.Next (event);
   }
   // 객체 바운딩 박스의 꼭지점들 중에 이벤트 존 내에 있는 꼭지점들의 수를 샌다.
   IPoint2D vertices[4];
   t_object->GetCornerPoints (vertices,0.85f);
   for (i = n = 0; i < 4; i++) {
      FPoint2D pos;
      pos.X = vertices[i].X * Scale.X;
      pos.Y = vertices[i].Y * Scale.Y;
      if (IsPointInsidePolygon (pos)) n++;
   }
   // 객체가 이미 이벤트 존 내에 들어온 경우
   if (flag_entered) {
      // 객체 바운딩 박스의 네 꼭지점이 모두 이벤트 존 밖에 있어야 객체가 이벤트 존을 벗어난 것으로 판단한다.
      if (n == 0) return (1);
      else return (DONE);
   }
   // 그렇지 않은 경우
   else {
      // 객체 바운딩 박스의 네 꼭지점이 모두 이벤트 존 내에 있어야 객체가 이벤트 존에 진입한 것으로 판단한다.
      if (n == 4) return (DONE);
      else return (1);
   }
}

 int EventZone::CheckPositionInsideZone (FPoint2D& s_pos)
 
{
   FPoint2D pos = s_pos * Scale;
   if (IsPointInsidePolygon (pos)) return (DONE);
   else return (1);
}

 int EventZone::CheckPositionInsideZone (IPoint2D& s_pos)
 
{
   FPoint2D pos;
   pos.X = s_pos.X * Scale.X;
   pos.Y = s_pos.Y * Scale.Y;
   if (IsPointInsidePolygon (pos)) return (DONE);
   else return (1);
}

 int EventZone::CheckRegionInsideZone (IBox2D &s_rgn)
#if defined(__CPNI_CERT_SZM)
// 객체 바운딩 박스의 중심, 하단 좌측 꼭지점, 하단 우측 꼭지점 중 하나가 이벤트 존 내에 있으면
// 객체가 이벤트 존 내에 있는 것으로 판단한다.
{
   FPoint2D pos;
   // Center
   pos.X = s_rgn.X + 0.5f * s_rgn.Width;
   pos.Y = s_rgn.Y + 0.5f * s_rgn.Height;
   pos *= Scale;
   if (IsPointInsidePolygon (pos)) return (DONE);
   // Bottom-Left
   pos.X = s_rgn.X + 0.0f * s_rgn.Width;
   pos.Y = s_rgn.Y + 1.0f * s_rgn.Height;
   pos *= Scale;
   if (IsPointInsidePolygon (pos)) return (DONE);
   // Bottom-Right
   pos.X = s_rgn.X + 1.0f * s_rgn.Width;
   pos.Y = s_rgn.Y + 1.0f * s_rgn.Height;
   pos *= Scale;
   if (IsPointInsidePolygon (pos)) return (DONE);
   return (1);
}
#else
// 객체 바운딩 박스의 중심이 이벤트 존 내에 있으면 객체가 이벤트 존 내에 있는 것으로 판단한다.
{
   FPoint2D pos;
   pos.X = s_rgn.X + BasePos.X * s_rgn.Width;
   pos.Y = s_rgn.Y + BasePos.Y * s_rgn.Height;
   pos *= Scale;
   if (IsPointInsidePolygon (pos)) return (DONE);
   return (1);
}
#endif

 int EventZone::CheckSetup (EventZone* ez)

{
   int ret_flag = 0;

   FileIO buffThis,buffFrom;   
   CXMLIO xmlIOThis,xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis,XMLIO_Write);
   xmlIOFrom.SetFileIO (&buffFrom,XMLIO_Write);
   WriteFile (&xmlIOThis);
   ez->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom))
      ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength (0);
   buffFrom.SetLength (0);
   if (ret_flag & CHECK_SETUP_RESULT_CHANGED) {
      ret_flag |= CHECK_SETUP_RESULT_RESTART;
   }
   return (ret_flag);
}

 int EventZone::DetectEvents (ObjectTracking &obj_tracker)

{
   if (EvtRule == NULL || !(EvtRule->Status & ER_STATUS_ENABLED)) return (0);
   return (EvtRule->DetectEvents (obj_tracker,this));
}

 int EventZone::DetectEvents (GroupTracking &grp_tracker)
 
{
   if (EvtRule == NULL || !(EvtRule->Status & ER_STATUS_ENABLED)) return (0);
   return (EvtRule->DetectEvents (grp_tracker,this));
}

 int EventZone::DetectEvents (FlameDetection &flm_detector)
 
{
   if (EvtRule == NULL || EvtRule->EventType != ER_EVENT_FLAME || !(EvtRule->Status & ER_STATUS_ENABLED)) return (0);
   return (EvtRule->DetectEvents_Presence (flm_detector,this,EvtRule->ERC_Flame.DetectionTime,1.0f,TO_TYPE_FLAME,TRUE,FALSE));
}

 int EventZone::DetectEvents (SmokeDetection &smk_detector)
 
{
   if (EvtRule == NULL || EvtRule->EventType != ER_EVENT_SMOKE || !(EvtRule->Status & ER_STATUS_ENABLED)) return (0);
   return (EvtRule->DetectEvents_Presence (smk_detector,this,EvtRule->ERC_Smoke.DetectionTime,1.0f,TO_TYPE_SMOKE,TRUE,FALSE));
}

 int EventZone::DetectEvents (ColorChangeDetection& clc_detector)
 
{
   if (EvtRule == NULL || EvtRule->EventType != ER_EVENT_COLOR_CHANGE || !(EvtRule->Status & ER_STATUS_ENABLED)) return (0);
   int flag_cont = FALSE;
   int flag_bkg  = TRUE;
   if (EvtRule->ObjectFilter->Options & OBF_OPTION_USE_TARGET_COLOR) {
      flag_cont = TRUE;
      flag_bkg  = FALSE;
   }
   return (EvtRule->DetectEvents_Presence (clc_detector,this,EvtRule->ERC_ColorChange.DetectionTime,0.0f,TO_TYPE_UNKNOWN,flag_cont,flag_bkg));
}

 int EventZone::Enable (int flag_enable)
 
{
   if (EvtRule == NULL) return (1);
   if (flag_enable) EvtRule->Status |= ER_STATUS_ENABLED;
   else EvtRule->Status &= ~ER_STATUS_ENABLED;
   return (DONE);
}

 int EventZone::IsEnabled (   )

{
   if (EvtRule == NULL || !(EvtRule->Status & ER_STATUS_ENABLED)) return (FALSE);
   else return (TRUE);
}

 int EventZone::ReadFile (FileIO &file)
 
{
   if (Polygon::ReadFile (file)) return (1);
   if (file.Read ((byte *)&ID       ,1,sizeof(int))     ) return (2);
   if (file.Read ((byte *)&EvtRuleID,1,sizeof(int))     ) return (3);
   if (file.Read ((byte *)Name      ,1,sizeof(Name))    ) return (4);
   if (file.Read ((byte *)&BasePos  ,1,sizeof(FPoint2D))) return (5);
   return (DONE);
}

 int EventZone::ReadFile (CXMLIO* pIO)

{
   static EventZone dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("EventZone"))
   {
      Polygon::ReadFile (pIO);
      xmlNode.Attribute (TYPE_ID(int)     ,"ID"       ,&ID);
      xmlNode.Attribute (TYPE_ID(int)     ,"EvtRuleID",&EvtRuleID);
      xmlNode.Attribute (TYPE_ID(char)    ,"Name"     ,Name,sizeof (Name));
      xmlNode.Attribute (TYPE_ID(FPoint2D),"BasePos"  ,&BasePos,&dv.BasePos);
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}

 void EventZone::UpdateAreaMap (FPoint2D &scale)
 
{
   Scale = scale;
   UpdateCenterPosition (   );
   EvtRule->ERC_PathPassing.ZoneTree.UpdateAreaMaps (scale);
}

 int EventZone::WriteFile (FileIO &file)
 
{
   if (EvtRule) EvtRuleID = EvtRule->ID;
   if (Polygon::WriteFile (file)) return (1);
   if (file.Write ((byte *)&ID       ,1,sizeof(int))     ) return (2);
   if (file.Write ((byte *)&EvtRuleID,1,sizeof(int))     ) return (3);
   if (file.Write ((byte *)Name      ,1,sizeof(Name))    ) return (4);
   if (file.Write ((byte *)&BasePos  ,1,sizeof(FPoint2D))) return (5);
   return (DONE);
}

 int EventZone::WriteFile (CXMLIO* pIO)

{
   static EventZone dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("EventZone"))
   {
      if (EvtRule) EvtRuleID = EvtRule->ID;
      Polygon::WriteFile (pIO);
      xmlNode.Attribute (TYPE_ID(int)     ,"ID"       ,&ID);
      xmlNode.Attribute (TYPE_ID(int)     ,"EvtRuleID",&EvtRuleID);
      xmlNode.Attribute (TYPE_ID(char)    ,"Name"     ,Name,sizeof (Name));
      xmlNode.Attribute (TYPE_ID(FPoint2D),"BasePos"  ,&BasePos,&dv.BasePos);
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventZoneList
//
///////////////////////////////////////////////////////////////////////////////

 int EventZoneList::ReadFile (FileIO &file)

{
   int i,n_items;

   if (file.Read ((byte*)&Width  ,1,sizeof(int))) return (1);
   if (file.Read ((byte*)&Height ,1,sizeof(int))) return (2);
   if (file.Read ((byte*)&n_items,1,sizeof(int))) return (3);
   EventZone *evt_zone;
   if (n_items != GetNumItems ()) {
      Delete ();
      for (i = 0; i < n_items; i++) {
         evt_zone = CreateInstance_EventZone (   );
         if (evt_zone->ReadFile (file)) {
            delete evt_zone;
            return (2);
         }
         else Add (evt_zone);
      }
   }
   else {
      evt_zone = First (   );
      while (evt_zone) {
         if (evt_zone->ReadFile (file)) 
            return (2);
         evt_zone = Next (evt_zone);
      }
   }
   return (DONE);
}

 int EventZoneList::ReadFile (CXMLIO* pIO, const char* szName)

{
   int i,n_items;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start (szName))
   {
      xmlNode.Attribute (TYPE_ID(int),"Width"  ,&Width);
      xmlNode.Attribute (TYPE_ID(int),"Height" ,&Height);
      xmlNode.Attribute (TYPE_ID(int),"ItemNum",&n_items);
      EventZone *evt_zone;
      if (n_items != GetNumItems ()) {
         Delete ();
         for (i = 0; i < n_items; i++) {
            EventZone *evt_zone = CreateInstance_EventZone (   );
            if (DONE == evt_zone->ReadFile (pIO)) Add (evt_zone);
            else delete evt_zone;
         }
      }
      else {
         evt_zone = First (   );
         while (evt_zone) {
            if (evt_zone->ReadFile (pIO)) return (2);
            evt_zone = Next (evt_zone);
         }
      }
      xmlNode.End ();
   }
   else {
      Delete ();
   }
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventZoneListManager
//
///////////////////////////////////////////////////////////////////////////////

 EventZoneListManager::~EventZoneListManager (   )
 
{
   Items.Delete (   );
}

 void EventZoneListManager::Add (EventZone *s_item)

{
   Lock (   );
   Item* new_item = new Item;
   new_item->ItemPtr = s_item;
   Items.Add (new_item);
   Unlock (   );
}

 int EventZoneListManager::GetNewID (   ) 

{
   Lock (   );
   int new_id = 0;
   while (TRUE) {
      int flag_same_id = FALSE;
      Item* item = Items.First (   );
      while (item) {
         if (new_id == item->ItemPtr->ID) {
            flag_same_id = TRUE;
            break;
         }
         item = Items.Next (item);
      }
      if (!flag_same_id) break;
      new_id++;
   }
   Unlock (   );
   return (new_id);
}

 void EventZoneListManager::Lock (   )
 
{
   CS_List.Lock (   );
}

 void EventZoneListManager::Remove (EventZone *s_item)
 
{
   Lock (   );
   Item* item = Items.First (   );
   while (item) {
      if (item->ItemPtr == s_item) {
         Items.Remove (item);
         delete item;
         break;
      }
      item = Items.Next (item);
   }
   Unlock (   );
}

 void EventZoneListManager::Unlock (   )
 
{
   CS_List.Unlock (   );
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 EventZone* CreateInstance_EventZone (   )

{
   if (__CreationFunction_EventZone == NULL) {
      EventZone *evt_zone = new EventZone;
      return (evt_zone);
   }
   else return (__CreationFunction_EventZone (   ));
}

 void SetCreationFunction_EventZone (EventZone*(*func)(   ))

{
   __CreationFunction_EventZone = func;
}

}

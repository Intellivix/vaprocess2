#include "vacl_crowd.h"
#include "vacl_event.h"
#include "vacl_feature.h"
#include "vacl_nms.h"

#if defined(__DEBUG_CDE)
#include "Win32CL/Win32CL.h"
extern int _DVX_CDE,_DVY_CDE;
extern CImageView* _DebugView_CDE;
#endif

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: CrowdZone
//
///////////////////////////////////////////////////////////////////////////////

 CrowdZone::CrowdZone (   )

{
   EvtZone = NULL;
}

 int CrowdZone::DetectEvent (   )

{
   if (EvtZone == NULL) return (0);
   EventRule* evt_rule = EvtZone->EvtRule;
   if (evt_rule == NULL) return (0);
   EventRuleConfig_CrowdDensity& erc = evt_rule->ERC_CrowdDensity;
   EventDetection* evt_detector = evt_rule->EventDetector;
   if (evt_detector == NULL) return (0);
   int n_objects = 0;
   int flag_detection = FALSE;
   if (EvtZone->CurCrowdCount > erc.CountThreshold) flag_detection = TRUE;
   Event* m_event = NULL;
   TrackedObject* eg_object = NULL;
   TrackedObject* t_object  = evt_detector->EvtGenObjects.First (   );
   while (t_object != NULL) {
      Event* event = t_object->FindEvent (EvtZone);
      if (event != NULL) {
         m_event   = event;
         eg_object = t_object;
         break;
      }
      t_object = evt_detector->EvtGenObjects.Next (t_object);
   }
   if (m_event == NULL) {
      if (flag_detection) {
         eg_object         = CreateInstance_TrackedObject (   );
         eg_object->ID     = evt_detector->GetNewObjectID (   );
         eg_object->Status = TO_STATUS_VALIDATED | TO_STATUS_VERIFIED;
         eg_object->Type   = TO_TYPE_CROWD;
         m_event = CreateInstance_Event  (EvtZone,evt_rule);
         m_event->StartTime.GetLocalTime (   );
         eg_object->Events.Add (m_event);
         evt_detector->EvtGenObjects.Add (eg_object);
         n_objects = 1;
      }
   }
   else if (m_event->EvtRule == evt_rule) {
      m_event->ElapsedTime += evt_detector->FrameDuration;
      switch (m_event->Status) {
         case EV_STATUS_BEGUN:
            m_event->Status = EV_STATUS_ONGOING;
            break;
         case EV_STATUS_ONGOING:
            if (!flag_detection) {
               m_event->BonusTime += evt_detector->FrameDuration;
               if (m_event->BonusTime >= 1.0f) { // PARAMETERS
                  m_event->Status = EV_STATUS_ENDED | EV_STATUS_TO_BE_REMOVED;
                  m_event->EndTime.GetLocalTime (   );
                  eg_object->Status |= TO_STATUS_TO_BE_REMOVED;
               }
            }
            else m_event->BonusTime = 0.0f;
            break;
         default:
            break;
      }
   }
   if (eg_object != NULL) eg_object->SetRegion (CrowdRegion);
   evt_rule->ERC_Counting.Count += n_objects;
   return (n_objects);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CrowdDensityEstimation
//
///////////////////////////////////////////////////////////////////////////////

 CrowdDensityEstimation::CrowdDensityEstimation (   )

{
   Flag_Enabled = TRUE;
   FrameRate    = 0.0f;

   UpdateRate   = 0.3f;
}

 CrowdDensityEstimation::~CrowdDensityEstimation (   )

{
   Close (   );
}

 void CrowdDensityEstimation::Close (   )

{
   CrowdZones.Delete (   );
}

 int CrowdDensityEstimation::Initialize (EventDetection& evt_detector)

{
   Close (   );
   if (!IsEnabled (   )) return (1);
   SrcImgSize = evt_detector.SrcImgSize;
   FrameRate  = evt_detector.FrameRate;
   EventZoneList& ez_list = evt_detector.EventZones;
   EventZone* evt_zone = ez_list.First (   );
   while (evt_zone != NULL) {
      if (evt_zone->EvtRule->EventType == ER_EVENT_CROWD_DENSITY) {
         CrowdZone* zone = new CrowdZone;
         zone->EvtZone = evt_zone;
         CrowdZones.Add (zone);
      }
      evt_zone = ez_list.Next (evt_zone);
   }
   if (!CrowdZones.GetNumItems (   )) return (2);
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CrowdDensityEstimation_OBS
//
///////////////////////////////////////////////////////////////////////////////

 CrowdDensityEstimation_OBS::CrowdDensityEstimation_OBS (   )

{
   UpdateRate = 0.75f;
}

 int CrowdDensityEstimation_OBS::Perform (ForegroundDetection& frg_detector,CrowdZone* zone)

{
   int i,j;
   
   EventZone* evt_zone = zone->EvtZone;
   if (evt_zone == NULL) return (1);
   int count = 0;
   IP2DArray1D cp_array(4);
   IPoint2D min_p(SrcImgSize.Width,SrcImgSize.Height);
   IPoint2D max_p(-1,-1);
   CRArray1D& fr_array = frg_detector.FrgRegions;
   for (i = 1; i < fr_array.Length; i++) {
      FPoint2D centroid(fr_array[i].CX,fr_array[i].CY);
      if (evt_zone->CheckPositionInsideZone (centroid) == DONE) {
         count++;
         fr_array[i].GetCornerPoints (cp_array);
         for (j = 0; j < cp_array.Length; j++) {
            IPoint2D& cp = cp_array[j];
            if (cp.X < min_p.X) min_p.X = cp.X;
            if (cp.Y < min_p.Y) min_p.Y = cp.Y;
            if (cp.X > max_p.X) max_p.X = cp.X;
            if (cp.Y > max_p.Y) max_p.Y = cp.Y;
         }
      }
   }
   if (evt_zone->CurCrowdCount < 0) evt_zone->CurCrowdCount = count;
   else evt_zone->CurCrowdCount = (int)((1.0f - UpdateRate) * evt_zone->CurCrowdCount + UpdateRate * count);
   zone->CrowdRegion.Clear (   );
   if (max_p.X >= 0 && max_p.Y >= 0) {
      IBox2D& rgn = zone->CrowdRegion;
      rgn.X      = min_p.X;
      rgn.Y      = min_p.Y;
      rgn.Width  = max_p.X - min_p.X;
      rgn.Height = max_p.Y - min_p.Y;
      CropRegion (SrcImgSize,rgn);
   }
   return (DONE);
}

 int CrowdDensityEstimation_OBS::Perform (ForegroundDetection& frg_detector)

{
   if (!IsEnabled (   )) return (1);
   if (!CrowdZones.GetNumItems (   )) return (2);
   CrowdZone* zone = CrowdZones.First (   );
   while (zone != NULL) {
      if (zone->EvtZone->IsEnabled (   )) Perform (frg_detector,zone);
      zone = CrowdZones.Next (zone);
   }
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CrowdDensityEstimation_FPD
//
///////////////////////////////////////////////////////////////////////////////

 CrowdDensityEstimation_FPD::CrowdDensityEstimation_FPD (   )

{
   MaxHFPWidth  = 50;
   HFPNMSSize   = 19;
   HPVThreshold = 0.5f;
   HumanHeight  = 1.7f;
   UpdateRate   = 0.5f;
}
 
 int CrowdDensityEstimation_FPD::CheckHeadPosition (GImage& s_image,IPoint2D& hd_pos,int hd_size,IPoint2D& ft_pos)

{
   int x,y;
   
   int ey = ft_pos.Y;
   if (ey > s_image.Height) ey = s_image.Height;
   int h1 = ft_pos.Y - hd_pos.Y;
   int h2 = ey - hd_pos.Y;
   float hr = (float)h2 / h1;
   if (hr < 0.2f) return (1);
   int r  = hd_size / 2;
   int cx = hd_pos.X;
   int sx = cx - r;
   int ex = cx + r + 1;
   int sy = hd_pos.Y;
   if (sx < 0) sx = 0;
   if (ex > s_image.Width) ex = s_image.Width;
   float a1 = 0.0f;
   int   y1 = (int)(0.2f * h1);
   int   y2 = (int)(0.6f * h1);
   for (y = sy; y < ey; y++) {
      int y0 = y - sy;
      float w;
      if      (y0 <= y1) w = 1.25f;
      else if (y0 <= y2) w = 1.0f;
      else               w = 1.25f;
      byte* l_s_image = s_image[y];
      for (x = sx; x < ex; x++) {
         if (l_s_image[x]) a1 += w;
      }
   }
   int   a2 = (ex - sx) * (ey - sy);
   float ar = a1 / a2;
   if (ar < HPVThreshold) return (2);
   return (DONE);
}

 int CrowdDensityEstimation_FPD::GetHeadCandidatePositions (GImage& fg_image,IP2DArray1D& fp_array,RealObjectSizeEstimation& ros_estimator,IP2DArray1D& d_hp_array,IArray1D& d_hs_array)

{
   int i;

   d_hp_array.Delete (   );
   d_hs_array.Delete (   );   
   if (!ros_estimator.IsCalibrated (   )) {
      logd ("[WARNING] The camera is not calibrated!\n");
      return (1);
   }
   ISize2D si_size(fg_image.Width,fg_image.Height);
   int n_hps = 0;
   IP2DArray1D hp_array(fp_array.Length);
   IArray1D    hs_array(fp_array.Length);
   for (i = 0; i < fp_array.Length; i++) {
      IPoint2D hd_pos = fp_array[i];
      IPoint2D ft_pos; int hd_size; ISize2D bd_size;
      ros_estimator.GetFeetPosAndBodySize (hd_pos,si_size,HumanHeight,ft_pos,hd_size,bd_size);
      if (CheckHeadPosition (fg_image,hd_pos,hd_size,ft_pos) == DONE) {
         hp_array[n_hps] = hd_pos;
         hs_array[n_hps] = hd_size;
         n_hps++;
      }
   }
   if (!n_hps) return (1);
   d_hp_array = hp_array.Extract (0,n_hps);
   d_hs_array = hs_array.Extract (0,n_hps);
   return (DONE);
}

 int CrowdDensityEstimation_FPD::Perform (IP2DArray1D& hp_array,IArray1D& hs_array,CrowdZone* zone)

{
   int i;

   EventZone* evt_zone = zone->EvtZone;
   if (evt_zone == NULL) return (1);
   float tf = evt_zone->EvtRule->ERC_CrowdDensity.TransformFactor;
   if (tf < 0.1f) tf = 0.1f;
   float a   = 1.0f / tf;
   float sum = 0.0f;
   IPoint2D min_p(SrcImgSize.Width,SrcImgSize.Height);
   IPoint2D max_p(-1,-1);
   for (i = 0; i < hp_array.Length; i++) {
      IPoint2D& hp = hp_array[i];
      if (evt_zone->CheckPositionInsideZone (hp) == DONE) {
         sum += a;
         if (hp.X < min_p.X) min_p.X = hp.X;
         if (hp.Y < min_p.Y) min_p.Y = hp.Y;
         if (hp.X > max_p.X) max_p.X = hp.X;
         if (hp.Y > max_p.Y) max_p.Y = hp.Y;
      }
   }
   if (evt_zone->CurCrowdCount < 0) evt_zone->CurCrowdCount = (int)(sum + 0.5f);
   else evt_zone->CurCrowdCount = (int)((1.0f - UpdateRate) * evt_zone->CurCrowdCount + UpdateRate * sum + 0.5f);
   zone->CrowdRegion.Clear (   );
   if (max_p.X >= 0 && max_p.Y >= 0) {
      IBox2D& rgn = zone->CrowdRegion;
      rgn.X      = min_p.X;
      rgn.Y      = min_p.Y;
      rgn.Width  = max_p.X - min_p.X;
      rgn.Height = max_p.Y - min_p.Y;
      CropRegion (SrcImgSize,rgn);
   }
   return (DONE);
}

 int CrowdDensityEstimation_FPD::Perform (ForegroundDetection& frg_detector,RealObjectSizeEstimation& ros_estimator)

{
   if (!IsEnabled (   )) return (1);
   if (!CrowdZones.GetNumItems (   )) return (2);

   #if defined(__DEBUG_CDE)
   _DVX_CDE = _DVY_CDE = 0;
   _DebugView_CDE->Clear (   );
   #endif
   
   GImage& s_image  = frg_detector.SrcImage;
   GImage& fg_image = frg_detector.FrgImage;

   #if defined(__DEBUG_CDE)
   _DebugView_CDE->DrawImage (fg_image,_DVX_CDE,_DVY_CDE);
   _DVY_CDE += SrcImgSize.Height;
   _DebugView_CDE->DrawText ("Foreground Detection Result",_DVX_CDE,_DVY_CDE,PC_GREEN);
   _DVY_CDE += 20;
   #endif

   IP2DArray1D fp_array;
   DetectHeadFeaturePoints (s_image,fg_image,fp_array);

   #if defined(__DEBUG_CDE)
   _DebugView_CDE->DrawImage (s_image,_DVX_CDE,_DVY_CDE);
   for (i = 0; i < fp_array.Length; i++)
      _DebugView_CDE->DrawPoint (_DVX_CDE + fp_array[i].X,_DVY_CDE + fp_array[i].Y,PC_GREEN,PT_CROSS);
   _DVY_CDE += SrcImgSize.Height;
   _DebugView_CDE->DrawText ("Head Feature Point Detection Result",_DVX_CDE,_DVY_CDE,PC_GREEN);
   _DVY_CDE += 20;
   #endif

   IP2DArray1D hp_array;
   IArray1D    hs_array;
   GetHeadCandidatePositions (fg_image,fp_array,ros_estimator,hp_array,hs_array);

   #if defined(__DEBUG_CDE)
   _DebugView_CDE->DrawImage (s_image,_DVX_CDE,_DVY_CDE);
   for (i = 0; i < hp_array.Length; i++) {
      _DebugView_CDE->DrawPoint (_DVX_CDE + hp_array[i].X,_DVY_CDE + hp_array[i].Y,PC_GREEN,PT_CROSS);
   }
   _DVY_CDE += SrcImgSize.Height;
   _DebugView_CDE->DrawText ("Head Candidate Position Detection Result",_DVX_CDE,_DVY_CDE,PC_GREEN);
   _DVY_CDE += 20;
   #endif

   CrowdZone* zone = CrowdZones.First (   );
   while (zone != NULL) {
      if (zone->EvtZone->IsEnabled (   )) Perform (hp_array,hs_array,zone);
      zone = CrowdZones.Next (zone);
   }

   #if defined(__DEBUG_CDE)
   _DebugView_CDE->Invalidate (   );
   #endif

   return (DONE);
}

}

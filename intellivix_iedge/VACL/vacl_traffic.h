#if !defined(__VACL_TRAFFIC_H)
#define __VACL_TRAFFIC_H

#include "vacl_objtrack.h"

 namespace VACL
 
{

class EventZone;
class EventDetection;

///////////////////////////////////////////////////////////////////////////////
//
// Class: TrafficZone
//
///////////////////////////////////////////////////////////////////////////////

 class TrafficZone
 
{
   protected:
      int Flag_Processed;
   
   public:
      float      AvgRealSpeed;
      float      CongestionTime;
      float      NumVehicles;
      IBox2D     VehicleRegion;
      EventZone* EvtZone;
      
   public:
      TrafficZone* Prev;
      TrafficZone* Next;

   public:
      TrafficZone (   );
      
   public:
      int  DetectEvent     (   );
      void GetTrafficState (ObjectTracking& obj_tracker,float learn_rate);
};

typedef LinkedList<TrafficZone> TrafficZoneList;

///////////////////////////////////////////////////////////////////////////////
//
// Class: TrafficCongestionDetection
//
///////////////////////////////////////////////////////////////////////////////

 class TrafficCongestionDetection
 
{
   protected:
      int Flag_Enabled;
   
   public:
      float LearningRate;
   
   public:
      float   FrameRate;
      ISize2D SrcImgSize;

   public:
      TrafficZoneList TrafficZones;
   
   public:
      TrafficCongestionDetection (   );
      virtual ~TrafficCongestionDetection (   );
   
   protected:
      void Perform (TrackedObjectList& to_list,TrafficZone* zone);
   
   public:
      virtual void Close (   );
   
   public:
      void Enable     (int flag_enable);
      int  Initialize (EventDetection& evt_detector);
      int  IsEnabled  (   );
      int  Perform    (ObjectTracking& obj_tracker);
};

 inline void TrafficCongestionDetection::Enable (int flag_enable)
 
{
   Flag_Enabled = flag_enable;
}

 inline int TrafficCongestionDetection::IsEnabled (  )
 
{
   return (Flag_Enabled);
}

}

#endif

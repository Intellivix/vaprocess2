#if !defined(__VACL_DWELL_H)
#define __VACL_DWELL_H

#include "vacl_objtrack.h"

 namespace VACL

{

class EventZone;
class EventDetection;

///////////////////////////////////////////////////////////////////////////////
//
// Class: DwellZone
//
///////////////////////////////////////////////////////////////////////////////

 class DwellZone

{
   public:
      int        ExitObjectCount; // 현재까지 존을 떠난 객체들의 수
      float      DwellTimeSum;    // 현재까지 존을 떠난 객체들의 체류 시간의 총 합
      IBox2D     EntObjRegion;
      EventZone* EvtZone;
   
   public:
      DwellZone* Prev;
      DwellZone* Next;

   public:
      DwellZone (   );

   public:
      // "Overcrowded" 이벤트를 감지한다.
      int  DetectEvent   (   );
      // 카운터 값들을 초기화한다.
      void ResetCounters (   );
      // NOTE(yhpark): removed. rapidxml.
      // 존 카운터 값들을 저장한다.
      // int  SaveDataFile  (XMLFile& file);
      int  SaveDataFile  (CXMLIO* pIO);
};

typedef LinkedList<DwellZone> DwellZoneList;

///////////////////////////////////////////////////////////////////////////////
//
// Class: DwellAnalysis
//
///////////////////////////////////////////////////////////////////////////////

 class DwellAnalysis

{
   protected:
      int     Flag_Enabled;
      int     FrameCount;
      Time    PrevTime;
      StringA DataFileName; 
   
   public:
      float   FrameRate;
      ISize2D SrcImgSize;

   public:
      DwellZoneList DwellZones;

   public:
      DwellAnalysis (   );
      virtual ~DwellAnalysis (   );

   protected:
      DwellZone* FindZone (int zone_id);
      int  LoadDataFile   (   );
      void Perform        (ObjectTracking& obj_tracker,DwellZone* dw_zone);
      int  SaveDataFile   (   );

   public:
      virtual void Close (   );

   public:
      void Enable            (int flag_enable);
      int  Initialize        (EventDetection& evt_detector,const char* data_file_name = NULL);
      int  IsEnabled         (   );
      int  Perform           (ObjectTracking& obj_tracker);
      void ResetZoneCounters (   );
};

 inline void DwellAnalysis::Enable (int flag_enable)

{
   Flag_Enabled = flag_enable;
}

 inline int DwellAnalysis::IsEnabled (   )

{
   return (Flag_Enabled);
}

}

#endif

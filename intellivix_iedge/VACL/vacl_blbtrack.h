#if !defined(__VACL_BLBTRACK_H)
#define __VACL_BLBTRACK_H

#include "vacl_conrgn.h"
#include "vacl_tgtrack.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Forward Declaration
//
///////////////////////////////////////////////////////////////////////////////

class TrackedObject;

///////////////////////////////////////////////////////////////////////////////
//
// Class: TrkBlobRegion
//
///////////////////////////////////////////////////////////////////////////////

 class TrkBlobRegion : public IBox2D
 
{
   public:
      int RgnID;
      int Status;
  
   public:
      int      Area;
      int      UPArea;
      float    Disorder;
      float    MovingDistance1;
      float    MovingDistance2;
      FPoint2D Velocity;
   
   public:
      TrkBlobRegion* Prev;
      TrkBlobRegion* Next;
};

typedef LinkedList<TrkBlobRegion> TrkBlobRegionList;

///////////////////////////////////////////////////////////////////////////////
//
// Class: TrackedBlob
//
///////////////////////////////////////////////////////////////////////////////

#define TB_STATUS_CREATED          0x00000001
#define TB_STATUS_FILTERED         0x00000002
#define TB_STATUS_IN_EVENT_ZONE    0x00000004
#define TB_STATUS_MERGED           0x00000008
#define TB_STATUS_ON_EVENT         0x00000010
#define TB_STATUS_STATIC           0x00000020
#define TB_STATUS_TO_BE_REMOVED    0x00000040
#define TB_STATUS_UNDETECTED       0x00000080
#define TB_STATUS_VALIDATED        0x00000100
#define TB_STATUS_VERIFIED         0x00000200
#define TB_STATUS_VERIFIED_START   0x00000400

 class TrackedBlob : public ConnectedRegion
 
{
   public:
      int ID;     //
      int RgnID;  // BlobTracking.BlobRegionMap 상에서 해당 블럽의 레이블 값
      int Status; //

   public:
      int   Count_Tracked;
      float Time_InEventZone;
      float Time_Tracked;
      float Time_Triggered;
      float Time_Undetected1;
      float Time_Undetected2;

   public:
      float    MovingDistance1;    // Max_i(Mag(FourCorners[t][i] - FourCorners[0][i]))
      float    MovingDistance2;    // Min_i(Mag(FourCorners[t][i] - FourCorners[0][i]))
      float    MaxMovingDistance1; // Max_t(MovingDistance1[t])
      float    MaxMovingDistance2; // Max_t(MovingDistance2[t])
      float    NorSpeed;           // 100 * Speed / Height
      float    Speed;              // Mag(MeanVelocity) * FrameRate (pixles/sec)
      FPoint2D MeanVelocity;       // Mean(Velocity[t],Velocity[t - 1],...,Velocity[t - n])
      FPoint2D Velocity;           // CenterPos[t] - CenterPos[t - 1]

   public:
      int   UPArea;   // Area of the upper part of a blob
      float Disorder; // Perimeter / (2.0 * sqrt (PI * Area))
      
   public:
      struct BlobColor {
         float B,G,R;
      };
      BlobColor CntBkgColor;
      BlobColor CntFrgColor;
      BlobColor RgnBkgColor;
      BlobColor RgnFrgColor;
   
   public:
      TrackedObject* EvtGenObject;

   public:
      IBox2D            InitRegion;
      IBox2D            CurRegion;
      FPoint2D          StaticPos;
      TrkBlobRegionList TrkBlobRegions;
   
   public:
      TargetTracking_NCC TargetTracker;

   public:
      TrackedBlob* Prev;
      TrackedBlob* Next;
      
   public:
      TrackedBlob (   );
      virtual ~TrackedBlob (   ) {   };
   
   public:
      void GetCenterColors    (BGRImage& s_cimage,BGRImage& b_cimage,BGRColor& f_color,BGRColor& b_color);
      void GetPropertyValues  (float frm_rate);
      int  GetRegionColors    (BGRImage& s_cimage,BGRImage& b_cimage,IArray2D& br_map,BGRColor& f_color,BGRColor& b_color);
      void Initialize         (ConnectedRegion &s_rgn);
      void InitTargetTracker  (GImage& s_image);
      void PutRectMask        (ushort m_value,USArray2D &d_array,int margin = 5);
      void SetEvtGenObject    (TrackedObject* eg_object);
      int  TrackTarget        (GImage& s_image,IBox2D& d_rgn,float& m_score);
      int  UpdateCenterColors (BGRColor& f_color,BGRColor& b_color);
      int  UpdateCRInfo       (IArray2D& cr_map,IBox2D& s_rgn,int cr_no);
      void UpdateRegion       (ConnectedRegion &s_cr,float ud_rate = 1.0f);
      int  UpdateRegionColors (BGRColor& f_color,BGRColor& b_color);
};

typedef Array1D<TrackedBlob*>   TBArray1D;
typedef LinkedList<TrackedBlob> TrackedBlobList;

///////////////////////////////////////////////////////////////////////////////
//
// Class: BRMData
//
///////////////////////////////////////////////////////////////////////////////

 class BRMData

{
  public:
     IArray2D BlobRegionMap;
     
  public:
     BRMData* Prev;
     BRMData* Next; 
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: BlobTracking
//
///////////////////////////////////////////////////////////////////////////////

#define BLT_OPTION_GET_BLOB_DISORDER    0x00000001   // 본 옵션을 사용하려면 BlobRegionMap의 바운더리 값이 0이어야 한다.

 class BlobTracking
 
{
   protected:
      int     Flag_Enabled;
      int     Flag_Initialized;
      int     BlobCount;
      int     FrameCount;
      float   FrameDuration;
      GImage* SrcImage;
      
   protected:
      CRArray1D             BlobRegions;
      LinkedList<BRMData>   BRMList;
      BoundaryPixelTracking BPTracker;
         
   // Input (Set directly)
   public:
      int   Options;             //
      int   MinBlobArea;         //
      int   MaxBlobArea;         //
      int   MinCount_Tracked;    //
      float MaxBRMLLength;       //
      float MaxNorSpeed_Static;  //
      float MaxTime_Undetected1; // TB가 UNDETECTED 상태를 유지할 수 있는 최대 시간 (이벤트 미검출 상태일 때 적용)
      float MaxTime_Undetected2; // TB가 UNDETECTED 상태를 유지할 수 있는 최대 시간 (이벤트 검출 상태일 때 적용)
      float MaxTime_Undetected3; //
      float MaxTime_Verified;    // TB가 최초 검출 후 본 시간이 지나도록 VERIFIED 상태가 안되면 제거된다.
      float MaxTrjLength;        //
      float MinMovingDistance1;  // TB가 VERIFIED 상태가 되려면 TB.MovingDistance1 >= MinMovingDistance1 를 만족하애 한다.
      float MinMovingDistance2;  // TB가 VERIFIED 상태가 되려면 TB.MovingDistance2 >= MinMovingDistance2 를 만족해야 한다.
      float RegionUpdateRate;    //

   // Input (Set indirectly)
   public:
      float   FrameRate;
      ISize2D SrcImgSize;

   // Output
   public:
      IArray2D        BlobRegionMap;
      TrackedBlobList TrackedBlobs;
      
   public:
      BlobTracking (   );
      virtual ~BlobTracking (   );

   protected:
      void  _Init           (   );
      void AddToBRMList     (IArray2D& br_map);
      int  CheckBlobStatic  (TrackedBlob* t_blob);
      void CleanBlobList    (   );
      int  CopyBlobRegion   (IArray2D& s_cr_map,TrackedBlob* t_blob,int s_cr_no,IArray2D& d_cr_map,int d_cr_no,int flag_predict_pos = TRUE);
      int  TrackBlobRegion  (TrackedBlob* t_blob,IArray2D& d_cr_map,int d_cr_no);
      void GetBlobArray     (TBArray1D& d_array);
      void GetBlobDisorder  (TrackedBlob* t_blob);
      void GetBlobUPArea    (TrackedBlob* t_blob);
      int  GetNewBlobID     (   );
      int  Perform          (   );

   protected:
      virtual TrkBlobRegion* AddToTrkBlobRegionList (TrackedBlob *t_blob);
      virtual TrackedBlob*   CreateTrackedBlob      (   );
      virtual TrkBlobRegion* CreateTrkBlobRegion    (   );
      virtual int            FilterTrackedBlob      (TrackedBlob *t_blob) { return (FALSE); };
      virtual int            VerifyTrackedBlob      (TrackedBlob *t_blob);

   public:
      virtual void Close                   (   );
      virtual int  GetBkgModelUpdateScheme (GImage &d_image);
   
   public:
      void ClearBlobEventStatus (   );
      void Enable               (int flag_enable);
      int  Initialize           (ISize2D& si_size,float frm_rate);
      int  IsEnabled            (   );
      int  IsInitialized        (   );
      int  Perform              (GImage& b_image,GImage* s_image = NULL);
      int  Perform              (IArray2D& cr_map,int n_crs,GImage* s_image = NULL);
      int  PutMask              (TrackedBlob* t_blob,ushort d_label,USArray2D& d_array);
      void Reset                (   );
};

 inline void BlobTracking::Enable (int flag_enable)
 
{
   Flag_Enabled = flag_enable;
}

 inline int BlobTracking::IsEnabled (   )

{
   return (Flag_Enabled);
}

 inline int BlobTracking::IsInitialized (   )

{
   return (Flag_Initialized);
}

}

#endif

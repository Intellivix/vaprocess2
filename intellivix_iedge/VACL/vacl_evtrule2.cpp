﻿#include "vacl_objclass.h"
#include "vacl_evtrule.h"
#include "vacl_event.h"

#if defined(__DEBUG_EVD)
#include "Win32CL/Win32CL.h"
extern int _DVX_EVD,_DVY_EVD;
extern CImageView* _DebugView_EVD;
#endif

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Global Variables
//
///////////////////////////////////////////////////////////////////////////////

EventRuleListManager* __ERLM = NULL;

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRule
//
///////////////////////////////////////////////////////////////////////////////

 EventRule::EventRule (   )

{
   if (__ERLM) ID = __ERLM->GetNewID (   );
   else ID = 0;
   Status        = 0;
   Options       = 0;
   EventType     = ER_EVENT_LOITERING;
   ObjectType    = TO_TYPE_HUMAN|TO_TYPE_VEHICLE|TO_TYPE_UNKNOWN;
   PersonAttrs   = 0;
   Schedule_WD   = ER_WEEKDAY_SUN|ER_WEEKDAY_MON|ER_WEEKDAY_TUE|ER_WEEKDAY_WED|ER_WEEKDAY_THU|ER_WEEKDAY_FRI|ER_WEEKDAY_SAT;
   sprintf (Name,"Event Rule #%d",ID);
   AlarmPeriod   = 10.0f;
   EventDetector = NULL;
   ObjectFilter  = CreateInstance_ObjectFiltering (   );
   if (__ERLM) __ERLM->Add (this);
   #if defined(__CPNI_CERT_SZM)
   Options       = ER_OPTION_ENFORCE_ALARM_PERIOD;
   AlarmPeriod   = 1.0f;
   #endif
   #if defined(__KISA_CERT)
   AlarmPeriod   = 300.0f;
   #endif
}

 EventRule::~EventRule (   )

{
   if (ObjectFilter) delete ObjectFilter;
   if (__ERLM) __ERLM->Remove (this);
}

#define ER_PARAM_HMFALL_OBSERVATION_PERIOD                4.5f
#define ER_PARAM_HMFALL_PERIOD_BEFORE_FALLING             0.5f
#define ER_PARAM_HMFALL_PERIOD_AFTER_FALLING              0.5f
#define ER_PARAM_HMFALL_RANGE_INACTIVE                    0.35f
#define ER_PARAM_HMFALL_MIN_ASPECT_RATIO_BEFORE_FALLING   1.5f
#define ER_PARAM_HMFALL_MAX_ASPECT_RATIO_AFTER_FALLING    1.15f

// #define __DEBUG_HUMAN_FALL

 int EventRule::CheckHumanFalling (TrackedObject *t_object)
 
{
   int n;

   if (EventDetector == NULL) return (1);
   if (t_object->Type != TO_TYPE_HUMAN) return (2);
   // TO의 이동 거리가 너무 짧으면 배제한다.
   float md = t_object->GetMovingDistance1 (   );
   float md_thrsld1 = 0.35f * (t_object->Width + t_object->Height); // PARAMETERS
   float md_thrsld2 = 0.1f  * EventDetector->SrcImgSize.Height; // PARAMETERS
   float md_thrsld  = GetMinimum (md_thrsld1,md_thrsld2);
   if (md < md_thrsld) return (3);
   // TO가 쓰러진 후 다른 타입으로 분류되는 것을 막는다.
   if (t_object->AspectRatio < ER_PARAM_HMFALL_MIN_ASPECT_RATIO_BEFORE_FALLING) t_object->Status |= TO_STATUS_HUMAN;
   else t_object->Status &= ~TO_STATUS_HUMAN;
   int nf_1s    = (int)(EventDetector->FrameRate + 0.5f);
   int nf_total = (int)(ER_PARAM_HMFALL_OBSERVATION_PERIOD * nf_1s + 1.5f);
   TrkObjRegionList &tr_list = t_object->TrkObjRegions;
   if (tr_list.GetNumItems (   ) < nf_total) return (4);
   // 쓰러진 후의 기간
   int   en  = (int)(ER_PARAM_HMFALL_PERIOD_AFTER_FALLING * nf_1s);
   int   a1  = 0, by1 = 0, ty1 = 0, np = 0;
   float ar1 = 0.0f, maa1 = 0.0f;
   TrkObjRegion *to_rgn = tr_list.First (   );
   for (n = 0; n < en; n++) {
      if (to_rgn == NULL) break;
      a1   += to_rgn->Width * to_rgn->Height; // 바운딩 박스의 면적
      by1  += to_rgn->Y + to_rgn->Height;     // 바운딩 박스 최하단의 Y좌표
      ty1  += to_rgn->Y;                      // 바운딩 박스 최상단의 Y좌표
      ar1  += to_rgn->AspectRatio;            // 바운딩 박스의 세로/가로 비율
      maa1 += to_rgn->MajorAxisAngle;         // Y축과 객체 블럽의 주축 사이의 각도
      to_rgn = tr_list.Next (to_rgn);
   }
   a1 /= n; by1 /= n; ty1 /= n, ar1 /= (float)n; maa1 /= (float)n;
   // 쓰러지는 중간의 기간
   en = (int)(nf_1s * (ER_PARAM_HMFALL_OBSERVATION_PERIOD - (ER_PARAM_HMFALL_PERIOD_BEFORE_FALLING + ER_PARAM_HMFALL_PERIOD_AFTER_FALLING)));
   for (n = 0; n < en; n++) {
      if (to_rgn == NULL) break;
      to_rgn = tr_list.Next (to_rgn);
   }
   // 쓰러지기 전의 기간
   en = (int)(ER_PARAM_HMFALL_PERIOD_BEFORE_FALLING * nf_1s);
   int   a2 = 0, by2 = 0, ty2 = 0, h2 = 0; 
   float ar2 = 0.0f, maa2 = 0.0f;
   for (n = 0; n < en; n++) {
      if (to_rgn == NULL) break;
      a2   += to_rgn->Width * to_rgn->Height; // 바운딩 박스의 면적
      h2   += to_rgn->Height;                 // 바운딩 박스의 높이
      by2  += to_rgn->Y + to_rgn->Height;     // 바운딩 박스의 최하단의 Y좌표
      ty2  += to_rgn->Y;                      // 바운딩 박스의 최상단의 Y좌표
      ar2  += to_rgn->AspectRatio;            // 바운딩 박스의 세로/가로 비율
      maa2 += to_rgn->MajorAxisAngle;         // Y축과 객체 블럽의 주축 사이의 각도
      np   += to_rgn->NumPersons;             // 사람 수
      to_rgn = tr_list.Next (to_rgn);
   }
   a2 /= n; h2 /= n; by2 /= n; ty2 /= n; ar2 /= (float)n; maa2 /= (float)n;
   
   int   ety = (int)(ty2 + 0.15f * h2); // PARAMETERS
   int   eby = (int)(by2 - 0.35f * h2); // PARAMETERS
   float nhs = (float)np / n;
   float arr = (float)a1 / a2;

   #if defined(__DEBUG_HUMAN_FALL)
   int tgt_id = 1;
   if (t_object->ID == tgt_id) {
      logd ("[Tracked Object #%04d]\n",t_object->ID);
      logd ("- nhs  = %.2f, arr  = %.2f\n",nhs,arr);
      logd ("- by1  = %d, eby = %d, ty1 = %d, ety = %d\n",by1,eby,ty1,ety);
      logd ("- ar2  = %.2f, ar1  = %.2f\n",ar2,ar1);
      logd ("- maa2 = %.2f, maa1 = %.2f\n",maa2,maa1);
   }
   #endif

   //
   // "쓰러짐" 조건 체크
   //

   // 한 사람인가 체크
   if (nhs <= 0.7f || nhs >= 1.5f) { // PARAMETERS
      #if defined(__DEBUG_HUMAN_FALL)
      if (t_object->ID == tgt_id) {
         logd ("- Returned in Step 1\n");
      }
      #endif
      return (5);
   }
   // 쓰러지기 전후의 면적비 체크
   if (arr >= 1.4f) { // PARAMETERS
      #if defined(__DEBUG_HUMAN_FALL)
      if (t_object->ID == tgt_id) {
         logd ("- Returned in Step 2\n");
      }
      #endif
      return (6);
   }
   // 쓰러진 후의 바운딩 박스의 최하단 위치 값 체크
   if (by1 <= eby) {
      #if defined(__DEBUG_HUMAN_FALL)
      if (t_object->ID == tgt_id) {
         logd ("- Returned in Step 3\n");
      }
      #endif
      return (7);
   }
   // 쓰러진 후의 바운딩 박스의 최상단 위치 값 체크
   if (ty1 <= ety) {
      #if defined(__DEBUG_HUMAN_FALL)
      if (t_object->ID == tgt_id) {
         logd ("- Returned in Step 4\n");
      }
      #endif
      return (8);
   }
   // 쓰러지기 전후의 바운딩 박스의 Aspect Ratio 체크
   if (ar2 >= ER_PARAM_HMFALL_MIN_ASPECT_RATIO_BEFORE_FALLING && ar1 <= ER_PARAM_HMFALL_MAX_ASPECT_RATIO_AFTER_FALLING) {
      #if defined(__DEBUG_HUMAN_FALL)
      if (t_object->ID == tgt_id) {
         logd ("- Returned in Step 5 (OK)\n");
      }
      #endif
      return (DONE);
   }
   // 쓰러지기 전후의 주축의 각도 체크
   if (maa2 < 15.0f && maa1 > 45.0f) {
      #if defined(__DEBUG_HUMAN_FALL)
      if (t_object->ID == tgt_id) {
         logd ("- Returned in Step 6 (OK)\n");
      }
      #endif
      return (DONE); // PARAMETERS
   }
   #if defined(__DEBUG_HUMAN_FALL)
   if (t_object->ID == tgt_id) {
      logd ("- Returned in Step 7\n");
   }
   #endif
   return (9);
}

 int EventRule::CheckSchedule (Time &c_time)

{
   if (c_time.Year < 0 || c_time.Month < 0 || c_time.Day < 0 || c_time.Weekday < 0 || c_time.Hour < 0 || c_time.Minute < 0) return (TRUE);
   if (!Schedule_WD) return (FALSE);
   if (Schedule_WD) {
      int mask = 0x00000001 << c_time.Weekday;
      if (!(Schedule_WD & mask)) return (FALSE);
   }
   if (Schedule_YMD.GetNumItems (   )) {
      Period_YMD *period_ymd = Schedule_YMD.First (   );
      while (period_ymd != NULL) {
         if (period_ymd->IsTimeInsidePeriod (c_time.Year,c_time.Month,c_time.Day)) break;
         period_ymd = Schedule_YMD.Next (period_ymd);
      }
      if (period_ymd == NULL) return (FALSE);
   }
   if (Schedule_HM.GetNumItems (   )) {
      Period_HM *period_hm = Schedule_HM.First (   );
      while (period_hm != NULL) {
         if (period_hm->IsTimeInsidePeriod (c_time.Hour,c_time.Minute)) break;
         period_hm = Schedule_HM.Next (period_hm);
      }
      if (period_hm == NULL) return (FALSE);
   }
   return (TRUE);
}

 int EventRule::CheckSetup (EventRule* er)

{
   int ret_flag = 0;
   FileIO buffThis,buffFrom;
   CXMLIO xmlIOThis,xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis,XMLIO_Write);
   xmlIOFrom.SetFileIO (&buffFrom,XMLIO_Write);
   WriteFile (&xmlIOThis);
   er->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom))
      ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength (0);
   buffFrom.SetLength (0);
   if (ret_flag & CHECK_SETUP_RESULT_CHANGED) {
      if (ID != er->ID) ret_flag |= CHECK_SETUP_RESULT_RESTART;
      if (er->EventType != EventType) {
         if (ER_EVENT_SMOKE == EventType || ER_EVENT_SMOKE == er->EventType)
            ret_flag |= CHECK_SETUP_RESULT_RESTART;
         if (ER_EVENT_FLAME == EventType || ER_EVENT_FLAME == er->EventType)
            ret_flag |= CHECK_SETUP_RESULT_RESTART;
         if (ER_EVENT_GATHERING == EventType || ER_EVENT_GATHERING == er->EventType)
            ret_flag |= CHECK_SETUP_RESULT_RESTART;
         if (ER_EVENT_PATH_PASSING == EventType || ER_EVENT_PATH_PASSING == er->EventType)
            ret_flag |= CHECK_SETUP_RESULT_RESTART;
      }
      ret_flag |= ERC_PathPassing.CheckSetup (&er->ERC_PathPassing);
      ret_flag |= ObjectFilter->CheckSetup (er->ObjectFilter);
   }
   return (ret_flag);
}

 int EventRule::DetectEvent_Abandoned (TrackedObject *t_object,EventZone *evt_zone)

{
   if (EventDetector == NULL) return (0);
   int n_objects = 0;
   Event *event = t_object->FindEvent (evt_zone);
   if (event == NULL) {
      if ((t_object->EventStatus & TO_EVENT_ABANDONED) && (t_object->CheckTypeAndAttrs (ObjectType,PersonAttrs) == DONE) && (evt_zone->CheckRegionInsideZone (*t_object) == DONE) && !ObjectFilter->Filter (t_object)) {
         event = CreateInstance_Event (evt_zone,this);
         event->StartTime.GetLocalTime (   );
         t_object->Events.Add (event);
         if (Options & ER_OPTION_ENABLE_COUNTER) event->NumObjects = n_objects = EstimateObjectCount (t_object,evt_zone);
         else n_objects = 1;
      }
   }
   else if (event->EvtRule == this) {
      event->ElapsedTime += EventDetector->FrameDuration;
      switch (event->Status) {
         case EV_STATUS_BEGUN:
            event->Status = EV_STATUS_ONGOING;
            break;
         case EV_STATUS_ONGOING:
            if (event->ElapsedTime > AlarmPeriod || !(t_object->Status & TO_STATUS_STATIC2)) {
               event->EndTime.GetLocalTime (   );
               event->Status = EV_STATUS_ENDED;
            }
            break;
         case EV_STATUS_ENDED:
            event->Status = EV_STATUS_TO_BE_REMOVED;
            t_object->EventStatus &= ~TO_EVENT_ABANDONED;
            t_object->EventStatus |=  TO_EVENT_ABANDONED_END;
            if (t_object->Status & TO_STATUS_STATIC2) EventDetector->PutBkgMask (t_object);
            break;
         default:
            break;
      }
   }
   return (n_objects);
}

 int EventRule::DetectEvent_CarAccident (ObjectTracking &obj_tracker,EventZone *evt_zone)
 
{
   int i;

   if (EventDetector == NULL) return (0);
   int n_objects  = 0;
   // 이벤트 존 내에 있는 사고 차량(정차한 차량) 및 사람 수를 센다.
   int n_cars    = 0;
   int n_persons = 0;
   IPoint2D min_p( 100000, 100000);
   IPoint2D max_p(-100000,-100000);
   TOArray1D to_array(obj_tracker.TrackedObjects.GetNumItems (   ));
   TrackedObject* t_object = obj_tracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED) && (t_object->Status & TO_STATUS_VERIFIED)) {
         if (!(t_object->Status & TO_STATUS_STATIC2)) t_object->EventStatus &= ~TO_EVENT_CAR_ACCIDENT;
         if (evt_zone->CheckRegionInsideZone (*t_object) == DONE && !ObjectFilter->Filter (t_object)) {
            if (t_object->EventStatus & TO_EVENT_CAR_ACCIDENT) {
               to_array[n_cars++] = t_object;
               int sx = t_object->X;
               int sy = t_object->Y;
               int ex = sx + t_object->Width;
               int ey = sy + t_object->Height;
               if (sx < min_p.X) min_p.X = sx;
               if (sy < min_p.Y) min_p.Y = sy;
               if (ex > max_p.X) max_p.X = ex;
               if (ey > max_p.Y) max_p.Y = ey;
            }
            if (t_object->Type == TO_TYPE_HUMAN) n_persons++;
         }
      }
      t_object = obj_tracker.TrackedObjects.Next (t_object);
   }
   // 이벤트 검출을 수행한다.
   int flag_detection = FALSE;
   if (n_cars >= ERC_CarAccident.MinNumCars) {
      if (ERC_CarAccident.Flag_Human) {
         if (n_persons) flag_detection = TRUE;
      }
      else flag_detection = TRUE;
   }
   // 이벤트 처리를 수행한다.
   Event*         m_event   = NULL;
   TrackedObject* eg_object = NULL;
   t_object  = EventDetector->EvtGenObjects.First (   );
   while (t_object != NULL) {
      Event* event = t_object->FindEvent (evt_zone);
      if (event != NULL) {
         m_event   = event;
         eg_object = t_object;
      }
      t_object = EventDetector->EvtGenObjects.Next (t_object);
   }
   if (m_event == NULL) {
      if (flag_detection) {
         eg_object         = CreateInstance_TrackedObject  (   );
         eg_object->ID     = EventDetector->GetNewObjectID (   );
         eg_object->Status = TO_STATUS_VALIDATED | TO_STATUS_VERIFIED;
         eg_object->Type   = TO_TYPE_VEHICLE;
         m_event = CreateInstance_Event  (evt_zone,this);
         m_event->StartTime.GetLocalTime (   );
         eg_object->Events.Add (m_event);
         EventDetector->EvtGenObjects.Add (eg_object);
         n_objects = 1;
      }
   }
   else if (m_event->EvtRule == this) {
      m_event->ElapsedTime += EventDetector->FrameDuration;
      if (flag_detection) {
         switch (m_event->Status) {
            case EV_STATUS_BEGUN:
               m_event->Status = EV_STATUS_ONGOING;
               break;
            case EV_STATUS_ONGOING:
               if (m_event->ElapsedTime > AlarmPeriod) {
                  m_event->EndTime.GetLocalTime (   );
                  m_event->Status = EV_STATUS_ENDED;
               }
               break;
            case EV_STATUS_ENDED:
               m_event->Status = EV_STATUS_AFTER_ENDED;
               for (i = 0; i < n_cars; i++) {
                  to_array[i]->EventStatus &= ~TO_EVENT_CAR_ACCIDENT;
                  to_array[i]->EventStatus |=  TO_EVENT_ABANDONED_END;
               }
               break;
            default:
               break;
         }
      }
      else {
         if (m_event->Status == EV_STATUS_AFTER_ENDED) m_event->Status = EV_STATUS_TO_BE_REMOVED;
         else {
            m_event->Status = EV_STATUS_ENDED | EV_STATUS_TO_BE_REMOVED;
            m_event->EndTime.GetLocalTime (   );
         }
         eg_object->Status |= TO_STATUS_TO_BE_REMOVED;
      }
   }
   if (flag_detection) {
      IBox2D rgn;
      rgn.X      = min_p.X - 5;
      rgn.Y      = min_p.Y - 5;
      rgn.Width  = max_p.X - min_p.X + 10;
      rgn.Height = max_p.Y - min_p.Y + 10;
      CropRegion (obj_tracker.SrcImgSize,rgn);
      eg_object->SetRegion (rgn);
   }
   return (n_objects);
}

 int EventRule::DetectEvent_CarParking (TrackedObject *t_object,EventZone *evt_zone)

{
   if (EventDetector == NULL) return (0);
   int n_objects = 0;
   Event *event = t_object->FindEvent (evt_zone);
   if (event == NULL) {
      if ((t_object->EventStatus & TO_EVENT_CAR_PARKING) && evt_zone->CheckRegionInsideZone (*t_object) == DONE && !ObjectFilter->Filter (t_object)) {
         event = CreateInstance_Event (evt_zone,this);
         event->StartTime.GetLocalTime (   );
         t_object->Events.Add (event);
         if (Options & ER_OPTION_ENABLE_COUNTER) event->NumObjects = n_objects = EstimateObjectCount (t_object,evt_zone);
         else n_objects = 1;
      }
   }
   else if (event->EvtRule == this) {
      event->ElapsedTime += EventDetector->FrameDuration;
      switch (event->Status) {
         case EV_STATUS_BEGUN:
            event->Status = EV_STATUS_ONGOING;
            break;
         case EV_STATUS_ONGOING:
            if (!(t_object->Status & TO_STATUS_STATIC2) || (event->ElapsedTime > AlarmPeriod)) {
               event->EndTime.GetLocalTime (   );
               event->Status = EV_STATUS_ENDED;
            }
            break;
         case EV_STATUS_ENDED:
            event->Status = EV_STATUS_TO_BE_REMOVED;
            t_object->EventStatus &= ~TO_EVENT_CAR_PARKING;
            t_object->EventStatus |=  TO_EVENT_ABANDONED_END;
            break;
         default:
            break;
      }
   }
   return (n_objects);
}

 int EventRule::DetectEvent_DirectionalMotion (TrackedObject *t_object,EventZone *evt_zone)

{
   if (EventDetector == NULL) return (0);
   int n_objects      = 0;
   int flag_detection = FALSE;
   if (evt_zone->CheckRegionInsideZone (*t_object) == DONE && !ObjectFilter->Filter (t_object)) {
      t_object->EventZones |= TO_EVENTZONE_DIRECTIONAL_MOTION;
      if (!(t_object->Status & TO_STATUS_STATIC1)) {
         float angle = MC_DEG2RAD * ERC_DirectionalMotion.Direction;
         FPoint2D v1((float)cos(angle),(float)sin(angle));
         FPoint2D v2 = t_object->GetDisplacement ((int)(EventDetector->FrameRate + 0.5f));
         float m2 = Mag(v2);
         if (m2 > MC_VSV) {
            v2 /= m2;
            angle = MC_RAD2DEG * (float)acos (IP(v1,v2));
            if (angle <= 0.5f * ERC_DirectionalMotion.Range) {
               t_object->EventStatus |= TO_EVENT_DIRECTIONAL_MOTION;
               flag_detection = TRUE;
            }
         }
      }
   }
   if (flag_detection) {
      if (!t_object->Time_DirecMotion) t_object->InitDMRegion = *(IBox2D*)t_object;
      t_object->Time_DirecMotion += EventDetector->FrameDuration;
   }
   Event *event = t_object->FindEvent (evt_zone);
   if (event == NULL) {
      if (flag_detection && (t_object->CheckTypeAndAttrs (ObjectType,PersonAttrs) == DONE) && (t_object->Time_DirecMotion >= ERC_DirectionalMotion.DetectionTime)) {
         float sz1 = 0.5f * (t_object->InitDMRegion.Width + t_object->InitDMRegion.Height);
         float sz2 = 0.5f * (t_object->Width + t_object->Height);
         float sz  = GetMinimum (sz1,sz2);
         FPoint2D cp1 = t_object->InitDMRegion.GetCenterPosition (   );
         FPoint2D cp2 = t_object->GetCenterPosition (   );
         float md  = Mag(cp2 - cp1);
         float md_thrsld = 0.01f * sz * ERC_DirectionalMotion.MinMovingDistance;
         if (md >= md_thrsld) {
            event = CreateInstance_Event (evt_zone,this);
            event->StartTime.GetLocalTime (   );
            t_object->Events.Add (event);
            if (Options & ER_OPTION_ENABLE_COUNTER) event->NumObjects = n_objects = EstimateObjectCount (t_object,evt_zone);
            else n_objects = 1;
         }
      }
   }
   else if (event->EvtRule == this) {
      event->ElapsedTime += EventDetector->FrameDuration;
      switch (event->Status) {
         case EV_STATUS_BEGUN:
            event->Status = EV_STATUS_ONGOING;
            break;
         case EV_STATUS_ONGOING:
            if (!flag_detection) {
               event->BonusTime += EventDetector->FrameDuration;
               if (event->BonusTime >= 1.0f) { // PARAMETERS
                  event->Status = EV_STATUS_ENDED | EV_STATUS_TO_BE_REMOVED;
                  event->EndTime.GetLocalTime (   );
               }
            }
            else event->BonusTime = 0.0f;
            break;
         default:
            break;
      }
   }
   return (n_objects);
}

 int EventRule::DetectEvent_Dwell (TrackedObject *t_object,EventZone *evt_zone)

{
   if (EventDetector == NULL) return (0);
   int n_objects        = 0;
   int flag_detection   = FALSE;
   int flag_inside_zone = FALSE;
   #if defined(__KISA_CERT)
   if (evt_zone->CheckObjectInsideZone (t_object) == DONE ) flag_inside_zone = TRUE;
   #else
   if (evt_zone->CheckRegionInsideZone (*t_object) == DONE) flag_inside_zone = TRUE;
   #endif
   TrackedObject::EvtPrgTime* ep_time = t_object->GetEventProgressTime (evt_zone);
   if ((t_object->CheckTypeAndAttrs (ObjectType,PersonAttrs) == DONE) && flag_inside_zone && !ObjectFilter->Filter (t_object)) {
      t_object->EventZones  |= TO_EVENTZONE_DWELL;
      t_object->EventStatus |= TO_EVENT_DWELL;
      t_object->Time_Dwell  += EventDetector->FrameDuration;
      if (!ep_time->Flag_InZone) ep_time->PrgTime = EventDetector->FrameDuration;
      else ep_time->PrgTime += EventDetector->FrameDuration;
      ep_time->Flag_InZone   = TRUE;
      if (t_object->Status & TO_STATUS_VERIFIED) flag_detection = TRUE;
   }
   else ep_time->Flag_InZone = FALSE;
   // Dwell 이벤트 처리
   Event *event = t_object->FindEvent (evt_zone,ER_EVENT_DWELL);
   if (event == NULL) {
      if (flag_detection) {
         if (ep_time->PrgTime >= ERC_Dwell.DetectionTime) {
            event = CreateInstance_Event (evt_zone,this,ER_EVENT_DWELL);
            event->StartTime.GetLocalTime (   );
            t_object->Events.Add (event);
            if (Options & ER_OPTION_ENABLE_COUNTER) event->NumObjects = n_objects = EstimateObjectCount (t_object,evt_zone);
            else n_objects = 1;
         }
      }
   }
   else if (event->EvtRule == this) {
      event->ElapsedTime += EventDetector->FrameDuration;
      switch (event->Status) {
         case EV_STATUS_BEGUN:
            event->Status = EV_STATUS_ONGOING;
            break;
         case EV_STATUS_ONGOING:
            if (!flag_detection) {
               t_object->EventStatus |= TO_EVENT_DWELL;
               event->BonusTime += EventDetector->FrameDuration;
               if (event->BonusTime >= 0.5f) { // PARAMETERS
                  t_object->EventStatus &= ~TO_EVENT_DWELL;
                  event->Status = EV_STATUS_ENDED | EV_STATUS_TO_BE_REMOVED;
                  event->EndTime.GetLocalTime (   );
               }
            }
            else event->BonusTime = 0.0f;
            break;
         default:
            break;
      }
   }
   // Overwaiting 이벤트 처리
   event = t_object->FindEvent (evt_zone,ER_EVENT_DWELL_OVERWAITING);
   if (event == NULL) {
      if (flag_detection) {
         if (t_object->Time_Dwell >= ERC_Dwell.MaxWaitingTime) {
            event = CreateInstance_Event (evt_zone,this,ER_EVENT_DWELL_OVERWAITING);
            event->StartTime.GetLocalTime (   );
            t_object->Events.Add (event);
         }
      }
   }
   else if (event->EvtRule == this) {
      event->ElapsedTime += EventDetector->FrameDuration;
      switch (event->Status) {
         case EV_STATUS_BEGUN:
            event->Status = EV_STATUS_ONGOING;
            break;
         case EV_STATUS_ONGOING:
            if (!flag_detection) {
               event->BonusTime += EventDetector->FrameDuration;
               if (event->BonusTime >= 0.5f) { // PARAMETERS
                  event->Status = EV_STATUS_ENDED | EV_STATUS_TO_BE_REMOVED;
                  event->EndTime.GetLocalTime (   );
               }
            }
            else event->BonusTime = 0.0f;
            break;
         default:
            break;
      }
   }
   return (n_objects);
}

 int EventRule::DetectEvent_Entering (TrackedObject *t_object,EventZone *evt_zone)
// 이벤트 존으로 진입한 객체가 이벤트 존 내에서 사라진 경우를 감지한다.
{
   if (EventDetector == NULL) return (0);
   int n_objects      = 0;
   int flag_detection = FALSE;
   if ((t_object->CheckTypeAndAttrs (ObjectType,PersonAttrs) == DONE) && (evt_zone->CheckRegionInsideZone (*t_object) == DONE) && !ObjectFilter->Filter (t_object)) {
      t_object->EventZones |= TO_EVENTZONE_ENTERING;
      flag_detection = TRUE;
   }
   Event *event = t_object->FindEvent (evt_zone);
   if (event == NULL) {
      if (flag_detection) {
         if (evt_zone->CheckRegionInsideZone (t_object->InitRegion) != DONE) {
            t_object->EventStatus |= TO_EVENT_ENTERING;
            event = CreateInstance_Event (evt_zone,this);
            event->StartTime.GetLocalTime (   );
            t_object->Events.Add (event);
            if (Options & ER_OPTION_ENABLE_COUNTER) event->NumObjects = n_objects = EstimateObjectCount (t_object,evt_zone);
            else n_objects = 1;
         }
      }
   }
   else if (event->EvtRule == this) {
      event->ElapsedTime += EventDetector->FrameDuration;
      switch (event->Status) {
         case EV_STATUS_BEGUN:
            event->Status = EV_STATUS_ONGOING;
            break;
         case EV_STATUS_ONGOING:
            if (event->ElapsedTime > AlarmPeriod) {
               event->EndTime.GetLocalTime (   );
               event->Status = EV_STATUS_ENDED | EV_STATUS_TO_BE_REMOVED;
               t_object->EventStatus &= ~TO_EVENT_ENTERING;
            }
            break;
         default:
            break;
      }
   }
   return (n_objects);
}

 int EventRule::DetectEvent_Falling (TrackedObject *t_object,EventZone *evt_zone)
 
{
   if (EventDetector == NULL) return (0);
   int n_objects      = 0;
   int flag_detection = FALSE;
   if (t_object->Type == TO_TYPE_HUMAN && evt_zone->CheckRegionInsideZone (*t_object) == DONE && !ObjectFilter->Filter (t_object)) {
      t_object->EventZones |= TO_EVENTZONE_FALLING;
      flag_detection = TRUE;
   }
   Event* event = t_object->FindEvent (evt_zone);
   if (event == NULL) {
      if (flag_detection) {
         if (t_object->EventStatus & TO_EVENT_FALLING) {
            if (t_object->IsInactive (ER_PARAM_HMFALL_RANGE_INACTIVE) && t_object->AspectRatio2 < ER_PARAM_HMFALL_MIN_ASPECT_RATIO_BEFORE_FALLING) {
               t_object->Time_Falling += EventDetector->FrameDuration;
               if (t_object->Time_Falling >= ERC_Falling.DetectionTime) {
                  event = CreateInstance_Event (evt_zone,this);
                  event->StartTime.GetLocalTime (   );
                  t_object->Events.Add (event);
                  n_objects = 1;
               }
            }
            else {
               t_object->EventStatus &= ~TO_EVENT_FALLING;
               t_object->ResetRegion_Inactive (   );
            }
         }
         else {
            t_object->Time_Falling = 0.0f;
            int r_code = CheckHumanFalling (t_object);
            if (r_code == DONE) {
               t_object->Time_Falling += EventDetector->FrameDuration;
               t_object->EventStatus  |= TO_EVENT_FALLING;
               t_object->SetRegion_Inactive (   );
            }
         }
      }
      else t_object->Time_Falling = 0.0f;
   }
   else if (event->EvtRule == this) {
      event->ElapsedTime += EventDetector->FrameDuration;
      switch (event->Status) {
         case EV_STATUS_BEGUN:
            event->Status = EV_STATUS_ONGOING;
            break;
         case EV_STATUS_ONGOING:
            if (event->ElapsedTime > AlarmPeriod && !t_object->IsInactive (ER_PARAM_HMFALL_RANGE_INACTIVE)) {
               event->Status = EV_STATUS_ENDED | EV_STATUS_TO_BE_REMOVED;
               event->EndTime.GetLocalTime (   );
               t_object->EventStatus &= ~TO_EVENT_FALLING;
            }
            break;
         default:
            break;
      }
   }
   return (n_objects);
}

 int EventRule::DetectEvent_Gathering (TrackedGroup *t_group,EventZone *evt_zone,GroupTracking &grp_tracker)
 
{
   int i;
   
   int n_objects      = 0;
   int flag_detection = FALSE;
   if (evt_zone->CheckRegionInsideZone (*t_group) == DONE) {
      for (i = 0; i < t_group->Members.Length; i++) t_group->Members[i]->EventZones |= TO_EVENTZONE_GATHERING;
      if (t_group->NumPersons >= ERC_Gathering.MinNumPersons && (t_group->Status & TB_STATUS_STATIC)) {
         for (i = 0; i < t_group->Members.Length; i++) t_group->Members[i]->EventStatus |= TO_EVENT_GATHERING;
         t_group->Time_Gathering += EventDetector->FrameDuration;
         if (t_group->Time_Gathering >= ERC_Gathering.DetectionTime) flag_detection = TRUE;
      }
      else {
         t_group->Time_Gathering -= EventDetector->FrameDuration;
         if (t_group->Time_Gathering < 0.0f) t_group->Time_Gathering = 0.0f;
      }
   }
   TrackedObject *eg_object = t_group->EvtGenObject;
   if (flag_detection && eg_object == NULL) {
      eg_object         = CreateInstance_TrackedObject  (   );
      eg_object->ID     = EventDetector->GetNewObjectID (   );
      eg_object->Status = TO_STATUS_VALIDATED | TO_STATUS_VERIFIED;
      eg_object->Type   = TO_TYPE_HUMAN;
      EventDetector->EvtGenObjects.Add (eg_object);
      t_group->SetEvtGenObject (eg_object);
   }
   if (eg_object != NULL) {
      IBox2D b_box = t_group->GetBoundingBox (   );
      eg_object->SetRegion (b_box);
      Event *event = eg_object->FindEvent (evt_zone);
      if (event == NULL) {
         if (flag_detection) {
            event = CreateInstance_Event (evt_zone,this);
            event->StartTime.GetLocalTime (   );
            eg_object->Events.Add (event);
            if (Options & ER_OPTION_ENABLE_COUNTER) event->NumObjects = n_objects = t_group->NumPersons;
            else n_objects = 1;
         }
      }
      else if (event->EvtRule == this) {
         event->ElapsedTime += EventDetector->FrameDuration;
         switch (event->Status) {
            case EV_STATUS_BEGUN:
               event->Status = EV_STATUS_ONGOING;
               break;
            case EV_STATUS_ONGOING:
               if (!flag_detection) {
                  event->BonusTime += EventDetector->FrameDuration;
                  if (event->BonusTime >= 2.0f) { // PARAMETERS
                     event->Status = EV_STATUS_ENDED | EV_STATUS_TO_BE_REMOVED;
                     event->EndTime.GetLocalTime (   );
                     eg_object->Status |= TO_STATUS_TO_BE_REMOVED;
                     t_group->SetEvtGenObject (NULL);
                  }
               }
               else event->BonusTime = 0.0f;
               break;
            default:
               break;
         }
      }
   }
   return (n_objects);
}

 int EventRule::DetectEvent_Leaving (TrackedObject *t_object,EventZone *evt_zone)
// 이벤트 존 내에서 출현한 객체가 이벤트 존을 벗어난 경우를 감지한다.
{
   if (EventDetector == NULL) return (0);
   int n_objects      = 0;
   int flag_detection = FALSE;
   TrkObjRegion *p_rgn = t_object->TrkObjRegions.Get (1);
   if (p_rgn == NULL) return (FALSE);
   if ((t_object->CheckTypeAndAttrs (ObjectType,PersonAttrs) == DONE) && (evt_zone->CheckRegionInsideZone (*p_rgn) == DONE) && !ObjectFilter->Filter (t_object)) {
      t_object->EventZones |= TO_EVENTZONE_LEAVING;
      flag_detection = TRUE;
   }
   Event *event = t_object->FindEvent (evt_zone);
   if (event == NULL) {
      if (flag_detection) {
         if (evt_zone->CheckRegionInsideZone (*t_object) != DONE) {
            if (evt_zone->CheckRegionInsideZone (t_object->InitRegion) == DONE) {
               t_object->EventStatus |= TO_EVENT_LEAVING;
               event = CreateInstance_Event (evt_zone,this);
               event->StartTime.GetLocalTime (   );
               t_object->Events.Add (event);
               if (Options & ER_OPTION_ENABLE_COUNTER) event->NumObjects = n_objects = EstimateObjectCount (t_object,evt_zone);
               else n_objects = 1;
            }
         }
      }
   }
   else if (event->EvtRule == this) {
      event->ElapsedTime += EventDetector->FrameDuration;
      switch (event->Status) {
         case EV_STATUS_BEGUN:
            event->Status = EV_STATUS_ONGOING;
            break;
         case EV_STATUS_ONGOING:
            if (event->ElapsedTime > AlarmPeriod) {
               event->EndTime.GetLocalTime (   );
               event->Status = EV_STATUS_ENDED | EV_STATUS_TO_BE_REMOVED;
               t_object->EventStatus &= ~TO_EVENT_LEAVING;
            }
            break;
         default:
            break;
      }
   }
   return (n_objects);
}
 int EventRule::DetectEvent_LineCrossing (TrackedObject *t_object,EventZone *evt_zone)
 
{
   if (EventDetector == NULL) return (0);
   if (ERC_LineCrossing.CrossingLine[0].X < 0) return (0);
   int n_objects      = 0;
   int flag_event     = FALSE;
   int flag_detection = FALSE;
   int p_pos = (int)EventDetector->FrameRate;
   TrkObjRegion* p_rgn = NULL;
   for (   ; p_rgn == NULL && p_pos >= 1; p_pos--) {
      p_rgn = t_object->TrkObjRegions.Get (p_pos);
   }
   if (p_rgn == NULL) return (0);
   FPoint2D cp0 = p_rgn->GetCenterPosition (   );
   if ((t_object->CheckTypeAndAttrs (ObjectType,PersonAttrs) == DONE) && (evt_zone->CheckRegionInsideZone (*t_object) == DONE) && !ObjectFilter->Filter (t_object)) {
      t_object->EventZones |= TO_EVENTZONE_LINE_CROSSING;
      flag_detection = TRUE;
   }
   Event *event = t_object->FindEvent (evt_zone);
   if (event == NULL) {
      if (flag_detection) {
         if (Mag(cp0 - t_object->CenterPos) > 1.0f) {
            FPoint2D scale(1.0f / evt_zone->Scale.X,1.0f / evt_zone->Scale.Y);
            Matrix mat_A(2,2);
            mat_A[0][0] = scale.X * (ERC_LineCrossing.CrossingLine[1].X - ERC_LineCrossing.CrossingLine[0].X);
            mat_A[1][0] = scale.Y * (ERC_LineCrossing.CrossingLine[1].Y - ERC_LineCrossing.CrossingLine[0].Y);
            mat_A[0][1] = cp0.X - t_object->CenterPos.X;
            mat_A[1][1] = cp0.Y - t_object->CenterPos.Y;
            float d = (float)Det(mat_A);
            if (fabs (d) > 1E-10) {
               Matrix vec_b(2);
               vec_b(0) = cp0.X - scale.X * ERC_LineCrossing.CrossingLine[0].X;
               vec_b(1) = cp0.Y - scale.Y * ERC_LineCrossing.CrossingLine[0].Y;
               Matrix vec_t = Inv(mat_A) * vec_b;
               if (0.0f <= vec_t(0) && vec_t(0) <= 1.0f && 0.0f <= vec_t(1) && vec_t(1) <= 1.0f) {
                  if (ERC_LineCrossing.Direction == 2) flag_event = TRUE;
                  else {
                     if      (d < 0.0 && ERC_LineCrossing.Direction == 0) flag_event = TRUE;
                     else if (d > 0.0 && ERC_LineCrossing.Direction == 1) flag_event = TRUE;
                  }
                  if (flag_event) {
                     event = CreateInstance_Event (evt_zone,this);
                     event->StartTime.GetLocalTime (   );
                     t_object->Events.Add (event);
                     t_object->EventStatus |= TO_EVENT_LINE_CROSSING;
                     if (Options & ER_OPTION_ENABLE_COUNTER) event->NumObjects = n_objects = EstimateObjectCount (t_object,evt_zone);
                     else n_objects = 1;
                  }
               }
            }
         }
      }
   }
   else if (event->EvtRule == this) {
      event->ElapsedTime += EventDetector->FrameDuration;
      switch (event->Status) {
         case EV_STATUS_BEGUN:
            event->Status = EV_STATUS_ONGOING;
            break;
         case EV_STATUS_ONGOING:
            if (event->ElapsedTime > AlarmPeriod) {
               event->EndTime.GetLocalTime (   );
               event->Status = EV_STATUS_ENDED | EV_STATUS_TO_BE_REMOVED;
               t_object->EventStatus &= ~TO_EVENT_LINE_CROSSING;
            }
            break;
         default:
            break;
      }
   }
   return (n_objects);
}

 int EventRule::DetectEvent_Loitering (TrackedObject *t_object,EventZone *evt_zone)

{
   if (EventDetector == NULL) return (0);
   int n_objects        = 0;
   int flag_detection   = FALSE;
   int flag_inside_zone = FALSE;
   #if defined(__KISA_CERT)
   if (evt_zone->CheckObjectInsideZone (t_object) == DONE ) flag_inside_zone = TRUE;
   #else
   if (evt_zone->CheckRegionInsideZone (*t_object) == DONE) flag_inside_zone = TRUE;
   #endif
   TrackedObject::EvtPrgTime* ep_time = t_object->GetEventProgressTime (evt_zone);
   if ((t_object->CheckTypeAndAttrs (ObjectType,PersonAttrs) == DONE) && flag_inside_zone && !ObjectFilter->Filter (t_object)) {
      t_object->EventZones     |= TO_EVENTZONE_LOITERING;
      t_object->EventStatus    |= TO_EVENT_LOITERING;
      t_object->Time_Loitering += EventDetector->FrameDuration;
      if (!ep_time->Flag_InZone) ep_time->PrgTime = EventDetector->FrameDuration;
      else ep_time->PrgTime += EventDetector->FrameDuration;
      ep_time->Flag_InZone = TRUE;
      flag_detection = TRUE;
   }
   else ep_time->Flag_InZone = FALSE;
   Event *event = t_object->FindEvent (evt_zone);
   if (event == NULL) {
      if (flag_detection) {
         if (ep_time->PrgTime >= ERC_Loitering.DetectionTime) {
            event = CreateInstance_Event (evt_zone,this);
            event->StartTime.GetLocalTime (   );
            t_object->Events.Add (event);
            if (Options & ER_OPTION_ENABLE_COUNTER) event->NumObjects = n_objects = EstimateObjectCount (t_object,evt_zone);
            else n_objects = 1;
         }
      }
   }
   else if (event->EvtRule == this) {
      event->ElapsedTime += EventDetector->FrameDuration;
      switch (event->Status) {
         case EV_STATUS_BEGUN:
            event->Status = EV_STATUS_ONGOING;
            break;
         case EV_STATUS_ONGOING:
            if (flag_detection) {
               event->BonusTime = 0.0f;
               if ((Options & ER_OPTION_ENFORCE_ALARM_PERIOD) && (event->ElapsedTime > AlarmPeriod)) {
                  event->Status = EV_STATUS_ENDED;
                  event->EndTime.GetLocalTime (   );
               }
            }
            else {
               event->BonusTime += EventDetector->FrameDuration;
               if (event->BonusTime >= 0.5f) {
                  #if defined(__CPNI_CERT_SZM)
                  // 객체가 이벤트 존을 벗어났다가 재진입할 때 이벤트 알람이 재발생하는 것을 막는다.
                  event->Status = EV_STATUS_ENDED;
                  #else
                  // 객체가 이벤트 존을 벗어났다가 재진입할 때 이벤트 알람의 재발생을 허용한다.
                  event->Status = EV_STATUS_ENDED | EV_STATUS_TO_BE_REMOVED;
                  #endif
                  event->EndTime.GetLocalTime (   );
               }
            }
            break;
         case EV_STATUS_ENDED:
            event->Status = EV_STATUS_AFTER_ENDED;
            break;
         case EV_STATUS_AFTER_ENDED:
            #if !defined(__CPNI_CERT_SZM)
            // 객체가 이벤트 존을 벗어났다가 재진입할 떄 이벤트 알람의 재발생을 허용한다.
            if (!flag_detection) event->Status = EV_STATUS_TO_BE_REMOVED;
            #endif
            break;
         default:
            break;
      }
   }
   return (n_objects);
}

 int EventRule::DetectEvent_PathPassing (TrackedObject *t_object,EventZone *evt_zone)

{
   if (EventDetector == NULL) return (0);
   if (ERC_PathPassing.PathZonePtrs.GetNumItems (   ) < 2) return (0);
   int n_objects           = 0;
   int flag_detection     = FALSE;
   int flag_in_event_zone = FALSE;
   Event *event = t_object->FindEvent (evt_zone);
   if (event == NULL) {
      int p_pos = 1;
      if (!(t_object->Status & TO_STATUS_FAST_MOVING_OBJECT)) {
         int   fr = (int)EventDetector->FrameRate;
         float sp = t_object->Speed / fr; // 프레임 간 객체 이동 거리
         if (sp < 1.0f) sp = 1.0f;
         p_pos = (int)(2.0f / sp * fr);   // sp 값이 2일 때 1초 전의 위치 값을 사용하도록 설정
         if (p_pos < 1) p_pos = 1;
         else if (p_pos > fr) p_pos = fr;
      }
      int n_tors = t_object->TrkObjRegions.GetNumItems (   );
      if (p_pos >= n_tors) p_pos = n_tors - 1;
      if ((t_object->CheckTypeAndAttrs (ObjectType,PersonAttrs) == DONE) && !ObjectFilter->Filter (t_object) && p_pos > 0) {
         PathZonePtr *f_zptr = ERC_PathPassing.PathZonePtrs.First (   );
         PathZonePtr *l_zptr = ERC_PathPassing.PathZonePtrs.Last  (   );
         PathZone *c_zone = ERC_PathPassing.ZoneTree.FindPathZone (*t_object);
         if (c_zone != NULL) {
            flag_in_event_zone = TRUE;
         }
         Path *path = t_object->FindPath (evt_zone);
         if (path == NULL) {
            if (c_zone != NULL) {
               int direction = -1;
               if      (c_zone == f_zptr->Zone && ERC_PathPassing.Direction != 1) direction = 0;
               else if (c_zone == l_zptr->Zone && ERC_PathPassing.Direction != 0) direction = 1;
               if (direction != -1) {
                  Path *path = new Path;
                  path->DirectionToPass = direction;
                  path->EvtZone         = evt_zone;
                  if      (direction == 0)  path->CurZonePtr = f_zptr;
                  else if (direction == 1)  path->CurZonePtr = l_zptr;
                  t_object->Paths.Add (path);
               }
            }
         }
         else if (path->EvtZone == evt_zone) {
            int flag_passing = FALSE;
            if (c_zone != NULL) {
               path->Time_Elapsed += EventDetector->FrameDuration;
               if (path->Time_Elapsed <= ERC_PathPassing.PassingTime) {
                  TrkObjRegion *p_rgn = t_object->TrkObjRegions.Get (p_pos);
                  PathZone *p_zone = ERC_PathPassing.ZoneTree.FindPathZone (*p_rgn);           
                  if (c_zone == path->CurZonePtr->Zone || c_zone == p_zone || Mag(p_rgn->GetCenterPosition(   ) - t_object->CenterPos) < 1.0f) 
                     flag_passing = TRUE;
                  else {
                     if (path->DirectionToPass == 0) {
                        PathZonePtr *n_zptr = path->CurZonePtr->Next;
                        if (c_zone == n_zptr->Zone) {
                           if (c_zone == l_zptr->Zone) flag_detection = TRUE;
                           else {
                              path->CurZonePtr = n_zptr;
                              flag_passing = TRUE;
                           }
                        }
                     }
                     else if (path->DirectionToPass == 1) {
                        PathZonePtr *p_zptr = path->CurZonePtr->Prev;
                        if (c_zone == p_zptr->Zone) {
                           if (c_zone == f_zptr->Zone) flag_detection = TRUE;
                           else {
                              path->CurZonePtr = p_zptr;
                              flag_passing = TRUE;
                           }
                        }
                     }
                  }
               }
            }
            if (!flag_passing) {
               t_object->Paths.Remove (path);
               delete path;
            }
         }
         if (flag_detection) {
            TrackedObject::EvtZonePathPassInfo* evt_pp_info = t_object->FindEventZonePathPassingInfo (evt_zone);
            if (!evt_pp_info) {
               TrackedObject::EvtZonePathPassInfo* new_evt_pp_info = new TrackedObject::EvtZonePathPassInfo;
               new_evt_pp_info->EvtZone          = evt_zone;
               new_evt_pp_info->Time_PathPassing = 0.0f;
               t_object->EventZonePathPassingInfoList.Add (new_evt_pp_info);
            }
         }
         TrackedObject::EvtZonePathPassInfo* evt_pp_info = t_object->FindEventZonePathPassingInfo (evt_zone);
         if (evt_pp_info && flag_in_event_zone) {
            evt_pp_info->Time_PathPassing += EventDetector->FrameDuration;
            if (evt_pp_info->Time_PathPassing >= ERC_PathPassing.DetectionTime) {
               event = CreateInstance_Event (evt_zone,this);
               event->StartTime.GetLocalTime (   );
               t_object->Events.Add (event);
               if (Options & ER_OPTION_ENABLE_COUNTER) event->NumObjects = n_objects = EstimateObjectCount (t_object,evt_zone);
               else n_objects = 1;
            }
         }
      }
   }
   else {
      if (event->EvtRule == this) {
         event->ElapsedTime += EventDetector->FrameDuration;
         switch (event->Status) {
            case EV_STATUS_BEGUN:
               event->Status = EV_STATUS_ONGOING;
               break;
            case EV_STATUS_ONGOING:
               if (event->ElapsedTime > AlarmPeriod) {
                  event->EndTime.GetLocalTime (   );
                  event->Status = EV_STATUS_ENDED | EV_STATUS_TO_BE_REMOVED;
                  TrackedObject::EvtZonePathPassInfo* evt_pp_info = t_object->FindEventZonePathPassingInfo (evt_zone);
                  t_object->EventZonePathPassingInfoList.Remove (evt_pp_info);
               }
               break;
            default:
               break;
         }
      }
   }
   return (n_objects);
}

 int EventRule::DetectEvent_Presence (TrackedBlob *t_blob,EventZone *evt_zone,BlobTracking &blb_tracker,float dt_time,float bo_time,int to_type,int flag_cont,int flag_bkg)
// blb_tracker를 통해 현재 추적 중인 블럽들 중에 evt_zone에 속하고 추적 시간이 dt_time 이상
// 되는 블럽들 각각에 대해 그에 대응하는 TrackedObject 객체를 생성하여 to_list에 등록한다.
{
   if (EventDetector == NULL) return (0);
   int n_objects      = 0;
   int flag_detection = FALSE;
   if (!(t_blob->Status & TB_STATUS_TO_BE_REMOVED) &&
       evt_zone->CheckRegionInsideZone (*t_blob) == DONE &&
       !evt_zone->EvtRule->ObjectFilter->Filter (t_blob)) {
      t_blob->Status |= TB_STATUS_IN_EVENT_ZONE;
      t_blob->Time_InEventZone += EventDetector->FrameDuration;
      if (t_blob->Time_InEventZone >= dt_time) flag_detection = TRUE;
   }
   TrackedObject* eg_object = t_blob->EvtGenObject;
   if (flag_detection && eg_object == NULL) {
      if (!EventDetector->IsInsideBkgMask (*t_blob)) {
         eg_object         = CreateInstance_TrackedObject  (   );
         eg_object->ID     = EventDetector->GetNewObjectID (   );
         eg_object->Status = TO_STATUS_VALIDATED | TO_STATUS_VERIFIED;
         eg_object->Type   = to_type;
         EventDetector->EvtGenObjects.Add (eg_object);
         t_blob->SetEvtGenObject (eg_object);
         t_blob->Status |= TB_STATUS_ON_EVENT;
      }
   }
   if (eg_object != NULL) {
      eg_object->SetRegion (*t_blob);
      Event* event = eg_object->FindEvent (evt_zone);
      if (event == NULL) {
         if (flag_detection) {
            event = CreateInstance_Event  (evt_zone,this);
            event->StartTime.GetLocalTime (   );
            eg_object->Events.Add (event);
            n_objects = 1;
         }
      }
      else if (event->EvtRule == this) {
         event->ElapsedTime += EventDetector->FrameDuration;
         switch (event->Status) {
            case EV_STATUS_BEGUN:
               event->Status = EV_STATUS_ONGOING;
               break;
            case EV_STATUS_ONGOING: {
               int flag_evt_end = FALSE;
               if (!flag_detection) {
                  if (t_blob->Status & TB_STATUS_TO_BE_REMOVED) flag_evt_end = TRUE;
                  else {
                     event->BonusTime += EventDetector->FrameDuration;
                     if (event->BonusTime >= bo_time) flag_evt_end = TRUE;
                  }
               }
               else event->BonusTime = 0.0f;
               if (!flag_cont && event->ElapsedTime > AlarmPeriod) flag_evt_end = TRUE;
               if (flag_evt_end) {
                  event->Status = EV_STATUS_ENDED | EV_STATUS_TO_BE_REMOVED;
                  event->EndTime.GetLocalTime (   );
                  eg_object->Status |= TO_STATUS_TO_BE_REMOVED;
                  t_blob->SetEvtGenObject (NULL);
                  t_blob->Status &= ~TB_STATUS_ON_EVENT;
                  if (flag_bkg) EventDetector->PutRectBkgMask (t_blob);
               }
            }  break;
            default:
               break;
         }
      }
/*
      logd ("Object ID: %d\n",eg_object->ID);
      logd ("   Center Foreground Color: R = %03d,G = %03d,B = %03d\n",eg_object->CntFrgColor.R,eg_object->CntFrgColor.G,eg_object->CntFrgColor.B);
      logd ("   Center Background Color: R = %03d,G = %03d,B = %03d\n",eg_object->CntBkgColor.R,eg_object->CntBkgColor.G,eg_object->CntBkgColor.B);
      logd ("   Region Foreground Color: R = %03d,G = %03d,B = %03d\n",eg_object->RgnFrgColor.R,eg_object->RgnFrgColor.G,eg_object->RgnFrgColor.B);
      logd ("   Region Background Color: R = %03d,G = %03d,B = %03d\n",eg_object->RgnBkgColor.R,eg_object->RgnBkgColor.G,eg_object->RgnBkgColor.B);
*/
   }
   return (n_objects);
}

 int EventRule::DetectEvent_Presence (BlobTracking &blb_tracker,EventZone *evt_zone,float dt_time,int to_type)
// blb_tracker를 통해 현재 추적 중인 블럽들 중에 evt_zone에 속하고, 추적 시간이 dt_time 이상
// 되는 블럽들을 묶어서 하나의 TrackedObject 객체를 생성하여 to_list에 등록한다.
{
   if (EventDetector == NULL) return (0);
   int n_objects      = 0;
   int flag_detection = FALSE;
   int to_sx = blb_tracker.SrcImgSize.Width;
   int to_sy = blb_tracker.SrcImgSize.Height;
   int to_ex = -1;
   int to_ey = -1;
   TrackedBlob *t_blob = blb_tracker.TrackedBlobs.First (   );
   while (t_blob != NULL) {
      if (!(t_blob->Status & TB_STATUS_TO_BE_REMOVED) && (t_blob->Status & TB_STATUS_VERIFIED)) {
         if (evt_zone->CheckRegionInsideZone (*t_blob) == DONE && !evt_zone->EvtRule->ObjectFilter->Filter (t_blob)) {
            t_blob->Status |= TB_STATUS_IN_EVENT_ZONE;
            t_blob->Time_InEventZone += EventDetector->FrameDuration;
            if (t_blob->Time_InEventZone >= dt_time) {
               int sx = t_blob->CurRegion.X;
               int sy = t_blob->CurRegion.Y;
               int ex = sx + t_blob->CurRegion.Width  - 1;
               int ey = sy + t_blob->CurRegion.Height - 1;
               if (sx < to_sx) to_sx = sx;
               if (sy < to_sy) to_sy = sy;
               if (ex > to_ex) to_ex = ex;
               if (ey > to_ey) to_ey = ey;
               flag_detection = TRUE;
               t_blob->Status |= TB_STATUS_ON_EVENT;
            }
         }
      }
      t_blob = blb_tracker.TrackedBlobs.Next (t_blob);
   }
   Event *m_event = NULL;
   TrackedObject *eg_object = NULL;
   TrackedObject *t_object  = EventDetector->EvtGenObjects.First (   );
   while (t_object != NULL) {
      Event *event = t_object->FindEvent (evt_zone);
      if (event != NULL) {
         m_event   = event;
         eg_object = t_object;
         break;
      }
      t_object = EventDetector->EvtGenObjects.Next (t_object);
   }
   if (flag_detection) {
      IBox2D rect(to_sx,to_sy,to_ex - to_sx + 1,to_ey - to_sy + 1);
      if (m_event == NULL) {
         eg_object         = CreateInstance_TrackedObject (   );
         eg_object->ID     = EventDetector->GetNewObjectID (   );
         eg_object->Status = TO_STATUS_VALIDATED | TO_STATUS_VERIFIED;
         eg_object->Type   = to_type;
         m_event = CreateInstance_Event (evt_zone,this);
         m_event->StartTime.GetLocalTime (   );
         eg_object->Events.Add (m_event);
         EventDetector->EvtGenObjects.Add (eg_object);
         n_objects = 1;
      }
      else if (m_event->EvtRule == this) {
         m_event->ElapsedTime += EventDetector->FrameDuration;
         if (m_event->Status == EV_STATUS_BEGUN) m_event->Status = EV_STATUS_ONGOING;
      }
      eg_object->SetRegion (rect);
   }
   else if (m_event != NULL && m_event->EvtRule == this) {
      eg_object->Status |= TO_STATUS_TO_BE_REMOVED;
      m_event->EndTime.GetLocalTime (   );
      m_event->Status = EV_STATUS_ENDED | EV_STATUS_TO_BE_REMOVED;
   }
   ERC_Counting.Count += n_objects;
   return (n_objects);
}

 int EventRule::DetectEvent_Removed (TrackedObject *t_object,EventZone *evt_zone)

{
   if (EventDetector == NULL) return (0);
   int n_objects = 0;
   Event *event = t_object->FindEvent (evt_zone);
   if (event == NULL) {
      if ((t_object->EventStatus & TO_EVENT_REMOVED) && (t_object->CheckTypeAndAttrs (ObjectType,PersonAttrs) == DONE) && (evt_zone->CheckRegionInsideZone (*t_object) == DONE) && !ObjectFilter->Filter (t_object)) {
         event = CreateInstance_Event (evt_zone,this);
         event->StartTime.GetLocalTime (   );
         t_object->Events.Add (event);
         if (Options & ER_OPTION_ENABLE_COUNTER) event->NumObjects = n_objects = EstimateObjectCount (t_object,evt_zone);
         else n_objects = 1;
      }
   }
   else if (event->EvtRule == this) {
      event->ElapsedTime += EventDetector->FrameDuration;
      switch (event->Status) {
         case EV_STATUS_BEGUN:
            event->Status = EV_STATUS_ONGOING;
            break;
         case EV_STATUS_ONGOING:
            if (event->ElapsedTime > AlarmPeriod) {
               event->EndTime.GetLocalTime (   );
               event->Status = EV_STATUS_ENDED;
            }
            break;
         case EV_STATUS_ENDED:
            event->Status = EV_STATUS_TO_BE_REMOVED;
            t_object->EventStatus &= ~TO_EVENT_REMOVED;
            t_object->EventStatus |=  TO_EVENT_ABANDONED_END;
            if (t_object->Status & TO_STATUS_STATIC2) EventDetector->PutBkgMask (t_object);
            break;
         default:
            break;
      }
   }
   return (n_objects);
}

 int EventRule::DetectEvent_Stopping (TrackedObject *t_object,EventZone *evt_zone)
// 이동 중인 객체가 정지하면 AlarmPeriod 동안 "멈춤" 이벤트 발생.
// 이벤트 발생 시간이 AlarmPeriod를 지났거나, 도중에 객체가 다시 이동하면 이벤트 종료.
// 이벤트 종료 후 객체가 계속 정지 상태에 있더라도 이벤트 재발생 안함.
// 이벤트가 재발생하려면 일단 객체가 다시 이동했다가 정지해야 함.
{
   if (EventDetector == NULL) return (0);
   int n_objects      = 0;
   int flag_detection = FALSE;
   if ((t_object->CheckTypeAndAttrs (ObjectType,PersonAttrs) == DONE) && (evt_zone->CheckRegionInsideZone (*t_object) == DONE) && !ObjectFilter->Filter (t_object)) {
      t_object->EventZones |= TO_EVENTZONE_STOPPING;
      if ((t_object->Status & TO_STATUS_STATIC1) && (t_object->MaxMovingDistance > 20.0f)) { // PARAMETERS
         t_object->EventStatus |= TO_EVENT_STOPPING;
         flag_detection = TRUE;
      }
   }
   if (flag_detection) t_object->Time_Stopping += EventDetector->FrameDuration;
   Event *event = t_object->FindEvent (evt_zone);
   if (event == NULL) {
      if (flag_detection) {
         if (t_object->Time_Stopping >= ERC_Stopping.DetectionTime) {
            event = CreateInstance_Event (evt_zone,this);
            event->StartTime.GetLocalTime (   );
            t_object->Events.Add (event);
            t_object->SetRegion_Inactive (   );
            if (Options & ER_OPTION_ENABLE_COUNTER) event->NumObjects = n_objects = EstimateObjectCount (t_object,evt_zone);
            else n_objects = 1;
         }
      }
   }
   else if (event->EvtRule == this) {
      event->ElapsedTime += EventDetector->FrameDuration;
      if (t_object->Status & TO_STATUS_STATIC1) {
         switch (event->Status) {
            case EV_STATUS_BEGUN:
               event->Status = EV_STATUS_ONGOING;
               break;
            case EV_STATUS_ONGOING:
               if (event->ElapsedTime > AlarmPeriod) {
                  event->EndTime.GetLocalTime (   );
                  event->Status = EV_STATUS_ENDED;
               }
               break;
            case EV_STATUS_ENDED:
               event->Status = EV_STATUS_AFTER_ENDED;
               break;
            default:
               break;
         }
      }
      else {
         if (event->Status == EV_STATUS_AFTER_ENDED) event->Status = EV_STATUS_TO_BE_REMOVED;
         else {
            event->Status = EV_STATUS_ENDED | EV_STATUS_TO_BE_REMOVED;
            event->EndTime.GetLocalTime (   );
         }
      }
   }
   return (n_objects);
}

 int EventRule::DetectEvent_Violence (TrackedGroup *t_group,EventZone *evt_zone,GroupTracking &grp_tracker)
 
{
   int i;
   
   if (EventDetector == NULL) return (0);
   int   n_objects      = 0;
   int   flag_detection = FALSE;
   float ba_thrsld      = 0.9f;  // PARAMETERS
   float fa_thrsld      = 1.7f;  // PARAMETERS
   float fv_thrsld      = 0.85f; // PARAMETERS
   if (evt_zone->CheckRegionInsideZone (*t_group) == DONE) {
      for (i = 0; i < t_group->Members.Length; i++) t_group->Members[i]->EventZones |= TO_EVENTZONE_VIOLENCE;
      int np_thrsld = 2; // PARAMETERS
      if (t_group->NumPersons      >= np_thrsld &&
          t_group->BoxActivity     >= ba_thrsld &&
          t_group->MaxFlowActivity >= fa_thrsld &&
          t_group->MaxFlowVariance >= fv_thrsld) t_group->Status |= TG_STATUS_VIOLENCE;
      if (t_group->Status & TG_STATUS_VIOLENCE) {
         float w   = 0.8f; // PARAMETERS
         np_thrsld = 1;    // PARAMETERS
         if (!(t_group->NumPersons      >=     np_thrsld &&
               t_group->MaxBoxActivity  >= w * ba_thrsld &&
               t_group->MaxFlowActivity >= w * fa_thrsld &&
               t_group->MaxFlowVariance >= w * fv_thrsld)) t_group->Status &= ~TG_STATUS_VIOLENCE;
      }
      if (t_group->Status & TG_STATUS_VIOLENCE) {
         for (i = 0; i < t_group->Members.Length; i++) t_group->Members[i]->EventStatus |= TO_EVENT_VIOLENCE;
         t_group->Time_Violence += EventDetector->FrameDuration;
         if (t_group->Time_Violence >= ERC_Violence.DetectionTime) flag_detection = TRUE;
      }
      else {
         for (i = 0; i < t_group->Members.Length; i++) t_group->Members[i]->EventStatus &= ~TO_EVENT_VIOLENCE;
         t_group->Time_Violence -= 0.5f * EventDetector->FrameDuration;
         if (t_group->Time_Violence < 0.0f) t_group->Time_Violence = 0.0f;
      }
   }
   TrackedObject* eg_object = t_group->EvtGenObject;
   if (flag_detection && eg_object == NULL) {
      eg_object         = CreateInstance_TrackedObject  (   );
      eg_object->ID     = EventDetector->GetNewObjectID (   );
      eg_object->Status = TO_STATUS_VALIDATED | TO_STATUS_VERIFIED;
      eg_object->Type   = TO_TYPE_HUMAN;
      EventDetector->EvtGenObjects.Add (eg_object);
      t_group->SetEvtGenObject (eg_object);
   }
   if (eg_object != NULL) {
      if (t_group->Members.Length) {   
         IBox2D b_box = t_group->GetBoundingBox (   );
         eg_object->SetRegion (b_box);
      }
      Event *event = eg_object->FindEvent (evt_zone);
      if (event == NULL) {
         if (flag_detection) {
            event = CreateInstance_Event (evt_zone,this);
            event->StartTime.GetLocalTime (   );
            eg_object->Events.Add (event);
            if (Options & ER_OPTION_ENABLE_COUNTER) event->NumObjects = n_objects = t_group->NumPersons;
            else n_objects = 1;
         }
      }
      else if (event->EvtRule == this) {
         event->ElapsedTime += EventDetector->FrameDuration;
         switch (event->Status) {
            case EV_STATUS_BEGUN:
               event->Status = EV_STATUS_ONGOING;
               break;
            case EV_STATUS_ONGOING:
               if (event->ElapsedTime > AlarmPeriod) {
                  if (!flag_detection) {
                     event->Status = EV_STATUS_ENDED | EV_STATUS_TO_BE_REMOVED;
                     event->EndTime.GetLocalTime (   );
                     eg_object->Status |= TO_STATUS_TO_BE_REMOVED;
                     t_group->SetEvtGenObject (NULL);
                     t_group->Time_Violence = 0.0f;
                  }
               }
               break;
            default:
               break;
         }
      }
   }
   return (n_objects);
}

 int EventRule::DetectEvents (GroupTracking &grp_tracker,EventZone *evt_zone)
 
{
   int n_objects = 0;
   switch (EventType) {
      case ER_EVENT_VIOLENCE:
         n_objects = DetectEvents_Violence (grp_tracker,evt_zone);
         break;
      case ER_EVENT_GATHERING:
         n_objects = DetectEvents_Gathering (grp_tracker,evt_zone);
         break;
      default:
         break;
   }
   ERC_Counting.Count += n_objects;
   return (n_objects);
}

 int EventRule::DetectEvents (ObjectTracking &obj_tracker,EventZone *evt_zone)

{
   int n_objects = 0;
   switch (EventType) {
      case ER_EVENT_ABANDONED:
         n_objects = DetectEvents_Abandoned (obj_tracker,evt_zone);
         break;
      case ER_EVENT_CAR_ACCIDENT:
         n_objects = DetectEvent_CarAccident (obj_tracker,evt_zone);
         break;
      case ER_EVENT_CAR_PARKING:
         n_objects = DetectEvents_CarParking (obj_tracker,evt_zone);
         break;
      case ER_EVENT_CAR_STOPPING:
         n_objects = DetectEvents_CarStopping (obj_tracker,evt_zone);
         break;
      case ER_EVENT_DIRECTIONAL_MOTION:
         n_objects = DetectEvents_DirectionalMotion (obj_tracker,evt_zone);
         break;
      case ER_EVENT_DWELL:
         n_objects = DetectEvents_Dwell (obj_tracker,evt_zone);
         break;
      case ER_EVENT_ENTERING:
         n_objects = DetectEvents_Entering (obj_tracker,evt_zone);
         break;
      case ER_EVENT_FALLING:
         n_objects = DetectEvents_Falling (obj_tracker,evt_zone);
         break;
      case ER_EVENT_LEAVING:
         n_objects = DetectEvents_Leaving (obj_tracker,evt_zone);
         break;
      case ER_EVENT_LINE_CROSSING:
         n_objects = DetectEvents_LineCrossing (obj_tracker,evt_zone);
         break;
      case ER_EVENT_LOITERING:
         n_objects = DetectEvents_Loitering (obj_tracker,evt_zone);
         break;
      case ER_EVENT_PATH_PASSING:
         n_objects = DetectEvents_PathPassing (obj_tracker,evt_zone);
         break;
      case ER_EVENT_REMOVED:
         n_objects = DetectEvents_Removed (obj_tracker,evt_zone);
         break;
      case ER_EVENT_STOPPING:
         n_objects = DetectEvents_Stopping (obj_tracker,evt_zone);
         break;
      case ER_EVENT_CAMERA_DAMAGE:
         break;
      default:
         break;
   }
   ERC_Counting.Count += n_objects;
   return (n_objects);
}

 int EventRule::DetectEvents_Abandoned (ObjectTracking &obj_tracker,EventZone *evt_zone)
 
{
   int n_objects = 0;
   TrackedObject *t_object = obj_tracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED) && (t_object->Status & TO_STATUS_VALIDATED)) {
         n_objects += DetectEvent_Abandoned (t_object,evt_zone);
      }
      t_object = obj_tracker.TrackedObjects.Next (t_object);
  }
  return (n_objects);
}

 int EventRule::DetectEvents_CarParking (ObjectTracking &obj_tracker,EventZone *evt_zone)
 
{
   int n_objects = 0;
   TrackedObject *t_object = obj_tracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED) && (t_object->Status & TO_STATUS_VALIDATED)) {
         n_objects += DetectEvent_CarParking (t_object,evt_zone);
      }
      t_object = obj_tracker.TrackedObjects.Next (t_object);
  }
  return (n_objects);
}

 int EventRule::DetectEvents_CarStopping (ObjectTracking &obj_tracker,EventZone *evt_zone)
 
{
   int i,j;

   #if defined(__DEBUG_EVD)
   if (obj_tracker.ForegroundDetector) {
      BGRImage* s_cimage = obj_tracker.ForegroundDetector->SrcColorImage;
      if (s_cimage != NULL) _DebugView_EVD->DrawImage (*s_cimage,_DVX_EVD,_DVY_EVD);
   }
   #endif
   int n_objects = 0;
   if (EventDetector == NULL) return (0);
   TrackedObjectList& to_list = obj_tracker.TrackedObjects;
   int n_tos = to_list.GetNumItems (   );
   if (!n_tos) return (0);
   // 서행 또는 정지한 객체들의 배열을 얻는다.
   TOArray1D to_array1(n_tos);
   n_tos = 0;
   TrackedObject* t_object = to_list.First (   );
   while (t_object != NULL) {
      t_object->Status &= ~TO_STATUS_REPRESENTATIVE;
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED) && (t_object->Status & TO_STATUS_VERIFIED)) {
         if ((t_object->MaxMovingDistance > 20.0f) && (t_object->Speed < 50.0f)) {
            
            // MaxMovingDistance > 20.0f => 이 조건이 문제가 될 수도 있다.
            // 대형 트럭이 옆 차선을 훓고 지나가는 경우, 옆 차선에 정지되어 있던 차량은 새로 나타난 차량이 되므로...

            if ((t_object->Type == TO_TYPE_VEHICLE) || (t_object->Type == TO_TYPE_HUMAN)) {
               if (evt_zone->CheckRegionInsideZone (*t_object) == DONE && !ObjectFilter->Filter (t_object)) {
                  t_object->EventZones |= TO_EVENTZONE_STOPPING;
                  if (t_object->Type == TO_TYPE_VEHICLE) t_object->EventZones |= TO_EVENTZONE_CAR_STOPPING;
                  to_array1[n_tos++] = t_object;
                  #if defined(__DEBUG_EVD)
                  _DebugView_EVD->DrawRectangle (_DVX_EVD + t_object->X,_DVY_EVD + t_object->Y,t_object->Width,t_object->Height,PC_BLUE,2);
                  #endif
               }
            }
         }
      }
      t_object = to_list.Next (t_object);
   }
   if (n_tos) {
      TOArray1D to_array2 = to_array1.Extract (0,n_tos);
      // 객체 별로 "정지" 상태를 체크한다.
      for (i = 0; i < to_array2.Length; i++) {
         t_object = to_array2[i];
         if (t_object->Status & TO_STATUS_STATIC1) {
            t_object->Time_Stopping += EventDetector->FrameDuration;
            t_object->EventStatus |= TO_EVENT_STOPPING;
         }
      }
      // 인접한 객체들의 그룹핑을 위한 맵을 생성한다.
      ISize2D& si_size = obj_tracker.SrcImgSize;
      GImage s_image(si_size.Width,si_size.Height);
      s_image.Clear (   );
      for (i = 0; i < to_array2.Length; i++) {
         t_object = to_array2[i];
         t_object->PutRectMask (t_object->X,t_object->Y,PG_WHITE,s_image,5);
      }
      IArray2D cr_map(s_image.Width,s_image.Height);
      int n_crs = LabelCRs (s_image,cr_map);
      // 그룹 별로 중심점이 가장 밑에 있는 차량 객체(그룹 별 대표 차량 객체)를 찾는다.
      TOArray1D vo_array(n_crs + 1);
      vo_array.Clear (   );
      for (i = 0; i < to_array2.Length; i++) {
         t_object = to_array2[i];
         if (t_object->Type == TO_TYPE_VEHICLE) {
            FPoint2D cp = t_object->GetCenterPosition (   );
            int gn = cr_map[(int)cp.Y][(int)cp.X];
            if (gn) {
               if (vo_array[gn] == NULL) vo_array[gn] = t_object;
               else {
                  FPoint2D p1 = t_object->GetPosition (0.5f,0.8f);
                  FPoint2D p2 = vo_array[gn]->GetPosition (0.5f,0.8f);
                  if (p1.Y > p2.Y) vo_array[gn] = t_object;
               }
            }
         }
      }
      for (i = 0; i < vo_array.Length; i++) {
         if (vo_array[i] != NULL) vo_array[i]->Status |= TO_STATUS_REPRESENTATIVE;
      }
      // "차량 정지" 이벤트 발생 후보 객체들을 모은다.
      TOArray1D to_array3(to_array2.Length);
      n_tos = 0;
      int y_thrsld = (int)(0.5f * obj_tracker.SrcImgSize.Height); // PARAMETERS
      for (i = 0; i < to_array2.Length; i++) {
         t_object = to_array2[i];
         // 정적 객체들 중에 그룹의 대표 차량 객체이거나 영상의 하단에 위치하는 객체인 경우
         if (t_object->Status & TO_STATUS_STATIC1) {
            if (t_object->Status & TO_STATUS_REPRESENTATIVE) to_array3[n_tos++] = t_object;
            else {
               FPoint2D p = t_object->GetPosition (0.5f,0.8f);
               if (p.Y > y_thrsld) to_array3[n_tos++] = t_object;
            }
         }
      }
      if (n_tos) {
         // 이벤트 발생 후보 객체가 실제로 이벤트를 발생시켰는지 검사한다.
         TOArray1D to_array4 = to_array3.Extract (0,n_tos);
         for (i = 0; i < to_array4.Length; i++) {
            t_object = to_array4[i];
            #if defined(__DEBUG_EVD)
            _DebugView_EVD->DrawRectangle (_DVX_EVD + t_object->X,_DVY_EVD + t_object->Y,t_object->Width,t_object->Height,PC_RED,2);
            #endif
            t_object->Time_CarStopping += EventDetector->FrameDuration;
            t_object->EventStatus |= TO_EVENT_CAR_STOPPING;
            if (t_object->Time_CarStopping > ERC_CarStopping.DetectionTime) {
               Event* event = t_object->FindEvent (evt_zone);
               if (event == NULL) {
                  event = CreateInstance_Event  (evt_zone,this);
                  event->StartTime.GetLocalTime (   );
                  t_object->Events.Add (event);
                  t_object->SetRegion_Inactive (   );
                  if (Options & ER_OPTION_ENABLE_COUNTER) {
                     event->NumObjects = EstimateObjectCount (t_object,evt_zone);
                     n_objects += event->NumObjects;
                  }
                  else n_objects++;
                  // TO와 같은 그룹에 속하는 객체들 중에 "정지" 이벤트 조건을 만족하는 객체들을 찾아 이벤트를 발생시킨다.
                  FPoint2D cp1 = t_object->GetCenterPosition (   );
                  int gn1 = cr_map[(int)cp1.Y][(int)cp1.X];
                  if (gn1) {
                     for (j = 0; j < to_array2.Length; j++) {
                        TrackedObject* t_object2 = to_array2[j];
                        if (t_object != t_object2) {
                           FPoint2D cp2 = t_object2->GetCenterPosition (   );
                           int gn2 = cr_map[(int)cp2.Y][(int)cp2.X];
                           if (gn1 == gn2) {
                              if ((t_object2->EventStatus & TO_EVENT_STOPPING) && (t_object2->Time_Stopping > (ERC_CarStopping.DetectionTime - 1.0f))) {
                                 float d_thrsld = 3.0f * 0.5f * (t_object->Width + t_object->Height); // PARAMETERS
                                 float d = Mag(cp1 - cp2);
                                 if (d < d_thrsld) {
                                    event = t_object2->FindEvent (evt_zone);
                                    if (event == NULL) {
                                       event = CreateInstance_Event  (evt_zone,this);
                                       event->StartTime.GetLocalTime (   );
                                       t_object2->Events.Add (event);
                                       t_object2->SetRegion_Inactive (   );
                                       if (Options & ER_OPTION_ENABLE_COUNTER) {
                                          event->NumObjects = EstimateObjectCount (t_object2,evt_zone);
                                          n_objects += event->NumObjects;
                                       }
                                       else n_objects++;
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }
      #if defined(__DEBUG_EVD)
      _DVY_EVD += EventDetector->SrcImgSize.Height;
      _DebugView_EVD->DrawText ("Slow-Going Vehicle Detection Result",_DVX_EVD,_DVY_EVD,PC_GREEN);
      _DVY_EVD += 20;
      _DebugView_EVD->DrawImage (cr_map,_DVX_EVD,_DVY_EVD);
      _DVY_EVD += cr_map.Height;
      _DebugView_EVD->DrawText ("Region Map for Vehicle Grouping",_DVX_EVD,_DVY_EVD,PC_GREEN);
      _DVY_EVD += 20;
      #endif
      // 객체 분류에 대한 특수 처리를 한다.
      // 사람이라고 분류된 TO가 속한 그룹에 두 대 이상의 서행하는 차량 객체가 포함되어
      // 있으면 TO도 차량으로 분류하도록 강제한다.
      for (i = 0; i < to_array2.Length; i++) {
         t_object = to_array2[i];
         if ((t_object->Type == TO_TYPE_HUMAN) || (t_object->Status & TO_STATUS_VEHICLE)) {
            FPoint2D cp = t_object->GetCenterPosition (   );
            int gn1   = cr_map[(int)cp.Y][(int)cp.X];
            int n_vos = 0;
            for (j = 0; j < to_array2.Length; j++) {
               TrackedObject* t_object2 = to_array2[j];
               if ((t_object != t_object2) && (t_object2->Type == TO_TYPE_VEHICLE) && !(t_object2->Status & TO_STATUS_STATIC1)) {
                  cp = t_object2->GetCenterPosition (   );
                  int gn2 = cr_map[(int)cp.Y][(int)cp.X];
                  if (gn1 == gn2) n_vos++;
               }
            }
            if ((t_object->Status & TO_STATUS_VEHICLE) && (n_vos < 2)) t_object->Status &= ~TO_STATUS_VEHICLE;
            if ((t_object->Type == TO_TYPE_HUMAN) && (n_vos >= 2)) {
               memset (t_object->OTLikelihood,0,sizeof(t_object->OTLikelihood));
               t_object->OTLikelihood[OC_TYPE_VEHICLE] = 1.0f;
               t_object->Type    = TO_TYPE_VEHICLE;
               t_object->Status |= TO_STATUS_VEHICLE;
            }
         }
      }
   }
   // 기 발생한 "차량 정지" 이벤트들의 진행 상태를 관리한다.
   t_object = to_list.First (   );
   while (t_object != NULL) {
      Event* event = t_object->FindEvent (evt_zone);
      if (event != NULL && event->EvtRule == this) {
         if (t_object->Status & TO_STATUS_STATIC1) {
            switch (event->Status) {
               case EV_STATUS_BEGUN:
                  if (event->ElapsedTime) event->Status = EV_STATUS_ONGOING;
                  break;
               case EV_STATUS_ONGOING:
                  if (event->ElapsedTime > AlarmPeriod) {
                     event->EndTime.GetLocalTime (   );
                     event->Status = EV_STATUS_ENDED;
                  }
                  break;
               case EV_STATUS_ENDED:
                  event->Status = EV_STATUS_AFTER_ENDED;
                  break;
               default:
                  break;
            }
         }
         else {
            if (event->Status == EV_STATUS_AFTER_ENDED) event->Status = EV_STATUS_TO_BE_REMOVED;
            else {
               event->Status = EV_STATUS_ENDED | EV_STATUS_TO_BE_REMOVED;
               event->EndTime.GetLocalTime (   );
            }
         }
         event->ElapsedTime += EventDetector->FrameDuration;
      }
      t_object = to_list.Next (t_object);
   }
   return (n_objects);
}

 int EventRule::DetectEvents_DirectionalMotion (ObjectTracking &obj_tracker,EventZone *evt_zone)

{
   int n_objects = 0;
   TrackedObject *t_object = obj_tracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED) && (t_object->Status & TO_STATUS_VERIFIED)) {
         n_objects += DetectEvent_DirectionalMotion (t_object,evt_zone);
      }
      t_object = obj_tracker.TrackedObjects.Next (t_object);
   }
   return (n_objects);
}

 int EventRule::DetectEvents_Dwell (ObjectTracking &obj_tracker,EventZone *evt_zone)

{
   int n_objects = 0;
   TrackedObject *t_object = obj_tracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED)) {
         n_objects += DetectEvent_Dwell (t_object,evt_zone);
      }
      t_object = obj_tracker.TrackedObjects.Next (t_object);
   }
   return (n_objects);
}

 int EventRule::DetectEvents_Entering (ObjectTracking &obj_tracker,EventZone *evt_zone)

{
   int n_objects = 0;
   TrackedObject *l_object = obj_tracker.LostObjects.First (   );
   while (l_object != NULL) {
      // [주의] if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED)) { } => 이거 하면 안된다.
      n_objects += DetectEvent_Entering (l_object,evt_zone);
      l_object = obj_tracker.LostObjects.Next (l_object);
   }
   return (n_objects);
}

 int EventRule::DetectEvents_Falling (ObjectTracking &obj_tracker,EventZone *evt_zone)
 
{
   int n_objects = 0;
   TrackedObject *t_object = obj_tracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED) && (t_object->Status & TO_STATUS_VERIFIED)) {
         n_objects += DetectEvent_Falling (t_object,evt_zone);
      }
      t_object = obj_tracker.TrackedObjects.Next (t_object);
   }
   return (n_objects);
}

 int EventRule::DetectEvents_Gathering (GroupTracking &grp_tracker,EventZone *evt_zone)
 
{
   int n_objects = 0;
   TrackedGroup *t_group = (TrackedGroup*)grp_tracker.TrackedBlobs.First (   );
   while (t_group != NULL) {
      if (!(t_group->Status & TB_STATUS_TO_BE_REMOVED) && (t_group->Status & TB_STATUS_VERIFIED)) {
         n_objects += DetectEvent_Gathering (t_group,evt_zone,grp_tracker);
      }
      t_group = (TrackedGroup*)grp_tracker.TrackedBlobs.Next (t_group);
   }
   return (n_objects);
}

 int EventRule::DetectEvents_Leaving (ObjectTracking &obj_tracker,EventZone *evt_zone)

{
   int n_objects = 0;
   TrackedObject *t_object = obj_tracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED) && (t_object->Status & TO_STATUS_VERIFIED)) {
         n_objects += DetectEvent_Leaving (t_object,evt_zone);
      }
      t_object = obj_tracker.TrackedObjects.Next (t_object);
   }
   return (n_objects);
}

 int EventRule::DetectEvents_LineCrossing (ObjectTracking &obj_tracker,EventZone *evt_zone)

{
   int n_objects = 0;
   TrackedObject *t_object = obj_tracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED) && (t_object->Status & TO_STATUS_VERIFIED)) {
         n_objects += DetectEvent_LineCrossing (t_object,evt_zone);
      }
      t_object = obj_tracker.TrackedObjects.Next (t_object);
   }
   return (n_objects);
}

 int EventRule::DetectEvents_Loitering (ObjectTracking &obj_tracker,EventZone *evt_zone)

{
   int n_objects = 0;
   TrackedObject *t_object = obj_tracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED) && (t_object->Status & TO_STATUS_VERIFIED)) {
         n_objects += DetectEvent_Loitering (t_object,evt_zone);
      }
      t_object = obj_tracker.TrackedObjects.Next (t_object);
   }
   return (n_objects);
}

 int EventRule::DetectEvents_PathPassing (ObjectTracking &obj_tracker,EventZone *evt_zone)

{
   int n_objects = 0;
   TrackedObject *t_object = obj_tracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED) && (t_object->Status & TO_STATUS_VERIFIED)) {
         n_objects += DetectEvent_PathPassing (t_object,evt_zone);
      }
      t_object = obj_tracker.TrackedObjects.Next (t_object);
   }
   return (n_objects);
}

 int EventRule::DetectEvents_Presence (BlobTracking &blb_tracker,EventZone *evt_zone,float dt_time,float bo_time,int to_type,int flag_cont,int flag_bkg)
 
{
   int n_objects = 0;
   TrackedBlob* t_blob = blb_tracker.TrackedBlobs.First (   );
   while (t_blob != NULL) {
      if (!(t_blob->Status & TB_STATUS_TO_BE_REMOVED) && (t_blob->Status & TB_STATUS_VERIFIED)) {
         n_objects += DetectEvent_Presence (t_blob,evt_zone,blb_tracker,dt_time,bo_time,to_type,flag_cont,flag_bkg);
      }
      t_blob = blb_tracker.TrackedBlobs.Next (t_blob);
   }
   return (n_objects);
}

 int EventRule::DetectEvents_Removed (ObjectTracking &obj_tracker,EventZone *evt_zone)
 
{
   int n_objects = 0;
   TrackedObject *t_object = obj_tracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED) && (t_object->Status & TO_STATUS_VALIDATED)) {
         n_objects += DetectEvent_Removed (t_object,evt_zone);
      }
      t_object = obj_tracker.TrackedObjects.Next (t_object);
  }
  return (n_objects);
}

 int EventRule::DetectEvents_Stopping (ObjectTracking &obj_tracker,EventZone *evt_zone)

{
   int n_objects = 0;
   TrackedObject *t_object = obj_tracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED) && (t_object->Status & TO_STATUS_VERIFIED)) {
         n_objects += DetectEvent_Stopping (t_object,evt_zone);
      }
      t_object = obj_tracker.TrackedObjects.Next (t_object);
   }
   return (n_objects);
}

 int EventRule::DetectEvents_Violence (GroupTracking &grp_tracker,EventZone *evt_zone)
 
{
   int n_objects = 0;
   TrackedGroup *t_group = (TrackedGroup*)grp_tracker.TrackedBlobs.First (   );
   while (t_group != NULL) {
      if (!(t_group->Status & TB_STATUS_TO_BE_REMOVED) && (t_group->Status & TB_STATUS_VERIFIED)) {
         n_objects += DetectEvent_Violence (t_group,evt_zone,grp_tracker);
      }
      t_group = (TrackedGroup*)grp_tracker.TrackedBlobs.Next (t_group);
   }
   return (n_objects);
}

 int EventRule::EstimateObjectCount (TrackedObject *t_object,EventZone *evt_zone)
 
{
   if (ERC_Counting.MajorAxis[0].X == -1) return (1);
   FPoint2D scale(1.0f / evt_zone->Scale.X,1.0f / evt_zone->Scale.Y);
   IPoint2D d  = ERC_Counting.MajorAxis[1] - ERC_Counting.MajorAxis[0];
   float width = (float)sqrt (scale.X * scale.X * d.X * d.X + scale.Y * scale.Y * d.Y * d.Y);
   float area  = ERC_Counting.AspectRatio * width * width;
   float n_objects = (float)t_object->Area / area + 0.5f;
   if (n_objects <= 0.75f) return (0);
   if (n_objects <= 1.0f ) return (1);
   else return ((int)n_objects);
}

 int EventRule::ReadFile (FileIO &file)
 
{
   int i,n_items;
   
   if (file.Read ((byte*)&ID         ,1,sizeof(int  ))) return (1);
   if (file.Read ((byte*)&Options    ,1,sizeof(int  ))) return (2);
   if (file.Read ((byte*)&EventType  ,1,sizeof(int  ))) return (3);
   if (file.Read ((byte*)&ObjectType ,1,sizeof(int  ))) return (4);
   if (file.Read ((byte*)&PersonAttrs ,1,sizeof(int  ))) return (5);
   if (file.Read ((byte*)&Schedule_WD,1,sizeof(int  ))) return (6);
   if (file.Read ((byte*)&AlarmPeriod,1,sizeof(float))) return (7);
   if (file.Read ((byte*)Name        ,1,sizeof(Name ))) return (8);
   if (ERC_Abandoned.ReadFile         (file)) return (11);
   if (ERC_CarAccident.ReadFile       (file)) return (12);
   if (ERC_CarParking.ReadFile        (file)) return (13);
   if (ERC_CarStopping.ReadFile       (file)) return (14);
   if (ERC_ColorChange.ReadFile       (file)) return (15);
   if (ERC_Counting.ReadFile          (file)) return (16);
   if (ERC_CrowdDensity.ReadFile      (file)) return (17);
   if (ERC_DirectionalMotion.ReadFile (file)) return (18);
   if (ERC_Dwell.ReadFile             (file)) return (19);
   if (ERC_Falling.ReadFile           (file)) return (20);
   if (ERC_Flame.ReadFile             (file)) return (21);
   if (ERC_Gathering.ReadFile         (file)) return (22);
   if (ERC_LineCrossing.ReadFile      (file)) return (23);
   if (ERC_Loitering.ReadFile         (file)) return (24);
   if (ERC_PathPassing.ReadFile       (file)) return (25);
   if (ERC_Removed.ReadFile           (file)) return (26);
   if (ERC_Smoke.ReadFile             (file)) return (27);
   if (ERC_Stopping.ReadFile          (file)) return (28);
   if (ERC_TrafficCongestion.ReadFile (file)) return (29);
   if (ERC_Violence.ReadFile          (file)) return (30);
   if (ERC_WaterLevel.ReadFile        (file)) return (31);
   if (ERC_ZoneColor.ReadFile         (file)) return (32);
   if (ObjectFilter->ReadFile (file)) return (51);
   if (file.Read ((byte*)&n_items,1,sizeof(int))) return (52);
   Schedule_HM.Delete (  );
   for (i = 0; i < n_items; i++) {
      Period_HM *period_hm = new Period_HM;
      if (period_hm->ReadFile (file)) {
         delete period_hm;
         return (53);
      }
      else Schedule_HM.Add (period_hm);
   }
   if (file.Read ((byte*)&n_items,1,sizeof(int))) return (54);
   Schedule_YMD.Delete (  );
   for (i = 0; i < n_items; i++) {
      Period_YMD *period_ymd = new Period_YMD;
      if (period_ymd->ReadFile (file)) {
         delete period_ymd;
         return (55);
      }
      else Schedule_YMD.Add (period_ymd);
   }
   return (DONE);
}

 int EventRule::ReadFile (CXMLIO* pIO)

{
   int i,n_items = 0;
   static EventRule dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("EventRule"))
   {
      xmlNode.Attribute (TYPE_ID(int)  ,"ID"         ,&ID);
      xmlNode.Attribute (TYPE_ID(int)  ,"Options"    ,&Options    ,&dv.Options);
      xmlNode.Attribute (TYPE_ID(int)  ,"EventType"  ,&EventType  ,&dv.EventType);
      xmlNode.Attribute (TYPE_ID(int)  ,"ObjectType" ,&ObjectType ,&dv.ObjectType);
      xmlNode.Attribute (TYPE_ID(int)  ,"PersonAttrs" ,&PersonAttrs ,&dv.PersonAttrs);
      xmlNode.Attribute (TYPE_ID(int)  ,"Schedule_WD",&Schedule_WD,&dv.Schedule_WD);
      xmlNode.Attribute (TYPE_ID(float),"AlarmPeriod",&AlarmPeriod,&dv.AlarmPeriod);
      xmlNode.Attribute (TYPE_ID(char) ,"Name"       ,Name,sizeof(Name));
      
      ERC_Abandoned.ReadFile         (pIO);
      ERC_CarAccident.ReadFile       (pIO);
      ERC_CarParking.ReadFile        (pIO);
      ERC_CarStopping.ReadFile       (pIO);
      ERC_ColorChange.ReadFile       (pIO);
      ERC_Counting.ReadFile          (pIO);
      ERC_CrowdDensity.ReadFile      (pIO);
      ERC_DirectionalMotion.ReadFile (pIO);
      ERC_Dwell.ReadFile             (pIO);
      ERC_Falling.ReadFile           (pIO);
      ERC_Flame.ReadFile             (pIO);
      ERC_Gathering.ReadFile         (pIO); 
      ERC_LineCrossing.ReadFile      (pIO); 
      ERC_Loitering.ReadFile         (pIO);
      ERC_PathPassing.ReadFile       (pIO);
      ERC_Removed.ReadFile           (pIO);
      ERC_Smoke.ReadFile             (pIO);
      ERC_Stopping.ReadFile          (pIO);
      ERC_TrafficCongestion.ReadFile (pIO);
      ERC_Violence.ReadFile          (pIO);
      ERC_WaterLevel.ReadFile        (pIO);
      ERC_ZoneColor.ReadFile         (pIO);
      ObjectFilter->ReadFile         (pIO);
      xmlNode.Attribute (TYPE_ID(int),"PeriodHM_Num",&n_items);
      Schedule_HM.Delete (  );
      for (i = 0; i < n_items; i++) {
         Period_HM *period_hm = new Period_HM;
         if (DONE == period_hm->ReadFile (pIO))
            Schedule_HM.Add (period_hm);
         else
            delete period_hm;
      }
      xmlNode.Attribute (TYPE_ID(int),"PeriodYMD_Num",&n_items);
      Schedule_YMD.Delete (  );
      for (i = 0; i < n_items; i++) {
         Period_YMD *period_ymd = new Period_YMD;
         if (DONE == period_ymd->ReadFile (pIO))
            Schedule_YMD.Add (period_ymd);
         else
            delete period_ymd;
      }
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}

 int EventRule::WriteFile (FileIO &file)
 
{
   if (file.Write ((byte*)&ID         ,1,sizeof(int  ))) return (1);
   if (file.Write ((byte*)&Options    ,1,sizeof(int  ))) return (2);
   if (file.Write ((byte*)&EventType  ,1,sizeof(int  ))) return (3);
   if (file.Write ((byte*)&ObjectType ,1,sizeof(int  ))) return (4);
   if (file.Write ((byte*)&PersonAttrs ,1,sizeof(int  ))) return (5);
   if (file.Write ((byte*)&Schedule_WD,1,sizeof(int  ))) return (6);
   if (file.Write ((byte*)&AlarmPeriod,1,sizeof(float))) return (7);
   if (file.Write ((byte*)Name        ,1,sizeof(Name ))) return (8);
   if (ERC_Abandoned.WriteFile         (file)) return (11);
   if (ERC_CarAccident.WriteFile       (file)) return (12);
   if (ERC_CarParking.WriteFile        (file)) return (13);
   if (ERC_CarStopping.WriteFile       (file)) return (14);
   if (ERC_ColorChange.WriteFile       (file)) return (15);
   if (ERC_Counting.WriteFile          (file)) return (17);
   if (ERC_CrowdDensity.WriteFile      (file)) return (18);
   if (ERC_DirectionalMotion.WriteFile (file)) return (19);
   if (ERC_Dwell.WriteFile             (file)) return (20);
   if (ERC_Falling.WriteFile           (file)) return (21);
   if (ERC_Flame.WriteFile             (file)) return (22);
   if (ERC_Gathering.WriteFile         (file)) return (23);
   if (ERC_LineCrossing.WriteFile      (file)) return (24);
   if (ERC_Loitering.WriteFile         (file)) return (25);
   if (ERC_PathPassing.WriteFile       (file)) return (26);
   if (ERC_Removed.WriteFile           (file)) return (27);
   if (ERC_Smoke.WriteFile             (file)) return (28);
   if (ERC_Stopping.WriteFile          (file)) return (29);
   if (ERC_TrafficCongestion.WriteFile (file)) return (30);
   if (ERC_Violence.WriteFile          (file)) return (31);
   if (ERC_WaterLevel.WriteFile        (file)) return (32);
   if (ERC_ZoneColor.WriteFile         (file)) return (33);
   if (ObjectFilter->WriteFile (file)) return (51);
   int n_items = Schedule_HM.GetNumItems (   );
   if (file.Write ((byte*)&n_items,1,sizeof(int))) return (52);
   Period_HM *period_hm = Schedule_HM.First (   );
   while (period_hm) {
      if (period_hm->WriteFile (file)) return (53);
      period_hm = Schedule_HM.Next (period_hm);
   }
   n_items = Schedule_YMD.GetNumItems (   );
   if (file.Write ((byte*)&n_items,1,sizeof(int))) return (54);
   Period_YMD *period_ymd = Schedule_YMD.First (   );
   while (period_ymd) {
      if (period_ymd->WriteFile (file)) return (55);
      period_ymd = Schedule_YMD.Next (period_ymd);
   }
   return (DONE);
}

 int EventRule::WriteFile (CXMLIO* pIO)

{
   static EventRule dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("EventRule"))
   {
      xmlNode.Attribute (TYPE_ID(int)  ,"ID"         ,&ID);
      xmlNode.Attribute (TYPE_ID(int)  ,"Options"    ,&Options    ,&dv.Options);
      xmlNode.Attribute (TYPE_ID(int)  ,"EventType"  ,&EventType  ,&dv.EventType);
      xmlNode.Attribute (TYPE_ID(int)  ,"ObjectType" ,&ObjectType ,&dv.ObjectType);
      xmlNode.Attribute (TYPE_ID(int)  ,"PersonAttrs" ,&PersonAttrs ,&dv.PersonAttrs);
      xmlNode.Attribute (TYPE_ID(int)  ,"Schedule_WD",&Schedule_WD,&dv.Schedule_WD);
      xmlNode.Attribute (TYPE_ID(float),"AlarmPeriod",&AlarmPeriod,&dv.AlarmPeriod);
      xmlNode.Attribute (TYPE_ID(char) ,"Name"       ,Name,sizeof(Name));
      ERC_Abandoned.WriteFile         (pIO);
      ERC_CarAccident.WriteFile       (pIO);
      ERC_CarParking.WriteFile        (pIO);
      ERC_CarStopping.WriteFile       (pIO);
      ERC_ColorChange.WriteFile       (pIO);
      ERC_Counting.WriteFile          (pIO);
      ERC_CrowdDensity.WriteFile      (pIO);
      ERC_DirectionalMotion.WriteFile (pIO);
      ERC_Dwell.WriteFile             (pIO);
      ERC_Falling.WriteFile           (pIO);
      ERC_Flame.WriteFile             (pIO);
      ERC_Gathering.WriteFile         (pIO);
      ERC_LineCrossing.WriteFile      (pIO);
      ERC_Loitering.WriteFile         (pIO);
      ERC_PathPassing.WriteFile       (pIO);
      ERC_Removed.WriteFile           (pIO);
      ERC_Smoke.WriteFile             (pIO);
      ERC_Stopping.WriteFile          (pIO);
      ERC_TrafficCongestion.WriteFile (pIO);
      ERC_Violence.WriteFile          (pIO);
      ERC_WaterLevel.WriteFile        (pIO);
      ERC_ZoneColor.WriteFile         (pIO);
      ObjectFilter->WriteFile         (pIO);
      int n_items = Schedule_HM.GetNumItems (   );
      xmlNode.Attribute (TYPE_ID(int),"PeriodHM_Num",&n_items);
      Period_HM *period_hm = Schedule_HM.First (   );
      while (period_hm) {
         period_hm->WriteFile (pIO);
         period_hm = Schedule_HM.Next (period_hm);
      }
      n_items = Schedule_YMD.GetNumItems (   );
      xmlNode.Attribute (TYPE_ID(int),"PeriodYMD_Num",&n_items);
      Period_YMD *period_ymd = Schedule_YMD.First (   );
      while (period_ymd) {
         period_ymd->WriteFile (pIO);
         period_ymd = Schedule_YMD.Next (period_ymd);
      }
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}

}

#include "vacl_ipp.h"
#include "vacl_noise.h"
#include "vacl_tgtrack.h"
#include "vacl_warping.h"

#if defined(__DEBUG_TGT)
#include "Win32CL/Win32CL.h"
extern int _DVX_TGT,_DVY_TGT;
extern CImageView* _DebugView_TGT;
#endif

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: TargetTracking
//
///////////////////////////////////////////////////////////////////////////////

 TargetTracking::TargetTracking (   )
 
{
   Flag_Initialized   = FALSE;
   Flag_SetTargetSize = FALSE;

   MinColorSimilarity = 0.7f;
   MinMatchingScore   = 0.6f;

   MinTargetLostTime  = 0.2f;
   MinTargetOutRatio  = 0.5f;
   ModelUpdateRate    = 0.1f;
   VelocityUpdateRate = 0.1f;

   Status             = 0;
   FrameCount         = 0;
   FrameRate          = 0.0f;
   FrameDuration      = 0.0f;
   MatchingScore      = 0.0f;
   TargetLostTime     = 0.0f;
}

 TargetTracking::~TargetTracking (   )

{
   Close (   );
}

 void TargetTracking::Close (   )

{
   Flag_Initialized = FALSE;
   SrcImgSize.Clear (   );
}

 int TargetTracking::CheckTargetOutOfRange (   )

{
   Status &= ~TT_STATUS_TARGET_OUT_OF_RANGE;
   int a = TargetRegion.Width * TargetRegion.Height;
   if (a < 10) {
      Status |= TT_STATUS_TARGET_OUT_OF_RANGE;
      return (TRUE);
   }
   int sx = TargetRegion.X;
   int sy = TargetRegion.Y;
   int ex = sx + TargetRegion.Width;
   int ey = sy + TargetRegion.Height;
   if (sx < 2) sx = 0;
   if (sy < 2) sy = 0;
   if (ex > SrcImgSize.Width  - 2) ex = SrcImgSize.Width  - 2;
   if (ey > SrcImgSize.Height - 2) ey = SrcImgSize.Height - 2;
   int w = ex - sx;
   int h = ey - sy;
   if (w <= 0 || h <= 0) {
      Status |= TT_STATUS_TARGET_OUT_OF_RANGE;
      return (TRUE);
   }
   float r = 1.0f - (float)(w * h) / a; // 밖으로 나간 타겟의 면적 비율
   if (r >= MinTargetOutRatio) { 
      Status |= TT_STATUS_TARGET_OUT_OF_RANGE;
      return (TRUE);
   }
   return (FALSE);
}

 IPoint2D TargetTracking::GetPredictedPosition (   )

{
   IPoint2D p_pos(TargetRegion.X,TargetRegion.Y);
   if (TargetVelocity.X != MC_VLV) {
      p_pos.X = (int)(p_pos.X + TargetVelocity.X + 0.5f);
      p_pos.Y = (int)(p_pos.Y + TargetVelocity.Y + 0.5f);
   }
   return (p_pos);
}

 void TargetTracking::Initialize (ISize2D& si_size,IBox2D& tg_rgn,float frm_rate)

{
   Close (  );
   Flag_SetTargetSize = FALSE;
   Flag_Initialized   = TRUE;
   Status             = 0;
   FrameCount         = 0;
   MatchingScore      = 0.0f;
   TargetLostTime     = 0.0f;
   FrameRate          = frm_rate;
   FrameDuration      = 1.0f / FrameRate;
   SrcImgSize         = si_size;
   TargetRegion       = tg_rgn;
   CropRegion (SrcImgSize,TargetRegion);
   TargetVelocity(MC_VLV,MC_VLV);
}

 void TargetTracking::SetTargetSize (ISize2D& size)

{
   Flag_SetTargetSize  = TRUE;
   TargetRegion.X      = TargetRegion.X + (TargetRegion.Width  - size.Width ) / 2;
   TargetRegion.Y      = TargetRegion.Y + (TargetRegion.Height - size.Height) / 2;
   TargetRegion.Width  = size.Width;
   TargetRegion.Height = size.Height;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: TargetSample
//
///////////////////////////////////////////////////////////////////////////////

 void TargetSample::Clear (   )

{
   CX          = 0;
   CY          = 0;
   Width       = 0;
   Height      = 0;
   Probability = 0.0f;
}

 IBox2D TargetSample::GetRegion (   )

{
   IBox2D d_rgn;
   d_rgn.X      = CX - Width  / 2;
   d_rgn.Y      = CY - Height / 2;
   d_rgn.Width  = Width;
   d_rgn.Height = Height;
   return (d_rgn);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: TargetTracking_PF
//
///////////////////////////////////////////////////////////////////////////////

 TargetTracking_PF::TargetTracking_PF (   )
 
{
   NumSamples    = 300;
   GNSD_Scale    = 0.03f;
   GNSD_Center   = 0.3f;
   SamplingRange = 2.0f;
}

 void TargetTracking_PF::Close (   )
 
{
   TargetTracking::Close (   );
   Samples.Delete (   );
}

 TargetSample TargetTracking_PF::GetSampleMean (   )

{
   int i;

   float cx     = 0.0f;
   float cy     = 0.0f;
   float width  = 0.0f;
   float height = 0.0f;
   for (i = 0; i < Samples.Length; i++) {
      float prob = Samples[i].Probability;
      cx += prob * Samples[i].CX;
      cy += prob * Samples[i].CY;
      width  += prob * Samples[i].Width;
      height += prob * Samples[i].Height;
   }
   TargetSample smp_mean;
   smp_mean.CX     = (int)(cx + 0.5f);
   smp_mean.CY     = (int)(cy + 0.5f);
   smp_mean.Width  = (int)(width  + 0.5f);
   smp_mean.Height = (int)(height + 0.5f);
   return (smp_mean);
}

 void TargetTracking_PF::NormalizeSampleProbabilities (   )

{
   int i;

   float sum = 0.0f;
   for (i = 0; i < Samples.Length; i++) sum += Samples[i].Probability;
   if (sum < MC_VSV) {
      float p = 1.0f / Samples.Length;
      for (i = 0; i < Samples.Length; i++) Samples[i].Probability = p;
      return;
   }
   for (i = 0; i < Samples.Length; i++) Samples[i].Probability /= sum;
}

 float TargetTracking_PF::Perform (   )
 
{
   FrameCount++;
   Status             = 0;
   MatchingScore      = 0.0f;
   Flag_SetTargetSize = FALSE;
   if (!IsInitialized (   )) {
      Status |= TT_STATUS_NOT_INITIALIZED;
      return (0.0f);
   }
   if (CheckTargetOutOfRange (   )) {
      #if defined(__DEBUG_TGT)
      logd ("Target out of range.\n");
      #endif
      return (0.0f);
   }
   PerformPreprocessing (   );
   if (!Samples) PerformInitialSampling (   );
   else {
      PerformResampling (   );
      PropagateSamples  (   );
   }
   MatchingScore = PerformMatching (SampleMean);
   #if defined(__DEBUG_TGT)
   if (_DebugView_TGT) {
      _DebugView_TGT->DrawRectangle (_DVX_TGT + TargetRegion.X,_DVY_TGT + TargetRegion.Y,TargetRegion.Width,TargetRegion.Height,PC_GREEN);
      float mp = 1.0f / Samples.Length;
      for (int i = 0; i < Samples.Length; i++) {
         COLORREF color = PC_BLUE;
         if (Samples[i].Probability >= mp) color = PC_RED;
         _DebugView_TGT->DrawPoint ((int)(_DVX_TGT + Samples[i].CX + 0.5f),(int)(_DVY_TGT + Samples[i].CY + 0.5f),color,PT_DOT);
      }
      _DVY_TGT += SrcImgSize.Height;
   }
   #endif
   #if defined(__DEBUG_TGT)
   logd ("Matching score: %f\n",MatchingScore);
   logd ("Matching position: X = %d, Y = %d\n",SampleMean.CX,SampleMean.CY);
   #endif
   UpdateTargetRegion    (   );
   UpdateTargetModel     (   );
   PerformPostprocessing (   );
   #if defined(__DEBUG_TGT)
   if (_DebugView_TGT) {
      _DebugView_TGT->Invalidate (   );
   }
   #endif
   return (MatchingScore);
}

 void TargetTracking_PF::PerformInitialSampling (   )
 
{
   int i,cx,cy;
   static time_t seed = 0;

   if (!seed) {
      time (&seed);
      seed += 1000;
   }
   else seed++;
   srand ((uint)seed);
   int sws  = (int)(0.5f * (TargetRegion.Width + TargetRegion.Height) * SamplingRange);
   int hsws = sws / 2;
   int bx   = TargetRegion.X + TargetRegion.Width  / 2 - hsws;
   int by   = TargetRegion.Y + TargetRegion.Height / 2 - hsws;
   int ex   = SrcImgSize.Width  - 1;
   int ey   = SrcImgSize.Height - 1;
   Samples.Create (NumSamples);
   for (i = 0; i < Samples.Length; i++) {
      cx = bx + (rand (   ) % sws);
      cy = by + (rand (   ) % sws);
      if (cx < 0) cx = 0;
      else if (cx > ex) cx = ex;
      if (cy < 0) cy = 0;
      else if (cy > ey) cy = ey;
      Samples[i].CX     = cx;
      Samples[i].CY     = cy;
      Samples[i].Width  = TargetRegion.Width;
      Samples[i].Height = TargetRegion.Height;
   }
}

 void TargetTracking_PF::PerformResampling (   )

{
   int i,sn;
   static time_t seed = 0;

   TSArray1D nsmpls(Samples.Length);
   FArray1D  cps(Samples.Length + 1);
   cps[0] = 0.0f;
   for (i = 0; i < Samples.Length; i++)
      cps[i + 1] = cps[i] + Samples[i].Probability;
   if (!seed) time (&seed);
   else seed++;
   srand ((uint)seed);
   for (i = 0; i < Samples.Length; i++) {
      float r = (float)rand(   ) / RAND_MAX;
      if (r == 0.0f) sn = 0;
      else if (r == 1.0f) sn = Samples.Length - 1;
      else {
         int is = 0;
         int ie = cps.Length - 1;
         while(TRUE) {
            int im = (is + ie) / 2;
            if (cps[im] == r) {
               sn = im - 1;
               break;
            }
            else if (ie - is <= 1) {
               sn = ie - 1;
               break;
            }
            else if (cps[im] < r) is = im;
            else ie = im;
         }
      }
      nsmpls[i] = Samples[sn];
   }
   Samples = nsmpls;
}

 void TargetTracking_PF::PropagateSamples (   )

{
   int i,cx,cy;
   GaussianNoise gn_center,gn_scale;

   cx = TargetRegion.X + TargetRegion.Width  / 2;
   cy = TargetRegion.Y + TargetRegion.Height / 2;
   int sws  = (int)(0.5f * (TargetRegion.Width + TargetRegion.Height) * SamplingRange);
   int hsws = sws / 2;
   int sx   = cx - hsws;
   int sy   = cy - hsws;
   int ex   = cx + hsws;
   int ey   = cy + hsws;
   if (sx < 0) sx = 0;
   if (sy < 0) sy = 0;
   if (ex >= SrcImgSize.Width ) ex = SrcImgSize.Width  - 1;
   if (ey >= SrcImgSize.Height) ey = SrcImgSize.Height - 1;
   float gnsd_center = GNSD_Center * 0.5f * (TargetRegion.Width + TargetRegion.Height);
   gn_center.Create (gnsd_center);
   if (GNSD_Scale) gn_scale.Create (GNSD_Scale);
   for (i = 0; i < Samples.Length; i++) {
      cx = (int)(Samples[i].CX + TargetVelocity.X + gn_center.Generate (   ) + 0.5f);
      cy = (int)(Samples[i].CY + TargetVelocity.Y + gn_center.Generate (   ) + 0.5f);
      if (cx < sx) cx = sx;
      else if (cx > ex) cx = ex;
      if (cy < sy) cy = sy;
      else if (cy > ey) cy = ey;
      Samples[i].CX = cx;
      Samples[i].CY = cy;
      if (GNSD_Scale) {
         float scale = gn_scale.Generate (   );
         if (scale > 0.0f) scale *= 0.7f; // PARAMETERS
         scale += 1.0f;
         Samples[i].Width  = (int)(Samples[i].Width  * scale + 0.5f);
         Samples[i].Height = (int)(Samples[i].Height * scale + 0.5f);
      }
   }
}

 void TargetTracking_PF::SetTargetSize (ISize2D &size)

{
   int i;

   TargetTracking::SetTargetSize (size);
   for (i = 0; i < Samples.Length; i++) {
      Samples[i].Width  = size.Width;
      Samples[i].Height = size.Height;
   }
}

 void TargetTracking_PF::UpdateTargetRegion (   )

{
   if (VelocityUpdateRate) {
      int cx = TargetRegion.X + TargetRegion.Width  / 2;
      int cy = TargetRegion.Y + TargetRegion.Height / 2;
      if (TargetVelocity.X == MC_VLV) {
         TargetVelocity.X = (float)(SampleMean.CX - cx);
         TargetVelocity.Y = (float)(SampleMean.CY - cy);
      }
      else {
         float a = (1.0f - VelocityUpdateRate);
         TargetVelocity.X  = a * TargetVelocity.X + VelocityUpdateRate * (SampleMean.CX - cx);
         TargetVelocity.Y  = a * TargetVelocity.Y + VelocityUpdateRate * (SampleMean.CY - cy);
      }
   }
   else TargetVelocity(0.0f,0.0f);
   TargetRegion = SampleMean.GetRegion (   );
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: TargetTracking_PF_CH
//
///////////////////////////////////////////////////////////////////////////////

 TargetTracking_PF_CH::TargetTracking_PF_CH (   )
 
{
   MinMatchingScore = 0.95f;
   ModelUpdateRate  = 0.01f;
}
 
 void TargetTracking_PF_CH::Close (   )
 
{
   TargetTracking_PF::Close (   );
   TargetModel.Delete (   );
   TargetHistogram.Delete (   );
}

 void TargetTracking_PF_CH::GetColorHistogram (IBox2D &s_rgn,FArray1D &d_hist)
 
{
   int x,y,ix,iy;

   BGRImage &s_image = *SrcImage;
   int hw = s_rgn.Width  / 2;
   int hh = s_rgn.Height / 2;
   int cx = s_rgn.X + hw;
   int cy = s_rgn.Y + hh;
   int sx = s_rgn.X;
   int sy = s_rgn.Y;
   int ex = s_rgn.X + s_rgn.Width  - 1;
   int ey = s_rgn.Y + s_rgn.Height - 1;
   if (sx < 0) sx = 0;
   if (ex >= SrcImgSize.Width)  ex = SrcImgSize.Width - 1;
   if (sy < 0) sy = 0;
   if (ey >= SrcImgSize.Height) ey = SrcImgSize.Height - 1;
   int size_thrsld = (SrcImgSize.Width + SrcImgSize.Height) / 6;
   if (ex - sx >= size_thrsld) ix = 2;
   else ix = 1;
   if (ey - sy >= size_thrsld) iy = 2;
   else iy = 1;
   d_hist.Clear (   );
   for (y = sy; y <= ey; y += iy) {
      float dy  = (float)(y - cy) / hh;
      float sdy = dy * dy;
      BGRPixel *l_s_image = s_image[y] + sx;
      for (x = sx; x <= ex; x += ix) {
         float dx  = (float)(x - cx) / hw;
         float sd  = dx * dx + sdy;
         if (sd <= 1.0f) {
            int idx = GET_COLOR_HISTOGRAM_BIN_INDEX_NO(l_s_image->R,l_s_image->G,l_s_image->B);
            d_hist[idx] += (1.0f - sd);
         }
         l_s_image += ix;
      }
   }
   NormalizeHistogram (d_hist);
}

 float TargetTracking_PF_CH::GetSampleProbability (float similarity)

{
   float probability = exp (-20.0f * (1.0f - similarity));
   return (probability);
}

 void TargetTracking_PF_CH::Initialize (BGRImage& s_image,IBox2D& tg_rgn,float frm_rate)
 
{
   ISize2D si_size(s_image.Width,s_image.Height);
   TargetTracking_PF::Initialize (si_size,tg_rgn,frm_rate);
   SrcImage = &s_image;
   TargetModel.Create (NUM_COLOR_HISTOGRAM_BINS);
   TargetHistogram.Create (NUM_COLOR_HISTOGRAM_BINS);
   GetColorHistogram (TargetRegion,TargetModel);
}

 float TargetTracking_PF_CH::Perform (BGRImage &s_image)
 
{
   #if defined(__DEBUG_TGT)
   if (_DebugView_TGT) {
      _DVX_TGT = _DVY_TGT = 0;
      _DebugView_TGT->DrawImage (s_image,_DVX_TGT,_DVY_TGT);
   }
   #endif
   SrcImage = &s_image;
   return (TargetTracking_PF::Perform (   ));
}

 float TargetTracking_PF_CH::PerformMatching (TargetSample &smp_mean)

{
   int    i;
   IBox2D rgn;
   
   FArray1D t_hist(NUM_COLOR_HISTOGRAM_BINS);
   for (i = 0; i < Samples.Length; i++) {
     rgn = Samples[i].GetRegion( );
      GetColorHistogram (rgn,t_hist);
      float similarity = GetHistogramSimilarity (TargetModel,t_hist);
      Samples[i].Probability = GetSampleProbability (similarity);
   }
   NormalizeSampleProbabilities (   );
   smp_mean = GetSampleMean (   );
   rgn = smp_mean.GetRegion (   );
   GetColorHistogram (rgn,TargetHistogram);
   return (GetHistogramSimilarity (TargetModel,TargetHistogram));
}

 void TargetTracking_PF_CH::PerformPreprocessing (   )
 
{

}

 void TargetTracking_PF_CH::UpdateTargetModel (   )
 
{
   int i;

   if (MatchingScore < MinMatchingScore) return;
   float a = 1.0f - ModelUpdateRate;
   for (i = 0; i < TargetModel.Length; i++)
      TargetModel[i] = a * TargetModel[i] + ModelUpdateRate * TargetHistogram[i];
}
 
///////////////////////////////////////////////////////////////////////////////
//
// Class: TargetTracking_MS
//
///////////////////////////////////////////////////////////////////////////////

 TargetTracking_MS::TargetTracking_MS (   )

{
   MaxNumIterations = 20;
   MinMatchingScore = 0.8f;
   ModelUpdateRate  = 0.05f;
}

 void TargetTracking_MS::Close (   )
 
{
   TargetTracking::Close  (   );
   TargetHistogram.Delete (   );
   PixelIndices.Delete    (   );
   TargetModel.Delete     (   );
}

 void TargetTracking_MS::GetColorHistogram (BGRImage &s_image,IBox2D &s_rgn,FArray1D &d_hist)

{
   int i,x,y;

   int hw = s_rgn.Width  / 2;
   int hh = s_rgn.Height / 2;
   int cx = s_rgn.X + hw;
   int cy = s_rgn.Y + hh;
   int sx = s_rgn.X;
   int sy = s_rgn.Y;
   int ex = s_rgn.X + s_rgn.Width  - 1;
   int ey = s_rgn.Y + s_rgn.Height - 1;
   if (sx < 0) sx = 0;
   if (ex >= s_image.Width)  ex = s_image.Width - 1;
   if (sy < 0) sy = 0;
   if (ey >= s_image.Height) ey = s_image.Height - 1;
   d_hist.Clear (   );
   PixelIndices.Create ((ex - sx + 1) * (ey - sy + 1));
   for (y = sy,i = 0; y <= ey; y++) {
      float dy  = (float)(y - cy) / hh;
      float sdy = dy * dy;
      for (x = sx; x <= ex; x++) {
         float dx  = (float)(x - cx) / hw;
         float sd  = dx * dx + sdy;
         int   idx = -1;
         if (sd <= 1.0f) {
            idx = GET_COLOR_HISTOGRAM_BIN_INDEX_NO(s_image[y][x].R,s_image[y][x].G,s_image[y][x].B);
            d_hist[idx] += (1.0f - sd);
         }
         PixelIndices[i++] = (short)idx;
      }
   }
   NormalizeHistogram (d_hist);
}

 FPoint2D TargetTracking_MS::GetMeanShiftVector (IBox2D &s_rgn,FArray1D &t_hist)

{
   int i,x,y;

   FArray1D weights(t_hist.Length);
   for (i = 0; i < weights.Length; i++) {
      if (t_hist[i] > MC_VSV) weights[i] = (float)sqrt (TargetModel[i] / t_hist[i]);
      else weights[i] = 0.0f;
   }
   int hw = s_rgn.Width  / 2;
   int hh = s_rgn.Height / 2;
   int cx = s_rgn.X + hw;
   int cy = s_rgn.Y + hh;
   int sx = s_rgn.X;
   int sy = s_rgn.Y;
   int ex = s_rgn.X + s_rgn.Width  - 1;
   int ey = s_rgn.Y + s_rgn.Height - 1;
   if (sx < 0) sx = 0;
   if (ex >= SrcImgSize.Width ) ex = SrcImgSize.Width  - 1;
   if (sy < 0) sy = 0;
   if (ey >= SrcImgSize.Height) ey = SrcImgSize.Height - 1;
   float sum_x = 0.0f;
   float sum_y = 0.0f;
   float sum_w = 0.0f;
   for (y = sy,i = 0; y <= ey; y++) {
      int dy = y - cy;
      for (x = sx; x <= ex; x++) {
         int idx = PixelIndices[i++];
         if (idx != -1) {
            float w = weights[idx];
            sum_x += w * (x - cx);
            sum_y += w * dy;
            sum_w += w;
         }
      }
   }
   FPoint2D msv;
   if (sum_w == 0.0f) // jun: sum_w 값이 0인 경우가 매우 드물게 있음. 
      msv.X = msv.Y = 0.0f;
   else {
      msv.X = sum_x / sum_w;
      msv.Y = sum_y / sum_w;
   }
   return (msv);
}

 void TargetTracking_MS::Initialize (BGRImage& s_image,IBox2D& tg_rgn,float frm_rate)

{
   ISize2D si_size(s_image.Width,s_image.Height);
   TargetTracking::Initialize (si_size,tg_rgn,frm_rate);
   TargetModel.Create (NUM_COLOR_HISTOGRAM_BINS);
   TargetHistogram.Create (NUM_COLOR_HISTOGRAM_BINS);
   GetColorHistogram (s_image,TargetRegion,TargetModel);
}

 float TargetTracking_MS::PerformMatching (BGRImage &s_image,IBox2D &s_rgn,FArray1D &t_hist,IPoint2D &m_pos)

{
   int i;
   float s0,s1;
   
   IBox2D t_rgn = s_rgn;
   FPoint2D mv(0.0f,0.0f);
   GetColorHistogram (s_image,t_rgn,t_hist);
   s0 = GetHistogramSimilarity (TargetModel,t_hist);
   for (i = 0; i < MaxNumIterations || MaxNumIterations < 0; i++) {
      FPoint2D msv = GetMeanShiftVector (t_rgn,t_hist);
      t_rgn.X = (int)(s_rgn.X + mv.X + msv.X + 0.5f);
      t_rgn.Y = (int)(s_rgn.Y + mv.Y + msv.Y + 0.5f);
      GetColorHistogram (s_image,t_rgn,t_hist);
      s1 = GetHistogramSimilarity (TargetModel,t_hist);
      if (s0 > s1) break;
      mv += msv;
      float md = Mag(msv);
      if (md <= 0.2f) break; // PARAMETERS
   }
   #if defined(__DEBUG_TGT)
   logd ("Motion Vector: X = %f, Y = %f\n",mv.X,mv.Y);
   #endif
   m_pos.X = (int)(s_rgn.X + s_rgn.Width  / 2 + mv.X + 0.5f);
   m_pos.Y = (int)(s_rgn.Y + s_rgn.Height / 2 + mv.Y + 0.5f);
   float similarity = GetHistogramSimilarity (TargetModel,t_hist);
   return (similarity);
}

 float TargetTracking_MS::Perform (BGRImage &s_image)

{
   FrameCount++;
   Status             = 0;
   MatchingScore      = 0.0f;
   Flag_SetTargetSize = FALSE;
   if (!IsInitialized (   )) {
      Status |= TT_STATUS_NOT_INITIALIZED;
      return (0.0f);
   }
   if (CheckTargetOutOfRange (   )) {
      #if defined(__DEBUG_TGT)
      logd ("Target out of range.\n");
      #endif
      return (0.0f);
   }
   IPoint2D p_pos = GetPredictedPosition (   );
   IBox2D p_rgn(p_pos.X,p_pos.Y,TargetRegion.Width,TargetRegion.Height);
   MatchingScore = PerformMatching (s_image,p_rgn,TargetHistogram,MatchingPos);
   #if defined(__DEBUG_TGT)
   logd ("Matching score: %f\n",MatchingScore);
   logd ("Matching position: X = %d, Y = %d\n",MatchingPos.X,MatchingPos.Y);
   #endif
   UpdateTargetRegion (   );
   UpdateTargetModel  (   );
   return (MatchingScore);
}
 
 void TargetTracking_MS::UpdateTargetModel (   )

{
   int i;

   if (MatchingScore < MinMatchingScore) return;
   float a = 1.0f - ModelUpdateRate;
   for (i = 0; i < TargetModel.Length; i++)
      TargetModel[i] = a * TargetModel[i] + ModelUpdateRate * TargetHistogram[i];
}

 void TargetTracking_MS::UpdateTargetRegion (   )

{
   FPoint2D r_pos,t_pos;
   r_pos.X = (float)(TargetRegion.X + TargetRegion.Width  / 2);
   r_pos.Y = (float)(TargetRegion.Y + TargetRegion.Height / 2);
   t_pos.X = (float)MatchingPos.X;
   t_pos.Y = (float)MatchingPos.Y;
   if (VelocityUpdateRate) {
      if (TargetVelocity.X == MC_VLV) TargetVelocity = t_pos - r_pos;
      else TargetVelocity = (1.0f - VelocityUpdateRate) * TargetVelocity + VelocityUpdateRate * (t_pos - r_pos);
   }
   else TargetVelocity(0.0f,0.0f);
   TargetRegion.X = (int)(t_pos.X - TargetRegion.Width  / 2 + 0.5f);
   TargetRegion.Y = (int)(t_pos.Y - TargetRegion.Height / 2 + 0.5f);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: TargetTracking_NCC
//
///////////////////////////////////////////////////////////////////////////////

 TargetTracking_NCC::TargetTracking_NCC (   )

{
   MatchingRange    = 12;
   MinMatchingScore = 0.6f;
   ModelUpdateRate  = 0.1f;
   MSThreshold      = 0.8f;
   TSThreshold      = 45.0f;
   ZoomStep         = 0.04f;
}

 void TargetTracking_NCC::Close (   )
 
{
   TargetTracking::Close (   );
   TargetModel.Delete (   );
}

 void TargetTracking_NCC::GetImagePatch (GImage &s_image,IBox2D &s_rgn,GImage &d_image)

{
   int x1,y1,x2,y2;

   d_image.Clear (   );
   int sx1 = s_rgn.X;
   int sy1 = s_rgn.Y;
   int ex1 = sx1 + s_rgn.Width;
   int ey1 = sy1 + s_rgn.Height;
   int sx2 = 0, sy2 = 0;
   if (sx1 < 0) {
      sx2 = -sx1;
      sx1 = 0;
   }
   if (sy1 < 0) {
      sy2 = -sy1;
      sy1 = 0;
   }
   if (ex1 > s_image.Width ) ex1 = s_image.Width;
   if (ey1 > s_image.Height) ey1 = s_image.Height;
   for (y1 = sy1,y2 = sy2; y1 < ey1; y1++,y2++) {
      byte *l_s_image = s_image[y1];
      byte *l_d_image = d_image[y2];
      for (x1 = sx1,x2 = sx2; x1 < ex1; x1++,x2++)
         l_d_image[x2] = l_s_image[x1];
   }
}

 void TargetTracking_NCC::Initialize (GImage& s_image,IBox2D& tg_rgn,float frm_rate)

{
   ISize2D si_size(s_image.Width,s_image.Height);
   TargetTracking::Initialize (si_size,tg_rgn,frm_rate);
   TargetModel.Create (TargetRegion.Width,TargetRegion.Height);
   GetImagePatch (s_image,TargetRegion,TargetModel);
}

 float TargetTracking_NCC::Perform (GImage &s_image)

{
   #if defined(__DEBUG_TGT)
   if (_DebugView_TGT) {
      _DVX_TGT = _DVY_TGT = 0;
      _DebugView_TGT->Clear (   );
   }
   #endif
   FrameCount++;
   Status             = 0;
   MatchingScore      = 0.0f;
   Flag_SetTargetSize = FALSE;
   if (!IsInitialized (   )) {
      Status |= TT_STATUS_NOT_INITIALIZED;
      return (0.0f);
   }
   if (CheckTargetOutOfRange (   )) {
      #if defined(__DEBUG_TGT)
      logd ("Target out of range.\n");
      #endif
      return (0.0f);
   }
   int m_range1 = 0;
   float t_size = (float)sqrt ((float)(TargetModel.Width * TargetModel.Height));
   float scale  = TSThreshold / t_size;
   IPoint2D p_pos = GetPredictedPosition (   );
   GImage   s_image2,t_model2;
   IPoint2D p_pos2,m_pos2;
   if (scale < 1.0f) {
      m_range1 = (int)(scale * MatchingRange + 0.5f);
      s_image2.Create ((int)(s_image.Width * scale + 0.5f),(int)(s_image.Height * scale + 0.5f));
      Resize_2 (s_image,s_image2);
      t_model2.Create ((int)(TargetModel.Width * scale + 0.5f),(int)(TargetModel.Height * scale + 0.5f));
      Resize_2 (TargetModel,t_model2);
      p_pos2.X = (int)(p_pos.X * scale + 0.5f);
      p_pos2.Y = (int)(p_pos.Y * scale + 0.5f);
      MatchingScore = PerformMatching (s_image2,t_model2,p_pos2,m_range1,m_pos2);
      MatchingPos.X = (int)(m_pos2.X / scale + 0.5f);
      MatchingPos.Y = (int)(m_pos2.Y / scale + 0.5f);
   }
   else {
      m_range1 = MatchingRange;
      MatchingScore = PerformMatching (s_image,TargetModel,p_pos,m_range1,MatchingPos);
   }
   int m_range2 = m_range1 / 2;
   #if defined(__DEBUG_TGT)
   logd ("Matching score: %f\n",MatchingScore);
   logd ("Matching position: X = %d, Y = %d\n",MatchingPos.X,MatchingPos.Y);
   #endif
   MatchingSize(TargetModel.Width,TargetModel.Height);
   GImage t_model;
   if (MatchingScore > 0.0f && MatchingScore < MSThreshold && ZoomStep > 0.0f) {
      FArray1D m_score(3);
      IP2DArray1D m_pos(3);
      IS2DArray1D m_size(3);
      m_score.Clear (   );
      m_pos[0]   = MatchingPos;
      m_size[0]  = MatchingSize;
      m_score[0] = MatchingScore;
      if (scale < 1.0f) {
         float zf = 1.0f - ZoomStep;
         t_model.Create ((int)(t_model2.Width * zf + 0.5f),(int)(t_model2.Height * zf + 0.5f));
         Resize_2 (t_model2,t_model);
         m_score[1] = PerformMatching (s_image2,t_model,m_pos2,m_range2,m_pos[1]);
         m_pos[1].X = (int)(m_pos[1].X / scale + 0.5f);
         m_pos[1].Y = (int)(m_pos[1].Y / scale + 0.5f);
         m_size[1].Width  = (int)(t_model.Width  / scale + 0.5f);
         m_size[1].Height = (int)(t_model.Height / scale + 0.5f);
         zf = 1.0f + ZoomStep;
         t_model.Create ((int)(t_model2.Width * zf + 0.5f),(int)(t_model2.Height * zf + 0.5f));
         Resize_2 (t_model2,t_model);
         m_score[2] = PerformMatching (s_image2,t_model,m_pos2,m_range2,m_pos[2]);
         m_pos[2].X = (int)(m_pos[2].X / scale + 0.5f);
         m_pos[2].Y = (int)(m_pos[2].Y / scale + 0.5f);
         m_size[2].Width  = (int)(t_model.Width  / scale + 0.5f);
         m_size[2].Height = (int)(t_model.Height / scale + 0.5f);
      }
      else {
         float zf = 1.0f - ZoomStep;
         t_model.Create ((int)(TargetModel.Width * zf + 0.5f),(int)(TargetModel.Height * zf + 0.5f));
         Resize_2 (TargetModel,t_model);
         m_score[1] = PerformMatching (s_image,t_model,MatchingPos,m_range2,m_pos[1]);
         m_size[1](t_model.Width,t_model.Height);
         zf = 1.0f + ZoomStep;
         t_model.Create ((int)(TargetModel.Width * zf + 0.5f),(int)(TargetModel.Height * zf + 0.5f));
         Resize_2 (TargetModel,t_model);
         m_score[2] = PerformMatching (s_image,t_model,MatchingPos,m_range2,m_pos[2]);
         m_size[2](t_model.Width,t_model.Height);
      }
      int   max_i;
      float max_score;
      FindMaximum (m_score,max_score,max_i);
      MatchingPos   = m_pos[max_i];
      MatchingSize  = m_size[max_i];
      MatchingScore = m_score[max_i];
      #if defined(__DEBUG_TGT)
      int i;
      for (i = 0; i < 3; i++)
         logd ("[%d] Matching score: %f, Matching position: (%d,%d), Matching size: (%d,%d)\n",i,m_score[i],m_pos[i].X,m_pos[i].Y,m_size[i].Width,m_size[i].Height);
      #endif
   }
   if (MatchingScore > 0.0f) {
      UpdateTargetRegion (   );
      #if defined(__DEBUG_TGT)
      logd ("Target region updated.\n");
      #endif
      UpdateTargetModel (s_image);
      #if defined(__DEBUG_TGT)
      logd ("Target model updated.\n");
      #endif
   }
   #if defined(__DEBUG_TGT)
   if (_DebugView_TGT) {
      _DebugView_TGT->DrawImage (TargetModel,_DVX_TGT,_DVY_TGT);
      _DebugView_TGT->Invalidate (   );
   }
   #endif
   return (MatchingScore);
}

 float TargetTracking_NCC::PerformMatching (GImage &s_image,GImage &t_image,IPoint2D &b_pos,int m_range,IPoint2D &m_pos)

{
   int xs,ys,xt,yt;
   int x0,y0,x1,y1;
   int sxs,exs,sys,eys;
   int sxt,ext,syt,eyt;
   int ps_s,ps_ss,ps_t,ps_tt;
   int old_sxs = 0,old_exs = 0;
   int old_sxt = 0,old_ext = 0;
   int ts_s = 0, ts_t = 0;
   double ts_ss = 0.0,ts_tt = 0.0;

   m_pos = b_pos;
   int xm = 0, ym = 0;
   double max_score = 0.0;
   IPoint2D min_pos,max_pos;
   min_pos.X = b_pos.X - m_range;
   min_pos.Y = b_pos.Y - m_range;
   max_pos.X = b_pos.X + m_range;
   max_pos.Y = b_pos.Y + m_range;
   for (y0 = min_pos.Y; y0 <= max_pos.Y; y0++) {
      y1 = y0 + t_image.Height;
      if (y0 < 0) sys = 0;
      else sys = y0;
      if (y1 < s_image.Height) eys = y1;
      else eys = s_image.Height;
      if (sys >= eys) return (0.0f);
      syt = sys - y0; 
      eyt = eys - y0;
      for (x0 = min_pos.X; x0 <= max_pos.X; x0++) {
         x1 = x0 + t_image.Width;
         if (x0 < 0) sxs = 0;
         else sxs = x0;
         if (x1 < s_image.Width) exs = x1;
         else exs = s_image.Width;
         if (sxs >= exs) return (0.0f);
         if (x0 == min_pos.X) {
            ts_s  = ts_t  = 0;
            ts_ss = ts_tt = 0.0;
            for (ys = sys; ys < eys; ys++) {
               yt = ys - y0;
               for (xs = sxs; xs < exs; xs++) {
                  xt = xs - x0;
                  ts_s  += s_image[ys][xs];
                  ts_t  += t_image[yt][xt];
                  ts_ss += (int)s_image[ys][xs] * s_image[ys][xs];
                  ts_tt += (int)t_image[yt][xt] * t_image[yt][xt];
               }
            }
         }
         else {
            if (sxs != old_sxs) {
               ps_s = ps_ss = 0;
               for (ys = sys; ys < eys; ys++) {
                  ps_s  += s_image[ys][old_sxs];
                  ps_ss += (int)s_image[ys][old_sxs] * s_image[ys][old_sxs];
               }
               ts_s  -= ps_s;
               ts_ss -= ps_ss;
            }
            if (exs != old_exs) {
               xs = exs - 1;
               ps_s = ps_ss = 0;
               for (ys = sys; ys < eys; ys++) {
                  ps_s  += s_image[ys][xs];
                  ps_ss += (int)s_image[ys][xs] * s_image[ys][xs];
               }
               ts_s  += ps_s;
               ts_ss += ps_ss;
            }
            sxt = sxs - x0;
            ext = exs - x0;
            if (sxt != old_sxt) {
               ps_t = ps_tt = 0;
               for (yt = syt; yt < eyt; yt++) {
                  ps_t  += t_image[yt][sxt];
                  ps_tt += (int)t_image[yt][sxt] * t_image[yt][sxt];
               }
               ts_t  += ps_t;
               ts_tt += ps_tt;
            }
            if (ext != old_ext) {
               xt = old_ext - 1;
               ps_t = ps_tt = 0;
               for (yt = syt; yt < eyt; yt++) {
                  ps_t  += t_image[yt][xt];
                  ps_tt += (int)t_image[yt][xt] * t_image[yt][xt];
               }
               ts_t  -= ps_t;
               ts_tt -= ps_tt;
            }
         }
         old_sxs = sxs;
         old_exs = exs;
         old_sxt = sxs - x0;
         old_ext = exs - x0;
         uint   nps   = 0;
         double ts_st = 0.0;
         for (ys = sys; ys < eys; ys++) {
            yt = ys - y0;
            for (xs = sxs; xs < exs; xs++) {
               ts_st += (int)s_image[ys][xs] * t_image[yt][xs - x0];
               nps++;
            }
         }
         if (nps) {
            double m_s  = (double)ts_s / nps;
            double m_t  = (double)ts_t / nps;
            double m_ss = ts_ss / nps;
            double m_tt = ts_tt / nps;
            double m_st = ts_st / nps;
            double score  = (m_st - m_s * m_t) / (sqrt(m_ss - m_s * m_s) * sqrt(m_tt - m_t * m_t));
            if (score > max_score) {
               max_score = score;
               xm = x0;
               ym = y0;
            }
         }
      }
   }
   m_pos(xm,ym);
   return ((float)max_score);
}

 void TargetTracking_NCC::SetTargetSize (ISize2D &size)
 
{
   TargetTracking::SetTargetSize (size);
   GImage t_image(size.Width,size.Height);
   Resize_2 (TargetModel,t_image);
   TargetModel = t_image;
}

 void TargetTracking_NCC::UpdateTargetModel (GImage &s_image)

{
   int x,y;

   if (MatchingScore >= MSThreshold || MatchingScore < MinMatchingScore) return;
   GImage t_model(MatchingSize.Width,MatchingSize.Height);
   Resize_2 (TargetModel,t_model);
   GImage d_image(MatchingSize.Width,MatchingSize.Height);
   IBox2D s_rgn(MatchingPos.X,MatchingPos.Y,MatchingSize.Width,MatchingSize.Height);
   GetImagePatch (s_image,s_rgn,d_image);
   float a = 1.0f - ModelUpdateRate;
   for (y = 0; y < t_model.Height; y++) {
      byte *l_t_model = t_model[y];
      byte *l_d_image = d_image[y];
      for (x = 0; x < t_model.Width; x++)
         l_t_model[x] = (byte)(a * l_t_model[x] + ModelUpdateRate * l_d_image[x]);
   }
   TargetModel = t_model;
}

 void TargetTracking_NCC::UpdateTargetRegion (   )

{
   FPoint2D r_pos,t_pos;
   r_pos.X = (float)(TargetRegion.X + TargetRegion.Width  / 2);
   r_pos.Y = (float)(TargetRegion.Y + TargetRegion.Height / 2);
   t_pos.X = (float)(MatchingPos.X  + MatchingSize.Width  / 2);
   t_pos.Y = (float)(MatchingPos.Y  + MatchingSize.Height / 2);
   if (VelocityUpdateRate) {
      if (TargetVelocity.X == MC_VLV) TargetVelocity = t_pos - r_pos;
      else TargetVelocity = (1.0f - VelocityUpdateRate) * TargetVelocity + VelocityUpdateRate * (t_pos - r_pos);
   }
   else TargetVelocity(0.0f,0.0f);
   TargetRegion.X      = MatchingPos.X;
   TargetRegion.Y      = MatchingPos.Y;
   TargetRegion.Width  = MatchingSize.Width;
   TargetRegion.Height = MatchingSize.Height;
}

}

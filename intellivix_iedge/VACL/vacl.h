#if !defined(__VACL_H)
#define __VACL_H

#include "vacl_abdobj.h"
#include "vacl_binimg.h"
#include "vacl_blbtrack.h"
#include "vacl_boxtrack.h"
#include "vacl_bsttree.h"
#include "vacl_cam2cam.h"
#include "vacl_camera.h"
#include "vacl_color.h"
#include "vacl_conrgn.h"
#include "vacl_corner.h"
#include "vacl_crowd.h"
#include "vacl_define.h"
#include "vacl_defog.h"
#include "vacl_dnn.h"
#include "vacl_dwell.h"
#include "vacl_edge.h"
#include "vacl_environ.h"
#include "vacl_event.h"
#include "vacl_evtrule.h"
#include "vacl_evtzone.h"
//#include "vacl_face.h"
#include "vacl_feature.h"
#include "vacl_filter.h"
#include "vacl_fire.h"
#include "vacl_fmod.h"
#include "vacl_foregnd.h"
#include "vacl_foregnd2.h"
#include "vacl_foregnd3.h"
#include "vacl_foregnd4.h"
#include "vacl_geometry.h"
#include "vacl_globjtrk.h"
#include "vacl_grptrack.h"
#include "vacl_histo.h"
#include "vacl_hough.h"
#include "vacl_human.h"
#include "vacl_imgproc.h"
#include "vacl_imgreg.h"
#include "vacl_imgsynth.h"
#include "vacl_ipp.h"
#include "vacl_light.h"
#include "vacl_misc.h"
#include "vacl_morph.h"
#include "vacl_motion.h"
#include "vacl_nms.h"
#include "vacl_noise.h"
#include "vacl_objclass.h"
#include "vacl_objdetec.h"
#include "vacl_objfilter.h"
#include "vacl_objmatch.h"
#include "vacl_objrecog.h"
#include "vacl_objtrack.h"
#include "vacl_opencv.h"
#include "vacl_optimize.h"
//#include "vacl_panorama.h"
#include "vacl_pathpass.h"
#include "vacl_profile.h"
#include "vacl_realsize.h"
#include "vacl_sample.h"
#include "vacl_schedule.h"
#include "vacl_segment.h"
#include "vacl_shadow.h"
#include "vacl_svm.h"
#include "vacl_tgtrack.h"
#include "vacl_thumbcap.h"
#include "vacl_traffic.h"
//#include "vacl_vaengine.h"
#include "vacl_videnhnc.h"
//#include "vacl_vidstab.h"
#include "vacl_warping.h"
#include "vacl_water.h"

using namespace VACL;

#endif


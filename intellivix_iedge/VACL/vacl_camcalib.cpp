#include "vacl_camcalib.h"
#include "vacl_event.h"

#if defined(__DEBUG_CAC)
#include "Win32CL/Win32CL.h"
extern int _DVX_CAC,_DVY_CAC;
extern CImageView* _DebugView_CAC;
#endif

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: Calibrate_WithModelDepth
//
///////////////////////////////////////////////////////////////////////////////

Calibrate_WithModelDepth::Calibrate_WithModelDepth(int width, int height, vector<DPoint2D>& model_ts, vector<DPoint2D>& model_bs, vector<double>& model_hs, double k1)
{
   HalfWidth = double(width) / 2.0;
   HalfHeight = double(height) / 2.0;
   ModelTops = model_ts;
   ModelBottoms = model_bs;
   ModelHeights = model_hs;
   Kappa = k1;
}

double Calibrate_WithModelDepth::getCost(double focal, double tan_tilt, double cam_height, double k1)
{
   double cost = 0.0;
   int d_idx = 0;
   for (uint i = 0; i < ModelHeights.size(); ++i)
   {
      DPoint2D bottom_undistorted = getUndistortedPixelPosition(ModelBottoms[i], k1);
      double vb = bottom_undistorted.Y - HalfHeight;
      double z_bot = cam_height * (focal - vb * tan_tilt);
      z_bot /= focal * tan_tilt + vb;

      DPoint2D top_undistorted = getUndistortedPixelPosition(ModelTops[i], k1);
      double vt = top_undistorted.Y - HalfHeight;
      double z_top = (cam_height - ModelHeights[i]) * (focal - vt * tan_tilt);
      z_top /= focal * tan_tilt + vt;

      cost += (z_top - z_bot) * (z_top - z_bot);
      //cost += abs(z_top - z_bot);

      double ut = top_undistorted.X - HalfWidth;
      double ub = bottom_undistorted.X - HalfWidth;
      double ut1 = ut * (tan_tilt * (cam_height - ModelHeights[i]) + z_top) / (z_top * focal);
      double ub1 = ub * (tan_tilt * cam_height + z_bot) / (z_bot * focal);
      //cost += (ut1 - ub1) * (ut1 - ub1);
      cost += abs(ut1 - ub1);
      
      // 현재는 사용하지 않는 코드
      // 물체들 간의 실거리 정보를 아는 경우, tilt의 경계값(0, 90)에서 f가 소거되는 문제를 보완하기 위한 추가 코드
      #if 0
      double denom1 = focal * tan_tilt + vb;
      double factor = cam_height * cam_height * (tan_tilt * tan_tilt + 1.0);
      for (uint j = i + 1; j < ModelHeights.size(); ++j)
      {
         Point2d b2 = getUndistortedPixelPosition(ModelBottoms[j], k1);
         double u2 = b2.x - HalfWidth;
         double v2 = b2.y - HalfHeight;
         double denom2 = focal * tan_tilt + v2;
         double dist = pow(ub / denom1 - u2 / denom2, 2) + pow(vb / denom1 - v2 / denom2, 2) + pow(focal / denom1 - focal / denom2, 2);
         dist *= factor;
   
         double d2 = model_distances[d_idx] * model_distances[d_idx];
         cost += abs(d2 - dist);
         d_idx++;
      }
      #endif
   }
   return cost;
}

double Calibrate_WithModelDepth::getSubCost(double focal, double tan_tilt, double cam_height, double k1)
{
   double cost = 0.0;
   double ftan = focal * tan_tilt;
   double sec2 = tan_tilt * tan_tilt + 1.0;
   for (uint i = 0; i < ModelHeights.size(); ++i)
   {
      DPoint2D top_undistorted = getUndistortedPixelPosition(ModelTops[i], k1);
      DPoint2D bottom_undistorted = getUndistortedPixelPosition(ModelBottoms[i], k1);
      double ut = top_undistorted.X - HalfWidth;
      double vt = top_undistorted.Y - HalfHeight;
      double ub = bottom_undistorted.X - HalfWidth;
      double vb = bottom_undistorted.Y - HalfHeight;	
      
      double H_ratio = (ftan + vb) * sqrt(ut * ut * sec2 + pow(vt * tan_tilt - focal, 2));
      double h_ratio = (ftan + vt) * sqrt(ub * ub * sec2 + pow(vb * tan_tilt - focal, 2));
      h_ratio = H_ratio - h_ratio;
      double est_height = cam_height * h_ratio / H_ratio;
      cost += abs(ModelHeights[i] - est_height);
   }
   return cost;
}

DPoint2D Calibrate_WithModelDepth::getUndistortedPixelPosition(DPoint2D& uv_d, double k1)
{
   double xd = uv_d.X - HalfWidth;
   double yd = uv_d.Y - HalfHeight;
   double r2 = xd * xd + yd * yd;
   double distort = 1.0f + k1 * r2;
   
   DPoint2D uv;
   uv.X = xd * distort + HalfWidth;
   uv.Y = yd * distort + HalfHeight;
   return uv;
}

// tilt가 0도 및 90도일 때 식이 정의되지 않음
double Calibrate_WithModelDepth::getFocalLength(double tan_tilt, double cam_height, double k1)
{
   double sum = 0.0;
#if 0
   for (uint i = 0; i < ModelHeights.size(); ++i)
   {
      Point2d top_undistorted = getUndistortedPixelPosition(ModelTops[i], k1);
      Point2d bottom_undistorted = getUndistortedPixelPosition(ModelBottoms[i], k1);
      double ut = top_undistorted.x - HalfWidth;
      double vt = top_undistorted.y - HalfHeight;
      double ub = bottom_undistorted.x - HalfWidth;
      double vb = bottom_undistorted.y - HalfHeight;

      double ubH = ub * cam_height;
      double utHh = ut * (cam_height - ModelHeights[i]);
      double f = vt * ubH - vb * utHh;
      f /= (utHh - ubH) * tan_tilt;
      
      sum += f;
   }
#else
   double Hsec2 = cam_height * (tan_tilt * tan_tilt + 1.0);
   double tan2 = tan_tilt * tan_tilt;
   for (uint i = 0; i < ModelHeights.size(); ++i)
   {
      DPoint2D top_undistorted = getUndistortedPixelPosition(ModelTops[i], k1);
      DPoint2D bottom_undistorted = getUndistortedPixelPosition(ModelBottoms[i], k1);
      double vt = top_undistorted.Y - HalfHeight;
      double vb = bottom_undistorted.Y - HalfHeight;
      double htan2 = ModelHeights[i] * tan2;
      double f = Hsec2 * (vb - vt) + vt * htan2 - ModelHeights[i] * vb;
      double d = 4.0 * vt * vb * ModelHeights[i] * htan2 + f * f;
      f += sqrt(d);
      f /= 2.0 * ModelHeights[i] * tan_tilt;
      sum += f;
   }
#endif
   return sum / ModelHeights.size();
}

double Calibrate_WithModelDepth::getTiltAngle(double f, double cam_height, double k1)
{
#if 0
   double sum = 0.0;
   for (uint i = 0; i < ModelHeights.size(); ++i)
   {
      Point2d top_undistorted = getUndistortedPixelPosition(ModelTops[i], k1);
      Point2d bottom_undistorted = getUndistortedPixelPosition(ModelBottoms[i], k1);
      double ut = top_undistorted.x - HalfWidth;
      double vt = top_undistorted.y - HalfHeight;
      double ub = bottom_undistorted.x - HalfWidth;
      double vb = bottom_undistorted.y - HalfHeight;

      double Hh = cam_height - ModelHeights[i];
      double tantilt = ub * vt * cam_height - ut * vb * Hh;
      tantilt /= f * (ut * Hh - ub * cam_height);

      sum += tantilt;
   }
   return atan(sum / ModelHeights.size()) * MC_RAD2DEG;
#else
   double fsquared = f * f;

   int num = 0;
   double sum1 = 0.0, sum2 = 0.0;
   for (uint i = 0; i < ModelHeights.size(); ++i)
   {
      DPoint2D top_undistorted = getUndistortedPixelPosition(ModelTops[i], k1);
      DPoint2D bottom_undistorted = getUndistortedPixelPosition(ModelBottoms[i], k1);
      double vt = top_undistorted.Y - HalfHeight;
      double vb = bottom_undistorted.Y - HalfHeight;
      double f2vtvb = fsquared - vt * vb;
      double hH = ModelHeights[i] - cam_height;
      double tan_tilt = ModelHeights[i] * f2vtvb;
      double d = tan_tilt * tan_tilt + 4.0 * fsquared * (vt * hH + vb * cam_height) * (vt * cam_height + vb * hH);
      if (d < 0.0) continue;
      d = sqrt(abs(d));
      double denom = 2.0 * f * (vt * hH + vb * cam_height);

      sum1 += (tan_tilt + d) / denom;
      sum2 += (tan_tilt - d) / denom;
      num++;
   }
   
   sum1 /= num;
   sum2 /= num;

   double f1 = getFocalLength(sum1, cam_height, k1);
   double f2 = getFocalLength(sum2, cam_height, k1);
   
   if (fabs(f1 - f) > fabs(f2 - f)) sum1 = sum2;
   return atan(sum1) * MC_RAD2DEG;
#endif
}

double Calibrate_WithModelDepth::getCameraHeight(double f, double tan_tilt, double k1)
{
   double sum = 0.0;
   double sec2 = tan_tilt * tan_tilt + 1.0;
   for (uint i = 0; i < ModelHeights.size(); ++i)
   {
      DPoint2D top_undistorted = getUndistortedPixelPosition(ModelTops[i], k1);
      DPoint2D bottom_undistorted = getUndistortedPixelPosition(ModelBottoms[i], k1);
      double vt = top_undistorted.Y - HalfHeight;
      double vb = bottom_undistorted.Y - HalfHeight;
      double H = ModelHeights[i] * (vt * tan_tilt - f) * (vb + f * tan_tilt);
      H /= f * (vt - vb) * sec2;
      H = H * H;
      sum += H;
   }
   // H(camera height)가 음수로 추정되는 경우가 있어서
   // H의 제곱을 최적화한 뒤 H를 리턴하도록 함
   return sqrt(sum / ModelHeights.size());
}

void Calibrate_WithModelDepth::setModelDistances(vector<double>& model_ds)
{
   ModelDistances = model_ds;
}

// =========================================================================================== //
//                                                                                             //
// Focal Length, Tilt Angle, Camera Height + (Distortion Coefficient) 추정하는 클래스          //
//                                                                                             //
// =========================================================================================== //

///////////////////////////////////////////////////////////////////////////////
//
// Class: EstimateFTH_WithModelDepth
//
///////////////////////////////////////////////////////////////////////////////

EstimateFTH_WithModelDepth::EstimateFTH_WithModelDepth(int width, int height,
                                                       vector<DPoint2D>& model_ts, vector<DPoint2D>& model_bs, vector<double>& model_hs,
                                                       double k1)
                           : Calibrate_WithModelDepth(width, height, model_ts, model_bs, model_hs, k1) 
{
}

double EstimateFTH_WithModelDepth::CostFunction(Matrix &vec_p)
{
   double f = vec_p(0);
   double tan_tilt = tan(vec_p(1) * MC_DEG2RAD);
   double cam_height = vec_p(2);

   MinCost = getCost(f, tan_tilt, cam_height, Kappa);
   return MinCost;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EstimateFTHK_WithModelDepth
//
///////////////////////////////////////////////////////////////////////////////

EstimateFTHK_WithModelDepth::EstimateFTHK_WithModelDepth(int width, int height,
                                                         vector<DPoint2D>& model_ts, vector<DPoint2D>& model_bs, vector<double>& model_hs)
                            : Calibrate_WithModelDepth(width, height, model_ts, model_bs, model_hs)
{
}

double EstimateFTHK_WithModelDepth::CostFunction(Matrix &vec_p)
{
   double f = vec_p(0);
   double tan_tilt = tan(vec_p(1) * MC_DEG2RAD);
   double cam_height = vec_p(2);
   double k1 = vec_p(3);

   MinCost = getCost(f, tan_tilt, cam_height, k1);
   return MinCost;
}

// =========================================================================================== //
//                                                                                             //
// Focal Length, Tilt Angle + (Distortion Coefficient) 추정하는 클래스                         //
//                                                                                             //
// =========================================================================================== //

///////////////////////////////////////////////////////////////////////////////
//
// Class: EstimateFT_WithModelDepth
//
///////////////////////////////////////////////////////////////////////////////

EstimateFT_WithModelDepth::EstimateFT_WithModelDepth(int width, int height, double cam_h,
                                                     vector<DPoint2D>& model_ts, vector<DPoint2D>& model_bs, vector<double>& model_hs,
                                                     double k1)
                          : Calibrate_WithModelDepth(width, height, model_ts, model_bs, model_hs, k1), CamHeight(cam_h)
{
}

double EstimateFT_WithModelDepth::CostFunction(Matrix &vec_p)
{
   double f = vec_p(0);
   double tan_tilt = tan(vec_p(1) * MC_DEG2RAD);

   MinCost = getCost(f, tan_tilt, CamHeight, Kappa);
   return MinCost;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EstimateFTK_WithModelDepth
//
///////////////////////////////////////////////////////////////////////////////

EstimateFTK_WithModelDepth::EstimateFTK_WithModelDepth(int width, int height, double cam_h,
                                                       vector<DPoint2D>& model_ts, vector<DPoint2D>& model_bs, 
                                                       vector<double>& model_hs)
                           : Calibrate_WithModelDepth(width, height, model_ts, model_bs, model_hs), CamHeight(cam_h)
{
}

double EstimateFTK_WithModelDepth::CostFunction(Matrix &vec_p)
{
   double f = vec_p(0);
   double tan_tilt = tan(vec_p(1) * MC_DEG2RAD);
   double k1 = vec_p(2);

   MinCost = getCost(f, tan_tilt, CamHeight, k1);
   return MinCost;
}


// =========================================================================================== //
//                                                                                             //
// Focal Length, Camera Height + (Distortion Coefficient) 추정하는 클래스                      //
//                                                                                             //
// =========================================================================================== //

///////////////////////////////////////////////////////////////////////////////
//
// Class: EstimateFH_WithModelDepth
//
///////////////////////////////////////////////////////////////////////////////

EstimateFH_WithModelDepth::EstimateFH_WithModelDepth(int width, int height, double tilt,
                                                     vector<DPoint2D>& model_ts, vector<DPoint2D>& model_bs, vector<double>& model_hs,
                                                     double k1)
                          : Calibrate_WithModelDepth(width, height, model_ts, model_bs, model_hs, k1), TanTilt(tan(tilt * MC_DEG2RAD))
{
}

double EstimateFH_WithModelDepth::CostFunction(Matrix &vec_p)
{
   double f = vec_p(0);
   double cam_height = vec_p(1);

   MinCost = getCost(f, TanTilt, cam_height, Kappa);
   return MinCost;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EstimateFHK_WithModelDepth
//
///////////////////////////////////////////////////////////////////////////////

EstimateFHK_WithModelDepth::EstimateFHK_WithModelDepth(int width, int height, double tilt,
                                                       vector<DPoint2D>& model_ts, vector<DPoint2D>& model_bs, 
                                                       vector<double>& model_hs)
                           : Calibrate_WithModelDepth(width, height, model_ts, model_bs, model_hs), TanTilt(tan(tilt * MC_DEG2RAD))
{
}

double EstimateFHK_WithModelDepth::CostFunction(Matrix &vec_p)
{
   double f = vec_p(0);
   double cam_height = vec_p(1);
   double k1 = vec_p(2);

   MinCost = getSubCost(f, TanTilt, cam_height, k1);
   MinCost += getCost(f, TanTilt, cam_height, k1);
   return MinCost;
}

// =========================================================================================== //
//                                                                                             //
// Tilt Angle, Camera Height + (Distortion Coefficient) 추정하는 클래스                        //
//                                                                                             //
// =========================================================================================== //

///////////////////////////////////////////////////////////////////////////////
//
// Class: EstimateTH_WithModelDepth
//
///////////////////////////////////////////////////////////////////////////////

EstimateTH_WithModelDepth::EstimateTH_WithModelDepth(int width, int height, double focal,
                                                     vector<DPoint2D>& model_ts, vector<DPoint2D>& model_bs, vector<double>& model_hs,
                                                     double k1)
                          : Calibrate_WithModelDepth(width, height, model_ts, model_bs, model_hs, k1), F(focal)
{
}

double EstimateTH_WithModelDepth::CostFunction(Matrix &vec_p)
{
   double tan_tilt = tan(vec_p(0) * MC_DEG2RAD);
   double cam_height = vec_p(1);

   MinCost = getCost(F, tan_tilt, cam_height, Kappa);
   return MinCost;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EstimateTHK_WithModelDepth
//
///////////////////////////////////////////////////////////////////////////////

EstimateTHK_WithModelDepth::EstimateTHK_WithModelDepth(int width, int height, double focal,
                                                       vector<DPoint2D>& model_ts, vector<DPoint2D>& model_bs, 
                                                       vector<double>& model_hs)
                           : Calibrate_WithModelDepth(width, height, model_ts, model_bs, model_hs), F(focal)
{
}

double EstimateTHK_WithModelDepth::CostFunction(Matrix &vec_p)
{
   double tan_tilt = tan(vec_p(0) * MC_DEG2RAD);
   double cam_height = vec_p(1);
   double k1 = vec_p(2);

   MinCost = getCost(F, tan_tilt, cam_height, k1);
   return MinCost;
}

// =========================================================================================== //
//                                                                                             //
// Focal Length + (Distortion Coefficient) 추정하는 클래스                                     //
//                                                                                             //
// =========================================================================================== //

///////////////////////////////////////////////////////////////////////////////
//
// Class: EstimateF_WithModelDepth
//
///////////////////////////////////////////////////////////////////////////////

EstimateF_WithModelDepth::EstimateF_WithModelDepth(int width, int height, double tilt, double cam_h,
                                                   vector<DPoint2D>& model_ts, vector<DPoint2D>& model_bs, vector<double>& model_hs,
                                                   double k1)
                         : Calibrate_WithModelDepth(width, height, model_ts, model_bs, model_hs, k1), 
                           TanTilt(tan(tilt * MC_DEG2RAD)), CamHeight(cam_h)
{
}

double EstimateF_WithModelDepth::CostFunction(Matrix &vec_p) 
{ 
   MinCost = getCost(vec_p(0), TanTilt, CamHeight, Kappa);
   return MinCost; 
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EstimateFK_WithModelDepth
//
///////////////////////////////////////////////////////////////////////////////

EstimateFK_WithModelDepth::EstimateFK_WithModelDepth(int width, int height, double tilt, double cam_h,
                                                     vector<DPoint2D>& model_ts, vector<DPoint2D>& model_bs, vector<double>& model_hs)
                          : EstimateF_WithModelDepth(width, height, tilt, cam_h, model_ts, model_bs, model_hs)
{
}

double EstimateFK_WithModelDepth::CostFunction(Matrix &vec_p)
{
   double k1 = vec_p(0);
   double f = getFocalLength(TanTilt, CamHeight, k1);
   MinCost = getCost(f, TanTilt, CamHeight, k1);
   return MinCost;
}

// =========================================================================================== //
//                                                                                             //
// Tilt Angle + (Distortion Coefficient) 추정하는 클래스                                       //
//                                                                                             //
// =========================================================================================== //

///////////////////////////////////////////////////////////////////////////////
//
// Class: EstimateT_WithModelDepth
//
///////////////////////////////////////////////////////////////////////////////

EstimateT_WithModelDepth::EstimateT_WithModelDepth(int width, int height, double focal, double cam_h,
                                                   vector<DPoint2D>& model_ts, vector<DPoint2D>& model_bs, vector<double>& model_hs, 
                                                   double k1)
                         : Calibrate_WithModelDepth(width, height, model_ts, model_bs, model_hs, k1), 
                           F(focal), CamHeight(cam_h)
{
}

double EstimateT_WithModelDepth::CostFunction(Matrix &vec_p)
{
   double tan_tilt = tan(vec_p(0) * MC_DEG2RAD);

   MinCost = getCost(F, tan_tilt, CamHeight, Kappa);
   return MinCost;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EstimateTK_WithModelDepth
//
///////////////////////////////////////////////////////////////////////////////

EstimateTK_WithModelDepth::EstimateTK_WithModelDepth(int width, int height, double focal, double cam_h,
                                                     vector<DPoint2D>& model_ts, vector<DPoint2D>& model_bs, 
                                                     vector<double>& model_hs)
                          : EstimateT_WithModelDepth(width, height, focal, cam_h, model_ts, model_bs, model_hs)
{
}

double EstimateTK_WithModelDepth::CostFunction(Matrix &vec_p)
{
   double k1 = vec_p(0);
   double tan_tilt = tan(getTiltAngle(F, CamHeight, k1) * MC_DEG2RAD);
   MinCost = getCost(F, tan_tilt, CamHeight, k1);
   return MinCost;
}


// =========================================================================================== //
//                                                                                             //
// Camera Height + (Distortion Coefficient) 추정하는 클래스                                    //
//                                                                                             //
// =========================================================================================== //

///////////////////////////////////////////////////////////////////////////////
//
// Class: EstimateH_WithModelDepth
//
///////////////////////////////////////////////////////////////////////////////

EstimateH_WithModelDepth::EstimateH_WithModelDepth(int width, int height, double focal, double tilt,
                                                   vector<DPoint2D>& model_ts, vector<DPoint2D>& model_bs, vector<double>& model_hs,
                                                   double k1)
                         : Calibrate_WithModelDepth(width, height, model_ts, model_bs, model_hs, k1), 
                           F(focal), TanTilt(tan(tilt * MC_DEG2RAD))
{
}

double EstimateH_WithModelDepth::CostFunction(Matrix &vec_p)
{
   double cam_height = vec_p(0);

   MinCost = getCost(F, TanTilt, cam_height, Kappa);
   return MinCost;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EstimateHK_WithModelDepth
//
///////////////////////////////////////////////////////////////////////////////

EstimateHK_WithModelDepth::EstimateHK_WithModelDepth(int width, int height, double focal, double tilt,
                                                     vector<DPoint2D>& model_ts, vector<DPoint2D>& model_bs, vector<double>& model_hs)
                          : EstimateH_WithModelDepth(width, height, focal, tilt, model_ts, model_bs, model_hs)
{
}

double EstimateHK_WithModelDepth::CostFunction(Matrix &vec_p)
{
   double k1 = vec_p(0);
   double cam_height = getCameraHeight(F, TanTilt, k1);
   MinCost = getCost(F, TanTilt, cam_height, k1);
   return MinCost;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CamAutoCalibByRefObjs
//
///////////////////////////////////////////////////////////////////////////////

CamAutoCalibByRefObjs::CamAutoCalibByRefObjs(int width, int height, 
                                 vector<DPoint2D>& model_ts, vector<DPoint2D>& model_bs, vector<double>& model_hs)
{
   Width = width;
   Height = height;
   ModelTops = model_ts;
   ModelBottoms = model_bs;
   ModelHeights = model_hs;
}

CamAutoCalibByRefObjs::~CamAutoCalibByRefObjs()
{
   
}

void CamAutoCalibByRefObjs::calibrateF(double& est_focal, double tilt, double cam_h, double k1)
{
   if (tilt == 0.0 || tilt == 90.0)
   {
      logd("Invalid Tilt Angle...\n");
      return;
   }

   EstimateF_WithModelDepth est_params(Width, Height, tilt, cam_h, ModelTops, ModelBottoms, ModelHeights, k1);
   est_focal = est_params.getFocalLength(est_params.TanTilt, est_params.CamHeight, k1);
}

void CamAutoCalibByRefObjs::calibrateFK(double& est_focal, double& est_k1, double tilt, double cam_h)
{
   if (tilt == 0.0 || tilt == 90.0)
   {
      logd("Invalid Tilt Angle...\n");
      return;
   }

   EstimateFK_WithModelDepth est_params(Width, Height, tilt, cam_h, ModelTops, ModelBottoms, ModelHeights);
   est_params.Initialize(1);
   est_params.ParamVector(0) = 0.0;  // kappa1
   est_params.Perform();

   est_k1 = est_params.ParamVector(0);
   est_focal = est_params.getFocalLength(est_params.TanTilt, est_params.CamHeight, est_k1);
   est_params.Close();
}

void CamAutoCalibByRefObjs::calibrateT(double& est_tilt, double focal, double cam_h, double k1)
{
   EstimateT_WithModelDepth est_params(Width, Height, focal, cam_h, ModelTops, ModelBottoms, ModelHeights, k1);
   est_tilt = est_params.getTiltAngle(est_params.F, est_params.CamHeight, k1);
}

void CamAutoCalibByRefObjs::calibrateTK(double& est_tilt, double& est_k1, double focal, double cam_h)
{
   EstimateTK_WithModelDepth est_params(Width, Height, focal, cam_h, ModelTops, ModelBottoms, ModelHeights);
   est_params.Initialize(1);
   est_params.ParamVector(0) = 0.0;  // kappa1
   est_params.Perform();

   est_k1 = est_params.ParamVector(0);
   est_tilt = est_params.getTiltAngle(est_params.F, est_params.CamHeight, est_k1);
   est_params.Close();
}

void CamAutoCalibByRefObjs::calibrateH(double& est_camh, double focal, double tilt, double k1)
{
   if (tilt == 0.0 || tilt == 90.0)
   {
      logd("Invalid Tilt Angle...\n");
      return;
   }

   EstimateH_WithModelDepth est_params(Width, Height, focal, tilt, ModelTops, ModelBottoms, ModelHeights, k1);
   est_camh = est_params.getCameraHeight(est_params.F, est_params.TanTilt, k1);
}

void CamAutoCalibByRefObjs::calibrateHK(double& est_camh, double& est_k1, double focal, double tilt)
{
   if (tilt == 0.0 || tilt == 90.0)
   {
      logd("Invalid Tilt Angle...\n");
      return;
   }

   EstimateHK_WithModelDepth est_params(Width, Height, focal, tilt, ModelTops, ModelBottoms, ModelHeights);
   est_params.Initialize(1);
   est_params.ParamVector(0) = 0.0;  // kappa1
   est_params.Perform();

   est_k1 = est_params.ParamVector(0);
   est_camh = est_params.getCameraHeight(est_params.F, est_params.TanTilt, est_k1);
   est_params.Close();
}

void CamAutoCalibByRefObjs::calibrateFT(double& est_focal, double& est_tilt, double cam_h, double k1)
{
   EstimateFT_WithModelDepth est_params(Width, Height, cam_h, ModelTops, ModelBottoms, ModelHeights, k1);
   est_params.Initialize(2);

   double cost = MC_VLV;
   for (int t = 1; t < 90; ++t)
   {
      est_params.ParamVector(0) = double(Height); // focal length
      est_params.ParamVector(1) = double(t);      // tilt in degree
      est_params.Perform();

      if (est_params.ParamVector(0) < 100.0 || est_params.ParamVector(0) > 2000.0) continue;

      if (cost > est_params.MinCost)
      {
         cost = est_params.MinCost;
         est_focal = est_params.ParamVector(0);
         est_tilt = est_params.ParamVector(1);
      }
   }
   est_params.Close();
}

void CamAutoCalibByRefObjs::calibrateFTK(double& est_focal, double& est_tilt, double& est_k1, double cam_h)
{
   EstimateFTK_WithModelDepth est_params(Width, Height, cam_h, ModelTops, ModelBottoms, ModelHeights);
   est_params.Initialize(3);

   double cost = MC_VLV;
   for (int t = 1; t < 90; ++t)
   {
      est_params.ParamVector(0) = double(Height); // focal length
      est_params.ParamVector(1) = double(t);      // tilt in degree
      est_params.ParamVector(2) = 0.0;            // kappa1
      est_params.Perform();

      if (est_params.ParamVector(0) < 100.0 || est_params.ParamVector(0) > 2000.0) continue;

      if (cost > est_params.MinCost)
      {
         cost = est_params.MinCost;
         est_focal = est_params.ParamVector(0);
         est_tilt = est_params.ParamVector(1);
         est_k1 = est_params.ParamVector(2);
      }
   }
   est_params.Close();
}

void CamAutoCalibByRefObjs::calibrateFH(double& est_focal, double& est_camh, double tilt, double k1)
{
   if (tilt == 0.0 || tilt == 90.0)
   {
      logd("Invalid Tilt Angle...\n");
      return;
   }

   EstimateFH_WithModelDepth est_params(Width, Height, tilt, ModelTops, ModelBottoms, ModelHeights, k1);
   est_params.Initialize(2);

   est_params.ParamVector(0) = double(Height); // focal length
   est_params.ParamVector(1) = 1.0;            // camera height in meter
   est_params.Perform();

   est_focal = est_params.ParamVector(0);
   est_camh = est_params.ParamVector(1);
   est_params.Close();
}

void CamAutoCalibByRefObjs::calibrateFHK(double& est_focal, double& est_camh, double& est_k1, double tilt)
{
   if (tilt == 0.0 || tilt == 90.0)
   {
      logd("Invalid Tilt Angle...\n");
      return;
   }

   EstimateFHK_WithModelDepth est_params(Width, Height, tilt, ModelTops, ModelBottoms, ModelHeights);
   est_params.Initialize(3);

   est_params.ParamVector(0) = double(Height); // focal length
   est_params.ParamVector(1) = 1.0;            // camera height in meter
   est_params.ParamVector(2) = 0.0;            // kappa1
   est_params.Perform();

   est_focal = est_params.ParamVector(0);
   est_camh = est_params.ParamVector(1);
   est_k1 = est_params.ParamVector(2);
   est_params.Close();
}

void CamAutoCalibByRefObjs::calibrateTH(double& est_tilt, double& est_camh, double focal, double k1)
{
   EstimateTH_WithModelDepth est_params(Width, Height, focal, ModelTops, ModelBottoms, ModelHeights, k1);
   est_params.Initialize(2);

   double cost = MC_VLV;
   for (int t = 1; t < 90; ++t)
   {
      est_params.ParamVector(0) = double(t);  // tilt in degree
      est_params.ParamVector(1) = 1.0;        // camera height in meter
      est_params.Perform();

      if (est_params.ParamVector(1) < 0.0f) continue;

      if (cost > est_params.MinCost)
      {
         cost = est_params.MinCost;
         est_tilt = est_params.ParamVector(0);
         est_camh = est_params.ParamVector(1);
      }
   }
   est_params.Close();
}

void CamAutoCalibByRefObjs::calibrateTHK(double& est_tilt, double& est_camh, double& est_k1, double focal)
{
   EstimateTHK_WithModelDepth est_params(Width, Height, focal, ModelTops, ModelBottoms, ModelHeights);
   est_params.Initialize(3);

   double cost = MC_VLV;
   for (int t = 1; t < 90; ++t)
   {
      est_params.ParamVector(0) = double(t);  // tilt in degree
      est_params.ParamVector(1) = 1.0;        // camera height in meter
      est_params.ParamVector(2) = 0.0;        // kappa1
      est_params.Perform();

      if (est_params.ParamVector(1) < 0.0) continue;

      if (cost > est_params.MinCost)
      {
         cost = est_params.MinCost;
         est_tilt = est_params.ParamVector(0);
         est_camh = est_params.ParamVector(1);
         est_k1 = est_params.ParamVector(2);
      }
   }
   est_params.Close();
}

void CamAutoCalibByRefObjs::calibrateFTH(double& est_focal, double& est_tilt, double& est_camh, double k1)
{
   EstimateFTH_WithModelDepth est_params(Width, Height, ModelTops, ModelBottoms, ModelHeights, k1);
   est_params.Initialize(3);

   double cost = MC_VLV;
   for (int t = 1; t < 90; ++t)
   {
      est_params.ParamVector(0) = double(Height); // focal length
      est_params.ParamVector(1) = double(t);      // tilt in degree
      est_params.ParamVector(2) = 1.0;            // camera height in meter
      est_params.Perform();

      if(est_params.ParamVector(0) < 100.0 || est_params.ParamVector(0) > 2000.0 || est_params.ParamVector(2) < 0.0) continue;

      if (cost > est_params.MinCost)
      {
         cost = est_params.MinCost;
         est_focal = est_params.ParamVector(0);
         est_tilt = est_params.ParamVector(1);
         est_camh = est_params.ParamVector(2);
      }
   }
   est_params.Close();
}

void CamAutoCalibByRefObjs::calibrateFTHK(double& est_focal, double& est_tilt, double& est_camh, double& est_k1)
{
   EstimateFTHK_WithModelDepth est_params(Width, Height, ModelTops, ModelBottoms, ModelHeights);
   est_params.Initialize(4);

   double cost = MC_VLV;
   for (int t = 1; t < 90; ++t)
   {
      est_params.ParamVector(0) = double(Height); // focal length
      est_params.ParamVector(1) = double(t);      // tilt in degree
      est_params.ParamVector(2) = 1.0;            // camera height in meter
      est_params.ParamVector(3) = 0.0;            // kappa1
      est_params.Perform();

      if(est_params.ParamVector(0) < 100.0 || est_params.ParamVector(0) > 2000.0 || est_params.ParamVector(2) < 0.0) continue;

      if (cost > est_params.MinCost)
      {
         cost = est_params.MinCost;
         est_focal = est_params.ParamVector(0);
         est_tilt = est_params.ParamVector(1);
         est_camh = est_params.ParamVector(2);
         est_k1 = est_params.ParamVector(3);
      }
   }
   est_params.Close();
}

void CamAutoCalibByRefObjs::setModels(vector<DPoint2D>& model_ts, vector<DPoint2D>& model_bs, vector<double>& model_hs)
{
   ModelTops = model_ts;
   ModelBottoms = model_bs;
   ModelHeights = model_hs;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CalibObject
//
///////////////////////////////////////////////////////////////////////////////

 CalibObject::CalibObject (   )

{
   Length     = 0.0f;
   RealHeight = 0.0f;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CameraAutoCalibration_ObjTrk
//
///////////////////////////////////////////////////////////////////////////////

 CameraAutoCalibration_ObjTrk::CameraAutoCalibration_ObjTrk (   )

{
   MinNumObjects     = 5;
   RealObjHeight     = 1.7f;
   SamplingRate      = 5.0f;
   MinLearningTime   = 0.1f * 60.0f;
   MaxLearningTime   = 3.0f * 60.0f;
   MinObjHeight      = 0.05f;
   MaxObjHeight      = 0.30f;
   MinObjNorSpeed    = 0.0f;
   MaxObjNorSpeed    = 250.0f;
   MinObjAspectRatio = 1.4f;
   MaxObjAspectRatio = 4.0f;
   _Init (   );
}

 CameraAutoCalibration_ObjTrk::~CameraAutoCalibration_ObjTrk (   )

{
   Close (   );
}

 void CameraAutoCalibration_ObjTrk::_Init (   )

{
   Flag_Initialized = FALSE;
   Flag_Timeout     = FALSE;
   LearningTime     = 0.0f;
   SamplingTime     = 0.0f;
   FocalLength      = 0.0f;
   TiltAngle        = 0.0f;
   CamHeight        = 0.0f;
   RefImgSize.Clear (   );
}

 int CameraAutoCalibration_ObjTrk::CalibrateCamera (   )

{
   int i,j;

   #if defined(__DEBUG_CAC)
   _DVX_CAC = _DVY_CAC = 0;
   _DebugView_CAC->Clear (   );
   _DebugView_CAC->DrawRectangle (_DVX_CAC,_DVY_CAC,RefImgSize.Width,RefImgSize.Height,PC_YELLOW);
   #endif
   int n_objects = 0;
   vector<double>   rhs;
   vector<DPoint2D> tps,bps;
   for (i = 0; i < Objects.Height; i++) {
      CalibObject* l_objects = Objects[i];
      for (j = 0; j < Objects.Width; j++) {
         CalibObject& co = l_objects[j];
         if (co.Length > 0.0f) {
            n_objects++;
            rhs.push_back ((double)RealObjHeight);
            DPoint2D tp(co.TopPos.X,co.TopPos.Y);
            tps.push_back (tp);
            DPoint2D bp(co.BottomPos.X,co.BottomPos.Y);
            bps.push_back (bp);
            #if defined(__DEBUG_CAC)
            int x1 = (int)(_DVX_CAC + co.TopPos.X    + 0.5f);
            int y1 = (int)(_DVY_CAC + co.TopPos.Y    + 0.5f);
            int x2 = (int)(_DVX_CAC + co.BottomPos.X + 0.5f);
            int y2 = (int)(_DVY_CAC + co.BottomPos.Y + 0.5f);
            _DebugView_CAC->DrawLine (x1,y1,x2,y2,PC_RED);
            #endif
         }
      }
   }
   #if defined(__DEBUG_CAC)
   _DebugView_CAC->Invalidate (   );
   logd ("\n[Camera Calibration Result]\n");
   logd ("- # of Valid Objects: %d\n",n_objects);
   #endif
   if (n_objects < MinNumObjects) {
      #if defined(__DEBUG_CAC)
      logd ("- Insufficient number of objects!\n");
      #endif
      return (1);
   }
   CamAutoCalibByRefObjs calibrator(RefImgSize.Width,RefImgSize.Height,tps,bps,rhs);
   double focal_length,tilt_angle,cam_height;
   calibrator.calibrateFTH (focal_length,tilt_angle,cam_height);
   FocalLength = (float)focal_length;
   TiltAngle   = (float)tilt_angle;
   CamHeight   = (float)cam_height;
   #if defined(__DEBUG_CAC)
   logd ("- Focal Length : %f\n",FocalLength);
   logd ("- Tilt Angle   : %f\n",TiltAngle);
   logd ("- Camera Height: %f\n",CamHeight);
   #endif
   return (DONE);
}

 void CameraAutoCalibration_ObjTrk::Close (   )

{
   NumObjects.Delete (   );
   Objects.Delete    (   );
   _Init (   );
}

 int CameraAutoCalibration_ObjTrk::Initialize (ISize2D& ri_size)

{
   Close (   );
   RefImgSize = ri_size;
   NumObjects.Create (8);
   NumObjects.Clear  (   );
   Objects.Create    (5000,NumObjects.Length);
   Flag_Initialized = TRUE;
   return (DONE);
}

 int CameraAutoCalibration_ObjTrk::Perform (ObjectTracking& obj_tracker)

{
   int i;
   
   if (!IsInitialized (   )) return (CAC_RCODE_ERROR);
   if (FocalLength > 0.0f  ) return (CAC_RCODE_ALREADY_CALIBRATED);
   if (Flag_Timeout) return (CAC_RCODE_TIMEOUT);
   LearningTime += obj_tracker.FrameDuration;
   if (LearningTime < MinLearningTime) return (CAC_RCODE_LEARNING);
   SamplingTime += SamplingRate / obj_tracker.FrameRate;
   if (SamplingTime < 1.0f) return (CAC_RCODE_LEARNING);
   SamplingTime -= 1.0f;
   ISize2D& si_size = obj_tracker.SrcImgSize;
   float dv_size    = (float)si_size.Height / Objects.Height;
   FPoint2D scale;
   scale.X = (float)RefImgSize.Width  / si_size.Width;
   scale.Y = (float)RefImgSize.Height / si_size.Height;
   TrackedObject* t_object = obj_tracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      t_object->Status &= ~TO_STATUS_LOCKED;
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED) &&
           (t_object->Status & TO_STATUS_VERIFIED     ) &&
          !(t_object->Status & TO_STATUS_STATIC1      ) &&
          !(t_object->Status & TO_STATUS_MERGED       ) &&
          !(t_object->Status & TO_STATUS_UNDETECTED   )) {
         float obj_height = scale.Y * t_object->Height;
         float oh_ratio   = obj_height / RefImgSize.Height;
         if (MinObjHeight      <= oh_ratio              && oh_ratio              <= MaxObjHeight      &&
             MinObjNorSpeed    <= t_object->NorSpeed    && t_object->NorSpeed    <= MaxObjNorSpeed    &&  
             MinObjAspectRatio <= t_object->AspectRatio && t_object->AspectRatio <= MaxObjAspectRatio) {
            i = (int)(t_object->CenterPos.Y / dv_size);
            int& n = NumObjects[i];
            if (n < Objects.Width) {
               CalibObject& co = Objects[i][n];
               co.CenterPos   = scale * t_object->CenterPos;
               co.TopPos     = scale * (-0.5f * t_object->MajorAxisLength * t_object->MajorAxis + t_object->CenterPos);
               co.BottomPos  = scale * ( 0.5f * t_object->MajorAxisLength * t_object->MajorAxis + t_object->CenterPos);
               co.Length     = Mag(co.BottomPos - co.TopPos);
               co.RealHeight = RealObjHeight;
               n++;
               t_object->Status |= TO_STATUS_LOCKED;
            }
         }
      }
      t_object = obj_tracker.TrackedObjects.Next (t_object);
   }
   #if defined(__DEBUG_CAC)
   logd ("Learning Time: %.1f seconds (< %.1f)\n",LearningTime,MaxLearningTime);
   logd ("[# of Objects Collected]\n");
   for (i = 0; i < NumObjects.Length; i++) {
      logd ("- Section %d: %d\n",i,NumObjects[i]);
   }
   #endif
   if (LearningTime <= MaxLearningTime) return (CAC_RCODE_LEARNING);
   Flag_Timeout = TRUE;
   RemoveNoisyObjects (   );
   if (CalibrateCamera (   ) == DONE) return (CAC_RCODE_CALIBRATION_DONE);
   else return (CAC_RCODE_TIMEOUT);
}

 void CameraAutoCalibration_ObjTrk::RemoveNoisyObjects (   )

{
   int i,j,k;

   for (i = 0; i < Objects.Height; i++) {
      int n = NumObjects[i];
      if (n > 2) {
         OSArray1D os_array(n);
         CalibObject* l_objects = Objects[i];
         for (j = 0; j < n; j++) {
            os_array[j].IndexNo = j;
            os_array[j].Score   = l_objects[j].Length;
         }
         // 객체들의 길이의 median 값(m_length)을 구한다.
         qsort (os_array,n,sizeof(ObjectScore),Compare_ObjectScore_Descending);
         k = os_array[n / 2].IndexNo;
         float m_length = l_objects[k].Length;
         float d_thrsld = 0.25f * m_length; // PARAMETERS
         // 길이가 m_length로부터 많이 벗어나는 객체들을 제거한다.
         for (j = 0; j < n; j++) {
            if (fabs (l_objects[j].Length - m_length) >= d_thrsld) l_objects[j].Length = 0.0f;
         }
      }
   }
   GImage oc_map(RefImgSize.Width,RefImgSize.Height);
   oc_map.Clear (   );
   for (i = 0; i < Objects.Height; i++) {
      int n = NumObjects[i];
      CalibObject* l_objects = Objects[i];
      for (j = 0; j < n; j++) {
         CalibObject& co = l_objects[j];
         if (co.Length > 0.0f) {
            int cx = (int)co.CenterPos.X;
            int cy = (int)co.CenterPos.Y;
            if (oc_map[cy][cx]) co.Length = 0.0f;
            else {
               int w1 = (int)co.Length;
               int w2 = 2 * w1;
               IBox2D s_rgn;
               s_rgn.X = cx - w1;
               s_rgn.Y = cy - w1;
               s_rgn.Width = s_rgn.Height = w2;
               CropRegion (RefImgSize,s_rgn);
               oc_map.Set (s_rgn,PG_WHITE);
            }
         }
      }
   }
}

}

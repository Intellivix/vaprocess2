#include "vacl_objmatch.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: TargetColor
//
///////////////////////////////////////////////////////////////////////////////

 void TargetColor::Initialize (byte red,byte green,byte blue,float ms_thrsld)
 
{
   Color(blue,green,red);
   ConvertBGRToHSV (Color,Hue,Saturation,Value);
   IndexNo = HVHistogram::GetBinIndexNo (Hue,Saturation,Value);
   MSThreshold = ms_thrsld;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectColorMatching
//
///////////////////////////////////////////////////////////////////////////////

 void ObjectColorMatching::AddTargetColor (byte red,byte green,byte blue,float ms_thrsld)
 
{
   TargetColor *t_color = new TargetColor;
   t_color->Initialize (red,green,blue,ms_thrsld);
   TargetColors.Add (t_color);
}

 void ObjectColorMatching::ClearTargetColorList (   )
 
{
   TargetColors.Delete (   );
}

 int ObjectColorMatching::Perform (float *s_histogram,float &m_score)
 
{
   int n_colors = TargetColors.GetNumItems (   );
   if (!n_colors) return (0);
   int n_mcs = 0;
   m_score = 0.0f;
   TargetColor *t_color = TargetColors.First (   );
   while (t_color != NULL) {
      float score = s_histogram[t_color->IndexNo];
      if (score >= t_color->MSThreshold) n_mcs++;
      m_score += score;
      t_color = TargetColors.Next (t_color);
   }
   return (n_mcs);
}

}

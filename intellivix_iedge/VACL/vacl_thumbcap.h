#if !defined(__VACL_THUMBCAP_H)
#define __VACL_THUMBCAP_H

#include "vacl_define.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: ThumbnailCapture
//
///////////////////////////////////////////////////////////////////////////////

 class ThumbnailCapture
 
{
   public:
      int      Width;              // 썸네일 영상의 너비(주의: 4의 배수여야 함)
      int      Height;             // 썸네일 영상의 높이
      float    CapBoxSizeRatio;    // 
      float    MinObjectSizeRatio; //
      BGRPixel BkgColor;           // 썸네일 영상의 배경색
      int      CropMode;
      
   public:
      ThumbnailCapture (   );
      virtual ~ThumbnailCapture (   ) {   };
   
   protected:
      void GetCaptureBox    (ISize2D &si_size,IBox2D &obj_box,IBox2D &d_box);
      void GetCaptureParams (ISize2D &si_size,IBox2D &obj_box,ISize2D &di_size,IPoint2D &sp1,IPoint2D &ep1,IPoint2D &sp2);

   protected:
      virtual void Capture       (byte *si_buffer,ISize2D &si_size,IPoint2D &sp1,IPoint2D &ep1,IPoint2D &sp2,byte *di_buffer,ISize2D &di_size) = 0;
      virtual void Clear         (byte *si_buffer,ISize2D &si_size) = 0;
      virtual void Finalize      (BArray1D &si_buffer,ISize2D &si_size,BArray1D &di_buffer,ISize2D &di_size) = 0;
      virtual int  GetLineLength (int img_width) = 0;

   public:
      int  IsCaptureBoxInsideImage (ISize2D &si_size,IBox2D &so_box);
      int  GetImageBufferLength    (ISize2D &img_size);
      void Perform                 (byte *si_buffer,ISize2D &si_size,IBox2D &so_box,BArray1D &di_buffer,ISize2D &di_size);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: ThumbnailCapture_BGR24
//
///////////////////////////////////////////////////////////////////////////////

 class ThumbnailCapture_BGR24 : public ThumbnailCapture
 
{
   protected:
      virtual void Capture       (byte *si_buffer,ISize2D &si_size,IPoint2D &sp1,IPoint2D &ep1,IPoint2D &sp2,byte *di_buffer,ISize2D &di_size);
      virtual void Clear         (byte *si_buffer,ISize2D &si_size);
      virtual void Finalize      (BArray1D &si_buffer,ISize2D &si_size,BArray1D &di_buffer,ISize2D &di_size);
      virtual int  GetLineLength (int img_width);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: ThumbnailCapture_Gray
//
///////////////////////////////////////////////////////////////////////////////

 class ThumbnailCapture_Gray : public ThumbnailCapture
 
{
   protected:
      virtual void Capture       (byte *si_buffer,ISize2D &si_size,IPoint2D &sp1,IPoint2D &ep1,IPoint2D &sp2,byte *di_buffer,ISize2D &di_size);
      virtual void Clear         (byte *si_buffer,ISize2D &si_size);
      virtual void Finalize      (BArray1D &si_buffer,ISize2D &si_size,BArray1D &di_buffer,ISize2D &di_size);
      virtual int  GetLineLength (int img_width);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: ThumbnailCapture_YUY2
//
///////////////////////////////////////////////////////////////////////////////

 class ThumbnailCapture_YUY2 : public ThumbnailCapture
 
{
   protected:
      virtual void Capture       (byte *si_buffer,ISize2D &si_size,IPoint2D &sp1,IPoint2D &ep1,IPoint2D &sp2,byte *di_buffer,ISize2D &di_size);
      virtual void Clear         (byte *si_buffer,ISize2D &si_size);
      virtual void Finalize      (BArray1D &si_buffer,ISize2D &si_size,BArray1D &di_buffer,ISize2D &di_size);
      virtual int  GetLineLength (int img_width);
};

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

int CaptureThumbnail (byte *si_buffer,ISize2D &si_size,int pix_fmt,IBox2D &so_box,BArray1D &di_buffer,ISize2D &di_size,float cbs_ratio = 0.0f,ISize2D *tn_size = NULL);

}

#endif

#include "vacl_evtrule.h"
#include "vacl_event.h"

 namespace VACL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Global Variables
//
///////////////////////////////////////////////////////////////////////////////

extern int __DisabledEventTypes;

EventRule*       (*__CreationFunction_EventRule)()       = NULL;
ObjectFiltering* (*__CreationFunction_ObjectFiltering)() = NULL;

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_Abandoned
//
///////////////////////////////////////////////////////////////////////////////

 EventRuleConfig_Abandoned::EventRuleConfig_Abandoned (   )

{
   DetectionTime      = 5.0f;
   MaxInitMovTime     = 7.0f;
   MaxInitMovDistance = 5.0f;
}

 int EventRuleConfig_Abandoned::CheckSetup (EventRuleConfig_Abandoned* other)

{
   int ret_flag = 0;
   FileIO buffThis,buffFrom;   
   CXMLIO xmlIOThis,xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis,XMLIO_Write); 
   xmlIOFrom.SetFileIO (&buffFrom,XMLIO_Write);
   WriteFile (&xmlIOThis);
   other->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom))
      ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength (0); 
   buffFrom.SetLength (0);
   return (ret_flag);
}

 int EventRuleConfig_Abandoned::ReadFile (FileIO &file)
 
{
   if (file.Read ((byte*)&DetectionTime ,1,sizeof(float))) return (1);
   if (file.Read ((byte*)&MaxInitMovTime,1,sizeof(float))) return (2);
   if (file.Read ((byte*)&MaxInitMovDistance,1,sizeof(float))) return (3);
   return (DONE);
}

 int EventRuleConfig_Abandoned::ReadFile (CXMLIO* pIO)

{
   static EventRuleConfig_Abandoned dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Abandoned")) {
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime",&DetectionTime ,&dv.DetectionTime );
      xmlNode.Attribute (TYPE_ID(float),"MaxInitMovTime",&MaxInitMovTime,&dv.MaxInitMovTime);
      xmlNode.Attribute (TYPE_ID(float),"MaxInitMovDistance",&MaxInitMovDistance,&dv.MaxInitMovDistance);
      xmlNode.End (   );
      return (DONE);
   }   
   return (1);
}

 int EventRuleConfig_Abandoned::WriteFile (FileIO &file)
 
{
   if (file.Write ((byte*)&DetectionTime,1,sizeof(float))) return (1);
   if (file.Write ((byte*)&MaxInitMovTime,1,sizeof(float))) return (2);
   if (file.Write ((byte*)&MaxInitMovDistance,1,sizeof(float))) return (3);
   return (DONE);
}

 int EventRuleConfig_Abandoned::WriteFile (CXMLIO* pIO)

{
   static EventRuleConfig_Abandoned dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Abandoned")) {
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime" ,&DetectionTime,&dv.DetectionTime);
      xmlNode.Attribute (TYPE_ID(float),"MaxInitMovTime",&MaxInitMovTime,&dv.MaxInitMovTime);
      xmlNode.Attribute (TYPE_ID(float),"MaxInitMovDistance",&MaxInitMovDistance,&dv.MaxInitMovDistance);
      xmlNode.End (   );
      return (DONE);
   }   
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_CarAccident
//
///////////////////////////////////////////////////////////////////////////////

 EventRuleConfig_CarAccident::EventRuleConfig_CarAccident (   )
 
{
   Flag_Human    = FALSE;
   MinNumCars    = 1;
   DetectionTime = 10.0f;
}

 int EventRuleConfig_CarAccident::CheckSetup (EventRuleConfig_CarAccident* other)

{
   int ret_flag = 0;
   FileIO buffThis,buffFrom;   
   CXMLIO xmlIOThis,xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis,XMLIO_Write); 
   xmlIOFrom.SetFileIO (&buffFrom,XMLIO_Write);
   WriteFile (&xmlIOThis);
   other->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom))
      ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength (0); 
   buffFrom.SetLength (0);
   return (ret_flag);
}

 int EventRuleConfig_CarAccident::ReadFile (FileIO &file)
 
{
   if (file.Read ((byte*)&Flag_Human   ,1,sizeof(int)  )) return (1);
   if (file.Read ((byte*)&MinNumCars   ,1,sizeof(int)  )) return (2);
   if (file.Read ((byte*)&DetectionTime,1,sizeof(float))) return (3);
   return (DONE);
}

 int EventRuleConfig_CarAccident::ReadFile (CXMLIO* pIO)

{
   static EventRuleConfig_CarAccident dv;
   
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("CarAccident")) {
      xmlNode.Attribute (TYPE_ID(int)  ,"Flag_Human"   ,&Flag_Human   ,&dv.Flag_Human);
      xmlNode.Attribute (TYPE_ID(int)  ,"MinNumCars"   ,&MinNumCars   ,&dv.MinNumCars);
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime",&DetectionTime,&dv.DetectionTime);
      xmlNode.End (   );
      return (DONE);
   }   
   return (1);
}

 int EventRuleConfig_CarAccident::WriteFile (FileIO &file)
 
{
   if (file.Write ((byte*)&Flag_Human   ,1,sizeof(int)  )) return (1);
   if (file.Write ((byte*)&MinNumCars   ,1,sizeof(int)  )) return (2);
   if (file.Write ((byte*)&DetectionTime,1,sizeof(float))) return (3);
   return (DONE);
}

 int EventRuleConfig_CarAccident::WriteFile (CXMLIO* pIO)

{
   static EventRuleConfig_CarAccident dv;
   
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("CarAccident")) {
      xmlNode.Attribute (TYPE_ID(int)  ,"Flag_Human"   ,&Flag_Human   ,&dv.Flag_Human);
      xmlNode.Attribute (TYPE_ID(int)  ,"MinNumCars"   ,&MinNumCars   ,&dv.MinNumCars);
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime",&DetectionTime,&dv.DetectionTime);
      xmlNode.End (   );
      return (DONE);
   }   
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_CarParking
//
///////////////////////////////////////////////////////////////////////////////

 EventRuleConfig_CarParking::EventRuleConfig_CarParking (   )
 
{
   DetectionTime = 5.0f;
}

 int EventRuleConfig_CarParking::CheckSetup (EventRuleConfig_CarParking* other)

{
   int ret_flag = 0;
   FileIO buffThis,buffFrom;   
   CXMLIO xmlIOThis,xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis,XMLIO_Write); 
   xmlIOFrom.SetFileIO (&buffFrom,XMLIO_Write);
   WriteFile (&xmlIOThis);
   other->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom))
      ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength (0); 
   buffFrom.SetLength (0);
   return (ret_flag);
}

 int EventRuleConfig_CarParking::ReadFile (FileIO& file)
 
{
   if (file.Read ((byte*)&DetectionTime,1,sizeof(float))) return (1);
   return (DONE);
}

 int EventRuleConfig_CarParking::ReadFile (CXMLIO* pIO)
 
{
   static EventRuleConfig_CarParking dv;
   
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("CarParking")) {
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime",&DetectionTime,&dv.DetectionTime);
      xmlNode.End (   );
      return (DONE);
   }   
   return (1);
}

 int EventRuleConfig_CarParking::WriteFile (FileIO& file)
 
{
   if (file.Write ((byte*)&DetectionTime,1,sizeof(float))) return (1);
   return (DONE);
}

 int EventRuleConfig_CarParking::WriteFile (CXMLIO* pIO)
 
{
   static EventRuleConfig_CarParking dv;
   
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("CarParking")) {
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime",&DetectionTime,&dv.DetectionTime);
      xmlNode.End (   );
      return (DONE);
   }   
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_CarStopping
//
///////////////////////////////////////////////////////////////////////////////

 EventRuleConfig_CarStopping::EventRuleConfig_CarStopping (   )
 
{
   DetectionTime = 5.0f;
}

 int EventRuleConfig_CarStopping::CheckSetup (EventRuleConfig_CarStopping* other)

{
   int ret_flag = 0;
   FileIO buffThis,buffFrom;   
   CXMLIO xmlIOThis,xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis,XMLIO_Write); 
   xmlIOFrom.SetFileIO (&buffFrom,XMLIO_Write);
   WriteFile (&xmlIOThis);
   other->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom))
      ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength (0); 
   buffFrom.SetLength (0);
   return (ret_flag);
}

 int EventRuleConfig_CarStopping::ReadFile (FileIO& file)
 
{
   if (file.Read ((byte*)&DetectionTime,1,sizeof(float))) return (1);
   return (DONE);
}

 int EventRuleConfig_CarStopping::ReadFile (CXMLIO* pIO)
 
{
   static EventRuleConfig_CarStopping dv;
   
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("CarStopping")) {
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime",&DetectionTime,&dv.DetectionTime);
      xmlNode.End (   );
      return (DONE);
   }   
   return (1);
}

 int EventRuleConfig_CarStopping::WriteFile (FileIO& file)
 
{
   if (file.Write ((byte*)&DetectionTime,1,sizeof(float))) return (1);
   return (DONE);
}

 int EventRuleConfig_CarStopping::WriteFile (CXMLIO* pIO)
 
{
   static EventRuleConfig_CarStopping dv;
   
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("CarStopping")) {
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime",&DetectionTime,&dv.DetectionTime);
      xmlNode.End (   );
      return (DONE);
   }   
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_ColorChange
//
///////////////////////////////////////////////////////////////////////////////

 EventRuleConfig_ColorChange::EventRuleConfig_ColorChange (   )
 
{
   DetectionTime = 0.0f;
}

 int EventRuleConfig_ColorChange::CheckSetup (EventRuleConfig_ColorChange* other)

{
   int ret_flag = 0;
   FileIO buffThis,buffFrom;   
   CXMLIO xmlIOThis,xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis,XMLIO_Write); 
   xmlIOFrom.SetFileIO (&buffFrom,XMLIO_Write);
   WriteFile (&xmlIOThis);
   other->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom))
      ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength (0); 
   buffFrom.SetLength (0);
   return (ret_flag);
}

 int EventRuleConfig_ColorChange::ReadFile (FileIO& file)
 
{
   if (file.Read ((byte*)&DetectionTime,1,sizeof(float))) return (1);
   return (DONE);
}

 int EventRuleConfig_ColorChange::ReadFile (CXMLIO* pIO)
 
{
   static EventRuleConfig_ColorChange dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("ColorChange")) {
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime",&DetectionTime,&dv.DetectionTime);
      xmlNode.End (   );
      return (DONE);
   }   
   return (1);
}

 int EventRuleConfig_ColorChange::WriteFile (FileIO& file)
 
{
   if (file.Write ((byte*)&DetectionTime,1,sizeof(float))) return (1);
   return (DONE);
}

 int EventRuleConfig_ColorChange::WriteFile (CXMLIO* pIO)
 
{
   static EventRuleConfig_ColorChange dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("ColorChange")) {
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime",&DetectionTime,&dv.DetectionTime);
      xmlNode.End (   );
      return (DONE);
   }   
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_Counting
//
///////////////////////////////////////////////////////////////////////////////

 EventRuleConfig_Counting::EventRuleConfig_Counting (   )

{
   AspectRatio = 0.6f;
   Clear (   );
}

 void EventRuleConfig_Counting::Clear (   )
 
{
   Count = 0;
   MajorAxis[0](-1,-1);
   MajorAxis[1](-1,-1);
}

 int EventRuleConfig_Counting::CheckSetup (EventRuleConfig_Counting* other)

{
   int ret_flag = 0;
   FileIO buffThis,buffFrom;   
   CXMLIO xmlIOThis,xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis,XMLIO_Write); 
   xmlIOFrom.SetFileIO (&buffFrom,XMLIO_Write);
   WriteFile (&xmlIOThis);
   other->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom))
      ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength (0); 
   buffFrom.SetLength (0);
   return (ret_flag);
}

 int EventRuleConfig_Counting::ReadFile (FileIO &file)
 
{
   if (file.Read ((byte*)&AspectRatio,1,sizeof(float))) return (1);
   if (file.Read ((byte*)&MajorAxis,1,sizeof(ILine2D))) return (2);
   return (DONE);
}

 int EventRuleConfig_Counting::ReadFile (CXMLIO* pIO)

{
   static EventRuleConfig_Counting dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Counting")) {
      xmlNode.Attribute (TYPE_ID(float)  ,"AspectRatio",&AspectRatio,&dv.AspectRatio);
      xmlNode.Attribute (TYPE_ID(ILine2D),"MajorAxis"  ,&MajorAxis  ,&dv.MajorAxis);
      // jun: 분석 설정 창에서 Count 정보가 Read/Write되면 현재 Count값이 이전 값으로 바뀌는 문제가 있습니다. 
      // 이를 해결하기 위해 xmlIO를 통하여 전파된 값이 0x80000000가 아닌 경우에만 카운트 값을 복사하도록 하였습니다. 
      if (!(pIO->m_nCustOpt & 0x80000000)) 
         xmlNode.Attribute (TYPE_ID(int),"Count",&Count,&dv.Count);
      xmlNode.End (   );
      return (DONE);
   }   
   return (1);
}

 int EventRuleConfig_Counting::WriteFile (FileIO &file)
 
{
   if (file.Write ((byte*)&AspectRatio,1,sizeof(float  ))) return (1);
   if (file.Write ((byte*)&MajorAxis  ,1,sizeof(ILine2D))) return (2);
   return (DONE);
}

 int EventRuleConfig_Counting::WriteFile (CXMLIO* pIO)

{
   static EventRuleConfig_Counting dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Counting")) {
      xmlNode.Attribute (TYPE_ID(float)  ,"AspectRatio",&AspectRatio,&dv.AspectRatio);
      xmlNode.Attribute (TYPE_ID(ILine2D),"MajorAxis"  ,&MajorAxis  ,&dv.MajorAxis);
      // jun: 분석 설정 창에서 Count 정보가 Read/Write되면 현재 Count값이 이전 값으로 바뀌는 문제가 있습니다. 
      // 이를 해결하기 위해 xmlIO를 통하여 전파된 값이 0x80000000가 아닌 경우에만 카운트 값을 복사하도록 하였습니다. 
      if(!(pIO->m_nCustOpt & 0x80000000))
         xmlNode.Attribute (TYPE_ID(int),"Count",&Count,&dv.Count);
      xmlNode.End (   );
      return (DONE);
   }   
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_CrowdDensity
//
///////////////////////////////////////////////////////////////////////////////

 EventRuleConfig_CrowdDensity::EventRuleConfig_CrowdDensity (   )

{
   CountThreshold  = 0;
   TransformFactor = 3.0f;
}

 int EventRuleConfig_CrowdDensity::CheckSetup (EventRuleConfig_CrowdDensity* other)

{
   int ret_flag = 0;
   FileIO buffThis,buffFrom;   
   CXMLIO xmlIOThis,xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis,XMLIO_Write);
   xmlIOFrom.SetFileIO (&buffFrom,XMLIO_Write);
   WriteFile (&xmlIOThis);
   other->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom))
      ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength (0);
   buffFrom.SetLength (0);
   return (ret_flag);
}

 int EventRuleConfig_CrowdDensity::ReadFile (FileIO& file)

{
   if (file.Read ((byte*)&CountThreshold ,1,sizeof(int  ))) return (1);
   if (file.Read ((byte*)&TransformFactor,1,sizeof(float))) return (2);
   return (DONE);
}

 int EventRuleConfig_CrowdDensity::ReadFile (CXMLIO* pIO)

{
   static EventRuleConfig_CrowdDensity dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("CrowdCounting")) {
      xmlNode.Attribute (TYPE_ID(int)  ,"CountThreshold" ,&CountThreshold ,&dv.CountThreshold );
      xmlNode.Attribute (TYPE_ID(float),"TransformFactor",&TransformFactor,&dv.TransformFactor);
      xmlNode.End (   );
      return (DONE);
   }   
   return (1);
}

 int EventRuleConfig_CrowdDensity::WriteFile (FileIO& file)

{
   if (file.Write ((byte*)&CountThreshold ,1,sizeof(int  ))) return (1);
   if (file.Write ((byte*)&TransformFactor,1,sizeof(float))) return (2);
   return (DONE);
}

 int EventRuleConfig_CrowdDensity::WriteFile (CXMLIO* pIO)

{
   static EventRuleConfig_CrowdDensity dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("CrowdCounting")) {
      xmlNode.Attribute (TYPE_ID(int)  ,"CountThreshold" ,&CountThreshold ,&dv.CountThreshold );
      xmlNode.Attribute (TYPE_ID(float),"TransformFactor",&TransformFactor,&dv.TransformFactor);
      xmlNode.End (   );
      return (DONE);
   }   
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_DirectionalMotion
//
///////////////////////////////////////////////////////////////////////////////

 EventRuleConfig_DirectionalMotion::EventRuleConfig_DirectionalMotion (   )
 
{
   DetectionTime     = 1.5f;
   Direction         = 0.0f;
   Range             = 120.0f;
   MinMovingDistance = 200.0f;
}

 int EventRuleConfig_DirectionalMotion::CheckSetup (EventRuleConfig_DirectionalMotion* other)

{
   int ret_flag = 0;
   FileIO buffThis,buffFrom;   
   CXMLIO xmlIOThis,xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis,XMLIO_Write); 
   xmlIOFrom.SetFileIO (&buffFrom,XMLIO_Write);
   WriteFile (&xmlIOThis);
   other->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom))
      ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength (0); 
   buffFrom.SetLength (0);
   return (ret_flag);
}
 
 int EventRuleConfig_DirectionalMotion::ReadFile (FileIO &file)
 
{
   if (file.Read ((byte*)&DetectionTime    ,1,sizeof(float))) return (1);
   if (file.Read ((byte*)&Direction        ,1,sizeof(float))) return (2);
   if (file.Read ((byte*)&Range            ,1,sizeof(float))) return (3);
   if (file.Read ((byte*)&MinMovingDistance,1,sizeof(float))) return (4);
   return (DONE);
}

 int EventRuleConfig_DirectionalMotion::ReadFile (CXMLIO* pIO)

{
   static EventRuleConfig_DirectionalMotion dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("DirectionalMotion")) {
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime"    ,&DetectionTime    ,&dv.DetectionTime);
      xmlNode.Attribute (TYPE_ID(float),"Direction"        ,&Direction        ,&dv.Direction);
      xmlNode.Attribute (TYPE_ID(float),"Range"            ,&Range            ,&dv.Range);
      xmlNode.Attribute (TYPE_ID(float),"MinMovingDistance",&MinMovingDistance,&dv.MinMovingDistance);
      xmlNode.End (   );
      return (DONE);
   }   
   return (1);
}

 int EventRuleConfig_DirectionalMotion::WriteFile (FileIO &file)
 
{
   if (file.Write ((byte*)&DetectionTime    ,1,sizeof(float))) return (1);
   if (file.Write ((byte*)&Direction        ,1,sizeof(float))) return (2);
   if (file.Write ((byte*)&Range            ,1,sizeof(float))) return (3);
   if (file.Write ((byte*)&MinMovingDistance,1,sizeof(float))) return (4);
   return (DONE);
}

 int EventRuleConfig_DirectionalMotion::WriteFile (CXMLIO* pIO)

{
   static EventRuleConfig_DirectionalMotion dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("DirectionalMotion")) {
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime"    ,&DetectionTime    ,&dv.DetectionTime);
      xmlNode.Attribute (TYPE_ID(float),"Direction"        ,&Direction        ,&dv.Direction);
      xmlNode.Attribute (TYPE_ID(float),"Range"            ,&Range            ,&dv.Range);
      xmlNode.Attribute (TYPE_ID(float),"MinMovingDistance",&MinMovingDistance,&dv.MinMovingDistance);
      xmlNode.End (   );
      return (DONE);
   }   
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_Dwell
//
///////////////////////////////////////////////////////////////////////////////

 EventRuleConfig_Dwell::EventRuleConfig_Dwell (   )

{
   MaxZoneCapacity = 1000;
   DetectionTime   = 0.0f;
   MaxWaitingTime  = 1000.0f;
}

 int EventRuleConfig_Dwell::CheckSetup (EventRuleConfig_Dwell* other)

{
   int ret_flag = 0;
   FileIO buffThis,buffFrom;
   CXMLIO xmlIOThis,xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis,XMLIO_Write); 
   xmlIOFrom.SetFileIO (&buffFrom,XMLIO_Write);
   WriteFile (&xmlIOThis);
   other->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom))
      ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength (0); 
   buffFrom.SetLength (0);
   return (ret_flag);
}
 
 int EventRuleConfig_Dwell::ReadFile (FileIO &file)
 
{
   if (file.Read ((byte*)&MaxZoneCapacity,1,sizeof(int)  )) return (1);
   if (file.Read ((byte*)&DetectionTime  ,1,sizeof(float))) return (2);
   if (file.Read ((byte*)&MaxWaitingTime ,1,sizeof(float))) return (3);
   return (DONE);
}

 int EventRuleConfig_Dwell::ReadFile (CXMLIO* pIO)

{
   static EventRuleConfig_Dwell dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Dwell")) {
      xmlNode.Attribute (TYPE_ID(int)  ,"MaxZoneCapacity",&MaxZoneCapacity,&dv.MaxZoneCapacity);
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime"  ,&DetectionTime  ,&dv.DetectionTime);
      xmlNode.Attribute (TYPE_ID(float),"MaxWaitingTime" ,&MaxWaitingTime ,&dv.MaxWaitingTime);
      xmlNode.End (   );
   }   
   return (DONE);
}

 int EventRuleConfig_Dwell::WriteFile (FileIO &file)
 
{
   if (file.Write ((byte*)&MaxZoneCapacity,1,sizeof(int)  )) return (1);
   if (file.Write ((byte*)&DetectionTime  ,1,sizeof(float))) return (2);
   if (file.Write ((byte*)&MaxWaitingTime ,1,sizeof(float))) return (3);
   return (DONE);
}

 int EventRuleConfig_Dwell::WriteFile (CXMLIO* pIO)

{
   static EventRuleConfig_Dwell dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Dwell")) {
      xmlNode.Attribute (TYPE_ID(int)  ,"MaxZoneCapacity",&MaxZoneCapacity,&dv.MaxZoneCapacity);
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime"  ,&DetectionTime  ,&dv.DetectionTime);
      xmlNode.Attribute (TYPE_ID(float),"MaxWaitingTime" ,&MaxWaitingTime ,&dv.MaxWaitingTime);
      xmlNode.End (   );
   }   
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_Falling
//
///////////////////////////////////////////////////////////////////////////////

 EventRuleConfig_Falling::EventRuleConfig_Falling (   )

{
   DetectionTime = 1.5f;
}

 int EventRuleConfig_Falling::CheckSetup (EventRuleConfig_Falling* other)

{
   int ret_flag = 0;
   FileIO buffThis,buffFrom;   
   CXMLIO xmlIOThis,xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis,XMLIO_Write); 
   xmlIOFrom.SetFileIO (&buffFrom,XMLIO_Write);
   WriteFile (&xmlIOThis);
   other->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom))
      ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength (0); 
   buffFrom.SetLength (0);
   return (ret_flag);
}

 int EventRuleConfig_Falling::ReadFile (FileIO &file)
 
{
   if (file.Read ((byte*)&DetectionTime,1,sizeof(float))) return (1);
   return (DONE);
}

 int EventRuleConfig_Falling::ReadFile (CXMLIO* pIO)

{
   static EventRuleConfig_Falling dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Falling")) {
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime",&DetectionTime,&dv.DetectionTime);
      xmlNode.End (   );
   }   
   return (DONE);
}

 int EventRuleConfig_Falling::WriteFile (FileIO &file)
 
{
   if (file.Write ((byte*)&DetectionTime,1,sizeof(float))) return (1);
   return (DONE);
}

 int EventRuleConfig_Falling::WriteFile (CXMLIO* pIO)

{
   static EventRuleConfig_Falling dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Falling")) {
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime",&DetectionTime,&dv.DetectionTime);
      xmlNode.End (   );
   }   
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_Flame
//
///////////////////////////////////////////////////////////////////////////////

 EventRuleConfig_Flame::EventRuleConfig_Flame (   )
 
{
   DetectionTime = 5.0f;
}

 int EventRuleConfig_Flame::CheckSetup (EventRuleConfig_Flame* other)

{
   int ret_flag = 0;
   FileIO buffThis,buffFrom;   
   CXMLIO xmlIOThis,xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis,XMLIO_Write); 
   xmlIOFrom.SetFileIO (&buffFrom,XMLIO_Write);
   WriteFile (&xmlIOThis);
   other->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom))
      ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength (0); 
   buffFrom.SetLength (0);
   return (ret_flag);
}

 int EventRuleConfig_Flame::ReadFile (FileIO &file)
 
{
   if (file.Read ((byte*)&DetectionTime,1,sizeof(float))) return (1);
   return (DONE);
}

 int EventRuleConfig_Flame::ReadFile (CXMLIO* pIO)

{
   static EventRuleConfig_Flame dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Flame")) {
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime",&DetectionTime,&dv.DetectionTime);
      xmlNode.End (   );
   }   
   return (DONE);
}

 int EventRuleConfig_Flame::WriteFile (FileIO &file)
 
{
   if (file.Write ((byte*)&DetectionTime,1,sizeof(float))) return (1);
   return (DONE);
}

 int EventRuleConfig_Flame::WriteFile (CXMLIO* pIO)

{
   static EventRuleConfig_Flame dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Flame")) {
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime",&DetectionTime,&dv.DetectionTime);
      xmlNode.End (   );
   }   
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_Gathering
//
///////////////////////////////////////////////////////////////////////////////

 EventRuleConfig_Gathering::EventRuleConfig_Gathering (   )
 
{
   MinNumPersons = 2;
   DetectionTime = 5.0f;
}

 int EventRuleConfig_Gathering::CheckSetup (EventRuleConfig_Gathering* other)

{
   int ret_flag = 0;
   FileIO buffThis,buffFrom;   
   CXMLIO xmlIOThis,xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis,XMLIO_Write); 
   xmlIOFrom.SetFileIO (&buffFrom,XMLIO_Write);
   WriteFile (&xmlIOThis);
   other->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom))
      ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength (0); 
   buffFrom.SetLength (0);
   return (ret_flag);
}

 int EventRuleConfig_Gathering::ReadFile (FileIO &file)
 
{
   if (file.Read ((byte*)&MinNumPersons,1,sizeof(int  ))) return (1);
   if (file.Read ((byte*)&DetectionTime,1,sizeof(float))) return (2);
   return (DONE);
}

 int EventRuleConfig_Gathering::ReadFile (CXMLIO* pIO)

{
   static EventRuleConfig_Gathering dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Gathering")) {
      xmlNode.Attribute (TYPE_ID(int)  ,"MinNumPersons",&MinNumPersons,&dv.MinNumPersons);
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime",&DetectionTime,&dv.DetectionTime);
      xmlNode.End (   );
      return (DONE);
   }
   else if (xmlNode.Start ("Grouping")) { // 이전 버전 호환 용
      xmlNode.Attribute (TYPE_ID(int)  ,"MinNumPersons",&MinNumPersons,&dv.MinNumPersons);
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime",&DetectionTime,&dv.DetectionTime);
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}

 int EventRuleConfig_Gathering::WriteFile (FileIO &file)
 
{
   if (file.Write ((byte*)&MinNumPersons,1,sizeof(int  ))) return (1);
   if (file.Write ((byte*)&DetectionTime,1,sizeof(float))) return (2);
   return (DONE);
}

 int EventRuleConfig_Gathering::WriteFile (CXMLIO* pIO)

{
   static EventRuleConfig_Gathering dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Gathering")) {
      xmlNode.Attribute (TYPE_ID(int)  ,"MinNumPersons",&MinNumPersons,&dv.MinNumPersons);
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime",&DetectionTime,&dv.DetectionTime);
      xmlNode.End (   );
      return (DONE);
   }   
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_LineCrossing
//
///////////////////////////////////////////////////////////////////////////////

 EventRuleConfig_LineCrossing::EventRuleConfig_LineCrossing (   ) 
 
{
   Direction = 0;
   CrossingLine[0](-1,-1);
   CrossingLine[1](-1,-1);
}

 int EventRuleConfig_LineCrossing::CheckSetup (EventRuleConfig_LineCrossing* other)

{
   int ret_flag = 0;
   FileIO buffThis,buffFrom;   
   CXMLIO xmlIOThis,xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis,XMLIO_Write); 
   xmlIOFrom.SetFileIO (&buffFrom,XMLIO_Write);
   WriteFile (&xmlIOThis);
   other->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom))
      ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength (0); 
   buffFrom.SetLength (0);
   return (ret_flag);
}

 int EventRuleConfig_LineCrossing::ReadFile (FileIO &file)
 
{
   if (file.Read ((byte*)&Direction   ,1,sizeof(int    ))) return (1);
   if (file.Read ((byte*)&CrossingLine,1,sizeof(ILine2D))) return (2);
   return (DONE);
}

 int EventRuleConfig_LineCrossing::ReadFile (CXMLIO* pIO)

{
   static EventRuleConfig_LineCrossing dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("LineCrossing")) {
      xmlNode.Attribute (TYPE_ID(int)    ,"Direction"   ,&Direction   ,&dv.Direction);
      xmlNode.Attribute (TYPE_ID(ILine2D),"CrossingLine",&CrossingLine,&dv.CrossingLine);
      xmlNode.End (   );
      return (DONE);
   }   
   return (1);
}

 int EventRuleConfig_LineCrossing::WriteFile (FileIO &file)
 
{
   if (file.Write ((byte*)&Direction   ,1,sizeof(int    ))) return (1);
   if (file.Write ((byte*)&CrossingLine,1,sizeof(ILine2D))) return (2);
   return (DONE);
}

 int EventRuleConfig_LineCrossing::WriteFile (CXMLIO* pIO)

{
   static EventRuleConfig_LineCrossing dv;
 
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("LineCrossing")) {
      xmlNode.Attribute (TYPE_ID(int)    ,"Direction"   ,&Direction   ,&dv.Direction);
      xmlNode.Attribute (TYPE_ID(ILine2D),"CrossingLine",&CrossingLine,&dv.CrossingLine);
      xmlNode.End ();
      return (DONE);
   }   
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_Loitering
//
///////////////////////////////////////////////////////////////////////////////

 EventRuleConfig_Loitering::EventRuleConfig_Loitering (   )

{
   DetectionTime = 0.0f;
   #if defined(__CPNI_CERT_SZM)
   DetectionTime = 2.0f;
   #endif
}

 int EventRuleConfig_Loitering::CheckSetup (EventRuleConfig_Loitering* other)

{
   int ret_flag = 0;
   FileIO buffThis,buffFrom;   
   CXMLIO xmlIOThis,xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis,XMLIO_Write); 
   xmlIOFrom.SetFileIO (&buffFrom,XMLIO_Write);
   WriteFile (&xmlIOThis);
   other->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom))
      ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength (0); 
   buffFrom.SetLength (0);
   return (ret_flag);
}

 int EventRuleConfig_Loitering::ReadFile (FileIO &file)
 
{
   if (file.Read ((byte*)&DetectionTime,1,sizeof(float))) return (1);
   return (DONE);
}

 int EventRuleConfig_Loitering::ReadFile (CXMLIO* pIO)

{
   static EventRuleConfig_Loitering dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Loitering")) {
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime",&DetectionTime,&dv.DetectionTime);
      xmlNode.End (   );
   }   
   return (DONE);
}

 int EventRuleConfig_Loitering::WriteFile (FileIO &file)
 
{
   if (file.Write ((byte*)&DetectionTime,1,sizeof(float))) return (1);
   return (DONE);
}

 int EventRuleConfig_Loitering::WriteFile (CXMLIO* pIO)

{
   static EventRuleConfig_Loitering dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Loitering")) {
      xmlNode.Attribute (TYPE_ID (float),"DetectionTime",&DetectionTime,&dv.DetectionTime);
      xmlNode.End ();
   }   
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_PathPassing
//
///////////////////////////////////////////////////////////////////////////////

 EventRuleConfig_PathPassing::EventRuleConfig_PathPassing (   )
 
{
   Direction     = 0;
   DetectionTime = 0.0f;
   PassingTime   = 120.0f;
}

 int EventRuleConfig_PathPassing::CheckSetup (EventRuleConfig_PathPassing* ers_pp)

{
   int ret_flag = 0;
   FileIO buffThis,buffFrom;   
   CXMLIO xmlIOThis,xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis,XMLIO_Write);
   xmlIOFrom.SetFileIO (&buffFrom,XMLIO_Write);
   WriteFile (&xmlIOThis);
   ers_pp->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom))
      ret_flag |= CHECK_SETUP_RESULT_CHANGED | CHECK_SETUP_RESULT_RESTART;
   buffThis.SetLength (0);
   buffFrom.SetLength (0);
   return (ret_flag);
}

 void EventRuleConfig_PathPassing::Initialize (EventZone *evt_zone)
 
{
   Arrows.Delete (   );
   ZoneTree.MergeZones (   );
   ZoneTree.Vertices = evt_zone->Vertices;
   ZoneTree.UpdateCenterPosition (   );
}

 int EventRuleConfig_PathPassing::ReadFile (FileIO &file)

{
   if (file.Read ((byte*)&Direction    ,1,sizeof(int  ))) return (1);
   if (file.Read ((byte*)&DetectionTime,1,sizeof(float))) return (2);
   if (file.Read ((byte*)&PassingTime  ,1,sizeof(float))) return (3);
   if (ZoneTree.ReadFile (file)) return (4);
   if (Arrows.ReadFile   (file)) return (5);
   return (DONE);
}

 int EventRuleConfig_PathPassing::ReadFile (CXMLIO* pIO)

{
   static EventRuleConfig_PathPassing dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PathPassing")) {
      xmlNode.Attribute (TYPE_ID(int)  ,"Direction"    ,&Direction    ,&dv.Direction);
      xmlNode.Attribute (TYPE_ID(float),"PassingTime"  ,&PassingTime  ,&dv.PassingTime);
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime",&DetectionTime,&dv.DetectionTime);
      ZoneTree.ReadFile (pIO);
      Arrows.ReadFile (pIO);
      xmlNode.End (   );
      return (DONE);
   }   
   return (1);
}

 void EventRuleConfig_PathPassing::UpdatePathZonePtrs (   )

{
   PathZonePtrs.Delete (   );
   Arrow *arrow = Arrows.First (   );
   if (arrow) {
      PathZone *pth_zone = ZoneTree.FindPathZone ((*arrow)[0]);
      if (pth_zone) {
         PathZonePtr *pz_ptr = new PathZonePtr (pth_zone);
         PathZonePtrs.Add (pz_ptr);
      }
   }
   while (arrow) {
      PathZone *pth_zone = ZoneTree.FindPathZone ((*arrow)[1]);
      if (pth_zone) {
         PathZonePtr *pz_ptr = new PathZonePtr (pth_zone);
         PathZonePtrs.Add (pz_ptr);
      }
      arrow = Arrows.Next (arrow);
   }
}

 int EventRuleConfig_PathPassing::WriteFile (FileIO &file)

{
   if (file.Write ((byte*)&Direction    ,1,sizeof(int  ))) return (1);
   if (file.Write ((byte*)&DetectionTime,1,sizeof(float))) return (2);
   if (file.Write ((byte*)&PassingTime  ,1,sizeof(float))) return (3);
   if (ZoneTree.WriteFile (file)) return (4);
   if (Arrows.WriteFile (file))   return (5);
   return (DONE);
}

 int EventRuleConfig_PathPassing::WriteFile (CXMLIO* pIO)

{
   static EventRuleConfig_PathPassing dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PathPassing")) {
      xmlNode.Attribute (TYPE_ID(int)  ,"Direction"    ,&Direction    ,&dv.Direction);
      xmlNode.Attribute (TYPE_ID(float),"PassingTime"  ,&PassingTime  ,&dv.PassingTime);
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime",&DetectionTime,&dv.DetectionTime);
      ZoneTree.WriteFile (pIO);
      Arrows.WriteFile (pIO);
      xmlNode.End (   );
      return (DONE);
   }   
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_Removed
//
///////////////////////////////////////////////////////////////////////////////

 EventRuleConfig_Removed::EventRuleConfig_Removed (   )

{
   DetectionTime      = 5.0f;
   MaxInitMovTime     = 7.0f;
   MaxInitMovDistance = 5.0f;
}

 int EventRuleConfig_Removed::CheckSetup (EventRuleConfig_Removed* other)

{
   int ret_flag = 0;
   FileIO buffThis,buffFrom;   
   CXMLIO xmlIOThis,xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis,XMLIO_Write);
   xmlIOFrom.SetFileIO (&buffFrom,XMLIO_Write);
   WriteFile (&xmlIOThis);
   other->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom))
      ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength (0);
   buffFrom.SetLength (0);
   return (ret_flag);
}

 int EventRuleConfig_Removed::ReadFile (FileIO &file)
 
{
   if (file.Read ((byte*)&DetectionTime ,1,sizeof(float))) return (1);
   if (file.Read ((byte*)&MaxInitMovTime,1,sizeof(float))) return (2);
   return (DONE);
}

 int EventRuleConfig_Removed::ReadFile (CXMLIO* pIO)

{
   static EventRuleConfig_Removed dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Removed")) {
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime" ,&DetectionTime ,&dv.DetectionTime);
      xmlNode.Attribute (TYPE_ID(float),"MaxInitMovTime",&MaxInitMovTime,&dv.MaxInitMovTime);
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}

 int EventRuleConfig_Removed::WriteFile (FileIO &file)
 
{
   if (file.Write ((byte*)&DetectionTime ,1,sizeof(float))) return (1);
   if (file.Write ((byte*)&MaxInitMovTime,1,sizeof(float))) return (2);
   return (DONE);
}

 int EventRuleConfig_Removed::WriteFile (CXMLIO* pIO)

{
   static EventRuleConfig_Removed dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Removed")) {
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime" ,&DetectionTime,&dv.DetectionTime);
      xmlNode.Attribute (TYPE_ID(float),"MaxInitMovTime",&MaxInitMovTime,&dv.MaxInitMovTime);
      xmlNode.End (   );
      return (DONE);
   }   
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_Smoke
//
///////////////////////////////////////////////////////////////////////////////

 EventRuleConfig_Smoke::EventRuleConfig_Smoke (   )
 
{
   DetectionTime = 7.0f;
}

 int EventRuleConfig_Smoke::CheckSetup (EventRuleConfig_Smoke* other)

{
   int ret_flag = 0;
   FileIO buffThis,buffFrom;   
   CXMLIO xmlIOThis,xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis,XMLIO_Write);
   xmlIOFrom.SetFileIO (&buffFrom,XMLIO_Write);
   WriteFile (&xmlIOThis);
   other->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom))
      ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength (0);
   buffFrom.SetLength (0);
   return (ret_flag);
}

 int EventRuleConfig_Smoke::ReadFile (FileIO &file)
 
{
   if (file.Read ((byte*)&DetectionTime,1,sizeof(float))) return (1);
   return (DONE);
}

 int EventRuleConfig_Smoke::ReadFile (CXMLIO* pIO)

{
   static EventRuleConfig_Smoke dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Smoke")) {
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime",&DetectionTime,&dv.DetectionTime);
      xmlNode.End (   );
   }   
   return (DONE);
}

 int EventRuleConfig_Smoke::WriteFile (FileIO &file)
 
{
   if (file.Write ((byte*)&DetectionTime,1,sizeof(float))) return (1);
   return (DONE);
}

 int EventRuleConfig_Smoke::WriteFile (CXMLIO* pIO)

{
   static EventRuleConfig_Smoke dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Smoke")) {
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime",&DetectionTime,&dv.DetectionTime);
      xmlNode.End (   );
   }   
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_Stopping
//
///////////////////////////////////////////////////////////////////////////////

 EventRuleConfig_Stopping::EventRuleConfig_Stopping (   )

{
   DetectionTime = 5.0f;
}

 int EventRuleConfig_Stopping::CheckSetup (EventRuleConfig_Stopping* other)

{
   int ret_flag = 0;
   FileIO buffThis,buffFrom;   
   CXMLIO xmlIOThis,xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis,XMLIO_Write);
   xmlIOFrom.SetFileIO (&buffFrom,XMLIO_Write);
   WriteFile (&xmlIOThis);
   other->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom))
      ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength (0);
   buffFrom.SetLength (0);
   return (ret_flag);
}

 int EventRuleConfig_Stopping::ReadFile (FileIO &file)
 
{
   if (file.Read ((byte*)&DetectionTime,1,sizeof(float))) return (1);
   return (DONE);
}

 int EventRuleConfig_Stopping::ReadFile (CXMLIO* pIO)

{
   static EventRuleConfig_Stopping dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Stopping")) {
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime",&DetectionTime,&dv.DetectionTime);
      xmlNode.End (   );
   }   
   return (DONE);
}

 int EventRuleConfig_Stopping::WriteFile (FileIO &file)
 
{
   if (file.Write ((byte*)&DetectionTime,1,sizeof(float))) return (1);
   return (DONE);
}

 int EventRuleConfig_Stopping::WriteFile (CXMLIO* pIO)

{
   static EventRuleConfig_Stopping dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Stopping")) {
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime",&DetectionTime,&dv.DetectionTime);
      xmlNode.End (   );
   }   
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_TrafficCongestion
//
///////////////////////////////////////////////////////////////////////////////

 EventRuleConfig_TrafficCongestion::EventRuleConfig_TrafficCongestion (   )
 
{
   DetectionTime   = 15.0f;
   MaxAvgRealSpeed = 40.0f;
}

 int EventRuleConfig_TrafficCongestion::CheckSetup (EventRuleConfig_TrafficCongestion* other)

{
   int ret_flag = 0;
   FileIO buffThis,buffFrom;   
   CXMLIO xmlIOThis,xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis,XMLIO_Write);
   xmlIOFrom.SetFileIO (&buffFrom,XMLIO_Write);
   WriteFile (&xmlIOThis);
   other->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom))
      ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength (0);
   buffFrom.SetLength (0);
   return (ret_flag);
}

 int EventRuleConfig_TrafficCongestion::ReadFile (FileIO &file)
 
{
   if (file.Read ((byte*)&DetectionTime  ,1,sizeof(float))) return (1);
   if (file.Read ((byte*)&MaxAvgRealSpeed,1,sizeof(float))) return (2);
   return (DONE);
}

 int EventRuleConfig_TrafficCongestion::ReadFile (CXMLIO* pIO)

{
   static EventRuleConfig_TrafficCongestion dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("TrafficCongestion")) {
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime"  ,&DetectionTime  ,&dv.DetectionTime);
      xmlNode.Attribute (TYPE_ID(float),"MaxAvgRealSpeed",&MaxAvgRealSpeed,&dv.MaxAvgRealSpeed);
      xmlNode.End ();
   }   
   return (DONE);
}

 int EventRuleConfig_TrafficCongestion::WriteFile (FileIO &file)
 
{
   if (file.Write ((byte*)&DetectionTime  ,1,sizeof(float))) return (1);
   if (file.Write ((byte*)&MaxAvgRealSpeed,1,sizeof(float))) return (2);
   return (DONE);
}

 int EventRuleConfig_TrafficCongestion::WriteFile (CXMLIO* pIO)

{
   static EventRuleConfig_TrafficCongestion dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("TrafficCongestion")) {
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime"  ,&DetectionTime  ,&dv.DetectionTime);
      xmlNode.Attribute (TYPE_ID(float),"MaxAvgRealSpeed",&MaxAvgRealSpeed,&dv.MaxAvgRealSpeed);
      xmlNode.End (   );
   }   
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_Violence
//
///////////////////////////////////////////////////////////////////////////////

 EventRuleConfig_Violence::EventRuleConfig_Violence (   )
 
{
   DetectionTime = 2.0f;
}

 int EventRuleConfig_Violence::CheckSetup (EventRuleConfig_Violence* other)

{
   int ret_flag = 0;
   FileIO buffThis,buffFrom;   
   CXMLIO xmlIOThis,xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis,XMLIO_Write);
   xmlIOFrom.SetFileIO (&buffFrom,XMLIO_Write);
   WriteFile (&xmlIOThis);
   other->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom))
      ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength (0);
   buffFrom.SetLength (0);
   return (ret_flag);
}

 int EventRuleConfig_Violence::ReadFile (FileIO &file)
 
{
   if (file.Read ((byte*)&DetectionTime,1,sizeof(float))) return (1);
   return (DONE);
}

 int EventRuleConfig_Violence::ReadFile (CXMLIO* pIO)

{
   static EventRuleConfig_Violence dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Violence")) {
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime",&DetectionTime,&dv.DetectionTime);
      xmlNode.End (   );
   }   
   return (DONE);
}

 int EventRuleConfig_Violence::WriteFile (FileIO &file)
 
{
   if (file.Write ((byte*)&DetectionTime,1,sizeof(float))) return (1);
   return (DONE);
}

 int EventRuleConfig_Violence::WriteFile (CXMLIO* pIO)

{
   static EventRuleConfig_Violence dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Violence")) {
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime",&DetectionTime,&dv.DetectionTime);
      xmlNode.End (   );
   }   
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_WaterLevel
//
///////////////////////////////////////////////////////////////////////////////

 EventRuleConfig_WaterLevel::EventRuleConfig_WaterLevel (   )

{
   Direction     = 0;
   DetectionTime = 2.0f;
   Threshold     = 1.0f;
   CrossingLine[0](-1,-1);
   CrossingLine[1](-1,-1);
}

 int EventRuleConfig_WaterLevel::CheckSetup (EventRuleConfig_WaterLevel* other)

{
   int ret_flag = 0;
   FileIO buffThis,buffFrom;   
   CXMLIO xmlIOThis,xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis,XMLIO_Write);
   xmlIOFrom.SetFileIO (&buffFrom,XMLIO_Write);
   WriteFile (&xmlIOThis);
   other->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom))
      ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength (0);
   buffFrom.SetLength (0);
   return (ret_flag);
}
 
 int EventRuleConfig_WaterLevel::ReadFile (FileIO& file)

{
   if (file.Read ((byte*)&Direction    ,1,sizeof(int    ))) return (1);
   if (file.Read ((byte*)&DetectionTime,1,sizeof(float  ))) return (2);
   if (file.Read ((byte*)&Threshold    ,1,sizeof(float  ))) return (3);
   if (file.Read ((byte*)&CrossingLine ,1,sizeof(ILine2D))) return (4);
   return (DONE);
}

 int EventRuleConfig_WaterLevel::ReadFile (CXMLIO* pIO)

{
   static EventRuleConfig_WaterLevel dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("WaterLevel")) {
      xmlNode.Attribute (TYPE_ID(int)    ,"Direction"    ,&Direction    ,&dv.Direction    );
      xmlNode.Attribute (TYPE_ID(float)  ,"DetectionTime",&DetectionTime,&dv.DetectionTime);
      xmlNode.Attribute (TYPE_ID(float)  ,"Threshold"    ,&Threshold    ,&dv.Threshold    );
      xmlNode.Attribute (TYPE_ID(ILine2D),"CrossingLine" ,&CrossingLine ,&dv.CrossingLine );
      xmlNode.End (   );
      return (DONE);
   }   
   return (1);
}

 int EventRuleConfig_WaterLevel::WriteFile (FileIO& file)

{
   if (file.Write ((byte*)&Direction    ,1,sizeof(int    ))) return (1);
   if (file.Write ((byte*)&DetectionTime,1,sizeof(float  ))) return (2);
   if (file.Write ((byte*)&Threshold    ,1,sizeof(float  ))) return (3);
   if (file.Write ((byte*)&CrossingLine ,1,sizeof(ILine2D))) return (4);
   return (DONE);
}

 int EventRuleConfig_WaterLevel::WriteFile (CXMLIO* pIO)

{
   static EventRuleConfig_WaterLevel dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("WaterLevel")) {
      xmlNode.Attribute (TYPE_ID(int)    ,"Direction"    ,&Direction    ,&dv.Direction    );
      xmlNode.Attribute (TYPE_ID(float)  ,"DetectionTime",&DetectionTime,&dv.DetectionTime);
      xmlNode.Attribute (TYPE_ID(float)  ,"Threshold"    ,&Threshold    ,&dv.Threshold    );
      xmlNode.Attribute (TYPE_ID(ILine2D),"CrossingLine" ,&CrossingLine ,&dv.CrossingLine );
      xmlNode.End (   );
      return (DONE);
   }   
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_ZoneColor
//
///////////////////////////////////////////////////////////////////////////////

 EventRuleConfig_ZoneColor::EventRuleConfig_ZoneColor (   )
 
{
   DetectionTime = 0.0f;
   LastingTime   = 0.0f;
   ResetCDS (   );
   TgtColor(0,0,255);
}

 int EventRuleConfig_ZoneColor::CheckSetup (EventRuleConfig_ZoneColor* other)

{
   int ret_flag = 0;
   FileIO buffThis,buffFrom;   
   CXMLIO xmlIOThis,xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis,XMLIO_Write);
   xmlIOFrom.SetFileIO (&buffFrom,XMLIO_Write);
   WriteFile (&xmlIOThis);
   other->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom))
      ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength (0);
   buffFrom.SetLength (0);
   return (ret_flag);
}

 int EventRuleConfig_ZoneColor::ReadFile (FileIO& file)
 
{
   if (file.Read ((byte*)&DetectionTime,1,sizeof(float)   )) return (1);
   if (file.Read ((byte*)&LastingTime  ,1,sizeof(float)   )) return (2);
   if (file.Read ((byte*)&CDS          ,1,sizeof(int)     )) return (3);
   if (file.Read ((byte*)&TgtColor     ,1,sizeof(BGRColor))) return (4);
   return (DONE);
}

 int EventRuleConfig_ZoneColor::ReadFile (CXMLIO* pIO)
 
{
   static EventRuleConfig_ZoneColor dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("ZoneColor")) {
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime",&DetectionTime,&dv.DetectionTime);
      xmlNode.Attribute (TYPE_ID(float),"LastingTime"  ,&LastingTime  ,&dv.LastingTime  );
      xmlNode.Attribute (TYPE_ID(int)  ,"CDS"          ,&CDS          ,&dv.CDS          );
      xmlNode.Attribute (TYPE_ID(byte) ,"TgtColor_B"   ,&TgtColor.B   ,&dv.TgtColor.B   );
      xmlNode.Attribute (TYPE_ID(byte) ,"TgtColor_G"   ,&TgtColor.G   ,&dv.TgtColor.G   );
      xmlNode.Attribute (TYPE_ID(byte) ,"TgtColor_R"   ,&TgtColor.R   ,&dv.TgtColor.R   );
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}

 void EventRuleConfig_ZoneColor::ResetCDS (   )

{
   CDS = 70;
}

 int EventRuleConfig_ZoneColor::WriteFile (FileIO& file)
 
{
   if (file.Write ((byte*)&DetectionTime,1,sizeof(float)   )) return (1);
   if (file.Write ((byte*)&LastingTime  ,1,sizeof(float)   )) return (2);
   if (file.Write ((byte*)&CDS          ,1,sizeof(int)     )) return (3);
   if (file.Write ((byte*)&TgtColor     ,1,sizeof(BGRColor))) return (4);
   return (DONE);
}

 int EventRuleConfig_ZoneColor::WriteFile (CXMLIO* pIO)
 
{
   static EventRuleConfig_ZoneColor dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("ZoneColor")) {
      xmlNode.Attribute (TYPE_ID(float),"DetectionTime",&DetectionTime,&dv.DetectionTime);
      xmlNode.Attribute (TYPE_ID(float),"LastingTime"  ,&LastingTime  ,&dv.LastingTime  );
      xmlNode.Attribute (TYPE_ID(int)  ,"CDS"          ,&CDS          ,&dv.CDS          );
      xmlNode.Attribute (TYPE_ID(byte) ,"TgtColor_B"   ,&TgtColor.B   ,&dv.TgtColor.B   );
      xmlNode.Attribute (TYPE_ID(byte) ,"TgtColor_G"   ,&TgtColor.G   ,&dv.TgtColor.G   );
      xmlNode.Attribute (TYPE_ID(byte) ,"TgtColor_R"   ,&TgtColor.R   ,&dv.TgtColor.R   );
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleList
//
///////////////////////////////////////////////////////////////////////////////

 void EventRuleList::CheckDisabledEvents (   )
 
{
   EventRule *evt_rule = First (   );
   while (evt_rule != NULL) {
      if (evt_rule->Status & ER_STATUS_ENABLED) {
         if (__DisabledEventTypes & (0x00000001 << evt_rule->EventType))
            evt_rule->Status &= ~ER_STATUS_ENABLED;
      }
      evt_rule = Next (evt_rule);
   }
}

 void EventRuleList::CheckSchedule (int flag_check)

{
   Time c_time;
   c_time.GetLocalTime (   );
   EventRule *evt_rule = First (   );
   while (evt_rule != NULL) {
      if (flag_check) {
         if (evt_rule->CheckSchedule (c_time)) evt_rule->Status |= ER_STATUS_ENABLED;
         else evt_rule->Status &= ~ER_STATUS_ENABLED;
      }
      else evt_rule->Status |= ER_STATUS_ENABLED;
      evt_rule = Next (evt_rule);
   }
}

 int EventRuleList::ReadFile (FileIO &file)
 
{
   int i,n_items;
   
   if (file.Read ((byte *)&n_items,1,sizeof(int))) return (1);
   for (i = 0; i < n_items; i++) {
      EventRule *evt_rule;
      if (n_items != GetNumItems (   )) {
         Delete (   );
         for (i = 0; i < n_items; i++) {
            evt_rule = CreateInstance_EventRule (   );
            if (DONE == evt_rule->ReadFile (file)) Add (evt_rule);
            else delete evt_rule;
         }
      }
      else {
         evt_rule = First (   );
         while (evt_rule) {
            if (evt_rule->ReadFile (file)) return (2);
            evt_rule = Next (evt_rule);
         }
      }
   }
   return (DONE);
}

 int EventRuleList::ReadFile (CXMLIO* pIO)

{
   int i,n_items;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("EventRuleList"))
   {
      xmlNode.Attribute (TYPE_ID(int),"ItemNum",&n_items);
      EventRule *evt_rule;
      if (n_items != GetNumItems (   )) {
         Delete (   );
         for (i = 0; i < n_items; i++) {
            evt_rule = CreateInstance_EventRule (   );
            if (DONE == evt_rule->ReadFile (pIO)) Add (evt_rule);
            else delete evt_rule;
         }
      }
      else {
         evt_rule = First (   );
         while (evt_rule) {
            if (evt_rule->ReadFile (pIO)) return (2);
            evt_rule = Next (evt_rule);
         }
      }
      xmlNode.End (   );
   }
   else {
      Delete (   );
   }
   return (DONE);
 }

 void EventRuleList::ResetObjectCounters (   )
 
{
   EventRule * evt_rule = First (   );
   while (evt_rule != NULL) {
      evt_rule->ERC_Counting.Count = 0;
      evt_rule = Next (evt_rule);
   }
}

 int EventRuleList::WriteFile (FileIO &file)
 
{
   int n_items = GetNumItems (   );
   if (file.Write ((byte *)&n_items,1,sizeof(int))) return (1);
   EventRule * evt_rule = First (   );
   while (evt_rule != NULL) {
      if (evt_rule->WriteFile (file)) return (1);
      evt_rule = Next (evt_rule);
   }
   return (DONE);
}

 int EventRuleList::WriteFile (CXMLIO* pIO)

{
   int n_items = GetNumItems (   );
   if (n_items == 0) return (DONE);
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("EventRuleList"))
   {
      xmlNode.Attribute (TYPE_ID (int),"ItemNum",&n_items);
      EventRule * evt_rule = First (   );
      while (evt_rule != NULL) {
         evt_rule->WriteFile (pIO);
         evt_rule = Next (evt_rule);
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleListManager
//
///////////////////////////////////////////////////////////////////////////////

 EventRuleListManager::~EventRuleListManager (   )
 
{
   Items.Delete (   );
}

 void EventRuleListManager::Add (EventRule *s_item)

{
   Lock (   );
   Item* new_item = new Item;
   new_item->ItemPtr = s_item;
   Items.Add (new_item);
   Unlock (   );
}

 int EventRuleListManager::GetNewID (   ) 

{
   Lock (   );
   int new_id = 0;
   while (TRUE) {
      int flag_same_id = FALSE;
      Item* item = Items.First (   );
      while (item) {
         if (new_id == item->ItemPtr->ID) {
            flag_same_id = TRUE;
            break;
         }
         item = Items.Next (item);
      }
      if (!flag_same_id) break;
      new_id++;
   }
   Unlock (   );
   return (new_id);
}

 void EventRuleListManager::Lock (   )
 
{
   CS_List.Lock (   );
}

 void EventRuleListManager::Remove (EventRule *s_item)
 
{
   Lock (   );
   Item* item = Items.First (   );
   while (item) {
      if (item->ItemPtr == s_item) {
         Items.Remove (item);
         delete item;
         break;
      }
      item = Items.Next (item);
   }
   Unlock (   );
}

 void EventRuleListManager::Unlock (   )
 
{
   CS_List.Unlock (   );
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 EventRule* CreateInstance_EventRule (   )

{
   if (__CreationFunction_EventRule == NULL) {
      EventRule *evt_rule = new EventRule;
      return (evt_rule);
   }
   else return (__CreationFunction_EventRule (   ));
}

 ObjectFiltering* CreateInstance_ObjectFiltering (   )

{
   if (__CreationFunction_ObjectFiltering == NULL) {
      ObjectFiltering *obj_filtering = new ObjectFiltering;
      return (obj_filtering);
   }
   else return (__CreationFunction_ObjectFiltering (   ));
}

 int GetEventTypeName (int evt_type,char* d_name)
 
{
   struct _ET2ETN {
      int  EvtType;
      char EvtName[64];
   };
   #if defined(__KOREAN)
   static _ET2ETN et2etn[] = {
      #if defined(__GS_CERT)
      { ER_EVENT_ABANDONED,          "버려짐/제거됨"   },
      #else
      { ER_EVENT_ABANDONED,          "버려짐"          },
      #endif
      { ER_EVENT_CAR_ACCIDENT,       "차량 사고"       },
      { ER_EVENT_CAR_PARKING,        "차량 주차"       },
      { ER_EVENT_CAR_STOPPING,       "차량 정지"       },
      { ER_EVENT_COLOR_CHANGE,       "색상 변화"       },
      { ER_EVENT_CROWD_DENSITY,      "과밀 군중"       },
      { ER_EVENT_DIRECTIONAL_MOTION, "방향성 이동"     },
      { ER_EVENT_DWELL,              "체류"            },
      { ER_EVENT_DWELL_OVERCROWDED,  "체류: 인원 초과" },
      { ER_EVENT_DWELL_OVERWAITING,  "체류: 시간 초과" },
      { ER_EVENT_ENTERING,           "진입"            },
      { ER_EVENT_FALLING,            "쓰러짐"          },
      { ER_EVENT_FLAME,              "불꽃"            },
      { ER_EVENT_GATHERING,          "군집"            },
      { ER_EVENT_LEAVING,            "진출"            },
      { ER_EVENT_LINE_CROSSING,      "가상선 통과"     },
      { ER_EVENT_LOITERING,          "배회"            },
      { ER_EVENT_PATH_PASSING,       "가상경로 통과"   },
      { ER_EVENT_REMOVED,            "제거됨"          },
      { ER_EVENT_SMOKE,              "연기"            },
      { ER_EVENT_STOPPING,           "멈춤"            },
      { ER_EVENT_TRAFFIC_CONGESTION, "교통 정체"       },
      { ER_EVENT_VIOLENCE,           "폭력"            },
      { ER_EVENT_WATER_LEVEL,        "비정상 수위"     },
      { ER_EVENT_ZONE_COLOR,         "영역 색상"       },
   };
   #else
   static _ET2ETN et2etn[] = {
      #if defined(__GS_CERT)
      { ER_EVENT_ABANDONED,          "Abandoned/Removed"     },
      #else
      { ER_EVENT_ABANDONED,          "Abandoned"             },
      #endif
      { ER_EVENT_CAR_ACCIDENT,       "Car Accident"          },
      { ER_EVENT_CAR_PARKING,        "Car Parking"           },
      { ER_EVENT_CAR_STOPPING,       "Car Stopping"          },
      { ER_EVENT_COLOR_CHANGE,       "Color Change"          },
      { ER_EVENT_CROWD_DENSITY,      "Overcrowded"           },
      { ER_EVENT_DIRECTIONAL_MOTION, "Directional Motion"    },
      { ER_EVENT_DWELL,              "Dwell"                 },
      { ER_EVENT_DWELL_OVERCROWDED,  "Dwell: Overcrowded"    },
      { ER_EVENT_DWELL_OVERWAITING,  "Dwell: Overwaiting"    },
      { ER_EVENT_ENTERING,           "Entering"              },
      { ER_EVENT_FALLING,            "Falling"               },
      { ER_EVENT_FLAME,              "Flame"                 },
      { ER_EVENT_GATHERING,          "Gathering"             },
      { ER_EVENT_LEAVING,            "Leaving"               },
      { ER_EVENT_LINE_CROSSING,      "Virtual Line Crossing" },
      { ER_EVENT_LOITERING,          "Loitering"             },
      { ER_EVENT_PATH_PASSING,       "Virtual Path Passing"  },
      { ER_EVENT_REMOVED,            "Removed"               },
      { ER_EVENT_SMOKE,              "Smoke"                 },
      { ER_EVENT_STOPPING,           "Stopping"              },
      { ER_EVENT_TRAFFIC_CONGESTION, "Traffic Congestion"    },
      { ER_EVENT_VIOLENCE,           "Violence"              },
      { ER_EVENT_WATER_LEVEL,        "Abnormal Water Level"  },
      { ER_EVENT_ZONE_COLOR,         "Zone Color"            },
   };
   #endif

   int n = sizeof(et2etn) / sizeof(_ET2ETN);
   for (int i = 0; i < n; i++) {
      if (et2etn[i].EvtType == evt_type) {
         strcpy (d_name,et2etn[i].EvtName);
         return (DONE);
      }
   }
   d_name[0] = 0;
   return (1);
}

 void SetCreationFunction_EventRule (EventRule*(*func)(   ))

{
   __CreationFunction_EventRule = func;
}

 void SetCreationFunction_ObjectFiltering (ObjectFiltering*(*func)(   ))

{
   __CreationFunction_ObjectFiltering = func;
}

}

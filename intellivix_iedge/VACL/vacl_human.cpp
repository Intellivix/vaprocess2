#include "vacl_human.h"
#include "vacl_warping.h"

 namespace VACL

{

#if defined(__LIB_DARKNET)

///////////////////////////////////////////////////////////////////////////////
// 
// Class: HumanDetection_YOLO
//
///////////////////////////////////////////////////////////////////////////////

 HumanDetection_YOLO::HumanDetection_YOLO (   )

{
   MinAspectRatio = 0.7f;
   MaxAspectRatio = 6.0f;
}

 HumanDetection_YOLO::~HumanDetection_YOLO (   )

{
   Close (   );
}

 void HumanDetection_YOLO::Close (   )

{
   DNN.Close (   );
}

 int HumanDetection_YOLO::Initialize (const char* dir_name)

{
   const char cfg_file_name[] = "IVXODCDKN1.dat";
   const char wgt_file_name[] = "IVXODWDKN1.dat";
   const char cls_file_name[] = "IVXODNDKN1.dat";

   StringA cfg_path_name;
   cfg_path_name.Format ("%s/%s",dir_name,cfg_file_name);
   StringA wgt_path_name;
   wgt_path_name.Format ("%s/%s",dir_name,wgt_file_name);
   StringA cls_path_name;
   cls_path_name.Format ("%s/%s",dir_name,cls_file_name);
   return (DNN.Initialize (cfg_path_name,wgt_path_name,cls_path_name));
}

 int HumanDetection_YOLO::Perform (GImage& s_image,IB2DArray1D& d_rects)

{
   int i,n;
   
   d_rects.Delete (   );
   if (!IsInitialized (   )) return (1);
   GImage  t_image;
   GImage* p_s_image = &s_image;
   if (DNN.SrcImgSize.Width != s_image.Width || DNN.SrcImgSize.Height != s_image.Height) {
      t_image.Create (DNN.SrcImgSize.Width,DNN.SrcImgSize.Height);
      Resize2 (s_image,t_image);
      p_s_image = &t_image;
   }
   BGRImage s_cimage = *p_s_image;
   OBBArray1D obb_array;
   DNN.Predict (s_cimage,obb_array);
   if (!obb_array) return (DONE);
   float scale_x = (float)s_image.Width  / DNN.SrcImgSize.Width;
   float scale_y = (float)s_image.Height / DNN.SrcImgSize.Height;
   IB2DArray1D t_rects(obb_array.Length);
   for (i = n = 0; i < obb_array.Length; i++) {
      if (obb_array[i].ClassIdx == 0) { // 사람인 경우
         IBox2D t_rect;
         ObjBndBox& obb = obb_array[i];
         t_rect.X      = (int)(scale_x * obb.X);
         t_rect.Y      = (int)(scale_y * obb.Y);
         t_rect.Width  = (int)(scale_x * obb.Width);
         t_rect.Height = (int)(scale_y * obb.Height);
         if (t_rect.Width) {
            float ar = (float)t_rect.Height / t_rect.Width;
            if (MinAspectRatio <= ar && ar <= MaxAspectRatio) {

               if (t_rect.Height > t_rect.Width) t_rect.Height = t_rect.Width;

               t_rects[n++] = t_rect;
            }
         }
      }
   }
   if (n) {
      Array1D<ObjectScore> os_array(n);
      for (i = 0; i < n; i++) {
         os_array[i].IndexNo = i;
         os_array[i].Score   = (double)t_rects[i].Y;
      }
      qsort (os_array,n,sizeof(ObjectScore),Compare_ObjectScore_Descending);
      d_rects.Create (n);
      for (i = 0; i < n; i++) {
         d_rects[i] = t_rects[os_array[i].IndexNo];
      }
   }
   return (DONE);
}

#endif

}

//
// 이곳에서 정의된 클래스들에 대한 자세한 사용법은 IntelliVIX의 소스 파일들 중에
// "IVCP_LiveSender.cpp" 파일의 내용을 참조하기 바람.
//

#include "ivcp_pkstruct.h"

 namespace IVCP
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

typedef struct _IvcpPacktIDString {
   const char *prefix;
   const char *desc;
} IvcpPacktIDString;

static IvcpPacktIDString s_IvcpPacktIDString[] = 
{
   "U", "U",            // IVCP_ID_Unknown
   "N", "OPTION",       // IVCP_ID_Conn_OptionsReq
   "N", "OPTION",       // IVCP_ID_Conn_OptionsRes
   "N", "LOGIN",        // IVCP_ID_Conn_LoginReq
   "N", "LOGIN",        // IVCP_ID_Conn_LoginRes
   "N", "LOGOUT",       // IVCP_ID_Conn_LogoutReq
   "N", "LOGOUT",       // IVCP_ID_Conn_LogoutRes
   "N", "CAMLIST",      // IVCP_ID_Conn_CameraListReq
   "N", "CAMLIST",      // IVCP_ID_Conn_CameraListRes
   "N", "STARTUP",      // IVCP_ID_Conn_StartUpReq
   "N", "STARTUP",      // IVCP_ID_Conn_StartUpRes
   "N", "SHUTDOWN",     // IVCP_ID_Conn_ShutDownReq
   "N", "SHUTDOWN",     // IVCP_ID_Conn_ShutDownRes
   "N", "SNAPSHOT",     // IVCP_ID_Conn_SnapshotReq,              // 초기 접속시 카메라 스냅 화면 요청
   "N", "SNAPSHOT",     // IVCP_ID_Conn_SnapshotRes,
   
   "C", "LIVEMETA",     // IVCP_ID_Camera_LiveMetaDataReq,
   "C", "LIVEMETA",     // IVCP_ID_Camera_LiveMetaDataRes,
   "C", "LIVEVIDEO",    // IVCP_ID_Camera_LiveVideoReq,
   "C", "LIVEVIDEO",    // IVCP_ID_Camera_LiveVideoRes,

   "C", "PLAYSTART",    // IVCP_ID_Camera_PlayStartReq,
   "C", "PLAYSTART",    // IVCP_ID_Camera_PlayStartRes,
   "C", "PLAYSTOP",     // IVCP_ID_Camera_PlayStopReq,
   "C", "PLAYSTOP",     // IVCP_ID_Camera_PlayStopRes,
   "C", "PLAYCTRL",     // IVCP_ID_Camera_PlayControlReq,
   "C", "PLAYCTRL",     // IVCP_ID_Camera_PlayControlRes,

   "C", "SNAPSHOT",     // IVCP_ID_Camera_SnapshotReq
   "C", "SNAPSHOT",     // IVCP_ID_Camera_SnapshotRes,
   "C", "SETTING",      // IVCP_ID_Camera_SettingReq
   "C", "SETTING",      // IVCP_ID_Camera_SettingRes,
   "C", "COMMAND",      // IVCP_ID_Camera_CommandReq,
   "C", "COMMAND",      // IVCP_ID_Camera_CommandRes,
   "C", "TALKAUDIO",    // IVCP_ID_Camera_TalkAudioReq,
   "C", "TALKAUDIO",    // IVCP_ID_Camera_TalkAudioRes,
   "C", "TALKSTATUS",   // IVCP_ID_Camera_TalkStatus,
   "C", "AUDIODATA",    // IVCP_ID_Camera_SendAudioData,
   "C", "AUDIODATA",    // IVCP_ID_Camera_RecvAudioData,

   "Z", "CONTMOV",      // IVCP_ID_Ptz_ContMoveReq,
   "Z", "CONTMOV",      // IVCP_ID_Ptz_ContMoveRes,
   "Z", "ABSMOV",       // IVCP_ID_Ptz_AbsMoveReq,
   "Z", "ABSMOV",       // IVCP_ID_Ptz_AbsMoveRes,
   "Z", "BOXMOV",       // IVCP_ID_Ptz_BoxMoveReq,
   "Z", "BOXMOV",       // IVCP_ID_Ptz_BoxMoveRes,
   "Z", "GETABSPOS",    // IVCP_ID_Ptz_GetAbsPosReq,
   "Z", "GETABSPOS",    // IVCP_ID_Ptz_GetAbsPosRes,

   "L", "FRAMEINFO",    // IVCP_ID_Live_FrameInfo,
   "L", "EVENTINFO",    // IVCP_ID_Live_EventInfo,
   "L", "OBJECTINFO",   // IVCP_ID_Live_ObjectInfo,
   "L", "PLATEINFO",    // IVCP_ID_Live_PlateInfo,
   "L", "FACEINFO",     // IVCP_ID_Live_FaceInfo,
   "L", "LOGINFO",      // IVCP_ID_Live_LogInfo,
   "L", "PTZDATAINFO",  // IVCP_ID_Live_PtzDataInfo,
   "L", "PRESETIDINFO", // IVCP_ID_Live_PresetIdInfo,
   "L", "EVTZONEINFO",  // IVCP_ID_Live_EventZoneInfo,
   "L", "EVTZONECNT",   // IVCP_ID_Live_EventZoneCount,
   "L", "PRESETMAPINFO",// IVCP_ID_Live_PresetMapInfo,
   "L", "SETTINGINFO",  // IVCP_ID_Live_SettingInfo,
   "L", "VIDEO",        // IVCP_ID_Live_VideoData,
   "L", "AUDIO",        // IVCP_ID_Live_AudioData,
   "L", "FRAMEOBJ",     // IVCP_ID_Live_FrmObjInfo
   "L", "STATUS",       // IVCP_ID_Live_Status

   "P", "FRAMEINFO",    // IVCP_ID_Play_FrameInfo,
   "P", "EVENTINFO",    // IVCP_ID_Play_EventInfo,
   "P", "OBJECTINFO",   // IVCP_ID_Play_ObjectInfo,
   "P", "PLATEINFO",    // IVCP_ID_Play_PlateInfo,
   "P", "FACEINFO",     // IVCP_ID_Play_FaceInfo,
   "P", "PTZDATAINFO",  // IVCP_ID_Play_PtzDataInfo,
   "P", "PRESETIDINFO", // IVCP_ID_Play_PresetIdInfo,
   "P", "EVTZONEINFO",  // IVCP_ID_Play_EventZoneInfo,
   "P", "EVTZONECNT",   // IVCP_ID_Play_EventZoneCount,
   "P", "VIDEO",        // IVCP_ID_Play_VideoData,
   "P", "AUDIO",        // IVCP_ID_Play_AudioData,
   "P", "PROGRESS",     // IVCP_ID_Play_Progress,

   "F", "FRAME",        // IVCP_ID_Search_FrameReq
   "F", "FRAME",        // IVCP_ID_Search_FrameRes
   "F", "EVENT",        // IVCP_ID_Search_EventReq
   "F", "EVENT",        // IVCP_ID_Search_EventRes
   "F", "OBJECT",       // IVCP_ID_Search_ObjectReq
   "F", "OBJECT",       // IVCP_ID_Search_ObjectRes
   "F", "PLATE",        // IVCP_ID_Search_PlateReq
   "F", "PLATE",        // IVCP_ID_Search_PlateRes
   "F", "FACE",         // IVCP_ID_Search_FaceReq
   "F", "FACE",         // IVCP_ID_Search_FaceRes
   "F", "LOG",          // IVCP_ID_Search_LogReq
   "F", "LOG",          // IVCP_ID_Search_LogRes
   "F", "STOP",         // IVCP_ID_Search_StopReq (통합)
   "F", "STOP",         // IVCP_ID_Search_StopRes

   "F", "PROGRESS",     // IVCP_ID_Search_Progress (검색 진행 정보)
   "F", "FRAMEINFO",    // IVCP_ID_Search_FreameInfo
   "F", "EVENTINFO",    // IVCP_ID_Search_EventInfo
   "F", "OBJECTINFO",   // IVCP_ID_Search_ObjectInfo
   "F", "PLATEINFO",    // IVCP_ID_Search_PlateInfo
   "F", "FACEINFO",     // IVCP_ID_Search_FaceInfo
   "F", "LOGINFO",      // IVCP_ID_Search_LogInfo

   "S", "SETTING",      // IVCP_ID_System_SettingReq
   "S", "SETTING",      // IVCP_ID_System_SettingRes,
   "S", "COMMAND",      // IVCP_ID_System_CommandReq,
   "S", "COMMAND",      // IVCP_ID_System_CommandRes,
   "S", "MONITOR",      // IVCP_ID_System_MonitorReq,
   "S", "MONITOR",      // IVCP_ID_System_MonitorRes,
   "S", "MONITORINFO",  // IVCP_ID_System_MonitorInfo,
   "S", "AUDIOINOUT",   // IVCP_ID_System_AudioInOutReq,
   "S", "AUDIOINOUT",   // IVCP_ID_System_AudioInOutRes,
   "S", "AUDIODATA",    // IVCP_ID_System_SendAudioData,
   "S", "AUDIODATA",    // IVCP_ID_System_RecvAudioData,

   "C", "EXPORTSTART",  // IVCP_ID_Camera_ExportStartReq,
   "C", "EXPORTSTART",  // IVCP_ID_Camera_ExportStartRes,
   "C", "EXPORTSTOP",   // IVCP_ID_Camera_ExportStopReq,
   "C", "EXPORTSTOP",   // IVCP_ID_Camera_ExportStopRes,

   "E", "PROGRESS",     // IVCP_ID_Export_Progress,
   "E", "FRAMEINFO",    // IVCP_ID_Export_FrameInfo,
   "E", "EVENTINFO",    // IVCP_ID_Export_EventInfo,
   "E", "OBJECTINFO",   // IVCP_ID_Export_ObjectInfo,
   "E", "PLATEINFO",    // IVCP_ID_Export_PlateInfo,
   "E", "FACEINFO",     // IVCP_ID_Export_FaceInfo,
   "E", "PTZDATAINFO",  // IVCP_ID_Export_PtzDataInfo,
   "E", "PRESETIDINFO", // IVCP_ID_Export_PresetIdInfo,
   "E", "EVTZONEINFO",  // IVCP_ID_Export_EventZoneInfo,
   "E", "EVTZONECNT",   // IVCP_ID_Export_EventZoneCount,
   "E", "VIDEO",        // IVCP_ID_Export_VideoData,
   "E", "AUDIO",        // IVCP_ID_Export_AudioData,
};

const char *IvcpGetPacketIDString (int nPacketID)
{
   return s_IvcpPacktIDString[nPacketID].desc;
}

const char *IvcpGetPacketDescString (int nPacketID)
{
   return s_IvcpPacktIDString[nPacketID].prefix;
}

void IvcpGetPacketString (char *strMessage, int nPacketID, int nBodyType)
{
   switch (nBodyType)
   {
   case IVCP_TYPE_XML:  // XML 텍스트
      sprintf(strMessage, "%s_%s", s_IvcpPacktIDString[nPacketID].prefix, s_IvcpPacktIDString[nPacketID].desc);
      break;
   case IVCP_TYPE_TXT:  // Text 형태
      sprintf(strMessage, "%s.%s", s_IvcpPacktIDString[nPacketID].prefix, s_IvcpPacktIDString[nPacketID].desc);
      break;
   case IVCP_TYPE_BIN:  // Binary 형태
      sprintf(strMessage, "%s-%s", s_IvcpPacktIDString[nPacketID].prefix, s_IvcpPacktIDString[nPacketID].desc);
      break;
   case IVCP_TYPE_XMZ:  // XML 압축 (with Zip)
      sprintf(strMessage, "%s=%s", s_IvcpPacktIDString[nPacketID].prefix, s_IvcpPacktIDString[nPacketID].desc);
      break;
   }
}

BOOL IvcpIsBinaryPacket (const char *strMessage)
{
   if (strMessage[2] == '.') return TRUE;
   return FALSE;
}

int IvcpGetPacketType (const char *strMessage)
{
   switch (*strMessage)
   {
   case 'N': // Connection
      return IVCP_TYPE_CONN;
   case 'S': // System
      return IVCP_TYPE_SYSTEM;
   case 'C': // Camera
      return IVCP_TYPE_CAMERA;
   case 'Z': // PTZ 제어
      return IVCP_TYPE_PTZ;
   case 'L': // 실시간 데이터
      return IVCP_TYPE_LIVE;
   case 'P': // 재생 데이터
      return IVCP_TYPE_PLAY;
   case 'F': // 검색 데이터
      return IVCP_TYPE_SEARCH;
   case 'E': // 내보내기 데이터
      return IVCP_TYPE_EXPORT;
   }
   return (IVCP_TYPE_UNKNOWN);
}

int IvcpGetPacketIDforReq (const char *strMessage) // Request 만 비교한다.
{
   int nPacketType = IvcpGetPacketType(strMessage);
   char *strString = (char*)strMessage + 2; // "C_STRING"
   switch (nPacketType)
   {
   case IVCP_TYPE_CONN:     // 연결
      if      (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Conn_OptionsReq].desc) == 0)    return IVCP_ID_Conn_OptionsReq;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Conn_LoginReq].desc) == 0)      return IVCP_ID_Conn_LoginReq;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Conn_LogoutReq].desc) == 0)     return IVCP_ID_Conn_LogoutReq;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Conn_CameraListReq].desc) == 0) return IVCP_ID_Conn_CameraListReq;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Conn_StartUpReq].desc) == 0)    return IVCP_ID_Conn_StartUpReq;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Conn_SnapshotReq].desc) == 0)   return IVCP_ID_Conn_SnapshotReq;
      break;
   case IVCP_TYPE_SYSTEM:   // 시스템
      if      (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_System_SettingReq].desc) == 0)    return IVCP_ID_System_SettingReq;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_System_CommandReq].desc) == 0)    return IVCP_ID_System_CommandReq;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_System_MonitorReq].desc) == 0)    return IVCP_ID_System_MonitorReq;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_System_AudioInOutReq].desc) == 0) return IVCP_ID_System_AudioInOutReq;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_System_SendAudioData].desc) == 0) return IVCP_ID_System_SendAudioData;
      break;
   case IVCP_TYPE_CAMERA:   // 카메라
      if      (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Camera_LiveMetadataReq].desc) == 0)      return IVCP_ID_Camera_LiveMetadataReq;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Camera_LiveVideoReq].desc) == 0)         return IVCP_ID_Camera_LiveVideoReq;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Camera_PlayStreamStartReq].desc) == 0)   return IVCP_ID_Camera_PlayStreamStartReq;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Camera_PlayStreamStopReq].desc) == 0)    return IVCP_ID_Camera_PlayStreamStopReq;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Camera_PlayStreamControlReq].desc) == 0) return IVCP_ID_Camera_PlayStreamControlReq;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Camera_SnapshotReq].desc) == 0)          return IVCP_ID_Camera_SnapshotReq;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Camera_SettingReq].desc) == 0)           return IVCP_ID_Camera_SettingReq;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Camera_CommandReq].desc) == 0)           return IVCP_ID_Camera_CommandReq;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Camera_TalkAudioReq].desc) == 0)         return IVCP_ID_Camera_TalkAudioReq;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Camera_SendAudioData].desc) == 0)        return IVCP_ID_Camera_SendAudioData;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Camera_ExportStartReq].desc) == 0)       return IVCP_ID_Camera_ExportStartReq;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Camera_ExportStopReq].desc) == 0)        return IVCP_ID_Camera_ExportStopReq;
      break;
   case IVCP_TYPE_PTZ:     // PTZ 제어
      if      (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Ptz_ContMoveReq].desc) == 0)  return IVCP_ID_Ptz_ContMoveReq;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Ptz_AbsMoveReq].desc) == 0)   return IVCP_ID_Ptz_AbsMoveReq;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Ptz_BoxMoveReq].desc) == 0)   return IVCP_ID_Ptz_BoxMoveReq;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Ptz_GetAbsPosReq].desc) == 0) return IVCP_ID_Ptz_GetAbsPosReq;
      break;
   case IVCP_TYPE_SEARCH:  // 검색 관련
      if      (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Search_EventReq].desc) == 0)  return IVCP_ID_Search_EventReq;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Search_FrameReq].desc) == 0)  return IVCP_ID_Search_FrameReq;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Search_ObjectReq].desc) == 0) return IVCP_ID_Search_ObjectReq;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Search_PlateReq].desc) == 0)  return IVCP_ID_Search_PlateReq;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Search_FaceReq].desc) == 0)   return IVCP_ID_Search_FaceReq;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Search_LogReq].desc) == 0)    return IVCP_ID_Search_LogReq;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Search_StopReq].desc) == 0)   return IVCP_ID_Search_StopReq;
      break;
   }
   return (IVCP_ID_Unknown);
}

int IvcpGetPacketIDforRes (const char *strMessage) // Response 만 비교한다.
{
   int nPacketType = IvcpGetPacketType(strMessage);
   char *strString = (char*)strMessage + 2; // "C_STRING"
   switch (nPacketType)
   {
   case IVCP_TYPE_CONN:     // 연결
      if      (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Conn_OptionsRes].desc) == 0)    return IVCP_ID_Conn_OptionsRes;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Conn_LoginRes].desc) == 0)      return IVCP_ID_Conn_LoginRes;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Conn_LogoutRes].desc) == 0)     return IVCP_ID_Conn_LogoutRes;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Conn_CameraListRes].desc) == 0) return IVCP_ID_Conn_CameraListRes;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Conn_StartUpRes].desc) == 0)    return IVCP_ID_Conn_StartUpRes;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Conn_SnapshotRes].desc) == 0)   return IVCP_ID_Conn_SnapshotRes;
      break;
   case IVCP_TYPE_SYSTEM:   // 시스템
      if      (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_System_SettingRes].desc) == 0)    return IVCP_ID_System_SettingRes;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_System_CommandRes].desc) == 0)    return IVCP_ID_System_CommandRes;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_System_MonitorRes].desc) == 0)    return IVCP_ID_System_MonitorRes;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_System_MonitorInfo].desc) == 0)   return IVCP_ID_System_MonitorInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_System_AudioInOutRes].desc) == 0) return IVCP_ID_System_AudioInOutRes;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_System_RecvAudioData].desc) == 0) return IVCP_ID_System_RecvAudioData;
      break;
   case IVCP_TYPE_CAMERA:   // 카메라
      if      (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Camera_LiveMetadataRes].desc) == 0)      return IVCP_ID_Camera_LiveMetadataRes;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Camera_LiveVideoRes].desc) == 0)         return IVCP_ID_Camera_LiveVideoRes;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Camera_PlayStreamStartRes].desc) == 0)   return IVCP_ID_Camera_PlayStreamStartRes;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Camera_PlayStreamStopRes].desc) == 0)    return IVCP_ID_Camera_PlayStreamStopRes;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Camera_PlayStreamControlRes].desc) == 0) return IVCP_ID_Camera_PlayStreamControlRes;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Camera_SnapshotRes].desc) == 0)          return IVCP_ID_Camera_SnapshotRes;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Camera_SettingRes].desc) == 0)           return IVCP_ID_Camera_SettingRes;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Camera_CommandRes].desc) == 0)           return IVCP_ID_Camera_CommandRes;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Camera_TalkAudioRes].desc) == 0)         return IVCP_ID_Camera_TalkAudioRes;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Camera_TalkStatus].desc) == 0)           return IVCP_ID_Camera_TalkStatus;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Camera_RecvAudioData].desc) == 0)        return IVCP_ID_Camera_RecvAudioData;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Camera_ExportStartRes].desc) == 0)       return IVCP_ID_Camera_ExportStartRes;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Camera_ExportStopRes].desc) == 0)        return IVCP_ID_Camera_ExportStopRes;
      break;
   case IVCP_TYPE_PTZ:     // PTZ 제어
      if      (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Ptz_ContMoveRes].desc) == 0)  return IVCP_ID_Ptz_ContMoveRes;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Ptz_AbsMoveRes].desc) == 0)   return IVCP_ID_Ptz_AbsMoveRes;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Ptz_BoxMoveRes].desc) == 0)   return IVCP_ID_Ptz_BoxMoveRes;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Ptz_GetAbsPosRes].desc) == 0) return IVCP_ID_Ptz_GetAbsPosRes;
      break;
   case IVCP_TYPE_LIVE:    // 실시간 데이터
      if      (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Live_FrameInfo].desc) == 0)      return IVCP_ID_Live_FrameInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Live_FrmObjInfo].desc) == 0)     return IVCP_ID_Live_FrmObjInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Live_EventInfo].desc) == 0)      return IVCP_ID_Live_EventInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Live_ObjectInfo].desc) == 0)     return IVCP_ID_Live_ObjectInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Live_PlateInfo].desc) == 0)      return IVCP_ID_Live_PlateInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Live_FaceInfo].desc) == 0)       return IVCP_ID_Live_FaceInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Live_PtzDataInfo].desc) == 0)    return IVCP_ID_Live_PtzDataInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Live_PresetIdInfo].desc) == 0)   return IVCP_ID_Live_PresetIdInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Live_EventZoneInfo].desc) == 0)  return IVCP_ID_Live_EventZoneInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Live_EventZoneCount].desc) == 0) return IVCP_ID_Live_EventZoneCount;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Live_PresetMapInfo].desc) == 0)  return IVCP_ID_Live_PresetMapInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Live_SettingInfo].desc) == 0)    return IVCP_ID_Live_SettingInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Live_VideoData].desc) == 0)      return IVCP_ID_Live_VideoData;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Live_AudioData].desc) == 0)      return IVCP_ID_Live_AudioData;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Live_Status].desc) == 0)         return IVCP_ID_Live_Status;      
      break;
   case IVCP_TYPE_PLAY:    // 재생 데이터
      if      (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Play_FrameInfo].desc) == 0)      return IVCP_ID_Play_FrameInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Play_EventInfo].desc) == 0)      return IVCP_ID_Play_EventInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Play_ObjectInfo].desc) == 0)     return IVCP_ID_Play_ObjectInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Play_PlateInfo].desc) == 0)      return IVCP_ID_Play_PlateInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Play_FaceInfo].desc) == 0)       return IVCP_ID_Play_FaceInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Play_PtzDataInfo].desc) == 0)    return IVCP_ID_Play_PtzDataInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Play_PresetIdInfo].desc) == 0)   return IVCP_ID_Play_PresetIdInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Play_EventZoneInfo].desc) == 0)  return IVCP_ID_Play_EventZoneInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Play_EventZoneCount].desc) == 0) return IVCP_ID_Play_EventZoneCount;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Play_VideoData].desc) == 0)      return IVCP_ID_Play_VideoData;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Play_AudioData].desc) == 0)      return IVCP_ID_Play_AudioData;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Play_Progress].desc) == 0)       return IVCP_ID_Play_Progress;
      break;
   case IVCP_TYPE_SEARCH:  // 검색 관련
      if      (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Search_Progress].desc) == 0)   return IVCP_ID_Search_Progress;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Search_FrameInfo].desc) == 0)  return IVCP_ID_Search_FrameInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Search_EventInfo].desc) == 0)  return IVCP_ID_Search_EventInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Search_ObjectInfo].desc) == 0) return IVCP_ID_Search_ObjectInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Search_PlateInfo].desc) == 0)  return IVCP_ID_Search_PlateInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Search_FaceInfo].desc) == 0)   return IVCP_ID_Search_FaceInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Search_LogInfo].desc) == 0)    return IVCP_ID_Search_LogInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Search_FrameRes].desc) == 0)   return IVCP_ID_Search_FrameRes;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Search_EventRes].desc) == 0)   return IVCP_ID_Search_EventRes;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Search_ObjectRes].desc) == 0)  return IVCP_ID_Search_ObjectRes;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Search_PlateRes].desc) == 0)   return IVCP_ID_Search_PlateRes;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Search_FaceRes].desc) == 0)    return IVCP_ID_Search_FaceRes;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Search_LogRes].desc) == 0)     return IVCP_ID_Search_LogRes;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Search_StopRes].desc) == 0)    return IVCP_ID_Search_StopRes;
      break;
   case IVCP_TYPE_EXPORT:  // 내보내기 관련
      if      (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Export_FrameInfo].desc) == 0)      return IVCP_ID_Export_FrameInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Export_EventInfo].desc) == 0)      return IVCP_ID_Export_EventInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Export_ObjectInfo].desc) == 0)     return IVCP_ID_Export_ObjectInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Export_PlateInfo].desc) == 0)      return IVCP_ID_Export_PlateInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Export_FaceInfo].desc) == 0)       return IVCP_ID_Export_FaceInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Export_PtzDataInfo].desc) == 0)    return IVCP_ID_Export_PtzDataInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Export_PresetIdInfo].desc) == 0)   return IVCP_ID_Export_PresetIdInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Export_EventZoneInfo].desc) == 0)  return IVCP_ID_Export_EventZoneInfo;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Export_EventZoneCount].desc) == 0) return IVCP_ID_Export_EventZoneCount;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Export_VideoData].desc) == 0)      return IVCP_ID_Export_VideoData;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Export_AudioData].desc) == 0)      return IVCP_ID_Export_AudioData;
      else if (strcmp(strString, s_IvcpPacktIDString[IVCP_ID_Export_Progress].desc) == 0)       return IVCP_ID_Export_Progress;
      break;
   }
   return (IVCP_ID_Unknown);
}

void ivcpGetFileTimeFromString(FILETIME &ftTime, const char *strTime)
{
   int nLen = (int)strlen(strTime);
   if (nLen < 17) return;
   //                      01234567890123456
   // Time String Format : 20050105101010123
   char szYear [8];
   char szMonth[4];
   char szDay  [4];
   char szHour [4];
   char szMin  [4];
   char szSec  [4];
   char szMSec [4];
   strncpy(szYear,  strTime,    4); szYear [4] = 0;
   strncpy(szMonth, strTime+4,  2); szMonth[2] = 0;
   strncpy(szDay,   strTime+6,  2); szDay  [2] = 0;
   strncpy(szHour,  strTime+8,  2); szHour [2] = 0;
   strncpy(szMin,   strTime+10, 2); szMin  [2] = 0;
   strncpy(szSec,   strTime+12, 2); szSec  [2] = 0;
   strncpy(szMSec,  strTime+14, 3); szMSec [3] = 0;

   SYSTEMTIME st;
   st.wYear   = atoi(szYear );
   st.wMonth  = atoi(szMonth);
   st.wDay    = atoi(szDay  );
   st.wHour   = atoi(szHour );
   st.wMinute = atoi(szMin  );
   st.wSecond = atoi(szSec  );
   st.wMilliseconds = atoi(szMSec);
   Convert (st, ftTime);
}

void ivcpGetStringFromFileTime(StringA &strTime, FILETIME &ftTime)
{
   Time t_time;
   Convert (ftTime,t_time);
   strTime.Format("%04d%02d%02d_%02d%02d%02d_%03d",t_time.Year,t_time.Month,t_time.Day,t_time.Hour,t_time.Minute,t_time.Second,t_time.Millisecond);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CIVCPPacketExt
//
///////////////////////////////////////////////////////////////////////////////

int CIVCPPacketExt::ReadPacketBody (FileIO& buff)
{
   CXMLIO xmlIO (&buff, XMLIO_Read);
   if (DONE != ReadXML (&xmlIO)) return (1);
   return (DONE);
}

int CIVCPPacketExt::WritePacketBody (FileIO& buff)
{
   CXMLIO xmlIO (&buff, XMLIO_Write);
   if (DONE != WriteXML (&xmlIO)) return (1);
   return (DONE);
}

int CIVCPPacketExt::ReadFromPacket (CIVCPPacket& aPacket)
{
   if (aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody) 
   {
      FileIO buffBody;
      buffBody.Attach((byte*)aPacket.m_pBody, aPacket.m_Header.m_nBodyLen);
      if (DONE != ReadPacketBody (buffBody)) 
      {
         buffBody.Detach();
         return (2);
      }
      buffBody.Detach();
      return (DONE);
   }
   return (1);
}

/*
// 원본
//
// 본 함수를 그대로 사용하면 m_DataLen의 값은 "0", m_nBodyLen의 값은 "XML 데이터의 길이"가 된다.
// 그러나 다른 클래스의 MakeBodyBuffer() 함수를 분석해보면 m_DataLen의 값은 "XML 데이터의 길이",
// m_nBodyLen의 값은 "XML 데이터의 길이 + BIN 데이터의 길이"가 된다. 따라서 본 함수를 그대로
// 사용하는 클래스와 MakeBodyBuffer() 함수를 사용하는 클래스 간에 프로토콜 정의의 일관성이
// 떨어지는 문제가 있다.
//

int CIVCPPacketExt::WriteToPacket (CIVCPPacket& aPacket)
{
   if (aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody) 
   {
      aPacket.FreeBody();
   }
   FileIO buffBody;
   if (DONE == WritePacketBody (buffBody)) 
   {
      aPacket.m_Header.m_nBodyLen = buffBody.GetLength();
      aPacket.m_pBody = (char*)buffBody.Detach();
      return (DONE);
   }
   return (1);
}
*/

//
// 수정본
//
int CIVCPPacketExt::WriteToPacket (CIVCPPacket& aPacket)
{
   if (aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody) 
   {
      aPacket.FreeBody (   );
   }
   FileIO buffBody;
   if (DONE == WritePacketBody (buffBody)) 
   {
      aPacket.m_Header.m_nDataLen = buffBody.GetLength();
      aPacket.m_Header.m_nBodyLen = aPacket.m_Header.m_nDataLen;
      aPacket.m_pBody = (char*)buffBody.Detach();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnOptionsReq : 서버 기본 정보
//
///////////////////////////////////////////////////////////////////////////////

CIVCPConnOptionsReq::CIVCPConnOptionsReq ()
{
   m_nPacketID   = IVCP_ID_Conn_OptionsReq;
   m_fNetworkVer = 3.0;
}
int CIVCPConnOptionsReq::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Options"))
   {
      xmlNode.Attribute (TYPE_ID (float),"NetVer", &m_fNetworkVer);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPConnOptionsReq::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Options"))
   {
      xmlNode.Attribute (TYPE_ID (float),"NetVer", &m_fNetworkVer);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnOptionsRes : 서버 기본 정보
// 
///////////////////////////////////////////////////////////////////////////////

CIVCPConnOptionsRes::CIVCPConnOptionsRes()
{
   m_nPacketID   = IVCP_ID_Conn_OptionsRes;
   m_nDeviceType = 0;
   m_nDeviceVer  = 0;
   m_fNetworkVer = 3.0;
   m_nCameraNum  = 0;
}
int CIVCPConnOptionsRes::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Options"))
   {
      StringA strValue;
      xmlNode.Attribute (TYPE_ID (StringA), "DevName",   &strValue);
      strcpy(m_strDeviceName, strValue);
      xmlNode.Attribute (TYPE_ID (int),     "DevType",   &m_nDeviceType);
      xmlNode.Attribute (TYPE_ID (int),     "DevVer",    &m_nDeviceVer);
      xmlNode.Attribute (TYPE_ID (float),   "NetVer",    &m_fNetworkVer);
      xmlNode.Attribute (TYPE_ID (StringA), "SysName",   &strValue);
      strcpy(m_strSystemName, strValue);
      xmlNode.Attribute (TYPE_ID (int),     "ChNum",     &m_nCameraNum);
      xmlNode.Attribute (TYPE_ID (int),     "SystemUID", &m_nSystemUID);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPConnOptionsRes::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Options"))
   {
      StringA strValue;
      strValue = m_strDeviceName;
      xmlNode.Attribute (TYPE_ID (StringA), "DevName",   &strValue);
      xmlNode.Attribute (TYPE_ID (int),     "DevType",   &m_nDeviceType);
      xmlNode.Attribute (TYPE_ID (int),     "DevVer",    &m_nDeviceVer);
      xmlNode.Attribute (TYPE_ID (float),   "NetVer",    &m_fNetworkVer);
      strValue = m_strSystemName;
      xmlNode.Attribute (TYPE_ID (StringA), "SysName",   &strValue);
      xmlNode.Attribute (TYPE_ID (int),     "ChNum",     &m_nCameraNum);
      xmlNode.Attribute (TYPE_ID (int),     "SystemUID", &m_nSystemUID);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnLoginReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPConnLoginReq::CIVCPConnLoginReq ()
{
   m_nPacketID    = IVCP_ID_Conn_LoginReq;
   m_nLoginType   = 0;
   m_nDeviceType  = 0;
   m_nServiceType = 0;
   m_nServiceCaps = 0;
}
int CIVCPConnLoginReq::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Login"))
   {
      StringA strValue;
      xmlNode.Attribute (TYPE_ID (int),     "Type",    &m_nLoginType);
      xmlNode.Attribute (TYPE_ID (StringA), "ID",      &strValue);
      strcpy(m_strUserID, strValue);
      xmlNode.Attribute (TYPE_ID (StringA), "PW",      &strValue);
      strcpy(m_strPasswd, strValue);
      xmlNode.Attribute (TYPE_ID (StringA), "DevName", &strValue);
      strcpy(m_strDeviceName, strValue);
      xmlNode.Attribute (TYPE_ID (int),      "DevType", &m_nDeviceType);
      xmlNode.Attribute (TYPE_ID (int),      "SOType",  &m_nServiceType);
      xmlNode.Attribute (TYPE_ID (int),      "SOCaps",  &m_nServiceCaps);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPConnLoginReq::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Login"))
   {
      StringA strValue;
      xmlNode.Attribute (TYPE_ID (int),     "Type",    &m_nLoginType);
      strValue = m_strUserID;
      xmlNode.Attribute (TYPE_ID (StringA), "ID",      &strValue);
      strValue = m_strPasswd;
      xmlNode.Attribute (TYPE_ID (StringA), "PW",      &strValue);
      strValue = m_strDeviceName;
      xmlNode.Attribute (TYPE_ID (StringA), "DevName", &strValue);
      xmlNode.Attribute (TYPE_ID (int),      "DevType", &m_nDeviceType);
      xmlNode.Attribute (TYPE_ID (int),      "SOType",  &m_nServiceType);
      xmlNode.Attribute (TYPE_ID (int),      "SOCaps",  &m_nServiceCaps);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnLoginRes
//
///////////////////////////////////////////////////////////////////////////////

CIVCPConnLoginRes::CIVCPConnLoginRes ()
{
   m_nPacketID    = IVCP_ID_Conn_LoginRes;
   m_nLoginResult = 0;
}
int CIVCPConnLoginRes::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Login"))
   {
      xmlNode.Attribute (TYPE_ID (int), "Result", &m_nLoginResult);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPConnLoginRes::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Login"))
   {
      xmlNode.Attribute (TYPE_ID (int), "Result", &m_nLoginResult);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnLogoutReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPConnLogoutReq::CIVCPConnLogoutReq ()
{
   m_nPacketID   = IVCP_ID_Conn_LogoutReq;
   m_nLogoutType = 0;
}
int CIVCPConnLogoutReq::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Logout"))
   {
      xmlNode.Attribute (TYPE_ID (int), "Type", &m_nLogoutType);
      xmlNode.End ();
      return (DONE);
   }

   return (1);
}
int CIVCPConnLogoutReq::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Logout"))
   {
      xmlNode.Attribute (TYPE_ID (int), "Type", &m_nLogoutType);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnLogoutRes
//
///////////////////////////////////////////////////////////////////////////////

CIVCPConnLogoutRes::CIVCPConnLogoutRes ()
{
   m_nPacketID     = IVCP_ID_Conn_LogoutRes;
   m_nLogoutResult = 0;
}
int CIVCPConnLogoutRes::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Logout"))
   {
      xmlNode.Attribute (TYPE_ID (int), "Result", &m_nLogoutResult);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPConnLogoutRes::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Logout"))
   {
      xmlNode.Attribute (TYPE_ID (int), "Result", &m_nLogoutResult);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnCameraListReq : 카메라 목록 정보를 얻어온다.
//
///////////////////////////////////////////////////////////////////////////////

CIVCPConnCameraListReq::CIVCPConnCameraListReq ()
{
   m_nPacketID = IVCP_ID_Conn_CameraListReq;
   m_nStart    = 0;
   m_nCount    = 0;
}
int CIVCPConnCameraListReq::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("CameraList"))
   {
      xmlNode.Attribute (TYPE_ID (int), "Start", &m_nStart);
      xmlNode.Attribute (TYPE_ID (int), "Count", &m_nCount);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPConnCameraListReq::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("CameraList"))
   {
      xmlNode.Attribute (TYPE_ID (int), "Start", &m_nStart);
      xmlNode.Attribute (TYPE_ID (int), "Count", &m_nCount);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// class CIVCPCameraInfo
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraInfo::CIVCPCameraInfo ()
{
   m_nCameraUID       = 0;
   m_strGuid[0]       = '\0';
   m_nCameraNo        = 0;
   m_strCameraName[0] = '\0';
   m_nCameraType      = 0;
   m_nCameraParam     = 0;
   m_nWidth           = 0;
   m_nHeight          = 0;
   m_fFrameRate       = 0.0f;
   m_nVStreamNum      = 0;
   m_nAStreamNum      = 0;
   for (int i=0; i<MAX_VSTREAMS; i++)
   {
      m_nVideoCodec[i]  = 0;
      m_nQuality[i]     = 0;
      m_nVideoWidth[i]  = 0;
      m_nVideoHeight[i] = 0;
      m_fFrmRate[i]     = 0.0f;
      m_nSendType[i]    = 0;
   }
   for (int i=0; i<MAX_ASTREAMS; i++)
   {
      m_nAudioCodec[i]    = 0;
      m_nChannels[i]      = 0;
      m_nSamplingRate[i]  = 0;
      m_nBitsPerSample[i] = 0;
   }
}
int CIVCPCameraInfo::ReadXML (CXMLIO* pIO)
{
   int i;
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("CamInfo")) 
   {
      StringA strValue;
      xmlNode.Attribute (TYPE_ID (int),     "UID",   &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (StringA), "Guid",  &strValue);
      strcpy(m_strGuid, strValue);
      xmlNode.Attribute (TYPE_ID (int),     "No",    &m_nCameraNo);
      xmlNode.Attribute (TYPE_ID (StringA), "Name",  &strValue); 
      strcpy(m_strCameraName, strValue);
      xmlNode.Attribute (TYPE_ID (int),     "Type",  &m_nCameraType);
      xmlNode.Attribute (TYPE_ID (int),     "Param", &m_nCameraParam);
      xmlNode.Attribute (TYPE_ID (int),     "VW",    &m_nWidth);
      xmlNode.Attribute (TYPE_ID (int),     "VH",    &m_nHeight);
      xmlNode.Attribute (TYPE_ID (float),   "FR",    &m_fFrameRate);
      xmlNode.Attribute (TYPE_ID (int),     "VNum",  &m_nVStreamNum);
      xmlNode.Attribute (TYPE_ID (int),     "ANum",  &m_nAStreamNum);

      StringA strStreamIndex;
      StringA strStreamDesc;
      if (m_nVStreamNum > MAX_VSTREAMS) m_nVStreamNum = MAX_VSTREAMS; // 제한
      for (i=0; i<m_nVStreamNum; i++) 
      {
         strStreamIndex.Format("VDesc%d", i+1);
         xmlNode.Attribute (TYPE_ID (StringA), strStreamIndex, &strStreamDesc);
         ParseVideoStreamDesc(i, strStreamDesc);
      }
      if (m_nAStreamNum > MAX_ASTREAMS) m_nAStreamNum = MAX_ASTREAMS; // 제한
      for (i=0; i<m_nAStreamNum; i++) 
      {
         strStreamIndex.Format("ADesc%d", i+1);
         xmlNode.Attribute (TYPE_ID (StringA), strStreamIndex, &strStreamDesc);
         ParseAudioStreamDesc(i, strStreamDesc);
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPCameraInfo::WriteXML (CXMLIO* pIO)
{
   int i;
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("CamInfo")) 
   {
      StringA strValue;
      xmlNode.Attribute (TYPE_ID (int),     "UID",   &m_nCameraUID);
      strValue = m_strGuid;
      xmlNode.Attribute (TYPE_ID (StringA), "Guid",  &strValue);
      xmlNode.Attribute (TYPE_ID (int),     "No",    &m_nCameraNo);
      strValue = m_strCameraName;
      xmlNode.Attribute (TYPE_ID (StringA), "Name",  &strValue);      
      xmlNode.Attribute (TYPE_ID (int),     "Type",  &m_nCameraType);
      xmlNode.Attribute (TYPE_ID (int),     "Param", &m_nCameraParam);
      xmlNode.Attribute (TYPE_ID (int),     "VW",    &m_nWidth);
      xmlNode.Attribute (TYPE_ID (int),     "VH",    &m_nHeight);
      xmlNode.Attribute (TYPE_ID (float),   "FR",    &m_fFrameRate);
      xmlNode.Attribute (TYPE_ID (int),     "VNum",  &m_nVStreamNum);
      xmlNode.Attribute (TYPE_ID (int),     "ANum",  &m_nAStreamNum);

      StringA strStreamIndex;
      StringA strStreamDesc;
      for (i=0; i<m_nVStreamNum; i++) 
      {
         MakeVideoStreamDesc(i, strStreamDesc);
         strStreamIndex.Format("VDesc%d", i+1);
         xmlNode.Attribute (TYPE_ID (StringA), strStreamIndex, &strStreamDesc);
      }
      for (i=0; i<m_nAStreamNum; i++) 
      {
         MakeAudioStreamDesc(i, strStreamDesc);
         strStreamIndex.Format("ADesc%d", i+1);
         xmlNode.Attribute (TYPE_ID (StringA), strStreamIndex, &strStreamDesc);
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
void CIVCPCameraInfo::ParseVideoStreamDesc (int nIdx, StringA &strDesc)
{
   char resString[200];
   strcpy(resString, strDesc);
   int nCount = 0;
   int i = 0;
   int nLen = (int)strlen(resString);
   while ((i < nLen) && (nCount < 6))
   {
      while ((i < nLen) && (resString[i] == ' ')) i++;
      switch (nCount)
      {
      case 0: // Codec
         m_nVideoCodec[nIdx] = atoi(&resString[i]); 
         break;
      case 1: // Quality
         m_nQuality[nIdx] = atoi(&resString[i]); 
         break;
      case 2: // Width
         m_nVideoWidth[nIdx] = atoi(&resString[i]); 
         break;
      case 3: // Height
         m_nVideoHeight[nIdx] = atoi(&resString[i]); 
         break;
      case 4: // FrameRate
         m_fFrmRate[nIdx] = (float)atof(&resString[i]); 
         break;
      case 5: // SendType
         m_nSendType[nIdx] = atoi(&resString[i]); 
         break;
      }
      nCount++;
      while ((i < nLen) && (resString[i] != ' ')) i++;
      if (resString[i] != ' ')
         break;
   }
}
void CIVCPCameraInfo::ParseAudioStreamDesc (int nIdx, StringA &strDesc)
{
   char resString[200];
   strcpy(resString, strDesc);
   int nCount = 0;
   int i = 0;
   int nLen = (int)strlen(resString);
   while ((i < nLen) && (nCount < 4))
   {
      while ((i < nLen) && (resString[i] == ' ')) i++;
      switch (nCount)
      {
      case 0: // Codec
         m_nAudioCodec[nIdx] = atoi(&resString[i]); 
         break;
      case 1: // Channel
         m_nChannels[nIdx] = atoi(&resString[i]); 
         break;
      case 2: // SamplingRate
         m_nSamplingRate[nIdx] = atoi(&resString[i]); 
         break;
      case 3: // BitsPerSample
         m_nBitsPerSample[nIdx] = atoi(&resString[i]); 
         break;
      }
      nCount++;
      while ((i < nLen) && (resString[i] != ' ')) i++;
      if (resString[i] != ' ')
         break;
   }
}
void CIVCPCameraInfo::MakeVideoStreamDesc (int nIdx, StringA &strDesc)
{
   // "0 50 320 240 7.0 0.5" (Codec Quality Width Height FrameRate ReductionFactor)
   strDesc.Format("%d %d %d %d %f %d", m_nVideoCodec[nIdx], m_nQuality[nIdx], m_nVideoWidth[nIdx], m_nVideoHeight[nIdx], m_fFrmRate[nIdx], m_nSendType[nIdx]);
}
void CIVCPCameraInfo::MakeAudioStreamDesc (int nIdx, StringA &strDesc)
{
   // "0 2 44100 16" (Codec Channel SamplingRate BitsPerSample)
   strDesc.Format("%d %d %d %d", m_nAudioCodec[nIdx], m_nChannels[nIdx], m_nSamplingRate[nIdx], m_nBitsPerSample[nIdx]);
}

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnSystemInfoResponse
//
///////////////////////////////////////////////////////////////////////////////

CIVCPConnCameraListRes::CIVCPConnCameraListRes ()
{
   m_nPacketID = IVCP_ID_Conn_CameraListRes;
   m_nStart    = 0;   // 읽어온 카메라 인덱스 (0: 전부)
   m_nCount    = 0;   // 읽어온 카메라 개수
}
CIVCPConnCameraListRes::~CIVCPConnCameraListRes ()
{
   ClearCameras ();
}
int CIVCPConnCameraListRes::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("CameraList")) 
   {
      xmlNode.Attribute (TYPE_ID (int), "Start", &m_nStart);
      xmlNode.Attribute (TYPE_ID (int), "Count", &m_nCount);
      ClearCameras ();
      for (int i=0; i<m_nCount; i++) 
      {
         CIVCPCameraInfo* pCameraInfo = new CIVCPCameraInfo ();
         pCameraInfo->ReadXML (pIO);
         m_CameraList.push_back (pCameraInfo);
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPConnCameraListRes::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("CameraList")) 
   {
      xmlNode.Attribute (TYPE_ID (int), "Start", &m_nStart);
      xmlNode.Attribute (TYPE_ID (int), "Count", &m_nCount);
      for (int i=0; i<m_nCount; i++)
      {
         m_CameraList[i]->WriteXML (pIO);
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
void CIVCPConnCameraListRes::ClearCameras ()
{
   int nCameraNum = (int)m_CameraList.size (   );
   for (int i=0; i<nCameraNum; i++) 
   {
      CIVCPCameraInfo* pCameraInfo = m_CameraList[i];
      if (pCameraInfo) delete pCameraInfo;
   }
   m_CameraList.clear (   );
}

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnStartUpReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPConnStartUpReq::CIVCPConnStartUpReq ()
{
   m_nPacketID   = IVCP_ID_Conn_StartUpReq;
   m_nStreamFlag = 0;
   m_nMetaFlag   = 0;
   m_nOptions    = 0;
}
int CIVCPConnStartUpReq::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("StartUp"))
   {
      xmlNode.Attribute (TYPE_ID (int), "StreamFlag", &m_nStreamFlag);
      xmlNode.Attribute (TYPE_ID (int), "MetaFlag",   &m_nMetaFlag);
      xmlNode.Attribute (TYPE_ID (int), "Options",    &m_nOptions);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPConnStartUpReq::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("StartUp"))
   {
      xmlNode.Attribute (TYPE_ID (int), "StreamFlag", &m_nStreamFlag);
      xmlNode.Attribute (TYPE_ID (int), "MetaFlag",   &m_nMetaFlag);
      xmlNode.Attribute (TYPE_ID (int), "Options",    &m_nOptions);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnStartUpRes
// 
///////////////////////////////////////////////////////////////////////////////

CIVCPConnStartUpRes::CIVCPConnStartUpRes()
{
   m_nPacketID = IVCP_ID_Conn_StartUpRes;
   m_nResult   = 0;
}
int CIVCPConnStartUpRes::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("StartUp"))
   {
      xmlNode.Attribute (TYPE_ID (int), "Result", &m_nResult);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPConnStartUpRes::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("StartUp"))
   {
      xmlNode.Attribute (TYPE_ID (int), "Result", &m_nResult);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnShutDownReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPConnShutDownReq::CIVCPConnShutDownReq ()
{
   m_nPacketID   = IVCP_ID_Conn_ShutDownReq;
   m_nStreamFlag = 0;
   m_nMetaFlag   = 0;
   m_nOptions    = 0;
}
int CIVCPConnShutDownReq::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("ShutDown"))
   {
      xmlNode.Attribute (TYPE_ID (int), "StreamFlag", &m_nStreamFlag);
      xmlNode.Attribute (TYPE_ID (int), "MetaFlag",   &m_nMetaFlag);
      xmlNode.Attribute (TYPE_ID (int), "Options",    &m_nOptions);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPConnShutDownReq::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("ShutDown"))
   {
      xmlNode.Attribute (TYPE_ID (int), "StreamFlag", &m_nStreamFlag);
      xmlNode.Attribute (TYPE_ID (int), "MetaFlag",   &m_nMetaFlag);
      xmlNode.Attribute (TYPE_ID (int), "Options",    &m_nOptions);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnShutDownRes
// 
///////////////////////////////////////////////////////////////////////////////

CIVCPConnShutDownRes::CIVCPConnShutDownRes()
{
   m_nPacketID = IVCP_ID_Conn_ShutDownRes;
   m_nResult   = 0;
}
int CIVCPConnShutDownRes::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("ShutDown"))
   {
      xmlNode.Attribute (TYPE_ID (int), "Result", &m_nResult);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPConnShutDownRes::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("ShutDown"))
   {
      xmlNode.Attribute (TYPE_ID (int), "Result", &m_nResult);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnSnapshotReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPConnSnapshotReq::CIVCPConnSnapshotReq()
{
   m_nPacketID    = IVCP_ID_Conn_SnapshotReq;
   m_nChannelFlag = 0;
   m_nResizeFlag  = 0;
   m_nWidth       = 0;
   m_nHeight      = 0;
}
int CIVCPConnSnapshotReq::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Snapshot"))
   {
      xmlNode.Attribute (TYPE_ID (UINT64), "ChFlag",     &m_nChannelFlag);
      xmlNode.Attribute (TYPE_ID (int)   , "ResizeFlag", &m_nResizeFlag);
      xmlNode.Attribute (TYPE_ID (int)   , "Width",      &m_nWidth);
      xmlNode.Attribute (TYPE_ID (int)   , "Height",     &m_nHeight);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPConnSnapshotReq::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Snapshot"))
   {
      xmlNode.Attribute (TYPE_ID (UINT64), "ChFlag",     &m_nChannelFlag);
      xmlNode.Attribute (TYPE_ID (int)   , "ResizeFlag", &m_nResizeFlag);
      xmlNode.Attribute (TYPE_ID (int)   , "Width",      &m_nWidth);
      xmlNode.Attribute (TYPE_ID (int)   , "Height",     &m_nHeight);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnSnapshotRes
//
///////////////////////////////////////////////////////////////////////////////

CIVCPConnSnapshotRes::CIVCPConnSnapshotRes () 
{
   m_nPacketID  = IVCP_ID_Conn_SnapshotRes;
   m_nCameraUID = 0;
   m_nCodecID   = 0;
   m_nWidth     = 0;
   m_nHeight    = 0;
}
CIVCPConnSnapshotRes::~CIVCPConnSnapshotRes ()
{
   if (m_pBodyBuffer) free(m_pBodyBuffer);
}
int CIVCPConnSnapshotRes::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Snapshot"))
   {
      StringA strValue;
      xmlNode.Attribute (TYPE_ID (int)     , "UID"     , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int)     , "CodecID" , &m_nCodecID);
      xmlNode.Attribute (TYPE_ID (int)     , "Width"   , &m_nWidth);
      xmlNode.Attribute (TYPE_ID (int)     , "Height"  , &m_nHeight);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPConnSnapshotRes::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Event"))
   {
      StringA strValue;
      xmlNode.Attribute (TYPE_ID (int)     , "UID"     , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int)     , "CodecID" , &m_nCodecID);
      xmlNode.Attribute (TYPE_ID (int)     , "Width"   , &m_nWidth);
      xmlNode.Attribute (TYPE_ID (int)     , "Height"  , &m_nHeight);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
void CIVCPConnSnapshotRes::ParseBodyBuffer ()
{
   FileIO buff;
   buff.Attach ((byte*)m_pBodyBuffer, m_nDataLen);
   CXMLIO xmlIO (&buff, XMLIO_Read);
   if (DONE != ReadXML(&xmlIO)) 
   {
      buff.Detach();
      return;
   }
}
void CIVCPConnSnapshotRes::MakeBodyBuffer (byte *pEncodedBuff, int nEncodedLen)
{
   if (m_pBodyBuffer) delete m_pBodyBuffer;
   m_pBodyBuffer = NULL;
   m_nDataLen = 0;
   m_nBodyLen = 0;

   FileIO buff;
   CXMLIO xmlIO (&buff, XMLIO_Write);
   if (WriteXML(&xmlIO)) return;
   
   m_nDataLen = buff.GetLength();
   m_nBodyLen = m_nDataLen + nEncodedLen;
   
   // 먼저 버퍼를 만든다.
   m_pBodyBuffer = (char*)malloc(m_nDataLen + nEncodedLen);
   if (m_pBodyBuffer)
   {
      memcpy(m_pBodyBuffer, (byte*)buff, m_nDataLen);
      memcpy(m_pBodyBuffer+m_nDataLen, pEncodedBuff, nEncodedLen);
   }
}
int CIVCPConnSnapshotRes::ReadFromPacket (CIVCPPacket& aPacket)
{
   if (aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody) 
   {
      m_nCameraUID = aPacket.m_Header.m_nOptions;
      m_nDataLen = aPacket.m_Header.m_nDataLen;
      m_nBodyLen = aPacket.m_Header.m_nBodyLen;
      m_pBodyBuffer = aPacket.m_pBody;
      aPacket.m_pBody = NULL;

      ParseBodyBuffer(); // 바로 파싱한다.
      return (DONE);
   }
   return (1);
}
int CIVCPConnSnapshotRes::WriteToPacket (CIVCPPacket& aPacket)
{
   if (aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody) 
   {
      aPacket.FreeBody();
   }
   if (m_pBodyBuffer)
   {
      aPacket.m_Header.m_nOptions = m_nCameraUID;
      aPacket.m_Header.m_nDataLen = m_nDataLen;
      aPacket.m_Header.m_nBodyLen = m_nBodyLen;
      aPacket.m_pBody = m_pBodyBuffer;
      m_pBodyBuffer = NULL;
      return (DONE);
   }
   return (1);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPSystemResultRes
// 
///////////////////////////////////////////////////////////////////////////////

CIVCPSystemResultRes::CIVCPSystemResultRes()
{
   m_nPacketID   = 0;
   m_nResponseID = 0;
   m_nResult     = 0;
}
int CIVCPSystemResultRes::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("SystemResult"))
   {
      xmlNode.Attribute (TYPE_ID (int), "ReqID",  &m_nResponseID);
      xmlNode.Attribute (TYPE_ID (int), "Result", &m_nResult);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPSystemResultRes::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("SystemResult"))
   {
      xmlNode.Attribute (TYPE_ID (int), "ResID",  &m_nResponseID);
      xmlNode.Attribute (TYPE_ID (int), "Result", &m_nResult);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraResultRes
// 
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraResultRes::CIVCPCameraResultRes()
{
   m_nPacketID   = 0;
   m_nCameraUID  = 0;
   m_nResponseID = 0;
   m_nResult     = 0;
}
int CIVCPCameraResultRes::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("CameraResult"))
   {
      xmlNode.Attribute (TYPE_ID (int), "UID",    &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int), "ResID",  &m_nResponseID);
      xmlNode.Attribute (TYPE_ID (int), "Result", &m_nResult);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPCameraResultRes::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("CameraResult"))
   {
      xmlNode.Attribute (TYPE_ID (int), "UID",    &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int), "ResID",  &m_nResponseID);
      xmlNode.Attribute (TYPE_ID (int), "Result", &m_nResult);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraLiveMetadataReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraLiveMetadataReq::CIVCPCameraLiveMetadataReq ()
{
   m_nPacketID   = IVCP_ID_Camera_LiveMetadataReq;
   m_nCameraUID  = 0;
   m_nMetaFlag   = 0;
   m_nOptionFlag = 0;
}
int CIVCPCameraLiveMetadataReq::ReadXML  (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("LiveMeta")) 
   {
      xmlNode.Attribute (TYPE_ID (int), "UID",    &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int), "ResID",  &m_nResponseID);
      xmlNode.Attribute (TYPE_ID (int), "Flag",   &m_nMetaFlag);
      xmlNode.Attribute (TYPE_ID (int), "Option", &m_nOptionFlag);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPCameraLiveMetadataReq::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("LiveMeta")) 
   {
      xmlNode.Attribute (TYPE_ID (int), "UID",    &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int), "ResID",  &m_nResponseID);
      xmlNode.Attribute (TYPE_ID (int), "Flag",   &m_nMetaFlag);
      xmlNode.Attribute (TYPE_ID (int), "Option", &m_nOptionFlag);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraLiveVideoReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraLiveVideoReq::CIVCPCameraLiveVideoReq ()
{
   m_nPacketID   = IVCP_ID_Camera_LiveVideoReq;
   m_nCameraUID  = 0;
   m_nStreamFlag = 0;
}
int CIVCPCameraLiveVideoReq::ReadXML  (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("LiveVideo")) 
   {
      xmlNode.Attribute (TYPE_ID (int), "UID",   &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int), "ResID", &m_nResponseID);
      xmlNode.Attribute (TYPE_ID (int), "Flag",  &m_nStreamFlag);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPCameraLiveVideoReq::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("LiveVideo")) 
   {
      xmlNode.Attribute (TYPE_ID (int), "UID",   &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int), "ResID", &m_nResponseID);
      xmlNode.Attribute (TYPE_ID (int), "Flag",  &m_nStreamFlag);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraPlayStreamStartReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraPlayStreamStartReq::CIVCPCameraPlayStreamStartReq ()
{
   m_nPacketID   = IVCP_ID_Camera_PlayStreamStartReq;
   m_nCameraUID  = 0;      // 카메라 UID
   m_nResponseID = 0;
   m_nStreamID   = 0;      // 스트림 콜백 ID
   m_nStreamFlag = 0;      // 전송받을 스트림 플래그
   m_nMetaFlag   = 0;      // 전송받을 메타 플래그
   m_bReverse    = FALSE;  // 재생 방향
   m_fPlaySpeed  = 1.0f;   // 재생 속도 (1/8 ~ 32)
   m_nPlayMode   = 0;

   m_bTranscode = FALSE;   // 트랜스코딩 여부
   m_nCodecID = 0;         // 코덱 ID
   m_nResizeOption = 0;    // 축소 방법 (0:비율, 1:너비기준크기, 2:고정크기)
   m_fResizeRate = 1.0;    // 축소 비율
   m_nWidth = 0;           // 크기
   m_nHeight = 0;          // 높이
   m_nQuality = 50;        // 품질
}
int CIVCPCameraPlayStreamStartReq::ReadXML  (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PlayStart")) 
   {
      xmlNode.Attribute (TYPE_ID (int),      "UID",        &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int),      "ResID",      &m_nResponseID);
      xmlNode.Attribute (TYPE_ID (int),      "StreamID",   &m_nStreamID);
      xmlNode.Attribute (TYPE_ID (int),      "StreamFlag", &m_nStreamFlag);
      xmlNode.Attribute (TYPE_ID (int),      "MetaFlag",   &m_nMetaFlag);
      xmlNode.Attribute (TYPE_ID (FILETIME), "Start",      &m_ftStart);
      xmlNode.Attribute (TYPE_ID (FILETIME), "End",        &m_ftEnd);
      xmlNode.Attribute (TYPE_ID (BOOL),     "Reverse",    &m_bReverse);
      xmlNode.Attribute (TYPE_ID (float),    "Speed",      &m_fPlaySpeed);
      xmlNode.Attribute (TYPE_ID (int),      "Mode",       &m_nPlayMode);

      xmlNode.Attribute (TYPE_ID (BOOL),     "TransCode",  &m_bTranscode);
      xmlNode.Attribute (TYPE_ID (BOOL),     "CodecID",    &m_nCodecID);
      xmlNode.Attribute (TYPE_ID (BOOL),     "ResizeOpt",  &m_nResizeOption);
      xmlNode.Attribute (TYPE_ID (BOOL),     "ResizeRete", &m_fResizeRate);
      xmlNode.Attribute (TYPE_ID (BOOL),     "Width",      &m_nWidth);
      xmlNode.Attribute (TYPE_ID (BOOL),     "Height",     &m_nHeight);
      xmlNode.Attribute (TYPE_ID (BOOL),     "Quality",    &m_nQuality);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPCameraPlayStreamStartReq::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PlayStart")) 
   {
      xmlNode.Attribute (TYPE_ID (int),      "UID",        &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int),      "ResID",      &m_nResponseID);
      xmlNode.Attribute (TYPE_ID (int),      "StreamID",   &m_nStreamID);
      xmlNode.Attribute (TYPE_ID (int),      "StreamFlag", &m_nStreamFlag);
      xmlNode.Attribute (TYPE_ID (int),      "MetaFlag",   &m_nMetaFlag);
      xmlNode.Attribute (TYPE_ID (FILETIME), "Start",      &m_ftStart);
      xmlNode.Attribute (TYPE_ID (FILETIME), "End",        &m_ftEnd);
      xmlNode.Attribute (TYPE_ID (BOOL),     "Reverse",    &m_bReverse);
      xmlNode.Attribute (TYPE_ID (float),    "Speed",      &m_fPlaySpeed);
      xmlNode.Attribute (TYPE_ID (int),      "Mode",       &m_nPlayMode);

      xmlNode.Attribute (TYPE_ID (BOOL),     "TransCode",  &m_bTranscode);
      xmlNode.Attribute (TYPE_ID (int),      "CodecID",    &m_nCodecID);
      xmlNode.Attribute (TYPE_ID (int),      "ResizeOpt",  &m_nResizeOption);
      xmlNode.Attribute (TYPE_ID (float),    "ResizeRete", &m_fResizeRate);
      xmlNode.Attribute (TYPE_ID (int),      "Width",      &m_nWidth);
      xmlNode.Attribute (TYPE_ID (int),      "Height",     &m_nHeight);
      xmlNode.Attribute (TYPE_ID (int),      "Quality",    &m_nQuality);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraPlayStreamStopReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraPlayStreamStopReq::CIVCPCameraPlayStreamStopReq ()
{
   m_nPacketID = IVCP_ID_Camera_PlayStreamStopReq;
   m_nCameraUID = 0;
   m_nStreamID  = 0;
}
int CIVCPCameraPlayStreamStopReq::ReadXML  (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PlayStop")) 
   {
      xmlNode.Attribute (TYPE_ID (int), "UID",      &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int), "ResID",    &m_nResponseID);
      xmlNode.Attribute (TYPE_ID (int), "StreamID", &m_nStreamID);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPCameraPlayStreamStopReq::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PlayStop")) 
   {
      xmlNode.Attribute (TYPE_ID (int), "UID",      &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int), "ResID",    &m_nResponseID);
      xmlNode.Attribute (TYPE_ID (int), "StreamID", &m_nStreamID);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraPlayStreamControlReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraPlayStreamControlReq::CIVCPCameraPlayStreamControlReq ()
{
   m_nPacketID  = IVCP_ID_Camera_PlayStreamControlReq;
   m_nCameraUID = 0;
   m_nStreamID  = 0;
   m_fPlaySpeed = 1.0f;
   m_bReverse = FALSE;
}
int CIVCPCameraPlayStreamControlReq::ReadXML  (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PlayControl")) 
   {
      xmlNode.Attribute (TYPE_ID (int),      "UID",      &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int),      "ResID",    &m_nResponseID);
      xmlNode.Attribute (TYPE_ID (int),      "StreamID", &m_nStreamID);
      xmlNode.Attribute (TYPE_ID (int),      "CtrlFlag", &m_nCtrlFlag);
      xmlNode.Attribute (TYPE_ID (FILETIME), "PlayTime", &m_ftPlayTime);
      xmlNode.Attribute (TYPE_ID (float),    "Speed",    &m_fPlaySpeed);
      xmlNode.Attribute (TYPE_ID (BOOL),     "Reverse",  &m_bReverse);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPCameraPlayStreamControlReq::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PlayControl")) 
   {
      xmlNode.Attribute (TYPE_ID (int),      "UID",      &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int),      "ResID",    &m_nResponseID);
      xmlNode.Attribute (TYPE_ID (int),      "StreamID", &m_nStreamID);
      xmlNode.Attribute (TYPE_ID (int),      "CtrlFlag", &m_nCtrlFlag);
      xmlNode.Attribute (TYPE_ID (FILETIME), "PlayTime", &m_ftPlayTime);
      xmlNode.Attribute (TYPE_ID (float),    "Speed",    &m_fPlaySpeed);
      xmlNode.Attribute (TYPE_ID (BOOL),     "Reverse",  &m_bReverse);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraSnapshotReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraSnapshotReq::CIVCPCameraSnapshotReq ()
{
   m_nPacketID   = IVCP_ID_Camera_SnapshotReq;
   m_nCameraUID  = 0;
   m_nResizeFlag = 0;
   m_nWidth      = 0;
   m_nHeight     = 0;
}
int CIVCPCameraSnapshotReq::ReadXML  (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Snapshot")) 
   {
      xmlNode.Attribute (TYPE_ID (int), "UID",        &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int), "ResID",      &m_nResponseID);
      xmlNode.Attribute (TYPE_ID (int), "ResizeFlag", &m_nResizeFlag);
      xmlNode.Attribute (TYPE_ID (int), "Width",      &m_nWidth);
      xmlNode.Attribute (TYPE_ID (int), "Height",     &m_nHeight);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPCameraSnapshotReq::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Snapshot")) 
   {
      xmlNode.Attribute (TYPE_ID (int), "UID",        &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int), "ResID",      &m_nResponseID);
      xmlNode.Attribute (TYPE_ID (int), "ResizeFlag", &m_nResizeFlag);
      xmlNode.Attribute (TYPE_ID (int), "Width",      &m_nWidth);
      xmlNode.Attribute (TYPE_ID (int), "Height",     &m_nHeight);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraSnapshotRes
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraSnapshotRes::CIVCPCameraSnapshotRes () 
{
   m_nPacketID  = IVCP_ID_Camera_SnapshotRes;
   m_nCameraUID = 0;
   m_nCodecID   = 0;
   m_nWidth     = 0;
   m_nHeight    = 0;
}
CIVCPCameraSnapshotRes::~CIVCPCameraSnapshotRes ()
{
   if (m_pBodyBuffer) free(m_pBodyBuffer);
}
int CIVCPCameraSnapshotRes::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Snapshot"))
   {
      StringA strValue;
      xmlNode.Attribute (TYPE_ID (int)     , "UID"     , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int)     , "ResID"   , &m_nResponseID);
      xmlNode.Attribute (TYPE_ID (int)     , "CodecID" , &m_nCodecID);
      xmlNode.Attribute (TYPE_ID (int)     , "Width"   , &m_nWidth);
      xmlNode.Attribute (TYPE_ID (int)     , "Height"  , &m_nHeight);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPCameraSnapshotRes::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Event"))
   {
      StringA strValue;
      xmlNode.Attribute (TYPE_ID (int)     , "UID"     , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int)     , "ResID"   , &m_nResponseID);
      xmlNode.Attribute (TYPE_ID (int)     , "CodecID" , &m_nCodecID);
      xmlNode.Attribute (TYPE_ID (int)     , "Width"   , &m_nWidth);
      xmlNode.Attribute (TYPE_ID (int)     , "Height"  , &m_nHeight);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
void CIVCPCameraSnapshotRes::ParseBodyBuffer ()
{
   FileIO buff;
   buff.Attach ((byte*)m_pBodyBuffer, m_nDataLen);
   CXMLIO xmlIO (&buff, XMLIO_Read);
   if (DONE != ReadXML(&xmlIO)) 
   {
      buff.Detach();
      return;
   }
}
void CIVCPCameraSnapshotRes::MakeBodyBuffer (byte *pEncodedBuff, int nEncodedLen)
{
   if (m_pBodyBuffer) delete m_pBodyBuffer;
   m_pBodyBuffer = NULL;
   m_nDataLen = 0;
   m_nBodyLen = 0;

   FileIO buff;
   CXMLIO xmlIO (&buff, XMLIO_Write);
   if (WriteXML(&xmlIO)) return;
   
   m_nDataLen = buff.GetLength();
   m_nBodyLen = m_nDataLen + nEncodedLen;
   
   // 먼저 버퍼를 만든다.
   m_pBodyBuffer = (char*)malloc(m_nDataLen + nEncodedLen);
   if (m_pBodyBuffer)
   {
      memcpy(m_pBodyBuffer, (byte*)buff, m_nDataLen);
      memcpy(m_pBodyBuffer+m_nDataLen, pEncodedBuff, nEncodedLen);
   }
}
int CIVCPCameraSnapshotRes::ReadFromPacket (CIVCPPacket& aPacket)
{
   if (aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody) 
   {
      m_nCameraUID  = aPacket.m_Header.m_nOptions;
      m_nDataLen    = aPacket.m_Header.m_nDataLen;
      m_nBodyLen    = aPacket.m_Header.m_nBodyLen;
      m_pBodyBuffer = aPacket.m_pBody;
      aPacket.m_pBody = NULL;

      ParseBodyBuffer(); // 바로 파싱한다.
      return (DONE);
   }
   return (1);
}
int CIVCPCameraSnapshotRes::WriteToPacket (CIVCPPacket& aPacket)
{
   if (aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody) 
   {
      aPacket.FreeBody();
   }
   if (m_pBodyBuffer)
   {
      aPacket.m_Header.m_nOptions = m_nCameraUID;
      aPacket.m_Header.m_nDataLen = m_nDataLen;
      aPacket.m_Header.m_nBodyLen = m_nBodyLen;
      aPacket.m_pBody = m_pBodyBuffer;
      m_pBodyBuffer = NULL;
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraSettingReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraSettingReq::CIVCPCameraSettingReq ()
{
   m_nPacketID    = IVCP_ID_Camera_SettingReq;
   m_nCameraUID   = 0;
   m_nSettingMask = 0;
   m_nSettingFlag = 0;
}
int CIVCPCameraSettingReq::ReadXML  (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Setting")) 
   {
      xmlNode.Attribute (TYPE_ID (int)   , "UID",   &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int)   , "ResID", &m_nResponseID);
      xmlNode.Attribute (TYPE_ID (xint64), "Mask",  &m_nSettingMask);
      xmlNode.Attribute (TYPE_ID (xint64), "Flag",  &m_nSettingFlag);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPCameraSettingReq::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Setting")) 
   {
      xmlNode.Attribute (TYPE_ID (int)   , "UID",   &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int)   , "ResID", &m_nResponseID);
      xmlNode.Attribute (TYPE_ID (xint64), "Mask",  &m_nSettingMask);
      xmlNode.Attribute (TYPE_ID (xint64), "Flag",  &m_nSettingFlag);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraCommandReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraCommandReq::CIVCPCameraCommandReq ()
{
   m_nPacketID    = IVCP_ID_Camera_CommandReq;
   m_nCameraUID   = 0;
   m_nCommand     = 0;
   m_nParam1      = 0;
   m_nParam2      = 0;
}
int CIVCPCameraCommandReq::ReadXML  (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Command")) 
   {
      xmlNode.Attribute (TYPE_ID (int), "UID"    , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int), "ResID"  , &m_nResponseID);
      xmlNode.Attribute (TYPE_ID (int), "Command", &m_nCommand);
      xmlNode.Attribute (TYPE_ID (int), "Param1" , &m_nParam1);
      xmlNode.Attribute (TYPE_ID (int), "Param2" , &m_nParam2);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPCameraCommandReq::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Command")) 
   {
      xmlNode.Attribute (TYPE_ID (int), "UID"    , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int), "ResID"  , &m_nResponseID);
      xmlNode.Attribute (TYPE_ID (int), "Command", &m_nCommand);
      xmlNode.Attribute (TYPE_ID (int), "Param1" , &m_nParam1);
      xmlNode.Attribute (TYPE_ID (int), "Param2" , &m_nParam2);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraAudioInOutReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraTalkAudioReq::CIVCPCameraTalkAudioReq ()
{
   m_nPacketID  = IVCP_ID_Camera_TalkAudioReq;
   m_nCameraUID = 0;
   m_nTalkID = 0;
   m_nAudioFlag = 0;
}
int CIVCPCameraTalkAudioReq::ReadXML  (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("TalkAudio")) 
   {
      xmlNode.Attribute (TYPE_ID (int), "UID",   &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int), "ResID", &m_nResponseID);
      xmlNode.Attribute (TYPE_ID (int), "TID",   &m_nTalkID);
      xmlNode.Attribute (TYPE_ID (int), "Flag",  &m_nAudioFlag);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPCameraTalkAudioReq::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("TalkAudio")) 
   {
      xmlNode.Attribute (TYPE_ID (int), "UID",   &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int), "ResID", &m_nResponseID);
      xmlNode.Attribute (TYPE_ID (int), "TID",   &m_nTalkID);
      xmlNode.Attribute (TYPE_ID (int), "Flag",  &m_nAudioFlag);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraTalkStatus
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraTalkStatus::CIVCPCameraTalkStatus ()
{
   m_nPacketID  = IVCP_ID_Camera_TalkStatus;
   m_nCameraUID = 0;
   m_nStatus = 0;
}
int CIVCPCameraTalkStatus::ReadXML  (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("TalkStatus")) 
   {
      xmlNode.Attribute (TYPE_ID (int), "UID",    &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int), "TID",    &m_nTalkID);
      xmlNode.Attribute (TYPE_ID (int), "Status", &m_nStatus);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPCameraTalkStatus::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("TalkStatus")) 
   {
      xmlNode.Attribute (TYPE_ID (int), "UID",    &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int), "TID",    &m_nTalkID);
      xmlNode.Attribute (TYPE_ID (int), "Status", &m_nStatus);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (DATA) CIVCPCameraSendAudioData
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraSendAudioData::CIVCPCameraSendAudioData () 
{
   m_nPacketID = IVCP_ID_Camera_SendAudioData;
   m_nCameraUID = 0;
   m_nTalkID = 0;
   m_nCodecID = 0;
   m_nChannel = 1;
   m_nBitPerSamples = 16;
   m_nSamplingRate = 8000;
   m_pBodyBuffer = NULL;
}
CIVCPCameraSendAudioData::~CIVCPCameraSendAudioData ()
{
   if (m_pBodyBuffer) free(m_pBodyBuffer);
}
void CIVCPCameraSendAudioData::ParseBodyBuffer ()
{
   char* resString = m_pBodyBuffer;
   int nLen = m_nDataLen - 1; // 마지막은 '\0';
   int nCount = 0;
   int i = 0;
   while ((i < nLen) && (nCount < 5))
   {
      while ((i < nLen) && (resString[i] == ' ')) i++;
      switch (nCount)
      {
      case 0: // TalkID
         m_nTalkID = atoi(&resString[i]); 
         break;
      case 1: // Codec
         m_nCodecID = atoi(&resString[i]); 
         break;
      case 2: // Channel
         m_nChannel = atoi(&resString[i]); 
         break;
      case 3: // BitPerSamples
         m_nBitPerSamples = atoi(&resString[i]); 
         break;
      case 4: // SamplingRate
         m_nSamplingRate = atoi(&resString[i]); 
         break;
      }
      nCount++;
      while ((i < nLen) && (resString[i] != ' ')) i++;
      if (resString[i] != ' ')
         break;
   }
}
void CIVCPCameraSendAudioData::MakeBodyBuffer (byte *pEncodedBuff, int nEncodedLen)
{
   // "0 0 1 16 44100" (TalkID Codec Channel BitPerSamples SamplingRate)
   StringA strDesc;
   strDesc.Format("%d %d %d %d %d", m_nTalkID, m_nCodecID, m_nChannel, m_nBitPerSamples, m_nSamplingRate);

   m_nDataLen = strDesc.GetStringLength() + 1; // '\0' 추가
   m_nBodyLen = m_nDataLen + nEncodedLen;
   
   // 먼저 버퍼를 만든다.
   if (m_pBodyBuffer) delete m_pBodyBuffer;
   m_pBodyBuffer = (char*)malloc(m_nDataLen + nEncodedLen);
   if (m_pBodyBuffer)
   {
      memcpy(m_pBodyBuffer, (void*)strDesc, m_nDataLen);
      m_pBodyBuffer[m_nDataLen-1] = '\0';
      memcpy(m_pBodyBuffer+m_nDataLen, pEncodedBuff, nEncodedLen);
   }
}
int CIVCPCameraSendAudioData::ReadFromPacket (CIVCPPacket& aPacket)
{
   if (aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody) 
   {
      m_nCameraUID  = aPacket.m_Header.m_nOptions;
      m_nDataLen    = aPacket.m_Header.m_nDataLen;
      m_nBodyLen    = aPacket.m_Header.m_nBodyLen;
      m_pBodyBuffer = aPacket.m_pBody;
      aPacket.m_pBody = NULL;

      ParseBodyBuffer(); // 바로 파싱한다.
      return (DONE);
   }
   return (1);
}
int CIVCPCameraSendAudioData::WriteToPacket (CIVCPPacket& aPacket)
{
   if (aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody) 
   {
      aPacket.FreeBody();
   }
   if (m_pBodyBuffer)
   {
      aPacket.m_Header.m_nOptions = m_nCameraUID;
      aPacket.m_Header.m_nDataLen = m_nDataLen;
      aPacket.m_Header.m_nBodyLen = m_nBodyLen;
      aPacket.m_pBody = m_pBodyBuffer;
      m_pBodyBuffer = NULL;
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (PTZ) CIVCPCameraPtzContMoveReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraPtzContMoveReq::CIVCPCameraPtzContMoveReq()
{
   m_nPacketID  = IVCP_ID_Ptz_ContMoveReq;
   m_nCameraUID = 0;
   m_fPan   = IVCP_PTZ_NO_CMD;
   m_fTilt  = IVCP_PTZ_NO_CMD;
   m_fZoom  = IVCP_PTZ_NO_CMD;
   m_fFocus = IVCP_PTZ_NO_CMD;
}
int CIVCPCameraPtzContMoveReq::ReadXML  (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PtzContMove")) 
   {
      xmlNode.Attribute (TYPE_ID (int),   "UID",   &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int),   "ResID", &m_nResponseID);
      xmlNode.Attribute (TYPE_ID (float), "Pan",   &m_fPan);
      xmlNode.Attribute (TYPE_ID (float), "Tilt",  &m_fTilt);
      xmlNode.Attribute (TYPE_ID (float), "Zoom",  &m_fZoom);
      xmlNode.Attribute (TYPE_ID (float), "Focus", &m_fFocus);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPCameraPtzContMoveReq::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PtzContMove")) 
   {
      xmlNode.Attribute (TYPE_ID (int),   "UID",   &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int),   "ResID", &m_nResponseID);
      xmlNode.Attribute (TYPE_ID (float), "Pan",   &m_fPan);
      xmlNode.Attribute (TYPE_ID (float), "Tilt",  &m_fTilt);
      xmlNode.Attribute (TYPE_ID (float), "Zoom",  &m_fZoom);
      xmlNode.Attribute (TYPE_ID (float), "Focus", &m_fFocus);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (PTZ) CIVCPCameraPtzAbsMoveReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraPtzAbsMoveReq::CIVCPCameraPtzAbsMoveReq()
{
   m_nPacketID  = IVCP_ID_Ptz_AbsMoveReq;
   m_nCameraUID = 0;
   m_fPan   = IVCP_PTZ_NO_CMD;
   m_fTilt  = IVCP_PTZ_NO_CMD;
   m_fZoom  = IVCP_PTZ_NO_CMD;
   m_fFocus = IVCP_PTZ_NO_CMD;
}
int CIVCPCameraPtzAbsMoveReq::ReadXML  (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PtzAbsMove")) 
   {
      xmlNode.Attribute (TYPE_ID (int),   "UID",   &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int),   "ResID", &m_nResponseID);
      xmlNode.Attribute (TYPE_ID (float), "Pan",   &m_fPan);
      xmlNode.Attribute (TYPE_ID (float), "Tilt",  &m_fTilt);
      xmlNode.Attribute (TYPE_ID (float), "Zoom",  &m_fZoom);
      xmlNode.Attribute (TYPE_ID (float), "Focus", &m_fFocus);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPCameraPtzAbsMoveReq::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PtzAbsMove")) 
   {
      xmlNode.Attribute (TYPE_ID (int),   "UID",   &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int),   "ResID", &m_nResponseID);
      xmlNode.Attribute (TYPE_ID (float), "Pan",   &m_fPan);
      xmlNode.Attribute (TYPE_ID (float), "Tilt",  &m_fTilt);
      xmlNode.Attribute (TYPE_ID (float), "Zoom",  &m_fZoom);
      xmlNode.Attribute (TYPE_ID (float), "Focus", &m_fFocus);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (PTZ) CIVCPCameraPtzBoxMoveReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraPtzBoxMoveReq::CIVCPCameraPtzBoxMoveReq()
{
   m_nCameraUID = 0;
   m_fLeft  = 0.0f;
   m_fTop   = 0.0f;
   m_fWidth = 0.0f;
   m_fHeight = 0.0f;
}
int CIVCPCameraPtzBoxMoveReq::ReadXML  (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PtzBoxMove")) 
   {
      xmlNode.Attribute (TYPE_ID (int),   "UID",    &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int),   "ResID",  &m_nResponseID);
      xmlNode.Attribute (TYPE_ID (float), "Left",   &m_fLeft);
      xmlNode.Attribute (TYPE_ID (float), "Top",    &m_fTop);
      xmlNode.Attribute (TYPE_ID (float), "Width",  &m_fWidth);
      xmlNode.Attribute (TYPE_ID (float), "Height", &m_fHeight);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPCameraPtzBoxMoveReq::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PtzBoxMove")) 
   {
      xmlNode.Attribute (TYPE_ID (int),   "UID",    &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int),   "ResID",  &m_nResponseID);
      xmlNode.Attribute (TYPE_ID (float), "Left",   &m_fLeft);
      xmlNode.Attribute (TYPE_ID (float), "Top",    &m_fTop);
      xmlNode.Attribute (TYPE_ID (float), "Width",  &m_fWidth);
      xmlNode.Attribute (TYPE_ID (float), "Height", &m_fHeight);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (PTZ) CIVCPCameraPtzGetAbsPosReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraPtzGetAbsPosReq::CIVCPCameraPtzGetAbsPosReq()
{
   m_nPacketID   = IVCP_ID_Ptz_GetAbsPosReq;
   m_nCameraUID  = 0;
   m_nResponseID = 0;
}
int CIVCPCameraPtzGetAbsPosReq::ReadXML  (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PtzGetAbsPos")) 
   {
      xmlNode.Attribute (TYPE_ID (int),   "UID",   &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int),   "ResID", &m_nResponseID);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPCameraPtzGetAbsPosReq::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PtzGetAbsPos")) 
   {
      xmlNode.Attribute (TYPE_ID (int),   "UID",   &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int),   "ResID", &m_nResponseID);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (PTZ) CIVCPCameraPtzGetAbsPosRes
//
///////////////////////////////////////////////////////////////////////////////

CIVCPCameraPtzGetAbsPosRes::CIVCPCameraPtzGetAbsPosRes()
{
   m_nPacketID   = IVCP_ID_Ptz_GetAbsPosRes;
   m_nCameraUID  = 0;
   m_nResponseID = 0;
   m_fPanAngle   = 0.0f;   // Pan 각도 : 반시계 방향이 증가하는 방향이다. 단위(degree, 0~360도)
   m_fTiltAngle  = 0.0f;   // Tilt 각도 : 아래로 향하는 방향이 증가하는 방향임. 단위(degree, 0~360도)
   m_fZoomFactor = 1.0f;   // Zoom 값 : 현재 줌 위치를 나타냄. 18배줌 카메라이면 1~18사이의 값을 가짐.
   m_fFOV        = 60.0f;  // FOV (Field Of View) : Pan 축으로의 화각을 나타냄. 단위(degree, 0~360도)
}
int CIVCPCameraPtzGetAbsPosRes::ReadXML  (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PtzAbsPos")) 
   {
      xmlNode.Attribute (TYPE_ID (int),      "UID",   &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int),      "ResID", &m_nResponseID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "Time",  &m_ftFrameTime);
      xmlNode.Attribute (TYPE_ID (float),    "Pan",   &m_fPanAngle);
      xmlNode.Attribute (TYPE_ID (float),    "Tilt",  &m_fTiltAngle);
      xmlNode.Attribute (TYPE_ID (float),    "Zoom",  &m_fZoomFactor);
      xmlNode.Attribute (TYPE_ID (float),    "FOV",   &m_fFOV);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPCameraPtzGetAbsPosRes::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PtzAbsPos")) 
   {
      xmlNode.Attribute (TYPE_ID (int),      "UID",   &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int),      "ResID", &m_nResponseID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "Time",  &m_ftFrameTime);
      xmlNode.Attribute (TYPE_ID (float),    "Pan",   &m_fPanAngle);
      xmlNode.Attribute (TYPE_ID (float),    "Tilt",  &m_fTiltAngle);
      xmlNode.Attribute (TYPE_ID (float),    "Zoom",  &m_fZoomFactor);
      xmlNode.Attribute (TYPE_ID (float),    "FOV",   &m_fFOV);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLiveFrameMetaInfo
//
///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// CIVCPFrameEvent

CIVCPFrameEvent::CIVCPFrameEvent ()
{
   m_nEventID   = 0;
   m_nEventType = 0;
}
int CIVCPFrameEvent::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Event"))
   {
      xmlNode.Attribute (TYPE_ID (int), "ID"  , &m_nEventID);
      xmlNode.Attribute (TYPE_ID (int), "Type", &m_nEventType);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPFrameEvent::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Event"))
   {
      xmlNode.Attribute (TYPE_ID (int), "ID"  , &m_nEventID);
      xmlNode.Attribute (TYPE_ID (int), "Type", &m_nEventType);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

/////////////////////////////////////////////////////////////////////
// CIVCPFrameObject

CIVCPFrameObject::CIVCPFrameObject ()
{
   m_nObjectID   = 0;
   m_nObjectType = 0;
   m_pEvents     = NULL;
   m_nEventNum   = 0;
}
CIVCPFrameObject::~CIVCPFrameObject ()
{
   ClearEvents ();
}
int CIVCPFrameObject::ReadXML (CXMLIO* pIO)
{
   int nEventNum = 0;
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Object"))
   {
      xmlNode.Attribute (TYPE_ID (int)   , "ID"    , &m_nObjectID);
      xmlNode.Attribute (TYPE_ID (int)   , "Type"  , &m_nObjectType);
      xmlNode.Attribute (TYPE_ID (IBox2D), "Box"   , &m_ObjectBox);
      xmlNode.Attribute (TYPE_ID (int)   , "EvtNum", &nEventNum);
      ClearEvents ();
      m_nEventNum = 0;
      if (nEventNum > 0)
      {
         m_pEvents = new CIVCPFrameEvent[nEventNum];
         for (m_nEventNum=0; m_nEventNum<nEventNum; m_nEventNum++)
         {
            if (m_pEvents[m_nEventNum].ReadXML(pIO) != DONE)
               break;
         }
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPFrameObject::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Object"))
   {
      xmlNode.Attribute (TYPE_ID (int)   , "ID"    , &m_nObjectID);
      xmlNode.Attribute (TYPE_ID (int)   , "Type"  , &m_nObjectType);
      xmlNode.Attribute (TYPE_ID (IBox2D), "Box"   , &m_ObjectBox);
      xmlNode.Attribute (TYPE_ID (int)   , "EvtNum", &m_nEventNum);
      if (m_pEvents)
      {
         for (int i=0; i<m_nEventNum; i++)
         {
            m_pEvents[i].WriteXML(pIO);
         }
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
void CIVCPFrameObject::ClearEvents ()
{
   if (m_pEvents)
   {
      delete []m_pEvents;
      m_pEvents = NULL;
   }
}

/////////////////////////////////////////////////////////////////////
// CIVCPLiveFrameInfo

CIVCPLiveFrameInfo::CIVCPLiveFrameInfo ()
{
   m_nPacketID   = IVCP_ID_Live_FrameInfo;
   m_nCameraUID  = 0;
   m_nObjectNum  = 0;
   m_pObjects    = NULL;
}

CIVCPLiveFrameInfo::~CIVCPLiveFrameInfo ()
{
   ClearObjects ();
}

int CIVCPLiveFrameInfo::ReadXML (CXMLIO* pIO)
{
   int nObjectNum = 0;
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Frame"))
   {
      xmlNode.Attribute (TYPE_ID (int)     , "UID"    , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "FrmTime", &m_ftFrameTime);
      xmlNode.Attribute (TYPE_ID (int)     , "ObjNum" , &nObjectNum);

      xmlNode.Attribute (TYPE_ID (xint)   , "CamState"  , &m_nCameraState);
      xmlNode.Attribute (TYPE_ID (xint)   , "PTZState"  , &m_nPTZState);
      xmlNode.Attribute (TYPE_ID (xint)   , "PTZStateEx", &m_nPTZStateEx);
      xmlNode.Attribute (TYPE_ID (float)  , "FrameRate" , &m_fFrameRate);
      xmlNode.Attribute (TYPE_ID (int)    , "Width"     , &m_nProcWidth);
      xmlNode.Attribute (TYPE_ID (int)    , "Height"    , &m_nProcHeight);

      ClearObjects ();
      m_nObjectNum = 0;
      if (nObjectNum > 0)
      {
         m_pObjects = new CIVCPFrameObject[nObjectNum];
         for (m_nObjectNum=0; m_nObjectNum<nObjectNum; m_nObjectNum++)
         {
            if (m_pObjects[m_nObjectNum].ReadXML(pIO) != DONE)
               break;
         }
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPLiveFrameInfo::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Frame"))
   {
      xmlNode.Attribute (TYPE_ID (int)     , "UID"    , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "FrmTime", &m_ftFrameTime);
      xmlNode.Attribute (TYPE_ID (int)     , "ObjNum" , &m_nObjectNum);

      xmlNode.Attribute (TYPE_ID (xint)   , "CamState"  , &m_nCameraState);
      xmlNode.Attribute (TYPE_ID (xint)   , "PTZState"  , &m_nPTZState);
      xmlNode.Attribute (TYPE_ID (xint)   , "PTZStateEx", &m_nPTZStateEx);
      xmlNode.Attribute (TYPE_ID (float)  , "FrameRate" , &m_fFrameRate);
      xmlNode.Attribute (TYPE_ID (int)    , "Width"     , &m_nProcWidth);
      xmlNode.Attribute (TYPE_ID (int)    , "Height"    , &m_nProcHeight);

      if (m_pObjects)
      {
         for (int i=0; i<m_nObjectNum; i++)
         {
            m_pObjects[i].WriteXML(pIO);
         }
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
void CIVCPLiveFrameInfo::ClearObjects ()
{
   if (m_pObjects)
   {
      delete []m_pObjects;
      m_pObjects = NULL;
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLiveFrameMetaInfo
//
///////////////////////////////////////////////////////////////////////////////

CIVCPFrameObjectEx::CIVCPFrameObjectEx ()
{
   m_nObjectID   = 0;
   m_nObjectType = 0;
   m_pEvents     = NULL;
   m_nEventNum   = 0;
}
CIVCPFrameObjectEx::~CIVCPFrameObjectEx ()
{
   ClearEvents ();
}
int CIVCPFrameObjectEx::ReadXML (CXMLIO* pIO)
{
   int nEventNum = 0;
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Object"))
   {
      xmlNode.Attribute (TYPE_ID (int)   , "ID",               &m_nObjectID);
      xmlNode.Attribute (TYPE_ID (int)   , "Type",             &m_nObjectType);
      xmlNode.Attribute (TYPE_ID (IBox2D), "Box",              &m_ObjectBox);
      xmlNode.Attribute (TYPE_ID (int)   , "Status",           &m_nObjectStatus);
      xmlNode.Attribute (TYPE_ID (float) , "CalcArea",         &m_fCalcArea);
      xmlNode.Attribute (TYPE_ID (float) , "CalcWidth",        &m_fCalcWidth);
      xmlNode.Attribute (TYPE_ID (float) , "CalcHeight",       &m_fCalcHeight);
      xmlNode.Attribute (TYPE_ID (float) , "CalcMajorAxisLen", &m_fCalcMajorAxisLen);
      xmlNode.Attribute (TYPE_ID (float) , "CalcMinorAxisLen", &m_fCalcMinorAxisLen);
      xmlNode.Attribute (TYPE_ID (float) , "CalcSpeed",        &m_fCalcSpeed);
      xmlNode.Attribute (TYPE_ID (float) , "RealArea",         &m_fRealArea);
      xmlNode.Attribute (TYPE_ID (float) , "RealWidth",        &m_fRealWidth);
      xmlNode.Attribute (TYPE_ID (float) , "RealHeight",       &m_fRealHeight);
      xmlNode.Attribute (TYPE_ID (float) , "RealMajorAxisLen", &m_fRealMajorAxisLen);
      xmlNode.Attribute (TYPE_ID (float) , "RealMinorAxisLen", &m_fRealMinorAxisLen);
      xmlNode.Attribute (TYPE_ID (float) , "RealSpeed",        &m_fRealSpeed);
      xmlNode.Attribute (TYPE_ID (float) , "NorSpeed",         &m_fNorSpeed);
      xmlNode.Attribute (TYPE_ID (int)   , "EvtNum",           &nEventNum);
      ClearEvents ();
      m_nEventNum = 0;
      if (nEventNum > 0)
      {
         m_pEvents = new CIVCPFrameEvent[nEventNum];
         for (m_nEventNum=0; m_nEventNum<nEventNum; m_nEventNum++)
         {
            if (m_pEvents[m_nEventNum].ReadXML(pIO) != DONE)
               break;
         }
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPFrameObjectEx::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Object"))
   {
      xmlNode.Attribute (TYPE_ID (int)   , "ID",               &m_nObjectID);
      xmlNode.Attribute (TYPE_ID (int)   , "Type",             &m_nObjectType);
      xmlNode.Attribute (TYPE_ID (IBox2D), "Box",              &m_ObjectBox);
      xmlNode.Attribute (TYPE_ID (int)   , "Status",           &m_nObjectStatus);
      xmlNode.Attribute (TYPE_ID (float) , "CalcArea",         &m_fCalcArea);
      xmlNode.Attribute (TYPE_ID (float) , "CalcWidth",        &m_fCalcWidth);
      xmlNode.Attribute (TYPE_ID (float) , "CalcHeight",       &m_fCalcHeight);
      xmlNode.Attribute (TYPE_ID (float) , "CalcMajorAxisLen", &m_fCalcMajorAxisLen);
      xmlNode.Attribute (TYPE_ID (float) , "CalcMinorAxisLen", &m_fCalcMinorAxisLen);
      xmlNode.Attribute (TYPE_ID (float) , "CalcSpeed",        &m_fCalcSpeed);
      xmlNode.Attribute (TYPE_ID (float) , "RealArea",         &m_fRealArea);
      xmlNode.Attribute (TYPE_ID (float) , "RealWidth",        &m_fRealWidth);
      xmlNode.Attribute (TYPE_ID (float) , "RealHeight",       &m_fRealHeight);
      xmlNode.Attribute (TYPE_ID (float) , "RealMajorAxisLen", &m_fRealMajorAxisLen);
      xmlNode.Attribute (TYPE_ID (float) , "RealMinorAxisLen", &m_fRealMinorAxisLen);
      xmlNode.Attribute (TYPE_ID (float) , "RealSpeed",        &m_fRealSpeed);
      xmlNode.Attribute (TYPE_ID (float) , "NorSpeed",         &m_fNorSpeed);
      xmlNode.Attribute (TYPE_ID (int)   , "EvtNum",           &m_nEventNum);
      if (m_pEvents)
      {
         for (int i=0; i<m_nEventNum; i++)
         {
            m_pEvents[i].WriteXML(pIO);
         }
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
void CIVCPFrameObjectEx::ClearEvents ()
{
   if (m_pEvents)
   {
      delete []m_pEvents;
      m_pEvents = NULL;
   }
}

/////////////////////////////////////////////////////////////////////

CIVCPLiveFrameObjectInfo::CIVCPLiveFrameObjectInfo ()
{
   m_nPacketID   = IVCP_ID_Live_FrmObjInfo;
   m_nCameraUID  = 0;
   m_nObjectNum  = 0;
   m_pObjects    = NULL;
}

CIVCPLiveFrameObjectInfo::~CIVCPLiveFrameObjectInfo ()
{
   ClearObjects ();
}

int CIVCPLiveFrameObjectInfo::ReadXML (CXMLIO* pIO)
{
   int nObjectNum = 0;
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("FrameObj"))
   {
      StringA strValue;
      xmlNode.Attribute (TYPE_ID (int)     , "UID"      , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (StringA) , "GUID"     , &strValue);
      strcpy(m_strCameraGuid, strValue);
      xmlNode.Attribute (TYPE_ID (FILETIME), "FrameTime", &m_ftFrameTime);
      xmlNode.Attribute (TYPE_ID (float)   , "FrameRate", &m_fFrameRate);
      xmlNode.Attribute (TYPE_ID (ISize2D) , "FrameSize", &m_szFrameSize);
      xmlNode.Attribute (TYPE_ID (int)     , "ObjectNum", &nObjectNum);

      ClearObjects ();
      m_nObjectNum = 0;
      if (nObjectNum > 0)
      {
         m_pObjects = new CIVCPFrameObjectEx[nObjectNum];
         for (m_nObjectNum=0; m_nObjectNum<nObjectNum; m_nObjectNum++)
         {
            if (m_pObjects[m_nObjectNum].ReadXML(pIO) != DONE)
               break;
         }
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPLiveFrameObjectInfo::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("FrameObj"))
   {
      StringA strValue;
      xmlNode.Attribute (TYPE_ID (int)     , "UID"      , &m_nCameraUID);
      strValue = m_strCameraGuid;
      xmlNode.Attribute (TYPE_ID (StringA) , "GUID"     , &strValue);
      xmlNode.Attribute (TYPE_ID (FILETIME), "FrameTime", &m_ftFrameTime);
      xmlNode.Attribute (TYPE_ID (float)   , "FrameRate", &m_fFrameRate);
      xmlNode.Attribute (TYPE_ID (ISize2D) , "FrameSize", &m_szFrameSize);
      xmlNode.Attribute (TYPE_ID (int)     , "ObjectNum", &m_nObjectNum);

      if (m_pObjects)
      {
         for (int i=0; i<m_nObjectNum; i++)
         {
            m_pObjects[i].WriteXML(pIO);
         }
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
void CIVCPLiveFrameObjectInfo::ClearObjects ()
{
   if (m_pObjects)
   {
      delete []m_pObjects;
      m_pObjects = NULL;
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLiveEventInfo
//
///////////////////////////////////////////////////////////////////////////////

CIVCPLiveEventInfo::CIVCPLiveEventInfo () 
{
   m_nPacketID = IVCP_ID_Live_EventInfo;
   m_nCameraUID = 0;
   m_nEventStatus = 0;
   m_nEventID = 0;
   m_nEventType = 0;
   m_nEventRuleID = 0;
   m_nObjectID = 0;
   m_nObjectType = 0;
   m_nEventZoneID = 0;
   m_nProcWidth = 0;
   m_nProcHeight = 0;
   m_nThumbCodecID = 0;
   m_nThumbWidth = 0;
   m_nThumbHeight = 0;
   m_bThumbnail = FALSE;
   m_pBodyBuffer = NULL;
}
CIVCPLiveEventInfo::~CIVCPLiveEventInfo ()
{
   if (m_pBodyBuffer) free(m_pBodyBuffer);
}
int CIVCPLiveEventInfo::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Event"))
   {
      StringA strValue;
      xmlNode.Attribute (TYPE_ID (int)     , "UID"      , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "Start"    , &m_ftStartTime);
      xmlNode.Attribute (TYPE_ID (FILETIME), "End"      , &m_ftEndTime);
      xmlNode.Attribute (TYPE_ID (int)     , "EvtStatus", &m_nEventStatus);
      xmlNode.Attribute (TYPE_ID (int)     , "EvtID"    , &m_nEventID);
      xmlNode.Attribute (TYPE_ID (int)     , "EvtType"  , &m_nEventType);
      xmlNode.Attribute (TYPE_ID (int)     , "EvtRuleID", &m_nEventRuleID);
      xmlNode.Attribute (TYPE_ID (int)     , "ObjID"    , &m_nObjectID);
      xmlNode.Attribute (TYPE_ID (int)     , "ObjType"  , &m_nObjectType);
      xmlNode.Attribute (TYPE_ID (IBox2D)  , "ObjBox"   , &m_ObjectBox);
      xmlNode.Attribute (TYPE_ID (int)     , "EZID"     , &m_nEventZoneID);
      xmlNode.Attribute (TYPE_ID (StringA) , "EZName"   , &strValue);
      strcpy(m_strEventZoneName, strValue);
      xmlNode.Attribute (TYPE_ID (int)     , "Width"    , &m_nProcWidth);
      xmlNode.Attribute (TYPE_ID (int)     , "Height"   , &m_nProcHeight);
      xmlNode.Attribute (TYPE_ID (int)     , "TCodecID" , &m_nThumbCodecID);
      xmlNode.Attribute (TYPE_ID (int)     , "TWidth"   , &m_nThumbWidth);
      xmlNode.Attribute (TYPE_ID (int)     , "THeight"  , &m_nThumbHeight);
      xmlNode.Attribute (TYPE_ID (BOOL)    , "Thumbnail", &m_bThumbnail);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPLiveEventInfo::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Event"))
   {
      StringA strValue;
      xmlNode.Attribute (TYPE_ID (int)     , "UID"      , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "Start"    , &m_ftStartTime);
      xmlNode.Attribute (TYPE_ID (FILETIME), "End"      , &m_ftEndTime);
      xmlNode.Attribute (TYPE_ID (int)     , "EvtStatus", &m_nEventStatus);
      xmlNode.Attribute (TYPE_ID (int)     , "EvtID"    , &m_nEventID);
      xmlNode.Attribute (TYPE_ID (int)     , "EvtType"  , &m_nEventType);
      xmlNode.Attribute (TYPE_ID (int)     , "EvtRuleID", &m_nEventRuleID);
      xmlNode.Attribute (TYPE_ID (int)     , "ObjID"    , &m_nObjectID);
      xmlNode.Attribute (TYPE_ID (int)     , "ObjType"  , &m_nObjectType);
      xmlNode.Attribute (TYPE_ID (IBox2D)  , "ObjBox"   , &m_ObjectBox);
      xmlNode.Attribute (TYPE_ID (int)     , "EZID"     , &m_nEventZoneID);
      strValue = m_strEventZoneName;
      xmlNode.Attribute (TYPE_ID (StringA) , "EZName"   , &strValue);
      xmlNode.Attribute (TYPE_ID (int)     , "Width"    , &m_nProcWidth);
      xmlNode.Attribute (TYPE_ID (int)     , "Height"   , &m_nProcHeight);
      xmlNode.Attribute (TYPE_ID (int)     , "TCodecID" , &m_nThumbCodecID);
      xmlNode.Attribute (TYPE_ID (int)     , "TWidth"   , &m_nThumbWidth);
      xmlNode.Attribute (TYPE_ID (int)     , "THeight"  , &m_nThumbHeight);
      xmlNode.Attribute (TYPE_ID (BOOL)    , "Thumbnail", &m_bThumbnail);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
void CIVCPLiveEventInfo::ParseBodyBuffer ()
{
   FileIO buff;
   buff.Attach ((byte*)m_pBodyBuffer, m_nDataLen);
   CXMLIO xmlIO (&buff, XMLIO_Read);
   if (DONE != ReadXML(&xmlIO)) 
   {
      buff.Detach();
      return;
   }
}
void CIVCPLiveEventInfo::MakeBodyBuffer (byte *pEncodedBuff, int nEncodedLen)
{
   if (m_pBodyBuffer) delete m_pBodyBuffer;
   m_pBodyBuffer = NULL;
   m_nDataLen = 0;
   m_nBodyLen = 0;

   FileIO buff;
   CXMLIO xmlIO (&buff, XMLIO_Write);
   if (WriteXML(&xmlIO)) return;
   
   m_nDataLen = buff.GetLength();;
   m_nBodyLen = m_nDataLen + nEncodedLen;
   
   // 먼저 버퍼를 만든다.
   m_pBodyBuffer = (char*)malloc(m_nDataLen + nEncodedLen);
   if (m_pBodyBuffer)
   {
      memcpy(m_pBodyBuffer, (byte*)buff, m_nDataLen);
      memcpy(m_pBodyBuffer+m_nDataLen, pEncodedBuff, nEncodedLen);
   }
}
int CIVCPLiveEventInfo::ReadFromPacket (CIVCPPacket& aPacket)
{
   if (aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody) 
   {
      m_nCameraUID  = aPacket.m_Header.m_nOptions;
      m_nDataLen    = aPacket.m_Header.m_nDataLen;
      m_nBodyLen    = aPacket.m_Header.m_nBodyLen;
      m_pBodyBuffer = aPacket.m_pBody;
      aPacket.m_pBody = NULL;

      ParseBodyBuffer(); // 바로 파싱한다.
      return (DONE);
   }
   return (1);
}
int CIVCPLiveEventInfo::WriteToPacket (CIVCPPacket& aPacket)
{
   if (aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody) 
   {
      aPacket.FreeBody();
   }
   if (m_pBodyBuffer)
   {
      aPacket.m_Header.m_nOptions = m_nCameraUID;
      aPacket.m_Header.m_nDataLen = m_nDataLen;
      aPacket.m_Header.m_nBodyLen = m_nBodyLen;
      aPacket.m_pBody = m_pBodyBuffer;
      m_pBodyBuffer = NULL;
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (NETA) CIVCPLiveObjectInfo
//
///////////////////////////////////////////////////////////////////////////////

CIVCPLiveObjectInfo::CIVCPLiveObjectInfo (   )
{
   m_nPacketID     = IVCP_ID_Live_ObjectInfo;
   m_nProcWidth    = 0;
   m_nProcHeight   = 0;
   m_bThumbnail    = FALSE;
   m_nThumbCodecID = 0;
   m_nThumbWidth   = 0;
   m_nThumbHeight  = 0;
   m_pBodyBuffer   = NULL;
}
CIVCPLiveObjectInfo::~CIVCPLiveObjectInfo ()
{
   if (m_pBodyBuffer) free(m_pBodyBuffer);
}
int CIVCPLiveObjectInfo::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Object"))
   {
      xmlNode.Attribute (TYPE_ID (int)     , "UID"      , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "StartTime", &m_ftStartTime);
      xmlNode.Attribute (TYPE_ID (FILETIME), "EndTime"  , &m_ftEndTime);
      xmlNode.Attribute (TYPE_ID (int)     , "ObjID"    , &m_nObjectID);
      xmlNode.Attribute (TYPE_ID (int)     , "ObjType"  , &m_nObjectType);
      xmlNode.Attribute (TYPE_ID (int)     , "ObjStatus", &m_nObjectStatus);
      // Average
      xmlNode.Attribute (TYPE_ID(float), "AvgArea"                , &m_fAvgArea                 );
      xmlNode.Attribute (TYPE_ID(float), "AvgWidth"               , &m_fAvgWidth                );
      xmlNode.Attribute (TYPE_ID(float), "AvgHeight"              , &m_fAvgHeight               );
      xmlNode.Attribute (TYPE_ID(float), "AvgAspectRatio"         , &m_fAvgAspectRatio          );
      xmlNode.Attribute (TYPE_ID(float), "AvgAxisLengthRatio"     , &m_fAvgAxisLengthRatio      );
      xmlNode.Attribute (TYPE_ID(float), "AvgMajorAxisLength"     , &m_fAvgMajorAxisLength      );
      xmlNode.Attribute (TYPE_ID(float), "AvgMinorAxisLength"     , &m_fAvgMinorAxisLength      );
      xmlNode.Attribute (TYPE_ID(float), "AvgNorSpeed"            , &m_fAvgNorSpeed             );
      xmlNode.Attribute (TYPE_ID(float), "AvgSpeed"               , &m_fAvgSpeed                );
      xmlNode.Attribute (TYPE_ID(float), "AvgRealArea"            , &m_fAvgRealArea             );
      xmlNode.Attribute (TYPE_ID(float), "AvgRealDistance"        , &m_fAvgRealDistance         );
      xmlNode.Attribute (TYPE_ID(float), "AvgRealWidth"           , &m_fAvgRealWidth            );
      xmlNode.Attribute (TYPE_ID(float), "AvgRealHeight"          , &m_fAvgRealHeight           );
      xmlNode.Attribute (TYPE_ID(float), "AvgRealMajorAxisLength" , &m_fAvgRealMajorAxisLength  );
      xmlNode.Attribute (TYPE_ID(float), "AvgRealMinorAxisLength" , &m_fAvgRealMinorAxisLength  );
      xmlNode.Attribute (TYPE_ID(float), "AvgRealSpeed"           , &m_fAvgRealSpeed            );
      // Max
      xmlNode.Attribute (TYPE_ID(int)  , "MaxWidth"               , &m_nMaxWidth                );
      xmlNode.Attribute (TYPE_ID(int)  , "MaxHeight"              , &m_nMaxHeight               );
      xmlNode.Attribute (TYPE_ID(float), "MaxArea"                , &m_fMaxArea                 );
      xmlNode.Attribute (TYPE_ID(float), "MaxAspectRatio"         , &m_fMaxAspectRatio          );
      xmlNode.Attribute (TYPE_ID(float), "MaxAxisLengthRatio"     , &m_fMaxAxisLengthRatio      );
      xmlNode.Attribute (TYPE_ID(float), "MaxMajorAxisLength"     , &m_fMaxMajorAxisLength      );
      xmlNode.Attribute (TYPE_ID(float), "MaxMinorAxisLength"     , &m_fMaxMinorAxisLength      );
      xmlNode.Attribute (TYPE_ID(float), "MaxNorSpeed"            , &m_fMaxNorSpeed             );
      xmlNode.Attribute (TYPE_ID(float), "MaxSpeed"               , &m_fMaxSpeed                );
      xmlNode.Attribute (TYPE_ID(float), "MaxRealDistance"        , &m_fMaxRealDistance         );
      xmlNode.Attribute (TYPE_ID(float), "MaxRealSpeed"           , &m_fMaxRealSpeed            );
      // Min
      xmlNode.Attribute (TYPE_ID(int)  , "MinWidth"               , &m_nMinWidth                );
      xmlNode.Attribute (TYPE_ID(int)  , "MinHeight"              , &m_nMinHeight               );
      xmlNode.Attribute (TYPE_ID(float), "MinArea"                , &m_fMinArea                 );
      xmlNode.Attribute (TYPE_ID(float), "MinAspectRatio"         , &m_fMinAspectRatio          );
      xmlNode.Attribute (TYPE_ID(float), "MinAxisLengthRatio"     , &m_fMinAxisLengthRatio      );
      xmlNode.Attribute (TYPE_ID(float), "MinMajorAxisLength"     , &m_fMinMajorAxisLength      );
      xmlNode.Attribute (TYPE_ID(float), "MinMinorAxisLength"     , &m_fMinMinorAxisLength      );
      xmlNode.Attribute (TYPE_ID(float), "MinNorSpeed"            , &m_fMinNorSpeed             );
      xmlNode.Attribute (TYPE_ID(float), "MinSpeed"               , &m_fMinSpeed                );
      xmlNode.Attribute (TYPE_ID(float), "MinRealDistance"        , &m_fMinRealDistance         );
      xmlNode.Attribute (TYPE_ID(float), "MinRealSpeed"           , &m_fMinRealSpeed            );
      xmlNode.Attribute (TYPE_ID(float), "Histogram"              , m_fHistogram, 10            );

      xmlNode.Attribute (TYPE_ID (int) , "Width"    , &m_nProcWidth);
      xmlNode.Attribute (TYPE_ID (int) , "Height"   , &m_nProcHeight);
      xmlNode.Attribute (TYPE_ID (int) , "TCodecID" , &m_nThumbCodecID);
      xmlNode.Attribute (TYPE_ID (int) , "TWidth"   , &m_nThumbWidth);
      xmlNode.Attribute (TYPE_ID (int) , "THeight"  , &m_nThumbHeight);
      xmlNode.Attribute (TYPE_ID (BOOL), "Thumbnail", &m_bThumbnail);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPLiveObjectInfo::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Object"))
   {
      xmlNode.Attribute (TYPE_ID (int)     , "UID"      , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "StartTime", &m_ftStartTime);
      xmlNode.Attribute (TYPE_ID (FILETIME), "EndTime"  , &m_ftEndTime);
      xmlNode.Attribute (TYPE_ID (int)     , "ObjID"    , &m_nObjectID);
      xmlNode.Attribute (TYPE_ID (int)     , "ObjType"  , &m_nObjectType);
      xmlNode.Attribute (TYPE_ID (int)     , "ObjStatus", &m_nObjectStatus);
      // Average
      xmlNode.Attribute (TYPE_ID(float), "AvgArea"                , &m_fAvgArea                 );
      xmlNode.Attribute (TYPE_ID(float), "AvgWidth"               , &m_fAvgWidth                );
      xmlNode.Attribute (TYPE_ID(float), "AvgHeight"              , &m_fAvgHeight               );
      xmlNode.Attribute (TYPE_ID(float), "AvgAspectRatio"         , &m_fAvgAspectRatio          );
      xmlNode.Attribute (TYPE_ID(float), "AvgAxisLengthRatio"     , &m_fAvgAxisLengthRatio      );
      xmlNode.Attribute (TYPE_ID(float), "AvgMajorAxisLength"     , &m_fAvgMajorAxisLength      );
      xmlNode.Attribute (TYPE_ID(float), "AvgMinorAxisLength"     , &m_fAvgMinorAxisLength      );
      xmlNode.Attribute (TYPE_ID(float), "AvgNorSpeed"            , &m_fAvgNorSpeed             );
      xmlNode.Attribute (TYPE_ID(float), "AvgSpeed"               , &m_fAvgSpeed                );
      xmlNode.Attribute (TYPE_ID(float), "AvgRealArea"            , &m_fAvgRealArea             );
      xmlNode.Attribute (TYPE_ID(float), "AvgRealDistance"        , &m_fAvgRealDistance         );
      xmlNode.Attribute (TYPE_ID(float), "AvgRealWidth"           , &m_fAvgRealWidth            );
      xmlNode.Attribute (TYPE_ID(float), "AvgRealHeight"          , &m_fAvgRealHeight           );
      xmlNode.Attribute (TYPE_ID(float), "AvgRealMajorAxisLength" , &m_fAvgRealMajorAxisLength  );
      xmlNode.Attribute (TYPE_ID(float), "AvgRealMinorAxisLength" , &m_fAvgRealMinorAxisLength  );
      xmlNode.Attribute (TYPE_ID(float), "AvgRealSpeed"           , &m_fAvgRealSpeed            );
      // Max
      xmlNode.Attribute (TYPE_ID(int)  , "MaxWidth"               , &m_nMaxWidth                );
      xmlNode.Attribute (TYPE_ID(int)  , "MaxHeight"              , &m_nMaxHeight               );
      xmlNode.Attribute (TYPE_ID(float), "MaxArea"                , &m_fMaxArea                 );
      xmlNode.Attribute (TYPE_ID(float), "MaxAspectRatio"         , &m_fMaxAspectRatio          );
      xmlNode.Attribute (TYPE_ID(float), "MaxAxisLengthRatio"     , &m_fMaxAxisLengthRatio      );
      xmlNode.Attribute (TYPE_ID(float), "MaxMajorAxisLength"     , &m_fMaxMajorAxisLength      );
      xmlNode.Attribute (TYPE_ID(float), "MaxMinorAxisLength"     , &m_fMaxMinorAxisLength      );
      xmlNode.Attribute (TYPE_ID(float), "MaxNorSpeed"            , &m_fMaxNorSpeed             );
      xmlNode.Attribute (TYPE_ID(float), "MaxSpeed"               , &m_fMaxSpeed                );
      xmlNode.Attribute (TYPE_ID(float), "MaxRealDistance"        , &m_fMaxRealDistance         );
      xmlNode.Attribute (TYPE_ID(float), "MaxRealSpeed"           , &m_fMaxRealSpeed            );
      // Min
      xmlNode.Attribute (TYPE_ID(int)  , "MinWidth"               , &m_nMinWidth                );
      xmlNode.Attribute (TYPE_ID(int)  , "MinHeight"              , &m_nMinHeight               );
      xmlNode.Attribute (TYPE_ID(float), "MinArea"                , &m_fMinArea                 );
      xmlNode.Attribute (TYPE_ID(float), "MinAspectRatio"         , &m_fMinAspectRatio          );
      xmlNode.Attribute (TYPE_ID(float), "MinAxisLengthRatio"     , &m_fMinAxisLengthRatio      );
      xmlNode.Attribute (TYPE_ID(float), "MinMajorAxisLength"     , &m_fMinMajorAxisLength      );
      xmlNode.Attribute (TYPE_ID(float), "MinMinorAxisLength"     , &m_fMinMinorAxisLength      );
      xmlNode.Attribute (TYPE_ID(float), "MinNorSpeed"            , &m_fMinNorSpeed             );
      xmlNode.Attribute (TYPE_ID(float), "MinSpeed"               , &m_fMinSpeed                );
      xmlNode.Attribute (TYPE_ID(float), "MinRealDistance"        , &m_fMinRealDistance         );
      xmlNode.Attribute (TYPE_ID(float), "MinRealSpeed"           , &m_fMinRealSpeed            );
      xmlNode.Attribute (TYPE_ID(float), "Histogram"              , m_fHistogram, 10            );

      xmlNode.Attribute (TYPE_ID (int) , "Width"    , &m_nProcWidth);
      xmlNode.Attribute (TYPE_ID (int) , "Height"   , &m_nProcHeight);
      xmlNode.Attribute (TYPE_ID (int) , "TCodecID" , &m_nThumbCodecID);
      xmlNode.Attribute (TYPE_ID (int) , "TWidth"   , &m_nThumbWidth);
      xmlNode.Attribute (TYPE_ID (int) , "THeight"  , &m_nThumbHeight);
      xmlNode.Attribute (TYPE_ID (BOOL), "Thumbnail", &m_bThumbnail);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
void CIVCPLiveObjectInfo::ParseBodyBuffer ()
{
   FileIO buff;
   buff.Attach ((byte*)m_pBodyBuffer, m_nDataLen);
   CXMLIO xmlIO (&buff, XMLIO_Read);
   if (DONE != ReadXML(&xmlIO)) 
   {
      buff.Detach();
      return;
   }
}
void CIVCPLiveObjectInfo::MakeBodyBuffer (byte *pEncodedBuff, int nEncodedLen)
{
   if (m_pBodyBuffer) delete m_pBodyBuffer;
   m_pBodyBuffer = NULL;
   m_nDataLen = 0;
   m_nBodyLen = 0;

   FileIO buff;
   CXMLIO xmlIO (&buff, XMLIO_Write);
   if (WriteXML(&xmlIO)) return;
   
   m_nDataLen = buff.GetLength();;
   m_nBodyLen = m_nDataLen + nEncodedLen;
   
   // 먼저 버퍼를 만든다.
   m_pBodyBuffer = (char*)malloc(m_nDataLen + nEncodedLen);
   if (m_pBodyBuffer)
   {
      memcpy(m_pBodyBuffer, (byte*)buff, m_nDataLen);
      memcpy(m_pBodyBuffer+m_nDataLen, pEncodedBuff, nEncodedLen);
   }
}
int CIVCPLiveObjectInfo::ReadFromPacket (CIVCPPacket& aPacket)
{
   if (aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody) 
   {
      m_nCameraUID  = aPacket.m_Header.m_nOptions;
      m_nDataLen    = aPacket.m_Header.m_nDataLen;
      m_nBodyLen    = aPacket.m_Header.m_nBodyLen;
      m_pBodyBuffer = aPacket.m_pBody;
      aPacket.m_pBody = NULL;

      ParseBodyBuffer(); // 바로 파싱한다.
      return (DONE);
   }
   return (1);
}
int CIVCPLiveObjectInfo::WriteToPacket (CIVCPPacket& aPacket)
{
   if (aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody) 
   {
      aPacket.FreeBody();
   }
   if (m_pBodyBuffer)
   {
      aPacket.m_Header.m_nOptions = m_nCameraUID;
      aPacket.m_Header.m_nDataLen = m_nDataLen;
      aPacket.m_Header.m_nBodyLen = m_nBodyLen;
      aPacket.m_pBody = m_pBodyBuffer;
      m_pBodyBuffer = NULL;
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLivePlateInfo
//
///////////////////////////////////////////////////////////////////////////////

CIVCPLivePlateInfo::CIVCPLivePlateInfo () 
{
   m_nPacketID = IVCP_ID_Live_PlateInfo;
   m_nCameraUID = 0;
   m_nStatus = 0;
   m_strPlateName[0] ='\0';
   m_nPlateID = 0;
   m_nPlateType = 0;
   m_nProcWidth = 0;
   m_nProcHeight = 0;
   m_nThumbCodecID = 0;
   m_nThumbWidth = 0;
   m_nThumbHeight = 0;
   m_bThumbnail = FALSE;
   m_bPicture = FALSE;
   m_pBodyBuffer = NULL;
}
CIVCPLivePlateInfo::~CIVCPLivePlateInfo ()
{
   if (m_pBodyBuffer) free(m_pBodyBuffer);
}
int CIVCPLivePlateInfo::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Plate"))
   {
      StringA strValue;
      xmlNode.Attribute (TYPE_ID (int)     , "UID"      , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "Start"    , &m_ftStartTime);
      xmlNode.Attribute (TYPE_ID (FILETIME), "End"      , &m_ftEndTime);
      xmlNode.Attribute (TYPE_ID (int)     , "Status"   , &m_nStatus);
      xmlNode.Attribute (TYPE_ID (StringA) , "PltName"  , &strValue);
      strcpy(m_strPlateName, strValue);
      xmlNode.Attribute (TYPE_ID (int)     , "PltID"    , &m_nPlateID);
      xmlNode.Attribute (TYPE_ID (int)     , "PltType"  , &m_nPlateType);
      xmlNode.Attribute (TYPE_ID (IBox2D)  , "PltBox"   , &m_PlateBox);
      xmlNode.Attribute (TYPE_ID (int)     , "Width"    , &m_nProcWidth);
      xmlNode.Attribute (TYPE_ID (int)     , "Height"   , &m_nProcHeight);
      xmlNode.Attribute (TYPE_ID (int)     , "TCodecID" , &m_nThumbCodecID);
      xmlNode.Attribute (TYPE_ID (int)     , "TWidth"   , &m_nThumbWidth);
      xmlNode.Attribute (TYPE_ID (int)     , "THeight"  , &m_nThumbHeight);
      xmlNode.Attribute (TYPE_ID (BOOL)    , "Thumbnail", &m_bThumbnail);
      xmlNode.Attribute (TYPE_ID (BOOL)    , "Picture"  , &m_bPicture);
      xmlNode.Attribute (TYPE_ID (int)     , "MainTLen" , &m_nMainThumbLen);
      xmlNode.Attribute (TYPE_ID (int)     , "SubTLen"  , &m_nSubThumbLen);
      xmlNode.Attribute (TYPE_ID (int)     , "MainILen" , &m_nMainImageLen);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPLivePlateInfo::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Plate"))
   {
      StringA strValue;
      xmlNode.Attribute (TYPE_ID (int)     , "UID"      , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "Start"    , &m_ftStartTime);
      xmlNode.Attribute (TYPE_ID (FILETIME), "End"      , &m_ftEndTime);
      xmlNode.Attribute (TYPE_ID (int)     , "Status"   , &m_nStatus);
      strValue = m_strPlateName;
      xmlNode.Attribute (TYPE_ID (StringA) , "PltName"  , &strValue);
      xmlNode.Attribute (TYPE_ID (int)     , "PltID"    , &m_nPlateID);
      xmlNode.Attribute (TYPE_ID (int)     , "PltType"  , &m_nPlateType);
      xmlNode.Attribute (TYPE_ID (IBox2D)  , "PltBox"   , &m_PlateBox);
      xmlNode.Attribute (TYPE_ID (int)     , "Width"    , &m_nProcWidth);
      xmlNode.Attribute (TYPE_ID (int)     , "Height"   , &m_nProcHeight);
      xmlNode.Attribute (TYPE_ID (int)     , "TCodecID" , &m_nThumbCodecID);
      xmlNode.Attribute (TYPE_ID (int)     , "TWidth"   , &m_nThumbWidth);
      xmlNode.Attribute (TYPE_ID (int)     , "THeight"  , &m_nThumbHeight);
      xmlNode.Attribute (TYPE_ID (BOOL)    , "Thumbnail", &m_bThumbnail);
      xmlNode.Attribute (TYPE_ID (BOOL)    , "Picture"  , &m_bPicture);
      xmlNode.Attribute (TYPE_ID (int)     , "MainTLen" , &m_nMainThumbLen);
      xmlNode.Attribute (TYPE_ID (int)     , "SubTLen"  , &m_nSubThumbLen);
      xmlNode.Attribute (TYPE_ID (int)     , "MainILen" , &m_nMainImageLen);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
void CIVCPLivePlateInfo::ParseBodyBuffer ()
{
   FileIO buff;
   buff.Attach ((byte*)m_pBodyBuffer, m_nDataLen);
   CXMLIO xmlIO (&buff, XMLIO_Read);
   if (DONE != ReadXML(&xmlIO)) 
   {
      buff.Detach();
      return;
   }
}
void CIVCPLivePlateInfo::MakeBodyBuffer (byte *pMainThumbBuff, int nMainThumbLen, byte *pSubThumbBuff, int nSubThumbLen, byte *pMainImageBuff, int nMainImageLen)
{
   if (m_pBodyBuffer) delete m_pBodyBuffer;
   m_pBodyBuffer = NULL;
   m_nDataLen = 0;
   m_nBodyLen = 0;
 
   m_nMainThumbLen = nMainThumbLen;   // 사이즈
   m_nSubThumbLen  = nSubThumbLen;
   m_nMainImageLen = nMainImageLen;

   FileIO buff;
   CXMLIO xmlIO (&buff, XMLIO_Write);
   if (WriteXML(&xmlIO)) return;
   
   m_nDataLen = buff.GetLength();;
   m_nBodyLen = m_nDataLen + nMainThumbLen + nSubThumbLen + nMainImageLen;
   
   // 먼저 버퍼를 만든다.
   m_pBodyBuffer = (char*)malloc(m_nBodyLen);
   if (m_pBodyBuffer)
   {
      memcpy(m_pBodyBuffer, (byte*)buff, m_nDataLen);
      memcpy(m_pBodyBuffer+m_nDataLen, pMainThumbBuff, nMainThumbLen);
      memcpy(m_pBodyBuffer+m_nDataLen+nMainThumbLen, pSubThumbBuff, m_nSubThumbLen);
      memcpy(m_pBodyBuffer+m_nDataLen+nMainThumbLen+m_nSubThumbLen, pMainImageBuff, nMainImageLen);
   }
}
int CIVCPLivePlateInfo::ReadFromPacket (CIVCPPacket& aPacket)
{
   if (aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody) 
   {
      m_nCameraUID  = aPacket.m_Header.m_nOptions;
      m_nDataLen    = aPacket.m_Header.m_nDataLen;
      m_nBodyLen    = aPacket.m_Header.m_nBodyLen;
      m_pBodyBuffer = aPacket.m_pBody;
      aPacket.m_pBody = NULL;

      ParseBodyBuffer(); // 바로 파싱한다.
      return (DONE);
   }
   return (1);
}
int CIVCPLivePlateInfo::WriteToPacket (CIVCPPacket& aPacket)
{
   if (aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody) 
   {
      aPacket.FreeBody();
   }
   if (m_pBodyBuffer)
   {
      aPacket.m_Header.m_nOptions = m_nCameraUID;
      aPacket.m_Header.m_nDataLen = m_nDataLen;
      aPacket.m_Header.m_nBodyLen = m_nBodyLen;
      aPacket.m_pBody = m_pBodyBuffer;
      m_pBodyBuffer = NULL;
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLivePTZDataInfo
//
///////////////////////////////////////////////////////////////////////////////

CIVCPLivePTZDataInfo::CIVCPLivePTZDataInfo()
{
   m_nPacketID   = IVCP_ID_Live_PtzDataInfo;
   m_nCameraUID  = 0;
   m_fTiltAngle  = 0.0f;   // Pan 각도 : 반시계 방향이 증가하는 방향이다. 단위(degree, 0~360도)
   m_fTiltAngle  = 0.0f;   // Tilt 각도 : 아래로 향하는 방향이 증가하는 방향임. 단위(degree, 0~360도)
   m_fZoomFactor = 1.0f;   // Zoom 값 : 현재 줌 위치를 나타냄. 18배줌 카메라이면 1~18사이의 값을 가짐.
   m_fFOV        = 60.0f;  // FOV (Field Of View) : Pan 축으로의 화각을 나타냄. 단위(degree, 0~360도)
}
int CIVCPLivePTZDataInfo::ReadXML  (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PtzData")) 
   {
      xmlNode.Attribute (TYPE_ID (int),      "UID",  &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "Time", &m_ftFrameTime);
      xmlNode.Attribute (TYPE_ID (float),    "Pan",  &m_fPanAngle);
      xmlNode.Attribute (TYPE_ID (float),    "Tilt", &m_fTiltAngle);
      xmlNode.Attribute (TYPE_ID (float),    "Zoom", &m_fZoomFactor);
      xmlNode.Attribute (TYPE_ID (float),    "FOV",  &m_fFOV);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPLivePTZDataInfo::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PtzData")) 
   {
      xmlNode.Attribute (TYPE_ID (int),      "UID",  &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "Time", &m_ftFrameTime);
      xmlNode.Attribute (TYPE_ID (float),    "Pan",  &m_fPanAngle);
      xmlNode.Attribute (TYPE_ID (float),    "Tilt", &m_fTiltAngle);
      xmlNode.Attribute (TYPE_ID (float),    "Zoom", &m_fZoomFactor);
      xmlNode.Attribute (TYPE_ID (float),    "FOV",  &m_fFOV);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLivePresetIdInfo
//
///////////////////////////////////////////////////////////////////////////////

CIVCPLivePresetIdInfo::CIVCPLivePresetIdInfo ()
{
   m_nPacketID    = IVCP_ID_Live_PresetIdInfo;
   m_nCameraUID   = 0;
   m_nCurPresetID = 0;
}
int CIVCPLivePresetIdInfo::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PresetID"))
   {
      xmlNode.Attribute (TYPE_ID (int)     , "UID"     , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "FrmTime" , &m_ftFrameTime);
      xmlNode.Attribute (TYPE_ID (int)     , "PresetID", &m_nCurPresetID);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPLivePresetIdInfo::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PresetID"))
   {
      xmlNode.Attribute (TYPE_ID (int)     , "UID"     , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "FrmTime" , &m_ftFrameTime);
      xmlNode.Attribute (TYPE_ID (int)     , "PresetID", &m_nCurPresetID);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLiveEventZoneInfo
//
///////////////////////////////////////////////////////////////////////////////

CIVCPEventRule_DetectionTime::CIVCPEventRule_DetectionTime ()
{
   m_fDetectionTime = 0.0f;
}
int CIVCPEventRule_DetectionTime::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("RuleTime"))
   {
      xmlNode.Attribute (TYPE_ID (float), "DectectionTime", &m_fDetectionTime);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPEventRule_DetectionTime::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("RuleTime"))
   {
      xmlNode.Attribute (TYPE_ID (float), "DectectTime", &m_fDetectionTime);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

CIVCPEventRule_Counting::CIVCPEventRule_Counting ()
{
   m_nCount = 0;
   m_fAspectRatio = 0.0f;
}
int CIVCPEventRule_Counting::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("RuleCounting"))
   {
      xmlNode.Attribute (TYPE_ID (int),     "Count",       &m_nCount);
      xmlNode.Attribute (TYPE_ID (float),   "AspectRatio", &m_fAspectRatio);
      xmlNode.Attribute (TYPE_ID (ILine2D), "MajorAxis",   &m_aMajorAxis);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPEventRule_Counting::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("RuleCounting"))
   {
      xmlNode.Attribute (TYPE_ID (int),     "Count",       &m_nCount);
      xmlNode.Attribute (TYPE_ID (float),   "AspectRatio", &m_fAspectRatio);
      xmlNode.Attribute (TYPE_ID (ILine2D), "MajorAxis",   &m_aMajorAxis);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

CIVCPEventRule_DirectionalMotion::CIVCPEventRule_DirectionalMotion ()
{
   m_fDetectionTime = 0.0f;
   m_fDirection     = 0.0f;
   m_fRange         = 0.0f;
}
int CIVCPEventRule_DirectionalMotion::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("RuleDirMotion"))
   {
      xmlNode.Attribute (TYPE_ID (float), "DectectTime", &m_fDetectionTime);
      xmlNode.Attribute (TYPE_ID (float), "Direction",   &m_fDirection);
      xmlNode.Attribute (TYPE_ID (float), "Range",       &m_fRange);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPEventRule_DirectionalMotion::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("RuleDirMotion"))
   {
      xmlNode.Attribute (TYPE_ID (float), "DectectTime", &m_fDetectionTime);
      xmlNode.Attribute (TYPE_ID (float), "Direction",   &m_fDirection);
      xmlNode.Attribute (TYPE_ID (float), "Range",       &m_fRange);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

CIVCPEventRule_PathPassingItem::CIVCPEventRule_PathPassingItem ()
{
   m_nID          = 0;
   m_nVerticesNum = 0;
   m_pVertices    = NULL;
}
CIVCPEventRule_PathPassingItem::~CIVCPEventRule_PathPassingItem ()
{
   ClearVertices ();
}
int CIVCPEventRule_PathPassingItem::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PathPassing"))
   {
      xmlNode.Attribute (TYPE_ID (float),    "ID",          &m_nID);
      xmlNode.Attribute (TYPE_ID (IPoint2D), "CenterPt",    &m_ptCenter);
      xmlNode.Attribute (TYPE_ID (int),      "VerticesNum", &m_nVerticesNum);
      ClearVertices ();
      if (m_nVerticesNum > 0)
      {
         m_pVertices = new IPoint2D [m_nVerticesNum];
         xmlNode.Attribute (TYPE_ID (IPoint2D), "Vertices", m_pVertices, m_nVerticesNum);
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPEventRule_PathPassingItem::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PathPassing"))
   {
      xmlNode.Attribute (TYPE_ID (float),    "ID",          &m_nID);
      xmlNode.Attribute (TYPE_ID (IPoint2D), "CenterPt",    &m_ptCenter);
      xmlNode.Attribute (TYPE_ID (int),      "VerticesNum", &m_nVerticesNum);
      if (m_nVerticesNum > 0)
      {
         xmlNode.Attribute (TYPE_ID (IPoint2D), "Vertices", m_pVertices, m_nVerticesNum);
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
void CIVCPEventRule_PathPassingItem::ClearVertices ()
{
   if (m_pVertices)
   {
      delete []m_pVertices;
      m_pVertices = NULL;
   }
}

CIVCPEventRule_PathPassingArrow::CIVCPEventRule_PathPassingArrow ()
{
   m_nSplitZoneID = 0;
}
int CIVCPEventRule_PathPassingArrow::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Arrow"))
   {
      xmlNode.Attribute (TYPE_ID (int),     "SplitID", &m_nSplitZoneID);
      xmlNode.Attribute (TYPE_ID (ILine2D), "Arrow",   &m_lnArrow);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPEventRule_PathPassingArrow::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Arrow"))
   {
      xmlNode.Attribute (TYPE_ID (int),     "SplitID", &m_nSplitZoneID);
      xmlNode.Attribute (TYPE_ID (ILine2D), "Arrow",   &m_lnArrow);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

CIVCPEventRule_PathPassing::CIVCPEventRule_PathPassing ()
{
   m_nDirection     = 0;
   m_fDetectionTime = 0.0f;
   m_fPassingTime   = 0.0f;
   m_nItemNum       = 0;
   m_pItems         = NULL;
   m_nArrowNum      = 0;
   m_pArrows        = NULL;
}
CIVCPEventRule_PathPassing::~CIVCPEventRule_PathPassing ()
{
   ClearItems ();
}
int CIVCPEventRule_PathPassing::ReadXML (CXMLIO* pIO)
{
   int nItemNum = 0;
   int nArrowNum = 0;
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("RulePassPathing"))
   {
      xmlNode.Attribute (TYPE_ID (int),   "Direction",   &m_nDirection);
      xmlNode.Attribute (TYPE_ID (float), "DectectTime", &m_fDetectionTime);
      xmlNode.Attribute (TYPE_ID (float), "PassingTime", &m_fPassingTime);
      xmlNode.Attribute (TYPE_ID (int),   "ItemNum",     &nItemNum);
      xmlNode.Attribute (TYPE_ID (int),   "ArrowNum",    &nArrowNum);
      ClearItems ();
      m_nItemNum = 0;
      if (nItemNum > 0)
      {
         m_pItems = new CIVCPEventRule_PathPassingItem[nItemNum];
         for (m_nItemNum=0; m_nItemNum<nItemNum; m_nItemNum++)
         {
            if (m_pItems[m_nItemNum].ReadXML(pIO) != DONE)
               break;
         }
      }
      m_nArrowNum = 0;
      if (nArrowNum > 0)
      {
         m_pArrows = new CIVCPEventRule_PathPassingArrow[nArrowNum];
         for (m_nArrowNum=0; m_nArrowNum<nArrowNum; m_nArrowNum++)
         {
            if (m_pArrows[m_nArrowNum].ReadXML(pIO) != DONE)
               break;
         }
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPEventRule_PathPassing::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("RulePassPathing"))
   {
      xmlNode.Attribute (TYPE_ID (int),   "Direction",   &m_nDirection);
      xmlNode.Attribute (TYPE_ID (float), "DectectTime", &m_fDetectionTime);
      xmlNode.Attribute (TYPE_ID (float), "PassingTime", &m_fPassingTime);
      xmlNode.Attribute (TYPE_ID (int),   "ItemNum",     &m_nItemNum);
      xmlNode.Attribute (TYPE_ID (int),   "ArrowNum",    &m_nArrowNum);
      if (m_pItems)
      {
         for (int i=0; i<m_nItemNum; i++)
         {
            m_pItems[i].WriteXML(pIO);
         }
      }
      if (m_pArrows)
      {
         for (int i=0; i<m_nArrowNum; i++)
         {
            m_pArrows[i].WriteXML(pIO);
         }
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
void CIVCPEventRule_PathPassing::ClearItems ()
{
   if (m_pItems)
   {
      delete []m_pItems;
      m_pItems = NULL;
   }
   if (m_pArrows)
   {
      delete []m_pArrows;
      m_pArrows = NULL;
   }
}

/////////////////////////////////////////////////////////////////////

CIVCPEventZone::CIVCPEventZone ()
{
   m_nZoneID = 0;
   m_nPresetID = 0;
   m_nPriority = 0;
   m_nOptions = 0;
   m_nVerticesNum = 0;
   m_pVertices = NULL;

   m_nRuleOptions = 0;
   m_nRuleEventType = 0;
   m_nRuleObjectType = 0;
}
CIVCPEventZone::~CIVCPEventZone ()
{
   ClearVertices ();
}
void CIVCPEventZone::ClearVertices ()
{
   if (m_pVertices)
   {
      delete []m_pVertices;
      m_pVertices = NULL;
   }
}
int CIVCPEventZone::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("EventZone"))
   {
      StringA strValue;
      xmlNode.Attribute (TYPE_ID (int),      "ZoneID",     &m_nZoneID);
      xmlNode.Attribute (TYPE_ID (int),      "PresetID",   &m_nPresetID);
      xmlNode.Attribute (TYPE_ID (int),      "Priority",   &m_nPriority);
      xmlNode.Attribute (TYPE_ID (int),      "Options",    &m_nOptions);
      xmlNode.Attribute (TYPE_ID (int),      "Width",      &m_nWidth);
      xmlNode.Attribute (TYPE_ID (int),      "Height",     &m_nHeight);
      xmlNode.Attribute (TYPE_ID (StringA),  "ZoneName",   &strValue);
      strcpy(m_strName, strValue);
      xmlNode.Attribute (TYPE_ID (IPoint2D), "CenterPt",   &m_ptCenter);
      xmlNode.Attribute (TYPE_ID (int),      "VerticesNum", &m_nVerticesNum);
      ClearVertices ();
      if (m_nVerticesNum > 0)
      {
         m_pVertices = new IPoint2D [m_nVerticesNum];
         xmlNode.Attribute (TYPE_ID (IPoint2D), "Vertices", m_pVertices, m_nVerticesNum);
      }
      xmlNode.Attribute (TYPE_ID (int),      "RuleOptions", &m_nRuleOptions);
      xmlNode.Attribute (TYPE_ID (int),      "RuleEvtType", &m_nRuleEventType);
      xmlNode.Attribute (TYPE_ID (int),      "RuleObjType", &m_nRuleObjectType);
      xmlNode.Attribute (TYPE_ID (StringA),  "RuleName",    &strValue);
      strcpy(m_strRuleName, strValue);

      m_aAbandoned.ReadXML(pIO);
      m_aCounting.ReadXML(pIO);
      m_aDirectionalMotion.ReadXML(pIO);
      m_aPathPassing.ReadXML(pIO);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPEventZone::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("EventZone"))
   {
      StringA strValue;
      xmlNode.Attribute (TYPE_ID (int),      "ZoneID",     &m_nZoneID);
      xmlNode.Attribute (TYPE_ID (int),      "PresetID",   &m_nPresetID);
      xmlNode.Attribute (TYPE_ID (int),      "Priority",   &m_nPriority);
      xmlNode.Attribute (TYPE_ID (int),      "Options",    &m_nOptions);
      xmlNode.Attribute (TYPE_ID (int),      "Width",      &m_nWidth);
      xmlNode.Attribute (TYPE_ID (int),      "Height",     &m_nHeight);
      strValue = m_strName;
      xmlNode.Attribute (TYPE_ID (StringA),  "ZoneName",   &strValue);
      xmlNode.Attribute (TYPE_ID (IPoint2D), "CenterPt",   &m_ptCenter);
      xmlNode.Attribute (TYPE_ID (int),      "VerticesNum", &m_nVerticesNum);
      if (m_nVerticesNum > 0)
      {
         xmlNode.Attribute (TYPE_ID (IPoint2D), "Vertices", m_pVertices, m_nVerticesNum);
      }
      xmlNode.Attribute (TYPE_ID (int),      "RuleOptions", &m_nRuleOptions);
      xmlNode.Attribute (TYPE_ID (int),      "RuleEvtType", &m_nRuleEventType);
      xmlNode.Attribute (TYPE_ID (int),      "RuleObjType", &m_nRuleObjectType);
      strValue = m_strRuleName;
      xmlNode.Attribute (TYPE_ID (StringA),  "RuleName",    &strValue);

      m_aAbandoned.WriteXML(pIO);
      m_aCounting.WriteXML(pIO);
      m_aDirectionalMotion.WriteXML(pIO);
      m_aPathPassing.WriteXML(pIO);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

/////////////////////////////////////////////////////////////////////

CIVCPLiveEventZoneInfo::CIVCPLiveEventZoneInfo ()
{
   m_nPacketID   = IVCP_ID_Live_EventZoneInfo;
   m_nCameraUID  = 0;
   m_nZoneNum    = 0;
   m_pEventZones = NULL;
}
CIVCPLiveEventZoneInfo::~CIVCPLiveEventZoneInfo ()
{
   ClearEventZones ();
}
void CIVCPLiveEventZoneInfo::ClearEventZones ()
{
   if (m_pEventZones)
   {
      delete []m_pEventZones;
      m_pEventZones = NULL;
   }
}
int CIVCPLiveEventZoneInfo::ReadXML (CXMLIO* pIO)
{
   int nZoneNum = 0;
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("EventZoneInfo"))
   {
      xmlNode.Attribute (TYPE_ID (int)     , "UID"     , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "FrmTime" , &m_ftFrameTime);
      xmlNode.Attribute (TYPE_ID (int)     , "Width"   , &m_nWidth);
      xmlNode.Attribute (TYPE_ID (int)     , "Height"  , &m_nHeight);
      xmlNode.Attribute (TYPE_ID (int)     , "ZoneNum" , &nZoneNum);
      ClearEventZones ();
      m_nZoneNum = 0;
      if (nZoneNum > 0)
      {
         m_pEventZones = new CIVCPEventZone[nZoneNum];
         for (m_nZoneNum=0; m_nZoneNum<nZoneNum; m_nZoneNum++)
         {
            if (m_pEventZones[m_nZoneNum].ReadXML(pIO) != DONE)
               break;
         }
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPLiveEventZoneInfo::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("EventZoneInfo"))
   {
      xmlNode.Attribute (TYPE_ID (int)     , "UID"     , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "FrmTime" , &m_ftFrameTime);
      xmlNode.Attribute (TYPE_ID (int)     , "Width"   , &m_nWidth);
      xmlNode.Attribute (TYPE_ID (int)     , "Height"  , &m_nHeight);
      xmlNode.Attribute (TYPE_ID (int)     , "ZoneNum" , &m_nZoneNum);
      if (m_pEventZones)
      {
         for (int i=0; i<m_nZoneNum; i++)
         {
            m_pEventZones[i].WriteXML(pIO);
         }
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLiveEventZoneCountInfo
//
///////////////////////////////////////////////////////////////////////////////

CIVCPEventZoneCount::CIVCPEventZoneCount()
{
   m_nZoneID = 0;
   m_nZoneCount = 0;
}
int CIVCPEventZoneCount::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Item"))
   {
      xmlNode.Attribute (TYPE_ID (int), "ZID",   &m_nZoneID);
      xmlNode.Attribute (TYPE_ID (int), "Count", &m_nZoneCount);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPEventZoneCount::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Item"))
   {
      xmlNode.Attribute (TYPE_ID (int), "ZID",   &m_nZoneID);
      xmlNode.Attribute (TYPE_ID (int), "Count", &m_nZoneCount);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////

CIVCPLiveEventZoneCountInfo::CIVCPLiveEventZoneCountInfo ()
{
   m_nPacketID   = IVCP_ID_Live_EventZoneCount;
   m_nCameraUID  = 0;
   m_nZoneNum    = 0;
   m_pZoneCounts = NULL;
}
CIVCPLiveEventZoneCountInfo::~CIVCPLiveEventZoneCountInfo ()
{
   ClearZoneCounts ();
}
int CIVCPLiveEventZoneCountInfo::ReadXML (CXMLIO* pIO)
{
   int nZoneNum = 0;
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("ZoneCnt"))
   {
      xmlNode.Attribute (TYPE_ID (int)     , "UID"     , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "FrmTime" , &m_ftFrameTime);
      xmlNode.Attribute (TYPE_ID (int)     , "ZoneNum" , &nZoneNum);

      ClearZoneCounts ();
      m_nZoneNum = 0;
      if (nZoneNum > 0)
      {
         m_pZoneCounts = new CIVCPEventZoneCount[nZoneNum];
         for (m_nZoneNum=0; m_nZoneNum<nZoneNum; m_nZoneNum++)
         {
            if (m_pZoneCounts[m_nZoneNum].ReadXML(pIO) != DONE)
               break;
         }
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPLiveEventZoneCountInfo::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("ZoneCnt"))
   {
      xmlNode.Attribute (TYPE_ID (int)     , "UID"     , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "FrmTime" , &m_ftFrameTime);
      xmlNode.Attribute (TYPE_ID (int)     , "ZoneNum" , &m_nZoneNum);

      if (m_pZoneCounts)
      {
         for (int i=0; i<m_nZoneNum; i++)
         {
            m_pZoneCounts[i].WriteXML(pIO);
         }
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
void CIVCPLiveEventZoneCountInfo::ClearZoneCounts ()
{
   if (m_pZoneCounts)
   {
      delete []m_pZoneCounts;
      m_pZoneCounts = NULL;
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLiveEventZoneCountInfo
//
///////////////////////////////////////////////////////////////////////////////

CIVCPPresetItemInfo::CIVCPPresetItemInfo()
{
   m_nID = 0;
   m_strName[0] = '\0';
}
int CIVCPPresetItemInfo::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Item"))
   {
      StringA strValue;
      xmlNode.Attribute (TYPE_ID (int),     "ID",   &m_nID);
      xmlNode.Attribute (TYPE_ID (StringA), "Name", &strValue);
      strcpy(m_strName, strValue);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPPresetItemInfo::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Item"))
   {
      StringA strValue;
      xmlNode.Attribute (TYPE_ID (int),     "ID",   &m_nID);
      strValue = m_strName;
      xmlNode.Attribute (TYPE_ID (StringA), "Name", &strValue);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

CIVCPPresetGroupInfo::CIVCPPresetGroupInfo()
{
   m_nID = 0;
   m_strName[0] = '\0';
   m_nPresetNum = 0;
   m_pPresetItems = NULL;
}
CIVCPPresetGroupInfo::~CIVCPPresetGroupInfo ()
{
   ClearPresetItems ();
}
int CIVCPPresetGroupInfo::ReadXML (CXMLIO* pIO)
{
   int nItemNum = 0;
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Group"))
   {
      StringA strValue;
      xmlNode.Attribute (TYPE_ID (int),     "ID",      &m_nID);
      xmlNode.Attribute (TYPE_ID (StringA), "Name",    &strValue);
      strcpy(m_strName, strValue);

      ClearPresetItems ();
      xmlNode.Attribute (TYPE_ID (int)    , "ItemNum", &nItemNum);
      m_nPresetNum = 0;
      if (nItemNum > 0)
      {
         m_pPresetItems = new CIVCPPresetItemInfo[nItemNum];
         for (m_nPresetNum=0; m_nPresetNum<nItemNum; m_nPresetNum++)
         {
            if (m_pPresetItems[m_nPresetNum].ReadXML(pIO) != DONE)
               break;
         }
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPPresetGroupInfo::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Group"))
   {
      StringA strValue;
      xmlNode.Attribute (TYPE_ID (int),     "ID",      &m_nID);
      strValue = m_strName;
      xmlNode.Attribute (TYPE_ID (StringA), "Name",    &strValue);
      xmlNode.Attribute (TYPE_ID (int ),    "ItemNum", &m_nPresetNum);
      if (m_pPresetItems)
      {
         for (int i=0; i<m_nPresetNum; i++)
         {
            m_pPresetItems[i].WriteXML(pIO);
         }
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
void CIVCPPresetGroupInfo::ClearPresetItems ()
{
   if (m_pPresetItems)
   {
      delete []m_pPresetItems;
      m_pPresetItems = NULL;
   }
}

///////////////////////////////////////////////////////////

CIVCPLivePresetMapInfo::CIVCPLivePresetMapInfo ()
{
   m_nPacketID    = IVCP_ID_Live_PresetMapInfo;
   m_nCameraUID   = 0;
   m_nGroupNum    = 0;
   m_pGroupItems  = NULL;
}
CIVCPLivePresetMapInfo::~CIVCPLivePresetMapInfo ()
{
   ClearGroupItems ();
}
int CIVCPLivePresetMapInfo::ReadXML (CXMLIO* pIO)
{
   int nGroupNum = 0;
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PresetMap"))
   {
      xmlNode.Attribute (TYPE_ID (int)     , "UID"     ,   &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "FrmTime" ,   &m_ftFrameTime);

      ClearGroupItems ();
      xmlNode.Attribute (TYPE_ID (int)     , "GroupNum" , &nGroupNum);
      m_nGroupNum = 0;
      if (nGroupNum > 0)
      {
         m_pGroupItems = new CIVCPPresetGroupInfo[nGroupNum];
         for (m_nGroupNum=0; m_nGroupNum<nGroupNum; m_nGroupNum++)
         {
            if (m_pGroupItems[m_nGroupNum].ReadXML(pIO) != DONE)
               break;
         }
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPLivePresetMapInfo::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PresetMap"))
   {
      xmlNode.Attribute (TYPE_ID (int)     , "UID"     ,   &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "FrmTime" ,   &m_ftFrameTime);
      xmlNode.Attribute (TYPE_ID (int)     , "GroupNum" , &m_nGroupNum);
      if (m_pGroupItems)
      {
         for (int i=0; i<m_nGroupNum; i++)
         {
            m_pGroupItems[i].WriteXML(pIO);
         }
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
void CIVCPLivePresetMapInfo::ClearGroupItems ()
{
   if (m_pGroupItems)
   {
      delete []m_pGroupItems;
      m_pGroupItems = NULL;
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLiveSettingInfo
//
///////////////////////////////////////////////////////////////////////////////

CIVCPLiveSettingInfo::CIVCPLiveSettingInfo ()
{
   m_nPacketID         = IVCP_ID_Live_SettingInfo;
   m_nCameraUID        = 0;
   m_nSettingMask      = 0;
   m_nSettingFlag      = 0;
   m_nWhiteBalanceMode = 0;
   m_nDayNightMode     = 0;
}
CIVCPLiveSettingInfo::~CIVCPLiveSettingInfo ()
{
}
int CIVCPLiveSettingInfo::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Setting"))
   {
      xmlNode.Attribute (TYPE_ID (int)     , "UID"    , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "FrmTime", &m_ftFrameTime);
      xmlNode.Attribute (TYPE_ID (xint64)  , "Mask"   , &m_nSettingMask);
      xmlNode.Attribute (TYPE_ID (xint64)  , "Flag"   , &m_nSettingFlag);
      xmlNode.Attribute (TYPE_ID (int)     , "Stabil" , &m_nStabilNum);
      xmlNode.Attribute (TYPE_ID (int)     , "Defog"  , &m_nDefogLevel);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPLiveSettingInfo::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Setting"))
   {
      xmlNode.Attribute (TYPE_ID (int)     , "UID"     , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "FrmTime" , &m_ftFrameTime);
      xmlNode.Attribute (TYPE_ID (xint64)  , "Mask"    , &m_nSettingMask);
      xmlNode.Attribute (TYPE_ID (xint64)  , "Flag"    , &m_nSettingFlag);
      xmlNode.Attribute (TYPE_ID (int)     , "Stabil"  , &m_nStabilNum);
      xmlNode.Attribute (TYPE_ID (int)     , "Defog"   , &m_nDefogLevel);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (DATA) CIVCPLiveVideoStreamData
//
///////////////////////////////////////////////////////////////////////////////

CIVCPLiveVideoStreamData::CIVCPLiveVideoStreamData () 
{
   m_nPacketID = IVCP_ID_Live_VideoData;
   m_nCameraUID = 0;
   m_nStreamIdx = 0;
   m_nCodecID = 0;
   m_nWidth = 0;
   m_nHeight = 0;
   m_fFrameRate = 0.0f;
   m_nFrameFlag = 0;
   m_pBodyBuffer = NULL;
}
CIVCPLiveVideoStreamData::~CIVCPLiveVideoStreamData ()
{
   if (m_pBodyBuffer) free(m_pBodyBuffer);
}
void CIVCPLiveVideoStreamData::ParseBodyBuffer ()
{
   char* resString = m_pBodyBuffer;
   int nLen = m_nDataLen - 1; // 마지막은 '\0';
   int nCount = 0;
   int i = 0;
   while ((i < nLen) && (nCount < 7))
   {
      while ((i < nLen) && (resString[i] == ' ')) i++;
      switch (nCount)
      {
      case 0: // Stream Index
         m_nStreamIdx = atoi(&resString[i]); 
         break;
      case 1: // Codec
         m_nCodecID = atoi(&resString[i]); 
         break;
      case 2: // Width
         m_nWidth = atoi(&resString[i]); 
         break;
      case 3: // Height
         m_nHeight = atoi(&resString[i]); 
         break;
      case 4: // m_fFrameRate
         m_fFrameRate = (float)atof(&resString[i]); 
         break;
      case 5: // FrameFlag
         m_nFrameFlag = atoi(&resString[i]); 
         break;
      case 6: // FrameTime
         ivcpGetFileTimeFromString (m_ftFrameTime, &resString[i]);
         break;
      }
      nCount++;
      while ((i < nLen) && (resString[i] != ' ')) i++;
      if (resString[i] != ' ')
         break;
   }
}
void CIVCPLiveVideoStreamData::MakeBodyBuffer (byte *pEncodedBuff, int nEncodedLen)
{
   // "0 0 320 240 7.0 1" (StreamIdx Codec Width Height FrameRate FrameFlag FrameTime)
   StringA strDesc;
#ifdef __WIN32
   SYSTEMTIME st;
   FileTimeToSystemTime (&m_ftFrameTime, &st);
   strDesc.Format("%d %d %d %d %.2f %d %04d%02d%02d%02d%02d%02d%03d", m_nStreamIdx, m_nCodecID, m_nWidth, m_nHeight, m_fFrameRate, m_nFrameFlag,
      st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
#else
   struct tm * ptm;
   time_t rawtime = m_ftFrameTime.dwHighDateTime;
   DWORD nMSec = (m_ftFrameTime.dwLowDateTime / 1000000);
   ptm = localtime ( &rawtime ); // LocalTime
   strDesc.Format("%d %d %d %d %.2f %d %04d%02d%02d%02d%02d%02d%03d", m_nStreamIdx, m_nCodecID, m_nWidth, m_nHeight, m_fFrameRate, m_nFrameFlag,
      ptm->tm_year, ptm->tm_mon, ptm->tm_mday, ptm->tm_hour, ptm->tm_min, ptm->tm_sec, nMSec);
#endif

   m_nDataLen = strDesc.GetStringLength() + 1; // '\0' 추가
   m_nBodyLen = m_nDataLen + nEncodedLen;
   
   // 먼저 버퍼를 만든다.
   if (m_pBodyBuffer) delete m_pBodyBuffer;
   m_pBodyBuffer = (char*)malloc(m_nDataLen + nEncodedLen);
   if (m_pBodyBuffer)
   {
      memcpy(m_pBodyBuffer, (void*)strDesc, m_nDataLen);
      m_pBodyBuffer[m_nDataLen-1] = '\0';
      memcpy(m_pBodyBuffer+m_nDataLen, pEncodedBuff, nEncodedLen);
   }
}
int CIVCPLiveVideoStreamData::ReadFromPacket (CIVCPPacket& aPacket)
{
   if (aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody) 
   {
      m_nCameraUID  = aPacket.m_Header.m_nOptions;
      m_nDataLen    = aPacket.m_Header.m_nDataLen;
      m_nBodyLen    = aPacket.m_Header.m_nBodyLen;
      m_pBodyBuffer = aPacket.m_pBody;
      aPacket.m_pBody = NULL;

      ParseBodyBuffer(); // 바로 파싱한다.
      return (DONE);
   }
   return (1);
}
int CIVCPLiveVideoStreamData::WriteToPacket (CIVCPPacket& aPacket)
{
   if (aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody) 
   {
      aPacket.FreeBody();
   }
   if (m_pBodyBuffer)
   {
      aPacket.m_Header.m_nOptions = m_nCameraUID;
      aPacket.m_Header.m_nDataLen = m_nDataLen;
      aPacket.m_Header.m_nBodyLen = m_nBodyLen;
      aPacket.m_pBody = m_pBodyBuffer;
      m_pBodyBuffer = NULL;
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (DATA) CIVCPLiveAudioStreamData
//
///////////////////////////////////////////////////////////////////////////////

CIVCPLiveAudioStreamData::CIVCPLiveAudioStreamData () 
{
   m_nPacketID = IVCP_ID_Live_AudioData;
   m_nCameraUID = 0;
   m_nStreamIdx = 0;
   m_nCodecID = 0;
   m_nChannel = 1;
   m_nBitPerSamples = 16;
   m_nSamplingRate = 44100;
   m_pBodyBuffer = NULL;
}
CIVCPLiveAudioStreamData::~CIVCPLiveAudioStreamData ()
{
   if (m_pBodyBuffer) free(m_pBodyBuffer);
}
void CIVCPLiveAudioStreamData::ParseBodyBuffer ()
{
   char* resString = m_pBodyBuffer;
   int nLen = m_nDataLen - 1; // 마지막은 '\0';
   int nCount = 0;
   int i = 0;
   while ((i < nLen) && (nCount < 6))
   {
      while ((i < nLen) && (resString[i] == ' ')) i++;
      switch (nCount)
      {
      case 0: // Stream Index
         m_nStreamIdx = atoi(&resString[i]); 
         break;
      case 1: // Codec
         m_nCodecID = atoi(&resString[i]); 
         break;
      case 2: // Channel
         m_nChannel = atoi(&resString[i]); 
         break;
      case 3: // BitPerSamples
         m_nBitPerSamples = atoi(&resString[i]); 
         break;
      case 4: // SamplingRate
         m_nSamplingRate = atoi(&resString[i]); 
         break;
      case 5: // FrameTime
         ivcpGetFileTimeFromString (m_ftFrameTime, &resString[i]);
         break;
      }
      nCount++;
      while ((i < nLen) && (resString[i] != ' ')) i++;
      if (resString[i] != ' ')
         break;
   }
}
void CIVCPLiveAudioStreamData::MakeBodyBuffer (byte *pEncodedBuff, int nEncodedLen)
{
   // "0 0 1 16 44100" (StreamIdx Codec Channel BitPerSamples SamplingRate FrameTime)
   StringA strDesc;
#ifdef __WIN32
   SYSTEMTIME st;
   FileTimeToSystemTime (&m_ftFrameTime, &st);
   strDesc.Format("%d %d %d %d %d %04d%02d%02d%02d%02d%02d%03d", m_nStreamIdx, m_nCodecID, m_nChannel, m_nBitPerSamples, m_nSamplingRate,
      st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
#else
   struct tm * ptm;
   time_t rawtime = m_ftFrameTime.dwHighDateTime;
   DWORD nMSec = (m_ftFrameTime.dwLowDateTime / 1000000);
   ptm = localtime ( &rawtime ); // LocalTime
   strDesc.Format("%d %d %d %d %d %04d%02d%02d%02d%02d%02d%03d", m_nStreamIdx, m_nCodecID, m_nChannel, m_nBitPerSamples, m_nSamplingRate,
      ptm->tm_year, ptm->tm_mon, ptm->tm_mday, ptm->tm_hour, ptm->tm_min, ptm->tm_sec, nMSec);
#endif

   m_nDataLen = strDesc.GetStringLength() + 1; // '\0' 추가
   m_nBodyLen = m_nDataLen + nEncodedLen;
   
   // 먼저 버퍼를 만든다.
   if (m_pBodyBuffer) delete m_pBodyBuffer;
   m_pBodyBuffer = (char*)malloc(m_nDataLen + nEncodedLen);
   if (m_pBodyBuffer)
   {
      memcpy(m_pBodyBuffer, (void*)strDesc, m_nDataLen);
      m_pBodyBuffer[m_nDataLen-1] = '\0';
      memcpy(m_pBodyBuffer+m_nDataLen, pEncodedBuff, nEncodedLen);
   }
}
int CIVCPLiveAudioStreamData::ReadFromPacket (CIVCPPacket& aPacket)
{
   if (aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody) 
   {
      m_nCameraUID  = aPacket.m_Header.m_nOptions;
      m_nDataLen    = aPacket.m_Header.m_nDataLen;
      m_nBodyLen    = aPacket.m_Header.m_nBodyLen;
      m_pBodyBuffer = aPacket.m_pBody;
      aPacket.m_pBody = NULL;

      ParseBodyBuffer(); // 바로 파싱한다.
      return (DONE);
   }
   return (1);
}
int CIVCPLiveAudioStreamData::WriteToPacket (CIVCPPacket& aPacket)
{
   if (aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody) 
   {
      aPacket.FreeBody();
   }
   if (m_pBodyBuffer)
   {
      aPacket.m_Header.m_nOptions = m_nCameraUID;
      aPacket.m_Header.m_nDataLen = m_nDataLen;
      aPacket.m_Header.m_nBodyLen = m_nBodyLen;
      aPacket.m_pBody = m_pBodyBuffer;
      m_pBodyBuffer = NULL;
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (INFO) CIVCPLiveStatus
//
///////////////////////////////////////////////////////////////////////////////

CIVCPLiveStatus::CIVCPLiveStatus ()
{
   m_nPacketID  = IVCP_ID_Live_Status;
   m_nCameraUID = 0;
   m_nType      = 0;
   m_nParam     = 0;
}
CIVCPLiveStatus::~CIVCPLiveStatus ()
{
}
int CIVCPLiveStatus::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("LiveStatus"))
   {
      xmlNode.Attribute (TYPE_ID (int)     , "UID"    , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "FrmTime", &m_ftFrameTime);
      xmlNode.Attribute (TYPE_ID (int)     , "Type"   , &m_nType);
      xmlNode.Attribute (TYPE_ID (int)     , "Param"  , &m_nParam);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPLiveStatus::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("LiveStatus"))
   {
      xmlNode.Attribute (TYPE_ID (int)     , "UID"    , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "FrmTime", &m_ftFrameTime);
      xmlNode.Attribute (TYPE_ID (int)     , "Type"   , &m_nType);
      xmlNode.Attribute (TYPE_ID (int)     , "Param"  , &m_nParam);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPPlayFrameMetaInfo
//
///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// CIVCPPlayFrameObject

CIVCPPlayFrameObject::CIVCPPlayFrameObject ()
{
   m_nObjectID   = 0;
   m_nObjectType = 0;
   m_nEventFlag  = 0;
   m_nFlagLocked = 0;
}
CIVCPPlayFrameObject::~CIVCPPlayFrameObject ()
{
}
int CIVCPPlayFrameObject::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Object"))
   {
      xmlNode.Attribute (TYPE_ID (int)   , "ID"    , &m_nObjectID);
      xmlNode.Attribute (TYPE_ID (int)   , "Type"  , &m_nObjectType);
      xmlNode.Attribute (TYPE_ID (int)   , "Event" , &m_nEventFlag);
      xmlNode.Attribute (TYPE_ID (int)   , "Locked", &m_nFlagLocked);
      xmlNode.Attribute (TYPE_ID (IBox2D), "Box"   , &m_ObjectBox);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPPlayFrameObject::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Object"))
   {
      xmlNode.Attribute (TYPE_ID (int)   , "ID"    , &m_nObjectID);
      xmlNode.Attribute (TYPE_ID (int)   , "Type"  , &m_nObjectType);
      xmlNode.Attribute (TYPE_ID (int)   , "Event" , &m_nEventFlag);
      xmlNode.Attribute (TYPE_ID (int)   , "Locked", &m_nFlagLocked);
      xmlNode.Attribute (TYPE_ID (IBox2D), "Box"   , &m_ObjectBox);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

/////////////////////////////////////////////////////////////////////
// CIVCPLiveFrameInfo

CIVCPPlayFrameInfo::CIVCPPlayFrameInfo ()
{
   m_nPacketID  = IVCP_ID_Play_FrameInfo;
   m_nCameraUID = 0;
   m_nStreamID  = 0;
   m_nObjectNum = 0;
   m_pObjects   = NULL;
}
CIVCPPlayFrameInfo::~CIVCPPlayFrameInfo ()
{
   ClearObjects ();
}
int CIVCPPlayFrameInfo::ReadXML (CXMLIO* pIO)
{
   int nObjectNum = 0;
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Frame"))
   {
      xmlNode.Attribute (TYPE_ID (int)     , "UID"     , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int)     , "StreamID", &m_nStreamID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "FrmTime" ,  &m_ftFrameTime);
      xmlNode.Attribute (TYPE_ID (int)     , "ObjNum"  ,  &nObjectNum);

      xmlNode.Attribute (TYPE_ID (xint)   , "CamState"  , &m_nCameraState);
      xmlNode.Attribute (TYPE_ID (xint)   , "PTZState"  , &m_nPTZState);
      xmlNode.Attribute (TYPE_ID (xint)   , "PTZStateEx", &m_nPTZStateEx);
      xmlNode.Attribute (TYPE_ID (float)  , "FrameRate" , &m_fFrameRate);
      xmlNode.Attribute (TYPE_ID (int)    , "Width"     , &m_nProcWidth);
      xmlNode.Attribute (TYPE_ID (int)    , "Height"    , &m_nProcHeight);

      ClearObjects ();
      m_nObjectNum = 0;
      if (nObjectNum > 0)
      {
         m_pObjects = new CIVCPPlayFrameObject[nObjectNum];
         for (m_nObjectNum=0; m_nObjectNum<nObjectNum; m_nObjectNum++)
         {
            if (m_pObjects[m_nObjectNum].ReadXML(pIO) != DONE)
               break;
         }
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPPlayFrameInfo::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Frame"))
   {
      xmlNode.Attribute (TYPE_ID (int)     , "UID"     , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int)     , "StreamID", &m_nStreamID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "FrmTime" , &m_ftFrameTime);
      xmlNode.Attribute (TYPE_ID (int)     , "ObjNum"  , &m_nObjectNum);

      xmlNode.Attribute (TYPE_ID (xint)   , "CamState"  , &m_nCameraState);
      xmlNode.Attribute (TYPE_ID (xint)   , "PTZState"  , &m_nPTZState);
      xmlNode.Attribute (TYPE_ID (xint)   , "PTZStateEx", &m_nPTZStateEx);
      xmlNode.Attribute (TYPE_ID (float)  , "FrameRate" , &m_fFrameRate);
      xmlNode.Attribute (TYPE_ID (int)    , "Width"     , &m_nProcWidth);
      xmlNode.Attribute (TYPE_ID (int)    , "Height"    , &m_nProcHeight);

      if (m_pObjects)
      {
         for (int i=0; i<m_nObjectNum; i++)
         {
            m_pObjects[i].WriteXML(pIO);
         }
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
void CIVCPPlayFrameInfo::ClearObjects ()
{
   if (m_pObjects)
   {
      delete []m_pObjects;
      m_pObjects = NULL;
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPPlayPTZDataInfo
//
///////////////////////////////////////////////////////////////////////////////

CIVCPPlayPTZDataInfo::CIVCPPlayPTZDataInfo()
{
   m_nPacketID   = IVCP_ID_Play_PtzDataInfo;
   m_nCameraUID  = 0;
   m_fTiltAngle  = 0.0f;   // Pan 각도 : 반시계 방향이 증가하는 방향이다. 단위(degree, 0~360도)
   m_fTiltAngle  = 0.0f;   // Tilt 각도 : 아래로 향하는 방향이 증가하는 방향임. 단위(degree, 0~360도)
   m_fZoomFactor = 1.0f;   // Zoom 값 : 현재 줌 위치를 나타냄. 18배줌 카메라이면 1~18사이의 값을 가짐.
   m_fFOV        = 60.0f;  // FOV (Field Of View) : Pan 축으로의 화각을 나타냄. 단위(degree, 0~360도)
}
int CIVCPPlayPTZDataInfo::ReadXML  (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PtzData")) 
   {
      xmlNode.Attribute (TYPE_ID (int),      "UID",  &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int)     , "StreamID", &m_nStreamID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "Time", &m_ftFrameTime);
      xmlNode.Attribute (TYPE_ID (float),    "Pan",  &m_fPanAngle);
      xmlNode.Attribute (TYPE_ID (float),    "Tilt", &m_fTiltAngle);
      xmlNode.Attribute (TYPE_ID (float),    "Zoom", &m_fZoomFactor);
      xmlNode.Attribute (TYPE_ID (float),    "FOV",  &m_fFOV);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPPlayPTZDataInfo::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PtzData")) 
   {
      xmlNode.Attribute (TYPE_ID (int),      "UID",  &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int)     , "StreamID", &m_nStreamID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "Time", &m_ftFrameTime);
      xmlNode.Attribute (TYPE_ID (float),    "Pan",  &m_fPanAngle);
      xmlNode.Attribute (TYPE_ID (float),    "Tilt", &m_fTiltAngle);
      xmlNode.Attribute (TYPE_ID (float),    "Zoom", &m_fZoomFactor);
      xmlNode.Attribute (TYPE_ID (float),    "FOV",  &m_fFOV);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (PLAY) CIVCPPlayPresetIdInfo
//
///////////////////////////////////////////////////////////////////////////////

CIVCPPlayPresetIdInfo::CIVCPPlayPresetIdInfo ()
{
   m_nPacketID    = IVCP_ID_Play_PresetIdInfo;
   m_nCameraUID   = 0;
   m_nStreamID    = 0;
   m_nCurPresetID = 0;
}
int CIVCPPlayPresetIdInfo::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PresetID"))
   {
      xmlNode.Attribute (TYPE_ID (int)     , "UID"     , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int)     , "StreamID", &m_nStreamID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "FrmTime" , &m_ftFrameTime);
      xmlNode.Attribute (TYPE_ID (int)     , "PresetID", &m_nCurPresetID);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPPlayPresetIdInfo::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PresetID"))
   {
      xmlNode.Attribute (TYPE_ID (int)     , "UID"     , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int)     , "StreamID", &m_nStreamID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "FrmTime" , &m_ftFrameTime);
      xmlNode.Attribute (TYPE_ID (int)     , "PresetID", &m_nCurPresetID);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (PLAY) CIVCPPlayEventZoneInfo
//
///////////////////////////////////////////////////////////////////////////////

CIVCPPlayEventZoneInfo::CIVCPPlayEventZoneInfo ()
{
   m_nPacketID   = IVCP_ID_Play_EventZoneInfo;
   m_nCameraUID  = 0;
   m_nStreamID   = 0;
   m_nZoneNum    = 0;
   m_pEventZones = NULL;
}
CIVCPPlayEventZoneInfo::~CIVCPPlayEventZoneInfo ()
{
   ClearEventZones ();
}
void CIVCPPlayEventZoneInfo::ClearEventZones ()
{
   if (m_pEventZones)
   {
      delete []m_pEventZones;
      m_pEventZones = NULL;
   }
}
int CIVCPPlayEventZoneInfo::ReadXML (CXMLIO* pIO)
{
   int nZoneNum = 0;
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("EventZoneInfo"))
   {
      xmlNode.Attribute (TYPE_ID (int)     , "UID"     , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int)     , "StreamID", &m_nStreamID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "FrmTime" , &m_ftFrameTime);
      xmlNode.Attribute (TYPE_ID (int)     , "Width"   , &m_nWidth);
      xmlNode.Attribute (TYPE_ID (int)     , "Height"  , &m_nHeight);
      xmlNode.Attribute (TYPE_ID (int)     , "ZoneNum" , &nZoneNum);
      ClearEventZones ();
      m_nZoneNum = 0;
      if (nZoneNum > 0)
      {
         m_pEventZones = new CIVCPEventZone[nZoneNum];
         for (m_nZoneNum=0; m_nZoneNum<nZoneNum; m_nZoneNum++)
         {
            if (m_pEventZones[m_nZoneNum].ReadXML(pIO) != DONE)
               break;
         }
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPPlayEventZoneInfo::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("EventZoneInfo"))
   {
      xmlNode.Attribute (TYPE_ID (int)     , "UID"     , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int)     , "StreamID", &m_nStreamID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "FrmTime" , &m_ftFrameTime);
      xmlNode.Attribute (TYPE_ID (int)     , "Width"   , &m_nWidth);
      xmlNode.Attribute (TYPE_ID (int)     , "Height"  , &m_nHeight);
      xmlNode.Attribute (TYPE_ID (int)     , "ZoneNum" , &m_nZoneNum);
      if (m_pEventZones)
      {
         for (int i=0; i<m_nZoneNum; i++)
         {
            m_pEventZones[i].WriteXML(pIO);
         }
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (PLAY) CIVCPPlayEventZoneCountInfo
//
///////////////////////////////////////////////////////////////////////////////

CIVCPPlayEventZoneCountInfo::CIVCPPlayEventZoneCountInfo ()
{
   m_nPacketID   = IVCP_ID_Play_EventZoneCount;
   m_nCameraUID  = 0;
   m_nZoneNum    = 0;
   m_pZoneCounts = NULL;
}
CIVCPPlayEventZoneCountInfo::~CIVCPPlayEventZoneCountInfo ()
{
   ClearZoneCounts ();
}
int CIVCPPlayEventZoneCountInfo::ReadXML (CXMLIO* pIO)
{
   int nZoneNum = 0;
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("ZoneCnt"))
   {
      xmlNode.Attribute (TYPE_ID (int)     , "UID"     , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int)     , "StreamID", &m_nStreamID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "FrmTime" , &m_ftFrameTime);
      xmlNode.Attribute (TYPE_ID (int)     , "ZoneNum" , &nZoneNum);

      ClearZoneCounts ();
      m_nZoneNum = 0;
      if (nZoneNum > 0)
      {
         m_pZoneCounts = new CIVCPEventZoneCount[nZoneNum];
         for (m_nZoneNum=0; m_nZoneNum<nZoneNum; m_nZoneNum++)
         {
            if (m_pZoneCounts[m_nZoneNum].ReadXML(pIO) != DONE)
               break;
         }
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPPlayEventZoneCountInfo::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("ZoneCnt"))
   {
      xmlNode.Attribute (TYPE_ID (int)     , "UID"     , &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int)     , "StreamID", &m_nStreamID);
      xmlNode.Attribute (TYPE_ID (FILETIME), "FrmTime" , &m_ftFrameTime);
      xmlNode.Attribute (TYPE_ID (int)     , "ZoneNum" , &m_nZoneNum);

      if (m_pZoneCounts)
      {
         for (int i=0; i<m_nZoneNum; i++)
         {
            m_pZoneCounts[i].WriteXML(pIO);
         }
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
void CIVCPPlayEventZoneCountInfo::ClearZoneCounts ()
{
   if (m_pZoneCounts)
   {
      delete []m_pZoneCounts;
      m_pZoneCounts = NULL;
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// (DATA) CIVCPPlayVideoStreamData
//
///////////////////////////////////////////////////////////////////////////////

CIVCPPlayVideoStreamData::CIVCPPlayVideoStreamData () 
{
   m_nPacketID = IVCP_ID_Play_VideoData;
   m_nCameraUID = 0;
   m_nStreamID = 0;
   m_nCodecID = 0;
   m_nWidth = 0;
   m_nHeight = 0;
   m_fFrameRate = 0.0f;
   m_nFrameFlag = 0;
   m_pBodyBuffer = NULL;
}
CIVCPPlayVideoStreamData::~CIVCPPlayVideoStreamData ()
{
   if (m_pBodyBuffer) free(m_pBodyBuffer);
}
void CIVCPPlayVideoStreamData::ParseBodyBuffer ()
{
   char* resString = m_pBodyBuffer;
   int nLen = m_nDataLen - 1; // 마지막은 '\0';
   int nCount = 0;
   int i = 0;
   while ((i < nLen) && (nCount < 8))
   {
      while ((i < nLen) && (resString[i] == ' ')) i++;
      switch (nCount)
      {
      case 0: // Stream Index
         m_nStreamID = atoi(&resString[i]); 
         break;
      case 1: // Codec
         m_nCodecID = atoi(&resString[i]); 
         break;
      case 2: // Width
         m_nWidth = atoi(&resString[i]); 
         break;
      case 3: // Height
         m_nHeight = atoi(&resString[i]); 
         break;
      case 4: // m_fFrameRate
         m_fFrameRate = (float)atof(&resString[i]); 
         break;
      case 5: // FrameFlag
         m_nFrameFlag = atoi(&resString[i]); 
         break;
      case 6: // FrameTime
         ivcpGetFileTimeFromString (m_ftFrameTime, &resString[i]);
         break;
      case 7: // PlayTime
         ivcpGetFileTimeFromString (m_ftPlayTime, &resString[i]);
         break;
      }
      nCount++;
      while ((i < nLen) && (resString[i] != ' ')) i++;
      if (resString[i] != ' ')
         break;
   }
}
void CIVCPPlayVideoStreamData::MakeBodyBuffer (byte *pEncodedBuff, int nEncodedLen)
{
   // "0 50 320 240 7.0 1" (StreamID Codec Quality Width Height FrameRate FrameFlag FrameTime PlayTime)
   StringA strDesc;
#ifdef __WIN32
   SYSTEMTIME st, pt;
#ifdef __AFXMFC
   FileTimeToSystemTime (&m_ftFrameTime, &st);
   FileTimeToSystemTime (&m_ftPlayTime, &pt);
   strDesc.Format("%d %d %d %d %.2f %d %04d%02d%02d%02d%02d%02d%03d %04d%02d%02d%02d%02d%02d%03d", m_nStreamID, m_nCodecID, m_nWidth, m_nHeight, m_fFrameRate, m_nFrameFlag,
      st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds,
      pt.wYear, pt.wMonth, pt.wDay, pt.wHour, pt.wMinute, pt.wSecond, pt.wMilliseconds);
#else
   FileTimeToSystemTime (&m_ftFrameTime, &st);
   FileTimeToSystemTime (&m_ftPlayTime, &pt);
   strDesc.Format("%d %d %d %d %.2f %d %04d%02d%02d%02d%02d%02d%03d", m_nStreamID, m_nCodecID, m_nWidth, m_nHeight, m_fFrameRate, m_nFrameFlag,
      st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
   StringA strPlayTime;
   strPlayTime.Format(" %04d%02d%02d%02d%02d%02d%03d", pt.wYear, pt.wMonth, pt.wDay, pt.wHour, pt.wMinute, pt.wSecond, pt.wMilliseconds);
   strDesc += strPlayTime;
#endif
#else
   struct tm * ptm;
   time_t rawtime = m_ftFrameTime.dwHighDateTime;
   DWORD nMSec = (m_ftFrameTime.dwLowDateTime / 1000000);
   ptm = localtime ( &rawtime ); // LocalTime
   strDesc.Format("%d %d %d %d %.2f %d %04d%02d%02d%02d%02d%02d%03d", m_nStreamID, m_nCodecID, m_nWidth, m_nHeight, m_fFrameRate, m_nFrameFlag,
      ptm->tm_year, ptm->tm_mon, ptm->tm_mday, ptm->tm_hour, ptm->tm_min, ptm->tm_sec, nMSec);

   time_t playtime = m_ftPlayTime.dwHighDateTime;
   DWORD nPlayMSec = (m_ftPlayTime.dwLowDateTime / 1000000);
   ptm = localtime ( &playtime ); // LocalTime
   StringA strPlayTime;
   strPlayTime.Format(" %04d%02d%02d%02d%02d%02d%03d", ptm->tm_year, ptm->tm_mon, ptm->tm_mday, ptm->tm_hour, ptm->tm_min, ptm->tm_sec, nPlayMSec);
   strDesc += strPlayTime;
#endif
   
   m_nDataLen = strDesc.GetStringLength() + 1; // '\0' 추가
   m_nBodyLen = m_nDataLen + nEncodedLen;
   
   // 먼저 버퍼를 만든다.
   if (m_pBodyBuffer) delete m_pBodyBuffer;
   m_pBodyBuffer = (char*)malloc(m_nDataLen + nEncodedLen);
   if (m_pBodyBuffer)
   {
      memcpy(m_pBodyBuffer, (void*)strDesc, m_nDataLen);
      m_pBodyBuffer[m_nDataLen-1] = '\0';
      memcpy(m_pBodyBuffer+m_nDataLen, pEncodedBuff, nEncodedLen);
   }
}
int CIVCPPlayVideoStreamData::ReadFromPacket (CIVCPPacket& aPacket)
{
   if (aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody) 
   {
      m_nCameraUID  = aPacket.m_Header.m_nOptions;
      m_nDataLen    = aPacket.m_Header.m_nDataLen;
      m_nBodyLen    = aPacket.m_Header.m_nBodyLen;
      m_pBodyBuffer = aPacket.m_pBody;
      aPacket.m_pBody = NULL;

      ParseBodyBuffer(); // 바로 파싱한다.
      return (DONE);
   }
   return (1);
}
int CIVCPPlayVideoStreamData::WriteToPacket (CIVCPPacket& aPacket)
{
   if (aPacket.m_Header.m_nBodyLen > 0 && aPacket.m_pBody) 
   {
      aPacket.FreeBody();
   }
   if (m_pBodyBuffer)
   {
      aPacket.m_Header.m_nOptions = m_nCameraUID;
      aPacket.m_Header.m_nDataLen = m_nDataLen;
      aPacket.m_Header.m_nBodyLen = m_nBodyLen;
      aPacket.m_pBody = m_pBodyBuffer;
      m_pBodyBuffer = NULL;
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (PLAY) CIVCPPlayProgress
//
///////////////////////////////////////////////////////////////////////////////

CIVCPPlayProgress::CIVCPPlayProgress ()
{
   m_nPacketID  = IVCP_ID_Play_Progress;
   m_nCameraUID = 0;
   m_nStreamID  = 0;
   m_nStep      = 0;
   m_nParam     = 0;
}
int CIVCPPlayProgress::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Progress"))
   {
      xmlNode.Attribute (TYPE_ID (int), "UID",   &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int), "SID",   &m_nStreamID);
      xmlNode.Attribute (TYPE_ID (int), "Step",  &m_nStep);
      xmlNode.Attribute (TYPE_ID (int), "Param", &m_nParam);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPPlayProgress::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Progress"))
   {
      xmlNode.Attribute (TYPE_ID (int), "UID",   &m_nCameraUID);
      xmlNode.Attribute (TYPE_ID (int), "SID",   &m_nStreamID);
      xmlNode.Attribute (TYPE_ID (int), "Step",  &m_nStep);
      xmlNode.Attribute (TYPE_ID (int), "Param", &m_nParam);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchPlateReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPSearchFrameReq::CIVCPSearchFrameReq ()
{
   m_nPacketID    = IVCP_ID_Search_FrameReq;
   m_nSearchID    = 0;
   m_nChannelFlag = 0; 
}
int CIVCPSearchFrameReq::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("SearchFrame"))
   {
      StringA strValue;
      xmlNode.Attribute (TYPE_ID (int)     , "SrchID",   &m_nSearchID);
      xmlNode.Attribute (TYPE_ID (UINT64)  , "ChFlag",   &m_nChannelFlag);
      xmlNode.Attribute (TYPE_ID (FILETIME), "Start",    &m_ftStart);
      xmlNode.Attribute (TYPE_ID (FILETIME), "End",      &m_ftEnd);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPSearchFrameReq::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("SearchFrame"))
   {
      StringA strValue;
      xmlNode.Attribute (TYPE_ID (int)     , "SrchID",   &m_nSearchID);
      xmlNode.Attribute (TYPE_ID (UINT64)  , "ChFlag",   &m_nChannelFlag);
      xmlNode.Attribute (TYPE_ID (FILETIME), "Start",    &m_ftStart);
      xmlNode.Attribute (TYPE_ID (FILETIME), "End",      &m_ftEnd);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchEventReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPSearchEventReq::CIVCPSearchEventReq ()
{
   m_nPacketID    = IVCP_ID_Search_EventReq;
   m_nSearchID    = 0;
   m_nChannelFlag = 0; 
   m_nEvtFlag     = 0;
   m_nObjFlag     = 0;
   m_nEvtZoneNum  = 0;
   m_pEvtZoneIDs  = NULL;
   m_bThumbnail   = FALSE;
}
CIVCPSearchEventReq::~CIVCPSearchEventReq ()
{
   ClearEvtZoneIDs ();
}
int CIVCPSearchEventReq::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("SearchEvent"))
   {
      xmlNode.Attribute (TYPE_ID (int)     , "SrchID",  &m_nSearchID);
      xmlNode.Attribute (TYPE_ID (UINT64)  , "ChFlag",  &m_nChannelFlag);
      xmlNode.Attribute (TYPE_ID (FILETIME), "Start",   &m_ftStart);
      xmlNode.Attribute (TYPE_ID (FILETIME), "End",     &m_ftEnd);
      xmlNode.Attribute (TYPE_ID (int)     , "EvtFlag", &m_nEvtFlag);
      xmlNode.Attribute (TYPE_ID (int)     , "ObjFlag", &m_nObjFlag);
      xmlNode.Attribute (TYPE_ID (int)     , "EZNum",   &m_nEvtZoneNum);
      ClearEvtZoneIDs ();
      if (m_nEvtZoneNum > 0)
      {
         m_pEvtZoneIDs = new int[m_nEvtZoneNum];
         xmlNode.Attribute (TYPE_ID (int)     , "EZIDs",   &m_pEvtZoneIDs, m_nEvtZoneNum);
      }
      xmlNode.Attribute (TYPE_ID (BOOL)    , "Thumbnail",  &m_bThumbnail);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPSearchEventReq::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("SearchEvent"))
   {
      xmlNode.Attribute (TYPE_ID (int)     , "SrchID",  &m_nSearchID);
      xmlNode.Attribute (TYPE_ID (UINT64)  , "ChFlag",  &m_nChannelFlag);
      xmlNode.Attribute (TYPE_ID (FILETIME), "Start",   &m_ftStart);
      xmlNode.Attribute (TYPE_ID (FILETIME), "End",     &m_ftEnd);
      xmlNode.Attribute (TYPE_ID (int)     , "EvtFlag", &m_nEvtFlag);
      xmlNode.Attribute (TYPE_ID (int)     , "ObjFlag", &m_nObjFlag);
      xmlNode.Attribute (TYPE_ID (int)     , "EZNum",   &m_nEvtZoneNum);
      if (m_nEvtZoneNum > 0)
      {
         xmlNode.Attribute (TYPE_ID (int)     , "EZIDs",   &m_pEvtZoneIDs, m_nEvtZoneNum);
      }
      xmlNode.Attribute (TYPE_ID (BOOL)    , "Thumbnail",  &m_bThumbnail);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
void CIVCPSearchEventReq::ClearEvtZoneIDs ()
{
   if (m_pEvtZoneIDs)
   {
      delete []m_pEvtZoneIDs;
      m_pEvtZoneIDs = NULL;
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchObjectReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPSearchColorFilter::CIVCPSearchColorFilter()
{
   m_nColorType = 0;
   m_nColorMode = 0;
   m_nColorSensibility = 0;
   m_nColorNum = 0;
   for (int i=0; i<SEARCH_SELECT_COLOR_MAX; i++)
   {
      m_crColor[i] = 0;    // 대표 색상
      m_nPercent[i] = 0;   // 점유률
   }
   m_nWidth = m_nHeight = m_nDepth = 0;
   m_nHistogramLen = 0;
   m_pHistogramData = NULL;
}
CIVCPSearchColorFilter::~CIVCPSearchColorFilter()
{
   if (m_pHistogramData) delete ((byte*)m_pHistogramData);
}
void CIVCPSearchColorFilter::SetHistogramData(void *pBuffer, int nLen, int nWidth, int nHeight, int nDepth)
{
   // 먼저 삭제한다.
   if (m_pHistogramData) delete ((byte*)m_pHistogramData);
   m_pHistogramData = NULL;
   m_nHistogramLen = 0;
   m_nWidth = m_nHeight = m_nDepth = 0;

   if (pBuffer == NULL) return;

   // 히스토그램을 복사한다.
   if ((nLen > 0) && (pBuffer != NULL))
   {
      m_nWidth = nWidth;
      m_nHeight = nHeight;
      m_nDepth = nDepth;

      m_nHistogramLen = nLen;
      m_pHistogramData = new byte [m_nHistogramLen];
      memcpy (m_pHistogramData, pBuffer, m_nHistogramLen);
   }
}
BOOL CIVCPSearchColorFilter::GetHistogramData(void **pBuffer, int &nLen, int &nWidth, int &nHeight, int &nDepth)
{
   if (m_nHistogramLen == 0) return FALSE;
   //if ((m_nWidth == 0) || (m_nHeight == 0) || (m_nDepth == 0)) return FALSE;

   nWidth = m_nWidth;
   nHeight = m_nHeight;
   nDepth = m_nDepth;

   nLen = m_nHistogramLen;
   *pBuffer = ((void*)m_pHistogramData);
   return TRUE;
}
int CIVCPSearchColorFilter::ReadXML (CXMLIO* pIO)
{
   StringA strKey;
   CXMLNode xmlNode(pIO);
   if (xmlNode.Start("ColorFilter"))
   {
      xmlNode.Attribute (TYPE_ID (uint), "ColorType",   &m_nColorType);
      xmlNode.Attribute (TYPE_ID (uint), "ColorMode",   &m_nColorMode);
      xmlNode.Attribute (TYPE_ID (uint), "Sensibility", &m_nColorSensibility);
      xmlNode.Attribute (TYPE_ID (uint), "ColorNum",    &m_nColorNum);
      for (int i=0; i<m_nColorNum; i++)
      {
         strKey.Format("CR%02d", i);
         xmlNode.Attribute(TYPE_ID (uint), strKey, &m_crColor[i]);
         strKey.Format("PE%02d", i);
         xmlNode.Attribute(TYPE_ID (uint), strKey, &m_nPercent[i]);
      }
      xmlNode.Attribute (TYPE_ID (int)  , "HistoLen",  &m_nHistogramLen);
      if (m_nHistogramLen > 0) 
      {
         xmlNode.Attribute (TYPE_ID (ushort), "Width" , &m_nWidth);
         xmlNode.Attribute (TYPE_ID (ushort), "Height", &m_nHeight);
         xmlNode.Attribute (TYPE_ID (ushort), "Depth" , &m_nDepth);
         if (m_pHistogramData) delete ((byte*)m_pHistogramData);
         m_pHistogramData = new byte [m_nHistogramLen];
         xmlNode.Attribute (TYPE_ID (XBYTE), "HistoData", m_pHistogramData, m_nHistogramLen);
      }
      xmlNode.End();
   }
   else
      return FALSE;
   return TRUE;
}
int CIVCPSearchColorFilter::WriteXML (CXMLIO* pIO)
{
   StringA strKey;
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start("ColorFilter"))
   {
      xmlNode.Attribute (TYPE_ID (uint), "ColorType",   &m_nColorType);
      xmlNode.Attribute (TYPE_ID (uint), "ColorMode",   &m_nColorMode);
      xmlNode.Attribute (TYPE_ID (uint), "Sensibility", &m_nColorSensibility);
      xmlNode.Attribute (TYPE_ID (uint), "ColorNum",    &m_nColorNum);
      for (int i=0; i<m_nColorNum; i++)
      {
         strKey.Format("CR%02d", i);
         xmlNode.Attribute(TYPE_ID (uint), strKey, &m_crColor[i]);
         strKey.Format("PE%02d", i);
         xmlNode.Attribute(TYPE_ID (uint), strKey, &m_nPercent[i]);
      }
      xmlNode.Attribute (TYPE_ID (int)  , "HistoLen",  &m_nHistogramLen);
      if (m_nHistogramLen > 0) 
      {
         xmlNode.Attribute (TYPE_ID (ushort), "Width" , &m_nWidth);
         xmlNode.Attribute (TYPE_ID (ushort), "Height", &m_nHeight);
         xmlNode.Attribute (TYPE_ID (ushort), "Depth" , &m_nDepth);
         xmlNode.Attribute (TYPE_ID (XBYTE), "HistoData", m_pHistogramData, m_nHistogramLen);
      }
      xmlNode.End ();
   }
   else
      return FALSE;
   return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

CIVCPSearchObjectFilter::CIVCPSearchObjectFilter (   )
{
   m_nOptions = FILTER_ENABLE         |
                  USE_WIDTH             |
                  USE_HEIGHT            |
                  USE_AREA              |
                  USE_ASPECT_RATIO      |
                  USE_AXIS_LENGTH_RATIO |
                  USE_MAJOR_AXIS_LENGTH |
                  USE_MINOR_AXIS_LENGTH |
                  USE_NORMALIZED_SPEED  |
                  USE_SPEED             |
                  USE_REAL_DISTANCE;

   m_fMinWidth               = 0.0f;
   m_fMaxWidth               = 1000.0f;
   m_fMinHeight              = 0.0f;
   m_fMaxHeight              = 1000.0f;
   m_fMinArea                = 0.0f;
   m_fMaxArea                = 1000000.0f;
   m_fMinAspectRatio         = 0.1f;
   m_fMaxAspectRatio         = 10.0f;
   m_fMinAxisLengthRatio     = 1.0f;
   m_fMaxAxisLengthRatio     = 10.0f;
   m_fMinMajorAxisLength     = 0.0f;
   m_fMaxMajorAxisLength     = 1000.0f;
   m_fMinMinorAxisLength     = 0.0f;
   m_fMaxMinorAxisLength     = 1000.0f;
   m_fMinNorSpeed            = 0.0f;
   m_fMaxNorSpeed            = 1000.0f;
   m_fMinSpeed               = 0.0f;
   m_fMaxSpeed               = 1000.0f;
   m_fMinRealArea            = 0.0f;
   m_fMaxRealArea            = 1000000.0f;
   m_fMinRealDistance        = 0.0f;
   m_fMaxRealDistance        = 10000.0f;
   m_fMinRealWidth           = 0.0f;
   m_fMaxRealWidth           = 1000.0f;
   m_fMinRealHeight          = 0.0f;
   m_fMaxRealHeight          = 1000.0f;
   m_fMinRealMajorAxisLength = 0.0f;
   m_fMaxRealMajorAxisLength = 1000.0f;
   m_fMinRealMinorAxisLength = 0.0f;
   m_fMaxRealMinorAxisLength = 1000.0f;
   m_fMinRealSpeed           = 0.0f;
   m_fMaxRealSpeed           = 1000.0f;
}
int CIVCPSearchObjectFilter::ReadXML (CXMLIO* pIO)
{
   static CIVCPSearchObjectFilter dv; // defalut value (기본값과 같으면 xml로 부터 읽거나 저장하지 않음)

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("ObjectFilter"))
   {
      xmlNode.Attribute (TYPE_ID (xint) , "Options"                , &m_nOptions                , &dv.m_nOptions               );
      xmlNode.Attribute (TYPE_ID (float), "MinWidth"               , &m_fMinWidth               , &dv.m_fMinWidth              );
      xmlNode.Attribute (TYPE_ID (float), "MaxWidth"               , &m_fMaxWidth               , &dv.m_fMaxWidth              );
      xmlNode.Attribute (TYPE_ID (float), "MinHeight"              , &m_fMinHeight              , &dv.m_fMinHeight             );
      xmlNode.Attribute (TYPE_ID (float), "MaxHeight"              , &m_fMaxHeight              , &dv.m_fMaxHeight             );
      xmlNode.Attribute (TYPE_ID (float), "MinArea"                , &m_fMinArea                , &dv.m_fMinArea               );
      xmlNode.Attribute (TYPE_ID (float), "MaxArea"                , &m_fMaxArea                , &dv.m_fMaxArea               );
      xmlNode.Attribute (TYPE_ID (float), "MinAspectRatio"         , &m_fMinAspectRatio         , &dv.m_fMinAspectRatio        );
      xmlNode.Attribute (TYPE_ID (float), "MaxAspectRatio"         , &m_fMaxAspectRatio         , &dv.m_fMaxAspectRatio        );
      xmlNode.Attribute (TYPE_ID (float), "MinAxisLengthRatio"     , &m_fMinAxisLengthRatio     , &dv.m_fMinAxisLengthRatio    );
      xmlNode.Attribute (TYPE_ID (float), "MaxAxisLengthRatio"     , &m_fMaxAxisLengthRatio     , &dv.m_fMaxAxisLengthRatio    );
      xmlNode.Attribute (TYPE_ID (float), "MinMajorAxisLength"     , &m_fMinMajorAxisLength     , &dv.m_fMinMajorAxisLength    );
      xmlNode.Attribute (TYPE_ID (float), "MaxMajorAxisLength"     , &m_fMaxMajorAxisLength     , &dv.m_fMaxMajorAxisLength    );
      xmlNode.Attribute (TYPE_ID (float), "MinMinorAxisLength"     , &m_fMinMinorAxisLength     , &dv.m_fMinMinorAxisLength    );
      xmlNode.Attribute (TYPE_ID (float), "MaxMinorAxisLength"     , &m_fMaxMinorAxisLength     , &dv.m_fMaxMinorAxisLength    );
      xmlNode.Attribute (TYPE_ID (float), "MinNorSpeed"            , &m_fMinNorSpeed            , &dv.m_fMinNorSpeed           );
      xmlNode.Attribute (TYPE_ID (float), "MaxNorSpeed"            , &m_fMaxNorSpeed            , &dv.m_fMaxNorSpeed           );
      xmlNode.Attribute (TYPE_ID (float), "MinSpeed"               , &m_fMinSpeed               , &dv.m_fMinSpeed              );
      xmlNode.Attribute (TYPE_ID (float), "MaxSpeed"               , &m_fMaxSpeed               , &dv.m_fMaxSpeed              );
      xmlNode.Attribute (TYPE_ID (float), "MinRealArea"            , &m_fMinRealArea            , &dv.m_fMinRealArea           );
      xmlNode.Attribute (TYPE_ID (float), "MaxRealArea"            , &m_fMaxRealArea            , &dv.m_fMaxRealArea           );
      xmlNode.Attribute (TYPE_ID (float), "MinRealDistance"        , &m_fMinRealDistance        , &dv.m_fMinRealDistance       );
      xmlNode.Attribute (TYPE_ID (float), "MaxRealDistance"        , &m_fMaxRealDistance        , &dv.m_fMaxRealDistance       );
      xmlNode.Attribute (TYPE_ID (float), "MinRealWidth"           , &m_fMinRealWidth           , &dv.m_fMinRealWidth          );
      xmlNode.Attribute (TYPE_ID (float), "MaxRealWidth"           , &m_fMaxRealWidth           , &dv.m_fMaxRealWidth          );
      xmlNode.Attribute (TYPE_ID (float), "MinRealHeight"          , &m_fMinRealHeight          , &dv.m_fMinRealHeight         );
      xmlNode.Attribute (TYPE_ID (float), "MaxRealHeight"          , &m_fMaxRealHeight          , &dv.m_fMaxRealHeight         );
      xmlNode.Attribute (TYPE_ID (float), "MinRealMajorAxisLength" , &m_fMinRealMajorAxisLength , &dv.m_fMinRealMajorAxisLength);
      xmlNode.Attribute (TYPE_ID (float), "MaxRealMajorAxisLength" , &m_fMaxRealMajorAxisLength , &dv.m_fMaxRealMajorAxisLength);
      xmlNode.Attribute (TYPE_ID (float), "MinRealMinorAxisLength" , &m_fMinRealMinorAxisLength , &dv.m_fMinRealMinorAxisLength);
      xmlNode.Attribute (TYPE_ID (float), "MaxRealMinorAxisLength" , &m_fMaxRealMinorAxisLength , &dv.m_fMaxRealMinorAxisLength);
      xmlNode.Attribute (TYPE_ID (float), "MinRealSpeed"           , &m_fMinRealSpeed           , &dv.m_fMinRealSpeed          );
      xmlNode.Attribute (TYPE_ID (float), "MaxRealSpeed"           , &m_fMaxRealSpeed           , &dv.m_fMaxRealSpeed          );
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}
int CIVCPSearchObjectFilter::WriteXML (CXMLIO* pIO)
{
   static CIVCPSearchObjectFilter dv; // defalut value (기본값과 같으면 xml로 부터 읽거나 저장하지 않음. 

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("ObjectFilter"))
   {
      xmlNode.Attribute (TYPE_ID (xint) , "Options"                , &m_nOptions                , &dv.m_nOptions               );
      xmlNode.Attribute (TYPE_ID (float), "MinWidth"               , &m_fMinWidth               , &dv.m_fMinWidth              );
      xmlNode.Attribute (TYPE_ID (float), "MaxWidth"               , &m_fMaxWidth               , &dv.m_fMaxWidth              );
      xmlNode.Attribute (TYPE_ID (float), "MinHeight"              , &m_fMinHeight              , &dv.m_fMinHeight             );
      xmlNode.Attribute (TYPE_ID (float), "MaxHeight"              , &m_fMaxHeight              , &dv.m_fMaxHeight             );
      xmlNode.Attribute (TYPE_ID (float), "MinArea"                , &m_fMinArea                , &dv.m_fMinArea               );
      xmlNode.Attribute (TYPE_ID (float), "MaxArea"                , &m_fMaxArea                , &dv.m_fMaxArea               );
      xmlNode.Attribute (TYPE_ID (float), "MinAspectRatio"         , &m_fMinAspectRatio         , &dv.m_fMinAspectRatio        );
      xmlNode.Attribute (TYPE_ID (float), "MaxAspectRatio"         , &m_fMaxAspectRatio         , &dv.m_fMaxAspectRatio        );
      xmlNode.Attribute (TYPE_ID (float), "MinAxisLengthRatio"     , &m_fMinAxisLengthRatio     , &dv.m_fMinAxisLengthRatio    );
      xmlNode.Attribute (TYPE_ID (float), "MaxAxisLengthRatio"     , &m_fMaxAxisLengthRatio     , &dv.m_fMaxAxisLengthRatio    );
      xmlNode.Attribute (TYPE_ID (float), "MinMajorAxisLength"     , &m_fMinMajorAxisLength     , &dv.m_fMinMajorAxisLength    );
      xmlNode.Attribute (TYPE_ID (float), "MaxMajorAxisLength"     , &m_fMaxMajorAxisLength     , &dv.m_fMaxMajorAxisLength    );
      xmlNode.Attribute (TYPE_ID (float), "MinMinorAxisLength"     , &m_fMinMinorAxisLength     , &dv.m_fMinMinorAxisLength    );
      xmlNode.Attribute (TYPE_ID (float), "MaxMinorAxisLength"     , &m_fMaxMinorAxisLength     , &dv.m_fMaxMinorAxisLength    );
      xmlNode.Attribute (TYPE_ID (float), "MinNorSpeed"            , &m_fMinNorSpeed            , &dv.m_fMinNorSpeed           );
      xmlNode.Attribute (TYPE_ID (float), "MaxNorSpeed"            , &m_fMaxNorSpeed            , &dv.m_fMaxNorSpeed           );
      xmlNode.Attribute (TYPE_ID (float), "MinSpeed"               , &m_fMinSpeed               , &dv.m_fMinSpeed              );
      xmlNode.Attribute (TYPE_ID (float), "MaxSpeed"               , &m_fMaxSpeed               , &dv.m_fMaxSpeed              );
      xmlNode.Attribute (TYPE_ID (float), "MinRealArea"            , &m_fMinRealArea            , &dv.m_fMinRealArea           );
      xmlNode.Attribute (TYPE_ID (float), "MaxRealArea"            , &m_fMaxRealArea            , &dv.m_fMaxRealArea           );
      xmlNode.Attribute (TYPE_ID (float), "MinRealDistance"        , &m_fMinRealDistance        , &dv.m_fMinRealDistance       );
      xmlNode.Attribute (TYPE_ID (float), "MaxRealDistance"        , &m_fMaxRealDistance        , &dv.m_fMaxRealDistance       );
      xmlNode.Attribute (TYPE_ID (float), "MinRealWidth"           , &m_fMinRealWidth           , &dv.m_fMinRealWidth          );
      xmlNode.Attribute (TYPE_ID (float), "MaxRealWidth"           , &m_fMaxRealWidth           , &dv.m_fMaxRealWidth          );
      xmlNode.Attribute (TYPE_ID (float), "MinRealHeight"          , &m_fMinRealHeight          , &dv.m_fMinRealHeight         );
      xmlNode.Attribute (TYPE_ID (float), "MaxRealHeight"          , &m_fMaxRealHeight          , &dv.m_fMaxRealHeight         );
      xmlNode.Attribute (TYPE_ID (float), "MinRealMajorAxisLength" , &m_fMinRealMajorAxisLength , &dv.m_fMinRealMajorAxisLength);
      xmlNode.Attribute (TYPE_ID (float), "MaxRealMajorAxisLength" , &m_fMaxRealMajorAxisLength , &dv.m_fMaxRealMajorAxisLength);
      xmlNode.Attribute (TYPE_ID (float), "MinRealMinorAxisLength" , &m_fMinRealMinorAxisLength , &dv.m_fMinRealMinorAxisLength);
      xmlNode.Attribute (TYPE_ID (float), "MaxRealMinorAxisLength" , &m_fMaxRealMinorAxisLength , &dv.m_fMaxRealMinorAxisLength);
      xmlNode.Attribute (TYPE_ID (float), "MinRealSpeed"           , &m_fMinRealSpeed           , &dv.m_fMinRealSpeed          );
      xmlNode.Attribute (TYPE_ID (float), "MaxRealSpeed"           , &m_fMaxRealSpeed           , &dv.m_fMaxRealSpeed          );
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////

CIVCPSearchObjectReq::CIVCPSearchObjectReq ()
{
   m_nPacketID    = IVCP_ID_Search_ObjectReq;
   m_nSearchID    = 0;
   m_nChannelFlag = 0;
   m_nCondFlag    = 0;   // 검색 조건에 대한 플래그
   m_nObjFlag     = 0;
   m_bThumbnail   = FALSE;
}
int CIVCPSearchObjectReq::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("SearchObject"))
   {
      xmlNode.Attribute (TYPE_ID (int)     , "SrchID",   &m_nSearchID);
      xmlNode.Attribute (TYPE_ID (UINT64)  , "ChFlag",   &m_nChannelFlag);
      xmlNode.Attribute (TYPE_ID (FILETIME), "Start",    &m_ftStart);
      xmlNode.Attribute (TYPE_ID (FILETIME), "End",      &m_ftEnd);
      xmlNode.Attribute (TYPE_ID (int)     , "CondFlag", &m_nCondFlag);
      xmlNode.Attribute (TYPE_ID (int)     , "ObjFlag",  &m_nObjFlag);
      m_ColorFilter.ReadXML(pIO);
      m_ObjectFilter.ReadXML(pIO);
      xmlNode.Attribute (TYPE_ID (BOOL)    , "Thumbnail",  &m_bThumbnail);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPSearchObjectReq::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("SearchObject"))
   {
      xmlNode.Attribute (TYPE_ID (int)     , "SrchID",   &m_nSearchID);
      xmlNode.Attribute (TYPE_ID (UINT64)  , "ChFlag",   &m_nChannelFlag);
      xmlNode.Attribute (TYPE_ID (FILETIME), "Start",    &m_ftStart);
      xmlNode.Attribute (TYPE_ID (FILETIME), "End",      &m_ftEnd);
      xmlNode.Attribute (TYPE_ID (int)     , "CondFlag", &m_nCondFlag);
      xmlNode.Attribute (TYPE_ID (int)     , "ObjFlag",  &m_nObjFlag);
      m_ColorFilter.WriteXML(pIO);
      m_ObjectFilter.WriteXML(pIO);
      xmlNode.Attribute (TYPE_ID (BOOL)    , "Thumbnail",  &m_bThumbnail);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchPlateReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPSearchPlateReq::CIVCPSearchPlateReq ()
{
   m_nPacketID    = IVCP_ID_Search_PlateReq;
   m_nSearchID    = 0;
   m_nChannelFlag = 0; 
}
int CIVCPSearchPlateReq::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("SearchPlate"))
   {
      StringA strValue;
      xmlNode.Attribute (TYPE_ID (int)     , "SrchID",   &m_nSearchID);
      xmlNode.Attribute (TYPE_ID (UINT64)  , "ChFlag",   &m_nChannelFlag);
      xmlNode.Attribute (TYPE_ID (FILETIME), "Start",    &m_ftStart);
      xmlNode.Attribute (TYPE_ID (FILETIME), "End",      &m_ftEnd);
      xmlNode.Attribute (TYPE_ID (StringA) , "PlateNum", &strValue);
      strcpy(m_strPlateNumber, strValue);
      xmlNode.Attribute (TYPE_ID (BOOL)    , "Thumbnail",&m_bThumbnail);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPSearchPlateReq::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("SearchPlate"))
   {
      StringA strValue;
      xmlNode.Attribute (TYPE_ID (int)     , "SrchID",   &m_nSearchID);
      xmlNode.Attribute (TYPE_ID (UINT64)  , "ChFlag",   &m_nChannelFlag);
      xmlNode.Attribute (TYPE_ID (FILETIME), "Start",    &m_ftStart);
      xmlNode.Attribute (TYPE_ID (FILETIME), "End",      &m_ftEnd);
      strValue = m_strPlateNumber;
      xmlNode.Attribute (TYPE_ID (StringA) , "PlateNum", &strValue);
      xmlNode.Attribute (TYPE_ID (BOOL)    , "Thumbnail",&m_bThumbnail);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchFaceReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPSearchFaceReq::CIVCPSearchFaceReq ()
{
   m_nPacketID    = IVCP_ID_Search_FaceReq;
   m_nSearchID    = 0;
   m_nChannelFlag = 0; 
}
int CIVCPSearchFaceReq::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("SearchFace"))
   {
      StringA strValue;
      xmlNode.Attribute (TYPE_ID (int)     , "SrchID",   &m_nSearchID);
      xmlNode.Attribute (TYPE_ID (UINT64)  , "ChFlag",   &m_nChannelFlag);
      xmlNode.Attribute (TYPE_ID (FILETIME), "Start",    &m_ftStart);
      xmlNode.Attribute (TYPE_ID (FILETIME), "End",      &m_ftEnd);
      xmlNode.Attribute (TYPE_ID (StringA) , "Name",     &strValue);
      strcpy(m_strName, strValue);
      xmlNode.Attribute (TYPE_ID (BOOL)    , "Thumbnail",&m_bThumbnail);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPSearchFaceReq::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("SearchFace"))
   {
      StringA strValue;
      xmlNode.Attribute (TYPE_ID (int)     , "SrchID",   &m_nSearchID);
      xmlNode.Attribute (TYPE_ID (UINT64)  , "ChFlag",   &m_nChannelFlag);
      xmlNode.Attribute (TYPE_ID (FILETIME), "Start",    &m_ftStart);
      xmlNode.Attribute (TYPE_ID (FILETIME), "End",      &m_ftEnd);
      strValue = m_strName;
      xmlNode.Attribute (TYPE_ID (StringA) , "Name",     &strValue);
      xmlNode.Attribute (TYPE_ID (BOOL)    , "Thumbnail",&m_bThumbnail);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchLogReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPSearchLogReq::CIVCPSearchLogReq ()
{
   m_nPacketID    = IVCP_ID_Search_LogReq;
   m_nSearchID    = 0;
   m_nChannelFlag = 0; 
}
int CIVCPSearchLogReq::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("SearchLog"))
   {
      StringA strValue;
      xmlNode.Attribute (TYPE_ID (int)     , "SrchID",    &m_nSearchID);
      xmlNode.Attribute (TYPE_ID (UINT64)  , "ChFlag",    &m_nChannelFlag);
      xmlNode.Attribute (TYPE_ID (FILETIME), "Start",     &m_ftStart);
      xmlNode.Attribute (TYPE_ID (FILETIME), "End",       &m_ftEnd);
      xmlNode.Attribute (TYPE_ID (StringA) , "Keyword",   &strValue);
      strcpy(m_strKeyword, strValue);
      xmlNode.Attribute (TYPE_ID (int)     , "Option",    &m_nOption);
      xmlNode.Attribute (TYPE_ID (int)     , "EvtFlag",   &m_nEvtFlag);
      xmlNode.Attribute (TYPE_ID (int)     , "ObjFlag",   &m_nObjFlag);
      xmlNode.Attribute (TYPE_ID (BOOL)    , "Thumbnail", &m_bThumbnail);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPSearchLogReq::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("SearchLog"))
   {
      StringA strValue;
      xmlNode.Attribute (TYPE_ID (int)     , "SrchID",    &m_nSearchID);
      xmlNode.Attribute (TYPE_ID (UINT64)  , "ChFlag",    &m_nChannelFlag);
      xmlNode.Attribute (TYPE_ID (FILETIME), "Start",     &m_ftStart);
      xmlNode.Attribute (TYPE_ID (FILETIME), "End",       &m_ftEnd);
      strValue = m_strKeyword;
      xmlNode.Attribute (TYPE_ID (StringA) , "Keyword",   &strValue);
      xmlNode.Attribute (TYPE_ID (int)     , "Option",    &m_nOption);
      xmlNode.Attribute (TYPE_ID (int)     , "EvtFlag",   &m_nEvtFlag);
      xmlNode.Attribute (TYPE_ID (int)     , "ObjFlag",   &m_nObjFlag);
      xmlNode.Attribute (TYPE_ID (BOOL)    , "Thumbnail", &m_bThumbnail);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchStopReq
//
///////////////////////////////////////////////////////////////////////////////

CIVCPSearchStopReq::CIVCPSearchStopReq ()
{
   m_nPacketID = IVCP_ID_Search_StopReq;
   m_nSearchID = 0;
}
int CIVCPSearchStopReq::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("SearchStop"))
   {
      xmlNode.Attribute (TYPE_ID (int), "SrchID", &m_nSearchID);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPSearchStopReq::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("SearchStop"))
   {
      xmlNode.Attribute (TYPE_ID (int), "SrchID", &m_nSearchID);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchResultRes
//
///////////////////////////////////////////////////////////////////////////////

CIVCPSearchResultRes::CIVCPSearchResultRes ()
{
   m_nPacketID = 0;
   m_nSearchID = 0;
   m_nResult   = 0;
}
int CIVCPSearchResultRes::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("SearchRet"))
   {
      xmlNode.Attribute (TYPE_ID (int), "SrchID", &m_nSearchID);
      xmlNode.Attribute (TYPE_ID (int), "Result", &m_nResult);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPSearchResultRes::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("SearchRet"))
   {
      xmlNode.Attribute (TYPE_ID (int), "SrchID", &m_nSearchID);
      xmlNode.Attribute (TYPE_ID (int), "Result", &m_nResult);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchProgress
//
///////////////////////////////////////////////////////////////////////////////

CIVCPSearchProgress::CIVCPSearchProgress ()
{
   m_nPacketID = IVCP_ID_Search_Progress;
   m_nSearchID = 0;
   m_nStep     = 0;
   m_nParam    = 0;
}
int CIVCPSearchProgress::ReadXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("SearchProgress"))
   {
      xmlNode.Attribute (TYPE_ID (int), "SrchID", &m_nSearchID);
      xmlNode.Attribute (TYPE_ID (int), "Step",   &m_nStep);
      xmlNode.Attribute (TYPE_ID (int), "Param",  &m_nParam);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
int CIVCPSearchProgress::WriteXML (CXMLIO* pIO)
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("SearchProgress"))
   {
      xmlNode.Attribute (TYPE_ID (int), "SrchID", &m_nSearchID);
      xmlNode.Attribute (TYPE_ID (int), "Step",   &m_nStep);
      xmlNode.Attribute (TYPE_ID (int), "Param",  &m_nParam);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}

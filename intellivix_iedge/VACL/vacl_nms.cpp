#include "vacl_conrgn.h"
#include "vacl_geometry.h"
#include "vacl_nms.h"

#if defined(__DEBUG_NMS)
#include "Win32CL/Win32CL.h"
extern int _DVX_NMS,_DVY_NMS;
extern CImageView* _DebugView_NMS;
#endif

 namespace VACL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Class: DetRegion
//
///////////////////////////////////////////////////////////////////////////////

 DetRegion::DetRegion (   )

{
   Clear (   );
}

 void DetRegion::Clear (   )

{
   AvgVotScore = 0.0f;
   MaxVotScore = 0.0f;
   Center.Clear (   );
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: NonmaximumSuppression_Island
//
///////////////////////////////////////////////////////////////////////////////

 NonmaximumSuppression_Island::NonmaximumSuppression_Island (   )
 
{
   VRRadius = 0;
}

 NonmaximumSuppression_Island::~NonmaximumSuppression_Island (   )

{
   Close (   );
}

 void NonmaximumSuppression_Island::Clear (   )

{
   if (!IsInitialized (   )) return;
   VotScrMap.Clear   (   );
   DetRgnMap.Clear   (   );
   DetRegions.Delete (   );
}

 void NonmaximumSuppression_Island::Close (   )
 
{
   VotScrMap.Delete  (   );
   DetRgnMap.Delete  (   );
   DetRegions.Delete (   );
}

 int NonmaximumSuppression_Island::GetDetRegions (GImage& s_image,int min_area)

{
   int i,x,y;
   
   int n_crs = LabelCRs (s_image,DetRgnMap);
   if (!n_crs) return (0);
   DRArray1D dr_array(n_crs + 1);
   for (i = 1; i <= n_crs; i++) {
      dr_array[i].X      = DetRgnMap.Width;
      dr_array[i].Y      = DetRgnMap.Height;
      dr_array[i].Width  = -1;
      dr_array[i].Height = -1;
   }
   FArray1D vss_array(dr_array.Length);
   FArray1D mvs_array(dr_array.Length);
   IArray1D dra_array(dr_array.Length);
   vss_array.Clear (   );
   mvs_array.Clear (   );
   dra_array.Clear (   );
   for (y = 0; y < DetRgnMap.Height; y++) {
      int*   l_dr_map = DetRgnMap[y];
      float* l_vs_map = VotScrMap[y];
      for (x = 0; x < DetRgnMap.Width; x++) {
         if (l_dr_map[x]) {
            i = l_dr_map[x];
            DetRegion* dr = &dr_array[i];
            if (x < dr->X     ) dr->X      = x;
            if (y < dr->Y     ) dr->Y      = y;
            if (x > dr->Width ) dr->Width  = x;
            if (y > dr->Height) dr->Height = y;
            if (l_vs_map[x] > mvs_array[i]) mvs_array[i] = l_vs_map[x];
            vss_array[i] += l_vs_map[x];
            dra_array[i]++;
         }
      }
   }
   for (i = 1; i <= n_crs; i++) {
      DetRegion* dr = &dr_array[i];
      dr->Center.X    = (dr->X + dr->Width ) / 2;
      dr->Center.Y    = (dr->Y + dr->Height) / 2;
      dr->Width       = dr->Width  - dr->X + 1;
      dr->Height      = dr->Height - dr->X + 1;
      dr->Area        = dra_array[i];
      dr->AvgVotScore = vss_array[i] / dr->Area;
      dr->MaxVotScore = mvs_array[i];
   }
   IArray1D cv_table(n_crs + 1);
   cv_table.Clear (   );
   int n_drs = 0;
   for (i = 1; i <= n_crs; i++) {
      if (dr_array[i].Area >= min_area) {
         n_drs++;
         cv_table[i] = n_drs;
      }
   }
   if (!n_drs) return (0);
   DetRegions.Create (n_drs + 1);
   for (i = 1; i <= n_crs; i++)
      if (cv_table[i]) DetRegions[cv_table[i]] = dr_array[i];
   ConvertCRMap (DetRgnMap,cv_table,DetRgnMap);
   return (n_drs);
}

 int NonmaximumSuppression_Island::GetDetRgnIdxNo (IBox2D& s_rect)

{
   IPoint2D cp;
   cp.X = s_rect.X + s_rect.Width  / 2;
   cp.Y = s_rect.Y + s_rect.Height / 2;
   return (GetDetRgnIdxNo (cp));
}

 int NonmaximumSuppression_Island::GetDetRgnIdxNo (IPoint2D& s_pos)

{
   int x,y;
   
   if (!DetRegions) return (0);
   int sx = s_pos.X - VRRadius;
   int sy = s_pos.Y - VRRadius;
   int ex = s_pos.X + VRRadius;
   int ey = s_pos.Y + VRRadius;
   if (sx < 0) sx = 0;
   if (sy < 0) sy = 0;
   if (ex > DetRgnMap.Width ) ex = DetRgnMap.Width;
   if (ey > DetRgnMap.Height) ey = DetRgnMap.Height;
   for (y = sy; y < ey; y++) {
      int* l_dr_map = DetRgnMap[y];
      for (x = sx; x < ex; x++) {
         if (l_dr_map[x]) return (l_dr_map[x]);
      }
   }
   return (0);
}

 float NonmaximumSuppression_Island::GetVotScrMapMean (   )

{
   int x,y;

   int   n   = 0;
   float sum = 0.0f;
   for (y = 0; y < VotScrMap.Height; y++) {
      float* l_vs_map = VotScrMap[y];
      for (x = 0; x < VotScrMap.Width; x++) {
         if (l_vs_map[x] > 0.0f) {
            sum += l_vs_map[x];
            n++;
         }
      }
   }
   float mean = 0.0f;
   if (n) mean = sum / n;
   return (mean);
}

 void NonmaximumSuppression_Island::Initialize (ISize2D& si_size,int vr_radius)
 
{
   Close (   );
   VRRadius = vr_radius;
   VotScrMap.Create (si_size.Width,si_size.Height);
   DetRgnMap.Create (si_size.Width,si_size.Height);
   VotScrMap.Clear  (   );
   DetRgnMap.Clear  (   );
}

 int NonmaximumSuppression_Island::IsInitialized (   )
 
{
   if (!VotScrMap) return (FALSE);
   else return (TRUE);
}

 int NonmaximumSuppression_Island::Vote (IBox2D& s_rect,float score)

{
   IPoint2D s_pos;
   s_pos.X = s_rect.X + s_rect.Width  / 2;
   s_pos.Y = s_rect.Y + s_rect.Height / 2;
   return (Vote (s_pos,score));
}

 int NonmaximumSuppression_Island::Vote (IPoint2D& s_pos,float score)
 
{
   int x,y;
   
   if (!IsInitialized (   )) return (1);
   int sx = s_pos.X - VRRadius;
   int sy = s_pos.Y - VRRadius;
   int ex = s_pos.X + VRRadius;
   int ey = s_pos.Y + VRRadius;
   CropRegion (VotScrMap.Width,VotScrMap.Height,sx,sy,ex,ey);
   for (y = sy; y < ey; y++) {
      float* l_vs_map = VotScrMap[y];
      for (x = sx; x < ex; x++)
         l_vs_map[x] += score;
   }
   return (DONE);
}

 int NonmaximumSuppression_Island::Perform (float a_vs_thrsld,float r_vs_thrsld,int min_area)
 
{
   DetRegions.Delete (   );
   if (!IsInitialized (   )) return (0);
   #if defined(__DEBUG_NMS)
   _DebugView_NMS->DrawImage (VotScrMap,_DVX_NMS,_DVY_NMS);
   _DVY_NMS += VotScrMap.Height;
   _DebugView_NMS->DrawText ("Voting Score Map",_DVX_NMS,_DVY_NMS,PC_GREEN);
   _DVY_NMS += 20;
   #endif
   float mean = GetVotScrMapMean (   );
   if (!mean) {
      DetRgnMap.Clear (   );
      return (0);
   }
   r_vs_thrsld *= mean;
   float thrsld = GetMaximum (a_vs_thrsld,r_vs_thrsld);
   GImage dr_image(VotScrMap.Width,VotScrMap.Height);
   Binarize (VotScrMap,thrsld,dr_image);
   #if defined(__DEBUG_NMS)
   _DebugView_NMS->DrawImage (dr_image,_DVX_NMS,_DVY_NMS);
   _DVY_NMS += dr_image.Height;
   _DebugView_NMS->DrawText ("Detection Regions",_DVX_NMS,_DVY_NMS,PC_GREEN);
   _DVY_NMS += 20;
   #endif
   return (GetDetRegions (dr_image,min_area));
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 int PerformPointNMS (FArray2D& s_array,int win_size,float sc_thrsld,GImage &d_image)

{
   int x,y,x2,y2;
   
   d_image.Clear (   );
   int n_points = 0;
   int hws = win_size / 2;
   int ex = s_array.Width  - hws;
   int ey = s_array.Height - hws;
   for (y = hws; y < ey; y++) {
      float* l_s_array = s_array[y];
      byte*  l_d_image = d_image[y];
      for (x = hws; x < ex; x++) {
         if (l_s_array[x] > sc_thrsld) {
            int sx2 = x - hws;
            int ex2 = x + hws;
            int sy2 = y - hws;
            int ey2 = y + hws;
            float value = l_s_array[x];
            for (y2 = sy2; y2 <= ey2; y2++) {
               float* l_s_array2 = s_array[y2];
               for (x2 = sx2; x2 <= ex2; x2++) {
                  if (l_s_array2[x2] > value) {
                     value = l_s_array2[x2];
                     goto escape;
                  }
               }
            }
            escape:
            if (value == l_s_array[x]) {
               l_d_image[x] = PG_WHITE;
               n_points++;
            }
         }
      }
   }
   d_image.SetBoundary (hws,PG_BLACK);
   return (n_points);
}

 int PerformRectNMS_Greedy (IB2DArray1D& s_rects,FArray1D& s_scores,float ov_thrsld,float ar_thrsld,IB2DArray1D& d_rects)

{
   int i,j,n;

   d_rects.Delete (   );
   if (!s_rects) return (0);
   OSArray1D ri_array(s_rects.Length);
   for (i = 0; i < s_rects.Length; i++) {
      ri_array[i].IndexNo = i;
      ri_array[i].Score   = s_scores[i];
   }
   qsort (ri_array,s_rects.Length,sizeof(ObjectScore),Compare_ObjectScore_Descending);
   BArray1D rm_array(s_rects.Length);
   rm_array.Clear (   );
   for (i = 0; i < s_rects.Length; i++) {
      if (!rm_array[i]) {
         IBox2D& rect1 = s_rects[ri_array[i].IndexNo];
         int area1 = rect1.Width * rect1.Height;
         for (j = 0; j < s_rects.Length; j++) {
            if (i != j && !rm_array[j]) {
               IBox2D& rect2 = s_rects[ri_array[j].IndexNo];
               int ov_area   = GetOverlapArea (rect1,rect2);
               int area2     = rect2.Width * rect2.Height;
               float ov_ratio = (float)ov_area / area2;
               float ar_ratio = GetMinimum ((float)area1 / area2,(float)area2 / area1);
               if (ov_ratio > ov_thrsld && ar_ratio > ar_thrsld) rm_array[j] = TRUE;
            }
         }
      }
   }
   for (i = n = 0; i < rm_array.Length; i++) if (!rm_array[i]) n++;
   if (!n) return (0);
   d_rects.Create (n);
   for (i = j = 0; i < rm_array.Length; i++)
      if (!rm_array[i]) d_rects[j++] = s_rects[ri_array[i].IndexNo];
   return (n);
}

 int PerformRectNMS_Island (ISize2D& si_size,IB2DArray1D& s_rects,FArray1D& s_scores,float sc_thrsld,int vr_radius,float a_vs_thrsld,float r_vs_thrsld,IB2DArray1D& d_rects,FArray1D& d_scores)

{
   int i,j;

   #if defined(__DEBUG_NMS)
   _DebugView_NMS->Clear (   );
   _DVX_NMS = _DVY_NMS = 0;
   #endif
   d_rects.Delete (   );
   NonmaximumSuppression_Island nms;
   nms.Initialize (si_size,vr_radius);
   for (i = 0; i < s_rects.Length; i++) {
      if (s_scores[i] > sc_thrsld) nms.Vote (s_rects[i],s_scores[i]);
   }
   int min_area = (int)(0.7f * vr_radius * vr_radius); // PARAMETERS
   int n = nms.Perform (a_vs_thrsld,r_vs_thrsld,min_area);
   #if defined(__DEBUG_NMS)
   _DebugView_NMS->Invalidate (   );
   #endif
   if (!n) return (0);
   n++;
   IArray1D w_array(n);
   IArray1D h_array(n);
   IArray1D n_array(n);
   FArray1D sc_array(n);
   w_array.Clear  (   );
   h_array.Clear  (   );
   n_array.Clear  (   );
   sc_array.Clear (   );
   for (i = 0; i < s_rects.Length; i++) {
      if (s_scores[i] > sc_thrsld) {
         j = nms.GetDetRgnIdxNo (s_rects[i]);
         if (j) {
            w_array[j] += s_rects[i].Width;
            h_array[j] += s_rects[i].Height;
            n_array[j]++;
            sc_array[j] += s_scores[i];
         }
      }
   }
   n = 0;
   for (i = 1; i < n_array.Length; i++) if (n_array[i]) n++;
   if (!n) return (0);
   d_rects.Create  (n);
   d_scores.Create (n);
   for (i = 1,j = 0; i < n_array.Length; i++) {
      n = n_array[i];
      if (n) {
         IBox2D& d_rect = d_rects[j];
         d_rect.Width   = w_array[i] / n;
         d_rect.Height  = h_array[i] / n;
         d_rect.X       = nms.DetRegions[i].Center.X - d_rect.Width  / 2;
         d_rect.Y       = nms.DetRegions[i].Center.Y - d_rect.Height / 2;
         d_scores[j]    = sc_array[i];
         j++;
      }
   }
   return (d_rects.Length);
}

}

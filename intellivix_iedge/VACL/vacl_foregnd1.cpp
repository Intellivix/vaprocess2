﻿#include "vacl_dnn.h"
#include "vacl_binimg.h"
#include "vacl_edge.h"
#include "vacl_event.h"
#include "vacl_filter.h"
#include "vacl_foregnd.h"
#include "vacl_histo.h"
#include "vacl_imgreg.h"
#include "vacl_morph.h"
#include "vacl_objdetec.h"
#include "vacl_objtrack.h"
#include "vacl_profile.h"
#include "vacl_realsize.h"
#include "vacl_shadow.h"
#include "vacl_warping.h"

#if defined(__DEBUG_FGD)
#include "Win32CL/Win32CL.h"
extern int _DVX_FGD,_DVY_FGD;
extern CImageView* _DebugView_FGD;
#endif

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Definitions
//
///////////////////////////////////////////////////////////////////////////////

#define FGD_PARAM_INIT_FGD_THRESHOLD   0.35f

// LA: Lighting Area
// BS: Background Subtraction
#define FGD_PARAM_LA_BS_THRESHOLD      35   // 조선왕릉: 1, 4대궁: 35

///////////////////////////////////////////////////////////////////////////////
//
// Global Variables/Functions
//
///////////////////////////////////////////////////////////////////////////////

extern int __Flag_VACL_Initialized;
extern int __CheckLicenseKey (   );

///////////////////////////////////////////////////////////////////////////////
//
// Class: ForegroundDetection
//
///////////////////////////////////////////////////////////////////////////////

 ForegroundDetection::ForegroundDetection (   )
 
{
   ClassID          = CLSID_ForegroundDetection;
   Flag_Enabled     = TRUE;
   Time_SSCD        = 0.0f;

   Options          = 0;
   CFRadius         = 3;
   VGFFSize         = 8;
   BkgUpdatePeriod  = 0.5f;
   InitBkgLearnTime = 2.0f;
   SSCDThreshold    = 0.6f;
   DBSRange         = 2;
   DBSThreshold     = 6;
   EDThreshold      = 65;
   MinBlobArea      = 9;
   MaxBlobArea      = 1000 * 1000;
   SNRThreshold     = 3;

   #if defined(__CPNI_CERT_SZM)
   CFRadius         = 3;
   VGFFSize         = 10;
   #endif

   _Init (   );
}

 ForegroundDetection::~ForegroundDetection (   )
 
{
   Close (   );
}

 void ForegroundDetection::_Init (   )

{
   FrameRate            = 0.0f;
   SrcColorImage        = NULL;
   SrcFrmBuffer         = NULL;

   ObjectTracker        = NULL;
   RealObjSizeEstimator = NULL;

   FrameCount           = 0;
   Status               = 0;
   InitFrgDensity       = 0.0f;
}

 int ForegroundDetection::CheckBWImage (BGRImage& s_cimage,int ss_size,float s_thrsld)

{
   int x,y;

   int sx = ss_size;
   int sy = ss_size;
   int ex = s_cimage.Width  - ss_size;
   int ey = s_cimage.Height - ss_size;
   int n  = 0;
   int64 sum = 0;
   for (y = sy; y < ey; y += ss_size) {
      BGRPixel* sp = &s_cimage[y][sx];
      for (x = sx; x < ex; x += ss_size) {
         int max_v = (int)GetMaximum (GetMaximum (sp->R,sp->G),sp->B);
         int min_v = (int)GetMinimum (GetMinimum (sp->R,sp->G),sp->B);
         sum += max_v - min_v;
         sp  += ss_size;
         n++;
      }
   }
   float s = (float)((double)sum / n);
   if (s > s_thrsld) return (1);
   return (DONE);
}

 int ForegroundDetection::CheckEdgeThreshold (   )
#if defined(__CPNI_CERT_SZM)
{
   int r_code = 0;
   if (SrcColorImage) {
      if (CheckBWImage (*SrcColorImage,4,14.0f) == DONE) { // 흑백 영상인 경우
         if (Status & FGD_STATUS_LOW_ILLUMINATION) { // 밤인 경우
            r_code = -1;
         }
         else { // 낮인 경우
            if (InitFrgDensity <= FGD_PARAM_INIT_FGD_THRESHOLD) r_code = -1; // 급격한 조명 변화가 없는 경우
         }
      }
      else { // 컬러 영상인 경우
         if (InitFrgDensity > FGD_PARAM_INIT_FGD_THRESHOLD) r_code = 1; // 급격한 조명 변화가 있는 경우
      }
   }
   Status &= ~(FGD_STATUS_LOW_EDGE_THRESHOLD | FGD_STATUS_HIGH_EDGE_THRESHOLD);
   if (r_code < 0) Status |= FGD_STATUS_LOW_EDGE_THRESHOLD;
   if (r_code > 0) Status |= FGD_STATUS_HIGH_EDGE_THRESHOLD;
   return (r_code);
}
#else
{
   int r_code = 0;
   if (Status & (FGD_STATUS_IR_MODE|FGD_STATUS_LOW_ILLUMINATION)) r_code = -1; // 밤인 경우
   Status &= ~(FGD_STATUS_LOW_EDGE_THRESHOLD|FGD_STATUS_HIGH_EDGE_THRESHOLD);
   if (r_code < 0) Status |= FGD_STATUS_LOW_EDGE_THRESHOLD;
   if (r_code > 0) Status |= FGD_STATUS_HIGH_EDGE_THRESHOLD;
   return (r_code);
}
#endif

 int ForegroundDetection::CheckLowIllumination (GImage& s_image)

{
   int x,y;
   
   float radius    = 1.0f; // PARAMETERS
   int   br_thrsld = 75;   // PARAMETERS
   GImage m_image(s_image.Width,s_image.Height);
   m_image.Clear (   );
   int diameter = (int)(radius * s_image.Width);
   int cx = (int)(0.5f * s_image.Width );
   int cy = (int)(0.5f * s_image.Height);
   DrawFilledCircle (cx,cy,diameter,(byte)PG_WHITE,m_image);
   // 원 바깥 영역의 픽셀 값의 평균을 구한다.
   int n = 0;
   int64 sum = 0;
   for (y = 0; y < s_image.Height; y++) {
      byte* l_s_image = s_image[y];
      byte* l_m_image = m_image[y];
      for (x = 0; x < s_image.Width; x++) {
         if (!l_m_image[x]) {
            sum += l_s_image[x];
            n++;
         }
      }
   }
   int br = (int)((double)sum / n);
   if (br > br_thrsld) return (1);
   return (DONE);
}

 int ForegroundDetection::CheckIRMode (BGRImage& s_cimage)

{
   int x,y;
   BGRPixel* sp;
   
   int d_thrsld = 40; // PARAMETERS
   int r_code = DONE;
   for (y = 0; y < s_cimage.Height; y += 4) {
      sp = s_cimage[y];
      for (x = 0; x < s_cimage.Width; x += 4) {
         if (abs ((int)sp->R - sp->G) > d_thrsld ||
             abs ((int)sp->G - sp->B) > d_thrsld ||
             abs ((int)sp->R - sp->B) > d_thrsld) {
            r_code = 1;
            goto end;
         }
         sp += 4;
      }
   }
   end:;
   return (r_code);
}

 void ForegroundDetection::CheckSchedules (   )
 
{
   Time c_time;
   c_time.GetLocalTime (   );
   CheckSchedule_DBS (c_time.Hour,c_time.Minute);
}

 int ForegroundDetection::CheckSetup (ForegroundDetection* fd)

{
   int ret_flag = 0;

   FileIO buffThis,buffFrom;
   CXMLIO xmlIOThis,xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis,XMLIO_Write);
   xmlIOFrom.SetFileIO (&buffFrom,XMLIO_Write);
   WriteFile (&xmlIOThis);
   fd->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom)) ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength (0);
   buffFrom.SetLength (0);
   if (ret_flag & CHECK_SETUP_RESULT_CHANGED) {
      if (CFRadius != fd->CFRadius) ret_flag |= CHECK_SETUP_RESULT_RESTART;
   }
   return (ret_flag);
}

 int ForegroundDetection::CheckSchedule_DBS (int hour,int minute)
 
{
   if (Options & FGD_OPTION_PERFORM_DYNAMIC_BACKGROUND_SUPPRESSION) {
      if (Options & FGD_OPTION_ENABLE_SCHEDULE_FOR_DYNAMIC_BACKGROUND_SUPPRESSION) {
         if (Schedule_DBS.IsTimeInsidePeriod (hour,minute)) {
            if (!(Status & FGD_STATUS_DYNAMIC_BACKGROUND_SUPPRESSION)) {
               Status |= FGD_STATUS_DYNAMIC_BACKGROUND_SUPPRESSION;
               return (1);
            }
         }
         else {
            if (Status & FGD_STATUS_DYNAMIC_BACKGROUND_SUPPRESSION) {
               Status &= ~FGD_STATUS_DYNAMIC_BACKGROUND_SUPPRESSION;
               return (2);
            }
         }
      }
      else {
         if (!(Status & FGD_STATUS_DYNAMIC_BACKGROUND_SUPPRESSION)) {
            Status |= FGD_STATUS_DYNAMIC_BACKGROUND_SUPPRESSION;
            return (1);
         }
      }
   }
   else {
      if (Status & FGD_STATUS_DYNAMIC_BACKGROUND_SUPPRESSION) {
         Status &= ~FGD_STATUS_DYNAMIC_BACKGROUND_SUPPRESSION;
         return (2);
      }
   }
   return (DONE);
}

 void ForegroundDetection::Close (   )
 
{
   BMUSImage.Delete     (   );
   RefImage.Delete      (   );
   LDR.Close            (   );
   SrcImgSize.Clear     (   );
   SrcImage.Delete      (   );
   FrgRegionMap.Delete  (   );
   FrgRegions.Delete    (   );
   BkgEdgeImage.Delete  (   );
   BkgImage.Delete      (   );
   DetAreaMap.Delete    (   );
   FrgEdgeImage1.Delete (   );
   FrgEdgeImage2.Delete (   );
   FrgEdgeImage3.Delete (   );
   FrgImage.Delete      (   );
   IntFrgImage1.Delete  (   );
   IntFrgImage2.Delete  (   );
   BkgEdgeMap.Delete    (   );
   BkgColorImage.Delete (   );
   _Init (   );
}

 void ForegroundDetection::CompensateForeground (GImage& fg_image)

{
   int x,y;
   
   if (ObjectTracker == NULL) return;
   GImage m_image(fg_image.Width,fg_image.Height);
   m_image.Clear (   );
   float p_time = 1.0f / FrameRate;
   TrackedObject* t_object = ObjectTracker->TrackedObjects.First (   );
   while (t_object != NULL) {
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED) && (t_object->Status & TO_STATUS_VALIDATED) && !(t_object->Status & TO_STATUS_STATIC2)) {
         IPoint2D p_pos;
         t_object->GetPredictedPosition (p_pos,p_time);
         float ar = GetOverlapAreaRatio (fg_image,t_object->MaskImage,p_pos);
         // 현재 객체 영역에 대응하는 전경 영역의 면적이 반 이하로 급격하게 줄어든 경우
         // 전경 영역 검출에 문제가 있는 것으로 보고 Compensation 영역으로 설정한다.
         if (ar <= 0.5f) t_object->PutMask (p_pos.X,p_pos.Y,PG_WHITE,m_image); // PARAMETERS
      }
      t_object = ObjectTracker->TrackedObjects.Next (t_object);
   }
   GImage bs_image = IntFrgImage1;
   PerformLightDisturbanceReduction_TS (SrcImage,BkgImage,0.4f,bs_image); // PARAMETERS
   for (y = 0; y < fg_image.Height; y++) {
      byte* l_m_image  = m_image[y];
      byte* l_bs_image = bs_image[y];
      byte* l_fg_image = fg_image[y];
      for (x = 0; x < fg_image.Width; x++) {
         if (l_m_image[x] && l_bs_image[x]) l_fg_image[x] = PG_WHITE;
      }
   }
}

 void ForegroundDetection::CopyParams1 (ForegroundDetection& s_fg_detector)
 
{
   Options       = s_fg_detector.Options;
   CFRadius      = s_fg_detector.CFRadius;
   VGFFSize      = s_fg_detector.VGFFSize;
   SSCDThreshold = s_fg_detector.SSCDThreshold;
   Schedule_DBS  = s_fg_detector.Schedule_DBS;
}

 void ForegroundDetection::CopyParams2 (ForegroundDetection& s_fg_detector)

{
   CopyParams1 (s_fg_detector);
   InitBkgLearnTime = s_fg_detector.InitBkgLearnTime;
   BkgUpdatePeriod  = s_fg_detector.BkgUpdatePeriod;
   SSCDThreshold    = s_fg_detector.SSCDThreshold;
   DBSRange         = s_fg_detector.DBSRange;
   DBSThreshold     = s_fg_detector.DBSThreshold;
   EDThreshold      = s_fg_detector.EDThreshold;
   MinBlobArea      = s_fg_detector.MinBlobArea;
   MaxBlobArea      = s_fg_detector.MaxBlobArea;
}

 int ForegroundDetection::DetectSuddenSceneChange (GImage& fg_image)

{
   if (!BkgImage) return (FGD_RCODE_NORMAL);
   int   e_code = FGD_RCODE_NORMAL;
   int   n1 = GetNumEntirePixels (fg_image);
   int   n2 = fg_image.Width * fg_image.Height;
   int   flag_detection = FALSE;
   float d_time = 1.0f / FrameRate;
   InitFrgDensity = (float)n1 / n2;
   if (InitFrgDensity > SSCDThreshold) {
      GImage bs_image = fg_image;
      PerformLightDisturbanceReduction_TS (SrcImage,BkgImage,0.4f,bs_image); // PARAMETERS
      n1 = GetNumEntirePixels (bs_image);
      float density = (float)n1 / n2;
      if (density > 0.5f * SSCDThreshold) flag_detection = TRUE;
   }
   if (flag_detection) Time_SSCD += d_time;
   else {
      Time_SSCD -= d_time;
      if (Time_SSCD < 0.0f) Time_SSCD = 0.0f;
   }
   if (Time_SSCD > 0.5f) {
      e_code  = FGD_RCODE_SUDDEN_SCENE_CHANGE; 
      Status |= FGD_STATUS_SUDDEN_SCENE_CHANGE;
   }
   else Status &= ~FGD_STATUS_SUDDEN_SCENE_CHANGE;
   if (Time_SSCD > 3.0f) e_code = FGD_RCODE_RESET;
   return (e_code);
}

 void ForegroundDetection::DrawObjBndBox (IBox2D& s_rgn,int id,const char* label,uint b_color,uint l_color)

{
   #if defined(__DEBUG_FGD)
   CString label2;
   ConvertString (label,label2);
   CString text;
   text.Format (_T("%d: %s"),id,label2);
   int x = _DVX_FGD + s_rgn.X;
   int y = _DVY_FGD + s_rgn.Y;
   _DebugView_FGD->DrawRectangle (x,y,s_rgn.Width,s_rgn.Height,b_color,2);
   x += s_rgn.Width + 5;
   _DebugView_FGD->DrawText (text,x + 1,y + 1,PC_BLACK,TRANSPARENT,FT_ARIAL,14);
   _DebugView_FGD->DrawText (text,x,y,l_color,TRANSPARENT,FT_ARIAL,14);
   #endif
}

 void ForegroundDetection::DrawObjBndBoxes (   )

{
   #if defined(__DEBUG_FGD)
   if (SrcColorImage == NULL) return;
   CString text;
   _DebugView_FGD->DrawImage (*SrcColorImage,_DVX_FGD,_DVY_FGD);
   for (int i = 0; i < ObjBndBoxes.Length; i++) {
      ObjBndBox& obb = ObjBndBoxes[i];
      DrawObjBndBox (obb,i,obb.ClassName,PC_GREEN,PC_YELLOW);
      if (obb.SubRgn_Head.Width ) DrawObjBndBox (obb.SubRgn_Head ,i,"Head" ,PC_BLUE,PC_YELLOW);
      if (obb.SubRgn_Face.Width ) DrawObjBndBox (obb.SubRgn_Face ,i,"Face" ,PC_CYAN,PC_YELLOW);
      if (obb.SubRgn_Stick.Width) DrawObjBndBox (obb.SubRgn_Stick,i,"Stick",PC_MAGENTA,PC_YELLOW);
   }
   _DVY_FGD += SrcColorImage->Height;
   _DebugView_FGD->DrawText (_T("Object Detection Result"),_DVX_FGD,_DVY_FGD,PC_GREEN);
   _DVY_FGD += 20;
   #endif
}

 void ForegroundDetection::EnableVideoStabilization (int flag_enable)

{
   if (flag_enable) Options |= FGD_OPTION_PERFORM_VIDEO_STABILIZATION;
   else Options &= ~FGD_OPTION_PERFORM_VIDEO_STABILIZATION;
   Status |= FGD_STATUS_RESET_VIDEO_STABILIZATION;
}

 int ForegroundDetection::GetBkgModelUpdateMode (   )
 
{
   if (FrameCount == 1) return (FGD_BMU_MODE_INITIAL);
   if (FrameCount <= (int)(InitBkgLearnTime * FrameRate)) return (FGD_BMU_MODE_INITIAL);
   int u_period = (int)(BkgUpdatePeriod * FrameRate);
   if (FrameCount % u_period) return (FGD_BMU_MODE_MINOR);
   else return (FGD_BMU_MODE_MAJOR);
}

 void ForegroundDetection::GetFrgEdgeImages (GImage& fg_image)
 
{
   int x,y;

   float a = 1.0f;
   if (Status & FGD_STATUS_LOW_EDGE_THRESHOLD ) a = 0.4f; // PARAMETERS
   if (Status & FGD_STATUS_HIGH_EDGE_THRESHOLD) a = 1.3f; // PARAMETERS
   int ed_thrsld  = (int)(a * EDThreshold  + 0.5f);
   BkgEdgeImage.Clear  (   );
   FrgEdgeImage1.Clear (   );
   FrgEdgeImage2.Clear (   );
   FrgEdgeImage3.Clear (   );
   int ex = SrcImage.Width  - 1;
   int ey = SrcImage.Height - 1;
   // FrgEdgeImage1, FrgEdgeImage2, BkgEdgeImage를 구한다.
   for (y = 1; y < ey; y++) {
      byte*  l_s_image   = SrcImage[y];
      byte*  l_s_image1  = SrcImage[y - 1];
      byte*  l_s_image2  = SrcImage[y + 1];
      byte*  l_fg_image  = fg_image[y];
      byte*  l_fe_image1 = FrgEdgeImage1[y];
      byte*  l_fe_image2 = FrgEdgeImage2[y];
      byte*  l_be_image  = BkgEdgeImage[y];
      short* l_be_map    = BkgEdgeMap[y];
      for (x = 1; x < ex; x++) {
         if (l_fg_image[x]) {
            int x1 = x - 1;
            int x2 = x + 1;
            short gx = (l_s_image2[x2] + 2 * (short)l_s_image[x2] + l_s_image1[x2]) -
                       (l_s_image2[x1] + 2 * (short)l_s_image[x1] + l_s_image1[x1]);
            short gy = (l_s_image2[x2] + 2 * (short)l_s_image2[x] + l_s_image2[x1]) -
                       (l_s_image1[x1] + 2 * (short)l_s_image1[x] + l_s_image1[x2]);
            int   gm = abs (gx) + abs (gy);
            if (abs (gm - l_be_map[x]) >= ed_thrsld) l_fe_image1[x] = PG_WHITE;
            if (gm >= ed_thrsld) l_fe_image2[x] = PG_WHITE;
            if (l_be_map[x] >= ed_thrsld) l_be_image[x] = PG_WHITE;
         }
      }
   }
   // 빛 반사 또는 카메라 흔들림에 의해 배경 에지가 전경 에지로 오검출 되는 것을 막는다.
   PerformLightDisturbanceReduction_TS (SrcImage,BkgImage,0.5f,FrgEdgeImage1); // PARAMETERS
   // FrgEdgeImage3을 구한다.
   for (y = 1; y < ey; y++) {
      byte* l_fe_image1 = FrgEdgeImage1[y];
      byte* l_fe_image2 = FrgEdgeImage2[y];
      byte* l_fe_image3 = FrgEdgeImage3[y];
      for (x = 1; x < ex; x++)
         if (l_fe_image1[x] && l_fe_image2[x]) l_fe_image3[x] = PG_WHITE;
   }
}

 void ForegroundDetection::GetFrgImageInLightingArea (GImage& fg_image)

{
   int x,y;

   if (!LightingArea.Width || !LightingArea.Height) return;
   if (!BkgImage) return;
   int x0 = LightingArea.X;
   int y0 = LightingArea.Y;
   int sx = x0;
   int sy = y0;
   int ex = LightingArea.X + LightingArea.Width;
   int ey = LightingArea.Y + LightingArea.Height;
   if (sx < 0) sx = 0;
   if (sy < 0) sy = 0;
   if (ex > SrcImage.Width ) ex = SrcImage.Width;
   if (ey > SrcImage.Height) ey = SrcImage.Height;
   GImage bd_image(LightingArea.Width,LightingArea.Height);
   bd_image.Clear (   );
   int mbd = 0, n = 0;
   for (y = sy; y < ey; y++) {
      byte* l_s_image  = SrcImage[y];
      byte* l_fg_image = fg_image[y];
      byte* l_bg_image = BkgImage[y];
      byte* l_bd_image = bd_image[y - y0];
      for (x = sx; x <= ex; x++) {
         if (l_fg_image[x]) {
            byte bd = (byte)abs ((short)l_s_image[x] - l_bg_image[x]);
            l_bd_image[x - x0] = bd;
            mbd += bd;
            n++;
         }
      }
   }
   if (n) mbd /= n;
   else return;
   for (y = sy; y < ey; y++) {
      byte* l_fg_image = fg_image[y];
      byte* l_bd_image = bd_image[y - y0];
      for (x = sx; x < ex; x++) {
         if (l_fg_image[x]) {
            if (abs(l_bd_image[x - x0] - mbd) <= FGD_PARAM_LA_BS_THRESHOLD) l_fg_image[x] = PG_BLACK;
         }
      }
   }
}

 void ForegroundDetection::GetFrgRegionMap (   )
 
{
   // 비검출 영역을 제거한다.
   if (DetAreaMap.Width == FrgImage.Width && DetAreaMap.Height == DetAreaMap.Height) And (FrgImage,DetAreaMap,FrgImage);
   // FrgRegionMap을 얻는다.
   GetCRMap (FrgImage,MinBlobArea,MaxBlobArea,FrgRegionMap,FrgRegions);
}

 float ForegroundDetection::GetOverlapAreaRatio (GImage& fg_image,GImage& om_image,IPoint2D& om_pos)

{
   int x1,y1,x2,y2;
   
   int sx1 = om_pos.X;
   int sy1 = om_pos.Y;
   int ex1 = sx1 + om_image.Width;
   int ey1 = sy1 + om_image.Height;
   if (sx1 < 1) sx1 = 1;
   if (sy1 < 1) sy1 = 1;
   if (ex1 > fg_image.Width  - 1) ex1 = fg_image.Width  - 1;
   if (ey1 > fg_image.Height - 1) ey1 = fg_image.Height - 1;
   int sx2 = sx1 - om_pos.X;
   int sy2 = sy1 - om_pos.Y;
   int n_fgps = 0, n_omps = 0;
   for (y1 = sy1,y2 = sy2; y1 < ey1; y1++,y2++) {
      byte* l_fg_image = fg_image[y1];
      byte* l_om_image = om_image[y2];
      for (x1 = sx1,x2 = sx2; x1 < ex1; x1++,x2++) {
         if (l_fg_image[x1]) n_fgps++;
         if (l_om_image[x2]) n_omps++;
      }
   }
   float oa_ratio = (float)n_fgps / n_omps;
   return (oa_ratio);
}

 void ForegroundDetection::GetPreprocSrcImage (GImage& s_image)

{
   int flag_smoothing     = FALSE;
   if ((ClassID == CLSID_ForegroundDetection_FAD) || (Status & FGD_STATUS_SMOKE_DETECTION)) flag_smoothing = TRUE;
   int flag_enhancement   = FALSE;
   if (Status & (FGD_STATUS_IR_MODE)) flag_enhancement = TRUE;
   int flag_stabilization = FALSE;
   if (Options & FGD_OPTION_PERFORM_VIDEO_STABILIZATION) flag_stabilization = TRUE;
   Shift.Clear (   );
   GImage  t_image1;
   GImage* s_image2 = &t_image1;
   if (flag_smoothing) {
      if (flag_enhancement || flag_stabilization) {
         t_image1.Create (s_image.Width,s_image.Height);
      }
   }
   else s_image2 = &SrcImage;
   float a = 1.2f, b = -10.0f; // PARAMETERS
   if (flag_enhancement && flag_stabilization) {
      GImage t_image2(s_image.Width,s_image.Height);
      HistogramStretching (s_image,a,b,t_image2);
      GetStabilizedImage  (t_image2,*s_image2);
   }
   else if (flag_enhancement && !flag_stabilization) {
      HistogramStretching (s_image,a,b,*s_image2);
   }
   else if (!flag_enhancement && flag_stabilization) {
      GetStabilizedImage  (s_image,*s_image2);
   }
   else {
      if (flag_smoothing) s_image2 = &s_image;
      else *s_image2 = s_image;
   }
   if (flag_smoothing) {
      GaussianFiltering_3x3 (*s_image2,SrcImage);
   }
   #if defined(__DEBUG_FGD)
   if (ClassID != CLSID_ForegroundDetection_FCD_Seeta &&
       ClassID != CLSID_ForegroundDetection_OBD_YOLO  &&
       ClassID != CLSID_ForegroundDetectionClient_OBD_YOLO) {
      if ((Options & FGD_OPTION_PERFORM_VIDEO_STABILIZATION) || flag_smoothing) {
         _DebugView_FGD->DrawImage (s_image,_DVX_FGD,_DVY_FGD);
         _DVY_FGD += s_image.Height;
         _DebugView_FGD->DrawText (_T("Input Image"),_DVX_FGD,_DVY_FGD,PC_GREEN);
         _DVY_FGD += 20;
      }
      _DebugView_FGD->DrawImage (SrcImage,_DVX_FGD,_DVY_FGD);
      _DVY_FGD += SrcImage.Height;
      _DebugView_FGD->DrawText (_T("Source Image"),_DVX_FGD,_DVY_FGD,PC_GREEN);
      _DVY_FGD += 20;
   }
   #endif
}

 int ForegroundDetection::GetStabilizedImage (GImage& s_image,GImage& d_image)

{
   if (BkgImage.Width != s_image.Width || BkgImage.Height != s_image.Height) {
      d_image = s_image;
      return (1);
   }
   if (FrameCount <= 1 || RefImage.Width != s_image.Width || RefImage.Height != s_image.Height || (Status & FGD_STATUS_RESET_VIDEO_STABILIZATION)) {
      Status &= ~FGD_STATUS_RESET_VIDEO_STABILIZATION;
      RefImage = d_image = s_image;
      return (DONE);
   }
   HTM_NCC1 htm;
   FPoint2D p1(0.65f,0.65f);                    // PARAMETERS
   FPoint2D p2(0.65f,0.65f);                    // PARAMETERS
   htm.Initialize (RefImage,s_image,100,p1,p2); // PARAMETERS
   htm.Perform (2.0f,5,2);                      // PARAMETERS
   FPoint2D shift = htm.GetMatchingPosition (   );
   VACL::Shift (s_image,shift,d_image);
   Shift = -shift;
   return (DONE);
}

 int ForegroundDetection::Initialize (ISize2D& si_size,float frm_rate)
 
{
   #if defined(__LICENSE_MAC)
   if (__CheckLicenseKey (   )) __Flag_VACL_Initialized = FALSE;
   #endif
   if (!__Flag_VACL_Initialized) return (-1);
   if (!IsEnabled (   )) return (1);
   Close (   );
   Status         = 0;
   InitFrgDensity = 0.0f;
   FrameRate      = frm_rate;
   SrcImgSize     = si_size;
   SrcImage.Create     (si_size.Width,si_size.Height);
   SrcImage.Clear      (   );
   FrgImage.Create     (si_size.Width,si_size.Height);
   FrgImage.Clear      (   );
   FrgRegionMap.Create (si_size.Width,si_size.Height);
   FrgRegionMap.Clear  (   );
   LightingArea.Clear  (   );
   Reset (   );
   return (DONE);
}

 void ForegroundDetection::InitParams (EventDetection &evt_detector)
 
{
   if (evt_detector.IsEventZoneAvailable (ER_EVENT_REMOVED)) {
      Status |= FGD_STATUS_REMOVED_OBJECT_DETECTION;
   }
   else {
      Status &= ~FGD_STATUS_REMOVED_OBJECT_DETECTION;
   }
   if (evt_detector.IsEventZoneAvailable (ER_EVENT_SMOKE)) {
      Status |= FGD_STATUS_SMOKE_DETECTION;
   }
   else {
      Status &= ~FGD_STATUS_SMOKE_DETECTION;
   }
   if (evt_detector.IsEventZoneAvailable (ER_EVENT_CROWD_DENSITY)) {
      LDR.Object_Threshold = 0.3f;
      LDR.Light_Threshold  = 0.7f;
      LDR.Shadow_Threshold = 0.7f;
   }
}

 int ForegroundDetection::Perform (GImage& s_image,BGRImage* s_cimage,byte* s_frm_buf,ISize2D* s_frm_size,ObjectTracking* obj_tracker,RealObjectSizeEstimation* ros_estimator)

{
   #if defined(__DEBUG_FGD)
   _DVX_FGD = _DVY_FGD = 0;
   _DebugView_FGD->Clear (   );
   #endif
   if (!(FrameCount % 1100)) {
      #if defined(__LICENSE_MAC)
      if (__CheckLicenseKey (   )) __Flag_VACL_Initialized = FALSE;
      #endif
   }
   if (!__Flag_VACL_Initialized) return (-1);
   if (!IsEnabled (   )) return (FGD_RCODE_DISABLED);
   FrameCount++;
   SrcColorImage        = s_cimage;
   SrcFrmBuffer         = s_frm_buf;
   ObjectTracker        = obj_tracker;
   RealObjSizeEstimator = ros_estimator;
   if (s_frm_size != NULL) SrcFrmSize = *s_frm_size;
   else SrcFrmSize.Clear (   );
   Status &= ~(FGD_STATUS_LOW_ILLUMINATION|FGD_STATUS_IR_MODE);
   // 입력 영상이 저조도 영상이거나 IR 조명 영상인 경우 전경 검출 민감도를 높인다.
   if (CheckLowIllumination (s_image) == DONE) {
      Status |= FGD_STATUS_LOW_ILLUMINATION;
      #if defined(__DEBUG_FGD)
      logd ("Low Illumination!\n");
      #endif
   }
   if (s_cimage != NULL && CheckIRMode (*s_cimage) == DONE) {
      Status |= FGD_STATUS_IR_MODE;
      #if defined(__DEBUG_FGD)
      logd ("IR Mode!\n");
      #endif
   }
   // 전처리된 SrcImage를 얻는다.
   GetPreprocSrcImage (s_image);
   // 각 필터들에 대한 동작 스케쥴을 체크한다.
   CheckSchedules (   );
   // Foreground Image를 얻는다.
   int r_code = GetFrgImage (   );
   if (r_code != FGD_RCODE_NORMAL) {
      #if defined(__DEBUG_FGD)
      _DebugView_FGD->Invalidate (   );
      #endif
      return (r_code);
   }
   // Foreground Region Map을 얻는다.
   GetFrgRegionMap (   );
   #if defined(__DEBUG_FGD)
   if (ClassID != CLSID_ForegroundDetection_FCD_Seeta &&
       ClassID != CLSID_ForegroundDetection_OBD_YOLO  &&
       ClassID != CLSID_ForegroundDetectionClient_OBD_YOLO) {
      _DebugView_FGD->DrawImage (FrgImage,_DVX_FGD,_DVY_FGD);
      _DVY_FGD += FrgImage.Height;
      _DebugView_FGD->DrawText  (_T("Foreground Detection Result"),_DVX_FGD,_DVY_FGD,PC_GREEN);
      _DVY_FGD += 20;
   }
   _DebugView_FGD->Invalidate (   );
   #endif
   return (FGD_RCODE_NORMAL);
}

 void ForegroundDetection::PerformDynamicBackgroundSuppression (GImage& fg_image)

{
   int x,y,x2,y2;

   int range = (int)(DBSRange * fg_image.Height / 240.0f + 0.5f);
   int sx = range + 1;
   int sy = sx;
   int ex = SrcImage.Width  - sx;
   int ey = SrcImage.Height - sy;
   for (y = sy; y < ey; y++) {
      byte* l_s_image  = SrcImage[y];
      byte* l_s_image1 = SrcImage[y - 1];
      byte* l_s_image2 = SrcImage[y + 1];
      byte* l_fg_image = fg_image[y];
      for (x = sx; x < ex; x++) {
         if (l_fg_image[x]) {
            int xm1 = x - 1;
            int xp1 = x + 1;
            int sx2 = x - range;
            int ex2 = x + range;
            int sy2 = y - range;
            int ey2 = y + range;
            byte sp = l_s_image[x];
            for (y2 = sy2; y2 <= ey2; y2++) {
               int y2m1 = y2 - 1;
               int y2p1 = y2 + 1;
               for (x2 = sx2; x2 <= ex2; x2++) {
                  int score = 0;
                  if (IsBkgPixel (sp,x2,y2)) {
                     score++;
                     int x2m1 = x2 - 1;
                     int x2p1 = x2 + 1;
                     if (IsBkgPixel (l_s_image1[xm1],x2m1,y2m1)) score++;
                     if (IsBkgPixel (l_s_image1[x  ],x2  ,y2m1)) score++;
                     if (IsBkgPixel (l_s_image1[xp1],x2p1,y2m1)) score++;
                     if (IsBkgPixel (l_s_image [xm1],x2m1 ,y2 )) score++;
                     if (IsBkgPixel (l_s_image [xp1],x2p1 ,y2 )) score++;
                     if (IsBkgPixel (l_s_image2[xm1],x2m1,y2p1)) score++;
                     if (IsBkgPixel (l_s_image2[x  ],x2  ,y2p1)) score++;
                     if (IsBkgPixel (l_s_image2[xp1],x2p1,y2p1)) score++;
                  }
                  if (score >= DBSThreshold) {
                     l_fg_image[x] = PG_BLACK;
                     goto escape;
                  }
               }
            }
         }
         escape:;
      }
   }
}

 void ForegroundDetection::PerformLightDisturbanceReduction (GImage& fg_image)
 
{
   GImage* fe_image;
   if (Status & FGD_STATUS_REMOVED_OBJECT_DETECTION) fe_image = &FrgEdgeImage1;
   else fe_image = &FrgEdgeImage3;
   LDR.MinArea = SNRThreshold;
   LDR.Perform (SrcImage,BkgImage,*fe_image,fg_image);
}

 void ForegroundDetection::PerformMorphFiltering (GImage& fg_image)

{
   int se_radius = (int)(CFRadius * fg_image.Height / 240.0f + 0.5f);
   if (se_radius < 1) se_radius = 1;
   int gap_size = (int)(VGFFSize * fg_image.Height  / 240.0f + 0.5f);
   if (gap_size) {
      GImage t_image(fg_image.Width,fg_image.Height);
      PerformBinClosing (fg_image,se_radius,t_image);
      BinVRLFiltering  (t_image,gap_size,fg_image);
   }
   else PerformBinClosing (fg_image,se_radius,fg_image);
}

 int ForegroundDetection::ReadFile (FileIO& file)
 
{
   if (file.Read ((byte*)&Options         ,1,sizeof(int  ))) return (1);
   if (file.Read ((byte*)&CFRadius        ,1,sizeof(int  ))) return (2);
   if (file.Read ((byte*)&VGFFSize        ,1,sizeof(int  ))) return (3);
   if (file.Read ((byte*)&InitBkgLearnTime,1,sizeof(float))) return (4);
   if (file.Read ((byte*)&BkgUpdatePeriod ,1,sizeof(float))) return (5);
   if (file.Read ((byte*)&SSCDThreshold   ,1,sizeof(float))) return (6);
   if (Schedule_DBS.ReadFile (file)) return (7);
   return (DONE);
}

 int ForegroundDetection::ReadFile (CXMLIO* pIO)
 
{
   static ForegroundDetection dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("ForegroundDetection")) {
      xmlNode.Attribute (TYPE_ID(xint) ,"Options"         ,&Options         ,&dv.Options);
      xmlNode.Attribute (TYPE_ID(int)  ,"CFRadius"        ,&CFRadius        ,&dv.CFRadius);
      xmlNode.Attribute (TYPE_ID(int)  ,"VGFFSize"        ,&VGFFSize        ,&dv.VGFFSize);
      xmlNode.Attribute (TYPE_ID(float),"InitBkgLearnTime",&InitBkgLearnTime,&dv.InitBkgLearnTime);
      xmlNode.Attribute (TYPE_ID(float),"BkgUpdatePeriod" ,&BkgUpdatePeriod ,&dv.BkgUpdatePeriod);
      xmlNode.Attribute (TYPE_ID(float),"SSCDThreshold"   ,&SSCDThreshold   ,&dv.SSCDThreshold);
      Schedule_DBS.ReadFile (pIO,"Schedule_DynamicBackgroundSuppression");
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}

 void ForegroundDetection::Reset (   )

{
   FrameCount = 0;
   Time_SSCD  = 0.0f;
}

 int ForegroundDetection::SetDetectionAreaMap (GImage& ta_map)
 
{
   int x1,y1,x2,y2;

   if (!IsEnabled (   )) return (1);
   if (!ta_map) {
      DetAreaMap.Create (SrcImgSize.Width,SrcImgSize.Height);
      DetAreaMap.Set (0,0,DetAreaMap.Width,DetAreaMap.Height,PG_WHITE);
      return (DONE);
   }
   int radius   = 8 * ta_map.Height / 240;
   int diameter = 2 * radius;
   GImage t_image1(ta_map.Width + diameter,ta_map.Height + diameter);
   GImage t_image2(t_image1.Width,t_image1.Height);
   t_image1.SetBoundary (radius,PG_BLACK);
   for (y1 = 0; y1 < ta_map.Height; y1++) {
      y2 = y1 + radius;
      byte *l_ta_map   = ta_map[y1];
      byte *l_t_image1 = t_image1[y2];
      for (x1 = 0; x1 < ta_map.Width; x1++) {
         x2 = x1 + radius;
         if (l_ta_map[x1] == PG_WHITE) {
            // 추적 영역에 해당하는 픽셀들 추가
            l_t_image1[x2] = PG_WHITE;
         }
         else l_t_image1[x2] = PG_BLACK;
      }
   }
   diameter++;
   GImage disc(diameter,diameter);
   DrawFilledCircle (radius,radius,diameter,(byte)PG_WHITE,disc);
   BinDilation (t_image1,disc,t_image2);
   if (DetAreaMap.Width != ta_map.Width || DetAreaMap.Height != ta_map.Height) DetAreaMap.Create (ta_map.Width,ta_map.Height);
   DetAreaMap.Copy  (t_image2,radius,radius,ta_map.Width,ta_map.Height,0,0);
   for (y1 = 0; y1 < ta_map.Height; y1++) {
      byte *l_ta_map = ta_map[y1];
      byte *l_da_map = DetAreaMap[y1];
      for (x1 = 0; x1 < ta_map.Width; x1++) {
         if (l_ta_map[x1] == 128) {
            // 비추적 영역에 해당하는 픽셀들 제거
            l_da_map[x1] = PG_BLACK;
         }
      }
   }
   return (DONE);
}

 void ForegroundDetection::SetFrgRegions (IB2DArray1D& s_array,float expansion)

{
   int i,x,y;

   FrgImage.Clear  (   );
   FrgRegionMap.Clear (   );
   if (!s_array) {
      FrgRegions.Create (1);
      FrgRegions[0].Clear (   );
      return;
   }
   int cr_no  = 1;
   for (i = 0; i < s_array.Length; i++) {
      IBox2D& s_rgn = s_array[i];
      int mx = (int)(s_rgn.Width  * expansion);
      int my = (int)(s_rgn.Height * expansion);
      int sx = s_rgn.X - mx;
      int sy = s_rgn.Y - my;
      int ex = sx + s_rgn.Width  + 2 * mx;
      int ey = sy + s_rgn.Height + 2 * my;
      CropRegion (SrcImgSize.Width,SrcImgSize.Height,sx,sy,ex,ey);
      int n = 0;
      for (y = sy; y < ey; y++) {
         byte* l_fg_image = FrgImage[y];
         int*  l_fr_map   = FrgRegionMap[y];
         for (x = sx; x < ex; x++) {
            if (!l_fr_map[x]) {
               l_fg_image[x] = PG_WHITE;
               l_fr_map[x]   = cr_no;
               n++;
            }
         }
      }
      if (n) cr_no++;
   }
   if (cr_no <= 1) {
      FrgRegions.Create (1);
      FrgRegions[0].Clear (   );
      return;
   }
   FrgRegions.Create (cr_no);
   GetCRInfo (FrgRegionMap,FrgRegions);
   Binarize  (FrgRegionMap,0,FrgImage);
}

 void ForegroundDetection::SetLightingArea (int cx,int cy,int width,int height)
 
{
   if (!width || !height) {
      LightingArea.Clear (   );
      return;
   }
   int hw = width  / 2;
   int hh = height / 2;
   int sx = cx - hw;
   int ex = cx + hw;
   int sy = cy - hh;
   int ey = cy + hh;
   if (sx < 0) sx = 0;
   if (ex >= SrcImgSize.Width ) ex = SrcImgSize.Width  - 1;
   if (sy < 0) sy = 0;
   if (ey >= SrcImgSize.Height) ey = SrcImgSize.Height - 1;
   if (sx > ex || sy > ey) {
      LightingArea.Clear (   );
      return;
   }
   LightingArea.X      = sx;
   LightingArea.Y      = sy;
   LightingArea.Width  = ex - sx + 1;
   LightingArea.Height = ey - sy + 1;
}

 void ForegroundDetection::TrimForegroundRegions (GImage& fg_image)

{
   int i,j,x,y;
   
   // 현재 추적 중인 객체(TO)들의 영역에 의한 Coverage Map을 만든다.
   GImage m_image(fg_image.Width,fg_image.Height);
   m_image.Clear (   );
   float p_time = 1.0f / FrameRate;
   if (ObjectTracker != NULL) {
      i = 1;
      TrackedObject* t_object = ObjectTracker->TrackedObjects.First (   );
      while (t_object != NULL) {
         if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED) && (t_object->Status & TO_STATUS_VERIFIED)) {
            IPoint2D p_pos;
            t_object->GetPredictedPosition (p_pos,p_time);
            t_object->PutRectMask (p_pos.X,p_pos.Y,(byte)i,m_image,0);
            i++;
            if (i > 255) i = 1;
         }
         t_object = ObjectTracker->TrackedObjects.Next (t_object);
      }
   }
   // 전경 영역에 대한 CR Map을 만든다.
   IArray2D cr_map(fg_image.Width,fg_image.Height);
   int n_crs = LabelCRs (fg_image,cr_map);
   if (!n_crs) return;
   CRArray1D cr_array(n_crs + 1);
   GetCRInfo (cr_map,cr_array);
   // CR Trimming을 수행한다.
   fg_image.Clear (   );
   for (i = 1; i <= n_crs; i++) {
      int flag_copy_org_rgn = TRUE;
      ConnectedRegion& c_rgn = cr_array[i];
      // 면적이 일정량 이상 되는 CR에 대해 Trimming을 수행한다.
      if (c_rgn.Area >= 200) { // PARAMETERS
         #if defined(__DEBUG_CR_TRMMING)
         logd ("CR: %d x %d\n",c_rgn.Width,c_rgn.Height);
         #endif
         // CR이 TO들의 영역에 의해 얼마나 커버가 되는지 계산한다.
         int n1 = 0,n2 = 0,n3 = 0;
         int sx = c_rgn.X;
         int ex = sx + c_rgn.Width;
         int sy = c_rgn.Y;
         int ey = sy + c_rgn.Height;
         byte p = 0;
         for (y = sy; y < ey; y++) {
            int*  l_cr_map  = cr_map[y];
            byte* l_m_image = m_image[y];
            for (x = sx; x < ex; x++) {
               if (l_cr_map[x] == i) {
                  n1++; // CR의 영역
                  byte c = l_m_image[x];
                  if (c) {
                     n2++; // CR의 영역 중 TO들에 의해 커버되는 영역
                     if (c != p) {
                        p = c;
                        n3++; // CR과 겹치는 TO의 수가 1개를 초과하는지 판단하기 위한 변수
                     }
                  }
               }
            }
         }
         float cr = (float)n2 / n1;
         #if defined(__DEBUG_CR_TRIMMING)
         logd ("- cr = %.2f, n3 = %d\n",cr,n3);
         #endif
         // CR과 겹치는 TO가 1개 뿐이거나 CR이 TO들의 영역에 의해 충분히 커버가 안되는 경우에 한해 다음 단계로 진행한다.
         if (n3 == 1 || cr < 0.8f) { // PARAMETERS
            // CR의 수직 투영 프로파일을 구한 후 Trimming을 수행한다.
            FArray1D v_p_array(c_rgn.Width);
            VerticalProjection (cr_map,c_rgn,i,v_p_array);
            FArray1D v_fp_array(v_p_array.Length);
            GaussianFiltering (v_p_array,1.0f,v_fp_array);
            float thrsld = 0.8f * GetMean (v_fp_array); // PARAMETERS
            RPArray v_rp_array;
            if (GetRectPulses (v_fp_array,thrsld,v_rp_array) == DONE) {
               int max_w,max_i;
               FindMaxWidth (v_rp_array,max_w,max_i);
               float ar = (float)c_rgn.Height / max_w;
               float wr = (float)max_w / c_rgn.Width;
               #if defined(__DEBUG_CR_TRIMMING)
               logd ("- ar = %,2f,wr = %.2f\n",ar,wr);
               #endif
               // Trimming 후의 CR의 Aspect Ratio가 1.5보다 크거가, Trimming 전후의 CR의 너비 변화가 큰 경우에 한하여 실제로 Trimming을 수행한다.
               if (ar > 1.5f && wr < 0.7f) { // PARAMETERS
                  flag_copy_org_rgn = FALSE;
                  for (j = 0; j < v_rp_array.Length; j++) {
                     float w1 = (float)v_rp_array[j].Width;
                     float w2 = 1.3f * w1; // PARAMETERS
                     float cx = v_rp_array[j].StartPos + w1 / 2.0f;
                     float sx = cx - w2 / 2.0f;
                     IBox2D s_rgn((int)(c_rgn.X + sx),c_rgn.Y,(int)w2,c_rgn.Height);
                     ISize2D cr_size(cr_map.Width,cr_map.Height);
                     CropRegion (cr_size,s_rgn);
                     CopyCR (cr_map,s_rgn,i,fg_image,PG_WHITE);
                  }
               }
            }
         }
      }
      if (flag_copy_org_rgn) CopyCR (cr_map,c_rgn,i,fg_image,PG_WHITE);
   }
}

 int ForegroundDetection::UpdateBkgModel (GImage& m_image)
 
{
   if (!IsEnabled (   )) return (1);
   UpdateBkgModel_Main (m_image);
   if (!!BkgImage && !!BkgEdgeMap) GetGradientMagnitudes_Sobel (BkgImage,BkgEdgeMap);
   return (DONE);
}

 int ForegroundDetection::WriteFile (FileIO& file)
 
{
   if (file.Write ((byte*)&Options         ,1,sizeof(int  ))) return (1);
   if (file.Write ((byte*)&CFRadius        ,1,sizeof(int  ))) return (2);
   if (file.Write ((byte*)&VGFFSize        ,1,sizeof(int  ))) return (3);
   if (file.Write ((byte*)&InitBkgLearnTime,1,sizeof(float))) return (4);
   if (file.Write ((byte*)&BkgUpdatePeriod ,1,sizeof(float))) return (5);
   if (file.Write ((byte*)&SSCDThreshold   ,1,sizeof(float))) return (6);
   if (Schedule_DBS.WriteFile (file)) return (7);
   return (DONE);
}

 int ForegroundDetection::WriteFile (CXMLIO* pIO)
 
{
   static ForegroundDetection dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("ForegroundDetection")) {
      xmlNode.Attribute (TYPE_ID(xint) ,"Options"         ,&Options         ,&dv.Options);
      xmlNode.Attribute (TYPE_ID(int)  ,"CFRadius"        ,&CFRadius        ,&dv.CFRadius);
      xmlNode.Attribute (TYPE_ID(int)  ,"VGFFSize"        ,&VGFFSize        ,&dv.VGFFSize);
      xmlNode.Attribute (TYPE_ID(float),"InitBkgLearnTime",&InitBkgLearnTime,&dv.InitBkgLearnTime);
      xmlNode.Attribute (TYPE_ID(float),"BkgUpdatePeriod" ,&BkgUpdatePeriod ,&dv.BkgUpdatePeriod);
      xmlNode.Attribute (TYPE_ID(float),"SSCDThreshold"   ,&SSCDThreshold   ,&dv.SSCDThreshold);
      Schedule_DBS.WriteFile (pIO,"Schedule_DynamicBackgroundSuppression");
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: ForegroundDetectionClient
//
///////////////////////////////////////////////////////////////////////////////

 ForegroundDetectionClient::ForegroundDetectionClient (   )

{
   Server = NULL;
}

 void ForegroundDetectionClient::Connect (ObjectDetectionServer& server)

{
   Server = &server;
}

 void ForegroundDetectionClient::Disconnect (   )
// [주의] 본 함수를 Close() 함수 내에서 호출하는 것은 위험하다.
//        Server 객체가 먼저 소멸되었을 수도 있기 때문이다.
{
   if (Server != NULL) Server->RemoveAllRequests (this);
   Server = NULL;
}

 int ForegroundDetectionClient::IsConnected (   )

{
   if (Server != NULL) return (TRUE);
   else return (FALSE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: ForegroundDetection_BS
//
///////////////////////////////////////////////////////////////////////////////

 ForegroundDetection_BS::ForegroundDetection_BS (   )
 
{
   ClassID = CLSID_ForegroundDetection_BS;
   #if defined(__CPNI_CERT_SZM)
   Options = FGD_OPTION_PERFORM_DYNAMIC_BACKGROUND_SUPPRESSION;
   #endif
}

 int ForegroundDetection_BS::GetFrgImage (   )

{
   #if defined(__DEBUG_FGD)
   if (!BkgColorImage) _DebugView_FGD->DrawImage (BkgImage,_DVX_FGD,_DVY_FGD);
   else _DebugView_FGD->DrawImage (BkgColorImage,_DVX_FGD,_DVY_FGD);
   _DVY_FGD += BkgImage.Height;
   _DebugView_FGD->DrawText (_T("Background Image"),_DVX_FGD,_DVY_FGD,PC_GREEN);
   _DVY_FGD += 20;
   #endif
   // 초기 배경 학습 기간인 경우에는 더 이상 진행하지 않고 리턴한다.
   if (GetBkgModelUpdateMode (   ) == FGD_BMU_MODE_INITIAL) return (FGD_RCODE_INITIAL_BACKGROUND_LEARNING);
   // 소스 영상에 대한 Background Subtraction을 수행한다.
   PerformBkgSubtraction (FrgImage);
   if (SNRThreshold) RemoveCRs (FrgImage,0,SNRThreshold,FrgImage);
   IntFrgImage1 = FrgImage;
   #if defined(__DEBUG_FGD)
   _DebugView_FGD->DrawImage (FrgImage,_DVX_FGD,_DVY_FGD);
   _DVY_FGD += FrgImage.Height;
   _DebugView_FGD->DrawText  (_T("Background Subtraction Result"),_DVX_FGD,_DVY_FGD,PC_GREEN);
   _DVY_FGD += 20;
   #endif
   // 급작스러운 장면 변화를 체크한다.
   int e_code = DetectSuddenSceneChange (FrgImage);
   if (e_code != FGD_RCODE_NORMAL) return (e_code);
   // Lighting Area에서 Background Subtraction을 수행한다.
   GetFrgImageInLightingArea (FrgImage);
   // Foreground Edge Image들을 얻는다.
   CheckEdgeThreshold (   );
   #if defined(__DEBUG_FGD)
   if (Status & FGD_STATUS_LOW_EDGE_THRESHOLD ) logd ("LOW Edge Threshold!\n\n" );
   if (Status & FGD_STATUS_HIGH_EDGE_THRESHOLD) logd ("HIGH Edge Threshold!\n\n");
   #endif
   GetFrgEdgeImages (FrgImage);
   /***********
   #if defined(__DEBUG_FGD)
   _DebugView_FGD->DrawImage (FrgEdgeImage1,_DVX_FGD,_DVY_FGD);
   _DVY_FGD += FrgEdgeImage1.Height;
   _DebugView_FGD->DrawText (_T("Foreground Edge Detection Result 1"),_DVX_FGD,_DVY_FGD,PC_GREEN);
   _DVY_FGD += 20;
   #endif
   #if defined(__DEBUG_FGD)
   _DebugView_FGD->DrawImage (FrgEdgeImage2,_DVX_FGD,_DVY_FGD);
   _DVY_FGD += FrgEdgeImage2.Height;
   _DebugView_FGD->DrawText (_T("Foreground Edge Detection Result 2"),_DVX_FGD,_DVY_FGD,PC_GREEN);
   _DVY_FGD += 20;
   #endif
   #if defined(__DEBUG_FGD)
   _DebugView_FGD->DrawImage (BkgEdgeImage,_DVX_FGD,_DVY_FGD);
   _DVY_FGD += FrgEdgeImage1.Height;
   _DebugView_FGD->DrawText (_T("Background Edge Detection Result"),_DVX_FGD,_DVY_FGD,PC_GREEN);
   _DVY_FGD += 20;
   #endif
   **********/
   #if defined(__DEBUG_FGD)
   _DebugView_FGD->DrawImage (FrgEdgeImage3,_DVX_FGD,_DVY_FGD);
   _DVY_FGD += FrgEdgeImage3.Height;
   _DebugView_FGD->DrawText (_T("Foreground Edge Detection Result 3"),_DVX_FGD,_DVY_FGD,PC_GREEN);
   _DVY_FGD += 20;
   #endif
   // Light Disturbance Reduction을 수행한다.
   PerformLightDisturbanceReduction (FrgImage);
   #if defined(__DEBUG_FGD)
   _DebugView_FGD->DrawImage (FrgImage,_DVX_FGD,_DVY_FGD);
   _DVY_FGD += FrgImage.Height;
   _DebugView_FGD->DrawText (_T("Light Disturbance Reduction Result"),_DVX_FGD,_DVY_FGD,PC_GREEN);
   _DVY_FGD += 20;
   #endif
   IntFrgImage2 = FrgImage;
   // Dynamic Background Suppression을 수행한다.
   if (Status & FGD_STATUS_DYNAMIC_BACKGROUND_SUPPRESSION) {
      PerformDynamicBackgroundSuppression (FrgImage);
      #if defined(__DEBUG_FGD)
      _DebugView_FGD->DrawImage (FrgImage,_DVX_FGD,_DVY_FGD);
      _DVY_FGD += FrgImage.Height;
      _DebugView_FGD->DrawText  (_T("Dynamic Background Suppression Result"),_DVX_FGD,_DVY_FGD,PC_GREEN);
      _DVY_FGD += 20;
      #endif
   }
   // Foreground Compensation을 수행한다.
   /* 잠시 막음
   if (!(Status & FGD_STATUS_LOW_ILLUMINATION) && (InitFrgDensity < FGD_PARAM_INIT_FGD_THRESHOLD)) {
      CompensateForeground (FrgImage);
      #if defined(__DEBUG_FGD)
      _DebugView_FGD->DrawImage (FrgImage,_DVX_FGD,_DVY_FGD);
      _DVY_FGD += FrgImage.Height;
      _DebugView_FGD->DrawText  (_T("Foreground Compensation Result"),_DVX_FGD,_DVY_FGD,PC_GREEN);
      _DVY_FGD += 20;
      #endif
   }
   */
   // FrgImage에 대하여 Morphological Filtering을 수행한다.
   PerformMorphFiltering (FrgImage);
   #if defined(__DEBUG_FGD)
   _DebugView_FGD->DrawImage (FrgImage,_DVX_FGD,_DVY_FGD);
   _DVY_FGD += FrgImage.Height;
   _DebugView_FGD->DrawText (_T("Morphological Filtering Result"),_DVX_FGD,_DVY_FGD,PC_GREEN);
   _DVY_FGD += 20;
   #endif
   // 그림자 제거를 위해 전경 영역을 Trimming 한다.
   TrimForegroundRegions (FrgImage);
   return (FGD_RCODE_NORMAL);
}

 int ForegroundDetection_BS::Initialize (ISize2D& si_size,float frm_rate)
 
{
   if (ForegroundDetection::Initialize (si_size,frm_rate)) return (1);
   Status |= FGD_STATUS_FOREGROUND_EDGE_IMAGE;
   BkgEdgeImage.Create  (si_size.Width,si_size.Height);
   BkgImage.Create      (si_size.Width,si_size.Height);
   BkgImage.Clear       (   );
   FrgEdgeImage1.Create (si_size.Width,si_size.Height);
   FrgEdgeImage2.Create (si_size.Width,si_size.Height);
   FrgEdgeImage3.Create (si_size.Width,si_size.Height);
   IntFrgImage1.Create  (si_size.Width,si_size.Height);
   IntFrgImage2.Create  (si_size.Width,si_size.Height);
   BkgEdgeMap.Create    (si_size.Width,si_size.Height);
   BkgEdgeMap.Clear     (   );
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: ForegroundDetection_SGBM
//
///////////////////////////////////////////////////////////////////////////////

 ForegroundDetection_SGBM::ForegroundDetection_SGBM (   )

{
   ClassID      = CLSID_ForegroundDetection_SGBM;

   DThreshold   = 3.0f;
   MinVariance  = 5.0f  * 5.0f;
   MaxVariance  = 30.0f * 30.0f;
   InitVariance = 10.0f * 10.0f;

   Alpha[FGD_BMU_SPEED_NORMAL] = 0.025f;
   Alpha[FGD_BMU_SPEED_FAST  ] = 0.05f;
   Alpha[FGD_BMU_SPEED_SLOW  ] = 0.001f;
}

 ForegroundDetection_SGBM::~ForegroundDetection_SGBM (   )
 
{
   Close (   );
}

 void ForegroundDetection_SGBM::Close (   )
 
{
   ForegroundDetection_BS::Close (   );
   Variances.Delete (   );
   BkgModel.Delete  (   );
}

 void ForegroundDetection_SGBM::InitBkgModel (BGRImage* s_cimage)

{
   int x,y;

   int    init_var1 = (int)(InitVariance * 256);
   ushort init_var2 = (ushort)(init_var1 >> 8);
   for (y = 0; y < BkgModel.Height; y++) {
      byte*     l_s_image   = SrcImage[y];
      byte*     l_bg_image  = BkgImage[y];
      ushort*   l_variances = Variances[y];
      BkgPixel* bp          = BkgModel[y];
      for (x = 0; x < BkgModel.Width; x++) {
         l_bg_image[x]  = l_s_image[x];
         if (!l_bg_image[x]) l_bg_image[x] = 1;
         bp->MeanY      = (ushort)l_s_image[x] << 8;
         bp->VarianceY  = init_var1;
         l_variances[x] = init_var2;
         bp++;
      }
   }
   if (s_cimage != NULL) {
      if (BkgColorImage.Width != s_cimage->Width || BkgColorImage.Height != s_cimage->Height) BkgColorImage.Create (s_cimage->Width,s_cimage->Height);
      for (y = 0; y < BkgModel.Height; y++) {
         BGRPixel* sp = (*s_cimage)[y];
         BGRPixel* dp = BkgColorImage[y];
         BkgPixel* bp = BkgModel[y];
         for (x = 0; x < BkgModel.Width; x++) {
            bp->MeanR = (ushort)sp->R << 8;
            bp->MeanG = (ushort)sp->G << 8;
            bp->MeanB = (ushort)sp->B << 8;
            *dp = *sp;
            sp++,dp++,bp++;
         }
      }
   }
}

 int ForegroundDetection_SGBM::Initialize (ISize2D& si_size,float frm_rate)
 
{
   if (ForegroundDetection_BS::Initialize (si_size,frm_rate)) return (1);
   BkgModel.Create  (si_size.Width,si_size.Height);
   Variances.Create (si_size.Width,si_size.Height);
   return (DONE);
}

 inline int ForegroundDetection_SGBM::IsBkgPixel (byte p,int x,int y)

{
   int dp = (int)p - BkgImage[y][x];
   if (dp * dp <= (int)Variances[y][x]) return (TRUE);
   else return (FALSE);
}

 void ForegroundDetection_SGBM::PerformBkgSubtraction (GImage &fg_image)

{
   if ((SrcColorImage != NULL) && !(Options & FGD_OPTION_PERFORM_VIDEO_STABILIZATION)) {
      PerformBkgSubtraction_Main (SrcImage,*SrcColorImage,fg_image);
   }
   else {
      PerformBkgSubtraction_Main (SrcImage,fg_image);
   }
}

 void ForegroundDetection_SGBM::PerformBkgSubtraction_Main (GImage& s_image,GImage& d_image)

{
   int x,y;

   for (y = 0; y < s_image.Height; y++) {
      byte*   l_s_image   = s_image[y];
      byte*   l_d_image   = d_image[y];
      byte*   l_bg_image  = BkgImage[y];
      ushort* l_variances = Variances[y];
      for (x = 0; x < s_image.Width; x++) {
         int dp = (int)l_s_image[x] - l_bg_image[x];
         if (dp * dp > (int)l_variances[x]) l_d_image[x] = PG_WHITE;
         else l_d_image[x] = PG_BLACK;
      }
   }
   d_image.SetBoundary (2,PG_BLACK);
}

 void ForegroundDetection_SGBM::PerformBkgSubtraction_Main (GImage& s_image,BGRImage& s_cimage,GImage& d_image)

{
   int x,y;

   for (y = 0; y < s_cimage.Height; y++) {
      BGRPixel* sp          = s_cimage[y];
      BGRPixel* bp          = BkgColorImage[y];
      byte*     l_s_image   = s_image[y];
      byte*     l_bg_image  = BkgImage[y];
      byte*     l_d_image   = d_image[y];
      ushort*   l_variances = Variances[y];
      for (x = 0; x < s_cimage.Width; x++) {
         int var1 = (int)l_variances[x];
         int var2 = 2 * var1;
         int dp  = (int)l_s_image[x] - l_bg_image[x];
         if (dp * dp > var1) l_d_image[x] = PG_WHITE;
         else {
            dp = (int)sp->R - bp->R;
            if (dp * dp > var2) l_d_image[x] = PG_WHITE;
            else {
               dp = (int)sp->G - bp->G;
               if (dp * dp > var2) l_d_image[x] = PG_WHITE;
               else {
                  dp = (int)sp->B - bp->B;
                  if (dp * dp > var2) l_d_image[x] = PG_WHITE;
                  else l_d_image[x]= PG_BLACK;
               }
            }
         }
         sp++,bp++;
      }
   }
   d_image.SetBoundary (2,PG_BLACK);
}

 void ForegroundDetection_SGBM::UpdateBkgModel_Main (GImage& m_image)

{
   int i,x,y;
   int alpha[3];

   if (Status & (FGD_STATUS_LOW_ILLUMINATION|FGD_STATUS_IR_MODE)) {
      DThreshold  = 2.3f;
      MaxVariance = 25.0f * 25.0f;
   }
   if (FrameCount == 1) {
      InitBkgModel (SrcColorImage);
      return;
   }
   int bmup = GetBkgModelUpdateMode (   );
   if (bmup == FGD_BMU_MODE_INITIAL) {
      // 초기 배경 학습 시에는 입력 영상의 평균을 구한다.
      int a = 256 / FrameCount;
      for (i = 0; i < 3; i++) alpha[i] = a;
   }
   else {
      for (i = 0; i < 3; i++) alpha[i] = (int)(Alpha[i] * 256);
      if (LightingArea.Width && LightingArea.Height) {
         int ey = LightingArea.Y + LightingArea.Height;
         int ex = LightingArea.X + LightingArea.Width;
         for (y = LightingArea.Y; y < ey; y++) {
            byte* l_m_image = m_image[y];
            for (x = LightingArea.X; x < ex; x++) {
               if (l_m_image[x] == FGD_BMU_SPEED_NORMAL) l_m_image[x] = FGD_BMU_SPEED_SLOW;
            }
         }
      }
   }
   int sd_thrsld = (int)(DThreshold  * DThreshold);
   int min_var   = (int)(MinVariance * 256);
   int max_var   = (int)(MaxVariance * 256);
   for (y = 0; y < BkgModel.Height; y++) {
      BkgPixel* bp          = BkgModel[y];
      byte*     l_s_image   = SrcImage[y];
      byte*     l_m_image   = m_image[y];
      byte*     l_bg_image  = BkgImage[y];
      ushort*   l_variances = Variances[y];
      for (x = 0; x < BkgModel.Width; x++) {
         if (l_m_image[x] != FGD_BMU_SPEED_ZERO) {
            int a         = alpha[l_m_image[x]];
            int dp        = (int)((ushort)l_s_image[x] << 8) - bp->MeanY;
            int adp       = (a * dp) >> 8;
            bp->MeanY     = (ushort)(bp->MeanY + adp);
            bp->VarianceY = (int)(((256 - a) * bp->VarianceY + dp * adp * sd_thrsld) >> 8);
            if      (bp->VarianceY < min_var) bp->VarianceY = min_var;
            else if (bp->VarianceY > max_var) bp->VarianceY = max_var;
            byte p = (byte)(bp->MeanY >> 8);
            // 배경 영상의 픽셀 값이 0인 경우는 없도록 만든다.
            if (!p) l_bg_image[x] = 1;
            else l_bg_image[x] = p;
            l_variances[x] = (ushort)(bp->VarianceY >> 8);
         }
         bp++;
      }
   }
   if ((SrcColorImage != NULL) && !(Options & FGD_OPTION_PERFORM_VIDEO_STABILIZATION)) UpdateBkgColorImage (*SrcColorImage,m_image,alpha);
   if ((bmup == FGD_BMU_MODE_MAJOR) && (Options & FGD_OPTION_PERFORM_VIDEO_STABILIZATION)) RefImage = SrcImage;
}

 void ForegroundDetection_SGBM::UpdateBkgColorImage (BGRImage& s_cimage,GImage& m_image,int* alpha)

{
   int x,y;
   
   if (!BkgColorImage) return;
   for (y = 0; y < BkgModel.Height; y++) {
      BGRPixel* sp = s_cimage[y];
      BGRPixel* dp = BkgColorImage[y];
      BkgPixel* bp = BkgModel[y];
      byte* l_m_image = m_image[y];
      for (x = 0; x < BkgModel.Width; x++) {
         if (l_m_image[x] != FGD_BMU_SPEED_ZERO) {
            int a = alpha[l_m_image[x]];
            bp->MeanR = (ushort)(bp->MeanR + ((a * (((int)sp->R << 8) - bp->MeanR)) >> 8));
            bp->MeanG = (ushort)(bp->MeanG + ((a * (((int)sp->G << 8) - bp->MeanG)) >> 8));
            bp->MeanB = (ushort)(bp->MeanB + ((a * (((int)sp->B << 8) - bp->MeanB)) >> 8));
            dp->R = (byte)(bp->MeanR >> 8);
            dp->G = (byte)(bp->MeanG >> 8);
            dp->B = (byte)(bp->MeanB >> 8);
         }
         sp++,dp++,bp++;
      }
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: ForegroundDetection_MGBM
//
///////////////////////////////////////////////////////////////////////////////

 ForegroundDetection_MGBM::ForegroundDetection_MGBM (   )

{
   ClassID                     = CLSID_ForegroundDetection_MGBM;
   WThreshold                  = 0.7f;
   DThreshold                  = 2.5f;
   MinVariance                 = 5.0f  * 5.0f;
   InitVariance                = 10.0f * 10.0f;
   Alpha[FGD_BMU_SPEED_NORMAL] = 0.01f;
   Alpha[FGD_BMU_SPEED_FAST  ] = 0.05f;
}

 ForegroundDetection_MGBM::~ForegroundDetection_MGBM (   )
 
{
   Close (   );
}

 void ForegroundDetection_MGBM::Close (   )
 
{
   ForegroundDetection_BS::Close (   );
   Variances.Delete (   );
   BkgModel.Delete  (   );
}

 inline int ForegroundDetection_MGBM::IsBkgPixel (byte p,int x,int y)

{
   int dp = (int)p - BkgImage[y][x];
   if (dp * dp <= Variances[y][x]) return (TRUE);
   else return (FALSE);
}

 void ForegroundDetection_MGBM::GetBkgImage (int n,GImage &d_image)

{
   int x,y;

   for (y = 0; y < d_image.Height; y++) {
      BkgPixel *l_bg_model = BkgModel[y];
      byte     *l_d_image  = d_image[y];
      for (x = 0; x < d_image.Width; x++)
         l_d_image[x] = (byte)(l_bg_model[x].MeansY[n]);
   }
}

 void ForegroundDetection_MGBM::InitBkgModel (BGRImage* s_cimage)

{
   int x,y;

   BkgModel.Clear (   );
   ushort init_var = (ushort)InitVariance;
   for (y = 0; y < BkgModel.Height; y++) {
      byte*     l_s_image   = SrcImage[y];
      byte*     l_bg_image  = BkgImage[y];
      ushort*   l_variances = Variances[y];
      BkgPixel* bp          = BkgModel[y];
      for (x = 0; x < BkgModel.Width; x++) {
         bp->NumGCs        = 1;
         bp->NumBGCs       = 1;
         bp->MeansY[0]     = (float)l_s_image[x];
         bp->Weights[0]    = 1.0f;
         bp->VariancesY[0] = InitVariance;
         l_variances[x]    = init_var;
         l_bg_image[x]     = l_s_image[x];
         if (!l_bg_image[x]) l_bg_image[x] = 1;
         bp++;
      }
   }
   if (s_cimage != NULL) {
      if (BkgColorImage.Width != s_cimage->Width || BkgColorImage.Height != s_cimage->Height) BkgColorImage.Create (s_cimage->Width,s_cimage->Height);
      for (y = 0; y < BkgModel.Height; y++) {
         BGRPixel* sp = (*s_cimage)[y];
         BGRPixel* dp = BkgColorImage[y];
         BkgPixel* bp = BkgModel[y];
         for (x = 0; x < BkgModel.Width; x++) {
            bp->MeanR = (float)sp->R;
            bp->MeanG = (float)sp->G;
            bp->MeanB = (float)sp->B;
            *dp = *sp;
            sp++,dp++,bp++;
         }
      }
   }
}

 int ForegroundDetection_MGBM::Initialize (ISize2D& si_size,float frm_rate)
 
{
   if (ForegroundDetection_BS::Initialize (si_size,frm_rate)) return (DONE);
   BkgModel.Create  (si_size.Width,si_size.Height);
   Variances.Create (si_size.Width,si_size.Height);
   return (1);
}

 void ForegroundDetection_MGBM::PerformBkgSubtraction (GImage &fg_image)

{
   int x,y,z;

   fg_image.Set (0,0,fg_image.Width,fg_image.Height,PG_WHITE);
   for (y = 0; y < SrcImage.Height; y++) {
      byte   *l_s_image   = SrcImage[y];
      byte   *l_fg_image  = fg_image[y];
      byte   *l_bg_image  = BkgImage[y];
      ushort *l_variances = Variances[y];
      BkgPixel *bp = BkgModel[y];
      for (x = 0; x < SrcImage.Width; x++) {
         int dp1 = (int)l_s_image[x] - l_bg_image[x];
         if (dp1 * dp1 <= (int)l_variances[x]) l_fg_image[x] = PG_BLACK;
         if (l_fg_image[x] == PG_WHITE) {
            for (z = 1; z < bp->NumBGCs; z++) {
               float dp2 = l_s_image[x] - bp->MeansY[z];
               if (dp2 * dp2 <= bp->VariancesY[z]) {
                  l_fg_image[x] = PG_BLACK;
                  break;
               }
            }
         }
         bp++;
      }
   }
}

 void ForegroundDetection_MGBM::UpdateBkgModel_Main (GImage &m_image)

{
   int   i,x,y,z;
   float alpha[3];

   if (FrameCount == 1) {
      InitBkgModel (SrcColorImage);
      return;
   }
   int bmup = GetBkgModelUpdateMode (   );
   if (bmup == FGD_BMU_MODE_INITIAL) {
      float a = 1.0f / FrameCount;
      for (i = 0; i < 3; i++) alpha[i] = a;
   }
   else {
      for (i = 0; i < 3; i++) alpha[i] = Alpha[i];
   }
   float sd_threshold = DThreshold * DThreshold;
   for (y = 0; y < BkgModel.Height; y++) {
      byte*     l_s_image   = SrcImage[y];
      byte*     l_m_image   = m_image[y];
      byte*     l_bg_image  = BkgImage[y];
      ushort*   l_variances = Variances[y];
      BkgPixel* bp          = BkgModel[y];
      for (x = 0; x < BkgModel.Width; x++) {
         if (l_m_image[x] != FGD_BMU_SPEED_ZERO) {
            // p에 대응하는 Gaussian Component를 Matched Component라고 하자.
            // 만약 Matched Component가 존재하면 그 컴포넌트의 파라미터 값들을 업데이트한다.
            // Matched Component가 아닌 경우에는 Weight 값만 업데이트한다.
            int   mc    = -1;
            float a     = Alpha[l_m_image[x]];
            float b     = 1.0f - a;
            float p     = (float)l_s_image[x];
            float sum_w = 0.0f;
            for (z = 0; z < bp->NumGCs; z++) {
               float dp  = p - bp->MeansY[z];
               float sdp = dp * dp;
               if (sdp <= bp->VariancesY[z]) {
                  mc = z;
                  bp->MeansY[z]  += a * dp;
                  bp->Weights[z] = b * bp->Weights[z] + a;
                  float var = b * bp->VariancesY[z] + a * sdp * sd_threshold;
                  if (var < MinVariance) bp->VariancesY[z] = MinVariance;
                  else bp->VariancesY[z] = var;
                  sum_w += bp->Weights[z];
                  break;
               }
               else {
                  bp->Weights[z] *= b;
                  if (bp->Weights[z] < 0.1f) bp->Weights[z] = 0.1f;
                  sum_w += bp->Weights[z];
               }
            }
            // Matched Component가 없으면 새로운 Gaussian Component를 추가한다. 만약 더 이상 새로운
            // Gaussian Component를 추가할 수 없으면 Least Probable Component를 새 컴포넌트로 교체한다.
            if (mc == -1) {
               if (bp->NumGCs == 3) {
                  bp->MeansY[2]     = p;
                  bp->Weights[2]    = 0.1f;
                  bp->VariancesY[2] = InitVariance;
               }
               else {
                  bp->MeansY[bp->NumGCs]     = p;
                  bp->Weights[bp->NumGCs]    = 0.1f;
                  bp->VariancesY[bp->NumGCs] = InitVariance;
                  bp->NumGCs++;
               }
            }
            else {
               // Matched Component가 아닌 컴포넌트들에 대하여 Weight 값을 마저 업데이트한다.
               for (++z; z < bp->NumGCs; z++) {
                  bp->Weights[z] *= b;
                  if (bp->Weights[z] < 0.1f) bp->Weights[z] = 0.1f;
                  sum_w += bp->Weights[z];
               }
               // 각 컴포넌트를 Weight / Sigma 값에 의해 내림차순으로 정렬한다.
               for (z = mc; z > 0; z--) {
                  int z1 = z - 1;
                  float v1 = bp->Weights[z1] * bp->Weights[z1] / bp->VariancesY[z1];
                  float v2 = bp->Weights[z ] * bp->Weights[z ] / bp->VariancesY[z ]; 
                  if (v1 < v2) {
                     Swap (bp->MeansY[z1],bp->MeansY[z]);
                     Swap (bp->Weights[z1],bp->Weights[z]);
                     Swap (bp->VariancesY[z1],bp->VariancesY[z]);
                  }
                  else break;
               }
            }
            // Weight 값들을 합이 1이 되도록 정규화한다.
            for (z = 0; z < bp->NumGCs; z++) bp->Weights[z] /= sum_w;
            // Background에 속하는 Gaussian Component의 수를 센다.
            for (z = 0, sum_w = 0.0f; z < bp->NumGCs; z++) {
               sum_w += bp->Weights[z];
               if (sum_w >= WThreshold) {
                  bp->NumBGCs = z + 1;
                  break;
               }
            }
            l_bg_image[x] = (byte)bp->MeansY[0];
            if (!l_bg_image[x]) l_bg_image[x] = 1;
            l_variances[x] = (ushort)bp->VariancesY[0];
         }
         bp++;
      }
   }
   if ((SrcColorImage != NULL) && !(Options & FGD_OPTION_PERFORM_VIDEO_STABILIZATION)) UpdateBkgColorImage (*SrcColorImage,m_image,alpha);
   if ((bmup == FGD_BMU_MODE_MAJOR) && (Options & FGD_OPTION_PERFORM_VIDEO_STABILIZATION)) Convert (SrcImage,RefImage);
}

 void ForegroundDetection_MGBM::UpdateBkgColorImage (BGRImage& s_cimage,GImage& m_image,float* alpha)
 
{
   int x,y;
   
   if (!BkgColorImage) return;
   for (y = 0; y < s_cimage.Height; y++) {
      BGRPixel* sp = s_cimage[y];
      BGRPixel* dp = BkgColorImage[y];
      BkgPixel* bp = BkgModel[y];
      byte *l_m_image = m_image[y];
      for (x = 0; x < s_cimage.Width; x++) {
         if (l_m_image[x] != FGD_BMU_SPEED_ZERO) {
            float a = alpha[l_m_image[x]];
            bp->MeanR += a * (sp->R - bp->MeanR);
            bp->MeanG += a * (sp->G - bp->MeanG);
            bp->MeanB += a * (sp->B - bp->MeanB);
            dp->R = (byte)bp->MeanR;
            dp->G = (byte)bp->MeanG;
            dp->B = (byte)bp->MeanB;
         }
         sp++,dp++,bp++;
      }
   }
}

}

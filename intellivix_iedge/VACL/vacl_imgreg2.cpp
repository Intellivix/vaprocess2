#include "vacl_corner.h"
#include "vacl_geometry.h"
#include "vacl_imgreg.h"
#include "vacl_opencv.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: LocalImageRegistration_FPM
//
///////////////////////////////////////////////////////////////////////////////

#define MIN_NUM_FEATURE_POINTS   10

 LocalImageRegistration_FPM::LocalImageRegistration_FPM (   )
 
{
   // Input Parameters
   NumSamples         = 50;
   CNMSWinSize        = 9;
   SearchRange        = 20;
   CornerWinSize      = 5;
   CornerPatchSize    = 15;
   MinEdgeStrength    = 21;
   MinCornerStrength  = 15.0f;
   MatchingThreshold  = 0.0f;
   InlierSupportRange = 2.0f;
}

 LocalImageRegistration_FPM::~LocalImageRegistration_FPM (   )

{
   Close (   );
}

 void LocalImageRegistration_FPM::Close (   )
 
{
   RefImage.Delete  (   );
   SrcImage.Delete  (   );
   Inliers.Delete   (   );
   RefPoints.Delete (   );
   SrcPoints.Delete (   );
   Correspondences.Delete (   );
}

 Matrix LocalImageRegistration_FPM::ComputeHomography (IArray1D &pi_array,int flag_normalize)
 
{
   int i;
   
   IP2DArray1D s_points1(pi_array.Length);
   IP2DArray1D s_points2(pi_array.Length);
   for (i = 0; i < pi_array.Length; i++) {
      s_points1[i] = Correspondences[pi_array[i]][0];
      s_points2[i] = Correspondences[pi_array[i]][1];
   }
   Matrix mat_H(3,3);
   GetHomography (s_points1,s_points2,mat_H,flag_normalize);
   return (Matrix(&mat_H));
}

 Matrix LocalImageRegistration_FPM::EstimateHomography (   )
 
{
   int i,j;
   static time_t seed = 0;
   
   if (!seed) {
      time (&seed);
      seed += 1000;
   }
   else seed++;
   srand ((uint)seed);
   IArray1D pi_array(Correspondences.Length);
   for (i = 0; i < pi_array.Length; i++) pi_array[i] = i;
   int ei = pi_array.Length * 5;
   for (i = 0; i < ei; i++) {
      int i1 = rand (   ) % pi_array.Length;
      int i2 = rand (   ) % pi_array.Length;
      Swap(pi_array[i1],pi_array[i2]);
   }
   int n_inliers = -1;
   IArray1D s_points(4);
   IArray1D m_points(Correspondences.Length);
   int range = Correspondences.Length - 4;
   for (i = 0; i < NumSamples; i++) {
      int i1 = rand (   ) % range;
      for (j = 0; j < 4; j++) s_points[j] = pi_array[i1 + j];
      Matrix mat_H = ComputeHomography (s_points,FALSE);
      int n = GetInliers (mat_H,m_points);
      if (n > n_inliers) {
         n_inliers = n;
         Inliers   = m_points.Extract (0,n);
      }
   }
   Matrix mat_H = ComputeHomography (Inliers,TRUE);
   return (Matrix(&mat_H));
}

 int LocalImageRegistration_FPM::FindMatchingPoint12 (int idx_p1,Matrix &mat_H,IArray2D &p_map2,FArray2D &ms_array)
 
{
   int   x,y;
   float score;
   
   IPoint2D p1 = RefPoints[idx_p1];
   Matrix vec_p2 = mat_H * GetHomogeneousVector (p1);
   int cx = (int)(vec_p2(0) / vec_p2(2) + 0.5f);
   int cy = (int)(vec_p2(1) / vec_p2(2) + 0.5f);
   int sx = cx - SearchRange;
   int sy = cy - SearchRange;
   int ex = cx + SearchRange;
   int ey = cy + SearchRange;
   if (sx < 0) sx = 0;
   if (sy < 0) sy = 0;
   if (ex >= p_map2.Width ) ex = p_map2.Width  - 1;
   if (ey >= p_map2.Height) ey = p_map2.Height - 1;
   int   idx_p2    = -1;
   float max_score = -1.0f;
   for (y = sy; y <= ey; y++) {
      for (x = sx; x <= ex; x++) {
         if (p_map2[y][x]) {
            int idx_tp2 = p_map2[y][x];
            if (ms_array[idx_p1][idx_tp2]) score = ms_array[idx_p1][idx_tp2];
            else {
               IPoint2D pt(x,y);
               score = GetRMSD (RefImage,p1,SrcImage,pt);
               ms_array[idx_p1][idx_tp2] = score;
            }
            if (score > max_score) {
               max_score = score;
               idx_p2    = idx_tp2;
            }
         }
      }
   }
   if (max_score >= MatchingThreshold) return (idx_p2);
   else return (-1);
}

 int LocalImageRegistration_FPM::FindMatchingPoint21 (int idx_p2,Matrix &mat_IH,IArray2D &p_map1,FArray2D &ms_array)
 
{
   int   x,y;
   float score;
   
   IPoint2D p2 = SrcPoints[idx_p2];
   Matrix vec_p1 = mat_IH * GetHomogeneousVector (p2);
   int cx = (int)(vec_p1(0) / vec_p1(2) + 0.5f);
   int cy = (int)(vec_p1(1) / vec_p1(2) + 0.5f);
   int sx = cx - SearchRange;
   int sy = cy - SearchRange;
   int ex = cx + SearchRange;
   int ey = cy + SearchRange;
   if (sx < 0) sx = 0;
   if (sy < 0) sy = 0;
   if (ex >= p_map1.Width ) ex = p_map1.Width  - 1;
   if (ey >= p_map1.Height) ey = p_map1.Height - 1;
   int   idx_p1    = -1;
   float max_score = -1.0f;
   for (y = sy; y <= ey; y++) {
      for (x = sx; x <= ex; x++) {
         if (p_map1[y][x]) {
            int idx_tp1 = p_map1[y][x];
            if (ms_array[idx_tp1][idx_p2]) score = ms_array[idx_tp1][idx_p2];
            else {
               IPoint2D pt(x,y);
               score = GetRMSD (SrcImage,p2,RefImage,pt);
               ms_array[idx_tp1][idx_p2] = score;
            }
            if (score > max_score) {
               max_score = score;
               idx_p1    = idx_tp1;
            }
         }
      }
   }
   if (max_score >= MatchingThreshold) return (idx_p1);
   else return (-1);
}

 int LocalImageRegistration_FPM::GetInliers (IP2DPArray1D &inliers)
 
{
   int i;

   if (!Inliers) {
      inliers.Delete (   );
      return (1);
   }
   inliers.Create (Inliers.Length);
   for (i = 0; i < Inliers.Length; i++) inliers[i] = Correspondences[Inliers[i]];
   return (DONE);
}
 
 int LocalImageRegistration_FPM::GetInliers (Matrix &mat_H,IArray1D &inliers)

{
   int i;
   IPoint2D p2;

   int n_inliers = 0;
   int sd_thrsld = (int)(InlierSupportRange * InlierSupportRange + 0.5f);
   for (i = 0; i < Correspondences.Length; i++) {
      Matrix vec_p2 = mat_H * GetHomogeneousVector (Correspondences[i][0]);
      p2.X = (int)(vec_p2(0) / vec_p2(2));
      p2.Y = (int)(vec_p2(1) / vec_p2(2));
      int sd = SquaredMag (Correspondences[i][1] - p2);
      if (sd <= sd_thrsld) inliers[n_inliers++] = i;
   }
   return (n_inliers);
}

 float LocalImageRegistration_FPM::GetNCC (GImage &s_image1,IPoint2D &p1,GImage &s_image2,IPoint2D &p2)
 // NCC: Normalized Cross-Correlation
{
   int x1,y1,y2;

   int h_cps = CornerPatchSize / 2;
   int sx1 = p1.X - h_cps;
   int sy1 = p1.Y - h_cps;
   int ex1 = sx1  + CornerPatchSize;
   int ey1 = sy1  + CornerPatchSize;
   int sx2 = p2.X - h_cps;
   int sy2 = p2.Y - h_cps;
   int sum_r = 0,sum_s = 0;
   int64 sum_rr = 0,sum_ss = 0,sum_rs = 0;
   for (y1 = sy1,y2 = sy2; y1 < ey1; y1++,y2++) {
      byte* l_s_image1 = s_image1[y1] + sx1;
      byte* l_s_image2 = s_image2[y2] + sx2;
      for (x1 = sx1; x1 < ex1; x1++) {
         sum_r  += (*l_s_image1);
         sum_s  += (*l_s_image2);
         sum_rs += (int)(*l_s_image1) * (*l_s_image2);
         sum_rr += (int)(*l_s_image1) * (*l_s_image1);
         sum_ss += (int)(*l_s_image2) * (*l_s_image2);
         l_s_image1++;
         l_s_image2++;
      }
   }
   int area   = CornerPatchSize * CornerPatchSize;
   float m_r  = (float)((double)sum_r  / area);
   float m_s  = (float)((double)sum_s  / area);
   float m_rs = (float)((double)sum_rs / area);
   float m_rr = (float)((double)sum_rr / area);
   float m_ss = (float)((double)sum_ss / area);
   float ncc = (m_rs - m_r * m_s) / ((float)sqrt (m_rr - m_r * m_r) * (float)sqrt (m_ss - m_s * m_s));
   return (ncc);
}

 void LocalImageRegistration_FPM::GetPointList (GImage &s_image,IP2DArray1D &p_array)

{
   int x,y;

   int n = 0;
   for (y = 0; y < s_image.Height; y++)
      for (x = 0; x < s_image.Width; x++)
         if (s_image[y][x]) p_array[n++](x,y);
}

 void LocalImageRegistration_FPM::GetPointMap (IP2DArray1D &s_array,IArray2D &d_map)

{
   int i;

   d_map.Clear (   );
   for (i = 0; i < s_array.Length; i++)
      d_map[s_array[i].Y][s_array[i].X] = i;
}

 float LocalImageRegistration_FPM::GetRMSD (GImage &s_image1,IPoint2D &p1,GImage &s_image2,IPoint2D &p2)
// RMSD: Root - Mean - Squared Difference
{
   int x1,y1,x2,y2;

   int h_cps = CornerPatchSize / 2;
   int sx1 = p1.X - h_cps;
   int sy1 = p1.Y - h_cps;
   int ex1 = p1.X + h_cps;
   int ey1 = p1.Y + h_cps;
   int sx2 = p2.X - h_cps;
   int sy2 = p2.Y - h_cps;
   int ex2 = p2.X + h_cps;
   int ey2 = p2.Y + h_cps;
   if (sx1 < 0 || sy1 < 0 || sx2 < 0 || sy2 < 0) return (-1.0f);
   if (ex1 >= s_image1.Width  || ex2 >= s_image1.Width ) return (-1.0f);
   if (ey1 >= s_image1.Height || ey2 >= s_image1.Height) return (-1.0f);
   int64 ssd = 0;
   for (y1 = sy1,y2 = sy2; y1 < ey1; y1++,y2++) {
      byte* l_s_image1 = s_image1[y1];
      byte* l_s_image2 = s_image2[y2];
      for (x1 = sx1,x2 = sx2; x1 < ex1; x1++,x2++) {
         int d = (int)l_s_image1[x1] - l_s_image2[x2];
         ssd += d * d;
      }
   }
   float rmsd = (255.0f - (float)sqrt ((double)ssd / (CornerPatchSize * CornerPatchSize))) / 255.0f;
   return (rmsd);
}

 int LocalImageRegistration_FPM::Initialize (GImage &s_image)
 
{
   Close (   );
   RefImage = s_image;
   GImage p_image(s_image.Width,s_image.Height);
   int n_points = DetectCorners_Tomasi (RefImage,CornerWinSize,MinEdgeStrength,MinCornerStrength,CNMSWinSize,p_image);
   if (n_points < MIN_NUM_FEATURE_POINTS) return (1);
   RefPoints.Create (n_points);
   GetPointList (p_image,RefPoints);
   return (DONE);
}

 Matrix LocalImageRegistration_FPM::NormalizeImageCoordinates (IP2DArray1D &s_points,FP2DArray1D &d_points)

{
   int i;

   int v = 0;
   IPoint2D m(0,0);
   for (i = 0; i < s_points.Length; i++) {
      m.X += s_points[i].X;
      m.Y += s_points[i].Y;
      v   += s_points[i].X * s_points[i].X + s_points[i].Y * s_points[i].Y;
   }
   m /= s_points.Length;
   v /= s_points.Length;
   float s = (float)sqrt (2.0f / (v - IP(m,m)));
   for (i = 0; i < s_points.Length; i++) {
      d_points[i].X = s * (s_points[i].X - m.X);
      d_points[i].Y = s * (s_points[i].Y - m.Y);
   }
   Matrix mat_T(3,3);
   mat_T.Clear (   );
   mat_T[0][0] = mat_T[1][1] = (double)s;
   mat_T[0][2] = (double)(-s * m.X);
   mat_T[1][2] = (double)(-s * m.Y);
   mat_T[2][2] = 1.0;
   return (Matrix(&mat_T));
}

 int LocalImageRegistration_FPM::Perform (GImage &s_image,Matrix &mat_H)
 
{
   Inliers.Delete (   );
   if (!RefImage) return (1);
   else if (!SrcImage) SrcImage = s_image;
   else {
      RefImage  = SrcImage;
      SrcImage  = s_image;
      RefPoints = SrcPoints;
      SrcPoints.Delete (   );
      Correspondences.Delete (   );
   }
   if (PerformFeaturePointMatching (mat_H)) return (2);
   mat_H = EstimateHomography (   );
   return (DONE);
}

 int LocalImageRegistration_FPM::PerformFeaturePointMatching (Matrix &mat_H)
 
{
   int i,j,n;
   
   GImage p_image(SrcImage.Width,SrcImage.Height);
   int n_points = DetectCorners_Tomasi (SrcImage,CornerWinSize,MinEdgeStrength,MinCornerStrength,CNMSWinSize,p_image);
   if (n_points < MIN_NUM_FEATURE_POINTS) {
      SrcPoints.Delete (   );
      return (1);
   }
   SrcPoints.Create (n_points);
   GetPointList (p_image,SrcPoints);
   if (!RefPoints) return (2);
   IArray2D p_map1(RefImage.Width,RefImage.Height);
   GetPointMap (RefPoints,p_map1);
   IArray2D p_map2(SrcImage.Width,SrcImage.Height);
   GetPointMap (SrcPoints,p_map2);
   FArray2D ms_array(SrcPoints.Length,RefPoints.Length);
   ms_array.Clear (   );
   IArray1D pi_array2(RefPoints.Length);
   for (i = 0; i < RefPoints.Length; i++)
      pi_array2[i] = FindMatchingPoint12 (i,mat_H,p_map2,ms_array);
   Matrix mat_IH = Inv(mat_H);
   IArray1D pi_array1(SrcPoints.Length);
   for (i = 0; i < SrcPoints.Length; i++)
      pi_array1[i] = FindMatchingPoint21 (i,mat_IH,p_map1,ms_array);
   IP2DPArray1D pc_array(RefPoints.Length);
   for (i = n = 0; i < RefPoints.Length; i++) {
      j = pi_array2[i];
      if (j != -1 && i == pi_array1[j]) {
         pc_array[n][0] = RefPoints[i];
         pc_array[n][1] = SrcPoints[j];
         n++;
      }
   }
   if (n >= MIN_NUM_FEATURE_POINTS) {
      Correspondences = pc_array.Extract (0,n);
      return (DONE);
   }
   else {
      Correspondences.Delete (   );
      return (3);
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: OpenCV_PhaseCorrelation
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_OPENCV)

 OpenCV_PhaseCorrelation::~OpenCV_PhaseCorrelation (   )
 
{
   Close (   );
}

 void OpenCV_PhaseCorrelation::Close (   )
 
{
   HanningWindow.release (   );
   SrcImgSize.Clear (   );
}

 void OpenCV_PhaseCorrelation::Initialize (ISize2D& si_size)
 
{
   Close (   );
   SrcImgSize = si_size;
   createHanningWindow (HanningWindow,Size(si_size.Width,si_size.Height),CV_32F);
}

 int OpenCV_PhaseCorrelation::Perform (FArray2D &s_array1,FArray2D &s_array2,FPoint2D& d_shift)
 
{
   d_shift.Clear (   );
   if (SrcImgSize.Width != s_array1.Width || SrcImgSize.Height != s_array1.Height) return (1);
   cv::Mat s_ary1,s_ary2;
   OpenCV_InitMat (s_array1,s_ary1);
   OpenCV_InitMat (s_array2,s_ary2);
   Point2d shift = phaseCorrelate (s_ary1,s_ary2,HanningWindow);
   d_shift((float)shift.x,(float)shift.y);
   return (DONE);
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 FPoint2D GetShift (Matrix& mat_H)

{
   FPoint2D shift;
   shift.X = (float)(mat_H[0][2] / mat_H[2][2]);
   shift.Y = (float)(mat_H[1][2] / mat_H[2][2]);
   return (shift);
}

 int PerformImageRegistration_KLT (GImage& s_image,GImage& r_image,Matrix& d_mat_H)
#if defined(__LIB_OPENCV)
{
   int i;
   
   cv::Mat m_s_image;
   OpenCV_InitMat (s_image,m_s_image);
   cv::Mat m_r_image;
   OpenCV_InitMat (r_image,m_r_image);
   // s_image로부터 FP(Feature Point)들을 추출한다.
   vector<cv::Point2f> s_fps;
   int    max_n_points  = 300;   // PARAMETERS
   double quality_level = 0.001; // PARAMETERS
   double min_distance  = 20.0;  // PARAMETERS
   int    block_size    = 5;     // PARAMETERS
   int    max_level     = 5;     // PARAMETERS
   cv::Size win_size(21,21);     // PARAMETERS
   goodFeaturesToTrack (m_s_image,s_fps,max_n_points,quality_level,min_distance,noArray(),block_size,true);
   if ((int)s_fps.size (   ) < 4) return (1);
   // s_image와 r_image의 Image Pyramid를 만든다.
   vector<cv::Mat> si_pyramid;
   buildOpticalFlowPyramid (m_s_image,si_pyramid,win_size,max_level,true);
   vector<cv::Mat> ri_pyramid;
   buildOpticalFlowPyramid (m_r_image,ri_pyramid,win_size,max_level,true);
   // FP Flow(s_image => r_image)를 구한다.
   vector<cv::Point2f> r_fps;
   vector<uchar> status;
   vector<float> error;
   calcOpticalFlowPyrLK (si_pyramid,ri_pyramid,s_fps,r_fps,status,error,win_size,max_level);
   float d_thrsld = 0.25f * GetMinimum (s_image.Width,s_image.Height); // PARAMETERS
   int n = (int)r_fps.size (   );
   vector<cv::Point2f> s_fps2,r_fps2;
   for (i = 0; i < n; i++) {
      if (status[i]) {
         Point2f dp = s_fps[i] - r_fps[i];
         float d = (float)sqrt (dp.x * dp.x + dp.y * dp.y);
         if (d < d_thrsld) {
            s_fps2.push_back (s_fps[i]);
            r_fps2.push_back (r_fps[i]);
         }
      }
   }
   cv::Mat m_mat_H = findHomography (s_fps2,r_fps2,CV_RANSAC);
   d_mat_H.Create (3,3);
   OpenCV_Copy (m_mat_H,d_mat_H);
   return (DONE);
}
#else
{
   return (-1);
}
#endif

}

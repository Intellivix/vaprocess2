#if !defined(__VACL_FOREGND_H)
#define __VACL_FOREGND_H

#include "vacl_boxtrack.h"
#include "vacl_conrgn.h"
#include "vacl_light.h"
#include "vacl_opencv.h"
#include "vacl_schedule.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//
///////////////////////////////////////////////////////////////////////////////

class EventDetection;
class ObjectTracking;
class ObjectDetectionServer;
class RealObjectSizeEstimation;

///////////////////////////////////////////////////////////////////////////////
//
// Class: ForegroundDetection
//
///////////////////////////////////////////////////////////////////////////////

#define FGD_BMU_SPEED_NORMAL   0
#define FGD_BMU_SPEED_FAST     1
#define FGD_BMU_SPEED_SLOW     2
#define FGD_BMU_SPEED_ZERO     255

#define FGD_BMU_MODE_INITIAL   0
#define FGD_BMU_MODE_MAJOR     1
#define FGD_BMU_MODE_MINOR     2

// [주의] 각 항목의 플레그 값을 변경하는 경우 설정값 호환성에 문제가 생길 수 있음. 
//#define FGD_OPTION_PERFORM_DYNAMIC_BACKGROUND_SUPPRESSION               0x00000004
//#define FGD_OPTION_ENABLE_SCHEDULE_FOR_DYNAMIC_BACKGROUND_SUPPRESSION   0x00000040
//#define FGD_OPTION_PERFORM_VIDEO_STABILIZATION                          0x00000100
#define FGD_OPTION_ENABLE_SCHEDULE_FOR_LIGHT_DISTURBANCE_REDUCTION      0x00000001
#define FGD_OPTION_ENABLE_SCHEDULE_FOR_SHADOW_SUPPRESSION               0x00000002
#define FGD_OPTION_PERFORM_DYNAMIC_BACKGROUND_SUPPRESSION               0x00000004
#define FGD_OPTION_PERFORM_LIGHT_DISTURBANCE_REDUCTION                  0x00000008
#define FGD_OPTION_PERFORM_SHADOW_SUPPRESSION                           0x00000010
#define FGD_OPTION_PERFORM_STRONG_NOISE_REDUCTION                       0x00000020
#define FGD_OPTION_ENABLE_SCHEDULE_FOR_DYNAMIC_BACKGROUND_SUPPRESSION   0x00000040
#define FGD_OPTION_PERFORM_VIDEO_STABILIZATION                          0x00000100
#define FGD_OPTION_PERFORM_HISTOGRAM_EQUALIZATION                       0x00000200

#define FGD_RCODE_ERROR                        -1
#define FGD_RCODE_NORMAL                        0
#define FGD_RCODE_INITIAL_BACKGROUND_LEARNING   1
#define FGD_RCODE_SUDDEN_SCENE_CHANGE           2
#define FGD_RCODE_RESET                         3
#define FGD_RCODE_DISABLED                      4
                                                    
#define FGD_STATUS_SUDDEN_SCENE_CHANGE              0x00000001
#define FGD_STATUS_LOW_ILLUMINATION                 0x00000002
#define FGD_STATUS_IR_MODE                          0x00000004
#define FGD_STATUS_DYNAMIC_BACKGROUND_SUPPRESSION   0x00000008
#define FGD_STATUS_REMOVED_OBJECT_DETECTION         0x00000010
#define FGD_STATUS_SMOKE_DETECTION                  0x00000020
#define FGD_STATUS_RESET_VIDEO_STABILIZATION        0x00000040
#define FGD_STATUS_FOREGROUND_EDGE_IMAGE            0x00000080
#define FGD_STATUS_LOW_EDGE_THRESHOLD               0x00000100
#define FGD_STATUS_HIGH_EDGE_THRESHOLD              0x00000200
                                                  
 class ForegroundDetection
 
{
   protected:
      int    ClassID;      // Class ID (CLSID_XXX 정의문 참조)
      int    Flag_Enabled; // 
      float  Time_SSCD;    // Sudden Scene Change Detection 경과 시간
      IBox2D LightingArea; // IR PTZ 카메라에 의해 IR 조명을 받는 영역 (특별한 처리 필요)
      GImage BMUSImage;    // Background Model Update Scheme Image
      GImage RefImage;     // Reference Image for Video Stabilization
      
   // Input (Set directly; Default values available; Saved in a configuration file)
   public:
      int       Options;          // Options (FGD_OPTION_XXX 정의문 참조)
      int       CFRadius;         // Closing Filter Radius (CIF급 해상도 기준)
      int       VGFFSize;         // Vertical Gap Filling Filter Size (CIF급 해상도 기준)
      float     InitBkgLearnTime; // Initial Background Learning Time
      float     BkgUpdatePeriod;  // Background Update Period
      float     SSCDThreshold;    // Sudden Scene Change Detection Threshold
      Period_HM Schedule_DBS;     // Dynamic Background Suppression Schedule
   
   // Input (Set directly; Default values available)
   public:
      int DBSRange;     // Dynamic Background Suppression Range
      int DBSThreshold; // Dynamic Background Suppression Threshold
      int EDThreshold;  // Edge Detection Threshold
      int MinBlobArea;  // Minimum Foreground Blob Area
      int MaxBlobArea;  // Maximum Foreground Blob Area
      int SNRThreshold; // Sparse Noise Removal Threshold

   // Input (Set indirectly)
   public:
      float     FrameRate;     // Processing Frame Rate
      ISize2D   SrcImgSize;    // Source Image Size
      ISize2D   SrcFrmSize;    // Source Frame Size
      byte*     SrcFrmBuffer;  // Source Frame Buffer (YUY2 Pixel Format)
      BGRImage* SrcColorImage; // Source Color Image

   public:
      SingleEvent Event_Processed;

   // Input (Set indirectly)
   public:
      ObjectTracking*           ObjectTracker;        // Object Tracker
      RealObjectSizeEstimation* RealObjSizeEstimator; // Real Object Size Estimator

   // Output      
   public:
      int      FrameCount;     // Frame Count
      int      Status;         // Status
      float    InitFrgDensity; // Initial Foreground Density
      FPoint2D Shift;          // Frame Shift Vector for Video Stabilization
      GImage   BkgEdgeImage;   // Background Edge Image
      GImage   BkgImage;       // Background Image
      GImage   DetAreaMap;     // Detection Area Map
      GImage   FrgEdgeImage1;  // Foreground Edge Image #1
                               // FrgEdgeImage1[y][x] = abs (fg_es[y][x] - bg_es[y][x]) > EDThreshold ? 255:0
      GImage   FrgEdgeImage2;  // Foreground Edge Image #2
                               // FrgEdgeImage2[y][x] = fg_es[y][x] > EDThreshold ? 255:0                              
      GImage   FrgEdgeImage3;  // Foreground Edge Image #3
                               // FrgEdgeImage3[y][x] = FrgEdgeImage1[y][x] & FrgEdgeImage2[y][x]
      GImage   FrgImage;       // Final Foreground Image
      GImage   IntFrgImage1;   // Intermediate Foreground Image #1 (Background Subtraction Result)
      GImage   IntFrgImage2;   // Intermediate Foreground Image #2 (Light Disturbance Reduction Result)
      SArray2D BkgEdgeMap;     // Background Edge Map
      BGRImage BkgColorImage;  // Background Color Image

   // Output (Required for ObjectTracking_Legacy)
   public:
      GImage    SrcImage;     // Source Image (Preprocessed)
      IArray2D  FrgRegionMap; // Foreground Region Map
      CRArray1D FrgRegions;   // Foreground Regions

   // Output (Required for ObjectDetection_DNN)
   public:
      BGRImage DNNInputImage; // Input image fit to the receptive field of a DNN

   // Output (Required for BoxTracking)
   public:
      OBBArray1D ObjBndBoxes; // Array of object bounding boxes obtained by an object detector
   
   public:
      LightDisturbanceReduction LDR;

   public:
      ForegroundDetection (   );
      virtual ~ForegroundDetection (   );
      
   protected:
      void  _Init                 (   );
      int   CheckBWImage          (BGRImage& s_cimage,int ss_size,float s_thrsld);
      int   CheckEdgeThreshold    (   );
      int   CheckIRMode           (BGRImage& s_cimage);
      int   CheckLowIllumination  (GImage& s_image);
      int   CheckSchedule_DBS     (int hour,int minute);
      void  CompensateForeground  (GImage& fg_image);
      void  DrawObjBndBox         (IBox2D& s_rgn,int id,const char* label,uint b_color,uint l_color);
      void  DrawObjBndBoxes       (   );
      float GetOverlapAreaRatio   (GImage& fg_image,GImage& om_image,IPoint2D& om_pos);
      int   GetStabilizedImage    (GImage& s_image,GImage& d_image);
      void  SetFrgRegions         (IB2DArray1D& s_array,float expansion = 0.0f);
      void  TrimForegroundRegions (GImage& fg_image);

   // Functions which should be implemented in sub-classes
   protected:
      virtual int  GetFrgImage         (   ) { return (FGD_RCODE_NORMAL); }
      virtual int  IsBkgPixel          (byte p,int x,int y) { return (FALSE); }
      virtual void UpdateBkgModel_Main (GImage& m_image) {   }

   protected:
      virtual void CheckSchedules            (   );
      virtual int  DetectSuddenSceneChange   (GImage& fg_image);
      virtual void GetFrgEdgeImages          (GImage& fg_image);
      virtual void GetFrgImageInLightingArea (GImage& fg_image);
      virtual void GetFrgRegionMap           (   );
      virtual void GetPreprocSrcImage        (GImage& s_image);
      virtual void PerformMorphFiltering     (GImage& fg_image);
   
   public:
      virtual int  CheckSetup     (ForegroundDetection* fd);
      virtual void Close          (   );
      virtual int  Initialize     (ISize2D& si_size,float frm_rate);
      virtual int  ReadFile       (FileIO& file);
      virtual int  ReadFile       (CXMLIO* pIO);
      virtual int  UpdateBkgModel (GImage& m_image);
      virtual int  WriteFile      (FileIO& file);
      virtual int  WriteFile      (CXMLIO* pIO);

   public:
      void CopyParams1                         (ForegroundDetection& s_fg_detector);
      void CopyParams2                         (ForegroundDetection& s_fg_detector);
      void Enable                              (int flag_enable);
      void EnableVideoStabilization            (int flag_enable);
      int  GetBkgModelUpdateMode               (   );
      int  GetClassID                          (   );
      void InitParams                          (EventDetection& evt_detector);
      int  IsEnabled                           (   );
      int  IsFrgEdgeImageSupported             (   );
      int  Perform                             (GImage& s_image,BGRImage* s_cimage = NULL,byte* s_frm_buf = NULL,ISize2D* s_frm_size = NULL,ObjectTracking* obj_tracker = NULL,RealObjectSizeEstimation* ros_estimator = NULL);
      void PerformDynamicBackgroundSuppression (GImage& fg_image);
      void PerformLightDisturbanceReduction    (GImage& fg_image);
      void Reset                               (   );
      int  SetDetectionAreaMap                 (GImage& ta_map);
      void SetLightingArea                     (int cx,int cy,int width,int height);
};

 inline void ForegroundDetection::Enable (int flag_enable)

{
   Flag_Enabled = flag_enable;
}

 inline int ForegroundDetection::GetClassID (   )

{
   return (ClassID);
}

 inline int ForegroundDetection::IsEnabled (   )

{
   return (Flag_Enabled);
}

 inline int ForegroundDetection::IsFrgEdgeImageSupported (   )

{
   if (Status & FGD_STATUS_FOREGROUND_EDGE_IMAGE) return (TRUE);
   else return (FALSE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: ForegroundDetectionClient
//
///////////////////////////////////////////////////////////////////////////////

 class ForegroundDetectionClient : public ForegroundDetection

{
   protected:
      ObjectDetectionServer* Server;

   public:
      ForegroundDetectionClient (   );
   
   public:
      void Connect     (ObjectDetectionServer& server);
      void Disconnect  (   );
      int  IsConnected (   );
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: ForegroundDetection_BS
//
///////////////////////////////////////////////////////////////////////////////
//
// BS: Background Subtraction
//
 
 class ForegroundDetection_BS : public ForegroundDetection
 
{
   public:
       ForegroundDetection_BS  (   );
      
   protected:
      virtual void PerformBkgSubtraction (GImage &fg_image) = 0;

   protected:
      virtual int GetFrgImage (   );

   public:
      virtual int Initialize (ISize2D& si_size,float frm_rate);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: ForegroundDetection_SGBM
//
///////////////////////////////////////////////////////////////////////////////
//
// SGBM: Single Gaussian Background Model
//

 class ForegroundDetection_SGBM : public ForegroundDetection_BS

{
   public:
      float Alpha[3];
      float DThreshold;
      float InitVariance;
      float MinVariance;
      float MaxVariance;
 
   protected:
      USArray2D Variances; // 배경의 Y 성분의 실제 분산 값

   protected:
      struct BkgPixel {
         ushort MeanY;     // 배경의 Y 성분의 실제 평균값 * 256
         ushort MeanR;     // 배경의 R 성분의 실제 평균값 * 256
         ushort MeanG;     // 배경의 G 성분의 실제 평균값 * 256
         ushort MeanB;     // 배경의 B 성분의 실제 평균값 * 256
         int    VarianceY; // 배경의 Y 성분의 실제 분산값 * 256
      };
      Array2D<BkgPixel> BkgModel;

   public:
      ForegroundDetection_SGBM (   );
      virtual ~ForegroundDetection_SGBM (   );

   protected:
      virtual int  IsBkgPixel            (byte p,int x,int y);
      virtual void PerformBkgSubtraction (GImage& d_image);
      virtual void UpdateBkgModel_Main   (GImage& m_image);

   protected:
      void InitBkgModel               (BGRImage* s_cimage);
      void PerformBkgSubtraction_Main (GImage& s_image,GImage& d_image);
      void PerformBkgSubtraction_Main (GImage& s_image,BGRImage& s_cimage,GImage& d_image);
      void UpdateBkgColorImage        (BGRImage& s_cimage,GImage& m_image,int* alpha);
      
   public:
      virtual void Close      (   );
      virtual int  Initialize (ISize2D& si_size,float frm_rate);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: ForegroundDetection_MGBM
//
///////////////////////////////////////////////////////////////////////////////
//
// MGBM: Multi-Gaussian Background Model
//

 class ForegroundDetection_MGBM : public ForegroundDetection_BS

{
   protected:
      float Alpha[3];
      float DThreshold;
      float WThreshold;
      float MinVariance;
      float InitVariance;
 
   protected:
      USArray2D Variances;

   protected:
      struct BkgPixel {
         byte  NumGCs;
         byte  NumBGCs;
         float MeansY[3];
         float MeanR;
         float MeanG;
         float MeanB;
         float Weights[3];
         float VariancesY[3];
      };
      Array2D<BkgPixel> BkgModel;

   public:
       ForegroundDetection_MGBM (   );
       virtual ~ForegroundDetection_MGBM (   );

   protected:
      virtual int  IsBkgPixel            (byte p,int x,int y);
      virtual void PerformBkgSubtraction (GImage& fg_image);
      virtual void UpdateBkgModel_Main   (GImage& m_image);

   protected:
      void InitBkgModel        (BGRImage* s_cimage);
      void UpdateBkgColorImage (BGRImage& s_image,GImage& m_image,float* alpha);
   
   public:
      virtual void Close      (   );
      virtual int  Initialize (ISize2D& si_size,float frm_rate);
   
   public:
      void GetBkgImage (int n,GImage &d_image);
};

}

#endif

#include "vacl_profile.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 extern "C"

{

 int Compare_RectPulse_Area_Ascending (const void *a,const void *b)

{
  if      (((RectPulse *)a)->Area < ((RectPulse *)b)->Area) return (-1);
  else if (((RectPulse *)a)->Area > ((RectPulse *)b)->Area) return ( 1);
  else return (0);
}

 int Compare_RectPulse_Area_Descending (const void *a,const void *b)

{
  if      (((RectPulse *)a)->Area < ((RectPulse *)b)->Area) return ( 1);
  else if (((RectPulse *)a)->Area > ((RectPulse *)b)->Area) return (-1);
  else return (0);
}

 int Compare_RectPulse_Width_Ascending (const void *a,const void *b)

{
  if      (((RectPulse *)a)->Width < ((RectPulse *)b)->Width) return (-1);
  else if (((RectPulse *)a)->Width > ((RectPulse *)b)->Width) return ( 1);
  else return (0);
}

 int Compare_RectPulse_Width_Descending (const void *a,const void *b)

{
  if      (((RectPulse *)a)->Width < ((RectPulse *)b)->Width) return ( 1);
  else if (((RectPulse *)a)->Width > ((RectPulse *)b)->Width) return (-1);
  else return (0);
}

}

 void FindLocalMinMax (FArray1D &s_array,BArray1D &d_array)
 
{
   int i,flat_start = 0;
   float flat_flag = 0.0f;
   
   d_array.Clear (   );
   for (i = 1; i < s_array.Length; i++)
      if (s_array[i] - s_array[i - 1]) break;
   if  (i == s_array.Length) return;
   float flag = s_array[i] - s_array[i - 1];
   for (++i; i < s_array.Length; i++) {
      float delta = s_array[i] - s_array[i - 1];
      if (flag < 0.0f) {
         if (delta > 0.0f) d_array[i] = LOCAL_MINIMUM;
         else if (delta == 0.0f) {
            flat_start = i;
            flat_flag  = flag;
         }
      }
      else if (flag > 0.0f) {
         if (delta < 0.0f) d_array[i] = LOCAL_MAXIMUM;
         else if (delta == 0.0f) {
            flat_start = i;
            flat_flag  = flag;
         }
      }
      else {
         if      (delta < 0.0f && flat_flag > 0.0f) d_array[(flat_start + i) / 2] = LOCAL_MAXIMUM;
         else if (delta > 0.0f && flat_flag < 0.0f) d_array[(flat_start + i) / 2] = LOCAL_MINIMUM;
      }
      flag = delta;
   }
}

 void FindLocalMinMax (FArray1D &s_array,IArray1D &min_array,IArray1D &max_array)

{
   int i,j,k;

   min_array.Delete (   );
   max_array.Delete (   );
   BArray1D mm_array(s_array.Length);
   FindLocalMinMax (s_array,mm_array);
   int n_min = 0, n_max = 0;
   for (i = 0; i < mm_array.Length; i++) {
      if      (mm_array[i] == LOCAL_MINIMUM) n_min++;
      else if (mm_array[i] == LOCAL_MAXIMUM) n_max++;
   }
   if (n_min) min_array.Create (n_min);
   if (n_max) max_array.Create (n_max);
   for (i = j = k = 0; i < mm_array.Length; i++) {
      if      (mm_array[i] == LOCAL_MINIMUM) min_array[j++] = i;
      else if (mm_array[i] == LOCAL_MAXIMUM) max_array[k++] = i;
   }
}

 void FindMaxArea (RPArray &s_array,float &max_area,int &max_i)

{
   int i;

   max_i = 0;
   max_area = s_array[0].Area;
   for (i = 1; i < s_array.Length; i++) {
      if (s_array[i].Area > max_area) {
         max_area  = s_array[i].Area;
         max_i = i;
      }
   }
}

 void FindMaxHeight (RPArray &s_array,float &max_height,int &max_i)

{
   int i;

   max_i = 0;
   max_height = s_array[0].Area / s_array[0].Width;
   for (i = 1; i < s_array.Length; i++) {
      float height = s_array[i].Area / s_array[i].Width;
      if (height > max_height) {
         max_height = height;
         max_i = i;
      }
   }
}

 void FindMaxWidth (RPArray &s_array,int &max_width,int &max_i)

{
   int i;

   max_i = 0;
   max_width = s_array[0].Width;
   for (i = 1; i < s_array.Length; i++) {
      if (s_array[i].Width > max_width) {
         max_width = s_array[i].Width;
         max_i = i;
      }
   }
}

 void FindMinArea (RPArray &s_array,float &min_area,int &min_i)

{
   int i;

   min_i = 0;
   min_area = s_array[0].Area;
   for (i = 1; i < s_array.Length; i++) {
      if (s_array[i].Area < min_area) {
         min_area  = s_array[i].Area;
         min_i = i;
      }
   }
}

 void FindMinHeight (RPArray &s_array,float &min_height,int &min_i)

{
   int i;

   min_i = 0;
   min_height = s_array[0].Area / s_array[0].Width;
   for (i = 1; i < s_array.Length; i++) {
      float height = s_array[i].Area / s_array[i].Width;
      if (height < min_height) {
         min_height = height;
         min_i = i;
      }
   }
}

 void FindMinMaxAreas (RPArray &s_array,float &min_area,float &max_area,int &min_i,int &max_i)
 
{
   int i;
   
   min_i = max_i = 0;
   min_area  = max_area  = s_array[0].Area;
   for (i = 1; i < s_array.Length; i++) {
      if (s_array[i].Area < min_area) {
         min_area  = s_array[i].Area;
         min_i = i;
      }
      else if (s_array[i].Area > max_area) {
         max_area  = s_array[i].Area;
         max_i = i;
      }
   }
}

 void FindMinMaxHeights (RPArray &s_array,float &min_height,float &max_height,int &min_i,int &max_i)
 
{
   int i;
   
   min_i = max_i = 0;
   min_height = max_height = s_array[0].Area / s_array[0].Width;
   for (i = 1; i < s_array.Length; i++) {
      float height = s_array[i].Area / s_array[i].Width;
      if (height < min_height) {
         min_height = height;
         min_i = i;
      }
      else if (height > max_height) {
         max_height = height;
         max_i = i;
      }
   }
}

 void FindMinMaxWidths (RPArray &s_array,int &min_width,int &max_width,int &min_i,int &max_i)
 
{
   int i;
   
   min_i = max_i = 0;
   min_width = max_width = s_array[0].Width;
   for (i = 1; i < s_array.Length; i++) {
      if (s_array[i].Width < min_width) {
         min_width = s_array[i].Width;
         min_i = i;
      }
      else if (s_array[i].Width > max_width) {
         max_width = s_array[i].Width;
         max_i = i;
      }
   }
}

 void FindMinWidth (RPArray &s_array,int &min_width,int &min_i)

{
   int i;

   min_i = 0;
   min_width = s_array[0].Width;
   for (i = 1; i < s_array.Length; i++) {
      if (s_array[i].Width < min_width) {
         min_width = s_array[i].Width;
         min_i = i;
      }
   }
}

 int GetRectPulses (FArray1D &s_array,float thrsld,RPArray &d_array)

{
   int i,j,n_rps;
   int flag_high = FALSE;

   int length = s_array.Length / 2;
   if (length < 2) length = 2;
   RPArray rp_array(length);
   for (i = n_rps = 0; i < s_array.Length; i++) {
      if (s_array[i] >= thrsld && flag_high == FALSE) {
         flag_high = TRUE;
         rp_array[n_rps].StartPos = i;
      }
      else if (s_array[i] < thrsld && flag_high == TRUE) {
         flag_high = FALSE;
         rp_array[n_rps++].EndPos = i - 1;
      }
      else continue;
   }
   if (flag_high == TRUE) rp_array[n_rps++].EndPos = s_array.Length - 1;
   for (i = 0; i < n_rps; i++) rp_array[i].Width = rp_array[i].EndPos - rp_array[i].StartPos + 1;
   if (!n_rps) {
      d_array.Delete (   );
      return (1);
   }
   d_array.Create (n_rps);
   d_array.Copy (rp_array,0,n_rps,0);
   for (i = 0; i < d_array.Length; i++) {
      d_array[i].Area = 0.0f;
      for (j = d_array[i].StartPos; j <= d_array[i].EndPos; j++) d_array[i].Area += s_array[j] - thrsld;
   }
   return (DONE);
}

 void HorizontalProjection (FArray2D &s_array,int sx,int length,FArray1D &d_array,int flag_clear)

{
   int x,y;
   
   if (flag_clear) d_array.Clear (   );
   int ex = sx + length;
   for (y = 0; y < s_array.Height; y++) {
      float* l_s_array = s_array[y];
      float sum = 0.0f;
      for (x = sx; x < ex; x++) sum += l_s_array[x];
      d_array[y] = sum;
   }
}

 void HorizontalProjection (GImage &s_image,int sx,int length,FArray1D &d_array,int flag_clear)

{
   int x,y;
   
   if (flag_clear) d_array.Clear (   );
   int ex = sx + length;
   for (y = 0; y < s_image.Height; y++) {
      byte* l_s_image = s_image[y];
      float sum = 0.0f;
      for (x = sx; x < ex; x++) sum += l_s_image[x];
      d_array[y] = sum;
   }
}

 void HorizontalProjection (IArray2D& cr_map,IBox2D& s_rgn,int cr_no,FArray1D& d_array)

{
   int x,y;

   d_array.Clear (   );
   int ex = s_rgn.X + s_rgn.Width;
   int ey = s_rgn.Y + s_rgn.Height;
   for (y = s_rgn.Y; y < ey; y++) {
      int* l_cr_map = cr_map[y];
      int sum = 0;
      for (x = s_rgn.X; x < ex; x++) {
         if (l_cr_map[x] == cr_no) sum++;
      }
      d_array[y - s_rgn.Y] = (float)sum;
   }
}

 int InvertRectPulses (RPArray &s_array,int length,RPArray &d_array)

{
   int i,j;

   FArray1D p_array(length);
   p_array.Set (0,length,1.0f);
   for (i = 0; i < s_array.Length; i++)
      for (j = s_array[i].StartPos; j <= s_array[i].EndPos; j++) p_array[j] = 0.0f;
   return (GetRectPulses (p_array,0.5f,d_array));
}

 void SortByArea (RPArray &s_array,int flag_ascending)

{
   if (flag_ascending) qsort (s_array,s_array.Length,sizeof(RectPulse),Compare_RectPulse_Area_Ascending);
   else qsort (s_array,s_array.Length,sizeof(RectPulse),Compare_RectPulse_Area_Descending);
}

 void SortByWidth (RPArray &s_array,int flag_ascending)

{
   if (flag_ascending) qsort (s_array,s_array.Length,sizeof(RectPulse),Compare_RectPulse_Width_Ascending);
   else qsort (s_array,s_array.Length,sizeof(RectPulse),Compare_RectPulse_Width_Descending);
}

 void VerticalProjection (FArray2D &s_array,int sy,int length,FArray1D &d_array,int flag_clear)

{
   int x,y;

   if (flag_clear) d_array.Clear (   );
   int ey = sy + length;
   for (x = 0; x < s_array.Width; x++) {
      float sum = 0.0f;
      for (y = sy; y < ey; y++)
         sum += s_array[y][x];
      d_array[x] = sum;
   }
}

 void VerticalProjection (GImage &s_image,int sy,int length,FArray1D &d_array,int flag_clear)

{
   int x,y;

   if (flag_clear) d_array.Clear (   );
   int ey = sy + length;
   for (x = 0; x < s_image.Width; x++) {
      int sum = 0;
      for (y = sy; y < ey; y++)
         sum += s_image[y][x];
      d_array[x] = (float)sum;
   }
}

 void VerticalProjection (IArray2D& cr_map,IBox2D& s_rgn,int cr_no,FArray1D& d_array)

{
   int x,y;

   d_array.Clear (   );
   int ex = s_rgn.X + s_rgn.Width;
   int ey = s_rgn.Y + s_rgn.Height;
   for (x = s_rgn.X; x < ex; x++) {
      int sum = 0;
      for (y = s_rgn.Y; y < ey; y++)
         if (cr_map[y][x] == cr_no) sum++;
      d_array[x - s_rgn.X] = (float)sum;
   }
}

}

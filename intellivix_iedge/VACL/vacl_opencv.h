#if !defined(__VACL_OPENCV_H)
#define __VACL_OPENCV_H

#include "vacl_define.h"

#if defined(__LIB_OPENCV)

#include "opencv2/opencv.hpp"
#include "opencv2/core/internal.hpp"
#include "opencv2/nonfree/features2d.hpp"
#include "opencv2/nonfree/nonfree.hpp"

using namespace cv;

 namespace VACL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// "CvMat" object initialization functions
///////////////////////////////////////////////////////////////////////////////

void OpenCV_InitCvMat (FArray1D&    s_array ,CvMat& d_mat,int flag_col_vec = FALSE);
void OpenCV_InitCvMat (FArray2D&    s_array ,CvMat& d_mat);
void OpenCV_InitCvMat (IArray2D&    s_array ,CvMat& d_mat);
void OpenCV_InitCvMat (GImage&      s_image ,CvMat& d_mat);
void OpenCV_InitCvMat (BGRImage&    s_cimage,CvMat& d_mat);
void OpenCV_InitCvMat (Matrix&      mat_S   ,CvMat& d_mat);
void OpenCV_InitCvMat (FP2DArray1D& s_array ,CvMat& d_mat);
void OpenCV_InitCvMat (FP2DArray2D& s_array ,CvMat& d_mat);
void OpenCV_InitCvMat (FP3DArray1D& s_array ,CvMat& d_mat);
void OpenCV_InitCvMat (FP3DArray2D& s_array ,CvMat& d_mat);

///////////////////////////////////////////////////////////////////////////////
// "Mat" object initialization functions
///////////////////////////////////////////////////////////////////////////////

void OpenCV_InitMat (int width,int height,int type,void* s_buffer,int step,cv::Mat& d_mat);
void OpenCV_InitMat (FArray1D&   s_array ,cv::Mat& d_mat,int flag_col_vec = FALSE);
void OpenCV_InitMat (FArray2D&   s_array ,cv::Mat& d_mat);
void OpenCV_InitMat (IArray2D&   s_array ,cv::Mat& d_mat);
void OpenCV_InitMat (SArray2D&   s_array ,cv::Mat& d_mat);
void OpenCV_InitMat (USArray2D&  s_array ,cv::Mat& d_mat);
void OpenCV_InitMat (GImage&     s_image ,cv::Mat& d_mat);
void OpenCV_InitMat (BGRImage&   s_cimage,cv::Mat& d_mat);
void OpenCV_InitMat (Matrix&     mat_S   ,cv::Mat& d_mat);
void OpenCV_InitMat (FP2DArray1D& s_array,cv::Mat& d_mat);
void OpenCV_InitMat (FP2DArray2D& s_array,cv::Mat& d_mat);
void OpenCV_InitMat (FP3DArray1D& s_array,cv::Mat& d_mat);
void OpenCV_InitMat (FP3DArray2D& s_array,cv::Mat& d_mat);

///////////////////////////////////////////////////////////////////////////////
// Utility functions
///////////////////////////////////////////////////////////////////////////////

void OpenCV_Copy (Matrix &vec_s,vector<double> &d_vector);
void OpenCV_Copy (vector<double> &s_vector,Matrix &vec_d);
void OpenCV_Copy (FP2DArray1D &s_array,vector<Point2f> &d_vector);
void OpenCV_Copy (vector<Point2f> &s_vector,FP2DArray1D &d_array);
void OpenCV_Copy (FP3DArray1D &s_array,vector<Point3f> &d_vector);
void OpenCV_Copy (vector<Point3f> &s_vector,FP3DArray1D &d_array);
int  OpenCV_Copy (cv::Mat& s_img,GImage& d_image);
int  OpenCV_Copy (cv::Mat& s_cimg,BGRImage& d_cimage);
int  OpenCV_Copy (cv::Mat& s_mat,Matrix& d_mat);

///////////////////////////////////////////////////////////////////////////////
// OpenCV function wrappers
///////////////////////////////////////////////////////////////////////////////

void OpenCV_CLAHE             (GImage& s_image,GImage& d_image,float clip_limit = 4.0f,int tile_size = 32);
void OpenCV_CLAHE_YUY2        (byte* s_buffer,ISize2D& si_size,float clip_limit = 4.0f,int tile_size = 32);
void OpenCV_CLAHE_YV12        (byte* s_buffer,ISize2D& si_size,float clip_limit = 4.0f,int tile_size = 32);
void OpenCV_DetectEdges_Canny (GImage& s_image,float l_es_thrsld,float h_es_thrsld,GImage &d_image);
void OpenCV_DistanceTransform (GImage& s_image,FArray2D& d_array);
void OpenCV_DrawCircle        (GImage& s_image,IPoint2D &center,int radius,byte color,int thickness = 1);
void OpenCV_DrawFilledCircle  (GImage& s_image,IPoint2D &center,int radius,byte color);
void OpenCV_Watershed         (GImage& s_image,IArray2D& s_cr_map);
void OpenCV_Watershed         (BGRImage& s_cimage,IArray2D& s_cr_map);

double OpenCV_CalibrateCamera        (FP3DArray1D &op_array,FP2DArray1D &ip_array,IArray1D &n_array,ISize2D &ci_size,Matrix &mat_K,Matrix &vec_DCs,Matrix &mat_RVs,Matrix &mat_TVs);
double OpenCV_CalibrateStereoCameras (FP3DArray1D &op_array,FP2DArray1D &ip_array1,FP2DArray1D &ip_array2,IArray1D &n_array,ISize2D &ci_size,Matrix &mat_K1,Matrix &vec_DCs1,Matrix &mat_K2,Matrix &vec_DCs2,Matrix &mat_R,Matrix &vec_t);
void   OpenCV_DrawChessboardCorners  (BGRImage &s_image,ISize2D &pt_size,FP2DArray1D &cp_array,int flag_pt_found);
int    OpenCV_FindChessboardCorners  (GImage &s_image,ISize2D &pt_size,FP2DArray1D &d_array,int options = CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_NORMALIZE_IMAGE | CV_CALIB_CB_FAST_CHECK);
void   OpenCV_GetDenseOpticalFlow    (GImage &p_image,GImage &c_image,FP2DArray2D &d_array,float pyr_scale = 0.5f,int n_levels = 1,int win_size = 5,int n_iterations = 1,int poly_n = 5,float poly_sigma = 1.1f,int flags = 0);
void   OpenCV_Rodrigues              (double* vec_r,Matrix &mat_R);

} 

#endif
#endif

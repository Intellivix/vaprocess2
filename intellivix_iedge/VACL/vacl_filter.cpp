#include "vacl_filter.h"
 
 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 void BoxFiltering (GImage &s_image,int kernel_size,GImage &d_image)

{
   int i,j;

   int h_kernel_size = kernel_size / 2;
   int kernel_area   = kernel_size * kernel_size;
   IArray2D h_array(s_image.Width,s_image.Height);
   IArray2D v_array(s_image.Width,s_image.Height);
   GetHorizontalSums (s_image,kernel_size,h_array);
   GetVerticalSums   (h_array,kernel_size,v_array);
   int ei = s_image.Height - h_kernel_size;
   int ej = s_image.Width  - h_kernel_size;
   for (i = h_kernel_size; i < ei; i++) {
      int  *_RESTRICT l_v_array = v_array[i];
      byte *_RESTRICT l_d_image = d_image[i];
      for (j = h_kernel_size; j < ej; j++) 
         l_d_image[j] = (byte)(l_v_array[j] / kernel_area);
   }
   d_image.FillBoundary (h_kernel_size);
}

 void BoxFiltering (IArray2D &s_array,int kernel_size,IArray2D &d_array)

{
   int i,j;

   int h_kernel_size = kernel_size / 2;
   int kernel_area   = kernel_size * kernel_size;
   IArray2D h_array(s_array.Width,s_array.Height);
   IArray2D v_array(s_array.Width,s_array.Height);
   GetHorizontalSums (s_array,kernel_size,h_array);
   GetVerticalSums   (h_array,kernel_size,v_array);
   int ei = s_array.Height - h_kernel_size;
   int ej = s_array.Width  - h_kernel_size;
   for (i = h_kernel_size; i < ei; i++) {
      int *_RESTRICT l_v_array = v_array[i];
      int *_RESTRICT l_d_array = d_array[i];
      for (j = h_kernel_size; j < ej; j++) 
         l_d_array[j] = l_v_array[j] / kernel_area;
   }
   d_array.FillBoundary (h_kernel_size);
}

 void Convolve (FArray1D &s_array,FArray1D &kernel,FArray1D &d_array)

{
   int i,j,k;
   int h_win_size;
   float sum;
 
   FArray1D kernel2(kernel.Length);
   MirrorKernel (kernel,kernel2);  
   h_win_size = kernel2.Length / 2;
   FArray1D s_array2(s_array.Length + h_win_size * 2);
   s_array2.Clear (   );
   s_array2.Copy (s_array,0,s_array.Length,h_win_size);
   for (i = 0; i < s_array.Length; i++) {
      for (j = 0,k = i,sum = 0.0f; j < kernel2.Length; j++,k++) sum += kernel2[j] * s_array2[k];
      d_array[i] = sum;
   }
}

 void ConvolveHorizontally (GImage &s_image,FArray1D &kernel,FArray2D &d_array)

{
   int i,j,k,l,m;
   int h_win_size;
   float sum;

   FArray1D kernel2(kernel.Length);
   MirrorKernel (kernel,kernel2);
   h_win_size = kernel2.Length / 2;
   GImage s_image2(s_image.Width + h_win_size * 2,s_image.Height);
   s_image2.Set (0,0,h_win_size,s_image2.Height,0);
   s_image2.Set (s_image2.Width - h_win_size,0,h_win_size,s_image2.Height,0);
   s_image2.Copy (s_image,0,0,s_image.Width,s_image.Height,h_win_size,0);
   for (i = 0; i < s_image2.Height; i++) {
      byte  *_RESTRICT l_s_image2 = s_image2[i];
      float *_RESTRICT l_d_array  = d_array[i];
      for (j = h_win_size,m = 0; m < d_array.Width; j++,m++) {
         for (k = 0,l = m,sum = 0.0f; k < kernel2.Length; k++,l++) sum += kernel2[k] * l_s_image2[l];
         l_d_array[m] = sum;
      }
   }
}

 void ConvolveVertically (FArray2D &s_array,FArray1D &kernel,GImage &d_image)

{
   int i,j,k,l,m;
   int h_win_size;
   float sum;

   FArray1D kernel2(kernel.Length);
   MirrorKernel (kernel,kernel2);
   h_win_size = kernel2.Length / 2;
   FArray2D s_array2(s_array.Width,s_array.Height + h_win_size * 2);
   s_array2.Set  (0,0,s_array2.Width,h_win_size,0.0f);
   s_array2.Set  (0,s_array2.Height - h_win_size,s_array2.Width,h_win_size,0.0f);
   s_array2.Copy (s_array,0,0,s_array.Width,s_array.Height,0,h_win_size);
   for (j = 0; j < s_array2.Width; j++) {
      for (i = h_win_size,m = 0; m < d_image.Height; i++,m++) {
         for (k = 0,l = m,sum = 0.0f; k < kernel2.Length; k++,l++) sum += kernel2[k] * s_array2[l][j];
         short p = (short)sum;
         if (p > PG_WHITE) p = PG_WHITE;
         else if (p < 0)   p = 0;
         d_image[m][j] = (byte)p;
      }
   }
}

 void FillGaps (GImage& s_image,int win_width,int win_height,GImage& d_image)

{
   int x1,y1,x2,y2;
   
   int hw  = win_width  / 2;
   int hh  = win_height / 2;
   int sx1 = hw;
   int sy1 = hh;
   int ex1 = s_image.Width  - hw;
   int ey1 = s_image.Height - hh;
   for (y1 = sy1; y1 < ey1; y1++) {
      int sy2 = y1 - hh;
      int ey2 = y1 + hh;
      byte* l_d_image = d_image[y1];
      for (x1 = sx1; x1 < ex1; x1++) {
         int sx2 = x1 - hw;
         int ex2 = x1 + hw;
         byte max_v = 0;
         for (y2 = sy2; y2 <= ey2; y2++) {
            byte* l_s_image2 = s_image[y2];
            for (x2 = sx2; x2 <= ex2; x2++) {
               if (l_s_image2[x2] > max_v) max_v = l_s_image2[x2];
            }
         }
         l_d_image[x1] = max_v;
      }
   }
   d_image.Copy (s_image,0  ,0  ,s_image.Width,hh ,0,0  );
   d_image.Copy (s_image,0  ,ey1,s_image.Width,hh ,0,ey1);
   d_image.Copy (s_image,0  ,0  ,hw,s_image.Height,0,0  );
   d_image.Copy (s_image,ex1,0  ,hw,s_image.Height,ex1,0);
}

 void GaussianFiltering (FArray1D &s_array,float sigma,FArray1D &d_array)

{
   if (!sigma) {
      d_array = s_array;
      return;
   }
   FArray1D kernel(GAUSSIAN_KERNEL_SIZE(sigma));
   GetGaussianKernel (sigma,kernel);
   Convolve (s_array,kernel,d_array);
}

 void GaussianFiltering (GImage &s_image,float sigma,GImage &d_image)

{
   if (!sigma) {
      d_image = s_image;
      return;
   }
   FArray1D kernel(GAUSSIAN_KERNEL_SIZE(sigma));
   GetGaussianKernel (sigma,kernel);
   FArray2D m_array(s_image.Width,s_image.Height);
   ConvolveHorizontally (s_image,kernel,m_array);
   ConvolveVertically   (m_array,kernel,d_image);
}

 void GaussianFiltering (GImage &s_image,float sigma_x,float sigma_y,GImage &d_image)
 
{
   if (!sigma_x || !sigma_y) {
      d_image = s_image;
      return;
   }
   FArray1D kernel;
   FArray2D m_array(s_image.Width,s_image.Height);
   kernel.Create (GAUSSIAN_KERNEL_SIZE(sigma_x));
   GetGaussianKernel (sigma_x,kernel);
   ConvolveHorizontally (s_image,kernel,m_array);
   kernel.Create (GAUSSIAN_KERNEL_SIZE(sigma_y));
   GetGaussianKernel (sigma_y,kernel);
   ConvolveVertically (m_array,kernel,d_image);
}

 void GaussianFiltering_3x3 (GImage &s_image,GImage &d_image)

{
   int y,x;

   int ey = s_image.Height - 1;
   int ex = s_image.Width  - 1;
   for (y = 1; y < ey; y++) {
      int y1 = y - 1;
      int y2 = y + 1;
      byte *_RESTRICT l_s_image  = s_image[y];
      byte *_RESTRICT l_s_image1 = s_image[y1];
      byte *_RESTRICT l_s_image2 = s_image[y2];
      byte *_RESTRICT l_d_image  = d_image[y];
      for (x = 1; x < ex; x++) {
         int x1 = x - 1;
         int x2 = x + 1;
         ushort sum1 = ((ushort)l_s_image1[x1])     + ((ushort)l_s_image1[x] << 1) + ((ushort)l_s_image1[x2]);
         ushort sum2 = ((ushort)l_s_image[x1] << 1) + ((ushort)l_s_image [x] << 2) + ((ushort)l_s_image[x2] << 1);
         ushort sum3 = ((ushort)l_s_image2[x1])     + ((ushort)l_s_image2[x] << 1) + ((ushort)l_s_image2[x2]);
         l_d_image[x] = (byte)((sum1 + sum2 + sum3) >> 4);
      }
   }
   d_image.SetBoundary (1,0);
}

 void GetGaussianKernel (float sigma,FArray1D &d_array)

{
   int i;
   int h_win_size;
   float b,s,x;

   b = -1.0f / (2.0f * sigma * sigma);
   h_win_size = d_array.Length / 2;
   if (d_array.Length - h_win_size * 2) x = (float)-h_win_size;
   else x = 0.5f - h_win_size;
   for (i = 0,s = 0.0f; i < d_array.Length; i++,x += 1.0f)
      s += (d_array[i] = (float)exp (b * x * x));
   for (i = 0; i < d_array.Length; i++) d_array[i] /= s;
}

 void GetHorizontalSums (GImage &s_image,int kernel_size,IArray2D &d_array)

{
   int i,j,k,l;
   
   int h_kernel_size = kernel_size / 2;
   int sj = h_kernel_size + 1;
   int ej = s_image.Width - h_kernel_size;
   int ek = h_kernel_size * 2;
   d_array.SetBoundary (1,0);
   for (i = 0; i < s_image.Height; i++) {
      int sum = 0;
      byte *_RESTRICT l_s_image = s_image[i];
      int  *_RESTRICT l_d_array = d_array[i];
      for (k = 0; k <= ek; k++) sum += l_s_image[k];
      l_d_array[h_kernel_size] = sum;
      for (j = sj,l = 0; j < ej; j++,k++,l++) {
         sum += l_s_image[k];
         sum -= l_s_image[l];
         l_d_array[j] = sum;
      }
   }
}

 void GetHorizontalSums (IArray2D &s_array,int kernel_size,IArray2D &d_array)

{
   int i,j,k,l;
   
   int h_kernel_size = kernel_size / 2;
   int sj = h_kernel_size + 1;
   int ej = s_array.Width - h_kernel_size;
   int ek = h_kernel_size * 2;
   d_array.SetBoundary (1,0);
   for (i = 0; i < s_array.Height; i++) {
      int sum = 0;
      int *_RESTRICT l_s_array = s_array[i];
      int *_RESTRICT l_d_array = d_array[i];
      for (k = 0; k <= ek; k++) sum += l_s_array[k];
      l_d_array[h_kernel_size] = sum;
      for (j = sj,l = 0; j < ej; j++,k++,l++) {
         sum += l_s_array[k];
         sum -= l_s_array[l];
         l_d_array[j] = sum;
      }
   }
}

 void GetVerticalSums (IArray2D &s_array,int kernel_size,IArray2D &d_array)

{
   int i,j,k,l;

   int h_kernel_size = kernel_size / 2;
   int si = h_kernel_size + 1;
   int ei = s_array.Height - h_kernel_size;
   int ek = h_kernel_size * 2;
   d_array.SetBoundary (1,0);
   for (j = 0; j < s_array.Width; j++) {
      int sum = 0;
      for (k = 0; k <= ek; k++) sum += s_array[k][j];
      d_array[h_kernel_size][j] = sum;
      for (i = si,l = 0; i < ei; i++,k++,l++) {
         sum += s_array[k][j];
         sum -= s_array[l][j];
         d_array[i][j] = sum;
      }
   }
}

 void HistogramStretching (GImage &s_image,float a,float b,GImage &d_image)
 
{
   int x,y;
   
   for (y = 0; y < s_image.Height; y++) {
      byte* l_s_image = s_image[y];
      byte* l_d_image = d_image[y];
      for (x = 0; x < s_image.Width; x++) {
         int p = (int)(a * l_s_image[x] + b);
         if (p < 0) p = 0;
         else if (p > 255) p = 255;
         l_d_image[x] = (byte)p;
      }
   }
}

 void MirrorKernel (FArray1D &s_array,FArray1D &d_array)

{
   int i,n;

   n = s_array.Length - 1;
   for (i = 0; i < s_array.Length; i++) d_array[n - i] = s_array[i];
}

///////////////////////////////////////////////////////////////////////////////
//
//  Computer and Robot Vision
//  Robert M. Haralick and Linda G. Shapiro
//  Addison-Wesley, 1992
//  Vol. 1, pp. 325
//
///////////////////////////////////////////////////////////////////////////////

 void SigmaFiltering (GImage &s_image,int win_size,int sigma,int n,GImage &d_image)

{
   int i,j,k,l,m;

   sigma *= 2;
   int h_win_size = win_size / 2;
   int ei = s_image.Height - h_win_size;
   int ej = s_image.Width  - h_win_size;
   for (i = h_win_size; i < ei; i++) {
      int i1 = i - 1;
      int i2 = i + 1;
      for (j = h_win_size; j < ej; j++) {
         int p1 = (int)s_image[i][j] - sigma;
         int p2 = (int)s_image[i][j] + sigma;
         int sk = i - h_win_size;
         int ek = i + h_win_size;
         int sl = j - h_win_size;
         int el = j + h_win_size;
         int sum = 0;
         m = 0;
         for (k = sk; k <= ek; k++) {
            for (l = sl; l <= el; l++) {
               int p = s_image[k][l];
               if (p1 <= p && p <= p2) {
                  sum += s_image[k][l];
                  m++;
               }
            }
         }
         sum -= s_image[i][j];
         m--;
         if (m < n) {
            int j1 = j - 1;
            int j2 = j + 1;
            sum = (int)s_image[i1][j1] + s_image[i1][j] + s_image[i1][j2] +
                       s_image[i ][j1] +                  s_image[i ][j2] +
                       s_image[i2][j1] + s_image[i2][j] + s_image[i2][j2];
            d_image[i][j] = (byte)(sum / 8);
         }
         else d_image[i][j] = (byte)(sum / m);
      }
   }
   d_image.SetBoundary (h_win_size,PG_BLACK);
}

}

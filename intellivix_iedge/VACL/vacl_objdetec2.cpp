#include "vacl_filter.h"
#include "vacl_ipp.h"
#include "vacl_objdetec2.h"
#include "vacl_warping.h"

 namespace VACL

{

// ObjectDetection_HOG
// - 관련 클래스 추가 필요
// ObjectDetection_AFC
// - 멀티 스케일 처리에 대한 최적화 필요

#if defined(__LIB_ML)
  
///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectDetection_ACF
//
///////////////////////////////////////////////////////////////////////////////

#define SIG_OBJECT_DETECTION_ACF   MAKEFOURCC('O','D','A','C')

 ObjectDetection_ACF::ObjectDetection_ACF (   )

{
   NumGHBins         = 6;
   CellSize          = 4;
   DetWinSize.Width  = 64;
   DetWinSize.Height = 128;

   StepSize          = 2;
   MinObjHeight      = 0.5f * DetWinSize.Height;
   MaxObjHeight      = 2.0f * DetWinSize.Height;
   ScaleInc          = 1.1f;

   SmpImgSize.Width  = DetWinSize.Width  + 2 * CellSize;
   SmpImgSize.Height = DetWinSize.Height + 2 * CellSize;
   DetWinPos.X       = CellSize;
   DetWinPos.Y       = CellSize;

   NumChannels       = 0;
   NumFeatures       = 0;
   NumPosSamples     = 0;
   MinNumNegSamples  = 0;
   MaxNumNegSamples  = 0;
}

 ObjectDetection_ACF::~ObjectDetection_ACF (   )

{
   Close (   );
}

 void ObjectDetection_ACF::Close (   )

{
   Classifier.Close (   );
   GradTable.Delete (   );
   ICFArray.Delete  (   );

   NumChannels      = 0;
   NumFeatures      = 0;
   NumPosSamples    = 0;
   MinNumNegSamples = 0;
   MaxNumNegSamples = 0;
}

 int ObjectDetection_ACF::CheckSmpImgSize (   )

{
   if (SmpImgSize.Width <= 0 || SmpImgSize.Height <= 0) return (1);
   if (SmpImgSize.Width < DetWinSize.Width || SmpImgSize.Height < DetWinSize.Height) return (2);
   int ex = DetWinPos.X + DetWinSize.Width;
   int ey = DetWinPos.Y + DetWinSize.Height;
   if (DetWinPos.X < 0 || ex > SmpImgSize.Width || DetWinPos.X < 0 || ey > SmpImgSize.Height) return (3);
   return (DONE);
}

 int ObjectDetection_ACF::CreateICFArray (ISize2D& si_size)

{
   int i;

   if (NumChannels <= 0) return (1);
   int width  = si_size.Width  + 1;
   int height = si_size.Height + 1;
   if (ICFArray.Length == NumChannels && ICFArray[0].Width == width && ICFArray[0].Height == height) return (DONE);
   ICFArray.Create (NumChannels);
   for (i = 0; i < NumChannels; i++)
      ICFArray[i].Create (width,height);
   return (DONE);
}

 int ObjectDetection_ACF::Detect_MS_IP (GImage& s_image,IB2DArray1D& d_rects,FArray1D& d_scores)

{
   int i,n;
   
   d_rects.Delete  (   );
   d_scores.Delete (   );
   IB2DArray1D t_rects(MAX_NUM_OBJ_DET_RECTS);
   FArray1D t_scores(MAX_NUM_OBJ_DET_RECTS);
   float min_scale = DetWinSize.Height / MaxObjHeight;
   float max_scale = DetWinSize.Height / MinObjHeight;
   float scale = min_scale;
   for (i = n = 0; scale <= max_scale; scale *= ScaleInc,i++) {
      int width  = (int)(scale * s_image.Width);
      int height = (int)(scale * s_image.Height);
      if (width >= (DetWinSize.Width + StepSize) && height >= (DetWinSize.Height + StepSize)) {
//       BCCL::Print ("[%02d] width = %04d, height = %04d, scale = %5.2f\n",i + 1,width,height,scale);
         int n_rects = Detect_SS (s_image,scale,&t_rects[n],&t_scores[n],t_rects.Length - n);
         n += n_rects;
      }
   }
   if (n) {
      d_rects  = t_rects.Extract  (0,n);
      d_scores = t_scores.Extract (0,n);
   }
   return (n);
}

 int ObjectDetection_ACF::Detect_SS (GImage& s_image,float scale,IBox2D* d_rects,float* d_scores,int max_n_rects)

{
   int x,y;

   GImage* p_s_image = &s_image;
   GImage t_image;
   if (scale != 1.0f) {
      t_image.Create ((int)(s_image.Width * scale),(int)(s_image.Height * scale));
      Resize_2 (s_image,t_image);
      p_s_image = &t_image;
   }
   ISize2D si_size(p_s_image->Width,p_s_image->Height);
   if (CreateICFArray (si_size)) return (0);
   GetICFs (*p_s_image,ICFArray);
   FArray1D fv(NumFeatures);
   int n  = 0;
   int mg = StepSize / 2;
   if (mg < 1) mg = 1;
   int ex = p_s_image->Width  - DetWinSize.Width  - mg;
   int ey = p_s_image->Height - DetWinSize.Height - mg;
   for (y = mg; y < ey; y += StepSize) {
      for (x = mg; x < ex; x += StepSize) {
         GetFeatureVector (ICFArray,x,y,fv);
         double score;
         if (Classifier.Classify (fv,NumFeatures,score) > 0) {
            d_rects[n].X      = (int)(x / scale + 0.5f);
            d_rects[n].Y      = (int)(y / scale + 0.5f);
            d_rects[n].Width  = (int)(DetWinSize.Width  / scale + 0.5f);
            d_rects[n].Height = (int)(DetWinSize.Height / scale + 0.5f);
            d_scores[n]       = (float)score;
            n++;
            if (n >= max_n_rects) goto escape;
         }
      }
   }
   escape:;
   return (n);
}

 int ObjectDetection_ACF::Detect_SS (GImage& s_image,float scale,IB2DArray1D& d_rects,FArray1D& d_scores)

{
   d_rects.Delete  (   );
   d_scores.Delete (   );
   IB2DArray1D t_rects(MAX_NUM_OBJ_DET_RECTS);
   FArray1D t_scores(MAX_NUM_OBJ_DET_RECTS);
   int n_rects = Detect_SS (s_image,scale,t_rects,t_scores,t_rects.Length);
   if (n_rects) {
      d_rects  = t_rects.Extract  (0,n_rects);
      d_scores = t_scores.Extract (0,n_rects);
   }
   return (n_rects);
}

 void ObjectDetection_ACF::GetFeatureVector (Array1D<FArray2D>& icf_array,int sx,int sy,float* d_array)

{
   int i,x,y;

   int n  = 0;
   int ex = sx + DetWinSize.Width;
   int ey = sy + DetWinSize.Height;
   for (i = 0; i < icf_array.Length; i++) {
      float** s_array = icf_array[i];
      for (y = sy; y < ey; y += CellSize) {
         int y2 = y + CellSize;
         for (x = sx; x < ex; x += CellSize) {
            int x2 = x + CellSize;
            d_array[n++] = (s_array[y2][x2] + s_array[y][x]) - (s_array[y][x2] + s_array[y2][x]);
         }
      }
   }
}

 void ObjectDetection_ACF::GetICFs (GImage& s_image,Array1D<FArray2D>& d_array)

{
   int i;
   
   Array1D<FArray2D> gh_array(NumGHBins);
   for (i = 0; i < gh_array.Length; i++)
      gh_array[i].Create (s_image.Width,s_image.Height);
   FArray2D gm_array(s_image.Width,s_image.Height);
   GetGradientChannelFeatures (s_image,GradTable,gh_array,gm_array);
   for (i = 0; i < gh_array.Length; i++)
      GetIntegral (gh_array[i],d_array[i]); // Gradient Histogram Feature
   GetIntegral (s_image ,d_array[i++]);     // Luminance Feature
   GetIntegral (gm_array,d_array[i++]);     // Gradient Magnitude Feature
}

 int ObjectDetection_ACF::GetBootNSFVs (   )

{
   int i,j,m,n,n1;
   int x,y,n_neg;
   double score;
   static float scales[] = { 1.0f, 0.25f, 0.5f, 0.75f, 1.25f, 1.5f };
   
   BCCL::Print (">> Obtaining bootstrapped negative sample images and extracting feature vectors from them...\n");
   BCCL::Print ("   # of negative samples: ");
   int n_scales = sizeof(scales) / sizeof(float);
   if (Classifier.CascadeMode == BTC_CASCADE_MODE_SOFT && Classifier.MaxNumStages == 1) {
      n = n1 = NumSamples;
      NumSamples += NumBootNegSamples;
   }
   else {
      n = n1 = NumPosSamples;
      NumSamples = NumPosSamples + NumBootNegSamples;
   }
   CLArray.Set (n,CLArray.Length - n,0.0f);
   SmpFileList& bi_list = *BkgImgList;
   IArray1D idx_array(bi_list.Height);
   GetShuffledIndexNumbers (idx_array);
   for (i = m = 0; i < n_scales; i++) {
      for (j = 0; j < idx_array.Length; j++) {
         GImage s_image;
         if (bi_list.LoadImage (idx_array[j],s_image)) {
            BCCL::Print ("\n[ERROR] Cannot load the image [%s].\n",bi_list[idx_array[j]]);
            return (1);
         }
         GImage f_image(s_image.Width,s_image.Height);
         GaussianFiltering_3x3 (s_image,f_image);
         ISize2D si_size(s_image.Width,s_image.Height);
         CreateICFArray (si_size);
         GetICFs (f_image,ICFArray);
         int sx = 2, sy = 2;
         int ex = s_image.Width  - DetWinSize.Width  - 2;
         int ey = s_image.Height - DetWinSize.Height - 2;
         for (y = sy; y < ey; y += CellSize) {
            for (x = sx; x < ex; x += CellSize) {
               GetFeatureVector (ICFArray,x,y,FVArray[n]);
               m++;
               if (m && !(m % 100000)) BCCL::Print ("<%.1fM>",(double)m / (1024 * 1024));
               if (Classifier.Classify (FVArray[n],NumFeatures,score) > 0) {
                  CLArray[n++] = -1.0f;
                  n_neg = n - n1;
                  if (n_neg && !(n_neg % 100)) BCCL::Print ("[%d]",n_neg);
                  if (n >= NumSamples) goto escape;
               }
            }
         }
      }
   }
   escape:;
   n_neg = n - n1;
   BCCL::Print ("\n   # of negative samples collected: %d\n",n_neg);
   int r_code = DONE;
   if (n < NumSamples) {
      NumSamples = n;
      BCCL::Print ("   [WARNING] Cannot collect negative samples sufficiently.\n");
      if (n_neg <= MinNumNegSamples) {
         BCCL::Print ("   [WARNING] The number of negative samples collected does not satisfy the minimum number requirement.\n");
         r_code = -1;
      }
   }
   BCCL::Print ("   # of samples in total: %d (pos = %d, neg = %d)\n",NumSamples,NumPosSamples,NumSamples - NumPosSamples);
   return (r_code);
}

 int ObjectDetection_ACF::GetInitNSFVs (   )

{
   int i,j;
   int n_neg;

   BCCL::Print (">> Obtaining initial negative sample images and extracting feature vectors from them...\n");
   BCCL::Print ("   # of negative samples: ");

   if (CreateICFArray (SmpImgSize)) return (1);
   int n = NumPosSamples;
   CLArray.Set (n,CLArray.Length - n,0.0f);
   SmpFileList& bi_list = *BkgImgList;
   int n_nspbi = (int)((float)NumInitNegSamples / bi_list.Height + 0.5f);
   if (n_nspbi < 5) n_nspbi = 5;
   GImage t_image(SmpImgSize.Width,SmpImgSize.Height);
   RndSmpImgExtraction rsie;
   rsie.Initialize (   );
   for (i = 0;   ; i++) {
      if (i >= bi_list.Height) i = 0;
      GImage s_image;
      if (bi_list.LoadImage (i,s_image)) {
         BCCL::Print ("\n[ERROR] Cannot load the image [%s].\n",bi_list[i]);
         return (1);
      }
      GImage f_image(s_image.Width,s_image.Height);
      GaussianFiltering_3x3 (s_image,f_image);
      for (j = 0; j < n_nspbi; j++) {
         rsie.Perform (f_image,t_image);
         GetICFs (t_image,ICFArray);
         GetFeatureVector (ICFArray,DetWinPos.X,DetWinPos.Y,FVArray[n]);
         CLArray[n++] = -1.0f;
         n_neg = n - NumPosSamples;
         if (n_neg && !(n_neg % 500)) BCCL::Print ("[%d]",n_neg);
         if (n >= NumSamples) goto escape;
      }
   }
   escape:;
   n_neg = n - NumPosSamples;
   BCCL::Print ("[%d]\n",n_neg);
   return (DONE);
}

 int ObjectDetection_ACF::GetPSFVs (   )

{
   int i;

   BCCL::Print (">> Obtaining positive sample images and extracting feature vectors from them...\n");
   BCCL::Print ("   # of postivie samples: ");

   if (CreateICFArray (SmpImgSize)) return (1);
   SmpFileList& ps_list = *PosSmpList;
   GImage t_image(SmpImgSize.Width,SmpImgSize.Height);
   GImage f_image(SmpImgSize.Width,SmpImgSize.Height);
   for (i = 0; i < ps_list.Height; i++) {
      if (i && !(i % 500)) BCCL::Print ("[%d]",i);
      GImage s_image;
      if (ps_list.LoadImage (i,s_image)) {
         BCCL::Print ("[ERROR] Cannot load the image [%s].\n",ps_list[i]);
         return (1);
      }
      GImage* p_s_image = &s_image;
      if (s_image.Width != SmpImgSize.Width || s_image.Height != SmpImgSize.Height) {
         Resize_2 (s_image,t_image);
         p_s_image = &t_image;
      }
      GaussianFiltering_3x3 (*p_s_image,f_image);
      GetICFs (f_image,ICFArray);
      GetFeatureVector (ICFArray,DetWinPos.X,DetWinPos.Y,FVArray[i]);
      CLArray[i] = 1.0f;
   }
   BCCL::Print ("[%d]\n",i);
   return (DONE);
}

 int ObjectDetection_ACF::Initialize (   )

{
   if (NumGHBins <= 2) return (1);
   NumChannels = NumGHBins + 2;
   if (CellSize < 1 ) return (2);
   if (DetWinSize.Width < CellSize || DetWinSize.Height < CellSize) return (3);
   if ((DetWinSize.Width % CellSize) || (DetWinSize.Height % CellSize)) return (4);
   NumFeatures = (DetWinSize.Width * DetWinSize.Height) / (CellSize * CellSize) * NumChannels;
   GradTable.Initialize (NumGHBins);
   return (DONE);
}

 int ObjectDetection_ACF::InitTraining (SmpFileList& ps_list,SmpFileList& bi_list)

{
   if (Initialize (   )) return (1);
   if (CheckSmpImgSize (   )) return (2);
   PosSmpList        = &ps_list;
   BkgImgList        = &bi_list;
   NumPosSamples     = PosSmpList->Height;
   NumInitNegSamples = (int)(NumPosSamples * Classifier.InitNPSCRatio);
   NumBootNegSamples = (int)(NumPosSamples * Classifier.BootNPSCRatio);
   MinNumNegSamples  = (int)(NumPosSamples * Classifier.MinNPSCRatio);
   if (Classifier.CascadeMode == BTC_CASCADE_MODE_SOFT && Classifier.MaxNumStages == 1)
      MaxNumNegSamples = NumInitNegSamples + Classifier.NumNSBTimes * NumBootNegSamples;
   else MaxNumNegSamples = GetMaximum (NumInitNegSamples,NumBootNegSamples);
   NumSamples = NumPosSamples + NumInitNegSamples;
   int max_n_samples = NumPosSamples + MaxNumNegSamples;
   FVArray.Create (NumFeatures,max_n_samples);
   CLArray.Create (max_n_samples);
   CLArray.Clear  (   );
   BCCL::Print (">> Training Info\n");
   BCCL::Print ("   Cascade mode                                : %d\n",Classifier.CascadeMode);
   BCCL::Print ("   Max tree depth                              : %d\n",Classifier.MaxTreeDepth);
   BCCL::Print ("   # of weak classifiers in the first stage    : %d\n",Classifier.InitNumWCs);
   BCCL::Print ("   Increment of # of weak classifiers per stage: %d\n",Classifier.IncNumWCs);
   BCCL::Print ("   Max # of stages                             : %d\n",Classifier.MaxNumStages);
   BCCL::Print ("   Sample image size                           : %03d x %03d\n",SmpImgSize.Width,SmpImgSize.Height);
   BCCL::Print ("   Detection window size                       : %03d x %03d\n",DetWinSize.Width,DetWinSize.Height);
   BCCL::Print ("   Detection window offset                     : X = %d, Y = %d\n",DetWinPos.X,DetWinPos.Y);
   BCCL::Print ("   # of channels                               : %d\n",NumChannels);
   BCCL::Print ("   # of features                               : %d\n",NumFeatures);
   BCCL::Print ("   Cell size                                   : %d\n",CellSize);
   BCCL::Print ("   # of positive samples                       : %d\n",NumPosSamples);
   BCCL::Print ("   # of initial negative samples               : %d\n",NumInitNegSamples);
   BCCL::Print ("   # of bootstrapped negative samples          : %d\n",NumBootNegSamples);
   BCCL::Print ("   Min # of negative samples                   : %d\n",MinNumNegSamples);
   BCCL::Print ("   Max # of negative samples                   : %d\n",MaxNumNegSamples);
   return (DONE);
}

 int ObjectDetection_ACF::Load (const char* cfg_file_name,const char* cls_base_file_name)

{
   Close (   );
   if (LoadConfig (cfg_file_name)) return (1);
   if (Classifier.Load (cls_base_file_name)) return (2);
   Initialize (   );
   return (DONE);
}

 int ObjectDetection_ACF::LoadConfig (const char* file_name)

{
   FileIO file(file_name,FIO_BINARY,FIO_READ);
   if (!file) return (1);
   uint signature;
   if (file.Read ((byte*)&signature,1,sizeof(signature))) return (2);
   if (signature != SIG_OBJECT_DETECTION_ACF) return (3);
   if (file.Read ((byte*)&NumGHBins,1,sizeof(NumGHBins))) return (4);
   if (file.Read ((byte*)&CellSize,1,sizeof(CellSize))) return (5);
   if (file.Read ((byte*)&DetWinSize,1,sizeof(DetWinSize))) return (6);
   if (file.Read ((byte*)&SmpImgSize,1,sizeof(SmpImgSize))) return (7);
   if (file.Read ((byte*)&DetWinPos,1,sizeof(DetWinPos))) return (8);
   return (DONE);
}

 int ObjectDetection_ACF::PerformTesting (   )

{
   int    i;
   double score;
   
   BCCL::Print (">> Testing the classifier with the training samples...\n");
   BCCL::Print ("   # of samples tested: ");
   int n_tps = 0;
   int n_fps = 0;
   int n_samples = NumPosSamples + MaxNumNegSamples;
   for (i = 0; i < NumPosSamples; i++) {
      if (i && !(i % 500)) BCCL::Print ("[%d]",i);
      if (Classifier.Classify (FVArray[i],FVArray.Width,score) > 0) n_tps++;
   }
   for (i = NumPosSamples; i < n_samples; i++) {
      if (i && !(i % 500)) BCCL::Print ("[%d]",i);
      if (Classifier.Classify (FVArray[i],FVArray.Width,score) > 0) n_fps++;
   }
   BCCL::Print ("[%d]\n",i);
   float tp_rate = (float)n_tps / NumPosSamples * 100.0f;
   float fp_rate = (float)n_fps / MaxNumNegSamples * 100.0f;
   BCCL::Print ("   True positive rate  = %5.1f%% (%d/%d)\n",tp_rate,n_tps,NumPosSamples);
   BCCL::Print ("   False positive rate = %5.1f%% (%d/%d)\n",fp_rate,n_fps,MaxNumNegSamples);
   return (DONE);
}

 int ObjectDetection_ACF::PerformTraining (   )

{
   int i;

   // Pos 샘플들의 FV를 얻는다.
   if (GetPSFVs (   )) return (1);
   // 초기 Neg 샘플들의 FV를 얻는다.
   if (GetInitNSFVs (   )) return (2);
   int flag_stop = FALSE;
   int n_wcs     = Classifier.InitNumWCs;
   for (i = 0; !flag_stop; i++,n_wcs += Classifier.IncNumWCs) {
      if (i) {
         // 부트스트랩 Neg 샘플들의 FV를 얻는다.
         int e_code = GetBootNSFVs (   );
         if (e_code < 0) flag_stop = TRUE;
         else if (e_code > 0) return (1);
      }
      // 소프트 케스케이드 모드이면 스테이지 분류기 개수를 한 개로 유지한다. 
      if (Classifier.CascadeMode == BTC_CASCADE_MODE_SOFT && Classifier.MaxNumStages == 1) {
         Classifier.Delete (   );
         if (Classifier.TrainNewStageClassifier (FVArray,CLArray,NumSamples,Classifier.InitNumWCs)) return (3);
         if (i >= Classifier.NumNSBTimes) break;
      }
      // 하드 케스케이드 모드이면 스테이지 분류기를 새로 추가한다.
      else {
         if (Classifier.TrainNewStageClassifier (FVArray,CLArray,NumSamples,n_wcs)) return (3);
         if ((i + 1) >= Classifier.MaxNumStages) break;
      }
   }
   Classifier.NumStages = Classifier.GetNumItems (   );
   BCCL::Print (">> Training completed.\n");
   return (DONE);
}

 int ObjectDetection_ACF::Save (const char* cfg_file_name,const char* cls_base_file_name)

{
   if (SaveConfig (cfg_file_name)) return (1);
   if (Classifier.Save (cls_base_file_name)) return (2);
   return (DONE);
}

 int ObjectDetection_ACF::SaveConfig (const char* file_name)

{
   FileIO file(file_name,FIO_BINARY,FIO_CREATE);
   if (!file) return (1);
   uint signature = SIG_OBJECT_DETECTION_ACF;
   if (file.Write ((byte*)&signature,1,sizeof(signature))) return (2);
   if (file.Write ((byte*)&NumGHBins,1,sizeof(NumGHBins))) return (3);
   if (file.Write ((byte*)&CellSize,1,sizeof(CellSize))) return (4);
   if (file.Write ((byte*)&DetWinSize,1,sizeof(DetWinSize))) return (5);
   if (file.Write ((byte*)&SmpImgSize,1,sizeof(SmpImgSize))) return (6);
   if (file.Write ((byte*)&DetWinPos,1,sizeof(DetWinPos))) return (7);
   return (DONE);
}

 int ObjectDetection_ACF::TestWithNegSamples (SmpFileList& bi_list,int n_samples,int& n_fps)

{
   int    i,j,n;
   double score;
   
   BCCL::Print (">> Testing the classifier with new negative samples...\n");
   BCCL::Print ("   # of negative samples: %d\n",n_samples);
   BCCL::Print ("   # of samples tested: ");
   n_fps = 0;
   if (Initialize (   )) return (1);
   if (CheckSmpImgSize (   )) return (2);
   if (CreateICFArray (SmpImgSize)) return (3);
   int n_nspbi = n_samples / bi_list.Height;
   if (n_nspbi < 5) n_nspbi = 5;
   IArray1D idx_array(bi_list.Height);
   GetShuffledIndexNumbers (idx_array);
   FArray1D fv(NumFeatures);
   GImage t_image(SmpImgSize.Width,SmpImgSize.Height);
   GImage f_image(SmpImgSize.Width,SmpImgSize.Height);
   RndSmpImgExtraction rsie;
   rsie.Initialize (   );
   for (i = n = 0;   ; i++) {
      int i1 = idx_array[i % idx_array.Length];
      GImage s_image;
      if (bi_list.LoadImage (i1,s_image)) {
         BCCL::Print ("\n[ERROR] Cannot load the image [%s].\n",bi_list[i1]);
         return (1);
      }
      for (j = 0; j < n_nspbi; j++) {
         rsie.Perform (s_image,t_image);
         GaussianFiltering_3x3 (t_image,f_image);
         GetICFs (f_image,ICFArray);
         GetFeatureVector (ICFArray,DetWinPos.X,DetWinPos.Y,fv);
         if (Classifier.Classify (fv,fv.Length,score) > 0) n_fps++;
         n++;
         if (n && !(n % 500)) BCCL::Print ("[%d]",n);
         if (n >= n_samples) goto escape;
      }
   }
   escape:;
   BCCL::Print ("[%d]\n",n);
   float fp_rate = (float)n_fps / n_samples * 100.0f;
   BCCL::Print ("   False positive rate = %5.1f%% (%d/%d)\n",fp_rate,n_fps,n_samples);
   return (DONE);
}

 int ObjectDetection_ACF::TestWithPosSamples (SmpFileList& ps_list,int& n_tps)

{
   int    i;
   double score;

   BCCL::Print (">> Testing the classifier with new positive samples...\n");
   BCCL::Print ("   # of positive samples: %d\n",ps_list.Height);
   BCCL::Print ("   # of samples tested: ");
   n_tps = 0;
   if (Initialize (   )) return (1);
   if (CheckSmpImgSize (   )) return (2);
   if (CreateICFArray (SmpImgSize)) return (3);
   FArray1D fv(NumFeatures);
   GImage t_image(SmpImgSize.Width,SmpImgSize.Height);
   GImage f_image(SmpImgSize.Width,SmpImgSize.Height);
   for (i = 0; i < ps_list.Height; i++) {
      if (i && !(i % 500)) BCCL::Print ("[%d]",i);
      GImage s_image;
      if (ps_list.LoadImage (i,s_image)) {
         BCCL::Print ("[ERROR] Cannot load the image [%s].\n",ps_list[i]);
         return (5);
      }
      GImage* p_s_image = &s_image;
      if (s_image.Width != SmpImgSize.Width || s_image.Height != SmpImgSize.Height) {
         Resize_2 (s_image,t_image);
         p_s_image = &t_image;
      }
      GaussianFiltering_3x3 (*p_s_image,f_image);
      GetICFs (f_image,ICFArray);
      GetFeatureVector (ICFArray,DetWinPos.X,DetWinPos.Y,fv);
      if (Classifier.Classify (fv,fv.Length,score) > 0) n_tps++;
   }
   BCCL::Print ("[%d]\n",i);
   float tp_rate = (float)n_tps / ps_list.Height * 100.0f;
   BCCL::Print ("   True positive rate = %5.1f%% (%d/%d)\n",tp_rate,n_tps,ps_list.Height);
   return (DONE);
}

 int ObjectDetection_ACF::Train (SmpFileList& ps_list,SmpFileList& bi_list)

{
   Close (   );
   if (InitTraining (ps_list,bi_list)) return (1);
   if (PerformTraining (   )) return (2);
   return (DONE);
}

#endif

}



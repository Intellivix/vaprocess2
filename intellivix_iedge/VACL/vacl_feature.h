#if !defined(__VACL_FEATURE_H)
#define __VACL_FEATURE_H

#include "vacl_define.h"

 namespace VACL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Class: GTElement
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__OS_WINDOWS) || defined(__OS_LINUX)
#pragma pack(1)
#endif

 struct GTElement
 
{
   float Mag;
   byte  BinIdx;
};

#if defined(__OS_WINDOWS) || defined(__OS_LINUX)
#pragma pack()
#endif

///////////////////////////////////////////////////////////////////////////////
// 
// Class: FeatureVector
//
///////////////////////////////////////////////////////////////////////////////

 class FeatureVector
 
{
   protected:
      float* Array;
      int    ArrayLength;

   public:
      FeatureVector (   );
      FeatureVector (float* s_array,int length);
      FeatureVector (const FeatureVector& s_vector);

   public:
      FeatureVector& operator =            (const FeatureVector& s_vector);
              float& operator []           (uint i) const;
                     operator float*       (   ) const;
                     operator const float* (   ) const;

   public:
      uint Length (   ) const;
      void Set    (float* s_array,uint length);
};

 inline float& FeatureVector::operator [] (uint i) const

{
   return (Array[i]);
}

 inline FeatureVector::operator float* (   ) const

{
   return (Array);  
}

 inline FeatureVector::operator const float* (   ) const

{
   return (Array);  
}

 inline uint FeatureVector::Length (   ) const

{
   return (ArrayLength);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: GradientTable
//
///////////////////////////////////////////////////////////////////////////////

 class GradientTable : public Array2D<GTElement>
 
{
   public:
      int NumBins; // # of histogram bins
      
   public:
      GradientTable (   );
   
   public:
      void Initialize (int n_bins);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: HOGExtraction
//
///////////////////////////////////////////////////////////////////////////////

 class HOGExtraction
 
{
   protected:
      Array1D<FArray2D> IntHistogram; // Integral Histogram
   
   public:
      GradientTable* GradTable;

   public:
      HOGExtraction (   );
   
   public:
      static void Normalize (float* s_array,int length);

   public:
      void Close         (   );
      void Extract       (IBox2D& s_rgn,float* d_array);
      void Initialize    (GradientTable& gr_table,GImage& s_image);
      int  IsInitialized (   );
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: HOGCellArray
//
///////////////////////////////////////////////////////////////////////////////

 class HOGCellArray : public FArray3D
// Width : # of histogram bins per cell
// Height: Cell array width
// Depth : Cell array height
// 1 block = 2 x 2 super-cells
{
   protected:
      FArray1D       HOGBuffer;
      HOGExtraction* HOGExtractor;
   
   public:
      IPoint2D StartPos;
      ISize2D  CellSize;
      ISize2D  CellArraySize;
   
   public:
      HOGCellArray (   );
   
   protected:
      void GetHOG_SuperCell (int sx,int sy,int sc_size);
   
   // 하기의 위치/크기 값은 특별한 언급이 없으면 Cell을 기본 단위로 한다. 
   public:
      void Close (   );
      // HOG Feature Vector의 Dimension을 계산한다.
      // dw_widt   [IN]: Detection window width
      // dw_height [IN]: Detection window height
      // sc_size   [IN]: Super-cell size
      // 리턴 값       : Feature vector dimension
      int GetFeatureVectorSize (int dw_width,int dw_height,int sc_size);
      // HOG Feature Vector를 계산한다.
      // dw_rect [IN] : Detection window rectangle
      // sc_size [IN] : Super-cell size
      // d_array [OUT]: Calculated feature vector
      void GetFeatureVector (IBox2D& dw_rect,int sc_size,float* d_array);
      // 2D HOG Cell Array를 구성한다.
      // hog_extractor [IN]: HOGExtraction object
      // s_pos         [IN]: (Left,Top) pixel coordinates of the cell array region
      // cell_size     [IN]: Cell size in pixel
      // cell_arr_size [IN]: Cell array size
      int Initialize (HOGExtraction& hog_extractor,IPoint2D& s_pos,ISize2D& cell_size,ISize2D& cell_arr_size);
      // 초기화가 되었으면 TRUE를 리턴한다.
      int IsInitialized (   );
};

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

int  DetectHeadFeaturePoints    (GImage& s_image,GImage& fg_image,IP2DArray1D& d_array);
void GetGradientChannelFeatures (GImage& s_image,GradientTable& gr_table,Array1D<FArray2D>& gh_array,FArray2D& gm_array);
// Integral을 구하는 함수들
// [주의] d_array.Width = s_array.Width + 1, d_array.Height = s_array.Height + 1 이어야 한다.
// [참고] 직사각형 (x,y,w,h)의 영역 내의 합 = (d_array[y + h][x + w] + d_array[y][x]) - (d_array[y + h][x] + d_array[y][x + w])
void GetIntegral (GImage& s_image,FArray2D& d_array);
void GetIntegral (GImage& s_image,IArray2D& d_array);
void GetIntegral (FArray2D& s_array,FArray2D& d_array);
void GetIntegral (IArray2D& s_array,IArray2D& d_array);
void GetIntegral (IArray2D& s_array,FArray2D& d_array);

}

#endif

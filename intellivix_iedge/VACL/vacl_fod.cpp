#include "vacl_binimg.h"
#include "vacl_conrgn.h"
#include "vacl_edge.h"
#include "vacl_filter.h"
#include "vacl_fod.h"
#include "vacl_morph.h"

#if defined(__DEBUG_FOD)
#include "Win32CL/Win32CL.h"
extern int _DVX_FOD;
extern int _DVY_FOD;
extern CImageView* _DebugView_FOD;
#endif

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: CThread_FODD
//
///////////////////////////////////////////////////////////////////////////////

 CThread_FODD::CThread_FODD (   )

{
   ThreadIdx = -1;
   SrcImage  = NULL;
}

 int CThread_FODD::Perform (   )

{
   int i;
   
   EdgeCRArray.Delete (   );
   DetObjects.Delete  (   );
   if (SrcImage == NULL) return (1);
   
   GImage s_image = SrcImage->Extract (ROI);
   GImage t_image(s_image.Width,s_image.Height);

   int margin  = 6; // PARAMETERS
   int margin2 = 2 * margin;
   
   // 입력 영상의 스무딩을 수행한다.
   GImage gf_image(s_image.Width,s_image.Height);
   GaussianFiltering (s_image,2.5f,gf_image); // PARAMETERS
   int si_mean = (int)GetMean (s_image,margin,margin,s_image.Width - margin2,s_image.Height - margin2);

   // 에지 이미지를 구한다. (에지 검출 임계치를 작게...)
   GImage eg_image1(s_image.Width,s_image.Height);
   DetectEdges_Sobel (gf_image,15,t_image);   // PARAMETERS
   t_image.SetBoundary (margin,PG_BLACK);
   PerformBinDilation  (t_image,1,eg_image1); // PARAMETERS
   IArray2D cr_map1(s_image.Width,s_image.Height);
   int n_crs1 = LabelCRs (eg_image1,cr_map1);
   if (!n_crs1) return (DONE);
   CRArray1D cr_array1(n_crs1 + 1);
   GetCRInfo (cr_map1,cr_array1);

   // 도로에 페인트된 마크들의 평균 밝기 값을 구한다.
   int bba_thrsld = (int)(0.5f * MaxBBArea);
   int t_num = 0; int64 t_sum = 0;
   for (i= 1; i < cr_array1.Length; i++) {
      ConnectedRegion& cr = cr_array1[i];
      FPoint2D cp = cr.GetCenterPosition (   );
      int bb_area = cr.Width * cr.Height;
      // CR의 BB의 면적이 일정 크기 이상 되는지 체크한다.
      if (bb_area > bba_thrsld) {
         int p_num; int64 p_sum;
         int cr_mean = (int)GetCRMean (gf_image,cr_map1,cr,i,p_num,p_sum);
         if (cr_mean > si_mean) {
             t_num += p_num;
             t_sum += p_sum;
         }
      }
   }
   
   // 도로에 페인트된 마크 영역을 구한다.
   GImage m_image(s_image.Width,s_image.Height);
   if (t_num) {
      int m      = (int)((double)t_sum / t_num);
      int min_pv = GetMaximum (m - 3,(m + si_mean) / 2); // PARAMETERS
      int max_pv = m + 35; // PARAMETERS
      Binarize (gf_image,min_pv,max_pv,t_image);
      PerformBinDilation (t_image,3,m_image); // PARAMETERS
   }
   else m_image.Clear (   );
   IArray2D cr_map2(s_image.Width,s_image.Height);
   int n_crs2 = LabelCRs (m_image,cr_map2);
   CRArray1D cr_array2(n_crs2 + 1);
   GetCRInfo (cr_map2,cr_array2);

   // 스무딩된 입력 영상의 에지 이미지를 구한다. (에지 검출 임계치를 크게...)
   EdgeImage.Create (s_image.Width,s_image.Height);
   DetectEdges_Sobel (gf_image,22,t_image);   // PARAMETERS
   t_image.SetBoundary (margin,PG_BLACK);
   PerformBinDilation  (t_image,1,EdgeImage); // PARAMETERS
   EdgeCRMap.Create (s_image.Width,s_image.Height);
   int n_crs3 = LabelCRs (EdgeImage,EdgeCRMap);
   if (!n_crs3) return (DONE);
   EdgeCRArray.Create (n_crs3 + 1);
   GetCRInfo (EdgeCRMap,EdgeCRArray);

   // FOD를 검출한다.
   DetObjects.Create (n_crs3 + 1);
   DetObjects.Clear  (   );
   BoundaryPixelTracking bp_tracker;
   bp_tracker.Initialize (EdgeCRMap);
   for (i = 1; i < EdgeCRArray.Length; i++) {
      ConnectedRegion& ecr = EdgeCRArray[i];
      FPoint2D cp = ecr.GetCenterPosition (   );
      // ECR의 BB의 면적이 지정된 범위 내에 들어오는지 체크한다.
      int bb_area = ecr.Width * ecr.Height;
      if (ecr.Area < (int)(0.5f * MinBBArea) || bb_area < MinBBArea || bb_area > MaxBBArea) continue; // PARAMETERS
      // ECR의 principal axis length ratio를 구한다.
      FPoint2D mj_axis,mn_axis;
      float    mja_length,mna_length;
      GetCRPrincipalAxes (EdgeCRMap,ecr,i,mj_axis,mn_axis,mja_length,mna_length);
      float palr = mja_length / mna_length;
      if (palr > MaxPALRatio) continue;
      // ECR이 도로 마크로부터 나왔는지 체크한다.
      int ov_area,m_cr_no;
      m_cr_no = FindMaxOverlappedCR (EdgeCRMap,ecr,i,cr_map2,n_crs2,ov_area);
      if (m_cr_no) {
         ConnectedRegion& m_cr = cr_array2[m_cr_no];
         float ar1 = (float)ov_area / ecr.Area;
         float ar2 = (float)GetCRArea (cr_map2,ecr,m_cr_no) / ecr.Area;
         if (ar1 > 0.3f && ar2 > 0.9f) continue; // PARAMETERS
      }
      // ECR의 disorder를 구한다.
      bp_tracker.Perform (ecr.SX,ecr.SY);
      int perimeter  = bp_tracker.NumBoundaryPixels + bp_tracker.NumMarginalPixels;
      float disorder = (float)sqrt ((perimeter * perimeter) / (4.0f * MC_PI * ecr.Area));
      // palr 또는 disorder 값이 큰 ECR에 대해 그보다 훨씬 긴 에지 영역의 일부인지 체크한다.
      if (palr > 1.5f || disorder > 1.5f) { // PARAMETERS
         m_cr_no = FindMaxOverlappedCR (EdgeCRMap,ecr,i,cr_map1,n_crs1,ov_area);
         if (m_cr_no) {
            ConnectedRegion& m_cr = cr_array1[m_cr_no];
            float ar1 = (float)ov_area   / ecr.Area;
            float ar2 = (float)m_cr.Area / ecr.Area;
            if (ar1 > 0.7f && ar2 > 2.0f) continue; // PARAMETERS
         }
      }
      DetObjects[i] = TRUE;
   }
   return (DONE);
}

 void CThread_FODD::ThreadProc (   )

{
   if (ThreadIdx == -1) return;
   HANDLE events[2];
   events[0] = (HANDLE)Event_Stop;
   events[1] = (HANDLE)Event_Start;
   while (TRUE) {
      DWORD result = WaitForMultipleObjects (2,events,FALSE,INFINITE);
      if (result == WAIT_OBJECT_0) break;       // Stop  이벤트인 경우
      else if (result == (WAIT_OBJECT_0 + 1)) { // Start 이벤트인 경우
         if (SrcImage != NULL) Perform (   );
         Event_End.SetEvent (   );
      }
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: FODDetection_MT
//
///////////////////////////////////////////////////////////////////////////////

 FODDetection_MT::FODDetection_MT (   )

{
   Flag_Initialized = FALSE;
   
   NumThreads    = 8;
   MinBBArea     = 28 * 28;
   MaxBBArea     = 75 * 75;
   MaxPALRatio   = 6.0f;
   OuterZoneSize = 0.03f;
}

 FODDetection_MT::~FODDetection_MT (   )

{
   Close (   );
}

 int FODDetection_MT::CheckRegionInOuterZone (IBox2D& s_rgn)

{
   int sx = (int)(SrcImgSize.Width  * OuterZoneSize);
   int sy = (int)(SrcImgSize.Height * OuterZoneSize);
   int ex = SrcImgSize.Width  - sx;
   int ey = SrcImgSize.Height - sy;
   FPoint2D cp = s_rgn.GetCenterPosition (   );
   if (sx < (int)cp.X && (int)cp.X < ex && sy < (int)cp.Y && (int)cp.Y < ey) return (1);
   return (DONE);
}

 void FODDetection_MT::Close (   )

{
   Threads.Delete    (   );
   EndEvents.Delete  (   );
   DetAreaMap.Delete (   );
   Flag_Initialized = FALSE;
}

 int FODDetection_MT::Initialize (ISize2D& si_size,ISize2D& ri_size)

{
   int i;
   
   Close (   );
   SrcImgSize = si_size;
   if (NumThreads < 1) return (1);
   // 비감지 영역을 설정한다.
   DetAreaMap.Create (si_size.Width,si_size.Height);
   if (DetAreas.GetNumItems (   )) {
      DetAreas.SetRefImageSize (ri_size);
      DetAreaMap.Clear (   );
      DetAreas.RenderAreaMap (DetAreaMap,PG_WHITE);
   }
   else DetAreaMap.Set (0,0,DetAreaMap.Width,DetAreaMap.Height,PG_WHITE);
   if (NonDetAreas.GetNumItems (   )) {
      NonDetAreas.SetRefImageSize (ri_size);
      NonDetAreas.RenderAreaMap   (DetAreaMap,PG_BLACK);
   }
   // 쓰레드 구성을 한다.
   EndEvents.Create (NumThreads);
   Threads.Create   (NumThreads);
   // 영상을 수평 방향으로 분할하여 처리할 수 있도록 각 쓰레드마다 ROI를 설정한다.
   // 이때 인접하는 ROI끼리 약간씩 겹치게 설정한다.
   int sx = 0, sy = 0;
   int width1 = (si_size.Width / NumThreads) / 2 * 2;
   int margin = (int)(0.05f * width1) / 2 * 2; // PARAMETERS
   int width2 = width1 + margin;
   int width3 = width2 + margin;
   int height = si_size.Height;
   for (i = 0; i < Threads.Length; i++) {
      CThread_FODD& thread = Threads[i];
      thread.Event_Start.ResetEvent (   );
      thread.Event_End.ResetEvent   (   );
      EndEvents[i] = (HANDLE)thread.Event_End;
      thread.ThreadIdx = i;
      if (NumThreads <= 1) {
         thread.ROI(0,0,si_size.Width,si_size.Height);
      }
      else {
         if (i == 0) thread.ROI(sx,sy,width2,height);
         else if (i == NumThreads - 1) thread.ROI(sx - margin,sy,width2,height);
         else thread.ROI(sx - margin,sy,width3,height);
         sx += width1;
      }
      CropRegion (si_size,thread.ROI);
      thread.MinBBArea   = MinBBArea;
      thread.MaxBBArea   = MaxBBArea;
      thread.MaxPALRatio = MaxPALRatio;
      // 쓰레드를 시작한다.
      thread.StartThread (   );
   }
   // 모든 쓰레드가 시작했는지 체크한다.
   while (TRUE) {
      int n = 0;
      for (i = 0; i < Threads.Length; i++)
         if (Threads[i].IsThreadRunning (   )) n++;
      if (n == Threads.Length) break;
   }
   Flag_Initialized = TRUE;
   return (DONE);
}

 void FODDetection_MT::MergeImages (Array1D<GImage*>& s_images,IB2DArray1D& s_rects,GImage& d_image)

{
   int i,x1,y1,x2,y2;

   for (i = 0; i < s_images.Length; i++) {
      int dx = s_rects[i].X;
      int dy = s_rects[i].Y;
      GImage& s_image = *(s_images[i]);
      for (y1 = 0,y2 = dy; y1 < s_image.Height; y1++,y2++) {
         byte* l_s_image = s_image[y1];
         byte* l_d_image = d_image[y2];
         for (x1 = 0,x2 = dx; x1 < s_image.Width; x1++,x2++) {
            l_d_image[x2] |= l_s_image[x1];
         }
      }
   }
}

 int FODDetection_MT::Perform (GImage& s_image,IB2DArray1D& d_array)

{
   int i,j,n;
  
   #if defined(__DEBUG_FOD)
   _DVX_FOD = _DVY_FOD = 0;
   _DebugView_FOD->Clear (   );
   #endif
   d_array.Delete (   );
   if (!Flag_Initialized) return (1);
   // 입력 영상을 여러 조각으로 분할한 후 각 조각 영상마다 해당 쓰레드가 처리한다.
   for (i = 0; i < Threads.Length; i++) {
      CThread_FODD& thread = Threads[i];
      thread.SrcImage = &s_image;
      thread.Event_End.ResetEvent (   );
      thread.Event_Start.SetEvent (   );
   }
   WaitForMultipleObjects (EndEvents.Length,EndEvents,TRUE,INFINITE);
   // 각 쓰레드가 얻은 조각 에지 영상들을 하나로 합친다.
   Array1D<GImage*> s_images(Threads.Length);
   IB2DArray1D s_rects(Threads.Length);
   for (i = 0; i < Threads.Length; i++) {
      CThread_FODD& thread = Threads[i];
      s_images[i] = &thread.EdgeImage;
      s_rects[i]  = thread.ROI;
   }
   GImage eg_image(s_image.Width,s_image.Height);
   eg_image.Clear (   );
   MergeImages (s_images,s_rects,eg_image);
   #if defined(__DEBUG_FOD)
   _DebugView_FOD->DrawImage (eg_image,_DVX_FOD,_DVY_FOD);
   _DVY_FOD += eg_image.Height;
   #endif
   // 에지 영상에 대한 CR 맵을 얻는다.
   IArray2D cr_map(eg_image.Width,eg_image.Height);
   int n_crs = LabelCRs (eg_image,cr_map);
   if (!n_crs) return (DONE);
   CRArray1D cr_array(n_crs + 1);
   GetCRInfo (cr_map,cr_array);
   BArray1D do_array(cr_array.Length);
   do_array.Clear (   );
   // 각 쓰레드에서 검출한 FOD 후보들에 대해 검증을 수행한다.
   for (i = n = 0; i < Threads.Length; i++) {
      CThread_FODD& thread = Threads[i];
      CRArray1D& crs1 = thread.EdgeCRArray;
      BArray1D&  dos  = thread.DetObjects;
      int sx = thread.ROI.X;
      int sy = thread.ROI.Y;
      for (j = 1; j < dos.Length; j++) {
         if (dos[j]) {
            ConnectedRegion& cr1 = crs1[j];
            int dx = sx + cr1.SX;
            int dy = sy + cr1.SY;
            int cr_no = cr_map[dy][dx];
            if (cr_no) {
               ConnectedRegion& cr2 = cr_array[cr_no];
               int bb_area = cr2.Width * cr2.Height;
               if (bb_area <= MaxBBArea) {
                  FPoint2D cp = cr2.GetCenterPosition (   );
                  if (DetAreaMap[(int)cp.Y][(int)cp.X]) {
                     if (CheckRegionInOuterZone (cr2) != DONE) {
                        if (!do_array[cr_no]) {
                           do_array[cr_no] = TRUE;
                           n++;
                        }
                     }
                  }
               }
            }
         }
      }
   }
   if (!n) return (DONE);
   // 검출된 FOD의 영역을 저장한다.
   d_array.Create (n);
   for (i = 1,n = 0; i < do_array.Length; i++)
      if (do_array[i]) d_array[n++].Set (cr_array[i]);
   return (DONE);
}

}

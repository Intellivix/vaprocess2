#if !defined(__VACL_FACE_H)
#define __VACL_FACE_H

#include "vacl_dnn.h"
#include "vacl_opencv.h"

#if defined(__LIB_DLIB)
#include "dlib/image_processing.h"
#endif

#if defined(__LIB_SEETA)
#include "Seeta/FaceDetection/face_detection.h"
#include "Seeta/FaceAlignment/face_alignment.h"
#endif

#if defined(__LIB_VERILOOK)
#include "NCore.hpp"
#include "NImage.hpp"
#include "NImages.hpp"
#include "NLExtractor.hpp"
#include "NMatcher.hpp"
#include "NMatchers.hpp"
#include "NTemplate.hpp"
#include "NTemplates.hpp"
using namespace Neurotec::Biometrics;
using namespace Neurotec::Images;
#endif

 namespace VACL

{

class ObjectTracking;

///////////////////////////////////////////////////////////////////////////////
//
// Class: FaceInfo
//
///////////////////////////////////////////////////////////////////////////////

 class FaceInfo : public IBox2D

{
   protected:
      int Flag_Occluded;
   
   public:
      float Time_FaceOccluded;
      float Time_FaceUndetected;
      float Time_LeftEyeUndetected;
      float Time_RightEyeUndetected;
      float Time_MouthUndetected;
   
   // 하기의 모든 좌표 값은 얼굴 바운딩 박스의 (Left,Top) 좌표를 원점으로 했을 때의 좌표 값임
   public:
      FPoint2D    Eyes[2];  // 눈의 중심 좌표 (Eyes[0]: 왼쪽 눈 Eyes[1]: 오른쪽 눈)
      FPoint2D    Nose;     // 코 끝의 좌표
      FPoint2D    Mouth[2]; // 입의 좌표 (Mouth[0]: 입가 좌측 끝, Mouth[1]: 입가 우측 끝)
      FP2DArray1D Points;   // Face Landmark Detector가 검출한 모든 포인트들의 좌표

   public:
      IBox2D LeftEyeRegion;
      IBox2D RightEyeRegion;
      IBox2D MouthRegion;
   
   public:
      float    Pitch; // Rotation about X-axis (끄덕끄덕)
      float    Yaw;   // Rotation about Y-axis (도리도리)
      float    Roll;  // Rotation about Z-axis (갸우뚱)
      FPoint2D Pose;  // 영상 위에 얼굴의 Normal Vector를 표시할 때 필요
                      // (Nose 좌표와 Pose 좌표를 연결하는 선을 그어서 Normal Vector를 표시한다.)

   public:
      FaceInfo (   );
   
   public:
      int  AreLandmarksValid  (   );
      void Clear              (   );
      int  IsBoundingBoxValid (   );
      int  IsOccluded         (   );
      int  IsPoseValid        (   );
      void SetFaceOccluded    (int flag_occluded);
};

typedef Array1D<FaceInfo> FIArray1D;

 inline int FaceInfo::IsOccluded (   )

{
   return (Flag_Occluded);
}

 inline void FaceInfo::SetFaceOccluded (int flag_occluded)

{
   Flag_Occluded = flag_occluded;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: FaceDetection
//
///////////////////////////////////////////////////////////////////////////////

 class FaceDetection

{
   protected:
      int Flag_Initialized;

   public:
      FaceDetection (   );
      virtual ~FaceDetection (   );

   public:
      virtual void Close      (   );
      virtual int  Initialize (const char* dir_name) { return (1); };
      virtual int  Perform    (GImage&   s_image ,IB2DArray1D& d_rects) { return (1); };
      virtual int  Perform    (BGRImage& s_cimage,IB2DArray1D& d_rects) { return (1); };

   public:
      int IsInitialized (   );
};

 inline int FaceDetection::IsInitialized (   )

{
   return (Flag_Initialized);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: FaceDetection_OpenCV
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_OPENCV)

 class FaceDetection_OpenCV : public FaceDetection

{
   protected:
      cv::CascadeClassifier FaceDetector;
   
   public:
      int   MinFaceSize;
      int   NMSThreshold;
      float IPScaleFactor;

   public:
      FaceDetection_OpenCV (   );

   public:
      virtual int Initialize (const char* dir_name);
      virtual int Perform    (GImage& s_image,IB2DArray1D& d_rects);
};

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: FaceDetection_OpenVINO
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_OPENVINO)

 class FaceDetection_OpenVINO : public FaceDetection

{
   protected:
      DNN_OpenVINO_ObjectDetector FaceDetector;

   public:
      int   Flag_Advanced;
      int   Flag_UseGPU;
      float DThreshold;

   public:
      FaceDetection_OpenVINO (   );

   public:
      virtual void Close      (   );
      virtual int  Initialize (const char* dir_name);
      virtual int  Perform    (BGRImage& s_cimage,IB2DArray1D& d_rects);
};

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: FaceDetection_Seeta
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_SEETA)

 class FaceDetection_Seeta : public FaceDetection

{
   protected:
      seeta::FaceDetection* FaceDetector;

   public:
      int   MinFaceSize;
      int   MaxFaceSize;
      int   SWStepSize;
      float IPScaleFactor;
      float DThreshold;
   
   public:
      FaceDetection_Seeta (   );

   public:
      void SetMinFaceSize(int size);
      void SetMaxFaceSize(int size);
      void SetSWStepSize(int step_size);
      void SetIPScaleFactor(float s_factor);
      void SetFDScoreThreshold(float thres);

   public:
      virtual void Close      (   );
      virtual int  Initialize (const char* dir_name);
      virtual int  Perform    (GImage& s_image,IB2DArray1D& d_rects);
};

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: FaceDetection_VeriLook
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_VERILOOK)

 class FaceDetection_VeriLook : public FaceDetection

{
   protected:
      N_CLASS(NLExtractor)* FaceDetector;
      
   public:
      int MinIod;                // 눈 사이의 최소 거리 (단위: 픽셀)
      int MaxRollAngleDeviation; // 2D 회전 허용 각도 (단위: 도)
      int MaxYawAngleDeviation;  // 3D 회전(좌우회전) 허용 각도 (단위: 도)

   public:
      FaceDetection_VeriLook (   );
   
   public:
      virtual void Close      (   );
      virtual int  Initialize (const char* dir_name);
      virtual int  Perform    (GImage& s_image,IB2DArray1D& d_rects);
};

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: FaceLandmarkDetection
//
///////////////////////////////////////////////////////////////////////////////

 class FaceLandmarkDetection

{
   protected:
      int Flag_Initialized;
   
   public:
      FaceLandmarkDetection (   );
      virtual ~FaceLandmarkDetection (   );
   
   public:
      virtual void Close           (   );
      virtual int  DetectLandmarks (GImage& s_image,FaceInfo& face_info) { return (1); };
      virtual int  Initialize      (const char* dir_name) { return (1); };
      
   public:
      int IsInitialized (   );
      int Perform       (GImage& s_image,ObjectTracking& obj_tracker);
};

 inline int FaceLandmarkDetection::IsInitialized (   )

{
   return (Flag_Initialized);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: FaceLandmarkDetection_DLib
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_DLIB)

 class FaceLandmarkDetection_DLib : public FaceLandmarkDetection

{
   protected:
      dlib::shape_predictor* FaceLandmarkDetector;
   
   public:
      FaceLandmarkDetection_DLib (   );

   public:
      virtual void Close           (   );
      virtual int  DetectLandmarks (GImage& s_image,FaceInfo& face_info);
      virtual int  Initialize      (const char* dir_name);
};

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: FaceLandmarkDetection_Seeta
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_SEETA)

 class FaceLandmarkDetection_Seeta : public FaceLandmarkDetection

{
   protected:
      seeta::FaceAlignment* FaceLandmarkDetector;

   public:
      FaceLandmarkDetection_Seeta (   );
   
   public:
      virtual void Close           (   );
      virtual int  DetectLandmarks (GImage& s_image,FaceInfo& face_info);
      virtual int  Initialize      (const char* dir_name);
};

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: FacePartsDetection
//
///////////////////////////////////////////////////////////////////////////////

 class FacePartsDetection

{
   protected:
      int Flag_Initialized;

   public:
      float MaxTime_Undetected;
   
   public:
      FacePartsDetection (   );
      virtual ~FacePartsDetection (   );

   public:
      virtual void Close       (   );
      virtual int  DetectParts (GImage& s_image,float frm_rate,FaceInfo& face_info) { return (1); };
      virtual int  Initialize  (const char* dir_name) { return (1); };

   public:
      int IsInitialized (   );
      int Perform       (GImage& s_image,ObjectTracking& obj_tracker);
};

 inline int FacePartsDetection::IsInitialized (   )

{
   return (Flag_Initialized);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: FacePartsDetection_OpenCV
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_OPENCV)

 class FacePartsDetection_OpenCV : public FacePartsDetection

{
   protected:
      cv::CascadeClassifier LeftEyeDetector;
      cv::CascadeClassifier RightEyeDetector;
      cv::CascadeClassifier MouthDetector;
   
   public:
      virtual int DetectParts (GImage& s_image,float frm_rate,FaceInfo& fi);
      virtual int Initialize  (const char* dir_name);
};

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: FacePoseEstimation
//
///////////////////////////////////////////////////////////////////////////////

 class FacePoseEstimation

{
   protected:
      int Flag_Initialized;

   public:
      FacePoseEstimation (   );
      virtual ~FacePoseEstimation (   );

   public:
      virtual void Close            (   );
      virtual int  EstimateFacePose (BGRImage& s_cimage,FaceInfo& face_info) { return (1); };
      virtual int  Initialize       (const char* dir_name) { return (1); };

   public:
      int IsInitialized (   );
      int Perform       (BGRImage& s_cimage,ObjectTracking& obj_tracker);
};

 inline int FacePoseEstimation::IsInitialized (   )

{
   return (Flag_Initialized);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: FacePoseEstimation_OpenVINO
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_OPENVINO)

 class FacePoseEstimation_OpenVINO : public FacePoseEstimation

{
   protected:
      DNN_OpenVINO_FacePoseEstimator FacePoseEstimator;

   public:
      int Flag_UseGPU;

   public:
      FacePoseEstimation_OpenVINO (   );

   public:
      virtual void Close            (   );
      virtual int  EstimateFacePose (BGRImage& s_cimage,FaceInfo& face_info);
      virtual int  Initialize       (const char* dir_name);
};

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: FaceExtraction
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_SEETA) && defined(__LIB_OPENCV)

 class FaceExtraction

{
   public:
      float MaxTime_Occluded;
      float MaxTime_Undetected;
   
   public:
      FaceDetection_Seeta         FaceDetector;
      FaceLandmarkDetection_Seeta FaceLandmarkDetector;
      FacePartsDetection_OpenCV   FacePartsDetector;
      
   public:
      FaceExtraction (   );
      virtual ~FaceExtraction (   );

   public:
      virtual void Close (   );

   public:
      int Initialize    (const char* dir_name);
      int IsInitialized (   );
      int Perform       (GImage& s_image,FIArray1D& d_array,int flag_fpd = FALSE);
      int Perform       (GImage& s_image,ObjectTracking& obj_tracker,int flag_fod = FALSE);
};

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: FrontalFaceVerification
//
///////////////////////////////////////////////////////////////////////////////

 class FrontalFaceVerification

{
   // 얼굴 분석/인식 용
   public:
      float MaxPitch1;
      float MaxYaw1;
      float MaxRoll1;

   // 주목 시간 계산 용
   public:
      float MaxPitch2;
      float MaxYaw2;
      float MaxRoll2;

   public:
      FrontalFaceVerification (   );
   
   public:
      int ReadFile  (CXMLIO* pIO);
      int WriteFile (CXMLIO* pIO);
   
   public:
      void Perform (ObjectTracking& obj_tracker);
};


///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

int EstimateFacePose (FaceInfo& face_info);

}

#endif

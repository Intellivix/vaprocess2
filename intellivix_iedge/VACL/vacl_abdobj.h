#if !defined(__VACL_ABDOBJ_H)
#define __VACL_ABDOBJ_H

#include "vacl_define.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: AbandonedObject
//
///////////////////////////////////////////////////////////////////////////////

 class AbandonedObject
 
{
   public:
      int   Flag_Matched;
      int   ID;
      int   SX,SY,EX,EY;
      float Score;
      float StartTime;
      
   public:
      AbandonedObject* Prev;
      AbandonedObject* Next;
      
   public:
      AbandonedObject (   );
   
   public:
      void GetOverlappedRegion (IPoint2D &s_ltp,IPoint2D &s_rbp,IPoint2D &d_ltp,IPoint2D &d_rbp);
      int  IsInside            (IPoint2D &s_pos);
      void SetRegion           (IPoint2D &lt_pos,IPoint2D &rb_pos);
      void UpdateRegion        (IPoint2D &lt_pos,IPoint2D &rb_pos);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: AbandonedObjectList
//
///////////////////////////////////////////////////////////////////////////////

 class AbandonedObjectList : public LinkedList<AbandonedObject>
 
{
   public:
      float MaxScore;
   
   public:
      AbandonedObjectList (   );
   
   public:
      void UpdateObjectScores (float frm_duration);
};

}

#endif

#if !defined(__VACL_EVTRULE_H)
#define __VACL_EVTRULE_H

#include "vacl_grptrack.h"
#include "vacl_objfilter.h"
#include "vacl_objtrack.h"

 namespace VACL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_Abandoned
//
///////////////////////////////////////////////////////////////////////////////

 class EventRuleConfig_Abandoned

{
   public:
      float DetectionTime;
      float MaxInitMovTime;
      float MaxInitMovDistance;

   public:
      EventRuleConfig_Abandoned (   );

   public:
      int CheckSetup (EventRuleConfig_Abandoned* other);
      int ReadFile   (FileIO& file);
      int ReadFile   (CXMLIO* pIO);
      int WriteFile  (FileIO& file);
      int WriteFile  (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_CarAccident
//
///////////////////////////////////////////////////////////////////////////////

 class EventRuleConfig_CarAccident
 
{
   public:
      int   Flag_Human;
      int   MinNumCars;
      float DetectionTime;
      
   public:
      EventRuleConfig_CarAccident (   );

   public:
      int CheckSetup (EventRuleConfig_CarAccident* other);
      int ReadFile   (FileIO& file);
      int ReadFile   (CXMLIO* pIO);
      int WriteFile  (FileIO& file);
      int WriteFile  (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_CarParking
//
///////////////////////////////////////////////////////////////////////////////

 class EventRuleConfig_CarParking
 
{
   public:
      float DetectionTime;
      
   public:
      EventRuleConfig_CarParking (   );

   public:
      int CheckSetup (EventRuleConfig_CarParking* other);
      int ReadFile   (FileIO& file);
      int ReadFile   (CXMLIO* pIO); 
      int WriteFile  (FileIO& file);
      int WriteFile  (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_CarStopping
//
///////////////////////////////////////////////////////////////////////////////

 class EventRuleConfig_CarStopping

{
   public:
      float DetectionTime;

   public:
      EventRuleConfig_CarStopping (   );

   public:
      int CheckSetup (EventRuleConfig_CarStopping* other);
      int ReadFile   (FileIO& file);
      int ReadFile   (CXMLIO* pIO);
      int WriteFile  (FileIO& file);
      int WriteFile  (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_ColorChange
//
///////////////////////////////////////////////////////////////////////////////

 class EventRuleConfig_ColorChange
 
{
   public:
      float DetectionTime;

   public:
      EventRuleConfig_ColorChange (   );
      
   public:
      int CheckSetup (EventRuleConfig_ColorChange* other);
      int ReadFile  (FileIO& file);
      int ReadFile  (CXMLIO* pIO);
      int WriteFile (FileIO& file);
      int WriteFile (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_Counting
//
///////////////////////////////////////////////////////////////////////////////

 class EventRuleConfig_Counting
 
{
   public:
      int     Count;
      float   AspectRatio;
      ILine2D MajorAxis;
      
   public:
      EventRuleConfig_Counting (   );
      
   public:
      void Clear (   );

   public:
      int CheckSetup (EventRuleConfig_Counting* other);
      int ReadFile   (FileIO& file);
      int ReadFile   (CXMLIO* pIO);
      int WriteFile  (FileIO& file);
      int WriteFile  (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_CrowdDensity
//
///////////////////////////////////////////////////////////////////////////////

 class EventRuleConfig_CrowdDensity

{
   public:
      int   CountThreshold;
      float TransformFactor; 
      
   public:
      EventRuleConfig_CrowdDensity (   );

   public:
      int CheckSetup (EventRuleConfig_CrowdDensity* other);
      int ReadFile   (FileIO& file);
      int ReadFile   (CXMLIO* pIO);
      int WriteFile  (FileIO& file);
      int WriteFile  (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_DirectionalMotion
//
///////////////////////////////////////////////////////////////////////////////

 class EventRuleConfig_DirectionalMotion
 
{
   public:
      float DetectionTime;
      float Direction;
      float Range;
      float MinMovingDistance;
      
   public:
      EventRuleConfig_DirectionalMotion (   );

   public:
      int CheckSetup (EventRuleConfig_DirectionalMotion* other);
      int ReadFile   (FileIO& file);
      int ReadFile   (CXMLIO* pIO);
      int WriteFile  (FileIO& file);
      int WriteFile  (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_Dwell
//
///////////////////////////////////////////////////////////////////////////////

 class EventRuleConfig_Dwell

{
   public:
      int   MaxZoneCapacity;
      float DetectionTime;
      float MaxWaitingTime;

   public:
      EventRuleConfig_Dwell (   );

   public:
      int CheckSetup (EventRuleConfig_Dwell* other);
      int ReadFile   (FileIO& file);
      int ReadFile   (CXMLIO* pIO);
      int WriteFile  (FileIO& file);
      int WriteFile  (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_Falling
//
///////////////////////////////////////////////////////////////////////////////

 class EventRuleConfig_Falling

{
   public:
      float DetectionTime;

   public:
      EventRuleConfig_Falling (   );

   public:
      int CheckSetup (EventRuleConfig_Falling* other);
      int ReadFile   (FileIO& file);
      int ReadFile   (CXMLIO* pIO);
      int WriteFile  (FileIO& file);
      int WriteFile  (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_Flame
//
///////////////////////////////////////////////////////////////////////////////

 class EventRuleConfig_Flame
 
{
   public:
      float DetectionTime;
      
   public:
      EventRuleConfig_Flame (   );
      
   public:
      int CheckSetup (EventRuleConfig_Flame* other);
      int ReadFile   (FileIO& file);
      int ReadFile   (CXMLIO* pIO);
      int WriteFile  (FileIO& file);
      int WriteFile  (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_Gathering
//
///////////////////////////////////////////////////////////////////////////////

 class EventRuleConfig_Gathering
 
{
   public:
      int   MinNumPersons;
      float DetectionTime;

   public:
      EventRuleConfig_Gathering (   );
      
   public:
      int CheckSetup (EventRuleConfig_Gathering* other);
      int ReadFile   (FileIO& file);
      int ReadFile   (CXMLIO *pIO);
      int WriteFile  (FileIO& file);
      int WriteFile  (CXMLIO *pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_LineCrossing
//
///////////////////////////////////////////////////////////////////////////////

 class EventRuleConfig_LineCrossing
 
{
   public:
      int     Direction;
      ILine2D CrossingLine;
      
   public:
      EventRuleConfig_LineCrossing (   );

   public:
      int CheckSetup (EventRuleConfig_LineCrossing* other);
      int ReadFile   (FileIO& file);
      int ReadFile   (CXMLIO* pIO);
      int WriteFile  (FileIO& file);
      int WriteFile  (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_Loitering
//
///////////////////////////////////////////////////////////////////////////////

 class EventRuleConfig_Loitering

{
   public:
      float DetectionTime;

   public:
      EventRuleConfig_Loitering (   );

   public:
      int CheckSetup (EventRuleConfig_Loitering* other);
      int ReadFile   (FileIO& file);
      int ReadFile   (CXMLIO* pIO);
      int WriteFile  (FileIO& file);
      int WriteFile  (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_PathPassing
//
///////////////////////////////////////////////////////////////////////////////

 class EventRuleConfig_PathPassing
 
{
   public:
      int   Direction;
      float DetectionTime;
      float PassingTime;

   public:
      ArrowList               Arrows;
      PathZoneRoot            ZoneTree;
      LinkedList<PathZonePtr> PathZonePtrs;
      
   public:
      EventRuleConfig_PathPassing (   );

   public:
      int CheckSetup (EventRuleConfig_PathPassing* other);
      int ReadFile   (FileIO& file);
      int ReadFile   (CXMLIO* pIO);
      int WriteFile  (FileIO& file);
      int WriteFile  (CXMLIO* pIO);

   public:
      void Initialize         (EventZone *evt_zone);
      void UpdatePathZonePtrs (   );
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_Removed
//
///////////////////////////////////////////////////////////////////////////////

 class EventRuleConfig_Removed
 
{
   public:
      float DetectionTime;
      float MaxInitMovTime;
      float MaxInitMovDistance;

   public:
      EventRuleConfig_Removed (   );
      
   public:
      int CheckSetup (EventRuleConfig_Removed* other);
      int ReadFile   (FileIO& file);
      int ReadFile   (CXMLIO* pIO);
      int WriteFile  (FileIO& file);
      int WriteFile  (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_Smoke
//
///////////////////////////////////////////////////////////////////////////////

 class EventRuleConfig_Smoke
 
{
   public:
      float DetectionTime;
      
   public:
      EventRuleConfig_Smoke (   );
      
   public:
      int CheckSetup (EventRuleConfig_Smoke* other);
      int ReadFile   (FileIO& file);
      int ReadFile   (CXMLIO* pIO);
      int WriteFile  (FileIO& file);
      int WriteFile  (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_Stopping
//
///////////////////////////////////////////////////////////////////////////////

 class EventRuleConfig_Stopping

{
   public:
      float DetectionTime;

   public:
      EventRuleConfig_Stopping (   );
      
   public:
      int CheckSetup (EventRuleConfig_Stopping* other);
      int ReadFile   (FileIO& file);
      int ReadFile   (CXMLIO* pIO);
      int WriteFile  (FileIO& file);
      int WriteFile  (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_TrafficCongestion
//
///////////////////////////////////////////////////////////////////////////////

 class EventRuleConfig_TrafficCongestion
 
{
   public:
      float DetectionTime;
      float MaxAvgRealSpeed;
      
   public:
      EventRuleConfig_TrafficCongestion (   );
      
   public:
      int CheckSetup (EventRuleConfig_TrafficCongestion* other);
      int ReadFile   (FileIO& file);
      int ReadFile   (CXMLIO* pIO);
      int WriteFile  (FileIO& file);
      int WriteFile  (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_Violence
//
///////////////////////////////////////////////////////////////////////////////
 
 class EventRuleConfig_Violence
 
{
   public:
      float DetectionTime;
      
   public:
      EventRuleConfig_Violence (   );

   public:
      int CheckSetup (EventRuleConfig_Violence* other);
      int ReadFile   (FileIO& file);
      int ReadFile   (CXMLIO* pIO);
      int WriteFile  (FileIO& file);
      int WriteFile  (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_WaterLevel
//
///////////////////////////////////////////////////////////////////////////////

 class EventRuleConfig_WaterLevel

{
   public:
      int     Direction;
      float   DetectionTime;
      float   Threshold;
      ILine2D CrossingLine;

   public:
      EventRuleConfig_WaterLevel (   );

   public:
      int CheckSetup (EventRuleConfig_WaterLevel* other);
      int ReadFile   (FileIO& file);
      int ReadFile   (CXMLIO* pIO);
      int WriteFile  (FileIO& file);
      int WriteFile  (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleConfig_ZoneColor
//
///////////////////////////////////////////////////////////////////////////////

 class EventRuleConfig_ZoneColor

{
   public:
      float DetectionTime;
      float LastingTime;

   public:
      int      CDS;      // Color Detection Sensitivity
      BGRColor TgtColor; // Target Color

   public:
      EventRuleConfig_ZoneColor (   );

   public:
      void ResetCDS (   );
   
   public:
      int CheckSetup (EventRuleConfig_ZoneColor* other);
      int ReadFile   (FileIO& file);
      int ReadFile   (CXMLIO* pIO); 
      int WriteFile  (FileIO& file);
      int WriteFile  (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRule
//
///////////////////////////////////////////////////////////////////////////////

#define ER_EVENT_LOITERING              0
#define ER_EVENT_PATH_PASSING           1
#define ER_EVENT_DIRECTIONAL_MOTION     2
#define ER_EVENT_ENTERING               3
#define ER_EVENT_LEAVING                4
#define ER_EVENT_STOPPING               5
#define ER_EVENT_ABANDONED              6
#define ER_EVENT_LINE_CROSSING          7
#define ER_EVENT_SMOKE                  8
#define ER_EVENT_FLAME                  9
#define ER_EVENT_FALLING                10
#define ER_EVENT_GATHERING              11
#define ER_EVENT_VIOLENCE               12
#define ER_EVENT_NONE_0002              13
#define ER_EVENT_CAR_ACCIDENT           14
#define ER_EVENT_CAR_STOPPING           15
#define ER_EVENT_TRAFFIC_CONGESTION     16
#define ER_EVENT_COLOR_CHANGE           17
#define ER_EVENT_CAR_PARKING            18
#define ER_EVENT_REMOVED                19
#define ER_EVENT_WATER_LEVEL            20
#define ER_EVENT_ZONE_COLOR             21
#define ER_EVENT_DWELL                  22
#define ER_EVENT_DWELL_OVERCROWDED      23
#define ER_EVENT_DWELL_OVERWAITING      24
#define ER_EVENT_CROWD_DENSITY          25
#define ER_EVENT_CAMERA_DAMAGE          60 // UPLUS CAMERA DAMAGE EVENT (SUDDENLY_SCENE_CHANGE)

#define ER_OPTION_ENABLE_COUNTER        0x00000001
#define ER_OPTION_ENFORCE_ALARM_PERIOD  0x00000002

#define ER_STATUS_ENABLED               0x00000001
 
#define ER_WEEKDAY_SUN                  0x00000001
#define ER_WEEKDAY_MON                  0x00000002
#define ER_WEEKDAY_TUE                  0x00000004
#define ER_WEEKDAY_WED                  0x00000008
#define ER_WEEKDAY_THU                  0x00000010
#define ER_WEEKDAY_FRI                  0x00000020
#define ER_WEEKDAY_SAT                  0x00000040

 class EventRule

{
   public:
      int   ID;
      int   Status;
      int   Options;
      int   EventType;
      int   ObjectType;
      int   PersonAttrs;
      int   Schedule_WD;
      char  Name[128];
      float AlarmPeriod;

   public:
      EventRuleConfig_Abandoned         ERC_Abandoned;
      EventRuleConfig_CarAccident       ERC_CarAccident;
      EventRuleConfig_CarParking        ERC_CarParking;
      EventRuleConfig_CarStopping       ERC_CarStopping;
      EventRuleConfig_ColorChange       ERC_ColorChange;
      EventRuleConfig_Counting          ERC_Counting;
      EventRuleConfig_CrowdDensity      ERC_CrowdDensity;
      EventRuleConfig_DirectionalMotion ERC_DirectionalMotion;
      EventRuleConfig_Dwell             ERC_Dwell;
      EventRuleConfig_Falling           ERC_Falling;
      EventRuleConfig_Flame             ERC_Flame;
      EventRuleConfig_Gathering         ERC_Gathering;
      EventRuleConfig_LineCrossing      ERC_LineCrossing;
      EventRuleConfig_Loitering         ERC_Loitering;
      EventRuleConfig_PathPassing       ERC_PathPassing;
      EventRuleConfig_Removed           ERC_Removed;
      EventRuleConfig_Smoke             ERC_Smoke;
      EventRuleConfig_Stopping          ERC_Stopping;
      EventRuleConfig_TrafficCongestion ERC_TrafficCongestion;
      EventRuleConfig_Violence          ERC_Violence;
      EventRuleConfig_WaterLevel        ERC_WaterLevel;
      EventRuleConfig_ZoneColor         ERC_ZoneColor;

   public:
      EventDetection*  EventDetector;
      ObjectFiltering* ObjectFilter;
      
   public:
      LinkedList<Period_HM>  Schedule_HM;
      LinkedList<Period_YMD> Schedule_YMD;

   public:
      EventRule* Prev;
      EventRule* Next;

   public:
      EventRule (   );
      virtual ~EventRule (   );
   
   public:
      virtual int ReadFile   (FileIO& file);
      virtual int ReadFile   (CXMLIO* pIO);
      virtual int WriteFile  (FileIO& file);
      virtual int WriteFile  (CXMLIO* pIO);
      virtual int CheckSetup (EventRule* er);

   protected:
      int CheckHumanFalling          (TrackedObject *t_object);
      int CheckObjectsAbandonedStart (ObjectTracking &obj_tracker,EventZone *evt_zone);

   protected:
      int DetectEvent_Presence (TrackedBlob* t_blob,EventZone *evt_zone,BlobTracking& blb_tracker,float dt_time,float bo_time,int to_type,int flag_cont,int flag_bkg);

   protected:
      int DetectEvent_Abandoned         (TrackedObject *t_object,EventZone *evt_zone);
      int DetectEvent_CarParking        (TrackedObject *t_object,EventZone *evt_zone);
      int DetectEvent_DirectionalMotion (TrackedObject *t_object,EventZone *evt_zone);
      int DetectEvent_Dwell             (TrackedObject *t_object,EventZone *evt_zone);
      int DetectEvent_Entering          (TrackedObject *t_object,EventZone *evt_zone);
      int DetectEvent_Falling           (TrackedObject *t_object,EventZone *evt_zone);
      int DetectEvent_Leaving           (TrackedObject *t_object,EventZone *evt_zone);
      int DetectEvent_LineCrossing      (TrackedObject *t_object,EventZone *evt_zone);      
      int DetectEvent_Loitering         (TrackedObject *t_object,EventZone *evt_zone);
      int DetectEvent_PathPassing       (TrackedObject *t_object,EventZone *evt_zone);
      int DetectEvent_Removed           (TrackedObject *t_object,EventZone *evt_zone);
      int DetectEvent_Stopping          (TrackedObject *t_object,EventZone *evt_zone);

   protected:
      int DetectEvent_Violence  (TrackedGroup* t_group,EventZone* evt_zone,GroupTracking& grp_tracker);
      int DetectEvent_Gathering (TrackedGroup* t_group,EventZone* evt_zone,GroupTracking& grp_tracker);
   
   protected:
      int DetectEvent_CarAccident (ObjectTracking& obj_tracker,EventZone* evt_zone);

   protected:
      int DetectEvents_Abandoned         (ObjectTracking &obj_tracker,EventZone *evt_zone);
      int DetectEvents_CarParking        (ObjectTracking &obj_tracker,EventZone *evt_zone);
      int DetectEvents_CarStopping       (ObjectTracking &obj_tracker,EventZone *evt_zone);
      int DetectEvents_DirectionalMotion (ObjectTracking &obj_tracker,EventZone *evt_zone);
      int DetectEvents_Dwell             (ObjectTracking &obj_tracker,EventZone *evt_zone);
      int DetectEvents_Entering          (ObjectTracking &obj_tracker,EventZone *evt_zone);
      int DetectEvents_Falling           (ObjectTracking &obj_tracker,EventZone *evt_zone);
      int DetectEvents_Leaving           (ObjectTracking &obj_tracker,EventZone *evt_zone);
      int DetectEvents_LineCrossing      (ObjectTracking &obj_tracker,EventZone *evt_zone);
      int DetectEvents_Loitering         (ObjectTracking &obj_tracker,EventZone *evt_zone);
      int DetectEvents_PathPassing       (ObjectTracking &obj_tracker,EventZone *evt_zone);
      int DetectEvents_Removed           (ObjectTracking &obj_tracker,EventZone *evt_zone);
      int DetectEvents_Stopping          (ObjectTracking &obj_tracker,EventZone *evt_zone);

   protected:
      int DetectEvents_Violence  (GroupTracking &grp_tracker,EventZone *evt_zone);
      int DetectEvents_Gathering (GroupTracking &grp_tracker,EventZone *evt_zone);

   public:
      int CheckSchedule       (Time &c_time);
      int EstimateObjectCount (TrackedObject *t_object,EventZone *evt_zone);

   public:
      int DetectEvents          (ObjectTracking& obj_tracker,EventZone* evt_zone);
      int DetectEvents          (GroupTracking& grp_tracker,EventZone* evt_zone);
      int DetectEvent_Presence  (BlobTracking& blb_tracker,EventZone* evt_zone,float dt_time,int to_type);
      int DetectEvents_Presence (BlobTracking& blb_tracker,EventZone* evt_zone,float dt_time,float bo_time,int to_type,int flag_cont,int flag_bkg);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleList
//
///////////////////////////////////////////////////////////////////////////////

 class EventRuleList : public LinkedList<EventRule>
 
{
   public:
      void CheckDisabledEvents (   );
      void CheckSchedule       (int flag_check = TRUE);
      int  ReadFile            (FileIO& file);
      int  ReadFile            (CXMLIO* pIO);
      void ResetObjectCounters (   );
      int  WriteFile           (FileIO& file);
      int  WriteFile           (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventRuleListManager
//
///////////////////////////////////////////////////////////////////////////////

 class EventRuleListManager
 
{
   protected:
      CCriticalSection CS_List;

   public:
      struct Item {
         EventRule *ItemPtr;
         Item *Prev,*Next;
      };
      LinkedList<Item> Items;
      
   public:
      virtual ~EventRuleListManager (   );
      
   protected:
      void Lock   (   );
      void Unlock (   );
   
   public:
      void Add      (EventRule *s_item);
      int  GetNewID (   );
      void Remove   (EventRule *s_item);
};

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

EventRule*       CreateInstance_EventRule            (   );
ObjectFiltering* CreateInstance_ObjectFiltering      (   );
int              GetEventTypeName                    (int evt_type,char* d_name);
void             SetCreationFunction_EventRule       (EventRule*(*func)(   ));
void             SetCreationFunction_ObjectFiltering (ObjectFiltering*(*func)(   ));

}

#endif

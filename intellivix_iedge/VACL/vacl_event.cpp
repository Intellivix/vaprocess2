﻿#include "vacl_event.h"
#include "vacl_geometry.h"
#include "VAEngine.h"

#if defined(__DEBUG_EVD)
#include "Win32CL/Win32CL.h"
extern int _DVX_EVD,_DVY_EVD;
extern CImageView* _DebugView_EVD;
#endif

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Global Variables/Functions
//
///////////////////////////////////////////////////////////////////////////////

extern int __Flag_VACL_Initialized;
extern int __CheckLicenseKey (   );

int __DisabledEventTypes = 0;
Event* (*__CreationFunction_Event)(EventZone *evt_zone,EventRule *evt_rule,int evt_type) = NULL;

///////////////////////////////////////////////////////////////////////////////
//
// Class: Event
//
///////////////////////////////////////////////////////////////////////////////

 Event::Event (EventZone *evt_zone,EventRule *evt_rule,int evt_type)

{
   static int count = 0;

   ID          = count++;
   Status      = EV_STATUS_BEGUN;
   NumObjects  = 1;
   BonusTime   = 0.0f;
   ElapsedTime = 0.0f;
   ThumbPixFmt = VAE_PIXFMT_NONE;
   EvtRule     = evt_rule;
   EvtZone     = evt_zone;
   if (evt_type < 0) Type = EvtRule->EventType;
   else Type = evt_type;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventDetection
//
///////////////////////////////////////////////////////////////////////////////

 EventDetection::EventDetection (   )

{
   Flag_CheckSchedule = TRUE;
   BkgPeriod          = 7.0f;
}

 EventDetection::~EventDetection (   )
 
{
   Close (   );
}

 void EventDetection::_Init (   )

{
   EZ_Abandoned   = FALSE;
   EZ_CarAccident = FALSE;
   EZ_CarParking  = FALSE;
   EZ_ColorChange = FALSE;
   EZ_Removed     = FALSE;
   ObjIDGenerator = NULL;
   CurrentTime    = 0.0f;
   FrameRate      = 0.0f;
   FrameDuration  = 0.0f;
   SrcImgSize.Clear (   );
}

 int EventDetection::CheckObjectsAbandonedStart (ObjectTracking& obj_tracker,EventZone* evt_zone)

{
   int obj_type,to_evt_start,to_event,to_evtzone;

   float det_time = 0.0f;
   float max_imd  = MC_VLV; // �ʱ� �ִ� �̵� �Ÿ�
   float max_imt  = MC_VLV; // �ʱ� �ִ� �̵� �ð�
   EventRule* evt_rule = evt_zone->EvtRule;
   if (evt_rule == NULL) return (1);
   switch (evt_rule->EventType) {
      case ER_EVENT_ABANDONED: {
         obj_type     = evt_rule->ObjectType;
         to_evt_start = TO_EVENT_ABANDONED_START;
         to_event     = TO_EVENT_ABANDONED;
         to_evtzone   = TO_EVENTZONE_ABANDONED;
         det_time     = evt_rule->ERC_Abandoned.DetectionTime;
         #if !defined(__KISA_CERT)
         max_imd      = evt_rule->ERC_Abandoned.MaxInitMovDistance;
         max_imt      = evt_rule->ERC_Abandoned.MaxInitMovTime;
         #endif
      }  break;
      case ER_EVENT_CAR_ACCIDENT: {
         obj_type     = TO_TYPE_VEHICLE;
         to_evt_start = TO_EVENT_CAR_ACCIDENT_START;
         to_event     = TO_EVENT_CAR_ACCIDENT;
         to_evtzone   = TO_EVENTZONE_CAR_ACCIDENT;
         det_time     = evt_rule->ERC_CarAccident.DetectionTime;
      }  break;
      case ER_EVENT_CAR_PARKING: {
         obj_type     = TO_TYPE_VEHICLE;
         to_evt_start = TO_EVENT_CAR_PARKING_START;
         to_event     = TO_EVENT_CAR_PARKING;
         to_evtzone   = TO_EVENTZONE_CAR_PARKING;
         det_time     = evt_rule->ERC_CarParking.DetectionTime;
      }  break;
      case ER_EVENT_REMOVED: {
         obj_type     = evt_rule->ObjectType;
         to_evt_start = TO_EVENT_REMOVED_START;
         to_event     = TO_EVENT_REMOVED;
         to_evtzone   = TO_EVENTZONE_REMOVED;
         det_time     = evt_rule->ERC_Removed.DetectionTime;
         max_imd      = evt_rule->ERC_Removed.MaxInitMovDistance;
         max_imt      = evt_rule->ERC_Removed.MaxInitMovTime;
      }  break;
      default:
         return (2);
   }
   TrackedObject *t_object = obj_tracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      int flag_abandoned = FALSE;
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED)) {
         // t_object�� evt_zone ���� ������...
         if (evt_zone->CheckRegionInsideZone (*t_object) == DONE) {
            t_object->EventZones |= to_evtzone;
            // �̺�Ʈ�� ���� ���̰ų� �Ϸ�� ���°� �ƴϸ�...
            if (!(t_object->EventStatus & (to_event|TO_EVENT_ABANDONED_END))) {
               // ��ü ���͸� ����ϸ�...
               if ((t_object->Type & obj_type) && !evt_rule->ObjectFilter->Filter (t_object)) {
                  // ��ü�� �ִ� �̵� �Ÿ��� mmd_thrsld���� ������...
                  float mmd_thrsld = max_imd * 0.5f * (t_object->Width + t_object->Height);
                  if (t_object->MaxMovingDistance < mmd_thrsld) {
                     // t_object�� BkgMask �ȿ� ���� ������...
                     if (!IsInsideBkgMask (*t_object)) {
                        // t_object�� TO_STATUS_STATIC2 ���¿��� �������� "������" ��ü��� ���� �� �ִ�.
                        if (t_object->Status & TO_STATUS_STATIC2) {
                           flag_abandoned = TRUE;
                        }
                     }
                  }
               }
            }
         }
      }
      if (flag_abandoned) {
         // ��ü�� ���� to_evt_start ���°� �ƴ� ���...
         if (!(t_object->EventStatus & to_evt_start)) {
            float dc = (float)(t_object->FrameCount - t_object->InitFrameCount);
            // ��ü�� ���� ���� �� ���� max_imt �ʰ� ������ �ʾ�����...
            if (dc < max_imt * FrameRate) {
               int flag_ok = TRUE;
               #if !defined(__GS_CERT)
               if (EZ_Removed && !EZ_Abandoned && !EZ_CarAccident && !EZ_CarParking && !EZ_ColorChange) {
                  if ((t_object->Status & TO_STATUS_ABANDONED_OBJECT) && !(t_object->Status & TO_STATUS_REMOVED_OBJECT)) PutBkgMask (t_object);
               }
               if (!EZ_Removed) {
                  if (!(t_object->Status & TO_STATUS_ABANDONED_OBJECT) && (t_object->Status & TO_STATUS_REMOVED_OBJECT)) PutBkgMask (t_object);
               }
               // "������", "���� ���", "���� ����" �̺�Ʈ�� ��쿡�� t_object�� Ghost Object�̸� �ȵȴ�.
               if (to_evtzone == TO_EVENTZONE_ABANDONED || to_evtzone == TO_EVENTZONE_CAR_ACCIDENT || to_evtzone == TO_EVENTZONE_CAR_PARKING) {
                  if (t_object->Status & TO_STATUS_GHOST_OBJECT) flag_ok = FALSE;
               }
               #endif
               if (flag_ok) t_object->EventStatus |= to_evt_start;
            }
         }
      }
      else t_object->EventStatus &= ~to_evt_start;
      if (t_object->EventStatus & to_evt_start) ManageAbandonedObjects (obj_tracker,t_object,det_time,to_evt_start,to_event);
      t_object = obj_tracker.TrackedObjects.Next (t_object);
   }
   return (DONE);
}

 int EventDetection::CheckSetup (EventDetection* ed)

{
   int ret_flag = 0;

   FileIO buffThis,buffFrom;   
   CXMLIO xmlIOThis,xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis, XMLIO_Write);
   xmlIOFrom.SetFileIO (&buffFrom, XMLIO_Write);
   WriteFile (&xmlIOThis);
   ed->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom)) ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength (0);
   buffFrom.SetLength (0);
   if (ret_flag & CHECK_SETUP_RESULT_CHANGED) {
      // EventRule ����Ʈ ���� üũ. 
      if (EventRules.GetNumItems () == ed->EventRules.GetNumItems ()) 
      {
         EventRule * evt_rule_this = EventRules.First (   );
         EventRule * evt_rule_from = ed->EventRules.First (   );
         while (evt_rule_this != NULL) {
            ret_flag |= evt_rule_this->CheckSetup (evt_rule_from);
            evt_rule_this = EventRules.Next (evt_rule_this);
            evt_rule_from = ed->EventRules.Next (evt_rule_from);
         }
      }
      else ret_flag |= CHECK_SETUP_RESULT_RESTART_ALL;
      // EventZone ����Ʈ ���� üũ. 
      if (EventZones.GetNumItems () == ed->EventZones.GetNumItems ()) 
      {
         EventZone * evt_zone_this = EventZones.First (   );
         EventZone * evt_zone_from = ed->EventZones.First (   );
         while (evt_zone_this != NULL) {
            ret_flag |= evt_zone_this->CheckSetup (evt_zone_from);
            evt_zone_this = EventZones.Next (evt_zone_this);
            evt_zone_from = ed->EventZones.Next (evt_zone_from);
         }
      }
      else ret_flag |= CHECK_SETUP_RESULT_RESTART_ALL;
   }
   return (ret_flag);
}

 void EventDetection::CleanEvtGenObjectList (   )

{
   CleanTrackedObjectList (EvtGenObjects);
}

 void EventDetection::ClearEventRuleList (   )

{
   EventRules.Delete (   );
}

 void EventDetection::ClearEventZoneList (   )

{
   EventZones.Delete (   );
}

 void EventDetection::Close (   )

{
   BkgMaskMap.Delete       (   );
   AbandonedObjects.Delete (   );
   EvtGenObjects.Delete    (   );
   _Init (   );
}

 void EventDetection::FinalizeEvents (ObjectTracking& obj_tracker)
 
{
   TrackedObject *t_object = obj_tracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      if (t_object->Status & TO_STATUS_TO_BE_REMOVED) {
         Event *event = t_object->Events.First (   );
         while (event != NULL) {
            // ��ü�� �̺�Ʈ ���� �����ų� ��ü ���Ϳ� ���� �ɷ��� ��� �ڵ���� ����
            // jun: �������� ��ü�� �߻��� �̺�Ʈ�� 1���� ��쿡�� Handoff �Ѵ�. 
            if (t_object->Events.GetNumItems (   ) == 1 && 
                event->Type == ER_EVENT_LOITERING       &&
                obj_tracker.CheckObjectInNonHandoffArea (t_object) != DONE) t_object->Status |= TO_STATUS_HANDOFF;
            if (event->Status & (EV_STATUS_BEGUN|EV_STATUS_ONGOING)) {
               event->EndTime.GetLocalTime (   );
               event->Status = EV_STATUS_ENDED|EV_STATUS_TO_BE_REMOVED;
            }
            event = t_object->Events.Next (event);
         }
      }
      else {
         Event *event = t_object->Events.First (   );
         while (event != NULL) {
            Event *n_event = t_object->Events.Next (event);
            if (event->Status & EV_STATUS_TO_BE_REMOVED) {
               // ��ü�� �̺�Ʈ ���� �����ų� ��ü ���Ϳ� ���� �ɷ��� ��� �ڵ���� ����
               // jun: �������� ��ü�� �߻��� �̺�Ʈ�� 1���� ��쿡�� Handoff �Ѵ�. 
               if (t_object->Events.GetNumItems (   ) == 1 &&
                   event->Type == ER_EVENT_LOITERING       &&
                   obj_tracker.CheckObjectInNonHandoffArea (t_object) != DONE) t_object->Status |= TO_STATUS_HANDOFF;
               t_object->Events.Remove (event);
               delete event;
            }
            event = n_event;
         }
      }
      t_object = obj_tracker.TrackedObjects.Next (t_object);
   }
}

 void EventDetection::FinalizeEvents (TrackedObjectList& to_list)
 
{
   TrackedObject *t_object = to_list.First (   );
   while (t_object != NULL) {
      if (t_object->Status & TO_STATUS_TO_BE_REMOVED) {
         Event *event = t_object->Events.First (   );
         while (event != NULL) {
            if (event->Status & (EV_STATUS_ENDED|EV_STATUS_AFTER_ENDED)) event->Status = EV_STATUS_TO_BE_REMOVED;
            else {
               event->EndTime.GetLocalTime (   );
               event->Status = EV_STATUS_ENDED|EV_STATUS_TO_BE_REMOVED;
            }
            event = t_object->Events.Next (event);
         }
      }
      else {
         Event *event = t_object->Events.First (   );
         while (event != NULL) {
            Event *n_event = t_object->Events.Next (event);
            if (event->Status & EV_STATUS_TO_BE_REMOVED) {
               t_object->Events.Remove (event);
               delete event;
            }
            event = n_event;
         }
      }
      t_object = to_list.Next (t_object);
   }
}

 void EventDetection::GetBkgModelUpdateScheme (GImage& d_image)
 
{
   int x,y;
  
   if (!(EZ_Abandoned || EZ_CarAccident || EZ_CarParking || EZ_ColorChange || EZ_Removed)) return;
   for (y = 0; y < BkgMaskMap.Height; y++) {
      ushort* l_bm_map  = BkgMaskMap[y];
      byte*   l_d_image = d_image[y];
      for (x = 0; x < BkgMaskMap.Width; x++) {
         if (l_bm_map[x] > 0) {
            l_d_image[x] = FGD_BMU_SPEED_FAST;
            l_bm_map[x]--;
         }
      }
   }
}

 int EventDetection::GetNewObjectID (   )

{
   if (ObjIDGenerator == NULL) return (0);
   else return (ObjIDGenerator->Generate (   ));
}

 int EventDetection::Initialize (ISize2D& si_size,float frm_rate,ObjectIDGeneration& id_generator)

{
   #if defined(__LICENSE_MAC)
   if (__CheckLicenseKey (   )) __Flag_VACL_Initialized = FALSE;
   #endif
   if (!__Flag_VACL_Initialized) return (-1);
   Close (   );
   SrcImgSize     = si_size;
   FrameRate      = frm_rate;
   FrameDuration  = 1.0f / FrameRate;
   ObjIDGenerator = &id_generator;
   BkgMaskMap.Create (si_size.Width,si_size.Height);
   BkgMaskMap.Clear  (   );
   return (DONE);
}

 int EventDetection::IsEventZoneAvailable (int evt_type,int flag_check_enabled,int flag_only_one_type)
 
{
   int n1 = 0, n2 = 0;
   EventZone* evt_zone = EventZones.First (   );
   while (evt_zone != NULL) {
      if (flag_check_enabled) {
         if (evt_zone->IsEnabled (   )) {
            if (evt_zone->EvtRule->EventType == evt_type) n1++;
            else n2++;
         }
      }
      else {
         if (evt_zone->EvtRule->EventType == evt_type) n1++;
         else n2++;
      }
      evt_zone = EventZones.Next (evt_zone);
   }
   if (flag_only_one_type) {
      if (n1 && !n2) return (TRUE);
      else return (FALSE);
   }
   else {
      if (n1) return (TRUE);
      else return (FALSE);
   }
}

 int EventDetection::IsInsideBkgMask (IBox2D& s_box,int range)
 
{
   int x,y;

   FPoint2D cp = s_box.GetCenterPosition (   );
   int cx = int(cp.X + 0.5f);
   int cy = int(cp.Y + 0.5f);
   int sx = cx - range;
   int sy = cy - range;
   int ex = cx + range;
   int ey = cy + range;
   if (sx < 0) sx = 0;
   if (sy < 0) sy = 0;
   if (ex > SrcImgSize.Width ) ex = SrcImgSize.Width;
   if (ey > SrcImgSize.Height) ey = SrcImgSize.Height;
   for (y = sy; y < ey; y++) {
      ushort* l_bm_map = BkgMaskMap[y];
      for (x = sx; x < ex; x++)
         if (l_bm_map[x]) return (TRUE);
   }
   return (FALSE);
}

 void EventDetection::LinkEvtZonesToEvtRules (   )
 
{
   EventZone * evt_zone = EventZones.First (   );
   while (evt_zone) {
      EventRule * evt_rule = EventRules.First (   );
      while (evt_rule) {
         if (evt_zone->EvtRuleID == evt_rule->ID) {
            evt_zone->EvtRule = evt_rule;
         }
         evt_rule = EventRules.Next (evt_rule);
      }
      evt_zone = EventZones.Next (evt_zone);
   }
}

 void EventDetection::ManageAbandonedObjects (ObjectTracking& obj_tracker,TrackedObject* t_object,float det_time,int to_evt_start,int to_event)
// t_object�� (TO_STATUS_STATIC2 && to_evt_start) �����̴�.
{
   float ar1,ar2;

   // t_object�� �ٿ�� �ڽ��� (left,top,right,bottom) �� �� �߽� ���� ���Ѵ�.
   IPoint2D to_ltp,to_rbp,to_cp;
   t_object->GetClippedBoundingBox (SrcImgSize.Width,SrcImgSize.Height,to_ltp,to_rbp,to_cp);
   // t_object�� ��ġ�Ǵ� a_object�� ã�´�.
   AbandonedObject* a_object = AbandonedObjects.First (   );
   while (a_object != NULL) {
      if (a_object->IsInside (to_cp)) {
         int flag_matched = FALSE;
         // �ϴ� a_object�� t_object�� ID�� �����ϸ� a_object�� t_object�� ��ġ�ȴ�.
         if (a_object->ID == t_object->ID) flag_matched = TRUE;
         else {
            // a_object�� t_object�� ������ �����ϰ�, a_object�� t_object�� ����� ���� ���� �ִ� �����̸� a_object�� t_object�� ��ġ�ȴ�.
            IPoint2D or_ltp,or_rbp;
            a_object->GetOverlappedRegion (to_ltp,to_rbp,or_ltp,or_rbp);
            int a1 = (a_object->EX - a_object->SX + 1) * (a_object->EY - a_object->SY + 1); // a_object�� ����
            int a2 = (to_rbp.X - to_ltp.X + 1) * (to_rbp.Y - to_ltp.Y + 1);                 // t_object�� ����
            int a3 = (or_rbp.X - or_ltp.X + 1) * (or_rbp.Y - or_ltp.Y + 1);                 // a_object�� t_object ������ ��ġ�� ������ ����
            if (a1 < a2) ar1 = (float)a3 / a1, ar2 = (float)a1 / a2;                        // ar1: a_object�� t_object ������ ��ģ ���� ����
            else ar1 = (float)a3 / a2, ar2 = (float)a2 / a1;                                // ar2: a_object�� t_object ������ ������ ���絵 ����
            if (ar1 >= 0.75f && ar2 >= 0.75f) flag_matched = TRUE;
         }
         if (flag_matched) {
            a_object->Flag_Matched = TRUE;
            a_object->ID           = t_object->ID;
            a_object->UpdateRegion (to_ltp,to_rbp);
            if ((CurrentTime - a_object->StartTime >= det_time) && !(t_object->EventStatus & to_event)) {
               #if defined(__GS_CERT)
               int flag_ok = TRUE;
               #else
               int flag_ok = FALSE;
               switch (to_event) {
                  case TO_EVENT_ABANDONED: {
                     if (t_object->Status & TO_STATUS_ABANDONED_OBJECT) {
                        if (t_object->Count_Abandoned >= t_object->Count_Removed) flag_ok = TRUE;
                     }
                  }  break;
                  case TO_EVENT_CAR_ACCIDENT:
                  case TO_EVENT_CAR_PARKING: {
                     if (!(t_object->Status & TO_STATUS_GHOST_OBJECT)) flag_ok = TRUE;
                  }  break;
                  case TO_EVENT_REMOVED: {
                     if (t_object->Status & TO_STATUS_REMOVED_OBJECT) {
                        if (t_object->Count_Removed >= t_object->Count_Abandoned) flag_ok = TRUE;
                     }
                  }  break;
                  default:
                     break;
               }
               #endif
               if (flag_ok) {
                  // ��ü�� �̺�Ʈ ���� �ð��� ���������, t_object�� �̺�Ʈ ���¸� to_evt_start���� to_event�� �ٲ۴�.
                  t_object->EventStatus  &= ~to_evt_start;
                  t_object->EventStatus  |=  to_event;
                  if (!(t_object->Status & TO_STATUS_VERIFIED)) obj_tracker.SetObjectVerified (t_object);
               }
            }
            return;
         }
      }
      a_object = AbandonedObjects.Next (a_object);
   }
   // t_object�� �����ϴ� a_object�� ������ t_object�� �����ϴ� a_object�� �����Ͽ� ��Ͽ� ����Ѵ�.
   a_object = new AbandonedObject;
   a_object->SetRegion (to_ltp,to_rbp);
   a_object->ID = t_object->ID;
   a_object->StartTime = CurrentTime;
   AbandonedObjects.Add (a_object);
}

 int EventDetection::Perform (ObjectTracking& obj_tracker)

{
   #if defined(__DEBUG_EVD)
   _DVX_EVD = _DVY_EVD = 0;
   _DebugView_EVD->Clear (   );
   #endif
   if (!__Flag_VACL_Initialized) return (-1);
   Shift = obj_tracker.Shift;
   CurrentTime += FrameDuration;
   // �̺�Ʈ ���� �������� üũ�Ͽ�, �۵� �ð� ���� �̺�Ʈ ���⸸ �����ϵ��� �����.
   EventRules.CheckSchedule (Flag_CheckSchedule);
   // �̺�Ʈ ������ ������ ��Ȱ�� ���θ� üũ�Ͽ�, ���������� ��Ȱ��ȭ�� �̺�Ʈ ������ �������� �ʵ��� �����.
   EventRules.CheckDisabledEvents (   );
   // ���� ��ü�� �ش��ϴ� �̺�Ʈ���� �����Ų��.
   FinalizeEvents (obj_tracker);
   FinalizeEvents (EvtGenObjects);
   if (!obj_tracker.IsEnabled (   )) return (0);
   int n_objects = 0;
   // TrackedObject�� EventStatus �� EventZones ���� �缳���Ѵ�.
   TrackedObjectList &to_list = obj_tracker.TrackedObjects;
   TrackedObject* t_object = to_list.First (   );
   while (t_object != NULL) {
      if (!(t_object->EventStatus & TO_EVENT_CAR_STOPPING      )) t_object->Time_CarStopping = 0.0f;
      if (!(t_object->EventStatus & TO_EVENT_DIRECTIONAL_MOTION)) t_object->Time_DirecMotion = 0.0f;
      if (!(t_object->EventStatus & TO_EVENT_DWELL             )) t_object->Time_Dwell       = 0.0f;
      if (!(t_object->EventStatus & TO_EVENT_LOITERING         )) t_object->Time_Loitering   = 0.0f;
      if (!(t_object->EventStatus & TO_EVENT_STOPPING          )) t_object->Time_Stopping    = 0.0f;
      t_object->EventStatus &= ~(
         TO_EVENT_CAR_STOPPING|TO_EVENT_DIRECTIONAL_MOTION|TO_EVENT_DWELL|TO_EVENT_GATHERING|TO_EVENT_LOITERING|TO_EVENT_STOPPING
      );
      t_object->EventZones = 0;
      t_object = to_list.Next (t_object);
   }
   // �̺�Ʈ �� ���� �̺�Ʈ�� �����Ѵ�.
   int flag_abandoned = FALSE;
   EZ_Abandoned   = IsEventZoneAvailable (ER_EVENT_ABANDONED,TRUE);
   EZ_CarAccident = IsEventZoneAvailable (ER_EVENT_CAR_ACCIDENT,TRUE);
   EZ_CarParking  = IsEventZoneAvailable (ER_EVENT_CAR_PARKING,TRUE);
   EZ_ColorChange = IsEventZoneAvailable (ER_EVENT_COLOR_CHANGE,TRUE);
   EZ_Removed     = IsEventZoneAvailable (ER_EVENT_REMOVED,TRUE);
   EventZone *evt_zone = EventZones.First (   );
   while (evt_zone != NULL) {
      if (evt_zone->IsEnabled (   )) {
         int evt_type = evt_zone->EvtRule->EventType;
         if (evt_type == ER_EVENT_ABANDONED    || 
             evt_type == ER_EVENT_CAR_ACCIDENT ||
             evt_type == ER_EVENT_CAR_PARKING  ||
             evt_type == ER_EVENT_REMOVED) {
            CheckObjectsAbandonedStart (obj_tracker,evt_zone);
            flag_abandoned = TRUE;
         }
         n_objects += evt_zone->DetectEvents (obj_tracker);
      }
      evt_zone = EventZones.Next (evt_zone);
   }
   // AbandonedObject���� �����Ѵ�.
   if (flag_abandoned) AbandonedObjects.UpdateObjectScores (FrameDuration);
   #if defined(__DEBUG_EVD)
   _DebugView_EVD->Invalidate (   );
   #endif
   return (n_objects);
}

 int EventDetection::Perform (ColorChangeDetection& clc_detector)
 
{
   if (!clc_detector.IsEnabled (   )) return (0);
   int n_objects = 0;
   clc_detector.ClearBlobEventStatus (   );
   EventZone *evt_zone = EventZones.First (   );
   while (evt_zone != NULL) {
      if (evt_zone->IsEnabled (   )) n_objects += evt_zone->DetectEvents (clc_detector);
      evt_zone = EventZones.Next (evt_zone);
   }
   return (n_objects);
}

 int EventDetection::Perform (CrowdDensityEstimation& crd_estimator)

{
   if (!crd_estimator.IsEnabled (   )) return (0);
   int n_objects = 0;
   CrowdZone* zone = crd_estimator.CrowdZones.First (   );
   while (zone != NULL) {
      if (zone->EvtZone->IsEnabled (   )) n_objects += zone->DetectEvent (   );
      zone = crd_estimator.CrowdZones.Next (zone);
   }
   return (n_objects);
}

 int EventDetection::Perform (DwellAnalysis& dwl_analyzer)
 
{
   if (!dwl_analyzer.IsEnabled (   )) return (0);
   int n_objects = 0;
   DwellZone* zone = dwl_analyzer.DwellZones.First (   );
   while (zone != NULL) {
      if (zone->EvtZone->IsEnabled (   )) n_objects += zone->DetectEvent (   );
      zone = dwl_analyzer.DwellZones.Next (zone);
   }
   return (n_objects);
}

 int EventDetection::Perform (FlameDetection& flm_detector)
 
{
   if (!flm_detector.IsEnabled (   )) return (0);
   int n_objects = 0;
   flm_detector.ClearBlobEventStatus (   );
   EventZone *evt_zone = EventZones.First (   );
   while (evt_zone != NULL) {
      if (evt_zone->IsEnabled (   )) n_objects += evt_zone->DetectEvents (flm_detector);
      evt_zone = EventZones.Next (evt_zone);
   }
   return (n_objects);
}

 int EventDetection::Perform (GroupTracking& grp_tracker)
 
{
   if (!grp_tracker.IsEnabled (   )) return (0);
   int n_objects = 0;
   grp_tracker.ClearBlobEventStatus (   );   
   EventZone *evt_zone = EventZones.First (   );
   while (evt_zone != NULL) {
      if (evt_zone->IsEnabled (   )) n_objects += evt_zone->DetectEvents (grp_tracker);
      evt_zone = EventZones.Next (evt_zone);
   }
   return (n_objects);
}

 int EventDetection::Perform (SmokeDetection& smk_detector)
 
{
   if (!smk_detector.IsEnabled (   )) return (0);
   int n_objects = 0;
   smk_detector.ClearBlobEventStatus (   );
   EventZone *evt_zone = EventZones.First (   );
   while (evt_zone != NULL) {
      if (evt_zone->IsEnabled (   )) n_objects += evt_zone->DetectEvents (smk_detector);
      evt_zone = EventZones.Next (evt_zone);
   }
   return (n_objects);
}

 int EventDetection::Perform (TrafficCongestionDetection& trc_detector)
 
{
   if (!trc_detector.IsEnabled (   )) return (0);
   int n_objects = 0;
   TrafficZone* zone = trc_detector.TrafficZones.First (   );
   while (zone != NULL) {
      if (zone->EvtZone->IsEnabled (   )) n_objects += zone->DetectEvent (   );
      zone = trc_detector.TrafficZones.Next (zone);
   }
   return (n_objects);
}

 int EventDetection::Perform (WaterLevelDetection& wtl_detector)
 
{
   if (!wtl_detector.IsEnabled (   )) return (0);
   int n_objects = 0;
   WaterZone* zone = wtl_detector.WaterZones.First (   );
   while (zone != NULL) {
      if (zone->EvtZone->IsEnabled (   )) n_objects += zone->DetectEvent (   );
      zone = wtl_detector.WaterZones.Next (zone);
   }
   return (n_objects);
}

 int EventDetection::Perform (ZoneColorDetection& znc_detector)
 
{
   if (!znc_detector.IsEnabled (   )) return (0);
   int n_objects = 0;
   ColorZone* zone = znc_detector.ColorZones.First (   );
   while (zone != NULL) {
      if (zone->EvtZone->IsEnabled (   )) n_objects += zone->DetectEvent (   );
      zone = znc_detector.ColorZones.Next (zone);
   }
   return (n_objects);
}

 void EventDetection::PutBkgMask (TrackedObject* t_object,int shift)
 
{
   int x = t_object->X;
   int y = t_object->Y;
   ushort m_value = (ushort)(FrameRate * BkgPeriod);
   t_object->PutMask (x,y,m_value,BkgMaskMap);
   if (shift) {
      t_object->PutMask (x - shift,y,m_value,BkgMaskMap);
      t_object->PutMask (x + shift,y,m_value,BkgMaskMap);
      t_object->PutMask (x,y - shift,m_value,BkgMaskMap);
      t_object->PutMask (x,y + shift,m_value,BkgMaskMap);
   }
}

 void EventDetection::PutBkgMask (TrackedBlob* t_blob,BlobTracking& blb_tracker)
 
{
   ushort m_value = (ushort)(FrameRate * BkgPeriod);
   blb_tracker.PutMask (t_blob,m_value,BkgMaskMap);
}

 void EventDetection::PutRectBkgMask (TrackedBlob* t_blob)

{
   ushort m_value = (ushort)(FrameRate * BkgPeriod);
   t_blob->PutRectMask (m_value,BkgMaskMap,0);
}

 int EventDetection::ReadFile (FileIO &file)
 
{
   if (EventRules.ReadFile (file)) return (1);
   if (EventZones.ReadFile (file)) return (2);
   LinkEvtZonesToEvtRules (   );
   return (DONE);
}

 int EventDetection::ReadFile (CXMLIO* pIO)

{  
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("EventDetection"))
   {
      EventRules.ReadFile (pIO);
      EventZones.ReadFile (pIO,"EventZoneList");
      LinkEvtZonesToEvtRules (   );
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}

 void EventDetection::Reset (   )

{
   InvalidateAllTrkObjects (EvtGenObjects);
}

 void EventDetection::ResetObjectCounters (   )
 
{
   EventRules.ResetObjectCounters (   );
}

 void EventDetection::UpdateAreaMaps (ISize2D& ri_size)
 
{
   FPoint2D scale;
   scale.X = (float)ri_size.Width  / SrcImgSize.Width;
   scale.Y = (float)ri_size.Height / SrcImgSize.Height;
   EventZones.SetRefImageSize (ri_size);
   EventZone *evt_zone = EventZones.First (   );
   while (evt_zone != NULL) {
      evt_zone->RefImgSize = ri_size;
      evt_zone->UpdateAreaMap (scale);
      evt_zone = EventZones.Next (evt_zone);
   }
   EventRule *evt_rule = EventRules.First (   );
   while (evt_rule != NULL) {
      evt_rule->EventDetector = this;
      if (evt_rule->EventType == ER_EVENT_PATH_PASSING)
         evt_rule->ERC_PathPassing.UpdatePathZonePtrs (   );
      evt_rule = EventRules.Next (evt_rule);
   }
}

 void EventDetection::UpdateEventThumbnails (TrackedObjectList &to_list,byte *si_buffer,ISize2D &si_size,int pix_fmt,float cbs_ratio,ISize2D *tn_size)
 
{
   FPoint2D scale;
   scale.X = (float)si_size.Width  / SrcImgSize.Width;
   scale.Y = (float)si_size.Height / SrcImgSize.Height;
   TrackedObject *t_object = to_list.First (   );
   while (t_object != NULL) {
      Event *event = t_object->Events.First (   );
      while (event != NULL) {
         if (event->Status & EV_STATUS_BEGUN) {
            IBox2D so_box = t_object->Scale (scale);
            event->ThumbPixFmt = pix_fmt;
            CaptureThumbnail (si_buffer,si_size,pix_fmt,so_box,event->ThumbBuffer,event->ThumbSize,cbs_ratio,tn_size);
         }
         event = t_object->Events.Next (event);
      }
      t_object = to_list.Next (t_object);
   }
}

 void EventDetection::UpdateEventThumbnails (byte *si_buffer,ISize2D &si_size,int pix_fmt,float cbs_ratio,ISize2D *tn_size)

{
   UpdateEventThumbnails (EvtGenObjects,si_buffer,si_size,pix_fmt,cbs_ratio,tn_size);   
}

 void EventDetection::UpdateEventThumbnails (ObjectTracking& obj_tracker,byte *si_buffer,ISize2D &si_size,int pix_fmt,float cbs_ratio,ISize2D *tn_size)

{
   UpdateEventThumbnails (obj_tracker.TrackedObjects,si_buffer,si_size,pix_fmt,cbs_ratio,tn_size);
   UpdateEventThumbnails (obj_tracker.LostObjects   ,si_buffer,si_size,pix_fmt,cbs_ratio,tn_size);
}

 int EventDetection::WriteFile (FileIO &file)
 
{
   if (EventRules.WriteFile (file)) return (1);
   if (EventZones.WriteFile (file)) return (2);
   return (DONE);
}

 int EventDetection::WriteFile (CXMLIO* pIO)

{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("EventDetection"))
   {
      EventRules.WriteFile (pIO);
      EventZones.WriteFile (pIO,"EventZoneList");
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: AlarmTriggerHandling
//
///////////////////////////////////////////////////////////////////////////////

 AlarmTriggerHandling::AlarmTriggerHandling (   )

{
   AlarmDuration = 1.0f;
   EvtIgnPeriod  = 2.0f;
   Initialize (   );
}

 void AlarmTriggerHandling::Initialize (   )

{
   Flag_Alarm  = FALSE;
   Flag_Event  = FALSE;
   CurrentTime = 0.0f;
   LstEvtTime  = 0.0f;
   AlmElpTime  = 0.0f;
}

 int AlarmTriggerHandling::Perform (ObjectTracking& obj_tracker,TrackedObject** p_t_object)

{
   CurrentTime += obj_tracker.FrameDuration;
   int flag_evt_start = FALSE;
   int flag_no_event  = TRUE;
   *p_t_object        = NULL;
   TrackedObject* t_object = obj_tracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      Event* event = t_object->Events.First (   );
      while (event != NULL) {
         if (event->Status == EV_STATUS_BEGUN) {
            flag_evt_start = TRUE;
            flag_no_event  = FALSE;
            *p_t_object    = t_object;
         }
         else if (event->Status == EV_STATUS_ONGOING) flag_no_event = FALSE;
         event = t_object->Events.Next (event);
      }
      t_object = obj_tracker.TrackedObjects.Next (t_object);
   }
   int r_code = 0;
   if (flag_evt_start) {
      if (!Flag_Event) {
         float d_time = CurrentTime - LstEvtTime;
         if (!LstEvtTime || d_time > EvtIgnPeriod) {
            Flag_Event = TRUE;
            Flag_Alarm = TRUE;
            AlmElpTime = 0.0f;
            r_code     = 1;
         }
      }
   }
   if (flag_no_event) {
      if (Flag_Event) {
         Flag_Event = FALSE;
         LstEvtTime = CurrentTime;
      }
      if (Flag_Alarm) {
         Flag_Alarm = FALSE;
         r_code     = -1;
      }
   }
   if (Flag_Alarm) {
      AlmElpTime += obj_tracker.FrameDuration;
      if (AlmElpTime >= AlarmDuration) {
         Flag_Alarm = FALSE;
         r_code     = -1;
      }
   }
   return (r_code);
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 Event* CreateInstance_Event (EventZone *evt_zone,EventRule *evt_rule,int evt_type)

{
   if (__CreationFunction_Event == NULL) {
      Event *event = new Event(evt_zone,evt_rule,evt_type);
      return (event);
   }
   else return (__CreationFunction_Event (evt_zone,evt_rule,evt_type));
}

 void SetCreationFunction_Event (Event*(*func)(EventZone *evt_zone,EventRule *evt_rule,int evt_type))

{
   __CreationFunction_Event = func;
}

 void SetDisabledEventType (int event_type)

{
   __DisabledEventTypes |= (0x00000001 << event_type);
}

}

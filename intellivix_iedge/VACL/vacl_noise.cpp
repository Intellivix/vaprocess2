#include "vacl_noise.h"

 namespace VACL

{

#define MAX_RAND_VALUE   0x7FFF

time_t GaussianNoise::Seed = 0;

 GaussianNoise::GaussianNoise (float sd)

{
   Create (sd);
}

 void GaussianNoise::Create (float sd)

{
   int i,index;
   float x,dx,sum,scale;

   Table.Create (MAX_RAND_VALUE);
   FArray1D g_cdf(MAX_RAND_VALUE);
   x = -4 * sd;
   scale = sd * 8.0f / MAX_RAND_VALUE;
   for (i = 0,sum = 0.0f; i < MAX_RAND_VALUE; i++,x += scale) {
      sum += GaussianFunction1D (x,sd);
      g_cdf[i] = sum;
   }
   for (i = 0; i < MAX_RAND_VALUE; i++) g_cdf[i] /= sum;
   g_cdf[MAX_RAND_VALUE - 1] = 1.0f;
   dx = 1.0f / MAX_RAND_VALUE;
   for (i = index = 0,sum = 0.0f; i < MAX_RAND_VALUE; i++) {
      sum += dx;
      for (   ;   ;   ) {
         if (sum <= g_cdf[index]) {
            Table[i] = (index - MAX_RAND_VALUE / 2) * scale;
            break;
         }
         else index++;
      }
   }
   if (Seed > 0) Seed++;
   else time (&Seed);
   srand ((uint)Seed);
}

 void GaussianNoise::Delete (   )

{
   Table.Delete (   );
}

 float GaussianNoise::Generate (   )

{
   return (Table[rand (   ) % MAX_RAND_VALUE]);
}

 float GaussianFunction1D (float x,float sd)

{
   float value;

   value = x / sd;
   return ((float)exp ((double)(-0.5 * value * value)) / (sd * MC_R_2_PI));
}

}

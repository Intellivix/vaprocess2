#include "vacl_hough.h"

 namespace VACL

{

#if defined(__LIB_OPENCV)

#define hough_cmp_gt(l1,l2) (aux[l1] > aux[l2])
static CV_IMPLEMENT_QSORT_EX(icvHoughSortDescent32s,int,hough_cmp_gt,const int*)
const int STORAGE_SIZE = 1 << 12;

 int OpenCV_DetectBrightCircles_HCT (GImage &s_image,GImage &e_image,float dp,float min_dist,int acc_threshold,int min_radius,int max_radius,FP3DArray1D &d_array)
 
{
    const int SHIFT = 10;
    const int ONE = 1 << SHIFT;
    const int circles_max = 1000;

    cv::Ptr<CvMat> dx, dy;
    cv::Ptr<CvMat> accum, dist_buf;
    std::vector<int> sort_buf;
    cv::Ptr<CvMemStorage> storage;

    int x, y, i, j, k;
    int center_count, nz_count;
    float min_radius2 = (float)min_radius*min_radius;
    float max_radius2 = (float)max_radius*max_radius;
    int rows, cols, arows, acols;
    int astep, *adata;
    float* ddata;
    CvSeq *nz, *centers;
    float idp, dr;
    CvSeqReader reader;

    Ptr<CvMemStorage> circle_storage = cvCreateMemStorage (STORAGE_SIZE);
    CvSeq *circles = cvCreateSeq (CV_32FC3,sizeof(CvSeq),sizeof(float)*3,circle_storage);

    CvMat s_img,e_img;
    OpenCV_InitCvMat (s_image,s_img);
    OpenCV_InitCvMat (e_image,e_img);
    CvMat *img   = &s_img;
    CvMat *edges = &e_img;

    dx = cvCreateMat( img->rows, img->cols, CV_16SC1 );
    dy = cvCreateMat( img->rows, img->cols, CV_16SC1 );
    cvSobel( img, dx, 1, 0, 3 );
    cvSobel( img, dy, 0, 1, 3 );

    if( dp < 1.f ) dp = 1.f;
    idp = 1.f/dp;
    accum = cvCreateMat( cvCeil(img->rows*idp)+2, cvCeil(img->cols*idp)+2, CV_32SC1 );
    cvZero(accum);

    storage = cvCreateMemStorage();
    nz = cvCreateSeq( CV_32SC2, sizeof(CvSeq), sizeof(CvPoint), storage );
    centers = cvCreateSeq( CV_32SC1, sizeof(CvSeq), sizeof(int), storage );

    rows  = img->rows - 1;
    cols  = img->cols - 1;
    arows = accum->rows - 2;
    acols = accum->cols - 2;
    adata = accum->data.i;
    astep = accum->step/sizeof(adata[0]);
    // Accumulate circle evidence for each edge pixel
    for( y = 1; y < rows; y++ )
    {
        const uchar* edges_row = edges->data.ptr + y*edges->step;
        const short* dx_row = (const short*)(dx->data.ptr + y*dx->step);
        const short* dy_row = (const short*)(dy->data.ptr + y*dy->step);

        for( x = 1; x < cols; x++ )
        {
            float vx = dx_row[x];
            float vy = dy_row[x];

            if( !edges_row[x] || (vx == 0 && vy == 0) ) continue;

            float mag = sqrt(vx*vx+vy*vy);
            assert( mag >= 1 );

            int x0 = cvRound((x*idp)*ONE);
            int y0 = cvRound((y*idp)*ONE);

            // 어두운 방향으로 voting하는 경우
            int sx = cvRound((vx*idp)*ONE/mag);
            int sy = cvRound((vy*idp)*ONE/mag);
            int x1 = x0 + min_radius * sx;
            int y1 = y0 + min_radius * sy;
            for(int r = min_radius; r <= max_radius; x1 += sx, y1 += sy, r++)
            {
               int x2 = x1 >> SHIFT, y2 = y1 >> SHIFT;
               if (x2 < 1 || y2 < 1 || x2 >= acols || y2 >= arows) break;
               int pos = y2*astep + x2;
               adata[pos]++;
               adata[pos - 1]++;
               adata[pos + 1]++;
               adata[pos - astep]++;
               adata[pos + astep]++;
            }
            
            CvPoint pt;
            pt.x = x; pt.y = y;
            cvSeqPush( nz, &pt );
        }
    }

    nz_count = nz->total;
    if (!nz_count) {
       d_array.Delete (   );
       return (0);
    }
    
    //Find possible circle centers
    for( y = 1; y < arows - 1; y++ )
    {
        for( x = 1; x < acols - 1; x++ )
        {
            int base = y*(acols+2) + x;
            if( adata[base] > acc_threshold &&
                adata[base] > adata[base-1] && adata[base] > adata[base+1] &&
                adata[base] > adata[base-acols-2] && adata[base] > adata[base+acols+2] )
                cvSeqPush(centers, &base);
        }
    }

    center_count = centers->total;
    if (!center_count) {
       d_array.Delete (   );
       return (0);
    }

    sort_buf.resize( MAX(center_count,nz_count) );
    cvCvtSeqToArray( centers, &sort_buf[0] );

    icvHoughSortDescent32s( &sort_buf[0], center_count, adata );
    cvClearSeq( centers );
    cvSeqPushMulti( centers, &sort_buf[0], center_count );

    dist_buf = cvCreateMat( 1, nz_count, CV_32FC1 );
    ddata = dist_buf->data.fl;

    dr = dp;
    min_dist = MAX( min_dist, dp );
    min_dist *= min_dist;
    // For each found possible center
    // Estimate radius and check support
    for( i = 0; i < centers->total; i++ )
    {
        int ofs = *(int*)cvGetSeqElem( centers, i );
        y = ofs/(acols+2);
        x = ofs - (y)*(acols+2);
        //Calculate circle's center in pixels
        float cx = (float)((x + 0.5f)*dp), cy = (float)(( y + 0.5f )*dp);
        float start_dist, dist_sum;
        float r_best = 0;
        int max_count = 0;
        // Check distance with previously detected circles
        for( j = 0; j < circles->total; j++ )
        {
            float* c = (float*)cvGetSeqElem( circles, j );
            if( (c[0] - cx)*(c[0] - cx) + (c[1] - cy)*(c[1] - cy) < min_dist )
                break;
        }

        if( j < circles->total )
            continue;
        // Estimate best radius
        cvStartReadSeq( nz, &reader );
        for( j = k = 0; j < nz_count; j++ )
        {
            CvPoint pt;
            float _dx, _dy, _r2;
            CV_READ_SEQ_ELEM( pt, reader );
            _dx = cx - pt.x; _dy = cy - pt.y;
            _r2 = _dx*_dx + _dy*_dy;
            if(min_radius2 <= _r2 && _r2 <= max_radius2 )
            {
                ddata[k] = _r2;
                sort_buf[k] = k;
                k++;
            }
        }

        int nz_count1 = k, start_idx = nz_count1 - 1;
        if( nz_count1 == 0 )
            continue;
        dist_buf->cols = nz_count1;
        cvPow( dist_buf, dist_buf, 0.5 );
        icvHoughSortDescent32s( &sort_buf[0], nz_count1, (int*)ddata );

        dist_sum = start_dist = ddata[sort_buf[nz_count1-1]];
        for( j = nz_count1 - 2; j >= 0; j-- )
        {
            float d = ddata[sort_buf[j]];

            if( d > max_radius )
                break;

            if( d - start_dist > dr )
            {
                float r_cur = ddata[sort_buf[(j + start_idx)/2]];
                if( (start_idx - j)*r_best >= max_count*r_cur ||
                    (r_best < FLT_EPSILON && start_idx - j >= max_count) )
                {
                    r_best = r_cur;
                    max_count = start_idx - j;
                }
                start_dist = d;
                start_idx = j;
                dist_sum = 0;
            }
            dist_sum += d;
        }
        // Check if the circle has enough support
        if( max_count > acc_threshold )
        {
            float c[3];
            c[0] = cx;
            c[1] = cy;
            c[2] = (float)r_best;
            cvSeqPush( circles, c );
            if( circles->total > circles_max) break;
        }
    }
    
    if (circles->total > 0) {
       d_array.Create  (circles->total);
       cvCvtSeqToArray (circles,(FPoint3D*)d_array);
    }
    else d_array.Delete (   );
    
    return (d_array.Length);
}

 int OpenCV_DetectCircles_HCT (GImage &s_image,GImage &e_image,float dp,float min_dist,int acc_threshold,int min_radius,int max_radius,FP3DArray1D &d_array)
 
{
    const int SHIFT = 10;
    const int ONE = 1 << SHIFT;
    const int circles_max = 1000;
    
    cv::Ptr<CvMat> dx, dy;
    cv::Ptr<CvMat> accum, dist_buf;
    std::vector<int> sort_buf;
    cv::Ptr<CvMemStorage> storage;

    int x, y, i, j, k;
    int center_count, nz_count;
    float min_radius2 = (float)min_radius*min_radius;
    float max_radius2 = (float)max_radius*max_radius;
    int rows, cols, arows, acols;
    int astep, *adata;
    float* ddata;
    CvSeq *nz, *centers;
    float idp, dr;
    CvSeqReader reader;

    Ptr<CvMemStorage> circle_storage = cvCreateMemStorage (STORAGE_SIZE);
    CvSeq *circles = cvCreateSeq (CV_32FC3,sizeof(CvSeq),sizeof(float)*3,circle_storage);

    CvMat s_img,e_img;
    OpenCV_InitCvMat (s_image,s_img);
    OpenCV_InitCvMat (e_image,e_img);
    CvMat *img   = &s_img;
    CvMat *edges = &e_img;

    dx = cvCreateMat( img->rows, img->cols, CV_16SC1 );
    dy = cvCreateMat( img->rows, img->cols, CV_16SC1 );
    cvSobel( img, dx, 1, 0, 3 );
    cvSobel( img, dy, 0, 1, 3 );

    if( dp < 1.f ) dp = 1.f;
    idp = 1.f/dp;
    accum = cvCreateMat( cvCeil(img->rows*idp)+2, cvCeil(img->cols*idp)+2, CV_32SC1 );
    cvZero(accum);

    storage = cvCreateMemStorage();
    nz = cvCreateSeq( CV_32SC2, sizeof(CvSeq), sizeof(CvPoint), storage );
    centers = cvCreateSeq( CV_32SC1, sizeof(CvSeq), sizeof(int), storage );

    rows  = img->rows - 1;
    cols  = img->cols - 1;
    arows = accum->rows - 2;
    acols = accum->cols - 2;
    adata = accum->data.i;
    astep = accum->step/sizeof(adata[0]);
    // Accumulate circle evidence for each edge pixel
    for( y = 0; y < rows; y++ )
    {
        const uchar* edges_row = edges->data.ptr + y*edges->step;
        const short* dx_row = (const short*)(dx->data.ptr + y*dx->step);
        const short* dy_row = (const short*)(dy->data.ptr + y*dy->step);

        for( x = 0; x < cols; x++ )
        {
            float vx, vy;
            int sx, sy, x0, y0, x1, y1, r;
            CvPoint pt;

            vx = dx_row[x];
            vy = dy_row[x];

            if( !edges_row[x] || (vx == 0 && vy == 0) )
                continue;

            float mag = sqrt(vx*vx+vy*vy);
            assert( mag >= 1 );
            sx = cvRound((vx*idp)*ONE/mag);
            sy = cvRound((vy*idp)*ONE/mag);

            x0 = cvRound((x*idp)*ONE);
            y0 = cvRound((y*idp)*ONE);
            // Step rom min_radius to max_radius in both directions of the gradient
            for(int k1 = 0; k1 < 2; k1++)
            {
               x1 = x0 + min_radius * sx;
               y1 = y0 + min_radius * sy;
               for( r = min_radius; r <= max_radius; x1 += sx, y1 += sy, r++ )
               {
                  int x2 = x1 >> SHIFT, y2 = y1 >> SHIFT;
                  if (x2 < 1 || y2 < 1 || x2 >= acols || y2 >= arows) break;
                  int pos = y2*astep + x2;
                  adata[pos]++;
                  adata[pos - 1]++;
                  adata[pos + 1]++;
                  adata[pos - astep]++;
                  adata[pos + astep]++;
               }
               sx = -sx; sy = -sy;
            }
            pt.x = x; pt.y = y;
            cvSeqPush( nz, &pt );
        }
    }

    nz_count = nz->total;
    if (!nz_count) {
       d_array.Delete (   );
       return (0);
    }
    
    //Find possible circle centers
    for( y = 1; y < arows - 1; y++ )
    {
        for( x = 1; x < acols - 1; x++ )
        {
            int base = y*(acols+2) + x;
            if( adata[base] > acc_threshold &&
                adata[base] > adata[base-1] && adata[base] > adata[base+1] &&
                adata[base] > adata[base-acols-2] && adata[base] > adata[base+acols+2] )
                cvSeqPush(centers, &base);
        }
    }

    center_count = centers->total;
    if (!center_count) {
       d_array.Delete (   );
       return (0);
    }

    sort_buf.resize( MAX(center_count,nz_count) );
    cvCvtSeqToArray( centers, &sort_buf[0] );

    icvHoughSortDescent32s( &sort_buf[0], center_count, adata );
    cvClearSeq( centers );
    cvSeqPushMulti( centers, &sort_buf[0], center_count );

    dist_buf = cvCreateMat( 1, nz_count, CV_32FC1 );
    ddata = dist_buf->data.fl;

    dr = dp;
    min_dist = MAX( min_dist, dp );
    min_dist *= min_dist;
    // For each found possible center
    // Estimate radius and check support
    for( i = 0; i < centers->total; i++ )
    {
        int ofs = *(int*)cvGetSeqElem( centers, i );
        y = ofs/(acols+2);
        x = ofs - (y)*(acols+2);
        //Calculate circle's center in pixels
        float cx = (float)((x + 0.5f)*dp), cy = (float)(( y + 0.5f )*dp);
        float start_dist, dist_sum;
        float r_best = 0;
        int max_count = 0;
        // Check distance with previously detected circles
        for( j = 0; j < circles->total; j++ )
        {
            float* c = (float*)cvGetSeqElem( circles, j );
            if( (c[0] - cx)*(c[0] - cx) + (c[1] - cy)*(c[1] - cy) < min_dist )
                break;
        }

        if( j < circles->total )
            continue;
        // Estimate best radius
        cvStartReadSeq( nz, &reader );
        for( j = k = 0; j < nz_count; j++ )
        {
            CvPoint pt;
            float _dx, _dy, _r2;
            CV_READ_SEQ_ELEM( pt, reader );
            _dx = cx - pt.x; _dy = cy - pt.y;
            _r2 = _dx*_dx + _dy*_dy;
            if(min_radius2 <= _r2 && _r2 <= max_radius2 )
            {
                ddata[k] = _r2;
                sort_buf[k] = k;
                k++;
            }
        }

        int nz_count1 = k, start_idx = nz_count1 - 1;
        if( nz_count1 == 0 )
            continue;
        dist_buf->cols = nz_count1;
        cvPow( dist_buf, dist_buf, 0.5 );
        icvHoughSortDescent32s( &sort_buf[0], nz_count1, (int*)ddata );

        dist_sum = start_dist = ddata[sort_buf[nz_count1-1]];
        for( j = nz_count1 - 2; j >= 0; j-- )
        {
            float d = ddata[sort_buf[j]];

            if( d > max_radius )
                break;

            if( d - start_dist > dr )
            {
                float r_cur = ddata[sort_buf[(j + start_idx)/2]];
                if( (start_idx - j)*r_best >= max_count*r_cur ||
                    (r_best < FLT_EPSILON && start_idx - j >= max_count) )
                {
                    r_best = r_cur;
                    max_count = start_idx - j;
                }
                start_dist = d;
                start_idx = j;
                dist_sum = 0;
            }
            dist_sum += d;
        }
        // Check if the circle has enough support
        if( max_count > acc_threshold )
        {
            float c[3];
            c[0] = cx;
            c[1] = cy;
            c[2] = (float)r_best;
            cvSeqPush( circles, c );
            if( circles->total > circles_max) break;
        }
    }
    
    if (circles->total > 0) {
       d_array.Create  (circles->total);
       cvCvtSeqToArray (circles,(FPoint3D*)d_array);
    }
    else d_array.Delete (   );
    
    return (d_array.Length);
}

 int OpenCV_DetectDarkCircles_HCT (GImage &s_image,GImage &e_image,float dp,float min_dist,int acc_threshold,int min_radius,int max_radius,FP3DArray1D &d_array)
 
{
    const int SHIFT = 10;
    const int ONE = 1 << SHIFT;
    const int circles_max = 1000;

    cv::Ptr<CvMat> dx, dy;
    cv::Ptr<CvMat> accum, dist_buf;
    std::vector<int> sort_buf;
    cv::Ptr<CvMemStorage> storage;

    int x, y, i, j, k;
    int center_count, nz_count;
    float min_radius2 = (float)min_radius*min_radius;
    float max_radius2 = (float)max_radius*max_radius;
    int rows, cols, arows, acols;
    int astep, *adata;
    float* ddata;
    CvSeq *nz, *centers;
    float idp, dr;
    CvSeqReader reader;

    Ptr<CvMemStorage> circle_storage = cvCreateMemStorage (STORAGE_SIZE);
    CvSeq *circles = cvCreateSeq (CV_32FC3,sizeof(CvSeq),sizeof(float)*3,circle_storage);

    CvMat s_img,e_img;
    OpenCV_InitCvMat (s_image,s_img);
    OpenCV_InitCvMat (e_image,e_img);
    CvMat *img   = &s_img;
    CvMat *edges = &e_img;

    dx = cvCreateMat( img->rows, img->cols, CV_16SC1 );
    dy = cvCreateMat( img->rows, img->cols, CV_16SC1 );
    cvSobel( img, dx, 1, 0, 3 );
    cvSobel( img, dy, 0, 1, 3 );

    if( dp < 1.f ) dp = 1.f;
    idp = 1.f/dp;
    accum = cvCreateMat( cvCeil(img->rows*idp)+2, cvCeil(img->cols*idp)+2, CV_32SC1 );
    cvZero(accum);

    storage = cvCreateMemStorage();
    nz = cvCreateSeq( CV_32SC2, sizeof(CvSeq), sizeof(CvPoint), storage );
    centers = cvCreateSeq( CV_32SC1, sizeof(CvSeq), sizeof(int), storage );

    rows  = img->rows;
    cols  = img->cols;
    arows = accum->rows - 2;
    acols = accum->cols - 2;
    adata = accum->data.i;
    astep = accum->step/sizeof(adata[0]);
    // Accumulate circle evidence for each edge pixel
    for( y = 0; y < rows; y++ )
    {
        const uchar* edges_row = edges->data.ptr + y*edges->step;
        const short* dx_row = (const short*)(dx->data.ptr + y*dx->step);
        const short* dy_row = (const short*)(dy->data.ptr + y*dy->step);

        for( x = 0; x < cols; x++ )
        {
            float vx = dx_row[x];
            float vy = dy_row[x];

            if( !edges_row[x] || (vx == 0 && vy == 0) ) continue;

            float mag = sqrt(vx*vx+vy*vy);
            assert( mag >= 1 );

            int x0 = cvRound((x*idp)*ONE);
            int y0 = cvRound((y*idp)*ONE);

            // 어두운 방향으로 voting하는 경우
            int sx = -cvRound((vx*idp)*ONE/mag);
            int sy = -cvRound((vy*idp)*ONE/mag);
            int x1 = x0 + min_radius * sx;
            int y1 = y0 + min_radius * sy;
            for(int r = min_radius; r <= max_radius; x1 += sx, y1 += sy, r++)
            {
               int x2 = x1 >> SHIFT, y2 = y1 >> SHIFT;
               if (x2 < 1 || y2 < 1 || x2 >= acols || y2 >= arows) break;
               int pos = y2*astep + x2;
               adata[pos]++;
               adata[pos - 1]++;
               adata[pos + 1]++;
               adata[pos - astep]++;
               adata[pos + astep]++;
            }
            
            CvPoint pt;
            pt.x = x; pt.y = y;
            cvSeqPush( nz, &pt );
        }
    }

    nz_count = nz->total;
    if (!nz_count) {
       d_array.Delete (   );
       return (0);
    }
    
    //Find possible circle centers
    for( y = 1; y < arows - 1; y++ )
    {
        for( x = 1; x < acols - 1; x++ )
        {
            int base = y*(acols+2) + x;
            if( adata[base] > acc_threshold &&
                adata[base] > adata[base-1] && adata[base] > adata[base+1] &&
                adata[base] > adata[base-acols-2] && adata[base] > adata[base+acols+2] )
                cvSeqPush(centers, &base);
        }
    }

    center_count = centers->total;
    if (!center_count) {
       d_array.Delete (   );
       return (0);
    }

    sort_buf.resize( MAX(center_count,nz_count) );
    cvCvtSeqToArray( centers, &sort_buf[0] );

    icvHoughSortDescent32s( &sort_buf[0], center_count, adata );
    cvClearSeq( centers );
    cvSeqPushMulti( centers, &sort_buf[0], center_count );

    dist_buf = cvCreateMat( 1, nz_count, CV_32FC1 );
    ddata = dist_buf->data.fl;

    dr = dp;
    min_dist = MAX( min_dist, dp );
    min_dist *= min_dist;
    // For each found possible center
    // Estimate radius and check support
    for( i = 0; i < centers->total; i++ )
    {
        int ofs = *(int*)cvGetSeqElem( centers, i );
        y = ofs/(acols+2);
        x = ofs - (y)*(acols+2);
        //Calculate circle's center in pixels
        float cx = (float)((x + 0.5f)*dp), cy = (float)(( y + 0.5f )*dp);
        float start_dist, dist_sum;
        float r_best = 0;
        int max_count = 0;
        // Check distance with previously detected circles
        for( j = 0; j < circles->total; j++ )
        {
            float* c = (float*)cvGetSeqElem( circles, j );
            if( (c[0] - cx)*(c[0] - cx) + (c[1] - cy)*(c[1] - cy) < min_dist )
                break;
        }

        if( j < circles->total )
            continue;
        // Estimate best radius
        cvStartReadSeq( nz, &reader );
        for( j = k = 0; j < nz_count; j++ )
        {
            CvPoint pt;
            float _dx, _dy, _r2;
            CV_READ_SEQ_ELEM( pt, reader );
            _dx = cx - pt.x; _dy = cy - pt.y;
            _r2 = _dx*_dx + _dy*_dy;
            if(min_radius2 <= _r2 && _r2 <= max_radius2 )
            {
                ddata[k] = _r2;
                sort_buf[k] = k;
                k++;
            }
        }

        int nz_count1 = k, start_idx = nz_count1 - 1;
        if( nz_count1 == 0 )
            continue;
        dist_buf->cols = nz_count1;
        cvPow( dist_buf, dist_buf, 0.5 );
        icvHoughSortDescent32s( &sort_buf[0], nz_count1, (int*)ddata );

        dist_sum = start_dist = ddata[sort_buf[nz_count1-1]];
        for( j = nz_count1 - 2; j >= 0; j-- )
        {
            float d = ddata[sort_buf[j]];

            if( d > max_radius )
                break;

            if( d - start_dist > dr )
            {
                float r_cur = ddata[sort_buf[(j + start_idx)/2]];
                if( (start_idx - j)*r_best >= max_count*r_cur ||
                    (r_best < FLT_EPSILON && start_idx - j >= max_count) )
                {
                    r_best = r_cur;
                    max_count = start_idx - j;
                }
                start_dist = d;
                start_idx = j;
                dist_sum = 0;
            }
            dist_sum += d;
        }
        // Check if the circle has enough support
        if( max_count > acc_threshold )
        {
            float c[3];
            c[0] = cx;
            c[1] = cy;
            c[2] = (float)r_best;
            cvSeqPush( circles, c );
            if( circles->total > circles_max) break;
        }
    }
    
    if (circles->total > 0) {
       d_array.Create  (circles->total);
       cvCvtSeqToArray (circles,(FPoint3D*)d_array);
    }
    else d_array.Delete (   );
    
    return (d_array.Length);
}

#endif

}

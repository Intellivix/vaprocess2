#if !defined(__VACL_WATER_H)
#define __VACL_WATER_H

#include "vacl_objtrack.h"

 namespace VACL

{

class EventZone;
class EventDetection;
class WaterLevelDetection;

///////////////////////////////////////////////////////////////////////////////
//
// Class: WaterZone
//
///////////////////////////////////////////////////////////////////////////////

 class WaterZone

{
   protected:
      int Flag_Processed;

   public:
      float      DetTime;
      IBox2D     ROI;
      EventZone* EvtZone;

   public:
      int      DangerHeight;
      int      PrevLevel;
      float    MergeThrsld;
      float    ThrsldLearnRate;
      float    WaterLearnRate;
      FPoint3D WaterMean;

   public:
      float   WaterRatio;
      ILine2D WaterLevel; // 검출된 수위 (좌표 값이 모두 0 이면 수위 검출에 실패했음을 의미함)

   public:
      WaterZone* Prev;
      WaterZone* Next;

   public:
      WaterZone (   );
   
   protected:
      float GetDistance (FPoint3D& p1,FPoint3D& p2);
      void  GetRGBMean  (BGRImage& s_cimage,int sx,int sy,int width,int height,float& mean_r,float& mean_g,float& mean_b);

   public:
      int  DetectEvent   (   );
      void GetWaterLevel (GImage& s_image,BGRImage& s_cimage);
};

typedef LinkedList<WaterZone> WaterZoneList;

///////////////////////////////////////////////////////////////////////////////
//
// Class: WaterLevelDetection
//
///////////////////////////////////////////////////////////////////////////////

 class WaterLevelDetection

{
   protected:
      int Flag_Enabled;
 
   public:
      float   FrameRate;
      ISize2D SrcImgSize;

   public:
      WaterZoneList WaterZones;
      
   public:
      WaterLevelDetection (   );
      virtual ~WaterLevelDetection (   );

   protected:
      void Perform (GImage& s_image,WaterZone* zone);
   
   public:
      virtual void Close (   );

   public:
      void Enable     (int flag_enable);
      int  Initialize (EventDetection& evt_detector);
      int  IsEnabled  (   );
      int  Perform    (GImage& s_image,BGRImage& s_cimage);
};

 inline void WaterLevelDetection::Enable (int flag_enable)

{
   Flag_Enabled = flag_enable;
}

 inline int WaterLevelDetection::IsEnabled (   )

{
   return (Flag_Enabled);
}

}

#endif

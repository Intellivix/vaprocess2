#include "vacl_optimize.h"
#include <limits>

 namespace VACL
  
{

///////////////////////////////////////////////////////////////////////////////
//
//  Class: Minimization_LM
//
///////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//
//  Multiple View Geometry in Computer Vision
//  Richard Hartley and Andrew Zisserman
//  Cambridge University Press, 2000
//  pp. 568-571
//
////////////////////////////////////////////////////////////////////////////////
 
 void Minimization_LM::Close (   )

{
   Mat_A.Delete (   );
   Vec_b.Delete (   );
   Vec_p.Delete (   );
   ParamVector.Delete (   );
}

 void Minimization_LM::GetJacobianMatrix (Matrix &vec_p,Matrix &mat_J)

{
   int i,j,k,m;

   DArray1D x_array(vec_p.NRows);
   DArray1D dx_array(vec_p.NRows);
   for (i = 0; i < vec_p.NRows; i++) {
      dx_array[i] = fabs (1E-4 * vec_p(i));
      if (dx_array[i] < 1E-6) dx_array[i] = 1E-6;
      x_array[i] = vec_p(i) + dx_array[i];
   }
   Matrix vec_x = vec_p;
   Matrix vec_m1(MsmVectorSize);
   Matrix vec_m2(MsmVectorSize);
   for (i = m = 0; i < NMsmVectors; i++,m += MsmVectorSize) {
      CostFunction (vec_p,i,vec_m1);
      for (j = 0; j < vec_p.NRows; j++) {
         vec_x(j) = x_array[j];
         CostFunction (vec_x,i,vec_m2);
         Matrix vec_dm = (vec_m2 - vec_m1) / dx_array[j];
         for (k = 0; k < MsmVectorSize; k++) mat_J[m + k][j] = vec_dm(k);
         vec_x(j) = vec_p(j);
      }
   }
}

 void Minimization_LM::Initialize (int n_m_vecs,int m_vec_size,int n_params)

{
   Error         = MC_VLV;
   Lambda        = 1E-1;
   NIterations   = 0;
   NMsmVectors   = n_m_vecs;
   MsmVectorSize = m_vec_size;
   ParamVector.Create (n_params);
}

 double Minimization_LM::Perform (int n_iterations,double d_thrsld,double e_thrsld)

{
   int    i;
   double p_error,c_error;

   p_error = MC_VLV;
   for (i = 0; n_iterations <= 0 || i < n_iterations; i++) {
      c_error = Update (   );
      if (fabs (c_error - p_error) < d_thrsld) break;
      else p_error = c_error;
      if (c_error <= e_thrsld) break;
   }
   return (c_error);
}

 double Minimization_LM::Update (   )

{
   int i,j,m;

   if (NIterations == 0) Vec_p = ParamVector;
   PrepareForUpdate (Vec_p);
   int n_measurements = NMsmVectors * MsmVectorSize;
   Matrix vec_e(n_measurements); 
   Matrix vec_m(MsmVectorSize);
   for (i = m = 0; i < NMsmVectors; i++,m += MsmVectorSize) {
      CostFunction (Vec_p,i,vec_m);
      for (j = 0; j < MsmVectorSize; j++) vec_e(m + j) = vec_m(j);
   }
   double error = sqrt (IP(vec_e,vec_e) / n_measurements);
   if (error >= Error && NIterations != 0) {
      if (Lambda > 1E+8) Lambda = 1E+8;
      else Lambda *= 5.0f;
   }
   else {
      Error = error;
      Lambda /= 5.0f;
      if (Lambda < 1E-4) Lambda = 1E-4;
      ParamVector = Vec_p;
      Matrix mat_J(n_measurements,ParamVector.NRows);
      GetJacobianMatrix (ParamVector,mat_J);
      Matrix mat_Jt = Tr(mat_J);
      Mat_A = mat_Jt * mat_J;
      Vec_b = mat_Jt * vec_e;
   }
   Matrix mat_A  = Mat_A;
   double lambda = 1.0 + Lambda;
   for (i = 0; i < mat_A.NRows; i++) mat_A[i][i] *= lambda;
   Vec_p = ParamVector - Inv(mat_A) * Vec_b;
   NIterations++;
   return (error);
}

///////////////////////////////////////////////////////////////////////////////
//
//  Class: Minimization_Powell
//
///////////////////////////////////////////////////////////////////////////////

 double Minimization_Powell::brent (double ax,double bx,double cx,double tol,double &xmin)

{
   const int ITMAX=100;
   const double CGOLD=0.3819660;
   const double ZEPS=std::numeric_limits<double>::epsilon()*1.0e-3;
   int    iter;
   double a,b,d=0.0,etemp,fu,fv,fw,fx;
   double p,q,r,tol1,tol2,u,v,w,x,xm;
   double e=0.0;

   a=(ax < cx ? ax : cx);
   b=(ax > cx ? ax : cx);
   x=w=v=bx;
   fw=fv=fx=f1dim(x);
   for (iter=0;iter<ITMAX;iter++) {
      xm=0.5*(a+b);
      tol2=2.0*(tol1=tol*fabs(x)+ZEPS);
      if (fabs(x-xm) <= (tol2-0.5*(b-a))) {
         xmin=x;
         return fx;
      }
      if (fabs(e) > tol1) {
         r=(x-w)*(fx-fv);
         q=(x-v)*(fx-fw);
         p=(x-v)*q-(x-w)*r;
         q=2.0*(q-r);
         if (q > 0.0) p = -p;
         q=fabs(q);
         etemp=e;
         e=d;
         if (fabs(p) >= fabs(0.5*q*etemp) || p <= q*(a-x) || p >= q*(b-x))
            d=CGOLD*(e=(x >= xm ? a-x : b-x));
         else {
            d=p/q;
            u=x+d;
            if (u-a < tol2 || b-u < tol2)
               d=SIGN(tol1,xm-x);
         }
      } else {
         d=CGOLD*(e=(x >= xm ? a-x : b-x));
      }
      u=(fabs(d) >= tol1 ? x+d : x+SIGN(tol1,d));
      fu=f1dim(u);
      if (fu <= fx) {
         if (u >= x) a=x; else b=x;
         shft3(v,w,x,u);
         shft3(fv,fw,fx,fu);
      } else {
         if (u < x) a=u; else b=u;
         if (fu <= fw || w == x) {
            v=w;
            w=u;
            fv=fw;
            fw=fu;
         } else if (fu <= fv || v == x || v == w) {
            v=u;
            fv=fu;
         }
      }
   }
   logd ("Minimization_Powell::brent( ): Too many iterations.\n");
   xmin=x;
   return fx;
}

 double Minimization_Powell::f1dim (double x)

{
   int j,ncom;

   ncom = pcom.NRows;
   Matrix xt(ncom);
   for (j=0;j<ncom;j++)
      xt(j)=pcom(j)+x*xicom(j);
   return CostFunction(xt);
}

 void Minimization_Powell::linmin (Matrix &p,Matrix &xi,double &fret)

{
   int j;
   const double TOL=1.0e-8;
   double xx,xmin,fx,fb,fa,bx,ax;

   int n=p.NRows;
   pcom.Create (n);
   xicom.Create (n);
   for (j=0;j<n;j++) {
      pcom(j)=p(j);
      xicom(j)=xi(j);
   }
   ax=0.0;
   xx=1.0;
   mnbrak(ax,xx,bx,fa,fx,fb);
   fret=brent(ax,xx,bx,TOL,xmin);
   for (j=0;j<n;j++) {
      xi(j) *= xmin;
      p(j) += xi(j);
   }
   xicom.Delete (   );
   pcom.Delete (   );
}

 void Minimization_Powell::mnbrak (double &ax,double &bx,double &cx,double &fa,double &fb,double &fc)

{
   const double GOLD=1.618034,GLIMIT=100.0,TINY=1.0e-20;
   double ulim,u,r,q,fu;

   fa=f1dim(ax);
   fb=f1dim(bx);
   if (fb > fa) {
      Swap(ax,bx);
      Swap(fb,fa);
   }
   cx=bx+GOLD*(bx-ax);
   fc=f1dim(cx);
   while (fb > fc) {
      r=(bx-ax)*(fb-fc);
      q=(bx-cx)*(fb-fa);
      u=bx-((bx-cx)*q-(bx-ax)*r)/
         (2.0*SIGN(GetMaximum(fabs(q-r),TINY),q-r));
      ulim=bx+GLIMIT*(cx-bx);
      if ((bx-u)*(u-cx) > 0.0) {
         fu=f1dim(u);
         if (fu < fc) {
            ax=bx;
            bx=u;
            fa=fb;
            fb=fu;
            return;
         } else if (fu > fb) {
            cx=u;
            fc=fu;
            return;
         }
         u=cx+GOLD*(cx-bx);
         fu=f1dim(u);
      } else if ((cx-u)*(u-ulim) > 0.0) {
         fu=f1dim(u);
         if (fu < fc) {
            shft3(bx,cx,u,cx+GOLD*(cx-bx));
            shft3(fb,fc,fu,f1dim(u));
         }
      } else if ((u-ulim)*(ulim-cx) >= 0.0) {
         u=ulim;
         fu=f1dim(u);
      } else {
         u=cx+GOLD*(cx-bx);
         fu=f1dim(u);
      }
      shft3(ax,bx,cx,u);
      shft3(fa,fb,fc,fu);
   }
}

 void Minimization_Powell::powell (Matrix &p,Matrix &xi,double ftol,int &iter,double &fret)

{
   const int ITMAX=200;
   const double TINY=1.0e-25;
   int i,j,ibig;
   double del,fp,fptt,t;

   int n=p.NRows;
   Matrix pt(n),ptt(n),xit(n);
   fret=CostFunction(p);
   for (j=0;j<n;j++) pt(j)=p(j);
   for (iter=0;;++iter) {
      fp=fret;
      ibig=0;
      del=0.0;
      for (i=0;i<n;i++) {
         for (j=0;j<n;j++) xit(j)=xi[j][i];
         fptt=fret;
         linmin(p,xit,fret);
         if (fptt-fret > del) {
            del=fptt-fret;
            ibig=i+1;
         }
      }
      if (2.0*(fp-fret) <= ftol*(fabs(fp)+fabs(fret))+TINY) {
         return;
      }
      if (iter == ITMAX) logd ("Minimization_Powell::powell( ): Exceeding maximum iterations.\n");
      for (j=0;j<n;j++) {
         ptt(j)=2.0*p(j)-pt(j);
         xit(j)=p(j)-pt(j);
         pt(j)=p(j);
      }
      fptt=CostFunction(ptt);
      if (fptt < fp) {
         t=2.0*(fp-2.0*fret+fptt)*SQR(fp-fret-del)-del*SQR(fp-fptt);
         if (t < 0.0) {
            linmin(p,xit,fret);
            for (j=0;j<n;j++) {
               xi[j][ibig-1]=xi[j][n-1];
               xi[j][n-1]=xit(j);
            }
         }
      }
   }
}

 inline void Minimization_Powell::shft3 (double &a,double &b,double &c,const double d)

{
   a=b; b=c; c=d;
}

 inline double Minimization_Powell::SIGN (const double a,const double b)

{
   return b >= 0.0 ? (a >= 0.0 ? a : -a) : (a >= 0.0 ? -a : a);
}

 inline double Minimization_Powell::SQR (const double a)
 
{
   return a*a;
}
 
 void Minimization_Powell::Close (   )

{
   ParamVector.Delete (   );
}

 void Minimization_Powell::Initialize (int n_params)

{
   ParamVector.Create (n_params);
}

 double Minimization_Powell::Perform (double ftol)

{
   int iter;
   double fret;

   Matrix mat_D = IM(ParamVector.NRows);
   powell (ParamVector,mat_D,ftol,iter,fret);
   return (fret);
}

///////////////////////////////////////////////////////////////////////////////
//
//  Class: Minimizaiton_UniformTilt
//
///////////////////////////////////////////////////////////////////////////////
 
 void Minimizaiton_UniformTilt::CostFunction (Matrix &vec_p,int n,Matrix &vec_m)
 
{
   Matrix mat_IT(3,3);
   GetInverseOfTransformationMatrix (vec_p,mat_IT);
   Matrix vec_z = mat_IT * Vecs_r[n];
   vec_m(0) = vec_z(1) - MeanY;
} 

 void Minimizaiton_UniformTilt::Close (   )

{
   Vecs_r.Delete (   );
   Minimization_LM::Close (   );
}

 void Minimizaiton_UniformTilt::GetInverseOfTransformationMatrix (Matrix &vec_p,Matrix &mat_IT)

{
   mat_IT[0][0] = 1.0;
   mat_IT[0][1] = mat_IT[0][2] = mat_IT[1][0] = mat_IT[2][0] = 0.0;
   mat_IT[1][1] = cos (vec_p(0));
   mat_IT[1][2] = sin (vec_p(0));
   mat_IT[2][1] = -mat_IT[1][2];
   mat_IT[2][2] =  mat_IT[1][1];
}

 void Minimizaiton_UniformTilt::GetTransformationMatrix (Matrix &mat_T)

{
   Matrix mat_IT(3,3);
   GetInverseOfTransformationMatrix (ParamVector,mat_IT);
   mat_T = Tr(mat_IT);
}

 void Minimizaiton_UniformTilt::Initialize (Array1D<Matrix> &mats_R)

{
   int i;
   
   Vecs_r.Create (mats_R.Length);
   for (i = 0; i < mats_R.Length; i++) {
      Matrix mat_IR = Inv(mats_R[i]);
      Vecs_r[i] = Nor(mat_IR.GetColumnVector (2));
   }
   Minimization_LM::Initialize (mats_R.Length,1,1);
   ParamVector(0) = 0.0;
}

 void Minimizaiton_UniformTilt::PrepareForUpdate (Matrix &vec_p)

{
   int i;
   
   Matrix mat_IT(3,3);
   GetInverseOfTransformationMatrix (vec_p,mat_IT);
   MeanY = 0.0;
   for (i = 0; i < Vecs_r.Length; i++) {
      Matrix vec_z = mat_IT * Vecs_r[i];
      MeanY += vec_z(1);
   }
   MeanY /= (double)Vecs_r.Length;
}

}

#if !defined(__IVCP_PACKET_H)
#define __IVCP_PACKET_H

#include "ivcp_define.h"

 namespace IVCP
 
{

 class CIVCPPacket

{
   ////////////////////////////////////////////////////////////////////
	// CIVCPPacketHeader Class

   class  CIVCPPacketHeader
   {
	public:
		CIVCPPacketHeader (   );

	public:
		char m_strMessage[20]; // Communication message of IVCP.
      int  m_nOptions;       // Packet Options
		int  m_nDataLen;       // Data length of IVCP.
		int  m_nBodyLen;       // Body length of IVCP.
	};

   ////////////////////////////////////////////////////////////////////

public:
   CIVCPPacketHeader m_Header;  // Packet block header of IVCP.
   char*	            m_pBody;   // Packet block body of IVCP.

public:
	CIVCPPacket (   );
	CIVCPPacket (const char *szMessage);
  ~CIVCPPacket (   );

public:
   CIVCPPacket& operator = (CIVCPPacket& from);

public:
   BOOL ParseHeader (const char* resString, int nLen); 
   BOOL AllocBody   (   );                        
	void FreeBody    (   );
};

}

#endif

#if !defined(__VACL_NMS_H)
#define __VACL_NMS_H

#include "vacl_define.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: DetRegion
//
///////////////////////////////////////////////////////////////////////////////

 class DetRegion : public IBox2D

{
   public:
      int      Area;
      float    AvgVotScore;
      float    MaxVotScore;
      IPoint2D Center;

   public:
      DetRegion (   );
   
   public:
      void Clear (   );
};

typedef Array1D<DetRegion> DRArray1D;

///////////////////////////////////////////////////////////////////////////////
//
// Class: NonmaximumSuppression_Island
//
///////////////////////////////////////////////////////////////////////////////

 class NonmaximumSuppression_Island
 
{
   protected:
      int VRRadius;
   
   // Output
   public:
      FArray2D  VotScrMap;  // Voting Score Map
      IArray2D  DetRgnMap;  // Detection Region Map
      DRArray1D DetRegions; // Detection Regions

   public:
      NonmaximumSuppression_Island (   );
      virtual ~NonmaximumSuppression_Island (   );
   
   protected:
      float GetVotScrMapMean (   );
      int   GetDetRegions    (GImage& s_image,int min_area);
   
   public:
      virtual void Close (   );
   
   public:
      void Clear          (   );
      int  GetDetRgnIdxNo (IBox2D& s_rect);
      int  GetDetRgnIdxNo (IPoint2D& s_pos);
      void Initialize     (ISize2D& si_size,int vr_radius);
      int  IsInitialized  (   );
      int  Perform        (float a_vs_thrsld,float r_vs_thrsld,int min_area);
      int  Vote           (IBox2D& s_rect,float score);
      int  Vote           (IPoint2D& s_pos,float score);
};

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

// 검출된 위치의 개수를 리턴한다.
int PerformPointNMS       (FArray2D& s_array,int win_size,float sc_thrsld,GImage& d_image);
int PerformRectNMS_Greedy (IB2DArray1D& s_rects,FArray1D& s_scores,float ov_thrsld,float ar_thrsld,IB2DArray1D& d_rects);
int PerformRectNMS_Island (ISize2D& si_size,IB2DArray1D& s_rects,FArray1D& s_scores,float sc_thrsld,int vr_radius,float a_vs_thrsld,float r_vs_thrsld,IB2DArray1D& d_rects,FArray1D& d_scores);

}

#endif

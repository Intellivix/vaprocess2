#if !defined(__VACL_REALSIZE_H)
#define __VACL_REALSIZE_H

#include "vacl_camcalib.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: RealObjectSIzeEstimation
//
///////////////////////////////////////////////////////////////////////////////

#define NUM_REF_OBJECTS   4

 class RealObjectSizeEstimation
 
{
   protected:
      int    Flag_Enabled; //
      Matrix Mat_H;        // "영상 좌표(참조 영상 해상도 기준) => 그라운드 평면 좌표(미터 단위)" 변환 호모그래피
      Matrix Mat_IH;       // Mat_H의 역행렬
      Matrix Mat_IK;       // 카메라 켈리브레이션 행렬의 역행렬
   
   // Input
   public:
      int     Options;                    // 현재 사용 안 함
      float   FrameRate;                  // 영상 처리 프레임 레이트
      float   UpdateRate;                 // 객체 속성 값들의 업데이트율(Default: 0.2)
      ISize2D RefImgSize;                 // 참조 영상의 해상도
      ILine2D Objects[NUM_REF_OBJECTS];   // 참조 객체들의 영상 좌표(참조 영상 해상도 기준)
                                          //    Objects[i][0]: 영상에서 i번째 객체의 Top Center 좌표
                                          //    Objects[i][1]: 영상에서 i번째 객체의 Bottom Center 좌표
      COArray1D CalibObjects;             // 카메라 캘리브레이션 시 사용되는 참조 객체들의 정보 

   // Input or Output
   public:
      float TiltAngle;                    // 카메라 틸트각(단위: 도)
                                          //    카메라가 지면과 평행일 때 틸트각은 0도가 되며, 카메라를 지면 쪽으로 숙일수록 틸트각이 증가한다.
      float DistanceFromGround;           // 지면과 카메라 사이의 거리(단위: 미터)

  // Output
  public: 
      float FocalLength;                  // 카메라의 초점거리
                                          //    UpdateFocalLength() 함수 호출함에 의해 구해진다.
                                          //    UpdateFocalLength() 함수를 호출하기 전에 적정한 TiltAngle, DistanceFromGround,
                                          //    Objects[0], RealHeights[0] 값이 주어져야 한다.
      float RealHeights[NUM_REF_OBJECTS]; // 참조 객체들의 실제 높이(미터 단위)
                                          //    RealHeights[i]: i번째 객체의 실제 높이
                                          //    RealHeights[0]의 값(0번째 참조 객체의 실제 높이)은 사용자가 제공하며,
                                          //    나머지는 UpdateTestObjectHeights() 함수 호출 시 자동 계산된다.

   public:
      RealObjectSizeEstimation (   );

   protected:
      // 계산을 위한 기본 가정들
      // 가정1: 지면은 평평하다.
      // 가정2: 객체의 밑면은 지면에 붙어 있다.
      // 가정3: 객체는 지면과 수직으로 서 있다.
      // 가정3: 카메라 폴은 지면에 수직으로 서 있다.

      // TiltAngle, DistanceFromGround, FocalLength 값을 사용하여 영상 좌표(참조 영상 해상도 기준)와
      // 그라운드 평면 좌표(미터 단위) 사이의 변환 관계를 나타내는 호모그래피를 구한다.
      int GetHomography (   );
      // 객체의 실제 높이를 계산한다.
      // bc_point[IN]: 영상에서 객체의 Bottom Center의 좌표(참조 영상 해상도 기준)
      // o_height[IN]: 영상에서 객체의 높이(참조 영상 해상도 기준)
      // r_pos   [IN]: 그라운드 평면에서의 객체의 위치 좌표(GetRealPosition() 함수의 리턴값)
      // 리턴 값     : 객체의 실제 높이 값
      float GetRealHeight (FPoint2D &bc_point,float o_height,FPoint2D &r_pos);
      // 그라운드 평면에서의 객체의 위치 좌표 구한다.
      // bc_point[IN]: 영상에서 객체의 Bottom Center의 좌표(참조 영상 해상도 기준)
      // 리턴 값     : 그라운드 평면에서의 객체의 위치 좌표
      FPoint2D GetRealPosition (FPoint2D &bc_point);
 
   public:
      int  CalibrateCamera        (   );
      void Enable                 (int flag_enable);
      int  GetFeetPosAndBodySize  (IPoint2D& hd_pos,ISize2D& si_size,float hm_height,IPoint2D& ft_pos,int& hd_size,ISize2D& bd_size);
      // 객체의 실제 크기 및 카메라 폴과의 실제 거리를 구한다.
      // s_box  [IN] : 입력 영상에서의 객체의 바운딩 박스 좌표
      // si_size[IN] : 입력 영상의 해상도
      // r_size [OUT]: 객체의 실제 크기
      // r_pos  [OUT]: 카메라 폴과 객체 간의 실제 거리
      // 리턴 값     : 오류가 없으면 DONE을 리턴한다.
      int  GetRealSizeAndPos      (IBox2D& s_rgn,ISize2D& si_size,FSize2D &d_size,FPoint2D &d_pos);
      int  Initialize             (ISize2D& ri_size);
      int  IsCalibrated           (   );
      int  IsEnabled              (   );
      int  Perform                (ObjectTracking& obj_tracker);
      // FocalLength, RealHeights, Objects 값을 리셋한다.
      void Reset                  (   );
      // 카메라의 초점거리(FocalLength) 값을 재계산한다.
      // 사용자가 제공한 TiltAngle, DistanceFromGround, Objects[0], RealHeights[0] 값을 이용한다.
      int  UpdateFocalLength      (   ); 
      // 참조 객체들 {Objects[i] (i > 0)}의 실제 높이를 재계산한다.
      int  UpdateRefObjectHeights (   );

   public:
      int ReadFile   (FileIO &file);
      int ReadFile   (CXMLIO* pIO);
      int WriteFile  (FileIO &file);
      int WriteFile  (CXMLIO* pIO);
      int CheckSetup (RealObjectSizeEstimation* rose);
};

 inline void RealObjectSizeEstimation::Enable (int flag_enable)

{
   Flag_Enabled = flag_enable;
}

 inline int RealObjectSizeEstimation::IsEnabled (   )

{
   return (Flag_Enabled);
}

}

#endif

#if !defined(__VACL_SAMPLE_H)
#define __VACL_SAMPLE_H

#include "vacl_define.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: SmpFileList
//
///////////////////////////////////////////////////////////////////////////////

 class SmpFileList : public CArray2D
 
{
   public:
      int MaxLineLength;
   
   public:
      SmpFileList (   ) { MaxLineLength = 512; };

   public:
      int LoadList  (const char* file_name);
      int LoadList  (FileIO& file);
      int LoadImage (int index_no,BGRImage &d_image);
      int LoadImage (int index_no,GImage &d_image);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: RndSmpImgExtraction
//
///////////////////////////////////////////////////////////////////////////////

 class RndSmpImgExtraction

{
   protected:
      int Flag_Initialized;
   
   public:
      RndSmpImgExtraction (   );
      virtual ~RndSmpImgExtraction (   );

   public:
      virtual void Close (   );

   public:
      void Initialize (   );

   public:
      int Perform (GImage& s_image,GImage& d_image);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: SeqSmpImgExtraction
//
///////////////////////////////////////////////////////////////////////////////

 class SeqSmpImgExtraction

{
   protected:
      GImage   SrcImage;
      ISize2D  SmpSize;
      IPoint2D StepSize;
      IPoint2D CurPos;

   public:
      SeqSmpImgExtraction (   );
      virtual ~SeqSmpImgExtraction (   );

   public:
      virtual void Close (   );

   public:
      int Initialize (GImage& s_image,float scale,ISize2D& smp_size,IPoint2D& step_size);

   public:
      int Perform (GImage& d_image);
};

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

void GetShuffledIndexNumbers (IArray1D& d_array);
void ShuffleElements         (IArray1D& s_array,int n_times);

}

#endif

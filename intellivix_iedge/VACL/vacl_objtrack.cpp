#include "vacl_binimg.h"
#include "vacl_edge.h"
#include "vacl_event.h"
#include "vacl_geometry.h"
#include "vacl_histo.h"
#include "vacl_objclass.h"
#include "vacl_objtrack.h"
#include "VAEngine.h"

#if defined(__DEBUG_OBT)
#include "Win32CL/Win32CL.h"
extern int _DVX_OBT,_DVY_OBT;
extern CImageView* _DebugView_OBT;
#endif

// #define __CONSOLE

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Definitions
//
///////////////////////////////////////////////////////////////////////////////

#define OBT_PARAM_OBJMAT_MAX_AREA_CHANGED_ABRUPTLY  100.0f // ํ์ฌ๋ ๊ด๋ จ ๊ธฐ๋ฅ OFF. ์ฌ ๊ฒํ  ํ์.
#define OBT_PARAM_TRKOBJ_MAX_TRAJECTORY_LENGTH      100

///////////////////////////////////////////////////////////////////////////////
//
// Global Variables/Functions
//
///////////////////////////////////////////////////////////////////////////////

extern int __Flag_VACL_Initialized;
extern int __CheckLicenseKey (   );
TrackedObject* (*__CreationFunction_TrackedObject)(   ) = NULL;

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectIDGeneration
//
///////////////////////////////////////////////////////////////////////////////

 ObjectIDGeneration::ObjectIDGeneration (   )

{
   Reset (   );
}

 int ObjectIDGeneration::Generate (   )

{
   return (Count++);
}

 void ObjectIDGeneration::Reset (   )

{
   Count = 1;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: FRArray1D
//
///////////////////////////////////////////////////////////////////////////////

 void FRArray1D::Initialize (CRArray1D& s_array)

{
   int i;
   
   if (!s_array) {
      Delete (   );
      return;
   }
   Create (s_array.Length);
   for (i = 0; i < s_array.Length; i++)
      *(ConnectedRegion*)&Array[i] = s_array[i];
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: TrkObjRegion
//
///////////////////////////////////////////////////////////////////////////////

 void TrkObjRegion::SetRegion (IBox2D &s_rgn)

{
   X      = s_rgn.X;
   Y      = s_rgn.Y;
   Width  = s_rgn.Width;
   Height = s_rgn.Height;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: TrackedObject
//
///////////////////////////////////////////////////////////////////////////////

 TrackedObject::TrackedObject (   )

{
   ID                     = 0;
   Status                 = 0;
   Type                   = TO_TYPE_UNKNOWN;

   Depth                  = 0;
   EventStatus            = 0;
   EventZones             = 0;
   NumPersons             = 0;
   NumBndPixels           = 0;
   NumRgnPixels           = 0;
   NumFrgBndEdgePixels    = 0;
   NumBkgBndEdgePixels    = 0;
   TrjLength              = 0;
   AreaVariation          = 0.0f;
   BndEdgeStrength        = 0.0f;
   Dispersedness          = 0.0f;
   Exposure               = 1.0f;
   MajorAxisAngle1        = 0.0f;
   MajorAxisAngle2        = -1.0f;
   MatchingScore          = 0.0f;
   MaxMovingDistance      = 0.0f;
   MaxScore_BestShot      = 0.0f;
   MeanArea               = 0.0f;
   Noisiness              = 0.0f;

   FrameCount             = 0;
   InitFrameCount         = 0;
   Count_Tracked          = 0;

   Time_BestShot          = 0.0f;
   Time_ChangedAbruptly   = 0.0f;
   Time_Lost              = 0.0f;
   Time_Merged            = 0.0f;
   Time_Static            = 0.0f;
   Time_Tracked           = 0.0f;
   Time_Undetected        = 0.0f;

   Time_CarAccident       = 0.0f;
   Time_CarStopping       = 0.0f;
   Time_DirecMotion       = 0.0f;
   Time_Dwell             = 0.0f;
   Time_Falling           = 0.0f;
   Time_FrontalFace1      = 0.0f;
   Time_FrontalFace2      = 0.0f;
   Time_Loitering         = 0.0f;
   Time_Stopping          = 0.0f;

   Area                   = 0.0f;
   AspectRatio            = 0.0f;
   AxisLengthRatio        = 0.0f;
   MajorAxisLength        = 0.0f;
   MinorAxisLength        = 0.0f;
   NorSpeed               = 0.0f;
   Speed                  = 0.0f;
   RealArea               = 0.0f;
   RealDistance           = 0.0f;
   RealWidth              = 0.0f;
   RealHeight             = 0.0f;
   RealMajorAxisLength    = 0.0f;
   RealMinorAxisLength    = 0.0f;
   RealSpeed              = -1.0f;
   
   AvgWidth               = 0;
   AvgHeight              = 0;
   AvgArea                = 0.0f;
   AvgAspectRatio         = 0.0f;
   AvgAxisLengthRatio     = 0.0f;
   AvgMajorAxisLength     = 0.0f;
   AvgMinorAxisLength     = 0.0f;
   AvgNorSpeed            = 0.0f;
   AvgSpeed               = 0.0f;
   AvgRealArea            = 0.0f;
   AvgRealDistance        = 0.0f;
   AvgRealWidth           = 0.0f;
   AvgRealHeight          = 0.0f;
   AvgRealMajorAxisLength = 0.0f;
   AvgRealMinorAxisLength = 0.0f;
   AvgRealSpeed           = 0.0f;
   
   MaxWidth               = 0;
   MaxHeight              = 0;
   MaxArea                = 0.0f;
   MaxAspectRatio         = 0.0f;
   MaxAxisLengthRatio     = 0.0f;
   MaxMajorAxisLength     = 0.0f;
   MaxMinorAxisLength     = 0.0f;
   MaxNorSpeed            = 0.0f;
   MaxSpeed               = 0.0f;
   MaxRealDistance        = 0.0f;
   MaxRealSpeed           = 0.0f;
   
   MinWidth               = 0x7FFFFFFF;
   MinHeight              = 0x7FFFFFFF;
   MinArea                = MC_VLV;
   MinAspectRatio         = MC_VLV;
   MinAxisLengthRatio     = MC_VLV;
   MinMajorAxisLength     = MC_VLV;
   MinMinorAxisLength     = MC_VLV;
   MinNorSpeed            = MC_VLV;
   MinSpeed               = MC_VLV;
   MinRealDistance        = MC_VLV;
   MinRealSpeed           = MC_VLV;

   ThumbPixFmt            = VAE_PIXFMT_NONE;
   ObjRgnID               = 0;
   ObjRgnArea             = 0;
   TrkBlob                = NULL;

   MeanVelocity(MC_VLV,MC_VLV);
   Velocity(MC_VLV,MC_VLV);

   CntBkgColor.Clear (   );
   CntFrgColor.Clear (   );
   RgnBkgColor.Clear (   );
   RgnFrgColor.Clear (   );
   
   GrayHistogram.Create (NUM_GRAY_HISTOGRAM_BINS);
   Trajectory.Create    (OBT_PARAM_TRKOBJ_MAX_TRAJECTORY_LENGTH);
   memset (TypeLikelihood,0,sizeof(TypeLikelihood));
}

 void TrackedObject::AddToTrkObjRegionList (int trj_length)

{
   int i;

   if (TrkObjRegions.GetNumItems (   ) > trj_length) {
      TrkObjRegion* to_rgn = TrkObjRegions.Last (   );
      TrkObjRegions.Remove (to_rgn);
      delete to_rgn;
   }
   TrkObjRegion *to_rgn = new TrkObjRegion;
   to_rgn->SetRegion (*this);
   to_rgn->Area           = Area;
   to_rgn->FrameCount     = FrameCount;
   to_rgn->NumPersons     = NumPersons;
   to_rgn->Status         = Status;
   to_rgn->AspectRatio    = AspectRatio;
   to_rgn->MajorAxisAngle = MajorAxisAngle1;
   to_rgn->NorSpeed       = NorSpeed;
   to_rgn->Speed          = Speed;
   to_rgn->MeanVelocity   = MeanVelocity;
   to_rgn->Centroid       = Centroid;
   to_rgn->Velocity       = Velocity;
   TrkObjRegions.Add (to_rgn,LL_FIRST);
   if ((Count_Tracked % 3) == 1) {
      if (TrjLength >= Trajectory.Length) {
         for (i = 1; i < Trajectory.Length; i++) Trajectory[i - 1] = Trajectory[i];
         Trajectory[Trajectory.Length - 1] = CenterPos;
      }
      else Trajectory[TrjLength++] = CenterPos;
   }
}

 void TrackedObject::ClearStatus (   )

{
   if (Status & TO_STATUS_FMO_ENDED) {
      if (Status & TO_STATUS_MERGED) Status &= ~TO_STATUS_FMO_ENDED;
      else {
         Status &= ~(TO_STATUS_FAST_MOVING_OBJECT | TO_STATUS_FMO_ENDED);
         Velocity.Clear  (   );
      }
   }
   Status &= ~(
      TO_STATUS_ABANDONED_OBJECT | TO_STATUS_BEST_SHOT      | TO_STATUS_CHANGED_ABRUPTLY | TO_STATUS_GHOST_OBJECT   |
      TO_STATUS_HANDOFF          | TO_STATUS_IN_OUTER_ZONE  | TO_STATUS_MASK_PUT         | TO_STATUS_MATCHED_1TO1   |
      TO_STATUS_MERGED           | TO_STATUS_REMOVED_OBJECT | TO_STATUS_SPLIT            | TO_STATUS_TO_BE_REMOVED  |
      TO_STATUS_UNDETECTED       | TO_STATUS_VERIFIED_START
   );
}

 Event *TrackedObject::FindEvent (EventZone *evt_zone,int evt_type)
 
{
   Event *event = Events.First (   );
   while (event != NULL) {
      if (event->EvtZone == evt_zone) {
         if (evt_type < 0) return (event);
         else if (event->Type == evt_type) return (event);
      }
      event = Events.Next (event);
   }
   return (NULL);
}

// jun: ์ถ๊ฐ, path passing
 TrackedObject::EventZonePathPassingInfo* TrackedObject::FindEventZonePathPassingInfo (EventZone* evt_zone)

{
   EventZonePathPassingInfo* evt_pp_info = EventZonePathPassingInfoList.First (   );
   while (evt_pp_info) {
      if (evt_pp_info->EvtZone == evt_zone)
         return evt_pp_info;
      evt_pp_info = EventZonePathPassingInfoList.Next (evt_pp_info);
   }
   return (0);
}

 Path *TrackedObject::FindPath (EventZone *evt_zone)
 
{
   Path *path = Paths.First (   );
   while (path != NULL) {
      if (path->EvtZone == evt_zone) return (path);
      path = Paths.Next (path);
   }
   return (NULL);
}

 void TrackedObject::GetArea (   )

{
   Area = 0.25f * MC_PI * MajorAxisLength * MinorAxisLength;
}

 int TrackedObject::GetAreaOfSupportedRegion (SArray2D &s_array,short thrsld)

{
   int x1,y1,x2,y2;
   
   int sx1 = X;
   int sy1 = Y;
   int ex1 = sx1 + MaskImage.Width;
   int ey1 = sy1 + MaskImage.Height;
   if (sx1 < 0) sx1 = 0;
   if (sy1 < 0) sy1 = 0;
   if (ex1 > s_array.Width ) ex1 = s_array.Width;
   if (ey1 > s_array.Height) ey1 = s_array.Height;
   int sx2 = sx1 - X;
   int sy2 = sy1 - Y;
   int area = 0;
   for (y1 = sy1,y2 = sy2; y1 < ey1; y1++,y2++) {
      byte*  l_m_image = MaskImage[y2];
      short* l_s_array = s_array[y1];
      for (x1 = sx1,x2 = sx2; x1 < ex1; x1++,x2++) {
         if (l_m_image[x2] && (l_s_array[x1] >= thrsld)) area++;
      }
   }
   return (area);
}

 void TrackedObject::GetAreaVariation (   )
 
{
   if (Count_Tracked <= 1) {
      AreaVariation = 0.0f;
      return;
   }
   TrkObjRegion *to_rgn = TrkObjRegions.First (   );
   int a1 = Width * Height;
   int a2 = to_rgn->Width * to_rgn->Height;
   int min_a = GetMinimum (a1,a2);
   AreaVariation = (float)abs (a1 - a2) / min_a;
}

 void TrackedObject::GetAspectRatio (   )

{
   AspectRatio = (float)Height / Width;
}

 void TrackedObject::GetAxisLengthRatio (   )

{
   AxisLengthRatio = MajorAxisLength / MinorAxisLength;
}

 float TrackedObject::GetBestShotScore (ISize2D &si_size)
 
{
   // ํ๊ท  ๋ฉด์ ๊ณผ ํ์ฌ ์ถ์  ๋ฉด์ ์ด ๊ฐ์์๋ก ์ข๋ค.
   float score1 = Area / AvgArea;
   if (score1 > 1.0f) score1 = 1.0f / score1;
   // ํ์ฌ ์ถ์  ๋ฉด์  ๋ด์ ๊ฐ์ง๋ ํฝ์์ ๋ฐ๋๊ฐ ๋์์๋ก ์ข๋ค.
   float score2 = NumRgnPixels / Area;
   if (score2 > 1.0f) score2 = 1.0f / score2;
   // ์ฌ๋์ผ๋ก ๋ถ๋ฅ๋ ๊ฒฝ์ฐ ๋์ด/๋๋น์ ๋น๊ฐ ์จ์ ํ ์ฌ๋ ๋ชจ์์ ๊ฐ๊น์ธ์๋ก ์ข๋ค.
   float score3 = 0.0f;
   if (Type == TO_TYPE_HUMAN) {
      if (2.0f <= AspectRatio2 && AspectRatio2 <= 4.5f) score3 = 1.0f;
   }
   // ์ฐจ๋์ผ๋ก ๋ถ๋ฅ๋ ๊ฒฝ์ฐ ์ฅ์ถ/๋จ์ถ์ ๊ธธ์ด ๋น๊ฐ ์จ์ ํ ์๋์ฐจ ๋ชจ์์ ๊ฐ๊น์ธ์๋ก ์ข๋ค.
   float score4 = 0.0f;
   if (Type == TO_TYPE_VEHICLE) {
      score4 = 2.5f / AxisLengthRatio;
      if (score4 > 1.0f) score4 = 1.0f / score4;
   }
   // ๊ฐ์ฒด๊ฐ ์์์ ๊ฐ์ฅ ์๋ฆฌ์ ์์ผ๋ฉด ์ข์ง ์๋ค.
   float score5 = 0.0f;
   int ex = X + Width  - 1;
   int ey = Y + Height - 1;
   if (X < 3 || Y < 3 || ex > si_size.Width - 4 || ey > si_size.Height - 4) score5 = -1.0f;
   // ๊ฐ์ฒด๊ฐ ์นด๋ฉ๋ผ์ ๊ฐ๊น์ธ์๋ก ์ข๋ค.
   float cy = 0.7f * si_size.Height;
   float score6 = 1.0f - fabs (CenterPos.Y - cy) / cy;
   // ๊ฐ์ฒด์ ํฌ๊ธฐ๊ฐ ํ๊ท  ํฌ๊ธฐ๋ณด๋ค ํด ์๋ก ์ข๋ค.
   float score7 = 0.0f;
   if (Area > AvgArea) score7 = 1.0f;
   float score = 1.0f * score1 + 1.3f * score2 + 0.7f * score3 +  1.0f * score4 + 1.0f * score5 + 0.7f * score6 + 1.5f * score7;
   return (score);
}

 void TrackedObject::GetClippedBoundingBox (int si_width,int si_height,IPoint2D& lt_pos,IPoint2D &rb_pos,IPoint2D& c_pos)
 
{
   lt_pos.X = X;
   lt_pos.Y = Y;
   rb_pos.X = lt_pos.X + Width  - 1;
   rb_pos.Y = lt_pos.Y + Height - 1;
   if (lt_pos.X < 0) lt_pos.X = 0;
   if (lt_pos.Y < 0) lt_pos.Y = 0;
   if (rb_pos.X >= si_width ) rb_pos.X = si_width  - 1;
   if (rb_pos.Y >= si_height) rb_pos.Y = si_height - 1;
   c_pos.X = (lt_pos.X + rb_pos.X) / 2;
   c_pos.Y = (lt_pos.Y + rb_pos.Y) / 2;
}

 void TrackedObject::GetColorHistogram (BGRImage &s_image,IArray2D &or_map,GImage &fg_image)

{
   int x,y;
   
   if (!ObjRgnID) return;
   ColorHistogram.Clear (   );
   int sx = ObjRgnBndBox.X;
   int sy = ObjRgnBndBox.Y;
   int ex = sx + ObjRgnBndBox.Width;
   int ey = sy + ObjRgnBndBox.Height;
   if (sx < 1) sx = 1;
   if (sy < 1) sy = 1;
   if (ex >= s_image.Width ) ex = s_image.Width  - 1;
   if (ey >= s_image.Height) ey = s_image.Height - 1;
   #if defined(__DEBUG_OBS)
   int ti_width  = ex - sx;
   int ti_height = ey - sy;
   BGRImage t_image(ti_width * 2,ti_height);
   t_image.Copy (s_image,sx,sy,ti_width,ti_height,0,0);
   t_image.Set  (ti_width,0,ti_width,ti_height,BGRPixel(59,157,165));
   int offset_x = ti_width - sx;
   int offset_y = -sy;
   #endif
   for (y = sy; y < ey; y++) {
      BGRPixel* l_s_image   = s_image [y];
      byte*     l_fg_image  = fg_image[y];
      byte*     l_fg_image1 = fg_image[y - 1];
      byte*     l_fg_image2 = fg_image[y + 1];
      int*      l_or_map    = or_map[y];
      for (x = sx; x < ex; x++) {
         if (l_or_map[x] == ObjRgnID && l_fg_image[x]) {
            if (l_fg_image[x - 1] && l_fg_image[x + 1] && l_fg_image1[x] && l_fg_image2[x]) {
               ColorHistogram.Accumulate (l_s_image[x]);
               #if defined(__DEBUG_OBS)
               HVHistogram::GetRepresentativeColor (l_s_image[x],t_image[offset_y + y][offset_x + x]);
               #endif
            }
         }
      }
   }
   #if defined(__DEBUG_OBS)
   char file_name[50];
   sprintf (file_name,"D:\\ObjectSamples\\Object_ID=%05d.bmp",ID);
   t_image.WriteFile (file_name);
   #endif
}

 void TrackedObject::GetDispersedness (   )

{
   int n_bps = GetRegionPerimeter (MaskImage);
   if (NumRgnPixels) Dispersedness = (float)(n_bps * n_bps) / NumRgnPixels;
   else Dispersedness = MC_VLV;
}

 void TrackedObject::GetDisplacement (   )

{
   if (Count_Tracked <= 1) return;
   TrkObjRegion* to_rgn = TrkObjRegions.First (   );
   FPoint2D p_pos(to_rgn->X + to_rgn->Centroid.X,to_rgn->Y + to_rgn->Centroid.Y);
   FPoint2D c_pos(X + Centroid.X,Y + Centroid.Y);
   FPoint2D d_pos1 = c_pos - p_pos;
   FPoint2D d_pos2 = CenterPos - to_rgn->GetCenterPosition (   );
   if (Mag(d_pos1) < Mag(d_pos2)) Displacement += d_pos1;
   else Displacement += d_pos2;
}

 FPoint2D TrackedObject::GetDisplacement (int n_frames)

{
   FPoint2D dv(0.0f,0.0f);
   TrkObjRegion* to_rgn = TrkObjRegions.Get (n_frames);
   if (to_rgn == NULL) to_rgn = TrkObjRegions.Last (   );
   if (to_rgn != NULL) dv = GetCenterPosition (   ) - to_rgn->GetCenterPosition (   );
   return (dv);
}

 TrackedObject::EvtPrgTime* TrackedObject::GetEventProgressTime (EventZone* evt_zone)
 // [์ฃผ์] ํฅ ํ ํ๋์ ์ด๋ฒคํธ ์กด์์ ๋ณต์์ ์ด๋ฒคํธ ์ข๋ฅ๋ฅผ ์ง์ํ๋ ค๋ฉด,
 //        ์ด๋ฒคํธ ์กด๋ง ์ฒดํฌํ  ๊ฒ์ด ์๋๋ผ ์ด๋ฒคํธ ์ข๋ฅ๋ ์ฒดํฌํด์ผ ํ๋ค.
{
   EvtPrgTime* ep_time = EvtPrgTimeList.First (   );
   while (ep_time != NULL) {
      if (ep_time->EvtZone == evt_zone) break;
      ep_time = EvtPrgTimeList.Next (ep_time);
   }
   if (ep_time == NULL) {
      ep_time = new EvtPrgTime;
      ep_time->EvtZone = evt_zone;
      EvtPrgTimeList.Add (ep_time);
   }
   return (ep_time);
}

 void TrackedObject::GetMajorAxisAngle1 (   )

{
   MajorAxisAngle1 = MC_RAD2DEG * (float)asin (MajorAxis.X);
}

 void TrackedObject::GetMajorAxisAngle2 (   )

{
   float mvm = Mag(MeanVelocity);
   if (mvm > 0.05f) MajorAxisAngle2 = MC_RAD2DEG * (float)acos (fabs(IP(MajorAxis,MeanVelocity / mvm)));
   else MajorAxisAngle2 = -1.0f;
}

 void TrackedObject::GetMatchingPosition (FrgRegion& fg_rgn)
 
{
   MatchingPos.X = (int)(fg_rgn.CX - Centroid.X);
   MatchingPos.Y = (int)(fg_rgn.CY - Centroid.Y);   
} 

 int TrackedObject::GetMatchingPosition (FrgRegion &fg_rgn,float min_sc,float max_sc)

{
   int min_w = (int)(min_sc * Width);
   int max_w = (int)(max_sc * Width);
   int min_h = (int)(min_sc * Height);
   int max_h = (int)(max_sc * Height);
   if (min_w <= fg_rgn.Width && fg_rgn.Width <= max_w && min_h <= fg_rgn.Height && fg_rgn.Height <= max_h) {
      // Observed Position์ ํ๋ํ๋ค.
      FPoint2D o_pos(fg_rgn.CX - Centroid.X,fg_rgn.CY - Centroid.Y);
      // Predicted Position์ ํ๋ํ๋ค.
      FPoint2D p_pos;
      GetPredictedPosition (p_pos);
      float ar = (float)(fg_rgn.Width * fg_rgn.Height) / (Width * Height);
      if (ar > 1.0f) ar = 1.0f / ar;
      // Observed Position๊ณผ Predicted Position์ ๊ฐ์ค ํฉ์ ๊ตฌํ๋ค.
      // fg_rgn๊ณผ ๋ณธ ๊ฐ์ฒด์ ๋ฉด์ ์ด ์ ์ฌํ ์๋ก Observed Position์ ๋ ๋ง์ ๊ฐ์ค์น๊ฐ ์ฃผ์ด์ง๋ค.
      FPoint2D m_pos = ar * o_pos + (1.0f - ar) * p_pos;
      MatchingPos.X = (int)(m_pos.X + 0.5f);
      MatchingPos.Y = (int)(m_pos.Y + 0.5f);
      return (DONE);
   }
   else return (1);
}

 void TrackedObject::GetMatchingPosition (GImage &s_image,GImage &m_image,IPoint2D &s_pos,int s_range)

{
   int x,y;
   
   int sx = s_pos.X - s_range;
   int sy = s_pos.Y - s_range;
   int ex = s_pos.X + s_range;
   int ey = s_pos.Y + s_range;
   int dx = Width  / 32 + 1;
   int dy = Height / 32 + 1;
   MatchingScore = MC_VLV;
   for (y = sy; y <= ey; y++) {
      for (x = sx; x <= ex; x++) {
         float score = GetMatchingScore (s_image,m_image,x,y,dx,dy);
         if (score < MatchingScore) {
            MatchingScore = score;
            MatchingPos(x,y);
         }
      }
   }
}

 float TrackedObject::GetMatchingScore (GImage &s_image,GImage &m_image,int ox,int oy,int dx,int dy)

{
   int x1,y1;

   int sx = ox;
   int sy = oy;
   int ex = sx + Width;
   int ey = sy + Height;
   if (sx < 0) sx = 0;
   if (sy < 0) sy = 0;
   if (ex > s_image.Width ) ex = s_image.Width;
   if (ey > s_image.Height) ey = s_image.Height;
   uint sum_w = 0, sum_wad = 0;
   for (y1 = sy; y1 < ey; y1 += dy) {
      int y2 = y1 - oy;
      byte* l_s_image = s_image[y1];
      byte* l_m_image = m_image[y1];
      byte* l_t_image = TemplateImage[y2];
      byte* l_w_image = WeightImage[y2];
      for (x1 = sx; x1 < ex; x1 += dx) {
         int x2 = x1 - ox;
         ushort w = (ushort)l_m_image[x1] * l_w_image[x2];
         sum_w   += w;
         sum_wad += w * abs((short)l_s_image[x1] - l_t_image[x2]);
      }
   }
   if (!sum_w) return (MC_VLV);
   else {
      double score = (double)sum_wad / sum_w;
      return ((float)score);
   }
}

 void TrackedObject::GetMaxMovingDistance (   )

{
   float md = Mag(Displacement);
   if (md > MaxMovingDistance) MaxMovingDistance = md;
}

 void TrackedObject::GetMeanArea (int n_frames)

{
   int n;
   
   float area = 0.0f;
   TrkObjRegion* to_rgn = TrkObjRegions.First (   );
   for (n = 0; n < n_frames;   ) {
      if (to_rgn == NULL) break;
      area += to_rgn->Area;
      n++;
      to_rgn = TrkObjRegions.Next (to_rgn);
   }
   if (n) MeanArea = area / n;
   else MeanArea = 0.0f;
}

 void TrackedObject::GetMeanSizeVelocity (int n_frames,FPoint2D& d_velocity)
 
{
   int n;
   
   d_velocity.Clear (   );
   if (n_frames < 2) return;
   IBox2D p_rgn(0,0,0,0);
   TrkObjRegion *to_rgn = TrkObjRegions.First (   );
   for (n = 0; n < n_frames; n++) {
      if (to_rgn == NULL) break;
      if (p_rgn.Width && p_rgn.Height) {
          d_velocity.X += p_rgn.Width  - to_rgn->Width;
          d_velocity.Y += p_rgn.Height - to_rgn->Height;
      }
      p_rgn.Width  = to_rgn->Width;
      p_rgn.Height = to_rgn->Height;
      to_rgn = TrkObjRegions.Next (to_rgn);
   }
   d_velocity /= (float)(n - 1);
}

 void TrackedObject::GetMeanVelocity (int n_frames,FPoint2D& d_velocity)
 
{
   int n;
   
   d_velocity.Clear (   );
   TrkObjRegion *to_rgn = TrkObjRegions.First (   );
   for (n = 0; n < n_frames;   ) {
      if (to_rgn == NULL) break;
      if (to_rgn->Velocity.X != MC_VLV) {
         d_velocity += to_rgn->Velocity;
         n++;
      }
      to_rgn = TrkObjRegions.Next (to_rgn);
   }
   if (n) d_velocity /= (float)n;
   if (n == n_frames) Status |= TO_STATUS_VALID_SPEED;
   else Status &= ~TO_STATUS_VALID_SPEED;
}

 float TrackedObject::GetMovingDistance (   )

{
   int i;

   IPoint2D ip[4];
   InitRegion.GetCornerPoints (ip);
   IPoint2D cp[4];
   GetCornerPoints (cp);
   float min_md = MC_VLV;
   for (i = 0; i < 4; i++) {
      float md = Mag (cp[i] - ip[i]);
      if (md < min_md) min_md = md;
   }
   return (min_md);
}

 int TrackedObject::GetNumEvents (int pos_evt_type,int neg_evt_type)
 
{
   int n_events = 0;
   
   Event *event = Events.First (   );
   while (event != NULL) {
      if (!(event->Status & (EV_STATUS_ENDED|EV_STATUS_AFTER_ENDED|EV_STATUS_TO_BE_REMOVED))) {
         if (pos_evt_type < 0) {
            if (neg_evt_type < 0) n_events++;
            else if (event->Type != neg_evt_type) n_events++;
         }
         else if (event->Type == pos_evt_type) n_events++;
      }
      event = Events.Next (event);
   }
   return (n_events);
}
 
 void TrackedObject::GetPredictedPosition (FPoint2D &p_pos,float p_time,int flag_smooth)

{
   FPoint2D velocity = Velocity;
   if (flag_smooth) velocity = MeanVelocity;
   if (velocity.X == MC_VLV) p_pos((float)X,(float)Y);
   else {
      p_pos.X = X + velocity.X * p_time;
      p_pos.Y = Y + velocity.Y * p_time;
   }
}

 void TrackedObject::GetPredictedPosition (IPoint2D &p_pos,float p_time,int flag_smooth)

{
   FPoint2D d_pos;
   GetPredictedPosition (d_pos,p_time,flag_smooth);
   p_pos.X = (int)(d_pos.X + 0.5f);
   p_pos.Y = (int)(d_pos.Y + 0.5f);
}

 void TrackedObject::GetPredictedSize (ISize2D &p_size,float p_time) // jun: ์ถ๊ฐ, ํ๊ท  ๋ฐ์คํฌ๊ธฐ ๋ณํ์๋ 

{
   if (MeanSizeVelocity.X == MC_VLV) p_size(Width,Height);
   else {
      p_size.Width  = (int)(Width  + MeanSizeVelocity.X * p_time + 0.5f);
      p_size.Height = (int)(Height + MeanSizeVelocity.Y * p_time + 0.5f);
      if (p_size.Width  < 4) p_size.Width  = 4;
      if (p_size.Height < 4) p_size.Height = 4;
   }
}

 void TrackedObject::GetPrincipalAxes (   )

{
   int x,y;
   
   uint  sum_x  = 0,sum_y  = 0;
   int64 sum_xx = 0,sum_yy = 0,sum_xy = 0;
   for (y = 0; y < MaskImage.Height; y++) {
      byte *l_m_image = MaskImage[y];
      uint yy = y * y;
      for (x = 0; x < MaskImage.Width; x++) {
         if (l_m_image[x]) {
            sum_x  += x;
            sum_y  += y;
            sum_xx += x * x;
            sum_xy += x * y;
            sum_yy += yy;
         }
      }
   }
   Matrix mat_C(2,2),vec_m(2);
   vec_m(0)    = (double)sum_x  / NumRgnPixels;
   vec_m(1)    = (double)sum_y  / NumRgnPixels;
   mat_C[0][0] = (double)sum_xx / NumRgnPixels;
   mat_C[0][1] = (double)sum_xy / NumRgnPixels;
   mat_C[1][1] = (double)sum_yy / NumRgnPixels;
   mat_C[1][0] = mat_C[0][1];
   mat_C = mat_C - vec_m * Tr(vec_m);
   Centroid.X  = (float)vec_m(0);
   Centroid.Y  = (float)vec_m(1);
   Matrix mat_E(2,2),vec_e(2);
   EigenDecomp (mat_C,vec_e,mat_E);
   if (mat_E[1][0] < 0.0f) mat_E = -mat_E; // MajorAxis.Y ๊ฐ์ด ํญ์ ์์๊ฐ ๋๊ฒ ๋ง๋ ๋ค.
   if (vec_e(0) > MC_VSV && vec_e(1) > MC_VSV) {
      MajorAxisLength = 4.0f * (float)sqrt (vec_e(0));
      MinorAxisLength = 4.0f * (float)sqrt (vec_e(1));
      MajorAxis.X     = (float)mat_E[0][0];
      MajorAxis.Y     = (float)mat_E[1][0];
      MinorAxis.X     = (float)mat_E[0][1];
      MinorAxis.Y     = (float)mat_E[1][1];
   }
   else {
      if (Width > Height) {
         MajorAxisLength = (float)Width;
         MinorAxisLength = (float)Height;
         MajorAxis(1.0f,0.0f);
         MinorAxis(0.0f,1.0f);
      }
      else {
         MajorAxisLength = (float)Height;
         MinorAxisLength = (float)Width;
         MajorAxis(0.0f,1.0f);
         MinorAxis(1.0f,0.0f);
      }
   }
}

 void TrackedObject::GetRegionBoundaryEdgeInfo (ForegroundDetection& frg_detector,IArray2D& or_map)

{
   NumBndPixels        = 0;
   BndEdgeStrength     = 0.0f;
   NumFrgBndEdgePixels = 0;
   NumBkgBndEdgePixels = 0;
   if (!ObjRgnID) return;
   if (!frg_detector.IsFrgEdgeImageSupported (   )) return;
   int n_rbps;   // # of region boundary pixels
   int n_fbeps1; // # of foreground boundary edge pixels 1
   int n_fbeps2; // # of foreground boundary edge pixels 2
   int n_fbeps3; // # of foreground boundary edge pixels 3
   int n_bbeps;  // # of background boundary edge pixels
   if (VACL::GetRegionBoundaryEdgeInfo (frg_detector,or_map,*this,ObjRgnID,n_rbps,n_fbeps1,n_fbeps2,n_fbeps3,n_bbeps)) return;
   NumBndPixels        = n_rbps;
   BndEdgeStrength     = (float)n_fbeps3 / n_rbps;
   NumFrgBndEdgePixels = n_fbeps2;
   NumBkgBndEdgePixels = n_bbeps;
}

 void TrackedObject::GetSpeeds (float frm_rate)

{
   Speed    = Mag(MeanVelocity) * frm_rate;
   NorSpeed = 100.0f * Speed / Height;
}

 void TrackedObject::InitTemplate (GImage &s_image,IArray2D &or_map,IBox2D &s_rgn,int s_rgn_id,BGRImage *s_cimage)

{
   int x1,y1,x2,y2;

   ObjRgnBndBox = s_rgn;
   ObjRgnID  = s_rgn_id;
   SetRegion (s_rgn);
   InitRegion = s_rgn;
   MaskImage.Create (Width,Height);
   MaskImage.Clear (   );
   WeightImage.Create (Width,Height);
   WeightImage.Clear (   );
   TemplateImage.Create (Width,Height);
   GrayHistogram.Clear (   );
   if (s_cimage != NULL) ColorHistogram.Initialize (   );
   int ex = X + Width  - 1;
   int ey = Y + Height - 1;
   for (y1 = Y,y2 = 0; y1 <= ey; y1++,y2++) {
      byte* l_s_image = s_image[y1];
      byte* l_m_image = MaskImage[y2];
      byte* l_w_image = WeightImage[y2];
      byte* l_t_image = TemplateImage[y2];
      int*  l_or_map  = or_map[y1];
      for (x1 = X,x2 = 0; x1 <= ex; x1++,x2++) {
         l_t_image[x2] = l_s_image[x1];
         if (l_or_map[x1] == s_rgn_id) {
            l_w_image[x2] = 128;
            l_m_image[x2] = PG_WHITE;
            GrayHistogram.Accumulate (GET_GRAY_HISTOGRAM_BIN_INDEX_NO(l_s_image[x1]));
            NumRgnPixels++;
         }
      }
   }
}

 int TrackedObject::IsInactive (float a)
 
{
   if (!Rgn_Inactive.Width || !Rgn_Inactive.Height) return (FALSE);
   float d        = Mag(CenterPos - Rgn_Inactive.GetCenterPosition());
   float d_thrsld = a * (Rgn_Inactive.Width + Rgn_Inactive.Height) / 2.0f;
   if (d < d_thrsld) return (TRUE);
   else return (FALSE);
}

 int TrackedObject::IsInMarginalArea (ISize2D& si_size)

{
   int margin = (int)(si_size.Height * 0.05f);
   int sx1 = margin;
   int sy1 = margin;
   int ex1 = si_size.Width  - margin;
   int ey1 = si_size.Height - margin;
   int sx2 = X;
   int sy2 = Y;
   int ex2 = sx2 + Width;
   int ey2 = sy2 + Height;
   if (sx1 < sx2 && ex2 < ex1 && sy1 < sy2 && ey2 < ey1) return (FALSE);
   else return (TRUE);
}

 int TrackedObject::IsStatic (   )
 
{
   int i;
   
   if (!Rgn_Static.Width || !Rgn_Static.Height) return (FALSE);
   IP2DArray1D cps1(4);
   GetCornerPoints (cps1);
   IP2DArray1D cps2(4);
   Rgn_Static.GetCornerPoints (cps2);
   float max_d = 0.0f;
   for (i = 0; i < 4; i++) {
      float d = Mag (cps1[i] - cps2[i]);
      if (d > max_d) max_d = d;
   }
   float d_thrsld = 0.3f * 0.5f * (Rgn_Static.Width + Rgn_Static.Height); // PARAMETERS
   if (max_d < d_thrsld && max_d < 15.0f) return (TRUE); // PARAMETERS
   else return (FALSE);
}

 void TrackedObject::Merge (TrackedObject *s_object)
 
{
   Area                = s_object->Area;
   AspectRatio         = s_object->AspectRatio;
   AxisLengthRatio     = s_object->AxisLengthRatio;
   MajorAxisLength     = s_object->MajorAxisLength;
   MinorAxisLength     = s_object->MinorAxisLength;
   NumRgnPixels        = s_object->NumRgnPixels;
   NorSpeed            = s_object->NorSpeed;
   Speed               = s_object->Speed;
   RealArea            = s_object->RealArea;
   RealDistance        = s_object->RealDistance;
   RealWidth           = s_object->RealWidth;
   RealHeight          = s_object->RealHeight;
   RealMajorAxisLength = s_object->RealMajorAxisLength;
   RealMinorAxisLength = s_object->RealMinorAxisLength;
   RealSpeed           = s_object->RealSpeed;
   CenterPos           = s_object->CenterPos;      
   Centroid            = s_object->Centroid;
   MajorAxis           = s_object->MajorAxis;      
   MinorAxis           = s_object->MinorAxis;
   RealPos             = s_object->RealPos;
   Velocity            = s_object->Velocity;
   MatchingPos         = s_object->MatchingPos;    
   MaskImage           = s_object->MaskImage;
   WeightImage         = s_object->WeightImage;
   TemplateImage       = s_object->TemplateImage;
   SetRegion (*s_object);
}

 void TrackedObject::PutMask (int sx,int sy,byte m_value,GImage &d_image,int flag_overwrite)

{
   int x1,y1,x2,y2;
   
   int sx1 = sx;
   int sy1 = sy;
   int ex1 = sx1 + MaskImage.Width;
   int ey1 = sy1 + MaskImage.Height;
   if (sx1 < 1) sx1 = 1;
   if (sy1 < 1) sy1 = 1;
   if (ex1 > d_image.Width  - 1) ex1 = d_image.Width  - 1;
   if (ey1 > d_image.Height - 1) ey1 = d_image.Height - 1;
   int sx2 = sx1 - sx;
   int sy2 = sy1 - sy;
   if (flag_overwrite) {
      for (y1 = sy1,y2 = sy2; y1 < ey1; y1++,y2++) {
         byte* l_d_image = d_image[y1];
         byte* l_m_image = MaskImage[y2];
         for (x1 = sx1,x2 = sx2; x1 < ex1; x1++,x2++)
            if (l_m_image[x2]) l_d_image[x1] = m_value;
      } 
   }
   else {
      for (y1 = sy1,y2 = sy2; y1 < ey1; y1++,y2++) {
         byte* l_d_image = d_image[y1];
         byte* l_m_image = MaskImage[y2];
         for (x1 = sx1,x2 = sx2; x1 < ex1; x1++,x2++)
            if (l_m_image[x2] && !l_d_image[x1]) l_d_image[x1] = m_value;
      } 
   }
}

 void TrackedObject::PutMask (int sx,int sy,int m_value,IArray2D &d_array,int flag_overwrite)

{
   int x1,y1,x2,y2;
   
   int sx1 = sx;
   int sy1 = sy;
   int ex1 = sx1 + MaskImage.Width;
   int ey1 = sy1 + MaskImage.Height;
   if (sx1 < 1) sx1 = 1;
   if (sy1 < 1) sy1 = 1;
   if (ex1 > d_array.Width  - 1) ex1 = d_array.Width  - 1;
   if (ey1 > d_array.Height - 1) ey1 = d_array.Height - 1;
   int sx2 = sx1 - sx;
   int sy2 = sy1 - sy;
   if (flag_overwrite) {
      for (y1 = sy1,y2 = sy2; y1 < ey1; y1++,y2++) {
         int*  l_d_array = d_array[y1];
         byte* l_m_image = MaskImage[y2];
         for (x1 = sx1,x2 = sx2; x1 < ex1; x1++,x2++)
            if (l_m_image[x2]) l_d_array[x1] = m_value;
      } 
   }
   else {
      for (y1 = sy1,y2 = sy2; y1 < ey1; y1++,y2++) {
         int*  l_d_array = d_array[y1];
         byte* l_m_image = MaskImage[y2];
         for (x1 = sx1,x2 = sx2; x1 < ex1; x1++,x2++)
            if (l_m_image[x2] && !l_d_array[x1]) l_d_array[x1] = m_value;
      } 
   }
}

 void TrackedObject::PutMask (int sx,int sy,ushort m_value,USArray2D &d_array,int flag_overwrite)

{
   int x1,y1,x2,y2;
   
   int sx1 = sx;
   int sy1 = sy;
   int ex1 = sx1 + MaskImage.Width;
   int ey1 = sy1 + MaskImage.Height;
   if (sx1 < 1) sx1 = 1;
   if (sy1 < 1) sy1 = 1;
   if (ex1 > d_array.Width  - 1) ex1 = d_array.Width  - 1;
   if (ey1 > d_array.Height - 1) ey1 = d_array.Height - 1;
   int sx2 = sx1 - sx;
   int sy2 = sy1 - sy;
   if (flag_overwrite) {
      for (y1 = sy1,y2 = sy2; y1 < ey1; y1++,y2++) {
         ushort* l_d_array = d_array[y1];
         byte*   l_m_image = MaskImage[y2];
         for (x1 = sx1,x2 = sx2; x1 < ex1; x1++,x2++)
            if (l_m_image[x2]) l_d_array[x1] = m_value;
      } 
   }
   else {
      for (y1 = sy1,y2 = sy2; y1 < ey1; y1++,y2++) {
         ushort* l_d_array = d_array[y1];
         byte*   l_m_image = MaskImage[y2];
         for (x1 = sx1,x2 = sx2; x1 < ex1; x1++,x2++)
            if (l_m_image[x2] && !l_d_array[x1]) l_d_array[x1] = m_value;
      } 
   }
}

 void TrackedObject::PutRectMask (int sx,int sy,byte m_value,GImage &d_image,int margin)
 
{
   int x,y;
   
   int sx1 = sx - margin;
   int sy1 = sy - margin;
   int ex1 = sx + Width  + margin;
   int ey1 = sy + Height + margin;
   if (sx1 < 0) sx1 = 0;
   if (sy1 < 0) sy1 = 0;
   if (ex1 >= d_image.Width ) ex1 = d_image.Width;
   if (ey1 >= d_image.Height) ey1 = d_image.Height;
   for (y = sy1; y < ey1; y++) {
      byte* l_d_image = d_image[y];
      for (x = sx1; x < ex1; x++) l_d_image[x] = m_value;
   }
}

 void TrackedObject::PutRectMask (int sx,int sy,int m_value,IArray2D& d_array,int margin)
 
{
   int x,y;
   
   int sx1 = sx - margin;
   int sy1 = sy - margin;
   int ex1 = sx + Width  + margin;
   int ey1 = sy + Height + margin;
   if (sx1 < 0) sx1 = 0;
   if (sy1 < 0) sy1 = 0;
   if (ex1 >= d_array.Width ) ex1 = d_array.Width;
   if (ey1 >= d_array.Height) ey1 = d_array.Height;
   for (y = sy1; y < ey1; y++) {
      int* l_d_array = d_array[y];
      for (x = sx1; x < ex1; x++) l_d_array[x] = m_value;
   }
}

 void TrackedObject::PutRectMask (int sx,int sy,ushort m_value,USArray2D& d_array,int margin)
 
{
   int x,y;
   
   int sx1 = sx - margin;
   int sy1 = sy - margin;
   int ex1 = sx + Width  + margin;
   int ey1 = sy + Height + margin;
   if (sx1 < 0) sx1 = 0;
   if (sy1 < 0) sy1 = 0;
   if (ex1 >= d_array.Width ) ex1 = d_array.Width;
   if (ey1 >= d_array.Height) ey1 = d_array.Height;
   for (y = sy1; y < ey1; y++) {
      ushort* l_d_array = d_array[y];
      for (x = sx1; x < ex1; x++) l_d_array[x] = m_value;
   }
}

 void TrackedObject::SetRegion (IBox2D &s_rgn)
 
{
   X         = s_rgn.X;
   Y         = s_rgn.Y;
   Width     = s_rgn.Width;
   Height    = s_rgn.Height;
   CenterPos = GetCenterPosition (   );
}

 void TrackedObject::SetRegion_Inactive (   )
 
{
   Rgn_Inactive = *(IBox2D*)this;
}

 void TrackedObject::SetRegion_Static (   )
 
{
   Rgn_Static = *(IBox2D*)this;
}

 void TrackedObject::ResetRegion_Inactive (   )
 
{
   Rgn_Inactive.Clear (   );
}

 void TrackedObject::ResetRegion_Static (   )
 
{
   Rgn_Static.Clear (   );
}

 void TrackedObject::UpdateAvgMinMaxPropertyValues (   )
 
{
   float a = (float)(Count_Tracked - 1) / Count_Tracked;
   float b = 1.0f / Count_Tracked;

   // ํ๊ท ๊ฐ ๊ณ์ฐ
   AvgWidth               = a * AvgWidth               + b * Width;
   AvgHeight              = a * AvgHeight              + b * Height;
   AvgArea                = a * AvgArea                + b * Area;
   AvgAspectRatio         = a * AvgAspectRatio         + b * AspectRatio;
   AvgAxisLengthRatio     = a * AvgAxisLengthRatio     + b * AxisLengthRatio;
   AvgMajorAxisLength     = a * AvgMajorAxisLength     + b * MajorAxisLength;
   AvgMinorAxisLength     = a * AvgMinorAxisLength     + b * MinorAxisLength;
   AvgNorSpeed            = a * AvgNorSpeed            + b * NorSpeed;
   AvgSpeed               = a * AvgSpeed               + b * Speed;
   AvgRealArea            = a * AvgRealArea            + b * RealArea;
   AvgRealDistance        = a * AvgRealDistance        + b * RealDistance;
   AvgRealWidth           = a * AvgRealWidth           + b * RealWidth;
   AvgRealHeight          = a * AvgRealHeight          + b * RealHeight;
   AvgRealMajorAxisLength = a * AvgRealMajorAxisLength + b * RealMajorAxisLength;
   AvgRealMinorAxisLength = a * AvgRealMinorAxisLength + b * RealMinorAxisLength;
   AvgRealSpeed           = a * AvgRealSpeed           + b * RealSpeed;

   // ์ต๋๊ฐ ๊ณ์ฐ
   if (Width           > MaxWidth          ) MaxWidth           = Width;
   if (Height          > MaxHeight         ) MaxHeight          = Height;
   if (Area            > MaxArea           ) MaxArea            = Area;
   if (AspectRatio     > MaxAspectRatio    ) MaxAspectRatio     = AspectRatio;
   if (AxisLengthRatio > MaxAxisLengthRatio) MaxAxisLengthRatio = AxisLengthRatio;
   if (MajorAxisLength > MaxMajorAxisLength) MaxMajorAxisLength = MajorAxisLength;
   if (MinorAxisLength > MaxMinorAxisLength) MaxMinorAxisLength = MinorAxisLength;
   if (NorSpeed        > MaxNorSpeed       ) MaxNorSpeed        = NorSpeed;
   if (Speed           > MaxSpeed          ) MaxSpeed           = Speed;
   if (RealDistance    > MaxRealDistance   ) MaxRealDistance    = RealDistance;
   if (RealSpeed       > MaxRealSpeed      ) MaxRealSpeed       = RealSpeed;

   // ์ต์๊ฐ ๊ณ์ฐ
   if (Width           < MinWidth          ) MinWidth           = Width;
   if (Height          < MinHeight         ) MinHeight          = Height;
   if (Area            < MinArea           ) MinArea            = Area;
   if (AspectRatio     < MinAspectRatio    ) MinAspectRatio     = AspectRatio;
   if (AxisLengthRatio < MinAxisLengthRatio) MinAxisLengthRatio = AxisLengthRatio;
   if (MajorAxisLength < MinMajorAxisLength) MinMajorAxisLength = MajorAxisLength;
   if (MinorAxisLength < MinMinorAxisLength) MinMinorAxisLength = MinorAxisLength;
   if (NorSpeed        < MinNorSpeed       ) MinNorSpeed        = NorSpeed;
   if (Speed           < MinSpeed          ) MinSpeed           = Speed;
   if (RealDistance    < MinRealDistance   ) MinRealDistance    = RealDistance;
   if (RealSpeed       < MinRealSpeed      ) MinRealSpeed       = RealSpeed;
}

 void TrackedObject::UpdateCountsAndTimes (float frm_duration)

{
   Count_Tracked++;
   Time_BestShot += frm_duration;
   Time_Tracked  += frm_duration;
   if (!(Status & TO_STATUS_CHANGED_ABRUPTLY)) Time_ChangedAbruptly = 0.0f;
   if (!(Status & TO_STATUS_MERGED          )) Time_Merged = 0.0f;
   if (!(Status & TO_STATUS_UNDETECTED      )) Time_Undetected = 0.0f;
}

 void TrackedObject::UpdateGrayHistogram (GImage &s_image,IArray2D &or_map)

{
   int x,y;

   if (!ObjRgnID || (Status & (TO_STATUS_MERGED|TO_STATUS_UNDETECTED))) return;
   int sx = ObjRgnBndBox.X;
   int sy = ObjRgnBndBox.Y;
   int ex = sx + ObjRgnBndBox.Width;
   int ey = sy + ObjRgnBndBox.Height;
   for (y = sy; y < ey; y++) {
      byte* l_s_image  = s_image[y];
      int*  l_or_map = or_map[y];
      for (x = sx; x < ex; x++) {
         if (l_or_map[x] == ObjRgnID) GrayHistogram.Accumulate (GET_GRAY_HISTOGRAM_BIN_INDEX_NO(l_s_image[x]));
      }
   }
}

 void TrackedObject::UpdateNoisiness (ISize2D& si_size)

{
   float noisiness = 0.0f;
   // ๊ฐ์ฒด์ ๋ฐ์ด๋ฉ ๋ฐ์ค๊ฐ ์์์ ๊ฐ์ฅ์๋ฆฌ์ ๊ฑธ์ณ ์์ผ๋ฉด ๊ฐ์ฒด๊ฐ ์์ ์์ผ๋ก ์ง์ํ๊ณ  ์์ ๊ฐ๋ฅ์ฑ์ด
   // ํฌ๋ฏ๋ก ํต์์ ์ผ๋ก AreaVariation์ด ํด ์ ๋ฐ์ ์๋ค. ๋ฐ๋ผ์ ์ด๋๋ AreaVariation์ ์ฒดํฌํ์ง ์๋๋ค.
   if (!IsInMarginalArea (si_size)) {
      // Area Variation
      if      (AreaVariation > 1.0f) noisiness += 0.8f;
      else if (AreaVariation > 0.5f) noisiness += 0.4f;
   }
   // Dispersedness
   if (BndEdgeStrength < 0.7f) {
      if      (60.0f <= Dispersedness && Dispersedness <= 90.0f) noisiness += 0.4f;
      else if (Dispersedness > 90.0f) noisiness += 0.8f;
   }
   // Moving Distance
   float md = Mag(Displacement);
   if (md < MaxMovingDistance) {
      float d = 0.2f * GetMinimum (Width,Height);
      if (d < 2.0f) d = 2.0f;
      if (md <= (MaxMovingDistance - d)) noisiness += 0.8f;
   }
   // Normalized Speed
   if (NorSpeed >= 150.0f) {
      if (Height <= 6) noisiness += 0.8f;
   }
   Noisiness += noisiness;
   #if defined(__DEBUG_OBT) && defined(__CONSOLE)
   if (ID == 0) {
      logd ("[Tracked Object #%05d]\n",ID);
      logd ("* AreaVariation = %.2f, Area = %.2f, BndEdgeStrength = %.2f, Dispersedness = %.2f\n",
         AreaVariation,Area,BndEdgeStrength,Dispersedness);
      logd ("* MovingDistance = %.2f, MaxMovingDistance = %.2f, NorSpeed = %.2f\n",
         md,MaxMovingDistance,NorSpeed);
      logd ("* noisiness = %.2f, Noisiness = %.2f\n",noisiness,Noisiness);
   }
   #endif
}

 void TrackedObject::UpdateRegion1 (IBox2D &s_rgn,float vu_rate)

{
   if (vu_rate >= 0.0f) {
      FPoint2D dp = s_rgn.GetCenterPosition (   ) - CenterPos;
      if (Velocity.X == MC_VLV) Velocity = dp;
      else Velocity = dp * vu_rate + Velocity * (1.0f - vu_rate);
   }
   else Velocity(MC_VLV,MC_VLV);
   SetRegion (s_rgn);
}

 void TrackedObject::UpdateRegion2 (IBox2D &s_rgn,float ru_rate)

{
   IBox2D n_rgn;
   n_rgn.Width  = (int)(Width  + ru_rate * (s_rgn.Width  - Width ) + 0.5f);
   n_rgn.Height = (int)(Height + ru_rate * (s_rgn.Height - Height) + 0.5f);
   n_rgn.X      = s_rgn.X + (s_rgn.Width  - n_rgn.Width ) / 2;
   n_rgn.Y      = s_rgn.Y + (s_rgn.Height - n_rgn.Height) / 2;
   SetRegion (n_rgn);
}

 void TrackedObject::UpdateTemplateImage (GImage &s_image,IArray2D &or_map,float tu_rate,IBox2D &d_rgn,float& vu_rate)

{
   int x1,y1,x2,y2;

   vu_rate = tu_rate;
   if (Width < 4 || Height < 4) {
      Status |= TO_STATUS_TO_BE_REMOVED;
      #if defined(__DEBUG_OBT) && defined(__CONSOLE)
      logd ("Tracked Object #%05d:\n",ID);
      logd ("     Invalid parameters (1): To be removed.\n");
      #endif
      return;
   }
   // ๋งค์นญ ์์น ์์์ TO๊ฐ ์ฐจ์งํ๋ ์์ญ์ ๊ตฌํ๋ค: (sx1,sy1) - (ex1,ey1)
   int sx1 = MatchingPos.X;
   int sy1 = MatchingPos.Y;
   int ex1 = sx1 + Width;
   int ey1 = sy1 + Height;
   if (sx1 >= s_image.Width || sy1 >= s_image.Height || ex1 <= 0 || ey1 <= 0) {
      Status |= TO_STATUS_TO_BE_REMOVED;
      #if defined(__DEBUG_OBT) && defined(__CONSOLE)
      logd ("Tracked Object #%05d:\n",ID);
      logd ("     Invalid parameters (2): To be removed.\n");
      #endif
      return;
   }
   // ๋ช๋ช ์ํฉ์์๋ TO์ ํํ๋ฆฟ์ ์๋ฐ์ดํธํ์ง ์๋๋ค.
   if (!ObjRgnID || (Status & TO_STATUS_UNDETECTED)) {
      d_rgn(sx1,sy1,ex1 - sx1,ey1 - sy1);
      return;
   }
   // TO์ ๋์ํ๋ FB์ ์์ญ์ ๊ตฌํ๋ค: (sx2,sy2) - (ex2,ey2)
   int sx2 = ObjRgnBndBox.X;
   int sy2 = ObjRgnBndBox.Y;
   int ex2 = sx2 + ObjRgnBndBox.Width;
   int ey2 = sy2 + ObjRgnBndBox.Height;
   // "TO + FB" ์์ญ์ ๊ตฌํ๋ค: (sx2,sy2) - (ex2,ey2)
   if (sx1 < sx2) sx2 = sx1;
   if (sy1 < sy2) sy2 = sy1;
   if (ex1 > ex2) ex2 = ex1;
   if (ey1 > ey2) ey2 = ey1;
   int ti_width  = ex2 - sx2;
   int ti_height = ey2 - sy2;
   if (ti_width < 4 || ti_height < 4) {
      Status |= TO_STATUS_TO_BE_REMOVED;
      #if defined(__DEBUG_OBT) && defined(__CONSOLE)
      logd ("Tracked Object #%05d:\n",ID);
      logd ("     Invalid parameters (3): To be removed.\n");
      #endif
      return;
   }
   // TO์ ํํ๋ฆฟ ์์๊ณผ ์จ์ดํธ ์์์ ์๋ฐ์ดํธํ  ์ค๋น๋ฅผ ํ๋ค.
   int dx1 = sx1 - sx2;
   int dy1 = sy1 - sy2;
   int dx2 = dx1 + Width;
   int dy2 = dy1 + Height;
   if (TemplateImage.Width < Width || TemplateImage.Height < Height || dx1 < 0 || dy1 < 0 || dx2 > ti_width || dy2 > ti_height) {
      Status |= TO_STATUS_TO_BE_REMOVED;
      #if defined(__DEBUG_OBT) && defined(__CONSOLE)
      logd ("Tracked Object #%05d:\n",ID);
      logd ("     Invalid parameters (4): To be removed.\n");
      #endif
      return;
   }
   GImage t_image(ti_width,ti_height); // ์๋ฐ์ดํธ๋ ํํ๋ฆฟ ์์์ ๋ด์ ๋ฒํผ
   GImage w_image(ti_width,ti_height); // ์๋ฐ์ดํธ๋ ์จ์ดํธ ์์์ ๋ด์ ๋ฒํผ
   t_image.Clear (   );
   w_image.Clear (   );
   // ์ผ๋จ t_image์ w_image์ ๊ธฐ์กด ํํ๋ฆฟ ์์๊ณผ ์จ์ดํธ ์์์ ๋ณต์ฌํ๋ค.
   t_image.Copy (TemplateImage,0,0,Width,Height,dx1,dy1);
   w_image.Copy (WeightImage,0,0,Width,Height,dx1,dy1);
   // TO์ ํํ๋ฆฟ ์๋ฐ์ดํธ๋ฅ ์ ์ ์์ ์ผ๋ก ์ ์ดํ๋ค.
   if ((Status & TO_STATUS_STATIC) && (Status & TO_STATUS_MERGED)) {
      if (Exposure < 0.6f) tu_rate = 0.01f; // PARAMETERS
      else tu_rate *= 0.5f;                 // PARAMETERS
   }
   if (Speed > 100.0f) tu_rate *= 3.0f;     // PARAMETERS
   if (tu_rate > 1.0f) tu_rate  = 1.0f;
   vu_rate = tu_rate;
   // t_image์ w_image๋ฅผ ์๋ฐ์ดํธํ๋ค.
   int sx3 = sx2, sy3 = sy2, ex3 = ex2, ey3 = ey2;
   CropRegion (s_image.Width,s_image.Height,sx3,sy3,ex3,ey3);
   int sx4 = sx3 - sx2;
   int sy4 = sy3 - sy2;
   ushort a1 = (ushort)(256.0f * tu_rate);
   ushort a2 = 256 - a1;
   ushort b  = (ushort)(tu_rate * PG_WHITE) * 256;
   byte   w0 = (byte)(tu_rate * PG_WHITE); // ์ด๊ธฐ ์จ์ดํธ ๊ฐ
   for (y1 = sy3,y2 = sy4; y1 < ey3; y1++,y2++) {
      byte* l_s_image = s_image[y1];
      byte* l_t_image = t_image[y2];
      byte* l_w_image = w_image[y2];
      int*  l_or_map  = or_map[y1];
      for (x1 = sx3,x2 = sx4; x1 < ex3; x1++,x2++) {
         if (l_w_image[x2]) {
            l_t_image[x2] = (byte)((a2 * l_t_image[x2] + a1 * l_s_image[x1]) >> 8);
            if (l_or_map[x1] == ObjRgnID) l_w_image[x2] = (byte)((a2 * l_w_image[x2] + b) >> 8);
            else l_w_image[x2] = (byte)((a2 * l_w_image[x2]) >> 8);
         }
         else {
            l_t_image[x2] = l_s_image[x1];
            if (l_or_map[x1] == ObjRgnID) l_w_image[x2] = w0;
         }
      }
   }
   if (!w0) w0 = 1;
   // w_image๋ฅผ ์ด์ฉํ์ฌ TO ์์ญ์ ๋ฒ์๋ฅผ ๊ตฌํ๋ค: (sx1,sy1) - (ex1,ey1)
   uint  sum_w = 0;
   int64 mx  = 0, my  = 0;
   int64 sdx = 0, sdy = 0;
   for (y1 = 0; y1 < w_image.Height; y1++) {
      byte *l_w_image = w_image[y1];
      for (x1 = 0; x1 < w_image.Width; x1++) {
         byte w = l_w_image[x1];
         if (w) {
            uint wx1 = w * x1;
            uint wy1 = w * y1;
            sum_w += w;
            mx    += wx1;
            my    += wy1;
            sdx   += wx1 * x1;
            sdy   += wy1 * y1;
         }
      }
   }
   float mx2  = (float)mx  / sum_w;
   float my2  = (float)my  / sum_w;
   float sdx2 = (float)sdx / sum_w;
   float sdy2 = (float)sdy / sum_w;
   sdx2 = sqrt (sdx2 - mx2 * mx2);
   sdy2 = sqrt (sdy2 - my2 * my2);
   if (sdx2 < 1.0f) sdx2 = 1.0f;
   if (sdy2 < 1.0f) sdy2 = 1.0f;
   float sigma = 2.0f; // PARAMETERS
   float rx    = sigma * sdx2;
   float ry    = sigma * sdy2;
   sx1 = (int)(mx2 - rx);
   sy1 = (int)(my2 - ry);
   ex1 = (int)(mx2 + rx);
   ey1 = (int)(my2 + ry);
   CropRegion (t_image.Width,t_image.Height,sx1,sy1,ex1,ey1);
   // TemplateImage, WeightImage, MaskImage๋ฅผ ์๋ฐ์ดํธํ๋ค.
   int width  = ex1 - sx1;
   int height = ey1 - sy1;
   if (width < 4 || height < 4) {
      Status |= TO_STATUS_TO_BE_REMOVED;
      #if defined(__DEBUG_OBT) && defined(__CONSOLE)
      logd ("Tracked Object #%05d:\n",ID);
      logd ("     Invalid parameters (5): To be removed.\n");
      #endif
      return;
   }
   // MaskImage ์์์ ๋ฉด์ ์ด ๊ฐ์ฅ ํฐ CR์ ์ฐพ๋๋ค.
   GImage w_image2(width + 2,height + 2);
   w_image2.Copy (w_image,sx1,sy1,width,height,1,1);
   w_image2.SetBoundary (1,0);
   GImage m_image(w_image2.Width,w_image2.Height);
   Binarize (w_image2,(byte)(w0 - 1),m_image);
   IArray2D cr_map(m_image.Width,m_image.Height);
   int n_crs = LabelCRs (m_image,cr_map);
   if (!n_crs) {
      Status |= TO_STATUS_TO_BE_REMOVED;
      #if defined(__DEBUG_OBT) && defined(__CONSOLE)
      logd ("Tracked Object #%05d:\n",ID);
      logd ("     Invalid parameters (6): To be removed.\n");
      #endif
      return;
   }
   CRArray1D cr_array(n_crs + 1);
   GetCRInfo (cr_map,cr_array);
   int max_a,max_i;
   FindMaxCRArea (cr_array,max_a,max_i);
   ConnectedRegion& cr = cr_array[max_i];
   sx1   += cr.X - 1;
   sy1   += cr.Y - 1;
   width  = cr.Width;
   height = cr.Height;
   TemplateImage = t_image.Extract (sx1,sy1,width,height);
   WeightImage   = w_image.Extract (sx1,sy1,width,height);
   MaskImage     = m_image.Extract (cr.X,cr.Y,width,height);
   NumRgnPixels  = cr.Area;
   // TO์ ์์ญ์ ๊ตฌํ๋ค.
   d_rgn(sx1 + sx2,sy1 + sy2,width,height);
   // ๋ฉด์ ์ ๋ณํ๊ฐ ์ฌํ๋ฉด Predictor๋ฅผ OFFํ๋ค.
   int area1 = width * height;
   int area2 = Width * Height;
   float ar = GetMaximum ((float)area1 / area2,(float)area2 / area1);
   if (ar >= 1.5f) vu_rate = -1.0f;
}

 void TrackedObject::UpdateType (   )
 
{
   int i;
   
   int   max_i  = 0;
   float max_lh = TypeLikelihood[0];
   for (i = 1; i < 3; i++) {
      if (TypeLikelihood[i] > max_lh) {
         max_lh = TypeLikelihood[i];
         max_i  = i;
      }
   }
   if (max_lh == TypeLikelihood[OC_TYPE_UNKNOWN]) max_i = OC_TYPE_UNKNOWN;
   Type = 0x01 << max_i;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectTracking
//
///////////////////////////////////////////////////////////////////////////////

 ObjectTracking::ObjectTracking (   )

{
   Flag_Enabled        = TRUE;
   SrcColorImage       = NULL;
   ObjIDGenerator      = NULL;
   Options             = 0;
   SearchRange         = 6;
   MaxTrjLength        = 10.0f;
   ObjUpdateRate       = 0.05f;

   MinMovingDistance   = 8.0f / 240.0f; // ์ค์  ์ต์ ์ด๋ ๊ฑฐ๋ฆฌ = 240.0f * MinMovingDistance
   MinMovingDistance2  = 0.0f;
   OuterZoneSize1      = 0.0f;
   OuterZoneSize2      = 0.0f;
   MinCount_Tracked    = 3;
   MaxTime_Lost        = 1.0f;
   MaxTime_Static      = 300.0f;
   MaxTime_Undetected  = 0.2f;
   MinTime_Tracked     = 0.8f;
   Status              = 0;
   FrameRate           = 7.0f;
   FrameDuration       = 1.0f / FrameRate;
   ForegroundDetector  = NULL;
   _Init (   );

   #if defined(__CPNI_CERT_SZM)
   Options             = OBT_OPTION_PERFORM_FAST_MOVING_OBJECT_DETECTION;
   MinMovingDistance   = 11.2f / 240.0f; // ์ค์  UI ์์์ ๋ํดํธ ๋จ๊ณ๋ณด๋ค 2๋จ๊ณ ์์ ํด๋น
   OuterZoneSize1      = 0.1f;
   OuterZoneSize2      = 0.15f;
   MaxTime_Static      = 150.0f;
   MinTime_Tracked     = 2.5f;
   #endif

   #if defined(__KISA_CERT)
   MinMovingDistance   = 11.2f / 240.0f;
   MinMovingDistance2  = 1.0f;
   MaxTime_Static      = 180.0f;
   MinTime_Tracked     = 1.5f;
   #endif

   SyncBlobTrackingParams (   );
}

 ObjectTracking::~ObjectTracking (   )
 
{
   Close (   );
}

 void ObjectTracking::_Init (   )
 
{
   NumObjLayers =  1;
   FrameCount      =  0;
   ObjectCount     = -1;
   Status          =  0;
}

 void ObjectTracking::ChangeObjectRegionLabel (IArray2D &s_map,IBox2D &s_rgn,int s_no,int d_no)

{
   int x,y;

   int sx = s_rgn.X;
   int sy = s_rgn.Y;   
   int ex = sx + s_rgn.Width;
   int ey = sy + s_rgn.Height;
   for (y = sy; y < ey; y++) {
      int* l_s_map = s_map[y];
      for (x = sx; x < ex; x++)
         if (l_s_map[x] == s_no) l_s_map[x] = d_no;
   }   
}

 int ObjectTracking::CheckBlobOverlapWithTrackingArea (IArray2D &s_map,IBox2D &s_rgn,int s_no)

{
   int x,y;

   int n_tps = 0;
   int n_ops = 0;
   int sx = s_rgn.X;
   int sy = s_rgn.Y;
   int ex = sx + s_rgn.Width;
   int ey = sy + s_rgn.Height;
   for (y = sy; y < ey; y++) {
      byte* l_t_map = TrkAreaMap[y];
      int*  l_s_map = s_map[y];
      for (x = sx; x < ex; x++) {
         if (l_s_map[x] == s_no) {
            n_tps++;
            if (l_t_map[x] == PG_WHITE) n_ops++;
         }
      }
   }
   if (!n_tps) return (1);
   float overlap = (float)n_ops / n_tps;
   if (overlap < 0.85f) return (2);
   return (DONE);
}

 int ObjectTracking::CheckObjectInDynamicBackgroundArea (TrackedObject *t_object)

{
   int cx = (int)(t_object->CenterPos.X + 0.5f);
   int cy = (int)(t_object->CenterPos.Y + 0.5f);
   if (cx < 0 || cy < 0 || cx >= SrcImgSize.Width || cy >= SrcImgSize.Height) return (1);
   if (DynBkgAreaMap[cy][cx] == PG_WHITE) return (DONE);
   return (2);
}

 int ObjectTracking::CheckObjectInitMotion (TrackedObject *t_object)
// ๋ฆฌํด ๊ฐ > 0: ๋ธ์ด์ฆ๋ก ํ๋ณํ ๊ฒฝ์ฐ
// ๋ฆฌํด ๊ฐ < 0: ํ๋ณ์ ์ ๋ณดํ๋ ๊ฒฝ์ฐ
// ๋ฆฌํด ๊ฐ = 0: ์ ํจ ๊ฐ์ฒด๋ก ํ๋ณํ ๊ฒฝ์ฐ
{
   t_object->UpdateNoisiness (SrcImgSize);
   if (t_object->Noisiness >= 2.0f) return (1);
   if (t_object->Count_Tracked >= MinCount_Tracked) {
      // ์ ๋ ์ด๋ ๊ฑฐ๋ฆฌ ์ฒดํฌ๋ฅผ ์ํ ์๊ณ์น๋ฅผ ์ค์ ํ๋ค.
      float md_thrsld0 = MinMovingDistance * 240.0f;  // ๊ธฐ๋ณธ
      float md_thrsld1 = 0.3f * md_thrsld0;           // ๋ฐ์ด๋๋ฆฌ ์์ญ์ ์๋ ๊ฐ์ฒด
      float md_thrsld2 = 0.7f * md_thrsld0;           // ๋ฉ๋ฆฌ ์๋ ์ฌ๋
      float md_thrsld3 = 1.5f * md_thrsld0;           // ์ฐจ๋
      float md_thrsld4 = 2.0f * md_thrsld0;           // ๋ฏธํ์ธ ๊ฐ์ฒด
      float md_thrsld5 = 3.0f * md_thrsld0;           // ๋น ๋ฅธ ์ด๋ ๊ฐ์ฒด
      float md_thrsld6 = 1.0f * GetMinimum (t_object->Width,t_object->Height);  // ๋น ๋ฅธ ์ด๋ ๊ฐ์ฒด
      // ์๋ ์ด๋ ๊ฑฐ๋ฆฌ(๊ฐ์ฒด์ ํฌ๊ธฐ์ ๋น๋กํ๋ ์ด๋ ๊ฑฐ๋ฆฌ) ์ฒดํฌ๋ฅผ ์ํ ์๊ณ์น๋ฅผ ์ค์ ํ๋ค.
      float to_size    = 0.5f * (GetMaximum (t_object->Width,(int)t_object->AvgWidth) + GetMaximum (t_object->Height,(int)t_object->AvgHeight));
      if (to_size > 0.2 * SrcImgSize.Height) to_size = 0.0f; // ํฐ ๊ฐ์ฒด์ ๋ํด์๋ ์ด๊ธฐ ์ด๋ ๊ฑฐ๋ฆฌ ์ฒดํฌ๋ฅผ ํ์ง ์๋๋ค.
      float md_thrsld9 = MinMovingDistance2 * to_size;
      // ๊ฐ์ฒด์ ์ด๊ธฐ ์ด๋ ๊ฑฐ๋ฆฌ๋ฅผ ๊ตฌํ๋ค.
      float md1 = t_object->GetMovingDistance (   ); // ๋ค ์ฝ๋์ ์ด๋ ๊ฑฐ๋ฆฌ ์ค ์ต์๊ฐ
      float md2 = Mag(t_object->Displacement);       // ์ผํฐ ํฌ์ธํธ์ ์ด๋ ๊ฑฐ๋ฆฌ
      float md3 = 0.5f * (md1 + md2);
      // ๊ฐ์ฒด์ ์ถ์  ์๊ฐ์ด MinTime_Tracked ์ด์์ธ ๊ฐ์ฒด๋ค์ ๋ํด ์ฒ๋ฆฌ
      if (t_object->Time_Tracked >= MinTime_Tracked) {
         // ๊ฐ์ฒด๊ฐ OuterZone์ ์์ผ๋ฉด ์ต์ด ์ด๋ ๊ฑฐ๋ฆฌ์ ์๊ณ์น๋ฅผ ์ ์๋ณด๋ค ๋ฎ์ ๊ฐ์ผ๋ก ์ ์ฉํ๋ค.
         // ๋งค์ฐ ๋๋ฆฌ๊ฒ ์ง์ํ๋ ๊ฐ์ฒด๋ฅผ 10์ด ๋ด์ ์ ํจ ๊ฐ์ฒด๋ก ํ์ ํ๊ธฐ ์ํจ์ด๋ค.
         if ((t_object->Status & TO_STATUS_IN_OUTER_ZONE) && md2 >= md_thrsld1 && md3 >= md_thrsld9) return (DONE);
         // ๊ฐ์ฒด๊ฐ ์ฌ๋์ผ ๊ฐ๋ฅ์ฑ์ด ํฐ ๊ฒฝ์ฐ
         if (1.0f <= t_object->AspectRatio && t_object->AspectRatio <= 4.5f) {
            if (md3 >= md_thrsld0 && md3 >= md_thrsld9) return (DONE);
            // ๊ฐ์ฒด๊ฐ ๋ฉ๋ฆฌ ์๋ ์ฌ๋์ผ ๊ฐ๋ฅ์ฑ์ด ํฐ ๊ฒฝ์ฐ
            if (t_object->AxisLengthRatio >= 1.5f && t_object->MajorAxisAngle2 <= 35.0f && t_object->Height <= 30 && md3 >= md_thrsld2 && md3 >= md_thrsld9) return (DONE);
         }
         // ๊ฐ์ฒด๊ฐ ์ฐจ๋์ผ ๊ฐ๋ฅ์ฑ์ด ํฐ ๊ฒฝ์ฐ
         else if (t_object->AxisLengthRatio <= 5.0f && md3 >= md_thrsld3 && md3 >= md_thrsld9) return (DONE);
         // ๊ทธ ์ธ?
         else if (md3 >= md_thrsld4 && md3 >= md_thrsld9) return (DONE);
      }
      // ๊ฐ์ฒด์ ์ถ์  ์๊ฐ์ด MinTime_Tracked ๋ฏธ๋ง์ธ ๊ฐ์ฒด๋ค์ ๋ํด ์ฒ๋ฆฌ
      else {
         // ๋น ๋ฅด๊ฒ ์ด๋ํ๋ ๊ฐ์ฒด์ธ ๊ฒฝ์ฐ
         if (md3 >= md_thrsld5 && md1 >= md_thrsld6 && md3 >= md_thrsld9) return (DONE);
      }
   }
   return (-1);
}

 int ObjectTracking::CheckObjectInNonHandoffArea (TrackedObject *t_object)
 
{
   int cx = (int)(t_object->CenterPos.X + 0.5f);
   int cy = (int)(t_object->CenterPos.Y + 0.5f);
   if (cx < 0 || cy < 0 || cx >= SrcImgSize.Width || cy >= SrcImgSize.Height) return (1);
   if (NonHandoffAreaMap[cy][cx] == PG_WHITE) return (DONE);
   return (2);
}

 int ObjectTracking::CheckObjectInOuterZone (TrackedObject* t_object)
// t_object์ ๋ค ์ฝ๋ ํฌ์ธํธ ์ค ์ ์ด๋ ํ๋๊ฐ OuterZone1 ๋ด์ ์ํ๊ณ 
// t_object์ ์ค์ฌ์ ์ด OuterZone2 ๋ด์ ์ํ๋ฉด DONE์ ๋ฆฌํดํ๋ค.
{
   int i,n;
   
   if (!SrcImgSize.Width || !SrcImgSize.Height) return (-1);
   if (!OuterZoneSize1 || !OuterZoneSize2) return (-2);
   int sx = (int)(SrcImgSize.Width  * OuterZoneSize1);
   int sy = 0;
   int ex = SrcImgSize.Width  - sx;
   int ey = SrcImgSize.Height - sy;
   IPoint2D cps[4];
   t_object->GetCornerPoints (cps);
   for (i = n = 0; i < 4; i++) {
      IPoint2D& cp = cps[i];
      if (sx <= cp.X && cp.X <= ex && sy <= cp.Y && cp.Y <= ey) n++;
   }
   if (n == 4) return (1);
   sx = (int)(SrcImgSize.Width  * OuterZoneSize2);
   sy = 0;
   ex = SrcImgSize.Width  - sx;
   ey = SrcImgSize.Height - sy;
   int cx = t_object->X + t_object->Width  / 2;
   int cy = t_object->Y + t_object->Height / 2;
   if (sx <= cx && cx <= ex && sy <= cy && cy <= ey) return (2);
   return (DONE);
}

 int ObjectTracking::CheckObjectInTrackingArea (TrackedObject *t_object)

{
   int cx = (int)(t_object->CenterPos.X + 0.5f);
   int cy = (int)(t_object->CenterPos.Y + 0.5f);
   if (cx < 0 || cy < 0 || cx >= SrcImgSize.Width || cy >= SrcImgSize.Height) {
      int sx = t_object->X;
      int sy = t_object->Y;
      int ex = sx + t_object->Width;
      int ey = sy + t_object->Height;
      if (sx < 0) sx = 0;
      if (sy < 0) sy = 0;
      if (ex > SrcImgSize.Width ) ex = SrcImgSize.Width;
      if (ey > SrcImgSize.Height) ey = SrcImgSize.Height;
      int w = ex - sx;
      int h = ey - sy;
      if (w <= 0 || h <= 0) return (1);
      float r = (float)(w * h) / (t_object->Width * t_object->Height);
      if (r < 0.25f) return (2);
      else return (DONE);
   }
   else if (TrkAreaMap[cy][cx] == PG_WHITE) return (DONE);
   else return (3);
}

 int ObjectTracking::CheckObjectStatic (TrackedObject *t_object)
 
{
   if (t_object->Status & TO_STATUS_STATIC) {
      if (t_object->IsStatic (   )) return (DONE);
      else {
         t_object->Status &= ~TO_STATUS_STATIC;
         t_object->ResetRegion_Static (   );
         return (1);
      }
   }
   else {
      if ((t_object->Status & TO_STATUS_VALID_SPEED) && (t_object->Speed <= 2.0f)) { // PARAMETERS
         t_object->Status |= TO_STATUS_STATIC;
         t_object->SetRegion_Static (    );
         return (DONE);
      }
      else return (1);
   }
}

 int ObjectTracking::CheckObjectStatus (int status)

{
   TrackedObject* t_object = TrackedObjects.First (   );
   while (t_object != NULL) {
      if (t_object->Status & status) return (DONE);
      t_object = TrackedObjects.Next (t_object);
   }
   return (1);
}

 int ObjectTracking::CheckRegionInMarginalArea (IBox2D& s_rgn)

{
   int margin = (int)(SrcImgSize.Height * 0.05f);
   int sx1 = margin;
   int sy1 = margin;
   int ex1 = SrcImgSize.Width  - margin;
   int ey1 = SrcImgSize.Height - margin;
   int sx2 = s_rgn.X;
   int sy2 = s_rgn.Y;
   int ex2 = sx2 + s_rgn.Width;
   int ey2 = sy2 + s_rgn.Height;
   if (sx1 < sx2 && ex2 < ex1 && sy1 < sy2 && ey2 < ey1) return (1);
   return (DONE);
}

 int ObjectTracking::CheckSetup (ObjectTracking* ot)

{
   int ret_flag = 0;
   FileIO buffThis,buffFrom;   
   CXMLIO xmlIOThis,xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis,XMLIO_Write);
   xmlIOFrom.SetFileIO (&buffFrom,XMLIO_Write);
   WriteFile (&xmlIOThis);
   ot->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom))
      ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength (0);
   buffFrom.SetLength (0);
   if (ret_flag & CHECK_SETUP_RESULT_CHANGED) {
      // ์ถ์ , ์ง๋ฉด, ๋์ ๋ฐฐ๊ฒฝ ์์ญ์ค ํ๋๋ผ๋ ๋ณ๊ฒฝ๋์์ผ๋ฉด VACL ์ ์ฒด ์ฌ์์
      xmlIOThis.SetFileIO (&buffThis,XMLIO_Write);
      xmlIOFrom.SetFileIO (&buffFrom,XMLIO_Write);
      TrackingAreas.WriteFile        (&xmlIOThis,"TA");
      NontrackingAreas.WriteFile     (&xmlIOThis,"NTA");
      DynBkgAreas.WriteFile          (&xmlIOThis,"DBA");
      NonHandoffAreas.WriteFile      (&xmlIOThis,"NHOA");
      ot->TrackingAreas.WriteFile    (&xmlIOFrom,"TA");
      ot->NontrackingAreas.WriteFile (&xmlIOFrom,"NTA");
      ot->DynBkgAreas.WriteFile      (&xmlIOFrom,"DBA");
      ot->NonHandoffAreas.WriteFile  (&xmlIOFrom,"NHOA");
      if (buffThis.IsDiff (buffFrom))
         ret_flag |= CHECK_SETUP_RESULT_RESTART_ALL;
      buffThis.SetLength (0);
      buffFrom.SetLength (0);
   }
   return (ret_flag);
}

 int ObjectTracking::CheckStaticObjectConditions (TrackedObject* t_object)

{
   if (!t_object->NumBndPixels) return (1);
   // ์ผ๋จ Ghost Object๊ฐ ๋๋ ค๋ฉด Static ์ํ์ฌ์ผ ํ๋ค.
   if (!(t_object->Status & TO_STATUS_STATIC)) return (2);
   // ๊ฐ์ฒด ์์ญ ๋ฐ์ด๋๋ฆฌ์์์ ์ ๊ฒฝ ์์ง์ ์ธ๊ธฐ๊ฐ ์ฝํ๋ฉด Ghost Object๋ก ํ๋จํ๋ค.
   if (t_object->BndEdgeStrength <= 0.3f) t_object->Status |= TO_STATUS_GHOST_OBJECT;
   // ๊ฐ์ฒด ์์ญ ๋ฐ์ด๋๋ฆฌ์์์ ์ ๊ฒฝ ์์ง์ ์์ด ๋ฐฐ๊ฒฝ ์์ง์ ์๋ณด๋ค ๋ง์ผ๋ฉด Abandoned Object๋ก ํ๋จํ๋ค.
   if      (!t_object->NumFrgBndEdgePixels) t_object->Status |= TO_STATUS_REMOVED_OBJECT;
   else if (!t_object->NumBkgBndEdgePixels) t_object->Status |= TO_STATUS_ABANDONED_OBJECT;
   else {
      float r1 = (float)t_object->NumFrgBndEdgePixels / t_object->NumBkgBndEdgePixels;
      float r2 = (float)t_object->NumBkgBndEdgePixels / t_object->NumFrgBndEdgePixels;
      float r  = GetMinimum (r1,r2);
      if (r < 0.7f) { // ๊ฐ์ฒด ์์ญ ๋ฐ์ด๋๋ฆฌ์์ ์ ๊ฒฝ ์์ง์ ๋ฐฐ๊ฒฝ ์์ง์ ์ธ๊ธฐ ์ฐจ๊ฐ ํฐ ๊ฒฝ์ฐ
         if (t_object->NumFrgBndEdgePixels > t_object->NumBkgBndEdgePixels) t_object->Status |= TO_STATUS_ABANDONED_OBJECT;
         else t_object->Status |= TO_STATUS_REMOVED_OBJECT;
      }
      else {          // ๊ฐ์ฒด ์์ญ ๋ฐ์ด๋๋ฆฌ์์ ์ ๊ฒฝ ์์ง์ ๋ฐฐ๊ฒฝ ์์ง์ ์ธ๊ธฐ ์ฐจ๊ฐ ์์ ๊ฒฝ์ฐ
         t_object->Status |= TO_STATUS_ABANDONED_OBJECT | TO_STATUS_REMOVED_OBJECT;
      }
   }
   return (DONE);
}

 void ObjectTracking::GetObjectProperties (TrackedObject* t_object)

{
   // ํจ์ ํธ์ถ ์์๋ฅผ ๋ฐ๊พธ์ง ๋ง ๊ฒ!
   t_object->GetPrincipalAxes          (   );
   t_object->GetArea                   (   );
   t_object->GetAxisLengthRatio        (   );
   t_object->GetMajorAxisAngle1        (   );
   t_object->GetDisplacement           (   );
   t_object->GetAspectRatio            (   );
   t_object->GetMeanArea               ((int)FrameRate);
   t_object->GetMeanVelocity           ((int)FrameRate,t_object->MeanVelocity);
   t_object->GetSpeeds                 (FrameRate);
   t_object->GetMajorAxisAngle2        (   );
   t_object->GetMaxMovingDistance      (   );
   t_object->GetAreaVariation          (   );
   t_object->GetRegionBoundaryEdgeInfo (*ForegroundDetector,ObjRegionMap);
   t_object->GetDispersedness          (   );
}

 void ObjectTracking::CleanTrackedObjectList (   )
 
{
   VACL::CleanTrackedObjectList (TrackedObjects);
}

 void ObjectTracking::ClearAllAreaLists (   )
 
{
   TrackingAreas.Delete    (   );
   NonHandoffAreas.Delete  (   );
   NontrackingAreas.Delete (   );
   DynBkgAreas.Delete      (   );
}

 void ObjectTracking::Close (   )

{
   MaskImage.Delete         (   );
   SrcImgSize.Clear         (   );
   DynBkgAreaMap.Delete     (   );
   NonHandoffAreaMap.Delete (   );
   TrkAreaMap.Delete        (   );
   ObjRegionMap.Delete      (   );
   LostObjects.Delete       (   );
   TrackedObjects.Delete    (   );
   BlobTracker.Close        (   );
   FMODetector.Close (   );
   _Init (   );
}

 void ObjectTracking::CopyBlobRegion (IArray2D &s_map,IBox2D &s_rgn,int s_no,int d_no,IArray2D &d_map)

{
   int x,y;
   
   int sx = s_rgn.X;
   int sy = s_rgn.Y;
   int ex = sx + s_rgn.Width;
   int ey = sy + s_rgn.Height;
   if (sx < 1) sx = 1;
   if (sy < 1) sy = 1;
   if (ex > s_map.Width  - 1) ex = s_map.Width  - 1;
   if (ey > s_map.Height - 1) ey = s_map.Height - 1;
   for (y = sy; y < ey; y++) {
      int* l_s_map = s_map[y];
      int* l_d_map = d_map[y];
      for (x = sx; x < ex; x++) {
         if (l_s_map[x] == s_no && !l_d_map[x]) l_d_map[x] = d_no;
      }
   }
}

 void ObjectTracking::CopyResidualBlobRegion (IArray2D& s_map,IBox2D& s_rgn,int s_no,TrackedObject* t_object,int d_no,IArray2D& d_map)

{
   int i;
   
   int sx1 = s_rgn.X;
   int sy1 = s_rgn.Y;
   int ex1 = sx1 + s_rgn.Width;
   int ey1 = sy1 + s_rgn.Height;
   int sx2 = t_object->MatchingPos.X;
   int sy2 = t_object->MatchingPos.Y;
   int ex2 = sx2 + t_object->MaskImage.Width;
   int ey2 = sy2 + t_object->MaskImage.Height;
   int sx3 = GetMinimum (sx1,sx2);
   int sy3 = GetMinimum (sy1,sy2);
   int ex3 = GetMaximum (ex1,ex2);
   int ey3 = GetMaximum (ey1,ey2);
   GImage t_image(ex3 - sx3,ey3 - sy3);
   t_image.Clear (   );
   IPoint2D d_pos(sx1 - sx3,sy1 - sy3);
   CopyCR (s_map,s_rgn,s_no,t_image,d_pos,PG_WHITE);
   t_object->PutMask (t_object->MatchingPos.X - sx3,t_object->MatchingPos.Y - sy3,PG_BLACK,t_image,TRUE);
   IArray2D cr_map(t_image.Width,t_image.Height);
   int n_crs = LabelCRs (t_image,cr_map);
   if (!n_crs) return;
   CRArray1D cr_array(n_crs + 1);
   GetCRInfo (cr_map,cr_array);
   int max_i = 0, max_a = 0;
   for (i = 1; i < cr_array.Length; i++) {
      if (cr_array[i].Area > max_a) {
         max_a = cr_array[i].Area;
         max_i = i;
      }
   }
   ConnectedRegion& cr = cr_array[max_i];
   d_pos(cr.X + sx3,cr.Y + sy3);
   CopyCR (cr_map,cr,max_i,d_map,d_pos,d_no);
}

 void ObjectTracking::Enable (int flag_enable)

{
   Flag_Enabled = flag_enable;
   BlobTracker.Enable (flag_enable);
}

 TrackedObject *ObjectTracking::FindMatchingObjectInLostObjectList (TrackedObject* t_object)

{
   int i;

   int n_items = LostObjects.GetNumItems (   );
   if (!n_items) return (NULL);
   Array1D<TrackedObject*> m_objects(n_items);
   float max_pe = 20.0f * ObjRegionMap.Height / 240.0f;
   n_items = 0;
   TrackedObject *l_object = LostObjects.First (   );
   while (l_object != NULL) {
      if (l_object->Status & TO_STATUS_UNDETECTED) {
         if (t_object->InitFrameCount > l_object->InitFrameCount) {
            FPoint2D cd = t_object->CenterPos - l_object->CenterPos;
            float m_cd = Mag(cd);
            float m_lv = Mag(l_object->MeanVelocity);
            float e_md = m_lv * l_object->Time_Lost * FrameRate; // ์์ ์ด๋ ๊ฑฐ๋ฆฌ
            // l_object์ t_object ์ฌ์ด์ ๊ฑฐ๋ฆฌ๊ฐ (e_md + max_pe)๋ณด๋ค ์์ ๊ฒฝ์ฐ
            if (m_cd < e_md + max_pe) {
               float ar = (float)t_object->Area / l_object->MeanArea;
               // l_object์ t_object์ ๋ฉด์ ์ด ์ ์ฌํ ๊ฒฝ์ฐ
               if (0.5f <= ar && ar <= 2.0f) {
                  // l_object๊ฐ ์ ์งํ ๊ฐ์ฒด์ธ ๊ฒฝ์ฐ
                  if (m_lv < 0.05f) m_objects[n_items++] = l_object;
                  // l_object๊ฐ ์ผ๋ฐ ์ด๋ ๊ฐ์ฒด์ธ ๊ฒฝ์ฐ
                  else {
                     FPoint2D lv = l_object->MeanVelocity / m_lv;
                     if (m_cd >= 0.05f) {
                        cd /= m_cd;
                        float ag = (float)(MC_RAD2DEG * acos (IP(cd,lv)));
                        // l_object์ ์งํ ๋ฐฉํฅ๊ณผ (l_object -> t_object) ๋ฐฉํฅ์ด ์ ์ฌํ ๊ฒฝ์ฐ
                        if (ag <= 60.0f) m_objects[n_items++] = l_object;
                     }
                     else m_objects[n_items++] = l_object;
                  }
               }
            }
         }
      }
      l_object = LostObjects.Next (l_object);
   }
   // m_objects ๋ฐฐ์ด์ ๋ฑ๋ก๋ l_object๋ค ์ค์ t_object์์ ํ์คํ ๊ทธ๋จ ์ฐจ์ด๊ฐ ์ ์ผ ์์ l_object๋ฅผ ์ฐพ๋๋ค.
   // ์ฐพ์ l_object์ t_object ์ฌ์ด์ ํ์คํ ๊ทธ๋จ ์ฐจ์ด๊ฐ ์ผ์ ๋ ์ดํ์ด๋ฉด, ์ฐพ์ l_object๋ฅผ t_object์
   // ๋งค์นญ์ด ๋๋ ์ต์ข ๊ฐ์ฒด๋ก ์ ์ ํ๋ค.
   int   min_i = 0;
   float min_h = 1.0f;
   for (i = 0; i < n_items; i++) {
      float h = 1.0f - t_object->GrayHistogram.GetSimilarity (m_objects[i]->GrayHistogram);
      if (h < min_h) min_h = h, min_i = i;
   }
   if (min_h <= 0.4f) return (m_objects[min_i]);
   return (NULL);
}

 void ObjectTracking::GetBkgModelUpdateScheme (int bmu_mode,GImage &d_image)
 
{
   switch (bmu_mode) {
      case FGD_BMU_MODE_INITIAL:
      case FGD_BMU_MODE_MAJOR: {
         d_image.Set (0,0,d_image.Width,d_image.Height,FGD_BMU_SPEED_NORMAL);
         TrackedObject *t_object = TrackedObjects.First (   );
         while (t_object != NULL) {
            int bmu_speed = FGD_BMU_SPEED_NORMAL;
            if (t_object->EventStatus & (TO_EVENT_ABANDONED_START | TO_EVENT_ABANDONED | TO_EVENT_CAR_PARKING_START |
               TO_EVENT_CAR_PARKING | TO_EVENT_DWELL | TO_EVENT_REMOVED_START | TO_EVENT_REMOVED | TO_EVENT_STOPPING))
               bmu_speed = FGD_BMU_SPEED_ZERO;
            else if (t_object->Status & TO_STATUS_VERIFIED) {
               bmu_speed = FGD_BMU_SPEED_ZERO;
               if (t_object->Status & TO_STATUS_STATIC    ) bmu_speed = FGD_BMU_SPEED_SLOW;
               if (t_object->Time_Static >= MaxTime_Static) bmu_speed = FGD_BMU_SPEED_NORMAL;
            }
            if (bmu_speed != FGD_BMU_SPEED_NORMAL) {
               int sx = t_object->X;
               int sy = t_object->Y;
               int dx = (int)(0.05f * t_object->Width);
               int dy = (int)(0.05f * t_object->Height);
               if (dx < 1) dx = 1;
               if (dy < 1) dy = 1;
               t_object->PutMask (sx,sy     ,bmu_speed,d_image);
               t_object->PutMask (sx - dx,sy,bmu_speed,d_image);
               t_object->PutMask (sx + dx,sy,bmu_speed,d_image);
               t_object->PutMask (sx,sy - dy,bmu_speed,d_image);
               t_object->PutMask (sx,sy + dy,bmu_speed,d_image);
            }
            t_object = TrackedObjects.Next (t_object);
         }
      }  break;
      case FGD_BMU_MODE_MINOR: {
         d_image.Set (0,0,d_image.Width,d_image.Height,FGD_BMU_SPEED_ZERO);
         TrackedObject *t_object = TrackedObjects.First (   );
         while (t_object != NULL) {
            int bgu_scheme = FGD_BMU_SPEED_ZERO;
            if (!(t_object->EventZones & TO_EVENTZONE_REMOVED) && (t_object->Status & TO_STATUS_GHOST_OBJECT)) bgu_scheme = FGD_BMU_SPEED_SLOW;
            if (bgu_scheme != FGD_BMU_SPEED_ZERO) t_object->PutMask (t_object->X,t_object->Y,bgu_scheme,d_image);
            t_object = TrackedObjects.Next (t_object);
         }
      }  break;
      default:
         break;
   }
}

 void ObjectTracking::GetFMOCorrespondenceMatrix (TOArray1D &to_array,IArray2D &fr_map,IArray2D &oc_matrix)

{
   int i,j;
   int x1,y1,x2,y2;
   
   oc_matrix.Clear (   );
   for (i = 1; i < to_array.Length; i++) {
      TrackedObject* t_object = to_array[i];
      int sx1 = t_object->X;
      int sy1 = t_object->Y;
      int ex1 = sx1 + t_object->Width;
      int ey1 = sy1 + t_object->Height;
      if (sx1 < 0) sx1 = 0;
      if (sy1 < 0) sy1 = 0;
      if (ex1 > fr_map.Width ) ex1 = fr_map.Width;
      if (ey1 > fr_map.Height) ey1 = fr_map.Height;
      int sx2 = sx1 - t_object->X;
      int sy2 = sy1 - t_object->Y;
      byte** m_image = t_object->MaskImage;
      for (y1 = sy1,y2 = sy2; y1 < ey1; y1++,y2++) {
         byte* l_m_image = m_image[y2];
         int*  l_fr_map  = fr_map[y1];
         int*  l_oc_mat  = oc_matrix[i];
         for (x1 = sx1,x2 = sx2; x1 < ex1; x1++,x2++)
            if (l_fr_map[x1] && l_m_image[x2]) l_oc_mat[l_fr_map[x1]]++;
      }
   }
   for (i = 1; i < oc_matrix.Height; i++) {
      int* l_oc_matrix = oc_matrix[i];
      int a = to_array[i]->NumRgnPixels;
      for (j = 1; j < oc_matrix.Width; j++) {
         if (l_oc_matrix[j]) {
            float ar = (float)l_oc_matrix[j] / a;
            if (ar < 0.35f) l_oc_matrix[j] = 0; // ์์ญ ๊ฐ ๊ฒน์น๋ ์์ด ๋๋ฌด ์์ผ๋ฉด ๋ฌด์ํ๋ค.
         }
      }
   }
}

 void ObjectTracking::GetFMOCorrespondences (IArray2D &oc_matrix,TOArray1D &to_array,FRArray1D &fr_array)

{
   int i,j,n;

   for (i = 1; i < oc_matrix.Height; i++) {
      int* l_oc_matrix = oc_matrix[i];
      TrackedObject* t_object = to_array[i];
      for (j = 1,n = 0; j < oc_matrix.Width; j++)
         if (l_oc_matrix[j]) n++;
      if (n) {
         t_object->Status |= TO_STATUS_FAST_MOVING_OBJECT;
         t_object->MatFrgRegions.Create (n);
         for (j = 1,n = 0; j < oc_matrix.Width; j++)
            if (l_oc_matrix[j]) t_object->MatFrgRegions[n++] = j;
      }
   }
   for (j = 1; j < oc_matrix.Width; j++) {
      FrgRegion* fg_rgn = &fr_array[j];
      for (i = 1,n = 0; i < oc_matrix.Height; i++)
         if (oc_matrix[i][j]) n++;
      if (n) {
         fg_rgn->MatTrkObjects.Create (n);
         for (i = 1,n = 0; i < oc_matrix.Height; i++)
            if (oc_matrix[i][j]) fg_rgn->MatTrkObjects[n++] = i;
      }
   }
}

 void ObjectTracking::GetFrgRegionMaskImage (IArray2D &fr_map,GImage &d_image)

{
   int x,y;
   
   for (y = 0; y < fr_map.Height; y++) {
      byte* l_d_image = d_image[y];
      int*  l_fr_map  = fr_map[y];
      for (x = 0; x < fr_map.Width; x++) {
         if (l_fr_map[x]) l_d_image[x] = 2;
         else l_d_image[x] = 1;
      }
   }  
}

 int ObjectTracking::GetNewExtObjectID (   )
 
{
   if (ObjIDGenerator == NULL) return (0);
   else return (ObjIDGenerator->Generate (   ));
}

 int ObjectTracking::GetNewIntObjectID (   )
 
{
   return (ObjectCount--);
}

 void ObjectTracking::GetObjectAreasAndBoundingBoxes (IArray2D &cr_map,IArray1D &ar_array,IB2DArray1D &bb_array)

{
   int i,x,y;

   ar_array.Clear (   );
   bb_array[0].Clear (   );
   for (i = 1; i < bb_array.Length; i++) bb_array[i](cr_map.Width,cr_map.Height,-1,-1);
   for (y = 0; y < cr_map.Height; y++) {
      int* l_cr_map = cr_map[y];
      for (x = 0; x < cr_map.Width; x++) {
         if (l_cr_map[x]) {
            ar_array[l_cr_map[x]]++;
            IBox2D &d_box = bb_array[l_cr_map[x]];
            if (x < d_box.X     ) d_box.X = x;
            if (y < d_box.Y     ) d_box.Y = y;
            if (x > d_box.Width ) d_box.Width  = x;
            if (y > d_box.Height) d_box.Height = y;
         }
      }
   }
   for (i = 1; i < bb_array.Length; i++) {
      bb_array[i].Width  -= bb_array[i].X - 1;
      bb_array[i].Height -= bb_array[i].Y - 1;
   }
}

 void ObjectTracking::GetTrackedObjectArray (TOArray1D &d_array)

{
   int i;
   
   d_array.Create (TrackedObjects.GetNumItems (   ) + 1);
   d_array[0] = NULL;
   TrackedObject *t_object = TrackedObjects.First (   );
   for (i = 1; t_object != NULL; i++) {
      d_array[i] = t_object;
      t_object = TrackedObjects.Next (t_object);
   }
}

 void ObjectTracking::GetObjectCorrespondenceMatrix (TOArray1D &to_array,IArray2D &fr_map,IArray2D &oc_matrix)

{
   int i,j;
   int x1,y1,x2,y2;
   
   // oc_matrix์ row: TO
   // oc_matrix์ col: FR
   oc_matrix.Clear (   );
   for (i = 1; i < to_array.Length; i++) {
      TrackedObject* t_object = to_array[i];
      int n_tries = 1;
      if (t_object->Status & TO_STATUS_FAST_MOVING_OBJECT) n_tries = 2;
      for (j = 0; j < n_tries; j++) {
         if (!t_object->MatFrgRegions) {
            IPoint2D m_pos = t_object->MatchingPos;
            if ((t_object->Status & TO_STATUS_FAST_MOVING_OBJECT) && (j == 0)) {
               m_pos.X = t_object->X;
               m_pos.Y = t_object->Y;
            }
            int sx1 = m_pos.X;
            int sy1 = m_pos.Y;
            int ex1 = sx1 + t_object->Width;
            int ey1 = sy1 + t_object->Height;
            if (sx1 < 0) sx1 = 0;
            if (sy1 < 0) sy1 = 0;
            if (ex1 > fr_map.Width ) ex1 = fr_map.Width;
            if (ey1 > fr_map.Height) ey1 = fr_map.Height;
            int sx2 = sx1 - m_pos.X;
            int sy2 = sy1 - m_pos.Y;
            int flag_matched = FALSE;
            byte** m_image = t_object->MaskImage;
            for (y1 = sy1,y2 = sy2; y1 < ey1; y1++,y2++) {
               byte* l_m_image   = m_image[y2];
               int*  l_fr_map    = fr_map[y1];
               int*  l_oc_matrix = oc_matrix[i];
               for (x1 = sx1,x2 = sx2; x1 < ex1; x1++,x2++) {
                  if (l_fr_map[x1] && l_m_image[x2]) {
                     l_oc_matrix[l_fr_map[x1]]++;
                     flag_matched = TRUE;
                  }
               }
            }
            if (flag_matched) break;
         }
      }
   }
   for (i = 1; i < oc_matrix.Height; i++) {
      int* l_oc_matrix = oc_matrix[i];
      int a = to_array[i]->NumRgnPixels;
      for (j = 1; j < oc_matrix.Width; j++) {
         if (l_oc_matrix[j]) {
            float ar = (float)l_oc_matrix[j] / a;
            if (ar < 0.1f) l_oc_matrix[j] = 0; // ์์ญ ๊ฐ ๊ฒน์น๋ ์์ด ๋๋ฌด ์์ผ๋ฉด ๋ฌด์ํ๋ค.
         }
      }
   }
}

 void ObjectTracking::GetObjectCorrespondences (IArray2D &oc_matrix,TOArray1D &to_array,FRArray1D &fr_array)

{
   int i,j,n;

   for (i = 1; i < oc_matrix.Height; i++) {
      TrackedObject* t_object = to_array[i];
      int* l_oc_matrix = oc_matrix[i];
      for (j = 1,n = 0; j < oc_matrix.Width; j++)
         if (l_oc_matrix[j]) n++;
      if (n) {
         t_object->MatFrgRegions.Create (n);
         for (j = 1,n = 0; j < oc_matrix.Width; j++)
            if (l_oc_matrix[j]) t_object->MatFrgRegions[n++] = j;
         // t_object์ ์ด๋ ์๋๊ฐ ์ค์ด FMO์์ ์ผ๋ฐ ๊ฐ์ฒด๋ก ์ ์ดํ๋ ๊ฒฝ์ฐ
         if (t_object->Status & TO_STATUS_FAST_MOVING_OBJECT) t_object->Status |= TO_STATUS_FMO_ENDED;
      }
   }
   for (j = 1; j < oc_matrix.Width; j++) {
      FrgRegion* fg_rgn = &fr_array[j];
      for (i = 1,n = 0; i < oc_matrix.Height; i++)
         if (oc_matrix[i][j]) n++;
      if (n) {
         fg_rgn->MatTrkObjects.Create (n);
         for (i = 1,n = 0; i < oc_matrix.Height; i++)
            if (oc_matrix[i][j]) fg_rgn->MatTrkObjects[n++] = i;
      }
   }
}

 void ObjectTracking::GetObjectMatchingPosition (GImage& s_image,TrackedObject* t_object)

{
   int s_range = SearchRange;
   if (t_object->Status & TO_STATUS_FAST_MOVING_OBJECT) s_range = 2 * SearchRange;
   if (t_object->Status & TO_STATUS_STATIC) {
      if (t_object->Exposure < 0.2f) s_range = 0;
   }

   if      (s_range > 0) t_object->GetMatchingPosition  (s_image,MaskImage,t_object->MatchingPos,s_range);
   else if (s_range < 0) t_object->GetPredictedPosition (t_object->MatchingPos,1.0f,TRUE);
   else t_object->MatchingPos(t_object->X,t_object->Y);
}

 int ObjectTracking::Initialize (ISize2D& si_size,float frm_rate,ObjectIDGeneration& id_generator)

{
   #if defined(__LICENSE_MAC)
   if (__CheckLicenseKey (   )) __Flag_VACL_Initialized = FALSE;
   #endif
   if (!__Flag_VACL_Initialized) return (-1);
   if (!IsEnabled (   )) return (1);
   Close (   );
   SrcImgSize     = si_size;
   FrameRate      = frm_rate;
   FrameDuration  = 1.0f / frm_rate;
   ObjIDGenerator = &id_generator;
   MaskImage.Create         (si_size.Width,si_size.Height);
   DynBkgAreaMap.Create     (si_size.Width,si_size.Height);
   DynBkgAreaMap.Clear      (   );
   NonHandoffAreaMap.Create (si_size.Width,si_size.Height);
   NonHandoffAreaMap.Clear  (   );
   TrkAreaMap.Create        (si_size.Width,si_size.Height);
   TrkAreaMap.Set           (0,0,si_size.Width,si_size.Height,PG_WHITE);
   ObjRegionMap.Create      (si_size.Width,si_size.Height);
   BlobTracker.Initialize   (si_size,frm_rate);
   SyncBlobTrackingParams   (   );
   return (DONE);
}

 void ObjectTracking::MergeObjects (TOArray1D &to_array,IArray1D &to_nos,IBox2D &fb_region,int fb_no,IArray2D &oc_matrix,IArray2D &d_map)

{
   int i,j;
   
   // ์ฃผ์ด์ง FR์ ๊ฐ์ฅ ๋ง์ด ๊ฒน์น๋ TO๋ฅผ ์ฐพ๋๋ค. ์ด๊ฒ์ TO_m์ด๋ผ๊ณ  ํ์.
   int max_a = 0,max_i = 0;
   for (i = 0; i < to_nos.Length; i++) {
      int a = oc_matrix[to_nos[i]][fb_no];
      if (a > max_a) max_a = a, max_i = i;
   }
   // TO_m์ ๋์ํ๋ FR๋ค ์ค์์ TO_m๊ณผ ๊ฐ์ฅ ๋ง์ด ๊ฒน์น๋ FR์ ์ธ๋ฑ์ค ๋ฒํธ๊ฐ fb_no์ ์ผ์นํ์ง ์์ผ๋ฉด ๋จธ์ง๋ฅผ ์ํํ์ง ์๋๋ค.
   max_a = 0;
   int max_j  = 0;
   int max_i2 = to_nos[max_i];
   TrackedObject* tm_object = to_array[max_i];
   for (j = 0; j < tm_object->MatFrgRegions.Length; j++) {
      int a = oc_matrix[max_i2][tm_object->MatFrgRegions[j]];
      if (a > max_a) max_a = a, max_j = j;
   }
   if (fb_no != tm_object->MatFrgRegions[max_j]) return;
   int flag_tm_ima = tm_object->IsInMarginalArea (SrcImgSize);
   // ์ฃผ์ด์ง ์กฐ๊ฑด์ ๋ง์กฑํ๋ฉด ๋ณํฉ์ ์ํํ๋ค.
   for (i = 0; i < to_array.Length; i++) {
      if (max_i != i) {
         int condition = 0;
         TrackedObject* ti_object = to_array[i];
         int flag_ti_ima = ti_object->IsInMarginalArea (SrcImgSize);
         float ar = (float)ti_object->NumRgnPixels / tm_object->NumRgnPixels;
         float ov = (float)GetOverlapArea (*ti_object,*tm_object) / (ti_object->Width * ti_object->Height);
         // TO_i๊ฐ TO_STATUS_VALIDATED ์ํ๊ฐ ์๋ ์ํฉ์์ TO_m๊ณผ ์กฐ๊ธ์ด๋ผ๋ ๊ฒน์น ๊ฒฝ์ฐ...
         if (!(ti_object->Status & TO_STATUS_VALIDATED) && (ov > 0.05f)) condition = 1; // PARAMTERS
         // TO_i๊ฐ TO_STATUS_VERIFIED ์ํ๊ฐ ์๋ ์ํฉ์์ TO_m์ ๋นํด TO_i์ ๋ฉด์ ์ด ๋งค์ฐ ์์ ๊ฒฝ์ฐ...
         if (!(ti_object->Status & TO_STATUS_VERIFIED ) && (ar < 0.02f)) condition = 2; // PARAMETERS
         // TO_i์ TO_m์ด ๋ชจ๋ ์์์ ๊ฐ์ฅ์๋ฆฌ์ ๊ฑธ์ณ ์๊ณ  ๋ ๊ฐ์ฒด๊ฐ์ ์์ฑ ์๊ฐ์ ์ฐจ์ด๊ฐ ์์ ๊ฒฝ์ฐ
         if (!(ti_object->Status & TO_STATUS_VERIFIED ) && flag_tm_ima && flag_ti_ima) {
             float d_time = fabs (tm_object->Time_Tracked - ti_object->Time_Tracked);
             if (d_time <= 1.0f) condition = 3;
         }
         // TO_m์ ๋นํด TO_i์ ๋ฉด์ ์ด ์ด๋ ์ ๋ ์์ ๊ฒฝ์ฐ...
         if (!condition && ar < 0.5f) { // PARAMETERS
            #if defined(__CPNI_CERT_SZM)
            // TO_i๊ฐ TO_STATUS_VERIFIED ์ํ๊ฐ ์๋ ๊ฒฝ์ฐ...
            if (!(ti_object->Status & TO_STATUS_VERIFIED)) {
            #else
            // TO_i๊ฐ TO_STATUS_VERIFIED ๋ฐ TO_STATUS_STATIC ์ํ๊ฐ ์๋ ๊ฒฝ์ฐ...
            if (!(ti_object->Status & (TO_STATUS_VERIFIED|TO_STATUS_STATIC))) {
            #endif
               ti_object->Time_Merged += FrameDuration;
               if (ti_object->Time_Merged > 0.3f) condition = 4; // PARAMETERS
            }
         }
         // TO_m๊ณผ TO_i๋ฅผ ํฉ์น๋ค.
         if (condition) {
            ChangeObjectRegionLabel (ObjRegionMap,fb_region,to_nos[i],to_nos[max_i]);
            ti_object->Status |= TO_STATUS_TO_BE_REMOVED;
            #if defined(__DEBUG_OBT) && defined(__CONSOLE)
            logd ("Tracked Object #%05d:\n",ti_object->ID);
            logd ("     Merged(codition=%d): to be removed.\n",condition);
            #endif
         }
      }
   }
}
 
 void ObjectTracking::MoveToLostObjectList (TrackedObject *t_object)

{
   TrackedObjects.Remove (t_object);
   LostObjects.Add (t_object);
   t_object->InitFrameCount = FrameCount;
   t_object->Status |= TO_STATUS_LOST;
   t_object->Time_Lost = 0.0f;
}

 int ObjectTracking::Perform (ForegroundDetection& frg_detector,BGRImage* s_cimage)

{
   #if defined(__DEBUG_OBT)
   _DVX_OBT = _DVY_OBT = 0;
   _DebugView_OBT->Clear (   );
   #endif
   // ์ผ์  ์ฃผ๊ธฐ๋ง๋ค ๋ผ์ด์ ์ค๋ฅผ ์ฒดํฌํ๋ค.
   if (!(FrameCount % 1000)) {
      #if defined(__LICENSE_MAC)
      if (__CheckLicenseKey (   )) __Flag_VACL_Initialized = FALSE;
      #endif
   }
   // VACL ๋ผ์ด๋ธ๋ฌ๋ฆฌ๋ฅผ ์ด๊ธฐํํ๋์ง ์ฒดํฌํ๋ค.
   if (!__Flag_VACL_Initialized) return (-1);
   // ๋ชจ๋ ํ์ฑ ์ํ๋ฅผ ์ฒดํฌํ๋ค.
   if (!IsEnabled (   )) return (1);
   // ์ฒ๋ฆฌ ์์์ ํด์๋๊ฐ ๋์ผํ์ง ์ฒดํฌํ๋ค.
   if (frg_detector.SrcImgSize.Width  != SrcImgSize.Width || frg_detector.SrcImgSize.Height != SrcImgSize.Height) return (2);
   // ๊ฐ์ข ๋ณ์ ์ค์ ์ ์ํํ๋ค.
   FrameCount++;
   ForegroundDetector = &frg_detector;
   SrcColorImage      = s_cimage;
   Shift              = frg_detector.Shift;
   int e_code;
   if (Options & OBT_OPTION_USE_BLOB_TRACKER) e_code = PerformBlobTracking (   );
   else e_code = PerformObjectTracking (   );
   #if defined(__DEBUG_OBT)
   _DebugView_OBT->Invalidate (   );
   #endif
   if (e_code != DONE) e_code += 2;
   return (e_code);
}

 int ObjectTracking::PerformBlobTracking (   )

{
   if (ForegroundDetector == NULL) return (1);
   #if defined(__DEBUG_OBT)
   _DebugView_OBT->DrawImage (BlobTracker.BlobRegionMap,_DVX_OBT,_DVY_OBT);
   _DVY_OBT += BlobTracker.BlobRegionMap.Height;
   _DebugView_OBT->DrawText  (_T("Previous Blob Region Map"),_DVX_OBT,_DVY_OBT,PC_GREEN);
   _DVY_OBT += 20;
   _DebugView_OBT->DrawImage (ForegroundDetector->FrgRegionMap,_DVX_OBT,_DVY_OBT);
   _DVY_OBT += BlobTracker.BlobRegionMap.Height;
   _DebugView_OBT->DrawText  (_T("Foreground Region Map"),_DVX_OBT,_DVY_OBT,PC_GREEN);
   _DVY_OBT += 20;
   #endif
   if (BlobTracker.Perform (ForegroundDetector->FrgRegionMap,ForegroundDetector->FrgRegions.Length - 1,&ForegroundDetector->SrcImage)) return (2);
   #if defined(__DEBUG_OBT)
   _DebugView_OBT->DrawImage (BlobTracker.BlobRegionMap,_DVX_OBT,_DVY_OBT);
   _DVY_OBT += BlobTracker.BlobRegionMap.Height;
   _DebugView_OBT->DrawText  (_T("Current Blob Region Map"),_DVX_OBT,_DVY_OBT,PC_GREEN);
   _DVY_OBT += 20;
   #endif
   float ru_rate = 30.0f * ObjUpdateRate / FrameRate;
   if (ru_rate < 0.1f) ru_rate = 0.1f;
   if (ru_rate > 1.0f) ru_rate = 1.0f;
   int trj_length = (int)(FrameRate * MaxTrjLength + 0.5f);
   #if defined(__DEBUG_OBT) && defined(__CONSOLE)
   logd ("\n[Tracked Blob List]\n");
   #endif
   TrackedBlob* t_blob = BlobTracker.TrackedBlobs.First (   );
   while (t_blob != NULL) {
      #if defined(__DEBUG_OBT) && defined(__CONSOLE)
      FPoint2D cp = t_blob->GetCenterPosition (   );
      logd ("[TB%05d] X = %04d, Y = %04d, W = %04d, H = %04d, CX = %04d, CY = %04d, RID = %04d ",
         t_blob->ID,t_blob->X,t_blob->Y,t_blob->Width,t_blob->Height,(int)cp.X,(int)cp.Y,t_blob->RgnID);
      if (t_blob->Status & TB_STATUS_TO_BE_REMOVED) logd ("[To be removed]");
      #endif
      if (t_blob->Status & TB_STATUS_CREATED) {
         TrackedObject* t_object = RegisterNewObject (   );
         t_object->TrkBlob = t_blob;
         #if defined(__DEBUG_OBT) && defined(__CONSOLE)
         logd ("[Created]");
         #endif
      }
      #if defined(__DEBUG_OBT) && defined(__CONSOLE)
      logd ("\n");
      #endif
      t_blob = BlobTracker.TrackedBlobs.Next (t_blob);
   }
   TrackedObject* t_object = TrackedObjects.First (   );
   while (t_object != NULL) {
      t_object->ClearStatus (   );
      t_object->FrameCount = FrameCount;
      TrackedBlob* t_blob = t_object->TrkBlob;
      if (t_blob != NULL) {
         if (!t_object->Count_Tracked) {
            // t_object->SetRegion (*t_blob);
            t_object->SetRegion (t_blob->CurRegion);
         }
         else {
            if (t_blob->Status & TB_STATUS_UNDETECTED) {
               // t_object->SetRegion (*t_blob);
               t_object->SetRegion (t_blob->CurRegion);
            }
            else {
               // t_object->UpdateRegion2 (*t_blob,ru_rate);
               t_object->UpdateRegion2 (t_blob->CurRegion,ru_rate);
            }
         }
         t_object->Area              = (float)t_blob->Area;
         t_object->AspectRatio       = (float)t_object->Height / t_object->Width;
         t_object->MaxMovingDistance = 0.5f * (t_blob->MaxMovingDistance1 + t_blob->MaxMovingDistance2);
         t_object->NorSpeed          = t_blob->NorSpeed;
         t_object->Speed             = t_blob->Speed;
         t_object->MeanVelocity      = t_blob->MeanVelocity;
         t_object->Velocity          = t_blob->Velocity;
         if (t_object->Width <= t_object->Height) {
            t_object->AxisLengthRatio = (float)t_object->Height / t_object->Width;
            t_object->MajorAxisLength = (float)t_object->Height;
            t_object->MinorAxisLength = (float)t_object->Width;
            t_object->MajorAxisAngle1 = 0.0f;
         }
         else {
            t_object->AxisLengthRatio = (float)t_object->Width / t_object->Height;
            t_object->MajorAxisLength = (float)t_object->Width;
            t_object->MinorAxisLength = (float)t_object->Height;
            t_object->MajorAxisAngle1 = 90.0f;
         }
         t_object->Centroid(t_blob->CX - t_blob->X,t_blob->CY - t_blob->Y);
         if (CheckObjectInTrackingArea (t_object) != DONE) t_object->Status |= TO_STATUS_TO_BE_REMOVED;
         if (!(t_object->Status & TO_STATUS_VALIDATED)) {
            if (CheckObjectInDynamicBackgroundArea (t_object) == DONE) t_object->Status |= TO_STATUS_TO_BE_REMOVED;
         }
         if (t_blob->Status & TB_STATUS_MERGED) {
            t_object->Status |= TO_STATUS_MERGED;
            t_object->Time_Merged += FrameDuration;
         }
         if (t_blob->Status & TB_STATUS_STATIC) {
            t_object->Status |= TO_STATUS_STATIC;
            t_object->Time_Static += FrameDuration;
            if (t_object->Time_Static >= (MaxTime_Static + 2.0f)) t_object->Time_Static = MaxTime_Static + 2.0f;
         }
         else {
            t_object->Time_Static -= FrameDuration;
            if (t_object->Time_Static < 0.0f) t_object->Time_Static = 0.0f;
         }
         if (t_blob->Status & TB_STATUS_TO_BE_REMOVED) {
            t_object->Status |= TO_STATUS_TO_BE_REMOVED;
         }
         if (t_blob->Status & TB_STATUS_UNDETECTED) {
            t_object->Status |= TO_STATUS_UNDETECTED;
            t_object->Time_Undetected += FrameDuration;
         }
         if (t_blob->Status & TB_STATUS_VALIDATED) {
            t_object->Status |= TO_STATUS_VALIDATED;
         }
         if (t_blob->Status & TB_STATUS_VERIFIED_START) {
            t_object->Status |= TO_STATUS_VERIFIED_START;
            t_object->ID = GetNewExtObjectID (   );
         }
         if (t_blob->Status & TB_STATUS_VERIFIED){
            t_object->Status |= TO_STATUS_VERIFIED;
         }
      }
      t_object->UpdateCountsAndTimes (FrameDuration);
      if (t_object->Time_BestShot > 0.2f) {
         t_object->Time_BestShot = 0.0f;
         float score = t_object->GetBestShotScore (SrcImgSize);
         if (score > t_object->MaxScore_BestShot) {
            t_object->MaxScore_BestShot = score;
            t_object->Status |= TO_STATUS_BEST_SHOT;
         }
      }
      t_object->UpdateAvgMinMaxPropertyValues (   );
      t_object->AddToTrkObjRegionList (trj_length);
      t_object = TrackedObjects.Next (t_object);
   }
   return (DONE);
}

// [์ฃผ์] ๋ณธ ํจ์ ๋ด๋ถ์์ TrackedObject ๊ฐ์ฒด๋ฅผ ์ง์  delete ํ์ง ๋ง ๊ฒ!
 int ObjectTracking::PerformObjectTracking (   )

{
   int i,j,k;
   #if defined(__DEBUG_OBT)
   CString text;
   #endif

   if (ForegroundDetector == NULL) return (1);
   int   trj_length = (int)(FrameRate * MaxTrjLength + 0.5f);
   float tu_rate    = 30.0f * ObjUpdateRate / FrameRate;
   if (tu_rate < 0.1f) tu_rate = 0.1f; // tu_rate์ ์ต์๊ฐ
   if (tu_rate > 1.0f) tu_rate = 1.0f; // tu_rate์ ์ต๋๊ฐ
   GImage& s_image  = ForegroundDetector->SrcImage;
   IArray2D& fr_map = ForegroundDetector->FrgRegionMap;
   FRArray1D fr_array;
   fr_array.Initialize (ForegroundDetector->FrgRegions);
   // TO๋ค์ ํฌ์ธํฐ์ ๋ฐฐ์ด์ ์ป๋๋ค.
   TOArray1D to_array;
   GetTrackedObjectArray (to_array);
   // TO๋ค์ ์ํ๋ฅผ ํด๋ฆฌ์ดํ๊ณ  ํ์ฌ ํ๋์์์ ์์ ์์น๋ฅผ ์ป๋๋ค.
   for (i = 1; i < to_array.Length; i++) {
      TrackedObject* t_object = to_array[i];
      t_object->ClearStatus (   );
      t_object->MatFrgRegions.Delete (   );
      t_object->GetPredictedPosition (t_object->MatchingPos);
   }
   #if defined(__DEBUG_OBT)
   _DebugView_OBT->DrawImage (fr_map,_DVX_OBT,_DVY_OBT);
   for (i = 1; i < to_array.Length; i++) {
      TrackedObject* t_object = to_array[i];
      _DebugView_OBT->DrawRectangle (_DVX_OBT + t_object->X,_DVY_OBT + t_object->Y,t_object->Width,t_object->Height,PC_RED,2);
      _DebugView_OBT->DrawRectangle (_DVX_OBT + t_object->MatchingPos.X,_DVY_OBT + t_object->MatchingPos.Y,t_object->Width,t_object->Height,PC_YELLOW);
      text.Format (_T("%d"),t_object->ID);
      _DebugView_OBT->DrawText (text,_DVX_OBT + t_object->X - 10,_DVY_OBT + t_object->Y - 10,PC_YELLOW,TRANSPARENT,FT_ARIAL,14);
   }
   _DVY_OBT += fr_map.Height;
   _DebugView_OBT->DrawText (_T("Current(Red) & Predicted(Yellow) Positions"),_DVX_OBT,_DVY_OBT,PC_GREEN);
   _DVY_OBT += 20;
   #endif
   int n_tos = to_array.Length - 1;
   int n_frs = fr_array.Length - 1;
   IArray2D oc_matrix(n_frs + 1,n_tos + 1);
   // ํ์ ์ ๋น ๋ฅธ ์ด๋ ๋ฌผ์ฒด์ ๊ฒ์ถ/์ถ์ ์ ์ํํ๋ค.
   if (Options & OBT_OPTION_PERFORM_FAST_MOVING_OBJECT_DETECTION) {
      if (!FMODetector.IsInitialized (   )) FMODetector.Initialize (ForegroundDetector->SrcImgSize);
      FMODetector.Perform (*ForegroundDetector);
      IArray2D& afmo_map = FMODetector.AFMOMap;
      GetFMOCorrespondenceMatrix (to_array,afmo_map,oc_matrix);
      GetFMOCorrespondences (oc_matrix,to_array,fr_array);
   }
   // TO๋ค์ FR(Foreground Region)๋ค ์ฌ์ด์ ๋์ ๊ด๊ณ๋ฅผ ๋ํ๋ด๋ ํ๋ ฌ์ ๊ตฌํ๋ค.
   // oc_matrix[i][j]์๋ TO_i์ FR_j ์ฌ์ด์ ๊ฒน์น๋ ํฝ์ ์๊ฐ ์ ์ฅ๋๋ค.
   GetObjectCorrespondenceMatrix (to_array,fr_map,oc_matrix);
   // ๊ฐ TO์ ๋ํ์ฌ ๊ทธ๊ฒ์ ๋์ํ๋ FR๋ค์ ์ฐพ์ ์ธ๋ฑ์ค ๊ฐ์ TO.MatFrgRegions ๋ฐฐ์ด์ ์ ์ฅํ๋ค.
   // ๋ํ ๊ฐ FR์ ๋ํ์ฌ ๊ทธ๊ฒ์ ๋์ํ๋ TO๋ค์ ์ฐพ์ ์ธ๋ฑ์ค ๊ฐ์ FR.MatTrkObjects ๋ฐฐ์ด์ ์ ์ฅํ๋ค.
   GetObjectCorrespondences (oc_matrix,to_array,fr_array);
   // ์ ๊ฒฝ ์์ญ์ ๋ํ ๋ง์คํฌ ์์์ ์ป๋๋ค.
   // ํน์  ํฝ์์ด ์ ๊ฒฝ ์์ญ์ ํด๋นํ๋ฉด 2์ ๊ฐ์ ๊ฐ๊ณ , ๊ทธ๋ ์ง ์์ผ๋ฉด 1์ ๊ฐ์ ๊ฐ๋๋ค.
   GetFrgRegionMaskImage (fr_map,MaskImage);
   // ObjRegionMap์ ํด๋ฆฌ์ดํ๋ค.
   ObjRegionMap.Clear (   );
   // ๋ถ๋ชจ ๊ฐ์ฒด์ ์ธ๋ฑ์ค ๊ฐ์ ์ ์ฅํ  ๋ฐฐ์ด์ ์์ฑํ๋ค.
   IArray1D pi_array(n_tos + n_frs + 1);
   pi_array.Clear (   );
   //
   // FR์ ๊ด์ ์์ ๋งค์นญ ์์์ ์ํํ๋ค.
   //
   #if defined(__DEBUG_OBT) && defined(__CONSOLE)
   logd ("Foreground Regions: %03d ----------------------------------------------------------\n\n",fr_array.Length - 1);
   #endif
   for (j = 1; j < fr_array.Length; j++) {
      FrgRegion &fg_rgn = fr_array[j];
      #if defined(__DEBUG_OBT) && defined(__CONSOLE)
      logd ("Foreground Region #%04d: [X:%d][Y:%d][W:%d][H:%d][CX:%4.1f][CY:%4.1f]\n",
         j,fg_rgn.X,fg_rgn.Y,fg_rgn.Width,fg_rgn.Height,fg_rgn.CX,fg_rgn.CY);
      #endif
      switch (fg_rgn.MatTrkObjects.Length) {
         // FR_j์ ๋์ํ๋ TO๊ฐ ์๋ ๊ฒฝ์ฐ...
         case 0: {
            #if defined(__DEBUG_OBT) && defined(__CONSOLE)
            logd ("     No corresponding TOs: ");
            #endif
            // FR_j๊ฐ TrackingArea ์์ ์กด์ฌํ๋ฉด ์๋ก์ด ๊ฐ์ฒด๋ก ๊ฐ์ฃผํ๋ค.
            if (CheckBlobOverlapWithTrackingArea (fr_map,fr_array[j],j) == DONE) {
               CopyBlobRegion (fr_map,fr_array[j],j,++n_tos,ObjRegionMap);
               #if defined(__DEBUG_OBT) && defined(__CONSOLE)
               logd (" Classified as a new object.\n");
               #endif
            }
            // FR_j๊ฐ TrackingArea ๋ฐ์ ์กด์ฌํ๋ฉด ๋ฌด์ํ๋ค.
            else {
               #if defined(__DEBUG_OBT) && defined(__CONSOLE)
               logd (" Out of tracking area.\n");
               #endif
            }
         }  break;
         // FR_j์ ๋์ํ๋ TO๊ฐ ํ ๊ฐ์ธ ๊ฒฝ์ฐ...
         // TO์ ๊ด์ ์์ ์์์ ์ํํ  ๋ ์ฒ๋ฆฌํ๋ค.
         case 1: {
            #if defined(__DEBUG_OBT) && defined(__CONSOLE)
            TrackedObject* m_object = to_array[fg_rgn.MatTrkObjects[0]];
            logd ("     Corresponds to [TO#%04d:%d]\n",m_object->ID,m_object->MatFrgRegions.Length);
            #endif
         }  break;
         // FR_j์ ๋์ํ๋ TO๊ฐ ์ฌ๋ฌ ๊ฐ์ธ ๊ฒฝ์ฐ...
         default: {
            #if defined(__DEBUG_OBT) && defined(__CONSOLE)
            logd ("     Corresponds to ");
            for (i = 0; i < fg_rgn.MatTrkObjects.Length; i++) {
               TrackedObject* m_object = to_array[fg_rgn.MatTrkObjects[i]];
               logd ("[TO#%04d:%d]",m_object->ID,m_object->MatFrgRegions.Length);
            }
            logd ("\n");
            #endif
            // Occlusion Reasoning์ ์ฐธ์ฌํ  TO๋ค์ so_array ๋ฐฐ์ด์ ๋ด๋๋ค.
            TOArray1D so_array(fg_rgn.MatTrkObjects.Length);
            for (i = 0; i < fg_rgn.MatTrkObjects.Length; i++) {
               so_array[i] = to_array[fg_rgn.MatTrkObjects[i]];
               so_array[i]->Status |= TO_STATUS_MERGED;
            }
            // ํํ๋ฆฟ ๋งค์นญ์ ํตํด ์ ํํ ๋งค์นญ ์์น๋ฅผ ์ฐพ๋๋ค.
            // ์ค๋ธ์ ํธ์ Depth ๊ฐ์ด ์์ ์์ผ๋ก ํํ๋ฆฟ ๋งค์นญ์ ์ํํ๋ค.
            for (k = 0; k < NumObjLayers; k++) {
               for (i = 0; i < so_array.Length; i++) {
                  TrackedObject* s_object = so_array[i];
                  if (s_object->Depth == k) {
                     GetObjectMatchingPosition (s_image,s_object);
                     if (!(s_object->Status & TO_STATUS_MASK_PUT)) {
                        s_object->PutMask (s_object->MatchingPos.X,s_object->MatchingPos.Y,0,MaskImage);
                        s_object->Status |= TO_STATUS_MASK_PUT;
                     }
                  }
               }
            }
            // Occlusion Reasoning์ ์ํํ๋ค.
            PerformOcclusionReasoning (s_image,so_array,fg_rgn.MatTrkObjects,fr_map,fr_array[j],j,ObjRegionMap);
            // FR_j์ ๋์ํ๋ TO๋ค ์ค์์ FR_j์ ๊ฐ์ฅ ๋ง์ด ๊ฒน์น๋ TO๋ฅผ TO_m์ด๋ผ๊ณ  ํ๋ฉด, ์กฐ๊ฑด์ ๋ง์กฑํ๋ TO๋ค์ TO_m๊ณผ ํฉ์น๋ค.
            MergeObjects (so_array,fg_rgn.MatTrkObjects,fg_rgn,j,oc_matrix,ObjRegionMap);
         }  break;
      }
   }
   //
   // TO์ ๊ด์ ์์ ๋งค์นญ ์์์ ์ํํ๋ค.
   //
   #if defined(__DEBUG_OBT) && defined(__CONSOLE)
   logd ("\nTracked Objects: %03d -------------------------------------------------------------\n\n",to_array.Length - 1);
   #endif
   for (i = 1; i < to_array.Length; i++) {
      TrackedObject *t_object = to_array[i];
      #if defined(__DEBUG_OBT) && defined(__CONSOLE)
      logd ("Tracked Object #%05d: [X:%d][Y:%d][W:%d][H:%d][CX:%4.1f][CY:%4.1f][PX:%d][PY:%d]\n",
         t_object->ID,t_object->X,t_object->Y,t_object->Width,t_object->Height,t_object->Centroid.X,t_object->Centroid.Y,t_object->MatchingPos.X,t_object->MatchingPos.Y);
      #endif
      switch (t_object->MatFrgRegions.Length) {
         // TO_i์ ๋์ํ๋ FR๊ฐ ์๋ ๊ฒฝ์ฐ
         case 0: {
            t_object->Status |= TO_STATUS_UNDETECTED;
            t_object->Time_Undetected += FrameDuration;
            #if defined(__DEBUG_OBT) && defined(__CONSOLE)
            logd ("     Undetected.\n");
            #endif
         }  break;
         // TO_i์ ๋์ํ๋ FR์ด ํ ๊ฐ์ธ ๊ฒฝ์ฐ
         case 1: {
            #if defined(__DEBUG_OBT) && defined(__CONSOLE)
            logd ("     Corresponds to [FR#%03d:%d]\n",t_object->MatFrgRegions[0],fr_array[t_object->MatFrgRegions[0]].MatTrkObjects.Length);
            #endif
            j = t_object->MatFrgRegions[0];
            // FR_j๊ฐ ์ค์ง TO_i์๋ง ๋์ํ๋ ๊ฒฝ์ฐ. ์ฆ, TO_i์ FR_j๊ฐ 1:1๋ก ๋์ํ๋ ๊ฒฝ์ฐ
            if (fr_array[j].MatTrkObjects.Length == 1) {
               int flag_copy_blob = TRUE;
               int flag_overwrite = FALSE;
               FrgRegion& fg_rgn = fr_array[j];
               // TO_i๊ฐ TO_STATUS_FAST_MOVING_OBJECT์ด๋ฉด, FR_j์ centroid ์์น๋ก TO_i์ centroid ์์น๋ฅผ ์ด๋์ํจ๋ค.
               if (!(t_object->Status & TO_STATUS_FAST_MOVING_OBJECT)) {
                  // FR_j์ TO_i ์ฌ์ด์ ํฌ๊ธฐ ๋ณํ๊ฐ ์ ์ผ๋ฉด FR_j์ centroid ์์น์ TO_i์ prediction ์์น์ ๊ฐ์ค ํฉ์ ์์น๋ก TO_i์ centroid ์์น๋ฅผ ์ด๋์ํจ๋ค.
                  if (t_object->GetMatchingPosition (fg_rgn,0.7f,1.3f) != DONE) {
                     // ๊ทธ๋ ์ง ์์ผ๋ฉด ํฌํ๋ฆฟ ๋งค์นญ์ ํตํด ๋งค์นญ ์์น๋ฅผ ์ฐพ๋๋ค.
                     GetObjectMatchingPosition (s_image,t_object);
                     if (t_object->Status & TO_STATUS_VERIFIED) {
                        // FR_j์ ๋ฉด์ ์ด TO_i์ ๋ฉด์ ์ ๋นํด ๋งค์ฐ ํฌ๋ฉด...
                        int fb_area = fg_rgn.Area;
                        int to_area = t_object->NumRgnPixels;
                        if (fb_area >= (int)(to_area * OBT_PARAM_OBJMAT_MAX_AREA_CHANGED_ABRUPTLY)) {
                           // FR_j๊ฐ ์์์ ๊ฐ์ฅ์๋ฆฌ์ ๊ฑธ์ณ ์๋์ง ํ๋จํ๋ค.
                           if (CheckRegionInMarginalArea (fg_rgn) == DONE) {
                              // FR_j๊ฐ ์์์ ๊ฐ์ฅ์๋ฆฌ์ ๊ฑธ์ณ ์๋ ์ํ๋ฉด, FR_j์ ์ ๊ท ์ถ์ฐ ๊ฐ์ฒด๊ฐ ์์ฌ ์์
                              // ๊ฐ๋ฅ์ฑ์ด ํฌ๊ธฐ ๋๋ฌธ์ FR_j๋ฅผ TO_i์ ์์ญ๊ณผ ๊ธฐํ ์์ญ์ผ๋ก ๋๋๋ค.
                              // ์ฐ์  FR_j์์ TO_i๋ฅผ ์ ์ธํ ์์ญ์ ๋ณต์ฌํ๋ค.
                              CopyResidualBlobRegion (fr_map,fg_rgn,j,t_object,++n_tos,ObjRegionMap);
                              flag_copy_blob = FALSE;
                              flag_overwrite = TRUE;
                           }
                           else {
                              // ๊ทธ๋ ์ง ์์ผ๋ฉด TO_i์ ๊ฐ๋ ค์ก๋ ๋ถ๋ถ์ด ๋์๊ฑฐ๋, ๋ธ์ด์ฆ์ผ ๊ฐ๋ฅ์ฑ์ด ํฌ๋ค.
                              t_object->Status |= TO_STATUS_CHANGED_ABRUPTLY;
                              t_object->Time_ChangedAbruptly += FrameDuration;
                              if (t_object->Time_ChangedAbruptly <= 0.2f) flag_copy_blob = FALSE; // PARAMETERS
                           }
                        }
                     }
                  }
               }
               // fg_rgn์ ObjRegionMap์ ๋ณต์ฌํ๋ค.
               if (flag_copy_blob) {
                  CopyBlobRegion (fr_map,fg_rgn,j,i,ObjRegionMap);
                  t_object->Status |= TO_STATUS_MATCHED_1TO1;
               }
               // ๊ทธ๋ ์ง ์์ผ๋ฉด TO_i๋ฅผ ObjRegionMap์ MatchingPos ์์น์ ๋ณต์ฌํ๋ค.
               else t_object->PutMask (t_object->MatchingPos.X,t_object->MatchingPos.Y,i,ObjRegionMap,flag_overwrite);
            }
         }  break;
         // TO_i์ ๋์ํ๋ FR๊ฐ ์ฌ๋ฌ ๊ฐ์ธ ๊ฒฝ์ฐ
         default: {
            #if defined(__DEBUG_OBT) && defined(__CONSOLE)
            logd ("     Corresponds to ");
            for (j = 0; j < t_object->MatFrgRegions.Length; j++)
               logd ("[FR#%03d:%d]",t_object->MatFrgRegions[j],fr_array[t_object->MatFrgRegions[j]].MatTrkObjects.Length);
            logd ("\n");
            #endif
            t_object->Status |= TO_STATUS_SPLIT;
            // ํํ๋ฆฟ ๋งค์นญ์ ํตํด ์ ํํ ๋งค์นญ ์์น๋ฅผ ์ฐพ๋๋ค.
            // t_object๊ฐ TO_STATUS_MERGED ์ํ์ด๋ฉด ์ด๋ฏธ ํํ๋ฆฟ ๋งค์นญ์ ํตํด ๋งค์นญ ์์น๋ฅผ ์ฐพ์ ์ํ์ด๋ฏ๋ก ๋ค์ ํ  ํ์๊ฐ ์๋ค.
            if (!(t_object->Status & TO_STATUS_MERGED)) GetObjectMatchingPosition (s_image,t_object);
            // TO_i์ ๊ฐ์ฅ ๋ง์ด ๊ฒน์น๋ FR๋ฅผ ์ฐพ๋๋ค.
            int max_ov = 0,max_j = 0;
            for (k = 0; k < t_object->MatFrgRegions.Length; k++) {
               j = t_object->MatFrgRegions[k];
               if (oc_matrix[i][j] > max_ov) max_ov = oc_matrix[i][j], max_j = j;
            }
            // TO_i์ ๋์ํ๋ ๊ฐ๊ฐ์ FR์ ๋ํ์ฌ TO_i์ ํธ๋์ ๋ฃ์์ง ๋ง์ง๋ฅผ ๊ฒฐ์ ํ๋ค.
            for (k = 0; k < t_object->MatFrgRegions.Length; k++) {
               j = t_object->MatFrgRegions[k];
               // FR_j๊ฐ ์ค์ง TO_i์๋ง ๋์ํ๋ ๊ฒฝ์ฐ.
               if (fr_array[j].MatTrkObjects.Length == 1) {
                  // FR_j๊ฐ TO_i์ ๋์ํ๋ FR๋ค ์ค์ ๊ฐ์ฅ ๋ง์ด TO_i์ ๊ฒน์น๋ FR์ด๋ฉด ๋ฌด์กฐ๊ฑด TO_i์ ํธ๋์ ๋ฃ๋๋ค.
                  if (j == max_j) CopyBlobRegion (fr_map,fr_array[j],j,i,ObjRegionMap);
                  //  ๊ทธ๋ ์ง ์์ผ๋ฉด FR_j๋ฅผ ์ ๊ท TO๋ก ๊ฐ์ฃผํ๋ค.
                  else {
                     CopyBlobRegion (fr_map,fr_array[j],j,++n_tos,ObjRegionMap);
                     pi_array[n_tos] = i; // FR_j์ ๋ถ๋ชจ TO์ ์ธ๋ฑ์ค ๊ฐ์ ๊ธฐ๋กํ๋ค.
                  }
               }
            }
         }  break;
      }
   }
   #if defined(__DEBUG_OBT)
   _DebugView_OBT->DrawImage (ObjRegionMap,_DVX_OBT,_DVY_OBT);
   for (i = 1; i < to_array.Length; i++) {
      TrackedObject* t_object = to_array[i];
      _DebugView_OBT->DrawRectangle (_DVX_OBT + t_object->X,_DVY_OBT + t_object->Y,t_object->Width,t_object->Height,PC_RED,2);
      _DebugView_OBT->DrawRectangle (_DVX_OBT + t_object->MatchingPos.X,_DVY_OBT + t_object->MatchingPos.Y,t_object->Width,t_object->Height,PC_GREEN);
      text.Format (_T("%d"),t_object->ID);
      _DebugView_OBT->DrawText (text,_DVX_OBT + t_object->X - 10,_DVY_OBT + t_object->Y - 10,PC_YELLOW,TRANSPARENT,FT_ARIAL,14);
   }
   _DVY_OBT += fr_map.Height;
   _DebugView_OBT->DrawText (_T("Current(Red) & Matching(Green) Positions"),_DVX_OBT,_DVY_OBT,PC_GREEN);
   _DVY_OBT += 20;
   #endif
   //
   // TO์ ๋ฉด์ , ๋ฐ์ด๋ฉ ๋ฐ์ค, Exposure ๊ฐ์ ๊ตฌํ๊ณ ,
   // TO์ ๊ฐ์ข ์นด์ดํธ/ํ์ ๊ฐ์ ์๋ฐ์ดํธํ๊ณ ,
   // TO์ ํํ๋ฆฟ, ์์ญ ์ ๋ณด, ํ์คํ ๊ทธ๋จ์ ์๋ฐ์ดํธํ๋ค.
   //
   IArray1D    ar_array(n_tos + 1);
   IB2DArray1D bb_array(ar_array.Length);
   GetObjectAreasAndBoundingBoxes (ObjRegionMap,ar_array,bb_array);
   for (i = 1; i < to_array.Length; i++) {
      TrackedObject *t_object = to_array[i];
      t_object->ObjRgnID = 0;
      t_object->ObjRgnBndBox.Clear (   );
      if (t_object->Status & TO_STATUS_TO_BE_REMOVED) continue;
      // TO์ ๋ฐ์ด๋ฉ ๋ฐ์ค๋ฅผ ์ฐพ์ง ๋ชปํ๋ฉด ํํ๋ฆฟ ์๋ฐ์ดํธ๋ฅผ ์ํํ์ง ์๋๋ค.
      if (ar_array[i] >= 4) {
         t_object->ObjRgnID     = i;
         t_object->ObjRgnArea   = ar_array[i];
         t_object->ObjRgnBndBox = bb_array[i];
      }
      // TO๊ฐ TO_STATUS_MERGED์ ์ํ์ธ ๊ฒฝ์ฐ, TO์ Exposure ๊ฐ์ ๊ตฌํ๋ค.
      if (t_object->Status & TO_STATUS_MERGED) {
         float exposure = (float)ar_array[i] / t_object->NumRgnPixels;
         if (exposure > 1.0f) exposure = 1.0f;
         t_object->Exposure = exposure;
      }
      // TO๊ฐ TO_STATUS_MERGED์ ์ํ๊ฐ ์๋๋ฉด Exposure ๊ฐ์ 1.0์ด ๋๋ค.
      else t_object->Exposure = 1.0f;
      // TO์ ์นด์ดํธ ๊ฐ๋ค๊ณผ ํ์ ๊ฐ์๋ค ์๋ฐ์ดํธํ๋ค.
      t_object->UpdateCountsAndTimes (FrameDuration);
      // TO์ ํํ๋ฆฟ ์์์ ์๋ฐ์ดํธํ๋ค.
      IBox2D d_rgn; float vu_rate;
      t_object->UpdateTemplateImage (s_image,ObjRegionMap,tu_rate,d_rgn,vu_rate);
      if (t_object->Status & TO_STATUS_TO_BE_REMOVED) continue;
      // TO์ ์์ญ ์ ๋ณด๋ฅผ ์๋ฐ์ดํธํ๋ค.
      t_object->UpdateRegion1 (d_rgn,vu_rate);
      // TO์ ๊ทธ๋ ์ด ํ์คํ ๊ทธ๋จ์ ์๋ฐ์ดํธํ๋ค.
      t_object->UpdateGrayHistogram (s_image,ObjRegionMap);
   }
   //
   // ์๋ก์ด TO๋ค์ ๋ํ์ฌ ํํ๋ฆฟ ์์๊ณผ ์์ญ ์ ๋ณด๋ฅผ ์ค์ ํ๋ค.
   //
   #if defined(__DEBUG_OBT) && defined(__CONSOLE)
   logd ("\nNew Objects ----------------------------------------------------------------------\n\n");
   #endif
   for (i = to_array.Length; i <= n_tos; i++) {
      if (ar_array[i] >= 4) {
         TrackedObject* t_object = RegisterNewObject (s_image,bb_array[i],i);
         // t_object์ ๋ถ๋ชจ TO๊ฐ ์๋ ๊ฒฝ์ฐ, ๋ถ๋ชจ TO์ Time_Dwell ๊ฐ์ ๋ฌผ๋ ค ๋ฐ๋๋ค.
         if (pi_array[i]) {
            TrackedObject* p_object = to_array[pi_array[i]]; 
            if (p_object->EventStatus & TO_EVENT_DWELL) t_object->EventStatus |= TO_EVENT_DWELL;
            t_object->Time_Dwell     = to_array[pi_array[i]]->Time_Dwell;
            t_object->EvtPrgTimeList = to_array[pi_array[i]]->EvtPrgTimeList;
         }
         #if defined(__DEBUG_OBT) && defined(__CONSOLE)
         logd ("Tracked Object #%05d: [X:%d][Y:%d][W:%d][H:%d]\n",t_object->ID,t_object->X,t_object->Y,t_object->Width,t_object->Height);
         #endif
      }
   }
   //
   // ์ฃผ์ด์ง ์กฐ๊ฑด๋ค์ ๋ํ์ฌ ๊ฐ๊ฐ์ TO๊ฐ ๋ง์กฑํ๋์ง ๊ฒ์ฌํ๋ค.
   //
   #if defined(__DEBUG_OBT) && defined(__CONSOLE)
   logd ("\nCondition Checking ---------------------------------------------------------------\n\n");
   #endif
   int n_layers = 0;
   TrackedObject *t_object = TrackedObjects.First (   );
   while (t_object != NULL) {
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED)) {
         // TO์ ๊ฐ์ข ํ๋กํผํฐ ๊ฐ์ ๊ตฌํ๋ค.
         GetObjectProperties (t_object);
         // 
         // ๋ชจ๋  ํ๋์์ ๋ํ์ฌ ํํ๋ ๊ฒ์ฌ.
         //
         // TO์ ๋ฐ์ด๋ฉ ๋ฐ์ค์ ์ค์ฌ์ด TrackingArea๋ฅผ ๋ฒ์ด๋๋ฉด 
         if (CheckObjectInTrackingArea (t_object) != DONE) {
            t_object->Status |= TO_STATUS_OUT_OF_TRACKING_AREA;
            #if defined(__DEBUG_OBT) && defined(__CONSOLE)
            logd ("Tracked Object #%05d:\n",t_object->ID);
            logd ("     Out of tracking area.\n");
            #endif
         }
         if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED)) {
            if (t_object->Count_Tracked >= MinCount_Tracked) t_object->Status |= TO_STATUS_VALIDATED;
            if (CheckObjectInOuterZone (t_object) == DONE  ) t_object->Status |= TO_STATUS_IN_OUTER_ZONE;
            //
            // TO๊ฐ TO_STATUS_VALIDATED ์ํ๊ฐ ๋ ํ์ ํํ๋ ๊ฒ์ฌ
            //
            if (t_object->Status & TO_STATUS_VALIDATED) {
               // TO๊ฐ ์์ง์์ด ์๋ ์ํ์ธ์ง ๊ฒ์ฌํ๋ค.
               // ์ผ์  ์๊ฐ๋์ ์์ง์์ด ์์ผ๋ฉด TO_STATUS_STATIC์ ์ํ๊ฐ ๋๋ค.
               if (CheckObjectStatic (t_object) == DONE) {
                  t_object->Time_Static += FrameDuration;
                  if (t_object->Time_Static >= (MaxTime_Static + 2.0f)) t_object->Time_Static = MaxTime_Static + 2.0f;
                  // TO๊ฐ Ghost Object์ธ์ง ๋๋ Abandoned Object์ธ์ง ๋๋ Removed Object์ธ์ง ํ๋จํ๋ค.
                  CheckStaticObjectConditions (t_object);
               }
               else {
                  t_object->Time_Static -= FrameDuration;
                  if (t_object->Time_Static < 0.0f) t_object->Time_Static = 0.0f;
                  t_object->EventStatus &= ~TO_EVENT_ABANDONED_END;
               }
            }
            //
            // TO๊ฐ TO_STATUS_VALIDATED ์ํ๊ฐ ๋๊ธฐ ์ ์ ํํ๋ ๊ฒ์ฌ
            //
            else {
               // TO๊ฐ TO_STATUS_UNDETECTED์ ์ํ์ด๋ฉด ์ ๊ฑฐ๋๋ค.
               if (t_object->Status & TO_STATUS_UNDETECTED) {
                  t_object->Status |= TO_STATUS_TO_BE_REMOVED;
                  #if defined(__DEBUG_OBT) && defined(__CONSOLE)
                  logd ("Tracked Object #%05d:\n",t_object->ID);
                  logd ("     Undetected & Not validated yet: To be removed.\n");
                  #endif
               }
               // TO๊ฐ TO_STATUS_MERGED์ ์ํ์ด๊ณ , TO์ ๋ฉด์ ์ด ์์ผ๋ฉด ์ ๊ฑฐ๋๋ค.
               if (t_object->Status & TO_STATUS_MERGED) {
                  if (t_object->Area < 25) { // PARAMETERS
                     t_object->Status |= TO_STATUS_TO_BE_REMOVED;
                     #if defined(__DEBUG_OBT) && defined(__CONSOLE)
                     logd ("Tracked Object #%05d:\n",t_object->ID);
                     logd ("     Merged & Very small area & Not validated yet: To be removed.\n");
                     #endif
                  }
               }
               // TO์ ๋ฐ์ด๋ฉ ๋ฐ์ค์ ์ค์ฌ์ด DynamicBkgArea ์์ ์กด์ฌํ๋ฉด ์ ๊ฑฐ๋๋ค.
               if (CheckObjectInDynamicBackgroundArea (t_object) == DONE) {
                  t_object->Status |= TO_STATUS_TO_BE_REMOVED;
                  #if defined(__DEBUG_OBT) && defined(__CONSOLE)
                  logd ("Tracked Object #%05d:\n",t_object->ID);
                  logd ("     Detected in the dynamic background areas: To be removed.\n");
                  #endif
               }
            }
         }
         //
         // TO์ ์ํ๊ฐ TO_STATUS_VALIDATED ์ฌ๋ถ์ ์๊ด ์์ด ์ํ
         //
         if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED)) {
            //
            // TO๊ฐ TO_STATUS_VERIFIED์ ์ํ๊ฐ ๋ ํ์ ํํ๋ ๊ฒ์ฌ.
            //
            if (t_object->Status & TO_STATUS_VERIFIED) {
               // 0.2์ด๋ง๋ค Best Shot ์ฌ๋ถ๋ฅผ ์ฒดํฌํ๋ค.
               if (t_object->Time_BestShot > 0.2f && !(t_object->Status & TO_STATUS_STATIC) && (t_object->Status & TO_STATUS_MATCHED_1TO1)) {
                  t_object->Time_BestShot = 0.0f;
                  float score = t_object->GetBestShotScore (SrcImgSize);
                  if (score > t_object->MaxScore_BestShot) {
                     t_object->MaxScore_BestShot = score;
                     t_object->Status |= TO_STATUS_BEST_SHOT;
                  }
               }
            }
            //
            // TO๊ฐ ์์ง TO_STATUS_VERIFIED์ ์ํ๊ฐ ๋๊ธฐ ์ ์ธ ๊ฒฝ์ฐ์ ํํ๋ ๊ฒ์ฌ.
            //
            else {
               // LostObjects ๋ฆฌ์คํธ์์ TO์ ๋งค์น๊ฐ ๋๋ ๊ฒ์ด ์๋์ง ์กฐ์ฌํ๋ค.
               // TO์ ๋งค์น๊ฐ ๋๋ LO๋ฅผ ์ฐพ์ผ๋ฉด TO์ LO๋ฅผ ํฉ์น๋ค.
               TrackedObject *l_object = FindMatchingObjectInLostObjectList (t_object);
               if (l_object != NULL) {
                  #if defined(__DEBUG_OBT) && defined(__CONSOLE)
                  logd ("Tracked Object #%05d\n",t_object->ID);
                  logd ("     Merged with Lost Object #%04d\n",l_object->ID);
                  #endif
                  l_object->Merge (t_object);
                  LostObjects.Remove (l_object);
                  TrackedObjects.Add (l_object);
                  l_object->Status &= ~(TO_STATUS_LOST|TO_STATUS_OUT_OF_TRACKING_AREA|TO_STATUS_UNDETECTED);
                  t_object->Status |= TO_STATUS_TO_BE_REMOVED;
               }
               if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED)) {
                  // TO๊ฐ TO_STATUS_UNDETECTED์ ์ํ์ธ ๊ฒฝ์ฐ, Time_Undetected > MaxTime_Undetected ์ํ์ด๋ฉด ์ ๊ฑฐํ๋ค.
                  if (t_object->Status & TO_STATUS_UNDETECTED) {
                     if (t_object->Time_Undetected > MaxTime_Undetected) {
                        t_object->Status |= TO_STATUS_TO_BE_REMOVED;
                        #if defined(__DEBUG_OBT) && defined(__CONSOLE)
                        logd ("Tracked Object #%05d:\n",t_object->ID);
                        logd ("     Undetected & Time_Undetected > MaxTime_Undetected: To be removed.\n");
                        #endif
                     }
                  }
                  else {
                     // TO์ ์ด๊ธฐ ๋ชจ์์ ๊ฒ์ฌํ์ฌ ์ ๋น์น ์์ TO๋ค์ ์ ๊ฑฐํ๋ค.
                     int e_code = CheckObjectInitMotion (t_object);
                     if (e_code == DONE) {  // TO๊ฐ ์ ํฉํ๋ค๊ณ  ๊ฒ์ฆ๋ ๊ฒฝ์ฐ
                        if (t_object->Status & TO_STATUS_VALIDATED) SetObjectVerified (t_object);
                     }
                     else if (e_code > 0) { // TO๋ฅผ ์ ๊ฑฐํ๋ ๊ฒฝ์ฐ
                        t_object->Status |= TO_STATUS_TO_BE_REMOVED;
                        #if defined(__DEBUG_OBT) && defined(__CONSOLE)
                        logd ("Tracked Object #%05d:\n",t_object->ID);
                        logd ("     Initial motion not acceptable: To be removed.\n");
                        #endif
                     }
                     // e_code < 0 ์ด๋ฉด ํ๋จ์ ์ ๋ณดํ๋ค.
                  }
               }
            }
         }
         if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED)) {
            t_object->FrameCount = FrameCount;
            t_object->UpdateAvgMinMaxPropertyValues (   );
            t_object->AddToTrkObjRegionList (trj_length);
            if (SrcColorImage != NULL && (t_object->Status & TO_STATUS_BEST_SHOT))
               t_object->GetColorHistogram (*SrcColorImage,ObjRegionMap,ForegroundDetector->FrgImage);
            if (t_object->Depth > n_layers) n_layers = t_object->Depth;
         }
      }
      #if defined(__DEBUG_OBT) && defined(__CONSOLE)
      if (t_object->Status & (TO_STATUS_TO_BE_REMOVED|TO_STATUS_OUT_OF_TRACKING_AREA|TO_STATUS_UNDETECTED|TO_STATUS_MERGED|TO_STATUS_SPLIT)) {
         logd ("Tracked Object #%05d: ",t_object->ID);
         if (t_object->Status & TO_STATUS_TO_BE_REMOVED       ) logd ("[To be removed]");
         if (t_object->Status & TO_STATUS_OUT_OF_TRACKING_AREA) logd ("[Out of tracking area]");
         if (t_object->Status & TO_STATUS_UNDETECTED          ) logd ("[Undetected]");
         if (t_object->Status & TO_STATUS_MERGED              ) logd ("[Merged]");
         if (t_object->Status & TO_STATUS_SPLIT               ) logd ("[Split]");
         logd ("\n");
      }
      #endif
      t_object = TrackedObjects.Next (t_object);
   }
   NumObjLayers = n_layers + 1; // ๊ฐ์ฒด๋ค์ ์ด ๋ ์ด์ด ์
   // TO_STATUS_OUT_OF_TRACKING_AREA ๋๋ TO_STATUS_UNDETECTED ์ํ์ TO๋ค์ LostObjects ๋ฆฌ์คํธ๋ก ์ฎ๊ธด๋ค.
   t_object = TrackedObjects.First (   );
   while (t_object != NULL) {
      TrackedObject* n_object = TrackedObjects.Next (t_object);
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED) && (t_object->Status & TO_STATUS_VERIFIED)) {
         if (t_object->Status & (TO_STATUS_OUT_OF_TRACKING_AREA|TO_STATUS_UNDETECTED)) {
            if (CheckObjectInNonHandoffArea (t_object) != DONE) {
               Event *event = t_object->Events.First (   );
               while (event != NULL) {
                  if (event->EvtRule->EventType == ER_EVENT_LOITERING) {
                     t_object->Status |= TO_STATUS_HANDOFF;
                     break;
                  }
                  event = t_object->Events.Next (event);
               }
            }
            MoveToLostObjectList (t_object);
            #if defined(__DEBUG_OBT) && defined(__CONSOLE)
            logd ("Tracked Object #%05d:\n",t_object->ID);
            logd ("     Undeteced: Moved to the lost object list.\n");
            #endif
         }
      }
      t_object = n_object;
   }
   // LO๊ฐ LostObjects ๋ฆฌ์คํธ์ ๋ฑ๋ก์ด ๋ ํ, MaxTime_Lost ๋งํผ์ ์๊ฐ์ด ๊ฒฝ๊ณผํ๋ฉด TO_STATUS_LOST_TIMEOUT์ ์ํ๊ฐ ๋๋ค.
   // TO_STATUS_LOST_TIMEOUT์ ์ํ๊ฐ ๋ ํ, TO_EVENT_LEAVING์ ์ํ๊ฐ ๋๋ฉด ์ญ์ ๊ฐ ์๋๊ณ  ์ ์ง๋๋ฉฐ, ์ง์ ๋ ์๋
   // ์๊ฐ์ด ์ง๋์ TO_EVENT_LEAVING ์ํ๊ฐ ํด์ ๋๋ฉด LO๋ ์ญ์ ๋๋ค.
   TrackedObject *l_object = LostObjects.First (   );
   while (l_object != NULL) {
      TrackedObject* n_object = LostObjects.Next (l_object);
      l_object->Time_Lost += FrameDuration;
      if (l_object->Status & TO_STATUS_LOST_TIMEOUT) {
         l_object->Status |= TO_STATUS_TO_BE_REMOVED;
         LostObjects.Remove (l_object);
         TrackedObjects.Add (l_object);
      }
      else if ((l_object->Status & TO_STATUS_FAST_MOVING_OBJECT) || (l_object->Time_Lost > MaxTime_Lost)) l_object->Status |= TO_STATUS_LOST_TIMEOUT;
      l_object = n_object;
   }
   // TO_STATUS_VERIFIED ์ํ์ธ TO๊ฐ TO_STATUS_TO_BE_REMOVED ์ํ๊ฐ ๋์์ ๋ TO.EndTime์ ๊ธฐ๋กํ๋ค.
   t_object = TrackedObjects.First (   );
   while (t_object != NULL) {
      if ((t_object->Status & TO_STATUS_VERIFIED) && (t_object->Status & TO_STATUS_TO_BE_REMOVED)) t_object->EndTime.GetLocalTime (   );
      t_object = TrackedObjects.Next (t_object);
   }
   #if defined(__DEBUG_OBT)
   _DebugView_OBT->DrawImage (ObjRegionMap,_DVX_OBT,_DVY_OBT);
   t_object = TrackedObjects.First (   );
   while (t_object != NULL) {
      _DebugView_OBT->DrawRectangle (_DVX_OBT + t_object->X,_DVY_OBT + t_object->Y,t_object->Width,t_object->Height,PC_GREEN,1);
      text.Format (_T("%d"),t_object->ID);
      _DebugView_OBT->DrawText (text,_DVX_OBT + t_object->X - 10,_DVY_OBT + t_object->Y - 10,PC_YELLOW,TRANSPARENT,FT_ARIAL,14);
      t_object = TrackedObjects.Next (t_object);
   }
   _DVY_OBT += ObjRegionMap.Height;
   _DebugView_OBT->DrawText (_T("Object Region Map"),_DVX_OBT,_DVY_OBT,PC_GREEN);
   _DVY_OBT += 40;
   _DebugView_OBT->DrawText (_T("Tracked Objects --------------------------------------------------------------------------------"),_DVX_OBT,_DVY_OBT,PC_RED);
   _DVY_OBT += 30;
   t_object = TrackedObjects.First (   );
   while (t_object != NULL) {
      _DebugView_OBT->DrawImage (t_object->TemplateImage,_DVX_OBT,_DVY_OBT);
      _DVX_OBT += t_object->Width + 10;
      _DebugView_OBT->DrawImage (t_object->WeightImage,_DVX_OBT,_DVY_OBT);
      _DVX_OBT += t_object->Width + 10;
      _DebugView_OBT->DrawImage (t_object->MaskImage,_DVX_OBT,_DVY_OBT);
      _DVX_OBT = 0, _DVY_OBT += t_object->Height + 4;
      text.Format (_T("Tracked Object #%05d "),t_object->ID);
      if (t_object->Count_Tracked == 1) text += _T("[NEW]");
      if (t_object->Status & TO_STATUS_FAST_MOVING_OBJECT) text += _T("[FMO]");
      if (t_object->Status & TO_STATUS_TO_BE_REMOVED     ) text += _T("[RMV]");
      if (t_object->Status & TO_STATUS_VERIFIED_START    ) text += _T("[VFS]");
      _DebugView_OBT->DrawText (text,_DVX_OBT,_DVY_OBT,PC_WHITE);
      _DVY_OBT += 25;
      t_object = TrackedObjects.Next (t_object);
   }
   #endif
   return (DONE);
}

 void ObjectTracking::PerformOcclusionReasoning (GImage &s_image,TOArray1D &to_array,IArray1D &to_nos,IArray2D &fr_map,IBox2D &fb_region,int fb_no,IArray2D &d_map)

{
   int i,j,x,y;
   int x1,y1,x2,y2;

   // TO๋ค์ MatchingPos์ ์์น์์ผฐ์ ๋, TO๋ค๊ณผ FR๋ฅผ ๋๋ฌ์ธ๋ ๋ฐ์ด๋ฉ ๋ฐ์ค๋ฅผ ๊ตฌํ๋ค.
   // min_p์ max_p๋ ๊ฐ๊ฐ ๋ฐ์ด๋ฉ ๋ฐ์ค์ Left-Top Corner์ Right-Bottom Corner์ ํด๋นํ๋ค.
   IP2DArray1D p_array(to_array.Length * 2 + 2);
   for (i = j = 0; i < to_array.Length; i++) {
      TrackedObject* t_object = to_array[i];
      p_array[j++] = t_object->MatchingPos;
      p_array[j++](t_object->MatchingPos.X + t_object->Width,t_object->MatchingPos.Y + t_object->Height);
   }
   p_array[j++](fb_region.X,fb_region.Y);
   p_array[j++](fb_region.X + fb_region.Width,fb_region.Y + fb_region.Height);
   IPoint2D min_x,max_x,min_p,max_p;
   FindMinMax (p_array,min_p,max_p,min_x,max_x);
   min_p.X--, min_p.Y--, max_p.X++, max_p.Y++; 
   int width  = max_p.X - min_p.X;
   int height = max_p.Y - min_p.Y;
   // ๊ฐ ํฝ์ ์์น์์ TO๋ค ์ฌ์ด์ ๊ฒน์น๋ ์๋ฅผ ๊ตฌํ๋ค.
   USArray2D ov_map(width,height); // ๊ฐ ํฝ์์์ TO๋ค์ ๊ฒน์น๋ ์๊ฐ ์ ์ฅ๋๋ค.
   ov_map.Clear (   );
   Array1D<GImage> t_images(to_array.Length); // ๊ฐ TO์ ํํ๋ฆฟ ์์์ด ์ ์ฅ๋๋ค.
   for (i = 0; i < t_images.Length; i++) {
      t_images[i].Create (width,height);
      t_images[i].Clear  (   );
      TrackedObject* t_object = to_array[i];
      int sx = t_object->MatchingPos.X - min_p.X;
      int sy = t_object->MatchingPos.Y - min_p.Y;
      int width  = t_object->Width;
      int height = t_object->Height;
      byte** st_image = t_object->TemplateImage;
      byte** sm_image = t_object->MaskImage;
      byte** dt_image = t_images[i];
      for (y1 = 0,y2 = sy; y1 < height; y1++,y2++) {
         byte*   l_sm_image = sm_image[y1];
         byte*   l_st_image = st_image[y1];
         byte*   l_dt_image = dt_image[y2];
         ushort* l_ov_map   = ov_map[y2];
         for (x1 = 0,x2 = sx; x1 < width; x1++,x2++) {
            if (l_sm_image[x1]) {
               l_dt_image[x2] = l_st_image[x1];
               l_ov_map[x2]++;
            }
         }
      }
   }
   // ๋ ๊ฐ ์ด์์ TO๊ฐ ๊ฒน์น๋ ์์ญ์์ ๊ฐ ํฝ์๋ง๋ค ์๋ ฅ ์์์ ํฝ์๊ฐ๊ณผ ๊ฐ TO์ ํฝ์๊ฐ์ ๋น๊ตํ์ฌ
   // ์๋ ฅ ์์๊ณผ ๊ฐ์ฅ ๊ฐ๊น์ด ํฝ์๊ฐ์ ๊ฐ๋ TO๋ฅผ ์ฐพ์ ๊ทธ๊ฒ์ ์ค์ฝ์ด๋ฅผ ์ฆ๊ฐ์ํจ๋ค.
   USArray1D ov_array(to_array.Length); // ๊ฐ TO๋ง๋ค ๊ฒน์น๋ ํฝ์ ์๊ฐ ์ ์ฅ๋๋ค.
   USArray1D oc_array(to_array.Length); // ๊ฐ TO๋ง๋ค ์๋ ฅ ์์๊ณผ ๊ฐ์ฅ ๊ฐ๊น์ด ํฝ์ ์๊ฐ ์ ์ ๋๋ค.
   ov_array.Clear (   );
   oc_array.Clear (   );
   int sx1 = min_p.X, sy1 = min_p.Y;
   int ex1 = max_p.X, ey1 = max_p.Y;
   if (sx1 < 0) sx1 = 0;
   if (sy1 < 0) sy1 = 0;
   if (ex1 > s_image.Width ) ex1 = s_image.Width;
   if (ey1 > s_image.Height) ey1 = s_image.Height;
   int sx2 = sx1 - min_p.X;
   int sy2 = sy1 - min_p.Y;
   for (y1 = sy1,y2 = sy2; y1 < ey1; y1++,y2++) {
      byte*   l_s_image = s_image[y1];
      ushort* l_ov_map  = ov_map[y2];
      for (x1 = sx1,x2 = sx2; x1 < ex1; x1++,x2++) {
         if (l_ov_map[x2] > 1) {
            int min_i = 0,min_d = 256;
            for (i = 0; i < t_images.Length; i++) {
               byte** dt_image   = t_images[i];
               byte*  l_dt_image = dt_image[y2];
               if (l_dt_image[x2]) {
                  int d = abs ((int)l_s_image[x1] - l_dt_image[x2]);
                  if (d < min_d) min_d = d, min_i = i;
                  ov_array[i] += l_ov_map[x2];
               }
            }
            oc_array[min_i] += l_ov_map[x2];
         }
      }
   }
   // TO_i๊ฐ ์นด๋ฉ๋ผ๋ก๋ถํฐ ๊ฐ๊น์ธ์๋ก oc_array[i]/ov_array[i] ๊ฐ์ ํฐ ๊ฐ์ ๊ฐ๋๋ค.
   Array1D<ObjectScore> os_array(to_array.Length);
   for (i = 0; i < os_array.Length; i++) {
      os_array[i].IndexNo = i;
      if (ov_array[i]) os_array[i].Score = (double)oc_array[i] / ov_array[i];
      else os_array[i].Score = 1.0;
   }
   qsort (os_array,os_array.Length,sizeof(ObjectScore),Compare_ObjectScore_Descending);
   // TO_i๊ฐ ์นด๋ฉ๋ผ๋ก๋ถํฐ ๊ฐ๊น์ธ์๋ก ์์ Depth ๊ฐ์ด ํ ๋น๋๋ค.
   for (i = 0; i < os_array.Length; i++) to_array[os_array[i].IndexNo]->Depth = i;
   // ํฐ Depth ๊ฐ์ ๊ฐ๋ TO๋ถํฐ or_map์ ํด๋น ์์ญ์ ๋ ์ด๋ธ ๊ฐ์ ๋ณต์ฌํ๋ค.
   USArray2D or_map(width,height);
   or_map.Clear (   );
   for (i = os_array.Length - 1; i >= 0; i--) {
      ushort n = (ushort)os_array[i].IndexNo;
      byte** t_image = t_images[n++];
      for (y = 0; y < height; y++) {
         byte*   l_t_image = t_image[y];
         ushort* l_or_map  = or_map[y];
         for (x = 0; x < width; x++)
            if (l_t_image[x]) l_or_map[x] = n;
      }   
   }
   // FR์ ์์ญ์์ TO๋ค์ ์ํด ๊ฒน์น์ง ์์ ๊ณณ๋ค์ ์ฐพ์๋ด์ด rm_image์ ๊ธฐ๋กํ๋ค.
   GImage rm_image(or_map.Width,or_map.Height);
   rm_image.Clear (   );
   for (y1 = sy1,y2 = sy2; y1 < ey1; y1++,y2++) {
      byte*   l_rm_image = rm_image[y2];
      int*    l_fr_map   = fr_map[y1];
      ushort* l_or_map   = or_map[y2];
      for (x1 = sx1,x2 = sx2; x1 < ex1; x1++,x2++) {
         if (l_fr_map[x1] == fb_no) {
            if (!l_or_map[x2]) l_rm_image[x2] = PG_WHITE;
         }
         else l_or_map[x2] = PG_BLACK;
      }
   }
   // rm_image์ ๊ฐ๊ฐ์ CC(Connected Component)์ ๋ํ์ฌ, ๊ทธ CC์ ์ ํ๋ TO๋ค์ ํฝ์์ ์๋ฅผ ์ผ๋ค.
   IArray2D  rm_map(rm_image.Width,rm_image.Height);
   int n_crs = LabelCRs (rm_image,rm_map);
   USArray2D cn_matrix(to_array.Length + 1,n_crs + 1);
   cn_matrix.Clear (   );
   int ex2 = rm_map.Width  - 1;
   int ey2 = rm_map.Height - 1;
   for (y = 1; y < ey2; y++) {
      y1 = y - 1, y2 = y + 1;
      byte*   l_rm_image0 = rm_image[y1];
      byte*   l_rm_image1 = rm_image[y];
      byte*   l_rm_image2 = rm_image[y2];
      ushort* l_or_map0   = or_map[y1];
      ushort* l_or_map1   = or_map[y];
      ushort* l_or_map2   = or_map[y2];
      int*    l_rm_map    = rm_map[y];
      for (x = 1; x < ex2; x++) {
         x1 = x - 1, x2 = x + 1;
         if (l_rm_image1[x] && !(l_rm_image0[x] && l_rm_image2[x] && l_rm_image1[x1] && l_rm_image1[x2])) {
            ushort* l_cn_mat = cn_matrix[l_rm_map[x]];
            l_cn_mat[l_or_map0[x] ]++;
            l_cn_mat[l_or_map2[x] ]++;
            l_cn_mat[l_or_map1[x1]]++;
            l_cn_mat[l_or_map1[x2]]++;
         }
      }
   }
   // ๊ฐ๊ฐ์ CC๋ ์ ํ๋ ํฝ์ ์๊ฐ ๊ฐ์ฅ ๋ง์ TO์ ์ํ๋ ๊ฒ์ผ๋ก ๊ฒฐ์ ํ๋ค.
   IArray1D cv_array1(cn_matrix.Width);
   IArray1D cv_array2(cn_matrix.Height);
   cv_array1.Clear (   );
   cv_array2.Clear (   );
   cv_array1.Copy (to_nos,0,to_nos.Length,1);
   for (i = 1; i < cn_matrix.Height; i++) {
      int    max_j  = 0;
      ushort max_cn = 0;
      ushort* l_cn_matrix = cn_matrix[i];
      for (j = 1; j < cn_matrix.Width; j++)
         if (l_cn_matrix[j] > max_cn) max_cn = l_cn_matrix[j], max_j = j;
      if (max_cn) cv_array2[i] = cv_array1[max_j];
   }
   for (y1 = sy1,y2 = sy2; y1 < ey1; y1++,y2++) {
      ushort* l_or_map = or_map[y2];
      int*    l_rm_map = rm_map[y2];
      int*    l_d_map  = d_map[y1];
      for (x1 = sx1,x2 = sx2; x1 < ex1; x1++,x2++) {
         if      (l_or_map[x2]) l_d_map[x1] = cv_array1[l_or_map[x2]];
         else if (l_rm_map[x2]) l_d_map[x1] = cv_array2[l_rm_map[x2]];
      }
   }
}

 int ObjectTracking::ReadFile (FileIO& file)

{
   if (file.Read ((byte*)&Options          ,1,sizeof(int  ))) return (1);
   if (file.Read ((byte*)&MinTime_Tracked  ,1,sizeof(float))) return (2);
   if (file.Read ((byte*)&MaxTime_Lost     ,1,sizeof(float))) return (3);
   if (file.Read ((byte*)&MaxTime_Static   ,1,sizeof(float))) return (4);
   if (file.Read ((byte*)&MinMovingDistance,1,sizeof(float))) return (5);
   if (TrackingAreas.ReadFile      (file)) return (6);
   if (NontrackingAreas.ReadFile   (file)) return (7);
   if (DynBkgAreas.ReadFile        (file)) return (8);
   if (NonHandoffAreas.ReadFile    (file)) return (9);
   if (FMODetector.ReadFile (file)) return (10);
   SyncBlobTrackingParams (   );
   return (DONE);
}

 int ObjectTracking::ReadFile (CXMLIO* pIO)

{
   static ObjectTracking dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("ObjectTracking")) {
      xmlNode.Attribute (TYPE_ID(xint) ,"Options"          ,&Options          ,&dv.Options);
      xmlNode.Attribute (TYPE_ID(float),"MinTime_Tracked"  ,&MinTime_Tracked  ,&dv.MinTime_Tracked);
      xmlNode.Attribute (TYPE_ID(float),"MaxTime_Lost"     ,&MaxTime_Lost     ,&dv.MaxTime_Lost);
      xmlNode.Attribute (TYPE_ID(float),"MaxTime_Static"   ,&MaxTime_Static   ,&dv.MaxTime_Static);
      xmlNode.Attribute (TYPE_ID(float),"MinMovingDistance",&MinMovingDistance,&dv.MinMovingDistance);
      TrackingAreas.ReadFile    (pIO,"TrackingAreas");
      NontrackingAreas.ReadFile (pIO,"NontrackingAreas");
      DynBkgAreas.ReadFile      (pIO,"DynBkgAreas");
      NonHandoffAreas.ReadFile  (pIO,"NonHandoffAreas");
      FMODetector.ReadFile (pIO);
      xmlNode.End (   );
      SyncBlobTrackingParams (   );
      return (DONE);
   }
   return (1);
}

 TrackedObject* ObjectTracking::RegisterNewObject (   )

{
   TrackedObject* t_object  = CreateInstance_TrackedObject (   );
   t_object->ID             = GetNewIntObjectID (   );
   t_object->InitFrameCount = FrameCount;
   t_object->StartTime.GetLocalTime (   );
   TrackedObjects.Add  (t_object);
   return (t_object);
}

 TrackedObject* ObjectTracking::RegisterNewObject (GImage& s_image,IBox2D& s_rgn,int s_no)

{
   TrackedObject* t_object = CreateInstance_TrackedObject (   );
   t_object->ID             = GetNewIntObjectID (   );
   t_object->InitFrameCount = FrameCount;
   t_object->Count_Tracked  = 1;
   t_object->Time_Tracked   = FrameDuration;
   t_object->StartTime.GetLocalTime (   );
   t_object->InitTemplate (s_image,ObjRegionMap,s_rgn,s_no,SrcColorImage);
   TrackedObjects.Add (t_object);
   return (t_object);
}

 void ObjectTracking::Reset (   )

{
   FrameCount = 0;
   Status     = 0;
   TrackedObjects.Move (LostObjects);
   InvalidateAllTrackedObjects (TrackedObjects);
}

 void ObjectTracking::SetObjectVerified (TrackedObject *t_object)
 
{
   t_object->Status |= TO_STATUS_VERIFIED;
   t_object->Status |= TO_STATUS_VERIFIED_START;
   t_object->Status |= TO_STATUS_BEST_SHOT;
   t_object->MaxScore_BestShot = t_object->GetBestShotScore (SrcImgSize);
   t_object->Time_BestShot = 0.0f;
   int new_id = GetNewExtObjectID (   );
   #if defined(__DEBUG_OBT) && defined(__CONSOLE)
   logd ("Tracked Object #%05d:\n",t_object->ID);
   logd ("     Verified: New ID [%d] assigned.\n",new_id);
   #endif
   t_object->ID = new_id;
}

 void ObjectTracking::SyncBlobTrackingParams (   )

{
   BlobTracker.MinCount_Tracked    = MinCount_Tracked;
   BlobTracker.MaxTime_Undetected1 = MaxTime_Undetected;
   BlobTracker.MaxTime_Undetected2 = MaxTime_Undetected;
   BlobTracker.MaxTime_Verified    = MC_VLV;
   BlobTracker.MinMovingDistance1  = MinMovingDistance;
   BlobTracker.MinMovingDistance2  = 0.0f;
   BlobTracker.RegionUpdateRate    = 1.0f;
}

 int ObjectTracking::UpdateAreaMaps (ISize2D& ri_size)
 
{
   if (!IsEnabled (   )) return (1);
   // Obtaining TrkAreaMap
   TrackingAreas.SetRefImageSize (ri_size);
   if (TrackingAreas.GetNumItems (   )) {
      TrkAreaMap.Clear (   );
      TrackingAreas.RenderAreaMap (TrkAreaMap,PG_WHITE);
   }
   else TrkAreaMap.Set (0,0,TrkAreaMap.Width,TrkAreaMap.Height,PG_WHITE);
   NontrackingAreas.SetRefImageSize (ri_size);
   if (NontrackingAreas.GetNumItems (   )) NontrackingAreas.RenderAreaMap (TrkAreaMap,128);
   // Obtaining DynBkgAreaMap
   DynBkgAreaMap.Clear (   );
   DynBkgAreas.SetRefImageSize (ri_size);
   if (DynBkgAreas.GetNumItems (   )) DynBkgAreas.RenderAreaMap (DynBkgAreaMap,PG_WHITE);
   // Obtaining NonHandoffAreaMap
   NonHandoffAreaMap.Clear (   );
   NonHandoffAreas.SetRefImageSize (ri_size);
   if (NonHandoffAreas.GetNumItems (   )) NonHandoffAreas.RenderAreaMap (NonHandoffAreaMap,PG_WHITE);
   return (DONE);
}

 void ObjectTracking::UpdateFaceRegions (   )

{
   TrackedObject* t_object = TrackedObjects.First (   );
   while (t_object != NULL) {
      t_object = TrackedObjects.Next (t_object);
   }
}

 int ObjectTracking::UpdateObjectThumbnails (byte *si_buffer,ISize2D &si_size,int pix_fmt,float cbs_ratio,ISize2D *tn_size)
 
{
   if (!IsEnabled (   )) return (1);
   FPoint2D scale;
   scale.X = (float)si_size.Width  / SrcImgSize.Width;
   scale.Y = (float)si_size.Height / SrcImgSize.Height;
   TrackedObject* t_object = TrackedObjects.First (   );
   while (t_object != NULL) {
      if (t_object->Status & TO_STATUS_BEST_SHOT) {
         IBox2D so_box = t_object->Scale (scale);
         t_object->ThumbPixFmt = pix_fmt;
         CaptureThumbnail (si_buffer,si_size,pix_fmt,so_box,t_object->ThumbBuffer,t_object->ThumbSize,cbs_ratio,tn_size);
      }
      t_object = TrackedObjects.Next (t_object);
   }
   return (DONE);
}

 int ObjectTracking::WriteFile (FileIO &file)

{
   if (file.Write ((byte *)&Options          ,1,sizeof(int  ))) return (1);
   if (file.Write ((byte *)&MinTime_Tracked  ,1,sizeof(float))) return (2);
   if (file.Write ((byte *)&MaxTime_Lost     ,1,sizeof(float))) return (3);
   if (file.Write ((byte *)&MaxTime_Static   ,1,sizeof(float))) return (4);
   if (file.Write ((byte *)&MinMovingDistance,1,sizeof(float))) return (5);
   if (TrackingAreas.WriteFile      (file)) return (6);
   if (NontrackingAreas.WriteFile   (file)) return (7);
   if (DynBkgAreas.WriteFile        (file)) return (8);
   if (NonHandoffAreas.WriteFile    (file)) return (9);
   if (FMODetector.WriteFile (file)) return (10);
   return (DONE);
}

 int ObjectTracking::WriteFile (CXMLIO* pIO)

{
   static ObjectTracking dv; // jun : default value;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("ObjectTracking"))
   {
      xmlNode.Attribute (TYPE_ID (xint) ,"Options"          ,&Options          ,&dv.Options);
      xmlNode.Attribute (TYPE_ID (float),"MinTime_Tracked"  ,&MinTime_Tracked  ,&dv.MinTime_Tracked);
      xmlNode.Attribute (TYPE_ID (float),"MaxTime_Lost"     ,&MaxTime_Lost     ,&dv.MaxTime_Lost);
      xmlNode.Attribute (TYPE_ID (float),"MaxTime_Static"   ,&MaxTime_Static   ,&dv.MaxTime_Static);
      xmlNode.Attribute (TYPE_ID (float),"MinMovingDistance",&MinMovingDistance,&dv.MinMovingDistance);
      TrackingAreas.WriteFile      (pIO,"TrackingAreas");
      NontrackingAreas.WriteFile   (pIO,"NontrackingAreas");
      DynBkgAreas.WriteFile        (pIO,"DynBkgAreas");
      NonHandoffAreas.WriteFile    (pIO,"NonHandoffAreas");
      FMODetector.WriteFile (pIO);
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 void CleanTrackedObjectList (TrackedObjectList &to_list)

{
   #if defined(__DEBUG_OBT) && defined(__CONSOLE)
   logd ("Removed Objects: ");
   #endif
   TrackedObject *t_object = to_list.First (   );
   while (t_object != NULL) {
      TrackedObject *n_object = to_list.Next (t_object);
      if (t_object->Status & TO_STATUS_TO_BE_REMOVED) {
         if (t_object->TrkBlob != NULL) t_object->TrkBlob->Status |= TB_STATUS_TO_BE_REMOVED;
         #if defined(__DEBUG_OBT) && defined(__CONSOLE)
         logd ("[%d]",t_object->ID);
         #endif
         to_list.Remove (t_object);
         delete t_object;
      }
      t_object = n_object;
   }
   #if defined(__DEBUG_OBT) && defined(__CONSOLE)
   logd ("\n\n");
   #endif
}

 TrackedObject* CreateInstance_TrackedObject (   )

{
   if (__CreationFunction_TrackedObject == NULL) {
      TrackedObject *t_object = new TrackedObject;
      return (t_object);
   }
   else return (__CreationFunction_TrackedObject (   ));
}

 int GetObjectTypeName (int obj_type,char *d_buffer)
 
{
   int r_code = DONE;
   switch (obj_type) {
      case TO_TYPE_HUMAN:
         #if defined(__KOREAN)
         strcpy (d_buffer,"์ฌ๋");
         #else
         strcpy (d_buffer,"Human");
         #endif
         break;
      case TO_TYPE_VEHICLE:
         #if defined(__KOREAN)
         strcpy (d_buffer,"์ฐจ๋");
         #else
         strcpy (d_buffer,"Vehicle");
         #endif
         break;
      case TO_TYPE_UNKNOWN:
         #if defined(__KOREAN)
         strcpy (d_buffer,"๋ฏธํ์ธ");
         #else
         strcpy (d_buffer,"Unknown");
         #endif
         break;
      case TO_TYPE_FACE:
         #if defined(__KOREAN)
         strcpy (d_buffer,"์ผ๊ตด");
         #else
         strcpy (d_buffer,"Face");
         #endif
         break;
      case TO_TYPE_CROWD:
         #if defined(__KOREAN)
         strcpy (d_buffer,"๊ตฐ์ค");
         #else
         strcpy (d_buffer,"Crowd");
         #endif
         break;
      case TO_TYPE_FLAME:
         #if defined(__KOREAN)
         strcpy (d_buffer,"๋ถ๊ฝ");
         #else      
         strcpy (d_buffer,"Flame");
         #endif
         break;
      case TO_TYPE_SMOKE:
         #if defined(__KOREAN)
         strcpy (d_buffer,"์ฐ๊ธฐ");
         #else
         strcpy (d_buffer,"Smoke");
         #endif
         break;
      case TO_TYPE_WATER_LEVEL:
         #if defined(__KOREAN)
         strcpy (d_buffer,"์์");
         #else
         strcpy (d_buffer,"Water Level");
         #endif
         break;
      default:
         d_buffer[0] = 0;
         r_code = 1;
         break;
   };
   return (r_code);
}

 void InvalidateAllTrackedObjects (TrackedObjectList &to_list)
 
{
   TrackedObject *t_object = to_list.First (   );
   while (t_object != NULL) {
      t_object->Status |= TO_STATUS_TO_BE_REMOVED;
      t_object = to_list.Next (t_object);
   }
}

 void SetCreationFunction_TrackedObject (TrackedObject*(*func)(   ))

{
   __CreationFunction_TrackedObject = func;
}

}

#include "vacl_event.h"
#include "vacl_filter.h"
#include "vacl_grptrack.h"
#include "vacl_profile.h"

#if defined(__DEBUG_GRT)
#include "Win32CL/Win32CL.h"
extern int _DVX_GRT,_DVY_GRT;
extern CImageView* _DebugView_GRT;
#endif

 namespace VACL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Class: TrackedGroup
//
///////////////////////////////////////////////////////////////////////////////

 TrackedGroup::TrackedGroup (   )
 
{
   
   MDAP            = 0;
   NumPersons      = 0;
   BoxActivity     = 0.0f;
   FlowActivity    = 0.0f;
   FlowVariance    = 0.0f;
   MaxBoxActivity  = 0.0f;
   MaxFlowActivity = 0.0f;
   MaxFlowVariance = 0.0f;
   Time_Gathering  = 0.0f;
   Time_Violence   = 0.0f;
}

 IBox2D TrackedGroup::GetBoundingBox (   )
 
{
   int i;
   
   int sx = 100000;
   int sy = 100000;
   int ex = -1;
   int ey = -1;
   for (i = 0; i < Members.Length; i++) {
      int x1 = Members[i]->X;
      int y1 = Members[i]->Y;
      int x2 = x1 + Members[i]->Width;
      int y2 = y1 + Members[i]->Height;
      if (sx > x1) sx = x1;
      if (sy > y1) sy = y1;
      if (ex < x2) ex = x2;
      if (ey < y2) ey = y2;
   }
   IBox2D b_box;
   b_box(sx - 4,sy - 4,ex - sx + 8,ey - sy + 8);
   return (b_box);
}

 float TrackedGroup::GetActivityScore (FArray1D &s_array,float duration,float gf_size,float d_thrsld)
 
{
   int i;

   FArray1D fs_array(s_array.Length);
   GaussianFiltering (s_array,gf_size,fs_array);
   BArray1D mm_array(fs_array.Length);
   FindLocalMinMax (fs_array,mm_array);
   d_thrsld *= Width;
   int   score   = 0;
   float l_value = mm_array[0];
   for (i = 0; i < mm_array.Length; i++) {
      if (mm_array[i] > 0) {
         if (fabs (l_value - fs_array[i]) >= d_thrsld) score++;
         l_value = fs_array[i];
      }
   }
   return (score / duration);
}

 void TrackedGroup::GetBoxActivity (float frm_rate,float period)
 
{
   int i;
   
   BoxActivity = 0.0f;
   int n_frames = (int)(frm_rate * period);
   int n_items = TrkBlobRegions.GetNumItems (   );
   if (n_items < n_frames) return;
   FArray1D hd_array(n_frames); // 그룹 내 사람들 간의 최대 거리
   FArray1D ty_array(n_frames); // 그룹 박스의 최상위 y 값
   int ei = n_frames - 1;
   TrackedGroupRegion *tg_rgn = (TrackedGroupRegion*)TrkBlobRegions.First (   );
   FPoint2D sp = tg_rgn->GetCenterPosition (   );
   FPoint2D ep;
   for (i = 0;   ; i++) {
      hd_array[i] = (float)tg_rgn->MDAP;
      ty_array[i] = (float)tg_rgn->Y;
      if (i == ei) {
         ep = tg_rgn->GetCenterPosition (   );
         break;
      }
      tg_rgn = (TrackedGroupRegion*)TrkBlobRegions.Next ((TrkBlobRegion*)tg_rgn);
   }
   float a1 = GetActivityScore (hd_array,period,1.2f,0.05f ); // PARAMETERS
   float a2 = GetActivityScore (ty_array,period,1.0f,0.005f); // PARAMETERS
   BoxActivity = 0.5f * (1.5f * a1 + a2); // PARAMETERS
}

 void TrackedGroup::GetPropertyValues (float frm_rate,float period)
 
{
   int i,j,n;

   MDAP         = 0;
   NumPersons   = 0;
   FlowActivity = FlowVariance = 0.0f;
   // 그룹 내의 멤버들 중에 최대 면적을 갖는 멤버를 구한다.
   int max_i = 0,max_a = 0;
   for (i = 0; i < Members.Length; i++) {
      TrackedObject* t_object = Members[i];
      int area = t_object->Width * t_object->Height;
      if (area > max_a) max_a = area, max_i = i;
   }
   // 그룹의 각종 속성 값을 구한다.
   int min_hp   = 100000;
   int max_hp   = -1;
   int a_thrsld = (int)(0.2f * max_a); // PARAMETERS
   float max_fa = 0.0f, max_fv = 0.0f;
   for (i = 0; i < Members.Length; i++) {
      TrackedObject* t_object = Members[i];
      if ((t_object->Type & TO_TYPE_HUMAN) || (t_object->TypeHistory & TO_TYPE_HUMAN)) {
         int area = t_object->Width * t_object->Height;
         // 일정 면적 이상되는 멤버들만 고려한다.
         if (area >= a_thrsld) {
            NumPersons += t_object->Persons.Length;
            if (t_object->FPFlowActivity > max_fa) max_fa = t_object->FPFlowActivity;
            if (t_object->FPFlowVariance > max_fv) max_fv = t_object->FPFlowVariance;
            Array1D<TrackedObject::PersonInfo>& hp_array = t_object->Persons;
            for (j = 0; j < hp_array.Length; j++) {
               int hp = t_object->X + hp_array[j].X;
               if (hp < min_hp) min_hp = hp;
               if (hp > max_hp) max_hp = hp;
            }
         }
      }
   }
   if (max_hp != -1) MDAP = max_hp - min_hp;
   float a = 0.5f; // PARAMETERS
   max_fa *= a; max_fv *= a;
   int   n_fa   = 0,n_fv = 0;
   float sum_fa = 0.0f,sum_fv = 0.0f;
   for (i = 0; i < Members.Length; i++) {
      TrackedObject* t_object = Members[i];
      if ((t_object->Type & TO_TYPE_HUMAN) || (t_object->TypeHistory & TO_TYPE_HUMAN)) {
         int area = t_object->Width * t_object->Height;
         // 일정 면적 이상되는 멤버들만 고려한다.
         if (area >= a_thrsld) {
            if (t_object->FPFlowActivity > max_fa) {
               sum_fa += t_object->FPFlowActivity;
               n_fa++;
            }
            if (t_object->FPFlowVariance > max_fv) {
               sum_fv += t_object->FPFlowVariance;
               n_fv++;
            }
         }
      }
   }
   if (n_fa) FlowActivity = sum_fa / n_fa;
   if (n_fv) FlowVariance = sum_fv / n_fv;
   int en = (int)(period * frm_rate);
   TrackedGroupRegion* tg_rgn = (TrackedGroupRegion*)TrkBlobRegions.First (   );
   for (n = 0; n < en && tg_rgn != NULL; n++) {
      if (tg_rgn->BoxActivity  > MaxBoxActivity ) MaxBoxActivity  = tg_rgn->BoxActivity;
      if (tg_rgn->FlowActivity > MaxFlowActivity) MaxFlowActivity = tg_rgn->FlowActivity;
      if (tg_rgn->FlowVariance > MaxFlowVariance) MaxFlowVariance = tg_rgn->FlowVariance;
      tg_rgn = (TrackedGroupRegion*)TrkBlobRegions.Next ((TrkBlobRegion*)tg_rgn);
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: GroupTracking
//
///////////////////////////////////////////////////////////////////////////////

 GroupTracking::GroupTracking (   )
 
{
   // Input Parameters (GroupTracking)
   Options             = 0;
   ActivityPeriod      = 3.0f;

   // Input Parameters (BlobTracking)
   MinBlobArea         = 0;
   MinCount_Tracked    = 1;
   MaxNorSpeed_Static  = 10.0f;
   MaxTime_Undetected1 = 0.01f;
   MaxTime_Undetected2 = 1.0f;
   MaxTime_Verified    = MC_VLV;
   MaxTrjLength        = ActivityPeriod + 1.0f;
      // [주의] MaxTrjLength > ActivityMeasurePeriod 를 만족해야 한다.
      //        그렇지 않으면 "폭력" 이벤트가 제대로 검출되지 않는다.
}

 GroupTracking::~GroupTracking (   )
 
{
   Close (   );
}

 TrkBlobRegion* GroupTracking::AddToTrkBlobRegionList (TrackedBlob *t_blob)
 
{
   TrackedGroupRegion *tg_rgn = (TrackedGroupRegion*)BlobTracking::AddToTrkBlobRegionList (t_blob);
   if (tg_rgn != NULL) {
      TrackedGroup *t_group = (TrackedGroup*)t_blob;
      tg_rgn->MDAP         = t_group->MDAP;
      tg_rgn->BoxActivity  = t_group->BoxActivity;
      tg_rgn->FlowActivity = t_group->FlowActivity;
      tg_rgn->FlowVariance = t_group->FlowVariance;
   }
   return ((TrkBlobRegion*)tg_rgn);
}

 void GroupTracking::Close (   )
 
{
   BlobTracking::Close   (   );
   GroupMaskImage.Delete (   );
}

 TrackedBlob* GroupTracking::CreateTrackedBlob (   )
 
{
   return ((TrackedBlob*)new TrackedGroup);
}

 TrkBlobRegion* GroupTracking::CreateTrkBlobRegion (   )
 
{
   return ((TrkBlobRegion*)new TrackedGroupRegion);
}

 void GroupTracking::GetGroupMaskImage (ObjectTracking &obj_tracker,GImage &d_image)
 
{
   d_image.Clear (   );
   TrackedObjectList &to_list = obj_tracker.TrackedObjects;
   TrackedObject *t_object = to_list.First (   );
   while (t_object != NULL) {
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED) && (t_object->Status & TO_STATUS_VERIFIED)) {
         if ((t_object->Type & TO_TYPE_HUMAN) || (t_object->TypeHistory & TO_TYPE_HUMAN)) PutMask (t_object,d_image);
      }
      t_object = to_list.Next (t_object);
   }
}

 void GroupTracking::GetGroupMembers (ObjectTracking &obj_tracker)
 
{
   int i,j,k;

   // TG: TrackedGroup, TO: TrackedObject
   TrackedObjectList &to_list = obj_tracker.TrackedObjects;
   IArray1D n_array(TrackedBlobs.GetNumItems (   ) + 1); // 각 TG에 속하는 TO의 개수를 기록하는 배열
   n_array.Clear (   );
   BArray2D m_array(to_list.GetNumItems (   ) + 1,n_array.Length); // 각 TG에 속하는 TO들을 표시한 배열
   m_array.Clear (   );
   // TG와 TO 사이의 대응 관계를 구한다.
   int ex = BlobRegionMap.Width  - 2;
   int ey = BlobRegionMap.Height - 2;
   TrackedObject *t_object = to_list.First (   );
   for (j = 1; t_object != NULL; j++) {
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED) && (t_object->Status & TO_STATUS_VERIFIED)) {
         if ((t_object->Type & TO_TYPE_HUMAN) || (t_object->TypeHistory & TO_TYPE_HUMAN)) {
            int cx = (int)(t_object->CenterPos.X + 0.5f);
            int cy = (int)(t_object->CenterPos.Y + 0.5f);
            if (cx < 1) cx = 1;
            else if (cx > ex) cx = ex;
            if (cy < 1) cy = 1;
            else if (cy > ey) cy = ey;
            i = BlobRegionMap[cy][cx]; // TO_j가 속하는 TG의 인덱스 번호
            m_array[i][j] = TRUE;
            n_array[i]++;
         }
      }
      t_object = to_list.Next (t_object);
   }
   // 각 TG에 속하는 TO들을 TG.Members에 저장한다.
   TOArray1D to_array;
   obj_tracker.GetObjectArray (to_array);
   TrackedGroup *t_group = (TrackedGroup*)TrackedBlobs.First (   );
   while (t_group != NULL) {
      if (t_group->RgnID) {
         i = t_group->RgnID;
         if (n_array[i]) { 
            t_group->Members.Create (n_array[i]);
            byte *l_m_array = m_array[i];
            for (j = 1,k = 0; j < m_array.Width; j++) {
               if (l_m_array[j]) t_group->Members[k++] = to_array[j];
            }
         }
         else {
            t_group->Members.Delete (   );
            t_group->Status |= TB_STATUS_TO_BE_REMOVED;
         }
      }
      t_group = (TrackedGroup*)TrackedBlobs.Next (t_group);
   }
}

 int GroupTracking::Initialize (ISize2D& si_size,float frm_rate)
 
{
   if (BlobTracking::Initialize (si_size,frm_rate)) return (1);
   GroupMaskImage.Create (si_size.Width,si_size.Height);
   Reset (   );
   return (DONE);
}

 void GroupTracking::InitParams (EventDetection& evt_detector)

{
   if (evt_detector.IsEventZoneAvailable (ER_EVENT_VIOLENCE)) Options |= GRT_OPTION_TRACK_FEATURE_POINTS_OF_GROUP_MEMBERS;
   else Options &= ~GRT_OPTION_TRACK_FEATURE_POINTS_OF_GROUP_MEMBERS;
}

 int GroupTracking::Perform (ObjectTracking& obj_tracker)
 
{
   if (!IsEnabled (   )) return (1);
   #if defined(__DEBUG_GRT)
   if (_DebugView_GRT) {
      _DVX_GRT = _DVY_GRT = 0;
      _DebugView_GRT->Clear (   );
      _DebugView_GRT->DrawImage (obj_tracker.ObjRegionMap,_DVX_GRT,_DVY_GRT);
      _DVY_GRT += obj_tracker.ObjRegionMap.Height;
      _DebugView_GRT->DrawText  (_T("Object Region Map"),_DVX_GRT,_DVY_GRT,PC_GREEN);
      _DVY_GRT += 20;
   }
   #endif
   GetGroupMaskImage (obj_tracker,GroupMaskImage);
   BlobTracking::Perform (GroupMaskImage);
   GetGroupMembers (obj_tracker);
   TrackedGroup *t_group = (TrackedGroup*)TrackedBlobs.First (   );
   while (t_group != NULL) {
      if (t_group->Status & TB_STATUS_TO_BE_REMOVED) {
         if (t_group->EvtGenObject != NULL) t_group->EvtGenObject->Status |= TO_STATUS_TO_BE_REMOVED;
      }
      else {
         t_group->GetPropertyValues (FrameRate,0.5f * ActivityPeriod); // PARAMETERS
         t_group->GetBoxActivity    (FrameRate,ActivityPeriod);
         #if defined(__DEBUG_GRT) && defined(__CONSOLE)
         logd ("[Tracked Group #%04d]\n",t_group->ID);
         if (!t_group->Members) logd ("No Members\n");
         else {
            logd ("- Members     : ");
            for (int i = 0; i < t_group->Members.Length; i++) logd ("%d ",t_group->Members[i]->ID);
            logd ("\n");
         }
         logd ("- NumPersons     : %d\n"  ,t_group->NumPersons);
         logd ("- BoxActivity    : %.2f\n",t_group->BoxActivity);
         logd ("- FlowActivity   : %.2f\n",t_group->FlowActivity);
         logd ("- FlowVariance   : %.2f\n",t_group->FlowVariance);
         logd ("- MaxBoxActivity : %.2f\n",t_group->MaxBoxActivity);
         logd ("- MaxFlowActivity: %.2f\n",t_group->MaxFlowActivity);
         logd ("- MaxFlowVariance: %.2f\n",t_group->MaxFlowVariance);
         #endif
      }
      t_group = (TrackedGroup*)TrackedBlobs.Next ((TrackedBlob*)t_group);
   }
   #if defined(__DEBUG_GRT)
   if (_DebugView_GRT) {
      _DebugView_GRT->DrawImage (GroupMaskImage,_DVX_GRT,_DVY_GRT);
      TrackedGroup *t_group = (TrackedGroup*)TrackedBlobs.First (   );
      while (t_group != NULL) {
         if (!(t_group->Status & TB_STATUS_TO_BE_REMOVED)) {
            COLORREF pen_color = PC_GREEN;
            int pen_width = 1;
            if (t_group->Status & TB_STATUS_STATIC) {
               pen_width = 2;
               pen_color = PC_YELLOW;
            }
            _DebugView_GRT->DrawRectangle (_DVX_GRT + t_group->X,_DVY_GRT + t_group->Y,t_group->Width,t_group->Height,pen_color,pen_width);
            int x = _DVX_GRT + t_group->X + t_group->Width + 5;
            int y = _DVY_GRT + t_group->Y;            
            CString text;
            text.Format (_T("ID: %d"),t_group->ID);
            _DebugView_GRT->DrawText (text,x,y,PC_YELLOW,TRANSPARENT,FT_ARIAL,14);
            y += 12;
            text.Format (_T("NP: %d"),t_group->NumPersons);
            _DebugView_GRT->DrawText (text,x,y,PC_YELLOW,TRANSPARENT,FT_ARIAL,14);
            y += 12;
            text.Format (_T("BA: %.2f"),t_group->BoxActivity);
            _DebugView_GRT->DrawText (text,x,y,PC_YELLOW,TRANSPARENT,FT_ARIAL,14);
            y += 12;
            text.Format (_T("FA: %.2f"),t_group->FlowActivity);
            _DebugView_GRT->DrawText (text,x,y,PC_YELLOW,TRANSPARENT,FT_ARIAL,14);
            y += 12;
            text.Format (_T("FV: %.2f"),t_group->FlowVariance);
            _DebugView_GRT->DrawText (text,x,y,PC_YELLOW,TRANSPARENT,FT_ARIAL,14);
            y += 12;
            text.Format (_T("MBA: %.2f"),t_group->MaxBoxActivity);
            _DebugView_GRT->DrawText (text,x,y,PC_YELLOW,TRANSPARENT,FT_ARIAL,14);
            y += 12;
            text.Format (_T("MFA: %.2f"),t_group->MaxFlowActivity);
            _DebugView_GRT->DrawText (text,x,y,PC_YELLOW,TRANSPARENT,FT_ARIAL,14);
            y += 12;
            text.Format (_T("MFV: %.2f"),t_group->MaxFlowVariance);
            _DebugView_GRT->DrawText (text,x,y,PC_YELLOW,TRANSPARENT,FT_ARIAL,14);
            y += 12;
         }
         t_group = (TrackedGroup*)TrackedBlobs.Next (t_group);
      }
      _DVY_GRT += GroupMaskImage.Height;
      _DebugView_GRT->DrawText (_T("Group Mask Image"),_DVX_GRT,_DVY_GRT,PC_GREEN);
      _DVY_GRT += 20;
      _DebugView_GRT->Invalidate (   );
   }
   #endif
   SetGroupMemberStatus (obj_tracker);
   return (DONE);
}

 void GroupTracking::PutMask (TrackedObject *t_object,GImage &d_image)
 
{
   int d1 = (int)(0.10f * t_object->Height); // 객체의 위/아래 줄이는 양
   int d2 = (int)(0.20f * t_object->Height); // 객체 간 수평 거리의 반
   int d3 = (int)(0.60f * t_object->Height); // 수평 바의 높이
   if (t_object->Width > t_object->Height) d1 = -(int)(0.2f * t_object->Height);
   IBox2D s_rgn;
   s_rgn.X      = t_object->X;
   s_rgn.Y      = t_object->Y + d1;
   s_rgn.Width  = t_object->Width;
   s_rgn.Height = t_object->Height - 2 * d1;
   CropRegion (SrcImgSize,s_rgn);
   d_image.Set (s_rgn,PG_WHITE);
   s_rgn.X      = t_object->X - d2;
   s_rgn.Y      = t_object->Y + (t_object->Height - d3) / 2;
   s_rgn.Width  = t_object->Width + 2 * d2;
   s_rgn.Height = d3;
   CropRegion (SrcImgSize,s_rgn);
   d_image.Set (s_rgn,PG_WHITE);
}

 void GroupTracking::Reset (   )
 
{
   BlobTracking::Reset  (   );
   GroupMaskImage.Clear (   );
}

 void GroupTracking::SetGroupMemberStatus (ObjectTracking& obj_tracker)

{
   int i;

   TrackedObject* t_object = obj_tracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      t_object->EnableFPTracking (FALSE);
      t_object->Status &= ~TO_STATUS_GROUP_MEMBER;
      t_object = obj_tracker.TrackedObjects.Next (t_object);
   }
   TrackedGroup* t_group = (TrackedGroup*)TrackedBlobs.First (   );
   while (t_group != NULL) {
      Array1D<TrackedObject*>& members = t_group->Members;
      if (t_group->Status & TB_STATUS_TO_BE_REMOVED) {
         for (i = 0; i < members.Length; i++) {
            members[i]->Status &= ~TO_STATUS_FEATURE_POINT_TRACKING;
         }
      }
      else if (t_group->NumPersons >= 2) {
         for (i = 0; i < members.Length; i++) {
            members[i]->Status |= TO_STATUS_GROUP_MEMBER;
            if (Options & GRT_OPTION_TRACK_FEATURE_POINTS_OF_GROUP_MEMBERS) members[i]->EnableFPTracking (TRUE);
         }
      }
      t_group = (TrackedGroup*)TrackedBlobs.Next (t_group);
   }
}      

 int GroupTracking::VerifyTrackedBlob (TrackedBlob *t_blob)
 
{
   return (DONE);
}

}

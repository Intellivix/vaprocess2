#if !defined(__VACL_DEFOG_H)
#define __VACL_DEFOG_H

#include "vacl_define.h"

 namespace VACL
 
{

////////////////////////////////////////////////////////////////////////////////
// 
// Class: VideoDefogging
//
////////////////////////////////////////////////////////////////////////////////

 class VideoDefogging
 
{
   protected:
      int    Flag_Initialized;
      int    FrameCount;
      byte   GCTable[256];
      GImage StrElement;
   
   // Input Parameters
   public:
      float   FrameRate; // Frame rate of an input video
      ISize2D FrameSize; // Frame size of an input video
   
   // Input Parameters
   public:
      int   BFSize;         // Size of the box filter applied to the rough transmission map
      int   NSPNSize;       // Non-sky pixel neightborhood size (RealSize = NSPNSize / 240 * ImageHeight)
      int   ALDThreshold;   // Atmospheric light didfference threshold
      int   EDThreshold;    // Edge detection threshold
      float ALEPeriod;      // Atmospheric light estimation period in second
      float ALEThreshold;   // Threshold for atmospheric light estimation
      float SkyRegionRange; // Range of the sky region in an input image. (RealRange = SkyRange * ImageHeight)

   // Input Parameters
   public:
      float GammaCorrectionParam; // Gamma correction parameter (0.1 < GammaCorrectionParam < 9.9)
      float MinTransmission;      // Minimum transmission value (0 < MinTransmission <= 1)

   // Output Parameters
   public:
      int      AtmosphericLight; // Estimated atmospheric light
      IArray2D TransmissionMap;  // Estimated transmission map (scaled)

   public:
      VideoDefogging (   );   
   
   protected:
      void CreateStrElement         (   );
      void EstimateAtmosphericLight (GImage &sr_image);
      void EstimateTransmissionMap  (BGRImage &s_cimage);
      void RecoverSceneRadiance     (BGRImage &s_cimage,BGRImage &d_cimage);

   public:
      void Close                      (   );
      void Initialize                 (int frm_width,int frm_height,float frm_rate);
      int  Perform                    (BGRImage& s_cimage,BGRImage& d_cimage);
      int  Perform_YUY2               (byte* si_buffer,byte* di_buffer);
      void UpdateGammaCorrectionTable (   );
};

}

#endif

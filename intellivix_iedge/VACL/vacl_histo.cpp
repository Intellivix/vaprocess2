#include "vacl_histo.h"
 
 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: Histogram1D
//
///////////////////////////////////////////////////////////////////////////////

 Histogram1D::Histogram1D (   )
 
{
   Sum = 0;
}

 void Histogram1D::Clear (   )
 
{
   UIArray1D::Clear (   );
   Sum = 0;
}

 float Histogram1D::GetSimilarity (Histogram1D &s_hist)

{
   int i;
   
   if (!Sum || !s_hist.Sum) return (0.0f);
   double similarity = 0.0;
   for (i = 0; i < Length; i++)
      similarity += sqrt ((double)Array[i] * s_hist[i]);
   similarity /= sqrt ((double)Sum * s_hist.Sum);
   return ((float)similarity);
}

 void Histogram1D::Normalize (FArray1D &d_hist)
 
{
   int i;
   
   for (i = 0; i < Length; i++)
      d_hist[i] = (float)((double)Array[i] / Sum);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: Histogram2D
//
///////////////////////////////////////////////////////////////////////////////

 Histogram2D::Histogram2D (   )
 
{
   Sum = 0;
}

 void Histogram2D::Clear (   )
 
{
   UIArray2D::Clear (   );
   Sum = 0;
}

 float Histogram2D::GetSimilarity (Histogram2D &s_hist)

{
   int i;
   
   if (!Sum || !s_hist.Sum) return (0.0f);
   int length = Width * Height;
   double similarity = 0.0;
   uint *s_array = Array[0];
   for (i = 0; i < length; i++)
      similarity += sqrt ((double)s_array[i] * s_hist(i));
   similarity /= sqrt ((double)Sum * s_hist.Sum);
   return ((float)similarity);
}

 void Histogram2D::Normalize (FArray2D &d_hist)
 
{
   int i;
   
   int length = Width * Height;
   uint *s_array = Array[0];
   for (i = 0; i < length; i++)
      d_hist(i) = (float)((double)s_array[i] / Sum);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: Histogram3D
//
///////////////////////////////////////////////////////////////////////////////

 Histogram3D::Histogram3D (   )
 
{
   Sum = 0;
}

 void Histogram3D::Clear (   )
 
{
   UIArray3D::Clear (   );
   Sum = 0;
}

 float Histogram3D::GetSimilarity (Histogram3D &s_hist)

{
   int i;
   
   if (!Sum || !s_hist.Sum) return (0.0f);
   int length = Width * Height * Depth;
   double similarity = 0.0;
   uint *s_array = Array[0][0];
   for (i = 0; i < length; i++)
      similarity += sqrt ((double)s_array[i] * s_hist(i));
   similarity /= sqrt ((double)Sum * s_hist.Sum);
   return ((float)similarity);
}

 void Histogram3D::Normalize (FArray3D &d_hist)
 
{
   int i;
   
   int length = Width * Height * Depth;
   uint *s_array = Array[0][0];
   for (i = 0; i < length; i++)
      d_hist(i) = (float)((double)s_array[i] / Sum);
}

///////////////////////////////////////////////////////////////////////////////
//
// HVHistogram
//
///////////////////////////////////////////////////////////////////////////////

#define HVH_RED_START      330
#define HVH_RED_END        10
#define HVH_ORANGE_START   10
#define HVH_ORANGE_END     40
#define HVH_YELLOW_START   40
#define HVH_YELLOW_END     70
#define HVH_GREEN_START    70
#define HVH_GREEN_END      170
#define HVH_CYAN_START     170
#define HVH_CYAN_END       210
#define HVH_BLUE_START     210
#define HVH_BLUE_END       258
#define HVH_VIOLET_START   258
#define HVH_VIOLET_END     330
#define HVH_BLACK_START    0
#define HVH_BLACK_END      60
#define HVH_GRAY_START     60
#define HVH_GRAY_END       140
#define HVH_WHITE_START    140
#define HVH_WHITE_END      256

#define HVH_RGB_RED        { 255,   0,  21 }
#define HVH_RGB_ORANGE     { 255, 106,   0 }
#define HVH_RGB_YELLOW     { 255, 233,   0 }
#define HVH_RGB_GREEN      {   0, 255,   0 }
#define HVH_RGB_CYAN       {   0, 212, 255 }
#define HVH_RGB_BLUE       {   0,  12, 255 }
#define HVH_RGB_VIOLET     { 255,   0, 246 }
#define HVH_RGB_BLACK      {  30,  30,  30 }
#define HVH_RGB_GRAY       { 100, 100, 100 }
#define HVH_RGB_WHITE      { 198, 198, 198 }

 void HVHistogram::Accumulate (BGRPixel &cp)

{
   int ch,cs,cv;
   ConvertBGRToHSV (cp,ch,cs,cv);
   int score = 10;
   if (IsGrayPixel (ch,cs,cv)) { // 그레이 픽셀로 분류된 경우
      if (cv >= 180) score =20;  // 흰 색 계통이면 가중치를 준다.
      ValueHistogram.Accumulate ((int)cv,score);
   }
   else { // 컬러 픽셀로 분류된 경우
      HueHistogram.Accumulate ((int)ch,score);
   }
}

 int HVHistogram::IsGrayPixel (int ch,int cs,int cv)
 
{
   if (cs <= 25 || cv <= 40) return (TRUE);
   int csv = (int)(0.153787f * cs * cv);
   if (csv <= 650) return (TRUE);
   if ((HVH_BLUE_START <= ch && ch < HVH_BLUE_END) || (HVH_CYAN_START <= ch && ch < HVH_CYAN_END)) {
      if (cs <= 30 || csv <= 800) return (TRUE);
   }
   return (FALSE);
}

 void HVHistogram::Clear (   )
 
{
   HueHistogram.Clear   (   );
   ValueHistogram.Clear (   );
}

 void HVHistogram::Close (   )
 
{
   HueHistogram.Delete   (   );
   ValueHistogram.Delete (   );
}

 void HVHistogram::Finalize (float *d_hist)

{
   int i,j;
   int si1,ei1,si2,ei2;
   static int h_ranges[HVH_NUM_COLORS][2] = {
      {HVH_RED_START   , HVH_RED_END   },
      {HVH_ORANGE_START, HVH_ORANGE_END},
      {HVH_YELLOW_START, HVH_YELLOW_END},
      {HVH_GREEN_START , HVH_GREEN_END },
      {HVH_CYAN_START  , HVH_CYAN_END  },
      {HVH_BLUE_START  , HVH_BLUE_END  },
      {HVH_VIOLET_START, HVH_VIOLET_END}
   };
   
   if (!HueHistogram || !ValueHistogram) {
      memset (d_hist,0,HVH_NUM_BINS * sizeof(float));
      return;
   }
   double sum = (double)(HueHistogram.Sum + ValueHistogram.Sum);
   if (!sum) {
      memset (d_hist,0,HVH_NUM_BINS * sizeof(float));
      return;
   }
   UIArray1D s_hist(HVH_NUM_BINS);
   s_hist.Clear (   );
   for (i = 0; i < HVH_NUM_COLORS; i++) {
      int sj = h_ranges[i][0];
      int ej = h_ranges[i][1];
      if (i == 0) {
         for (j = 0 ; j < ej ; j++) s_hist[i] += HueHistogram[j];
         for (j = sj; j < 360; j++) s_hist[i] += HueHistogram[j];
      }
      else {
         for (j = sj; j < ej ; j++) s_hist[i] += HueHistogram[j];
      }
   }
   int   d  = 10;
   float d2 = (float)(d * 2);
   // Black
   si1 = HVH_BLACK_START;
   ei1 = HVH_BLACK_END - d;
   ei2 = HVH_BLACK_END + d;
   for (i = si1; i < ei1; i++) s_hist[HVH_INDEX_BLACK] += ValueHistogram[i];
   for (i = ei1; i < ei2; i++) s_hist[HVH_INDEX_BLACK] += (uint)((1.0f - (i - ei1) / d2) * ValueHistogram[i]);
   // Gray
   si1 = HVH_GRAY_START - d;
   si2 = HVH_GRAY_START + d;
   ei1 = HVH_GRAY_END   - d;
   ei2 = HVH_GRAY_END   + d;
   for (i = si1; i < si2; i++) s_hist[HVH_INDEX_GRAY] += (uint)((i - si1) / d2 * ValueHistogram[i]);
   for (i = si2; i < ei1; i++) s_hist[HVH_INDEX_GRAY] += ValueHistogram[i];
   for (i = ei1; i < ei2; i++) s_hist[HVH_INDEX_GRAY] += (uint)((1.0f - (i - ei1) / d2) * ValueHistogram[i]);
   // White
   si1 = HVH_WHITE_START - d;
   si2 = HVH_WHITE_START + d;
   ei1 = HVH_WHITE_END;
   for (i = si1; i < si2; i++) s_hist[HVH_INDEX_WHITE] += (uint)((i - si1) / d2 * ValueHistogram[i]);
   for (i = si2; i < ei1; i++) s_hist[HVH_INDEX_WHITE] += ValueHistogram[i];
   for (i = 0  ; i < s_hist.Length; i++) d_hist[i] = (float)(s_hist[i] / sum);
}

 int HVHistogram::GetBinIndexNo (int ch,int cs,int cv)
 
{
   if (IsGrayPixel (ch,cs,cv)) {
      if      (cv < HVH_BLACK_END) return (HVH_INDEX_BLACK);
      else if (cv < HVH_GRAY_END ) return (HVH_INDEX_GRAY );
      else                         return (HVH_INDEX_WHITE);
   }
   else {
      if      (ch < HVH_RED_END   ) return (HVH_INDEX_RED   );
      else if (ch < HVH_ORANGE_END) return (HVH_INDEX_ORANGE);
      else if (ch < HVH_YELLOW_END) return (HVH_INDEX_YELLOW);
      else if (ch < HVH_GREEN_END ) return (HVH_INDEX_GREEN );
      else if (ch < HVH_CYAN_END  ) return (HVH_INDEX_CYAN  );
      else if (ch < HVH_BLUE_END  ) return (HVH_INDEX_BLUE  );
      else if (ch < HVH_VIOLET_END) return (HVH_INDEX_VIOLET);
      else                          return (HVH_INDEX_RED   );
   }
}

 int HVHistogram::GetBinIndexNo (BGRPixel &s_color)
 
{
   int ch,cs,cv;
   ConvertBGRToHSV (s_color,ch,cs,cv);
   return (GetBinIndexNo (ch,cs,cv));
}

 int HVHistogram::GetRepresentativeColor (int bin_idx,BGRPixel &d_color)
 
{
   static byte colors[HVH_NUM_BINS][3] = {
      HVH_RGB_RED   , HVH_RGB_ORANGE,
      HVH_RGB_YELLOW, HVH_RGB_GREEN ,
      HVH_RGB_CYAN  , HVH_RGB_BLUE  ,
      HVH_RGB_VIOLET, HVH_RGB_BLACK ,
      HVH_RGB_GRAY  , HVH_RGB_WHITE
   };
   
   d_color.Clear (   );
   if (bin_idx < 0 || bin_idx >= HVH_NUM_BINS) return (1);
   d_color(colors[bin_idx][2],colors[bin_idx][1],colors[bin_idx][0]);
   return (DONE);
}

 int HVHistogram::GetRepresentativeColor (BGRPixel &s_color,BGRPixel &d_color)
 
{
   return (GetRepresentativeColor (GetBinIndexNo (s_color),d_color));
}

 void HVHistogram::Initialize (   )
 
{
   HueHistogram.Create   (360);
   ValueHistogram.Create (256);
   Clear (   );
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 void GetColorHistogram (BGRImage &s_image,IBox2D &s_rgn,FArray1D &d_hist)
 
{
   int x,y;

   int hw = s_rgn.Width  / 2;
   int hh = s_rgn.Height / 2;
   int cx = s_rgn.X + hw;
   int cy = s_rgn.Y + hh;
   int sx = s_rgn.X;
   int sy = s_rgn.Y;
   int ex = s_rgn.X + s_rgn.Width;
   int ey = s_rgn.Y + s_rgn.Height;
   if (sx < 0) sx = 0;
   if (ex > s_image.Width)  ex = s_image.Width;
   if (sy < 0) sy = 0;
   if (ey > s_image.Height) ey = s_image.Height;
   d_hist.Clear (   );
   for (y = sy; y < ey; y++) {
      float dy  = (float)(y - cy) / hh;
      float sdy = dy * dy;
      BGRPixel *sp = s_image[y] + sx;
      for (x = sx; x < ex; x++) {
         float dx = (float)(x - cx) / hw;
         float sd = dx * dx + sdy;
         if (sd <= 1.0f) {
            int idx = GET_COLOR_HISTOGRAM_BIN_INDEX_NO(sp->R,sp->G,sp->B);
            d_hist[idx] += (1.0f - sd);
         }
         sp++;
      }
   }
   NormalizeHistogram (d_hist);
}

 float GetColorSimilarity (BGRImage &s_cimage,IBox2D &s_rgn,FArray1D &t_hist)
 
{
   FArray1D s_hist(t_hist.Length);
   GetColorHistogram (s_cimage,s_rgn,s_hist);
   return (GetHistogramSimilarity (s_hist,t_hist));
}

 void GetHistogram (GImage &s_image,IArray1D &d_array,int flag_clear)

{
   int x,y;

   if (flag_clear) d_array.Clear (   );
   for (y = 0; y < s_image.Height; y++) {
      byte* l_s_image = s_image[y];
      for (x = 0; x < s_image.Width; x++) {
         d_array[(int)l_s_image[x]]++;
      }
   }
}

 float GetHistogramSimilarity (float *s_hist1,float *s_hist2,int length)
 
{
   int i;

   float similarity = 0.0f;
   for (i = 0; i < length; i++) similarity += sqrt (s_hist1[i] * s_hist2[i]);
   return (similarity);
}

 float GetHistogramSimilarity (FArray1D &s_hist1,FArray1D &s_hist2)

{
   return (GetHistogramSimilarity (s_hist1,s_hist2,s_hist1.Length));
}

 float GetHistogramSimilarity (FArray2D &s_hist1,FArray2D &s_hist2)

{
   int i;

   float similarity = 0.0f;
   int length = s_hist1.Width * s_hist1.Height;
   for (i = 0; i < length; i++) similarity += sqrt (s_hist1(i) * s_hist2(i));
   return (similarity);
}

 float GetHistogramSimilarity (FArray3D &s_hist1,FArray3D &s_hist2)

{
   int i;

   float similarity = 0.0f;
   int length = s_hist1.Width * s_hist1.Height * s_hist1.Depth;
   for (i = 0; i < length; i++) similarity += sqrt (s_hist1(i) * s_hist2(i));
   return (similarity);
}

 void HistogramEqualization (GImage &s_image,GImage &d_image)

{
   int i,x,y;

   IArray1D cdf(256);
   GetHistogram (s_image,cdf,TRUE);
   for (i = 1; i < cdf.Length; i++) cdf[i] += cdf[i - 1];
   float a = (float)(cdf.Length - 1) / (s_image.Width * s_image.Height);
   for (i = 0; i < cdf.Length; i++) cdf[i] = (int)(cdf[i] * a);
   for (y = 0; y < s_image.Height; y++) {
      byte* l_s_image = s_image[y];
      byte* l_d_image = d_image[y];
      for (x = 0; x < s_image.Width; x++) {
         l_d_image[x] = (byte)cdf[(int)l_s_image[x]];
      }
   }
} 

 void NormalizeHistogram (FArray1D &d_hist)
 
{
   int i;
   
   float sum = 0.0f;
   for (i = 0; i < d_hist.Length; i++) sum += d_hist[i];
   if (sum) for (i = 0; i < d_hist.Length; i++) d_hist[i] /= sum;
}

}

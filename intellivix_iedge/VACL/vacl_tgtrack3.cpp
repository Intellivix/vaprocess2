#include "vacl_tgtrack.h"

#if defined(__DEBUG_TGT)
#include "Win32CL/Win32CL.h"
extern int _DVX_TGT,_DVY_TGT;
extern CImageView* _DebugView_TGT;
#endif

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: TargetTracking_DSST
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_DLIB)

 TargetTracking_DSST::TargetTracking_DSST (   )

{
   TargetTracker = NULL;
}

 void TargetTracking_DSST::Close (   )

{
   if (TargetTracker != NULL) {
      delete TargetTracker;
      TargetTracker = NULL;
   }
   TargetTracking::Close (   );
}

 void TargetTracking_DSST::Initialize (GImage& s_image,IBox2D& tg_rgn,float frm_rate)

{
   ISize2D si_size(s_image.Width,s_image.Height);
   TargetTracking::Initialize (si_size,tg_rgn,frm_rate);
   dlib::array2d<byte> s_img((byte*)s_image,s_image.Width,s_image.Height);
   dlib::drectangle s_rect(tg_rgn.X,tg_rgn.Y,tg_rgn.X + tg_rgn.Width - 1,tg_rgn.Y + tg_rgn.Height - 1);
   TargetTracker = new dlib::correlation_tracker(6,6,23);
   TargetTracker->start_track (s_img,s_rect);
   s_img.import (0,0,0);
}

 float TargetTracking_DSST::Perform (GImage& s_image)

{
   IBox2D tr;

   #if defined(__DEBUG_TGT)
   if (_DebugView_TGT) {
      _DVX_TGT = _DVY_TGT = 0;
      _DebugView_TGT->Clear (   );
   }
   #endif
   FrameCount++;
   MatchingScore = 0.0f;
   if (!IsInitialized (   ) || !TargetTracker) {
      Status |= TT_STATUS_NOT_INITIALIZED;
      return (0.0f);
   }
   if (Flag_SetTargetSize) {
      Initialize (s_image,TargetRegion,FrameRate);
      return (0.0f);
   }
   Flag_SetTargetSize = FALSE;
   if (CheckTargetOutOfRange (   )) {
      #if defined(__DEBUG_TGT)
      logd ("Target out of range.\n");
      #endif
      return (0.0f);
   }
   dlib::array2d<byte> s_img((byte*)s_image,s_image.Width,s_image.Height);
   MatchingScore = TargetTracker->update (s_img);
   s_img.import (0,0,0);
   UpdateTargetRegion (   );
   return (MatchingScore);
}

 void TargetTracking_DSST::UpdateTargetRegion (   )

{
   if (TargetTracker == NULL) return;
   dlib::drectangle t_rect = TargetTracker->get_position (   );
   MatchingPos.X = (int)(t_rect.left (   ) + 0.5);
   MatchingPos.Y = (int)(t_rect.top  (   ) + 0.5);
   FPoint2D r_pos,t_pos;
   r_pos.X = (float)(TargetRegion.X + TargetRegion.Width  / 2);
   r_pos.Y = (float)(TargetRegion.Y + TargetRegion.Height / 2);
   t_pos.X = (float)(t_rect.left (   ) + 0.5 * t_rect.width  (   ));
   t_pos.Y = (float)(t_rect.top  (   ) + 0.5 * t_rect.height (   ));
   if (VelocityUpdateRate) {
      if (TargetVelocity.X == MC_VLV) TargetVelocity = t_pos - r_pos;
      else TargetVelocity = (1.0f - VelocityUpdateRate) * TargetVelocity + VelocityUpdateRate * (t_pos - r_pos);
   }
   else TargetVelocity(0.0f,0.0f);
   TargetRegion.X      = MatchingPos.X;
   TargetRegion.Y      = MatchingPos.Y;
   TargetRegion.Width  = (int)(t_rect.width  (   ) + 0.5);
   TargetRegion.Height = (int)(t_rect.height (   ) + 0.5);
}

#endif

}

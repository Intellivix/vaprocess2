#include "vacl_feature.h"
#include "vacl_filter.h"
#include "vacl_foregnd3.h"
#include "vacl_morph.h"
#include "vacl_nms.h"

#if defined(__DEBUG_FGD)
#include "Win32CL/Win32CL.h"
extern int _DVX_FGD,_DVY_FGD;
extern CImageView* _DebugView_FGD;
#endif

#if defined(__DEBUG_MBS)
extern int _DVX_MBS,_DVY_MBS;
extern CImageView* _DebugView_MBS;
#endif

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: ForegroundDetection_HMD_OHV_MBS
//
///////////////////////////////////////////////////////////////////////////////

 ForegroundDetection_HMD_OHV_MBS::ForegroundDetection_HMD_OHV_MBS (   )

{
   ClassID     = CLSID_ForegroundDetection_HMD_OHV_MBS;

   EDThreshold = 100;
   HeadRadius  = 18.0f;
   RefImgSize(352,240);
}

 float ForegroundDetection_HMD_OHV_MBS::GetBodySize (   )

{
   return (3.75f * GetHeadRadius (   )); // PARAMETERS
}

 int ForegroundDetection_HMD_OHV_MBS::GetFrgImage (   )

{
   int i;

   int r_code = ForegroundDetection_SGBM::GetFrgImage (   );
   if (r_code != FGD_RCODE_NORMAL) return (r_code);
   if (DetAreaMap.Width == FrgImage.Width && DetAreaMap.Height == DetAreaMap.Height) And (FrgImage,DetAreaMap,FrgImage);
   FrgRegionMap.Clear (   );
   IArray2D fr_map(FrgImage.Width,FrgImage.Height);
   int n_frs = LabelCRs (FrgImage,fr_map);
   if (!n_frs) {
      FrgRegions.Create (1);
      FrgRegions.Clear  (   );
      return (FGD_RCODE_NORMAL);
   }

   #if defined(__DEBUG_MBS)
   _DebugView_MBS->Clear (   );
   _DVX_MBS = _DVY_MBS = 0;
   #endif

   #if defined(__DEBUG_MBS)
   _DebugView_MBS->DrawImage (FrgImage,_DVX_MBS,_DVY_MBS);
   _DVY_MBS += SrcImgSize.Height;
   _DebugView_MBS->DrawText ("Foreground Detection Result",_DVX_MBS,_DVY_MBS,PC_GREEN);
   _DVY_MBS += 20;
   #endif

   IP2DArray1D fp_array;
   DetectHeadFeaturePoints (SrcImage,FrgImage,fp_array);

   #if defined(__DEBUG_MBS)
   _DebugView_MBS->DrawImage (SrcImage,_DVX_MBS,_DVY_MBS);
   for (i = 0; i < fp_array.Length; i++)
      _DebugView_MBS->DrawPoint (_DVX_MBS + fp_array[i].X,_DVY_MBS + fp_array[i].Y,PC_GREEN,PT_CROSS);
   _DVY_MBS += SrcImgSize.Height;
   _DebugView_MBS->DrawText ("Feature Point Detection Result",_DVX_MBS,_DVY_MBS,PC_GREEN);
   _DVY_MBS += 20;
   #endif

   IB2DArray1D hr_array;
   GetHumanCandidateRegions (fr_map,fp_array,hr_array);

   GImage hrl_map(SrcImgSize.Width,SrcImgSize.Height);
   GetHumanRegionLikelihoodMap (IntFrgImage2,FrgEdgeImage3,hrl_map);

   #if defined(__DEBUG_MBS)
   _DebugView_MBS->DrawImage (hrl_map,_DVX_MBS,_DVY_MBS);
   for (i = 0; i < hr_array.Length; i++) {
      IBox2D& rgn = hr_array[i];
      _DebugView_MBS->DrawRectangle (_DVX_MBS + rgn.X,_DVY_MBS + rgn.Y,rgn.Width,rgn.Height,PC_GREEN);
   }
   _DVY_MBS += SrcImgSize.Height;
   _DebugView_MBS->DrawText ("Initial Regions",_DVX_MBS,_DVY_MBS,PC_GREEN);
   _DVY_MBS += 20;
   #endif

   FArray2D it_array(hrl_map.Width + 1,hrl_map.Height + 1);
   FArray2D ix_array(it_array.Width,it_array.Height);
   FArray2D iy_array(it_array.Width,it_array.Height);
   GetIntegrals (hrl_map,it_array,ix_array,iy_array);

   for (i = 0; i < hr_array.Length; i++)
      PerformMeanShift (it_array,ix_array,iy_array,hr_array[i]);

   #if defined(__DEBUG_MBS)
   _DebugView_MBS->DrawImage (SrcImage,_DVX_MBS,_DVY_MBS);
   for (i = 0; i < hr_array.Length; i++) {
      IBox2D& rgn = hr_array[i];
      _DebugView_MBS->DrawRectangle (_DVX_MBS + rgn.X,_DVY_MBS + rgn.Y,rgn.Width,rgn.Height,PC_GREEN);
   }
   _DVY_MBS += SrcImgSize.Height;
   _DebugView_MBS->DrawText ("Mean-Shift Result",_DVX_MBS,_DVY_MBS,PC_GREEN);
   _DVY_MBS += 20;
   #endif

   PerformNonmaximumSuppression (hr_array,it_array,hr_array);

   #if defined(__DEBUG_MBS)
   _DebugView_MBS->DrawImage (SrcImage,_DVX_MBS,_DVY_MBS);
   for (i = 0; i < hr_array.Length; i++) {
      IBox2D& rgn = hr_array[i];
      _DebugView_MBS->DrawRectangle (_DVX_MBS + rgn.X,_DVY_MBS + rgn.Y,rgn.Width,rgn.Height,PC_GREEN,2);
   }
   _DVY_MBS += SrcImgSize.Height;
   _DebugView_MBS->DrawText ("Non-maximum Suppression Result",_DVX_MBS,_DVY_MBS,PC_GREEN);
   _DVY_MBS += 20;
   #endif

   SetFrgRegions (FrgImage,fr_map,hr_array);

   #if defined(__DEBUG_MBS)
   _DebugView_MBS->DrawImage (FrgRegionMap,_DVX_MBS,_DVY_MBS);
   _DVY_MBS += FrgRegionMap.Height;
   _DebugView_MBS->DrawText ("Foreground Region Map",_DVX_MBS,_DVY_MBS,PC_GREEN);
   _DVY_MBS += 20;
   #endif

   #if defined(__DEBUG_MBS)
   _DebugView_MBS->Invalidate (   );
   #endif

   return (FGD_RCODE_NORMAL);
}

 float ForegroundDetection_HMD_OHV_MBS::GetHeadRadius (   )

{
   return (HeadRadius * SrcImgSize.Height / RefImgSize.Height);
}

 void ForegroundDetection_HMD_OHV_MBS::GetHeadRadius (float& hd_radius,ISize2D& ri_size)

{
   hd_radius = HeadRadius;
   ri_size   = RefImgSize;
}

 int ForegroundDetection_HMD_OHV_MBS::GetHumanCandidateRegions (IArray2D& fr_map,IP2DArray1D& fp_array,IB2DArray1D& d_array)

{
   int i;
   
   #if defined(__DEBUG_MBS)
   _DebugView_MBS->DrawImage (SrcImage,_DVX_MBS,_DVY_MBS);
   #endif
   int bd_size   = (int)GetBodySize (   );
   int h_bd_size = bd_size / 2;
   bd_size       = 2 * h_bd_size + 1;
   d_array.Delete (   );
   IB2DArray1D hr_array(fp_array.Length);
   int n = 0;
   for (i = 0; i < fp_array.Length; i++) {
      IPoint2D& fp  = fp_array[i];
      int fr_no     = fr_map[fp.Y][fp.X];
      IBox2D hc_rgn;
      hc_rgn.X      = fp.X - h_bd_size;
      hc_rgn.Y      = fp.Y;
      hc_rgn.Width  = bd_size;
      hc_rgn.Height = bd_size;
      float frd     = GetCRDensity (fr_map,hc_rgn,fr_no);
      if (frd >= 0.35f) { // PARAMETERS
         #if defined(__DEBUG_MBS)
         _DebugView_MBS->DrawPoint (_DVX_MBS + fp.X,_DVY_MBS + fp.Y,PC_GREEN,PT_CROSS);
         #endif
         CropRegion (SrcImgSize,hc_rgn);
         hr_array[n++] = hc_rgn;
      }
   }
   if (n) d_array = hr_array.Extract (0,n);

   #if defined(__DEBUG_MBS)
   _DVY_MBS += SrcImgSize.Height;
   _DebugView_MBS->DrawText ("Feature Point Filtering Result",_DVX_MBS,_DVY_MBS,PC_GREEN);
   _DVY_MBS += 20;
   #endif

   return (DONE);
}

 void ForegroundDetection_HMD_OHV_MBS::GetHumanRegionLikelihoodMap (GImage& fg_image,GImage& fe_image,GImage& d_image)

{
   int x,y;
   
   d_image.Clear (   );
   for (y = 0; y < d_image.Height; y++) {
      byte* l_fg_image = fg_image[y];
      byte* l_d_image  = d_image[y];
      for (x = 0; x < d_image.Width; x++) {
         if (l_fg_image[x]) l_d_image[x] = 100;
      }
   }
}

 void ForegroundDetection_HMD_OHV_MBS::GetIntegrals (GImage& s_image,FArray2D& d_array,FArray2D& dx_array,FArray2D& dy_array)

{
   int x,y;
   
   GetIntegral (s_image,d_array);
   IArray2D tx_array(s_image.Width,s_image.Height);
   IArray2D ty_array(s_image.Width,s_image.Height);
   for (y = 0; y < s_image.Height; y++) {
      byte* l_s_image  = s_image[y];
      int*  l_tx_array = tx_array[y];
      int*  l_ty_array = ty_array[y];
      for (x = 0; x < s_image.Width; x++) {
         l_tx_array[x] = x * l_s_image[x];
         l_ty_array[x] = y * l_s_image[x];
      }
   }
   GetIntegral (tx_array,dx_array);
   GetIntegral (ty_array,dy_array);
}

 void ForegroundDetection_HMD_OHV_MBS::GetRegionScores (IB2DArray1D& hr_array,FArray2D& it_array,FArray1D& d_array)

{
   int i;
   
   for (i = 0; i < hr_array.Length; i++) {
      IBox2D& s_rgn = hr_array[i];
      int sx  = s_rgn.X;
      int sy  = s_rgn.Y;
      int ex  = sx + s_rgn.Width;
      int ey  = sy + s_rgn.Height;
      float sum = (it_array[ey][ex] + it_array[sy][sx]) - (it_array[ey][sx] + it_array[sy][ex]);
      d_array[i] = (float)sum / (s_rgn.Width * s_rgn.Height);
   }   
}

 void ForegroundDetection_HMD_OHV_MBS::PerformMeanShift (FArray2D& it_array,FArray2D& ix_array,FArray2D& iy_array,IBox2D& rgn)

{
   int i;
   
   int mw = it_array.Width  - 1;
   int mh = it_array.Height - 1;
   int sx = rgn.X;
   int sy = rgn.Y;
   int ex = sx + rgn.Width;
   int ey = sy + rgn.Height;
   CropRegion (mw,mh,sx,sy,ex,ey);
   int hw = (ex - sx) / 2;
   int hh = (ey - sy) / 2;
   int cx = sx + hw;
   int cy = sy + hh;
   for (i = 0; i < 10; i++) {
      float a   = (it_array[ey][ex] + it_array[sy][sx]) - (it_array[ey][sx] + it_array[sy][ex]);
      if (!a) break;
      float bx  = (ix_array[ey][ex] + ix_array[sy][sx]) - (ix_array[ey][sx] + ix_array[sy][ex]);
      float by  = (iy_array[ey][ex] + iy_array[sy][sx]) - (iy_array[ey][sx] + iy_array[sy][ex]);
      int ncx = (int)(bx / a);
      int ncy = (int)(by / a);
      if (ncx == cx && ncy == cy) break;
      cx = ncx, cy = ncy;
      sx = cx - hw;
      sy = cy - hh;
      ex = sx + rgn.Width;
      ey = sy + rgn.Height;
      CropRegion (mw,mh,sx,sy,ex,ey);
   }
   rgn(sx,sy,ex - sx,ey - sy);
}

 void ForegroundDetection_HMD_OHV_MBS::PerformNonmaximumSuppression (IB2DArray1D& s_array,FArray2D& it_array,IB2DArray1D& d_array)

{
   int i;
   
   if (!s_array) {
      d_array.Delete (   );
      return;
   }
   FArray1D sc_array1(s_array.Length);
   GetRegionScores (s_array,it_array,sc_array1);
   ISize2D si_size(it_array.Width - 1,it_array.Height - 1);
   IB2DArray1D t_array; FArray1D sc_array2;
   PerformRectNMS_Island (si_size,s_array,sc_array1,0.0f,3,1.0f,0.0f,t_array,sc_array2); // PARAMETERS
   if (!t_array) {
      d_array.Delete (   );
      return;
   }
   PerformRectNMS_Greedy (t_array,sc_array2,0.35f,0.0f,d_array); // PARAMETERS
   for (i = 0; i < d_array.Length; i++) CropRegion (SrcImgSize,d_array[i]);
}

 int ForegroundDetection_HMD_OHV_MBS::ReadFileEx (FileIO& file)

{
   if (file.Read ((byte*)&HeadRadius,1,sizeof(HeadRadius))) return (1);
   if (file.Read ((byte*)&RefImgSize,1,sizeof(RefImgSize))) return (2);
   return (DONE);
}

 int ForegroundDetection_HMD_OHV_MBS::ReadFileEx (CXMLIO* pIO)
 
{
   CXMLNode xmlNode (pIO);
   int flag_found = FALSE;
   if      (xmlNode.Start ("ForegroundDetection_HMD_OHV_MBS")) flag_found = TRUE;
   else if (xmlNode.Start ("ForegroundDetection_MBS_OHV"))     flag_found = TRUE; // 이전 config와의 호환성 유지 목적
   else if (xmlNode.Start ("ForegroundDetection_HCT_MBS"))     flag_found = TRUE; // 이전 config와의 호환성 유지 목적
   if (flag_found) {
      xmlNode.Attribute (TYPE_ID(float),"HeadRadius"  ,&HeadRadius);
      xmlNode.Attribute (TYPE_ID(int)  ,"RefImgWidth" ,&RefImgSize.Width);
      xmlNode.Attribute (TYPE_ID(int)  ,"RefImgHeight",&RefImgSize.Height);
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}

 void ForegroundDetection_HMD_OHV_MBS::SetFrgRegions (GImage& fg_image,IArray2D& fr_map,IB2DArray1D& hr_array)

{
   int i,x,y;

   // 객체 영역의 Y 좌표 값을 이용하여 내림차순으로 객체 영역들을 정렬한다.
   // [참고] 객체 영역의 Y 좌표 값이 클수록 카메라로부터 가까이 있는 객체가 된다.
   OSArray1D os_array(hr_array.Length);
   for (i = 0; i < hr_array.Length; i++) {
      os_array[i].IndexNo = i;
      os_array[i].Score   = (double)hr_array[i].Y;
   }
   qsort (os_array,os_array.Length,sizeof(ObjectScore),Compare_ObjectScore_Descending);

   // 카메라로부터 가까이 있는 객체부터 순차적으로 객체 영역을 FrgRegionMap에 복사한다.
   int cr_no = 1;
   for (i = 0; i < os_array.Length; i++) {
      IBox2D& rgn = hr_array[os_array[i].IndexNo];
      int fr_no = GetCRID (fr_map,rgn);
      if (fr_no) {
         int ex = rgn.X + rgn.Width;
         int ey = rgn.Y + rgn.Height;
         int n  = 0;
         for (y = rgn.Y; y < ey; y++) {
            int* l_s_fr_map = fr_map[y];
            int* l_d_fr_map = FrgRegionMap[y];
            for (x = rgn.X; x < ex; x++) {
               if (l_s_fr_map[x] == fr_no && !l_d_fr_map[x]) {
                  l_d_fr_map[x] = cr_no;
                  n++;
               }
            }
         }
         if (n) cr_no++;
      }
   }

   // 전경 영상에서 검출된 사람 영역을 제외한 나머지 영역들을 처리한다.
   GImage rr_image = fg_image;
   for (i = 0; i < hr_array.Length; i++)
      rr_image.Set (hr_array[i],PG_BLACK);
   rr_image.SetBoundary (1,PG_BLACK);

   #if defined(__DEBUG_MBS)
   _DebugView_MBS->DrawImage (rr_image,_DVX_MBS,_DVY_MBS);
   _DVY_MBS += rr_image.Height;
   _DebugView_MBS->DrawText ("Residual Foreground Region Image",_DVX_MBS,_DVY_MBS,PC_GREEN);
   _DVY_MBS += 20;
   #endif

   int bd_size  = (int)GetBodySize (   );
   int min_area = (int)(0.25f * bd_size * bd_size); // PARAMETERS
   IArray2D rr_map(rr_image.Width,rr_image.Height);
   int n_rrs = LabelCRs (rr_image,rr_map);
   if (n_rrs) {
      CRArray1D rr_array(n_rrs + 1);
      GetCRInfo (rr_map,rr_array);
      // 각 RR에 대해 가장 많이 접하는 FR을 찾아 RR을 그 FR에 귀속시킨다.
      // 만약 접하는 FR이 없으면 RR을 새로운 FR로 등록시킨다.
      for (i = 1; i < rr_array.Length; i++) {
         IArray1D nb_array(cr_no);
         nb_array.Clear (   );
         ConnectedRegion& rr = rr_array[i];
         int sx = rr.X;
         int sy = rr.Y;
         int ex = sx + rr.Width;
         int ey = sy + rr.Height;
         for (y = sy; y < ey; y++) {
            int* l_rr_map  = rr_map[y];
            int* l_rr_map1 = rr_map[y - 1];
            int* l_rr_map2 = rr_map[y + 1];
            int* l_fr_map  = FrgRegionMap[y];
            int* l_fr_map1 = FrgRegionMap[y - 1];
            int* l_fr_map2 = FrgRegionMap[y + 1];
            for (x = sx; x < ex; x++) {
               if (l_rr_map[x] == i) {
                  if (l_rr_map[x - 1] != i ||  l_rr_map[x + 1] != i || l_rr_map1[x] != i || l_rr_map2[x] != i) {
                     nb_array[l_fr_map[x - 1]]++;
                     nb_array[l_fr_map[x + 1]]++;
                     nb_array[l_fr_map1[x]]++;
                     nb_array[l_fr_map2[x]]++;
                  }
               }
            }
         }
         nb_array[0] = 0;
         int max_v,max_i;
         FindMaximum (nb_array,max_v,max_i);
         if (max_v) CopyCR (rr_map,rr,i,FrgRegionMap,max_i);
         else if (rr.Area > min_area) CopyCR (rr_map,rr,i,FrgRegionMap,cr_no++);
      }
   }

   // 새로운 전경 영역 정보를 생성한다.
   FrgRegions.Create (cr_no);
   GetCRInfo (FrgRegionMap,FrgRegions);
   // 새로운 전경 영상을 생성한다.
   Binarize (FrgRegionMap,0,FrgImage);
}

 void ForegroundDetection_HMD_OHV_MBS::SetHeadRadius (float hd_radius,ISize2D& ri_size)

{
   HeadRadius = hd_radius;
   RefImgSize = ri_size;
}

 int ForegroundDetection_HMD_OHV_MBS::WriteFileEx (FileIO& file)

{
   if (file.Write ((byte*)&HeadRadius,1,sizeof(HeadRadius))) return (1);
   if (file.Write ((byte*)&RefImgSize,1,sizeof(RefImgSize))) return (2);
   return (DONE);
}

 int ForegroundDetection_HMD_OHV_MBS::WriteFileEx (CXMLIO* pIO)
 
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("ForegroundDetection_HMD_OHV_MBS"))
   {
      xmlNode.Attribute (TYPE_ID(float),"HeadRadius"  ,&HeadRadius);
      xmlNode.Attribute (TYPE_ID(int)  ,"RefImgWidth" ,&RefImgSize.Width);
      xmlNode.Attribute (TYPE_ID(int)  ,"RefImgHeight",&RefImgSize.Height);
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: ForegroundDetection_HMD_OHV_3DM
//
///////////////////////////////////////////////////////////////////////////////

#define DIFLX_HDPRO   237.2727272727f // 320x240  Depth   Image의 X축 Focal Length (fd_x) 
#define DIFLY_HDPRO   190.1886792453f // 320x240  Depth   Image의 Y축 Focal Length (fd_y)
#define OIFLX_HDPRO   657.2727272727f // 1280x720 Optical Image의 X축 Focal Length (fo_x)
#define OIFLY_HDPRO   620.3773584906f // 1280x720 Optical Image의 Y축 Focal Length (fo_y)

 ForegroundDetection_HMD_OHV_3DM::ForegroundDetection_HMD_OHV_3DM (   )

{
   ClassID       = CLSID_ForegroundDetection_HMD_OHV_3DM;

   Margin        = 15;
   GFSize        = 10.0f;
   FXD           = DIFLX_HDPRO;
   FYD           = DIFLY_HDPRO;
   FXO           = OIFLX_HDPRO;
   FYO           = OIFLY_HDPRO;
   RealHeadSizeH = 220.0f;
   RealHeadSizeV = 265.0f;
   SetParams (3500,500,2500);
}

 void ForegroundDetection_HMD_OHV_3DM::Close (   )

{
   ForegroundDetection::Close (   );
   InvDepthMap.Delete (   );
}

 void ForegroundDetection_HMD_OHV_3DM::FindHeadCandidates(GImage& s_image, int cell_size, int head_size, std::vector<Cell>& head_candidates)

{
   #if defined(__DEBUG_FGD)
   COLORREF colors[6] = { PC_RED,PC_GREEN,PC_BLUE,PC_YELLOW,PC_MAGENTA,PC_CYAN };
   #endif

   int   head_radius = head_size / 2;
   float head_area   = MC_PI * head_radius * head_radius;

   // 각 Cell 마다 Local Maximum 정보를 얻는다.
   int c_width  = ((s_image.Width  + (cell_size - 1)) / cell_size);
   int c_height = ((s_image.Height + (cell_size - 1)) / cell_size);
   Array2D<Cell> cl_array(c_width, c_height);
   for (int y = 0; y < cl_array.Height; ++y) {
      Cell* l_cl_array = cl_array[y];
      for (int x = 0; x < cl_array.Width; ++x) {
         FindLocalMaximum (s_image,x * cell_size,y * cell_size,cell_size,l_cl_array[x]);
      }
   }

   #if defined(__DEBUG_FGD)
   _DebugView_FGD->DrawImage (s_image,_DVX_FGD,_DVY_FGD);
   for (int y = cell_size; y < s_image.Height; y += cell_size)
     _DebugView_FGD->DrawLine(_DVX_FGD, y - 1 + _DVY_FGD, s_image.Width - 1 + _DVX_FGD, y - 1 + _DVY_FGD, PC_YELLOW);
   for (int x = cell_size; x < s_image.Width; x += cell_size)
     _DebugView_FGD->DrawLine(x - 1 + _DVX_FGD, _DVY_FGD, x - 1 + _DVX_FGD, s_image.Height - 1 + _DVY_FGD, PC_YELLOW);
   for (int y = 0; y < cl_array.Height; ++y) {
      for (int x = 0; x < cl_array.Width; ++x) {
         if (cl_array[y][x].is_valid) {
            _DebugView_FGD->DrawFilledCircle(cl_array[y][x].max_pos_x + _DVX_FGD, cl_array[y][x].max_pos_y + _DVY_FGD, 3, PC_RED);
         }
      }
   }
   _DVY_FGD += s_image.Height;
   _DebugView_FGD->DrawText (_T("Initial Local Maxima"),_DVX_FGD,_DVY_FGD,PC_GREEN);
   _DVY_FGD += 20;
   #endif
   
   std::vector<Cell> candidates;
   for (int y = 0; y < cl_array.Height; ++y) {
      for (int x = 0; x < cl_array.Width; ++x) {
         Cell cell;
         SelectMaximumInNeighborPixels(cl_array, s_image, x, y, cell_size, head_size, cell);
         if (cell.is_valid) candidates.push_back(cell);
      }
   }

   #if defined(__DEBUG_FGD)
   _DebugView_FGD->DrawImage (s_image,_DVX_FGD,_DVY_FGD);
   for (int y = cell_size; y < s_image.Height; y += cell_size)
      _DebugView_FGD->DrawLine(_DVX_FGD, y - 1 + _DVY_FGD, s_image.Width - 1 + _DVX_FGD, y - 1 + _DVY_FGD, PC_YELLOW);
   for (int x = cell_size; x < s_image.Width; x += cell_size)
      _DebugView_FGD->DrawLine(x - 1 + _DVX_FGD, _DVY_FGD, x - 1 + _DVX_FGD, s_image.Height - 1 + _DVY_FGD, PC_YELLOW);
   for (int i = 0; i < (int)candidates.size(); i++) {
      if (candidates[i].is_valid) {
         int cx = candidates[i].max_pos_x + _DVX_FGD;
         int cy = candidates[i].max_pos_y + _DVY_FGD;
         _DebugView_FGD->DrawFilledCircle (cx,cy,3,colors[i % 6]);
         _DebugView_FGD->DrawCircle (cx,cy,head_radius,colors[i % 6]);
      }
   }
   _DVY_FGD += s_image.Height;
   _DebugView_FGD->DrawText  (_T("Intermediate Local Maxima"),_DVX_FGD,_DVY_FGD,PC_GREEN);
   _DVY_FGD += 20;
   #endif

   // Greedy NMS(Non-maximum Suppression)을 수행한다.
   IB2DArray1D hd_array1((int)candidates.size());
   FArray1D    sc_array1(hd_array1.Length);
   for (int i = 0; i < hd_array1.Length; ++i)
   {
      hd_array1[i](
         candidates[i].max_pos_x - head_radius,
         candidates[i].max_pos_y - head_radius, 
         head_size,
         head_size
      );
      sc_array1[i] = (float)candidates[i].max_val;
   }

   IB2DArray1D hd_array2;
   PerformRectNMS_Greedy (hd_array1,sc_array1,0.3f,0.0f,hd_array2); // PARAMETERS
      // 박스 간의 겹치는 면적의 양이 0.3 이상이면 NMS를 수행한다.

   #if defined(__DEBUG_FGD)
   _DebugView_FGD->DrawImage (s_image,_DVX_FGD,_DVY_FGD);
   for (int y = cell_size; y < s_image.Height; y += cell_size)
      _DebugView_FGD->DrawLine(_DVX_FGD, y - 1 + _DVY_FGD, s_image.Width - 1 + _DVX_FGD, y - 1 + _DVY_FGD, PC_YELLOW);
   for (int x = cell_size; x < s_image.Width; x += cell_size)
      _DebugView_FGD->DrawLine(x - 1 + _DVX_FGD, _DVY_FGD, x - 1 + _DVX_FGD, s_image.Height - 1 + _DVY_FGD, PC_YELLOW);
   for (int i = 0; i < hd_array2.Length; ++i) {
      int cx = hd_array2[i].X + (hd_array2[i].Width  / 2) + _DVX_FGD;
      int cy = hd_array2[i].Y + (hd_array2[i].Height / 2) + _DVY_FGD;
      _DebugView_FGD->DrawFilledCircle (cx,cy,3,colors[i % 6]);
      _DebugView_FGD->DrawCircle (cx,cy,head_radius,colors[i % 6]);
   }
   _DVY_FGD += s_image.Height;
   _DebugView_FGD->DrawText (_T("After Non-maximum Suppression"),_DVX_FGD,_DVY_FGD,PC_GREEN);
   _DVY_FGD += 20;
   #endif

   for (int i = 0; i < hd_array2.Length; ++i) {
      int s_cx    = hd_array2[i].X + (hd_array2[i].Width  / 2);
      int s_cy    = hd_array2[i].Y + (hd_array2[i].Height / 2);
      int hd_size = (int)(0.8f * hd_array2[i].Width); // PARAMETERS
      // Head Candidate의 중심을 영역 내의 Centroid로 이동시킨다.
      int d_cx,d_cy;
      GetHeadCentroid (FrgImage,s_cx,s_cy,hd_size,d_cx,d_cy);
      // Head Candidate 좌표를 head_candidates 배열에 저장한다.
      Cell cell;
      cell.is_valid  = true;
      cell.max_val   = s_image[d_cy][d_cx];
      cell.max_pos_x = d_cx;
      cell.max_pos_y = d_cy;
      head_candidates.push_back(cell);
      // Residual Image를 구하기 위해 FrgImage로부터 Head Candidate 영역을 지운다.
      DrawFilledCircle (d_cx,d_cy,hd_array2[i].Width,(byte)0,FrgImage);
   }

   #if defined(__DEBUG_FGD)
   _DebugView_FGD->DrawImage (FrgImage,_DVX_FGD,_DVY_FGD);
   _DVY_FGD += FrgImage.Height;
   _DebugView_FGD->DrawText  (_T("Residual Image"),_DVX_FGD,_DVY_FGD,PC_GREEN);
   _DVY_FGD += 20;
   #endif

   // Residual Blob의 면적이 일정량 이상이고 Blob의 centroid가 영상의 가장자리에 속해 있으면 살린다.
   IArray2D cr_map(FrgImage.Width,FrgImage.Height);
   int n_crs = LabelCRs (FrgImage,cr_map);
   if (n_crs) {
      int rim_ths  = 40; // PARAMETERS: 영상 가장자리의 두께
      int area_ths = (int)(0.13f * head_area); // PARAMETERS: CR의 허용 면적의 최소값
      int ex = cr_map.Width  - rim_ths - 1;
      int ey = cr_map.Height - rim_ths - 1;
      CRArray1D cr_array(n_crs + 1);
      GetCRInfo(cr_map,cr_array);	
      for (int i = 1; i < cr_array.Length; ++i) {
         if (cr_array[i].CX > rim_ths && cr_array[i].CX < ex && cr_array[i].CY > rim_ths && cr_array[i].CY < ey) continue;
         if (cr_array[i].Area >= area_ths) {
            Cell cell;
            cell.is_valid  = true;
            cell.max_pos_x = (int)cr_array[i].CX;
            cell.max_pos_y = (int)cr_array[i].CY;
            cell.max_val   = s_image[cell.max_pos_y][cell.max_pos_x];
            head_candidates.push_back(cell);
         }
      }
   }

   #if defined(__DEBUG_FGD)
   _DebugView_FGD->DrawImage (s_image,_DVX_FGD,_DVY_FGD);
   for (int y = cell_size; y < s_image.Height; y += cell_size)
     _DebugView_FGD->DrawLine(_DVX_FGD, y - 1 + _DVY_FGD, s_image.Width - 1 + _DVX_FGD, y - 1 + _DVY_FGD, PC_YELLOW);
   for (int x = cell_size; x < s_image.Width; x += cell_size)
     _DebugView_FGD->DrawLine(x - 1 + _DVX_FGD, _DVY_FGD, x - 1 + _DVX_FGD, s_image.Height - 1 + _DVY_FGD, PC_YELLOW);
   for (int i = 0; i < (int)head_candidates.size(); i++) {
      int cx = head_candidates[i].max_pos_x + _DVX_FGD;
      int cy = head_candidates[i].max_pos_y + _DVY_FGD;
      _DebugView_FGD->DrawFilledCircle (cx,cy,3,colors[i % 6]);
      _DebugView_FGD->DrawCircle (cx,cy,head_radius,colors[i % 6]);
   }
   _DVY_FGD += s_image.Height;
   _DebugView_FGD->DrawText (_T("Final Local Maxima"),_DVX_FGD,_DVY_FGD,PC_GREEN);
   _DVY_FGD += 20;
   #endif
}

 void ForegroundDetection_HMD_OHV_3DM::FindLocalMaximum (GImage& s_image, int sx, int sy, int cell_size, Cell& cell)

{
   int x,y;

   int ex = sx + cell_size;
   int ey = sy + cell_size;
   ex = ex > s_image.Width  ? s_image.Width  : ex;
   ey = ey > s_image.Height ? s_image.Height : ey;
   byte max_v = 0;
   for (y = sy; y < ey; y++) {
      byte* l_s_image = s_image[y];
      for (x = sx; x < ex; x++) {
         if (l_s_image[x] > max_v) max_v = l_s_image[x];
      }
   }
   // 최대값이 너무 작으면 버린다.
   if (max_v <= 30) {
      cell.is_valid = false;
      cell.max_val  = 0;
      return;
   }
   // max_v와 동일한 값을 갖는 픽셀들의 좌표의 평균을 구한다.
   int cx = 0, cy = 0, n = 0;
   for (y = sy; y < ey; y++) {
      byte* l_s_image = s_image[y];
      for (x = sx; x < ex; x++) {
         if (l_s_image[x] == max_v) {
            cx += x;
            cy += y;
            n++;
         }
      }
   }
   cell.is_valid  = true;
   cell.max_val   = max_v;
   cell.max_pos_x = cx / n;
   cell.max_pos_y = cy / n;
}

void ForegroundDetection_HMD_OHV_3DM::GetFixedHeadSize(int cam_height)
{
   float proj_h = FXD * RealHeadSizeH / (cam_height - 2000.0f);
   float proj_v = FYD * RealHeadSizeV / (cam_height - 2000.0f);
   HeadSize = (int)(GetMaximum (proj_v,proj_h) * 2.5f);
   HeadSize = HeadSize > 90 ? 90 : HeadSize;
}

 int ForegroundDetection_HMD_OHV_3DM::GetFrgImage (   )

{
   GImage d_map(SrcImage.Width,SrcImage.Height);
   GetPreprocDepthMap (SrcImage,d_map);

   #if defined(__DEBUG_FGD)
   _DebugView_FGD->DrawImage (d_map,_DVX_FGD,_DVY_FGD);
   _DVY_FGD += d_map.Height;
   _DebugView_FGD->DrawText  (_T("Preprocessed Depth Map"),_DVX_FGD,_DVY_FGD,PC_GREEN);
   _DVY_FGD += 20;
   #endif

   int cell_size = 60;
   std::vector<Cell> head_candidates;
   FindHeadCandidates(d_map, cell_size, HeadSize,head_candidates);

   // Circle들을 중심점의 Depth(Inverted) 값을 이용하여 오름차순으로 정렬한다.
   OSArray1D os_array((int)head_candidates.size());
   for (int i = 0; i < os_array.Length; i++) {
      os_array[i].IndexNo = i;
      os_array[i].Score   = (double)head_candidates[i].max_val;
   }
   qsort (os_array,os_array.Length,sizeof(ObjectScore),Compare_ObjectScore_Ascending);

   // Depth 값이 작은 Circle부터 순차적으로 그린다.
   FrgRegionMap.Clear (   );
   int cr_no = 1;
   for (int i = 0; i < os_array.Length; i++) {
      int j  = os_array[i].IndexNo;
      int cx = head_candidates[j].max_pos_x;
      int cy = head_candidates[j].max_pos_y;
      if (DetAreaMap[cy][cx]) {
         DrawFilledCircle(cx,cy,HeadSize,cr_no++,FrgRegionMap);
      }
   }

   #if defined(__DEBUG_FGD)
   _DebugView_FGD->DrawImage (FrgRegionMap,_DVX_FGD,_DVY_FGD);
   _DVY_FGD += FrgRegionMap.Height;
   _DebugView_FGD->DrawText  (_T("Foreground Region Map"),_DVX_FGD,_DVY_FGD,PC_GREEN);
   _DVY_FGD += 20;
   #endif

   FrgRegions.Create (cr_no);
   GetCRInfo (FrgRegionMap,FrgRegions);
   Binarize (FrgRegionMap,(int)0,FrgImage);

   return (FGD_RCODE_NORMAL);
}

 void ForegroundDetection_HMD_OHV_3DM::GetHeadCentroid (GImage& s_image,int s_cx,int s_cy,int diameter,int& d_cx,int& d_cy) 

{
   int area = 0;
   int x = 0; 
   int y = diameter >> 1;
   int d = 3 - diameter;
   d_cx = d_cy = 0;
   while (x <= y) {
      GetHorizontalIntegral (s_image,s_cx - y,s_cx + y,s_cy + x,d_cx,d_cy,area); 
      if (x > 0) GetHorizontalIntegral (s_image,s_cx - y,s_cx + y,s_cy - x,d_cx,d_cy,area);
      if (d < 0) d += (x << 2) + 6; 
      else {
         d += ((x - y) << 2) + 10; 
         if (x != y) {
            GetHorizontalIntegral (s_image,s_cx - x,s_cx + x,s_cy - y,d_cx,d_cy,area); 
            GetHorizontalIntegral (s_image,s_cx - x,s_cx + x,s_cy + y,d_cx,d_cy,area); 
         }
         y--;
      } 
      x++;
   }
   d_cx /= area;
   d_cy /= area;
}

 void ForegroundDetection_HMD_OHV_3DM::GetHorizontalIntegral (GImage& s_image,int x1,int x2,int y,int& cx,int& cy,int& area)

{
   int x;
   
   if (y < 0 || y >= s_image.Height) return;
   if (x1 > x2) Swap (x1,x2);
   if (x2 < 0 || x1 >= s_image.Width) return;
   if (x1 < 0) x1 = 0;
   if (x2 >= s_image.Width) x2 = s_image.Width - 1;
   byte* l_s_image = s_image[y];
   for (x = x1; x <= x2; ++x) {
      if (l_s_image[x]) {
         cx += x;
         cy += y;
         area++;
      }
   }
}

 void ForegroundDetection_HMD_OHV_3DM::GetParams (int& cam_height,int& min_obj_height,int& max_obj_height)

{
   cam_height     = CameraHeight;
   min_obj_height = MinObjHeight;
   max_obj_height = MaxObjHeight;
}

void ForegroundDetection_HMD_OHV_3DM::GetPreprocDepthMap (GImage& s_image,GImage& d_image)
{
   // Local Maximum을 찾기 위하여 Depth Map을 반전시킨다.
   // 이미 0으로 처리된 값(Depth 추정 불가 픽셀)들은 미리 제거하기 위해 반전에서 제외하고,
   // 나머지 값들에 대해서는 유효한 Depth Range에서만 고려하도록 Thresholding 한다.
   InvDepthMap.Clear (   );
   int ex = s_image.Width  - Margin;
   int ey = s_image.Height - Margin;
   for (int y = Margin; y < ey; ++y) {
      byte* l_s_image     = s_image[y];
      byte* l_InvDepthMap = InvDepthMap[y];
      for (int x = Margin; x < ex; ++x) {
         if (l_s_image[x] > DepthUpperLimit || l_s_image[x] < DepthLowerLimit) continue;
         l_InvDepthMap[x] = byte(255) - l_s_image[x];
      }
   }
   Binarize (InvDepthMap,byte(0),FrgImage);
   PerformBinOpening (FrgImage,3,FrgImage);
   InvDepthMap &= FrgImage;
   GaussianFiltering (InvDepthMap,GFSize,d_image);
}

 void ForegroundDetection_HMD_OHV_3DM::GetPreprocSrcImage (GImage& s_image)

{
   SrcImage = s_image;
   #if defined(__DEBUG_FGD)
   _DebugView_FGD->Clear();
   _DVX_FGD = _DVY_FGD = 0;
   _DebugView_FGD->DrawImage (SrcImage,_DVX_FGD,_DVY_FGD);
   _DVY_FGD += SrcImage.Height;
   _DebugView_FGD->DrawText  (_T("Source Image"),_DVX_FGD,_DVY_FGD,PC_GREEN);
   _DVY_FGD += 20;
   #endif
}

 int ForegroundDetection_HMD_OHV_3DM::Initialize (ISize2D& si_size,float frm_rate)

{
   if (ForegroundDetection::Initialize (si_size,frm_rate)) return (1);
   InvDepthMap.Create (si_size.Width,si_size.Height);
   return (DONE);
}

 int ForegroundDetection_HMD_OHV_3DM::ReadFileEx (FileIO& file)

{
   if (file.Read ((byte*)&CameraHeight,1,sizeof(CameraHeight))) return (1);
   if (file.Read ((byte*)&MinObjHeight,1,sizeof(MinObjHeight))) return (2);
   if (file.Read ((byte*)&MaxObjHeight,1,sizeof(MaxObjHeight))) return (3);
   SetParams (CameraHeight,MinObjHeight,MaxObjHeight);
   return (DONE);
}

 int ForegroundDetection_HMD_OHV_3DM::ReadFileEx (CXMLIO* pIO)

{
   CXMLNode xmlNode (pIO);
   int flag_found = FALSE;
   if      (xmlNode.Start ("ForegroundDetection_HMD_OHV_3DM")) flag_found = TRUE;
   else if (xmlNode.Start ("ForegroundDetection_3DC_OHV"))     flag_found = TRUE; // 이전 config와의 호환성 유지 목적
   else if (xmlNode.Start ("ForegroundDetection_3DD_LMD"))     flag_found = TRUE; // 이전 config와의 호환성 유지 목적
   if (flag_found) {
      xmlNode.Attribute (TYPE_ID(int),"CameraHeight",&CameraHeight);
      xmlNode.Attribute (TYPE_ID(int),"MinObjHeight",&MinObjHeight);
      xmlNode.Attribute (TYPE_ID(int),"MaxObjHeight",&MaxObjHeight);
      xmlNode.End (   );
      SetParams (CameraHeight,MinObjHeight,MaxObjHeight);
      return (DONE);
   }
   return (1);
}

 void ForegroundDetection_HMD_OHV_3DM::SelectMaximumInNeighborPixels (Array2D<Cell>& table,GImage& s_image,int curr_x,int curr_y,int cell_size,int head_size,Cell& cell)

{
   Cell& c_cell = table[curr_y][curr_x];
   if (!c_cell.is_valid) {
      cell.is_valid = false;
      return;
   }
   int max_val     = (int)c_cell.max_val;
   int max_val2    = max_val + 5;
   int head_radius = head_size >> 1;
   int squared_head_radius = head_radius * head_radius;
   int head_center_x = c_cell.max_pos_x;
   int head_center_y = c_cell.max_pos_y;
   int inc = 0, dec = 0, interval = head_radius / 10;
   for (int y = -head_radius; y <= head_radius; y += interval) {
      if (head_center_y + y < 0 || head_center_y + y >= s_image.Height) continue;
      byte* l_s_image = s_image[head_center_y + y];
      for (int x = -head_radius; x <= head_radius; x += interval) {
         if (head_center_x + x < 0 || head_center_x + x >= s_image.Width) continue;
         int squared_distance = x * x + y * y;
         if (squared_distance <= squared_head_radius) {
            if (max_val2 < (int)l_s_image[head_center_x + x]) {
               if (!SeparatorBasedBresenham(s_image,head_center_x,head_center_y,head_center_x + x,head_center_y + y)) inc++;
               else dec++;
            }
         }
      }
   }
   if (5 * inc > dec) {
      cell.is_valid = false;
      return;
   }
   else {
      cell.is_valid = true;
      cell.max_val = (byte)max_val;
      cell.max_pos_x = head_center_x;
      cell.max_pos_y = head_center_y;
   }
}

 bool ForegroundDetection_HMD_OHV_3DM::SeparatorBasedBresenham (GImage& s_image,int x1,int y1,int x2,int y2)

{
   int i,x,y;
   int dx,dy,dx1,dy1;
   int px,py,xe,ye;

   std::vector<byte> elems;
   if (ClipLine (s_image.Width,s_image.Height,x1,y1,x2,y2)) return false;
   dx = x2 - x1;
   dy = y2 - y1;
   dx1 = abs(dx);
   dy1 = abs(dy);
   px = 2 * dy1 - dx1;
   py = 2 * dx1 - dy1;
   byte std_max = s_image[y1][x1] - byte(9);
   if (dy1 <= dx1) {
      if (dx >= 0) {
         x = x1;
         y = y1;
         xe = x2;
      }
      else {
         x = x2;
         y = y2;
         xe = x1;
      }
      elems.push_back(s_image[y][x]);
      if (s_image[y][x] < std_max) {
         return true;
      }
      for (i = 0; x < xe; i++) {
         x = x + 1;
         if (px < 0) {
            px = px + 2 * dy1;
         }
         else {
            if ((dx < 0 && dy < 0) || (dx > 0 && dy > 0)) {
               y = y + 1;
            }
            else {
               y = y - 1;
            }
            px = px + 2 * (dy1 - dx1);
         }
         elems.push_back(s_image[y][x]);
         if (s_image[y][x] < std_max) {
            return true;
         }
      }
   }
   else {
      if (dy >= 0) {
         x = x1;
         y = y1;
         ye = y2;
      }
      else {
         x = x2;
         y = y2;
         ye = y1;
      }
      elems.push_back(s_image[y][x]);
      if (s_image[y][x] < std_max) {
         return true;
      }
      for (i = 0; y < ye; i++) {
         y = y + 1;
         if (py <= 0) {
            py = py + 2 * dx1;
         }
         else {
            if ((dx < 0 && dy < 0) || (dx > 0 && dy > 0)) {
               x = x + 1;
            }
            else {
               x = x - 1;
            }
            py = py + 2 * (dx1 - dy1);
         }
         elems.push_back(s_image[y][x]);
         if (s_image[y][x] < std_max) {
            return true;
         }
      }
   }
   // (x1, y1)에서 (x2, y2)까지의 depth가 상승할지라도
   // 그 사이 거리가 충분히 작다면 non-maximum suppression에 주는 영향이 미미하므로 살려둔다.
   // cell_size * 0.5 = 30
   if (elems.size() < 30) return true;
   return false;
}

 int ForegroundDetection_HMD_OHV_3DM::SetParams (int cam_height,int min_obj_height,int max_obj_height)

{
   if (min_obj_height >= max_obj_height) return (1);
   if (cam_height <= max_obj_height) return (2);
   if (min_obj_height < 0) return (3);
   CameraHeight    = cam_height;
   MinObjHeight    = min_obj_height;
   MaxObjHeight    = max_obj_height;
   GetFixedHeadSize (CameraHeight);
   DepthLowerLimit = (byte)((cam_height - max_obj_height) / 32.0f + 0.5f);
   DepthUpperLimit = (byte)((cam_height - min_obj_height) / 32.0f + 0.5f);
   return (DONE);
}

 int ForegroundDetection_HMD_OHV_3DM::WriteFileEx (FileIO& file)

{
   if (file.Write ((byte*)&CameraHeight,1,sizeof(CameraHeight))) return (1);
   if (file.Write ((byte*)&MinObjHeight,1,sizeof(MinObjHeight))) return (2);
   if (file.Write ((byte*)&MaxObjHeight,1,sizeof(MaxObjHeight))) return (3);
   return (DONE);
}

 int ForegroundDetection_HMD_OHV_3DM::WriteFileEx (CXMLIO* pIO)

{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("ForegroundDetection_HMD_OHV_3DM"))
   {
      xmlNode.Attribute (TYPE_ID(int),"CameraHeight",&CameraHeight);
      xmlNode.Attribute (TYPE_ID(int),"MinObjHeight",&MinObjHeight);
      xmlNode.Attribute (TYPE_ID(int),"MaxObjHeight",&MaxObjHeight);
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 int ConvertDepthImage_HDPro (GImage& s_image,GImage& d_image)

{
   int x,y;

   if (!s_image || !d_image) return (1);
   if (s_image.Width != d_image.Width * 2 || s_image.Height != d_image.Height) return (2);
   int ox = d_image.Width;
   for (y = 0; y < d_image.Height; y++) {
      byte* l_s_image = s_image[y];
      byte* l_d_image = d_image[y];
      for (x = 0; x < d_image.Width; x++) {
         l_d_image[x] = (l_s_image[x] & 0xF0) | ((l_s_image[x + ox] & 0xF0) >> 4);
      }
   }
   return (DONE);
}

 void ConvertDepthToOpticalImageCoord_HDPro (int xd,int yd,int oi_width,int& xo,int& yo)

{
   float s = oi_width / 1280.0f;
   xo = int((640.0 + OIFLX_HDPRO * (xd - 160.0) / DIFLX_HDPRO) * s);
   yo = int((360.0 - OIFLY_HDPRO * (yd - 120.0) / DIFLY_HDPRO) * s);
      // Depth Image와 Optical Image가 상하반전 관계에 있는 경우!
}

}

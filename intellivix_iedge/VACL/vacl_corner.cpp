#include "vacl_corner.h"
#include "vacl_edge.h"
#include "vacl_nms.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 int DetectCorners_Tomasi (GImage &s_image,int win_size,int e_thrsld,float c_thrsld,int nms_win_size,GImage &d_image)
 // Typical parameter values: win_size = 5, e_thrsld = 25, c_thrsld = 15, nms_win_size = 9
{
   int x,y,x2,y2;

   SArray2D gx_array(s_image.Width,s_image.Height);
   SArray2D gy_array(s_image.Width,s_image.Height);
   FArray2D c_array (s_image.Width,s_image.Height);
   c_array.Clear (   );
   GetGradients (s_image,gx_array,gy_array);
   c_thrsld *= 2.0f * c_thrsld;
   int hws = win_size / 2;
   int ex = s_image.Width  - hws;
   int ey = s_image.Height - hws;
   for (y = hws; y < ey; y++) {
      short* l_gx_array = gx_array[y];
      short* l_gy_array = gy_array[y];
      float* l_c_array  = c_array[y];
      for (x = hws; x < ex; x++) {
         int mg = abs(l_gx_array[x]) + abs(l_gy_array[x]);
         if (mg > e_thrsld) {
            int sx2 = x - hws;
            int ex2 = x + hws;
            int sy2 = y - hws;
            int ey2 = y + hws;
            int a = 0, b = 0, c = 0;
            for (y2 = sy2; y2 <= ey2; y2++) {
               short* l_gx_array2 = gx_array[y2];
               short* l_gy_array2 = gy_array[y2];
               for (x2 = sx2; x2 <= ex2; x2++) {
                  a += (int)l_gx_array2[x2] * l_gx_array2[x2];
                  b += (int)l_gx_array2[x2] * l_gy_array2[x2];
                  c += (int)l_gy_array2[x2] * l_gy_array2[x2];
               }
            }
            int d = a - c;
            l_c_array[x] = a + c - (float)sqrt(d * d + 4.0f * b * b);
         }
      }
   }
   c_array.SetBoundary (hws,0.0f);
   int n_corners = PerformPointNMS (c_array,nms_win_size,c_thrsld,d_image);
   return (n_corners);
} 

}

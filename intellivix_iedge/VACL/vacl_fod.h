#if !defined(__VACL_FOD_H)
#define __VACL_FOD_H

#include "vacl_define.h"

#if defined(__LIB_WIN32CL)
#include "Win32CL/Win32CL.h"
#endif

 namespace VACL

{
///////////////////////////////////////////////////////////////////////////////
//
// Class: CThread_FODD
//
///////////////////////////////////////////////////////////////////////////////
//
// FODD: FOD(Foreign Object Debris) Detection
//

#if defined(__LIB_WIN32CL)

 class CThread_FODD : public CThread

{
   public:
      int   MinBBArea;
      int   MaxBBArea;
      float MaxPALRatio;

   public:
      int    ThreadIdx;
      IBox2D ROI;

   public:
      GImage*   SrcImage;
      GImage    EdgeImage;
      IArray2D  EdgeCRMap;
      CRArray1D EdgeCRArray;
      BArray1D  DetObjects;

   public:
      CEvent Event_Start;
      CEvent Event_End;

   public:
      CThread_FODD (   );

   protected:
      int Perform (   );
   
   protected:
      virtual void ThreadProc (   );
};

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: FODDetection_MT
//
///////////////////////////////////////////////////////////////////////////////
//
// MT: Multithreaded
//

#if defined(__LIB_WIN32CL)

 class FODDetection_MT

{
   protected:
      int Flag_Initialized;

   protected:
      Array1D<HANDLE>       EndEvents;
      Array1D<CThread_FODD> Threads;
         
   public:
      int   NumThreads;
      int   MinBBArea;
      int   MaxBBArea;
      float MaxPALRatio;
      float OuterZoneSize;
      GImage DetAreaMap;
      PolygonalAreaList DetAreas;
      PolygonalAreaList NonDetAreas;

   public:
      ISize2D SrcImgSize;

   public:
      FODDetection_MT (   );
      virtual ~FODDetection_MT (   );

   protected:
      int  CheckRegionInOuterZone (IBox2D& s_rgn);
      void MergeImages            (Array1D<GImage*>& s_images,IB2DArray1D& s_rects,GImage& d_image);

   public:
      virtual void Close (   );

   public:
      int Initialize (ISize2D& si_size,ISize2D& ri_size);
      int Perform    (GImage& s_image,IB2DArray1D& d_array);
};

#endif

}

#endif

#include "vacl_pathpass.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: Edge
//
///////////////////////////////////////////////////////////////////////////////

 Edge::Edge (   )

{
   IP(-1.0f,-1.0f);
   Prev = Next = NULL;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: Arrow
//
///////////////////////////////////////////////////////////////////////////////

 Arrow::Arrow (   )

{
   SplitZoneID = -1;
   Prev = Next = NULL;
}

 int Arrow::ReadFile (FileIO &file)

{
   if (file.Read ((byte*)this,1,sizeof(ILine2D)))     return (1);
   if (file.Read ((byte*)&SplitZoneID,1,sizeof(int))) return (2);
   return (DONE);
}

 int Arrow::ReadFile (CXMLIO* pIO)
 
{
   static Arrow dv; // jun : defalut value (기본값과 같으면 xml로 부터 읽거나 저장하지 않음). 

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Arrow")) {
      xmlNode.Attribute (TYPE_ID (ILine2D), "Line"        , this         , &dv);
      xmlNode.Attribute (TYPE_ID (int)    , "SplitZoneID" , &SplitZoneID , &dv.SplitZoneID);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

 int Arrow::WriteFile (FileIO &file)

{
   if (file.Write ((byte*)this,1,sizeof(ILine2D)))     return (1);
   if (file.Write ((byte*)&SplitZoneID,1,sizeof(int))) return (2);
   return (DONE);
}

 int Arrow::WriteFile (CXMLIO* pIO)
 
{
   static Arrow dv; // jun : defalut value

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("Arrow")) {
      xmlNode.Attribute (TYPE_ID (ILine2D), "Line"        , this         , &dv);
      xmlNode.Attribute (TYPE_ID (int)    , "SplitZoneID" , &SplitZoneID , &dv.SplitZoneID);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: ArrowList
//
///////////////////////////////////////////////////////////////////////////////

 void ArrowList::OffsetArrows (int dx,int dy)

{
   Arrow *arrow = First (   );
   while (arrow) {
      (*arrow)[0].X += dx;
      (*arrow)[0].Y += dy;
      (*arrow)[1].X += dx;
      (*arrow)[1].Y += dy;
      arrow = Next (arrow);
   }
}
  
 int ArrowList::ReadFile (FileIO &file)

{
   Delete (   );

   int i, n_items;
   if (file.Read ((byte*)&n_items,1,sizeof(int))) return (1);
   for (i = 0 ; i < n_items ; i++) {
      Arrow *arrow = new Arrow;
      if (arrow->ReadFile (file)) {
         delete arrow;
         return (1);
      }
      Add (arrow);
   }
   return (DONE);
}

 int ArrowList::ReadFile (CXMLIO* pIO)

{
   int i,n_items;

   Delete (   );
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("ArrowList")) {
      xmlNode.Attribute (TYPE_ID (int), "ItemNum", &n_items);
      for (i = 0 ; i < n_items ; i++) {
         Arrow *arrow = new Arrow;
         if (DONE == arrow->ReadFile (pIO)) 
            Add (arrow);
         else
            delete arrow;
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}
 
 int ArrowList::WriteFile (FileIO &file)

{
   int n_items = GetNumItems (   );
   if (file.Write ((byte*)&n_items,1,sizeof(int))) return (1);
   Arrow *arrow = First (   );
   while (arrow) {
      if (arrow->WriteFile (file)) return (1);
      arrow = Next (arrow);
   }
   return (DONE);
}

 int ArrowList::WriteFile (CXMLIO* pIO)

{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("ArrowList")) {
      int n_items = GetNumItems (   );
      xmlNode.Attribute (TYPE_ID (int), "ItemNum", &n_items);
      Arrow *arrow = First (   );
      while (arrow) {
         arrow->WriteFile (pIO);
         arrow = Next (arrow);
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: Path
//
///////////////////////////////////////////////////////////////////////////////

 Path::Path (   )

{
   Time_Elapsed    = 0.0f;
   DirectionToPass = 0;
   CurZonePtr      = NULL;
   EvtZone         = NULL;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: PathZonePtr
//
///////////////////////////////////////////////////////////////////////////////

 PathZonePtr::PathZonePtr (PathZone* zone)

{
   Zone = zone;
   Prev = Next = NULL;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: PathZoneRoot
//
///////////////////////////////////////////////////////////////////////////////

 PathZoneRoot::PathZoneRoot (   )

{
   NumChildren = 0;
   Root        = this;
}

 int PathZoneRoot::ReadFile (FileIO& file)

{
   if (PathZone::ReadFile (file))                     return (1);
   if (file.Read ((byte*)&NumChildren,1,sizeof(int))) return (2);
   return (DONE);
}

 int PathZoneRoot::ReadFile (CXMLIO* pIO)

{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PathZoneRoot")) {
      PathZone::ReadFile (pIO);
      xmlNode.Attribute (TYPE_ID (int), "ChildNum", &NumChildren);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

 int PathZoneRoot::WriteFile (FileIO& file)

{
   if (PathZone::WriteFile (file))                     return (1);
   if (file.Write ((byte*)&NumChildren,1,sizeof(int))) return (2);
   return (DONE);
}

 int PathZoneRoot::WriteFile (CXMLIO* pIO)
 
{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PathZoneRoot")) {
      PathZone::WriteFile (pIO);
      xmlNode.Attribute (TYPE_ID (int), "ChildNum" , &NumChildren);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: PathZone
//
///////////////////////////////////////////////////////////////////////////////

 PathZone::PathZone (   )

{
   ID = 0;
   Root = NULL;
   Scale(1.0f,1.0f);
}

 int PathZone::CheckRegionInsideZone (IBox2D &b_box)
 
{
   FPoint2D pos = b_box.GetCenterPosition (   );
   pos *= Scale;
   if (IsPointInsidePolygon (pos)) return (DONE);
   else return (1);
}

 PathZone* PathZone::FindPathZone (int id)

{
   int i;

   if (ID == id) return (this);
   for (i = 0 ; i < Children.Length ; i++) {
      PathZone* path_zone = Children[i].FindPathZone (id);
      if (path_zone) return (path_zone);
   }
   return NULL;
}

 PathZone* PathZone::FindPathZone (IBox2D &b_box)

{
   int i;
   
   for (i = 0 ; i < Children.Length ; i++) {
      PathZone* path_zone = Children[i].FindPathZone (b_box);
      if (path_zone) return (path_zone);
   }
   if (CheckRegionInsideZone (b_box) == DONE) return (this);
   return (NULL);
}

 PathZone* PathZone::FindPathZone (IPoint2D &s_point)

{
   int i;
   
   for (i = 0 ; i < Children.Length ; i++) {
      PathZone* path_zone = Children[i].FindPathZone (s_point);
      if (path_zone) return path_zone;
   }
   if (IsPointInsidePolygon (s_point)) return (this);
   return (NULL);
}

 char PathZone::GetIDLetter (   )

{
   char letter;
   if      (ID <= 26) letter = (char)(ID + 64); // 'A'~'Z'
   else if (ID <= 52) letter = (char)(ID + 70); // 'a'~'z'
   else               letter = (char)(ID - 52);
   return letter;
}

 void PathZone::GetLeafZonePtrs (LinkedList<PathZonePtr> &pz_ptrs)

{
   if (Children.Length == 0) {
      PathZonePtr *pz_ptr = new PathZonePtr (this);
      pz_ptrs.Add (pz_ptr);
   } else {
      int i;
      for (i = 0 ; i < Children.Length ; i++)
         Children[i].GetLeafZonePtrs (pz_ptrs);
   }
}

 void PathZone::MergeZones (   )

{
   int i;
   for (i = 0 ; i < Children.Length ; i++) {
      Children[i].MergeZones (   );
      Root->NumChildren--;
   }
   Children.Delete (   );
}

 void PathZone::OffsetZones (int dx,int dy) 

{
   Polygon::Offset (dx, dy);
   int i;
   for (i = 0 ; i < Children.Length ; i++)
      Children[i].OffsetZones (dx, dy);
}

 int PathZone::ReadFile (FileIO& file)

{
   int i;

   if (Polygon::ReadFile (file)) return (1);
   int n_children;
   if (file.Read ((byte*)&n_children,1,sizeof(int))) return (2);
   Children.Create (n_children);
   for (i = 0; i < n_children; i++) {
      Children[i].Root = Root;
      if (Children[i].ReadFile (file)) return (3);
   }
   return (DONE);
}

 int PathZone::ReadFile (CXMLIO* pIO)

{
   int i;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PathZone")) {
      Polygon::ReadFile (pIO);
      int n_children;
      xmlNode.Attribute (TYPE_ID (int), "ChildNum", &n_children);
      Children.Create (n_children);
      for (i = 0 ; i < n_children ; i++) {
         Children[i].Root = Root;
         Children[i].ReadFile (pIO);
      }
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

 int PathZone::SplitZone (Arrow &arrow)
 
{
   int e_code = SplitZone (arrow[0],arrow[1]);
   if (e_code) return e_code;
   arrow.SplitZoneID = ID;
   return (DONE);
}

 int PathZone::SplitZone (IPoint2D &s_pos,IPoint2D &e_pos,ILine2D *partition /*=NULL*/)

{
   int i;

   ILine2D d_partition;
   Array1D<Polygon> d_polygons;
   PolygonSplitter splitter (this);
   if (splitter.SplitPolygon (s_pos,e_pos,d_polygons,d_partition)) return (1);
   Children.Create (2);
   for (i = 0 ; i < 2 ; i++) {
      Children[i].Root     = Root;
      Children[i].ID       = ++Root->NumChildren;
      Children[i].Vertices = d_polygons[i].Vertices;
      Children[i].UpdateCenterPosition (   );
   }
   if (partition) (*partition) = d_partition;
   return (DONE);
}

 void PathZone::UpdateAreaMaps (FPoint2D &scale)
 
{
   int i;

   if (Children.Length == 0) {
      if (Vertices.Length > 0) {
         Scale = scale;
         UpdateCenterPosition (   );
      }
   }
   else {
      for (i = 0 ; i < Children.Length ; i++)
         Children[i].UpdateAreaMaps (scale);
   }
}

 int PathZone::WriteFile (FileIO &file)

{
   int i;

   if (Polygon::WriteFile (file)) return (1);
   if (file.Write ((byte*)&Children.Length,1,sizeof(int))) return (2);
   for (i = 0 ; i < Children.Length; i++)
      if (Children[i].WriteFile (file)) return (3);
   return (DONE);
}

 int PathZone::WriteFile (CXMLIO* pIO)
 
{
   int i;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PathZone")) {
      Polygon::WriteFile (pIO);
      xmlNode.Attribute (TYPE_ID (int), "ChildNum", &Children.Length);
      for (i = 0 ; i < Children.Length; i++) Children[i].WriteFile (pIO);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: PolygonSplitter
//
///////////////////////////////////////////////////////////////////////////////

 PolygonSplitter::PolygonSplitter (Polygon *s_polygon)

{
   Vertices = s_polygon->Vertices;
}

 int PolygonSplitter::CheckPointIsOnSegment (FPoint2D &pos,Segment &segment)
 
{
   FPoint2D r_pos = (pos - segment[0])/(segment[1] - segment[0]);
   if ((0.0f <= r_pos.X && r_pos.X <= 1.0f) || (0.0f <= r_pos.Y && r_pos.Y <= 1.0f))
      return (DONE);
   else
      return (1);
}

 int PolygonSplitter::GetEdges (LinkedList<Edge> &d_edges)

{
   int length = Vertices.Length;
   if (length < 3) return (1);

   int i;
   for (i = 0 ; i < length; i++) {
      Edge *edge   = new Edge;
      (*edge)[0].X = (float)Vertices[i].X;
      (*edge)[0].Y = (float)Vertices[i].Y;
      if (i == length - 1) {
         (*edge)[1].X = (float)Vertices[0].X;
         (*edge)[1].Y = (float)Vertices[0].Y;
      }
      else {
         (*edge)[1].X = (float)Vertices[i+1].X;
         (*edge)[1].Y = (float)Vertices[i+1].Y;
      }
      d_edges.Add (edge);
   }
   return (DONE);
}

 int PolygonSplitter::GetIntersectionPoints (IPoint2D &s_pos,IPoint2D &e_pos,LinkedList<Edge> &edges,FP2DArray1D &d_ips)
 
{
   if (s_pos == e_pos) return (1);
   if (!IsPointInsidePolygon (s_pos) || !IsPointInsidePolygon (e_pos))
      return (2);

   Matrix vec_a1(2), vec_a2(2);
   vec_a1(0) = s_pos.X;
   vec_a1(1) = s_pos.Y;
   vec_a2(0) = e_pos.X;
   vec_a2(1) = e_pos.Y;
   
   Matrix vec_ac  = (vec_a1 + vec_a2) * double (0.5);
   Matrix vec_ac2 = vec_a2 - vec_ac;
   Matrix vec_e1 = Nor(vec_ac2);
   Matrix vec_x1(3),vec_x2(3);
   vec_x1.Clear();
   vec_x2.Clear();
   vec_x1(0) = vec_e1(0);
   vec_x1(1) = vec_e1(1);
   vec_x2(2) = 1.0;
   Matrix vec_x3 = CP(vec_x1,vec_x2);
   Matrix vec_e2(2);
   vec_e2(0) = vec_x3(0);
   vec_e2(1) = vec_x3(1);

   int n_edges = edges.GetNumItems (   );
   FP2DArray1D s_ips;
   s_ips.Create (n_edges);

   int i, n_ips = 0;
   Matrix vec_p1(2), vec_p2(2);
   Edge *edge = edges.First (   );
   while (edge) {
      vec_p1(0) = (*edge)[0].X;
      vec_p1(1) = (*edge)[0].Y;
      vec_p2(0) = (*edge)[1].X;
      vec_p2(1) = (*edge)[1].Y;

      Matrix vec_p12 = vec_p2 - vec_p1;
      double t = (vec_e2(0) * (vec_p1(1) - vec_ac(1)) - vec_e2(1) * (vec_p1(0) - vec_ac(0)))/(vec_e2(1) * vec_p12(0) - vec_e2(0) * vec_p12(1));
      if (t >= 0.0 && t <= 1.0) {
         Matrix vec_ip = vec_p1 + t * vec_p12;
         FPoint2D ip((float)vec_ip(0),(float)vec_ip(1));
         edge->IP = s_ips[n_ips++] = ip;
      }
      edge = edges.Next (edge);
   }
   d_ips.Create (n_ips);
   for (i = 0 ; i < n_ips ; i++) d_ips[i] = s_ips[i];
   return (DONE);
}

 int PolygonSplitter::GetIntersectionSegments (LinkedList<Edge> &edges,FP2DArray1D &ips,LinkedList<Segment> &d_segments)
 
{
   int n_ips = ips.Length;
   if (n_ips < 2) return (1);
   else if (n_ips == 2) {
      Segment *segment = new Segment;
      (*segment)[0] = ips[0];
      (*segment)[1] = ips[1];
      d_segments.Add (segment);
   }
   else {
      int i;
      for (i = 0 ; i < n_ips ; i++) {
         Segment *segment = new Segment;
         (*segment)[0] = ips[i];
         if (i == n_ips - 1) (*segment)[1] = ips[0];
         else                (*segment)[1] = ips[i+1];

         FPoint2D c_pos = segment->GetCenterPosition (   );
         if (IsPointInsidePolygon (c_pos) && GetNumberOfEdgesIntersectedBySegment (edges,*segment) <= 2)
            d_segments.Add (segment);
         else
            delete segment;
      }
      if (d_segments.GetNumItems (   ) == 0) return (2);
   }
   return (DONE);
}

 int PolygonSplitter::GetNumberOfEdgesIntersectedBySegment (LinkedList<Edge> &edges,Segment &segment)
 
{
   int n_edges = 0;
   Edge *edge = edges.First (   );
   while (edge) {
      if (edge->IP.X != -1.0f && CheckPointIsOnSegment (edge->IP,segment) == DONE) 
         n_edges++;
      edge = edges.Next (edge);
   }
   return n_edges;
}

 int PolygonSplitter::GetSplitPolygons (LinkedList<Edge> &edges,Segment &segment,Array1D<Polygon> &d_polygons)

{
   int n = edges.GetNumItems (   );
   FP2DArray1D verts[2];
   verts[0].Create(n+2);
   verts[1].Create(n+2);

   int dn[2];
   dn[0] = dn[1] = 0;
   int side = 0;
   Edge *edge = edges.First (   );
   while (edge) {
      if (edge->IP.X != -1.0f) {
         if (edge->IP == segment[0] || edge->IP == segment[1]) {
            verts[0][dn[0]++] = verts[1][dn[1]++] = edge->IP;
            side = !side;
         }
      }
      verts[side][dn[side]++] = (*edge)[1];
      edge = edges.Next (edge);
   }
   if (dn[0] < 3 || dn[1] < 3) 
      return (1);

   int i, j;
   d_polygons.Create (2);
   for (i = 0 ; i < 2; i++) {
      d_polygons[i].Vertices.Create(dn[i]);
      for (j = 0 ; j < dn[i] ; j++) {
         d_polygons[i].Vertices[j].X = (int)verts[i][j].X;
         d_polygons[i].Vertices[j].Y = (int)verts[i][j].Y;
      }
   }
   return (DONE);
}

 int PolygonSplitter::SplitPolygon (IPoint2D &s_pos,IPoint2D &e_pos,Array1D<Polygon> &d_polygons,ILine2D &d_partition)

{
   LinkedList<Edge> edges;
   if (GetEdges (edges)) 
      return (1);

   FP2DArray1D ips;
   if (GetIntersectionPoints (s_pos,e_pos,edges,ips)) 
      return (2);

   LinkedList<Segment> segments;
   if (GetIntersectionSegments (edges,ips,segments)) 
      return (3);

   Segment *segment = segments.First (   );
   while (segment) {
      Array1D<Polygon> t_polygons;
      if (GetSplitPolygons (edges,*segment,t_polygons) == DONE) {
         int s_side = t_polygons[1].IsPointInsidePolygon (s_pos);
         int e_side = t_polygons[1].IsPointInsidePolygon (e_pos);
         if (s_side != e_side) {
            d_polygons.Create (2);
            d_polygons[0].Vertices = t_polygons[s_side].Vertices;
            d_polygons[1].Vertices = t_polygons[e_side].Vertices;

            d_partition[0].X = (int)(*segment)[0].X;
            d_partition[0].Y = (int)(*segment)[0].Y;
            d_partition[1].X = (int)(*segment)[1].X;
            d_partition[1].Y = (int)(*segment)[1].Y;
            return (DONE);
         }
      }
      segment = segments.Next (segment);
   }
   return (4);
}

}

#if !defined(__VACL_SVM_H)
#define __VACL_SVM_H

#include "vacl_define.h"

 namespace VACL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Class: LinearSVM
//
///////////////////////////////////////////////////////////////////////////////

 class LinearSVM
 
{
   public:
      int      Dimension;
      float    Bias;
      FArray1D Weights;

   public:
      LinearSVM (   );
   
   protected:
      void _Init (   );
   
   public:
      void  Close         (   );
      float Predict       (float* s_array);
      void  Initialize    (int dimension);
      int   IsInitialized (   );
      int   ReadFile      (const char* file_name);
      int   WriteFile     (const char* file_name);
};

}

#endif

#include "vacl_ipp.h"
#include "vacl_warping.h"

#if defined(__LIB_IPP) && defined(__OS_WINDOWS)
   #pragma comment(lib,"ippccmt.lib")
   #pragma comment(lib,"ippcoremt.lib")
   #pragma comment(lib,"ippimt.lib")
   #pragma comment(lib,"ippsmt.lib")
   #pragma comment(lib,"ippvmmt.lib")
#endif

 namespace VACL

{

#if defined(__LIB_IPP)

 void IPP_ConvertBGRToYUY2 (BGRImage& s_image,byte* di_buffer)
 
{
   IppiSize src_size;
   src_size.width  = s_image.Width;
   src_size.height = s_image.Height;
   ippiBGRToYCbCr422_8u_C3C2R (
      (byte*)(BGRPixel*)s_image, // pSrc
      s_image.GetLineLength(),   // srcStep
      di_buffer,                  // pDst
      src_size.width * 2,        // dstStep
      src_size                   // roiSize
   );
}

 void IPP_ConvertNV12ToBGR (byte* si_buffer,BGRImage& d_image)

{
   IppiSize roiSize;
   roiSize.width  = d_image.Width;
   roiSize.height = d_image.Height;
   int n = d_image.Width * d_image.Height;
   byte* pSrcY       = si_buffer;
   int   srcYStep    = d_image.Width;
   byte* pSrcCbCr    = si_buffer + n;
   int   srcCbCrStep = d_image.Width;
   BArray1D buffer_Y (n);
   BArray1D buffer_Cb(n / 4);
   BArray1D buffer_Cr(n / 4);
   byte* pDst1[3];
   pDst1[0] = buffer_Y;
   pDst1[1] = buffer_Cb;
   pDst1[2] = buffer_Cr;
   int dstStep1[3];
   dstStep1[0] = d_image.Width;
   dstStep1[1] = d_image.Width / 2;
   dstStep1[2] = d_image.Width / 2;
   ippiYCbCr420_8u_P2P3R (pSrcY,srcYStep,pSrcCbCr,srcCbCrStep,pDst1,dstStep1,roiSize);
   byte* pDst2  = (byte*)(BGRPixel*)d_image;
   int dstStep2 = d_image.GetLineLength (   );
   ippiYCbCr420ToBGR_8u_P3C3R ((const Ipp8u**)pDst1,dstStep1,pDst2,dstStep2,roiSize);
}

 void IPP_ConvertYUY2ToBGR (byte* si_buffer,BGRImage& d_image)

{
   IppiSize src_size;
   src_size.width  = d_image.Width;
   src_size.height = d_image.Height;
   ippiYCbCr422ToBGR_8u_C2C3R (
      si_buffer,                 // pSrc
      src_size.width * 2,        // srcStep
      (byte*)(BGRPixel*)d_image, // pDst
      d_image.GetLineLength(),   // dstStep
      src_size                   // roiSize
   );
}

 void IPP_ExtractAndResize_YUY2 (byte* si_buffer,ISize2D& si_size,IBox2D& e_rgn,byte* di_buffer,ISize2D& di_size)
// [주의] X 값과 Width 값은 짝수여야 한다.
{
   si_buffer += 2 * (e_rgn.Y * si_size.Width + e_rgn.X);
   IppiSize src_size;
   src_size.width  = e_rgn.Width;
   src_size.height = e_rgn.Height;
   IppiSize dst_size;
   dst_size.width  = di_size.Width;
   dst_size.height = di_size.Height;
   int spec_size = 0;
   int init_buf_size = 0;
   ippiResizeYUV422GetSize (src_size,dst_size,ippLinear,0,&spec_size,&init_buf_size);
   IppiResizeYUV422Spec* spec = (IppiResizeYUV422Spec*)ippsMalloc_8u (spec_size);
   ippiResizeYUV422LinearInit (src_size,dst_size,spec);
   int buf_size = 0;
   ippiResizeYUV422GetBufSize (spec,dst_size,&buf_size);
   Ipp8u* buffer = ippsMalloc_8u (buf_size);
   IppiPoint dst_offset;
   dst_offset.x = dst_offset.y = 0;
   ippiResizeYUV422Linear_8u_C2R (
      si_buffer,         // pSrc
      si_size.Width * 2, // srcStep 
      di_buffer,         // pDst
      di_size.Width * 2, // dstStep
      dst_offset,        // dstOffset
      dst_size,          // dstSize
      ippBorderRepl,     // border
      0,                 // pBorderValue
      spec,              // pSpec
      buffer             // pBuffer
   );
   ippsFree (spec);
   ippsFree (buffer);
}

 void IPP_Resize (GImage& s_image,GImage& d_image)

{
   IppiSize src_size;
   src_size.width  = s_image.Width;
   src_size.height = s_image.Height;
   IppiSize dst_size;
   dst_size.width  = d_image.Width;
   dst_size.height = d_image.Height;
   int spec_size = 0;
   int init_buf_size = 0;
   ippiResizeGetSize_8u (src_size,dst_size,ippLinear,0,&spec_size,&init_buf_size);
   IppiResizeSpec_32f* spec = (IppiResizeSpec_32f*)ippsMalloc_8u (spec_size);
   ippiResizeLinearInit_8u (src_size,dst_size,spec);
   int buf_size = 0;
   ippiResizeGetBufferSize_8u (spec,dst_size,1,&buf_size);
   Ipp8u* buffer = ippsMalloc_8u (buf_size);
   IppiPoint dst_offset;
   dst_offset.x = dst_offset.y = 0;
   ippiResizeLinear_8u_C1R (
      (byte*)s_image,          // pSrc
      s_image.GetLineLength(), // srcStep 
      (byte*)d_image,          // pDst
      d_image.GetLineLength(), // dstStep
      dst_offset,              // dstOffset
      dst_size,                // dstSize
      ippBorderRepl,           // border
      0,                       // pBorderValue
      spec,                    // pSpec
      buffer                   // pBuffer
   );
   ippsFree (spec);
   ippsFree (buffer);
}

 void IPP_Resize (BGRImage& s_image,BGRImage& d_image)

{
   IppiSize src_size;
   src_size.width  = s_image.Width;
   src_size.height = s_image.Height;
   IppiSize dst_size;
   dst_size.width  = d_image.Width;
   dst_size.height = d_image.Height;
   int spec_size = 0;
   int init_buf_size = 0;
   ippiResizeGetSize_8u (src_size,dst_size,ippLinear,0,&spec_size,&init_buf_size);
   IppiResizeSpec_32f* spec = (IppiResizeSpec_32f*)ippsMalloc_8u (spec_size);
   ippiResizeLinearInit_8u (src_size,dst_size,spec);
   int buf_size = 0;
   ippiResizeGetBufferSize_8u (spec,dst_size,3,&buf_size);
   Ipp8u* buffer = ippsMalloc_8u (buf_size);
   IppiPoint dst_offset;
   dst_offset.x = dst_offset.y = 0;
   ippiResizeLinear_8u_C3R (
      (byte*)(BGRPixel*)s_image, // pSrc
      s_image.GetLineLength(),   // srcStep 
      (byte*)(BGRPixel*)d_image, // pDst
      d_image.GetLineLength(),   // dstStep
      dst_offset,                // dstOffset
      dst_size,                  // dstSize
      ippBorderRepl,             // border
      0,                         // pBorderValue
      spec,                      // pSpec
      buffer                     // pBuffer
   );
   ippsFree (spec);
   ippsFree (buffer);
}

 void IPP_Resize_YUY2 (byte* si_buffer,ISize2D& si_size,byte* di_buffer,ISize2D& di_size)

{
   IppiSize src_size;
   src_size.width  = si_size.Width;
   src_size.height = si_size.Height;
   IppiSize dst_size;
   dst_size.width  = di_size.Width;
   dst_size.height = di_size.Height;
   int spec_size = 0;
   int init_buf_size = 0;
   ippiResizeYUV422GetSize (src_size,dst_size,ippLinear,0,&spec_size,&init_buf_size);
   IppiResizeYUV422Spec* spec = (IppiResizeYUV422Spec*)ippsMalloc_8u (spec_size);
   ippiResizeYUV422LinearInit (src_size,dst_size,spec);
   int buf_size = 0;
   ippiResizeYUV422GetBufSize (spec,dst_size,&buf_size);
   Ipp8u* buffer = ippsMalloc_8u (buf_size);
   IppiPoint dst_offset;
   dst_offset.x = dst_offset.y = 0;
   ippiResizeYUV422Linear_8u_C2R (
      si_buffer,         // pSrc
      si_size.Width * 2, // srcStep 
      di_buffer,         // pDst
      di_size.Width * 2, // dstStep
      dst_offset,        // dstOffset
      dst_size,          // dstSize
      ippBorderRepl,     // border
      0,                 // pBorderValue
      spec,              // pSpec
      buffer             // pBuffer
   );
   ippsFree (spec);
   ippsFree (buffer);
}

 void IPP_WarpAffine (GImage& s_image,Matrix& mat_H,GImage& d_image)
 
{
   int i,j;
   double coeffs[2][3];
   
   IppiSize src_size;
   src_size.width  = s_image.Width;
   src_size.height = s_image.Height;
   IppiSize dst_size;
   dst_size.width  = d_image.Width;
   dst_size.height = d_image.Height;
   for (i = 0; i < 2; i++)
      for (j = 0; j < 3; j++)
         coeffs[i][j] = mat_H[i][j];
   int spec_size = 0;
   int init_buf_size = 0;
   ippiWarpAffineGetSize (src_size,dst_size,ipp8u,coeffs,ippLinear,ippWarpForward,ippBorderTransp,&spec_size,&init_buf_size);
   IppiWarpSpec* spec = (IppiWarpSpec*)ippsMalloc_8u (spec_size);
   ippiWarpAffineLinearInit (src_size,dst_size,ipp8u,coeffs,ippWarpForward,1,ippBorderTransp,0,1,spec);
   int buf_size = 0;
   ippiWarpGetBufferSize (spec,dst_size,&buf_size);
   Ipp8u* buffer = ippsMalloc_8u (buf_size);
   IppiPoint dst_roi_offset;
   dst_roi_offset.x = 0;
   dst_roi_offset.y = 0;
   IppiSize dst_roi_size;
   dst_roi_size.width  = d_image.Width;
   dst_roi_size.height = d_image.Height;
   ippiWarpAffineLinear_8u_C1R (
      (byte*)s_image,          // pSrc
      s_image.GetLineLength(), // srcStep
      (byte*)d_image,          // pDst
      d_image.GetLineLength(), // dstStep
      dst_roi_offset,          // dstRoiOffset
      dst_roi_size,            // dstRoiSize
      spec,                    // pSpec
      buffer                   // pBuffer
   );
   ippsFree (spec);
   ippsFree (buffer);
}

 void IPP_WarpAffine (BGRImage& s_image,Matrix& mat_H,BGRImage& d_image)
 
{
   int i,j;
   double coeffs[2][3];
   
   IppiSize src_size;
   src_size.width  = s_image.Width;
   src_size.height = s_image.Height;
   IppiSize dst_size;
   dst_size.width  = d_image.Width;
   dst_size.height = d_image.Height;
   for (i = 0; i < 2; i++)
      for (j = 0; j < 3; j++)
         coeffs[i][j] = mat_H[i][j];
   int spec_size = 0;
   int init_buf_size = 0;
   ippiWarpAffineGetSize (src_size,dst_size,ipp8u,coeffs,ippLinear,ippWarpForward,ippBorderTransp,&spec_size,&init_buf_size);
   IppiWarpSpec* spec = (IppiWarpSpec*)ippsMalloc_8u (spec_size);
   ippiWarpAffineLinearInit (src_size,dst_size,ipp8u,coeffs,ippWarpForward,3,ippBorderTransp,0,1,spec);
   int buf_size = 0;
   ippiWarpGetBufferSize (spec,dst_size,&buf_size);
   Ipp8u* buffer = ippsMalloc_8u (buf_size);
   IppiPoint dst_roi_offset;
   dst_roi_offset.x = 0;
   dst_roi_offset.y = 0;
   IppiSize dst_roi_size;
   dst_roi_size.width  = d_image.Width;
   dst_roi_size.height = d_image.Height;
   ippiWarpAffineLinear_8u_C3R (
      (byte*)(BGRPixel*)s_image, // pSrc
      s_image.GetLineLength(),   // srcStep
      (byte*)(BGRPixel*)d_image, // pDst
      d_image.GetLineLength(),   // dstStep
      dst_roi_offset,            // dstRoiOffset
      dst_roi_size,              // dstRoiSize
      spec,                      // pSpec
      buffer                     // pBuffer
   );
   ippsFree (spec);
   ippsFree (buffer);
}

 void IPP_WarpPerspective (GImage& s_image,Matrix& mat_H,GImage& d_image)
 
{
   int i,j;
   double coeffs[3][3];
   
   IppiSize src_size;
   src_size.width  = s_image.Width;
   src_size.height = s_image.Height;
   IppiRect src_rect;
   src_rect.x = 0;
   src_rect.y = 0;
   src_rect.width  = s_image.Width;
   src_rect.height = s_image.Height;
   IppiSize dst_size;
   dst_size.width  = d_image.Width;
   dst_size.height = d_image.Height;
   for (i = 0; i < 3; i++)
      for (j = 0; j < 3; j++)
         coeffs[i][j] = mat_H[i][j];
   int spec_size = 0;
   int init_buf_size = 0;
   ippiWarpPerspectiveGetSize (src_size,src_rect,dst_size,ipp8u,coeffs,ippLinear,ippWarpForward,ippBorderTransp,&spec_size,&init_buf_size);
   IppiWarpSpec* spec = (IppiWarpSpec*)ippsMalloc_8u (spec_size);
   ippiWarpPerspectiveLinearInit (src_size,src_rect,dst_size,ipp8u,coeffs,ippWarpForward,1,ippBorderTransp,0,1,spec);
   int buf_size = 0;
   ippiWarpGetBufferSize (spec,dst_size,&buf_size);
   Ipp8u* buffer = ippsMalloc_8u (buf_size);
   IppiPoint dst_roi_offset;
   dst_roi_offset.x = 0;
   dst_roi_offset.y = 0;
   IppiSize dst_roi_size;
   dst_roi_size.width  = d_image.Width;
   dst_roi_size.height = d_image.Height;
   ippiWarpPerspectiveLinear_8u_C1R (
      (byte*)s_image,          // pSrc
      s_image.GetLineLength(), // srcStep
      (byte*)d_image,          // pDst
      d_image.GetLineLength(), // dstStep
      dst_roi_offset,          // dstRoiOffset
      dst_roi_size,            // dstRoiSize
      spec,                    // pSpec
      buffer                   // pBuffer
   );
   ippsFree (spec);
   ippsFree (buffer);
}

 void IPP_WarpPerspective (BGRImage& s_image,Matrix& mat_H,BGRImage& d_image)
 
{
   int i,j;
   double coeffs[3][3];
   
   IppiSize src_size;
   src_size.width  = s_image.Width;
   src_size.height = s_image.Height;
   IppiRect src_rect;
   src_rect.x = 0;
   src_rect.y = 0;
   src_rect.width  = s_image.Width;
   src_rect.height = s_image.Height;
   IppiSize dst_size;
   dst_size.width  = d_image.Width;
   dst_size.height = d_image.Height;
   for (i = 0; i < 3; i++)
      for (j = 0; j < 3; j++)
         coeffs[i][j] = mat_H[i][j];
   int spec_size = 0;
   int init_buf_size = 0;
   ippiWarpPerspectiveGetSize (src_size,src_rect,dst_size,ipp8u,coeffs,ippLinear,ippWarpForward,ippBorderTransp,&spec_size,&init_buf_size);
   IppiWarpSpec* spec = (IppiWarpSpec*)ippsMalloc_8u (spec_size);
   ippiWarpPerspectiveLinearInit (src_size,src_rect,dst_size,ipp8u,coeffs,ippWarpForward,3,ippBorderTransp,0,1,spec);
   int buf_size = 0;
   ippiWarpGetBufferSize (spec,dst_size,&buf_size);
   Ipp8u* buffer = ippsMalloc_8u (buf_size);
   IppiPoint dst_roi_offset;
   dst_roi_offset.x = 0;
   dst_roi_offset.y = 0;
   IppiSize dst_roi_size;
   dst_roi_size.width  = d_image.Width;
   dst_roi_size.height = d_image.Height;
   ippiWarpPerspectiveLinear_8u_C1R (
      (byte*)(BGRPixel*)s_image, // pSrc
      s_image.GetLineLength(),   // srcStep
      (byte*)(BGRPixel*)d_image, // pDst
      d_image.GetLineLength(),   // dstStep
      dst_roi_offset,            // dstRoiOffset
      dst_roi_size,              // dstRoiSize
      spec,                      // pSpec
      buffer                     // pBuffer
   );
   ippsFree (spec);
   ippsFree (buffer);
}

 void IPP_Rotate (BGRImage &s_image,double angle,BGRImage &d_image)

{ 
   IppStatus status = ippStsNoErr;
   IppiSize srcSize = { s_image.Width, s_image.Height };
   IppiSize dstSize = { d_image.Width, d_image.Height };

   double xCenter = s_image.Width *0.5;
   double yCenter = s_image.Height*0.5;

   IppiWarpSpec* pSpec = 0;
   double coeffs[2][3];
   int specSize = 0, initSize = 0, bufSize = 0;
   Ipp8u* pBuffer = 0;
   IppiPoint dstOffset = { 0, 0 };
   ippiGetRotateTransform(angle, 0, 0, coeffs);
   coeffs[0][2] = xCenter - coeffs[0][0] * xCenter - coeffs[0][1] * yCenter;
   coeffs[1][2] = yCenter - coeffs[1][0] * xCenter - coeffs[1][1] * yCenter;
   // Spec and init buffer sizes 
   status = ippiWarpAffineGetSize(srcSize, dstSize, ipp8u, coeffs, ippLinear, ippWarpForward, ippBorderTransp, &specSize, &initSize);
   pSpec = (IppiWarpSpec*)ippsMalloc_8u(specSize);
   // Filter initialization 
   ippiWarpAffineLinearInit(srcSize, dstSize, ipp8u, coeffs, ippWarpForward, 3, ippBorderTransp, NULL, 0, pSpec);
   // work buffer size
   status = ippiWarpGetBufferSize(pSpec, dstSize, &bufSize);
   pBuffer = ippsMalloc_8u(bufSize);
   status = ippiWarpAffineLinear_8u_C3R((byte*)(BGRPixel*)s_image, s_image.Width*3, (byte*)(BGRPixel*)d_image, d_image.Width*3, dstOffset, dstSize, pSpec, pBuffer);
   ippsFree(pSpec);
   ippsFree(pBuffer);
}

#endif

///////////////////////////////////////////////////////////////////////////////

 void InitIPP (   )

{
   #if defined(__LIB_IPP)
   ippInit (   );
   #endif
}

 void ConvertYUY2ToBGR_2 (byte* si_buffer,BGRImage& d_cimage)
{
   #if defined(__LIB_IPP)
   IPP_ConvertYUY2ToBGR (si_buffer,d_cimage);
   #else
   ConvertYUY2ToBGR (si_buffer,d_cimage);
   #endif
}

 void ConvertBGRToYUY2_2 (BGRImage& s_cimage,byte* di_buffer)

{
   #if defined(__LIB_IPP)
   IPP_ConvertBGRToYUY2 (s_cimage,di_buffer);
   #else
   ConvertBGRToYUY2 (s_cimage,di_buffer);
   #endif
}

 void ExtractAndResize_YUY2_2 (byte* si_buffer,ISize2D& si_size,IBox2D& e_rgn,byte* di_buffer,ISize2D& di_size)
// [주의] X 값과 Width 값은 짝수여야 한다.
{
   #if defined(__LIB_IPP)
   IPP_ExtractAndResize_YUY2 (si_buffer,si_size,e_rgn,di_buffer,di_size);
   #else
   ExtractAndResize_YUY2 (si_buffer,si_size,e_rgn,di_buffer,di_size);
   #endif
}

 void Resize_2 (GImage& s_image,GImage& d_image,int flag_hq_reduction)

{
   if (s_image.Width == d_image.Width && s_image.Height == d_image.Height) d_image = s_image;
   else if (s_image.Width > d_image.Width && s_image.Height > d_image.Height) {
      if (flag_hq_reduction) Reduce (s_image,d_image);
      else {
         #if defined(__LIB_IPP)
         IPP_Resize (s_image,d_image);
         #else
         Resize (s_image,d_image);
         #endif
      }
   }
   else {
      #if defined(__LIB_IPP)
      IPP_Resize (s_image,d_image);
      #else
      Resize (s_image,d_image);
      #endif
   }
}

 void Resize_2 (BGRImage& s_image,BGRImage& d_image,int flag_hq_reduction)

{
   if (s_image.Width == d_image.Width && s_image.Height == d_image.Height) d_image = s_image;
   else if (s_image.Width > d_image.Width && s_image.Height > d_image.Height) {
      if (flag_hq_reduction) Reduce (s_image,d_image);
      else {
         #if defined(__LIB_IPP)
         IPP_Resize (s_image,d_image);
         #else
         Resize (s_image,d_image);
         #endif
      }
   }
   else {
      #if defined(__LIB_IPP)
      IPP_Resize (s_image,d_image);
      #else
      Resize (s_image,d_image);
      #endif
   }
}

 void Resize_YUY2_2 (byte* si_buffer,ISize2D& si_size,byte* di_buffer,ISize2D& di_size)

{
   if (si_size.Width == di_size.Width && si_size.Height == di_size.Height) {
      memcpy (di_buffer,si_buffer,si_size.Width * si_size.Height * 2);
   }
   else {
      #if defined(__LIB_IPP)
      IPP_Resize_YUY2 (si_buffer,si_size,di_buffer,di_size);
      #else
      //Resize_YUY2 (si_buffer,si_size,di_buffer,di_size);
      Resize_YUY2 (si_buffer,si_size.Width,si_size.Height,di_buffer,di_size.Width,di_size.Height);
      #endif
   }
}

 void Warp_2 (GImage& s_image,Matrix& mat_H,GImage& d_image)

{
   #if defined(__LIB_IPP)
   IPP_WarpPerspective (s_image,mat_H,d_image);
   #else
   Warp (s_image,mat_H,d_image,FALSE);
   #endif
}

 void Warp_2 (BGRImage& s_cimage,Matrix& mat_H,BGRImage& d_cimage)

{
   #if defined(__LIB_IPP)
   IPP_WarpPerspective (s_cimage,mat_H,d_cimage);
   #else
   Warp (s_cimage,mat_H,d_cimage,FALSE);
   #endif
}

}

#pragma once


#include <vector>

#include <opencv2/opencv.hpp>

#ifdef _MSC_VER
#ifdef CAFFE_CLASSIFIER_DLL_EXPORTS
#define CLASSIFIER_API extern "C" __declspec(dllexport)
#else
#define CLASSIFIER_API extern "C" __declspec(dllimport)
#endif
#else
#define CLASSIFIER_API
#endif

/* Pair (label, confidence) representing a prediction. */
typedef std::pair<std::string, float> Prediction;

CLASSIFIER_API void* Initialize(const std::string& model_file,
                                const std::string& trained_file,
                                const std::string& mean_file,
                                const std::string& label_file,
                                const int flag_use_gpu = 1,
                                const int gpu_no = 0);
   
CLASSIFIER_API void Uninitialize(void* hClassifier);

CLASSIFIER_API void Predict(void* hClassifier,
                            const cv::Mat& image,
                            std::vector<float>& output);

CLASSIFIER_API void PredictBatch(void* hClassifier,
                                 const std::vector<cv::Mat>& images,
                                 std::vector<std::vector<float> >& outputs);


CLASSIFIER_API int Classify(void* hClassifier,
                            const cv::Mat& image,
                            std::vector<Prediction>& prediction);

CLASSIFIER_API int ClassifyBatch(void* hClassifier,
                                 const std::vector<cv::Mat>& images,
                                 std::vector<std::vector<Prediction> >& predictions);

CLASSIFIER_API void GetInputSize(void* hClassifier,
                                 int& width,
                                 int& height);

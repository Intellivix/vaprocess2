#include "vacl_conrgn.h"
#include "vacl_feature.h"
#include "vacl_nms.h"

 namespace VACL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Class: FeatureVector
//
///////////////////////////////////////////////////////////////////////////////

 FeatureVector::FeatureVector (   )

{
   Array       = NULL;
   ArrayLength = 0;
}

 FeatureVector::FeatureVector (float* s_array,int length)

{
   Set (s_array,length);
}

 FeatureVector::FeatureVector (const FeatureVector& s_vector)
 
{
   Array       = s_vector;
   ArrayLength = s_vector.Length (   );
} 

 FeatureVector& FeatureVector::operator = (const FeatureVector& s_vector)

{
   Array       = s_vector;
   ArrayLength = s_vector.Length (   );
   return (*this);
}

 void FeatureVector::Set (float* s_array,uint length)

{
   Array       = s_array;
   ArrayLength = length;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: GradientTable
//
///////////////////////////////////////////////////////////////////////////////

 GradientTable::GradientTable (   )
 
{
   NumBins = 0;
}

 void GradientTable::Initialize (int n_bins)
 
{
   int dx,dy;
   float go;   

   Create (511,511);
   NumBins = n_bins;
   int ebi = n_bins - 1;
   float a = n_bins / MC_PI;
   for (dy = -255; dy < 256; dy++) {
      int y   = dy + 255;
      int sdy = dy * dy;
      GTElement* _RESTRICT l_Array = Array[y];
      for (dx = -255; dx < 256; dx++) {
         int x   = dx + 255;
         int sdx = dx * dx;
         l_Array[x].Mag = sqrt ((float)(sdx + sdy));
         if (dx) go = atan ((float)dy / dx);
         else    go = atan ((float)dy / 0.00001f);
         int bi = (int)(a * (go + MC_PI_2));
         if      (bi < 0  ) bi = 0;
         else if (bi > ebi) bi = ebi;
         l_Array[x].BinIdx = (byte)bi;
      }
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: HOGExtraction
//
///////////////////////////////////////////////////////////////////////////////

 HOGExtraction::HOGExtraction (   )
 
{
   GradTable = NULL;
}

 void HOGExtraction::Close (   )
 
{
   GradTable = NULL;
   IntHistogram.Delete (   );
}

 void HOGExtraction::Extract (IBox2D& s_rgn,float* d_array)
 
{
   int i;
   
   int ex = s_rgn.X + s_rgn.Width;
   int ey = s_rgn.Y + s_rgn.Height;
   for (i = 0; i < IntHistogram.Length; i++) {
      float** integral = IntHistogram[i];
      d_array[i] = (integral[s_rgn.Y][s_rgn.X] + integral[ey][ex]) - (integral[s_rgn.Y][ex] + integral[ey][s_rgn.X]);
   }
}

 void HOGExtraction::Initialize (GradientTable& gr_table,GImage& s_image)
 
{
   int i,x,y;

   GradTable = &gr_table;
   int n_bins = gr_table.NumBins;
   Array1D<FArray2D> bins(n_bins);
   for (i = 0; i < n_bins; i++) {
      bins[i].Create (s_image.Width,s_image.Height);
      bins[i].Clear  (   );
   }
   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   Array1D<float*> l_bins(n_bins);
   for (y = 1; y < ey; y++) {
      byte* l_s_image  = s_image[y];
      byte* l_s_image1 = s_image[y - 1];
      byte* l_s_image2 = s_image[y + 1];
      for (i = 0; i < n_bins; i++) l_bins[i] = bins[i][y];
      for (x = 1; x < ex; x++) {
         int dx = (int)l_s_image[x + 1] - l_s_image[x - 1] + 255;
         int dy = (int)l_s_image2[x]    - l_s_image1[x]    + 255;
         GTElement& g = (*GradTable)[dy][dx];
         l_bins[g.BinIdx][x] += g.Mag;
      }
   }
   IntHistogram.Create (n_bins);
   for (i = 0; i < n_bins; i++) {
      IntHistogram[i].Create (s_image.Width + 1,s_image.Height + 1);
      GetIntegral (bins[i],IntHistogram[i]);
   }
}

 int HOGExtraction::IsInitialized (   )
 
{
   if (GradTable == NULL) return (FALSE);
   else return (TRUE);
}

 void HOGExtraction::Normalize (float* s_array,int length)
 
{
   int i;
   
   float m = 0.0f;
   for (i = 0; i < length; i++) m += s_array[i] * s_array[i];
   m = sqrt (m);
   if (m < MC_VSV) return;
   for (i = 0; i < length; i++) s_array[i] /= m;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: HOGCellArray
//
///////////////////////////////////////////////////////////////////////////////

 HOGCellArray::HOGCellArray (   )
 
{
   HOGExtractor = NULL;
}

 void HOGCellArray::Close (   )
 
{
   FArray3D::Delete (   );
   HOGExtractor = NULL;
}

 void HOGCellArray::GetFeatureVector (IBox2D& dw_rect,int sc_size,float* d_array)

{
   int x,y;

   // Width: # of histogram bins
   int hs = Width * sizeof(float);
   int bs = 4 * Width;
   int ex = dw_rect.X + dw_rect.Width  - sc_size;
   int ey = dw_rect.Y + dw_rect.Height - sc_size;
   for (y = dw_rect.Y; y < ey; y += sc_size) {
      int y1 = y + sc_size;
      for (x = dw_rect.X; x < ex; x += sc_size) {
         int x1 = x + sc_size;
         float* t_array = d_array;
         if (sc_size == 1) {
            memcpy (d_array,Array[y][x]  ,hs);
            d_array += Width;
            memcpy (d_array,Array[y][x1] ,hs);
            d_array += Width;
            memcpy (d_array,Array[y1][x] ,hs);
            d_array += Width;
            memcpy (d_array,Array[y1][x1],hs);
            d_array += Width;
         }
         else {
            GetHOG_SuperCell (x,y,sc_size);
            memcpy (d_array,HOGBuffer,hs);
            d_array += Width;
            GetHOG_SuperCell (x1,y,sc_size);
            memcpy (d_array,HOGBuffer,hs);
            d_array += Width;
            GetHOG_SuperCell (x,y1,sc_size);
            memcpy (d_array,HOGBuffer,hs);
            d_array += Width;
            GetHOG_SuperCell (x1,y1,sc_size);
            memcpy (d_array,HOGBuffer,hs);
            d_array += Width;
         }
         HOGExtraction::Normalize (t_array,bs);
      }
   }
}

 int HOGCellArray::GetFeatureVectorSize (int dw_width,int dw_height,int sc_size)
 
{
   return ((dw_width / sc_size - 1) * (dw_height / sc_size - 1) * 4 * Width);
}

 void HOGCellArray::GetHOG_SuperCell (int sx,int sy,int sc_size)
 
{
   int i,x,y;
   
   int ex = sx + sc_size;
   int ey = sy + sc_size;
   HOGBuffer.Clear (   );
   for (y = sy; y < ey; y++) {
      for (x = sx; x < ex; x++) {
         float* s_array = Array[y][x];
         for (i = 0; i < HOGBuffer.Length; i++) HOGBuffer[i] += s_array[i];
      }
   }
}

 int HOGCellArray::Initialize (HOGExtraction& hog_extractor,IPoint2D& s_pos,ISize2D& cell_size,ISize2D& cell_arr_size)
 
{
   int cx,cy;

   if (!hog_extractor.IsInitialized (   )) return (1);
   HOGExtractor  = &hog_extractor;
   StartPos      = s_pos;
   CellSize      = cell_size;
   CellArraySize = cell_arr_size;
   FArray3D::Create (HOGExtractor->GradTable->NumBins,cell_arr_size.Width,cell_arr_size.Height);
   IBox2D s_rgn(0,0,cell_size.Width,cell_size.Height);
   for (cy = 0,s_rgn.Y = s_pos.Y; cy < cell_arr_size.Height; cy++,s_rgn.Y += cell_size.Height)
      for (cx = 0,s_rgn.X = s_pos.X; cx < cell_arr_size.Width; cx++,s_rgn.X += cell_size.Width)
         HOGExtractor->Extract (s_rgn,Array[cy][cx]);
   HOGBuffer.Create (Width);
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 int DetectHeadFeaturePoints (GImage& s_image,GImage& fg_image,IP2DArray1D& d_array)

{
   int i,x,y;

   d_array.Delete (   );
   int   gm_thrsld = 7;     // PARAMETERS
   float goa_dev   = 45.0f; // PARAMETERS
   float min_goa   = -90.0f - goa_dev;
   float max_goa   = -90.0f + goa_dev;
   min_goa *= MC_DEG2RAD;
   max_goa *= MC_DEG2RAD;
   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   IArray2D gm_array(s_image.Width,s_image.Height);
   gm_array.Clear (   );
   GImage t_image(s_image.Width,s_image.Height);
   t_image.Clear  (   );
   for (y = 1; y < ey; y++) {
      byte* l_s_image  = s_image[y];
      byte* l_s_image1 = s_image[y - 1];
      byte* l_s_image2 = s_image[y + 1];
      byte* l_fg_image = fg_image[y];
      int*  l_gm_array = gm_array[y];
      byte* l_t_image  = t_image[y];
      for (x = 1; x < ex; x++) {
         if (l_fg_image[x]) {
            int gx = ((int)l_s_image[x + 1] - l_s_image[x - 1]) / 2;
            int gy = ((int)l_s_image2[x]    - l_s_image1[x]   ) / 2;
            int gm = abs (gx) + abs (gy);
            if (gm >= gm_thrsld) {
               float goa = (float)atan2 ((float)gy,(float)gx);
               if (min_goa <= goa && goa <= max_goa) {
                  l_gm_array[x] = gm;
                  l_t_image[x]  = PG_WHITE;
               }
            }
         }
      }
   }
   IArray2D cr_map(s_image.Width,s_image.Height);
   int n_crs = LabelCRs (t_image,cr_map);
   if (!n_crs) return (1);
   CRArray1D cr_array(n_crs + 1);
   GetCRInfo (cr_map,cr_array);
   FArray2D p_array(s_image.Width,s_image.Height);
   p_array.Clear (   );
   for (i = 1; i <= n_crs; i++) {
      ConnectedRegion& cr = cr_array[i];
      if (5 <= cr.Area && cr.Width <= 50) { // PARAMTERS
         int cx = (int)cr.CX;
         int cy = (int)cr.CY;
         if (fg_image[cy][cx]) {
            int sx = cr.X;
            int sy = cr.Y;
            int ex = sx + cr.Width;
            int ey = sy + cr.Height;
            int gm = 0;
            for (y = sy; y < ey; y++) {
               int* l_cr_map   = cr_map[y];
               int* l_gm_array = gm_array[y];
               for (x = sx; x < ex; x++) {
                  if (l_cr_map[x] == i) gm += l_gm_array[x];
               }
            }
            p_array[cy][cx] = (float)gm;
         }
      }
   }
   GImage p_image(s_image.Width,s_image.Height);
   PerformPointNMS (p_array,19,0.0f,p_image); // PARAMETERS
   IP2DArray1D fp_array(n_crs);
   int n_fps = 0;
   for (y = 0; y < p_image.Height; y++) {
      byte* l_p_image = p_image[y];
      for (x = 0; x < p_image.Width; x++) {
         if (l_p_image[x]) fp_array[n_fps++](x,y);
      }
   }
   if (!n_fps) return (1);
   d_array = fp_array.Extract (0,n_fps);
   return (DONE);
}

 void GetGradientChannelFeatures (GImage& s_image,GradientTable& gr_table,Array1D<FArray2D>& gh_array,FArray2D& gm_array)

{
   int i,x,y;

   gm_array.SetBoundary (1,0.0f);
   for (i = 0; i < gh_array.Length; i++)
      gh_array[i].Clear (  );
   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   for (y = 1; y < ey; y++) {
      byte*  l_s_image  = s_image[y];
      byte*  l_s_image1 = s_image[y - 1];
      byte*  l_s_image2 = s_image[y + 1];
      float* l_gm_array = gm_array[y];
      for (x = 1; x < ex; x++) {
         int dx = (int)l_s_image[x + 1] - l_s_image[x - 1] + 255;
         int dy = (int)l_s_image2[x]    - l_s_image1[x]    + 255;
         GTElement& g = gr_table[dy][dx];
         gh_array[g.BinIdx][y][x] = g.Mag;
         l_gm_array[x] = g.Mag;
      }
   }
}

 void GetIntegral (GImage& s_image,FArray2D& d_array)

{
   int x,y;
   
   byte*  l_s_image = s_image[0];
   float* l_d_array = d_array[1];
   l_d_array[1] = l_s_image[0];
   for (x = 1; x < s_image.Width; x++)
      l_d_array[x + 1] = l_d_array[x] + l_s_image[x];
   for (y = 1; y < s_image.Height; y++) {
      float r = 0.0f;
      l_s_image = s_image[y];
      l_d_array = d_array[y];
      float* l_d_array1 = d_array[y + 1];
      for (x = 0; x < s_image.Width; x++) {
         int x1 = x + 1;
         r += l_s_image[x];
         l_d_array1[x1] = l_d_array[x1] + r;
      }
   }
   d_array.Set (0,0,d_array.Width,1,0.0f);
   d_array.Set (0,0,1,d_array.Height,0.0f);
}

 void GetIntegral (GImage& s_image,IArray2D& d_array)

{
   int x,y;
   
   byte* l_s_image = s_image[0];
   int*  l_d_array = d_array[1];
   l_d_array[1] = l_s_image[0];
   for (x = 1; x < s_image.Width; x++)
      l_d_array[x + 1] = l_d_array[x] + l_s_image[x];
   for (y = 1; y < s_image.Height; y++) {
      int r = 0;
      l_s_image = s_image[y];
      l_d_array = d_array[y];
      int* l_d_array1 = d_array[y + 1];
      for (x = 0; x < s_image.Width; x++) {
         int x1 = x + 1;
         r += l_s_image[x];
         l_d_array1[x1] = l_d_array[x1] + r;
      }
   }
   d_array.Set (0,0,d_array.Width,1,0);
   d_array.Set (0,0,1,d_array.Height,0);
}

 void GetIntegral (FArray2D& s_array,FArray2D& d_array)

{
   int x,y;
   
   float* l_s_array = s_array[0];
   float* l_d_array = d_array[1];
   l_d_array[1] = l_s_array[0];
   for (x = 1; x < s_array.Width; x++)
      l_d_array[x + 1] = l_d_array[x] + l_s_array[x];
   for (y = 1; y < s_array.Height; y++) {
      float r = 0.0f;
      l_s_array = s_array[y];
      l_d_array = d_array[y];
      float* l_d_array1 = d_array[y + 1];
      for (x = 0; x < s_array.Width; x++) {
         int x1 = x + 1;
         r += l_s_array[x];
         l_d_array1[x1] = l_d_array[x1] + r;
      }
   }
   d_array.Set (0,0,d_array.Width,1,0.0f);
   d_array.Set (0,0,1,d_array.Height,0.0f);
}

 void GetIntegral (IArray2D& s_array,IArray2D& d_array)

{
   int x,y;
   
   int* l_s_array = s_array[0];
   int* l_d_array = d_array[1];
   l_d_array[1] = l_s_array[0];
   for (x = 1; x < s_array.Width; x++)
      l_d_array[x + 1] = l_d_array[x] + l_s_array[x];
   for (y = 1; y < s_array.Height; y++) {
      int r = 0;
      l_s_array = s_array[y];
      l_d_array = d_array[y];
      int* l_d_array1 = d_array[y + 1];
      for (x = 0; x < s_array.Width; x++) {
         int x1 = x + 1;
         r += l_s_array[x];
         l_d_array1[x1] = l_d_array[x1] + r;
      }
   }
   d_array.Set (0,0,d_array.Width,1,0);
   d_array.Set (0,0,1,d_array.Height,0);
}

 void GetIntegral (IArray2D& s_array,FArray2D& d_array)

{
   int x,y;
   
   int*   l_s_array = s_array[0];
   float* l_d_array = d_array[1];
   l_d_array[1] = (float)l_s_array[0];
   for (x = 1; x < s_array.Width; x++)
      l_d_array[x + 1] = l_d_array[x] + (float)l_s_array[x];
   for (y = 1; y < s_array.Height; y++) {
      float r = 0.0f;
      l_s_array = s_array[y];
      l_d_array = d_array[y];
      float* l_d_array1 = d_array[y + 1];
      for (x = 0; x < s_array.Width; x++) {
         int x1 = x + 1;
         r += (float)l_s_array[x];
         l_d_array1[x1] = l_d_array[x1] + r;
      }
   }
   d_array.Set (0,0,d_array.Width,1,0.0f);
   d_array.Set (0,0,1,d_array.Height,0.0f);
}

}

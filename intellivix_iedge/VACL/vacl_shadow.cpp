#include "vacl_binimg.h"
#include "vacl_conrgn.h"
#include "vacl_morph.h"
#include "vacl_shadow.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 void PerformShadowSuppression (GImage& s_image,GImage& bg_image,float min_ir,float max_irsd,GImage &fg_image)

{
   int x,y,x2,y2;

   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   float max_irv = max_irsd * max_irsd;
   GImage sh_image(s_image.Width,s_image.Height);
   sh_image.Clear (   );
   // 전경 영역의 바운더리 픽셀들을 구한다.
   for (y = 1; y < ey; y++) {
      byte* l_fg_image  = fg_image[y];
      byte* l_fg_image1 = fg_image[y - 1];
      byte* l_fg_image2 = fg_image[y + 1];
      byte* l_sh_image  = sh_image[y];
      for (x = 1; x < ex; x++) {
         if (l_fg_image[x]) {
            // 바운더리 픽셀인 경우 255로 마킹한다.
            if (!(l_fg_image[x - 1] && l_fg_image[x + 1] && l_fg_image1[x] && l_fg_image2[x])) l_sh_image[x] = 255;
         }
      }
   }
   // 그림자 픽셀들을 검출하여 전경 영역으로부터 제거한다.
   for (y = 1; y < ey; y++) {
      int sy2 = y - 1;
      int ey2 = y + 1;
      byte* l_s_image  = s_image[y];
      byte* l_fg_image = fg_image[y];
      byte* l_sh_image = sh_image[y];
      byte* l_bg_image = bg_image[y];
      for (x = 1; x < ex; x++) {
         if (l_fg_image[x]) {
            float ir = (float)l_s_image[x] / l_bg_image[x];
            if (min_ir <= ir && ir < 1.0f) {
               int sx2 = x - 1;
               int ex2 = x + 1;
               float r = 0.0f, rr = 0.0f;
               for (y2 = sy2; y2 <= ey2; y2++) {
                  byte* l_s_image2  = s_image[y2];
                  byte* l_bg_image2 = bg_image[y2];
                  for (x2 = sx2; x2 <= ex2; x2++) {
                     ir  = (float)l_s_image2[x2] / l_bg_image2[x2];
                     r  += ir;
                     rr += ir * ir;
                  }
               }
               r /= 9.0f;
               float irv = rr / 9.0f - r * r;
               if (irv < max_irv) {
                  l_fg_image[x] = PG_BLACK;
                  // 그림자 픽셀인 경우 128로 마킹한다.
                  l_sh_image[x] = 128;
               }
            }
         }
      }
   }
   // 그림자 영역 바운더리 근처에 남아 있는 그림자 픽셀들을 검출하여 전경 영역으로부터 제거한다.
   ey = s_image.Height - 2;
   ex = s_image.Width  - 2;
   for (y = 2; y < ey; y++) {
      int sy2 = y - 2;
      int ey2 = y + 2;
      byte* l_sh_image1 = sh_image[y];
      for (x = 2; x < ex; x++) {
         // 바운더리 픽셀인 경우
         if (l_sh_image1[x] == 255) {
            int sx2 = x - 2;
            int ex2 = x + 2;
            int flag_shadow = FALSE;
            for (y2 = sy2; y2 <= ey2; y2++) {
               byte* l_sh_image2 = sh_image[y2];
               for (x2 = sx2; x2 <= ex2; x2++) {
                  // 근처에 그림자 픽셀이 있는 경우
                  if (l_sh_image2[x2] == 128) {
                     flag_shadow = TRUE;
                     break;
                  }
               }
               if (flag_shadow) break;
            }
            // 그림자 픽셀들을 제거한다.
            if (flag_shadow) {
               for (y2 = sy2; y2 <= ey2; y2++) {
                  byte* l_s_image  = s_image[y2];
                  byte* l_bg_image = bg_image[y2];
                  byte* l_fg_image = fg_image[y2];
                  for (x2 = sx2; x2 <= ex2; x2++) {
                     if (l_fg_image[x2]) {
                        float ir = (float)l_s_image[x2] / l_bg_image[x2];
                        if (min_ir <= ir && ir < 1.0f) {
                           l_fg_image[x2] = PG_BLACK;
                        }
                     }
                  }
               }
            }
         }
      }
   }
}

}

#include "vacl_objclass.h"
#include "vacl_binimg.h"
#include "vacl_edge.h"
#include "vacl_event.h"
#include "vacl_filter.h"
#include "vacl_geometry.h"
#include "vacl_histo.h"
#include "vacl_ipp.h"
#include "vacl_objtrack.h"
#include "VAEngine.h"

#if defined(__DEBUG_OBT)
#include "Win32CL/Win32CL.h"
extern int _DVX_OBT,_DVY_OBT;
extern CImageView* _DebugView_OBT;
#endif

// #define __CONSOLE

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Global Variables/Functions
//
///////////////////////////////////////////////////////////////////////////////

TrackedObject* (*__CreationFunction_TrackedObject)(   ) = NULL;

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectIDGeneration
//
///////////////////////////////////////////////////////////////////////////////

 ObjectIDGeneration::ObjectIDGeneration (   )

{
   Reset (   );
}

 int ObjectIDGeneration::Generate (   )

{
   return (Count++);
}

 void ObjectIDGeneration::Reset (   )

{
   Count = 1;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: FRArray1D
//
///////////////////////////////////////////////////////////////////////////////

 void FRArray1D::Initialize (CRArray1D& s_array)

{
   int i;
   
   if (!s_array) {
      Delete (   );
      return;
   }
   Create (s_array.Length);
   for (i = 0; i < s_array.Length; i++)
      *(ConnectedRegion*)&Array[i] = s_array[i];
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: TrackedObject
//
///////////////////////////////////////////////////////////////////////////////

#define MAX_TRAJECTORY_LENGTH   100

 TrackedObject::TrackedObject (   )

{
   Flag_FPTEnabled        = FALSE;
   
   ID                     = 0;
   Type                   = TO_TYPE_UNKNOWN;
   Status                 = 0;
   PersonAttrs            = 0;
   VehicleType            = TO_VEHICLE_TYPE_UNKNOWN;
   EventStatus            = 0;
   EventZones             = 0;

   Depth                  = 0;
   NumBndPixels           = 0;
   NumRgnPixels           = 0;
   NumFrgBndEdgePixels    = 0;
   NumBkgBndEdgePixels    = 0;
   TrjLength              = 0;
   TypeHistory            = 0;
   AreaVariation          = 0.0f;
   BndEdgeStrength        = 0.0f;
   Dispersedness          = 0.0f;
   Exposure               = 1.0f;
   MajorAxisAngle1        = 0.0f;
   MajorAxisAngle2        = -1.0f;
   MatchingScore          = 0.0f;
   MaxMovingDistance      = 0.0f;
   MaxScore_BestShot      = 0.0f;
   MeanArea               = 0.0f;
   Noisiness              = 0.0f;
   PathLength             = 0.0f;

   FrameCount             = 0;
   InitFrameCount         = 0;
   Count_Tracked          = 0;
   Count_Abandoned        = 0;
   Count_Removed          = 0;

   Count_POC              = 0;
   Count_PAR              = 0;
   Count_VTR              = 0;
   Result_POC             = -1;

   Time_BestShot          = 0.0f;
   Time_ChangedAbruptly   = 0.0f;
   Time_Lost              = 0.0f;
   Time_Merged            = 0.0f;
   Time_Static            = 0.0f;
   Time_Tracked           = 0.0f;
   Time_TypeUnchanged     = 0.0f;
   Time_Undetected        = 0.0f;
   MaxTime_Static         = 0.0f;

   Time_PAR               = 0.0f;
   Time_POC               = 0.0f;
   Time_VTR               = 0.0f;

   Time_CarAccident       = 0.0f;
   Time_CarStopping       = 0.0f;
   Time_DirecMotion       = 0.0f;
   Time_Dwell             = 0.0f;
   Time_Falling           = 0.0f;
   Time_FrontalFace1      = 0.0f;
   Time_FrontalFace2      = 0.0f;
   Time_Ghost             = 0.0f;
   Time_Loitering         = 0.0f;
   Time_Stopping          = 0.0f;

   Area                   = 0.0f;
   AspectRatio            = 0.0f;
   AspectRatio2           = 0.0f;
   AxisLengthRatio        = 0.0f;
   MajorAxisLength        = 0.0f;
   MinorAxisLength        = 0.0f;
   NorSpeed               = 0.0f;
   Speed                  = 0.0f;
   RealArea               = 0.0f;
   RealDistance           = 0.0f;
   RealWidth              = 0.0f;
   RealHeight             = 0.0f;
   RealMajorAxisLength    = 0.0f;
   RealMinorAxisLength    = 0.0f;
   RealSpeed              = -1.0f;
   
   AvgWidth               = 0;
   AvgHeight              = 0;
   AvgArea                = 0.0f;
   AvgAspectRatio         = 0.0f;
   AvgAxisLengthRatio     = 0.0f;
   AvgMajorAxisLength     = 0.0f;
   AvgMinorAxisLength     = 0.0f;
   AvgNorSpeed            = 0.0f;
   AvgSpeed               = 0.0f;
   AvgRealArea            = 0.0f;
   AvgRealDistance        = 0.0f;
   AvgRealWidth           = 0.0f;
   AvgRealHeight          = 0.0f;
   AvgRealMajorAxisLength = 0.0f;
   AvgRealMinorAxisLength = 0.0f;
   AvgRealSpeed           = 0.0f;
   
   MaxWidth               = 0;
   MaxHeight              = 0;
   MaxArea                = 0.0f;
   MaxAspectRatio         = 0.0f;
   MaxAxisLengthRatio     = 0.0f;
   MaxMajorAxisLength     = 0.0f;
   MaxMinorAxisLength     = 0.0f;
   MaxNorSpeed            = 0.0f;
   MaxSpeed               = 0.0f;
   MaxRealDistance        = 0.0f;
   MaxRealSpeed           = 0.0f;
   
   MinWidth               = 0x7FFFFFFF;
   MinHeight              = 0x7FFFFFFF;
   MinArea                = MC_VLV;
   MinAspectRatio         = MC_VLV;
   MinAxisLengthRatio     = MC_VLV;
   MinMajorAxisLength     = MC_VLV;
   MinMinorAxisLength     = MC_VLV;
   MinNorSpeed            = MC_VLV;
   MinSpeed               = MC_VLV;
   MinRealDistance        = MC_VLV;
   MinRealSpeed           = MC_VLV;

   ThumbPixFmt            = VAE_PIXFMT_NONE;
   ObjRgnID               = 0;
   ObjRgnArea             = 0;
   TrkBox                 = NULL;
   TrkBlob                = NULL;

   GTOID                  = 0;
   
   FPTrackLength          = 0;
   FPFlowVariance         = 0.0f;
   FPFlowActivity         = 0.0f;

   MeanVelocity(MC_VLV,MC_VLV);
   Velocity(MC_VLV,MC_VLV);
   Pos_Static(-1.0f,-1.0f);

   CntBkgColor.Clear (   );
   CntFrgColor.Clear (   );
   RgnBkgColor.Clear (   );
   RgnFrgColor.Clear (   );
   
   GrayHistogram.Create (NUM_GRAY_HISTOGRAM_BINS);
   Trajectory.Create    (MAX_TRAJECTORY_LENGTH);
   memset (OTLikelihood,0,sizeof(OTLikelihood));
   memset (PALikelihood,0,sizeof(PALikelihood));
   memset (VTLikelihood,0,sizeof(VTLikelihood));
}

 void TrackedObject::AddToTrkObjRegionList (int trj_length)

{
   int i;

   if (TrkObjRegions.GetNumItems (   ) > trj_length) {
      TrkObjRegion* to_rgn = TrkObjRegions.Last (   );
      TrkObjRegions.Remove (to_rgn);
      delete to_rgn;
   }
   TrkObjRegion *to_rgn = new TrkObjRegion;
   to_rgn->Set (*this);
   to_rgn->FrameCount     = FrameCount;
   to_rgn->NumPersons     = Persons.Length;
   to_rgn->Status         = Status;
   to_rgn->Area           = Area;
   to_rgn->AspectRatio    = AspectRatio;
   to_rgn->MajorAxisAngle = MajorAxisAngle1;
   to_rgn->NorSpeed       = NorSpeed;
   to_rgn->Speed          = Speed;
   to_rgn->Centroid       = Centroid;
   to_rgn->MeanVelocity   = MeanVelocity;
   to_rgn->Velocity       = Velocity;
   TrkObjRegions.Add (to_rgn,LL_FIRST);
   if ((Count_Tracked % 3) == 1) {
      if (TrjLength >= Trajectory.Length) {
         for (i = 1; i < Trajectory.Length; i++) Trajectory[i - 1] = Trajectory[i];
         Trajectory[Trajectory.Length - 1] = CenterPos;
      }
      else Trajectory[TrjLength++] = CenterPos;
   }
}

 int TrackedObject::CheckTypeAndAttrs (int type,int attrs)

{
   if (Type & type) {
      if (Type == TO_TYPE_HUMAN && attrs) {
         if (PersonAttrs & attrs) return (DONE);
         else return (2);
      }
      else return (DONE);
   }
   else return (1);
}

 void TrackedObject::ClearFPMotionPropertyValues (   )

{
   FPFlowVariance = 0.0f;
   FPFlowActivity = 0.0f;
}

 void TrackedObject::ClearStatus (   )

{
   if (Status & TO_STATUS_FMO_ENDED) {
      if (Status & TO_STATUS_MERGED) Status &= ~TO_STATUS_FMO_ENDED;
      else {
         Status &= ~(TO_STATUS_FAST_MOVING_OBJECT | TO_STATUS_FMO_ENDED);
         Velocity.Clear  (   );
      }
   }
   Status &= ~(
      TO_STATUS_ABANDONED_OBJECT | TO_STATUS_BEST_SHOT      | TO_STATUS_CHANGED_ABRUPTLY | TO_STATUS_GHOST_OBJECT  |
      TO_STATUS_HANDOFF          | TO_STATUS_IN_OUTER_ZONE  | TO_STATUS_MASK_PUT         | TO_STATUS_MATCHED_1TO1  |
      TO_STATUS_MERGED           | TO_STATUS_REMOVED_OBJECT | TO_STATUS_SPLIT            | TO_STATUS_TO_BE_REMOVED |
      TO_STATUS_UNDETECTED       | TO_STATUS_VERIFIED_START
   );
}

 Event *TrackedObject::FindEvent (EventZone *evt_zone,int evt_type)
 
{
   Event *event = Events.First (   );
   while (event != NULL) {
      if (event->EvtZone == evt_zone) {
         if (evt_type < 0) return (event);
         else if (event->Type == evt_type) return (event);
      }
      event = Events.Next (event);
   }
   return (NULL);
}

// jun: 추가, path passing
 TrackedObject::EvtZonePathPassInfo* TrackedObject::FindEventZonePathPassingInfo (EventZone* evt_zone)

{
   EvtZonePathPassInfo* evt_pp_info = EventZonePathPassingInfoList.First (   );
   while (evt_pp_info) {
      if (evt_pp_info->EvtZone == evt_zone)
         return evt_pp_info;
      evt_pp_info = EventZonePathPassingInfoList.Next (evt_pp_info);
   }
   return (0);
}

 Path *TrackedObject::FindPath (EventZone *evt_zone)
 
{
   Path *path = Paths.First (   );
   while (path != NULL) {
      if (path->EvtZone == evt_zone) return (path);
      path = Paths.Next (path);
   }
   return (NULL);
}

 void TrackedObject::GetArea (   )

{
   Area = 0.25f * MC_PI * MajorAxisLength * MinorAxisLength;
}

 int TrackedObject::GetAreaOfSupportedRegion (SArray2D &s_array,short thrsld)

{
   int x1,y1,x2,y2;
   
   int sx1 = X;
   int sy1 = Y;
   int ex1 = sx1 + MaskImage.Width;
   int ey1 = sy1 + MaskImage.Height;
   if (sx1 < 0) sx1 = 0;
   if (sy1 < 0) sy1 = 0;
   if (ex1 > s_array.Width ) ex1 = s_array.Width;
   if (ey1 > s_array.Height) ey1 = s_array.Height;
   int sx2 = sx1 - X;
   int sy2 = sy1 - Y;
   int area = 0;
   for (y1 = sy1,y2 = sy2; y1 < ey1; y1++,y2++) {
      byte*  l_m_image = MaskImage[y2];
      short* l_s_array = s_array[y1];
      for (x1 = sx1,x2 = sx2; x1 < ex1; x1++,x2++) {
         if (l_m_image[x2] && (l_s_array[x1] >= thrsld)) area++;
      }
   }
   return (area);
}

 void TrackedObject::GetAreaVariation (   )
 
{
   TrkObjRegion *to_rgn = TrkObjRegions.First (   );
   if (to_rgn == NULL) {
      AreaVariation = 0.0f;
      return;
   }
   int a1 = Width * Height;
   int a2 = to_rgn->Width * to_rgn->Height;
   int min_a = GetMinimum (a1,a2);
   AreaVariation = (float)abs (a1 - a2) / min_a;
}

 void TrackedObject::GetAspectRatio (   )

{
   AspectRatio = (float)Height / Width;
}

 void TrackedObject::GetAxisLengthRatio (   )

{
   AxisLengthRatio = MajorAxisLength / MinorAxisLength;
}

 float TrackedObject::GetBestShotScore (ISize2D &si_size)
 
{
   // 평균 면적과 현재 추정 면적이 같을수록 좋다.
   float score1 = Area / AvgArea;
   if (score1 > 1.0f) score1 = 1.0f / score1;
   // 현재 추정 면적 내의 감지된 픽셀의 밀도가 높을수록 좋다.
   float score2 = NumRgnPixels / Area;
   if (score2 > 1.0f) score2 = 1.0f / score2;
   // 사람으로 분류된 경우 높이/너비의 비가 온전한 사람 모양에 가까울수록 좋다.
   float score3 = 0.0f;
   if (Type == TO_TYPE_HUMAN) {
      if (2.0f <= AspectRatio2 && AspectRatio2 <= 4.5f) score3 = 1.0f;
   }
   // 차량으로 분류된 경우 장축/단축의 길이 비가 온전한 자동차 모양에 가까울수록 좋다.
   float score4 = 0.0f;
   if (Type == TO_TYPE_VEHICLE) {
      score4 = 2.5f / AxisLengthRatio;
      if (score4 > 1.0f) score4 = 1.0f / score4;
   }
   // 객체가 영상의 가장 자리에 있으면 좋지 않다.
   float score5 = 0.0f;
   int ex = X + Width  - 1;
   int ey = Y + Height - 1;
   if (X < 3 || Y < 3 || ex > si_size.Width - 4 || ey > si_size.Height - 4) score5 = -1.0f;
   // 객체가 카메라와 가까울수록 좋다.
   float cy = 0.7f * si_size.Height;
   float score6 = 1.0f - fabs (CenterPos.Y - cy) / cy;
   // 객체의 크기가 평균 크기보다 클 수록 좋다.
   float score7 = 0.0f;
   if (Area > AvgArea) score7 = 1.0f;
   float score = 1.0f * score1 + 1.3f * score2 + 0.7f * score3 +  1.0f * score4 + 1.0f * score5 + 0.7f * score6 + 1.5f * score7;
   return (score);
}

 void TrackedObject::GetClippedBoundingBox (int si_width,int si_height,IPoint2D& lt_pos,IPoint2D &rb_pos,IPoint2D& c_pos)
 
{
   lt_pos.X = X;
   lt_pos.Y = Y;
   rb_pos.X = lt_pos.X + Width  - 1;
   rb_pos.Y = lt_pos.Y + Height - 1;
   if (lt_pos.X < 0) lt_pos.X = 0;
   if (lt_pos.Y < 0) lt_pos.Y = 0;
   if (rb_pos.X >= si_width ) rb_pos.X = si_width  - 1;
   if (rb_pos.Y >= si_height) rb_pos.Y = si_height - 1;
   c_pos.X = (lt_pos.X + rb_pos.X) / 2;
   c_pos.Y = (lt_pos.Y + rb_pos.Y) / 2;
}

 void TrackedObject::GetColorHistogram (BGRImage &s_image,IArray2D &or_map,GImage &fg_image)

{
   int x,y;
   
   if (!ObjRgnID) return;
   ColorHistogram.Clear (   );
   int sx = ObjRgnBndBox.X;
   int sy = ObjRgnBndBox.Y;
   int ex = sx + ObjRgnBndBox.Width;
   int ey = sy + ObjRgnBndBox.Height;
   if (sx < 1) sx = 1;
   if (sy < 1) sy = 1;
   if (ex >= s_image.Width ) ex = s_image.Width  - 1;
   if (ey >= s_image.Height) ey = s_image.Height - 1;
   #if defined(__DEBUG_OBS)
   int ti_width  = ex - sx;
   int ti_height = ey - sy;
   BGRImage t_image(ti_width * 2,ti_height);
   t_image.Copy (s_image,sx,sy,ti_width,ti_height,0,0);
   t_image.Set  (ti_width,0,ti_width,ti_height,BGRPixel(59,157,165));
   int offset_x = ti_width - sx;
   int offset_y = -sy;
   #endif
   for (y = sy; y < ey; y++) {
      BGRPixel* l_s_image   = s_image [y];
      byte*     l_fg_image  = fg_image[y];
      byte*     l_fg_image1 = fg_image[y - 1];
      byte*     l_fg_image2 = fg_image[y + 1];
      int*      l_or_map    = or_map[y];
      for (x = sx; x < ex; x++) {
         if (l_or_map[x] == ObjRgnID && l_fg_image[x]) {
            if (l_fg_image[x - 1] && l_fg_image[x + 1] && l_fg_image1[x] && l_fg_image2[x]) {
               ColorHistogram.Accumulate (l_s_image[x]);
               #if defined(__DEBUG_OBS)
               HVHistogram::GetRepresentativeColor (l_s_image[x],t_image[offset_y + y][offset_x + x]);
               #endif
            }
         }
      }
   }
   #if defined(__DEBUG_OBS)
   char file_name[50];
   sprintf (file_name,"D:\\ObjectSamples\\Object_ID=%05d.bmp",ID);
   t_image.WriteFile (file_name);
   #endif
}

 void TrackedObject::GetDispersedness (   )

{
   int n_bps = GetRegionPerimeter (MaskImage);
   if (NumRgnPixels) Dispersedness = (float)(n_bps * n_bps) / NumRgnPixels;
   else Dispersedness = MC_VLV;
}

 FPoint2D TrackedObject::GetDisplacement (int n_frames)

{
   FPoint2D dv(0.0f,0.0f);
   TrkObjRegion* to_rgn = TrkObjRegions.Get (n_frames);
   if (to_rgn == NULL) to_rgn = TrkObjRegions.Last (   );
   if (to_rgn != NULL) dv = GetCenterPosition (   ) - to_rgn->GetCenterPosition (   );
   return (dv);
}

 TrackedObject::EvtPrgTime* TrackedObject::GetEventProgressTime (EventZone* evt_zone)
 // [주의] 향 후 하나의 이벤트 존에서 복수의 이벤트 종류를 지원하려면,
 //        이벤트 존만 체크할 것이 아니라 이벤트 종류도 체크해야 한다.
{
   EvtPrgTime* ep_time = EvtPrgTimeList.First (   );
   while (ep_time != NULL) {
      if (ep_time->EvtZone == evt_zone) break;
      ep_time = EvtPrgTimeList.Next (ep_time);
   }
   if (ep_time == NULL) {
      ep_time = new EvtPrgTime;
      ep_time->EvtZone = evt_zone;
      EvtPrgTimeList.Add (ep_time);
   }
   return (ep_time);
}

 int TrackedObject::GetFPMotionProperties1 (float frm_duration)
#if defined(__LIB_OPENCV)
{
   int i,n;
   
   FPFlowVariance = 0.0f;
   if (!(Status & TO_STATUS_FEATURE_POINT_TRACKING)) return (1);
   int n_tracks = (int)FPTracks.size (   );
   int lp = FPTrackLength - 1;
   float sum_smv = 0.0f;
   cv::Point2f sum_v(0.0f,0.0f);
   for (i = n = 0; i < n_tracks; i++) {
      vector<cv::Point2f>& fp_track = FPTracks[i];
      if (FPTrackLength == (int)fp_track.size()) {
         cv::Point2f v = fp_track[lp] - fp_track[lp - 1];
         sum_v   += v;
         sum_smv += v.x * v.x + v.y * v.y;
         n++;
      }
   }
   if (n) {
      float fv = sqrt (sum_smv / n - (sum_v.x * sum_v.x + sum_v.y * sum_v.y) / (n * n));
      float nf = frm_duration * Width / GetMaximum (1,Persons.Length);
      FPFlowVariance = fv / nf;
   }
   return (DONE);
}
#else
{
   return (-1);
}
#endif

 int TrackedObject::GetFPMotionProperties2 (float frm_duration)
#if defined(__LIB_OPENCV)
{
   int i,j,n;
   
   FPFlowActivity = 0.0f;
   if (!(Status & TO_STATUS_FEATURE_POINT_TRACKING)) return (1);
   int n_tracks = (int)FPTracks.size (   );
   if (n_tracks < 2) return (2);
   FArray1D md_array(n_tracks);
   md_array.Clear (   );
   FArray1D sd_array(n_tracks);
   sd_array.Clear (   );
   for (i = 0; i < n_tracks; i++) {
      vector<cv::Point2f>& fp_track = FPTracks[i];
      int tr_len = (int)fp_track.size (   );
      if (tr_len >= 2) {
         float max_d = 0.0f;
         float sum_d = 0.0f;
         cv::Point2f sp = fp_track[0];
         for (j = 1; j < tr_len; j++) {
            cv::Point2f dp = fp_track[j] - sp;
            float d = (float)sqrt (dp.x * dp.x + dp.y * dp.y);
            if (d > max_d) max_d = d;
            dp = fp_track[j] - fp_track[j - 1];
            d = (float)sqrt (dp.x * dp.x + dp.y * dp.y);
            sum_d += d;
         }
         md_array[i] = max_d;
         sd_array[i] = sum_d;
      }
   }
   float md_thrsld = 0.1f * GetMinimum (Width,Height); // PARAMETERS
   float sum_nsd   = 0.0f;
   for (i = n = 0; i < n_tracks; i++) {
      if (md_array[i] >= md_thrsld) {
         sum_nsd += sd_array[i] / md_array[i];
         n++;
      }
   }
   if (n) FPFlowActivity = sum_nsd / n;
   return (DONE);
}
#else
{
   FPFlowActivity = 0.0f;
   return (-1);
}
#endif

 float TrackedObject::GetHumanRectAreaRatio (   )

{
   int   a1 = HumanRect.Width * HumanRect.Height;
   int   a2 = Width * Height;
   float ar = (float)a1 / a2;
   return (ar);
}

 void TrackedObject::GetImagePatch_YUY2 (byte* si_buffer,ISize2D& si_size,FPoint2D& scale,FPoint2D& inflation,BGRImage& d_image)

{
   IBox2D t_rgn = Scale  (scale);
   t_rgn = t_rgn.Inflate (inflation);
   CropRegion (si_size,t_rgn);
   t_rgn.X     = t_rgn.X / 2 * 2;
   t_rgn.Width = t_rgn.Width / 4 * 4;
   ISize2D  di_size(d_image.Width,d_image.Height);
   BArray1D di_buffer(di_size.Width * di_size.Height * 2);
   ExtractAndResize_YUY2_2 (si_buffer,si_size,t_rgn,di_buffer,di_size);
   ConvertYUY2ToBGR_2 (di_buffer,d_image);
}

 void TrackedObject::GetMajorAxisAngle1 (   )

{
   MajorAxisAngle1 = MC_RAD2DEG * (float)asin (MajorAxis.X);
}

 void TrackedObject::GetMajorAxisAngle2 (   )

{
   float mvm = Mag(MeanVelocity);
   if (mvm > 0.05f) MajorAxisAngle2 = MC_RAD2DEG * (float)acos (fabs(IP(MajorAxis,MeanVelocity / mvm)));
   else MajorAxisAngle2 = -1.0f;
}

 void TrackedObject::GetMatchingPosition (FrgRegion& fg_rgn)
 
{
   MatchingPos.X = (int)(fg_rgn.CX - Centroid.X);
   MatchingPos.Y = (int)(fg_rgn.CY - Centroid.Y);   
} 

 int TrackedObject::GetMatchingPosition (FrgRegion &fg_rgn,float min_sc,float max_sc)

{
   int min_w = (int)(min_sc * Width);
   int max_w = (int)(max_sc * Width);
   int min_h = (int)(min_sc * Height);
   int max_h = (int)(max_sc * Height);
   if (min_w <= fg_rgn.Width && fg_rgn.Width <= max_w && min_h <= fg_rgn.Height && fg_rgn.Height <= max_h) {
      // Observed Position을 획득한다.
      FPoint2D o_pos(fg_rgn.CX - Centroid.X,fg_rgn.CY - Centroid.Y);
      // Predicted Position을 획득한다.
      FPoint2D p_pos;
      GetPredictedPosition (p_pos);
      float ar = (float)(fg_rgn.Width * fg_rgn.Height) / (Width * Height);
      if (ar > 1.0f) ar = 1.0f / ar;
      // Observed Position과 Predicted Position의 가중 합을 구한다.
      // fg_rgn과 본 객체의 면적이 유사할수록 Observed Position에 더 많은 가중치가 주어진다.
      FPoint2D m_pos = ar * o_pos + (1.0f - ar) * p_pos;
      MatchingPos.X = (int)(m_pos.X + 0.5f);
      MatchingPos.Y = (int)(m_pos.Y + 0.5f);
      return (DONE);
   }
   else return (1);
}

 void TrackedObject::GetMatchingPosition (GImage &s_image,GImage &m_image,IPoint2D &s_pos,int s_range)

{
   int x,y;
   
   int sx = s_pos.X - s_range;
   int sy = s_pos.Y - s_range;
   int ex = s_pos.X + s_range;
   int ey = s_pos.Y + s_range;
   int dx = Width  / 32 + 1;
   int dy = Height / 32 + 1;
   MatchingScore = MC_VLV;
   for (y = sy; y <= ey; y++) {
      for (x = sx; x <= ex; x++) {
         float score = GetMatchingScore (s_image,m_image,x,y,dx,dy);
         if (score < MatchingScore) {
            MatchingScore = score;
            MatchingPos(x,y);
         }
      }
   }
}

 float TrackedObject::GetMatchingScore (GImage &s_image,GImage &m_image,int ox,int oy,int dx,int dy)

{
   int x1,y1;

   int sx = ox;
   int sy = oy;
   int ex = sx + Width;
   int ey = sy + Height;
   if (sx < 0) sx = 0;
   if (sy < 0) sy = 0;
   if (ex > s_image.Width ) ex = s_image.Width;
   if (ey > s_image.Height) ey = s_image.Height;
   uint sum_w = 0, sum_wad = 0;
   for (y1 = sy; y1 < ey; y1 += dy) {
      int y2 = y1 - oy;
      byte* l_s_image = s_image[y1];
      byte* l_m_image = m_image[y1];
      byte* l_t_image = TemplateImage[y2];
      byte* l_w_image = WeightImage[y2];
      for (x1 = sx; x1 < ex; x1 += dx) {
         int x2 = x1 - ox;
         ushort w = (ushort)l_m_image[x1] * l_w_image[x2];
         sum_w   += w;
         sum_wad += w * abs((short)l_s_image[x1] - l_t_image[x2]);
      }
   }
   if (!sum_w) return (MC_VLV);
   else {
      double score = (double)sum_wad / sum_w;
      return ((float)score);
   }
}

 void TrackedObject::GetMaxMovingDistance (   )

{
   float md = GetMovingDistance1 (   );
   if (md > MaxMovingDistance) MaxMovingDistance = md;
}

 void TrackedObject::GetMeanArea (int n_frames)

{
   int n;
   
   float area = 0.0f;
   TrkObjRegion* to_rgn = TrkObjRegions.First (   );
   for (n = 0; n < n_frames;   ) {
      if (to_rgn == NULL) break;
      area += to_rgn->Area;
      n++;
      to_rgn = TrkObjRegions.Next (to_rgn);
   }
   if (n) MeanArea = area / n;
   else MeanArea = 0.0f;
}

 void TrackedObject::GetMeanSizeVelocity (int n_frames,FPoint2D& d_velocity)
 
{
   int n;
   
   d_velocity.Clear (   );
   if (n_frames < 2) return;
   IBox2D p_rgn(0,0,0,0);
   TrkObjRegion *to_rgn = TrkObjRegions.First (   );
   for (n = 0; n < n_frames; n++) {
      if (to_rgn == NULL) break;
      if (p_rgn.Width && p_rgn.Height) {
          d_velocity.X += p_rgn.Width  - to_rgn->Width;
          d_velocity.Y += p_rgn.Height - to_rgn->Height;
      }
      p_rgn.Width  = to_rgn->Width;
      p_rgn.Height = to_rgn->Height;
      to_rgn = TrkObjRegions.Next (to_rgn);
   }
   d_velocity /= (float)(n - 1);
}

 void TrackedObject::GetMeanVelocity (int n_frames,FPoint2D& d_velocity)
 
{
   int n;
   
   d_velocity.Clear (   );
   TrkObjRegion *to_rgn = TrkObjRegions.First (   );
   for (n = 0; n < n_frames;   ) {
      if (to_rgn == NULL) break;
      if (to_rgn->Velocity.X != MC_VLV) {
         d_velocity += to_rgn->Velocity;
         n++;
      }
      to_rgn = TrkObjRegions.Next (to_rgn);
   }
   if (n) d_velocity /= (float)n;
   if (n == n_frames) Status |= TO_STATUS_VALID_SPEED;
   else Status &= ~TO_STATUS_VALID_SPEED;
}

 float TrackedObject::GetMovingDistance1 (   )

{
   FPoint2D cp = GetCenterPosition (   );
   FPoint2D ip = InitRegion.GetCenterPosition (   );
   float d = Mag (cp - ip);
   return (d);
}

 float TrackedObject::GetMovingDistance2 (   )

{
   int i;

   IPoint2D ip[4];
   InitRegion.GetCornerPoints (ip);
   IPoint2D cp[4];
   GetCornerPoints (cp);
   float min_md = MC_VLV;
   for (i = 0; i < 4; i++) {
      float md = Mag (cp[i] - ip[i]);
      if (md < min_md) min_md = md;
   }
   return (min_md);
}

 int TrackedObject::GetNumEvents (int pos_evt_type,int neg_evt_type)
 
{
   int n_events = 0;
   
   Event *event = Events.First (   );
   while (event != NULL) {
      if (!(event->Status & (EV_STATUS_ENDED|EV_STATUS_AFTER_ENDED|EV_STATUS_TO_BE_REMOVED))) {
         if (pos_evt_type < 0) {
            if (neg_evt_type < 0) n_events++;
            else if (event->Type != neg_evt_type) n_events++;
         }
         else if (event->Type == pos_evt_type) n_events++;
      }
      event = Events.Next (event);
   }
   return (n_events);
}

 void TrackedObject::GetPathLength (   )

{
   int i;
   
   TrkObjRegion* to_rgn = TrkObjRegions.First (   );
   if (to_rgn == NULL) return;
   IPoint2D cps1[4],cps2[4];
   to_rgn->GetCornerPoints (cps1);
   GetCornerPoints (cps2);
   float max_d = 0.0f;
   for (i = 0; i < 4; i++) {
      float d = Mag(cps2[i] - cps1[i]);
      if (d > max_d) max_d = d;
   }
   PathLength += max_d;
}

 void TrackedObject::GetPredictedPosition (FPoint2D &p_pos,float p_time,int flag_smooth)

{
   FPoint2D velocity = Velocity;
   if (flag_smooth) velocity = MeanVelocity;
   if (velocity.X == MC_VLV) p_pos((float)X,(float)Y);
   else {
      p_pos.X = X + velocity.X * p_time;
      p_pos.Y = Y + velocity.Y * p_time;
   }
}

 void TrackedObject::GetPredictedPosition (IPoint2D &p_pos,float p_time,int flag_smooth)

{
   FPoint2D d_pos;
   GetPredictedPosition (d_pos,p_time,flag_smooth);
   p_pos.X = (int)(d_pos.X + 0.5f);
   p_pos.Y = (int)(d_pos.Y + 0.5f);
}

 void TrackedObject::GetPredictedSize (ISize2D &p_size,float p_time) // jun: 추가, 평균 박스크기 변화속도 

{
   if (MeanSizeVelocity.X == MC_VLV) p_size(Width,Height);
   else {
      p_size.Width  = (int)(Width  + MeanSizeVelocity.X * p_time + 0.5f);
      p_size.Height = (int)(Height + MeanSizeVelocity.Y * p_time + 0.5f);
      if (p_size.Width  < 4) p_size.Width  = 4;
      if (p_size.Height < 4) p_size.Height = 4;
   }
}

 void TrackedObject::GetPrincipalAxes (   )

{
   int x,y;
   
   uint  sum_x  = 0,sum_y  = 0;
   int64 sum_xx = 0,sum_yy = 0,sum_xy = 0;
   for (y = 0; y < MaskImage.Height; y++) {
      byte *l_m_image = MaskImage[y];
      uint yy = y * y;
      for (x = 0; x < MaskImage.Width; x++) {
         if (l_m_image[x]) {
            sum_x  += x;
            sum_y  += y;
            sum_xx += x * x;
            sum_xy += x * y;
            sum_yy += yy;
         }
      }
   }
   Matrix mat_C(2,2),vec_m(2);
   vec_m(0)    = (double)sum_x  / NumRgnPixels;
   vec_m(1)    = (double)sum_y  / NumRgnPixels;
   mat_C[0][0] = (double)sum_xx / NumRgnPixels;
   mat_C[0][1] = (double)sum_xy / NumRgnPixels;
   mat_C[1][1] = (double)sum_yy / NumRgnPixels;
   mat_C[1][0] = mat_C[0][1];
   mat_C = mat_C - vec_m * Tr(vec_m);
   Centroid.X  = (float)vec_m(0);
   Centroid.Y  = (float)vec_m(1);
   Matrix mat_E(2,2),vec_e(2);
   EigenDecomp (mat_C,vec_e,mat_E);
   if (mat_E[1][0] < 0.0f) mat_E = -mat_E; // MajorAxis.Y 값이 항상 양수가 되게 만든다.
   if (vec_e(0) > MC_VSV && vec_e(1) > MC_VSV) {
      MajorAxisLength = 4.0f * (float)sqrt (vec_e(0));
      MinorAxisLength = 4.0f * (float)sqrt (vec_e(1));
      MajorAxis.X     = (float)mat_E[0][0];
      MajorAxis.Y     = (float)mat_E[1][0];
      MinorAxis.X     = (float)mat_E[0][1];
      MinorAxis.Y     = (float)mat_E[1][1];
   }
   else {
      if (Width > Height) {
         MajorAxisLength = (float)Width;
         MinorAxisLength = (float)Height;
         MajorAxis(1.0f,0.0f);
         MinorAxis(0.0f,1.0f);
      }
      else {
         MajorAxisLength = (float)Height;
         MinorAxisLength = (float)Width;
         MajorAxis(0.0f,1.0f);
         MinorAxis(1.0f,0.0f);
      }
   }
}

 void TrackedObject::GetSpeeds (float frm_rate)

{
   Speed    = Mag(MeanVelocity) * frm_rate;
   NorSpeed = 100.0f * Speed / Height;
}

 void TrackedObject::InitTemplate (GImage &s_image,IArray2D &or_map,IBox2D &s_rgn,int s_no,BGRImage *s_cimage)

{
   int x1,y1,x2,y2;

   ObjRgnID     = s_no;
   ObjRgnBndBox = s_rgn;
   SetRegion (s_rgn);
   InitRegion = s_rgn;
   MatchingPos(s_rgn.X,s_rgn.Y);
   MaskImage.Create (Width,Height);
   MaskImage.Clear (   );
   WeightImage.Create (Width,Height);
   WeightImage.Clear (   );
   TemplateImage.Create (Width,Height);
   GrayHistogram.Clear (   );
   if (s_cimage != NULL) ColorHistogram.Initialize (   );
   int ex = X + Width  - 1;
   int ey = Y + Height - 1;
   for (y1 = Y,y2 = 0; y1 <= ey; y1++,y2++) {
      byte* l_s_image = s_image[y1];
      byte* l_m_image = MaskImage[y2];
      byte* l_w_image = WeightImage[y2];
      byte* l_t_image = TemplateImage[y2];
      int*  l_or_map  = or_map[y1];
      for (x1 = X,x2 = 0; x1 <= ex; x1++,x2++) {
         l_t_image[x2] = l_s_image[x1];
         if (l_or_map[x1] == s_no) {
            l_w_image[x2] = 128;
            l_m_image[x2] = PG_WHITE;
            GrayHistogram.Accumulate (GET_GRAY_HISTOGRAM_BIN_INDEX_NO(l_s_image[x1]));
            NumRgnPixels++;
         }
      }
   }
}

 int TrackedObject::IsInactive (float a)
 
{
   if (!Rgn_Inactive.Width || !Rgn_Inactive.Height) return (FALSE);
   float d        = Mag(CenterPos - Rgn_Inactive.GetCenterPosition());
   float d_thrsld = a * (Rgn_Inactive.Width + Rgn_Inactive.Height) / 2.0f;
   if (d < d_thrsld) return (TRUE);
   else return (FALSE);
}

 int TrackedObject::IsInMarginalArea (ISize2D& si_size)

{
   int margin = (int)(si_size.Height * 0.05f);
   int sx1 = margin;
   int sy1 = margin;
   int ex1 = si_size.Width  - margin;
   int ey1 = si_size.Height - margin;
   int sx2 = X;
   int sy2 = Y;
   int ex2 = sx2 + Width;
   int ey2 = sy2 + Height;
   if (sx1 < sx2 && ex2 < ex1 && sy1 < sy2 && ey2 < ey1) return (FALSE);
   else return (TRUE);
}

 int TrackedObject::IsStatic1 (   )
 
{
   int i;
   
   if (!Rgn_Static.Width || !Rgn_Static.Height) return (FALSE);
   IPoint2D cps1[4],cps2[4];
   GetCornerPoints (cps1);
   Rgn_Static.GetCornerPoints (cps2);
   float max_d = 0.0f;
   for (i = 0; i < 4; i++) {
      float d = Mag (cps1[i] - cps2[i]);
      if (d > max_d) max_d = d;
   }
   float d_thrsld = 0.3f * 0.5f * (Rgn_Static.Width + Rgn_Static.Height); // PARAMETERS
   if (max_d < d_thrsld && max_d < 15.0f) return (TRUE); // PARAMETERS
   else return (FALSE);
}

 int TrackedObject::IsStatic2 (   )

{
   if (Pos_Static.X < 0.0f || Pos_Static.Y < 0.0f) return (FALSE);
   if (Speed > 6.0f) return (FALSE); // PARAMETERS
   float d = Mag(CenterPos - Pos_Static);
   float d_thrsld = 0.25f * GetMinimum (Width,Height); // PARAMETERS
   if (d >= d_thrsld) return (FALSE);
   return (TRUE);
}


 int TrackedObject::IsTypeFixed (   )

{
   if (Status & (TO_STATUS_GROUP_MEMBER|TO_STATUS_HUMAN|TO_STATUS_VEHICLE)) return (TRUE);
   if (EventStatus & (TO_EVENT_ABANDONED|TO_EVENT_CAR_PARKING)) return (TRUE);
   return (FALSE);
}

 void TrackedObject::Merge (TrackedObject *s_object)
 
{
   Area                = s_object->Area;
   AspectRatio         = s_object->AspectRatio;
   AxisLengthRatio     = s_object->AxisLengthRatio;
   MajorAxisLength     = s_object->MajorAxisLength;
   MinorAxisLength     = s_object->MinorAxisLength;
   NumRgnPixels        = s_object->NumRgnPixels;
   NorSpeed            = s_object->NorSpeed;
   Speed               = s_object->Speed;
   RealArea            = s_object->RealArea;
   RealDistance        = s_object->RealDistance;
   RealWidth           = s_object->RealWidth;
   RealHeight          = s_object->RealHeight;
   RealMajorAxisLength = s_object->RealMajorAxisLength;
   RealMinorAxisLength = s_object->RealMinorAxisLength;
   RealSpeed           = s_object->RealSpeed;
   CenterPos           = s_object->CenterPos;      
   Centroid            = s_object->Centroid;
   MajorAxis           = s_object->MajorAxis;      
   MinorAxis           = s_object->MinorAxis;
   RealPos             = s_object->RealPos;
   Velocity            = s_object->Velocity;
   MatchingPos         = s_object->MatchingPos;    
   MaskImage           = s_object->MaskImage;
   WeightImage         = s_object->WeightImage;
   TemplateImage       = s_object->TemplateImage;
   SetRegion (*s_object);
}

 void TrackedObject::PutMask (int sx,int sy,byte m_value,GImage &d_image,int flag_overwrite)

{
   int x1,y1,x2,y2;
   
   int sx1 = sx;
   int sy1 = sy;
   int ex1 = sx1 + MaskImage.Width;
   int ey1 = sy1 + MaskImage.Height;
   if (sx1 < 1) sx1 = 1;
   if (sy1 < 1) sy1 = 1;
   if (ex1 > d_image.Width  - 1) ex1 = d_image.Width  - 1;
   if (ey1 > d_image.Height - 1) ey1 = d_image.Height - 1;
   int sx2 = sx1 - sx;
   int sy2 = sy1 - sy;
   if (flag_overwrite) {
      for (y1 = sy1,y2 = sy2; y1 < ey1; y1++,y2++) {
         byte* l_d_image = d_image[y1];
         byte* l_m_image = MaskImage[y2];
         for (x1 = sx1,x2 = sx2; x1 < ex1; x1++,x2++)
            if (l_m_image[x2]) l_d_image[x1] = m_value;
      } 
   }
   else {
      for (y1 = sy1,y2 = sy2; y1 < ey1; y1++,y2++) {
         byte* l_d_image = d_image[y1];
         byte* l_m_image = MaskImage[y2];
         for (x1 = sx1,x2 = sx2; x1 < ex1; x1++,x2++)
            if (l_m_image[x2] && !l_d_image[x1]) l_d_image[x1] = m_value;
      } 
   }
}

 void TrackedObject::PutMask (int sx,int sy,int m_value,IArray2D &d_array,int flag_overwrite)

{
   int x1,y1,x2,y2;
   
   int sx1 = sx;
   int sy1 = sy;
   int ex1 = sx1 + MaskImage.Width;
   int ey1 = sy1 + MaskImage.Height;
   if (sx1 < 1) sx1 = 1;
   if (sy1 < 1) sy1 = 1;
   if (ex1 > d_array.Width  - 1) ex1 = d_array.Width  - 1;
   if (ey1 > d_array.Height - 1) ey1 = d_array.Height - 1;
   int sx2 = sx1 - sx;
   int sy2 = sy1 - sy;
   if (flag_overwrite) {
      for (y1 = sy1,y2 = sy2; y1 < ey1; y1++,y2++) {
         int*  l_d_array = d_array[y1];
         byte* l_m_image = MaskImage[y2];
         for (x1 = sx1,x2 = sx2; x1 < ex1; x1++,x2++)
            if (l_m_image[x2]) l_d_array[x1] = m_value;
      } 
   }
   else {
      for (y1 = sy1,y2 = sy2; y1 < ey1; y1++,y2++) {
         int*  l_d_array = d_array[y1];
         byte* l_m_image = MaskImage[y2];
         for (x1 = sx1,x2 = sx2; x1 < ex1; x1++,x2++)
            if (l_m_image[x2] && !l_d_array[x1]) l_d_array[x1] = m_value;
      } 
   }
}

 void TrackedObject::PutMask (int sx,int sy,ushort m_value,USArray2D &d_array,int flag_overwrite)

{
   int x1,y1,x2,y2;
   
   int sx1 = sx;
   int sy1 = sy;
   int ex1 = sx1 + MaskImage.Width;
   int ey1 = sy1 + MaskImage.Height;
   if (sx1 < 1) sx1 = 1;
   if (sy1 < 1) sy1 = 1;
   if (ex1 > d_array.Width  - 1) ex1 = d_array.Width  - 1;
   if (ey1 > d_array.Height - 1) ey1 = d_array.Height - 1;
   int sx2 = sx1 - sx;
   int sy2 = sy1 - sy;
   if (flag_overwrite) {
      for (y1 = sy1,y2 = sy2; y1 < ey1; y1++,y2++) {
         ushort* l_d_array = d_array[y1];
         byte*   l_m_image = MaskImage[y2];
         for (x1 = sx1,x2 = sx2; x1 < ex1; x1++,x2++)
            if (l_m_image[x2]) l_d_array[x1] = m_value;
      } 
   }
   else {
      for (y1 = sy1,y2 = sy2; y1 < ey1; y1++,y2++) {
         ushort* l_d_array = d_array[y1];
         byte*   l_m_image = MaskImage[y2];
         for (x1 = sx1,x2 = sx2; x1 < ex1; x1++,x2++)
            if (l_m_image[x2] && !l_d_array[x1]) l_d_array[x1] = m_value;
      } 
   }
}

 void TrackedObject::PutRectMask (int sx,int sy,byte m_value,GImage &d_image,int margin)
 
{
   int x,y;
   
   int sx1 = sx - margin;
   int sy1 = sy - margin;
   int ex1 = sx + Width  + margin;
   int ey1 = sy + Height + margin;
   if (sx1 < 0) sx1 = 0;
   if (sy1 < 0) sy1 = 0;
   if (ex1 >= d_image.Width ) ex1 = d_image.Width;
   if (ey1 >= d_image.Height) ey1 = d_image.Height;
   for (y = sy1; y < ey1; y++) {
      byte* l_d_image = d_image[y];
      for (x = sx1; x < ex1; x++) l_d_image[x] = m_value;
   }
}

 void TrackedObject::PutRectMask (int sx,int sy,int m_value,IArray2D& d_array,int margin)
 
{
   int x,y;
   
   int sx1 = sx - margin;
   int sy1 = sy - margin;
   int ex1 = sx + Width  + margin;
   int ey1 = sy + Height + margin;
   if (sx1 < 0) sx1 = 0;
   if (sy1 < 0) sy1 = 0;
   if (ex1 >= d_array.Width ) ex1 = d_array.Width;
   if (ey1 >= d_array.Height) ey1 = d_array.Height;
   for (y = sy1; y < ey1; y++) {
      int* l_d_array = d_array[y];
      for (x = sx1; x < ex1; x++) l_d_array[x] = m_value;
   }
}

 void TrackedObject::PutRectMask (int sx,int sy,ushort m_value,USArray2D& d_array,int margin)
 
{
   int x,y;
   
   int sx1 = sx - margin;
   int sy1 = sy - margin;
   int ex1 = sx + Width  + margin;
   int ey1 = sy + Height + margin;
   if (sx1 < 0) sx1 = 0;
   if (sy1 < 0) sy1 = 0;
   if (ex1 >= d_array.Width ) ex1 = d_array.Width;
   if (ey1 >= d_array.Height) ey1 = d_array.Height;
   for (y = sy1; y < ey1; y++) {
      ushort* l_d_array = d_array[y];
      for (x = sx1; x < ex1; x++) l_d_array[x] = m_value;
   }
}

 void TrackedObject::SetPos_Static (   )

{
   if (Pos_Static.X >= 0.0f && Pos_Static.Y >= 0.0f) return;
   Pos_Static = CenterPos;
}

 void TrackedObject::SetRegion (IBox2D &s_rgn)
 
{
   X         = s_rgn.X;
   Y         = s_rgn.Y;
   Width     = s_rgn.Width;
   Height    = s_rgn.Height;
   CenterPos = GetCenterPosition (   );
}

 void TrackedObject::SetRegion_Inactive (   )
 
{
   Rgn_Inactive = *(IBox2D*)this;
}

 void TrackedObject::SetRegion_Static (   )
 
{
   Rgn_Static = *(IBox2D*)this;
}

 void TrackedObject::ResetPos_Static (   )

{
   Pos_Static(-1.0f,-1.0f);
}

 void TrackedObject::ResetRegion_Inactive (   )
 
{
   Rgn_Inactive.Clear (   );
}

 void TrackedObject::ResetRegion_Static (   )
 
{
   Rgn_Static.Clear (   );
}

 void TrackedObject::UpdateAvgMinMaxPropertyValues (   )
 
{
   float a = (float)(Count_Tracked - 1) / Count_Tracked;
   float b = 1.0f / Count_Tracked;

   // 평균값 계산
   AvgWidth               = a * AvgWidth               + b * Width;
   AvgHeight              = a * AvgHeight              + b * Height;
   AvgArea                = a * AvgArea                + b * Area;
   AvgAspectRatio         = a * AvgAspectRatio         + b * AspectRatio;
   AvgAxisLengthRatio     = a * AvgAxisLengthRatio     + b * AxisLengthRatio;
   AvgMajorAxisLength     = a * AvgMajorAxisLength     + b * MajorAxisLength;
   AvgMinorAxisLength     = a * AvgMinorAxisLength     + b * MinorAxisLength;
   AvgNorSpeed            = a * AvgNorSpeed            + b * NorSpeed;
   AvgSpeed               = a * AvgSpeed               + b * Speed;
   AvgRealArea            = a * AvgRealArea            + b * RealArea;
   AvgRealDistance        = a * AvgRealDistance        + b * RealDistance;
   AvgRealWidth           = a * AvgRealWidth           + b * RealWidth;
   AvgRealHeight          = a * AvgRealHeight          + b * RealHeight;
   AvgRealMajorAxisLength = a * AvgRealMajorAxisLength + b * RealMajorAxisLength;
   AvgRealMinorAxisLength = a * AvgRealMinorAxisLength + b * RealMinorAxisLength;
   AvgRealSpeed           = a * AvgRealSpeed           + b * RealSpeed;

   // 최대값 계산
   if (Width           > MaxWidth          ) MaxWidth           = Width;
   if (Height          > MaxHeight         ) MaxHeight          = Height;
   if (Area            > MaxArea           ) MaxArea            = Area;
   if (AspectRatio     > MaxAspectRatio    ) MaxAspectRatio     = AspectRatio;
   if (AxisLengthRatio > MaxAxisLengthRatio) MaxAxisLengthRatio = AxisLengthRatio;
   if (MajorAxisLength > MaxMajorAxisLength) MaxMajorAxisLength = MajorAxisLength;
   if (MinorAxisLength > MaxMinorAxisLength) MaxMinorAxisLength = MinorAxisLength;
   if (NorSpeed        > MaxNorSpeed       ) MaxNorSpeed        = NorSpeed;
   if (Speed           > MaxSpeed          ) MaxSpeed           = Speed;
   if (RealDistance    > MaxRealDistance   ) MaxRealDistance    = RealDistance;
   if (RealSpeed       > MaxRealSpeed      ) MaxRealSpeed       = RealSpeed;

   // 최소값 계산
   if (Width           < MinWidth          ) MinWidth           = Width;
   if (Height          < MinHeight         ) MinHeight          = Height;
   if (Area            < MinArea           ) MinArea            = Area;
   if (AspectRatio     < MinAspectRatio    ) MinAspectRatio     = AspectRatio;
   if (AxisLengthRatio < MinAxisLengthRatio) MinAxisLengthRatio = AxisLengthRatio;
   if (MajorAxisLength < MinMajorAxisLength) MinMajorAxisLength = MajorAxisLength;
   if (MinorAxisLength < MinMinorAxisLength) MinMinorAxisLength = MinorAxisLength;
   if (NorSpeed        < MinNorSpeed       ) MinNorSpeed        = NorSpeed;
   if (Speed           < MinSpeed          ) MinSpeed           = Speed;
   if (RealDistance    < MinRealDistance   ) MinRealDistance    = RealDistance;
   if (RealSpeed       < MinRealSpeed      ) MinRealSpeed       = RealSpeed;
}

 void TrackedObject::UpdateCountsAndTimes (float frm_duration)

{
   Count_Tracked++;
   Time_BestShot += frm_duration;
   Time_Tracked  += frm_duration;
   if (Status & TO_STATUS_UNDETECTED) Time_Undetected += frm_duration;
   else Time_Undetected = 0.0f;
   if (!(Status & TO_STATUS_CHANGED_ABRUPTLY)) Time_ChangedAbruptly = 0.0f;
   if (!(Status & TO_STATUS_MERGED)) Time_Merged = 0.0f;
}

 void TrackedObject::UpdateGrayHistogram (GImage &s_image,IArray2D &or_map)

{
   int x,y;

   if (!ObjRgnID || (Status & (TO_STATUS_MERGED|TO_STATUS_UNDETECTED))) return;
   int sx = ObjRgnBndBox.X;
   int sy = ObjRgnBndBox.Y;
   int ex = sx + ObjRgnBndBox.Width;
   int ey = sy + ObjRgnBndBox.Height;
   for (y = sy; y < ey; y++) {
      byte* l_s_image  = s_image[y];
      int*  l_or_map = or_map[y];
      for (x = sx; x < ex; x++) {
         if (l_or_map[x] == ObjRgnID) GrayHistogram.Accumulate (GET_GRAY_HISTOGRAM_BIN_INDEX_NO(l_s_image[x]));
      }
   }
}

 void TrackedObject::UpdateNoisiness (ISize2D& si_size)

{
   float noisiness = 0.0f;
   // 객체의 바운딩 박스가 영상의 가장자리에 걸쳐 있으면 객체가 영상 안으로 진입하고 있을 가능성이
   // 크므로 통상적으로 AreaVariation이 클 수 밖에 없다. 따라서 이때는 AreaVariation을 체크하지 않는다.
   if (!IsInMarginalArea (si_size)) {
      // Area Variation
      if      (AreaVariation > 1.0f) noisiness += 0.8f;
      else if (AreaVariation > 0.5f) noisiness += 0.4f;
   }
   // Dispersedness
   if (BndEdgeStrength < 0.7f) {
      if      (60.0f <= Dispersedness && Dispersedness <= 90.0f) noisiness += 0.4f;
      else if (Dispersedness > 90.0f) noisiness += 0.8f;
   }
   // Moving Distance
   float md = GetMovingDistance1 (   );
   if (md < MaxMovingDistance) {
      float d = 0.2f * GetMinimum (Width,Height);
      if (d < 2.0f) d = 2.0f;
      if (md <= (MaxMovingDistance - d)) noisiness += 0.8f;
   }
   // Normalized Speed
   if (NorSpeed >= 150.0f) {
      if (Height <= 6) noisiness += 0.8f;
   }
   Noisiness += noisiness;
   #if defined(__DEBUG_OBT) && defined(__CONSOLE)
   if (ID == 0) {
      logd ("[Tracked Object #%05d]\n",ID);
      logd ("* AreaVariation = %.2f, Area = %.2f, BndEdgeStrength = %.2f, Dispersedness = %.2f\n",
         AreaVariation,Area,BndEdgeStrength,Dispersedness);
      logd ("* MovingDistance = %.2f, MaxMovingDistance = %.2f, NorSpeed = %.2f\n",
         md,MaxMovingDistance,NorSpeed);
      logd ("* noisiness = %.2f, Noisiness = %.2f\n",noisiness,Noisiness);
   }
   #endif
}

 void TrackedObject::UpdateObjectType (float frm_duration)
 
{
   int i;
   
   int   max_i = 0;
   float max_v = -MC_VLV;
   for (i = 0; i < NUM_OBJECT_TYPES; i++) {
      if (OTLikelihood[i] > max_v) {
         max_v = OTLikelihood[i];
         max_i = i;
      }
   }
   if (max_v == OTLikelihood[OC_TYPE_UNKNOWN]) max_i = OC_TYPE_UNKNOWN;
   int c_type = 1 << max_i;
   if (c_type == Type) Time_TypeUnchanged += frm_duration;
   else Time_TypeUnchanged = 0.0f;
   Type = c_type;
}

 void TrackedObject::UpdatePersonAttributes (   )

{
   int i;

   float thrsld = 0.5f * Count_PAR; // PARAMETERS
   for (i = 0; i < NUM_PERSON_ATTRS; i++) {
      int attr = 1 << i;
      if (PALikelihood[i] >= thrsld) PersonAttrs |= attr;
      else PersonAttrs &= ~attr;
   }
}

 void TrackedObject::UpdateRegionAndVelocity (IBox2D &s_rgn,float vu_rate)

{
   if (vu_rate >= 0.0f) {
      FPoint2D dp = s_rgn.GetCenterPosition (   ) - CenterPos;
      if (Velocity.X == MC_VLV) Velocity = dp;
      else Velocity = dp * vu_rate + Velocity * (1.0f - vu_rate);
   }
   else Velocity(MC_VLV,MC_VLV);
   SetRegion (s_rgn);
}

 void TrackedObject::UpdateRegion (IBox2D &s_rgn,float ru_rate)

{
   IBox2D n_rgn;
   n_rgn.Width  = (int)(Width  + ru_rate * (s_rgn.Width  - Width ) + 0.5f);
   n_rgn.Height = (int)(Height + ru_rate * (s_rgn.Height - Height) + 0.5f);
   n_rgn.X      = s_rgn.X + (s_rgn.Width  - n_rgn.Width ) / 2;
   n_rgn.Y      = s_rgn.Y + (s_rgn.Height - n_rgn.Height) / 2;
   SetRegion (n_rgn);
}

 void TrackedObject::UpdateTemplateImage (GImage &s_image,IArray2D &or_map,float tu_rate,IBox2D &d_rgn,float& vu_rate)

{
   int x1,y1,x2,y2;
   int min_size = 4; // PARAMETERS
   vu_rate = tu_rate;
   d_rgn(X,Y,Width,Height);
   // 매칭 위치 상에서 TO가 차지하는 영역을 구한다: (sx1,sy1) - (ex1,ey1)
   int sx1 = MatchingPos.X;
   int sy1 = MatchingPos.Y;
   int ex1 = sx1 + Width;
   int ey1 = sy1 + Height;
   if (sx1 >= s_image.Width || sy1 >= s_image.Height || ex1 <= 0 || ey1 <= 0) {
      Status |= TO_STATUS_TO_BE_REMOVED;
      #if defined(__DEBUG_OBT) && defined(__CONSOLE)
      logd ("Tracked Object #%05d:\n",ID);
      logd ("     Out of Tracking Area: To be removed.\n");
      #endif
      return;
   }
   // TO가 TO_STATUS_UNDETECTED 상태일 때 Predictor를 사용하여 TO의 위치를 이동시키려면 다음의 d_rgn을 사용한다.
   // d_rgn(sx1,sy1,ex1 - sx1,ey1 - sy1);
   if (Width < min_size || Height < min_size) {
      Status |= TO_STATUS_UNDETECTED;
      #if defined(__DEBUG_OBT) && defined(__CONSOLE)
      logd ("Tracked Object #%05d:\n",ID);
      logd ("     Undetected.\n");
      #endif
   }
   // 몇몇 상황에서는 TO의 템플릿을 업데이트하지 않는다.
   if (!ObjRgnID || (Status & TO_STATUS_UNDETECTED)) return;
   // TO에 대응하는 FB의 영역을 구한다: (sx2,sy2) - (ex2,ey2)
   int sx2 = ObjRgnBndBox.X;
   int sy2 = ObjRgnBndBox.Y;
   int ex2 = sx2 + ObjRgnBndBox.Width;
   int ey2 = sy2 + ObjRgnBndBox.Height;
   // "TO + FB" 영역을 구한다: (sx2,sy2) - (ex2,ey2)
   if (sx1 < sx2) sx2 = sx1;
   if (sy1 < sy2) sy2 = sy1;
   if (ex1 > ex2) ex2 = ex1;
   if (ey1 > ey2) ey2 = ey1;
   int ti_width  = ex2 - sx2;
   int ti_height = ey2 - sy2;
   if (ti_width < min_size || ti_height < min_size) {
      Status |= TO_STATUS_UNDETECTED;
      #if defined(__DEBUG_OBT) && defined(__CONSOLE)
      logd ("Tracked Object #%05d:\n",ID);
      logd ("     Undetected.\n");
      #endif
      return;
   }
   // TO의 템플릿 영상과 웨이트 영상을 업데이트할 준비를 한다.
   int dx1 = sx1 - sx2;
   int dy1 = sy1 - sy2;
   int dx2 = dx1 + Width;
   int dy2 = dy1 + Height;
   if (TemplateImage.Width < Width || TemplateImage.Height < Height || dx1 < 0 || dy1 < 0 || dx2 > ti_width || dy2 > ti_height) {
      Status |= TO_STATUS_TO_BE_REMOVED;
      #if defined(__DEBUG_OBT) && defined(__CONSOLE)
      logd ("Tracked Object #%05d:\n",ID);
      logd ("     Invalid parameters: To be removed.\n");
      #endif
      return;
   }
   GImage t_image(ti_width,ti_height); // 업데이트된 템플릿 영상을 담을 버퍼
   GImage w_image(ti_width,ti_height); // 업데이트된 웨이트 영상을 담을 버퍼
   t_image.Clear (   );
   w_image.Clear (   );
   // 일단 t_image와 w_image에 기존 템플릿 영상과 웨이트 영상을 복사한다.
   t_image.Copy (TemplateImage,0,0,Width,Height,dx1,dy1);
   w_image.Copy (WeightImage,0,0,Width,Height,dx1,dy1);
   // TO의 템플릿 업데이트률을 적응적으로 제어한다.
   if ((Status & TO_STATUS_MERGED) && (Status & TO_STATUS_STATIC2)) {
      // Abandoned/Removed Object에 대해 Occlusion이 발생해도 유지가 되도록 조치를 취한다.
      if (EventStatus & (TO_EVENT_ABANDONED_START|TO_EVENT_REMOVED_START|TO_EVENT_ABANDONED|TO_EVENT_REMOVED)) {
         if (Exposure < 0.8f) { // PARAMETERS
            tu_rate = vu_rate = 0.0f;
            return;
         }
         else tu_rate *= 0.1f;
      }
      else {
         if (Exposure < 0.6f) tu_rate *= 0.1f; // PARAMETERS
         else tu_rate *= 0.3f;
      }
   }
   if (Speed > 100.0f) tu_rate *= 3.0f; // PARAMETERS
   if (tu_rate > 1.0f) tu_rate  = 1.0f;
   vu_rate = tu_rate;
   // t_image와 w_image를 업데이트한다.
   int sx3 = sx2, sy3 = sy2, ex3 = ex2, ey3 = ey2;
   CropRegion (s_image.Width,s_image.Height,sx3,sy3,ex3,ey3);
   int sx4 = sx3 - sx2;
   int sy4 = sy3 - sy2;
   ushort a1 = (ushort)(256.0f * tu_rate);
   ushort a2 = 256 - a1;
   ushort b  = (ushort)(tu_rate * PG_WHITE) * 256;
   byte   w0 = (byte)(tu_rate * PG_WHITE); // 초기 웨이트 값
   for (y1 = sy3,y2 = sy4; y1 < ey3; y1++,y2++) {
      byte* l_s_image = s_image[y1];
      byte* l_t_image = t_image[y2];
      byte* l_w_image = w_image[y2];
      int*  l_or_map  = or_map[y1];
      for (x1 = sx3,x2 = sx4; x1 < ex3; x1++,x2++) {
         if (l_w_image[x2]) {
            l_t_image[x2] = (byte)((a2 * l_t_image[x2] + a1 * l_s_image[x1]) >> 8);
            if (l_or_map[x1] == ObjRgnID) l_w_image[x2] = (byte)((a2 * l_w_image[x2] + b) >> 8);
            else l_w_image[x2] = (byte)((a2 * l_w_image[x2]) >> 8);
         }
         else {
            l_t_image[x2] = l_s_image[x1];
            if (l_or_map[x1] == ObjRgnID) l_w_image[x2] = w0;
         }
      }
   }
   if (!w0) w0 = 1;
   if (HumanRect.Height) {
      sx1 = HumanRect.X - sx2;
      sy1 = HumanRect.Y - sy2;
      ex1 = sx1 + HumanRect.Width;
      ey1 = sy1 + HumanRect.Height;
   }
   else {
      // w_image를 이용하여 TO 영역의 범위를 구한다.
      uint  sum_w = 0;
      int64 mx  = 0, my  = 0;
      int64 sdx = 0, sdy = 0;
      for (y1 = 0; y1 < w_image.Height; y1++) {
         byte *l_w_image = w_image[y1];
         for (x1 = 0; x1 < w_image.Width; x1++) {
            byte w = l_w_image[x1];
            if (w) {
               uint wx1 = w * x1;
               uint wy1 = w * y1;
               sum_w += w;
               mx    += wx1;
               my    += wy1;
               sdx   += wx1 * x1;
               sdy   += wy1 * y1;
            }
         }
      }
      float mx2  = (float)mx  / sum_w;
      float my2  = (float)my  / sum_w;
      float sdx2 = (float)sdx / sum_w;
      float sdy2 = (float)sdy / sum_w;
      sdx2 = sqrt (sdx2 - mx2 * mx2);
      sdy2 = sqrt (sdy2 - my2 * my2);
      if (sdx2 < 1.0f) sdx2 = 1.0f;
      if (sdy2 < 1.0f) sdy2 = 1.0f;
      float sigma = 2.0f; // PARAMETERS
      float rx    = sigma * sdx2;
      float ry    = sigma * sdy2;
      sx1 = (int)(mx2 - rx);
      sy1 = (int)(my2 - ry);
      ex1 = (int)(mx2 + rx);
      ey1 = (int)(my2 + ry);
   }
   CropRegion (t_image.Width,t_image.Height,sx1,sy1,ex1,ey1);
   // TemplateImage, WeightImage, MaskImage를 업데이트한다.
   int width  = ex1 - sx1;
   int height = ey1 - sy1;
   if (width < min_size || height < min_size) {
      Status |= TO_STATUS_UNDETECTED;
      #if defined(__DEBUG_OBT) && defined(__CONSOLE)
      logd ("Tracked Object #%05d:\n",ID);
      logd ("     Undetected.\n");
      #endif
      return;
   }
   // MaskImage 상에서 면적이 가장 큰 CR을 찾는다.
   GImage w_image2(width + 2,height + 2);
   w_image2.Copy (w_image,sx1,sy1,width,height,1,1);
   w_image2.SetBoundary (1,0);
   GImage m_image(w_image2.Width,w_image2.Height);
   Binarize (w_image2,(byte)(w0 - 1),m_image);
   IArray2D cr_map(m_image.Width,m_image.Height);
   int n_crs = LabelCRs (m_image,cr_map);
   if (!n_crs) {
      Status |= TO_STATUS_UNDETECTED;
      #if defined(__DEBUG_OBT) && defined(__CONSOLE)
      logd ("Tracked Object #%05d:\n",ID);
      logd ("     Undetected.\n");
      #endif
      return;
   }
   CRArray1D cr_array(n_crs + 1);
   GetCRInfo (cr_map,cr_array);
   int max_a,max_i;
   FindMaxCRArea (cr_array,max_a,max_i);
   ConnectedRegion& cr = cr_array[max_i];
   sx1   += cr.X - 1;
   sy1   += cr.Y - 1;
   width  = cr.Width;
   height = cr.Height;
   TemplateImage = t_image.Extract (sx1,sy1,width,height);
   WeightImage   = w_image.Extract (sx1,sy1,width,height);
   MaskImage     = m_image.Extract (cr.X,cr.Y,width,height);
   NumRgnPixels  = cr.Area;
   // TO의 영역을 구한다.
   d_rgn(sx1 + sx2,sy1 + sy2,width,height);
   // 면적의 변화가 심하면 Predictor를 OFF한다.
   int area1 = width * height;
   int area2 = Width * Height;
   float ar = GetMaximum ((float)area1 / area2,(float)area2 / area1);
   if (ar >= 1.5f) vu_rate = -1.0f; // PARAMETERS
}

 void TrackedObject::UpdateVehicleType (   )
 
{
   int i;
   
   int   max_i = 0;
   float max_v = -MC_VLV;
   for (i = 0; i < NUM_VEHICLE_TYPES; i++) {
      if (VTLikelihood[i] > max_v) {
         max_v = VTLikelihood[i];
         max_i = i;
      }
   }
   VehicleType = 1 << max_i;
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 int CheckObjectInOuterZone (ISize2D& si_size,float oz_size1,float oz_size2,TrackedObject* t_object)
// t_object의 네 코너 포인트 중 적어도 하나가 OuterZone1 내에 속하고
// t_object의 중심점이 OuterZone2 내에 속하면 DONE을 리턴한다.
{
   int i,n;
   
   if (!si_size.Width || !si_size.Height) return (-1);
   if (!oz_size1 || !oz_size2) return (-2);
   int sx = (int)(si_size.Width  * oz_size1);
   int sy = (int)(si_size.Height * oz_size1);
   int ex = si_size.Width  - sx;
   int ey = si_size.Height - sy;
   IPoint2D cps[4];
   t_object->GetCornerPoints (cps);
   for (i = n = 0; i < 4; i++) {
      IPoint2D& cp = cps[i];
      if (sx <= cp.X && cp.X <= ex && sy <= cp.Y && cp.Y <= ey) n++;
   }
   if (n == 4) return (1);
   sx = (int)(si_size.Width  * oz_size2);
   sy = (int)(si_size.Height * oz_size2);
   ex = si_size.Width  - sx;
   ey = si_size.Height - sy;
   int cx = t_object->X + t_object->Width  / 2;
   int cy = t_object->Y + t_object->Height / 2;
   if (sx <= cx && cx <= ex && sy <= cy && cy <= ey) return (2);
   return (DONE);
}

 void CleanTrackedObjectList (TrackedObjectList &to_list)

{
   #if defined(__DEBUG_OBT) && defined(__CONSOLE)
   logd ("Removed Objects: ");
   #endif
   TrackedObject* t_object = to_list.First (   );
   while (t_object != NULL) {
      TrackedObject* n_object = to_list.Next (t_object);
      if (t_object->Status & TO_STATUS_TO_BE_REMOVED) {
         if (t_object->TrkBlob != NULL) t_object->TrkBlob->Status |= TB_STATUS_TO_BE_REMOVED;
         if (t_object->TrkBox  != NULL) t_object->TrkBox->Status  |= TBX_STATUS_TO_BE_REMOVED;
         #if defined(__DEBUG_OBT) && defined(__CONSOLE)
         logd ("[%d]",t_object->ID);
         #endif
         to_list.Remove (t_object);
         delete t_object;
      }
      t_object = n_object;
   }
   #if defined(__DEBUG_OBT) && defined(__CONSOLE)
   logd ("\n\n");
   #endif
}

 TrackedObject* CreateInstance_TrackedObject (   )

{
   if (__CreationFunction_TrackedObject == NULL) {
      TrackedObject *t_object = new TrackedObject;
      return (t_object);
   }
   else return (__CreationFunction_TrackedObject (   ));
}

 int ConvertObjTypeToTrkObjType (int64 obj_type)

{
   struct _OT2TOT {
      int64 ObjType;
      int   TrkObjType;
   };
   static _OT2TOT ot2tot[] = {
      { OBJ_TYPE_HUMAN,      TO_TYPE_HUMAN      },
      { OBJ_TYPE_VEHICLE,    TO_TYPE_VEHICLE    },
      { OBJ_TYPE_ANIMAL,     TO_TYPE_ANIMAL     },
      { OBJ_TYPE_HEAD,       TO_TYPE_HEAD       },
      { OBJ_TYPE_FACE,       TO_TYPE_FACE       },
      { OBJ_TYPE_WHEELCHAIR, TO_TYPE_WHEELCHAIR }
   };

   int n = sizeof(ot2tot) / sizeof(_OT2TOT);
   for (int i = 0; i < n; i++) {
      if (ot2tot[i].ObjType == obj_type) return (ot2tot[i].TrkObjType);
   }
   return (TO_TYPE_UNKNOWN);
}

 int GetTrkObjectTypeName (int to_type,char* d_name)
 
{
   struct _OT2TN {
      int  ObjType;
      char TypeName[64];
   };
   #if defined(__KOREAN)
   static _OT2TN ot2tn[] = {
      { TO_TYPE_HUMAN,       "사람"        },
      { TO_TYPE_VEHICLE,     "차량"        },
      { TO_TYPE_UNKNOWN,     "미확인"      },
      { TO_TYPE_FACE,        "얼굴"        },
      { TO_TYPE_FLAME,       "불꽃"        },
      { TO_TYPE_SMOKE,       "연기"        },
      { TO_TYPE_WATER_LEVEL, "수위"        },
      { TO_TYPE_CROWD,       "군중"        },
      { TO_TYPE_HEAD,        "머리"        },
      { TO_TYPE_ANIMAL,      "동물"        },
      { TO_TYPE_WHEELCHAIR,  "휠체어"      }
   };
   #else
   static _OT2TN ot2tn[] = {
      { TO_TYPE_HUMAN,       "Human"       },
      { TO_TYPE_VEHICLE,     "Vehicle"     },
      { TO_TYPE_UNKNOWN,     "Unknown"     },
      { TO_TYPE_FACE,        "Face"        },
      { TO_TYPE_FLAME,       "Flame"       },
      { TO_TYPE_SMOKE,       "Smoke"       },
      { TO_TYPE_WATER_LEVEL, "Water Level" },
      { TO_TYPE_CROWD,       "Crowd"       },
      { TO_TYPE_HEAD,        "Head"        },
      { TO_TYPE_ANIMAL,      "Animal"      },
      { TO_TYPE_WHEELCHAIR,  "Wheelchair"  }
   };
   #endif
   
   int n = sizeof(ot2tn) / sizeof(_OT2TN);
   for (int i = 0; i < n; i++) {
      if (ot2tn[i].ObjType == to_type) {
         strcpy (d_name,ot2tn[i].TypeName);
         return (DONE);
      }
   }
   d_name[0] = 0;
   return (1);
}

 int GetVehicleTypeName (int vc_type,char* d_name)
 
{
   struct _VT2TN {
      int  VehicleType;
      char TypeName[64];
   };
   #if defined(__KOREAN)
   static _VT2TN vt2tn[] = {
      { TO_VEHICLE_TYPE_UNKNOWN,    "미확인" },
      { TO_VEHICLE_TYPE_SEDAN,      "승용차" },
      { TO_VEHICLE_TYPE_SUV,        "SUV"    },
      { TO_VEHICLE_TYPE_VAN,        "승합차" },
      { TO_VEHICLE_TYPE_TRUCK,      "트럭"   },
      { TO_VEHICLE_TYPE_BUS,        "버스"   },
      { TO_VEHICLE_TYPE_TWOWHEELER, "이륜차" }
   };
   #else
   static _VT2TN vt2tn[] = {
      { TO_VEHICLE_TYPE_UNKNOWN,    "Unknown"     },
      { TO_VEHICLE_TYPE_SEDAN,      "Sedan"       },
      { TO_VEHICLE_TYPE_SUV,        "SUV"         },
      { TO_VEHICLE_TYPE_VAN,        "Van"         },
      { TO_VEHICLE_TYPE_TRUCK,      "Truck"       },
      { TO_VEHICLE_TYPE_BUS,        "Bus"         },
      { TO_VEHICLE_TYPE_TWOWHEELER, "Two-wheeler" }
   };
   #endif
   
   int n = sizeof(vt2tn) / sizeof(_VT2TN);
   for (int i = 0; i < n; i++) {
      if (vt2tn[i].VehicleType == vc_type) {
         strcpy (d_name,vt2tn[i].TypeName);
         return (DONE);
      }
   }
   d_name[0] = 0;
   return (1);
}

 void InvalidateAllTrkObjects (TrackedObjectList& to_list)
 
{
   TrackedObject *t_object = to_list.First (   );
   while (t_object != NULL) {
      t_object->Status |= TO_STATUS_TO_BE_REMOVED;
      t_object = to_list.Next (t_object);
   }
}

 void SetCreationFunction_TrackedObject (TrackedObject*(*func)(   ))

{
   __CreationFunction_TrackedObject = func;
}

}

#if !defined(__VACL_ENVIRON_H)
#define __VACL_ENVIRON_H

#include "bccl_environ.h"

///////////////////////////////////////////////////////////////////////////////
// 
// External Libraries
//
///////////////////////////////////////////////////////////////////////////////

   #define __LIB_CAFFE
// #define __LIB_CUDA
// #define __LIB_DARKNET
// #define __LIB_DLIB
// #define __LIB_IPP
// #define __LIB_ML
   #define __LIB_OPENCV
// #define __LIB_SEETA
// #define __LIB_VERILOOK
// #define __LIB_WIN32CL

#if defined(__LIB_DLIB) && (defined(__CPU_X86) || defined(__CPU_X64))
#define USE_SSE4_INSTRUCTIONS
#endif

///////////////////////////////////////////////////////////////////////////////
// 
// License
//
///////////////////////////////////////////////////////////////////////////////

// #define __LICENSE_MAC

///////////////////////////////////////////////////////////////////////////////
// 
// Supported Companies
//
///////////////////////////////////////////////////////////////////////////////

// #define __SDK_FOR_IDIS

#endif

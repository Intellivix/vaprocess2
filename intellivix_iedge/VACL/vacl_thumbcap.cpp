#include "vacl_ipp.h"
#include "vacl_thumbcap.h"
#include "vacl_warping.h"
#include "VAEngine.h"

#if defined(__LIB_IPP)
#include "vacl_ipp.h"
#endif

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: ThumbnailCapture
//
///////////////////////////////////////////////////////////////////////////////

 ThumbnailCapture::ThumbnailCapture (   )
 
{
   Width              = 120;
   Height             = 120;
   CapBoxSizeRatio    = 1.5f;
   MinObjectSizeRatio = 0.5f;
   BkgColor(0x30,0x30,0x30);
   CropMode           = 0;
}

 void ThumbnailCapture::GetCaptureBox (ISize2D &si_size,IBox2D &so_box,IBox2D &d_box)
 
{
   if (so_box.Width > so_box.Height) {
      d_box.Width  = ((int)(CapBoxSizeRatio * so_box.Width) + 1) / 2 * 2;
      d_box.Height = (int)((float)Height / Width * d_box.Width);
   }
   else {
      d_box.Height = (int)(CapBoxSizeRatio * so_box.Height);
      d_box.Width  = ((int)((float)Width / Height * d_box.Height) + 1) / 2 * 2;
   }
   float r = (float)d_box.Width / Width;
   if (r <= MinObjectSizeRatio) {
      d_box.Width  = (int)(Width  * MinObjectSizeRatio + 0.5f) / 2 * 2;
      d_box.Height = (int)(Height * MinObjectSizeRatio + 0.5f);
   }
   d_box.X = (so_box.X + (so_box.Width - d_box.Width) / 2) / 2 * 2;
   d_box.Y = so_box.Y + (so_box.Height - d_box.Height) / 2;
}

 void ThumbnailCapture::GetCaptureParams (ISize2D &si_size,IBox2D &obj_box,ISize2D &di_size,IPoint2D &sp1,IPoint2D &ep1,IPoint2D &sp2)

{
   if (0 == CropMode) {
      // 2017.04.24 JJong (#1328)
      // Object 가 모서리에 위치해 있으면 짤려서 Thumbnail Capture 되는 부분 수정   

      IBox2D c_box;
      GetCaptureBox (si_size,obj_box,c_box);
      if (c_box.X < 0) c_box.X = 0;
      if (c_box.Y < 0) c_box.Y = 0;

      sp1.X = c_box.X;
      sp1.Y = c_box.Y;
      ep1.X = c_box.X + c_box.Width - 1;
      ep1.Y = c_box.Y + c_box.Height -1;
 
      if (ep1.X >= si_size.Width)  { sp1.X -= ep1.X - (si_size.Width  - 1); ep1.X = si_size.Width  - 1; }
      if (ep1.Y >= si_size.Height) { sp1.Y -= ep1.Y - (si_size.Height - 1); ep1.Y = si_size.Height - 1; }
      if (sp1.X < 0) sp1.X = 0;
      if (sp1.Y < 0) sp1.Y = 0;

      sp2.X = 0;
      sp2.Y = 0;
      c_box.Width  = ep1.X - sp1.X + 1;
      c_box.Height = ep1.Y - sp1.Y + 1;
      di_size(c_box.Width, c_box.Height);   
   }
   else 
   {
   // 2017.04.24 JJong (원본 소스)
   IBox2D c_box;
   GetCaptureBox (si_size,obj_box,c_box);
   sp1.X = c_box.X;
   sp1.Y = c_box.Y;
   ep1.X = c_box.X + c_box.Width - 1;
   ep1.Y = c_box.Y + c_box.Height -1;
   if (sp1.X < 0) sp1.X = 0;
   if (sp1.Y < 0) sp1.Y = 0;
   if (ep1.X >= si_size.Width ) ep1.X = si_size.Width  - 1;
   if (ep1.Y >= si_size.Height) ep1.Y = si_size.Height - 1;
   sp2.X = sp1.X - c_box.X;
   sp2.Y = sp1.Y - c_box.Y;
   di_size(c_box.Width,c_box.Height);
   }
}

 int ThumbnailCapture::GetImageBufferLength (ISize2D &img_size)
 
{
   return (GetLineLength (img_size.Width) * img_size.Height);
}

 int ThumbnailCapture::IsCaptureBoxInsideImage (ISize2D &si_size,IBox2D &so_box)
 
{
   IBox2D c_box;
   GetCaptureBox (si_size,so_box,c_box);
   int ex = c_box.X + c_box.Width;
   int ey = c_box.Y + c_box.Height;
   if (c_box.X < 0 || ex > si_size.Width  || c_box.Y < 0 || ey > si_size.Height) return (FALSE);
    else return (TRUE);
}

 void ThumbnailCapture::Perform (byte *si_buffer,ISize2D &si_size,IBox2D &so_box,BArray1D &di_buffer,ISize2D &di_size)

{
   ISize2D  cp_size;
   IPoint2D sp1,ep1,sp2;
   GetCaptureParams (si_size,so_box,cp_size,sp1,ep1,sp2);
   BArray1D cp_buffer(GetImageBufferLength (cp_size));
   Clear    (cp_buffer,cp_size);
   Capture  (si_buffer,si_size,sp1,ep1,sp2,cp_buffer,cp_size);
   Finalize (cp_buffer,cp_size,di_buffer,di_size);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: ThumbnailCapture_BGR24
//
///////////////////////////////////////////////////////////////////////////////

 void ThumbnailCapture_BGR24::Capture (byte *si_buffer,ISize2D &si_size,IPoint2D &sp1,IPoint2D &ep1,IPoint2D &sp2,byte *di_buffer,ISize2D &di_size)
 
{
   int x1,y1,x2;
   
   int s_sll = GetLineLength (si_size.Width);
   int d_sll = GetLineLength (di_size.Width);
   BGRPixel *s_buf = (BGRPixel*)(si_buffer + sp1.Y * s_sll);
   BGRPixel *d_buf = (BGRPixel*)(di_buffer + sp2.Y * d_sll);
   for (y1 = sp1.Y; y1 <= ep1.Y; y1++) {
      for (x1 = sp1.X,x2 = sp2.X; x1 <= ep1.X; x1++,x2++) d_buf[x2] = s_buf[x1];
      s_buf = (BGRPixel*)((byte*)s_buf + s_sll);
      d_buf = (BGRPixel*)((byte*)d_buf + d_sll);
   }
}

 void ThumbnailCapture_BGR24::Clear (byte *si_buffer,ISize2D &si_size)
 
{
   int x,y;
   
   int s_sll = GetLineLength (si_size.Width);
   BGRPixel* s_buf = (BGRPixel*)si_buffer;
   for (y = 0; y < si_size.Height; y++) {
      for (x = 0; x < si_size.Width; x++) s_buf[x] = BkgColor;
      s_buf = (BGRPixel*)((byte*)s_buf + s_sll);
   }
}

 void ThumbnailCapture_BGR24::Finalize (BArray1D &si_buffer,ISize2D &si_size,BArray1D &di_buffer,ISize2D &di_size)
 
{
   di_size(Width,Height);
   di_buffer.Create (GetImageBufferLength (di_size));
   BGRImage s_image((BGRPixel*)(byte*)si_buffer,si_size.Width,si_size.Height);
   BGRImage d_image((BGRPixel*)(byte*)di_buffer,di_size.Width,di_size.Height);
   Resize_2 (s_image,d_image);
   s_image.Import ((BGRPixel*)NULL,0,0);
   d_image.Import ((BGRPixel*)NULL,0,0);
}

 int ThumbnailCapture_BGR24::GetLineLength (int img_width)
 
{
   return ((img_width * 3 + 3) / 4 * 4);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: ThumbnailCapture_Gray
//
///////////////////////////////////////////////////////////////////////////////

 void ThumbnailCapture_Gray::Capture (byte *si_buffer,ISize2D &si_size,IPoint2D &sp1,IPoint2D &ep1,IPoint2D &sp2,byte *di_buffer,ISize2D &di_size)
 
{
   int x1,y1,x2;
   
   int s_sll = GetLineLength (si_size.Width);
   int d_sll = GetLineLength (di_size.Width);
   byte *s_buf = si_buffer + sp1.Y * s_sll;
   byte *d_buf = di_buffer + sp2.Y * d_sll;
   for (y1 = sp1.Y; y1 <= ep1.Y; y1++) {
      for (x1 = sp1.X,x2 = sp2.X; x1 <= ep1.X; x1++,x2++) d_buf[x2] = s_buf[x1];
      s_buf += s_sll;
      d_buf += d_sll;
   }
}

 void ThumbnailCapture_Gray::Clear (byte *si_buffer,ISize2D &si_size)
 
{
   int color  = ((int)BkgColor.R + BkgColor.G + BkgColor.B) / 3;
   int length = GetImageBufferLength (si_size);
   memset (si_buffer,color,length);
}

 void ThumbnailCapture_Gray::Finalize (BArray1D &si_buffer,ISize2D &si_size,BArray1D &di_buffer,ISize2D &di_size)
 
{
   di_size(Width,Height);
   di_buffer.Create (GetImageBufferLength (di_size));
   GImage s_image((byte*)si_buffer,si_size.Width,si_size.Height);
   GImage d_image((byte*)di_buffer,di_size.Width,di_size.Height);
   Resize_2 (s_image,d_image);
   s_image.Import ((byte*)NULL,0,0);
   d_image.Import ((byte*)NULL,0,0);
}

 int ThumbnailCapture_Gray::GetLineLength (int img_width)
 
{
   return ((img_width + 3) / 4 * 4);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: ThumbnailCapture_YUY2
//
///////////////////////////////////////////////////////////////////////////////

 void ThumbnailCapture_YUY2::Capture (byte *si_buffer,ISize2D &si_size,IPoint2D &sp1,IPoint2D &ep1,IPoint2D &sp2,byte *di_buffer,ISize2D &di_size)
 
{
   int x1,y1,x2;
   
   int s_sll = GetLineLength (si_size.Width);
   int d_sll = GetLineLength (di_size.Width);
   ushort *s_buf = (ushort*)(si_buffer + sp1.Y * s_sll);
   ushort *d_buf = (ushort*)(di_buffer + sp2.Y * d_sll);
   for (y1 = sp1.Y; y1 <= ep1.Y; y1++) {
      for (x1 = sp1.X,x2 = sp2.X; x1 <= ep1.X; x1++,x2++) d_buf[x2] = s_buf[x1];
      s_buf = (ushort*)((byte*)s_buf + s_sll);
      d_buf = (ushort*)((byte*)d_buf + d_sll);
   }
}

 void ThumbnailCapture_YUY2::Clear (byte *si_buffer,ISize2D &si_size)
 
{
   int  i;
   byte cy,cu,cv;
   
   ConvertBGRToYUV (BkgColor,cy,cu,cv);
   uint color;
   byte *p = (byte*)&color;
   #if defined(__CPU_LITTLE_ENDIAN)
   p[0] = cy, p[1] = cu, p[2] = cy, p[3] = cv;
   #else
   p[3] = cy, p[2] = cu, p[1] = cy, p[0] = cv;
   #endif
   int length = GetImageBufferLength (si_size) / 4;
   uint *s_buffer = (uint*)si_buffer;
   for (i = 0; i < length; i++) s_buffer[i] = color;
}

 void ThumbnailCapture_YUY2::Finalize (BArray1D &si_buffer,ISize2D &si_size,BArray1D &di_buffer,ISize2D &di_size)
 
{
   di_size(Width,Height);
   di_buffer.Create (GetImageBufferLength (di_size));
   Resize_YUY2_2 (si_buffer,si_size,di_buffer,di_size);
}

 int ThumbnailCapture_YUY2::GetLineLength (int img_width)
 
{
   return ((img_width * 2 + 3) / 4 * 4);
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 int CaptureThumbnail (byte *si_buffer,ISize2D &si_size,int pix_fmt,IBox2D &so_box,BArray1D &di_buffer,ISize2D &di_size,float cbs_ratio,ISize2D *tn_size)
 
{
   ThumbnailCapture *tc = NULL;
   switch (pix_fmt) {
      case VAE_PIXFMT_BGR24:
         tc = new ThumbnailCapture_BGR24;
         break;
      case VAE_PIXFMT_GRAY8:
         tc = new ThumbnailCapture_Gray;
         break;
      case VAE_PIXFMT_YUY2:
         tc = new ThumbnailCapture_YUY2;
         break;
      default:
         break;
   }
   if (tc == NULL) return (1);
   if (cbs_ratio) tc->CapBoxSizeRatio = cbs_ratio;
   if (tn_size != NULL) {
      tc->Width  = tn_size->Width;
      tc->Height = tn_size->Height;
   }
   tc->Perform (si_buffer,si_size,so_box,di_buffer,di_size);
   delete tc;
   return (DONE);
}

}

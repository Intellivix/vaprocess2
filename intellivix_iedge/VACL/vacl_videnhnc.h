#if !defined(__VACL_VIDENHNC_H)
#define __VACL_VIDENHNC_H

#include "vacl_define.h"

 namespace VACL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Class: VideoEnhancement
//
///////////////////////////////////////////////////////////////////////////////

 class VideoEnhancement
 
{
   protected:
      int Flag_Initialized;
      int FrameCount;
   
   protected:
      USArray1D          SumArray;
      Array1D<BArray1D>  Frames;
      Array1D<BArray1D*> VFQueue;
      
   public:
      float   FrameRate;
      ISize2D FrameSize;

   public:
      int   CFRadius;    // Closing filter radius
      int   MDThreshold; // Motion detection threshold
      float FrameStep;   // Frame step in second
      float MAFSize;     // Moving average filter size in second

   public:
      VideoEnhancement (   );
      virtual ~VideoEnhancement (   );
      
   protected:
      void _Init                 (   );
      int  AddToFrameQueue       (byte *si_buffer);
      void GetMotionImage        (byte *si_buffer,GImage &d_image);
      void PerformMorphFiltering (GImage& d_image);
   
   public:
      virtual void Close (   );
   
   public:
      void Initialize    (ISize2D& frm_size,float frm_rate);
      int  IsInitialized (   );
      int  Perform_YUY2  (byte *si_buffer,byte *di_buffer);
};

 inline int VideoEnhancement::IsInitialized (   )

{
   return (Flag_Initialized);
}

}

#endif

#include "vacl_conrgn.h"
#include "vacl_light.h"

#if defined(__DEBUG_FGD)
#include "Win32CL/Win32CL.h"
#endif

#if defined(__DEBUG_FGD)
extern int _DVX_FGD,_DVY_FGD;
extern CImageView* _DebugView_FGD;
#endif

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Definitions
//
///////////////////////////////////////////////////////////////////////////////

#define LDR_UPSCALE_BITS    15
#define LDR_UPSCALE_VALUE   32768

#define LDR_ADD_IR(A,I)  ir = A[I]; r += ir; rr += (int64)ir * ir;

///////////////////////////////////////////////////////////////////////////////
//
// Class: LDR_Region
//
///////////////////////////////////////////////////////////////////////////////

 LDR_Region::LDR_Region (   )

{
   Flag_Remove = FALSE;
   Area        = 0;
   SX          = 0;
   SY          = 0;
   Type        = 0;
   Score1      = 0.0f;
   Score2      = 0.0f;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: LDR_RegionBoundaryCheck1
//
///////////////////////////////////////////////////////////////////////////////

 class LDR_RegionBoundaryCheck1 : public BoundaryPixelTracking
 
{
   protected:
      byte**      RSImage;
      LDR_Region* Regions;
   
   public:
      int NumEdgePixels;
      int NumObjectPixels;

   public:
      LDR_RegionBoundaryCheck1 (   );
      virtual ~LDR_RegionBoundaryCheck1 (   );
   
   protected:
      void _Init (   );
   
   public:
      virtual void ProcessBoundaryPixel (   );
      virtual void ProcessMarginalPixel (   ) {   };
      virtual int  StartTracking        (   );
      
   public:
     virtual void Close (   );
   
   public:
      void GetScores  (float& score1,float& score2);
      int  Initialize (IArray2D& rg_map,Array1D<LDR_Region>& rg_array,GImage& rs_image);
};

 LDR_RegionBoundaryCheck1::LDR_RegionBoundaryCheck1 (   )

{
   _Init (   );
}

 LDR_RegionBoundaryCheck1::~LDR_RegionBoundaryCheck1 (   )

{
   Close (   );
}

 void LDR_RegionBoundaryCheck1::_Init (   )
 
{
   RSImage         = NULL;
   Regions         = NULL;
   NumEdgePixels   = 0;
   NumObjectPixels = 0;
} 

 void LDR_RegionBoundaryCheck1::Close (   )

{
   BoundaryPixelTracking::Close (   );
   _Init (   );
}

 void LDR_RegionBoundaryCheck1::GetScores (float& score1,float& score2)

{
   score1 = score2 = 0.0f;
   if (!NumBoundaryPixels) return;
   score1 = (float)(NumEdgePixels + NumObjectPixels) / NumBoundaryPixels;
   score2 = (float)NumEdgePixels / NumBoundaryPixels;
}

 int LDR_RegionBoundaryCheck1::Initialize (IArray2D& rg_map,Array1D<LDR_Region>& rg_array,GImage& rs_image)

{
   Regions = rg_array;
   RSImage = rs_image;
   return (BoundaryPixelTracking::Initialize (rg_map,3));
}

 void LDR_RegionBoundaryCheck1::ProcessBoundaryPixel (   )
 
{
   int x1 = X - 1, x2 = X + 1;
   int y1 = Y - 1, y2 = Y + 1;
   if (RSImage[Y][x1] == LDR_RGNTYPE_EDGE || 
       RSImage[Y][x2] == LDR_RGNTYPE_EDGE ||
       RSImage[y1][X] == LDR_RGNTYPE_EDGE ||
       RSImage[y2][X] == LDR_RGNTYPE_EDGE) NumEdgePixels++;
   else if ((RSImage[Y][x1] == LDR_RGNTYPE_OBJECT && !Regions[(*CRMap)[Y][x1]].Flag_Remove) ||
            (RSImage[Y][x2] == LDR_RGNTYPE_OBJECT && !Regions[(*CRMap)[Y][x2]].Flag_Remove) ||
            (RSImage[y1][X] == LDR_RGNTYPE_OBJECT && !Regions[(*CRMap)[y1][X]].Flag_Remove) ||
            (RSImage[y2][X] == LDR_RGNTYPE_OBJECT && !Regions[(*CRMap)[y2][X]].Flag_Remove)) NumObjectPixels++;
}

 int LDR_RegionBoundaryCheck1::StartTracking (   )
 
{
   NumEdgePixels = 0;
   NumObjectPixels = 0;
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: LDR_RegionBoundaryCheck2
//
///////////////////////////////////////////////////////////////////////////////

 class LDR_RegionBoundaryCheck2 : public BoundaryPixelTracking
 
{
   protected:
      byte**      RSImage;
      LDR_Region* Regions;
   
   public:
      int NumValidPixels;

   public:
      LDR_RegionBoundaryCheck2 (   );
      virtual ~LDR_RegionBoundaryCheck2 (   );
   
   protected:
      void _Init (   );
   
   public:
      virtual void ProcessBoundaryPixel (   );
      virtual void ProcessMarginalPixel (   ) {   };
      virtual int  StartTracking        (   );
      
   public:
     virtual void Close (   );
   
   public:
      float GetScore   (   );
      int   Initialize (IArray2D& rg_map,Array1D<LDR_Region>& rg_array,GImage& rs_image);
};

 LDR_RegionBoundaryCheck2::LDR_RegionBoundaryCheck2 (   )

{
   _Init (   );
}

 LDR_RegionBoundaryCheck2::~LDR_RegionBoundaryCheck2 (   )

{
   Close (   );
}

 void LDR_RegionBoundaryCheck2::_Init (   )
 
{
   RSImage        = NULL;
   Regions        = NULL;
   NumValidPixels = 0;
} 

 void LDR_RegionBoundaryCheck2::Close (   )

{
   BoundaryPixelTracking::Close (   );
   _Init (   );
}

 float LDR_RegionBoundaryCheck2::GetScore (   )

{
   if (!NumBoundaryPixels) return (0.0f);
   return ((float)NumValidPixels / NumBoundaryPixels);
}

 int LDR_RegionBoundaryCheck2::Initialize (IArray2D& rg_map,Array1D<LDR_Region>& rg_array,GImage& rs_image)

{
   Regions = rg_array;
   RSImage = rs_image;
   return (BoundaryPixelTracking::Initialize (rg_map,3));
}

 void LDR_RegionBoundaryCheck2::ProcessBoundaryPixel (   )
 
{
   int x1 = X - 1, x2 = X + 1;
   int y1 = Y - 1, y2 = Y + 1;
   if ((RSImage[Y][x1] && !Regions[(*CRMap)[Y][x1]].Flag_Remove) ||
       (RSImage[Y][x2] && !Regions[(*CRMap)[Y][x2]].Flag_Remove) ||
       (RSImage[y1][X] && !Regions[(*CRMap)[y1][X]].Flag_Remove) ||
       (RSImage[y2][X] && !Regions[(*CRMap)[y2][X]].Flag_Remove)) NumValidPixels++;
}

 int LDR_RegionBoundaryCheck2::StartTracking (   )
 
{
   NumValidPixels = 0;
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: LightDisturbanceReduction
//
///////////////////////////////////////////////////////////////////////////////

 LightDisturbanceReduction::LightDisturbanceReduction (   )
 
{
   MinArea          = 3;
   MaxAreaFactor    = 0.2f;
   Object_Threshold = 0.1f;
   Light_MinIR      = 1.0f;
   Light_MaxIR      = 3.5f;
   Light_MaxIRSD    = 0.15f;
   Light_Threshold  = 0.5f;
   Shadow_MinIR     = 0.5f;
   Shadow_MaxIR     = 1.0f;
   Shadow_MaxIRSD   = 0.05f;
   Shadow_Threshold = 0.5f;
   Edge_Threshold   = 0.0f;
   #if defined(__CPNI_CERT_SZM)
   Edge_Threshold   = 0.1f;
   #endif
}

 void LightDisturbanceReduction::Close (   )
 
{
   RegionMap.Delete (   );
   Regions.Delete (   );
}

 void LightDisturbanceReduction::GetPixelIntensityRatios (GImage &s_image,GImage &fg_image,GImage &bg_image,int win_size,FArray2D &d_array)

{
   int x,y,x2,y2;

   d_array.Clear (   );
   int hw = win_size / 2;
   int ex = s_image.Width  - hw;
   int ey = s_image.Height - hw;
   for (y = hw; y < ey; y++) {
      int sy2 = y - hw;
      int ey2 = y + hw;
      byte*  l_s_image   = s_image[y];
      byte*  l_fg_image  = fg_image[y];
      byte*  l_fg_image1 = fg_image[y - 1];
      byte*  l_fg_image2 = fg_image[y + 1];
      byte*  l_bg_image  = bg_image[y];
      float* l_d_array   = d_array[y];
      for (x = hw; x < ex; x++) {
         if (l_fg_image[x]) {
            if (l_fg_image[x - 1] && l_fg_image[x + 1] && l_fg_image1[x] && l_fg_image2[x]) {
               l_d_array[x] = (float)l_s_image[x] / l_bg_image[x];
            }
            else {
               int sx2 = x - hw;
               int ex2 = x + hw;
               for (y2 = sy2; y2 <= ey2; y2++) {
                  byte*  l_s_image2  = s_image[y2];
                  byte*  l_bg_image2 = bg_image[y2];
                  float* l_d_array2  = d_array[y2];
                  for (x2 = sx2; x2 <= ex2; x2++) {
                     l_d_array2[x2] = (float)l_s_image2[x2] / l_bg_image2[x2];
                  }
               }
            }
         }
      }
   }
}

 void LightDisturbanceReduction::GetPixelIntensityRatios_3x3 (GImage &s_image,GImage &fg_image,GImage &bg_image,UIArray2D &d_array)

{
   int x,y;

   d_array.Clear (   );
   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   for (y = 1; y < ey; y++) {
      int y1 = y - 1;
      int y2 = y + 1;
      byte* l_s_image   = s_image[y];
      byte* l_s_image1  = s_image[y1];
      byte* l_s_image2  = s_image[y2];
      byte* l_bg_image  = bg_image[y];
      byte* l_bg_image1 = bg_image[y1];
      byte* l_bg_image2 = bg_image[y2];
      byte* l_fg_image  = fg_image[y];
      byte* l_fg_image1 = fg_image[y1];
      byte* l_fg_image2 = fg_image[y2];
      uint* l_d_array   = d_array[y];
      uint* l_d_array1  = d_array[y1];
      uint* l_d_array2  = d_array[y2];
      for (x = 1; x < ex; x++) {
         int x1 = x - 1;
         int x2 = x + 1;
         if (l_fg_image[x]) {
            l_d_array[x] = ((uint)l_s_image[x] << LDR_UPSCALE_BITS) / l_bg_image[x]; // (0,0)
            if (!(l_fg_image[x1] && l_fg_image[x2] && l_fg_image1[x] && l_fg_image2[x])) { // 바운더리 픽셀인 경우
               l_d_array1[x1] = ((uint)l_s_image1[x1] << LDR_UPSCALE_BITS) / l_bg_image1[x1]; // (-1,-1)
               l_d_array1[x ] = ((uint)l_s_image1[x ] << LDR_UPSCALE_BITS) / l_bg_image1[x ]; // (-1, 0)
               l_d_array1[x2] = ((uint)l_s_image1[x2] << LDR_UPSCALE_BITS) / l_bg_image1[x2]; // (-1, 1)
               l_d_array [x1] = ((uint)l_s_image [x1] << LDR_UPSCALE_BITS) / l_bg_image [x1]; // (0 ,-1)
               l_d_array [x2] = ((uint)l_s_image [x2] << LDR_UPSCALE_BITS) / l_bg_image [x2]; // (0 , 1)
               l_d_array2[x1] = ((uint)l_s_image2[x1] << LDR_UPSCALE_BITS) / l_bg_image2[x1]; // (1 ,-1)
               l_d_array2[x ] = ((uint)l_s_image2[x ] << LDR_UPSCALE_BITS) / l_bg_image2[x ]; // (1 , 0)
               l_d_array2[x2] = ((uint)l_s_image2[x2] << LDR_UPSCALE_BITS) / l_bg_image2[x2]; // (1 , 1)
            }
         }
      }
   }
}

 int LightDisturbanceReduction::GetRegionMap (UIArray2D &ir_array,GImage &rs_image)
 
{
   int i,x,y;

   if (RegionMap.Width != rs_image.Width || RegionMap.Height != rs_image.Height) RegionMap.Create (rs_image.Width,rs_image.Height);
   int n_crs = LabelCRs (rs_image,RegionMap);
   if (!n_crs) {
      Regions.Delete (   );
      return (1);
   }
   Regions.Create (n_crs + 1);
   Regions.Clear  (   );
   for (i = 1; i < Regions.Length; i++) {
      LDR_Region &rgn = Regions[i];
      rgn.X      = RegionMap.Width;
      rgn.Y      = RegionMap.Height;
      rgn.Width  = -1;
      rgn.Height = -1;
   }
   for (y = 0; y < RegionMap.Height; y++) {
      int*  l_rg_map   = RegionMap[y];
      byte* l_rs_image = rs_image[y];
      for (x = 0; x < RegionMap.Width; x++) {
         if (l_rg_map[x]) {
            LDR_Region &rgn = Regions[l_rg_map[x]];
            if (x < rgn.X     ) rgn.X     = x;
            if (y < rgn.Y     ) rgn.Y     = y;
            if (x > rgn.Width ) rgn.Width = x;
            if (y > rgn.Height) rgn.Height= y;
            if (!rgn.Type) {
               rgn.Type = l_rs_image[x];
               rgn.SX = x;
               rgn.SY = y;
            }
            rgn.Area++;
         }
      }
   }
   for (i = 1; i < Regions.Length; i++) {
      LDR_Region &rgn = Regions[i];
      rgn.Width  += 1 - rgn.X;
      rgn.Height += 1 - rgn.Y;
   }
   return (DONE);
}

 void LightDisturbanceReduction::MarkFrgEdgePixels (GImage &rs_image,GImage &fe_image)
 
{
   int x,y;
   
   for (y = 0; y < rs_image.Height; y++) {
      byte* l_rs_image = rs_image[y];
      byte* l_fe_image = fe_image[y];
      for (x = 0; x < rs_image.Width; x++) {
         if (l_fe_image[x]) l_rs_image[x] = LDR_RGNTYPE_EDGE;
      }
   }
}

 void LightDisturbanceReduction::Perform (GImage &s_image,GImage &bg_image,GImage &fe_image,GImage &fg_image)

{
   int i;

   // 전경 검출 결과를 rs_image에 복사한다.
   GImage rs_image = fg_image;
   // 각 픽셀마다 F/B Intensity Ratio를 구한다.
   UIArray2D ir_array(s_image.Width,s_image.Height);
   GetPixelIntensityRatios_3x3 (s_image,fg_image,bg_image,ir_array);
   // 그림자 후보 영역(배경보다 어둡고 텍스처 성분이 유지되는 영역)에 속하는 픽셀들을 LDR_RGNTYPE_SHADOW 값으로 마킹한다.
   PerformRegionSegmentation_3x3 (ir_array,Shadow_MinIR,Shadow_MaxIR,Shadow_MaxIRSD,LDR_RGNTYPE_OBJECT,LDR_RGNTYPE_SHADOW,rs_image);
   // 조명 후보 영역(배경보다 밝고 텍스처 성분이 유지되는 영역)에 속하는 픽셀들을 LDR_RGNTYPE_LIGHT 값으로 마킹한다.
   PerformRegionSegmentation_3x3 (ir_array,Light_MinIR,Light_MaxIR,Light_MaxIRSD,LDR_RGNTYPE_OBJECT,LDR_RGNTYPE_LIGHT,rs_image);
   // 전경 에지에 해당하는 픽셀들을 LDR_RGNTYPE_EDGE 값으로 마킹한다.
   MarkFrgEdgePixels (rs_image,fe_image);
   // Region Segmentation Result(rs_image): OBJECT(255), EDGE(192), LIGHT(128), SHADOW(64)
   #if defined(__DEBUG_FGD)
   _DebugView_FGD->DrawImage (rs_image,_DVX_FGD,_DVY_FGD);
   #endif
   // RegionMap과 Regions를 획득한다.
   // RegionMap: rs_image의 CRL(Connected Region Labeling) 결과
   // Regions  : 각 CR에 대한 정보를 저장한 배열
   if (GetRegionMap (ir_array,rs_image)) {
      #if defined(__DEBUG_FGD)
      _DVY_FGD += rs_image.Height;
      _DebugView_FGD->DrawText (_T("Region Segmentation Result"),_DVX_FGD,_DVY_FGD,PC_GREEN);
      _DVY_FGD += 20;
      #endif
      return;
   }
   //
   // 제거할 영역들을 마킹한다.
   //
   for (i = 1; i < Regions.Length; i++) {
      LDR_Region &rgn = Regions[i];
      if (rgn.Area <= MinArea) rgn.Flag_Remove = TRUE;
      else rgn.Flag_Remove = FALSE;
   }
   LDR_RegionBoundaryCheck1 rb_checker1;
   rb_checker1.Initialize (RegionMap,Regions,rs_image);
   // OBJECT 영역을 처리한다.
   for (i = 1; i < Regions.Length; i++) {
      LDR_Region &rgn = Regions[i];
      if (!rgn.Flag_Remove && rgn.Type == LDR_RGNTYPE_OBJECT) {
         rb_checker1.Perform (rgn.SX,rgn.SY);
         rb_checker1.GetScores (rgn.Score1,rgn.Score2);
         if (rgn.Score2 < Object_Threshold) rgn.Flag_Remove = TRUE;
      }
   }
   // LIGHT 및 SHADOW 영역을 처리한다.
   for (i = 1; i < Regions.Length; i++) {
      LDR_Region &rgn = Regions[i];
      if (!rgn.Flag_Remove && (rgn.Type == LDR_RGNTYPE_LIGHT || rgn.Type == LDR_RGNTYPE_SHADOW)) {
         rb_checker1.Perform (rgn.SX,rgn.SY);
         rb_checker1.GetScores (rgn.Score1,rgn.Score2);
         if (rgn.Type == LDR_RGNTYPE_LIGHT) {
            if (rgn.Score1 < Light_Threshold) rgn.Flag_Remove = TRUE;
         }
         else {
            if (rgn.Score1 < Shadow_Threshold) rgn.Flag_Remove = TRUE;
         }
         #if defined(__DEBUG_FGD)
         if (rgn.Area > 49) {
            CString text;
            text.Format (_T("%4.2f"),rgn.Score1,rgn.Score2);
            FPoint2D cp = rgn.GetCenterPosition (   );
            _DebugView_FGD->DrawText (text,_DVX_FGD + (int)cp.X - 10,_DVY_FGD + (int)cp.Y - 10,PC_RED,TRANSPARENT,FT_ARIAL,14);
         }
         #endif
      }
   }
   if (Edge_Threshold > 0.0f) {
      // EDGE 영역을 처리한다.
      LDR_RegionBoundaryCheck2 rb_checker2;
      rb_checker2.Initialize (RegionMap,Regions,rs_image);
      for (i = 1; i < Regions.Length; i++) {
         LDR_Region &rgn = Regions[i];
         if (!rgn.Flag_Remove && rgn.Type == LDR_RGNTYPE_EDGE) {
            rb_checker2.Perform (rgn.SX,rgn.SY);
            float score = rb_checker2.GetScore (   );
            if (score < Edge_Threshold) rgn.Flag_Remove = TRUE;
         }
      }
   }
   #if defined(__DEBUG_FGD)
   _DVY_FGD += rs_image.Height;
   _DebugView_FGD->DrawText (_T("Region Segmentation Result"),_DVX_FGD,_DVY_FGD,PC_GREEN);
   _DVY_FGD += 20;
   #endif
   // 마킹된 영역들을 제거한다.
   RemoveMarkedRegions (fg_image);
}

 void LightDisturbanceReduction::PerformRegionSegmentation (FArray2D &ir_array,int win_size,float min_ir,float max_ir,float max_irsd,byte s_label,byte t_label,GImage &d_image)

{
   int x,y,x2,y2;

   int hw = win_size / 2;
   int ar = 2 * hw + 1;
   ar *= ar;
   int ex = ir_array.Width  - hw;
   int ey = ir_array.Height - hw;
   float max_irv = max_irsd * max_irsd;
   for (y = hw; y < ey; y++) {
      int sy2 = y - hw;
      int ey2 = y + hw;
      float* l_ir_array = ir_array[y];
      byte*  l_d_image  = d_image[y];
      for (x = hw; x < ex; x++) {
         if (l_d_image[x] == s_label) {
            if (min_ir <= l_ir_array[x] && l_ir_array[x] < max_ir) {
               int sx2 = x - hw;
               int ex2 = x + hw;
               float r = 0.0f, rr = 0.0f;
               for (y2 = sy2; y2 <= ey2; y2++) {
                  float* l_ir_array2 = ir_array[y2];
                  for (x2 = sx2; x2 <= ex2; x2++) {
                     r  += l_ir_array2[x2];
                     rr += l_ir_array2[x2] * l_ir_array2[x2];
                  }
               }
               r /= ar;
               float irv = rr / ar - r * r;
               if (irv < max_irv) l_d_image[x] = t_label;
            }
         }
      }
   }
}

 void LightDisturbanceReduction::PerformRegionSegmentation_3x3 (UIArray2D &ir_array,float min_ir,float max_ir,float max_irsd,byte s_label,byte t_label,GImage &d_image)

{
   int x,y;

   int ex = ir_array.Width  - 1;
   int ey = ir_array.Height - 1;
   uint min_ir2      = (uint)(min_ir * LDR_UPSCALE_VALUE);
   uint max_ir2      = (uint)(max_ir * LDR_UPSCALE_VALUE);
   uint irsd_thrsld2 = (uint)(max_irsd * LDR_UPSCALE_VALUE);
   uint irv_thrsld2  = irsd_thrsld2 * irsd_thrsld2;
   for (y = 1; y < ey; y++) {
      uint* l_ir_array  = ir_array[y];
      uint* l_ir_array1 = ir_array[y - 1];
      uint* l_ir_array2 = ir_array[y + 1];
      byte* l_d_image   = d_image[y];
      for (x = 1; x < ex; x++) {
         if (l_d_image[x] == s_label) {
            if (min_ir2 <= l_ir_array[x] && l_ir_array[x] < max_ir2) {
               int x1   = x - 1;
               int x2   = x + 1;
               uint  r  = 0;
               int64 rr = 0;
               uint  ir;
               LDR_ADD_IR(l_ir_array1,x1)
               LDR_ADD_IR(l_ir_array1,x )
               LDR_ADD_IR(l_ir_array1,x2)
               LDR_ADD_IR(l_ir_array ,x1)
               LDR_ADD_IR(l_ir_array ,x )
               LDR_ADD_IR(l_ir_array ,x2)
               LDR_ADD_IR(l_ir_array2,x1)
               LDR_ADD_IR(l_ir_array2,x )
               LDR_ADD_IR(l_ir_array2,x2)
               r /= 9;
               uint irv = (uint)(rr / 9 - r * r);
               if (irv < irv_thrsld2) l_d_image[x] = t_label;
            }
         }
      }
   }
}

 void LightDisturbanceReduction::RemoveMarkedRegions (GImage &fg_image)
 
{
   int x,y;
    
   for (y = 0; y < RegionMap.Height; y++) {
      int*  l_rg_map   = RegionMap[y];
      byte* l_fg_image = fg_image[y];
      for (x = 0; x < RegionMap.Width; x++) {
         if (l_rg_map[x] && Regions[l_rg_map[x]].Flag_Remove) l_fg_image[x] = PG_BLACK;
      }
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 void GetFrgEdgeImage (GImage& s_image,GImage& bg_image,GImage& fg_image,int e_thrsld,GImage& d_image)

{
   int x,y;

   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   d_image.Clear (   );
   for (y = 1; y < ey; y++) {
      byte* l_s_image   = s_image[y];
      byte* l_s_image1  = s_image[y - 1];
      byte* l_s_image2  = s_image[y + 1];
      byte* l_bg_image  = bg_image[y];
      byte* l_bg_image1 = bg_image[y - 1];
      byte* l_bg_image2 = bg_image[y + 1];
      byte* l_fg_image  = fg_image[y];
      byte* l_d_image   = d_image[y];
      for (x = 1; x < ex; x++) {
         if (l_fg_image[x]) {
            int x1 = x - 1;
            int x2 = x + 1;
            short gx1 = (l_s_image2[x2] + 2 * (short)l_s_image[x2] + l_s_image1[x2]) -
                        (l_s_image2[x1] + 2 * (short)l_s_image[x1] + l_s_image1[x1]);
            short gy1 = (l_s_image2[x2] + 2 * (short)l_s_image2[x] + l_s_image2[x1]) -
                        (l_s_image1[x1] + 2 * (short)l_s_image1[x] + l_s_image1[x2]);
            int   gm1 = abs (gx1) + abs (gy1);
            short gx2 = (l_bg_image2[x2] + 2 * (short)l_bg_image[x2] + l_bg_image1[x2]) -
                        (l_bg_image2[x1] + 2 * (short)l_bg_image[x1] + l_bg_image1[x1]);
            short gy2 = (l_bg_image2[x2] + 2 * (short)l_bg_image2[x] + l_bg_image2[x1]) -
                        (l_bg_image1[x1] + 2 * (short)l_bg_image1[x] + l_bg_image1[x2]);
            int   gm2 = abs (gx2) + abs (gy2);
            if (abs (gm1 - gm2) >= e_thrsld && gm1 >= e_thrsld) l_d_image[x] = PG_WHITE;
         }
      }
   }
}

 void PerformLightDisturbanceReduction_BES (GImage& e_image,float bes_thrsld,GImage& fg_image)

{
   int i,x,y;
   
   IArray2D cr_map(fg_image.Width,fg_image.Height);
   int n_crs = LabelCRs (fg_image,cr_map);
   if (!n_crs) return;
   IArray1D nb_array(n_crs + 1);
   IArray1D ne_array(n_crs + 1);
   nb_array.Clear (   );
   ne_array.Clear (   );
   int ex = cr_map.Width  - 1;
   int ey = cr_map.Height - 1;
   for (y = 1; y < ey; y++) {
      int*  l_cr_map   = cr_map[y];
      int*  l_cr_map1  = cr_map[y - 1];
      int*  l_cr_map2  = cr_map[y + 1];
      byte* l_e_image  = e_image[y];
      byte* l_e_image1 = e_image[y - 1];
      byte* l_e_image2 = e_image[y + 1];
      for (x = 1; x < ex; x++) {
         if (l_cr_map[x]) {
            if (!(l_cr_map1[x] && l_cr_map2[x] && l_cr_map[x - 1] && l_cr_map[x + 1])) {
               nb_array[l_cr_map[x]]++;
               if (l_e_image[x] || l_e_image1[x] || l_e_image2[x] || l_e_image[x - 1] || l_e_image[x + 1]) {
                  ne_array[l_cr_map[x]]++;
               }
            }
         }
      }
   }
   BArray1D mk_array(n_crs + 1);
   mk_array.Clear (   );
   for (i = 1; i <= n_crs; i++) {
      float bes = (float)ne_array[i] / nb_array[i];
      if (bes < bes_thrsld) mk_array[i] = TRUE;
   }
   RemoveCRs (cr_map,mk_array,fg_image);
}

 void PerformLightDisturbanceReduction_IR (GImage &s_image,GImage &bg_image,int win_size,float min_ir,float max_ir,float max_irsd,GImage &fg_image)
 
{
   int x,y,x2,y2;
   
   int hw = win_size / 2;
   int ar = 2 * hw + 1;
   ar *= ar;
   int ex = s_image.Width  - hw;
   int ey = s_image.Height - hw;
   float max_irv = max_irsd * max_irsd;
   for (y = hw; y < ey; y++) {
      int sy2 = y   - hw;
      int ey2 = sy2 + win_size;
      byte* l_s_image  = s_image[y];
      byte* l_fg_image = fg_image[y];
      byte* l_bg_image = bg_image[y];
      for (x = hw; x < ex; x++) {
         if (l_fg_image[x]) {
            float ir = (float)l_s_image[x] / l_bg_image[x];
            if (min_ir <= ir && ir <= max_ir) {
               int sx2 = x   - hw;
               int ex2 = sx2 + win_size;
               float r = 0.0f, rr = 0.0f;
               for (y2 = sy2; y2 < ey2; y2++) {
                  byte* l_s_image2  = s_image[y2];
                  byte* l_bg_image2 = bg_image[y2];
                  for (x2 = sx2; x2 < ex2; x2++) {
                     ir  = (float)l_s_image2[x2] / l_bg_image2[x2];
                     r  += ir;
                     rr += ir * ir;
                  }
               }
               r /= ar;
               float irv = rr / ar - r * r;
               if (irv < max_irv) l_fg_image[x] = PG_BLACK;
            }
         }
      }
   }
} 

 void PerformLightDisturbanceReduction_NCC (GImage &s_image,GImage &bg_image,int win_size,float ncc_thrsld,GImage &fg_image)
 
{
   int   x,y,x2,y2;
   float sncc;
   
   int hw = win_size / 2;
   int ar = 2 * hw + 1;
   ar *= ar;
   int ex  = fg_image.Width  - hw;
   int ey  = fg_image.Height - hw;
   ncc_thrsld *= ncc_thrsld;
   for (y = hw; y < ey; y++) {
      int sy2 = y - hw;
      int ey2 = y + hw;
      byte* l_fg_image = fg_image[y];
      for (x = hw; x < ex; x++) {
         if (l_fg_image[x]) {
            int sx2 = x - hw;
            int ex2 = x + hw;
            int sum_b = 0, sum_s = 0, sum_bb = 0, sum_ss = 0, sum_bs = 0;
            for (y2 = sy2; y2 <= ey2; y2++) {
               byte* l_s_image  = s_image[y2];
               byte* l_bg_image = bg_image[y2];
               for (x2 = sx2; x2 <= ex2; x2++) {
                  int b = l_bg_image[x2];
                  int s = l_s_image[x2];
                  sum_b  += b;
                  sum_s  += s;
                  sum_bb += b * b;
                  sum_ss += s * s;
                  sum_bs += b * s;
               }
            }
            float mean_b  = (float)sum_b  / ar;
            float mean_s  = (float)sum_s  / ar;
            float mean_bb = (float)sum_bb / ar;
            float mean_ss = (float)sum_ss / ar;
            float mean_bs = (float)sum_bs / ar;
            float var_b   = mean_bb - mean_b * mean_b;
            float var_s   = mean_ss - mean_s * mean_s;
            float cov_bs  = mean_bs - mean_b * mean_s;
            float a = cov_bs * cov_bs;
            float b = var_b  * var_s;
            if (b > MC_VSV) sncc = a / b;
            else sncc = 0.0f;
            if (sncc >= ncc_thrsld) l_fg_image[x] = PG_BLACK;
         }
      }
   }
}

 void PerformLightDisturbanceReduction_TS (GImage &s_image,GImage &bg_image,float ts_thrsld,GImage &fg_image)

{
   int x,y;

   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   IArray2D ip_array(s_image.Width,s_image.Height);
   IArray2D sm_array(s_image.Width,s_image.Height);
   ip_array.Clear (   );
   sm_array.Clear (   );
   for (y = 1; y < ey; y++) {
      int y1 = y - 1;
      int y2= y + 1;
      byte* l_s_image   = s_image[y];
      byte* l_s_image1  = s_image[y1];
      byte* l_s_image2  = s_image[y2];
      byte* l_bg_image  = bg_image[y];
      byte* l_bg_image1 = bg_image[y1];
      byte* l_bg_image2 = bg_image[y2];
      byte* l_fg_image  = fg_image[y];
      int*  l_ip_array  = ip_array[y];
      int*  l_sm_array  = sm_array[y];
      for (x = 1; x < ex; x++) {
         if (l_fg_image[x]) {
            int x1  = x - 1;
            int x2  = x + 1;
            short gx1 = l_s_image2[x2]  + 2 * (short)l_s_image[x2]  + l_s_image1[x2]  -
                        l_s_image2[x1]  - 2 * (short)l_s_image[x1]  - l_s_image1[x1];
            short gy1 = l_s_image2[x2]  + 2 * (short)l_s_image2[x]  + l_s_image2[x1]  -
                        l_s_image1[x1]  - 2 * (short)l_s_image1[x]  - l_s_image1[x2];
            short gx2 = l_bg_image2[x2] + 2 * (short)l_bg_image[x2] + l_bg_image1[x2] -
                        l_bg_image2[x1] - 2 * (short)l_bg_image[x1] - l_bg_image1[x1];
            short gy2 = l_bg_image2[x2] + 2 * (short)l_bg_image2[x] + l_bg_image2[x1] -
                        l_bg_image1[x1] - 2 * (short)l_bg_image1[x] - l_bg_image1[x2];
            l_ip_array[x] = (int)gx1 * gx2 + (int)gy1 * gy2;
            l_sm_array[x] = (int)gx1 * gx1 + (int)gy1 * gy1 + (int)gx2 * gx2 + (int)gy2 * gy2;
         }
      }
   }
   for (y = 1; y < ey; y++) {
      int y1 = y - 1;
      int y2 = y + 1;
      int*  l_ip_array  = ip_array[y];
      int*  l_ip_array1 = ip_array[y1];
      int*  l_ip_array2 = ip_array[y2];
      int*  l_sm_array  = sm_array[y];
      int*  l_sm_array1 = sm_array[y1];
      int*  l_sm_array2 = sm_array[y2];
      byte* l_fg_image = fg_image[y];
      for (x = 1; x < ex; x++) {
         if (l_fg_image[x]) {
            int x1 = x - 1;
            int x2 = x + 1;
            int sum_n = l_ip_array1[x1] + l_ip_array1[x] + l_ip_array1[x2] + 
                        l_ip_array [x1] + l_ip_array [x] + l_ip_array [x2] + 
                        l_ip_array2[x1] + l_ip_array2[x] + l_ip_array2[x2];
            int sum_d = l_sm_array1[x1] + l_sm_array1[x] + l_sm_array1[x2] + 
                        l_sm_array [x1] + l_sm_array [x] + l_sm_array [x2] + 
                        l_sm_array2[x1] + l_sm_array2[x] + l_sm_array2[x2];
            float ts = 2.0f * (float)sum_n / sum_d;
            if (ts >= ts_thrsld) l_fg_image[x] = PG_BLACK;
         }
      }
   }
}

 void PrepareForLDR (GImage& s_image,GImage& d_image)

{
   int x,y;

   for (y = 0; y < s_image.Height; y++) {
      byte* l_s_image = s_image[y];
      byte* l_d_image = d_image[y];
      for (x = 0; x < s_image.Width; x++) {
         if (l_s_image[x]) l_d_image[x] = l_s_image[x];
         else l_d_image[x] = 1;
      }
   }
}

}

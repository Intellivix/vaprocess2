#include "vacl_conrgn.h"
#include "vacl_morph.h"
#include "vacl_videnhnc.h"

//#define __DEBUG_VEH

#if defined(__DEBUG_VEH)
#include "Win32CL/Win32CL.h"
extern int _DVX_VEH,_DVY_VEH;
extern CImageView* _DebugView_VEH;
#endif

 namespace VACL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Class: VideoEnhancement
//
///////////////////////////////////////////////////////////////////////////////

 VideoEnhancement::VideoEnhancement (   )
 
{
   CFRadius    = 6;
   MDThreshold = 15;
   FrameStep   = 1.0f / 15.0f;
   MAFSize     = 0.7f;

   _Init (   );
}

 VideoEnhancement::~VideoEnhancement (   )

{
   Close (   );
}

 void VideoEnhancement::_Init (   )

{
   Flag_Initialized = FALSE;
   FrameCount       = 0;
   FrameRate        = 0.0f;
   FrameSize.Clear (   );
}

 int VideoEnhancement::AddToFrameQueue (byte *si_buffer)
 
{
   int i;
   
   if (!Frames || !VFQueue) return (1);
   int buf_size = FrameSize.Width * FrameSize.Height * 2;
   if (FrameCount < VFQueue.Length) {
      Frames[FrameCount].Create (buf_size);
      memcpy ((byte*)Frames[FrameCount],si_buffer,buf_size);
      VFQueue[FrameCount] = &Frames[FrameCount];
      FrameCount++;
      return (DONE);
   }
   BArray1D *temp = VFQueue[0];
   for (i = 1; i < VFQueue.Length; i++) VFQueue[i - 1] = VFQueue[i];
   int n = VFQueue.Length - 1;
   VFQueue[n] = temp;
   memcpy ((byte*)(*VFQueue[n]),si_buffer,buf_size);
   FrameCount++;
   return (DONE);
}

 void VideoEnhancement::Close (   )
 
{
   Frames.Delete  (   );
   VFQueue.Delete (   );
   _Init (   );
}

 void VideoEnhancement::GetMotionImage (byte *si_buffer,GImage &d_image)
 
{
   int i,x,x2,y;
   
   d_image.Clear (   );
   int step_size  = FrameSize.Width * 2;
   int frame_step = (int)(FrameStep * FrameRate + 0.5f);
   if (frame_step < 1) frame_step = 1;
   for (i = 0; i < Frames.Length; i += frame_step) {
      byte* l_r_image = Frames[i];
      byte* l_s_image = si_buffer;
      for (y = 0; y < d_image.Height; y++) {
         byte* l_d_image = d_image[y];
         for (x = x2 = 0; x < d_image.Width; x++,x2 += 2) {
            if (abs ((short)l_r_image[x2] - l_s_image[x2]) > MDThreshold) l_d_image[x]++;
         }
         l_r_image += step_size;
         l_s_image += step_size;
      }
   }
   byte d_thrsld = 1;
   for (y = 0; y < d_image.Height; y++) {
      byte* l_d_image = d_image[y];
      for (x = 0; x < d_image.Width; x++) {
         if (l_d_image[x] >= d_thrsld) l_d_image[x] = PG_WHITE;
         else l_d_image[x] = PG_BLACK;
      }
   }
}

 void VideoEnhancement::Initialize (ISize2D& frm_size,float frm_rate)
 
{
   Close (   );
   FrameCount = 0;
   FrameSize = frm_size;
   FrameRate = frm_rate;
   int maf_size = (int)(FrameRate * MAFSize + 0.5f);
   if (maf_size < 1) maf_size = 1;
   Frames.Create   (maf_size);
   VFQueue.Create  (maf_size);
   SumArray.Create (frm_size.Width * frm_size.Height * 2);
   SumArray.Clear  (   );
   Flag_Initialized = TRUE;
}

 int VideoEnhancement::Perform_YUY2 (byte *si_buffer,byte *di_buffer)
 
{
   int i,x,y,x2;
   
   if (!IsInitialized (   )) return (1);
   #if defined(__DEBUG_VEH)
   _DVX_VEH = _DVY_VEH = 0;
   _DebugView_VEH->Clear (   );
   #endif
   int buf_size  = FrameSize.Width * FrameSize.Height * 2;
   if (FrameCount < Frames.Length) {
      ushort n = FrameCount + 1;
      for (i = 0; i < buf_size; i++) {
         SumArray[i] += si_buffer[i];
         di_buffer[i] = (byte)(SumArray[i] / n);
      }
   }
   else {
      ushort n = Frames.Length;
      byte *fi_buffer = *VFQueue[0];
      for (i = 0; i < buf_size; i++) {
         SumArray[i]  = SumArray[i] - fi_buffer[i] + si_buffer[i];
         di_buffer[i] = (byte)(SumArray[i] / n);
      }
      GImage m_image(FrameSize.Width,FrameSize.Height);
      GetMotionImage (si_buffer,m_image);
      #if defined(__DEBUG_VEH)
      _DebugView_VEH->DrawImage (m_image,_DVX_VEH,_DVY_VEH);
      _DVY_VEH += m_image.Height;
      _DebugView_VEH->DrawText (_T("Motion Detection Result"),_DVX_VEH,_DVY_VEH,PC_GREEN);
      _DVY_VEH += 20;
      #endif
      PerformMorphFiltering (m_image);
      #if defined(__DEBUG_VEH)
      _DebugView_VEH->DrawImage (m_image,_DVX_VEH,_DVY_VEH);
      _DVY_VEH += m_image.Height;
      _DebugView_VEH->DrawText (_T("Morphological Filtering Result"),_DVX_VEH,_DVY_VEH,PC_GREEN);
      _DVY_VEH += 20;
      #endif
      int step_size = FrameSize.Width * 2;
      byte *l_s_image = si_buffer;
      byte *l_d_image = di_buffer;
      for (y = 0; y < m_image.Height; y++) {
         byte *l_m_image = m_image[y];
         for (x = x2 = 0; x < m_image.Width; x++,x2 += 2) {
            if (l_m_image[x]) {
               l_d_image[x2    ] = l_s_image[x2    ];
               l_d_image[x2 + 1] = l_s_image[x2 + 1];
            }
         }
         l_s_image += step_size;
         l_d_image += step_size;
      }
   }
   AddToFrameQueue (si_buffer);
   #if defined(__DEBUG_VEH)
   _DebugView_VEH->Invalidate (   );
   #endif
   return (DONE);
}

 void VideoEnhancement::PerformMorphFiltering (GImage &s_image)
 
{
   GImage t_image(s_image.Width,s_image.Height);
   PerformBinErosion (s_image,1,t_image);
   #if defined(__DEBUG_VEH)
   _DebugView_VEH->DrawImage (t_image,_DVX_VEH,_DVY_VEH);
   _DVY_VEH += t_image.Height;
   _DebugView_VEH->DrawText (_T("Erosion Result"),_DVX_VEH,_DVY_VEH,PC_GREEN);
   _DVY_VEH += 20;
   #endif
   RemoveCRs (t_image,0,5,t_image);
   int se_radius = (int)(CFRadius * s_image.Height / 240.0f + 0.5f);
   if (se_radius < 1) se_radius = 1;
   PerformBinDilation (t_image,se_radius,s_image);
}

}

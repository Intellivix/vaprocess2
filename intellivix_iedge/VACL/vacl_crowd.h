#if !defined(__VACL_CROWD_H)
#define __VACL_CROWD_H

#include "vacl_realsize.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: CrowdZone
//
///////////////////////////////////////////////////////////////////////////////

 class CrowdZone

{
   public:
      EventZone* EvtZone;

   public:
      IBox2D CrowdRegion; // 검출된 군중 영역
   
   public:
      CrowdZone* Prev;
      CrowdZone* Next;

   public:
      CrowdZone (   );

   public:
      int DetectEvent (   );
};

typedef LinkedList<CrowdZone> CrowdZoneList;

///////////////////////////////////////////////////////////////////////////////
//
// Class: CrowdDensityEstimation
//
///////////////////////////////////////////////////////////////////////////////
  
 class CrowdDensityEstimation

{
   protected:
      int Flag_Enabled;
   
   public:
      float UpdateRate;
   
   public:
      float   FrameRate;
      ISize2D SrcImgSize;
   
   public:
      CrowdZoneList CrowdZones;

   public:
      CrowdDensityEstimation (   );
      virtual ~CrowdDensityEstimation (   );

   public:
      virtual void Close      (   );
      virtual int  Initialize (EventDetection& evt_detector);

   public:
      void Enable     (int flag_enable);
      int  IsEnabled  (   );
};

 inline void CrowdDensityEstimation::Enable (int flag_enable)

{
   Flag_Enabled = flag_enable;
}

 inline int CrowdDensityEstimation::IsEnabled (   )

{
   return (Flag_Enabled);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CrowdDensityEstimation_OBS
//
///////////////////////////////////////////////////////////////////////////////
//
// OBS: Object Segmentation
//

 class CrowdDensityEstimation_OBS : public CrowdDensityEstimation

{
   public:
      CrowdDensityEstimation_OBS (   );   
   
   protected:
      int Perform (ForegroundDetection& frg_detector,CrowdZone* zone);
   
   public:
      int Perform (ForegroundDetection& frg_detector);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: CrowdDensityEstimation_FPD
//
///////////////////////////////////////////////////////////////////////////////
//
// FPD: Feature Point Detection
//
  
 class CrowdDensityEstimation_FPD : public CrowdDensityEstimation

{
   public:
      int   MaxHFPWidth;
      int   HFPNMSSize;
      float HPVThreshold;
      float HumanHeight;

   public:
      float   FrameRate;
      ISize2D SrcImgSize;

   public:
      CrowdZoneList CrowdZones;

   public:
      CrowdDensityEstimation_FPD (   );

   protected:
      int CheckHeadPosition         (GImage& s_image,IPoint2D& hd_pos,int hd_size,IPoint2D& ft_pos);
      int GetHeadCandidatePositions (GImage& fg_image,IP2DArray1D& fp_array,RealObjectSizeEstimation& ros_estimator,IP2DArray1D& d_hp_array,IArray1D& d_hs_array);
      int Perform                   (IP2DArray1D& hp_array,IArray1D& hs_array,CrowdZone* zone);

   public:
      int Perform (ForegroundDetection& frg_detector,RealObjectSizeEstimation& ros_estimator);
};

}

#endif

#if !defined(__VACL_DEFINE_H)
#define __VACL_DEFINE_H

#include "bccl.h"
#include "vacl_environ.h"

#define CLSID_ForegroundDetection                  0x00000100
#define CLSID_ForegroundDetection_BS               0x00000101
#define CLSID_ForegroundDetection_SGBM             0x00000102
#define CLSID_ForegroundDetection_SGBM_MT          0x00000103
#define CLSID_ForegroundDetection_MGBM             0x00000104
#define CLSID_ForegroundDetection_FAD              0x00000105
#define CLSID_ForegroundDetection_FABS             0x00000106
#define CLSID_ForegroundDetection_Underwater       0x00000107
#define CLSID_ForegroundDetection_HMD_OHV_MBS      0x00000108
#define CLSID_ForegroundDetection_HMD_OHV_3DM      0x00000109
#define CLSID_ForegroundDetection_FCD_Seeta        0x0000010A
#define CLSID_ForegroundDetection_FCD_OpenVINO     0x0000010B
#define CLSID_ForegroundDetection_OBD_YOLO         0x0000010C
#define CLSID_ForegroundDetectionClient_OBD_YOLO   0x0000010D

#endif

#include "vacl_conrgn.h"
#include "vacl_filter.h"
#include "vacl_profile.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: BoundaryPixelTracking
//
///////////////////////////////////////////////////////////////////////////////

 BoundaryPixelTracking::BoundaryPixelTracking (   )

{
   _Init (   );
}

 BoundaryPixelTracking::~BoundaryPixelTracking (   )

{
   Close (   );
}

 void BoundaryPixelTracking::Close (   )
 
{
   CRBMap.Delete (   );
   _Init (   );
}

 void BoundaryPixelTracking::_Init (   )

{
   X                 = 0;
   Y                 = 0;
   SX                = 0;
   SY                = 0;
   Margin            = 1;
   Direction         = 0;
   CRMap             = NULL;
   NumBoundaryPixels = 0;
   NumMarginalPixels = 0;
}

 int BoundaryPixelTracking::Initialize (IArray2D &cr_map,int margin)

{
   Margin = margin;
   CRMap  = &cr_map;
   CRBMap.Create (cr_map.Width,cr_map.Height);
   CRBMap.Clear  (   );
   return (DONE);
}

 int BoundaryPixelTracking::Perform (int sx,int sy)
 
{
   if (CRMap == NULL) return (1);
   SX = X = sx;
   SY = Y = sy;
   Direction = 0;
   NumBoundaryPixels = 0;
   NumMarginalPixels = 0;
   if (StartTracking (   )) return (2);
   int mx1 = Margin;
   int mx2 = CRMap->Width  - Margin;
   int my1 = Margin;
   int my2 = CRMap->Height - Margin;
   int count = 0;
   int** cr_map = *CRMap;
   int lb = cr_map[sy][sx];
   while (TRUE) {
      if (cr_map[Y][X] == lb) {
         if (!CRBMap[Y][X] && !(cr_map[Y][X - 1] == lb && cr_map[Y][X + 1] == lb && cr_map[Y - 1][X] == lb && cr_map[Y + 1][X] == lb)) {
            if (mx1 <= X && X < mx2 && my1 <= Y && Y < my2) {
               NumBoundaryPixels++;
               ProcessBoundaryPixel (   );
            }
            else {
               NumMarginalPixels++;
               ProcessMarginalPixel (   );
            }
            CRBMap[Y][X] = lb;
         }
         Direction = (Direction + 1) % 4;
      }
      else {
         Direction--;
         if (Direction < 0) Direction = 3;
      }
      switch (Direction) {
         case 0:
            X++;
            break;
         case 1:
            Y--;
            break;
         case 2:
            X--;
            break;
         case 3:
            Y++;
            break;
         default:
            break;
      }
      if (X == SX && Y == SY) {
         if (count == 2) break;
         else count++;
      }
   }
   EndTracking (   );
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: ConnectedRegion
//
///////////////////////////////////////////////////////////////////////////////

 ConnectedRegion::ConnectedRegion (   )
 
{
   Clear (   );
}

 void ConnectedRegion::Clear (   )
 
{
   IBox2D::Clear (   );
   Area           = 0;
   Correspondence = 0;
   SX             = 0;
   SY             = 0;
   CX             = 0.0f;
   CY             = 0.0f;
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 void ConvertCRMap (IArray2D &s_cr_map,IArray1D &cv_table,IArray2D &d_cr_map)

{
   int x,y;
   
   for (y = 0; y < s_cr_map.Height; y++) {
      int* l_s_cr_map = s_cr_map[y];
      int* l_d_cr_map = d_cr_map[y];
      for (x = 0; x < s_cr_map.Width; x++) {
         if (l_s_cr_map[x]) l_d_cr_map[x] = cv_table[l_s_cr_map[x]];
         else l_d_cr_map[x] = 0;
      }
   }
}

 void CopyCR (IArray2D& s_cr_map,IBox2D& s_rgn,int s_cr_no,IArray2D& d_cr_map,int d_cr_no)

{
   int x,y;

   int sx = s_rgn.X;
   int sy = s_rgn.Y;   
   int ex = sx + s_rgn.Width;
   int ey = sy + s_rgn.Height;
   for (y = sy; y < ey; y++) {
      int* l_s_cr_map = s_cr_map[y];
      int* l_d_cr_map = d_cr_map[y];
      for (x = sx; x < ex; x++) {
         if (l_s_cr_map[x] == s_cr_no) l_d_cr_map[x] = d_cr_no;
      }
   }
}

 void CopyCR (IArray2D& s_cr_map,IBox2D& s_rgn,int s_cr_no,GImage& d_image,byte d_pv)

{
   int x,y;

   int sx = s_rgn.X;
   int sy = s_rgn.Y;   
   int ex = sx + s_rgn.Width;
   int ey = sy + s_rgn.Height;
   for (y = sy; y < ey; y++) {
      int*  l_s_cr_map = s_cr_map[y];
      byte* l_d_image  = d_image[y];
      for (x = sx; x < ex; x++) {
         if (l_s_cr_map[x] == s_cr_no) l_d_image[x] = d_pv;
      }
   }
}

 void CopyCR (IArray2D& s_cr_map,IBox2D& s_rgn,int s_cr_no,IArray2D& d_cr_map,IPoint2D& d_pos,int d_cr_no)

{
   int x1,y1,x2,y2;

   int sx1 = s_rgn.X;
   int sy1 = s_rgn.Y;   
   int sx2 = d_pos.X;
   int sy2 = d_pos.Y;
   int ex2 = sx2 + s_rgn.Width;
   int ey2 = sy2 + s_rgn.Height;
   if (sx2 < 1) sx2 = 1;
   if (sy2 < 1) sy2 = 1;
   if (ex2 > d_cr_map.Width  - 1) ex2 = d_cr_map.Width  - 1;
   if (ey2 > d_cr_map.Height - 1) ey2 = d_cr_map.Height - 1;
   sx1 += sx2 - d_pos.X;
   sy1 += sy2 - d_pos.Y;
   for (y1 = sy1,y2 = sy2; y2 < ey2; y1++,y2++) {
      int* l_s_cr_map = s_cr_map[y1];
      int* l_d_cr_map = d_cr_map[y2];
      for (x1 = sx1,x2 = sx2; x2 < ex2; x1++,x2++) {
         if (l_s_cr_map[x1] == s_cr_no) l_d_cr_map[x2] = d_cr_no;
      }
   }
}

 void CopyCR (IArray2D& s_cr_map,IBox2D& s_rgn,int s_cr_no,GImage& d_image,IPoint2D& d_pos,byte d_pv)

{
   int x1,y1,x2,y2;

   int sx1 = s_rgn.X;
   int sy1 = s_rgn.Y;
   int sx2 = d_pos.X;
   int sy2 = d_pos.Y;
   int ex2 = sx2 + s_rgn.Width;
   int ey2 = sy2 + s_rgn.Height;
   if (sx2 < 1) sx2 = 1;
   if (sy2 < 1) sy2 = 1;
   if (ex2 > d_image.Width  - 1) ex2 = d_image.Width  - 1;
   if (ey2 > d_image.Height - 1) ey2 = d_image.Height - 1;
   sx1 += sx2 - d_pos.X;
   sy1 += sy2 - d_pos.Y;
   for (y1 = sy1,y2 = sy2; y2 < ey2; y1++,y2++) {
      int*  l_s_cr_map = s_cr_map[y1];
      byte* l_d_image  = d_image[y2];
      for (x1 = sx1,x2 = sx2; x2 < ex2; x1++,x2++) {
         if (l_s_cr_map[x1] == s_cr_no) l_d_image[x2] = d_pv;
      }
   }
}

 void FindMaxCRArea (CRArray1D& cr_array,int& d_cr_area,int& d_cr_no)

{
   int i;

   d_cr_no   = 0;
   d_cr_area = 0;
   for (i = 1; i < cr_array.Length; i++) {
      if (cr_array[i].Area > d_cr_area) {
         d_cr_area = cr_array[i].Area;
         d_cr_no   = i;
      }
   }
}

 int FindMaxOverlappedCR (IArray2D& cr_map,IBox2D& s_rgn,int n_crs,int& d_ov_area)

{
   int x,y;

   IArray1D n_array(n_crs + 1);
   n_array.Clear (   );
   int ex = s_rgn.X + s_rgn.Width;
   int ey = s_rgn.Y + s_rgn.Height;
   for (y = s_rgn.Y; y < ey; y++) {
      int* l_cr_map = cr_map[y];
      for (x = s_rgn.X; x < ex; x++) {
         if (l_cr_map[x]) n_array[l_cr_map[x]]++;
      }
   }
   int d_cr_no = 0;
   FindMaximum (n_array,d_ov_area,d_cr_no);
   return (d_cr_no);
}

 int FindMaxOverlappedCR (IArray2D& s_cr_map,IBox2D& s_rgn,int s_cr_no,IArray2D& t_cr_map,int n_tcrs,int& d_ov_area)

{
   int x,y;
   
   IArray1D n_array(n_tcrs + 1);
   n_array.Clear (   );
   int ex = s_rgn.X + s_rgn.Width;
   int ey = s_rgn.Y + s_rgn.Height;
   for (y = s_rgn.Y; y < ey; y++) {
      int* l_s_cr_map = s_cr_map[y];
      int* l_t_cr_map = t_cr_map[y];
      for (x = s_rgn.X; x < ex; x++) {
         if (l_s_cr_map[x] == s_cr_no) {
            if (l_t_cr_map[x]) n_array[l_t_cr_map[x]]++;
         }
      }
   }
   int d_cr_no = 0;
   FindMaximum (n_array,d_ov_area,d_cr_no);
   return (d_cr_no);
}

 int GetCRArea (IArray2D& cr_map,IBox2D& s_rgn,int cr_no)

{
   int x,y;

   int n  = 0;
   int ex = s_rgn.X + s_rgn.Width;
   int ey = s_rgn.Y + s_rgn.Height;
   for (y = s_rgn.Y; y < ey; y++) {
      int* l_cr_map = cr_map[y];
      for (x = s_rgn.X; x < ex; x++) {
         if (l_cr_map[x] == cr_no) n++;
      }
   }
   return (n);
}

 float GetCRDensity (IArray2D& cr_map,IBox2D& s_rgn,int cr_no)

{
   IBox2D  c_rgn = s_rgn;
   ISize2D c_size(cr_map.Width,cr_map.Height);
   CropRegion (c_size,c_rgn);
   int area = GetCRArea (cr_map,c_rgn,cr_no);
   float density = (float)area / (s_rgn.Width * s_rgn.Height);
   return (density);
}

 int GetCRID (IArray2D& cr_map,IBox2D& s_rgn)

{
   int x,y;
   
   int cx = s_rgn.X + s_rgn.Width  / 2;
   int cy = s_rgn.Y + s_rgn.Height / 2;
   int cr_no = cr_map[cy][cx];
   if (cr_no) return (cr_no);
   for (x = cx - 1; x >= s_rgn.X; x--) {
      cr_no = cr_map[cy][x];
      if (cr_no) return (cr_no);
   }
   int ex = s_rgn.X + s_rgn.Width;
   for (x = cx + 1; x < ex; x++) {
      cr_no = cr_map[cy][x];
      if (cr_no) return (cr_no);
   }
   for (y = cy - 1; y >= s_rgn.Y; y--) {
      cr_no = cr_map[y][cx];
      if (cr_no) return (cr_no);
   }
   int ey = s_rgn.Y + s_rgn.Height;
   for (y = cy + 1; y < ey; y++) {
      cr_no = cr_map[y][cx];
      if (cr_no) return (cr_no);
   }
   return (0);
}

 void GetCRInfo (IArray2D& cr_map,CRArray1D &d_array)

{
   int i,x,y;
   struct _CR {
      int N;
      int BX,BY;
      int SX,SY;
      int EX,EY;
      int SumX;
      int SumY;
      int SumXX;
      int SumYY;
   };

   if (!d_array) return;
   d_array.Clear (   );
   Array1D<_CR> cr_array(d_array.Length);
   for (i = 1; i < cr_array.Length; i++) {
      _CR &cr = cr_array[i];
      cr.N    = 0;
      cr.BX   = -1;
      cr.BY   = -1;
      cr.SX   = cr_map.Width;
      cr.SY   = cr_map.Height;
      cr.EX   = -1;
      cr.EY   = -1;
      cr.SumX = 0;
      cr.SumY = 0;
   }
   for (y = 0; y < cr_map.Height; y++) {
      int* l_cr_map = cr_map[y];
      for (x = 0; x < cr_map.Width; x++) {
         if (l_cr_map[x] > 0) {
            _CR &cr = cr_array[l_cr_map[x]];
            cr.N++;
            if (cr.BX < 0) cr.BX = x, cr.BY = y;
            if (cr.SX > x) cr.SX = x;
            if (cr.SY > y) cr.SY = y;
            if (cr.EX < x) cr.EX = x;
            if (cr.EY < y) cr.EY = y;
            cr.SumX += x;
            cr.SumY += y;
         }
      }
   }
   for (i = 1; i < d_array.Length; i++) {
      _CR& s_cr = cr_array[i];
      if (s_cr.N) {
         ConnectedRegion &d_cr = d_array[i];
         d_cr.X      = s_cr.SX;
         d_cr.Y      = s_cr.SY;
         d_cr.Width  = s_cr.EX - s_cr.SX + 1;
         d_cr.Height = s_cr.EY - s_cr.SY + 1;
         d_cr.Area   = s_cr.N;
         d_cr.SX     = s_cr.BX;
         d_cr.SY     = s_cr.BY;
         d_cr.CX     = (float)s_cr.SumX / s_cr.N;
         d_cr.CY     = (float)s_cr.SumY / s_cr.N;
      }
   }
}

 void GetCRMap (GImage &s_image,int min_area,int max_area,IArray2D &cr_map,CRArray1D &cr_array,int option)

{
   int i,j,n;
   int x,y;
   
   int n_crs = LabelCRs (s_image,cr_map,option);
   if (!n_crs) {
      cr_array.Create (1);
      cr_array[0].Clear (   );
      return;
   }
   CRArray1D cr_info(n_crs + 1);
   GetCRInfo (cr_map,cr_info);
   IArray1D mk_array(cr_info.Length);
   mk_array.Clear (   );
   for (i = 1; i < cr_info.Length; i++) {
      if (cr_info[i].Area < min_area || cr_info[i].Area > max_area) mk_array[i] = TRUE;
   }
   for (i = 1,n = 0; i < mk_array.Length; i++) if (!mk_array[i]) n++;
   cr_array.Create (n + 1);
   cr_array[0].Clear (   );
   for (i = j = 1; i < mk_array.Length; i++) {
      if (!mk_array[i]) {
         mk_array[i] = j;
         cr_array[j] = cr_info[i];
         j++;
      }
      else mk_array[i] = 0;
   }
   for (y = 0; y < cr_map.Height; y++) {
      byte* l_s_image = s_image[y];
      int*  l_cr_map  = cr_map[y];
      for (x = 0; x < cr_map.Width; x++) {
         if (l_cr_map[x]) {
            l_cr_map[x] = mk_array[l_cr_map[x]];
            if (!l_cr_map[x]) l_s_image[x] = PG_BLACK;
         }
      }
   }
}

 float GetCRMean (GImage& s_image,IArray2D& cr_map,IBox2D& s_rgn,int cr_no,int& d_num,int64& d_sum)

{
   int x,y;

   int sx = s_rgn.X;
   int sy = s_rgn.Y;
   int ex = sx + s_rgn.Width;
   int ey = sy + s_rgn.Height;
   d_num = 0,d_sum = 0;
   for (y = sy; y < ey; y++) {
      byte* l_s_image = s_image[y];
      int*  l_cr_map  = cr_map[y];
      for (x = sx; x < ex; x++) {
         if (l_cr_map[x] == cr_no) {
            d_sum += l_s_image[x];
            d_num++;
         }
      }
   }
   float mean = 0.0f;
   if (d_num) mean = (float)((double)d_sum / d_num);
   return (mean);
}

 int GetCROverlapArea (IArray2D& cr_map,IBox2D& s_rgn,int cr_no,GImage& b_image)

{
   int x,y;
   
   int sx = s_rgn.X;
   int sy = s_rgn.Y;
   int ex = sx + s_rgn.Width;
   int ey = sy + s_rgn.Height;
   int ov_area = 0;
   for (y = sy; y < ey; y++) {
      int*  l_cr_map  = cr_map[y];
      byte* l_b_image = b_image[y];
      for (x = sx; x < ex; x++) {
         if (l_cr_map[x] == cr_no && l_b_image[x]) ov_area++;
      }
   }
   return (ov_area);
}

 void GetCRPrincipalAxes (IArray2D& cr_map,IBox2D& s_rgn,int cr_no,FPoint2D& mj_axis,FPoint2D& mn_axis,float& mja_length,float& mna_length)

{
   int x,y;
   
   int sx = s_rgn.X;
   int sy = s_rgn.Y;
   int ex = sx + s_rgn.Width;
   int ey = sy + s_rgn.Height;
   uint  sum_x  = 0,sum_y  = 0;
   int64 sum_xx = 0,sum_yy = 0,sum_xy = 0;
   int n = 0;
   for (y = sy; y < ey; y++) {
      int* l_cr_map = cr_map[y];
      uint yy = y * y;
      for (x = sx; x < ex; x++) {
         if (l_cr_map[x] == cr_no) {
            n++;
            sum_x  += x;
            sum_y  += y;
            sum_xx += x * x;
            sum_xy += x * y;
            sum_yy += yy;
         }
      }
   }
   Matrix mat_C(2,2),vec_m(2);
   vec_m(0)    = (double)sum_x  / n;
   vec_m(1)    = (double)sum_y  / n;
   mat_C[0][0] = (double)sum_xx / n;
   mat_C[0][1] = (double)sum_xy / n;
   mat_C[1][1] = (double)sum_yy / n;
   mat_C[1][0] = mat_C[0][1];
   mat_C = mat_C - vec_m * Tr(vec_m);
   Matrix mat_E(2,2),vec_e(2);
   EigenDecomp (mat_C,vec_e,mat_E);
   if (mat_E[1][0] < 0.0f) mat_E = -mat_E;
   if (vec_e(0) > MC_VSV && vec_e(1) > MC_VSV) {
      mja_length = 4.0f * (float)sqrt (vec_e(0));
      mna_length = 4.0f * (float)sqrt (vec_e(1));
      mj_axis.X  = (float)mat_E[0][0];
      mj_axis.Y  = (float)mat_E[1][0];
      mn_axis.X  = (float)mat_E[0][1];
      mn_axis.Y  = (float)mat_E[1][1];
   }
   else {
      if (s_rgn.Width > s_rgn.Height) {
         mja_length = (float)s_rgn.Width;
         mna_length = (float)s_rgn.Height;
         mj_axis(1.0f,0.0f);
         mn_axis(0.0f,1.0f);
      }
      else {
         mja_length = (float)s_rgn.Height;
         mna_length = (float)s_rgn.Width;
         mj_axis(0.0f,1.0f);
         mn_axis(1.0f,0.0f);
      }
   }
}

 void InitEquivalenceTable (ETElement* eq_table,int length)

{
   int i;
   
   for (i = 0; i < length; i++) {
      eq_table[i].MinLabelNo = i;
      eq_table[i].Next       = 0;
   }
}

 int LabelCRs (GImage &s_image,IArray2D &d_array,int option)

{
   int i,j,i1,j1;

   int c_label_no = 1;
   int ei = s_image.Height - 1;
   int ej = s_image.Width  - 1;
   s_image.SetBoundary (1,0);
   d_array.Clear (   );
   BArray1D et_buffer(s_image.Width * s_image.Height * sizeof(ETElement) / 2);
   ETElement* _RESTRICT eq_table = (ETElement*)(byte*)et_buffer;
   eq_table[0].MinLabelNo = eq_table[0].Next = 0;
   if (option == CNB_FOUR) {
      for (i = 1,i1 = 0; i < ei; i++,i1++) {
         byte* l_s_image  = s_image[i];
         byte* l_s_image1 = s_image[i1];
         int*  l_d_array  = d_array[i];
         int*  l_d_array1 = d_array[i1];
         for (j = 1,j1 = 0; j < ej; j++,j1++) {
            if (l_s_image[j]) {
               if (l_s_image[j1] == l_s_image[j]) {
                  l_d_array[j] = l_d_array[j1];
                  if (l_s_image[j] == l_s_image1[j]) UpdateEquivalenceTable (eq_table,l_d_array[j],l_d_array1[j]);
               }
               else if (l_s_image1[j] == l_s_image[j]) l_d_array[j] = l_d_array1[j];
               else {
                  eq_table[c_label_no].MinLabelNo = c_label_no;
                  eq_table[c_label_no].Next = 0;
                  l_d_array[j] = c_label_no;
                  c_label_no++;
               }
            }
         }
      }
   }
   else {
      for (i = 1,i1 = 0; i < ei; i++,i1++) {
         byte* l_s_image  = s_image[i];
         byte* l_s_image1 = s_image[i1];
         int*  l_d_array  = d_array[i];
         int*  l_d_array1 = d_array[i1];
         for (j = 1,j1 = 0; j < ej; j++,j1++) {
            if (l_s_image[j1] == l_s_image1[j] && l_d_array[j1] != l_d_array1[j]) UpdateEquivalenceTable (eq_table,l_d_array[j1],l_d_array1[j]);
            if (l_s_image[j]) {
               if      (l_s_image[j1]  == l_s_image[j]) l_d_array[j] = l_d_array[j1];
               else if (l_s_image1[j1] == l_s_image[j]) l_d_array[j] = l_d_array1[j1];
               else if (l_s_image1[j ] == l_s_image[j]) l_d_array[j] = l_d_array1[j ];
               else {
                  eq_table[c_label_no].MinLabelNo = c_label_no;
                  eq_table[c_label_no].Next = 0;
                  l_d_array[j] = c_label_no;
                  c_label_no++;
               }
            }
         }
      }
   }
   IArray1D conv(c_label_no);
   conv.Clear (   );
   for (i = 1; i < c_label_no; i++) conv[eq_table[i].MinLabelNo] = TRUE;
   int n_labels = 1;
   for (i = 1; i < c_label_no; i++) {
      if (conv[i]) {
         conv[i] = n_labels;
         n_labels++;
      }
   }
   n_labels--;
   for (i = 1; i < c_label_no; i++) eq_table[i].MinLabelNo = conv[eq_table[i].MinLabelNo];
   for (i = 1; i < ei; i++) {
      int *_RESTRICT l_d_array = d_array[i];
      for (j = 1; j < ej; j++)
         l_d_array[j] = eq_table[l_d_array[j]].MinLabelNo;
   }
   return (n_labels);
}

 void PutCR (IArray2D& cr_map,IBox2D& s_rgn,int cr_no,int flag_overwrite)

{
   int x,y;
 
   int sx = s_rgn.X;
   int sy = s_rgn.Y;
   int ex = sx + s_rgn.Width;
   int ey = sy + s_rgn.Height;
   CropRegion (cr_map.Width,cr_map.Height,sx,sy,ex,ey);
   if (flag_overwrite) {
      for (y = sy; y < ey; y++) {
         int* l_cr_map = cr_map[y];
         for (x = sx; x < ex; x++) {
            l_cr_map[x] = cr_no;
         }
      }
   }
   else {
      for (y = sy; y < ey; y++) {
         int* l_cr_map = cr_map[y];
         for (x = sx; x < ex; x++) {
            if (!l_cr_map[x]) l_cr_map[x] = cr_no;
         }
      }
   }
}

 void RemoveCR (GImage &s_image,IArray2D &cr_map,IBox2D &s_rgn,int cr_no)

{
   int x,y;
   
   int sx = s_rgn.X;
   int sy = s_rgn.Y;
   int ex = s_rgn.X + s_rgn.Width;
   int ey = s_rgn.Y + s_rgn.Height;
   for (y = sy; y < ey; y++) {
      int*  l_cr_map  = cr_map[y];
      byte* l_s_image = s_image[y];
      for (x = sx; x < ex; x++) {
         if (l_cr_map[x] == cr_no) l_s_image[x] = PG_BLACK;
      }
   }
}

 int RemoveCRs (IArray2D& cr_map,BArray1D& mk_array)

{
   int i,n;
   
   IArray1D cv_table(mk_array.Length);
   cv_table.Clear (   );
   for (i = n = 1; i < mk_array.Length; i++) {
      if (!mk_array[i]) cv_table[i] = n++;
   }
   ConvertCRMap (cr_map,cv_table,cr_map);
   return (n - 1);
}

 void RemoveCRs (IArray2D& cr_map,BArray1D& mk_array,GImage& d_image)

{
   int x,y;

   mk_array[0] = TRUE;
   for (y = 0; y < cr_map.Height; y++) {
      int*  l_cr_map  = cr_map[y];
      byte* l_d_image = d_image[y];
      for (x = 0; x < cr_map.Width; x++) {
         if (l_cr_map[x]) {
            if (mk_array[l_cr_map[x]]) l_d_image[x] = PG_BLACK;
            else l_d_image[x] = PG_WHITE;
         }
         else l_d_image[x] = PG_BLACK;
      }
   }
}

 void RemoveCRs (GImage& s_image,int min_area,int max_area,GImage& d_image)
 
{
   int i;
   
   IArray2D  cr_map(s_image.Width,s_image.Height);
   int n_crs = LabelCRs (s_image,cr_map);
   if (!n_crs) {
      d_image.Clear (   );
      return;
   }
   CRArray1D cr_array(n_crs + 1);
   GetCRInfo (cr_map,cr_array);
   BArray1D rm_array(cr_array.Length);
   rm_array.Clear (   );
   for (i = 1; i < cr_array.Length; i++)
      if (min_area <= cr_array[i].Area && cr_array[i].Area <= max_area) rm_array[i] = TRUE;
   RemoveCRs (cr_map,rm_array,d_image);
}

 void RemoveImpulsiveNoise (GImage &s_image,int n_nps,GImage& d_image)

{
   int n,x,y;
   
   d_image = s_image;
   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   if (!n_nps) { // Isolated Pixel만 제거하려는 경우
      for (y = 1; y < ey; y++) {
         byte* l_s_image  = s_image[y];
         byte* l_s_image1 = s_image[y - 1];
         byte* l_s_image2 = s_image[y + 1];
         byte* l_d_image  = d_image[y];
         for (x = 1; x < ex; x++) {
            if (l_s_image[x]) {
               int xm1 = x - 1;
               int xp1 = x + 1;
               if (l_s_image1[xm1]) continue;
               if (l_s_image1[x  ]) continue;
               if (l_s_image1[xp1]) continue;
               if (l_s_image [xm1]) continue;
               if (l_s_image [xp1]) continue;
               if (l_s_image2[xm1]) continue;
               if (l_s_image2[x  ]) continue;
               if (l_s_image2[xp1]) continue;
               l_d_image[x] = PG_BLACK;
            }
         }
      }
   }
   else {
      for (y = 1; y < ey; y++) {
         byte* l_s_image  = s_image[y];
         byte* l_s_image1 = s_image[y - 1];
         byte* l_s_image2 = s_image[y + 1];
         byte* l_d_image  = d_image[y];
         for (x = 1; x < ex; x++) {
            if (l_s_image[x]) {
               n = 0;
               int xm1 = x - 1;
               int xp1 = x + 1;
               if (l_s_image1[xm1]) n++;
               if (l_s_image1[x  ]) n++;
               if (l_s_image1[xp1]) n++;
               if (l_s_image [xm1]) n++;
               if (l_s_image [xp1]) n++;
               if (l_s_image2[xm1]) n++;
               if (l_s_image2[x  ]) n++;
               if (l_s_image2[xp1]) n++;
               if (n <= n_nps) l_d_image[x] = PG_BLACK;
            }
         }
      }
   }
}

 int TrimCR (IArray2D& cr_map,IBox2D& s_rgn,int cr_no,float h_thrsld,float v_thrsld,IBox2D& d_rgn)

{
   d_rgn = s_rgn;
   float gf_size = 0.1f * GetMinimum (s_rgn.Width,s_rgn.Height); // PARAMETERS
   if (gf_size < 1.0f) gf_size = 1.0f;
   // 수직 투영을 통해 영역의 수평 범위를 알아낸다.   
   FArray1D p_array(s_rgn.Width);
   VerticalProjection (cr_map,s_rgn,cr_no,p_array);
   FArray1D f_array(p_array.Length);
   GaussianFiltering (p_array,gf_size,f_array);
   float thrsld = GetMean (f_array) * h_thrsld;
   RPArray rp_array;
   GetRectPulses (f_array,thrsld,rp_array);
   if (rp_array.Length < 1) return (1);
   int max_w,max_i;
   FindMaxWidth (rp_array,max_w,max_i);
   d_rgn.X     = rp_array[max_i].StartPos + s_rgn.X;
   d_rgn.Width = rp_array[max_i].Width;
   // 수평 투영을 통해 영역의 수직 범위를 알아낸다.
   p_array.Create (s_rgn.Height);
   HorizontalProjection (cr_map,s_rgn,cr_no,p_array);
   f_array.Create (p_array.Length);
   GaussianFiltering (p_array,gf_size,f_array);
   thrsld = GetMean (f_array) * v_thrsld;
   GetRectPulses (f_array,thrsld,rp_array);
   if (rp_array.Length < 1) return (2);
   FindMaxWidth (rp_array,max_w,max_i);
   d_rgn.Y      = rp_array[max_i].StartPos + s_rgn.Y;
   d_rgn.Height = rp_array[max_i].Width;
   return (DONE);
}

 void UpdateEquivalenceTable (ETElement* eq_table,int a,int b)

{
   int c,d,e,f;

   if (eq_table[a].MinLabelNo == eq_table[b].MinLabelNo) return;
   if (eq_table[a].MinLabelNo < eq_table[b].MinLabelNo) c = eq_table[a].MinLabelNo, d = eq_table[b].MinLabelNo;
   else c = eq_table[b].MinLabelNo, d = eq_table[a].MinLabelNo;
   for (e = c;   ;   ) {
      f = eq_table[e].Next;
      if (!f) break;
      else e = f;
   }
   eq_table[e].Next = d;
   for (e = d;   ;   ) {
      eq_table[e].MinLabelNo = c;
      f = eq_table[e].Next;
      if (!f) break;
      else e = f;
   }
}

}

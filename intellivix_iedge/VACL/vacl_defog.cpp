#include "vacl_defog.h"
#include "vacl_edge.h"
#include "vacl_filter.h"
#include "vacl_ipp.h"
#include "vacl_morph.h"

#if defined(__DEBUG_VDF)
#include "Win32CL/Win32CL.h"
extern int _DVX_VDF,_DVY_VDF;
extern CImageView* _DebugView_VDF;
#endif

 namespace VACL

{

////////////////////////////////////////////////////////////////////////////////
// 
// Class: VideoDefogging
//
////////////////////////////////////////////////////////////////////////////////
 
 VideoDefogging::VideoDefogging (   )
 
{
   FrameCount           = 0;
   FrameRate            = 0.0f;
   AtmosphericLight     = 256;

   BFSize               = 20;
   NSPNSize             = 3;
   ALDThreshold         = 15;
   EDThreshold          = 30;
   ALEPeriod            = 6.0f / 30.0f;
   ALEThreshold         = 0.95f;
   SkyRegionRange       = 0.2f;

   GammaCorrectionParam = 1.3f;
   MinTransmission      = 0.25f;
}

 void VideoDefogging::Close (   )
 
{
   StrElement.Delete      (   );
   TransmissionMap.Delete (   );
   Flag_Initialized = FALSE;
}

 void VideoDefogging::CreateStrElement (   )
 
{
   int nspn_size = (int)(NSPNSize / 240.0f * FrameSize.Height + 0.5f);
   nspn_size = 2 * nspn_size + 1;
   StrElement.Create (nspn_size,nspn_size);
   GetBinStrElement (StrElement);
}

 void VideoDefogging::EstimateAtmosphericLight (GImage &sr_image)
 
{
   int x,y;

   GImage s_image(sr_image.Width,sr_image.Height);
   GaussianFiltering_3x3 (sr_image,s_image);
   #if defined(__DEBUG_VDF)
   _DVX_VDF = _DVY_VDF = 0;
   _DebugView_VDF->Clear (   );
   _DebugView_VDF->DrawImage (s_image,_DVX_VDF,_DVY_VDF);
   _DVY_VDF += s_image.Height;
   #endif
   GImage e_image(s_image.Width,s_image.Height);
   // 에지 픽셀 및 그 근방의 픽셀들을 검출한다.
   DetectEdges_Sobel (s_image,(short)EDThreshold,e_image);
   e_image.SetBoundary (3,PG_BLACK);
   #if defined(__DEBUG_VDF)
   _DebugView_VDF->DrawImage (e_image,_DVX_VDF,_DVY_VDF);
   _DVY_VDF += e_image.Height;
   #endif
   GImage m_image(e_image.Width,e_image.Height);
   BinDilation (e_image,StrElement,m_image);
   // 입력 영상에서 에지 및 그 근방의 픽셀들을 제외한 나머지 픽셀들 중 밝기 최대값을 얻는다.
   // - 하늘에 해당하는 영역이라면 에지 픽셀이 검출되지 않을 것이므로 제외시키고,
   // - 하늘에 해당하는 영역이라면 통상적으로 주변보다 밝기 때문에 픽셀 밝기의 최대값을 찾는다.
   #if defined(__DEBUG_VDF)
   _DebugView_VDF->DrawImage (m_image,_DVX_VDF,_DVY_VDF);
   _DVY_VDF += m_image.Height;
   #endif
   byte max_v = 0;
   for (y = 0; y < s_image.Height; y++) {
      byte *l_sr_image = s_image[y];
      byte *l_m_image  = m_image[y];
      for (x = 0; x < s_image.Width; x++) {
         if (!l_m_image[x]) {
            if (max_v < l_sr_image[x]) max_v = l_sr_image[x];
         }
      }
   }
   if (!max_v) return;
   // Atmospheric Light 값을 결정한다.
   int al = (int)(0.9f * max_v);
   if (AtmosphericLight > 255 || abs(al - AtmosphericLight) > ALDThreshold) AtmosphericLight = al;
   #if defined(__DEBUG_VDF)
   logd ("al = %03d, AtmosphericLight = %03d\n",al,AtmosphericLight);
   _DebugView_VDF->Invalidate (   );
   #endif
} 

 void VideoDefogging::EstimateTransmissionMap (BGRImage &s_cimage)
 
{
   int x,y;
   
   IArray2D t_map(s_cimage.Width,s_cimage.Height);
   int a = 0x10000 / AtmosphericLight;
   for (y = 0; y < s_cimage.Height; y++) {
      BGRPixel *s_cp    = s_cimage[y];
      int      *l_t_map = t_map[y];
      for (x = 0; x < s_cimage.Width; x++) {
         l_t_map[x] = 0x10000 - a * GetMinimum (GetMinimum (s_cp->B,s_cp->G),s_cp->R);
         s_cp++;
      }
   }
   int bfk_size = (int)(BFSize / 240.0f * s_cimage.Height + 0.5f);
   BoxFiltering (t_map,bfk_size,TransmissionMap);
}

 void VideoDefogging::Initialize (int frm_width,int frm_height,float frm_rate)
 
{
   Close (   );
   AtmosphericLight = 256;
   FrameCount       = 0;
   FrameRate        = frm_rate;
   FrameSize(frm_width,frm_height);
   TransmissionMap.Create (frm_width,frm_height);
   CreateStrElement (   );
   UpdateGammaCorrectionTable (   );
   Flag_Initialized = TRUE;
}

 int VideoDefogging::Perform (BGRImage& s_cimage,BGRImage& d_cimage)

{
   if (!Flag_Initialized) return (1);
   int alep = (int)(FrameRate * ALEPeriod + 0.5f);
   if (!(FrameCount % alep)) {
      BGRImage sr_cimage = s_cimage.Extract (0,0,s_cimage.Width,(int)(s_cimage.Height * SkyRegionRange));
      GImage sr_image    = sr_cimage;
      EstimateAtmosphericLight (sr_image);
   }
   EstimateTransmissionMap (s_cimage);
   RecoverSceneRadiance (s_cimage,d_cimage);
   return (DONE);
}

 int VideoDefogging::Perform_YUY2 (byte *si_buffer,byte *di_buffer)
 
{
   if (!Flag_Initialized) return (1);
   int alep = (int)(FrameRate * ALEPeriod + 0.5f);
   if (!(FrameCount % alep)) {
      GImage sr_image(FrameSize.Width,(int)(FrameSize.Height * SkyRegionRange));
      ConvertYUY2ToGray (si_buffer,sr_image);
      EstimateAtmosphericLight (sr_image);
   }
   BGRImage s_cimage(FrameSize.Width,FrameSize.Height);
   ConvertYUY2ToBGR_2 (si_buffer,s_cimage);
   EstimateTransmissionMap (s_cimage);
   BGRImage d_cimage(FrameSize.Width,FrameSize.Height);
   RecoverSceneRadiance (s_cimage,d_cimage);
   ConvertBGRToYUY2_2 (d_cimage,di_buffer);
   return (DONE);
}

 void VideoDefogging::RecoverSceneRadiance (BGRImage &s_cimage,BGRImage &d_cimage)
 
{
   int x,y;

   int ca  = 0x10000 * AtmosphericLight;
   int ct0 = (int)(0x10000 * MinTransmission);
   for (y = 0; y < s_cimage.Height; y++) {
      BGRPixel *s_cp    = s_cimage[y];
      BGRPixel *d_cp    = d_cimage[y];
      int      *l_t_map = TransmissionMap[y];
      for (x = 0; x < s_cimage.Width; x++) {
         int ct = GetMaximum (l_t_map[x],ct0);
         d_cp->B = GCTable[(byte)GetMaximum (GetMinimum ((((int)s_cp->B << 16) - ca) / ct + AtmosphericLight,255),0)];
         d_cp->G = GCTable[(byte)GetMaximum (GetMinimum ((((int)s_cp->G << 16) - ca) / ct + AtmosphericLight,255),0)];
         d_cp->R = GCTable[(byte)GetMaximum (GetMinimum ((((int)s_cp->R << 16) - ca) / ct + AtmosphericLight,255),0)];
         s_cp++, d_cp++;
      }
   }
}

 void VideoDefogging::UpdateGammaCorrectionTable (   )
 
{
   int x;
   
   float a = 1.0f / GammaCorrectionParam;
   for (x = 0; x < 256; x++) GCTable[x] = (byte)(255.0f * pow (x / 255.0f,a) + 0.5f);
}

}

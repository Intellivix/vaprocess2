#if !defined(__VAENGINE_H)
#define __VAENGINE_H

#define VAE_PIXFMT_NONE           0x00000000
#define VAE_PIXFMT_GRAY8          0x00000001
#define VAE_PIXFMT_BGR24          0x00000002
#define VAE_PIXFMT_YUY2           0x00000003

#define VAE_MODE_DEFAULT          0x00000000 /* Mode: Default                                         */
#define VAE_MODE_GENERAL          0x00000001 /* Mode: Moving Object Tracking with a Static Camera     */
#define VAE_MODE_FACE             0x00000002 /* Mode: Face Detection and Tracking                     */
#define VAE_MODE_OBJECT           0x00000003 /* Mode: Object Detection and Tracking                   */
#define VAE_MODE_OVERHEAD         0x00000004 /* Mode: People Tracking with an Overhead Camera         */
#define VAE_MODE_OBLIQUE          0x00000005 /* Mode: People Tracking with an Oblique Camera          */ 
#define VAE_MODE_MOVCAM           0x00000006 /* Mode: Object Tracking with a Moving Camera            */
#define VAE_MODE_OVERHEAD_3DCAM   0x00000007 /* Mode: People Tracking with an Overhead 3D Camera      */
#define VAE_MODE_UNDERWATER       0x00000008 /* Mode: Underwater-object Tracking with a Static Camera */

#if defined(__cplusplus)
extern "C" {
#endif

typedef void* HVAENGINE;

typedef struct _BOX

{
   int X;
   int Y;
   int Width;
   int Height;
} BOX;

/* ?? API */

void  VAEngine_CalcPVFSize       (int svf_width,int svf_height,float vfsr_factor,int* pvf_width,int* pvf_height);
int   VAEngine_Close             (HVAENGINE h_va_engine);
void* VAEngine_GetConfiguration  (HVAENGINE h_va_engine,int* cfg_buf_len);
int   VAEngine_GetMode           (HVAENGINE h_va_engine);
float VAEngine_GetPVFRate        (HVAENGINE h_va_engine);
int   VAEngine_GetPVFSize        (HVAENGINE h_va_engine,int* pvf_width,int* pvf_height);
int   VAEngine_GetPVFPixFmt      (HVAENGINE h_va_engine);
int   VAEngine_Initialize        (HVAENGINE h_va_engine,int va_mode,int pvf_width,int pvf_height,float pvf_rate,int pvf_pix_fmt);
int   VAEngine_IsInitialized     (HVAENGINE h_va_engine);
int   VAEngine_ProcessVideoFrame (HVAENGINE h_va_engine,void* s_pvf_buf,char* time_stamp,int flag_finalize,BOX* s_boxes,int n_boxes,void* d_pkt_buf,int d_buf_len);
int   VAEngine_SetConfiguration  (HVAENGINE h_va_engine,void* s_cfg_buf,int s_buf_len);

/* ? API */

int       VAEngine_InitLibrary  (const char* license_key,const char* serial_no);
void      VAEngine_CloseLibrary (   );
HVAENGINE VAEngine_Create       (int channel_id);
int       VAEngine_Delete       (HVAENGINE h_va_engine);
int       VAEngine_Run          (HVAENGINE h_va_engine,int va_mode,void* pvf_buf,int pvf_width,int pvf_height,float pvf_rate,int pvf_pix_fmt,char* time_stamp,int flag_finalize,void* cfg_buf,int cfg_buf_len,void* d_pkt_buf,int d_buf_len);
int       VAEngine_RunEx        (HVAENGINE h_va_engine,int va_mode,void* pvf_buf,int pvf_width,int pvf_height,float pvf_rate,int pvf_pix_fmt,char* time_stamp,int flag_finalize,void* cfg_buf,int cfg_buf_len,BOX* s_boxes,int n_boxes,void* d_pkt_buf,int d_buf_len);
int       VAEngine_Reset        (HVAENGINE h_va_engine);

#if defined(__cplusplus)
}
#endif

#endif

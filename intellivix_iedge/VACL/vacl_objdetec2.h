#if !defined(__VACL_OBJDETEC2_H)
#define __VACL_OBJDETEC2_H

#include "vacl_bsttree.h"
#include "vacl_feature.h"
#include "vacl_sample.h"

 namespace VACL

{

#if defined(__LIB_ML)

///////////////////////////////////////////////////////////////////////////////
// 
// Class: ObjectDetection_ACF
//
///////////////////////////////////////////////////////////////////////////////

 class ObjectDetection_ACF

{
   protected:
      GradientTable GradTable;

   protected:
      SmpFileList* PosSmpList;
      SmpFileList* BkgImgList;
   
   protected:
      FArray2D FVArray;           // Feature vector array (Width = [# of features], Height = [# of samples])
      FArray1D CLArray;           // Class label array (Length = [# of samples], Value = -1 or +1)
      Array1D<FArray2D> ICFArray; // Integral channel feature array (Length = [# of channels])
   
   public:
      BoostedTreeCascade Classifier;
   
   public:
      int     NumGHBins;  // # of gradient histogram bins
      int     CellSize;   // Cell size (Subsampling factor)
      ISize2D DetWinSize; // Detection window size

   public:
      int   StepSize;
      float MinObjHeight;
      float MaxObjHeight;
      float ScaleInc;

   public:
      ISize2D  SmpImgSize; // Sample image size
      IPoint2D DetWinPos;  // Detection window position inside a sample image

   public:
      int NumChannels;
      int NumFeatures;
      int NumSamples;
      int NumPosSamples;
      int NumInitNegSamples;
      int NumBootNegSamples;
      int MinNumNegSamples;
      int MaxNumNegSamples;
    
   public:
      ObjectDetection_ACF (   );
      virtual ~ObjectDetection_ACF (   );
   
   protected:
      int  CheckSmpImgSize  (   );
      int  CreateICFArray   (ISize2D& si_size);
      void GetFeatureVector (Array1D<FArray2D>& icf_array,int sx,int sy,float* d_array);
      void GetICFs          (GImage& s_image,Array1D<FArray2D>& d_array);
      int  GetBootNSFVs     (   );
      int  GetInitNSFVs     (   );
      int  GetPSFVs         (   );
      int  Initialize       (   );
      int  InitTraining     (SmpFileList& ps_list,SmpFileList& bi_list);
      int  LoadConfig       (const char* file_name);
      int  PerformTesting   (   );
      int  PerformTraining  (   );
      int  SaveConfig       (const char* file_name);

   public:
      virtual void Close (   );
   
   // MS: Multi-Scale
   // SS: Single-Scale
   // IP: Image Pyramid
   // FP: Feature Pyramid
   public:
      int Detect_MS_IP       (GImage& s_image,IB2DArray1D& d_rects,FArray1D& d_scores);
      int Detect_SS          (GImage& s_image,float scale,IBox2D* d_rects,float* d_scores,int max_n_rects);
      int Detect_SS          (GImage& s_image,float scale,IB2DArray1D& d_rects,FArray1D& d_scores);
      int Load               (const char* cfg_file_name,const char* cls_base_file_name);
      int Save               (const char* cfg_file_name,const char* cls_base_file_name);
      int TestWithNegSamples (SmpFileList& bi_list,int n_samples,int& n_fps);
      int TestWithPosSamples (SmpFileList& ps_list,int& n_tps);
      int Train              (SmpFileList& ps_list,SmpFileList& bi_list);
};

#endif

}

#endif

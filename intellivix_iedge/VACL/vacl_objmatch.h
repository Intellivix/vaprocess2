#if !defined(__VACL_OBJMATCH_H)
#define __VACL_OBJMATCH_H

#include "vacl_histo.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: TargetColor
//
///////////////////////////////////////////////////////////////////////////////

 class TargetColor

{
   public:
      BGRPixel Color;
   
   public:
      int Hue;
      int Saturation;
      int Value;
      
   public:
      int IndexNo;
   
   public:
      float MSThreshold; // 매칭 스코어의 임계치.
                         // 매칭 스코어가 본 임계치보다 크면 매칭에 성공한 것으로 판단한다.

   public:
      TargetColor *Prev;
      TargetColor *Next;

   public:
      void Initialize (byte red,byte green,byte blue,float ms_thrsld = 0.15f);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectColorMatching
//
///////////////////////////////////////////////////////////////////////////////

 class ObjectColorMatching
 
{
   public:
      LinkedList<TargetColor> TargetColors;
   
   public:
      void AddTargetColor       (byte red,byte green,byte blue,float ms_thrsld);
      void ClearTargetColorList (   );
      int  Perform              (float *s_histogram,float &m_score);
};

}

#endif

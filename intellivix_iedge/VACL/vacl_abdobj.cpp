#include "vacl_abdobj.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: AbandonedObject
//
///////////////////////////////////////////////////////////////////////////////

 AbandonedObject::AbandonedObject (   )
 
{
   Flag_Matched = TRUE;
   SX           = 0;
   SY           = 0;
   EX           = 0;
   EY           = 0;
   ID           = 0;
   Score        = 0.0f;
   StartTime    = 0.0f;
}

 void AbandonedObject::GetOverlappedRegion (IPoint2D &s_ltp,IPoint2D &s_rbp,IPoint2D &d_ltp,IPoint2D &d_rbp)
 
{
   if (SX < s_ltp.X) d_ltp.X = s_ltp.X;
   else d_ltp.X = SX;
   if (SY < s_ltp.Y) d_ltp.Y = s_ltp.Y;
   else d_ltp.Y = SY;
   if (EX < s_rbp.X) d_rbp.X = EX;
   else d_rbp.X = s_rbp.X;
   if (EY < s_rbp.Y) d_rbp.Y = EY;
   else d_rbp.Y = s_rbp.Y;
}

 int AbandonedObject::IsInside (IPoint2D &s_pos)
 
{
   if (SX <= s_pos.X && s_pos.X <= EX && SY <= s_pos.Y && s_pos.Y <= EY) return (TRUE);
   else return (FALSE);   
}

 void AbandonedObject::SetRegion (IPoint2D &lt_pos,IPoint2D &rb_pos)
 
{
   SX = lt_pos.X;
   SY = lt_pos.Y;
   EX = rb_pos.X;
   EY = rb_pos.Y;
}

 void AbandonedObject::UpdateRegion (IPoint2D &lt_pos,IPoint2D &rb_pos)
 
{
   IPoint2D min_cp,max_cp,min_i,max_i;

   IP2DArray1D cp_array(8);
   cp_array[0](SX,SY);
   cp_array[1](EX,SY);
   cp_array[2](EX,EY);
   cp_array[3](SX,EY);
   cp_array[4](lt_pos.X,lt_pos.Y);
   cp_array[5](rb_pos.X,lt_pos.Y);
   cp_array[6](rb_pos.X,rb_pos.Y);
   cp_array[7](lt_pos.X,rb_pos.Y);
   FindMinMax (cp_array,min_cp,max_cp,min_i,max_i);
   SX = min_cp.X;
   SY = min_cp.Y;
   EX = max_cp.X;
   EY = max_cp.Y;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: AbandonedObjectList
//
///////////////////////////////////////////////////////////////////////////////

 AbandonedObjectList::AbandonedObjectList (   )

{
   MaxScore = 2.0f; // 2.0초 이상 움직이면 "버려진 물체 후보" 목록에서 제거된다.
}

 void AbandonedObjectList::UpdateObjectScores (float frm_duration)
 
{
   AbandonedObject* c_object = First (   );
   while (c_object != NULL) {
      AbandonedObject* n_object = Next (c_object);
      if (c_object->Flag_Matched) {
         c_object->Score += frm_duration;
         if (c_object->Score > MaxScore) c_object->Score = MaxScore;
         c_object->Flag_Matched = FALSE;
      }
      else {
         c_object->Score -= frm_duration;
         if (c_object->Score < 0.0f) {
            Remove (c_object);
            delete c_object;
         }
      }
      c_object = n_object;
   }
}

}

#if !defined(__VACL_OBJCLASS_H)
#define __VACL_OBJCLASS_H
 
#include "vacl_dnn.h"
#include "vacl_objrecog.h"
 
 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectClassification_Common
//
///////////////////////////////////////////////////////////////////////////////

#define OC_TYPE_HUMAN     0
#define OC_TYPE_VEHICLE   1
#define OC_TYPE_UNKNOWN   2

 class ObjectClassification_Common

{
   public:
      int EnfObjType;

   public:
      float MaxTime_Unchanged;

   public:
      ObjectClassification_Common (   );

   protected:
      int GetHumanHeadPositions (TrackedObject* t_object);

   public:
      virtual void InitParams (EventDetection& evt_detector) {   };
      virtual int  ReadFile   (FileIO& file);
      virtual int  ReadFile   (CXMLIO* pIO);
      virtual int  WriteFile  (FileIO& file);
      virtual int  WriteFile  (CXMLIO* pIO);
      virtual int  CheckSetup (ObjectClassification_Common* fd); // jun

   public:
      int  GetEnforcedObjectType (   );
      void SetEnforcedObjectType (int obj_type);
      int  Preprocess            (ObjectTracking& obj_tracker);
};

 inline int ObjectClassification_Common::GetEnforcedObjectType (   )

{
   return (EnfObjType);
}

 inline void ObjectClassification_Common::SetEnforcedObjectType (int obj_type)

{
   EnfObjType = obj_type;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectClassification_Basic
//
///////////////////////////////////////////////////////////////////////////////

 class ObjectClassification_Basic : public ObjectRecognition, public ObjectClassification_Common
 
{
   public:
      int Flag_CheckHumanGroup;
   
   public:
      ObjectClassification_Basic (   );

   protected:
      int CheckHumanGroup (TrackedObject* t_object);
   
   protected:
      virtual int    CheckObject             (TrackedObject* t_object) { return (0);    };
      virtual int    DoPreprocessing         (ObjectTracking& ob_tracker);
      virtual int*   GetObjectCountVariable  (TrackedObject* t_object) { return (NULL); };
      virtual float* GetObjectTimeVariable   (TrackedObject* t_object) { return (NULL); };
      virtual int    UpdateLikelihood        (TrackedObject* t_object);
      virtual void   UpdateRecognitionResult (TrackedObject* t_object);
      virtual void   RecognizeObject         (TrackedObject* t_object) {   };

   public:
      virtual int  Initialize (   );
      virtual void InitParams (EventDetection& evt_detector);
      virtual int  Perform    (ObjectTracking& obj_tracker);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectClassification_DNN_Common
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_CAFFE)

 class ObjectClassification_DNN_Common

{
   protected:
      DNN_Caffe_ObjectClassifier DNN;
      
   protected:
      void _Close      (   );
      int  _Initialize (void* dll_handle, const char* dir_name,int flag_use_gpu, int gpu_idx);
};

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectClassification_DNN
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_CAFFE)

 class ObjectClassification_DNN : public ObjectRecognition, public ObjectClassification_Common, public ObjectClassification_DNN_Common

{
   public:
      ObjectClassification_DNN (   );

   protected:
      virtual int    CheckObject             (TrackedObject* t_object);
      virtual int    DoPreprocessing         (ObjectTracking& obj_tracker);
      virtual int*   GetObjectCountVariable  (TrackedObject* t_object);
      virtual float* GetObjectTimeVariable   (TrackedObject* t_object);
      virtual void   RecognizeObject         (TrackedObject* t_object);
      virtual int    UpdateLikelihood        (TrackedObject* t_object);
      virtual void   UpdateRecognitionResult (TrackedObject* t_object);
   
   public:
      virtual void Close   (   );
      virtual int  Perform (ObjectTracking& obj_tracker);
  
   public:
      int Initialize (void* dll_handle,const char* dir_name,int flag_use_gpu = TRUE,int gpu_idx = 0);
};

 inline int* ObjectClassification_DNN::GetObjectCountVariable (TrackedObject* t_object)

{
   return (&(t_object->Count_POC));
}

 inline float* ObjectClassification_DNN::GetObjectTimeVariable (TrackedObject* t_object)

{
   return (&(t_object->Time_POC));
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectClassificationServer_DNN
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_CAFFE)

 class ObjectClassificationServer_DNN : public ObjectRecognitionServer, public ObjectClassification_DNN_Common

{
   protected:
      void*   DLLHandle;
      int     GpuNo;
      int     Flag_UseGPU;
      StringA DirName;

   public:
      ObjectClassificationServer_DNN (   );

   protected:
      virtual void OnThreadEnded   (   );
      virtual int  OnThreadStarted (   );
      virtual void ProcessRequest  (ObjectTracking* obj_tracker);
   
   public:
      virtual void    Close           (   );
      virtual ISize2D GetSrcImageSize (   );

   public:
      int Initialize (void* dll_handle,const char* dir_name,int flag_use_gpu = FALSE,int gpu_idx = 0);
};

 inline ISize2D ObjectClassificationServer_DNN::GetSrcImageSize (   )

{
   return (DNN.SrcImgSize);
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectClassificationClient
//
///////////////////////////////////////////////////////////////////////////////

 class ObjectClassificationClient : public ObjectRecognitionClient, public ObjectClassification_Common

{
   public:
      ObjectClassificationClient (   );

   protected:
      virtual int    CheckObject             (TrackedObject* t_object);
      virtual int    DoPreprocessing         (ObjectTracking& obj_tracker);
      virtual int*   GetObjectCountVariable  (TrackedObject* t_object);
      virtual float* GetObjectTimeVariable   (TrackedObject* t_object);
      virtual void   UpdateRecognitionResult (TrackedObject* t_object);

   public:
      int Initialize (   );
};

 inline int* ObjectClassificationClient::GetObjectCountVariable (TrackedObject* t_object)

{
   return (&(t_object->Count_POC));
}

 inline float* ObjectClassificationClient::GetObjectTimeVariable (TrackedObject* t_object)

{
   return (&(t_object->Time_POC));
}

}

#endif

#include "vacl_ipp.h"
#include "vacl_sample.h"
#include "vacl_warping.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: SmpFileList
//
///////////////////////////////////////////////////////////////////////////////

 int SmpFileList::LoadImage (int index_no,BGRImage &d_image)
 
{
   if (index_no >= Height) return (1);
   if (d_image.ReadFile (Array[index_no])) return (2);
   return (DONE);
}

 int SmpFileList::LoadImage (int index_no,GImage &d_image)
 
{
   if (index_no >= Height) return (1);
   if (d_image.ReadFile (Array[index_no])) return (2);
   return (DONE);
}

 int SmpFileList::LoadList (const char* file_name)
 
{
   FileIO file(file_name,FIO_TEXT,FIO_READ);
   if (!file) return (1);
   int e_code = LoadList (file);
   if (e_code != DONE) return (e_code + 1);
   else return (DONE);
}

 int SmpFileList::LoadList (FileIO& file)
 
{
   int i;

   Delete (   );
   file.Seek (0,FIO_SEEK_BEGIN);
   StringA s_string(MaxLineLength);
   int n_samples = 0;
   while (TRUE) {
      if (file.ReadLine (s_string,s_string.Length)) break;
      if (strlen (s_string)) n_samples++;
   }
   if (!n_samples) return (DONE);
   file.Seek (0,FIO_SEEK_BEGIN);
   Create (MaxLineLength,n_samples);
   for (i = 0; i < n_samples;   ) {
      if (file.ReadLine (s_string,s_string.Length)) break;
      if (!strlen (s_string)) continue;
      strcpy (Array[i++],s_string);
   }
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: RndSmpImgExtraction
//
///////////////////////////////////////////////////////////////////////////////

 RndSmpImgExtraction::RndSmpImgExtraction (   )

{
   Flag_Initialized = FALSE;
}

 RndSmpImgExtraction::~RndSmpImgExtraction (   )

{
   Close (   );
}

 void RndSmpImgExtraction::Close (   )

{
   Flag_Initialized = FALSE;
}

 void RndSmpImgExtraction::Initialize (   )

{
   Close (   );
   time_t seed;
   time  (&seed);
   srand ((uint)seed);
   Flag_Initialized = TRUE;
}

 int RndSmpImgExtraction::Perform (GImage& s_image,GImage& d_image)

{
   if (!Flag_Initialized) return (1);
   // 가능하면 d_image 보다 큰 이미지를 s_image로부터 임의로 추출한 후 축소하여 d_image를 만든다.
   int rx = s_image.Width  - d_image.Width;
   int ry = s_image.Height - d_image.Height;
   if (rx < 1) rx = 1;
   if (ry < 1) ry = 1;
   int sx = rand (   ) % rx;
   int sy = rand (   ) % ry;
   int dx = s_image.Width  - sx - d_image.Width;
   int dy = s_image.Height - sy - d_image.Height;
   if (dx < 1) dx = 1;
   if (dy < 1) dy = 1;
   int ex = sx + d_image.Width  + (rand (   ) % dx);
   int ey = sy + d_image.Height + (rand (   ) % dy);
   if (ex > s_image.Width ) ex = s_image.Width;
   if (ey > s_image.Height) ey = s_image.Height;
   GImage t_image = s_image.Extract (sx,sy,ex - sx,ey - sy);
   Resize_2 (t_image,d_image);
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: SeqSmpImgExtraction
//
///////////////////////////////////////////////////////////////////////////////

 SeqSmpImgExtraction::SeqSmpImgExtraction (   )

{

}

 SeqSmpImgExtraction::~SeqSmpImgExtraction (   )

{
   Close (   );
}

 void SeqSmpImgExtraction::Close (   )

{
   SrcImage.Delete (   );
   SmpSize.Clear   (   );
   StepSize.Clear  (   );
   CurPos.Clear    (   );
}

 int SeqSmpImgExtraction::Initialize (GImage& s_image,float scale,ISize2D& smp_size,IPoint2D& step_size)
 
{
   Close (   );
   if (scale <= 0.0f) return (1);
   int si_width  = (int)(s_image.Width  * scale + 0.5f);
   int si_height = (int)(s_image.Height * scale + 0.5f);
   SrcImage.Create (si_width,si_height);
   Resize_2 (s_image,SrcImage,TRUE);
   SmpSize  = smp_size;
   StepSize = step_size;
   return (DONE);
}

 int SeqSmpImgExtraction::Perform (GImage& d_image)

{
   if (!SrcImage) return (1);
   int ex = CurPos.X + SmpSize.Width;
   int ey = CurPos.Y + SmpSize.Height;
   if (ex > SrcImage.Width) {
      CurPos.X = 0;
      CurPos.Y += StepSize.Y;
      ex = SmpSize.Width;
      ey = CurPos.Y + SmpSize.Height;
   }
   if (ey > SrcImage.Height) return (-1);
   d_image = SrcImage.Extract (CurPos.X,CurPos.Y,SmpSize.Width,SmpSize.Height);
   CurPos.X += StepSize.X;
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 void GetShuffledIndexNumbers (IArray1D& d_array)

{
   int i;

   for (i = 0; i < d_array.Length; i++) d_array[i] = i;
   ShuffleElements (d_array,d_array.Length);
}

 void ShuffleElements (IArray1D& s_array,int n_times)

{
   int i;

   time_t seed;
   time  (&seed);
   srand ((uint)seed);
   for (i = 0; i < n_times; i++) {
      int i1 = rand (   ) % s_array.Length;
      int i2 = rand (   ) % s_array.Length;
      Swap (s_array[i1],s_array[i2]);
   }
}

}

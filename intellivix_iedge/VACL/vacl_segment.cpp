#include "vacl_histo.h"
#include "vacl_segment.h"

 namespace VACL

{

 void Binarize_Otsu (GImage &s_image,GImage &d_image)

{
   IArray1D histogram(256);
   GetHistogram (s_image,histogram,TRUE);
   byte  threshold;
   float tm,bgm,fgm;
   GetThreshold_Otsu (histogram,threshold,tm,bgm,fgm);
   Binarize (s_image,threshold,d_image);
}

 void GetThreshold_Otsu (IArray1D &s_array,byte &threshold,float &tm,float &bgm,float &fgm)

{
   int i;

   threshold = 0;
   tm = bgm = fgm = 0.0f;
   int n   = 0;
   int r   = 0;
   int s   = s_array[0];
   int sum = 0;
   for (i = 0; i < s_array.Length; i++) {
      n   += s_array[i];
      sum += s_array[i] * i;
   }
   tm = (float)sum / n;
   if (n == s) return;
   float m1 = 0.0f;
   float m2 = (float)sum / (n - s);
   float q1 = (float)s / n;
   float max_bgv = q1 * (1 - q1) * (m1 - m2) * (m1 - m2);
   for (i = 1; i < s_array.Length; i++) {
      s += s_array[i];
      r += s_array[i] * i;
      q1 = (float)s / n;
      if (s) {
         m1 = (float)r / s;
         if (s == n) m2 = 0.0f;
         else m2 = (float)(sum - r) / (n - s);
      }
      else m1 = 0.0f;
      float bgv = q1 * (1 - q1) * (m1 - m2) * (m1 - m2);
      if (bgv > max_bgv) {
         threshold = i;
         bgm = m1;
         fgm = m2;
         max_bgv = bgv;
      }
   }
}

}
#include "vacl_binimg.h"
#include "vacl_conrgn.h"
#include "vacl_filter.h"
#include "vacl_imgreg.h"
#include "vacl_ipp.h"
#include "vacl_morph.h"
#include "vacl_motion.h"
#include "vacl_warping.h"

#if defined(__DEBUG_FGD)
#include "Win32CL/Win32CL.h"
extern int _DVX_FGD,_DVY_FGD;
extern CImageView* _DebugView_FGD;
#endif

 namespace VACL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Class: MotionDetection_FAD
//
///////////////////////////////////////////////////////////////////////////////

 MotionDetection_FAD::MotionDetection_FAD (   )
 
{
   FrameCount    = 0;
   MDThreshold   = 25;
   NBWSize       = 3;
   MinMDRArea    = 9;
   CFRadius      = 2;
   FrameInterval = 0.75f;
   MaxShift      = 0.2f;
   FrameRate     = 0.0f;
}

 MotionDetection_FAD::~MotionDetection_FAD (   )

{
   Close (   );
}

 void MotionDetection_FAD::AddToFrameQueue (GImage& s_image)
 
{
   Frame* n_frame = new Frame;
   n_frame->Image  = s_image;
   if (FrameQueue.Add (n_frame)) {
      Frame* o_frame = FrameQueue.Retrieve (   );
      delete o_frame;
      FrameQueue.Add (n_frame);
   }
}

 void MotionDetection_FAD::Close (   )
 
{
   FrameQueue.Delete (   );
   FrameCount = 0;
}

 int MotionDetection_FAD::Initialize (GImage& s_image,float frm_rate)

{
   Close (   );
   FrameRate = frm_rate;
   int n = (int)(FrameRate * FrameInterval + 0.5f);
   if (n < 1) n = 1;
   FrameQueue.QueueLength = n;
   AddToFrameQueue (s_image);
   FrameCount++;
   return (DONE);
}

 int MotionDetection_FAD::Perform (GImage& s_image,GImage& d_image)
 
{
   d_image.Clear (   );
   if (FrameCount < 1) return (1);
   #if defined(__DEBUG_FGD)
   _DebugView_FGD->DrawImage (s_image,_DVX_FGD,_DVY_FGD);
   _DVY_FGD += s_image.Height;
   _DebugView_FGD->DrawText  (_T("Source Image"),_DVX_FGD,_DVY_FGD,PC_GREEN);
   _DVY_FGD += 20;
   #endif
   if (FrameCount >= FrameQueue.QueueLength) {
      // 이전 프레임들 중 현재 프레임과 시간적으로 가장 먼 프레임을 얻는다.
      Frame* p_frame  = FrameQueue.First (   );
      GImage& p_image = p_frame->Image;
      // 이전 프레임과 현재 프레임 사이의 호모그래피를 구한다.
      Matrix mat_H;
      PerformImageRegistration_KLT (p_image,s_image,mat_H);
      FPoint2D shift = GetShift (mat_H);
      float dx_thrsld = MaxShift * s_image.Width;
      float dy_thrsld = MaxShift * s_image.Height;
      if (fabs (shift.X) < dx_thrsld && fabs (shift.Y) < dy_thrsld) { // PARAMETERS
         // p_image를 s_image에 align하여 wp_image를 얻는다.
         GImage wp_image(s_image.Width,s_image.Height);
         Warp_2 (p_image,mat_H,wp_image);
         #if defined(__DEBUG_FGD)
         _DebugView_FGD->DrawImage (wp_image,_DVX_FGD,_DVY_FGD);
         _DVY_FGD += s_image.Height;
         _DebugView_FGD->DrawText  (_T("Previous Image (Warped)"),_DVX_FGD,_DVY_FGD,PC_GREEN);
         _DVY_FGD += 20;
         #endif
         // wp_image와 s_image 간의 차이를 구하여 모션 픽셀을 검출한다.
         GImage fg_image(s_image.Width,s_image.Height);
         GetFrameDifference (s_image,wp_image,MDThreshold,NBWSize,fg_image);
         #if defined(__DEBUG_FGD)
         _DebugView_FGD->DrawImage (fg_image,_DVX_FGD,_DVY_FGD);
         _DVY_FGD += s_image.Height;
         _DebugView_FGD->DrawText (_T("Frame Difference Image"),_DVX_FGD,_DVY_FGD,PC_GREEN);
         _DVY_FGD += 20;
         #endif
         // 노이즈에 해당하는 모션 픽셀들을 제거하고 모폴로지 클로징을 수행한다.
         RemoveCRs (fg_image,0,MinMDRArea,fg_image);
         RemoveBoundaryMDPixels (shift,fg_image);
         PerformBinClosing2 (fg_image,CFRadius,d_image);
         #if defined(__DEBUG_FGD)
         _DebugView_FGD->DrawImage (d_image,_DVX_FGD,_DVY_FGD);
         _DVY_FGD += s_image.Height;
         _DebugView_FGD->DrawText (_T("Motion Detection Result"),_DVX_FGD,_DVY_FGD,PC_GREEN);
         _DVY_FGD += 20;
         #endif
      }
   }
   AddToFrameQueue (s_image);
   FrameCount++;
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 void GetFrameDifference (GImage& s_image1,GImage& s_image2,int md_thrsld,GImage& d_image)
 
{
   int x,y;
   
   d_image.Clear (   );
   for (y = 0; y < d_image.Height; y++) {
      byte* l_s_image1 = s_image1[y];
      byte* l_s_image2 = s_image2[y];
      byte* l_d_image  = d_image[y];
      for (x = 0; x < d_image.Width; x++) {
         if (l_s_image1[x] && l_s_image2[x]) {
            int d = abs ((int)l_s_image1[x] - l_s_image2[x]);
            if (d >= md_thrsld) l_d_image[x] = PG_WHITE;
         }
      }
   }
}

 void GetFrameDifference (GImage& s_image1,GImage& s_image2,int md_thrsld,int nw_size,GImage& d_image)

{
   int x,y,x2,y2;

   if (nw_size <= 1) {
      GetFrameDifference (s_image1,s_image2,md_thrsld,d_image);
      return;
   }
   int hnws = nw_size / 2;
   int ex   = d_image.Width  - hnws;
   int ey   = d_image.Height - hnws;
   d_image.Clear (   );
   for (y = hnws; y < ey; y++) {
      byte* l_s_image1 = s_image1[y];
      byte* l_s_image2 = s_image2[y];
      byte* l_d_image  = d_image[y];
      for (x = hnws; x < ex; x++) {
         if (l_s_image1[x] && l_s_image2[x]) {
            int d = abs ((int)l_s_image1[x] - l_s_image2[x]);
            if (d >= md_thrsld) {
               l_d_image[x] = PG_WHITE;
               int sx2 = x - hnws;
               int sy2 = y - hnws;
               int ex2 = x + hnws;
               int ey2 = y + hnws;
               int sp  = (int)l_s_image1[x];
               for (y2 = sy2; y2 <= ey2; y2++) {
                  byte* l_s_image3 = s_image2[y2];
                  for (x2 = sx2; x2 <= ex2; x2++) {
                     d = abs (sp - l_s_image3[x2]);
                     if (d < md_thrsld) {
                        l_d_image[x] = PG_BLACK;
                        break;
                     }
                  }
                  if (l_d_image[x] == PG_BLACK) break;
               }
            }
         }
      }
   }
}

 void RemoveBoundaryMDPixels (FPoint2D& shift,GImage& d_image)

{
   d_image.SetBoundary (3,PG_BLACK);
   if (shift.X > 0.0f) {
      int w = (int)(shift.X + 3.5f);
      d_image.Set (0,0,w,d_image.Height,PG_BLACK);
   }
   else if (shift.X < 0.0f) {
      int w = (int)(3.5f - shift.X);
      d_image.Set (d_image.Width - w,0,w,d_image.Height,PG_BLACK);
   }
   if (shift.Y > 0.0f) {
      int h = (int)(shift.Y + 3.5f);
      d_image.Set (0,0,d_image.Width,h,PG_BLACK);
   }
   else if (shift.Y < 0.0f) {
     int h = (int)(3.5f - shift.Y);
     d_image.Set (0,d_image.Height - h,d_image.Width,h,PG_BLACK);
   }
}

}

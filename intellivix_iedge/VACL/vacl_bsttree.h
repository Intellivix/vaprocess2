#if !defined(__VACL_BSTTREE_H)
#define __VACL_BSTTREE_H

#include "vacl_feature.h"
#include "vacl_opencv.h"

#if defined(__LIB_ML)
#include "ML/ML.h"
#endif

 namespace VACL

{

#define BTC_CASCADE_MODE_HARD1   0
#define BTC_CASCADE_MODE_HARD2   1
#define BTC_CASCADE_MODE_SOFT    2
#define MAX_NUM_OBJ_DET_RECTS    10000

///////////////////////////////////////////////////////////////////////////////
// 
// Class: BoostedTree
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_ML)

 class BoostedTree

{
   protected:
      ML::MulticlassClassifierFactoryDecisionTree<FeatureVector>* DecisionTreeFactory;
      ML::BinaryClassifierFactoryMulticlass<FeatureVector>*       WeakClassifierFactory;
      ML::BinaryClassifierAdaBoost<FeatureVector>*                AdaBoostClassifier;

   // 직접 설정 가능 값들
   public:
     double SCDThreshold;     // Strong classfier decision threshold

   // 학습 시 생성되는 값들
   public:
      int      MaxTreeDepth;  // Maximum tree depth
      int      NumWCs;        // # of weak classifiers
      int      NumFeatures;   // Feature vector size
      int      NumSamples;    // # of training samples
      double   HCRThreshold;  // Hard-cascade rejection threshold
      DArray1D SCRThresholds; // Soft-cascade rejection thresholds
   
   public:
      BoostedTree* Prev;
      BoostedTree* Next;

   public:
      BoostedTree (   );
      virtual ~BoostedTree (   );

   protected:
      void _Init              (   );
      int  CreateClassifier   (int max_td,int n_wcs,int n_samples);
      int  LoadClassifierData (const char* file_name);
      int  LoadConfig         (const char* file_name);
      int  SaveClassifierData (const char* file_name);
      int  SaveConfig         (const char* file_name);
   
   public:
      virtual void Close (   );
   
   public:
      int    Classify          (float* s_array,int length,double rt_offset,double& score);
      int    Classify_HC       (float* s_array,int length,double rt_offset,double& score);
      int    Classify_SC       (float* s_array,int length,double rt_offset,double& score);
      // s_array : Feature vector
      // length  : Feature vector size
      // s_wc_idx: Index number of the first WC 
      // e_wc_idx: Index number of the last WC
      double Evaluate          (float* s_array,int length);
      double Evaluate          (float* s_array,int length,int s_wc_idx,int e_wc_idex);
      int    GetNumWCs         (   );
      int    GetHCRThreshold   (FArray2D& fv_array,FArray1D& cl_array,int n_samples);
      int    GetSCRThresholds  (FArray2D& fv_array,FArray1D& cl_array,int n_samples);
      int    IsCreated         (   );
      int    IsTrained         (   );
      int    Load              (const char* base_file_name);
      // fv_array: Feature vector array (1 feature vector per row)
      // cl_array: Class label array
      // max_td  : Maximum tree depth
      // max_wcc : Maximum weak classifier count
      int    Save              (const char* base_file_name);
      int    Train             (FArray2D& fv_array,FArray1D& cl_array,int n_samples,int max_td,int n_wcs);
      int    Test_Org          (FArray2D& fv_array,FArray1D& cl_array,int n_samples,float rt_offset,int& n_pos,int& n_neg,int& n_tps,int& n_fps);
      int    Test_HC           (FArray2D& fv_array,FArray1D& cl_array,int n_samples,float rt_offset,int& n_pos,int& n_neg,int& n_tps,int& n_fps);
      int    Test_SC           (FArray2D& fv_array,FArray1D& cl_array,int n_samples,float rt_offset,int& n_pos,int& n_neg,int& n_tps,int& n_fps);
};

#endif

///////////////////////////////////////////////////////////////////////////////
// 
// Class: BoostedTreeCascade
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_ML)

#define BOOSTED_TREE   BoostedTree

 class BoostedTreeCascade : public LinkedList<BOOSTED_TREE>

{
   public:
      int    CascadeMode;  // Cascade mode
      double HCRTOffset;   // Hard-cascade rejection threshold offset
      double SCRTOffset;   // Soft-cascade rejection threshold offset

   public:
      int   MaxTreeDepth;  // Maximum depth of a decision tree (= weak classifier)
      int   InitNumWCs;    // Number of weak classifiers in the first stage when training a hard-cascade classifier
      int   IncNumWCs;     // Increment of the number of weak classifiers per stage when training a hard-cascade classifier
      int   MaxNumStages;  // Maximum number of stages when training a hard-cascade classifier
      int   NumNSBTimes;   // Number of times of negative sample bootstrapping when training a soft-cascade classifier
      float MinNPSCRatio;  // Minimum NPSCR (Negative-to-Positive Sample Count Ratio) ([# of negative samples] = [# of positive samples] * NPSCR)
      float InitNPSCRatio; // NPSCR for collecting initial negative samples
      float BootNPSCRatio; // NPSCR for collecting bootstrapped negative samples

   public:
      int NumFeatures;
      int NumStages;

   public:
      BoostedTreeCascade (   );
      virtual ~BoostedTreeCascade (   );

   protected:
      int LoadConfig (const char* base_file_name);
      int SaveConfig (const char* base_file_name);

   public:
      virtual void Close (   );
   
   public:
      int Classify                (float* s_array,int length,double& score);
      int Initialize              (   );
      int IsTrained               (   );
      int Load                    (const char* path_name);
      int Save                    (const char* path_name);
      int GetNumFPSamples         (FArray2D& fv_array,FArray1D& cl_array,int n_samples,int& n_neg,int& n_fps);
      int GetNumTPSamples         (FArray2D& fv_array,FArray1D& cl_array,int n_samples,int& n_pos,int& n_tps);
      int TrainNewStageClassifier (FArray2D& fv_array,FArray1D& cl_array,int n_samples,int n_wcs);
};

#endif

///////////////////////////////////////////////////////////////////////////////
// 
// Class: OpenCV_BoostedTree
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_OPENCV)

 class OpenCV_BoostedTree : public CvBoost

{
   // 직접 설정 가능 값들
   public:
      float SCDThreshold;

   // 학습 시 생성되는 값들
   public:
      float    HCRThreshold;
      FArray1D SCRThresholds1;
      FArray1D SCRThresholds2;
   
   public:
      OpenCV_BoostedTree* Prev;
      OpenCV_BoostedTree* Next;

   public:
      OpenCV_BoostedTree (   );
      virtual ~OpenCV_BoostedTree (   );

   protected:
      int LoadConfig (const char* file_name);
      int SaveConfig (const char* file_name);
   
   public:
      virtual void Close (   );
   
   public:
      int   Classify          (float* s_array,int length);
      int   Classify_HC       (float* s_array,int length,float rt_offset,float& score);
      int   Classify_SC1      (float* s_array,int length,float rt_offset,float& score);
      int   Classify_SC2      (float* s_array,int length,float rt_offset,float& score);
      float Evaluate          (float* s_array,int length);
      // s_array : Feature vector
      // length  : Feature vector size
      // s_wc_idx: Index number of the first WC 
      // e_wc_idx: Index number of the last WC
      float Evaluate          (float* s_array,int length,int s_wc_idx,int e_wc_idex);
      int   GetNumWCs         (   );
      int   GetHCRThreshold   (FArray2D& fv_array,FArray1D& cl_array,int n_samples);
      int   GetSCRThresholds1 (FArray2D& fv_array,FArray1D& cl_array,int n_samples);
      int   GetSCRThresholds2 (FArray2D& fv_array,FArray1D& cl_array,int n_samples);
      int   IsTrained         (   );
      int   Load              (const char* base_file_name);
      int   Save              (const char* base_file_name);
      // fv_array: Feature vector array (1 feature vector per row)
      // cl_array: Class label array
      // max_td  : Maximum tree depth
      // max_wcc : Maximum weak classifier count
      int   Train             (FArray2D& fv_array,FArray1D& cl_array,int n_samples,int max_td,int max_wcc);
      int   Test_Org          (FArray2D& fv_array,FArray1D& cl_array,int n_samples,float rt_offset,int& n_pos,int& n_neg,int& n_tps,int& n_fps);
      int   Test_SC1          (FArray2D& fv_array,FArray1D& cl_array,int n_samples,float rt_offset,int& n_pos,int& n_neg,int& n_tps,int& n_fps);
      int   Test_SC2          (FArray2D& fv_array,FArray1D& cl_array,int n_samples,float rt_offset,int& n_pos,int& n_neg,int& n_tps,int& n_fps);
};

#endif

}

#endif

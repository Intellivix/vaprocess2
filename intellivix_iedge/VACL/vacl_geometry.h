#if !defined(__VACL_GEOMETRY_H)
#define __VACL_GEOMETRY_H

#include "vacl_define.h"

 namespace VACL

{

#define RM_UNIFORM_TILT   0x0001
#define RM_CENTER         0x0002

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

int    CheckHomography            (Matrix& mat_H,float min_sc = 0.85f,float max_sc = 1.15f);
// 두 벡터 사이의 각도(범위: 0도 ~ 180도)를 리턴한다.
float  GetAngle                   (FPoint2D &s_vec1,FPoint2D &s_vec2);
void   GetEulerAngles_XYZ         (Matrix &mat_R,double &psi,double &theta,double &phi);
void   GetEulerAngles_ZXZ         (Matrix &mat_R,double &psi,double &theta,double &phi);
void   GetHomography              (FP2DArray1D &s_points1,FP2DArray1D &s_points2,Matrix &mat_H,int flag_normalize = TRUE);
void   GetHomography              (IP2DArray1D &s_points1,IP2DArray1D &s_points2,Matrix &mat_H,int flag_normalize = TRUE);
void   GetHomography              (FP3DArray1D &s_points1,FP3DArray1D &s_points2,Matrix &mat_H);
void   GetHomographyForRotation   (float cx,float cy,float angle,Matrix& mat_H);
void   GetTransformedPosition     (FPoint2D& s_point,Matrix& mat_H,FPoint2D& d_point);
void   GetTransformedPositions    (int si_width,int si_height,Matrix& mat_H,FP2DArray1D& d_points);
void   GetHomography_2DEuclidean  (double theta,double tx,double ty,Matrix &mat_H);
int    GetIntersectionPoint       (FPoint3D &vn,float d,FLine3D &s_line,FPoint3D &d_point);
int    GetIntersectionPoint       (FLine2D &s_line1,FLine2D &s_line2,FPoint2D &d_point);
void   GetRotationAngles_Cylinder (double phi,double tx,double ty,FPoint2D &lt_pos,FPoint2D &rb_pos,int img_width,int img_height,double &psi,double &theta);
void   GetRotationMatrices        (Array1D<Matrix> &mats_Rr,Array1D<Matrix> &mats_R,int option);
void   GetRotationMatrix_PT       (double pan_angle,double tilt_angle,Matrix &mat_R);
void   GetRotationMatrix_X        (double angle,Matrix &mat_R);
void   GetRotationMatrix_Y        (double angle,Matrix &mat_R);
void   GetRotationMatrix_Z        (double angle,Matrix &mat_R);
void   GetRotationMatrix_XYZ      (double psi,double theta,double phi,Matrix &mat_R);
void   GetRotationMatrix_ZXZ      (double psi,double theta,double phi,Matrix &mat_R);
Matrix NormalizeImageCoordinates  (FP2DArray1D &s_points,FP2DArray1D &d_points);

}

#endif

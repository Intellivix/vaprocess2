#if !defined(__IVCP_H)
#define __IVCP_H

#include "ivcp_define.h"
#include "ivcp_environ.h"
#include "ivcp_packet.h"
#include "ivcp_pkstruct.h"

using namespace IVCP;

#endif

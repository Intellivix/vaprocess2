#if !defined(__VACL_DNN_H)
#define __VACL_DNN_H

#include "vacl_boxtrack.h"

#if defined(__LIB_OPENVINO)
   #if defined(__OS_WINDOWS)
   #pragma warning(disable: 4251)
   #endif
   #include <inference_engine.hpp>
   using namespace InferenceEngine;
#endif

#if defined(__LIB_DARKNET)
   #if defined(__LIB_CUDA)
      #define __GPU
      #define __CUDNN
   #endif
   #include "darknet/darknet.h"
#endif

#if defined(__LIB_CAFFE)
   #include "vacl_opencv.h"
//   #include "caffe/caffe.hpp"
#endif

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: DNN_Caffe_ObjectClassifier
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_CAFFE)

/* Pair (label, confidence) representing a prediction. */
typedef std::pair<std::string, float> Prediction;

typedef void* (*Caffe_InitializeFunc)    (const std::string&,const std::string&,const std::string&,const std::string&,const int,const int);
typedef void  (*Caffe_UninitializeFunc)  (void*);
typedef void  (*Caffe_PredictFunc)       (void*,const cv::Mat&,std::vector<float>&);
typedef void  (*Caffe_PredictBatchFunc)  (void*,const std::vector<cv::Mat>&,std::vector<std::vector<float> >&);
typedef int   (*Caffe_ClassifyFunc)      (void*,const cv::Mat&,std::vector<Prediction>&);
typedef int   (*Caffe_ClassifyBatchFunc) (void*,const std::vector<cv::Mat>&,std::vector<std::vector<Prediction> >&);
typedef void  (*Caffe_GetInputSizeFunc)  (void*,int&,int&);

 class DNN_Caffe_ObjectClassifier
 
{
   protected:
      int Flag_Initialized;

   private:
      void* Caffe_Handle;
      Caffe_InitializeFunc Caffe_Initialize;
      Caffe_UninitializeFunc Caffe_Uninitialize;
      Caffe_PredictFunc Caffe_Predict;
      Caffe_PredictBatchFunc Caffe_PredictBatch;
      Caffe_ClassifyFunc Caffe_Classify;
      Caffe_ClassifyBatchFunc Caffe_ClassifyBatch;
      Caffe_GetInputSizeFunc Caffe_GetInputSize;

   public:
      ISize2D     SrcImgSize;
      STRArray1DA ClassNames;
   
   public:
      DNN_Caffe_ObjectClassifier (   );
      virtual ~DNN_Caffe_ObjectClassifier (   );

   protected:
      void Preprocess     (const cv::Mat& img,std::vector<cv::Mat>* input_channels,int n);
      void SetMean        (const std::string& mean_file);
      void WrapInputLayer (std::vector<cv::Mat>* input_channels,int n);
   
   public:
      virtual void Close (   );
   
   public:
      std::vector<Prediction> Classify               (cv::Mat& img,int N = 1);
      std::vector<std::vector<Prediction> > Classify (const std::vector<cv::Mat>& imgs,int N = 1);
      // Windows
      //  HMODULE handle = LoadLibrary("DNNClassifier.DLL");
      //  FreeLibrary(handle);
      int  Initialize                                (void* dll_handle, const char* cfg_file_name,const char* wgt_file_name,const char* cls_file_name,const char* mns_file_name,int flag_use_gpu = TRUE, int gpu_idx = 0);
      int  IsInitialized                             (   );
      void Predict                                   (const cv::Mat& img,std::vector<float>& output);
      void Predict                                   (const std::vector<cv::Mat>& imgs,std::vector<std::vector<float> >& outputs);
      int SetProcAddresses                           (void* dll_handle);

   // [주의] Initialize() 함수와 Predict()/Classify() 함수는 동일 쓰레드 내에서 호출해야 한다.
   //        만약 그렇지 않으면 GPU 모드로 동작하지 않는다.
};

 inline int DNN_Caffe_ObjectClassifier::IsInitialized (   )

{
   return (Flag_Initialized);
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: DNN_Darknet_YOLO
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_DARKNET)

typedef network*   (*Yolo_InitializeFunc)     (const char*,const char*,int);
typedef void       (*Yolo_UninitializeFunc)   (network*);
typedef detection* (*Yolo_PredictFunc)        (network*,float*,int,int,float,float,int&,int&);
typedef void       (*Yolo_FreeDetectionsFunc) (detection*,int);

 class DNN_Darknet_YOLO

{
   protected:
      int      Flag_Initialized;
      network* Network;

   public:
      float DThreshold;
      float NMSThreshold;

   public:
      ISize2D     SrcImgSize;
      STRArray1DA ClassNames;

   public:
      Yolo_InitializeFunc     Yolo_Initialize;
      Yolo_UninitializeFunc   Yolo_Uninitialize;
      Yolo_PredictFunc        Yolo_Predict;
      Yolo_FreeDetectionsFunc Yolo_FreeDetections;
   
   public:
      DNN_Darknet_YOLO (   );
      virtual ~DNN_Darknet_YOLO (   );
   
   protected:
      void Convert                (BGRImage& s_image,FArray1D& d_array);
      void GetObjectBoundingBoxes (int si_width,int si_height,detection* detections,int n_detections,int n_classes,OBBArray1D& d_array);
      int  LoadClassNames         (const char* cls_file_name);
   
   public:
      virtual void Close (   );
   
   public:
      int Initialize    (void* dll_handle,const char* cfg_file_name,const char* wgt_file_name,const char* cls_file_name, int gpu_idx = 0);
      int IsInitialized (   );
      int Predict       (BGRImage& s_cimage,OBBArray1D& d_array);
      int SetProcAddresses  (void* dll_handle);
};

 inline int DNN_Darknet_YOLO::IsInitialized (   )

{
   return (Flag_Initialized);
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: DNN_OpenVINO
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_OPENVINO)

 class DNN_OpenVINO

{
   protected:
      int Flag_Initialized;

   protected:
      InferRequest::Ptr  Request;
      InferencePlugin*   Plugin;
      ExecutableNetwork* Network;

   public:
      int BatchSize;

   public:
      int            NumSrcChannels;
      ISize2D        SrcImgSize;
      InputsDataMap  InputInfo;
      OutputsDataMap OutputInfo;
   
   public:
      DNN_OpenVINO (   );
      virtual ~DNN_OpenVINO (   );

   protected:
      void _Init (   );

   protected:
      virtual void ConfigureInput  (   );
      virtual void ConfigureOutput (   );
   
   public:
      virtual void Close (   );

   public:
      int Infer         (   );
      int Initialize    (const char* cfg_file_name,const char* wgt_file_name,int flag_use_gpu = FALSE);
      int IsInitialized (   );
      int SetInput_INT8 (BGRImage& s_cimage,int batch_idx = 0);
      int SetInput_FP32 (BGRImage& s_cimage,int batch_idx = 0);
};

 inline int DNN_OpenVINO::IsInitialized (   )

{
   return (Flag_Initialized);
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: DNN_OpenVINO_ObjectDetector
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_OPENVINO)

 class DNN_OpenVINO_ObjectDetector : public DNN_OpenVINO

{
   public:
      float DThreshold;
   
   public:
      DNN_OpenVINO_ObjectDetector (   );
   
   protected:
      int GetOutput (int si_width,int si_height,OBBArray1D& d_array);

   public:
      int Predict (BGRImage& s_cimage,OBBArray1D& d_array);
};

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: DNN_OpenVINO_FacePoseEstimator
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_OPENVINO)

 class DNN_OpenVINO_FacePoseEstimator : public DNN_OpenVINO

{
   public:
      DNN_OpenVINO_FacePoseEstimator (   );

   protected:
      virtual void ConfigureInput (   );

   protected:
      int GetOutput (float& yaw,float& pitch,float& roll);
   
   public:
      int Predict (BGRImage& s_cimage,float& d_yaw,float& d_pitch,float& d_roll);
};

#endif

}

#endif

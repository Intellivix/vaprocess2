#include "vacl_event.h"
#include "vacl_dwell.h"
#include "vacl_misc.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: DwellZone
//
///////////////////////////////////////////////////////////////////////////////

 DwellZone::DwellZone (   )

{
   ExitObjectCount = 0;
   DwellTimeSum    = 0.0f;
   EvtZone         = NULL;
}

 int DwellZone::DetectEvent (   )

{
   if (EvtZone  == NULL) return (0);
   EventRule* evt_rule = EvtZone->EvtRule;
   if (evt_rule == NULL) return (0);
   EventRuleConfig_Dwell& erc   = evt_rule->ERC_Dwell;
   EventDetection* evt_detector = evt_rule->EventDetector;
   if (evt_detector == NULL) return (0);
   int n_objects = 0;
   int flag_detection = FALSE;
   if (EvtZone->CurDwellObjectCount > erc.MaxZoneCapacity) flag_detection = TRUE;
   Event* m_event = NULL;
   TrackedObject* eg_object = NULL;
   TrackedObject* t_object  = evt_detector->EvtGenObjects.First (   );
   while (t_object != NULL) {
      Event* event = t_object->FindEvent (EvtZone,ER_EVENT_DWELL_OVERCROWDED);
      if (event != NULL) {
         m_event   = event;
         eg_object = t_object;
         break;
      }
      t_object = evt_detector->EvtGenObjects.Next (t_object);
   }
   if (m_event == NULL) {
      if (flag_detection) {
         eg_object         = CreateInstance_TrackedObject (   );
         eg_object->ID     = evt_detector->GetNewObjectID (   );
         eg_object->Status = TO_STATUS_VALIDATED | TO_STATUS_VERIFIED;
         eg_object->Type   = TO_TYPE_HUMAN; // WARNING: 객체 타입이 반드시 사람이 아닐 수도 있다!
         m_event = CreateInstance_Event  (EvtZone,evt_rule,ER_EVENT_DWELL_OVERCROWDED);
         m_event->StartTime.GetLocalTime (   );
         eg_object->Events.Add (m_event);
         evt_detector->EvtGenObjects.Add (eg_object);
         n_objects = 1;
      }
   }
   else if (m_event->EvtRule == evt_rule) {
      m_event->ElapsedTime += evt_detector->FrameDuration;
      switch (m_event->Status) {
         case EV_STATUS_BEGUN:
            m_event->Status = EV_STATUS_ONGOING;
            break;
         case EV_STATUS_ONGOING:
            if (!flag_detection) {
               m_event->Status = EV_STATUS_ENDED | EV_STATUS_TO_BE_REMOVED;
               m_event->EndTime.GetLocalTime (   );
               eg_object->Status |= TO_STATUS_TO_BE_REMOVED;
             }
             break;
         default:
             break;
      }
   }
   if (flag_detection) eg_object->SetRegion (EntObjRegion);
   evt_rule->ERC_Counting.Count += n_objects;
   return (n_objects);   
}

 void DwellZone::ResetCounters (   )

{
   ExitObjectCount                = 0;
   DwellTimeSum                   = 0.0f;
   EvtZone->CurDwellObjectCount   = 0;
   EvtZone->TotalDwellObjectCount = 0;
   EvtZone->AverageDwellTime      = 0.0f;
}

// NOTE(yhpark): removed. rapidxml.
// int DwellZone::SaveDataFile (XMLFile& file)

//{
//   if (file.WriteTagStart ("DwellZone")) return (1);
//   if (file.WriteAttribute ("ID",EvtZone->ID)) return (1);
//   if (file.WriteTagEnd (   )) return (1);
//   file.IncreaseIndentLevel (   );
//   if (file.WriteNode ("TotalDwellObjectCount",EvtZone->TotalDwellObjectCount)) return (1);
//   if (file.WriteNode ("ExitObjectCount",ExitObjectCount)) return (1);
//   if (file.WriteNode ("DwellTimeSum",DwellTimeSum)) return (1);
//   file.DecreaseIndentLevel (   );
//   if (file.WriteNodeEnd ("DwellZone")) return (1);
//   return (DONE);
//}

 int DwellZone::SaveDataFile (CXMLIO* pIO)

{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("DwellZone")) {
      xmlNode.Attribute (TYPE_ID(int)  ,"ID",&EvtZone->ID);
      xmlNode.Attribute (TYPE_ID(int)  ,"TotalDwellObjectCount",&EvtZone->TotalDwellObjectCount);
      xmlNode.Attribute (TYPE_ID(int)  ,"ExitObjectCount"      ,&ExitObjectCount);
      xmlNode.Attribute (TYPE_ID(float),"DwellTimeSum"         ,&DwellTimeSum);
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}


///////////////////////////////////////////////////////////////////////////////
//
// Class: DwellAnalysis
//
///////////////////////////////////////////////////////////////////////////////

 DwellAnalysis::DwellAnalysis (   )
 
{
   Flag_Enabled = TRUE;
   FrameCount   = 0;
   FrameRate    = 0.0f;
}

 DwellAnalysis::~DwellAnalysis (   )

{
   Close (   );
}

 void DwellAnalysis::Close (   )

{
   DataFileName.Delete (   );
   DwellZones.Delete   (   );
}
 
 DwellZone* DwellAnalysis::FindZone (int zone_id)

{
   DwellZone* dw_zone = DwellZones.First (   );
   while (dw_zone != NULL) {
      if (dw_zone->EvtZone->ID == zone_id) break;
      dw_zone = DwellZones.Next (dw_zone);
   }
   return (dw_zone);
}

 int DwellAnalysis::Initialize (EventDetection& evt_detector,const char* data_file_name)

{
   Close (   );
   if (!IsEnabled (   )) return (1);
   FrameCount = 0;
   PrevTime.GetLocalTime (   );
   SrcImgSize = evt_detector.SrcImgSize;
   FrameRate  = evt_detector.FrameRate;
   EventZoneList& ez_list = evt_detector.EventZones;
   // 이벤트 종류가 "Dwell"인 이벤트 존들을 찾아 등록한다.
   EventZone* evt_zone = ez_list.First (   );
   while (evt_zone != NULL) {
      if (evt_zone->EvtRule->EventType == ER_EVENT_DWELL) {
         DwellZone* dw_zone = new DwellZone;
         dw_zone->EvtZone = evt_zone;
         DwellZones.Add (dw_zone);
      }
      evt_zone = ez_list.Next (evt_zone);
   }
   ResetZoneCounters (   );
   if (data_file_name != NULL) {
      DataFileName = data_file_name;
      if (LoadDataFile (   )) return (2);
   }
   return (DONE);
}

 int DwellAnalysis::LoadDataFile (   )

{
   if (!DataFileName) return (DONE);
   // jun : IntelliVIX의 설정정보와 함께 저장될 필요가 있기 때문에 Attribute 저장하고 로드하는 기능을 구현하였습니다. 
   FileIO file;
   if (DONE == file.Open (DataFileName,FIO_BINARY,FIO_READ)) {
      CXMLIO xmlIO (&file,XMLIO_Read);
      CXMLNode xmlNode (&xmlIO);
      if (xmlNode.Start ("DwellZoneList")) {
         int nDwellZoneCount = 0;
         xmlNode.Attribute (TYPE_ID(int),"DwellZoneCount",&nDwellZoneCount);
         int i;
         for (i = 0; i < nDwellZoneCount; i++) {
            int   zoneID = -1;
            int   totalDwellObejectCount = 0;
            int   exitObjectCount = 0;
            float dwellTimeSum = 0.0f;
            CXMLNode xmlNode (&xmlIO);
            if (xmlNode.Start ("DwellZone")) {
               xmlNode.Attribute (TYPE_ID(int),"ID",&zoneID);
               DwellZone* dw_zone = FindZone (zoneID);
               if (dw_zone != NULL) {
                  if (xmlNode.Attribute (TYPE_ID(int),"TotalDwellObjectCount",&totalDwellObejectCount)) 
                     dw_zone->EvtZone->TotalDwellObjectCount = totalDwellObejectCount;
                  if (xmlNode.Attribute (TYPE_ID(int),"ExitObjectCount",&exitObjectCount)) 
                     dw_zone->ExitObjectCount = exitObjectCount;
                  if (xmlNode.Attribute (TYPE_ID(float),"DwellTimeSum",&dwellTimeSum)) 
                     dw_zone->DwellTimeSum = dwellTimeSum;
                  if (dw_zone->ExitObjectCount) dw_zone->EvtZone->AverageDwellTime = dw_zone->DwellTimeSum / dw_zone->ExitObjectCount;
               }
               xmlNode.End (   );
            }
         }
         xmlNode.End (   );
         file.Close (   );
      }
      return (DONE);
   }
   return (DONE);
}

 void DwellAnalysis::Perform (ObjectTracking& obj_tracker,DwellZone* dw_zone)

{
   EventZone* evt_zone = dw_zone->EvtZone;
   EventRule* evt_rule = evt_zone->EvtRule;
   int n_cur_objects = 0;    // 현재 존에 체류 중인 객체 수
   int n_ent_objects = 0;    // 존에 진입한 객체 수
   int n_ext_objects = 0;    // 존을 떠난 객체 수
   float sum_dw_time = 0.0f; // 존을 떠난 객체들의 체류 시간의 합
   float max_dw_time = 0.0f;
   IPoint2D min_p( 100000, 100000);
   IPoint2D max_p(-100000,-100000);
   TrackedObject* m_object = NULL;
   TrackedObject* t_object = obj_tracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      Event* event = t_object->FindEvent (evt_zone,ER_EVENT_DWELL);
      if (event) {
         int n_objects = 1;
         if (evt_rule->Options & ER_OPTION_ENABLE_COUNTER)
            n_objects = evt_rule->EstimateObjectCount (t_object,evt_zone);
         if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED)) n_cur_objects += n_objects;
         if (event->Status & EV_STATUS_BEGUN) n_ent_objects += n_objects;
         TrackedObject::EvtPrgTime* ep_time = t_object->GetEventProgressTime (evt_zone);
         if (event->Status & EV_STATUS_ENDED) {
            n_ext_objects += n_objects;
            sum_dw_time   += ep_time->PrgTime;
         }
         if (max_dw_time < ep_time->PrgTime) {
            max_dw_time = ep_time->PrgTime;
            m_object    = t_object;
         }
         int sx = t_object->X;
         int sy = t_object->Y;
         int ex = sx + t_object->Width;
         int ey = sy + t_object->Height;
         if (sx < min_p.X) min_p.X = sx;
         if (sy < min_p.Y) min_p.Y = sy;
         if (ex > max_p.X) max_p.X = ex;
         if (ey > max_p.Y) max_p.Y = ey;
      }
      t_object = obj_tracker.TrackedObjects.Next (t_object);
   }
   if (m_object) m_object->Status |= TO_STATUS_MAX_DWELL_TIME;
   evt_zone->CurDwellObjectCount    = n_cur_objects;
   evt_zone->TotalDwellObjectCount += n_ent_objects;
   dw_zone->ExitObjectCount        += n_ext_objects;
   dw_zone->DwellTimeSum           += sum_dw_time;
   if (dw_zone->ExitObjectCount) evt_zone->AverageDwellTime = dw_zone->DwellTimeSum / dw_zone->ExitObjectCount;
   else evt_zone->AverageDwellTime = 0.0f;
   IBox2D& rgn = dw_zone->EntObjRegion;
   if (obj_tracker.TrackedObjects.GetNumItems (   )) {
      rgn.X      = min_p.X - 5;
      rgn.Y      = min_p.Y - 5;
      rgn.Width  = max_p.X - min_p.X + 10;
      rgn.Height = max_p.Y - min_p.Y + 10;
      CropRegion (obj_tracker.SrcImgSize,rgn);
   }
   else rgn.Clear (   );
}

 int DwellAnalysis::Perform (ObjectTracking &obj_tracker)
 
{
   if (!IsEnabled (   )) return (1);
   if (!DwellZones.GetNumItems (   )) return (2);
   // 이전 날짜와 현재 날짜가 다르면 날이 바뀐 것이므로 존 카운터를 리셋한다.
   Time c_time;
   c_time.GetLocalTime (   );
   if (PrevTime.Day != c_time.Day) ResetZoneCounters (   );
   PrevTime = c_time;
   // 추적 객체의 상태 값의 일부를 초기화한다.
   TrackedObject* t_object = obj_tracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      t_object->Status &= ~TO_STATUS_MAX_DWELL_TIME;      
      t_object = obj_tracker.TrackedObjects.Next (t_object);
   }
   // 각 존마다 Dwell Analysis를 수행한다.
   DwellZone* dw_zone = DwellZones.First (   );
   while (dw_zone != NULL) {
      if (dw_zone->EvtZone->IsEnabled (   )) Perform (obj_tracker,dw_zone);
      dw_zone = DwellZones.Next (dw_zone);
   }
   // 존 카운터 값들을 주기적으로 파일로 저장한다.
   int period = (int)(FrameRate + 0.5f);
   if (!(FrameCount % period)) SaveDataFile (   );
   FrameCount++;
   return (DONE);
}

 void DwellAnalysis::ResetZoneCounters (   )

{
   DwellZone* dw_zone = DwellZones.First (   );
   while (dw_zone != NULL) {
      dw_zone->ResetCounters (   );
      dw_zone = DwellZones.Next (dw_zone);
   }
}

 int DwellAnalysis::SaveDataFile (   )

{
   if (!DataFileName) return (1);
   FileIO buffer;
   CXMLIO xmlIO (&buffer,XMLIO_Write);
   CXMLNode xmlNode (&xmlIO);
   if (xmlNode.Start ("DwellZoneList")) {
      int nDwellZoneCount = DwellZones.GetNumItems ();
      xmlNode.Attribute (TYPE_ID(int),"DwellZoneCount", &nDwellZoneCount);
      DwellZone* dw_zone = DwellZones.First (   );
      while (dw_zone != NULL) {
         dw_zone->SaveDataFile (&xmlIO);
         dw_zone = DwellZones.Next (dw_zone);
      }
      xmlNode.End (   );
   }
   FileIO file;
   if (file.Open (DataFileName,FIO_BINARY,FIO_CREATE) == DONE) {
      file.Write (buffer,1,buffer.GetLength() - 1);
      file.Close (   );
   }
   return (DONE);
 }

}

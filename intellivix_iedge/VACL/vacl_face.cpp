#include "vacl_dnn.h"
#include "vacl_event.h"
#include "vacl_face.h"
#include "vacl_objtrack.h"
#include "vacl_warping.h"

#if defined(__LIB_OPENCV)
#include "vacl_opencv.h"
#endif

#if defined(__LIB_DLIB)
#include "dlib/opencv.h"
#endif

#if defined(__LIB_SEETA)
#if defined(_DEBUG)
#pragma comment(lib,"SeetaFaceEngine_DS.lib")
#else
#pragma comment(lib,"SeetaFaceEngine_RS.lib")
#endif
#endif

#if defined(__LIB_VERILOOK)
#include <NCoreLibrary.cpp>
#include <NGuiLibrary.cpp>
#include <NDevicesLibrary.cpp>
#include <NBiometricsLibrary.cpp>
#include <NMediaLibrary.cpp>
#include <NLicensingLibrary.cpp>

#pragma comment(lib,"NCore.dll.lib")
#pragma comment(lib,"NMedia.dll.lib")
#pragma comment(lib,"NMediaProc.dll.lib")
#pragma comment(lib,"NBiometrics.dll.lib")
#pragma comment(lib,"NLicensing.dll.lib")
#pragma comment(lib,"NDevices.dll.lib")

using namespace Neurotec::Biometrics;
using namespace Neurotec::Images;
using namespace Neurotec::Licensing;
using namespace Neurotec;
#endif

#if defined(__DEBUG_FPD)
#include "Win32CL/Win32CL.h"
extern int _DVX_FPD,_DVY_FPD;
extern CImageView* _DebugView_FPD;
#endif

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: FaceInfo
//
///////////////////////////////////////////////////////////////////////////////

 FaceInfo::FaceInfo (   )

{
   Flag_Occluded           = FALSE;
   Time_FaceOccluded       = 0.0f;
   Time_FaceUndetected     = 0.0f;
   Time_LeftEyeUndetected  = 0.0f;
   Time_RightEyeUndetected = 0.0f;
   Time_MouthUndetected    = 0.0f;
   Clear (   );
}

 int FaceInfo::AreLandmarksValid (   )

{
   if (Eyes[0].X == MC_VLV || Eyes[1].X == MC_VLV) return (FALSE);
   else return (TRUE);
}

 void FaceInfo::Clear (   )

{
   IBox2D::Clear (   );
   Eyes[0](MC_VLV,MC_VLV);
   Eyes[1](MC_VLV,MC_VLV);
   Nose(MC_VLV,MC_VLV);
   Mouth[0](MC_VLV,MC_VLV);
   Mouth[1](MC_VLV,MC_VLV);
   Points.Delete (   );
   Pitch = Yaw = Roll = MC_VLV;
   Pose(MC_VLV,MC_VLV);
   LeftEyeRegion.Clear  (   );
   RightEyeRegion.Clear (   );
   MouthRegion.Clear    (   );
}

 int FaceInfo::IsBoundingBoxValid (   )

{
   if (Width && Height) return (TRUE);
   else return (FALSE);
}

 int FaceInfo::IsPoseValid (   )

{
   if (Pitch == MC_VLV || Yaw == MC_VLV || Roll == MC_VLV) return (FALSE);
   else return (TRUE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: FaceDetection
//
///////////////////////////////////////////////////////////////////////////////

 FaceDetection::FaceDetection (   )

{
   Flag_Initialized = FALSE;
}

 FaceDetection::~FaceDetection (   )

{
   Close (   );
}

 void FaceDetection::Close (   )

{
   Flag_Initialized = FALSE;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: FaceDetection_OpenCV
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_OPENCV)

 FaceDetection_OpenCV::FaceDetection_OpenCV (   )

{
   MinFaceSize   = 36;
   NMSThreshold  = 3;
   IPScaleFactor = 1.1f;
}

 int FaceDetection_OpenCV::Initialize (const char* dir_name)

{
   const char file_name[] = "IVXFCDOCV1.dat";
   
   Close (   );
   StringA path_name;
   if (dir_name == NULL) path_name = file_name;
   else {
      #if defined(__OS_WINDOWS)
      path_name.Format ("%s\\%s",dir_name,file_name);
      #else
      path_name.Format ("%s/%s",dir_name,file_name);
      #endif
   }
   string path_name2 = (char*)path_name;
   if (!FaceDetector.load (path_name2)) return (1);
   Flag_Initialized = TRUE;
   return (DONE);
}

 int FaceDetection_OpenCV::Perform (GImage& s_image,IB2DArray1D& d_rects)

{
   int i;

   d_rects.Delete (   );
   if (!Flag_Initialized) return (1);
   Mat m_s_image;
   OpenCV_InitMat (s_image,m_s_image);
   vector<Rect> rects;
   FaceDetector.detectMultiScale (m_s_image,rects,IPScaleFactor,NMSThreshold,0,Size(MinFaceSize,MinFaceSize));
   int n = (int)rects.size (   );
   if (!n) return (DONE);
   d_rects.Create (n);
   for (i = 0; i < n; i++) {
      IBox2D& rect = d_rects[i];
      rect.X      = rects[i].x;
      rect.Y      = rects[i].y;
      rect.Width  = rects[i].width;
      rect.Height = rects[i].height;
   }
   return (DONE);
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: FaceDetection_OpenVINO
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_OPENVINO)

 FaceDetection_OpenVINO::FaceDetection_OpenVINO (   )

{
   Flag_Advanced = FALSE;
   Flag_UseGPU   = FALSE;
   DThreshold    = FaceDetector.DThreshold;
}

 void FaceDetection_OpenVINO::Close (   )

{
   FaceDetector.Close   (   );
   FaceDetection::Close (   );
}

 int FaceDetection_OpenVINO::Initialize (const char* dir_name)

{
   const char cfg_file_name_a[] = "IVXFDACOV1.dat"; // face-detection-adas-0001.xml
   const char wgt_file_name_a[] = "IVXFDAWOV1.dat"; // face-detection-adas-0001.bin
   const char cfg_file_name_b[] = "IVXFDBCOV1.dat"; // face-detection-retail-0004.xml
   const char wgt_file_name_b[] = "IVXFDBWOV1.dat"; // face-detection-retail-0004.bin

   Close (   );
   StringA cfg_file_name = cfg_file_name_b;
   StringA wgt_file_name = wgt_file_name_b;
   if (Flag_Advanced) {
      cfg_file_name = cfg_file_name_a;
      wgt_file_name = wgt_file_name_a;
   }
   StringA cfg_path_name,wgt_path_name;
   if (dir_name == NULL) {
      cfg_path_name = cfg_file_name;
      wgt_path_name = wgt_file_name;
   }
   else {
      #if defined(__OS_WINDOWS)
      cfg_path_name.Format ("%s\\%s",dir_name,(char*)cfg_file_name);
      wgt_path_name.Format ("%s\\%s",dir_name,(char*)wgt_file_name);
      #else
      cfg_path_name.Format ("%s/%s",dir_name,(char*)cfg_file_name);
      wgt_path_name.Format ("%s/%s",dir_name,(char*)wgt_file_name);
      #endif
   }
   int e_code = FaceDetector.Initialize (cfg_path_name,wgt_path_name,Flag_UseGPU);
   if (e_code) return (e_code);
   Flag_Initialized = TRUE;
   return (DONE);
}

 int FaceDetection_OpenVINO::Perform (BGRImage& s_cimage,IB2DArray1D& d_rects)

{
   int i;
   
   OBBArray1D d_array;
   FaceDetector.DThreshold = DThreshold;
   int e_code = FaceDetector.Predict (s_cimage,d_array);
   if (e_code) return (e_code);
   if (!d_array) d_rects.Delete (   );
   else {
      d_rects.Create (d_array.Length);
      for (i = 0; i < d_array.Length; i++) d_rects[i].Set (d_array[i]);
   }
   return (DONE);
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: FaceDetection_Seeta
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_SEETA)

 FaceDetection_Seeta::FaceDetection_Seeta (   )

{
   FaceDetector  = NULL;

   MinFaceSize   = 36;
   MaxFaceSize   = 1000;
   SWStepSize    = 8;
   IPScaleFactor = 0.7f;
   DThreshold    = 1.7f;
}

 void FaceDetection_Seeta::Close (   )

{
   if (FaceDetector != NULL) {
       delete FaceDetector;
       FaceDetector = NULL;
   }
   FaceDetection::Close (   );
}

 int FaceDetection_Seeta::Initialize (const char* dir_name)

{
   const char file_name[] = "IVXFCDSFE1.dat";
   
   Close (   );
   StringA path_name;
   if (dir_name == NULL) path_name = file_name;
   else {
      #if defined(__OS_WINDOWS)
      path_name.Format ("%s\\%s",dir_name,file_name);
      #else
      path_name.Format ("%s/%s",dir_name,file_name);
      #endif
   }
   if (AccessFile (path_name) != DONE) return (1);
   FaceDetector = new seeta::FaceDetection(path_name);
   FaceDetector->SetMinFaceSize             (MinFaceSize);
   FaceDetector->SetMaxFaceSize             (MaxFaceSize);
   FaceDetector->SetWindowStep              (SWStepSize,SWStepSize);
   FaceDetector->SetImagePyramidScaleFactor (IPScaleFactor);
   FaceDetector->SetScoreThresh             (DThreshold);
   Flag_Initialized = TRUE;
   return (DONE);
}

void FaceDetection_Seeta::SetMinFaceSize(int size)
{
   FaceDetector->SetMinFaceSize(size);
}
void FaceDetection_Seeta::SetMaxFaceSize(int size)
{
   FaceDetector->SetMaxFaceSize(size);
}
void FaceDetection_Seeta::SetSWStepSize(int step_size)
{
   FaceDetector->SetWindowStep(step_size,step_size);
}
void FaceDetection_Seeta::SetIPScaleFactor(float s_factor)
{
   FaceDetector->SetImagePyramidScaleFactor(s_factor);
}
void FaceDetection_Seeta::SetFDScoreThreshold(float thres)
{
   FaceDetector->SetScoreThresh(thres);
}

 int FaceDetection_Seeta::Perform (GImage& s_image,IB2DArray1D& d_rects)

{
   int i;
   
   if (!Flag_Initialized) return (1);
   seeta::ImageData s_img_data(s_image.Width,s_image.Height);
   s_img_data.data         = (byte*)s_image;
   s_img_data.num_channels = 1;
   std::vector<seeta::FaceInfo> faces = FaceDetector->Detect (s_img_data);
   int n = (int)faces.size (   );
   if (n) {
      d_rects.Create (n);
      ISize2D c_size(s_image.Width,s_image.Height);
      for (i = 0; i < n; i++) {
         seeta::Rect& s_rect = faces[i].bbox;
         IBox2D& d_rect      = d_rects[i];
         d_rect.X            = s_rect.x;
         d_rect.Y            = s_rect.y;
         d_rect.Width        = s_rect.width;
         d_rect.Height       = (int)(1.1f * s_rect.height); // PARAMETERS
         CropRegion (c_size,d_rect);
      }
   }
   else d_rects.Delete (   );
   return (DONE);
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: FaceDetection_VeriLook
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_VERILOOK)

 FaceDetection_VeriLook::FaceDetection_VeriLook (   )
 
{
   FaceDetector          = NULL;
   MinIod                = 18;
   MaxRollAngleDeviation = 15;
   MaxYawAngleDeviation  = 30;
}

 void FaceDetection_VeriLook::Close (   )
 
{
   if (FaceDetector != NULL) {
      N_CLASS(NLicense)::ReleaseComponents (_T("Biometrics.FaceExtraction"));
      delete FaceDetector;
      FaceDetector = NULL;
   }
   FaceDetection::Close (   );
}

 int FaceDetection_VeriLook::Initialize (const char* dir_name)

{
   const N_CLASS(NStringWrapper) address = _T("/local");
   const N_CLASS(NStringWrapper) port    = _T("5000");

   Close (   );
   if (!N_CLASS(NLicense)::ObtainComponents (address,port,_T("Biometrics.FaceExtraction"))) return (1);
   FaceDetector = new N_CLASS(NLExtractor);
   FaceDetector->SetMaxIod                  (500);
   FaceDetector->SetDetectAllFeaturePoints  (FALSE);
   FaceDetector->SetDetectBaseFeaturePoints (FALSE);
   FaceDetector->SetDetectGender            (FALSE);
   FaceDetector->SetFavorLargestFace        (FALSE);
   Flag_Initialized = TRUE;
   return (DONE);
}

 int FaceDetection_VeriLook::Perform (GImage& s_image,IB2DArray1D& d_rects)

{
   int i;
   
   d_rects.Delete (   );
   if (!Flag_Initialized) return (1);
   std::auto_ptr<N_CLASS(NImage)> s_img(N_CLASS(NImage)::FromData(NPF_GRAYSCALE_8U,s_image.Width,s_image.Height,s_image.GetLineLength(),s_image.Width,(byte*)s_image,s_image.GetLineLength() * s_image.Height));
   HNImage h_s_img = s_img->GetHandle (   );
   if (h_s_img == NULL) return (2);
   FaceDetector->SetMinIod                (MinIod);
   FaceDetector->SetMaxRollAngleDeviation (MaxRollAngleDeviation);
   FaceDetector->SetMaxYawAngleDeviation  (MaxYawAngleDeviation);
   NleFace* faces;
   int n_faces = FaceDetector->DetectFaces (&dynamic_cast<N_CLASS(NGrayscaleImage)&>(*s_img),&faces);
   if (n_faces) {
      d_rects.Create (n_faces);
      for (i = 0; i < n_faces; i++) {
         d_rects[i].X      = faces[i].Rectangle.X;
         d_rects[i].Y      = faces[i].Rectangle.Y;
         d_rects[i].Width  = faces[i].Rectangle.Width;
         d_rects[i].Height = faces[i].Rectangle.Height;
      }
   }
   if (faces != NULL) NFree (faces);
   return (DONE);
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: FaceLandmarkDetection
//
///////////////////////////////////////////////////////////////////////////////

 FaceLandmarkDetection::FaceLandmarkDetection (   )

{
   Flag_Initialized = FALSE;
}

 FaceLandmarkDetection::~FaceLandmarkDetection (   )

{
   Close (   );
}

 void FaceLandmarkDetection::Close (   )

{
   Flag_Initialized = FALSE;
}

 int FaceLandmarkDetection::Perform (GImage& s_image,ObjectTracking& obj_tracker)

{
   int r_code = DONE;
   if (!Flag_Initialized) return (1);
   TrackedObject* t_object = obj_tracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED)) {
         if (DetectLandmarks (s_image,t_object->Face) != DONE) r_code = 2;
      }
      t_object = obj_tracker.TrackedObjects.Next (t_object);
   }
   return (r_code);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: FaceLandmarkDetection_DLib
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_DLIB)

 FaceLandmarkDetection_DLib::FaceLandmarkDetection_DLib (   )

{
   FaceLandmarkDetector = NULL;
}

 void FaceLandmarkDetection_DLib::Close (   )

{
   if (FaceLandmarkDetector != NULL) {
      delete FaceLandmarkDetector;
      FaceLandmarkDetector = NULL;
   }
   FaceLandmarkDetection::Close (   );
}

 int FaceLandmarkDetection_DLib::DetectLandmarks (GImage& s_image,FaceInfo& face_info)

{
   int i;
   
   if (FaceLandmarkDetector == NULL) return (1);
   if (!face_info.IsBoundingBoxValid (   )) return (DONE);
   dlib::array2d<byte> s_img((byte*)s_image,s_image.Width,s_image.Height);
   dlib::rectangle s_rect((long)face_info.X,(long)face_info.Y,(long)(face_info.X + face_info.Width - 1),(long)(face_info.Y + face_info.Height - 1));
   dlib::full_object_detection shape = (*FaceLandmarkDetector)(s_img,s_rect);
   s_img.import (0,0,0);
   int n_points = shape.num_parts (   );
   if (!n_points) return (2);
   face_info.Points.Create (n_points);
   FP2DArray1D& points = face_info.Points;
   for (i = 0; i < n_points; i++)
      points[i]((float)(shape.part(i).x() - face_info.X),(float)(shape.part(i).y() - face_info.Y));
   face_info.Eyes[0]  = 0.5f * (points[36] + points[39]);
   face_info.Eyes[1]  = 0.5f * (points[42] + points[45]);
   face_info.Nose     = points[30];
   face_info.Mouth[0] = points[48];
   face_info.Mouth[1] = points[54];
   EstimateFacePose (face_info);
   return (DONE);
}

 int FaceLandmarkDetection_DLib::Initialize (const char* dir_name)

{
   const char file_name[] = "IVXFLDDLB1.dat";
   
   Close (   );
   StringA path_name;
   if (dir_name == NULL) path_name = file_name;
   else {
      #if defined(__OS_WINDOWS)
      path_name.Format ("%s\\%s",dir_name,file_name);
      #else
      path_name.Format ("%s/%s",dir_name,file_name);
      #endif
   }
   if (AccessFile (path_name) != DONE) return (1);
   FaceLandmarkDetector = new dlib::shape_predictor;
   dlib::deserialize((char*)path_name) >> *FaceLandmarkDetector;
   Flag_Initialized = TRUE;
   return (DONE);
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: FaceLandmarkDetection_Seeta
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_SEETA)

 FaceLandmarkDetection_Seeta::FaceLandmarkDetection_Seeta (   )

{
   FaceLandmarkDetector = NULL;
}

 void FaceLandmarkDetection_Seeta::Close (   )

{
   if (FaceLandmarkDetector != NULL) {
       delete FaceLandmarkDetector;
       FaceLandmarkDetector = NULL;
   }
   FaceLandmarkDetection::Close (   );
}

 int FaceLandmarkDetection_Seeta::DetectLandmarks (GImage& s_image,FaceInfo& face_info)

{
   if (FaceLandmarkDetector == NULL) return (1);
   if (!face_info.IsBoundingBoxValid (   )) return (DONE);
   seeta::ImageData s_img_data(s_image.Width,s_image.Height);
   s_img_data.data         = (byte*)s_image;
   s_img_data.num_channels = 1;
   seeta::FaceInfo fi;
   fi.bbox.x      = face_info.X;
   fi.bbox.y      = face_info.Y;
   fi.bbox.width  = face_info.Width;
   fi.bbox.height = face_info.Height;
   seeta::FacialLandmark s_points[5];
   FaceLandmarkDetector->PointDetectLandmarks (s_img_data,fi,s_points);
   face_info.Points.Create (5);
   FP2DArray1D& d_points = face_info.Points;
   d_points[0]((float)(s_points[0].x - face_info.X),(float)(s_points[0].y - face_info.Y));
   d_points[1]((float)(s_points[1].x - face_info.X),(float)(s_points[1].y - face_info.Y));
   d_points[2]((float)(s_points[2].x - face_info.X),(float)(s_points[2].y - face_info.Y));
   d_points[3]((float)(s_points[3].x - face_info.X),(float)(s_points[3].y - face_info.Y));
   d_points[4]((float)(s_points[4].x - face_info.X),(float)(s_points[4].y - face_info.Y));
   face_info.Eyes[0]  = d_points[0];
   face_info.Eyes[1]  = d_points[1];
   face_info.Nose     = d_points[2];
   face_info.Mouth[0] = d_points[3];
   face_info.Mouth[1] = d_points[4];
   EstimateFacePose (face_info);
   return (DONE);
}

 int FaceLandmarkDetection_Seeta::Initialize (const char* dir_name)

{
   const char file_name[] = "IVXFLDSFE1.dat";
   
   Close (   );
   StringA path_name;
   if (dir_name == NULL) path_name = file_name;
   else {
      #if defined(__OS_WINDOWS)
      path_name.Format ("%s\\%s",dir_name,file_name);
      #else
      path_name.Format ("%s/%s",dir_name,file_name);
      #endif
   }
   if (AccessFile (path_name) != DONE) return (1);
   FaceLandmarkDetector = new seeta::FaceAlignment(path_name);
   Flag_Initialized = TRUE;
   return (DONE);
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: FacePartsDetection
//
///////////////////////////////////////////////////////////////////////////////

 FacePartsDetection::FacePartsDetection (   )

{
   Flag_Initialized   = FALSE;
   
   MaxTime_Undetected = 0.33f;
}

 FacePartsDetection::~FacePartsDetection (   )

{
   Close (   );
}

 void FacePartsDetection::Close (   )

{
   Flag_Initialized = FALSE;
}

 int FacePartsDetection::Perform (GImage& s_image,ObjectTracking& obj_tracker)

{
   #if defined(__DEBUG_FPD)
   _DVX_FPD = _DVY_FPD = 0;
   _DebugView_FPD->Clear (   );
   #endif
   int r_code = DONE;
   if (!Flag_Initialized) return (1);
   TrackedObject* t_object = obj_tracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED)) {
         if (DetectParts (s_image,obj_tracker.FrameRate,t_object->Face) != DONE) r_code = 2;
      }
      t_object = obj_tracker.TrackedObjects.Next (t_object);
   }
   #if defined(__DEBUG_FPD)
   _DebugView_FPD->Invalidate (   );
   #endif
   return (r_code);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: FacePartsDetection_OpenCV
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_OPENCV)

 int FacePartsDetection_OpenCV::DetectParts (GImage& s_image,float frm_rate,FaceInfo& fi)

{
   int r_code = 0;
   if (!fi.IsBoundingBoxValid (   )) return (1);
   if (!fi.AreLandmarksValid  (   )) return (2);
   float frm_duration = 0.0f;
   if (frm_rate) frm_duration = 1.0f / frm_rate;
   IBox2D fc_rgn(fi.X,fi.Y,fi.Width,fi.Height);
   ISize2D c_size(s_image.Width,s_image.Height);
   CropRegion (c_size,fc_rgn);
   // 얼굴 이미지를 얻는다.
   GImage fc_image = s_image.Extract (fc_rgn);
   #if defined(__DEBUG_FPD)
   _DebugView_FPD->DrawImage (fc_image,_DVX_FPD,_DVY_FPD);
   _DebugView_FPD->DrawPoint ((int)(fi.Eyes[0].X  + _DVX_FPD),(int)(fi.Eyes[0].Y  + _DVY_FPD),PC_GREEN);
   _DebugView_FPD->DrawPoint ((int)(fi.Eyes[1].X  + _DVX_FPD),(int)(fi.Eyes[1].Y  + _DVY_FPD),PC_GREEN);
   _DebugView_FPD->DrawPoint ((int)(fi.Mouth[0].X + _DVX_FPD),(int)(fi.Mouth[0].Y + _DVY_FPD),PC_RED);
   _DebugView_FPD->DrawPoint ((int)(fi.Mouth[1].X + _DVX_FPD),(int)(fi.Mouth[1].Y + _DVY_FPD),PC_RED);
   _DVX_FPD += fc_image.Width + 10;
   #endif
   // 두 눈의 위치가 수평이 되도록 얼굴 이미지를 보정한다.
   FPoint2D ev = fi.Eyes[1] - fi.Eyes[0];
   float angle = -MC_RAD2DEG * atan2 (ev.Y,ev.X);
   Matrix mat_H(3,3);
   GetHomographyForRotation (0.5f * fc_image.Width,0.5f * fc_image.Height,angle,mat_H);
   Matrix mat_IH = Inv(mat_H);
   GImage nf_image(fc_image.Width,fc_image.Height);
   nf_image.Clear (   );
   Warp (fc_image,mat_H,nf_image,FALSE);
   // 보정된 얼굴 이미지 상에서의 눈과 입의 위치를 구한다.
   FPoint2D eyes[2],mouth[2];
   GetTransformedPosition (fi.Eyes[0] ,mat_H,eyes[0] );
   GetTransformedPosition (fi.Eyes[1] ,mat_H,eyes[1] );
   GetTransformedPosition (fi.Mouth[0],mat_H,mouth[0]);
   GetTransformedPosition (fi.Mouth[1],mat_H,mouth[1]);
   #if defined(__DEBUG_FPD)
   _DebugView_FPD->DrawImage (nf_image,_DVX_FPD,_DVY_FPD);
   _DebugView_FPD->DrawPoint ((int)(eyes[0].X  + _DVX_FPD),(int)(eyes[0].Y  + _DVY_FPD),PC_GREEN);
   _DebugView_FPD->DrawPoint ((int)(eyes[1].X  + _DVX_FPD),(int)(eyes[1].Y  + _DVY_FPD),PC_GREEN);
   _DebugView_FPD->DrawPoint ((int)(mouth[0].X + _DVX_FPD),(int)(mouth[0].Y + _DVY_FPD),PC_RED);
   _DebugView_FPD->DrawPoint ((int)(mouth[1].X + _DVX_FPD),(int)(mouth[1].Y + _DVY_FPD),PC_RED);
   _DVX_FPD += nf_image.Width + 10;
   #endif
   #if defined(__DEBUG_FPD)
   _DebugView_FPD->DrawImage (nf_image,_DVX_FPD,_DVY_FPD);
   #endif
   float epl = Mag(eyes[1] - eyes[0]);
   int   epw = (int)(2.5f * epl); // PARAMETERS
   int   eph = (int)(0.8f * epl); // PARAMETERS
   cv::Size min_size;
   c_size(nf_image.Width,nf_image.Height);
   // 왼쪽 눈을 검출한다.
   int lew = epw / 2;
   int leh = eph;
   IBox2D le_rgn;
   le_rgn.X      = (int)(eyes[0].X - 0.5f * lew);
   le_rgn.Y      = (int)(eyes[0].Y - 0.5f * leh);
   le_rgn.Width  = lew;
   le_rgn.Height = leh;
   CropRegion (c_size,le_rgn);
   le_rgn.Width  = le_rgn.Width  / 4 * 4;
   #if defined(__DEBUG_FPD)
   _DebugView_FPD->DrawRectangle (le_rgn.X + _DVX_FPD,le_rgn.Y + _DVY_FPD,le_rgn.Width,le_rgn.Height,PC_YELLOW,2);
   #endif
   GImage le_image = nf_image.Extract (le_rgn);
   Mat m_le_image;
   OpenCV_InitMat (le_image,m_le_image);
   vector<cv::Rect> le_rects;
   min_size.width  = (int)(0.4f * le_image.Width);
   min_size.height = (int)(0.4f * le_image.Height);
   LeftEyeDetector.detectMultiScale (m_le_image,le_rects,1.1f,3,0,min_size);
   if (le_rects.size (   )) {
      fi.Time_LeftEyeUndetected = 0.0f;
      cv::Rect& rect = le_rects[0];
      int hw = rect.width  / 2;
      int hh = rect.height / 2;
      FPoint2D cp((float)(le_rgn.X + rect.x + hw),(float)(le_rgn.Y + rect.y + hh));
      GetTransformedPosition (cp,mat_IH,cp);
      fi.LeftEyeRegion((int)(cp.X - hw),(int)(cp.Y - hh),rect.width,rect.height);
      CropRegion (c_size,fi.LeftEyeRegion);
      #if defined(__DEBUG_FPD)
      _DebugView_FPD->DrawRectangle (rect.x + le_rgn.X + _DVX_FPD,rect.y + le_rgn.Y + _DVY_FPD,rect.width,rect.height,PC_RED,2);
      #endif
   }
   else {
      fi.Time_LeftEyeUndetected += frm_duration;
      if (!frm_rate || fi.Time_LeftEyeUndetected > MaxTime_Undetected) {
         fi.LeftEyeRegion.Clear (   );
         r_code--;
      }
      else {
         if (fi.LeftEyeRegion.Width) {
            fi.LeftEyeRegion.X = (int)(fi.Eyes[0].X - 0.5f * fi.LeftEyeRegion.Width);
            fi.LeftEyeRegion.Y = (int)(fi.Eyes[0].Y - 0.5f * fi.LeftEyeRegion.Height);
            CropRegion (c_size,fi.LeftEyeRegion);
         }
      }
   }
   // 오른쪽 눈을 검출한다.
   int rew = epw / 2;
   int reh = eph;
   IBox2D re_rgn;
   re_rgn.X      = (int)(eyes[1].X - 0.5f * rew);
   re_rgn.Y      = (int)(eyes[1].Y - 0.5f * reh);
   re_rgn.Width  = rew;
   re_rgn.Height = reh;
   CropRegion (c_size,re_rgn);
   re_rgn.Width  = re_rgn.Width / 4 * 4;
   #if defined(__DEBUG_FPD)
   _DebugView_FPD->DrawRectangle (re_rgn.X + _DVX_FPD,re_rgn.Y + _DVY_FPD,re_rgn.Width,re_rgn.Height,PC_YELLOW,2);
   #endif
   GImage re_image = nf_image.Extract (re_rgn);
   Mat m_re_image;
   OpenCV_InitMat (re_image,m_re_image);
   vector<cv::Rect> re_rects;
   min_size.width  = (int)(0.4f * re_image.Width);
   min_size.height = (int)(0.4f * re_image.Height);
   RightEyeDetector.detectMultiScale (m_re_image,re_rects,1.1f,3,0,min_size);
   if (re_rects.size (   )) {
      cv::Rect& rect = re_rects[0];
      int hw = rect.width  / 2;
      int hh = rect.height / 2;
      FPoint2D cp((float)(re_rgn.X + rect.x + hw),(float)(re_rgn.Y + rect.y + hh));
      GetTransformedPosition (cp,mat_IH,cp);
      fi.RightEyeRegion((int)(cp.X - hw),(int)(cp.Y - hh),rect.width,rect.height);
      CropRegion (c_size,fi.RightEyeRegion);
      #if defined(__DEBUG_FPD)
      _DebugView_FPD->DrawRectangle (rect.x + re_rgn.X + _DVX_FPD,rect.y + re_rgn.Y + _DVY_FPD,rect.width,rect.height,PC_RED,2);
      #endif
   }
   else {
      fi.Time_RightEyeUndetected += frm_duration;
      if (!frm_rate || fi.Time_RightEyeUndetected > MaxTime_Undetected) {
         fi.RightEyeRegion.Clear (   );
         r_code--;
      }
      else {
         if (fi.RightEyeRegion.Width) {
            fi.RightEyeRegion.X = (int)(fi.Eyes[1].X - 0.5f * fi.RightEyeRegion.Width);
            fi.RightEyeRegion.Y = (int)(fi.Eyes[1].Y - 0.5f * fi.RightEyeRegion.Height);
            CropRegion (c_size,fi.RightEyeRegion);
         }
      }
   }
   // 입을 검출한다.
   float    mol = Mag(mouth[1] - mouth[0]);
   int      mow = (int)(2.2f * mol); // PARAMETERS
   int      moh = (int)(1.3f * mol); // PARAMETERS
   FPoint2D moc = 0.5f * (mouth[0] + mouth[1]);
   IBox2D mo_rgn;
   mo_rgn.X      = (int)(moc.X - 0.5f * mow);
   mo_rgn.Y      = (int)(moc.Y - 0.4f * moh);
   mo_rgn.Width  = mow;
   mo_rgn.Height = moh;
   CropRegion (c_size,mo_rgn);
   mo_rgn.Width  = mo_rgn.Width / 4 * 4;
   #if defined(__DEBUG_FPD)
   _DebugView_FPD->DrawRectangle (mo_rgn.X + _DVX_FPD,mo_rgn.Y + _DVY_FPD,mo_rgn.Width,mo_rgn.Height,PC_YELLOW,2);
   #endif
   GImage mo_image = nf_image.Extract (mo_rgn);
   Mat m_mo_image;
   OpenCV_InitMat (mo_image,m_mo_image);
   vector<cv::Rect> mo_rects;
   min_size.width  = (int)(0.4f * mo_image.Width);
   min_size.height = (int)(0.4f * mo_image.Height);
   MouthDetector.detectMultiScale (m_mo_image,mo_rects,1.1f,3,0,min_size);
   if (mo_rects.size (   )) {
      cv::Rect& rect = mo_rects[0];
      int hw = rect.width  / 2;
      int hh = rect.height / 2;
      FPoint2D cp((float)(mo_rgn.X + rect.x + hw),(float)(mo_rgn.Y + rect.y + hh));
      GetTransformedPosition (cp,mat_IH,cp);
      fi.MouthRegion((int)(cp.X - hw),(int)(cp.Y - hh),rect.width,rect.height);
      CropRegion (c_size,fi.MouthRegion);
      #if defined(__DEBUG_FPD)
      _DebugView_FPD->DrawRectangle (rect.x + mo_rgn.X + _DVX_FPD,rect.y + mo_rgn.Y + _DVY_FPD,rect.width,rect.height,PC_RED,2);
      #endif
   }
   else {
      fi.Time_MouthUndetected += frm_duration;
      if (!frm_rate || fi.Time_MouthUndetected > MaxTime_Undetected) {
         fi.MouthRegion.Clear (   );
         r_code--;
      }
      else {
         if (fi.MouthRegion.Width) {
            FPoint2D cp = 0.5f * (fi.Mouth[0] + fi.Mouth[1]);
            fi.MouthRegion.X = (int)(cp.X - 0.5f * fi.MouthRegion.Width);
            fi.MouthRegion.Y = (int)(cp.Y - 0.5f * fi.MouthRegion.Height);
            CropRegion (c_size,fi.MouthRegion);
         }
      }
   }
   #if defined(__DEBUG_FPD)
   _DVX_FPD  = 0;
   _DVY_FPD += fc_image.Height + 10;
   #endif
   return (r_code);
}

 int FacePartsDetection_OpenCV::Initialize (const char* dir_name)

{
   const char led_file_name[] = "IVXLEDOCV1.dat";
   const char red_file_name[] = "IVXREDOCV1.dat";
   const char mod_file_name[] = "IVXMODOCV1.dat";

   Close (   );
   StringA path_name;
   string  path_name2;
   // Left-Eye Detector
   if (dir_name == NULL) path_name = led_file_name;
   else {
      #if defined(__OS_WINDOWS)
      path_name.Format ("%s\\%s",dir_name,led_file_name);
      #else
      path_name.Format ("%s/%s",dir_name,led_file_name);
      #endif
   }
   path_name2 = (char*)path_name;
   if (!LeftEyeDetector.load (path_name2)) return (2);
   // Right-Eye Detector
   if (dir_name == NULL) path_name = red_file_name;
   else {
      #if defined(__OS_WINDOWS)
      path_name.Format ("%s\\%s",dir_name,red_file_name);
      #else
      path_name.Format ("%s/%s",dir_name,red_file_name);
      #endif
   }
   path_name2 = (char*)path_name;
   if (!RightEyeDetector.load (path_name2)) return (3);
   // Mouth Detector
   if (dir_name == NULL) path_name = mod_file_name;
   else {
      #if defined(__OS_WINDOWS)
      path_name.Format ("%s\\%s",dir_name,mod_file_name);
      #else
      path_name.Format ("%s/%s",dir_name,mod_file_name);
      #endif
   }
   path_name2 = (char*)path_name;
   if (!MouthDetector.load (path_name2)) return (4);
   Flag_Initialized = TRUE;
   return (DONE);
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: FacePoseEstimation
//
///////////////////////////////////////////////////////////////////////////////

 FacePoseEstimation::FacePoseEstimation (   )

{
   Flag_Initialized = FALSE;
}
 
 FacePoseEstimation::~FacePoseEstimation (   )

{
   Close (   );
}

 void FacePoseEstimation::Close (   )

{
   Flag_Initialized = FALSE;
}

 int FacePoseEstimation::Perform (BGRImage& s_cimage,ObjectTracking& obj_tracker)

{
   if (!Flag_Initialized) return (1);
   int r_code = DONE;
   TrackedObject* t_object = obj_tracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED)) {
         if (EstimateFacePose (s_cimage,t_object->Face) != DONE) r_code = 2;
      }
      t_object = obj_tracker.TrackedObjects.Next (t_object);
   }
   return (r_code);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: FacePoseEstimation_OpenVINO
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_OPENVINO)

 FacePoseEstimation_OpenVINO::FacePoseEstimation_OpenVINO (   )

{
   Flag_UseGPU = FALSE;
}

 void FacePoseEstimation_OpenVINO::Close (   )

{
   FacePoseEstimator.Close (   );
   FacePoseEstimation::Close (   );
}

 int FacePoseEstimation_OpenVINO::EstimateFacePose (BGRImage& s_cimage,FaceInfo& face_info)

{
   if (!face_info.IsBoundingBoxValid (   )) return (1);
   ISize2D c_size(s_cimage.Width,s_cimage.Height);
   CropRegion (c_size,face_info);
   BGRImage t_cimage = s_cimage.Extract (face_info);
   return (FacePoseEstimator.Predict (t_cimage,face_info.Yaw,face_info.Pitch,face_info.Roll));
}

 int FacePoseEstimation_OpenVINO::Initialize (const char* dir_name)

{
   const char cfg_file_name[] = "IVXFPECOV1.dat"; // head-pose-estimation-adas-0001.xml
   const char wgt_file_name[] = "IVXFPEWOV1.dat"; // head-pose-estimation-adas-0001.bin

   Close (   );
   StringA cfg_path_name,wgt_path_name;
   if (dir_name == NULL) {
      cfg_path_name = cfg_file_name;
      wgt_path_name = wgt_file_name;
   }
   else {
      #if defined(__OS_WINDOWS)
      cfg_path_name.Format ("%s\\%s",dir_name,(char*)cfg_file_name);
      wgt_path_name.Format ("%s\\%s",dir_name,(char*)wgt_file_name);
      #else
      cfg_path_name.Format ("%s/%s",dir_name,(char*)cfg_file_name);
      wgt_path_name.Format ("%s/%s",dir_name,(char*)wgt_file_name);
      #endif
   }
   int e_code = FacePoseEstimator.Initialize (cfg_path_name,wgt_path_name,Flag_UseGPU);
   if (e_code) return (e_code);
   Flag_Initialized = TRUE;
   return (DONE);
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: FaceExtraction
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_SEETA) && defined(__LIB_OPENCV)

 FaceExtraction::FaceExtraction (   )

{
   MaxTime_Occluded   = 0.5f;
   MaxTime_Undetected = 0.5f;
}

 FaceExtraction::~FaceExtraction (   )

{
   Close (   );
}

 void FaceExtraction::Close (   )

{
   FaceDetector.Close (   );
   FaceLandmarkDetector.Close (   );
   FacePartsDetector.Close (   );
}

 int FaceExtraction::Initialize (const char* dir_name)

{
   if (FaceDetector.Initialize (dir_name)) return (1);
   if (FaceLandmarkDetector.Initialize (dir_name)) return (2);
   if (FacePartsDetector.Initialize (dir_name)) return (3);
   return (DONE);
}

 int FaceExtraction::IsInitialized (   )

{
   if (FaceDetector.IsInitialized         (   ) &&
       FaceLandmarkDetector.IsInitialized (   ) &&
       FacePartsDetector.IsInitialized    (   )) return (TRUE);
   else return (FALSE);
}

 int FaceExtraction::Perform (GImage& s_image,FIArray1D& d_array,int flag_fpd)

{
   int i;
   
   d_array.Delete (   );
   GImage* p_s_image = &s_image;
   GImage  t_image;
   // s_image의 너비가 4의 배수가 아니면 4의 배수가 되도록 크롭핑한다.
   if (s_image.Width % 4) {
      t_image   = s_image.Extract (0,0,s_image.Width / 4 * 4,s_image.Height);
      p_s_image = &t_image;
   }
   IB2DArray1D fc_rgns;
   if (FaceDetector.Perform (*p_s_image,fc_rgns)) return (1);
   if (!fc_rgns) return (DONE);
   d_array.Create (fc_rgns.Length);
   for (i = 0; i < fc_rgns.Length; i++) {
      d_array[i](fc_rgns[i]);
      if (FaceLandmarkDetector.DetectLandmarks (s_image,d_array[i])) return (2);
      if (flag_fpd && FacePartsDetector.DetectParts (s_image,0.0f,d_array[i])) return (3);
   }
   return (DONE);
}

 int FaceExtraction::Perform (GImage& s_image,ObjectTracking& obj_tracker,int flag_fod) 

{
   if (!IsInitialized (   )) return (1);
   #if defined(__DEBUG_FPD)
   _DVX_FPD = _DVY_FPD = 0;
   _DebugView_FPD->Clear (   );
   #endif
   ISize2D c_size(s_image.Width,s_image.Height);
   TrackedObject* t_object = obj_tracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED) && (t_object->Type == TO_TYPE_HUMAN)) {
         IBox2D s_rgn(*t_object);
         if (s_rgn.Height > s_rgn.Width) s_rgn.Height = s_rgn.Width;
         CropRegion (c_size,s_rgn);
         s_rgn.Width  = s_rgn.Width  / 4 * 4;
         s_rgn.Height = s_rgn.Height / 4 * 4;
         FaceInfo& fi = t_object->Face;
         int flag_face_detected = FALSE;
         if (s_rgn.Width > FaceDetector.MinFaceSize && s_rgn.Height > FaceDetector.MinFaceSize) {
            GImage t_image = s_image.Extract (s_rgn);
            IB2DArray1D fc_rgns;
            FaceDetector.Perform (t_image,fc_rgns);
            if (fc_rgns.Length) {
               flag_face_detected = TRUE;
               fi.Time_FaceUndetected = 0.0f;
               fi(fc_rgns[0]);
               FaceLandmarkDetector.DetectLandmarks (t_image,fi);
               if (flag_fod) {
                  if (FacePartsDetector.DetectParts (t_image,obj_tracker.FrameRate,fi)) fi.Time_FaceOccluded += obj_tracker.FrameDuration;
                  else {
                     fi.SetFaceOccluded (FALSE);
                     fi.Time_FaceOccluded = 0.0f;
                  }
               }
               fi.X += t_object->X;
               fi.Y += t_object->Y;
            }
         }
         if (!flag_face_detected) {
            fi.Time_FaceUndetected += obj_tracker.FrameDuration;
            if (fi.IsBoundingBoxValid (   ) && (fi.Time_FaceUndetected > MaxTime_Undetected)) fi.Clear (   );
            if (flag_fod) fi.Time_FaceOccluded += obj_tracker.FrameDuration;
         }
         if (flag_fod && (fi.Time_FaceOccluded > MaxTime_Occluded)) fi.SetFaceOccluded (TRUE);
      }
      t_object = obj_tracker.TrackedObjects.Next (t_object);
   }
   #if defined(__DEBUG_FPD)
   _DebugView_FPD->Invalidate (   );
   #endif
   return (DONE);
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: FrontalFaceVerification
//
///////////////////////////////////////////////////////////////////////////////

 FrontalFaceVerification::FrontalFaceVerification (   )

{
   MaxPitch1 = 25.0f;
   MaxYaw1   = 35.0f;
   MaxRoll1  = 25.0f;

   MaxPitch2 = 10.0f;
   MaxYaw2   = 15.0f;
   MaxRoll2  = 10.0f;
}

 void FrontalFaceVerification::Perform (ObjectTracking& obj_tracker)

{
   TrackedObject* t_object = obj_tracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      t_object->Status &= ~(TO_STATUS_FRONTAL_FACE1 | TO_STATUS_FRONTAL_FACE2);
      if (t_object->Face.IsPoseValid (   )) {
         FaceInfo& fi = t_object->Face;
         float pitch = fabs (fi.Pitch);
         float yaw   = fabs (fi.Yaw);
         float roll  = fabs (fi.Roll);
         if (pitch <= MaxPitch1 && yaw <= MaxYaw1 && roll <= MaxRoll1) {
            t_object->Status |= TO_STATUS_FRONTAL_FACE1;
            t_object->Time_FrontalFace1 += obj_tracker.FrameDuration;
         }
         if (pitch <= MaxPitch2 && yaw <= MaxYaw2 && roll <= MaxRoll2) {
            t_object->Status |= TO_STATUS_FRONTAL_FACE2;
            t_object->Time_FrontalFace2 += obj_tracker.FrameDuration;
         }
      }
      t_object = obj_tracker.TrackedObjects.Next (t_object);
   }
}

 int FrontalFaceVerification::ReadFile (CXMLIO* pIO)

{
   static FrontalFaceVerification dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("FrontalFaceVerification")) {
      xmlNode.Attribute (TYPE_ID(float),"MaxPitch1",&MaxPitch1,&dv.MaxPitch1);
      xmlNode.Attribute (TYPE_ID(float),"MaxYaw1"  ,&MaxYaw1  ,&dv.MaxYaw1);
      xmlNode.Attribute (TYPE_ID(float),"MaxRoll1" ,&MaxRoll1 ,&dv.MaxRoll1);
      xmlNode.Attribute (TYPE_ID(float),"MaxPitch2",&MaxPitch2,&dv.MaxPitch2);
      xmlNode.Attribute (TYPE_ID(float),"MaxYaw2"  ,&MaxYaw2  ,&dv.MaxYaw2);
      xmlNode.Attribute (TYPE_ID(float),"MaxRoll2" ,&MaxRoll2 ,&dv.MaxRoll2);
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}

 int FrontalFaceVerification::WriteFile (CXMLIO* pIO)

{
   static FrontalFaceVerification dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("FrontalFaceVerification")) {
      xmlNode.Attribute (TYPE_ID(float),"MaxPitch1",&MaxPitch1,&dv.MaxPitch1);
      xmlNode.Attribute (TYPE_ID(float),"MaxYaw1"  ,&MaxYaw1  ,&dv.MaxYaw1);
      xmlNode.Attribute (TYPE_ID(float),"MaxRoll1" ,&MaxRoll1 ,&dv.MaxRoll1);
      xmlNode.Attribute (TYPE_ID(float),"MaxPitch2",&MaxPitch2,&dv.MaxPitch2);
      xmlNode.Attribute (TYPE_ID(float),"MaxYaw2"  ,&MaxYaw2  ,&dv.MaxYaw2);
      xmlNode.Attribute (TYPE_ID(float),"MaxRoll2" ,&MaxRoll2 ,&dv.MaxRoll2);
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 int EstimateFacePose (FaceInfo& face_info)
#if defined(__LIB_OPENCV)
{
   if (!face_info.AreLandmarksValid (   )) return (1);
   // 3D model points
   FP3DArray1D mp_array(5);
   mp_array[0](   0.0f,   0.0f,150.0f); // 코 끝
   mp_array[1](-160.0f, 170.0f,0.0f  ); // 왼쪽 눈 중앙
   mp_array[2]( 160.0f, 170.0f,0.0f  ); // 오른쪽 눈 중앙
   mp_array[3](-150.0f,-150.0f,0.0f  ); // 입가 좌측 끝
   mp_array[4]( 150.0f,-150.0f,0.0f  ); // 입가 우측 끝
   cv::Mat m_mp_array;
   OpenCV_InitMat (mp_array,m_mp_array);
   // Image points
   FP2DArray1D ip_array(5);
   ip_array[0] = face_info.Nose;
   ip_array[1] = face_info.Eyes[0];
   ip_array[2] = face_info.Eyes[1];
   ip_array[3] = face_info.Mouth[0];
   ip_array[4] = face_info.Mouth[1];
   cv::Mat m_ip_array;
   OpenCV_InitMat (ip_array,m_ip_array);
   // Calibration matrix
   double fl = 1000.0; // 값이 적당히 큰게 안정적임
   double cx = 0.5 * face_info.Width;
   double cy = 0.5 * face_info.Height;
   Matrix mat_K = IM(3);
   mat_K[0][0] = fl;
   mat_K[1][1] = fl;
   mat_K[0][2] = cx;
   mat_K[1][2] = cy;
   cv::Mat m_mat_K;
   OpenCV_InitMat (mat_K,m_mat_K);
   // Lens distortion coefficients
   cv::Mat m_vec_d = (cv::Mat_<double>(4,1) << 0.0,0.0,0.0,0.0);
   // 카메라의 Rotation과 Translation을 구한다.
   cv::Mat m_vec_r,m_vec_t;
   cv::solvePnP (m_mp_array,m_ip_array,m_mat_K,m_vec_d,m_vec_r,m_vec_t);
   // 화면 상에 얼굴의 Normal Vector를 표시하기 위한 Pose 좌표 값을 구한다.
   vector<Point3d> m_mp_array2;
   vector<Point2d> m_ip_array2;
   Point3d mp(0.0,0.0,500.0);
   m_mp_array2.push_back(mp);
   #if NDEBUG // jun : debug 모드에서 다운
   cv::projectPoints (m_mp_array2,m_vec_r,m_vec_t,m_mat_K,m_vec_d,m_ip_array2);
   face_info.Pose.X = (float)m_ip_array2[0].x;
   face_info.Pose.Y = (float)m_ip_array2[0].y;
   // 카메라의 Rotation 값으로부터 얼굴의 Pitch, Yaw, Roll을 구한다.
   cv::Mat m_mat_R1;
   cv::Rodrigues (m_vec_r,m_mat_R1);
   cv::Mat m_mat_R2 = (cv::Mat_<double>(3,3) << 1.0,0.0,0.0,0.0,-1.0,0.0,0.0,0.0,-1.0);
   cv::Mat m_mat_R3 = m_mat_R2 * m_mat_R1;
   double x,y,z;
   double sy = sqrt(m_mat_R3.at<double>(0,0) * m_mat_R3.at<double>(0,0) + m_mat_R3.at<double>(1,0) * m_mat_R3.at<double>(1,0));
   if (sy >= 1E-6) {
      x = atan ( m_mat_R3.at<double>(2,1) / m_mat_R3.at<double>(2,2));
      y = atan (-m_mat_R3.at<double>(2,0) / sy);
      z = atan ( m_mat_R3.at<double>(1,0) / m_mat_R3.at<double>(0,0));
   }
   else {
      x = atan (-m_mat_R3.at<double>(1,2) / m_mat_R3.at<double>(1,1));
      y = atan (-m_mat_R3.at<double>(2,0) / sy);
      z = 0.0;
   }
   face_info.Pitch = (float)(MC_RAD2DEG * x);
   face_info.Yaw   = (float)(MC_RAD2DEG * y);
   face_info.Roll  = (float)(MC_RAD2DEG * z);
   #endif
   return (DONE);
}
#else
{
   return (-1);
}
#endif

}

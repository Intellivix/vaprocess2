#include "vacl_conrgn.h"
#include "vacl_fmod.h"
#include "vacl_foregnd.h"

#if defined(__DEBUG_FMD)
#include "Win32CL/Win32CL.h"
extern int _DVX_FMD,_DVY_FMD;
extern CImageView* _DebugView_FMD;
#endif

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: FastMovingObjectDetection
//
///////////////////////////////////////////////////////////////////////////////

 FastMovingObjectDetection::FastMovingObjectDetection (   )

{
   GroupingRange = 25;

   VFQLength     = 2;
   MinAreaRatio  = 0.25f;
   MaxOverlap    = 0.25f;

   _Init (   );
}

 FastMovingObjectDetection::~FastMovingObjectDetection (   )

{
   Close (   );
}

 void FastMovingObjectDetection::_Init (   )

{
   Flag_Initialized = FALSE;
   SrcImgSize.Clear (   );
}

 void FastMovingObjectDetection::AccumulateFMOs (VFData* vf_data,int fmo_type,GImage& d_image)

{
   int x,y;

   IArray2D& fr_map = vf_data->FRMap;
   byte* fmod_array = vf_data->FMODArray1;
   if (fmo_type == 2) fmod_array = vf_data->FMODArray2;
   for (y = 0; y < fr_map.Height; y++) {
      int*  l_fr_map  = fr_map[y];
      byte* l_d_image = d_image[y];
      for (x = 0; x < fr_map.Width; x++) {
         if (l_fr_map[x]) {
            if (fmod_array[l_fr_map[x]]) l_d_image[x] = PG_WHITE;
         }
      }
   }
}

 void FastMovingObjectDetection::AccumulateFMOs (VFData* vf_data,int fmo_type,int fr_no,int base_lb_no,IArray2D& d_map)

{
   int x,y;

   IArray2D& fr_map = vf_data->FRMap;
   byte* fmod_array = vf_data->FMODArray1;
   if (fmo_type == 2) fmod_array = vf_data->FMODArray2;
   for (y = 0; y < fr_map.Height; y++) {
      int* l_fr_map = fr_map[y];
      int* l_d_map  = d_map[y];
      for (x = 0; x < fr_map.Width; x++) {
         if (l_fr_map[x]) {
            int lb_no = l_fr_map[x];
            if (fmod_array[lb_no]) l_d_map[x] = (fr_no << 16) | (base_lb_no + lb_no);
         }
      }
   }
}

 void FastMovingObjectDetection::AddToVidFrmQueue (ForegroundDetection& frg_detector)

{
   int i;

   VFData* n_vfd = new VFData;
   n_vfd->FRMap = frg_detector.FrgRegionMap;
   IArray1D& fra_array = n_vfd->FRAArray;
   CRArray1D& fr_array = frg_detector.FrgRegions;
   fra_array.Create (fr_array.Length);
   for (i = 0; i < fra_array.Length; i++)
      fra_array[i] = fr_array[i].Area;
   BArray1D& fmod_array = n_vfd->FMODArray1;
   fmod_array.Create (fra_array.Length);
   fmod_array.Set (0,fmod_array.Length,TRUE);
   VFData* l_vfd = VFQueue.Last (   );
   if (l_vfd != NULL) UnmarkFalseFMO (l_vfd,n_vfd);
   VFQueue.Add (n_vfd);
   if (VFQueue.GetNumItems (   ) > VFQLength) {
      VFData* vf_data = VFQueue.Retrieve (   );
      if (vf_data != NULL) delete vf_data;
   }
}

 void FastMovingObjectDetection::CheckFMOValidity (Array1D<ETElement>& eq_table,IArray1D& nfra_array)

{
   int i;

   // 그룹 내의 FMO들의 면적의 합을 구한다.
   IArray1D mra_array = nfra_array;
   for (i = 1; i < eq_table.Length; i++) {
      int min_lb_no = eq_table[i].MinLabelNo;
      if (min_lb_no != i) mra_array[min_lb_no] += nfra_array[i];
   }
   // (FMO가 속한 그룹의 면적) / (FMO의 면적) > 1.0인 FMO들만 유효한 FMO로 간주한다.
   int base_no = 0;
   VFData* vf_data = VFQueue.First (   );
   while (vf_data != NULL) {
      IArray1D& fra_array  = vf_data->FRAArray;
      BArray1D& fmod_array = vf_data->FMODArray2;
      fmod_array = vf_data->FMODArray1;
      for (i = 1; i < fra_array.Length; i++) {
         int fr_lb_no = base_no + i;
         int gr_lb_no = eq_table[fr_lb_no].MinLabelNo;
         if (mra_array[gr_lb_no] == nfra_array[fr_lb_no]) fmod_array[i] = FALSE;
      }
      base_no += vf_data->FRAArray.Length - 1;
      vf_data = VFQueue.Next (vf_data);
   }
}

 void FastMovingObjectDetection::Close (   )
 
{
   VFQueue.Delete (   );
   AFMOMap.Delete     (   );
   _Init (   );
}

 void FastMovingObjectDetection::GetAccumulatedFMOImage (int fmo_type,GImage& d_image)

{
   d_image.Clear (    );
   VFData* vf_data = VFQueue.First (   );
   while (vf_data != NULL) {
      AccumulateFMOs (vf_data,fmo_type,d_image);
      vf_data = VFQueue.Next (vf_data);
   }
}

 void FastMovingObjectDetection::GetAccumulatedFMOMap (int fmo_type,IArray2D& d_map)

{
   d_map.Clear (   );
   int base_lb_no = 0;
   VFData* vf_data = VFQueue.First (   );
   for (int fr_no = 1; vf_data != NULL; fr_no++) {
      AccumulateFMOs (vf_data,fmo_type,fr_no,base_lb_no,d_map);
      base_lb_no += vf_data->FRAArray.Length - 1;
      vf_data = VFQueue.Next (vf_data);
   }
}

 void FastMovingObjectDetection::GetFinalAccumulatedFMOMap (Array1D<ETElement>& eq_table)

{
   int i,n,x,y;

   // 마지막 프레임의 FMO 레이블의 시작 값에 해당하는 base_no 값을 얻는다.
   int base_no = 0;
   n = VFQueue.GetNumItems (   ) - 1;
   if (n < 0) {
      AFMOMap.Clear (   );
      return;
   }
   VFData* vf_data = VFQueue.First (   );
   for (i = 0; i < n; i++) {
      base_no += vf_data->FRAArray.Length - 1;
      vf_data = VFQueue.Next (vf_data);
   }
   // 마지막 프레임의 fr_map과 fmod_array를 얻는다.
   vf_data = VFQueue.Last (   );
   BArray1D& fmod_array = vf_data->FMODArray2;
   // FMO의 레이블 값 변환을 위한 배열을 만든다.
   // 동일 그룹에 속하는 FMO들의 레이블 값은 마지막 프레임의 FMO의 레이블 값과 일치하도록 할당된다.
   IArray1D lc_array(eq_table.Length);
   for (i = 0; i < eq_table.Length; i++)
      lc_array[i] = eq_table[i].MinLabelNo;
   IArray1D t_array(lc_array.Length);
   t_array.Clear (   );
   for (i = base_no + 1; i < lc_array.Length; i++) {
      int lb_no = i - base_no;
      if (fmod_array[lb_no]) t_array[lc_array[i]] = lb_no;
   }
   for (i = 1; i < lc_array.Length; i++)
      lc_array[i] = t_array[lc_array[i]];
   // AFMOMap을 얻는다.
   for (y = 0; y < AFMOMap.Height; y++) {
      int* l_afmo_map = AFMOMap[y];
      for (x = 0; x < AFMOMap.Width; x++)
         l_afmo_map[x] = lc_array[l_afmo_map[x] & 0x0000FFFF];
   }
}

 int FastMovingObjectDetection::GetNetFRAArray (IArray1D& nfra_array)

{
   int i;
   
   nfra_array.Delete (   );
   int n_frs = 0;
   VFData* vf_data = VFQueue.First (   );
   while (vf_data != NULL) {
      n_frs += vf_data->FRAArray.Length - 1;
      vf_data = VFQueue.Next (vf_data);
   }
   if (!n_frs) return (1);
   nfra_array.Create (n_frs + 1);
   nfra_array[0] = 0;
   int base_no   = 0;
   vf_data = VFQueue.First (   );
   while (vf_data != NULL) {
      IArray1D& fra_array = vf_data->FRAArray;
      for (i = 1; i < fra_array.Length; i++) 
         nfra_array[base_no + i] = fra_array[i];
      base_no += fra_array.Length - 1;
      vf_data = VFQueue.Next (vf_data);
   }
   return (DONE);
}

 void FastMovingObjectDetection::GetRegionBoundaryMap (IArray2D& s_map,IArray2D& d_map)

{
   int x,y;
   
   d_map.Clear (   );
   int ex = s_map.Width  - 1;
   int ey = s_map.Height - 1;
   for (y = 1; y < ey; y++) {
      int* l_s_map  = s_map[y];
      int* l_s_map1 = s_map[y - 1];
      int* l_s_map2 = s_map[y + 1];
      int* l_d_map  = d_map[y];
      for (x = 1; x < ex; x++) {
         if (l_s_map[x]) {
            int lb_no = l_s_map[x];
            if (!(l_s_map[x - 1] == lb_no && l_s_map[x + 1] == lb_no && l_s_map1[x] == lb_no && l_s_map2[x] == lb_no)) {
               l_d_map[x] = lb_no;
            }
         }
      }
   }
}

 void FastMovingObjectDetection::GroupAdjacentRegions (IArray2D& rb_map,IArray1D& nfra_array,Array1D<ETElement>& eq_table)

{
   int x,y,x1,y1;
   
   eq_table.Create (nfra_array.Length);
   InitEquivalenceTable (eq_table,eq_table.Length);
   int grp_range = (int)(GroupingRange * rb_map.Height / 240.0f + 0.5f);
   int ex = rb_map.Width  - 1;
   int ey = rb_map.Height - 1;
   for (y = 1; y < ey; y++) {
      int* l_rbm = rb_map[y];
      for (x = 1; x < ex; x++) {
         if (l_rbm[x]) {
            int fr_no = l_rbm[x] & 0xFFFF0000;
            int lb_no = l_rbm[x] & 0x0000FFFF;
            int sx1 = x - grp_range;
            int ex1 = x + grp_range;
            int sy1 = y - grp_range;
            int ey1 = y + grp_range;
            if (sx1 <  0) sx1 = 0;
            if (ex1 > ex) ex1 = ex;
            if (sy1 <  0) sy1 = 0;
            if (ey1 > ey) ey1 = ey;
            for (y1 = sy1; y1 <= ey1; y1++) {
               int* l_rbm1 = rb_map[y1];
               for (x1 = sx1; x1 <= ex1; x1++) {
                  if (l_rbm1[x1]) {
                     int fr_no1 = l_rbm1[x1] & 0xFFFF0000;
                     if (fr_no != fr_no1) {
                        int lb_no1 = l_rbm1[x1] & 0x0000FFFF;
                        float ar = (float)nfra_array[lb_no] / nfra_array[lb_no1];
                        if (ar > 1.0f) ar = 1.0f / ar;
                        if (ar >= MinAreaRatio) UpdateEquivalenceTable (eq_table,lb_no,lb_no1);
                     }
                  }
               }
            }
         }
      }
   }
}

 void FastMovingObjectDetection::Initialize (ISize2D& si_size)

{
   Close (   );
   SrcImgSize = si_size;
   AFMOMap.Create (si_size.Width,si_size.Height);
   AFMOMap.Clear  (   );
   Flag_Initialized = TRUE;
}

 int FastMovingObjectDetection::Perform (ForegroundDetection& frg_detector)

{
   #if defined(__DEBUG_FMD)
   _DebugView_FMD->Clear (   );
   _DVX_FMD = _DVY_FMD = 0;
   #endif
   if (!IsInitialized (   )) return (1);
   if (SrcImgSize.Width != frg_detector.SrcImgSize.Width || SrcImgSize.Height!= frg_detector.SrcImgSize.Height) return (2);
   // frg_detector의 전경 검출 결과를 VFQueue에 저장한다.
   // 이때 이전 프레임의 전경 검출 결과와 비교하여, 프레임 간 겹침이 없는(또는 매우 적은)
   // FR(Foreground Region)들만 솎아낸다. 이러한 FR들이 FMO(Fast Moving Object) 후보가 된다.
   AddToVidFrmQueue (frg_detector);
   // 모든 프레임에서 검출된 FR들의 면적 값을 저장한 배열을 구한다.
   IArray1D nfra_array;
   if (GetNetFRAArray (nfra_array)) return (2);
   // 각 프레임에서 1차적으로 검출한 FMO들을 누적시켜 AFMOMap을 만든다.
   GetAccumulatedFMOMap (1,AFMOMap);
   #if defined(__DEBUG_FMD)
   _DebugView_FMD->DrawImage (AFMOMap,_DVX_FMD,_DVY_FMD);
   int x,y;
   StringA text;
   BArray1D m_array(nfra_array.Length);
   m_array.Clear (   );
   for (y = 0; y < AFMOMap.Height; y++) {
      int* l_AFMOMap = AFMOMap[y];
      for (x = 0; x < AFMOMap.Width; x++) {
         if (l_AFMOMap[x]) {
            int lb_no = l_AFMOMap[x] & 0x0000FFFF;
            if (!m_array[lb_no]) {
               m_array[lb_no] = TRUE;
               text.Format ("%d",lb_no);
               _DebugView_FMD->DrawText (text,_DVX_FMD + x - 15,_DVY_FMD + y - 15,PC_RED,TRANSPARENT,FT_ARIAL,14);
            }
         }
      }
   }
   _DVY_FMD += AFMOMap.Height;
   _DebugView_FMD->DrawText ("Accumulated FMO Map",_DVX_FMD,_DVY_FMD,PC_GREEN);
   _DVY_FMD += 20;
   #endif
   // AFMOMap에서 FMO 영역의 바운더리만 추출한다.
   IArray2D rb_map(AFMOMap.Width,AFMOMap.Height);
   GetRegionBoundaryMap (AFMOMap,rb_map);
   // 인접한 FMO 간 그룹핑을 수행한다.
   // 이때 서로 다른 프레임에서 나온 FMO 간의 그룹핑만 허용된다.
   Array1D<ETElement> eq_table;
   GroupAdjacentRegions (rb_map,nfra_array,eq_table);
   #if defined(__DEBUG_FMD)
   logd ("\n[FMO Grouping Result]\n");
   int i;
   for (i = 1; i < eq_table.Length; i++) {
      logd ("[L%d:A%d:G%d]",i,nfra_array[i],eq_table[i].MinLabelNo);
   }
   logd ("\n\n");
   #endif
   // 각 FMO의 유효성을 체크한다.
   CheckFMOValidity (eq_table,nfra_array);
   // 최종 Accumulated FMO Map을 획득한다.
   GetFinalAccumulatedFMOMap (eq_table);
   #if defined(__DEBUG_FMD)
   _DebugView_FMD->DrawImage (AFMOMap,_DVX_FMD,_DVY_FMD);
   _DVY_FMD += AFMOMap.Height;
   _DebugView_FMD->DrawText ("Final AFMO Map",_DVX_FMD,_DVY_FMD,PC_GREEN);
   _DVY_FMD += 20;
   #endif
   #if defined(__DEBUG_FMD)
   _DebugView_FMD->Invalidate (   );
   #endif
   return (DONE);
}

 int FastMovingObjectDetection::ReadFile (FileIO& file)

{
   if (file.Read ((byte*)&GroupingRange,1,sizeof(int))) return (1);
   return (DONE);
}

 int FastMovingObjectDetection::ReadFile (CXMLIO* pIO)

{
   static FastMovingObjectDetection dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("FastMovingObjectDetection")) {
      xmlNode.Attribute (TYPE_ID(int),"GroupingRange",&GroupingRange,&dv.GroupingRange);
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}

 void FastMovingObjectDetection::UnmarkFalseFMO (VFData* vfd1,VFData* vfd2)

{
   int i,x,y;
   
   BArray1D& fmod_array1 = vfd1->FMODArray1;
   IArray1D& fra_array1  = vfd1->FRAArray;
   IArray2D& fr_map1     = vfd1->FRMap;
   BArray1D& fmod_array2 = vfd2->FMODArray1;
   IArray1D& fra_array2  = vfd2->FRAArray;
   IArray2D& fr_map2     = vfd2->FRMap;
   IArray1D oc_array1(fra_array1.Length);
   oc_array1.Clear (   );
   IArray1D oc_array2(fra_array2.Length);
   oc_array2.Clear (   );
   for (y = 0; y < fr_map1.Height; y++) {
      int* l_fr_map1 = fr_map1[y];
      int* l_fr_map2 = fr_map2[y];
      for (x = 0; x < fr_map1.Width; x++) {
         if (l_fr_map1[x] && l_fr_map2[x]) {
            oc_array1[l_fr_map1[x]]++;
            oc_array2[l_fr_map2[x]]++;
         }
      }
   }
   for (i = 1; i < oc_array1.Length; i++) {
      float overlap = (float)oc_array1[i] / fra_array1[i];
      if (overlap > MaxOverlap) fmod_array1[i] = FALSE;
   }
   for (i = 1; i < oc_array2.Length; i++) {
      float overlap = (float)oc_array2[i] / fra_array2[i];
      if (overlap > MaxOverlap) fmod_array2[i] = FALSE;
   }
}

 int FastMovingObjectDetection::WriteFile (FileIO& file)

{
   if (file.Write ((byte*)&GroupingRange,1,sizeof(int))) return (1);
   return (DONE);
}

 int FastMovingObjectDetection::WriteFile (CXMLIO* pIO)

{
   static FastMovingObjectDetection dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("FastMovingObjectDetection")) {
      xmlNode.Attribute (TYPE_ID(int),"GroupingRange",&GroupingRange,&dv.GroupingRange);
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}

}

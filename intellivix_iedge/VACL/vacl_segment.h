#if !defined(__VACL_SEGMENT_H)
#define __VACL_SEGMENT_H

#include "vacl_define.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

void Binarize_Otsu     (GImage &s_image,GImage &d_image);
void GetThreshold_Otsu (IArray1D &s_array,byte &threshold,float &tm,float &bgm,float &fgm);

}

#endif

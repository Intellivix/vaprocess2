#if !defined(__VACL_IPP_H)
#define __VACL_IPP_H

#include "vacl_define.h"

#if defined(__LIB_IPP)
#include "ipp.h"
#endif

 namespace VACL
 
{

///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_IPP)

void IPP_ConvertBGRToYUY2      (BGRImage& s_image,byte* di_buffer);
void IPP_ConvertNV12ToBGR      (byte* si_buffer,BGRImage& d_image);
void IPP_ConvertYUY2ToBGR      (byte* si_buffer,BGRImage& d_image);
// [주의] X 값과 Width 값은 짝수여야 한다.
void IPP_ExtractAndResize_YUY2 (byte* si_buffer,ISize2D& si_size,IBox2D& e_rgn,byte* di_buffer,ISize2D& di_size);
void IPP_Resize                (GImage& s_image,GImage& d_image);
void IPP_Resize                (BGRImage& s_image,BGRImage& d_image);
void IPP_Resize_YUY2           (byte* si_buffer,ISize2D& si_size,byte* di_buffer,ISize2D& di_size);
void IPP_WarpAffine            (GImage& s_image,Matrix& mat_H,GImage& d_image);
void IPP_WarpAffine            (BGRImage& s_image,Matrix& mat_H,BGRImage& d_image);
void IPP_WarpPerspective       (GImage& s_image,Matrix& mat_H,GImage& d_image);
void IPP_WarpPerspective       (BGRImage& s_image,Matrix& mat_H,BGRImage& d_image);
void IPP_Rotate                (BGRImage &s_image, double angle, BGRImage &d_image);
#endif

///////////////////////////////////////////////////////////////////////////////

void InitIPP                 (   );
void ConvertYUY2ToBGR_2      (byte* si_buffer,BGRImage& d_cimage);
void ConvertBGRToYUY2_2      (BGRImage& s_cimage,byte* di_buffer);
// [주의] X 값과 Width 값은 짝수여야 한다.
void ExtractAndResize_YUY2_2 (byte* si_buffer,ISize2D& si_size,IBox2D& e_rgn,byte* di_buffer,ISize2D& di_size);
void Resize_2                (GImage& s_image,GImage& d_image,int flag_hq_reduction = FALSE);
void Resize_2                (BGRImage& s_image,BGRImage& d_image,int flag_hq_reduction = FALSE);
void Resize_YUY2_2           (byte* si_buffer,ISize2D& si_size,byte* di_buffer,ISize2D& di_size);
void Warp_2                  (GImage& s_image,Matrix& mat_H,GImage& d_image);
void Warp_2                  (BGRImage& s_cimage,Matrix& mat_H,BGRImage& d_cimage);

}

#endif

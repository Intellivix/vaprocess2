#include "vacl_opencv.h"

#if defined(__LIB_OPENCV)

#if defined(__OS_WINDOWS)
   #if defined(_DEBUG)
      #pragma comment(lib,"opencv_calib3d2413d.lib")
      #pragma comment(lib,"opencv_core2413d.lib")
      #pragma comment(lib,"opencv_features2d2413d.lib")
      #pragma comment(lib,"opencv_flann2413d.lib")
      #pragma comment(lib,"opencv_imgproc2413d.lib")
      #pragma comment(lib,"opencv_ml2413d.lib")
      #pragma comment(lib,"opencv_nonfree2413d.lib")
      #pragma comment(lib,"opencv_objdetect2413d.lib")
      #pragma comment(lib,"opencv_ocl2413d.lib")
      #pragma comment(lib,"opencv_video2413d.lib")
   #else
      #pragma comment(lib,"opencv_calib3d2413.lib")
      #pragma comment(lib,"opencv_core2413.lib")
      #pragma comment(lib,"opencv_features2d2413.lib")
      #pragma comment(lib,"opencv_flann2413.lib")
      #pragma comment(lib,"opencv_imgproc2413.lib")
      #pragma comment(lib,"opencv_ml2413.lib")
      #pragma comment(lib,"opencv_nonfree2413.lib")
      #pragma comment(lib,"opencv_objdetect2413.lib")
      #pragma comment(lib,"opencv_ocl2413.lib")
      #pragma comment(lib,"opencv_video2413.lib")
   #endif
#endif

 namespace VACL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////
 
///////////////////////////////////////////////////////////////////////////////
// "CvMat" object initialization functions
///////////////////////////////////////////////////////////////////////////////

 void OpenCV_InitCvMat (FArray1D& s_array,CvMat& d_mat,int flag_col_vec)
 
{
   int n_rows,n_cols;

   if (flag_col_vec) {
      n_rows = s_array.Length;
      n_cols = 1;
   }
   else {
     n_rows = 1;
     n_cols = s_array.Length;
   }
   cvInitMatHeader (&d_mat,n_rows,n_cols,CV_32FC1,(void*)s_array,s_array.Length * sizeof(float));
}

 void OpenCV_InitCvMat (FArray2D& s_array,CvMat& d_mat)
 
{
   cvInitMatHeader (&d_mat,s_array.Height,s_array.Width,CV_32FC1,(void*)s_array,s_array.Width * sizeof(float));
}

 void OpenCV_InitCvMat (IArray2D& s_array,CvMat& d_mat)
 
{
   cvInitMatHeader (&d_mat,s_array.Height,s_array.Width,CV_32SC1,(void*)s_array,s_array.Width * sizeof(int));
}

 void OpenCV_InitCvMat (GImage& s_image,CvMat& d_mat)
 
{
   cvInitMatHeader (&d_mat,s_image.Height,s_image.Width,CV_8UC1,(void*)s_image,s_image.GetLineLength());
}

 void OpenCV_InitCvMat (BGRImage& s_cimage,CvMat& d_mat)
 
{
   cvInitMatHeader (&d_mat,s_cimage.Height,s_cimage.Width,CV_8UC3,(void*)s_cimage,s_cimage.GetLineLength());
}

 void OpenCV_InitCvMat (Matrix& mat_S,CvMat& d_mat)
 
{
   cvInitMatHeader (&d_mat,mat_S.NRows,mat_S.NCols,CV_64FC1,(void*)mat_S,mat_S.NCols * sizeof(double));
}

 void OpenCV_InitCvMat (FP2DArray1D& s_array,CvMat& d_mat)
 
{
   cvInitMatHeader (&d_mat,s_array.Length,2,CV_32FC1,(void*)s_array,sizeof(FPoint2D));
}

 void OpenCV_InitCvMat (FP2DArray2D& s_array,CvMat& d_mat)
 
{
   cvInitMatHeader (&d_mat,s_array.Height,s_array.Width,CV_32FC2,(void*)s_array,s_array.Width * sizeof(FPoint2D));
}

 void OpenCV_InitCvMat (FP3DArray1D& s_array,CvMat& d_mat)
 
{
   cvInitMatHeader (&d_mat,s_array.Length,3,CV_32FC1,(void*)s_array,sizeof(FPoint3D));
}

 void OpenCV_InitCvMat (FP3DArray2D& s_array,CvMat& d_mat)
 
{
   cvInitMatHeader (&d_mat,s_array.Height,s_array.Width,CV_32FC3,(void*)s_array,s_array.Width * sizeof(FPoint3D));
}

///////////////////////////////////////////////////////////////////////////////
// "cv::Mat" object initialization functions
///////////////////////////////////////////////////////////////////////////////
 
 void OpenCV_InitMat (int width,int height,int type,void *s_buffer,int step,cv::Mat& d_mat)
 
{
   d_mat.flags     = cv::Mat::MAGIC_VAL + (type & TYPE_MASK);
   d_mat.dims      = 2;
   d_mat.rows      = height;
   d_mat.cols      = width;
   d_mat.data      = (uchar*)s_buffer;
   d_mat.refcount  = 0;
   d_mat.datastart = (uchar*)s_buffer;
   d_mat.dataend   = 0;
   d_mat.datalimit = 0;
   d_mat.allocator = 0;
   d_mat.size.p    = &d_mat.rows;
   int esz     = CV_ELEM_SIZE(type);
   int minstep = d_mat.cols * esz;
   if (step == cv::Mat::AUTO_STEP) {
      step = minstep;
      d_mat.flags |= cv::Mat::CONTINUOUS_FLAG;
   }
   else {
      if (d_mat.rows == 1) step = minstep;
      CV_DbgAssert (step >= minstep);
      d_mat.flags |= step == minstep ? cv::Mat::CONTINUOUS_FLAG : 0;
   }
   d_mat.step[0]   = step;
   d_mat.step[1]   = esz;
   d_mat.datalimit = d_mat.datastart + step * d_mat.rows;
   d_mat.dataend   = d_mat.datalimit - step + minstep;
}

 void OpenCV_InitMat (FArray1D& s_array,cv::Mat& d_mat,int flag_col_vec)
 
{
   int width,height;
   
   if (flag_col_vec) {
      width  = 1;
      height = s_array.Length;
   }
   else {
      width  = s_array.Length;
      height = 1;
   }
   OpenCV_InitMat (width,height,CV_32FC1,(void*)s_array,sizeof(float),d_mat);
}

 void OpenCV_InitMat (FArray2D& s_array,cv::Mat& d_mat)
 
{
   OpenCV_InitMat (s_array.Width,s_array.Height,CV_32FC1,(void*)s_array,s_array.Width * sizeof(float),d_mat);
}

 void OpenCV_InitMat (IArray2D& s_array,cv::Mat& d_mat)
 
{
   OpenCV_InitMat (s_array.Width,s_array.Height,CV_32SC1,(void*)s_array,s_array.Width * sizeof(int),d_mat);
}

 void OpenCV_InitMat (SArray2D& s_array,cv::Mat& d_mat)
 
{
   OpenCV_InitMat (s_array.Width,s_array.Height,CV_16SC1,(void*)s_array,s_array.Width * sizeof(short),d_mat);
}

 void OpenCV_InitMat (USArray2D& s_array,cv::Mat& d_mat)
 
{
   OpenCV_InitMat (s_array.Width,s_array.Height,CV_16UC1,(void*)s_array,s_array.Width * sizeof(ushort),d_mat);
}

 void OpenCV_InitMat (GImage& s_image,cv::Mat& d_mat)
 
{
   OpenCV_InitMat (s_image.Width,s_image.Height,CV_8UC1,(void*)s_image,s_image.GetLineLength(),d_mat);
}

 void OpenCV_InitMat (BGRImage& s_image,cv::Mat& d_mat)
 
{
   OpenCV_InitMat (s_image.Width,s_image.Height,CV_8UC3,(void*)s_image,s_image.GetLineLength(),d_mat);
}

 void OpenCV_InitMat (Matrix& mat_S,cv::Mat& d_mat)
 
{
   OpenCV_InitMat (mat_S.NCols,mat_S.NRows,CV_64FC1,(void*)mat_S,mat_S.NCols * sizeof(double),d_mat);
}

 void OpenCV_InitMat (FP2DArray1D& s_array,cv::Mat& d_mat)
 
{
   OpenCV_InitMat (2,s_array.Length,CV_32FC1,(void*)s_array,sizeof(FPoint2D),d_mat);
}

 void OpenCV_InitMat (FP2DArray2D& s_array,cv::Mat& d_mat)
 
{
   OpenCV_InitMat (s_array.Width,s_array.Height,CV_32FC2,(void*)s_array,s_array.Width * sizeof(FPoint2D),d_mat);
}

 void OpenCV_InitMat (FP3DArray1D& s_array,cv::Mat& d_mat)
 
{
   OpenCV_InitMat (3,s_array.Length,CV_32FC1,(void*)s_array,sizeof(FPoint3D),d_mat);
}

 void OpenCV_InitMat (FP3DArray2D& s_array,cv::Mat& d_mat)
 
{
   OpenCV_InitMat (s_array.Width,s_array.Height,CV_32FC3,(void*)s_array,s_array.Width * sizeof(FPoint3D),d_mat);
}

///////////////////////////////////////////////////////////////////////////////
// Utility functions
///////////////////////////////////////////////////////////////////////////////

 void OpenCV_Copy (Matrix &vec_s,vector<double> &d_vector)

{
   int i;
   
   int size = vec_s.NRows * vec_s.NCols;
   if (size != (int)d_vector.size()) d_vector.resize (size);
   for (i = 0; i < size; i++) d_vector[i] = vec_s(i); 
}

 void OpenCV_Copy (vector<double>& s_vector,Matrix &vec_d)

{
   int i;
   
   int size = vec_d.NRows * vec_d.NCols;
   if ((int)s_vector.size() != size) {
      size = (int)s_vector.size();
      vec_d.Create (size);
   }
   for (i = 0; i < size; i++) vec_d(i) = s_vector[i];
}

 void OpenCV_Copy (FP2DArray1D &s_array,vector<Point2f> &d_vector)

{
   int i;
   
   if (s_array.Length != (int)d_vector.size()) d_vector.resize (s_array.Length);
   for (i = 0; i < s_array.Length; i++) {
      d_vector[i].x = s_array[i].X;
      d_vector[i].y = s_array[i].Y;
   }
}

 void OpenCV_Copy (vector<Point2f> &s_vector,FP2DArray1D &d_array)

{
   int i;
   
   if ((int)s_vector.size() != d_array.Length) d_array.Create ((int)s_vector.size());
   for (i = 0; i < d_array.Length; i++) {
      d_array[i].X = s_vector[i].x;
      d_array[i].Y = s_vector[i].y;
   }
}

 void OpenCV_Copy (FP3DArray1D &s_array,vector<Point3f> &d_vector)

{
   int i;
   
   if (s_array.Length != (int)d_vector.size()) d_vector.resize (s_array.Length);
   for (i = 0; i < s_array.Length; i++) {
      d_vector[i].x = s_array[i].X;
      d_vector[i].y = s_array[i].Y;
      d_vector[i].z = s_array[i].Z;
   }
}

 void OpenCV_Copy (vector<Point3f> &s_vector,FP3DArray1D &d_array)

{
   int i;
   
   if ((int)s_vector.size() != d_array.Length) d_array.Create ((int)s_vector.size());
   for (i = 0; i < d_array.Length; i++) {
      d_array[i].X = s_vector[i].x;
      d_array[i].Y = s_vector[i].y;
      d_array[i].Z = s_vector[i].z;
   }
}

 int OpenCV_Copy (cv::Mat& s_img,GImage& d_image)

{
   int i;
   
   if (s_img.cols != d_image.Width ) return (1);
   if (s_img.rows != d_image.Height) return (2);
   if (s_img.depth()    != CV_8U   ) return (3);
   if (s_img.channels() != 1       ) return (4);
   int line_size = s_img.cols;
   for (i = 0; i < s_img.rows; i++)
      memcpy (d_image[i],s_img.ptr(i),line_size);
   return (DONE);
}

 int OpenCV_Copy (cv::Mat& s_cimg,BGRImage& d_cimage)

{
   int i;
   
   if (s_cimg.cols != d_cimage.Width ) return (1);
   if (s_cimg.rows != d_cimage.Height) return (2);
   if (s_cimg.depth()    != CV_8U    ) return (3);
   if (s_cimg.channels() != 3        ) return (4);
   int line_size = s_cimg.cols * 3;
   for (i = 0; i < s_cimg.rows; i++)
      memcpy (d_cimage[i],s_cimg.ptr(i),line_size);
   return (DONE);
}

 int OpenCV_Copy (cv::Mat& s_mat,Matrix& d_mat)

{
   int i;

   if (s_mat.cols != d_mat.NCols ) return (1);
   if (s_mat.rows != d_mat.NRows ) return (2);
   if (s_mat.depth()    != CV_64F) return (3);
   if (s_mat.channels() != 1     ) return (4);
   int line_size = s_mat.cols * sizeof(double);
   for (i = 0; i < s_mat.rows; i++)
      memcpy (d_mat[i],s_mat.ptr<double>(i),line_size);
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
// OpenCV function wrappers
///////////////////////////////////////////////////////////////////////////////

 void OpenCV_DetectEdges_Canny (GImage &s_image,float l_es_thrsld,float h_es_thrsld,GImage &d_image)
 
{
   CvMat m_s_image,m_d_image;
   OpenCV_InitCvMat (s_image,m_s_image);
   OpenCV_InitCvMat (d_image,m_d_image);
   cvCanny (&m_s_image,&m_d_image,l_es_thrsld,h_es_thrsld);
}

 void OpenCV_DistanceTransform (GImage& s_image,FArray2D& d_array)

{
   CvMat m_s_image,m_d_array;
   OpenCV_InitCvMat (s_image,m_s_image);
   OpenCV_InitCvMat (d_array,m_d_array);
   cvDistTransform  (&m_s_image,&m_d_array);
}

 void OpenCV_DrawCircle (GImage& s_image,IPoint2D& center,int radius,byte color,int thickness)

{
   CvMat m_s_image;
   OpenCV_InitCvMat (s_image,m_s_image);
   CvPoint ct;
   ct.x = center.X;
   ct.y = center.Y;
   CvScalar cl = cvRealScalar (color);
   cvCircle (&m_s_image,ct,radius,cl,thickness);
}

 void OpenCV_DrawFilledCircle (GImage& s_image,IPoint2D &center,int radius,byte color)
 
{
   CvMat m_s_image;
   OpenCV_InitCvMat (s_image,m_s_image);
   CvPoint ct;
   ct.x = center.X;
   ct.y = center.Y;
   CvScalar cl = cvRealScalar (color);
   cvCircle (&m_s_image,ct,radius,cl,-1);
}

 void OpenCV_Watershed (GImage& s_image,IArray2D& s_cr_map)

{
   BGRImage s_cimage = s_image;
   OpenCV_Watershed (s_cimage,s_cr_map);    
}

 void OpenCV_Watershed (BGRImage& s_cimage,IArray2D& s_cr_map)
// 함수 호출 시 s_cr_map에 seed 영역들이 입력되어 있어야 한다.
// segmentation 결과는 s_cr_map에 저장된다.
{
   CvMat m_s_cimage,m_s_cr_map;
   OpenCV_InitCvMat (s_cimage,m_s_cimage);
   OpenCV_InitCvMat (s_cr_map,m_s_cr_map);
   cvWatershed (&m_s_cimage,&m_s_cr_map);
}

 double OpenCV_CalibrateCamera (FP3DArray1D &op_array,FP2DArray1D &ip_array,IArray1D &n_array,ISize2D &ci_size,Matrix &mat_K,Matrix &vec_DCs,Matrix &mat_RVs,Matrix &mat_TVs)
 
{
   int i;

   CvMat object_points;
   OpenCV_InitCvMat (op_array,object_points);
   CvMat image_points;
   OpenCV_InitCvMat (ip_array,image_points);
   IArray2D np_array(1,n_array.Length);
   for (i = 0; i < n_array.Length; i++) np_array[i][0] = n_array[i];
   CvMat point_counts;
   OpenCV_InitCvMat (np_array,point_counts);
   CvSize image_size;
   image_size.width  = ci_size.Width;
   image_size.height = ci_size.Height;
   CvMat camera_matrix;
   OpenCV_InitCvMat (mat_K,camera_matrix);
   CvMat distortion_coeffs;
   OpenCV_InitCvMat (vec_DCs,distortion_coeffs);
   CvMat rotation_vectors;
   OpenCV_InitCvMat (mat_RVs,rotation_vectors);
   CvMat translation_vectors;
   OpenCV_InitCvMat (mat_TVs,translation_vectors);
   double rp_err = cvCalibrateCamera2 (&object_points,&image_points,&point_counts,image_size,&camera_matrix,&distortion_coeffs,&rotation_vectors,&translation_vectors);
   return (rp_err);
}

 double OpenCV_CalibrateStereoCameras (FP3DArray1D &op_array,FP2DArray1D &ip_array1,FP2DArray1D &ip_array2,IArray1D &n_array,ISize2D &ci_size,Matrix &mat_K1,Matrix &vec_DCs1,Matrix &mat_K2,Matrix &vec_DCs2,Matrix &mat_R,Matrix &vec_t)
 
{
   int i;
   
   CvMat object_points;
   OpenCV_InitCvMat (op_array,object_points);
   CvMat image_points1;
   OpenCV_InitCvMat (ip_array1,image_points1);
   CvMat image_points2;
   OpenCV_InitCvMat (ip_array2,image_points2);
   IArray2D np_array(1,n_array.Length);
   for (i = 0; i < n_array.Length; i++) np_array[i][0] = n_array[i];
   CvMat npoints;
   OpenCV_InitCvMat (np_array,npoints);
   CvSize image_size;
   image_size.width  = ci_size.Width;
   image_size.height = ci_size.Height;
   CvMat camera_matrix1;
   OpenCV_InitCvMat (mat_K1,camera_matrix1);
   CvMat dist_coeffs1;
   OpenCV_InitCvMat (vec_DCs1,dist_coeffs1);
   CvMat camera_matrix2;
   OpenCV_InitCvMat (mat_K2,camera_matrix2);
   CvMat dist_coeffs2;
   OpenCV_InitCvMat (vec_DCs2,dist_coeffs2);
   CvMat rotation_matrix;
   OpenCV_InitCvMat (mat_R,rotation_matrix);
   CvMat translation_vector;
   OpenCV_InitCvMat (vec_t,translation_vector);
   double rp_err = cvStereoCalibrate (&object_points,&image_points1,&image_points2,&npoints,&camera_matrix1,&dist_coeffs1,&camera_matrix2,&dist_coeffs2,image_size,&rotation_matrix,&translation_vector);
   return (rp_err);
}

 void OpenCV_CLAHE (GImage& s_image,GImage& d_image,float clip_limit,int tile_size)

{
   cv::Mat m_s_image;
   OpenCV_InitMat (s_image,m_s_image);
   cv::Mat m_d_image;
   OpenCV_InitMat (d_image,m_d_image);
   cv::Size grid_size(tile_size,tile_size);
   cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE (clip_limit,grid_size);
   clahe->apply (m_s_image,m_d_image);
}

 void OpenCV_CLAHE_YUY2 (byte* s_buffer,ISize2D& si_size,float clip_limit,int tile_size)

{
   GImage s_image(si_size.Width,si_size.Height);
   ConvertYUY2ToGray (s_buffer,s_image);
   GImage d_image(si_size.Width,si_size.Height);
   OpenCV_CLAHE (s_image,d_image,clip_limit,tile_size);
   ReplaceYComponents_YUY2 (s_buffer,d_image);
}

 void OpenCV_CLAHE_YV12 (byte* s_buffer,ISize2D& si_size,float clip_limit,int tile_size)

{
   GImage s_image(si_size.Width,si_size.Height);
   ConvertYV12ToGray (s_buffer,s_image);
   cv::Mat m_s_image;
   OpenCV_InitMat (s_image,m_s_image);
   cv::Mat m_d_image;
   OpenCV_InitMat (si_size.Width,si_size.Height,CV_8UC1,s_buffer,si_size.Width,m_d_image);
   cv::Size grid_size(tile_size,tile_size);
   cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE (clip_limit,grid_size);
   clahe->apply (m_s_image,m_d_image);
}

 void OpenCV_DrawChessboardCorners (BGRImage &s_image,ISize2D &pt_size,FP2DArray1D &cp_array,int flag_pt_found)
 
{
   int i;
 
   cv::Mat m_s_image;
   OpenCV_InitMat (s_image,m_s_image);
   Size patternSize(pt_size.Width,pt_size.Height);
   vector<Point2f> corners(cp_array.Length);
   for (i = 0; i < cp_array.Length; i++) {
      corners[i].x = cp_array[i].X;
      corners[i].y = cp_array[i].Y;
   }
   bool patternWasFound = false;
   if (flag_pt_found) patternWasFound = true;
   drawChessboardCorners (m_s_image,patternSize,corners,patternWasFound);
}

 int OpenCV_FindChessboardCorners (GImage &s_image,ISize2D &pt_size,FP2DArray1D &d_array,int options)
 
{
   int i;
   
   cv::Mat m_s_image;
   OpenCV_InitMat (s_image,m_s_image);
   Size patternSize(pt_size.Width,pt_size.Height);
   vector<Point2f> corners;
   d_array.Delete (   );
   if (findChessboardCorners (m_s_image,patternSize,corners,options)) {
      cornerSubPix (m_s_image,corners,Size(5,5),Size(-1,-1),TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER,30,0.1));
      int n = (int)corners.size (   );
      d_array.Create (n);
      for (i = 0; i < n; i++) {
         d_array[i].X = corners[i].x;
         d_array[i].Y = corners[i].y;
      }
      return (TRUE);
   }
   else return (FALSE);
}

 void OpenCV_GetDenseOpticalFlow (GImage &s_image1,GImage &s_image2,FP2DArray2D &d_array,float pyr_scale,int n_levels,int win_size,int n_iterations,int poly_n,float poly_sigma,int flags)

{
   CvMat m_s_image1,m_s_image2,m_d_array;
   OpenCV_InitCvMat (s_image1,m_s_image1);
   OpenCV_InitCvMat (s_image2,m_s_image2);
   OpenCV_InitCvMat (d_array,m_d_array);
   cvCalcOpticalFlowFarneback (&m_s_image1,&m_s_image2,&m_d_array,pyr_scale,n_levels,win_size,n_iterations,poly_n,poly_sigma,flags);
}

 void OpenCV_Rodrigues (double* vec_r,Matrix &mat_R)
 
{
   int i;
   
   vector<double> src(3);
   for (i = 0; i < 3; i++) src[i] = vec_r[i];
   cv::Mat dst;
   OpenCV_InitMat (mat_R,dst);
   Rodrigues (src,dst);
}

}

#endif

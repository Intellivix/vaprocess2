#if !defined(__VACL_OBJRECOG_H)
#define __VACL_OBJRECOG_H

#include "vacl_dnn.h"
#include "vacl_objtrack.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectRecognition
//
///////////////////////////////////////////////////////////////////////////////

 class ObjectRecognition

{
   protected:
      int Flag_Enabled;
      int Flag_Initialized;

   protected:
      float           FrameDuration;
      float           FrameRate;
      ISize2D         SrcImgSize;
      ISize2D         SrcFrmSize;
      FPoint2D        Scale;
      byte*           SrcFrmBuffer = nullptr;
      ObjectTracking* ObjectTracker;

   public:
      int      NumTries;
      float    ProcPeriod;
      float    MaxTime_Unrecognized;
      float    OuterZoneSize1;
      float    OuterZoneSize2;
      FPoint2D Inflation;

   protected:
      ObjectRecognition (   );
      virtual ~ObjectRecognition (   );

   protected:
      virtual int    CheckObject             (TrackedObject* t_object) = 0;
      virtual int*   GetObjectCountVariable  (TrackedObject* t_object) = 0;
      virtual float* GetObjectTimeVariable   (TrackedObject* t_object) = 0;
      virtual void   RecognizeObject         (TrackedObject* t_object) = 0;
      virtual void   UpdateRecognitionResult (TrackedObject* t_object) = 0;

   protected:
      virtual int DoPreprocessing  (ObjectTracking& obj_tracker) { return (0); };
      virtual int UpdateLikelihood (TrackedObject* t_object);

   public:
      virtual void Close      (   );
      virtual int  Initialize (   );
      virtual int  Perform    (ObjectTracking& obj_tracker);

   public:
      void Enable        (int flag_enable);
      int  IsEnabled     (   );
      int  IsInitialized (   );
};

 inline void ObjectRecognition::Enable (int flag_enable)

{
   Flag_Enabled = flag_enable;
}

 inline int ObjectRecognition::IsEnabled (   )

{
   return (Flag_Enabled);
}

 inline int ObjectRecognition::IsInitialized (   )

{
   return (Flag_Initialized);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectRecognitionServer
//
///////////////////////////////////////////////////////////////////////////////

 class ObjectRecognitionServer : public Thread

{
   protected:
      int Flag_Initialized;

   protected:
      struct Request {
         ObjectTracking* ObjectTracker;
         Request* Prev;
         Request* Next;
      };
      Queue<Request>  RequestQueue;
      CriticalSection CS_RequestQueue;

   public:
      MultiEvents Events;

   public:
      ObjectRecognitionServer (   );
      virtual ~ObjectRecognitionServer (   );

   protected:
      virtual void ProcessRequest (ObjectTracking* obj_tracker) = 0;

   protected:
      virtual void OnThreadEnded   (   ) {   };
      virtual int  OnThreadStarted (   ) { return (DONE); };
      virtual void ThreadProc      (   );

   public:
      virtual void    Close           (   );
      virtual ISize2D GetSrcImageSize (   ) = 0;
      virtual int     Initialize      (   );

   public:
      int  AddRequest        (ObjectTracking* obj_tracker);
      int  IsInitialized     (   );
      int  IsRunning         (   );
      void RemoveAllRequests (ObjectTracking* obj_tracker);
      int  Start             (   );
      void Stop              (   );
};

 inline int ObjectRecognitionServer::IsInitialized (   )

{
   return (Flag_Initialized);
}

 inline int ObjectRecognitionServer::IsRunning (   )

{
   return (IsThreadRunning (   ));
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectRecognitionClient
//
///////////////////////////////////////////////////////////////////////////////

 class ObjectRecognitionClient

{
   protected:
      int Flag_Enabled;
      int Flag_Initialized;

   protected:
      float    FrameDuration;
      float    FrameRate;
      ISize2D  SrcImgSize;
      ISize2D  SrcFrmSize;
      FPoint2D Scale;
      byte*    SrcFrmBuffer;

   protected:
      ObjectTracking*          ObjectTracker;
      ObjectRecognitionServer* Server;

   public:
      int      NumTries;
      float    ProcPeriod;
      float    MaxTime_Unrecognized;
      float    OuterZoneSize1;
      float    OuterZoneSize2;
      FPoint2D Inflation;

   public:
      ObjectRecognitionClient (   );
      virtual ~ObjectRecognitionClient (   );

   protected:
      virtual int    CheckObject             (TrackedObject* t_object) { return 0; };
      virtual int*   GetObjectCountVariable  (TrackedObject* t_object) { return nullptr; };
      virtual float* GetObjectTimeVariable   (TrackedObject* t_object) { return nullptr; };
      virtual void   UpdateRecognitionResult (TrackedObject* t_object) {   };

   protected:
      virtual int DoPreprocessing (ObjectTracking& obj_tracker) { return (0); };

   public:
      virtual void Close      (   );
      virtual int  Initialize (   );
      virtual int  Perform    (ObjectTracking& obj_tracker);

   public:
      void Connect       (ObjectRecognitionServer& server);
      void Disconnect    (   );
      void Enable        (int flag_enable);
      int  IsConnected   (   );
      int  IsEnabled     (   );
      int  IsInitialized (   );
};

 inline void ObjectRecognitionClient::Enable (int flag_enable)

{
   Flag_Enabled = flag_enable;
}

 inline int ObjectRecognitionClient::IsEnabled (   )

{
   return (Flag_Enabled);
}

 inline int ObjectRecognitionClient::IsInitialized (   )

{
   return (Flag_Initialized);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: PersonAttrRecognition_DNN_Common
//
///////////////////////////////////////////////////////////////////////////////

// TO_PERSON_ATTR_XXX 정의문과 순서가 같아야 한다.

#define PAR_ATTR_MALE            0
#define PAR_ATTR_CHILD           1
#define PAR_ATTR_BAG             2
#define PAR_ATTR_STRIPE          3
#define PAR_ATTR_SUIT            4
#define PAR_ATTR_TSHIRT          5
#define PAR_ATTR_SHORT_SLEEVES   6
#define PAR_ATTR_LONG_TROUSERS   7
#define PAR_ATTR_JEANS           8
#define PAR_ATTR_SKIRT           9

#if defined(__LIB_CAFFE)

 class PersonAttrRecognition_DNN_Common

{
   protected:
      struct AIDT {
         int   AttrIdx;
         float DThreshold;
      };
      Array1D<AIDT> CI2AI;

   protected:
      DNN_Caffe_ObjectClassifier DNN;

   protected:
      void _Close      (   );
      int  _Initialize (void* h_module,const char* dir_name,int flag_use_gpu,int gpu_no);
};

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: PersonAttrRecognition_DNN
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_CAFFE)

 class PersonAttrRecognition_DNN : public ObjectRecognition, public PersonAttrRecognition_DNN_Common

{
   public:
      PersonAttrRecognition_DNN (   );

   protected:
      virtual int    CheckObject             (TrackedObject* t_object);
      virtual int*   GetObjectCountVariable  (TrackedObject* t_object);
      virtual float* GetObjectTimeVariable   (TrackedObject* t_object);
      virtual void   RecognizeObject         (TrackedObject* t_object);
      virtual void   UpdateRecognitionResult (TrackedObject* t_object);

   public:
      virtual void Close (   );

   public:
      int Initialize (void* h_module,const char* dir_name,int flag_use_gpu = TRUE,int gpu_no = 0);
};

 inline int* PersonAttrRecognition_DNN::GetObjectCountVariable (TrackedObject* t_object)

{
   return (&(t_object->Count_PAR));
}

 inline float* PersonAttrRecognition_DNN::GetObjectTimeVariable (TrackedObject* t_object)

{
   return (&(t_object->Time_PAR));
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: PersonAttrRecognitionServer_DNN
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_CAFFE)

 class PersonAttrRecognitionServer_DNN : public ObjectRecognitionServer, public PersonAttrRecognition_DNN_Common

{
   protected:
      void*   DLLHandle;
      int     GpuNo;
      int     Flag_UseGPU;
      StringA DirName;

   public:
      PersonAttrRecognitionServer_DNN (   );

   protected:
      virtual void OnThreadEnded   (   );
      virtual int  OnThreadStarted (   );
      virtual void ProcessRequest  (ObjectTracking* obj_tracker);

   public:
      virtual void    Close           (   );
      virtual ISize2D GetSrcImageSize (   );

   public:
      int Initialize (void* h_module,const char* dir_name,int flag_use_gpu = FALSE,int gpu_no = 0);
};

 inline ISize2D PersonAttrRecognitionServer_DNN::GetSrcImageSize (   )

{
   return (DNN.SrcImgSize);
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: PersonAttrRecognitionClient
//
///////////////////////////////////////////////////////////////////////////////

 class PersonAttrRecognitionClient : public ObjectRecognitionClient

{
   public:
      PersonAttrRecognitionClient (   );

   protected:
      virtual int    CheckObject             (TrackedObject* t_object);
      virtual int*   GetObjectCountVariable  (TrackedObject* t_object);
      virtual float* GetObjectTimeVariable   (TrackedObject* t_object);
      virtual void   UpdateRecognitionResult (TrackedObject* t_object);

   public:
      int Initialize (   );
};

 inline int* PersonAttrRecognitionClient::GetObjectCountVariable (TrackedObject* t_object)

{
   return (&(t_object->Count_PAR));
}

 inline float* PersonAttrRecognitionClient::GetObjectTimeVariable (TrackedObject* t_object)

{
   return (&(t_object->Time_PAR));
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: VehicleTypeRecognition_DNN_Common
//
///////////////////////////////////////////////////////////////////////////////

// TO_VEHICLE_TYPE_XXX 정의문과 순서가 같아야 한다.

#define VTR_TYPE_UNKNOWN      0
#define VTR_TYPE_SEDAN        1
#define VTR_TYPE_SUV          2
#define VTR_TYPE_VAN          3
#define VTR_TYPE_TRUCK        4
#define VTR_TYPE_BUS          5
#define VTR_TYPE_TWOWHEELER   6

#if defined(__LIB_CAFFE)

 class VehicleTypeRecognition_DNN_Common


{
   protected:
      IArray1D CI2TI;

   protected:
      DNN_Caffe_ObjectClassifier DNN;

   protected:
      void _Close      (   );
      int  _Initialize (void* h_module,const char* dir_name,int flag_use_gpu,int gpu_no);
};

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: VehicleTypeRecognition_DNN
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_CAFFE)

 class VehicleTypeRecognition_DNN : public ObjectRecognition, public VehicleTypeRecognition_DNN_Common

{
   public:
      VehicleTypeRecognition_DNN (   );

   protected:
      virtual int    CheckObject             (TrackedObject* t_object);
      virtual int*   GetObjectCountVariable  (TrackedObject* t_object);
      virtual float* GetObjectTimeVariable   (TrackedObject* t_object);
      virtual void   RecognizeObject         (TrackedObject* t_object);
      virtual void   UpdateRecognitionResult (TrackedObject* t_object);

   public:
      virtual void Close (   );

   public:
      int Initialize (void* h_module,const char* dir_name,int flag_use_gpu = TRUE,int gpu_no = 0);
};

 inline int* VehicleTypeRecognition_DNN::GetObjectCountVariable (TrackedObject* t_object)

{
   return (&(t_object->Count_VTR));
}

 inline float* VehicleTypeRecognition_DNN::GetObjectTimeVariable (TrackedObject* t_object)

{
   return (&(t_object->Time_VTR));
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: VehicleTypeRecognitionServer_DNN
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_CAFFE)

 class VehicleTypeRecognitionServer_DNN : public ObjectRecognitionServer, public VehicleTypeRecognition_DNN_Common

{
   protected:
      void*   DLLHandle;
      int     GpuNo;
      int     Flag_UseGPU;
      StringA DirName;

   public:
      VehicleTypeRecognitionServer_DNN (   );

   protected:
      virtual void OnThreadEnded   (   );
      virtual int  OnThreadStarted (   );
      virtual void ProcessRequest  (ObjectTracking* obj_tracker);

   public:
      virtual void    Close           (   );
      virtual ISize2D GetSrcImageSize (   );

   public:
      int Initialize (void* h_module,const char* dir_name,int flag_use_gpu = FALSE,int gpu_no = 0);
};

 inline ISize2D VehicleTypeRecognitionServer_DNN::GetSrcImageSize (   )

{
   return (DNN.SrcImgSize);
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: VehicleTypeRecognitionClient
//
///////////////////////////////////////////////////////////////////////////////

 class VehicleTypeRecognitionClient : public ObjectRecognitionClient

{
   public:
      VehicleTypeRecognitionClient (   );

   protected:
      virtual int    CheckObject             (TrackedObject* t_object);
      virtual int*   GetObjectCountVariable  (TrackedObject* t_object);
      virtual float* GetObjectTimeVariable   (TrackedObject* t_object);
      virtual void   UpdateRecognitionResult (TrackedObject* t_object);

   public:
      int Initialize (   );
};

 inline int* VehicleTypeRecognitionClient::GetObjectCountVariable (TrackedObject* t_object)

{
   return (&(t_object->Count_VTR));
}

 inline float* VehicleTypeRecognitionClient::GetObjectTimeVariable (TrackedObject* t_object)

{
   return (&(t_object->Time_VTR));
}

}

#endif

#if !defined(__VACL_MISC_H)
#define __VACL_MISC_H
 
#include "vacl_define.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////
 
void CloseVACL                    (   );
int  InitVACL                     (   );
int  GetLicenseKeyAndSerialNumber (const char* file_name,StringA& license_key,StringA& serial_number);
void SetProductInfo               (const char* key,const char* code,const char* version);

}

#endif

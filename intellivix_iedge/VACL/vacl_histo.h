#if !defined(__VACL_HISTO_H)
#define __VACL_HISTO_H

#include "vacl_define.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Definitions
//
///////////////////////////////////////////////////////////////////////////////

#define NUM_COLOR_HISTOGRAM_BINS                  512
#define GET_COLOR_HISTOGRAM_BIN_INDEX_NO(R,G,B)   (((int)((R) >> 5) << 6) | ((int)((G) >> 5) << 3) | ((int)((B) >> 5)))
#define NUM_GRAY_HISTOGRAM_BINS                   16
#define GET_GRAY_HISTOGRAM_BIN_INDEX_NO(I)        ((int)((I) >> 4))

///////////////////////////////////////////////////////////////////////////////
//
// Class: Histogram1D
//
///////////////////////////////////////////////////////////////////////////////

 class Histogram1D : public UIArray1D
 
{
   public:
      int64 Sum;
      
   public:
      Histogram1D (   );
      virtual ~Histogram1D (   ) {   };
   
   public:
      void  Accumulate    (int x,int d = 1);
      void  Clear         (   );
      float GetSimilarity (Histogram1D &s_hist);
      void  Normalize     (FArray1D &d_hist);
};

 inline void Histogram1D::Accumulate (int x,int d)
 
{
   if (Array) {
      Array[x] += d;
      Sum += d;
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: Histogram2D
//
///////////////////////////////////////////////////////////////////////////////

 class Histogram2D : public UIArray2D
 
{
   public:
      int64 Sum;
      
   public:
      Histogram2D (   );
      virtual ~Histogram2D (   ) {   };
   
   public:
      void  Accumulate    (int x,int y,int d = 1);
      void  Clear         (   );
      float GetSimilarity (Histogram2D &s_hist);
      void  Normalize     (FArray2D &d_hist);
};

 inline void Histogram2D::Accumulate (int x,int y,int d)
 
{
   Array[y][x] += d;
   Sum += d;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: Histogram3D
//
///////////////////////////////////////////////////////////////////////////////

 class Histogram3D : public UIArray3D
 
{
   public:
      int64 Sum;

   public:
      Histogram3D (   );
      virtual ~Histogram3D (   ) {   };
   
   public:
      void  Accumulate    (int x,int y,int z,int d = 1);
      void  Clear         (   );
      float GetSimilarity (Histogram3D &s_hist);
      void  Normalize     (FArray3D &d_hist);
};

 inline void Histogram3D::Accumulate (int x,int y,int z,int d)
 
{
   Array[z][y][x] += d;
   Sum += d;
}

///////////////////////////////////////////////////////////////////////////////
//
// HVHistogram
//
///////////////////////////////////////////////////////////////////////////////

#define HVH_NUM_BINS         10
#define HVH_NUM_COLORS       7
#define HVH_NUM_GRAY_LEVLES  (HVH_NUM_BINS - HVH_NUM_COLORS)

#define HVH_INDEX_RED        0
#define HVH_INDEX_ORANGE     1
#define HVH_INDEX_YELLOW     2
#define HVH_INDEX_GREEN      3
#define HVH_INDEX_CYAN       4
#define HVH_INDEX_BLUE       5
#define HVH_INDEX_VIOLET     6
#define HVH_INDEX_BLACK      7
#define HVH_INDEX_GRAY       8
#define HVH_INDEX_WHITE      9

 class HVHistogram
 // 픽셀 색도(Hue)       : 0 ~ 359
 // 픽셀 채도(Saturation): 0 ~ 255
 // 픽셀 명도(Value)     : 0 ~ 255
{
   public:
      Histogram1D HueHistogram;   // 컬러 픽셀로 분류된 경우의 Hue Histogram
      Histogram1D ValueHistogram; // 그레이 픽셀로 분류된 경우의 Value Histogram
      
   public:
      static int GetBinIndexNo          (int ch,int cs,int cv);
      static int GetBinIndexNo          (BGRPixel &s_color);
      static int GetRepresentativeColor (int bin_idx,BGRPixel &d_color);
      static int GetRepresentativeColor (BGRPixel &s_color,BGRPixel &d_color);
      static int IsGrayPixel            (int ch,int cs,int cv);
   
   public:
      void Accumulate (BGRPixel &cp);
      void Clear      (   );
      void Close      (   );
      void Finalize   (float *d_hist); // d_hist[0] ~ d_hist[6]: Hue Histogram
                                       // d_hist[7] ~ d_hist[9]: Value Histogram
      void Initialize (   );
};

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

void  GetColorHistogram      (BGRImage &s_image ,IBox2D &s_rgn,FArray1D &d_hist);
float GetColorSimilarity     (BGRImage &s_cimage,IBox2D &s_rgn,FArray1D &t_hist);
void  GetHistogram           (GImage &s_image,IArray1D &d_array,int flag_clear = TRUE);
float GetHistogramSimilarity (float *s_hist1,float *s_hist2,int length);
float GetHistogramSimilarity (FArray1D &s_hist1,FArray1D &s_hist2);
float GetHistogramSimilarity (FArray2D &s_hist1,FArray2D &s_hist2);
float GetHistogramSimilarity (FArray3D &s_hist1,FArray3D &s_hist2);
void  HistogramEqualization  (GImage &s_image,GImage &d_image);
void  NormalizeHistogram     (FArray1D &d_hist);

}

#endif

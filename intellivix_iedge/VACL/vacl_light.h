#if !defined(__VACL_LIGHT_H)
#define __VACL_LIGHT_H

#include "vacl_define.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Forward Declaration
//
///////////////////////////////////////////////////////////////////////////////

class LightDisturbanceReduction;

///////////////////////////////////////////////////////////////////////////////
//
// Class: LDR_Region
//
///////////////////////////////////////////////////////////////////////////////

#define LDR_RGNTYPE_SHADOW   64
#define LDR_RGNTYPE_LIGHT    128
#define LDR_RGNTYPE_EDGE     192
#define LDR_RGNTYPE_OBJECT   255

 class LDR_Region : public IBox2D
 
{
   friend class LightDisturbanceReduction;

   public:
      int   Flag_Remove; // 제거 여부
      int   Area;        // 영역의 면적
      int   SX,SY;       // 영역 바운더리의 시작점
      int   Type;        // 영역 종류 (LDR_RGNTYPE_XXX 정의문 참조)
      float Score1;
      float Score2;

   public:
      LDR_Region (   );
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: LightDisturbanceReduction
//
///////////////////////////////////////////////////////////////////////////////

 class LightDisturbanceReduction
 
{
   public:
      int   MinArea;
      float MaxAreaFactor;
      float Object_Threshold;
      float Light_MinIR;
      float Light_MaxIR;
      float Light_MaxIRSD;
      float Light_Threshold;
      float Shadow_MinIR;
      float Shadow_MaxIR;
      float Shadow_MaxIRSD;
      float Shadow_Threshold;
      float Edge_Threshold;  
   
   public:
      IArray2D            RegionMap;
      Array1D<LDR_Region> Regions;
      
   public:
      LightDisturbanceReduction (   );
   
   protected:
     void GetPixelIntensityRatios       (GImage &s_image,GImage &fg_image,GImage &bg_image,int win_size,FArray2D &d_array);
     void GetPixelIntensityRatios_3x3   (GImage &s_image,GImage &fg_image,GImage &bg_image,UIArray2D &d_array);
     int  GetRegionMap                  (UIArray2D &ir_array,GImage &rs_image);
     void MarkFrgEdgePixels             (GImage &rs_image,GImage &fe_image);
     void PerformRegionSegmentation     (FArray2D &ir_array,int win_size,float min_ir,float max_ir,float max_irsd,byte s_label,byte t_label,GImage &d_image);
     void PerformRegionSegmentation_3x3 (UIArray2D &ir_array,float min_ir,float max_ir,float max_irsd,byte s_label,byte t_label,GImage &d_image);
     void RemoveMarkedRegions           (GImage &fg_image);

   public:
     void Close   (   );
     void Perform (GImage &s_image,GImage &bg_image,GImage &fe_image,GImage &fg_image);
};

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

void GetFrgEdgeImage                      (GImage& s_image,GImage& bg_image,GImage& fg_image,int e_thrsld,GImage& d_image);
void PerformLightDisturbanceReduction_BES (GImage &e_image,float bes_thrsld,GImage &fg_image);
void PerformLightDisturbanceReduction_IR  (GImage &s_image,GImage &bg_image,int win_size,float min_ir,float max_ir,float max_irsd,GImage &fg_image);
void PerformLightDisturbanceReduction_NCC (GImage &s_image,GImage &bg_image,int win_size,float ncc_thrsld,GImage &fg_image);
void PerformLightDisturbanceReduction_TS  (GImage &s_image,GImage &bg_image,float ts_thrsld,GImage &fg_image);
void PrepareForLDR                        (GImage& s_image,GImage& d_image);

}

#endif

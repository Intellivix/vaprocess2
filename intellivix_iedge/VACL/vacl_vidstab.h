#if !defined(__VACL_VIDSTAB_H)
#define __VACL_VIDSTAB_H

#include "vacl_imgreg.h"

#if defined(__LIB_OPENCV)

 namespace VACL
 
{

////////////////////////////////////////////////////////////////////////////////
// 
// Class: VideoStabilization
//
////////////////////////////////////////////////////////////////////////////////

 class VideoStabilization

{
   protected:
      int      FrameCount;
      FArray2D CurImage;
      FArray2D TmpImage;

   protected:
      OpenCV_PhaseCorrelation PhaseCorrelator;

   public:
      int MaxSrcImgHeight; // Maximum source image height allowed

   public:
      int     SubsmpSize;  // Image subsampling size
      ISize2D VidFrmSize;  // Input video frame size
      ISize2D SrcImgSize;  // Source image size (SrcImgSize = VidFrmSize / SubsmpSize)

   public:
      VideoStabilization (   );
      virtual ~VideoStabilization (   );

   public:
      virtual void Close (   );

   protected:
      void _Init                       (   );
      void ClearFrameBuffer_YUY2       (byte *si_buffer);
      void GetSubsampledGrayImage_YUY2 (byte *si_buffer,FArray2D &d_array);
  
   public:
      void Initialize (ISize2D& frm_size);

   public:
      virtual int  Perform_YUY2 (byte* si_buffer,byte* di_buffer) = 0;
      virtual void Reset      (   );
};

////////////////////////////////////////////////////////////////////////////////
// 
// Class: VideoStabilization_MovingCam
//
////////////////////////////////////////////////////////////////////////////////

 class VideoStabilization_MovingCam : public VideoStabilization
 
{
   protected:
      int         FPQIndex;
      FArray1D    MSFKernel;
      FArray2D    PrvImage;
      FP2DArray1D FrmPosQueue; // Frame position queue
      
   protected:
       struct Frame
      {
         BArray1D Buffer;
         Frame* Prev;
         Frame* Next;
      };
      Queue<Frame> FrameQueue; // Input video frame queue

   public:
      int   MSFKRadius;   // Motion smoothing filter kernel radius
      float MaxShiftSize; // Maximum shift size allowed

   public:
      VideoStabilization_MovingCam (   );
      virtual ~VideoStabilization_MovingCam (   );
   
   protected:
      void _Init                 (   );
      void CreateMSFKernel       (   );
      FPoint2D GetFilteredFrmPos (   );
   
   public:
      virtual void Close (   );

   public:
      void Initialize   (ISize2D& frm_size,int msfk_radius = 15);
         // frm_size   : 입력 비디오 프레임의 크기
         // msfk_radius: Motion Smoothing Filter Kernel의 반지름
         //              본 값이 클수록 영상의 떨림이 줄어드는 반면 영상 출력 딜레이가 커진다.
   public:
      virtual int  Perform_YUY2 (byte* si_buffer,byte* di_buffer);
         // si_buffer: YUY2 포맷의 입력 비디오 프레임 데이터가 저장되어 있는 버퍼의 포인터
         // di_buffer: YUY2 포맷의 출력 비디오 프레임 데이터가 저장되는 버퍼의 포인터
      virtual void Reset        (   );
};

////////////////////////////////////////////////////////////////////////////////
// 
// Class: VideoStabilization_StaticCam
//
////////////////////////////////////////////////////////////////////////////////

 class VideoStabilization_StaticCam : public VideoStabilization

{
   protected:
      FArray2D BkgImage;
   
   public:
      float BkgUpdateRate;
      float BkgUpdatePeriod;
   
   public:
      float FrameRate;

   public:
      VideoStabilization_StaticCam (   );
      virtual ~VideoStabilization_StaticCam (   );

   protected:
      void UpdateBkgImage_YUY2 (byte* si_buffer);
   
   public:
      virtual void Close (   );

   public:
      void Initialize   (ISize2D& frm_size,float frm_rate);

   public:
      virtual int  Perform_YUY2 (byte* si_buffer,byte* di_buffer);
};

}

#endif
#endif

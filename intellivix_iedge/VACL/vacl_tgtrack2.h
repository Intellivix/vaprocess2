#if !defined(__VACL_TGTRACK2_H)
#define __VACL_TGTRACK2_H

#include "vacl_motion.h"
#include "vacl_objfilter.h"
#include "vacl_tgtrack.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: TargetTracking_MT_NCC
//
///////////////////////////////////////////////////////////////////////////////
//
// MT : Motion
// NCC: Normalized Cross Correlation
//

 class TargetTracking_MT_NCC : public TargetTracking
 
{
   protected:
      int Flag_MatchingBlob;
      int FrameCount;
   
   protected:
      MotionDetection_FAD MotionDetector;
   
   public: // Input Parameters
      int     MatchingRange;
      float   MinMotionDensity;
      float   MinRecoveryRange;
      float   RecoveryRangeChange;
      FSize2D MinTargetSizeChange;
      FSize2D MaxTargetSizeChange;
      
   public: // Output Parameters
      int      TargetRecoveryCount;
      GImage   MotionImage;
      GImage   SrcImage;
      GImage   TargetMask;
      GImage   TargetModel;
      FArray1D TargetHistogram;
   
   public:
      TargetTracking_MT_NCC (   );
   
   protected:
      virtual void PerformLostTargetRecovery (BGRImage &s_cimage,GImage &s_image,GImage &m_image) = 0;
   
   protected:
      int      CheckMatchingForegroundBlob (BGRImage &s_cimage,IBox2D &sc_rgn,IBox2D &fb_rgn);
      float    GetAreaRatio                (IBox2D &s_rgn1,IBox2D &s_rgn2);
      IBox2D   GetLostTargetRecoveryRegion (   );
      int      GetMatchingForegroundBlob   (IArray2D &m_map,int n_crs,IBox2D &sc_area,FSize2D &min_tsc,FSize2D &max_tsc,IBox2D &fb_rgn);
      float    GetMotionDensity            (GImage &m_image,IPoint2D &m_pos);
      float    GetNCC                      (GImage &s_image,int ox,int oy,int dx,int dy);
      IPoint2D GetPredictedPosition        (   );
      float    GetSizeRatio                (IBox2D &fb_rgn);
      float    PerformMatching             (GImage &s_image,IPoint2D &s_pos,int m_range,IPoint2D &m_pos);
      void     UpdateMotionMap             (IArray2D &m_map,GImage &m_image,IBox2D &fb_rgn,int fb_no);
      void     UpdateTargetColorHistogram  (BGRImage &s_cimage,IBox2D &s_rgn);
      void     UpdateTargetModel           (GImage &s_image,float tu_rate);
      void     UpdateTargetModel           (GImage &s_image,IArray2D &m_map,IBox2D &fb_rgn,int fb_no);
      void     UpdateTargetRegion          (   );
   
   public:
      virtual void Close         (   );
      virtual void Initialize    (BGRImage &s_cimage,GImage &s_image,IBox2D &s_rgn,float frm_rate,GImage *m_image = NULL);
      virtual void SetTargetSize (ISize2D &size);

   public:
      int   IsMatchingBlobFound (   ) { return (Flag_MatchingBlob); };
      float Perform             (BGRImage &s_cimage,GImage &s_image);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: TargetTracking_MT_NCC1
//
///////////////////////////////////////////////////////////////////////////////

 class TargetTracking_MT_NCC1 : public TargetTracking_MT_NCC
 
{
   public:
      FSize2D MinTargetSizeChange2;
      FSize2D MaxTargetSizeChange2;

   public:
      TargetTracking_MT_NCC1 (   );
   
   protected:
      virtual void PerformLostTargetRecovery (BGRImage &s_cimage,GImage &s_image,GImage &m_image);
      
   public:
      virtual int GetTrackerType (   );
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: TargetTracking_MT_NCC2
//
///////////////////////////////////////////////////////////////////////////////

 class TargetTracking_MT_NCC2 : public TargetTracking_MT_NCC
 
{
   public: // 타겟과 매치가 되는 객체의 조건
      float MinAreaRatio; // "객체면적/타겟면적"의 최소값
      float MaxAreaRatio; // "객체면적/타겟면적"의 최대값
   
   public:
      ObjectIDGeneration       ObjIDGenerator;
      ForegroundDetection_SGBM ForegroundDetector;
      ObjectFiltering          ObjectFilter;
      ObjectTracking           ObjectTracker;
   
   public:
      TargetTracking_MT_NCC2 (   );
      
   protected:
      virtual void PerformLostTargetRecovery (BGRImage &s_cimage,GImage &s_image,GImage &m_image);
   
   protected:
      float GetMatchingScore (BGRImage &s_cimage,TrackedObject *t_object);
   
   public:
      virtual void Close          (   );
      virtual int  GetTrackerType (   );
      virtual void Initialize     (BGRImage &s_cimage,GImage &s_image,IBox2D &s_rgn,float frm_rate,GImage *m_image = NULL);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: TargetTracking_MT_NCC2_MS
//
///////////////////////////////////////////////////////////////////////////////

 class TargetTracking_MT_NCC2_MS : public TargetTracking
 
{
   public:
      float MinMatchingScore2;
      float Weight1,Weight2;
   
   public:
      TargetTracking_MT_NCC2 TargetTracker_MT_NCC2;
      TargetTracking_MS      TargetTracker_MS;
   
   public:
      TargetTracking_MT_NCC2_MS (   );
   
   protected:
      void BlendTargetRegions (IBox2D &s_rgn1,IBox2D &s_rgn2,float alpha,IBox2D &d_rgn);
   
   public:
      virtual void Close          (   );
      virtual int  GetTrackerType (   );
      virtual void SetTargetSize  (ISize2D &size);
   
   public:
      void  Initialize (BGRImage &s_cimage,GImage &s_image,IBox2D &s_rgn,float frm_rate,GImage *m_image = NULL);
      float Perform    (BGRImage &s_cimage,GImage &s_image);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: TargetTracking_ODT
//
///////////////////////////////////////////////////////////////////////////////
//
// ODT: Object Detection & Tracking
//

 class TargetTracking_ODT : public TargetTracking
 
{
   public:
      float MinAreaRatio;
      float MaxAreaRatio;
      float ROIRadius;          // 실제 ROI의 반지름 = ROIRadius * 영상 높이 / 2
   
   public:
      int Flag_MovePTZ;         // 타겟이 ROI를 벗어나는 순간 TRUE가 된다.
                                // PTZ 카메라 제어 모듈은 Flag_MovePTZ 값을 체크하여 TRUE인 경우에 TargetRegion의
                                // 중심이 영상의 중심에 오도록 PTZ 카메라를 재빨리 이동시킨다.
      FArray1D TargetHistogram; // 현재 타겟의 컬러 히스토그램
      
   protected:
      TrackedObject *TargetObject;
   
   public:
      ObjectIDGeneration       ObjIDGenerator;
      ForegroundDetection_SGBM ForegroundDetector;
      ObjectTracking           ObjectTracker;
      ObjectFiltering          ObjectFilter;
   
   public:
      TargetTracking_ODT (   );
   
   protected:
      float GetMatchingScore (BGRImage &s_cimage,TrackedObject *t_object);
   
   public:
      virtual void Close          (   );
      virtual int  GetTrackerType (   );
      virtual void SetTargetSize  (ISize2D &size);

   public:
      void Initialize (BGRImage &s_cimage,IBox2D & tg_rgn,float frm_rate);
      int  Perform    (BGRImage &s_cimage,GImage &s_image);
};

}

#endif

#include "vacl_camcalib.h"
#include "vacl_event.h"
#include "vacl_realsize.h"
 
 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: RealObjectSIzeEstimation
//
///////////////////////////////////////////////////////////////////////////////

 RealObjectSizeEstimation::RealObjectSizeEstimation (   )
 
{
   Flag_Enabled       = TRUE;
   Options            = 0;
   UpdateRate         = 0.2f;
   TiltAngle          = 20.0f;
   DistanceFromGround = 6.0f;
   RefImgSize (REF_IMAGE_WIDTH,REF_IMAGE_HEIGHT);
   Reset (   );
}

 int RealObjectSizeEstimation::CalibrateCamera (   )

{
   int i;

   if (CalibObjects.Length < 3) return (1);
   vector<double>   rh_array(CalibObjects.Length);
   vector<DPoint2D> tp_array(CalibObjects.Length);
   vector<DPoint2D> bp_array(CalibObjects.Length);
   for (i = 0; i < CalibObjects.Length; i++) {
      rh_array[i]   = (double)CalibObjects[i].RealHeight;
      tp_array[i].X = (double)CalibObjects[i].TopPos.X;
      tp_array[i].Y = (double)CalibObjects[i].TopPos.Y;
      bp_array[i].X = (double)CalibObjects[i].BottomPos.X;
      bp_array[i].Y = (double)CalibObjects[i].BottomPos.Y;
   }
   CamAutoCalibByRefObjs calibrator(RefImgSize.Width,RefImgSize.Height,tp_array,bp_array,rh_array);
   double focal_length,tilt_angle,cam_height;
   calibrator.calibrateFTH (focal_length,tilt_angle,cam_height);
   #if defined(__DEBUG_ROSE)
   logd ("[Calibration Result]\n");
   logd ("- Resolution   : %d x %d\n",RefImgSize.Width,RefImgSize.Height);
   logd ("- # of Objects : %d\n",CalibObjects.Length);
   logd ("- Focal Length : %f\n",focal_length);
   logd ("- Tilt Angle   : %f\n",tilt_angle);
   logd ("- Camera Height: %f\n",cam_height);
   #endif
   FocalLength        = (float)focal_length;
   TiltAngle          = (float)tilt_angle;
   DistanceFromGround = (float)cam_height;
   return (DONE);
}

 int RealObjectSizeEstimation::CheckSetup (RealObjectSizeEstimation* rose)

{
   int ret_flag = 0;
   FileIO buffThis,buffFrom;   
   CXMLIO xmlIOThis,xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis,XMLIO_Write);
   xmlIOFrom.SetFileIO (&buffFrom,XMLIO_Write);
   WriteFile (&xmlIOThis);
   rose->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom)) ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength (0); buffFrom.SetLength (0);
   if (ret_flag & CHECK_SETUP_RESULT_CHANGED) ret_flag |= CHECK_SETUP_RESULT_RESTART;
   return (ret_flag);
}

 int RealObjectSizeEstimation::GetFeetPosAndBodySize (IPoint2D& hd_pos,ISize2D& si_size,float hm_height,IPoint2D& ft_pos,int& hd_size,ISize2D& bd_size)

{
   if (!IsCalibrated (   )) return (1);
   FPoint2D scale;
   scale.X = (float)RefImgSize.Width  / si_size.Width;
   scale.Y = (float)RefImgSize.Height / si_size.Height;
   FPoint2D i_hp; // 이미지 상에서의 헤드 위치
   i_hp.X = hd_pos.X * scale.X;
   i_hp.Y = hd_pos.Y * scale.Y;
   Matrix vec_Ph = Mat_H * GetHomogeneousVector (i_hp);
   FPoint2D g_hp; // 그라운드 평면 상에서의 헤드 위치
   GetInhomogeneousCoordinates (vec_Ph,g_hp);
   float l1 = (float)sqrt (g_hp.X * g_hp.X + g_hp.Y * g_hp.Y);
   float l2 = hm_height * l1 / DistanceFromGround;
   float t  = (l1 - l2) / l1;
   FPoint2D g_fp = t * g_hp; // 그라운드 평면 상에서의 풋 위치
   Matrix vec_Pf = Mat_IH * GetHomogeneousVector (g_fp);
   FPoint2D i_fp;
   GetInhomogeneousCoordinates (vec_Pf,i_fp);
   ft_pos.X = (int)(i_fp.X / scale.X);
   ft_pos.Y = (int)(i_fp.Y / scale.Y);
   Matrix vec_o(3);
   vec_o(0) = (double)g_fp.X, vec_o(1) = (double)g_fp.Y, vec_o(2) = 0.0;
   Matrix vec_a(3);
   vec_a(0) = (double)(g_hp.X - g_fp.X), vec_a(1) = (double)(g_hp.Y - g_fp.Y), vec_a(2) = 0.0;
   Matrix vec_b(3);
   vec_b(0) = 0.0, vec_b(1) = 0.0, vec_b(2) = 1.0;
   Matrix vec_c  = 0.2 * Nor(CP(vec_a,vec_b)); // 어깨 너비의 반 // PARAMETERS
   Matrix vec_p1 = vec_o + vec_c;
   Matrix vec_p2 = vec_o - vec_c;
   FPoint2D p1,p2;
   GetInhomogeneousCoordinates (Mat_IH * vec_p1,p1);
   GetInhomogeneousCoordinates (Mat_IH * vec_p2,p2);
   p1.X /= scale.X, p1.Y /= scale.Y;
   p2.X /= scale.X, p2.Y /= scale.Y;
   float bd_width = Mag(p1 - p2);
   bd_size.Width  = (int)(bd_width + 0.5f);
   bd_size.Height = (int)(Mag(ft_pos - hd_pos) + 0.5f);
   hd_size        = (int)(bd_width / 2.0f + 0.5f); // 머리 크기 // PARAMETERS
   return (DONE);
}

 int RealObjectSizeEstimation::GetHomography (   )
 
{
   if (!IsCalibrated (   )) return (1);
   float a = MC_DEG2RAD * TiltAngle;
   float s = (float)sin (a);
   float c = (float)cos (a);
   Matrix mat_A(3,3);
   mat_A.Clear (   );
   mat_A[0][0] = DistanceFromGround;
   mat_A[1][1] = DistanceFromGround * s;
   mat_A[1][2] = DistanceFromGround * c;
   mat_A[2][1] = -c;
   mat_A[2][2] =  s;
   Matrix mat_K(3,3);
   mat_K.Clear (   );
   mat_K[0][0] = FocalLength;
   mat_K[0][2] = 0.5 * RefImgSize.Width;
   mat_K[1][1] = -FocalLength;
   mat_K[1][2] = 0.5 * RefImgSize.Height;
   mat_K[2][2] = 1.0;
   Mat_IK = Inv(mat_K);
   Mat_H  = mat_A * Mat_IK;
   Mat_IH = Inv(Mat_H);
   return (DONE);
}
 
 float RealObjectSizeEstimation::GetRealHeight (FPoint2D &bc_point,float o_height,FPoint2D &r_pos)
 
{
   if (!IsCalibrated (   )) return (0.0f);
   FPoint2D tc_point(bc_point.X,bc_point.Y - o_height);
   Matrix vec_bc = Mat_IK * GetHomogeneousVector (bc_point);
   Matrix vec_tc = Mat_IK * GetHomogeneousVector (tc_point);
   float d    = Mag(r_pos);
   float phi0 = (float)atan (d / DistanceFromGround);
   float phi1 = (float)acos (IP(vec_bc,vec_tc) / (Mag(vec_bc) * Mag(vec_tc)));
   float h    = DistanceFromGround + d * tan (phi0 + phi1 - MC_PI_2);
   return (h);
}

 FPoint2D RealObjectSizeEstimation::GetRealPosition (FPoint2D &bc_point)
 
{
   if (!IsCalibrated (   )) return (FPoint2D(0.0f,0.0f));
   Matrix vec_rp = Mat_H * GetHomogeneousVector (bc_point);
   FPoint2D r_pos;
   GetInhomogeneousCoordinates (vec_rp,r_pos);
   return (r_pos);
}

 int RealObjectSizeEstimation::GetRealSizeAndPos (IBox2D &s_rgn,ISize2D &si_size,FSize2D &d_size,FPoint2D &d_pos)
 
{
   d_size.Clear (   );
   d_pos.Clear  (   );
   if (!IsCalibrated (   )) return (1);
   FPoint2D scale;
   scale.X = (float)RefImgSize.Width  / si_size.Width;
   scale.Y = (float)RefImgSize.Height / si_size.Height;
   FPoint2D bc_point;
   bc_point.X = scale.X * (s_rgn.X + 0.5f * s_rgn.Width);
   bc_point.Y = scale.Y * (s_rgn.Y + s_rgn.Height - 1);
   d_pos = GetRealPosition (bc_point);
   d_size.Height = GetRealHeight (bc_point,s_rgn.Height * scale.Y,d_pos);
   d_size.Width  = d_size.Height * s_rgn.Width / s_rgn.Height;
   return (DONE);
}

 int RealObjectSizeEstimation::Initialize (ISize2D& ri_size)
 
{
   if (!IsEnabled (   )) return (1);
   RefImgSize = ri_size;
   GetHomography (   );
   return (DONE);
}

 int RealObjectSizeEstimation::IsCalibrated (   )

{
   if (!FocalLength || !DistanceFromGround || !RefImgSize.Width || !RefImgSize.Height) return (FALSE);
   return (TRUE);
}

 int RealObjectSizeEstimation::Perform (ObjectTracking &obj_tracker)

{
   if (!IsEnabled (   )) return (1);
   if (!IsCalibrated (   )) return (2);
   float a  = 1.0f - UpdateRate;
   float b  = UpdateRate;
   float fr = 3.6f * obj_tracker.FrameRate;
   TrackedObject *t_object = obj_tracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED)) {
         FSize2D  r_size;
         FPoint2D r_pos;
         if (GetRealSizeAndPos (*t_object,obj_tracker.SrcImgSize,r_size,r_pos) == DONE) {
            float r_mja_len = t_object->MajorAxisLength * r_size.Height / t_object->Height;
            float r_mna_len = t_object->MinorAxisLength * r_size.Height / t_object->Height;
            float r_area    = 0.25f * MC_PI * r_mja_len * r_mna_len;
            if (t_object->RealHeight == 0.0f) {
               t_object->RealArea            = r_area;
               t_object->RealWidth           = r_size.Width;
               t_object->RealHeight          = r_size.Height;
               t_object->RealMajorAxisLength = r_mja_len;
               t_object->RealMinorAxisLength = r_mna_len;
            }
            else {
               t_object->RealArea            = a * t_object->RealArea            + b * r_area;
               t_object->RealWidth           = a * t_object->RealWidth           + b * r_size.Width;
               t_object->RealHeight          = a * t_object->RealHeight          + b * r_size.Height;
               t_object->RealMajorAxisLength = a * t_object->RealMajorAxisLength + b * r_mja_len;
               t_object->RealMinorAxisLength = a * t_object->RealMinorAxisLength + b * r_mna_len;
               float r_speed = Mag(r_pos - t_object->RealPos) * fr;
               if (t_object->RealSpeed < 0.0f) t_object->RealSpeed = r_speed;
               else t_object->RealSpeed = a * t_object->RealSpeed + b * r_speed;
            }
            t_object->RealDistance = Mag(r_pos);
            t_object->RealPos      = r_pos;
         }
         else {
            t_object->RealArea            = 0.0f;
            t_object->RealWidth           = 0.0f;
            t_object->RealHeight          = 0.0f;
            t_object->RealMajorAxisLength = 0.0f;
            t_object->RealMinorAxisLength = 0.0f;
            t_object->RealSpeed           = -1.0f;
         }
      }
      t_object = obj_tracker.TrackedObjects.Next (t_object);
   }
   return (DONE);
}

 int RealObjectSizeEstimation::ReadFile (FileIO &file)
 
{
   if (file.Read ((byte *)&TiltAngle,1,sizeof(float))) return (1);
   if (file.Read ((byte *)&DistanceFromGround,1,sizeof(float))) return (2);
   if (file.Read ((byte *)&FocalLength,1,sizeof(float))) return (3);
   if (file.Read ((byte *)&RefImgSize,1,sizeof(ISize2D))) return (4);
   if (file.Read ((byte *)(float*)RealHeights,NUM_REF_OBJECTS,sizeof(float))) return (5);
   if (file.Read ((byte *)(ILine2D*)Objects,NUM_REF_OBJECTS,sizeof(ILine2D))) return (6);
   return (DONE);
}

 int RealObjectSizeEstimation::ReadFile (CXMLIO* pIO)
 
{
   static RealObjectSizeEstimation dv; // jun : defalut value (기본값과 같으면 xml로 부터 읽거나 저장하지 않음. 
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("RealObjectSizeEstimation"))
   {
      xmlNode.Attribute (TYPE_ID (float)  ,"TiltAngle"         ,&TiltAngle          ,&dv.TiltAngle);
      xmlNode.Attribute (TYPE_ID (float)  ,"FocalLength"       ,&FocalLength        ,&dv.FocalLength);
      xmlNode.Attribute (TYPE_ID (float)  ,"DistanceFromGround",&DistanceFromGround ,&dv.DistanceFromGround);
      xmlNode.Attribute (TYPE_ID (ISize2D),"RefImgSize"        ,&RefImgSize         ,&dv.RefImgSize);
      xmlNode.Attribute (TYPE_ID (float)  ,"RealHeights"       ,(float *)RealHeights,(float*)dv.RealHeights,NUM_REF_OBJECTS);
      xmlNode.Attribute (TYPE_ID (ILine2D),"Objects"           ,(ILine2D *)Objects  ,(ILine2D*)dv.Objects  ,NUM_REF_OBJECTS);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

 void RealObjectSizeEstimation::Reset (   )
 
{
   FocalLength = 0.0f;
   memset (Objects,0,sizeof(Objects));
   memset (RealHeights,0,sizeof(RealHeights));
}

 int RealObjectSizeEstimation::UpdateFocalLength (   )

{
   if (Objects[0][0].Y == Objects[0][1].Y) {
      FocalLength = 0.0f;
      return (1);
   }
   float cy = 0.5f * RefImgSize.Height;
   float t  = (float)tan (MC_DEG2RAD * TiltAngle);
   float v0 = Objects[0][1].Y - cy;
   float v1 = Objects[0][0].Y - cy;
   float y  = RealHeights[0];
   float a  = 2.0f * y * t;
   float b  = y * (v0 - v1 * t * t) - DistanceFromGround * (v0 - v1) * (1 + t * t);
   float c  = -y * v0 * v1 * t;
   float d  = (float)sqrt (b * b - 2.0f * a * c);
   float f1 = (-b + d) / a;
   float f2 = (-b - d) / a;
   if (f1 > f2) FocalLength = f1;
   else FocalLength = f2;
   GetHomography (   );
   return (DONE);
}

 int RealObjectSizeEstimation::UpdateRefObjectHeights (   )
 
{
   int i;
   
   if (!IsCalibrated (   )) return (1);
   for (i = 1; i < NUM_REF_OBJECTS; i++) {
      if (Objects[i][0].Y < Objects[i][1].Y) {
         FPoint2D bc_point((float)Objects[i][1].X,(float)Objects[i][1].Y);
         float o_height = (float)(Objects[i][1].Y - Objects[i][0].Y);
         FPoint2D r_pos = GetRealPosition (bc_point);
         RealHeights[i] = GetRealHeight (bc_point,o_height,r_pos);
      }
      else RealHeights[i] = 0.0f;
   }
   return (DONE);
}

 int RealObjectSizeEstimation::WriteFile (FileIO &file)
 
{
   if (file.Write ((byte *)&TiltAngle,1,sizeof(float))) return (1);
   if (file.Write ((byte *)&DistanceFromGround,1,sizeof(float))) return (2);
   if (file.Write ((byte *)&FocalLength,1,sizeof(float))) return (3);
   if (file.Write ((byte *)&RefImgSize,1,sizeof(ISize2D))) return (4);
   if (file.Write ((byte *)(float*)RealHeights,NUM_REF_OBJECTS,sizeof(float))) return (5);
   if (file.Write ((byte *)(ILine2D*)Objects,NUM_REF_OBJECTS,sizeof(ILine2D))) return (6);
   return (DONE);
}

 int RealObjectSizeEstimation::WriteFile (CXMLIO* pIO)
 
{
   static RealObjectSizeEstimation dv;
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("RealObjectSizeEstimation"))
   {
      xmlNode.Attribute (TYPE_ID (float)  , "TiltAngle"         ,&TiltAngle         ,&dv.TiltAngle);
      xmlNode.Attribute (TYPE_ID (float)  , "FocalLength"       ,&FocalLength       ,&dv.FocalLength);
      xmlNode.Attribute (TYPE_ID (float)  , "DistanceFromGround",&DistanceFromGround,&dv.DistanceFromGround);
      xmlNode.Attribute (TYPE_ID (ISize2D), "RefImgSize"        ,&RefImgSize        ,&dv.RefImgSize);
      xmlNode.Attribute (TYPE_ID (float)  , "RealHeights"       ,(float*)RealHeights,(float*)dv.RealHeights,NUM_REF_OBJECTS);
      xmlNode.Attribute (TYPE_ID (ILine2D), "Objects"           ,(ILine2D*)Objects  ,(ILine2D*)dv.Objects  ,NUM_REF_OBJECTS);
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}

}

#if !defined(__VACL_GLOBJTRK_H)
#define __VACL_GLOBJTRK_H

#include "vacl_event.h"
#include "vacl_objtrack.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: LocalView
//
///////////////////////////////////////////////////////////////////////////////
//
// 로컬 뷰와 글로벌 뷰 사이의 좌표 변환 관계를 가진다.
//

 class LocalView

{
   public:
      int             ID;
      Matrix          Mat_H;         // Homography: Local View => Global View
      Matrix          Mat_IH;        // Inverse Homography: Global View => Local View
      ISize2D         VidFrmSize;    // Video Frame Size
      ObjectTracking* ObjectTracker; // Local Object Tracker
      
   public:
      LocalView* Prev;
      LocalView* Next;
      
   public:
      LocalView (   );
      
   public:
      void     Initialize       (ISize2D& vf_size,Matrix& mat_H,ObjectTracking& obj_tracker);
      FPoint2D GetGlobalViewPos (FPoint2D& s_pos);         // 입력 영상 해상도 기준
      FPoint2D GetGlobalViewPos (TrackedObject* t_object); // 분석 영상 해상도 기준
};

typedef LinkedList<LocalView> LocalViewList;

///////////////////////////////////////////////////////////////////////////////
//
// Class: LocalTrackedObject
//
///////////////////////////////////////////////////////////////////////////////
//
// 로컬 뷰에서의 객체 추적 정보를 가진다.
//

 class LocalTrackedObject
 
{
   public:
      int            BirthCount;
      float          Time_Unstable;
      LocalView*     View;
      TrackedObject* TrkObject;
      FPoint2D       GlobalPos;
      
   public:
      LocalTrackedObject* Prev;
      LocalTrackedObject* Next;
      
   public:
      LocalTrackedObject (   );
      
   public:
      void  Initialize          (LocalView* l_view,TrackedObject* t_object,int b_count);
      float GetWeight           (   );
      void  UpdateGlobalViewPos (   );
};

typedef LinkedList<LocalTrackedObject> LocalTrackedObjectList;

///////////////////////////////////////////////////////////////////////////////
//
// Class: GlobalTrackedObject
//
///////////////////////////////////////////////////////////////////////////////
//
// 글로벌 뷰에서의 객체 추적 정보를 가진다.
//

#define GTO_STATUS_TO_BE_REMOVED   0x00000001
#define GTO_STATUS_VERIFIED        0x00000002

 class GlobalTrackedObject

{
   public:
      int   ID;
      int   PlayerID;
      int   BirthCount;
      float Radius;
   
   public:
      int   Status;
      int   Count_Tracked;
      float MaxMovingDistance;
      float Time_Lost;
      float Time_Tracked;
      float Time_Unstable;

   public:
      FPoint2D InitPos;
      FPoint2D Pos;
      FPoint2D PredPos;
      FPoint2D Velocity;

   public:
      FPoint2D Pos2;
      FPoint2D PredPos2;
      FPoint2D Velocity2;
   
   public:
      LocalTrackedObjectList LTOs;
   
   public:
      GlobalTrackedObject* Prev;
      GlobalTrackedObject* Next;
      
   public:
      GlobalTrackedObject (   );

   public:
      LocalTrackedObject* FindLTO (LocalView* l_view);
      LocalTrackedObject* FindLTO (TrackedObject* t_object);
      float GetDistance           (LocalTrackedObject* lt_object);
      float GetMaxMovingDistance  (   );
      void  GetPredictedPos       (   );
      void  Initialize            (LocalView* l_view,TrackedObject* t_object,float radius,int b_count);
      void  UpdatePos             (   );
};

typedef LinkedList<GlobalTrackedObject> GlobalTrackedObjectList;

///////////////////////////////////////////////////////////////////////////////
//
// Class: GlobalObjectTracking
//
///////////////////////////////////////////////////////////////////////////////

 class GlobalObjectTracking
 
{
   protected:
      int ObjectCount;
      int LocalViewCount;

   public:
      int    MinNumLTOs;
      float  ObjectRadius;
      float  MDThreshold;
      float  NRThreshold;
      float  MaxRcvDistance;
      float  MaxTime_Lost;
      float  MinTime_Tracked;
      float  MaxTime_Unstable;
      float  MaxTime_LTOUnstable;
      FBox2D TrackingArea;
   
   public:
      float FrameRate;
      float FrameDuration;

   public:
      int Flag_Break;
      int FrameCount;

   public:
      LocalViewList           LocalViews;
      GlobalTrackedObjectList TrackedObjects;
      GlobalTrackedObjectList LostObjects;

   public:
      GlobalObjectTracking (   );
      virtual ~GlobalObjectTracking (   );
   
   protected:
      void _Init (   );
      GlobalTrackedObject* FindClosestGTO   (LocalView* l_view,TrackedObject* t_object,float& distance);
      GlobalTrackedObject* FindClosestGTO   (LocalTrackedObject* lt_object,float& distance);
      GlobalTrackedObject* FindClosestLGTO  (GlobalTrackedObject* s_gt_object,float& distance);
      GlobalTrackedObject* FindOwningGTO    (TrackedObject* t_object);
      GlobalTrackedObject* FindMatchingLGTO (GlobalTrackedObject* s_gt_object);
   
   public:
      virtual void Close (   );

   public:
      void AddLocalView (ISize2D& vf_size,Matrix &mat_H,ObjectTracking& obj_tracker);
      void Initialize   (float frm_rate);
      void Perform      (   );
};

}

#endif

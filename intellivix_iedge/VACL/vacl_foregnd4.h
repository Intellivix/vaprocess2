#if !defined(__VACL_FOREGND3_H)
#define __VACL_FOREGND3_H

#include "vacl_dnn.h"
#include "vacl_face.h"
#include "vacl_foregnd.h"
#include "vacl_objdetec.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: ForegroundDetection_FCD_Seeta
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_SEETA)

 class ForegroundDetection_FCD_Seeta : public ForegroundDetection

{
   public:
      FaceDetection_Seeta FaceDetector;

   public:
      ForegroundDetection_FCD_Seeta (   );
   
   protected:
      virtual int  GetFrgImage     (   );
      virtual void GetFrgRegionMap (   ) {   };

   public:
      int InitFaceDetector (const char* dir_name);
};

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: ForegroundDetection_FCD_OpenVINO
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_OPENVINO)

 class ForegroundDetection_FCD_OpenVINO : public ForegroundDetection

{
   public:
      FaceDetection_OpenVINO FaceDetector;

   public:
      ForegroundDetection_FCD_OpenVINO (   );

   protected:
      virtual int  GetFrgImage     (   );
      virtual void GetFrgRegionMap (   ) {   };

   public:
      int InitFaceDetector (const char* dir_name,int flag_advanced = FALSE,int flag_use_gpu = FALSE);
}; 

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: ForegroundDetection_OBD_YOLO
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_DARKNET)

 class ForegroundDetection_OBD_YOLO : public ForegroundDetection

{
   public:
      ObjectDetection_YOLO ObjectDetector;

   public:
      ForegroundDetection_OBD_YOLO (   );

   protected:
      virtual int  GetFrgImage     (   );
      virtual void GetFrgRegionMap (   ) {   };
   
   public:
      int InitObjectDetector (void* dll_handle,const char* dir_name,int gpu_idx);
};

 inline int ForegroundDetection_OBD_YOLO::InitObjectDetector (void* dll_handle,const char* dir_name,int gpu_idx)

{
   return (ObjectDetector.Initialize (dll_handle,dir_name,gpu_idx));
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: ForegroundDetectionClient_OBD_YOLO
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_DARKNET)

 class ForegroundDetectionClient_OBD_YOLO : public ForegroundDetectionClient

{
   public:
      ForegroundDetectionClient_OBD_YOLO (   );

   protected:
      virtual int  GetFrgImage     (   );
      virtual void GetFrgRegionMap (   ) {   };
};

#endif

}

#endif

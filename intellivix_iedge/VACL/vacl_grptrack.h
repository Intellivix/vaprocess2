#if !defined(__VACL_GRPTRACK_H)
#define __VACL_GRPTRACK_H

#include "vacl_objtrack.h"
#include "vacl_opencv.h"

 namespace VACL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Class: TrackedGroupRegion
//
///////////////////////////////////////////////////////////////////////////////

 class TrackedGroupRegion : public TrkBlobRegion
 
{
   public:
      int   MDAP;         // Maximum Distance Among Persons
      float BoxActivity;  // Box Activity
      float FlowActivity; // Flow Activity
      float FlowVariance; // Flow Variance
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: TrackedGroup
//
///////////////////////////////////////////////////////////////////////////////

#define TG_STATUS_VIOLENCE   0x00010000

 class TrackedGroup : public TrackedBlob
 
{
   public:
      int   MDAP;            // 그룹 내에 있는 사람들 간의 최대 거리 (Maximum Distance Among Persons)
      int   NumPersons;      // 그룹 내에 있는 현재 사람 수
      float BoxActivity;     //
      float FlowActivity;    //
      float FlowVariance;    //
      float MaxBoxActivity;  //
      float MaxFlowActivity; //
      float MaxFlowVariance; //
      float Time_Gathering;  // 최초 군집이 감지된 후의 경과 시간
      float Time_Violence;   // 최초 폭력이 감지된 후의 경과 시간
 
   public:
      Array1D<TrackedObject*> Members; // 본 그룹을 구성하는 멤버 목록
      
   public:
      TrackedGroup (   );
      virtual ~TrackedGroup (   ) {   };
      
   protected:
      float GetActivityScore (FArray1D& s_array,float duration,float gf_size,float d_thrsld);
   
   public:
      IBox2D GetBoundingBox    (   );
      void   GetBoxActivity    (float frm_rate,float period);
      void   GetPropertyValues (float frm_rate,float period);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: GroupTracking
//
///////////////////////////////////////////////////////////////////////////////

#define GRT_OPTION_TRACK_FEATURE_POINTS_OF_GROUP_MEMBERS   0x00000001

 class GroupTracking : public BlobTracking
 
{
   // Input Parameters
   public:
      int   Options;
      float ActivityPeriod;
   
   // Output Parameters
   public:
      GImage GroupMaskImage;

   public:
      GroupTracking (   );
      virtual ~GroupTracking (   );
   
   protected:
      virtual TrkBlobRegion* AddToTrkBlobRegionList (TrackedBlob* t_blob);
      virtual TrackedBlob*   CreateTrackedBlob      (   );
      virtual TrkBlobRegion* CreateTrkBlobRegion    (   );
      virtual int            VerifyTrackedBlob      (TrackedBlob* t_blob);
   
   protected:
      void GetGroupMaskImage    (ObjectTracking& obj_tracker,GImage& d_image);
      void GetGroupMembers      (ObjectTracking& obj_tracker);
      void PutMask              (TrackedObject* t_object,GImage& d_image);
      void SetGroupMemberStatus (ObjectTracking& obj_tracker);
   
   public:
      virtual void Close (   );

   public:
      int  Initialize (ISize2D& si_size,float frm_rate);
      void InitParams (EventDetection& evt_detector);
      int  Perform    (ObjectTracking& obj_tracker);
      void Reset      (   );
};

}

#endif

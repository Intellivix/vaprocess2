#include "vacl_bsttree.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: BoostedTree
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_ML)

#define SIG_BOOSTED_TREE   MAKEFOURCC('B','S','T','R')

 BoostedTree::BoostedTree (   )

{
   DecisionTreeFactory   = NULL;
   WeakClassifierFactory = NULL;
   AdaBoostClassifier    = NULL;

   SCDThreshold          = 0.5;
   _Init (   );
}

 BoostedTree::~BoostedTree (   )

{
   Close (   );
}

 void BoostedTree::_Init (   )

{
   MaxTreeDepth = 0;
   NumWCs       = 0;
   NumFeatures  = 0;
   NumSamples   = 0;
   HCRThreshold = 0.5f;
}

 void BoostedTree::Close (   )

{
   if (AdaBoostClassifier) {
      delete AdaBoostClassifier;
      AdaBoostClassifier = NULL;
   }
   if (WeakClassifierFactory) {
      delete WeakClassifierFactory;
      WeakClassifierFactory = NULL;
   }
   if (DecisionTreeFactory) {
      delete DecisionTreeFactory;
      DecisionTreeFactory = NULL;
   }
   SCRThresholds.Delete (   );
   _Init (   );
}

 int BoostedTree::Classify (float* s_array,int length,double rt_offset,double& score)

{
    score = Evaluate (s_array,length);
   if (score >= (SCDThreshold + rt_offset)) return (1);
   else return (-1);
}

 int BoostedTree::Classify_HC (float* s_array,int length,double rt_offset,double& score)

{
   score = Evaluate (s_array,length);
   if (score > (HCRThreshold + rt_offset)) return (1);
   else return (-1);
}

 int BoostedTree::Classify_SC (float* s_array,int length,double rt_offset,double& score)

{
   if (AdaBoostClassifier == NULL) return (0);
   FeatureVector s_vector(s_array,length);
   UINT c_label;
   AdaBoostClassifier->Evaluate_SC (s_vector,SCRThresholds,rt_offset,c_label,score);
   if (c_label == 0) return (1);
   else return (-1);
}

 int BoostedTree::CreateClassifier (int max_td,int n_wcs,int n_samples)

{
   if (!max_td || !n_wcs || !n_samples) return (1);
   if (AdaBoostClassifier    != NULL) delete AdaBoostClassifier;
   if (WeakClassifierFactory != NULL) delete WeakClassifierFactory;
   if (DecisionTreeFactory   != NULL) delete DecisionTreeFactory;
   DecisionTreeFactory   = new ML::MulticlassClassifierFactoryDecisionTree<FeatureVector>(ML::DecisionTreeConfiguration(MaxTreeDepth));
   WeakClassifierFactory = new ML::BinaryClassifierFactoryMulticlass<FeatureVector>(DecisionTreeFactory);
   AdaBoostClassifier    = new ML::BinaryClassifierAdaBoost<FeatureVector>(ML::AdaBoostConfiguration<FeatureVector>(NumWCs,NumSamples,true,1.0,WeakClassifierFactory));
   return (DONE);
}

 double BoostedTree::Evaluate (float* s_array,int length)

{
   if (AdaBoostClassifier == NULL) return (0.0);
   FeatureVector s_vector(s_array,length);
   return (AdaBoostClassifier->Evaluate (s_vector));
}

 double BoostedTree::Evaluate (float* s_array,int length,int s_wc_idx,int e_wc_idx)

{
   if (AdaBoostClassifier == NULL) return (0.0);
   FeatureVector s_vector(s_array,length);
   return (AdaBoostClassifier->Evaluate (s_vector,s_wc_idx,e_wc_idx));
}

 int BoostedTree::GetNumWCs (   )

{
   if (AdaBoostClassifier == NULL) return (0);
   return (AdaBoostClassifier->GetNumWCs (   ));
}

 int BoostedTree::GetHCRThreshold (FArray2D& fv_array,FArray1D& cl_array,int n_samples)

{
   int i;
   
   HCRThreshold = 0.0f;
   if (!IsTrained (   )) return (1);
   double min_sc = MC_VLV;
   for (i = 0; i < n_samples; i++) {
      if (cl_array[i] > 0.0f) {
         double score = Evaluate (fv_array[i],fv_array.Width);
         if (score < min_sc) min_sc = score;
      }
   }
   if (min_sc == MC_VLV) return (2);
   HCRThreshold = min_sc;
   return (DONE);
}

 int BoostedTree::GetSCRThresholds (FArray2D& fv_array,FArray1D& cl_array,int n_samples)

{
   int    i,j;
   double score;
   
   logd ("   Calculating the stage rejection thresholds...\n");
   int n_wcs = GetNumWCs (   );
   if (!n_wcs) return (1);
   SCRThresholds.Create (n_wcs);
   SCRThresholds.Clear  (   );
   BArray1D ps_array(n_samples);
   ps_array.Clear (   );
   // Pos 샘플들 중에 Strong Classifier를 통해 Positive로 분류되는 샘플들만 사용한다.
   for (i = 0; i < n_samples; i++) {
      score = Evaluate (fv_array[i],fv_array.Width);
      if (cl_array[i] > 0.0f && score >= SCDThreshold) { // Pos/Neg를 구분하는 기준 값이 0.5임에 주의!
         ps_array[i] = TRUE;
      }
   }
   DArray1D sc_array(n_samples);
   sc_array.Clear (   );
   for (i = 0; i < n_wcs; i++) {
       // Pos 샘플들에 대해 i 번째 WC의 Prediction 값을 누적시킨다.
      for (j = 0; j < n_samples; j++)
         if (ps_array[j]) sc_array[j] += Evaluate (fv_array[j],fv_array.Width,i,i);
      // Pos 샘플들에 대해 최소 누적 스코어 값을 찾아, i 번째 스테이지에 대한 Rejection Threshold 값으로 설정한다. 
      double min_sc = MC_VLV;
      for (j = 0; j < n_samples; j++) {
         if (ps_array[j]) {
            if (sc_array[j] < min_sc) min_sc = sc_array[j];
         }
      }
      if (min_sc == MC_VLV) return (2);
      SCRThresholds[i] = min_sc;
   }
   return (DONE);
}

 int BoostedTree::IsCreated (   )

{
   if (DecisionTreeFactory == NULL || WeakClassifierFactory == NULL || AdaBoostClassifier == NULL) return (FALSE);
   else return (TRUE);
}

 int BoostedTree::IsTrained (   )

{
   if (!IsCreated (   )) return (FALSE);
   if (AdaBoostClassifier->GetNumWCs (   )) return (TRUE);
   else return (FALSE);
}

 int BoostedTree::Load (const char* base_file_name)

{
   Close (   );
   StringA file_name;
   file_name.Format ("%s.bt1",base_file_name);
   if (LoadConfig (file_name)) return (1);
   if (CreateClassifier (MaxTreeDepth,NumWCs,NumSamples)) return (2);
   file_name.Format ("%s.bt2",base_file_name);
   if (LoadClassifierData (file_name)) return (3);
   return (DONE);
}

 int BoostedTree::LoadClassifierData (const char* file_name)

{
   if (!IsCreated (   )) return (1);
   ML::InputDataStream stream;
   if (stream.LoadFromFile (ML::String(file_name))) return (2);
   if (AdaBoostClassifier->LoadFromBinaryStream (stream)) return (3);
   return (DONE);
}

 int BoostedTree::LoadConfig (const char* file_name)

{
   int n;

   FileIO file(file_name,FIO_BINARY,FIO_READ);
   if (!file) return (1);
   uint signature;
   if (file.Read ((byte*)&signature,1,sizeof(signature))) return (2);
   if (signature != SIG_BOOSTED_TREE) return (3);
   if (file.Read ((byte*)&MaxTreeDepth,1,sizeof(MaxTreeDepth))) return (4);
   if (file.Read ((byte*)&NumWCs,1,sizeof(NumWCs))) return (5);
   if (file.Read ((byte*)&NumFeatures,1,sizeof(NumFeatures))) return (6);
   if (file.Read ((byte*)&NumSamples,1,sizeof(NumSamples))) return (7);
   if (file.Read ((byte*)&SCDThreshold,1,sizeof(SCDThreshold))) return (8);
   if (file.Read ((byte*)&HCRThreshold,1,sizeof(HCRThreshold))) return (9);
   if (file.Read ((byte*)&n,1,sizeof(n))) return (10);
   SCRThresholds.Create (n);
   if (file.Read ((byte*)(double*)SCRThresholds,n,sizeof(double))) return (11);
   return (DONE);
}

 int BoostedTree::Save (const char* base_file_name)

{
   StringA file_name;
   file_name.Format ("%s.bt1",base_file_name);
   if (SaveConfig (file_name)) return (1);
   file_name.Format ("%s.bt2",base_file_name);
   if (SaveClassifierData (file_name)) return (2);
   return (DONE);
}

 int BoostedTree::SaveClassifierData (const char* file_name)

{
   if (!IsTrained (   )) return (1);
   ML::OutputDataStream stream;
   if (AdaBoostClassifier->SaveToBinaryStream (stream)) return (3);
   if (stream.SaveToFile (ML::String(file_name))) return (2);
   return (DONE);
}

 int BoostedTree::SaveConfig (const char* file_name)

{
   FileIO file(file_name,FIO_BINARY,FIO_CREATE);
   if (!file) return (1);
   uint signature = SIG_BOOSTED_TREE;
   if (file.Write ((byte*)&signature,1,sizeof(signature))) return (2);
   if (file.Write ((byte*)&MaxTreeDepth,1,sizeof(MaxTreeDepth))) return (3);
   if (file.Write ((byte*)&NumWCs,1,sizeof(NumWCs))) return (4);
   if (file.Write ((byte*)&NumFeatures,1,sizeof(NumFeatures))) return (5);
   if (file.Write ((byte*)&NumSamples,1,sizeof(NumSamples))) return (6);
   if (file.Write ((byte*)&SCDThreshold,1,sizeof(SCDThreshold))) return (7);
   if (file.Write ((byte*)&HCRThreshold,1,sizeof(HCRThreshold))) return (8);
   if (file.Write ((byte*)&SCRThresholds.Length,1,sizeof(SCRThresholds.Length))) return (9);
   if (file.Write ((byte*)(double*)SCRThresholds,SCRThresholds.Length,sizeof(double))) return (10);
   return (DONE);
}

 int BoostedTree::Test_Org (FArray2D& fv_array,FArray1D& cl_array,int n_samples,float rt_offset,int& n_pos,int& n_neg,int& n_tps,int& n_fps)

{
   int i;

   logd ("   Testing the classifier...\n");
   n_pos = 0, n_neg = 0;
   n_tps = 0, n_fps = 0;
   int n_wcs = GetNumWCs (   );
   if (!n_wcs) return (1);
   for (i = 0; i < n_samples; i++) {
      double score;
      int result = Classify (fv_array[i],fv_array.Width,0.0,score);
      if (cl_array[i] > 0.0f) {
         n_pos++;
         if (result > 0) n_tps++;
      }
      if (cl_array[i] < 0.0f) {
         n_neg++;
         if (result > 0) n_fps++;
      }
   }
   float tp_rate = (float)n_tps / n_pos * 100.0f;
   float fp_rate = (float)n_fps / n_neg * 100.0f;
   logd ("   True Positive Rate  = %5.1f%% (%d/%d)\n",tp_rate,n_tps,n_pos);
   logd ("   False Positive Rate = %5.1f%% (%d/%d)\n",fp_rate,n_fps,n_neg);
   return (DONE);
}

 int BoostedTree::Test_HC (FArray2D& fv_array,FArray1D& cl_array,int n_samples,float rt_offset,int& n_pos,int& n_neg,int& n_tps,int& n_fps)

{
   int    i;
   double score;

   logd ("   Testing the classifier with the hard-cascade rejection threshold...\n");
   n_pos = 0, n_neg = 0;
   n_tps = 0, n_fps = 0;
   int n_wcs = GetNumWCs (   );
   if (!n_wcs) return (1);
   for (i = 0; i < n_samples; i++) {
      int result = Classify_HC (fv_array[i],fv_array.Width,rt_offset,score);
      if (cl_array[i] > 0.0f) {
         n_pos++;
         if (result > 0) n_tps++;
      }
      if (cl_array[i] < 0.0f) {
         n_neg++;
         if (result > 0) n_fps++;
      }
   }
   float tp_rate = (float)n_tps / n_pos * 100.0f;
   float fp_rate = (float)n_fps / n_neg * 100.0f;
   logd ("   True Positive Rate  = %5.1f%% (%d/%d)\n",tp_rate,n_tps,n_pos);
   logd ("   False Positive Rate = %5.1f%% (%d/%d)\n",fp_rate,n_fps,n_neg);
   return (DONE);
}

 int BoostedTree::Test_SC (FArray2D& fv_array,FArray1D& cl_array,int n_samples,float rt_offset,int& n_pos,int& n_neg,int& n_tps,int& n_fps)

{
   int    i;
   double score;

   logd ("   Testing the classifier with the soft-cascade rejection thresholds...\n");
   n_pos = 0, n_neg = 0;
   n_tps = 0, n_fps = 0;
   int n_wcs = GetNumWCs (   );
   if (!n_wcs) return (1);
   for (i = 0; i < n_samples; i++) {
      int result = Classify_SC (fv_array[i],fv_array.Width,rt_offset,score);
      if (cl_array[i] > 0.0f) {
         n_pos++;
         if (result > 0) n_tps++;
      }
      if (cl_array[i] < 0.0f) {
         n_neg++;
         if (result > 0) n_fps++;
      }
   }
   float tp_rate = (float)n_tps / n_pos * 100.0f;
   float fp_rate = (float)n_fps / n_neg * 100.0f;
   logd ("   True Positive Rate  = %5.1f%% (%d/%d)\n",tp_rate,n_tps,n_pos);
   logd ("   False Positive Rate = %5.1f%% (%d/%d)\n",fp_rate,n_fps,n_neg);
   return (DONE);
}

 int BoostedTree::Train (FArray2D& fv_array,FArray1D& cl_array,int n_samples,int max_td,int n_wcs)

{
   int i;
   
   Close (   );
   MaxTreeDepth = max_td;
   NumWCs       = n_wcs;
   NumFeatures  = fv_array.Width;
   NumSamples   = n_samples;
   if (CreateClassifier (max_td,n_wcs,n_samples)) return (1);
   ML::ClassifierDataset<FeatureVector> samples(2);
   for (i = 0; i < n_samples; i++) {
      ML::ClassifierExample<FeatureVector> sample;
      sample.Input.Set (fv_array[i],fv_array.Width);
      if (cl_array[i] > 0.0f) sample.Class = 0; // Pos 샘플들의 클래스 레이블 값을 0으로 한다.
      else sample.Class = 1;                    // Neg 샘플들의 클래스 레이블 값을 1로 한다.
      samples.AddEntry (sample);
   }
   AdaBoostClassifier->Train (samples,0,1);
   return (DONE);
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: BoostedTreeCascade
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_ML)

#define SIG_BOOSTED_TREE_CASCADE   MAKEFOURCC('B','S','T','C')

 BoostedTreeCascade::BoostedTreeCascade (   )

{
   CascadeMode   = BTC_CASCADE_MODE_SOFT;
   HCRTOffset    =  0.0f;
   SCRTOffset    = -0.02f;
   
   MaxTreeDepth  = 2;
   InitNumWCs    = 2000;
   IncNumWCs     = 0;
   MaxNumStages  = 1;
   NumNSBTimes   = 3;
   MinNPSCRatio  = 0.5f;
   InitNPSCRatio = 1.0f;
   BootNPSCRatio = 1.0f;

   NumFeatures   = 0;
   NumStages     = 0;
}

 BoostedTreeCascade::~BoostedTreeCascade (   )

{
   Close (   );
}

 int BoostedTreeCascade::Classify (float* s_array,int length,double& score)

{
   int result;
   
   score = 0.0;
   BOOSTED_TREE* st_classifier = First (   );
   while (st_classifier != NULL) {
      double wc_score;
      switch (CascadeMode) {
         case BTC_CASCADE_MODE_HARD1:
            result = st_classifier->Classify (s_array,length,HCRTOffset,wc_score);
            break;
         case BTC_CASCADE_MODE_HARD2:
            result = st_classifier->Classify_HC (s_array,length,HCRTOffset,wc_score);
            break;
         case BTC_CASCADE_MODE_SOFT:
            result = st_classifier->Classify_SC (s_array,length,SCRTOffset,wc_score);
            break;
         default:
            return (1);
      }
      score += wc_score;
      if (result < 0) return (-1);
      st_classifier = Next (st_classifier);
   }
   return (2);
}

 void BoostedTreeCascade::Close (   )

{
   Delete (   );
}

 int BoostedTreeCascade::GetNumFPSamples (FArray2D& fv_array,FArray1D& cl_array,int n_samples,int& n_neg,int& n_fps)

{
   int    i;
   double score;
   
   n_neg = n_fps = 0;
   for (i = 0; i < n_samples; i++) {
      if (cl_array[i] < 0.0f) {
         n_neg++;
         if (Classify (fv_array[i],fv_array.Width,score) > 0) n_fps++;
      }
   }
   return (DONE);
}

 int BoostedTreeCascade::GetNumTPSamples (FArray2D& fv_array,FArray1D& cl_array,int n_samples,int& n_pos,int& n_tps)

{
   int    i;
   double score;
   
   n_pos = n_tps = 0;
   for (i = 0; i < n_samples; i++) {
      if (cl_array[i] > 0.0f) {
         n_pos++;
         if (Classify (fv_array[i],fv_array.Width,score) > 0) n_tps++;
      }
   }
   return (DONE);
}

 int BoostedTreeCascade::Initialize (   )

{
   Close (   );
   NumFeatures = 0;
   NumStages   = 0;
   return (DONE);
}

 int BoostedTreeCascade::Load (const char* base_file_name)

{
   int i;

   Initialize (   );
   // BoostedTreeCascade Configuration 파일을 읽는다.
   if (LoadConfig (base_file_name)) return (1);
   // 각 Stage Classifier의 데이터를 로드한다.
   for (i = 0; i < NumStages; i++) {
      StringA file_name;
      file_name.Format ("%s-Stage%02d",base_file_name,i + 1);
      BOOSTED_TREE* st_classifier = new BOOSTED_TREE;
      if (st_classifier->Load (file_name)) {
         delete st_classifier;
         return (2);
      }
      Add (st_classifier);
   }
   return (DONE);
}

 int BoostedTreeCascade::LoadConfig (const char* base_file_name)

{
   StringA file_name;
   file_name.Format ("%s.cfg",base_file_name);
   FileIO file(file_name,FIO_BINARY,FIO_READ);
   if (!file) return (1);
   uint signature;
   if (file.Read ((byte*)&signature,1,sizeof(signature))) return (2);
   if (signature != SIG_BOOSTED_TREE_CASCADE) return (3);
   if (file.Read ((byte*)&CascadeMode,1,sizeof(CascadeMode))) return (4);
   if (file.Read ((byte*)&HCRTOffset,1,sizeof(HCRTOffset))) return (5);
   if (file.Read ((byte*)&SCRTOffset,1,sizeof(SCRTOffset))) return (6);
   if (file.Read ((byte*)&MaxTreeDepth,1,sizeof(MaxTreeDepth))) return (7);
   if (file.Read ((byte*)&InitNumWCs,1,sizeof(InitNumWCs))) return (8);
   if (file.Read ((byte*)&IncNumWCs,1,sizeof(IncNumWCs))) return (9);
   if (file.Read ((byte*)&MaxNumStages,1,sizeof(MaxNumStages))) return (10);
   if (file.Read ((byte*)&NumNSBTimes,1,sizeof(NumNSBTimes))) return (11);
   if (file.Read ((byte*)&MinNPSCRatio,1,sizeof(MinNPSCRatio))) return (12);
   if (file.Read ((byte*)&InitNPSCRatio,1,sizeof(InitNPSCRatio))) return (13);
   if (file.Read ((byte*)&BootNPSCRatio,1,sizeof(BootNPSCRatio))) return (14);
   if (file.Read ((byte*)&NumFeatures,1,sizeof(NumFeatures))) return (15);
   if (file.Read ((byte*)&NumStages,1,sizeof(NumStages))) return (16);
   return (DONE);
}

 int BoostedTreeCascade::Save (const char* base_file_name)

{
   int i;

   if (SaveConfig (base_file_name)) return (1);
   BOOSTED_TREE* st_classifier = First (   );
   for (i = 0; st_classifier != NULL; i++) {
      StringA file_name;
      file_name.Format ("%s-Stage%02d",base_file_name,i + 1);
      if (st_classifier->Save (file_name)) return (2);
      st_classifier = Next (st_classifier);
   }
   return (DONE);
}

 int BoostedTreeCascade::SaveConfig (const char* base_file_name)

{
   StringA file_name;
   file_name.Format ("%s.cfg",base_file_name);
   FileIO file(file_name,FIO_BINARY,FIO_CREATE);
   if (!file) return (1);
   uint signature = SIG_BOOSTED_TREE_CASCADE;
   if (file.Write ((byte*)&signature,1,sizeof(signature))) return (2);
   if (file.Write ((byte*)&CascadeMode,1,sizeof(CascadeMode))) return (3);
   if (file.Write ((byte*)&HCRTOffset,1,sizeof(HCRTOffset))) return (4);
   if (file.Write ((byte*)&SCRTOffset,1,sizeof(SCRTOffset))) return (5);
   if (file.Write ((byte*)&MaxTreeDepth,1,sizeof(MaxTreeDepth))) return (6);
   if (file.Write ((byte*)&InitNumWCs,1,sizeof(InitNumWCs))) return (7);
   if (file.Write ((byte*)&IncNumWCs,1,sizeof(IncNumWCs))) return (8);
   if (file.Write ((byte*)&MaxNumStages,1,sizeof(MaxNumStages))) return (9);
   if (file.Write ((byte*)&NumNSBTimes,1,sizeof(NumNSBTimes))) return (10);
   if (file.Write ((byte*)&MinNPSCRatio,1,sizeof(MinNPSCRatio))) return (11);
   if (file.Write ((byte*)&InitNPSCRatio,1,sizeof(InitNPSCRatio))) return (12);
   if (file.Write ((byte*)&BootNPSCRatio,1,sizeof(BootNPSCRatio))) return (13);
   if (file.Write ((byte*)&NumFeatures,1,sizeof(NumFeatures))) return (14);
   if (file.Write ((byte*)&NumStages,1,sizeof(NumStages))) return (15);
   return (DONE);
}

 int BoostedTreeCascade::TrainNewStageClassifier (FArray2D& fv_array,FArray1D& cl_array,int n_samples,int n_wcs)

{
   int e_code;
   int n_pos,n_neg,n_tps,n_fps;
   int stage_no = GetNumItems (   ) + 1;
   logd (">> Training a stage classifier (Stage #%02d)...\n",stage_no);
   logd ("   # of samples: %d\n",n_samples);
   if (!NumFeatures) NumFeatures = fv_array.Width;
   else if (NumFeatures != fv_array.Width) return (1);
   BOOSTED_TREE* st_classifier = new BOOSTED_TREE;
   if (st_classifier->Train (fv_array,cl_array,n_samples,MaxTreeDepth,n_wcs)) {
      delete st_classifier;
      return (2);
   }
   st_classifier->Test_Org (fv_array,cl_array,n_samples,0.0f,n_pos,n_neg,n_tps,n_fps);
   e_code = st_classifier->GetHCRThreshold (fv_array,cl_array,n_samples);
   if (e_code) logd ("[ERROR] Error in GetHCRThreshold() (Error Code: %d)\n",e_code);
   st_classifier->Test_HC (fv_array,cl_array,n_samples,0.0f,n_pos,n_neg,n_tps,n_fps);
   e_code = st_classifier->GetSCRThresholds (fv_array,cl_array,n_samples);
   if (e_code) logd ("[ERROR] Error in GetSCRThresholds() (Error Code: %d)\n",e_code);
   st_classifier->Test_SC (fv_array,cl_array,n_samples,0.0f,n_pos,n_neg,n_tps,n_fps);
   Add (st_classifier);
   GetNumTPSamples (fv_array,cl_array,n_samples,n_pos,n_tps);
   if (n_pos) {
      float tp_rate = (float)n_tps / n_pos * 100.0f;
      logd ("   Stage #%02d: True positive rate  = %5.1f%% (%d/%d)\n",stage_no,tp_rate,n_tps,n_pos);
   }
   GetNumFPSamples (fv_array,cl_array,n_samples,n_neg,n_fps);
   if (n_neg) {
      float fp_rate = (float)n_fps / n_neg * 100.0f;
      logd ("              False positive rate = %5.1f%% (%d/%d)\n",fp_rate,n_fps,n_neg);
   }
   return (DONE);
}

#endif

}

#if !defined(__VACL_WARPING_H)
#define __VACL_WARPING_H

#include "vacl_camera.h"

 namespace VACL
 
{

////////////////////////////////////////////////////////////////////////////////
// 
// Class: WarpingTable
//
////////////////////////////////////////////////////////////////////////////////

 class WTElement

{
   public:
      float    Weights[4];
      FPoint2D Coord;
   
   public:
      WTElement (   ) { Coord(-1.0f,-1.0f); };
};

 class WarpingTable : public Array2D<WTElement>

{
   public:
      int SrcImgWidth;
      int SrcImgHeight;

   public:
      virtual int ReadFile  (FileIO &file);
      virtual int WriteFile (FileIO &file);

   public:
      int  Create                  (int si_width,int si_height,int di_width,int di_height);
      void Crop                    (int sx,int sy,int width,int height);
      void Crop                    (IBox2D &c_rgn);
      void GetInterpolationWeights (   );
      int  ReadFile                (const char *file_name);
      int  Warp                    (GImage &s_image,GImage &d_image);
      int  Warp                    (BGRImage &s_image,BGRImage &d_image,int flag_sse2 = FALSE);
      int  Warp_YUY2               (byte* si_buffer,byte* di_buffer);
      int  WriteFile               (const char *file_name);
};

////////////////////////////////////////////////////////////////////////////////
// 
// Class: ForwardWarpingTable
//
////////////////////////////////////////////////////////////////////////////////

 class ForwardWarpingTable : public WarpingTable

{
   public:
      IBox2D   ValidRegion;
      FPoint2D MinPos;
      FPoint2D MaxPos;

   public:
      FPoint2D operator () (float fx,float fy) const;

   protected:
      void GetForwardWarpingRange (int si_width,int si_height);
      void WarpRectangularPatch   (int sx1,int sy1,int sx2,int sy2);

   public:
      virtual FPoint2D ForwardWarpingFunction (int x,int y) = 0;
      virtual int      ReadFile               (FileIO &file);
      virtual int      WriteFile              (FileIO &file);

   public:
      int  Create           (int si_width,int si_height,int step_size);
      void CropNonimageArea (   );
};

////////////////////////////////////////////////////////////////////////////////
// 
// Class: LensDistortionCorrectionTable
//
////////////////////////////////////////////////////////////////////////////////

 class LensDistortionCorrectionTable : public ForwardWarpingTable
 
{
   public:
      double   Kappa1;
      double   Kappa2;
      FPoint2D ImageCenter;

   public:
      virtual FPoint2D ForwardWarpingFunction (int x,int y);

   public:
      int Create    (int s_width,int s_height,int step_size,FPoint2D &c_pos,double kappa1,double kappa2);
      int ReadFile  (const char *file_name);
      int WriteFile (const char *file_name);
};

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

void Rotate     (GImage& s_image,float cx,float cy,float angle,GImage& d_image,int flag_normalize = FALSE);
void Shift      (GImage& s_image,FPoint2D& shift,GImage& d_image);
void Shift_YUY2 (byte *si_buffer,int si_width,int si_height,FPoint2D &shift,byte *di_buffer);
void Warp       (GImage &s_image,Matrix &mat_IH,FP2DArray1D &m_points,GImage &d_image);
void Warp       (BGRImage &s_image,Matrix &mat_IH,FP2DArray1D &m_points,BGRImage &d_image);
void Warp_YUY2  (byte *si_buffer,int si_width,int si_height,Matrix &mat_IH,FP2DArray1D &m_points,byte *di_buffer,int di_width,int di_height);
void Warp       (GImage &s_image,Matrix &mat_H,GImage &d_image,int flag_normalize = FALSE);
void Warp       (BGRImage &s_image,Matrix &mat_H,BGRImage &d_image,int flag_normalize = FALSE);
void Warp_YUY2  (byte *si_buffer,int si_width,int si_height,Matrix &mat_H,byte *di_buffer,int di_width,int di_height);
void Warp       (GImage &s_image,FP2DArray1D &m_points,GImage &d_image,int flag_normalize = FALSE);
void Warp       (BGRImage &s_image,FP2DArray1D &m_points,BGRImage &d_image,int flag_normalize = FALSE);
void Warp       (GImage &s_image,FP2DArray1D &m_points1,FP2DArray1D &m_points2,GImage &d_image,int flag_normalize = FALSE);
void Warp       (BGRImage &s_image,FP2DArray1D &m_points1,FP2DArray1D &m_points2,BGRImage &d_image,int flag_normalize = FALSE);

}

#endif

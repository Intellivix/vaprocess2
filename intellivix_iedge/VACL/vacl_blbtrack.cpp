#include "vacl_event.h"
#include "vacl_objtrack.h"
#include "vacl_tgtrack.h"

// #define __DEBUG_BLT

 namespace VACL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Definitions
//
///////////////////////////////////////////////////////////////////////////////

#define TB_PARAM_COLOR_UPDATE_RATE   0.3f

///////////////////////////////////////////////////////////////////////////////
//
// Class: TrackedBlob
//
///////////////////////////////////////////////////////////////////////////////

 TrackedBlob::TrackedBlob (   )

{
   ID                 = 0;
   Correspondence     = 0;
   RgnID              = 0;
   Status             = 0;
   Count_Tracked      = 0;
   Time_InEventZone   = 0.0f;
   Time_Tracked       = 0.0f;
   Time_Triggered     = 0.0f;
   Time_Undetected1   = 0.0f;
   Time_Undetected2   = 0.0f;
   Area               = 0;
   MovingDistance1    = 0.0f;
   MovingDistance2    = 0.0f;
   MaxMovingDistance1 = 0.0f;
   MaxMovingDistance2 = 0.0f;
   NorSpeed           = -1.0f;
   Speed              = -1.0f;
   UPArea             = 0;
   Disorder           = 0.0f;
   EvtGenObject       = NULL;
   MeanVelocity(MC_VLV,MC_VLV);
   Velocity(MC_VLV,MC_VLV);
   StaticPos(-1.0f,-1.0f);
   memset (&CntBkgColor,0,sizeof(CntBkgColor));
   memset (&CntFrgColor,0,sizeof(CntFrgColor));
   memset (&RgnBkgColor,0,sizeof(RgnBkgColor));
   memset (&RgnFrgColor,0,sizeof(RgnFrgColor));
}

 void TrackedBlob::GetCenterColors (BGRImage& s_cimage,BGRImage& b_cimage,BGRColor& f_color,BGRColor& b_color)
 
{
   int x,y;
   
   int cx = X + Width  / 2;
   int cy = Y + Height / 2;
   int sx = cx - 1;
   int sy = cy - 1;
   int ex = cx + 2;
   int ey = cy + 2;
   if (sx < 0) sx = 0;
   if (sy < 0) sy = 0;
   if (ex > s_cimage.Width ) ex = s_cimage.Width;
   if (ey > s_cimage.Height) ey = s_cimage.Height;
   int n = 0;
   int fb = 0,fg = 0,fr = 0;
   int bb = 0,bg = 0,br = 0;
   for (y = sy; y < ey; y++) {
      BGRColor* sp = &s_cimage[y][sx];
      BGRColor* bp = &b_cimage[y][sx];
      for (x = sx; x < ex; x++) {
         fb += sp->B;
         fg += sp->G;
         fr += sp->R;
         bb += bp->B;
         bg += bp->G;
         br += bp->R;
         sp++,bp++;
         n++;
      }
   }
   f_color((byte)(fb/n),(byte)(fg/n),(byte)(fr/n));
   b_color((byte)(bb/n),(byte)(bg/n),(byte)(br/n));
}

 void TrackedBlob::GetPropertyValues (float frm_rate)
 
{
   int i,n;
   
   // 최초 위치로부터 현재 위치까지의 직선 이동 거리를 구한다.
   FArray1D    md_array(4);
   IP2DArray1D ip_array(4);
   IP2DArray1D cp_array(4);
   InitRegion.GetCornerPoints (ip_array);
   GetCornerPoints (cp_array);
   for (i = 0; i < 4; i++)
      md_array[i] = Mag(ip_array[i] - cp_array[i]);
   FindMaximum (md_array,MovingDistance1,i);
   FindMinimum (md_array,MovingDistance2,i);
   if (MovingDistance1 > MaxMovingDistance1) MaxMovingDistance1 = MovingDistance1;
   if (MovingDistance2 > MaxMovingDistance2) MaxMovingDistance2 = MovingDistance2;
   // 순간 속도(Velocity), 1초 동안의 평균 속도(MeanVelocity), 
   // 속력(Speed), 정규화된 속력(NorSpeed)를 구한다.
   int n_frames = (int)(frm_rate + 0.5f);
   Speed = NorSpeed = 0.0f;
   MeanVelocity.Clear (   );
   TrkBlobRegion* tb_rgn = TrkBlobRegions.First (   );
   if (tb_rgn != NULL) {
      Velocity = GetCenterPosition (   ) - tb_rgn->GetCenterPosition (   );
      int avg_height = Height;
      MeanVelocity = Velocity;
      for (i = n = 1; i < n_frames; i++) {
         if (tb_rgn == NULL) break;
         if (tb_rgn->Velocity.X == MC_VLV) break;
         avg_height   += tb_rgn->Height;
         MeanVelocity += tb_rgn->Velocity;
         n++;
         tb_rgn = TrkBlobRegions.Next (tb_rgn);
      }
      MeanVelocity /= (float)n;
      avg_height   /= n;
      Speed    = Mag(MeanVelocity) * n_frames;
      NorSpeed = 100.0f * Speed / avg_height;
   }
}

 int TrackedBlob::GetRegionColors (BGRImage& s_cimage,BGRImage& b_cimage,IArray2D& br_map,BGRColor& f_color,BGRColor& b_color)
 
{
   int x,y;

   int ex = X + Width;
   int ey = Y + Height;
   int n  = 0;
   int fb = 0,fg = 0,fr = 0;
   int bb = 0,bg = 0,br = 0;
   for (y = Y; y < ey; y++) {
      BGRColor* sp = &s_cimage[y][X];
      BGRColor* bp = &b_cimage[y][X];
      int* l_br_map = br_map[y];
      for (x = X; x < ex; x++) {
         if (l_br_map[x] == RgnID) {
            fb += sp->B;
            fg += sp->G;
            fr += sp->R;
            bb += bp->B;
            bg += bp->G;
            br += bp->R;
            n++;
         }
         sp++,bp++;
      }
   }
   if (n) {
      f_color((byte)(fb/n),(byte)(fg/n),(byte)(fr/n));
      b_color((byte)(bb/n),(byte)(bg/n),(byte)(br/n));
      return (DONE);
   }
   else {
      f_color.Clear (   );
      b_color.Clear (   );
      return (1);
   }
}

 void TrackedBlob::Initialize (ConnectedRegion &s_rgn)
 
{
   *(ConnectedRegion*)this = s_rgn;
   CurRegion               = s_rgn;
   InitRegion              = s_rgn;
}

 void TrackedBlob::InitTargetTracker (GImage& s_image)

{
   TargetTracker.ZoomStep         = 0.02f;
   TargetTracker.MatchingRange    = 15;
   TargetTracker.MinMatchingScore = 0.5f;
   TargetTracker.Initialize (s_image,*this,7.0f);
}

 void TrackedBlob::PutRectMask (ushort d_label,USArray2D& d_array,int margin)
 
{
   int x,y;
   
   int sx = X - margin;
   int sy = Y - margin;
   int ex = X + Width  + margin;
   int ey = Y + Height + margin;
   if (sx < 0) sx = 0;
   if (sy < 0) sy = 0;
   if (ex >= d_array.Width ) ex = d_array.Width;
   if (ey >= d_array.Height) ey = d_array.Height;
   for (y = sy; y < ey; y++) {
      ushort* l_d_array = d_array[y];
      for (x = sx; x < ex; x++) l_d_array[x] = d_label;
   }
}

 void TrackedBlob::SetEvtGenObject (TrackedObject* eg_object)
 
{
   EvtGenObject = eg_object;
   if (EvtGenObject == NULL) return;
   EvtGenObject->SetRegion (*this);
   EvtGenObject->CntBkgColor((byte)CntBkgColor.B,(byte)CntBkgColor.G,(byte)CntBkgColor.R);
   EvtGenObject->CntFrgColor((byte)CntFrgColor.B,(byte)CntFrgColor.G,(byte)CntFrgColor.R);
   EvtGenObject->RgnBkgColor((byte)RgnBkgColor.B,(byte)RgnBkgColor.G,(byte)RgnBkgColor.R);
   EvtGenObject->RgnFrgColor((byte)RgnFrgColor.B,(byte)RgnFrgColor.G,(byte)RgnFrgColor.R);
}

 int TrackedBlob::TrackTarget (GImage& s_image,IBox2D& d_rgn,float& m_score)

{
   m_score = TargetTracker.Perform (s_image);
   d_rgn = TargetTracker.TargetRegion;
   if      (m_score == 0.0f) return (1);
   else if (m_score <= 0.5f) return (-1);
   else return (DONE);
}

 int TrackedBlob::UpdateCenterColors (BGRColor& f_color,BGRColor& b_color)
 
{
   if (Count_Tracked <= 0) return (1);
   float a = TB_PARAM_COLOR_UPDATE_RATE;
   float b = 1.0f - a;
   CntFrgColor.B = b * CntFrgColor.B + a * f_color.B;
   CntFrgColor.G = b * CntFrgColor.G + a * f_color.G;
   CntFrgColor.R = b * CntFrgColor.R + a * f_color.R;
   CntBkgColor.B = b * CntBkgColor.B + a * b_color.B;
   CntBkgColor.G = b * CntBkgColor.G + a * b_color.G;
   CntBkgColor.R = b * CntBkgColor.R + a * b_color.R;
   if (EvtGenObject != NULL) {
      EvtGenObject->CntBkgColor((byte)CntBkgColor.B,(byte)CntBkgColor.G,(byte)CntBkgColor.R);
      EvtGenObject->CntFrgColor((byte)CntFrgColor.B,(byte)CntFrgColor.G,(byte)CntFrgColor.R);
   }
   return (DONE);
}

 int TrackedBlob::UpdateCRInfo (IArray2D& cr_map,IBox2D& s_rgn,int cr_no)

{
   int x,y;
   struct _CR {
      int N;
      int BX,BY;
      int SX,SY;
      int EX,EY;
      int SumX;
      int SumY;
   };

   _CR cr;
   cr.N    = 0;
   cr.BX   = -1;
   cr.BY   = -1;
   cr.SX   = cr_map.Width;
   cr.SY   = cr_map.Height;
   cr.EX   = -1;
   cr.EY   = -1;
   cr.SumX = 0;
   cr.SumY = 0;
   int sx = s_rgn.X;
   int sy = s_rgn.Y;
   int ex = sx + s_rgn.Width;
   int ey = sy + s_rgn.Height;
   for (y = sy; y < ey; y++) {
      int* l_cr_map = cr_map[y];
      for (x = sx; x < ex; x++) {
         if (l_cr_map[x] == cr_no) {
            cr.N++;
            if (cr.BX < 0) cr.BX = x, cr.BY = y;
            if (cr.SX > x) cr.SX = x;
            if (cr.SY > y) cr.SY = y;
            if (cr.EX < x) cr.EX = x;
            if (cr.EY < y) cr.EY = y;
            cr.SumX += x;
            cr.SumY += y;
         }
      }
   }
   if (!cr.N) return (1);
   X      = cr.SX;
   Y      = cr.SY;
   Width  = cr.EX - cr.SX + 1;
   Height = cr.EY - cr.SY + 1;
   Area   = cr.N;
   SX     = cr.BX;
   SY     = cr.BY;
   CX     = (float)cr.SumX / cr.N;
   CY     = (float)cr.SumY / cr.N;
   return (DONE);
}

 void TrackedBlob::UpdateRegion (ConnectedRegion& s_cr,float ud_rate)
 
{
   int c = Correspondence;
   *(ConnectedRegion*)this = s_cr;
   Correspondence = c;
   if (ud_rate == 1.0f) CurRegion = *(IBox2D*)&s_cr;
   else {
      FPoint2D cr_cp = CurRegion.GetCenterPosition (   );
      FPoint2D sr_cp = s_cr.GetCenterPosition (   );
      cr_cp += ud_rate * (sr_cp - cr_cp);
      float w = (float)CurRegion.Width;
      float h = (float)CurRegion.Height;
      w += ud_rate * (s_cr.Width  - w);
      h += ud_rate * (s_cr.Height - h);
      CurRegion.X      = (int)(cr_cp.X - 0.5f * w);
      CurRegion.Y      = (int)(cr_cp.Y - 0.5f * h);
      CurRegion.Width  = (int)(w + 0.5f);
      CurRegion.Height = (int)(h + 0.5f);
   }
}

 int TrackedBlob::UpdateRegionColors (BGRColor& f_color,BGRColor& b_color)
 
{
   if (Count_Tracked <= 0) return (1);
   float a = TB_PARAM_COLOR_UPDATE_RATE;
   float b = 1.0f - a;
   RgnFrgColor.B = b * RgnFrgColor.B + a * f_color.B;
   RgnFrgColor.G = b * RgnFrgColor.G + a * f_color.G;
   RgnFrgColor.R = b * RgnFrgColor.R + a * f_color.R;
   RgnBkgColor.B = b * RgnBkgColor.B + a * b_color.B;
   RgnBkgColor.G = b * RgnBkgColor.G + a * b_color.G;
   RgnBkgColor.R = b * RgnBkgColor.R + a * b_color.R;
   if (EvtGenObject != NULL) {
      EvtGenObject->RgnBkgColor((byte)RgnBkgColor.B,(byte)RgnBkgColor.G,(byte)RgnBkgColor.R);
      EvtGenObject->RgnFrgColor((byte)RgnFrgColor.B,(byte)RgnFrgColor.G,(byte)RgnFrgColor.R);
   }
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: BlobTracking
//
///////////////////////////////////////////////////////////////////////////////

 BlobTracking::BlobTracking (   )
 
{
   Flag_Enabled        = TRUE;
   Options             = 0;
   MinBlobArea         = 0;
   MaxBlobArea         = 1000000;
   MinCount_Tracked    = 3;
   MaxBRMLLength       = 0.0f;
   MaxNorSpeed_Static  = 6.0f;
   MaxTime_Undetected1 = 0.5f;
   MaxTime_Undetected2 = 1.0f;
   MaxTime_Undetected3 = 0.3f;
   MaxTime_Verified    = 1.0f;
   MaxTrjLength        = 2.0f;
   MinMovingDistance1  = 0.0f;
   MinMovingDistance2  = 0.0f;
   RegionUpdateRate    = 0.05f;
   _Init (   );
}

 BlobTracking::~BlobTracking (   )
 
{
   Close (   );
}

 void BlobTracking::_Init (   )
 
{
   Flag_Initialized = FALSE;
   FrameRate        = 0.0f;
   BlobCount        = 1;
   FrameCount       = 0;
   SrcImage         = NULL;
   SrcImgSize.Clear (   );
}

 void BlobTracking::AddToBRMList (IArray2D& br_map)

{
   int length = (int)(FrameRate * MaxBRMLLength + 0.5f);
   if (length <= 0) length = 1;
   if (BRMList.GetNumItems (   ) > length) {
      BRMData* brm_data = BRMList.Last (   );
      BRMList.Remove (brm_data);
      delete brm_data;
   }
   BRMData* brm_data = new BRMData;
   brm_data->BlobRegionMap = br_map;
   BRMList.Add (brm_data,LL_FIRST);
}

 TrkBlobRegion* BlobTracking::AddToTrkBlobRegionList (TrackedBlob *t_blob)
 
{
   int length = (int)(FrameRate * MaxTrjLength + 0.5f);
   if (length <= 0) length = 1;
   if (t_blob->TrkBlobRegions.GetNumItems (   ) > length) {
      TrkBlobRegion* tb_rgn = t_blob->TrkBlobRegions.Last (   );
      t_blob->TrkBlobRegions.Remove (tb_rgn);
      delete tb_rgn;
   }
   TrkBlobRegion* tb_rgn = CreateTrkBlobRegion (   );
   *(IBox2D*)tb_rgn = *(IBox2D*)t_blob;
   tb_rgn->RgnID           = t_blob->RgnID;
   tb_rgn->Status          = t_blob->Status;
   tb_rgn->Area            = t_blob->Area;
   tb_rgn->UPArea          = t_blob->UPArea;
   tb_rgn->Disorder        = t_blob->Disorder;
   tb_rgn->MovingDistance1 = t_blob->MovingDistance1;
   tb_rgn->MovingDistance2 = t_blob->MovingDistance2;
   tb_rgn->Velocity        = t_blob->Velocity;
   t_blob->TrkBlobRegions.Add (tb_rgn,LL_FIRST);
   return (tb_rgn);
}

 int BlobTracking::CheckBlobStatic (TrackedBlob *t_blob)
 
{
   if (0.0f <= t_blob->NorSpeed && t_blob->NorSpeed <= MaxNorSpeed_Static) {
      t_blob->Status |= TB_STATUS_STATIC;
      if (t_blob->StaticPos.X < 0.0f) t_blob->StaticPos = t_blob->GetCenterPosition (    );
      return (DONE);
   }
   else {
      if (t_blob->Status & TB_STATUS_STATIC) {
         float d = Mag(t_blob->GetCenterPosition (   ) - t_blob->StaticPos);
         if (d >= 0.5f * t_blob->Height) { // PARAMETERS
            t_blob->Status &= ~TB_STATUS_STATIC;
            t_blob->StaticPos(-1.0f,-1.0f);
            return (1);
         }
         else return (DONE);
      }
      else return (1);
   }
}

 void BlobTracking::CleanBlobList (   )
 
{
   TrackedBlob *t_blob = TrackedBlobs.First (   );
   while (t_blob != NULL) {
      TrackedBlob* n_blob = TrackedBlobs.Next (t_blob);
      if (t_blob->Status & TB_STATUS_TO_BE_REMOVED) {
         if (t_blob->EvtGenObject != NULL) t_blob->EvtGenObject->Status |= TO_STATUS_TO_BE_REMOVED;
         TrackedBlobs.Remove (t_blob);
         delete t_blob;
      }
      t_blob = n_blob;
   }
}

 void BlobTracking::ClearBlobEventStatus (   )
 
{
   TrackedBlob *t_blob = TrackedBlobs.First (   );
   while (t_blob != NULL) {
      if (!(t_blob->Status & TB_STATUS_IN_EVENT_ZONE)) t_blob->Time_InEventZone = 0.0f;
      t_blob->Status &= ~(TB_STATUS_IN_EVENT_ZONE | TB_STATUS_ON_EVENT);
      t_blob = TrackedBlobs.Next (t_blob);
   }
}

 void BlobTracking::Close (   )
 
{
   BlobRegions.Delete   (   );
   BlobRegionMap.Delete (   );
   TrackedBlobs.Delete  (   );
   _Init (   );
}

 int BlobTracking::CopyBlobRegion (IArray2D& s_cr_map,TrackedBlob* t_blob,int s_cr_no,IArray2D& d_cr_map,int d_cr_no,int flag_predict_pos)

{
   IPoint2D d_pos;   
   if (flag_predict_pos && t_blob->Velocity.X != MC_VLV) {
      d_pos.X = (int)(t_blob->X + t_blob->Velocity.X + 0.5f);
      d_pos.Y = (int)(t_blob->Y + t_blob->Velocity.Y + 0.5f);
   }
   else {
      d_pos.X = t_blob->X;
      d_pos.Y = t_blob->Y;
   }
   IBox2D d_rgn(d_pos.X,d_pos.Y,t_blob->Width,t_blob->Height);
   // 객체의 예측 영역(d_rgn)이 영상 바운더리에 걸리는 경우, 크롭핑된 d_rgn의 면적이
   // 원 영역의 면적의 1/3 이하로 줄어들면 객체가 영상 밖으로 나간 것으로 간주하고
   // 더 이상의 처리를 진행하지 않느다.
   ISize2D cr_size(d_cr_map.Width,d_cr_map.Height);
   CropRegion (cr_size,d_rgn);
   #if defined(__DEBUG_BLT)
   logd ("   Predicted: X = %04d, Y = %04d, W = %04d, H = %04d\n",d_rgn.X,d_rgn.Y,d_rgn.Width,d_rgn.Height);
   #endif
   float ar = (float)(d_rgn.Width * d_rgn.Height) / (t_blob->Width * t_blob->Height);
   if (ar <= 0.33f) return (1); // PARAMETERS
   CopyCR (s_cr_map,*t_blob,s_cr_no,d_cr_map,d_pos,d_cr_no);
   if (t_blob->UpdateCRInfo (d_cr_map,d_rgn,d_cr_no)) return (2);
   t_blob->UpdateRegion (*t_blob,RegionUpdateRate);
   return (DONE);
}

 TrackedBlob* BlobTracking::CreateTrackedBlob (   )
 
{
   return (new TrackedBlob);
}

 TrkBlobRegion* BlobTracking::CreateTrkBlobRegion (   )
 
{
   return (new TrkBlobRegion);
}

 int BlobTracking::GetBkgModelUpdateScheme (GImage &d_image)
 
{
   int x,y;
   
   if (!IsEnabled (   )) return (1);
   TrackedBlob* t_blob = TrackedBlobs.First (   );
   while (t_blob != NULL) {
      if (t_blob->Status & TB_STATUS_VERIFIED) {
         int sx = t_blob->X;
         int sy = t_blob->Y;
         int ex = sx + t_blob->Width;
         int ey = sy + t_blob->Height;
         int lb = t_blob->RgnID;
         for (y = sy; y < ey; y++) {
            int  *l_br_map  = BlobRegionMap[y];
            byte *l_d_image = d_image[y];
            for (x = sx; x < ex; x++) {
               if (l_br_map[x] == lb) l_d_image[x] = FGD_BMU_SPEED_ZERO;
            }
         }
      }
      t_blob = TrackedBlobs.Next (t_blob);
   }
   return (DONE);
}

 void BlobTracking::GetBlobArray (TBArray1D &d_array)
 
{
   int i;

   d_array.Create (TrackedBlobs.GetNumItems (   ) + 1);
   d_array[0] = NULL;
   TrackedBlob* t_blob = TrackedBlobs.First (   );
   for (i = 1; i < d_array.Length; i++) {
      d_array[i] = t_blob;
      t_blob = TrackedBlobs.Next (t_blob);
   }
}

 void BlobTracking::GetBlobDisorder (TrackedBlob* t_blob)

{
   int x,y;
   
   if (!t_blob->RgnID) return;
   float disorder = 0.0f;
   int sx = t_blob->X;
   int sy = t_blob->Y;
   int ex = sx + t_blob->Width;
   int ey = sy + t_blob->Height;
   int br_id = t_blob->RgnID;
   for (y = sy; y < ey; y++) {
      int* l_br_map = BlobRegionMap[y];
      for (x = sx; x < ex; x++) {
         if (l_br_map[x] == br_id) {
            BPTracker.Perform (x,y);
            int perimeter = BPTracker.NumBoundaryPixels + BPTracker.NumMarginalPixels;
            disorder = sqrt ((perimeter * perimeter) / (4.0f * MC_PI * t_blob->Area));
            goto end;
         }
      }
   }
   end:;
   t_blob->Disorder = disorder;
}

 void BlobTracking::GetBlobUPArea (TrackedBlob* t_blob)

{
   int x,y;
   
   if (!t_blob->RgnID) return;
   int area = 0;
   int sx = t_blob->X;
   int sy = t_blob->Y;
   int ex = sx + t_blob->Width;
   int ey = (int)(sy + 0.35f * t_blob->Height); // PARAMETERS
   int br_id = t_blob->RgnID;
   for (y = sy; y < ey; y++) {
      int* l_br_map = BlobRegionMap[y];
      for (x = sx; x < ex; x++) {
         if (l_br_map[x] == br_id) area++;
      }
   }
   t_blob->UPArea = area;
}

 int BlobTracking::GetNewBlobID (   )
 
{
   return (BlobCount++);
}

 int BlobTracking::Initialize (ISize2D& si_size,float frm_rate)
 
{
   if (!IsEnabled (   )) return (1);
   Close (   );
   SrcImgSize = si_size;
   FrameRate  = frm_rate;
   BlobRegionMap.Create (si_size.Width,si_size.Height);
   Reset (   );
   Flag_Initialized = TRUE;
   return (DONE);
}

 int BlobTracking::Perform (   )

{
   int i,j,n;
   int x,y;
   
   FrameDuration = 1.0f / FrameRate;
   BRMData* brm_data = BRMList.First (   );
   if (brm_data == NULL) return (1);
   IArray2D& pbr_map = brm_data->BlobRegionMap;
   // TB들의 포인터 배열을 생성한다.
   TBArray1D tb_array;
   GetBlobArray (tb_array);
   for (i = 1; i < tb_array.Length; i++) {
      // 현재 TB의 정보를 TrkBlobRegionList에 등록한다.
      AddToTrkBlobRegionList (tb_array[i]);
      // 매 프레임마다 판단하는 상태 값의 경우 클리어한다.
      tb_array[i]->Status &= ~(
         TB_STATUS_CREATED|TB_STATUS_MERGED|TB_STATUS_STATIC|TB_STATUS_UNDETECTED|TB_STATUS_VERIFIED_START
      );
   }
   // BR(BlobRegion)과 TB 사이의 대응 관계를 나타내는 행렬을 구한다.
   IArray2D ov_matrix(tb_array.Length,BlobRegions.Length);
      // ov_matrix[i][j]: BR_i와 TB_j 사이의 겹치는 픽셀 수
      // BR_i: BlobRegions[i]
      // TB_j: tb_array[j]
   ov_matrix.Clear (   );
   for (j = 1; j < tb_array.Length; j++) {
      TrackedBlob* t_blob = tb_array[j];
      int sx = t_blob->X;
      int sy = t_blob->Y;
      int ex = sx + t_blob->Width;
      int ey = sy + t_blob->Height;
      int cr_no = t_blob->RgnID;
      for (y = sy; y < ey; y++) {
         int* l_pbr_map = pbr_map[y];
         int* l_cbr_map = BlobRegionMap[y];
         for (x = sx; x < ex; x++) {
            if (l_pbr_map[x] == cr_no && l_cbr_map[x]) ov_matrix[l_cbr_map[x]][j]++;
         }
      }
   }
   for (i = 1; i < ov_matrix.Height; i++) {
      // BR_i와 가장 많이 겹치는 TB를 구한다.
      int* l_ov_matrix = ov_matrix[i];
      BlobRegions[i].Correspondence = 0;
      int max_j = 0, max_ov = 0;
      for (j = 1,n = 0; j < ov_matrix.Width; j++) {
         if (l_ov_matrix[j]) {
            n++;
            if (l_ov_matrix[j] > max_ov) {
               max_ov = l_ov_matrix[j];
               max_j  = j;
            }
         }
      }
      // BR_i와 가장 많이 겹치는 TB가 아직 TB_STATUS_VALIDATED 상태가 아니면, 다른 TB를 찾아본다.
      if (max_j && !(tb_array[max_j]->Status & TB_STATUS_VALIDATED)) {
         int max_j2 = 0, max_ov2 = 0;
         int* l_ov_matrix = ov_matrix[i];
         for (j = 1; j < ov_matrix.Width; j++) {
            if (j != max_j && (tb_array[j]->Status & TB_STATUS_VALIDATED) && (l_ov_matrix[j] > max_ov2) && ((float)l_ov_matrix[j] / max_ov > 0.5f)) { // PARAMETERS
               max_ov2 = l_ov_matrix[j];
               max_j2  = j;
            }
         }
         if (max_j2) {
            max_j  = max_j2;
            max_ov = max_ov2;
         }
      }
      // max_j 값이 0이면 BR_i에 대응하는 TB가 없으므로 BR_i는 신규 객체 영역에 해당한다.
      BlobRegions[i].Correspondence = max_j;
      if (n > 1) { 
         // BR_i와 대응되는 TB가 2개 이상인 경우 해당 TB의 상태를 TB_STATUS_MERGED로 셋한다.
         for (j = 1; j < ov_matrix.Width; j++) {
            if (l_ov_matrix[j]) {
               tb_array[j]->Status |= TB_STATUS_MERGED;
            }
         }
      }
   }
   // TB_j와 가장 많이 겹치는 BR을 구한다.
   for (j = 1; j < ov_matrix.Width; j++) {
      tb_array[j]->Correspondence = 0;
      int max_i = 0, max_ov = 0;
      for (i = 1; i < ov_matrix.Height; i++) {
         if (ov_matrix[i][j] > max_ov) {
            max_ov = ov_matrix[i][j];
            max_i  = i;
         }
      }
      // max_i 값이 0이면 TB_j에 대응하는 BR이 없음을 의미한다.
      tb_array[j]->Correspondence = max_i;
   }
   // TB들과 BR들이 1:1 대응이 되도록 만든다.
   // TB와 BR 사이의 겹치는 픽셀 수가 가장 많은 것을 대응쌍으로 본다.
   for (i = 1; i < BlobRegions.Length; i++) {
      j = BlobRegions[i].Correspondence;
      if (j) {
         if (tb_array[j]->Correspondence != i) BlobRegions[i].Correspondence = 0;
      }
   }
   for (j = 1; j < tb_array.Length; j++) {
      i = tb_array[j]->Correspondence;
      if (i) {
         if (BlobRegions[i].Correspondence != j) tb_array[j]->Correspondence = 0;
      }
      else tb_array[j]->Correspondence = 0;
   }
   #if defined(__DEBUG_BLT)
   logd ("Correspondences ------------------------------------------------------------\n");
   for (i = 1; i < BlobRegions.Length; i++) {
      ConnectedRegion& br = BlobRegions[i];
      logd ("[BR%05d] X = %04d, Y = %04d, W = %04d, H = %04d\n",i,br.X,br.Y,br.Width,br.Height);
      logd ("   corresponds to ");
      if (br.Correspondence) {
         TrackedBlob* tb = tb_array[br.Correspondence];
         if (tb->Status & TB_STATUS_MERGED) logd ("[TB%05d:M]\n",tb->ID);
         else logd ("[TB%05d]\n",tb->ID);
      }
      else logd ("[None]\n");
   }
   for (i = 1; i < tb_array.Length; i++) {
      TrackedBlob* tb = tb_array[i];
      logd ("[TB%05d] X = %04d, Y = %04d, W = %04d, H = %04d\n",tb->ID,tb->X,tb->Y,tb->Width,tb->Height);
      logd ("   corresponds to ");
      if (tb->Correspondence) logd ("[BR%05d]\n",tb->Correspondence);
      else logd ("[None]\n");
   }
   #endif
   // 각 BR에 대해 그것에 대응하는 TB의 영역 정보를 업데이트한다.
   // 만약 대응하는 TB가 없으면 신규 TB를 추가한다.
   for (i = 1; i < BlobRegions.Length; i++) {
      j = BlobRegions[i].Correspondence;
      if (j) { // BR_i에 대응되는 TB가 TB_j인 경우, TB_j의 내용을 업데이트한다.
         // TB_j의 영역 정보를 업데이트한다.
         tb_array[j]->UpdateRegion (BlobRegions[i],RegionUpdateRate);
      }
      else { // BR_i에 대응되는 TB가 없는 경우 신규 TB를 추가한다.
         TrackedBlob *t_blob = CreateTrackedBlob (   );
         t_blob->Status |= TB_STATUS_CREATED;
         t_blob->ID = GetNewBlobID (   );
         t_blob->Initialize (BlobRegions[i]);
         t_blob->Correspondence = i;
         TrackedBlobs.Add (t_blob);
      }
   }
   // BlobRegionMap 상의 BR들의 레이블 값을 해당 TB의 레이블 값으로 변경하는데 필요한 변환 테이블을 만든다.
   IArray1D cv_table(BlobRegions.Length + tb_array.Length);
   cv_table.Clear (   );
   n = BlobRegions.Length;
   TrackedBlob *t_blob = TrackedBlobs.First (   );
   for (i = 1; t_blob != NULL;   ) {
      int pbr_no = t_blob->RgnID;
      t_blob->RgnID = 0;
      // TB에 대응하는 BR이 있는 경우 경우
      if (t_blob->Correspondence) {
         t_blob->Time_Undetected1 = t_blob->Time_Undetected2 = 0.0f;
         cv_table[t_blob->Correspondence] = t_blob->RgnID = i++;
         if (SrcImage != NULL) t_blob->InitTargetTracker (*SrcImage);
      }
      // TB에 대응하는 BR이 없는 경우
      else {
         t_blob->Status |= TB_STATUS_UNDETECTED;
         #if defined(__DEBUG_BLT)
         logd ("[TB%05d] Undetected\n",t_blob->ID);
         #endif
         int flag_predict = FALSE;
         if ((t_blob->Status & TB_STATUS_VALIDATED) && (t_blob->Time_Undetected1 <= MaxTime_Undetected1)) flag_predict = TRUE;
         if ((t_blob->Status & TB_STATUS_ON_EVENT ) && (t_blob->Time_Undetected1 <= MaxTime_Undetected2)) flag_predict = TRUE;
         if (flag_predict) {
            t_blob->Time_Undetected1 += FrameDuration;
            if (SrcImage == NULL) {
               if (CopyBlobRegion (pbr_map,t_blob,pbr_no,BlobRegionMap,n,FALSE)) {
                  t_blob->Status |= TB_STATUS_TO_BE_REMOVED;
                  #if defined(__DEBUG_BLT)
                  logd ("   To be removed (Case 1)\n",t_blob->ID);
                  #endif
               }
               else cv_table[n++] = t_blob->RgnID = i++;
            }
            else {
               int e_code = TrackBlobRegion (t_blob,BlobRegionMap,n);
               if (e_code > 0) {       // 객체가 영상 밖으로 나갔거나 다른 문제가 생긴 경우
                  t_blob->Status |= TB_STATUS_TO_BE_REMOVED;
                  #if defined(__DEBUG_BLT)
                  logd ("   To be removed (Case 2)\n",t_blob->ID);
                  #endif
               }
               else if (e_code < 0) { // 매칭에 실패한 경우
                  // Predictor를 이용하여 짧은 시간 동안 때운다.
                  t_blob->Time_Undetected2 += FrameDuration;
                  if (t_blob->Time_Undetected2 > MaxTime_Undetected3) {
                     t_blob->Status |= TB_STATUS_TO_BE_REMOVED;
                     #if defined(__DEBUG_BLT)
                     logd ("   To be removed (Case 3)\n",t_blob->ID);
                     #endif
                  }
                  else {
                     if (CopyBlobRegion (pbr_map,t_blob,pbr_no,BlobRegionMap,n,FALSE)) {
                        t_blob->Status |= TB_STATUS_TO_BE_REMOVED;
                        #if defined(__DEBUG_BLT)
                        logd ("   To be removed (Case 4)\n",t_blob->ID);
                        #endif
                     }
                     else cv_table[n++] = t_blob->RgnID = i++;
                  }
               }
               else {
                  cv_table[n++] = t_blob->RgnID = i++;
                  t_blob->Time_Undetected2 = 0.0f;
               }
            }
         }
         else {
            t_blob->Status |= TB_STATUS_TO_BE_REMOVED;
            #if defined(__DEBUG_BLT)
            logd ("   To be removed (Case 5)\n",t_blob->ID);
            #endif
         }
      }
      t_blob = TrackedBlobs.Next (t_blob);
   }
   #if defined(__DEBUG_BLT)
   logd ("\nConversion Table ------------------------------------------------------------\n");
   for (i = 1; i < cv_table.Length; i++) logd ("[%03d=>%03d]",i,cv_table[i]);
   logd ("\n");
   #endif
   // BlobRegionMap 상의 BR들의 레이블 값을 해당 TB의 레이블 값으로 변경한다.
   // TB에 대응하는 BlobRegionMap 상의 BR의 레이블 값은 TB.RgnID가 된다.
   ConvertCRMap (BlobRegionMap,cv_table,BlobRegionMap);
   if (Options & BLT_OPTION_GET_BLOB_DISORDER) BPTracker.Initialize (BlobRegionMap,1);
   t_blob = TrackedBlobs.First (   );
   while (t_blob != NULL) {
      if (!(t_blob->Status & TB_STATUS_TO_BE_REMOVED)) {
         t_blob->Count_Tracked++;
         t_blob->Time_Tracked += FrameDuration;
         // TB의 각종 속성 값들을 구한다.
         t_blob->GetPropertyValues (FrameRate);
         // TB의 Upper-Part의 면적을 구한다.
         GetBlobUPArea (t_blob);
         // TB의 Disorder 값을 구한다.
         if (Options & BLT_OPTION_GET_BLOB_DISORDER) GetBlobDisorder (t_blob);
         // TB가 정적인(static) 상태인지 체크한다.
         CheckBlobStatic (t_blob);
         // TB가 유효한 객체인지 검사한다.
         if (t_blob->Count_Tracked == MinCount_Tracked) t_blob->Status |= TB_STATUS_VALIDATED;
         if (t_blob->Count_Tracked <= MinCount_Tracked) {
            if (t_blob->Status & TB_STATUS_UNDETECTED) t_blob->Status |= TB_STATUS_TO_BE_REMOVED;
         }
         if (!(t_blob->Status & TB_STATUS_TO_BE_REMOVED)) {
            if ((t_blob->Status & TB_STATUS_VALIDATED) && !(t_blob->Status & TB_STATUS_VERIFIED)) {
               if (t_blob->Time_Tracked > MaxTime_Verified) {
                  t_blob->Status |= TB_STATUS_TO_BE_REMOVED;
                  #if defined(__DEBUG_BLT)
                  logd ("[TB%05d] To be removed (Time_Tracked >= MaxTime_Verified)\n",t_blob->ID);
                  #endif
               }
               else if (VerifyTrackedBlob (t_blob) == DONE) t_blob->Status |= TB_STATUS_VERIFIED | TB_STATUS_VERIFIED_START;
            }
         }
         if (!(t_blob->Status & TB_STATUS_TO_BE_REMOVED)) {
            if (FilterTrackedBlob (t_blob)) {
               t_blob->Status |= TB_STATUS_FILTERED | TB_STATUS_TO_BE_REMOVED;
               #if defined(__DEBUG_BLT)
               logd ("[TB%05d] To be removed (Filtered)\n",t_blob->ID);
               #endif
            }
         }
      }
      t_blob = TrackedBlobs.Next (t_blob);
   }
   FrameCount++;
   return (DONE);
}

 int BlobTracking::Perform (GImage& b_image,GImage* s_image)

{
   if (!IsEnabled (   )) return (1);
   if (!IsInitialized (   )) return (2);
   SrcImage = s_image;
   CleanBlobList (   );
   AddToBRMList  (BlobRegionMap);
   GetCRMap (b_image,MinBlobArea,MaxBlobArea,BlobRegionMap,BlobRegions);
   int e_code = Perform (   );
   if (e_code > 0) return (e_code + 2);
   return (DONE);
}

 int BlobTracking::Perform (IArray2D& cr_map,int n_crs,GImage* s_image)

{
   if (!IsEnabled (   )) return (1);
   if (!IsInitialized (   )) return (2);
   SrcImage = s_image;
   CleanBlobList (   );
   AddToBRMList  (BlobRegionMap);
   BlobRegionMap = cr_map;
   BlobRegions.Create (n_crs + 1);
   GetCRInfo (BlobRegionMap,BlobRegions);
   int e_code = Perform (   );
   if (e_code > 0) return (e_code + 2);
   return (DONE);
}

 int BlobTracking::PutMask (TrackedBlob* t_blob,ushort d_label,USArray2D& d_array)
 
{
   int x,y;
   
   if (!IsEnabled (   )) return (1);
   int sx = t_blob->X;
   int sy = t_blob->Y;
   int ex = sx + t_blob->Width;
   int ey = sy + t_blob->Height;
   int lb = t_blob->Correspondence;
   for (y = sy; y < ey; y++) {
      int*    l_br_map  = BlobRegionMap[y];
      ushort* l_d_array = d_array[y];
      for (x = sx; x < ex; x++)
         if (l_br_map[x] == lb) l_d_array[x] = d_label;
   }
   return (DONE);
}

 void BlobTracking::Reset (   )
 
{
   FrameCount = 0;
   BlobRegions.Delete  (   );
   BRMList.Delete      (   );   
   BlobRegionMap.Clear (   );
   TrackedBlob* t_blob = TrackedBlobs.First (   );
   while (t_blob != NULL) {
      t_blob->Status |= TB_STATUS_TO_BE_REMOVED;
      t_blob = TrackedBlobs.Next (t_blob);
   }
}

 int BlobTracking::TrackBlobRegion (TrackedBlob* t_blob,IArray2D& d_cr_map,int d_cr_no)

{
   if (SrcImage == NULL) return (1);
   IBox2D d_rgn; float m_score;
   int e_code = t_blob->TrackTarget (*SrcImage,d_rgn,m_score);
   if      (e_code > 0) return (2 );
   else if (e_code < 0) return (-1);
   ISize2D cr_size(d_cr_map.Width,d_cr_map.Height);
   CropRegion (cr_size,d_rgn);
   #if defined(__DEBUG_BLT)
   logd ("   Tracked: X = %04d, Y = %04d, W = %04d, H = %04d\n",d_rgn.X,d_rgn.Y,d_rgn.Width,d_rgn.Height);
   #endif
   PutCR (d_cr_map,d_rgn,d_cr_no,FALSE);
   if (t_blob->UpdateCRInfo (d_cr_map,d_rgn,d_cr_no)) return (3);
   t_blob->UpdateRegion (*t_blob,RegionUpdateRate);
   return (DONE);
}

 int BlobTracking::VerifyTrackedBlob (TrackedBlob *t_blob)
 
{
   float md_thrsld1 = MinMovingDistance1 * SrcImgSize.Height / 240.0f;
   float md_thrsld2 = MinMovingDistance2 * SrcImgSize.Height / 240.0f;
   if (t_blob->MovingDistance1 >= md_thrsld1 && t_blob->MovingDistance2 >= md_thrsld2) return (DONE);
   else return (1);
}

}

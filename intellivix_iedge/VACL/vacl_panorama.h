#if !defined(__VACL_PANORAMA_H)
#define __VACL_PANORAMA_H

#include "vacl_cam2cam.h"
#include "vacl_warping.h"

#if defined(__LIB_WIN32CL)
#include "Win32CL/Win32CL.h"
#endif

 namespace VACL

{

#define PM_CYLINDER   0
#define PM_PLANE      1
#define PM_SPHERE     2

#define PAN_OVERLAP_WIDTH   70.0f
#define PAN_WIDTH_SCALE     0.98f

///////////////////////////////////////////////////////////////////////////////
//
// Class: CylindricalProjection 
//
///////////////////////////////////////////////////////////////////////////////

 class CylindricalProjection

{
   protected:
      int       Flag_Closed;
      int       SrcImgWidth;
      int       SrcImgHeight;
      FPoint2D  SrcImgCenter;
      FArray2D  SrcDistMap;
      FArray2D  PanDistMap;
      BGRImage *PanImage;
      Array1D<Matrix> *Mats_K;
      Array1D<Matrix> *Mats_R;

   public: // Input Parameters (Optional)
      float  OverlapWidth;
      float  PIWidthScale;
      double Kappa;
   
   public: // Output Parameters
      float    HFOV;
      float    VFOV;
      Matrix   Mat_H;
      IBox2D   ValidRegion;
      IPoint2D LFMPos;
      FPoint2D MinPos;
      FPoint2D MaxPos;
   
   public:
      CylindricalProjection (   );

   protected:
      void    GetPanImageMappingRange (   );
      ISize2D GetPanImageSize         (   );
      void    PlaneToCylinder         (FPoint2D &s_point,Matrix &mat_T,FPoint2D &d_point,int flag_add_2pi);

   public:
      void Close                   (   );
      void ClosePanorama           (BGRImage &s_cimage,float l_pos = 0.0f,float r_pos = 0.2f);
      void CropNonimageArea        (   );
      void GetSrcImageMappingRange (int img_no,FPoint2D &min_pos,FPoint2D &max_pos);
      void GetSrcImageMappingRange (int img_no,IPoint2D &min_pos,IPoint2D &max_pos);
      void Initialize              (Array1D<Matrix> &mats_K,Array1D<Matrix> &mats_R,int si_width,int si_height,BGRImage &p_image);
      void Perform                 (BGRImage &s_image,int img_no);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: PlanarProjection
//
///////////////////////////////////////////////////////////////////////////////

 class PlanarProjection

{
   protected:
      DArray1D    RDCoeffs;
      FArray2D    SrcDistMap;
      FArray2D    PanDistMap;
      BGRImage   *PanImage;
      IS2DArray1D SrcImgSizes;
      Array1D<Matrix> *Mats_K;
      Array1D<Matrix> *Mats_R;

   public: // Input Parameters (Optional)
      float OverlapWidth;

   public: // Output Parameters
      FPoint2D MinPos;
      FPoint2D MaxPos;
      Array1D<Matrix> Mats_H;

   public:
      PlanarProjection (   );
   
   protected:
      void    GetPanImageMappingRange (   );
      ISize2D GetPanImageSize         (   );
      void    GetHomographies         (   );
      void    Initialize              (BGRImage &p_image);

   public:
      void Close                   (   );
      void CropNonimageArea        (   );
      void GetSrcImageMappingRange (int img_no,FPoint2D &min_pos,FPoint2D &max_pos);
      void GetSrcImageMappingRange (int img_no,IPoint2D &min_pos,IPoint2D &max_pos);
      void Initialize              (Array1D<Matrix> &mats_H,int si_width,int si_height,BGRImage &p_image,double kappa = 0.0);
      void Initialize              (Array1D<Matrix> &mats_H,IS2DArray1D &si_sizes,DArray1D &rd_coeffs,BGRImage &p_image);
      void Initialize              (Array1D<Matrix> &mats_K,Array1D<Matrix> &mats_R,int si_width,int si_height,BGRImage &p_image,double kappa = 0.0);
      void Perform                 (BGRImage &s_image,int img_no);
      void Perform                 (BGRImage &s_image,FArray2D &sd_map,int img_no);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: SphericalProjection
//
///////////////////////////////////////////////////////////////////////////////

 class SphericalProjection

{
   protected:
      int       SrcImgWidth;
      int       SrcImgHeight;
      FPoint2D  SrcImgCenter;
      FArray2D  SrcDistMap;
      FArray2D  PanDistMap;
      BGRImage *PanImage;
      Array1D<Matrix> *Mats_K;
      Array1D<Matrix> *Mats_R;
   
   public: // Input Parameters (Optional)
      float  OverlapWidth;
      float  PIWidthScale;
      double Kappa;

   public: // Output Parameters
      FPoint2D MinPos;
      FPoint2D MaxPos;
   
   public:
      SphericalProjection (   );
   
   protected:
      void    GetPanImageMappingRange (   );
      ISize2D GetPanImageSize         (   );
      void    PlaneToSphere           (FPoint2D &s_point,Matrix &mat_T,int sign,FPoint2D &d_point);

   public:
      void Close                   (   );
      void CropNonimageArea        (   );
      void GetSrcImageMappingRange (int img_no,FPoint2D &min_pos,FPoint2D &max_pos);
      void GetSrcImageMappingRange (int img_no,IPoint2D &min_pos,IPoint2D &max_pos);
      void Initialize              (Array1D<Matrix> &mats_K,Array1D<Matrix> &mats_R,int si_width,int si_height,BGRImage &p_image);
      void Perform                 (BGRImage &s_image,int img_no);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: CylindricalProjectionTable
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__OS_WINDOWS) || defined(__OS_LINUX)
#pragma pack(1)
#endif

 struct CPTE

{
   char     SrcImgNo;
   ushort   BWeight;
   FPoint2D Coord;
};

 struct CPTEP

{
   CPTE Pair[2];
};

#if defined(__OS_WINDOWS) || defined(__OS_LINUX)
#pragma pack() 
#endif

 class CylindricalProjectionTable : public Array2D<CPTEP>

{
   protected:
      FPoint2D MinPos;
      FPoint2D MaxPos;
      FArray2D SrcDistMap;
      FPoint2D SrcImgCenter;
      Array1D<Matrix> *Mats_K;
      Array1D<Matrix> *Mats_R;
 
   protected:
      LensDistortionCorrectionTable *LDCTable;

   public: // Input Parameters
      int NumSrcImages;
      int SrcImgWidth;
      int SrcImgHeight;

   public: // Input Parameters (Optional)
      int   OverlapWidth;
      float PIWidthScale;

   public: // Output Parameters
      float HFOV;

   public:
      CylindricalProjectionTable (   );

   public:
      virtual void Delete (   );

   protected:
      void    GetPanImageMappingRange (   );
      ISize2D GetPanoramicImageSize   (   );
      void    GetSrcImageMappingRange (int img_no,FPoint2D &min_pos,FPoint2D &max_pos);
      void    GetSrcImageMappingRange (int img_no,IPoint2D &min_pos,IPoint2D &max_pos);
      void    JoinTwoEndImages        (IPoint2D &lfm_pos,Array2D<CPTEP> &d_table);
      void    PlaneToCylinder         (FPoint2D &s_point,Matrix &mat_T,FPoint2D &d_point,int flag_add_2pi);

   public:
      void ClosePanorama         (IPoint2D &lfm_pos,Matrix &mat_H);
      void CopyCameraInfo        (CylindricalProjectionTable &s_table);
      void CropNonimageArea      (IBox2D &v_rgn);
      void GetBlendingWeights    (   );
      void Initialize            (Array1D<Matrix> &mats_K,Array1D<Matrix> &mats_R,int si_width,int si_height,LensDistortionCorrectionTable *ldc_table = NULL);
      void Perform               (int img_no,byte **mask = NULL);
      void PerformPostprocessing (   );
      int  ReadFile              (const char *file_name);
      void Resize                (CylindricalProjectionTable &s_table);
      void Rotate180Degrees      (CylindricalProjectionTable &s_table);
      void Warp                  (Array2D<CPTEP> &s_table,Matrix &mat_H);
      int  WriteFile             (const char *file_name);
};

///////////////////////////////////////////////////////////////////////////////
//
// Struct: PICTE
//
///////////////////////////////////////////////////////////////////////////////
//
// PICTE: Panoramic Image Composition Table Element
//

#if defined(__OS_WINDOWS) || defined(__OS_LINUX)
#pragma pack(1)
#endif

 struct PICTE
 
{
   char   SrcImgNo;
   ushort IWeights[4];
   ushort BWeight;
   ushort X,Y;
};

#if defined(__OS_WINDOWS) || defined(__OS_LINUX)
#pragma pack()
#endif

///////////////////////////////////////////////////////////////////////////////
//
// Struct: PICTEP
//
///////////////////////////////////////////////////////////////////////////////
//
// PICTEP: Panoramic Image Composition Table Element Pair
//

#if defined(__OS_WINDOWS) || defined(__OS_LINUX)
#pragma pack(1)
#endif

 struct PICTEP

{
   PICTE Pair[2];
};

#if defined(__OS_WINDOWS) || defined(__OS_LINUX)
#pragma pack()
#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: PanImageCompositionTable
//
///////////////////////////////////////////////////////////////////////////////

#define PICT_TYPE_CYLINDRICAL   0
#define PICT_TYPE_PLANAR        1
#define PICT_TYPE_SPHERICAL     2
#define PICT_TYPE_ARBITRARY     3

 class PanImageCompositionTable : public Array2D<PICTEP>

{
   protected:
      int      Flag_IBCEnabled;
      int      IBCBaseImgIdx;
      FArray1D IBCParams1;
      FArray1D IBCParams2;

   public:
      int   ProjectionType;
      int   NumSrcImages;
      int   SrcImgWidth;
      int   SrcImgHeight;
      float HFOV;

   public:
      PanImageCompositionTable (   );
      
   public:
      int  Create                 (CylindricalProjectionTable &cp_table);
      void EnableIBC              (int flag_enable);
      int  EstimateIBCParams_YUY2 (byte** s_buffers);
      int  GetIBCBaseImgIdx       (   );
      int  IsIBCEnabled           (   );
      int  Perform                (Array1D<BGRImage> &s_images,BGRImage &d_image);
      int  Perform_YUY2           (byte** s_buffers,byte* d_buffer);
      int  Perform_YUY2           (byte** s_buffers,IBox2D& s_rect,byte* d_buffer);
      int  Perform_YUY2           (byte** s_buffers,IBox2D& s_rect,IPoint2D& d_pos,byte* d_buffer,ISize2D& di_size);
      int  ReadFile               (const char* file_name);
      void SetIBCBaseImgIdx       (int ri_idx);
      int  WriteFile              (const char* file_name);
};

 inline void PanImageCompositionTable::EnableIBC (int flag_enable)

{
   Flag_IBCEnabled = flag_enable;
}

 inline int PanImageCompositionTable::GetIBCBaseImgIdx (   )

{
   return (IBCBaseImgIdx);
}

 inline int PanImageCompositionTable::IsIBCEnabled (   )

{
   return (Flag_IBCEnabled);
}

 inline void PanImageCompositionTable::SetIBCBaseImgIdx (int ri_idx)

{
   IBCBaseImgIdx = ri_idx;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CThread_PIC
//
///////////////////////////////////////////////////////////////////////////////
//
// PIC: Panoramic Image Composition
//
 
#if defined(__LIB_WIN32CL)

 class CThread_PIC : public CThread

{
   public:
      int    ThreadIdx;
      int    ParImgWidth;
      byte** SrcImgBuffers;
      byte*  DstImgBuffer;
      PanImageCompositionTable* PICT;

   public:
      CEvent Event_Start;
      CEvent Event_End;
   
   public:
      CThread_PIC (   );
   
   protected:
      virtual void ThreadProc (   );
};

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: PanImageComposition_MT
//
///////////////////////////////////////////////////////////////////////////////
//
// MT: Multithreaded
//

#if defined(__LIB_WIN32CL)

 class PanImageComposition_MT

{
   protected:
      Array1D<HANDLE>      EndEvents;
      Array1D<CThread_PIC> Threads;
   
   public:
      int     NumSrcImages;
      ISize2D PanImgSize;
      ISize2D SrcImgSize;
   
   public:
      PanImageCompositionTable PICT;
   
   public:
      PanImageComposition_MT (   );
      virtual ~PanImageComposition_MT (   );

   protected:
      void _Init (   );
   
   public:
      virtual void Close (   );
   
   public:
      int  Initialize             (int n_threads);
      int  LoadPICT               (const char* file_name);
      void EnableIBC              (int flag_enable);
      int  EstimateIBCParams_YUY2 (byte** s_buffers);
      int  GetIBCBaseImgIdx       (   );
      int  IsIBCEnabled           (   );
      int  Perform_YUY2           (byte** s_buffers,byte* d_buffer);
      void SetIBCBaseImgIdx       (int ri_idx);
};
// [참고] 처음에 LoadPICT() 함수를 호출하여 Panoramic Image Composition Table을 로딩한 후,
//        Initialize( ) 함수를 호출하여 쓰레드들을 생성하고 구동시킨다.

 inline void PanImageComposition_MT::EnableIBC (int flag_enable)

{
   PICT.EnableIBC (flag_enable);
}

 inline int PanImageComposition_MT::EstimateIBCParams_YUY2 (byte** s_buffers)

{
   return (PICT.EstimateIBCParams_YUY2 (s_buffers));
}

 inline int PanImageComposition_MT::GetIBCBaseImgIdx (   )

{
   return (PICT.GetIBCBaseImgIdx (   ));
}

 inline int PanImageComposition_MT::IsIBCEnabled (   )

{
   return (PICT.IsIBCEnabled (   ));
}

 inline void PanImageComposition_MT::SetIBCBaseImgIdx (int ri_idx)

{
   PICT.SetIBCBaseImgIdx (ri_idx);
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: CylindricalPanorama
//
///////////////////////////////////////////////////////////////////////////////

 class CylindricalPanorama
 
{
   public: // Input Parameters
      int    NumCameras;   // 카메라 수
      int    CamImgWidth;  // 원 카메라 영상의 너비
      int    CamImgHeight; // 원 카메라 영상의 높이
      double Kappa1;       // 렌즈 왜곡 계수 (1차)
      double Kappa2;       // 렌즈 왜곡 계수 (2차)

   public: // Input/Output Parameters
      double FocalLength;  // 렌즈 왜곡 제거 및 크롭핑이 된 카메라 영상의 초점 거리

   public: // Input Parameters (Optional)
      int   Flag_Closed;         // 본 값이 TRUE이면 Closed Panorama (360도 파노라마) 영상을 생성한다.
      int   Flag_FixFocalLength; // 본 값이 TRUE이면 LIR를 통한 최적화 수행 시 고정된 초점 거리 값을 이용한다.
      float OverlapWidth;        // 인접하는 카메라 영상 간의 겹치는 부분에서 블랜딩 적용 폭
      float PIWidthScale;        // 생상되는 파노라마 영상의 너비의 스케일

   public: // Output Parameters
      int      SrcImgWidth1;   // 렌즈 왜곡이 제거된 카메라 영상의 너비
      int      SrcImgHeight1;  // 렌즈 왜곡이 제거된 카메라 영상의 높이
      int      SrcImgWidth2;   // 렌즈 왜곡 제거 및 크롭핑이 된 카메라 영상의 너비
      int      SrcImgHeight2;  // 렌즈 왜곡 제거 및 크롭핑이 된 카메라 영상의 높이
      double   FocalLength1;   // 렌즈 왜곡이 제거되고 크롭핑이 되지 않은 카메라 영상의 초점 거리
      IBox2D   ValidRegion_SI;
      IBox2D   ValidRegion_PI;
      Matrix   Mat_H;
      FPoint2D CamImgCenter;
      IPoint2D LFMPos;
      BGRImage PanImage;
      Array1D<Matrix> Mats_H1; // 렌즈 왜곡이 제거되고 크롭핑이 되지 않은 이웃 카메라 영상들 사이의 호모그래피들
      Array1D<Matrix> Mats_H2; // 렌즈 왜곡이 제거되고 크롭핑이 된 이웃 카메라 영상들 사이의 호모그래피들
      Array1D<Matrix> Mats_K;  // 이웃 카메라 영상들 간의 호모그래피들로부터 구한 각 카메라의 캘리브레이션 행렬
      Array1D<Matrix> Mats_R;  // 이웃 카메라 영상들 간의 호모그래피들로부터 구한 각 카메라의 회전 행렬

   public: // Output Parameters
      CylindricalProjectionTable    CPTable;
      LensDistortionCorrectionTable LDCTable;

   public:
      CylindricalPanorama (   );
   
   protected:
      void _Init          (   );
      int  GetLeftMargin  (BGRImage &s_image); // 영상의 왼쪽 검은 띠 영역을 찾는다.
      int  GetRightMargin (BGRImage &s_image); // 영상의 오른쪽 검은 띠 영역을 찾는다.

   public:
      void  Close                               (   );
      void  CorrectCameraParameters             (   );
         // 이웃하는 카메라 영상 간의 호모그래피를 PerformLocalImageRegistration() 함수를 통해 모두 계산한 후
         // 본 함수를 호출하여 카메라 파라미터들을 수정한다.
      void  CreateCylindricalProjectionTable    (Array1D<GImage> *m_images = NULL);
         // m_images: 각 카메라 영상에 대응하는 마스크 영상의 배열의 포인터
         //           마스크 영상에 표시된 영역에서만 스티칭이 이루어진다.
         //           본 인자 값이 NULL이면 카메라 영상의 모든 영역에서 스티칭이 이루어진다.
      void  CreateLensDistortionCorrectionTable (   );
      void  CreateMaskImages                    (Array1D<GImage> &s_images);
      void  CreatePanoramicImage                (Array1D<BGRImage> &s_images);
      void  EstimateFocalLength                 (GImage &r_image,GImage &s_image);
         // 이웃하는 두 카메라 영상을 정합함에 의해 초점 거리 값을 추정한다.
         // 본 함수 호출 후 FocalLength 변수 값이 추정된 초점 거리 값으로 바뀐다.
      void  GetProjectionMatrices               (   );
      float GetResolutionReductionFactor        (   );
      void  Initialize                          (int n_cameras,int si_width = 0,int si_height = 0,double f_length = 0.0,double kappa1 = 0.0,double kappa2 = 0.0);
         // 본 함수 호출 시 n_cameras 값만 지정하는 경우 바로 이어서 LoadLensDistortionCorrectionTable() 함수를
         // 호출하여 렌즈 왜곡 보정 테이블을 로드해야 한다. 렌즈 왜곡 보정 테이블을 로드하지 않고 진행하려면
         // 본 함수 호출 시 최소한 n_cameras, si_width, si_height 값을 지정해야 한다.
      int   LoadCylindricalProjectionTable      (const char *file_name);
      int   LoadLensDistortionCorrectionTable   (const char *file_name);
      int   LoadSourceImage                     (const char *file_name,BGRImage &d_image,int flag_crop = FALSE);
      int   LoadSourceImage                     (const char *file_name,GImage &d_image,int flag_crop = FALSE);
      int   PerformLocalImageRegistration       (GImage &r_image,GImage &s_image,int img_no);
      void  RenderPanoramicImage                (Array1D<BGRImage> &s_images);
      int   SaveCylindricalProjectionTable      (const char *file_name,int si_width,int si_height,int cpt_width,int cpt_height,int flag_rotation);
      int   SaveLensDistortionCorrectionTable   (const char *file_name);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: PanoramaToPTZCamera
//
///////////////////////////////////////////////////////////////////////////////

 class PanoramaToPTZCamera
 
{
   protected:
      Array1D<Matrix> Mats_IP;
      MasterToSlaveCamera2 M2SC;
      PanImageCompositionTable *PICT;
   
   public: // Input Parameters
           // Initialize() 함수 또는 ReadFile() 함수를 통해 설정된다.
      int   SrcImgWidth;       // 소스 영상의 너비
      int   SrcImgHeight;      // 소스 영상의 높이
      int   PanImgWidth;       // 파노라마 영상의 너비
      int   PanImgHeight;      // 파노라마 영상의 높이
      float SrcImgFocalLength; // 소스 영상의 초점 거리
      float PanScanTiltPos;    // Panoramic Scan 시 PTZ 카메라의 틸트 위치 값
      float PanScanZoomPos;    // Panoramic Scan 시 PTZ 카메라의 줌 위치 값

   public: // Output Parameters
           // Calibrate() 함수 호출 후에 설정된다.
      float PanImgFocalLength; // 파노라마 영상의 초점 거리
      float PanImgHFOV;        // 파노라마 영상의 수평 화각
   
   public:
      PanoramaToPTZCamera (   );
   
   protected:
      void _Init (   );
   
   public:
      int  Calibrate              (   );
         // 파노라마 영상의 픽셀 좌표와 PT 포지션 간의 변환 관계를 구한다.
      void Close                  (   );
      int  ConvertPixelPosToPTPos (int px,int py,float &pan_pos,float &tilt_pos);
         // 파노라마 영상 합성 테이블을 이용하여 파노라마 영상의 특정 픽셀 좌표에 해당하는 PT 포지션을 구한다.
      void GetPixelPos            (float pan_pos,float tilt_pos,int &px,int &py);
         // 주어진 PT 포지션에 대응하는 파노라마 영상에서의 픽셀 좌표를 구한다.
      void GetPTPos               (int px,int py,float &pan_pos,float &tilt_pos);
         // 주어진 파노라마 영상에서의 픽셀 좌표에 대응하는 PT 포지션을 구한다.
      void GetPTZPos              (IBox2D &s_rect,float &pan_pos,float &tilt_pos,float &zoom_pos);
         // 주어진 파노라마 영상에서의 영역에 대응하는 PTZ 포지션을 구한다.
      void GetViewRect            (float pan_pos,float tilt_pos,float zoom_pos,IBox2D &d_rect);
         // 주어진 PTZ 포지션에 대응하는 파노라마 영상에서의 대응 영역을 구한다.
      void Initialize             (PanImageCompositionTable &pict,PTZPArray1D &cp_array,float f_length,float tilt_pos,float zoom_pos);
         // pict   : Panoramic Scan 시 얻은 소스 영상들로부터 파노라마 영상을 합성할 때 사용되는 테이블
         // cp_array: Panoramic Scan 시 각 소스 영상을 획득한 위치(P/T/Z)에 대한 배열
         //         : Pan/Tilt 위치 값은 Degree로 주어져야 한다.
         //         : Zoom 위치 값은 사용되지 않는다.
         // f_length: Panoramic Scan 시 카메라 초점 거리
         //         : [주의] 원 카메라 영상 기준이 아닌 파노라마 영상 생성 시 사용되는 소스 영상 기준으로 주어져야 한다.
         // tilt_pos: Panoramic Scan 시 PTZ 카메라의 틸트 위치 값
         // zoom_pos: Panoramic Scan 시 PTZ 카메라의 줌 위치 값
      int ReadFile                (FileIO &file);
      int ReadFile                (CXMLIO *xmlio);
      int ReadFile                (const char *file_name);
      int WriteFile               (FileIO &file);
      int WriteFile               (CXMLIO *xmlio);
      int WriteFile               (const char *file_name);
};

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

void CombineTwoImages             (BGRImage &r_image,BGRImage &s_image,Matrix &mat_H,BGRImage &d_image,int prj_method = PM_PLANE,float scale = 1.0f,double kappa = 0.0,float ovl_width = PAN_OVERLAP_WIDTH);
void CropNonimageArea_Horizontal  (BGRImage &s_image,BGRImage &d_image,int &sy,int &ey);
void CropNonimageArea_Vertical1   (BGRImage &s_image,BGRImage &d_image,int &sx,int &ex);
void CropNonimageArea_Vertical2   (BGRImage &s_image,BGRImage &d_image,int &sx,int &ex);
void GetDistanceMapForBlending    (FArray2D &d_array);
void PerformCylindricalProjection (GImage &s_image,double f_length,FPoint2D &lt_pos,FPoint2D &rb_pos,GImage &d_image);

}

#endif

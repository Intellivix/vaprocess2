#include "vacl_objclass.h"
#include "vacl_binimg.h"
#include "vacl_edge.h"
#include "vacl_event.h"
#include "vacl_geometry.h"
#include "vacl_histo.h"
#include "vacl_objtrack.h"
#include "VAEngine.h"

#if defined(__DEBUG_OBT) || defined(__DEBUG_FPT)
#include "Win32CL/Win32CL.h"
#endif
#if defined(__DEBUG_OBT)
extern int _DVX_OBT,_DVY_OBT;
extern CImageView* _DebugView_OBT;
#endif
#if defined(__DEBUG_FPT)
extern int _DVX_FPT,_DVY_FPT;
extern CImageView* _DebugView_FPT;
#endif

// #define __CONSOLE

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Definitions
//
///////////////////////////////////////////////////////////////////////////////

#define OBT_PARAM_MAX_AREA_CHANGED_ABRUPTLY  100.0f // 현재는 관련 기능 OFF. 재 검토 필요.
#define OBT_PARAM_MAX_NONSTATIC_TIME         5.0f

///////////////////////////////////////////////////////////////////////////////
//
// Global Variables/Functions
//
///////////////////////////////////////////////////////////////////////////////

extern int __Flag_VACL_Initialized;
extern int __CheckLicenseKey (   );

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectTracking
//
///////////////////////////////////////////////////////////////////////////////

 ObjectTracking::ObjectTracking (   )

{
   Flag_Enabled        = TRUE;
   Flag_Calibrated     = FALSE;
   ObjIDGenerator      = NULL;

   Options             = 0;
   SearchRange         = 7;

   MaxTrjLength        = 10.0f;
   
   ObjUpdateRate       = 0.15f;
   
   MinBndEdgeStrength  = 0.3f;

   MinMovingDistance   = 8.0f / 240.0f; // 실제 최소 이동 거리 = 240.0f * MinMovingDistance
   MinMovingDistance2  = 0.0f;
   OuterZoneSize1      = 0.0f;
   OuterZoneSize2      = 0.0f;

   FocalLength         = 0.0f;
   CamTiltAngle        = 0.0f;
   CamHeight           = 0.0f;
   
   RefImgSize(REF_IMAGE_WIDTH,REF_IMAGE_HEIGHT);
   HumanSize(0.8f,1.7f);

   MinCount_Tracked    = 3;
   MaxTime_FPT         = 1.5f;
   MaxTime_Lost        = 1.0f;
   MaxTime_Static      = 300.0f;
   MaxTime_Undetected  = 0.0f;
   MaxTime_Undetected2 = 0.3f;
   MinTime_Tracked     = 0.8f;

   FrameRate           = 7.0f;
   FrameDuration       = 1.0f / FrameRate;
   ForegroundDetector  = NULL;
   _Init (   );

   #if defined(__CPNI_CERT_SZM)
   Options             = OBT_OPTION_PERFORM_FAST_MOVING_OBJECT_DETECTION;
   MinMovingDistance   = 11.2f / 240.0f; // 설정 UI 상에서 디폴트 단계보다 2단계 위에 해당
   OuterZoneSize1      = 0.1f;
   OuterZoneSize2      = 0.15f;
   MaxTime_Static      = 150.0f;
   MinTime_Tracked     = 2.5f;
   #endif

   #if defined(__KISA_CERT_OLD)
   MinMovingDistance   = 11.2f / 240.0f;
   MinMovingDistance2  = 1.0f;
   MaxTime_Static      = 180.0f;
   MinTime_Tracked     = 1.5f;
   #endif
}

 ObjectTracking::~ObjectTracking (   )
 
{
   Close (   );
}

 void ObjectTracking::_Init (   )
 
{
   NumObjLayers  =  1;
   FrameCount    =  0;
   ObjectCount   = -1;
   #if defined(__LIB_OPENCV)
   PrvImgPyramid = &ImagePyramid1;
   CurImgPyramid = &ImagePyramid2;
   #endif
}

 void ObjectTracking::ChangeObjectRegionLabel (IArray2D &s_map,IBox2D &s_rgn,int s_no,int d_no)

{
   int x,y;

   int sx = s_rgn.X;
   int sy = s_rgn.Y;   
   int ex = sx + s_rgn.Width;
   int ey = sy + s_rgn.Height;
   for (y = sy; y < ey; y++) {
      int* l_s_map = s_map[y];
      for (x = sx; x < ex; x++)
         if (l_s_map[x] == s_no) l_s_map[x] = d_no;
   }   
}

 int ObjectTracking::CheckBlobOverlapWithTrackingArea (IArray2D &s_map,IBox2D &s_rgn,int s_no)

{
   int x,y;

   int n_tps = 0;
   int n_ops = 0;
   int sx = s_rgn.X;
   int sy = s_rgn.Y;
   int ex = sx + s_rgn.Width;
   int ey = sy + s_rgn.Height;
   for (y = sy; y < ey; y++) {
      byte* l_t_map = TrkAreaMap[y];
      int*  l_s_map = s_map[y];
      for (x = sx; x < ex; x++) {
         if (l_s_map[x] == s_no) {
            n_tps++;
            if (l_t_map[x] == PG_WHITE) n_ops++;
         }
      }
   }
   if (!n_tps) return (1);
   float overlap = (float)n_ops / n_tps;
   if (overlap < 0.85f) return (2);
   return (DONE);
}

 int ObjectTracking::CheckObjectInDynamicBackgroundArea (TrackedObject *t_object)

{
   int cx = (int)(t_object->CenterPos.X + 0.5f);
   int cy = (int)(t_object->CenterPos.Y + 0.5f);
   if (cx < 0 || cy < 0 || cx >= SrcImgSize.Width || cy >= SrcImgSize.Height) return (1);
   if (DynBkgAreaMap[cy][cx] == PG_WHITE) return (DONE);
   return (2);
}

 int ObjectTracking::CheckObjectInitMotion (TrackedObject *t_object)
// 리턴 값 > 0: 노이즈로 판별한 경우
// 리턴 값 < 0: 판별을 유보하는 경우
// 리턴 값 = 0: 유효 객체로 판별한 경우
{
   if (t_object->HumanRect.Height) { // Camera Calibration이 되어 있는 경우
      // 객체의 높이가 사람 객체의 범위 밖에 있는 경우에는 즉시 그 객체를 제거한다.
      int min_h = (int)(0.7f * t_object->HumanRect.Height); // PARAMETERS
      int max_h = (int)(1.6f * t_object->HumanRect.Height); // PARAMETERS
      if (t_object->Height < min_h || t_object->Height > max_h) return (1);
      // 객체의 너비 및 높이가 사람 객체의 범위 안에 있으면 즉시 그 객체를 검증된 객체로 인정한다.
      int min_w = (int)(0.7f * t_object->HumanRect.Width);  // PARAMETERS
      int max_w = (int)(1.3f * t_object->HumanRect.Width);  // PARAMETERS
      if (min_w <= t_object->Width && t_object->Width <= max_w) return (DONE);
   }
   t_object->UpdateNoisiness (SrcImgSize);
   if (t_object->Noisiness >= 2.0f) return (3);
   if (t_object->Count_Tracked >= MinCount_Tracked) {
      // 절대 이동 거리 체크를 위한 임계치를 설정한다.
      float md_thrsld0 = MinMovingDistance * 240.0f; // 기본
      float md_thrsld1 = 0.3f * md_thrsld0;          // 바운더리 영역에 있는 객체
      float md_thrsld2 = 0.7f * md_thrsld0;          // 멀리 있는 사람
      float md_thrsld3 = 1.5f * md_thrsld0;          // 차량
      float md_thrsld4 = 2.0f * md_thrsld0;          // 미확인 객체
      float md_thrsld5 = 3.0f * md_thrsld0;          // 빠른 이동 객체
      float md_thrsld6 = 1.0f * GetMinimum (t_object->Width,t_object->Height); // 빠른 이동 객체
      // 상대 이동 거리(객체의 크기에 비례하는 이동 거리) 체크를 위한 임계치를 설정한다.
      float to_size    = 0.5f * (GetMaximum (t_object->Width,(int)t_object->AvgWidth) + GetMaximum (t_object->Height,(int)t_object->AvgHeight));
      if (to_size > 0.2 * SrcImgSize.Height) to_size = 0.0f; // 큰 객체에 대해서는 초기 이동 거리 체크를 하지 않는다.
      float md_thrsld9 = MinMovingDistance2 * to_size;
      // 객체의 초기 이동 거리를 구한다.
      float md1 = t_object->GetMovingDistance1 (   ); // 중심의 이동 거리
      float md2 = t_object->GetMovingDistance2 (   ); // 네 코너의 이동 거리 중 최소값
      float md3 = 0.5f * (md1 + md2);
      // 객체의 추적 시간이 MinTime_Tracked 이상인 객체들에 대해 처리
      if (t_object->Time_Tracked >= MinTime_Tracked) {
         // 객체가 OuterZone에 있으면 최초 이동 거리의 임계치를 정상보다 낮은 값으로 적용한다.
         // 매우 느리게 진입하는 객체를 10초 내에 유효 객체로 판정하기 위함이다.
         if ((t_object->Status & TO_STATUS_IN_OUTER_ZONE) && md1 >= md_thrsld1 && md3 >= md_thrsld9) return (DONE);
         // 객체가 사람일 가능성이 큰 경우
         if (1.0f <= t_object->AspectRatio && t_object->AspectRatio <= 4.5f) {
            if (md3 >= md_thrsld0 && md3 >= md_thrsld9) return (DONE);
            // 객체가 멀리 있는 사람일 가능성이 큰 경우
            if (t_object->AxisLengthRatio >= 1.5f && t_object->MajorAxisAngle2 <= 35.0f && t_object->Height <= 30 && md3 >= md_thrsld2 && md3 >= md_thrsld9) return (DONE);
         }
         // 객체가 차량일 가능성이 큰 경우
         else if (t_object->AxisLengthRatio <= 5.0f && md3 >= md_thrsld3 && md3 >= md_thrsld9) return (DONE);
         // 그 외?
         else if (md3 >= md_thrsld4 && md3 >= md_thrsld9) return (DONE);
      }
      // 객체의 추적 시간이 MinTime_Tracked 미만인 객체들에 대해 처리
      else {
         // 빠르게 이동하는 객체인 경우
         if (md3 >= md_thrsld5 && md2 >= md_thrsld6 && md3 >= md_thrsld9) return (DONE);
      }
   }
   return (-1);
}

 int ObjectTracking::CheckObjectInNonHandoffArea (TrackedObject *t_object)
 
{
   int cx = (int)(t_object->CenterPos.X + 0.5f);
   int cy = (int)(t_object->CenterPos.Y + 0.5f);
   if (cx < 0 || cy < 0 || cx >= SrcImgSize.Width || cy >= SrcImgSize.Height) return (1);
   if (NonHandoffAreaMap[cy][cx] == PG_WHITE) return (DONE);
   return (2);
}

 int ObjectTracking::CheckObjectInTrackingArea (TrackedObject *t_object)

{
   int cx = (int)(t_object->CenterPos.X + 0.5f);
   int cy = (int)(t_object->CenterPos.Y + 0.5f);
   if (cx < 0 || cy < 0 || cx >= SrcImgSize.Width || cy >= SrcImgSize.Height) {
      int sx = t_object->X;
      int sy = t_object->Y;
      int ex = sx + t_object->Width;
      int ey = sy + t_object->Height;
      if (sx < 0) sx = 0;
      if (sy < 0) sy = 0;
      if (ex > SrcImgSize.Width ) ex = SrcImgSize.Width;
      if (ey > SrcImgSize.Height) ey = SrcImgSize.Height;
      int w = ex - sx;
      int h = ey - sy;
      if (w <= 0 || h <= 0) return (1);
      float r = (float)(w * h) / (t_object->Width * t_object->Height);
      if (r < 0.25f) return (2);
      else return (DONE);
   }
   else if (TrkAreaMap[cy][cx] == PG_WHITE) return (DONE);
   else return (3);
}

 int ObjectTracking::CheckObjectStatic (TrackedObject *t_object)
 
{
   // [주의] TO가 TO_STATUS_VALID_SPEED 상태가 되려면 최소 1초 이상 추적을 한 상태여야 한다.
   int r_code = DONE;
   // TO_STATUS_STATIC1 조건에 대해 검토한다.
   if (t_object->Status & TO_STATUS_STATIC1) {
      if (!t_object->IsStatic1 (   )) {
         t_object->Status &= ~TO_STATUS_STATIC1;
         t_object->ResetRegion_Static (   );
         r_code = 2;
      }
   }
   else {
      if ((t_object->Status & TO_STATUS_VALID_SPEED) && (t_object->Speed <= 2.0f)) { // PARAMETERS
         t_object->Status |= TO_STATUS_STATIC1;
         t_object->SetRegion_Static (    );
      }
      else r_code = 1;
   }
   // TO_STATUS_STATIC2 조건에 대해 검토한다.
   if (t_object->Status & TO_STATUS_STATIC2) {
      if (!t_object->IsStatic2 (   )) {
         t_object->Status &= ~TO_STATUS_STATIC2;
         t_object->ResetPos_Static (   );
      }
   }
   else {
      if ((t_object->Status & TO_STATUS_VALID_SPEED) && (t_object->Speed <= 3.0f)) { // PARAMETERS
         t_object->Status |= TO_STATUS_STATIC2;
         t_object->SetPos_Static (   );
      }
   }
   return (r_code);
}

 int ObjectTracking::CheckObjectStatus (int status)

{
   TrackedObject* t_object = TrackedObjects.First (   );
   while (t_object != NULL) {
      if (t_object->Status & status) return (DONE);
      t_object = TrackedObjects.Next (t_object);
   }
   return (1);
}

 int ObjectTracking::CheckRegionInMarginalArea (IBox2D& s_rgn)

{
   int margin = (int)(SrcImgSize.Height * 0.05f);
   int sx1 = margin;
   int sy1 = margin;
   int ex1 = SrcImgSize.Width  - margin;
   int ey1 = SrcImgSize.Height - margin;
   int sx2 = s_rgn.X;
   int sy2 = s_rgn.Y;
   int ex2 = sx2 + s_rgn.Width;
   int ey2 = sy2 + s_rgn.Height;
   if (sx1 < sx2 && ex2 < ex1 && sy1 < sy2 && ey2 < ey1) return (1);
   return (DONE);
}

 int ObjectTracking::CheckSetup (ObjectTracking* ot)

{
   int ret_flag = 0;
   FileIO buffThis,buffFrom;   
   CXMLIO xmlIOThis,xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis,XMLIO_Write);
   xmlIOFrom.SetFileIO (&buffFrom,XMLIO_Write);
   WriteFile (&xmlIOThis);
   ot->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom))
      ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength (0);
   buffFrom.SetLength (0);
   if (ret_flag & CHECK_SETUP_RESULT_CHANGED) {
      // 추적, 지면, 동적배경 영역중 하나라도 변경되었으면 VACL 전체 재시작
      xmlIOThis.SetFileIO (&buffThis,XMLIO_Write);
      xmlIOFrom.SetFileIO (&buffFrom,XMLIO_Write);
      TrackingAreas.WriteFile        (&xmlIOThis,"TA");
      NontrackingAreas.WriteFile     (&xmlIOThis,"NTA");
      DynBkgAreas.WriteFile          (&xmlIOThis,"DBA");
      NonHandoffAreas.WriteFile      (&xmlIOThis,"NHOA");
      ot->TrackingAreas.WriteFile    (&xmlIOFrom,"TA");
      ot->NontrackingAreas.WriteFile (&xmlIOFrom,"NTA");
      ot->DynBkgAreas.WriteFile      (&xmlIOFrom,"DBA");
      ot->NonHandoffAreas.WriteFile  (&xmlIOFrom,"NHOA");
      if (buffThis.IsDiff (buffFrom))
         ret_flag |= CHECK_SETUP_RESULT_RESTART_ALL;
      buffThis.SetLength (0);
      buffFrom.SetLength (0);
   }
   return (ret_flag);
}

 int ObjectTracking::CheckStaticObjectConditions (TrackedObject* t_object)

{
   if (!t_object->NumBndPixels) return (1);
   if (!(t_object->Status & TO_STATUS_STATIC2)) return (2);
   // 객체 영역 바운더리에서의 전경 에지의 세기가 약하면 Ghost Object로 판단한다.
   if (t_object->BndEdgeStrength <= MinBndEdgeStrength) t_object->Status |= TO_STATUS_GHOST_OBJECT;
   // 객체 영역 바운더리에서의 전경 에지의 양이 배경 에지의 양보다 많으면 Abandoned Object로 판단한다.
   if      (!t_object->NumFrgBndEdgePixels) t_object->Status |= TO_STATUS_REMOVED_OBJECT;
   else if (!t_object->NumBkgBndEdgePixels) t_object->Status |= TO_STATUS_ABANDONED_OBJECT;
   else {
      float r1 = (float)t_object->NumFrgBndEdgePixels / t_object->NumBkgBndEdgePixels;
      float r2 = (float)t_object->NumBkgBndEdgePixels / t_object->NumFrgBndEdgePixels;
      float r  = GetMinimum (r1,r2);
      /*
      logd ("[Tracked Object #%04d]\n",t_object->ID);
      logd ("- NumBkgBndEdgePixels = %d\n",t_object->NumBkgBndEdgePixels);
      logd ("- NumFrgBndEdgePixels = %d\n",t_object->NumFrgBndEdgePixels);
      logd ("- r1 = %.2f, r2 = %.2f, r = %.2f\n",r1,r2,r);
      */
      if (r < 0.75f) { // PARAMETERS
         // 객체 영역 바운더리에서 전경 에지와 배경 에지의 세기 차가 큰 경우
         if (t_object->NumFrgBndEdgePixels > t_object->NumBkgBndEdgePixels) t_object->Status |= TO_STATUS_ABANDONED_OBJECT;
         else t_object->Status |= TO_STATUS_REMOVED_OBJECT;
      }
      else {
         // 객체 영역 바운더리에서 전경 에지와 배경 에지의 세기 차가 작은 경우
         t_object->Status |= TO_STATUS_ABANDONED_OBJECT | TO_STATUS_REMOVED_OBJECT;
      }
   }
   if (t_object->Status & TO_STATUS_ABANDONED_OBJECT) t_object->Count_Abandoned++;
   if (t_object->Status & TO_STATUS_REMOVED_OBJECT  ) t_object->Count_Removed++;
   return (DONE);
}

 void ObjectTracking::GetObjectProperties (TrackedObject* t_object)

{
   // 함수 호출 순서를 바꾸지 말 것!
   t_object->GetPrincipalAxes      (   );
   t_object->GetArea               (   );
   t_object->GetAspectRatio        (   );
   t_object->GetAxisLengthRatio    (   );
   t_object->GetMajorAxisAngle1    (   );
   t_object->GetPathLength         (   );
   t_object->GetMeanArea           ((int)FrameRate);
   t_object->GetMeanVelocity       ((int)FrameRate,t_object->MeanVelocity);
   t_object->GetSpeeds             (FrameRate);
   t_object->GetMajorAxisAngle2    (   );
   t_object->GetMaxMovingDistance  (   );
   t_object->GetAreaVariation      (   );
   t_object->GetDispersedness      (   );
   GetObjectRegionBoundaryEdgeInfo (t_object);
}

 void ObjectTracking::CleanTrackedObjectList (   )
 
{
   VACL::CleanTrackedObjectList (TrackedObjects);
}

 void ObjectTracking::ClearAllAreaLists (   )
 
{
   TrackingAreas.Delete    (   );
   NonHandoffAreas.Delete  (   );
   NontrackingAreas.Delete (   );
   DynBkgAreas.Delete      (   );
}

 void ObjectTracking::Close (   )

{
   MaskImage.Delete         (   );
   SrcImgSize.Clear         (   );
   DynBkgAreaMap.Delete     (   );
   NonHandoffAreaMap.Delete (   );
   TrkAreaMap.Delete        (   );
   ObjRegionMap.Delete      (   );
   LostObjects.Delete       (   );
   TrackedObjects.Delete    (   );
   BlobTracker.Close        (   );
   FMODetector.Close (   );
   _Init (   );
}

 void ObjectTracking::CopyBlobRegion (IArray2D &s_map,IBox2D &s_rgn,int s_no,int d_no,IArray2D &d_map)

{
   int x,y;
   
   int sx = s_rgn.X;
   int sy = s_rgn.Y;
   int ex = sx + s_rgn.Width;
   int ey = sy + s_rgn.Height;
   if (sx < 1) sx = 1;
   if (sy < 1) sy = 1;
   if (ex > s_map.Width  - 1) ex = s_map.Width  - 1;
   if (ey > s_map.Height - 1) ey = s_map.Height - 1;
   for (y = sy; y < ey; y++) {
      int* l_s_map = s_map[y];
      int* l_d_map = d_map[y];
      for (x = sx; x < ex; x++) {
         if (l_s_map[x] == s_no && !l_d_map[x]) l_d_map[x] = d_no;
      }
   }
}

 void ObjectTracking::CopyResidualBlobRegion (IArray2D& s_map,IBox2D& s_rgn,int s_no,TrackedObject* t_object,int d_no,IArray2D& d_map)

{
   int i;
   
   int sx1 = s_rgn.X;
   int sy1 = s_rgn.Y;
   int ex1 = sx1 + s_rgn.Width;
   int ey1 = sy1 + s_rgn.Height;
   int sx2 = t_object->MatchingPos.X;
   int sy2 = t_object->MatchingPos.Y;
   int ex2 = sx2 + t_object->MaskImage.Width;
   int ey2 = sy2 + t_object->MaskImage.Height;
   int sx3 = GetMinimum (sx1,sx2);
   int sy3 = GetMinimum (sy1,sy2);
   int ex3 = GetMaximum (ex1,ex2);
   int ey3 = GetMaximum (ey1,ey2);
   GImage t_image(ex3 - sx3,ey3 - sy3);
   t_image.Clear (   );
   IPoint2D d_pos(sx1 - sx3,sy1 - sy3);
   CopyCR (s_map,s_rgn,s_no,t_image,d_pos,PG_WHITE);
   t_object->PutMask (t_object->MatchingPos.X - sx3,t_object->MatchingPos.Y - sy3,PG_BLACK,t_image,TRUE);
   IArray2D cr_map(t_image.Width,t_image.Height);
   int n_crs = LabelCRs (t_image,cr_map);
   if (!n_crs) return;
   CRArray1D cr_array(n_crs + 1);
   GetCRInfo (cr_map,cr_array);
   int max_i = 0, max_a = 0;
   for (i = 1; i < cr_array.Length; i++) {
      if (cr_array[i].Area > max_a) {
         max_a = cr_array[i].Area;
         max_i = i;
      }
   }
   ConnectedRegion& cr = cr_array[max_i];
   d_pos(cr.X + sx3,cr.Y + sy3);
   CopyCR (cr_map,cr,max_i,d_map,d_pos,d_no);
}

 void ObjectTracking::Enable (int flag_enable)

{
   Flag_Enabled = flag_enable;
   BlobTracker.Enable (flag_enable);
}

 TrackedObject *ObjectTracking::FindMatchingObjectInLostObjectList (TrackedObject* t_object)

{
   int i;

   int n_items = LostObjects.GetNumItems (   );
   if (!n_items) return (NULL);
   Array1D<TrackedObject*> m_objects(n_items);
   float max_pe = 20.0f * ObjRegionMap.Height / 240.0f;
   n_items = 0;
   TrackedObject *l_object = LostObjects.First (   );
   while (l_object != NULL) {
      if (l_object->Status & TO_STATUS_UNDETECTED) {
         if (t_object->InitFrameCount > l_object->InitFrameCount) {
            FPoint2D cd = t_object->CenterPos - l_object->CenterPos;
            float m_cd = Mag(cd);
            float m_lv = Mag(l_object->MeanVelocity);
            float e_md = m_lv * l_object->Time_Lost * FrameRate; // 예상 이동 거리
            // l_object와 t_object 사이의 거리가 (e_md + max_pe)보다 작은 경우
            if (m_cd < e_md + max_pe) {
               float ar = (float)t_object->Area / l_object->MeanArea;
               // l_object와 t_object의 면적이 유사한 경우
               if (0.5f <= ar && ar <= 2.0f) {
                  // l_object가 정지한 객체인 경우
                  if (m_lv < 0.05f) m_objects[n_items++] = l_object;
                  // l_object가 일반 이동 객체인 경우
                  else {
                     FPoint2D lv = l_object->MeanVelocity / m_lv;
                     if (m_cd >= 0.05f) {
                        cd /= m_cd;
                        float ag = (float)(MC_RAD2DEG * acos (IP(cd,lv)));
                        // l_object의 진행 방향과 (l_object -> t_object) 방향이 유사한 경우
                        if (ag <= 60.0f) m_objects[n_items++] = l_object;
                     }
                     else m_objects[n_items++] = l_object;
                  }
               }
            }
         }
      }
      l_object = LostObjects.Next (l_object);
   }
   // m_objects 배열에 등록된 l_object들 중에 t_object와의 히스토그램 차이가 제일 작은 l_object를 찾는다.
   // 찾은 l_object와 t_object 사이의 히스토그램 차이가 일정량 이하이면, 찾은 l_object를 t_object와
   // 매칭이 되는 최종 객체로 선정한다.
   int   min_i = 0;
   float min_h = 1.0f;
   for (i = 0; i < n_items; i++) {
      float h = 1.0f - t_object->GrayHistogram.GetSimilarity (m_objects[i]->GrayHistogram);
      if (h < min_h) min_h = h, min_i = i;
   }
   if (min_h <= 0.4f) return (m_objects[min_i]);
   return (NULL);
}

 void ObjectTracking::GetBkgModelUpdateScheme (int bmu_mode,GImage& d_image)
 
{
   switch (bmu_mode) {
      case FGD_BMU_MODE_INITIAL:
      case FGD_BMU_MODE_MAJOR: {
         d_image.Set (0,0,d_image.Width,d_image.Height,FGD_BMU_SPEED_NORMAL);
         TrackedObject *t_object = TrackedObjects.First (   );
         while (t_object != NULL) {
            int bmu_speed = FGD_BMU_SPEED_NORMAL;
            if (t_object->EventStatus & (TO_EVENT_ABANDONED_START|TO_EVENT_ABANDONED|TO_EVENT_CAR_ACCIDENT_START|
               TO_EVENT_CAR_ACCIDENT|TO_EVENT_CAR_PARKING_START|TO_EVENT_CAR_PARKING|TO_EVENT_DWELL|
               TO_EVENT_REMOVED_START|TO_EVENT_REMOVED|TO_EVENT_STOPPING))
               bmu_speed = FGD_BMU_SPEED_ZERO;
            else if (t_object->Status & TO_STATUS_VERIFIED) {
               bmu_speed = FGD_BMU_SPEED_ZERO;
               if (t_object->Status & TO_STATUS_STATIC2   ) bmu_speed = FGD_BMU_SPEED_SLOW;
               if (t_object->Time_Static >= MaxTime_Static) bmu_speed = FGD_BMU_SPEED_NORMAL;
            }
            if (bmu_speed != FGD_BMU_SPEED_NORMAL) {
               int sx = t_object->X;
               int sy = t_object->Y;
               int dx = (int)(0.07f * t_object->Width);  // PARAMETERS
               int dy = (int)(0.07f * t_object->Height); // PARAMETERS
               if (dx < 1) dx = 1;
               if (dy < 1) dy = 1;
               t_object->PutMask (sx,sy     ,bmu_speed,d_image);
               t_object->PutMask (sx - dx,sy,bmu_speed,d_image);
               t_object->PutMask (sx + dx,sy,bmu_speed,d_image);
               t_object->PutMask (sx,sy - dy,bmu_speed,d_image);
               t_object->PutMask (sx,sy + dy,bmu_speed,d_image);
            }
            t_object = TrackedObjects.Next (t_object);
         }
      }  break;
      case FGD_BMU_MODE_MINOR: {
         d_image.Set (0,0,d_image.Width,d_image.Height,FGD_BMU_SPEED_ZERO);
         TrackedObject *t_object = TrackedObjects.First (   );
         while (t_object != NULL) {
            int bgu_scheme = FGD_BMU_SPEED_ZERO;
            if (t_object->Status & TO_STATUS_GHOST_OBJECT) {
               #if defined(__GS_CERT)
               if (!(t_object->EventZones & (TO_EVENTZONE_ABANDONED|TO_EVENTZONE_CAR_ACCIDENT|TO_EVENTZONE_CAR_PARKING|TO_EVENTZONE_REMOVED))) bgu_scheme = FGD_BMU_SPEED_SLOW;
               #else
               if (!(t_object->EventZones & TO_EVENTZONE_REMOVED)) bgu_scheme = FGD_BMU_SPEED_SLOW;
               #endif
            }
            if (bgu_scheme != FGD_BMU_SPEED_ZERO) t_object->PutMask (t_object->X,t_object->Y,bgu_scheme,d_image);
            t_object = TrackedObjects.Next (t_object);
         }
      }  break;
      default:
         break;
   }
}

 void ObjectTracking::GetFMOCorrespondenceMatrix (TOArray1D &to_array,IArray2D &fr_map,IArray2D &oc_matrix)

{
   int i,j;
   int x1,y1,x2,y2;
   
   oc_matrix.Clear (   );
   for (i = 1; i < to_array.Length; i++) {
      TrackedObject* t_object = to_array[i];
      int sx1 = t_object->X;
      int sy1 = t_object->Y;
      int ex1 = sx1 + t_object->Width;
      int ey1 = sy1 + t_object->Height;
      if (sx1 < 0) sx1 = 0;
      if (sy1 < 0) sy1 = 0;
      if (ex1 > fr_map.Width ) ex1 = fr_map.Width;
      if (ey1 > fr_map.Height) ey1 = fr_map.Height;
      int sx2 = sx1 - t_object->X;
      int sy2 = sy1 - t_object->Y;
      byte** m_image = t_object->MaskImage;
      for (y1 = sy1,y2 = sy2; y1 < ey1; y1++,y2++) {
         byte* l_m_image = m_image[y2];
         int*  l_fr_map  = fr_map[y1];
         int*  l_oc_mat  = oc_matrix[i];
         for (x1 = sx1,x2 = sx2; x1 < ex1; x1++,x2++)
            if (l_fr_map[x1] && l_m_image[x2]) l_oc_mat[l_fr_map[x1]]++;
      }
   }
   for (i = 1; i < oc_matrix.Height; i++) {
      int* l_oc_matrix = oc_matrix[i];
      int a = to_array[i]->NumRgnPixels;
      for (j = 1; j < oc_matrix.Width; j++) {
         if (l_oc_matrix[j]) {
            float ar = (float)l_oc_matrix[j] / a;
            if (ar < 0.35f) l_oc_matrix[j] = 0; // 영역 간 겹치는 양이 너무 작으면 무시한다.
         }
      }
   }
}

 void ObjectTracking::GetFMOCorrespondences (IArray2D &oc_matrix,TOArray1D &to_array,FRArray1D &fr_array)

{
   int i,j,n;

   for (i = 1; i < oc_matrix.Height; i++) {
      int* l_oc_matrix = oc_matrix[i];
      TrackedObject* t_object = to_array[i];
      for (j = 1,n = 0; j < oc_matrix.Width; j++)
         if (l_oc_matrix[j]) n++;
      if (n) {
         t_object->Status |= TO_STATUS_FAST_MOVING_OBJECT;
         t_object->MatFrgRegions.Create (n);
         for (j = 1,n = 0; j < oc_matrix.Width; j++)
            if (l_oc_matrix[j]) t_object->MatFrgRegions[n++] = j;
      }
   }
   for (j = 1; j < oc_matrix.Width; j++) {
      FrgRegion* fg_rgn = &fr_array[j];
      for (i = 1,n = 0; i < oc_matrix.Height; i++)
         if (oc_matrix[i][j]) n++;
      if (n) {
         fg_rgn->MatTrkObjects.Create (n);
         for (i = 1,n = 0; i < oc_matrix.Height; i++)
            if (oc_matrix[i][j]) fg_rgn->MatTrkObjects[n++] = i;
      }
   }
}

 void ObjectTracking::GetFrgRegionMaskImage (IArray2D &fr_map,GImage &d_image)

{
   int x,y;
   
   for (y = 0; y < fr_map.Height; y++) {
      byte* l_d_image = d_image[y];
      int*  l_fr_map  = fr_map[y];
      for (x = 0; x < fr_map.Width; x++) {
         if (l_fr_map[x]) l_d_image[x] = 2;
         else l_d_image[x] = 1;
      }
   }  
}

 int ObjectTracking::GetHumanRect (IArray2D& or_map,TrackedObject* t_object)

{
   if (!Flag_Calibrated) return (-1);
   FPoint2D hp,fp;
   hp.X = ScaleSR.X * (t_object->MatchingPos.X + 0.5f * t_object->Width);
   hp.Y = ScaleSR.Y * t_object->MatchingPos.Y;
   GetInhomogeneousCoordinates (Mat_Hh  * GetHomogeneousVector (hp),t_object->GndPos);
   GetInhomogeneousCoordinates (Mat_IHf * GetHomogeneousVector (t_object->GndPos),fp);
   FLine2D& stick = t_object->HumanStick;
   IBox2D&  rect  = t_object->HumanRect;
   stick[0] = ScaleRS * hp;
   stick[1] = ScaleRS * fp;
   rect.Height = (int)(1.0f * (stick[1].Y - stick[0].Y)); // PARAMETERS
   if (rect.Height <= 0) {
      stick.Clear (   );
      rect.Clear  (   );
      return (1);
   }
   rect.Width  = (int)(0.6f * rect.Height + 0.5f); // PARAMETERS
   rect.X      = (int)(stick[0].X - 0.5f * rect.Width);
   rect.Y      = (int)(stick[0].Y + 0.5f);
   ISize2D cr_size(or_map.Width,or_map.Height);
   CropRegion (cr_size,rect);
   if (t_object->Status & TO_STATUS_UNDETECTED) return (DONE);
   PerformMeanShift (or_map,rect,t_object->ObjRgnID,0.3f,1.2f,1,rect); // PARAMETERS
   hp.X = ScaleSR.X * (rect.X + 0.5f * rect.Width);
   hp.Y = ScaleSR.Y * rect.Y;
   GetInhomogeneousCoordinates (Mat_Hh  * GetHomogeneousVector (hp),t_object->GndPos);
   GetInhomogeneousCoordinates (Mat_IHf * GetHomogeneousVector (t_object->GndPos),fp);
   stick[0] = ScaleRS * hp;
   stick[1] = ScaleRS * fp;
   return (DONE);
}

 int ObjectTracking::GetNewExtObjectID (   )
 
{
   if (ObjIDGenerator == NULL) return (0);
   else return (ObjIDGenerator->Generate (   ));
}

 int ObjectTracking::GetNewIntObjectID (   )
 
{
   return (ObjectCount--);
}

 void ObjectTracking::GetObjectArray (TOArray1D &d_array)

{
   int i;
   
   d_array.Create (TrackedObjects.GetNumItems (   ) + 1);
   d_array[0] = NULL;
   TrackedObject *t_object = TrackedObjects.First (   );
   for (i = 1; t_object != NULL; i++) {
      d_array[i] = t_object;
      t_object = TrackedObjects.Next (t_object);
   }
}

 void ObjectTracking::GetObjectCorrespondenceMatrix (TOArray1D &to_array,IArray2D &fr_map,IArray2D &oc_matrix)

{
   int i,j;
   int x1,y1,x2,y2;
   
   // oc_matrix의 row: TO
   // oc_matrix의 col: FR
   oc_matrix.Clear (   );
   for (i = 1; i < to_array.Length; i++) {
      TrackedObject* t_object = to_array[i];
      int n_tries = 1;
      if (t_object->Status & TO_STATUS_FAST_MOVING_OBJECT) n_tries = 2;
      for (j = 0; j < n_tries; j++) {
         if (!t_object->MatFrgRegions) {
            IPoint2D m_pos = t_object->MatchingPos;
            if ((t_object->Status & TO_STATUS_FAST_MOVING_OBJECT) && (j == 0)) {
               m_pos.X = t_object->X;
               m_pos.Y = t_object->Y;
            }
            int sx1 = m_pos.X;
            int sy1 = m_pos.Y;
            int ex1 = sx1 + t_object->Width;
            int ey1 = sy1 + t_object->Height;
            if (sx1 < 0) sx1 = 0;
            if (sy1 < 0) sy1 = 0;
            if (ex1 > fr_map.Width ) ex1 = fr_map.Width;
            if (ey1 > fr_map.Height) ey1 = fr_map.Height;
            int sx2 = sx1 - m_pos.X;
            int sy2 = sy1 - m_pos.Y;
            int flag_matched = FALSE;
            byte** m_image = t_object->MaskImage;
            for (y1 = sy1,y2 = sy2; y1 < ey1; y1++,y2++) {
               byte* l_m_image   = m_image[y2];
               int*  l_fr_map    = fr_map[y1];
               int*  l_oc_matrix = oc_matrix[i];
               for (x1 = sx1,x2 = sx2; x1 < ex1; x1++,x2++) {
                  if (l_fr_map[x1] && l_m_image[x2]) {
                     l_oc_matrix[l_fr_map[x1]]++;
                     flag_matched = TRUE;
                  }
               }
            }
            if (flag_matched) break;
         }
      }
   }
}

 void ObjectTracking::GetObjectCorrespondences (IArray2D &oc_matrix,TOArray1D &to_array,FRArray1D &fr_array)

{
   int i,j,n;

   for (i = 1; i < oc_matrix.Height; i++) {
      TrackedObject* t_object = to_array[i];
      int* l_oc_matrix = oc_matrix[i];
      for (j = 1,n = 0; j < oc_matrix.Width; j++)
         if (l_oc_matrix[j]) n++;
      if (n) {
         t_object->MatFrgRegions.Create (n);
         for (j = 1,n = 0; j < oc_matrix.Width; j++)
            if (l_oc_matrix[j]) t_object->MatFrgRegions[n++] = j;
         // t_object의 이동 속도가 줄어 FMO에서 일반 객체로 전이하는 경우
         if (t_object->Status & TO_STATUS_FAST_MOVING_OBJECT) t_object->Status |= TO_STATUS_FMO_ENDED;
      }
   }
   for (j = 1; j < oc_matrix.Width; j++) {
      FrgRegion* fg_rgn = &fr_array[j];
      for (i = 1,n = 0; i < oc_matrix.Height; i++)
         if (oc_matrix[i][j]) n++;
      if (n) {
         fg_rgn->MatTrkObjects.Create (n);
         for (i = 1,n = 0; i < oc_matrix.Height; i++)
            if (oc_matrix[i][j]) fg_rgn->MatTrkObjects[n++] = i;
      }
   }
}

 void ObjectTracking::GetObjectMatchingPosition (GImage& s_image,TrackedObject* t_object)

{
   int s_range = SearchRange;
   if (t_object->Status & TO_STATUS_FAST_MOVING_OBJECT) s_range = (int)(2.0f * SearchRange);
   if ((t_object->Status & TO_STATUS_STATIC1) && (t_object->Exposure < 0.2f)) s_range = 0;
   if (s_range > 0) t_object->GetMatchingPosition (s_image,MaskImage,t_object->MatchingPos,s_range);
   else if (s_range < 0) t_object->GetPredictedPosition (t_object->MatchingPos,1.0f,TRUE);
   else t_object->MatchingPos(t_object->X,t_object->Y);
}

 void ObjectTracking::GetObjectRegionBoundaryEdgeInfo (TrackedObject* t_object)

{
   int x,y;
   
   t_object->NumBndPixels        = 0;
   t_object->BndEdgeStrength     = 0.0f;
   t_object->NumFrgBndEdgePixels = 0;
   t_object->NumBkgBndEdgePixels = 0;
   if (!t_object->ObjRgnID) return;
   if (!ForegroundDetector->IsFrgEdgeImageSupported (   )) return;
   int sx = t_object->ObjRgnBndBox.X;
   int sy = t_object->ObjRgnBndBox.Y;
   int ex = sx + t_object->ObjRgnBndBox.Width;
   int ey = sy + t_object->ObjRgnBndBox.Height;
   if (sx < 1) sx = 1;
   if (sy < 1) sy = 1;
   if (ex >= SrcImgSize.Width ) ex = SrcImgSize.Width  - 1;
   if (ey >= SrcImgSize.Height) ey = SrcImgSize.Height - 1;
   for (y = sy; y < ey; y++) {
      int* l_or_map = ObjRegionMap[y];
      for (x = sx; x < ex; x++) {
         if (l_or_map[x] == t_object->ObjRgnID) {
            if (BndEdgeInfoExtractor.Perform (x,y) == DONE) {
               t_object->NumBndPixels        = BndEdgeInfoExtractor.NumBoundaryPixels;
               t_object->BndEdgeStrength     = (float)BndEdgeInfoExtractor.NumFrgEdgePixels3 / BndEdgeInfoExtractor.NumBoundaryPixels;
               t_object->NumFrgBndEdgePixels = BndEdgeInfoExtractor.NumFrgEdgePixels2;
               t_object->NumBkgBndEdgePixels = BndEdgeInfoExtractor.NumBkgEdgePixels;
               return;
            }
         }
      }
   }
}

 int ObjectTracking::InitHomographies (   )

{
   Mat_Hf.Delete  (   );
   Mat_IHf.Delete (   );
   Mat_Hh.Delete  (   );
   Mat_IHh.Delete (   );
   ScaleRS.X = (float)SrcImgSize.Width  / RefImgSize.Width;
   ScaleRS.Y = (float)SrcImgSize.Height / RefImgSize.Height;
   ScaleSR.X = (float)RefImgSize.Width  / SrcImgSize.Width;
   ScaleSR.Y = (float)RefImgSize.Height / SrcImgSize.Height;
   if (!FocalLength) return (-1);
   Matrix mat_K(3,3);
   mat_K.Clear (   );
   mat_K[0][0] = FocalLength;
   mat_K[1][1] = -FocalLength;
   mat_K[2][2] = 1.0;
   mat_K[0][2] = 0.5 * RefImgSize.Width;
   mat_K[1][2] = 0.5 * RefImgSize.Height;
   Matrix mat_IK = Inv(mat_K);
   Matrix mat_A(3,3);
   mat_A.Clear (   );
   double ta = MC_DEG2RAD * CamTiltAngle;
   double sin_t = sin (ta);
   double cos_t = cos (ta);
   mat_A[0][0]  = CamHeight;
   mat_A[1][1]  = CamHeight * sin_t;
   mat_A[1][2]  = CamHeight * cos_t;
   mat_A[2][1]  = -cos_t;
   mat_A[2][2]  = sin_t;
   Mat_Hf  = mat_A * mat_IK;
   if (fabs (Det(Mat_Hf)) < 1.0e-10) return (1);
   Mat_IHf = Inv(Mat_Hf);
   double h = CamHeight - HumanSize.Height;
   mat_A[0][0] = h;
   mat_A[1][1] = h * sin_t;
   mat_A[1][2] = h * cos_t;
   Mat_Hh  = mat_A * mat_IK;
   if (fabs (Det(Mat_Hh)) < 1.0e-10) return (2);
   Mat_IHh = Inv(Mat_Hh);
   Flag_Calibrated = TRUE;
   return (DONE);
}

 int ObjectTracking::Initialize (ISize2D& si_size,float frm_rate,ObjectIDGeneration& id_generator)

{
   #if defined(__LICENSE_MAC)
   if (__CheckLicenseKey (   )) __Flag_VACL_Initialized = FALSE;
   #endif
   if (!__Flag_VACL_Initialized) return (-1);
   if (!IsEnabled (   )) return (1);
   Close (   );
   SrcImgSize     = si_size;
   FrameRate      = frm_rate;
   FrameDuration  = 1.0f / frm_rate;
   ObjIDGenerator = &id_generator;
   MaskImage.Create         (si_size.Width,si_size.Height);
   DynBkgAreaMap.Create     (si_size.Width,si_size.Height);
   DynBkgAreaMap.Clear      (   );
   NonHandoffAreaMap.Create (si_size.Width,si_size.Height);
   NonHandoffAreaMap.Clear  (   );
   TrkAreaMap.Create        (si_size.Width,si_size.Height);
   TrkAreaMap.Set           (0,0,si_size.Width,si_size.Height,PG_WHITE);
   ObjRegionMap.Create      (si_size.Width,si_size.Height);
   BlobTracker.Initialize   (si_size,frm_rate);
   BoxTracker.Initialize    (si_size,frm_rate);
   InitHomographies         (   );
   return (DONE);
}

 void ObjectTracking::InitParams (EventDetection& evt_detector)

{
   if (evt_detector.IsEventZoneAvailable (ER_EVENT_VIOLENCE)) Options |= OBT_OPTION_PERFORM_MEAN_SHIFT_TRACKING;
   else Options &= ~OBT_OPTION_PERFORM_MEAN_SHIFT_TRACKING;
}

 void ObjectTracking::ManageLostObjects (   )

{
   // TO_STATUS_LOST 상태인 TO들을 LostObjects 리스트로 이동시킨다.
   TrackedObject* t_object = TrackedObjects.First (   );
   while (t_object != NULL) {
      TrackedObject* n_object = TrackedObjects.Next (t_object);
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED) && (t_object->Status & TO_STATUS_VERIFIED) && (t_object->Status & TO_STATUS_LOST)) {
         MoveToLostObjectList (t_object);
         #if defined(__DEBUG_OBT) && defined(__CONSOLE)
         logd ("Tracked Object #%05d:\n",t_object->ID);
         logd ("     Lost: Moved to the lost object list.\n");
         #endif
      }
      t_object = n_object;
   }
   // LostObjects 리스트에 남아 있는 LO들 중에 MaxTime_Lost 초 이상 남아 있는 LO들을 제거한다.
   TrackedObject *l_object = LostObjects.First (   );
   while (l_object != NULL) {
      TrackedObject* n_object = LostObjects.Next (l_object);
      if ((l_object->Time_Lost > MaxTime_Lost) || (l_object->Status & TO_STATUS_FAST_MOVING_OBJECT)) {
         LostObjects.Remove (l_object);
         TrackedObjects.Add (l_object);
         l_object->Status |= TO_STATUS_TO_BE_REMOVED;
      }
      else l_object->Time_Lost += FrameDuration;
      l_object = n_object;
   }
}

 void ObjectTracking::MergeObjects (TOArray1D &to_array,IArray1D &to_nos,IBox2D &fb_region,int fb_no,IArray2D &oc_matrix,IArray2D &d_map)

{
   int i,j;
   
   // 주어진 FR와 가장 많이 겹치는 TO를 찾는다. 이것을 TO_m이라고 하자.
   int max_a1 = 0, max_i1 = -1;
   int max_a2 = 0, max_i2 = -1;
   for (i = 0; i < to_nos.Length; i++) {
      int a = oc_matrix[to_nos[i]][fb_no];
      if (a > max_a1) max_a1 = a, max_i1 = i;
      if (to_array[i]->Status & TO_STATUS_VERIFIED) {
         if (a > max_a2) max_a2 = a, max_i2 = i;
      }
   }
   int max_i = max_i1;
   if (max_i2 != -1) max_i = max_i2;
   // TO_m에 대응하는 FR들 중에서 TO_m과 가장 많이 겹치는 FR의 인덱스 번호가 fb_no와 일치하지 않으면 머지를 수행하지 않는다.
   int max_a = 0, max_j = 0;
   int tm_no = to_nos[max_i];
   TrackedObject* tm_object = to_array[max_i];
   for (j = 0; j < tm_object->MatFrgRegions.Length; j++) {
      int a = oc_matrix[tm_no][tm_object->MatFrgRegions[j]];
      if (a > max_a) max_a = a, max_j = j;
   }
   if (fb_no != tm_object->MatFrgRegions[max_j]) return;
   int flag_tm_ima = tm_object->IsInMarginalArea (SrcImgSize);
   // 주어진 조건을 만족하면 병합을 수행한다.
   for (i = 0; i < to_array.Length; i++) {
      if (max_i != i) {
         int condition = 0;
         TrackedObject* ti_object = to_array[i];
         int flag_ti_ima = ti_object->IsInMarginalArea (SrcImgSize);
         float ar = (float)ti_object->NumRgnPixels / tm_object->NumRgnPixels;
         float ov = (float)GetOverlapArea (*ti_object,*tm_object) / (ti_object->Width * ti_object->Height);
         // TO_m이 TO_STATUS_VERIFIED 상태일 때 TO_i가 TO_STATUS_VALIDATED 상태가 아닌 경우...
         if ((tm_object->Status & TO_STATUS_VERIFIED) && !(ti_object->Status & TO_STATUS_VALIDATED)) condition = 1;
         // TO_i가 TO_STATUS_VALIDATED 상태가 아닐 때 TO_m과 조금이라도 겹친 경우...
         if (!condition && !(ti_object->Status & TO_STATUS_VALIDATED) && (ov > 0.05f)) condition = 2; // PARAMTERS
         // TO_i가 TO_STATUS_VERIFIED 상태가 아닐 때 TO_m에 비해 TO_i의 면적이 매우 작은 경우...
         if (!condition && !(ti_object->Status & TO_STATUS_VERIFIED ) && (ar < 0.05f)) condition = 3; // PARAMETERS
         // TO_i와 TO_m이 모두 영상의 가장자리에 걸쳐 있고 두 객체간의 생성 시간의 차이가 작은 경우
         if (!condition && !(ti_object->Status & TO_STATUS_VERIFIED) && flag_tm_ima && flag_ti_ima) {
             float d_time = fabs (tm_object->Time_Tracked - ti_object->Time_Tracked);
             if (d_time <= 1.0f) condition = 4; // PARAMETERS
         }
         // TO_m에 비해 TO_i의 면적이 어느 정도 작은 경우...
         if (!condition && to_array.Length == 2 && (tm_object->Status & TO_STATUS_VERIFIED) &&
             tm_object->Type == TO_TYPE_HUMAN && ti_object->MaxAspectRatio < 2.0f &&
             ti_object->AspectRatio < tm_object->AspectRatio2 && ti_object->Time_Tracked < 10.0f &&
             ar < 0.7f && ov < 0.3f) { // PARAMETERS
            // TO_m과 TO_i를 합친 영역의 바운딩 박스 m_rgn을 구한다.
            IBox2D m_rgn = GetUnion (*tm_object,*ti_object);
            // TO_m과 TO_i를 병합했을 때 보다 좋은 "사람"의 형태가 나오는가 체크한다.
            float wr  = GetMinimum ((float)m_rgn.Width / tm_object->Width,(float)tm_object->Width / m_rgn.Width);
            float mar = (float)m_rgn.Height / m_rgn.Width;
            #if defined(__DEBUG_OBJECT_MERGING)
            int tgt_id = 7;
            if (tm_object->ID == tgt_id) {
               logd ("tm_object: ID = %d\n",tm_object->ID);
               logd ("- AspectRatio: %.2f\n",tm_object->AspectRatio);
               logd ("ti_object: ID = %d\n",ti_object->ID);
               logd ("- MaxAspectRatio: %.2f\n",ti_object->MaxAspectRatio);
               logd ("- AspectRatio: %.2f\n",ti_object->AspectRatio);
               logd ("- wr: %.2f\n",wr);
               logd ("- mar: %.2f\n",mar);
            }
            #endif
            if (wr > 0.7f && tm_object->AspectRatio < mar && mar < 6.0f) { // PARAMETERS
               ti_object->Time_Merged += FrameDuration;
               #if defined(__DEBUG_OBJECT_MERGING)
               if (tm_object->ID == tgt_id) {
                  logd ("- Time_Merged: %.2f\n",ti_object->Time_Merged);
               }
               #endif
               if (ti_object->Time_Merged >= 1.0f) condition = 5; // PARAMETERS
            }
         }
         // TO_m과 TO_i를 합친다.
         if (condition) {
            ChangeObjectRegionLabel (ObjRegionMap,fb_region,to_nos[i],to_nos[max_i]);
            ti_object->Status |= TO_STATUS_TO_BE_REMOVED;
            #if defined(__DEBUG_OBT) && defined(__CONSOLE)
            logd ("Tracked Object #%05d:\n",ti_object->ID);
            logd ("     Merged(codition=%d): to be removed.\n",condition);
            #endif
         }
      }
   }
}
 
 void ObjectTracking::MoveToLostObjectList (TrackedObject *t_object)

{
   TrackedObjects.Remove (t_object);
   LostObjects.Add (t_object);
   t_object->Status        &= ~TO_STATUS_FEATURE_POINT_TRACKING;
   t_object->Time_Lost      = 0.0f;
   t_object->InitFrameCount = FrameCount;
}

 int ObjectTracking::Perform (ForegroundDetection& frg_detector)

{
   #if defined(__DEBUG_OBT)
   _DVX_OBT = _DVY_OBT = 0;
   _DebugView_OBT->Clear (   );
   #endif
   // 일정 주기마다 라이선스를 체크한다.
   if (!(FrameCount % 1000)) {
      #if defined(__LICENSE_MAC)
      if (__CheckLicenseKey (   )) __Flag_VACL_Initialized = FALSE;
      #endif
   }
   // VACL 라이브러리를 초기화했는지 체크한다.
   if (!__Flag_VACL_Initialized) return (-1);
   // 모듈 활성 상태를 체크한다.
   if (!IsEnabled (   )) return (1);
   // 처리 영상의 해상도가 동일한지 체크한다.
   if (frg_detector.SrcImgSize.Width != SrcImgSize.Width || frg_detector.SrcImgSize.Height != SrcImgSize.Height) return (2);
   // 각종 변수 설정을 수행한다.
   FrameCount++;
   ForegroundDetector = &frg_detector;
   Shift              = frg_detector.Shift;
   // 주어진 ForegroundDetector의 타입에 따라 적합한 ObjectTracker를 실행한다.
   int e_code    = DONE;
   int fd_cls_id = frg_detector.GetClassID (   );
   switch (fd_cls_id) {
      case CLSID_ForegroundDetection_FAD:
         e_code = PerformObjectTracking (   );
         break;
      case CLSID_ForegroundDetection_HMD_OHV_MBS:
         MinCount_Tracked   = 3;
         MaxTime_Undetected = 0.2f;
         MinTime_Tracked    = 0.6f;
         MaxTime_Lost       = 0.2f;
         MinMovingDistance  = 8.0f / 240.0f;
         e_code = PerformObjectTracking (   );
         break;
      case CLSID_ForegroundDetection_HMD_OHV_3DM:
         MinCount_Tracked   = 3;
         MaxTime_Undetected = 0.2f;
         MinMovingDistance  = 0.0f;
         e_code = PerformBlobTracking (   );
         break;
      case CLSID_ForegroundDetection_FCD_Seeta:
         e_code = PerformBlobTracking (   );
         if (e_code == DONE) UpdateFaceRegions (    );
         break;
      case CLSID_ForegroundDetection_FCD_OpenVINO:
      case CLSID_ForegroundDetection_OBD_YOLO:
      case CLSID_ForegroundDetectionClient_OBD_YOLO:
         e_code = PerformBoxTracking (   );
         break;
      default:
         e_code = PerformObjectTracking (   );
         break;
   }
   if (e_code != DONE) e_code += 2;
   PerformPostProcessing (   );
   #if defined(__DEBUG_OBT)
   _DebugView_OBT->Invalidate (   );
   #endif
   return (e_code);
}

 int ObjectTracking::PerformBlobTracking (   )

{
   if (ForegroundDetector == NULL) return (1);
   TrackedObject* t_object = TrackedObjects.First (   );
   while (t_object != NULL) {
      t_object->FrameCount = FrameCount;
      t_object->ClearStatus (   );
      t_object = TrackedObjects.Next (t_object);
   }
   SyncBlobTrackingParams (   );
   #if defined(__DEBUG_OBT)
   _DebugView_OBT->DrawImage (BlobTracker.BlobRegionMap,_DVX_OBT,_DVY_OBT);
   _DVY_OBT += BlobTracker.BlobRegionMap.Height;
   _DebugView_OBT->DrawText  (_T("Previous Blob Region Map"),_DVX_OBT,_DVY_OBT,PC_GREEN);
   _DVY_OBT += 20;
   _DebugView_OBT->DrawImage (ForegroundDetector->FrgRegionMap,_DVX_OBT,_DVY_OBT);
   _DVY_OBT += BlobTracker.BlobRegionMap.Height;
   _DebugView_OBT->DrawText  (_T("Foreground Region Map"),_DVX_OBT,_DVY_OBT,PC_GREEN);
   _DVY_OBT += 20;
   #endif
   if (BlobTracker.Perform (ForegroundDetector->FrgRegionMap,ForegroundDetector->FrgRegions.Length - 1,&ForegroundDetector->SrcImage)) return (2);
   #if defined(__DEBUG_OBT)
   _DebugView_OBT->DrawImage (BlobTracker.BlobRegionMap,_DVX_OBT,_DVY_OBT);
   _DVY_OBT += BlobTracker.BlobRegionMap.Height;
   _DebugView_OBT->DrawText  (_T("Current Blob Region Map"),_DVX_OBT,_DVY_OBT,PC_GREEN);
   _DVY_OBT += 20;
   #endif
   int trj_length = (int)(FrameRate * MaxTrjLength + 0.5f);
   #if defined(__DEBUG_OBT) && defined(__CONSOLE)
   logd ("\n[Tracked Blob List]\n");
   #endif
   TrackedBlob* t_blob = BlobTracker.TrackedBlobs.First (   );
   while (t_blob != NULL) {
      #if defined(__DEBUG_OBT) && defined(__CONSOLE)
      FPoint2D cp = t_blob->GetCenterPosition (   );
      logd ("[TB%05d] X = %04d, Y = %04d, W = %04d, H = %04d, CX = %04d, CY = %04d, RID = %04d ",
         t_blob->ID,t_blob->X,t_blob->Y,t_blob->Width,t_blob->Height,(int)cp.X,(int)cp.Y,t_blob->RgnID);
      if (t_blob->Status & TB_STATUS_TO_BE_REMOVED) logd ("[To be removed]");
      #endif
      if (t_blob->Status & TB_STATUS_CREATED) {
         TrackedObject* t_object = RegisterNewObject (   );
         t_object->TrkBlob = t_blob;
         #if defined(__DEBUG_OBT) && defined(__CONSOLE)
         logd ("[Created]");
         #endif
      }
      #if defined(__DEBUG_OBT) && defined(__CONSOLE)
      logd ("\n");
      #endif
      t_blob = BlobTracker.TrackedBlobs.Next (t_blob);
   }
   t_object = TrackedObjects.First (   );
   while (t_object != NULL) {
      TrackedBlob* t_blob = t_object->TrkBlob;
      if (t_blob != NULL) {
         if (!t_object->Count_Tracked) t_object->SetRegion (t_blob->CurRegion);
         else {
            if (t_blob->Status & TB_STATUS_UNDETECTED) t_object->SetRegion (t_blob->CurRegion);
            else t_object->UpdateRegion (t_blob->CurRegion,ObjUpdateRate);
         }
         t_object->Area              = (float)t_blob->Area;
         t_object->AspectRatio       = (float)t_object->Height / t_object->Width;
         t_object->MaxMovingDistance = 0.5f * (t_blob->MaxMovingDistance1 + t_blob->MaxMovingDistance2);
         t_object->NorSpeed          = t_blob->NorSpeed;
         t_object->Speed             = t_blob->Speed;
         t_object->MeanVelocity      = t_blob->MeanVelocity;
         t_object->Velocity          = t_blob->Velocity;
         if (t_object->Width <= t_object->Height) {
            t_object->AxisLengthRatio = (float)t_object->Height / t_object->Width;
            t_object->MajorAxisLength = (float)t_object->Height;
            t_object->MinorAxisLength = (float)t_object->Width;
            t_object->MajorAxisAngle1 = 0.0f;
         }
         else {
            t_object->AxisLengthRatio = (float)t_object->Width / t_object->Height;
            t_object->MajorAxisLength = (float)t_object->Width;
            t_object->MinorAxisLength = (float)t_object->Height;
            t_object->MajorAxisAngle1 = 90.0f;
         }
         t_object->Centroid(t_blob->CX - t_blob->X,t_blob->CY - t_blob->Y);
         if (CheckObjectInTrackingArea (t_object) != DONE) t_object->Status |= TO_STATUS_TO_BE_REMOVED;
         if (!(t_object->Status & TO_STATUS_VALIDATED)) {
            if (CheckObjectInDynamicBackgroundArea (t_object) == DONE) t_object->Status |= TO_STATUS_TO_BE_REMOVED;
         }
         if (t_blob->Status & TB_STATUS_MERGED) {
            t_object->Status |= TO_STATUS_MERGED;
            t_object->Time_Merged += FrameDuration;
         }
         if (t_blob->Status & TB_STATUS_STATIC) {
            t_object->Status |= TO_STATUS_STATIC1;
            t_object->Time_Static += FrameDuration;
            if (t_object->Time_Static >= (MaxTime_Static + 2.0f)) t_object->Time_Static = MaxTime_Static + 2.0f;
         }
         else {
            t_object->Time_Static -= FrameDuration;
            if (t_object->Time_Static < 0.0f) t_object->Time_Static = 0.0f;
         }
         if (t_blob->Status & TB_STATUS_TO_BE_REMOVED) {
            t_object->Status |= TO_STATUS_TO_BE_REMOVED;
         }
         if (t_blob->Status & TB_STATUS_UNDETECTED) {
            t_object->Status |= TO_STATUS_UNDETECTED;
            t_object->Time_Undetected += FrameDuration;
         }
         if (t_blob->Status & TB_STATUS_VALIDATED) {
            t_object->Status |= TO_STATUS_VALIDATED;
         }
         if (t_blob->Status & TB_STATUS_VERIFIED_START) {
            t_object->Status |= TO_STATUS_VERIFIED_START;
            t_object->ID = GetNewExtObjectID (   );
         }
         if (t_blob->Status & TB_STATUS_VERIFIED){
            t_object->Status |= TO_STATUS_VERIFIED;
         }
      }
      t_object->UpdateCountsAndTimes (FrameDuration);
      if (t_object->Time_BestShot > 0.3f) {
         t_object->Time_BestShot = 0.0f;
         float score = t_object->GetBestShotScore (SrcImgSize);
         if (score > t_object->MaxScore_BestShot) {
            t_object->MaxScore_BestShot = score;
            t_object->Status |= TO_STATUS_BEST_SHOT;
         }
      }
      t_object->UpdateAvgMinMaxPropertyValues (   );
      t_object->AddToTrkObjRegionList (trj_length);
      t_object = TrackedObjects.Next (t_object);
   }
   return (DONE);
}

 int ObjectTracking::PerformBoxTracking (   )

{
   if (ForegroundDetector == NULL) return (1);
   SyncBoxTrackingParams (   );
   TrackedObject* t_object = TrackedObjects.First (   );
   while (t_object != NULL) {
      t_object->FrameCount = FrameCount;
      t_object->ClearStatus (   );
      t_object = TrackedObjects.Next (t_object);
   }
   if (BoxTracker.Perform (ForegroundDetector->ObjBndBoxes,ForegroundDetector->SrcColorImage)) return (2);
   int trj_length = (int)(FrameRate * MaxTrjLength + 0.5f);
   #if defined(__DEBUG_OBT) && defined(__CONSOLE)
   logd ("\n[Tracked Box List]\n");
   #endif
   TrackedBox* t_box = BoxTracker.TrackedBoxes.First (   );
   while (t_box != NULL) {
      #if defined(__DEBUG_OBT) && defined(__CONSOLE)
      logd ("[TB%05d] X = %04d, Y = %04d, W = %04d, H = %04d, ClassIdx = %04d ",
         t_box->ID,t_box->X,t_box->Y,t_box->Width,t_box->Height,t_box->ClassIdx);
      if (t_box->Status & TBX_STATUS_UNDETECTED          ) logd ("[Undetected]");
      if (t_box->Status & TBX_STATUS_TO_BE_REMOVED       ) logd ("[To be removed]");
      if (t_box->Status & TBX_STATUS_OUT_OF_TRACKING_AREA) logd ("[Out of Tracking Area]");
      if (t_box->Status & TBX_STATUS_VERIFIED_START      ) logd ("[Verified]");
      #endif
      if (t_box->Status & TBX_STATUS_CREATED) {
         TrackedObject* t_object = RegisterNewObject (   );
         t_object->SetRegion (*t_box);
         t_object->TrkBox = t_box;
         t_object->Type   = ConvertObjTypeToTrkObjType (t_box->ObjType);
         #if defined(__DEBUG_OBT) && defined(__CONSOLE)
         logd ("[Created] => [TO%05d]",t_object->ID);
         #endif
      }
      if (t_box->Status & TBX_STATUS_FACE_INITIALIZED) {
         TrackedObject* t_object = RegisterNewObject (   );
         t_object->SetRegion (t_box->Face);
         t_object->TrkBox = t_box;
         t_object->Type   = TO_TYPE_FACE;
         if ((t_box->Status & TBX_STATUS_VERIFIED) && !(t_box->Status & TBX_STATUS_VERIFIED_START)) {
            t_object->Status |= TO_STATUS_VERIFIED_START;
         }
         #if defined(__DEBUG_OBT) && defined(__CONSOLE)
         logd ("[Face Initialized]");
         #endif
      }
      if (t_box->Status & TBX_STATUS_HEAD_INITIALIZED) {
         TrackedObject* t_object = RegisterNewObject (   );
         t_object->SetRegion (t_box->Head);
         t_object->TrkBox = t_box;
         t_object->Type   = TO_TYPE_HEAD;
         if ((t_box->Status & TBX_STATUS_VERIFIED) && !(t_box->Status & TBX_STATUS_VERIFIED_START)) {
            t_object->Status |= TO_STATUS_VERIFIED_START;
         }
         #if defined(__DEBUG_OBT) && defined(__CONSOLE)
         logd ("[Head Initialized]");
         #endif
      }
      #if defined(__DEBUG_OBT) && defined(__CONSOLE)
      logd ("\n");
      #endif
      t_box = BoxTracker.TrackedBoxes.Next (t_box);
   }
   t_object = TrackedObjects.First (   );
   while (t_object != NULL) {
      TrackedBox* t_box = t_object->TrkBox;
      if (t_box != NULL) {
         if (t_object->Count_Tracked > 0) {
            if (t_box->ObjType == OBJ_TYPE_HUMAN) {
               switch (t_object->Type) {
                  case TO_TYPE_FACE: {
                     if (t_box->Face.Width) {
                        t_object->Face.Set (t_box->Face);
                        t_object->UpdateRegionAndVelocity (t_box->Face);
                     }
                     else t_object->Status |= TO_STATUS_TO_BE_REMOVED;
                  }  break;
                  case TO_TYPE_HEAD: {
                     t_object->Face.Set (t_box->Face);
                     t_object->Head.Set (t_box->Head);
                     t_object->UpdateRegionAndVelocity (t_box->Head);
                  }  break;
                  case TO_TYPE_HUMAN: {
                     t_object->Face.Set (t_box->Face);
                     t_object->Head.Set (t_box->Head);
                     t_object->UpdateRegionAndVelocity (*t_box);
                  }  break;
                  default:
                     break;
               }
            }
            else if (t_box->ObjType == OBJ_TYPE_FACE) {
               if (t_object->Type == TO_TYPE_FACE) {
                  t_object->Face.Set (*t_box);
                  t_object->UpdateRegionAndVelocity (*t_box);
               }
            }
            else t_object->UpdateRegionAndVelocity (*t_box);
         }
         t_object->Centroid = t_object->CenterPos;
         if (t_object->Width <= t_object->Height) {
            t_object->MajorAxisLength = (float)t_object->Height;
            t_object->MinorAxisLength = (float)t_object->Width;
            t_object->MajorAxis(0.0f,1.0f);
            t_object->MinorAxis(1.0f,0.0f);
         }
         else {
            t_object->MajorAxisLength = (float)t_object->Width;
            t_object->MinorAxisLength = (float)t_object->Height;
            t_object->MajorAxis(1.0f,0.0f);
            t_object->MinorAxis(0.0f,1.0f);
         }
         t_object->GetArea              (   );
         t_object->GetAspectRatio       (   );
         t_object->GetAxisLengthRatio   (   );
         t_object->GetMajorAxisAngle1   (   );
         t_object->GetMeanArea          ((int)FrameRate);
         t_object->GetMeanVelocity      ((int)FrameRate,t_object->MeanVelocity);
         t_object->GetSpeeds            (FrameRate);
         t_object->GetMajorAxisAngle2   (   );
         t_object->GetMaxMovingDistance (   );
         if (CheckObjectInTrackingArea (t_object) != DONE) {
            t_object->Status |= TO_STATUS_TO_BE_REMOVED;
         }
         if (!(t_object->Status & TO_STATUS_VALIDATED)) {
            if (CheckObjectInDynamicBackgroundArea (t_object) == DONE) {
               t_object->Status |= TO_STATUS_TO_BE_REMOVED;
            }
         }
         if (CheckObjectStatic (t_object) == DONE) {
            t_object->Time_Static += FrameDuration;
            if (t_object->Time_Static > t_object->MaxTime_Static) t_object->MaxTime_Static = t_object->Time_Static;
            if (t_object->Time_Static >= (MaxTime_Static + 2.0f)) t_object->Time_Static = MaxTime_Static + 2.0f;
         }
         else {
            t_object->Time_Static -= FrameDuration;
            if (t_object->Time_Static < 0.0f) t_object->Time_Static = 0.0f;
            if ((t_object->MaxTime_Static - t_object->Time_Static) > OBT_PARAM_MAX_NONSTATIC_TIME) {
               t_object->Time_Static = t_object->MaxTime_Static = 0.0f;
               t_object->Count_Abandoned = t_object->Count_Removed = 0;
            }
         }
         if (t_box->Status & TBX_STATUS_TO_BE_REMOVED) {
            t_object->Status |= TO_STATUS_TO_BE_REMOVED;
         }
         if (t_box->Status & TBX_STATUS_UNDETECTED) {
            t_object->Status |= TO_STATUS_UNDETECTED;
         }
         if (t_box->Status & TBX_STATUS_VALIDATED) {
            t_object->Status |= TO_STATUS_VALIDATED;
         }
         if (t_box->Status & TBX_STATUS_VERIFIED_START) {
            t_object->Status |= TO_STATUS_VERIFIED_START;
         }
         if (t_box->Status & TBX_STATUS_VERIFIED){
            t_object->Status |= TO_STATUS_VERIFIED;
         }
         if (t_object->Status & TO_STATUS_VERIFIED_START) {
            if (t_object->Type == TO_TYPE_FACE) {
               // 동일인에 대한 얼굴 추적이 중간에 끊겼다가 다시 시작될 경우,
               // 얼굴 객체의 ID가 신규 ID가 아닌 기존 ID로 유지되도록 조치를 취한다.
               if (t_box->FaceID > 0) t_object->ID = t_box->FaceID;
               else {
                  // 특정 인물에 대해 최초로 얼굴 추적을 시작하게 된 경우
                  t_object->ID = t_box->FaceID = GetNewExtObjectID (   );
               }
            }
            else t_object->ID = GetNewExtObjectID (   );
         }
         // 총 1.5초 이상 지팡이를 소유한 경우
         if ((t_object->Type == TO_TYPE_HUMAN) && (t_box->Time_Stick > 1.5f)) { // PARAMETERS
            t_object->PersonAttrs |= TO_PERSON_ATTR_STICK;
         }
      }
      t_object->UpdateCountsAndTimes (FrameDuration);
      if (t_object->Time_BestShot > 0.3f) {
         t_object->Time_BestShot = 0.0f;
         float score = t_object->GetBestShotScore (SrcImgSize);
         if (score > t_object->MaxScore_BestShot) {
            t_object->MaxScore_BestShot = score;
            t_object->Status |= TO_STATUS_BEST_SHOT;
         }
      }
      t_object->UpdateAvgMinMaxPropertyValues (   );
      t_object->AddToTrkObjRegionList (trj_length);
      t_object = TrackedObjects.Next (t_object);
   }
   return (DONE);
}

 void ObjectTracking::PerformMeanShift (IArray2D& or_map,IBox2D& s_rgn,int or_no,float ub_a,float ub_w,int n_iterations,IBox2D& d_rgn)

{
   int i,x,y;

   IBox2D rect = s_rgn;
   for (i = 0; i < n_iterations; i++) {
      int sx  = rect.X;
      int sy  = rect.Y;
      int ex  = sx + rect.Width;
      int ey1 = sy + (int)(ub_a * rect.Height);
      int ey2 = sy + rect.Height;
      CropRegion (or_map.Width,or_map.Height,sx,sy,ex,ey1);
      CropRegion (or_map.Width,or_map.Height,sx,sy,ex,ey2);
      int sum_x = 0,sum_y = 0,n = 0;
      for (y = sy; y < ey1; y++) {
         int* l_or_map = or_map[y];
         for (x = sx; x < ex; x++) {
            if (l_or_map[x] == or_no) {
               sum_x += x;
               sum_y += y;
               n++;
            }
         }
      }
      sum_x = (int)(ub_w * sum_x);
      sum_y = (int)(ub_w * sum_y);
      n = (int)(ub_w * n);
      for (y = ey1; y < ey2; y++) {
         int* l_or_map = or_map[y];
         for (x = sx; x < ex; x++) {
            if (l_or_map[x] == or_no) {
               sum_x += x;
               sum_y += y;
               n++;
            }
         }
      }
      if (n) {
         float cx1 = 0.5f * (sx + ex );
         float cy1 = 0.5f * (sy + ey2);
         float cx2 = (float)sum_x / n;
         float cy2 = (float)sum_y / n;
         rect.X = (int)(rect.X + (cx2 - cx1) + 0.5f);
         rect.Y = (int)(rect.Y + (cy2 - cy1) + 0.5f);
         if (rect.X == sx && rect.Y == sy) break;
      }
   }
   d_rgn = rect;
}

// [주의] 본 함수 내부에서 TrackedObject 객체를 직접 delete 하지 말 것!
 int ObjectTracking::PerformObjectTracking (   )

{
   int i,j,k;
   #if defined(__DEBUG_OBT)
   CString text;
   #endif

   if (ForegroundDetector == NULL) return (1);
   int   trj_length = (int)(FrameRate * MaxTrjLength + 0.5f);
   GImage& s_image  = ForegroundDetector->SrcImage;
   IArray2D& fr_map = ForegroundDetector->FrgRegionMap;
   FRArray1D fr_array;
   fr_array.Initialize (ForegroundDetector->FrgRegions);
   // TO들의 포인터의 배열을 얻는다.
   TOArray1D to_array;
   GetObjectArray (to_array);
   // TO들의 상태를 클리어하고 현재 프래임에서 예상 위치를 얻는다.
   for (i = 1; i < to_array.Length; i++) {
      TrackedObject* t_object = to_array[i];
      t_object->ClearStatus (   );
      t_object->MatFrgRegions.Delete (   );
      t_object->GetPredictedPosition (t_object->MatchingPos);
   }
   #if defined(__DEBUG_OBT)
   _DebugView_OBT->DrawImage (fr_map,_DVX_OBT,_DVY_OBT);
   for (i = 1; i < to_array.Length; i++) {
      TrackedObject* t_object = to_array[i];
      _DebugView_OBT->DrawRectangle (_DVX_OBT + t_object->X,_DVY_OBT + t_object->Y,t_object->Width,t_object->Height,PC_RED,2);
      _DebugView_OBT->DrawRectangle (_DVX_OBT + t_object->MatchingPos.X,_DVY_OBT + t_object->MatchingPos.Y,t_object->Width,t_object->Height,PC_YELLOW);
      text.Format (_T("%d"),t_object->ID);
      _DebugView_OBT->DrawText (text,_DVX_OBT + t_object->X - 10,_DVY_OBT + t_object->Y - 10,PC_YELLOW,TRANSPARENT,FT_ARIAL,14);
   }
   _DVY_OBT += fr_map.Height;
   _DebugView_OBT->DrawText (_T("Current(Red) & Predicted(Yellow) Positions"),_DVX_OBT,_DVY_OBT,PC_GREEN);
   _DVY_OBT += 20;
   #endif
   int n_tos = to_array.Length - 1;
   int n_frs = fr_array.Length - 1;
   IArray2D oc_matrix(n_frs + 1,n_tos + 1);
   // 필요 시 빠른 이동 물체의 검출/추적을 수행한다.
   if (Options & OBT_OPTION_PERFORM_FAST_MOVING_OBJECT_DETECTION) {
      if (!FMODetector.IsInitialized (   )) FMODetector.Initialize (ForegroundDetector->SrcImgSize);
      FMODetector.Perform (*ForegroundDetector);
      IArray2D& afmo_map = FMODetector.AFMOMap;
      GetFMOCorrespondenceMatrix (to_array,afmo_map,oc_matrix);
      GetFMOCorrespondences (oc_matrix,to_array,fr_array);
   }
   // TO들와 FR(Foreground Region)들 사이의 대응 관계를 나타내는 행렬을 구한다.
   // oc_matrix[i][j]에는 TO_i와 FR_j 사이의 겹치는 픽셀 수가 저장된다.
   GetObjectCorrespondenceMatrix (to_array,fr_map,oc_matrix);
   // 각 TO에 대하여 그것에 대응하는 FR들을 찾아 인덱스 값을 TO.MatFrgRegions 배열에 저장한다.
   // 또한 각 FR에 대하여 그것에 대응하는 TO들을 찾아 인덱스 값을 FR.MatTrkObjects 배열에 저장한다.
   GetObjectCorrespondences (oc_matrix,to_array,fr_array);
   // 전경 영역에 대한 마스크 영상을 얻는다.
   // 특정 픽셀이 전경 영역에 해당하면 2의 값을 갖고, 그렇지 않으면 1의 값을 갖는다.
   GetFrgRegionMaskImage (fr_map,MaskImage);
   // ObjRegionMap을 클리어한다.
   ObjRegionMap.Clear (   );
   // 부모 객체의 인덱스 값을 저장할 배열을 생성한다.
   IArray1D pi_array(n_tos + n_frs + 1);
   pi_array.Clear (   );
   //
   // FR의 관점에서 매칭 작업을 수행한다.
   //
   #if defined(__DEBUG_OBT) && defined(__CONSOLE)
   logd ("Foreground Regions: %03d ----------------------------------------------------------\n\n",fr_array.Length - 1);
   #endif
   for (j = 1; j < fr_array.Length; j++) {
      FrgRegion &fg_rgn = fr_array[j];
      #if defined(__DEBUG_OBT) && defined(__CONSOLE)
      logd ("Foreground Region #%04d: [X:%d][Y:%d][W:%d][H:%d][CX:%4.1f][CY:%4.1f]\n",
         j,fg_rgn.X,fg_rgn.Y,fg_rgn.Width,fg_rgn.Height,fg_rgn.CX,fg_rgn.CY);
      #endif
      switch (fg_rgn.MatTrkObjects.Length) {
         // FR_j에 대응하는 TO가 없는 경우...
         case 0: {
            #if defined(__DEBUG_OBT) && defined(__CONSOLE)
            logd ("     No corresponding TOs: ");
            #endif
            // FR_j가 TrackingArea 안에 존재하면 새로운 객체로 간주한다.
            if (CheckBlobOverlapWithTrackingArea (fr_map,fr_array[j],j) == DONE) {
               CopyBlobRegion (fr_map,fr_array[j],j,++n_tos,ObjRegionMap);
               #if defined(__DEBUG_OBT) && defined(__CONSOLE)
               logd (" Classified as a new object.\n");
               #endif
            }
            // FR_j가 TrackingArea 밖에 존재하면 무시한다.
            else {
               #if defined(__DEBUG_OBT) && defined(__CONSOLE)
               logd (" Out of tracking area.\n");
               #endif
            }
         }  break;
         // FR_j에 대응하는 TO가 한 개인 경우...
         // TO의 관점에서 작업을 수행할 때 처리한다.
         case 1: {
            #if defined(__DEBUG_OBT) && defined(__CONSOLE)
            TrackedObject* m_object = to_array[fg_rgn.MatTrkObjects[0]];
            logd ("     Corresponds to [TO#%04d:%d]\n",m_object->ID,m_object->MatFrgRegions.Length);
            #endif
         }  break;
         // FR_j에 대응하는 TO가 여러 개인 경우...
         default: {
            #if defined(__DEBUG_OBT) && defined(__CONSOLE)
            logd ("     Corresponds to ");
            for (i = 0; i < fg_rgn.MatTrkObjects.Length; i++) {
               TrackedObject* m_object = to_array[fg_rgn.MatTrkObjects[i]];
               logd ("[TO#%04d:%d]",m_object->ID,m_object->MatFrgRegions.Length);
            }
            logd ("\n");
            #endif
            // Occlusion Reasoning에 참여할 TO들을 so_array 배열에 담는다.
            TOArray1D so_array(fg_rgn.MatTrkObjects.Length);
            for (i = 0; i < fg_rgn.MatTrkObjects.Length; i++) {
               so_array[i] = to_array[fg_rgn.MatTrkObjects[i]];
               so_array[i]->Status |= TO_STATUS_MERGED;
            }
            // 템플릿 매칭을 통해 정확한 매칭 위치를 찾는다.
            // 오브젝트의 Depth 값이 작은 순으로 템플릿 매칭을 수행한다.
            for (k = 0; k < NumObjLayers; k++) {
               for (i = 0; i < so_array.Length; i++) {
                  TrackedObject* s_object = so_array[i];
                  if (s_object->Depth == k) {
                     IPoint2D p_pos = s_object->MatchingPos;
                     GetObjectMatchingPosition (s_image,s_object);
                     if (!(s_object->Status & TO_STATUS_MASK_PUT)) {
                        s_object->PutMask (s_object->MatchingPos.X,s_object->MatchingPos.Y,0,MaskImage);
                        s_object->Status |= TO_STATUS_MASK_PUT;
                     }
                  }
               }
            }
            // Occlusion Reasoning을 수행한다.
            PerformOcclusionReasoning (s_image,so_array,fg_rgn.MatTrkObjects,fr_map,fr_array[j],j,ObjRegionMap);
            // FR_j에 대응하는 TO들 중에서 FR_j와 가장 많이 겹치는 TO를 TO_m이라고 하면, 조건을 만족하는 TO들을 TO_m과 합친다.
            MergeObjects (so_array,fg_rgn.MatTrkObjects,fg_rgn,j,oc_matrix,ObjRegionMap);
         }  break;
      }
   }
   //
   // TO의 관점에서 매칭 작업을 수행한다.
   //
   #if defined(__DEBUG_OBT) && defined(__CONSOLE)
   logd ("\nTracked Objects: %03d -------------------------------------------------------------\n\n",to_array.Length - 1);
   #endif
   for (i = 1; i < to_array.Length; i++) {
      TrackedObject *t_object = to_array[i];
      #if defined(__DEBUG_OBT) && defined(__CONSOLE)
      logd ("Tracked Object #%05d: [X:%d][Y:%d][W:%d][H:%d][CX:%4.1f][CY:%4.1f][PX:%d][PY:%d]\n",
         t_object->ID,t_object->X,t_object->Y,t_object->Width,t_object->Height,t_object->Centroid.X,t_object->Centroid.Y,t_object->MatchingPos.X,t_object->MatchingPos.Y);
      #endif
      switch (t_object->MatFrgRegions.Length) {
         // TO_i에 대응하는 FR가 없는 경우
         case 0: {
            t_object->Status |= TO_STATUS_UNDETECTED;
            #if defined(__DEBUG_OBT) && defined(__CONSOLE)
            logd ("     Undetected.\n");
            #endif
         }  break;
         // TO_i에 대응하는 FR이 한 개인 경우
         case 1: {
            #if defined(__DEBUG_OBT) && defined(__CONSOLE)
            logd ("     Corresponds to [FR#%03d:%d]\n",t_object->MatFrgRegions[0],fr_array[t_object->MatFrgRegions[0]].MatTrkObjects.Length);
            #endif
            j = t_object->MatFrgRegions[0];
            // FR_j가 오직 TO_i에만 대응하는 경우. 즉, TO_i와 FR_j가 1:1로 대응하는 경우
            if (fr_array[j].MatTrkObjects.Length == 1) {
               int flag_copy_blob = TRUE;
               int flag_overwrite = FALSE;
               FrgRegion& fg_rgn = fr_array[j];
               // TO_i가 TO_STATUS_FAST_MOVING_OBJECT이면, FR_j의 centroid 위치로 TO_i의 centroid 위치를 이동시킨다.
               if (!(t_object->Status & TO_STATUS_FAST_MOVING_OBJECT)) {
                  // FR_j와 TO_i 사이의 크기 변화가 적으면 FR_j의 centroid 위치와 TO_i의 prediction 위치의 가중 합의 위치로 TO_i의 centroid 위치를 이동시킨다.
                  if (t_object->GetMatchingPosition (fg_rgn,0.7f,1.3f) != DONE) {
                     // 그렇지 않으면 탬플릿 매칭을 통해 매칭 위치를 찾는다.
                     GetObjectMatchingPosition (s_image,t_object);
                     if (t_object->Status & TO_STATUS_VERIFIED) {
                        // FR_j의 면적이 TO_i의 면적에 비해 매우 크면...
                        int fb_area = fg_rgn.Area;
                        int to_area = t_object->NumRgnPixels;
                        if (fb_area >= (int)(to_area * OBT_PARAM_MAX_AREA_CHANGED_ABRUPTLY)) {
                           // FR_j가 영상의 가장자리에 걸쳐 있는지 판단한다.
                           if (CheckRegionInMarginalArea (fg_rgn) == DONE) {
                              // FR_j가 영상의 가장자리에 걸쳐 있는 상태면, FR_j에 신규 출연 객체가 섞여 있을
                              // 가능성이 크기 때문에 FR_j를 TO_i의 영역과 기타 영역으로 나눈다.
                              // 우선 FR_j에서 TO_i를 제외한 영역을 복사한다.
                              CopyResidualBlobRegion (fr_map,fg_rgn,j,t_object,++n_tos,ObjRegionMap);
                              flag_copy_blob = FALSE;
                              flag_overwrite = TRUE;
                           }
                           else {
                              // 그렇지 않으면 TO_i의 가려졌던 부분이 나왔거나, 노이즈일 가능성이 크다.
                              t_object->Status |= TO_STATUS_CHANGED_ABRUPTLY;
                              t_object->Time_ChangedAbruptly += FrameDuration;
                              if (t_object->Time_ChangedAbruptly <= 0.2f) flag_copy_blob = FALSE; // PARAMETERS
                           }
                        }
                     }
                  }
               }
               // fg_rgn을 ObjRegionMap에 복사한다.
               if (flag_copy_blob) {
                  CopyBlobRegion (fr_map,fg_rgn,j,i,ObjRegionMap);
                  t_object->Status |= TO_STATUS_MATCHED_1TO1;
               }
               // 그렇지 않으면 TO_i를 ObjRegionMap의 MatchingPos 위치에 복사한다.
               else t_object->PutMask (t_object->MatchingPos.X,t_object->MatchingPos.Y,i,ObjRegionMap,flag_overwrite);
            }
         }  break;
         // TO_i에 대응하는 FR가 여러 개인 경우
         default: {
            #if defined(__DEBUG_OBT) && defined(__CONSOLE)
            logd ("     Corresponds to ");
            for (j = 0; j < t_object->MatFrgRegions.Length; j++)
               logd ("[FR#%03d:%d]",t_object->MatFrgRegions[j],fr_array[t_object->MatFrgRegions[j]].MatTrkObjects.Length);
            logd ("\n");
            #endif
            t_object->Status |= TO_STATUS_SPLIT;
            // 템플릿 매칭을 통해 정확한 매칭 위치를 찾는다.
            // t_object가 TO_STATUS_MERGED 상태이면 이미 템플릿 매칭을 통해 매칭 위치를 찾은 상태이므로 다시 할 필요가 없다.
            if (!(t_object->Status & TO_STATUS_MERGED)) GetObjectMatchingPosition (s_image,t_object);
            // TO_i와 가장 많이 겹치는 FR를 찾는다.
            int max_ov = 0,max_j = 0;
            for (k = 0; k < t_object->MatFrgRegions.Length; k++) {
               j = t_object->MatFrgRegions[k];
               if (oc_matrix[i][j] > max_ov) max_ov = oc_matrix[i][j], max_j = j;
            }
            // TO_i에 대응하는 각각의 FR에 대하여 TO_i의 트랙에 넣을지 말지를 결정한다.
            for (k = 0; k < t_object->MatFrgRegions.Length; k++) {
               j = t_object->MatFrgRegions[k];
               // FR_j가 오직 TO_i에만 대응하는 경우.
               if (fr_array[j].MatTrkObjects.Length == 1) {
                  // FR_j가 TO_i에 대응하는 FR들 중에 가장 많이 TO_i와 겹치는 FR이면 무조건 TO_i의 트랙에 넣는다.
                  if (j == max_j) CopyBlobRegion (fr_map,fr_array[j],j,i,ObjRegionMap);
                  //  그렇지 않으면 FR_j를 신규 TO로 간주한다.
                  else {
                     CopyBlobRegion (fr_map,fr_array[j],j,++n_tos,ObjRegionMap);
                     pi_array[n_tos] = i; // FR_j의 부모 TO의 인덱스 값을 기록한다.
                  }
               }
            }
         }  break;
      }
   }
   #if defined(__DEBUG_OBT)
   _DebugView_OBT->DrawImage (ObjRegionMap,_DVX_OBT,_DVY_OBT);
   for (i = 1; i < to_array.Length; i++) {
      TrackedObject* t_object = to_array[i];
      _DebugView_OBT->DrawRectangle (_DVX_OBT + t_object->X,_DVY_OBT + t_object->Y,t_object->Width,t_object->Height,PC_RED,2);
      _DebugView_OBT->DrawRectangle (_DVX_OBT + t_object->MatchingPos.X,_DVY_OBT + t_object->MatchingPos.Y,t_object->Width,t_object->Height,PC_GREEN);
      text.Format (_T("%d"),t_object->ID);
      _DebugView_OBT->DrawText (text,_DVX_OBT + t_object->X - 10,_DVY_OBT + t_object->Y - 10,PC_YELLOW,TRANSPARENT,FT_ARIAL,14);
   }
   _DVY_OBT += fr_map.Height;
   _DebugView_OBT->DrawText (_T("Current(Red) & Matching(Green) Positions"),_DVX_OBT,_DVY_OBT,PC_GREEN);
   _DVY_OBT += 20;
   #endif
   //
   // TO의 면적, 바운딩 박스, Exposure 값을 구하고,
   // TO의 각종 카운트/타임 값을 업데이트하고,
   // TO의 템플릿, 영역 정보, 히스토그램을 업데이트한다.
   //
   CRArray1D cr_array(n_tos + 1);
   GetCRInfo (ObjRegionMap,cr_array);
   for (i = 1; i < to_array.Length; i++) {
      TrackedObject* t_object = to_array[i];
      t_object->ObjRgnID   = 0;
      t_object->ObjRgnArea = 0;
      t_object->ObjRgnBndBox.Clear (   );
      if (t_object->Status & TO_STATUS_TO_BE_REMOVED) continue;
      // TO의 바운딩 박스를 찾지 못하면 템플릿 업데이트를 수행하지 않는다.
      if (cr_array[i].Area >= 4) {
         t_object->ObjRgnID     = i;
         t_object->ObjRgnArea   = cr_array[i].Area;
         t_object->ObjRgnBndBox = (IBox2D)cr_array[i];
      }
      else {
         t_object->Status |= TO_STATUS_UNDETECTED;
         #if defined(__DEBUG_OBT) && defined(__CONSOLE)
         logd ("Tracked Object #%05d:\n",t_object->ID);
         logd ("     Undetected.\n");
         #endif
      }
      // TO가 TO_STATUS_MERGED의 상태인 경우, TO의 Exposure 값을 구한다.
      if (t_object->Status & TO_STATUS_MERGED) {
         float exposure = (float)cr_array[i].Area / t_object->NumRgnPixels;
         if (exposure > 1.0f) exposure = 1.0f;
         t_object->Exposure = exposure;
      }
      else t_object->Exposure = 1.0f;
      // TO의 템플릿 영상을 업데이트한다.
      if (Flag_Calibrated) GetHumanRect (ObjRegionMap,t_object);
      else if (Options & OBT_OPTION_PERFORM_MEAN_SHIFT_TRACKING) {
         if (t_object->ObjRgnID > 0 && !(t_object->Status & TO_STATUS_MERGED)) {
            // 비교적 빨리 이동하는 객체를 놓치지 않고 추적하기 위해 Mean-Shift Tracking을 수행한다.
            IBox2D rect(t_object->MatchingPos.X,t_object->MatchingPos.Y,t_object->Width,t_object->Height);
            PerformMeanShift (ObjRegionMap,rect,t_object->ObjRgnID,0.0f,1.0f,1,rect); // PARAMETERS
            t_object->MatchingPos.X = rect.X;
            t_object->MatchingPos.Y = rect.Y;
         }
      }
      IBox2D d_rgn; float vu_rate;
      t_object->UpdateTemplateImage (s_image,ObjRegionMap,ObjUpdateRate,d_rgn,vu_rate);
      if (t_object->Status & TO_STATUS_TO_BE_REMOVED) continue;
      // TO의 카운트 값들과 타임 값을들 업데이트한다.
      t_object->UpdateCountsAndTimes (FrameDuration);
      // TO의 영역 및 속도를 업데이트한다.
      t_object->UpdateRegionAndVelocity (d_rgn,vu_rate);
      // TO의 그레이 히스토그램을 업데이트한다.
      t_object->UpdateGrayHistogram (s_image,ObjRegionMap);
   }
   //
   // 새로운 TO들에 대하여 템플릿 영상과 영역 정보를 설정한다.
   //
   #if defined(__DEBUG_OBT) && defined(__CONSOLE)
   logd ("\nNew Objects ----------------------------------------------------------------------\n\n");
   #endif
   for (i = to_array.Length; i <= n_tos; i++) {
      if (cr_array[i].Area >= 4) {
         TrackedObject* t_object = RegisterNewObject (s_image,cr_array[i],i);
         GetHumanRect (ObjRegionMap,t_object);
         // t_object의 부모 TO가 있는 경우, 부모 TO의 Time_Dwell 값을 물려 받는다.
         if (pi_array[i]) {
            TrackedObject* p_object = to_array[pi_array[i]]; 
            if (p_object->EventStatus & TO_EVENT_DWELL) t_object->EventStatus |= TO_EVENT_DWELL;
            t_object->Time_Dwell     = to_array[pi_array[i]]->Time_Dwell;
            t_object->EvtPrgTimeList = to_array[pi_array[i]]->EvtPrgTimeList;
         }
         #if defined(__DEBUG_OBT) && defined(__CONSOLE)
         logd ("Tracked Object #%05d: [X:%d][Y:%d][W:%d][H:%d]\n",t_object->ID,t_object->X,t_object->Y,t_object->Width,t_object->Height);
         #endif
      }
   }
   //
   // Human TO의 경우 바운딩 박스의 너비가 예측한 Human 너비보다 일정 이상 크면 객체 영역을 분할한다.
   //
   GetObjectArray (to_array);
   for (i = 1; i < to_array.Length; i++) {
      TrackedObject* t_object = to_array[i];
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED) && !(t_object->Status & TO_STATUS_VERIFIED) && (t_object->Status & TO_STATUS_MATCHED_1TO1)) {
         if ((t_object->HumanRect.Width) && (t_object->ObjRgnID > 0)) {
            int w_thrsld = (int)(1.5f * t_object->HumanRect.Width); // PARAMETERS
            if (t_object->Width >= w_thrsld) {
               int n = (int)(t_object->Width / t_object->HumanRect.Width);
               int w = t_object->Width / n;
               IBox2D s_rect(t_object->X,t_object->Y,w,t_object->Height);
               for (j = 0; j < n; j++) {
                  IBox2D t_rect = s_rect;
                  CropRegion (SrcImgSize,t_rect);
                  CopyCR (ObjRegionMap,t_rect,t_object->ObjRgnID,ObjRegionMap,++n_tos);
                  TrackedObject* n_object = RegisterNewObject (s_image,t_rect,n_tos);
                  GetHumanRect (ObjRegionMap,n_object);
                  s_rect.X += w;
               }
               t_object->Status |= TO_STATUS_TO_BE_REMOVED;
            }
         }
      }
   }
   //
   // 각 TO의 상태에 따라 다양한 처리를 수행한다.
   //
   #if defined(__DEBUG_OBT) && defined(__CONSOLE)
   logd ("\nCondition Checking ---------------------------------------------------------------\n\n");
   #endif
   int n_layers = 0;
   BndEdgeInfoExtractor.Initialize (*ForegroundDetector,ObjRegionMap);
   TrackedObject* t_object = TrackedObjects.First (   );
   while (t_object != NULL) {
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED)) {
         // TO의 각종 프로퍼티 값을 구한다.
         GetObjectProperties (t_object);
         // TO의 바운딩 박스의 중심이 TrackingArea를 벗어났는가 체크한다.
         if (CheckObjectInTrackingArea (t_object) != DONE) {
            t_object->Status |= TO_STATUS_OUT_OF_TRACKING_AREA|TO_STATUS_TO_BE_REMOVED;
            #if defined(__DEBUG_OBT) && defined(__CONSOLE)
            logd ("Tracked Object #%05d:\n",t_object->ID);
            logd ("     Out of tracking area: To be removed.\n");
            #endif
         }
         // TO의 TO_STATUS_VALIDATED 상태에 따른 처리를 수행한다.
         if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED)) {
            if (t_object->Count_Tracked >= MinCount_Tracked) t_object->Status |= TO_STATUS_VALIDATED;
            if (CheckObjectInOuterZone (SrcImgSize,OuterZoneSize1,OuterZoneSize2,t_object) == DONE) t_object->Status |= TO_STATUS_IN_OUTER_ZONE;
            // TO가 TO_STATUS_VALIDATED 상태가 된 후에 행하는 검사
            if (t_object->Status & TO_STATUS_VALIDATED) {
               // TO가 움직임이 없는 상태인지 검사한다.
               // 일정 시간동안 움직임이 없으면 TO_STATUS_STATIC의 상태가 된다.
               if (CheckObjectStatic (t_object) == DONE) {
                  t_object->Time_Static += FrameDuration;
                  if (t_object->Time_Static > t_object->MaxTime_Static) t_object->MaxTime_Static = t_object->Time_Static;
                  if (t_object->Time_Static >= (MaxTime_Static + 2.0f)) t_object->Time_Static = MaxTime_Static + 2.0f;
                  // TO가 Ghost Object인지 또는 Abandoned Object인지 또는 Removed Object인지 판단한다.
               }
               else {
                  t_object->Time_Static -= FrameDuration;
                  if (t_object->Time_Static < 0.0f) t_object->Time_Static = 0.0f;
                  if ((t_object->MaxTime_Static - t_object->Time_Static) > OBT_PARAM_MAX_NONSTATIC_TIME) {
                     t_object->Time_Static = t_object->MaxTime_Static = 0.0f;
                     t_object->Count_Abandoned = t_object->Count_Removed = 0;
                  }
                  t_object->EventStatus &= ~TO_EVENT_ABANDONED_END;
               }
               CheckStaticObjectConditions (t_object);
               // Ghost Object에 대한 처리를 수행한다.
               if (t_object->Status & TO_STATUS_GHOST_OBJECT) {
                  t_object->Time_Ghost += FrameDuration;
                  if (t_object->Time_Ghost >= 0.5f) { // PARAMETERS
                     #if defined(__GS_CERT)
                     if (!(t_object->EventZones & (TO_EVENTZONE_ABANDONED|TO_EVENTZONE_CAR_ACCIDENT|TO_EVENTZONE_CAR_PARKING|TO_EVENTZONE_REMOVED))) t_object->Status |= TO_STATUS_TO_BE_REMOVED;
                     #else
                     if (!(t_object->EventZones & TO_EVENTZONE_REMOVED)) t_object->Status |= TO_STATUS_TO_BE_REMOVED;
                     #endif
                  }
               }
               else {
                  t_object->Time_Ghost -= 0.5f * FrameDuration;
                  if (t_object->Time_Ghost < 0.0f) t_object->Time_Ghost = 0.0f;
               }
            }
            // TO가 TO_STATUS_VALIDATED 상태가 되기 전에 행하는 검사
            else {
               // TO가 TO_STATUS_UNDETECTED의 상태이면 제거된다.
               if (t_object->Status & TO_STATUS_UNDETECTED) {
                  t_object->Status |= TO_STATUS_TO_BE_REMOVED;
                  #if defined(__DEBUG_OBT) && defined(__CONSOLE)
                  logd ("Tracked Object #%05d:\n",t_object->ID);
                  logd ("     Undetected & Not validated yet: To be removed.\n");
                  #endif
               }
               // TO가 TO_STATUS_MERGED의 상태이고, TO의 면적이 작으면 제거된다.
               if (t_object->Status & TO_STATUS_MERGED) {
                  if (t_object->Area < 25) { // PARAMETERS
                     t_object->Status |= TO_STATUS_TO_BE_REMOVED;
                     #if defined(__DEBUG_OBT) && defined(__CONSOLE)
                     logd ("Tracked Object #%05d:\n",t_object->ID);
                     logd ("     Merged & Very small area & Not validated yet: To be removed.\n");
                     #endif
                  }
               }
               // TO의 바운딩 박스의 중심이 DynamicBkgArea 안에 존재하면 제거된다.
               if (CheckObjectInDynamicBackgroundArea (t_object) == DONE) {
                  t_object->Status |= TO_STATUS_TO_BE_REMOVED;
                  #if defined(__DEBUG_OBT) && defined(__CONSOLE)
                  logd ("Tracked Object #%05d:\n",t_object->ID);
                  logd ("     Detected in the dynamic background areas: To be removed.\n");
                  #endif
               }
            }
         }
         // TO의 TO_STATUS_VERIFIED 상태에 따른 처리를 수행한다.
         if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED)) {
            // TO가 TO_STATUS_VERIFIED의 상태가 된 후에 행하는 검사.
            if (t_object->Status & TO_STATUS_VERIFIED) {
               // TO가 TO_STATUS_UNDETECTED의 상태인 경우
               if (t_object->Status & TO_STATUS_UNDETECTED) {
                  if (t_object->Time_Undetected > MaxTime_Undetected) t_object->Status |= TO_STATUS_LOST;
               }
               // 0.3초마다 Best Shot 여부를 체크한다.
               if (t_object->Time_BestShot > 0.3f && !(t_object->Status & TO_STATUS_STATIC1) && (t_object->Status & TO_STATUS_MATCHED_1TO1)) { // PARAMETERS
                  t_object->Time_BestShot = 0.0f;
                  float score = t_object->GetBestShotScore (SrcImgSize);
                  if (score > t_object->MaxScore_BestShot) {
                     t_object->MaxScore_BestShot = score;
                     t_object->Status |= TO_STATUS_BEST_SHOT;
                  }
               }
            }
            // TO가 아직 TO_STATUS_VERIFIED의 상태가 되기 전인 경우에 행하는 검사.
            else {
               // LostObjects 리스트에서 TO와 매치가 되는 것이 있는지 조사한다.
               // TO와 매치가 되는 LO를 찾으면 TO와 LO를 합친다.
               TrackedObject *l_object = FindMatchingObjectInLostObjectList (t_object);
               if (l_object != NULL) {
                  #if defined(__DEBUG_OBT) && defined(__CONSOLE)
                  logd ("Tracked Object #%05d\n",t_object->ID);
                  logd ("     Merged with Lost Object #%04d\n",l_object->ID);
                  #endif
                  l_object->Merge (t_object);
                  LostObjects.Remove (l_object);
                  TrackedObjects.Add (l_object);
                  l_object->Status &= ~(TO_STATUS_LOST|TO_STATUS_UNDETECTED);
                  t_object->Status |= TO_STATUS_TO_BE_REMOVED;
               }
               if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED)) {
                  // TO가 TO_STATUS_UNDETECTED의 상태인 경우, Time_Undetected > MaxTime_Undetected 상태이면 제거한다.
                  if (t_object->Status & TO_STATUS_UNDETECTED) {
                     if (t_object->Time_Undetected > MaxTime_Undetected2) {
                        t_object->Status |= TO_STATUS_TO_BE_REMOVED;
                        #if defined(__DEBUG_OBT) && defined(__CONSOLE)
                        logd ("Tracked Object #%05d:\n",t_object->ID);
                        logd ("     Undetected & Time_Undetected > MaxTime_Undetected2: To be removed.\n");
                        #endif
                     }
                  }
                  else {
                     // TO의 초기 모션을 검사하여 적당치 않은 TO들을 제거한다.
                     int e_code = CheckObjectInitMotion (t_object);
                     if (e_code == DONE) {  // TO가 적합하다고 검증된 경우
                        if (t_object->Status & TO_STATUS_VALIDATED) SetObjectVerified (t_object);
                     }
                     else if (e_code > 0) { // TO를 제거하는 경우
                        t_object->Status |= TO_STATUS_TO_BE_REMOVED;
                        #if defined(__DEBUG_OBT) && defined(__CONSOLE)
                        logd ("Tracked Object #%05d:\n",t_object->ID);
                        logd ("     Initial motion not acceptable: To be removed.\n");
                        #endif
                     }
                     // e_code < 0 이면 판단을 유보한다.
                  }
               }
            }
         }
         // 생존한 객체들에 대해 기타 처리를 한다.
         if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED)) {
            t_object->FrameCount = FrameCount;
            t_object->UpdateAvgMinMaxPropertyValues (   );
            t_object->AddToTrkObjRegionList (trj_length);
            if (ForegroundDetector->SrcColorImage != NULL && (t_object->Status & TO_STATUS_BEST_SHOT))
               t_object->GetColorHistogram (*ForegroundDetector->SrcColorImage,ObjRegionMap,ForegroundDetector->FrgImage);
            if (t_object->Depth > n_layers) n_layers = t_object->Depth;
         }
      }
      #if defined(__DEBUG_OBT) && defined(__CONSOLE)
      if (t_object->Status & (TO_STATUS_TO_BE_REMOVED|TO_STATUS_OUT_OF_TRACKING_AREA|TO_STATUS_UNDETECTED|TO_STATUS_MERGED|TO_STATUS_SPLIT)) {
         logd ("Tracked Object #%05d: ",t_object->ID);
         if (t_object->Status & TO_STATUS_TO_BE_REMOVED       ) logd ("[To be removed]");
         if (t_object->Status & TO_STATUS_OUT_OF_TRACKING_AREA) logd ("[Out of tracking area]");
         if (t_object->Status & TO_STATUS_UNDETECTED          ) logd ("[Undetected]");
         if (t_object->Status & TO_STATUS_MERGED              ) logd ("[Merged]");
         if (t_object->Status & TO_STATUS_SPLIT               ) logd ("[Split]");
         logd ("\n");
      }
      #endif
      t_object = TrackedObjects.Next (t_object);
   }
   NumObjLayers = n_layers + 1; // 객체들의 총 레이어 수
   // LO들의 관리를 수행한다.
   ManageLostObjects  (   );
   // 추적이 종료된 TO의 종료 시각을 설정한다.
   SetObjectEndTime   (   );
   // 필요시 TO 별로 특징점 추적을 수행한다.
   TrackFeaturePoints (   );
   #if defined(__DEBUG_OBT)
   _DebugView_OBT->DrawImage (ObjRegionMap,_DVX_OBT,_DVY_OBT);
   t_object = TrackedObjects.First (   );
   while (t_object != NULL) {
      _DebugView_OBT->DrawRectangle (_DVX_OBT + t_object->X,_DVY_OBT + t_object->Y,t_object->Width,t_object->Height,PC_GREEN,1);
      text.Format (_T("%d"),t_object->ID);
      _DebugView_OBT->DrawText (text,_DVX_OBT + t_object->X - 10,_DVY_OBT + t_object->Y - 10,PC_YELLOW,TRANSPARENT,FT_ARIAL,14);
      t_object = TrackedObjects.Next (t_object);
   }
   _DVY_OBT += ObjRegionMap.Height;
   _DebugView_OBT->DrawText (_T("Object Region Map"),_DVX_OBT,_DVY_OBT,PC_GREEN);
   _DVY_OBT += 40;
   _DebugView_OBT->DrawText (_T("Tracked Objects --------------------------------------------------------------------------------"),_DVX_OBT,_DVY_OBT,PC_RED);
   _DVY_OBT += 30;
   t_object = TrackedObjects.First (   );
   while (t_object != NULL) {
      _DVX_OBT = 10;
      _DebugView_OBT->DrawImage (t_object->TemplateImage,_DVX_OBT,_DVY_OBT);
      _DVX_OBT += t_object->Width + 10;
      _DebugView_OBT->DrawImage (t_object->WeightImage,_DVX_OBT,_DVY_OBT);
      _DVX_OBT += t_object->Width + 10;
      _DebugView_OBT->DrawImage (t_object->MaskImage,_DVX_OBT,_DVY_OBT);
      _DVX_OBT = 0, _DVY_OBT += t_object->Height + 4;
      text.Format (_T("Tracked Object #%05d "),t_object->ID);
      if (t_object->Count_Tracked == 1) text += _T("[NEW]");
      if (t_object->Status & TO_STATUS_FAST_MOVING_OBJECT) text += _T("[FMO]");
      if (t_object->Status & TO_STATUS_TO_BE_REMOVED     ) text += _T("[RMV]");
      if (t_object->Status & TO_STATUS_VERIFIED_START    ) text += _T("[VFS]");
      _DebugView_OBT->DrawText (text,_DVX_OBT,_DVY_OBT,PC_WHITE);
      _DVY_OBT += 25;
      t_object = TrackedObjects.Next (t_object);
   }
   #endif
   return (DONE);
}

 void ObjectTracking::PerformOcclusionReasoning (GImage &s_image,TOArray1D &to_array,IArray1D &to_nos,IArray2D &fr_map,IBox2D &fb_region,int fb_no,IArray2D &d_map)

{
   int i,j,x,y;
   int x1,y1,x2,y2;
   
   // TO들을 MatchingPos에 위치시켰을 때, TO들과 FR를 둘러싸는 바운딩 박스를 구한다.
   // min_p와 max_p는 각각 바운딩 박스의 Left-Top Corner와 Right-Bottom Corner에 해당한다.
   IP2DArray1D p_array(to_array.Length * 2 + 2);
   for (i = j = 0; i < to_array.Length; i++) {
      TrackedObject* t_object = to_array[i];
      p_array[j++] = t_object->MatchingPos;
      p_array[j++](t_object->MatchingPos.X + t_object->Width,t_object->MatchingPos.Y + t_object->Height);
   }
   p_array[j++](fb_region.X,fb_region.Y);
   p_array[j++](fb_region.X + fb_region.Width,fb_region.Y + fb_region.Height);
   IPoint2D min_x,max_x,min_p,max_p;
   FindMinMax (p_array,min_p,max_p,min_x,max_x);
   min_p.X--, min_p.Y--, max_p.X++, max_p.Y++; 
   int width  = max_p.X - min_p.X;
   int height = max_p.Y - min_p.Y;
   // 각 픽셀 위치에서 TO들 사이의 겹치는 수를 구한다.
   IArray2D ov_map(width,height); // 각 픽셀에서 TO들의 겹치는 수가 저장된다.
   ov_map.Clear (   );
   Array1D<GImage> t_images(to_array.Length); // 각 TO의 템플릿 영상이 저장된다.
   for (i = 0; i < t_images.Length; i++) {
      t_images[i].Create (width,height);
      t_images[i].Clear  (   );
      TrackedObject* t_object = to_array[i];
      int sx = t_object->MatchingPos.X - min_p.X;
      int sy = t_object->MatchingPos.Y - min_p.Y;
      int width  = t_object->Width;
      int height = t_object->Height;
      byte** st_image = t_object->TemplateImage;
      byte** sm_image = t_object->MaskImage;
      byte** dt_image = t_images[i];
      for (y1 = 0,y2 = sy; y1 < height; y1++,y2++) {
         byte* l_sm_image = sm_image[y1];
         byte* l_st_image = st_image[y1];
         byte* l_dt_image = dt_image[y2];
         int*  l_ov_map   = ov_map[y2];
         for (x1 = 0,x2 = sx; x1 < width; x1++,x2++) {
            if (l_sm_image[x1]) {
               l_dt_image[x2] = l_st_image[x1];
               l_ov_map[x2]++;
            }
         }
      }
   }
   // 두 개 이상의 TO가 겹치는 영역에서 각 픽셀마다 입력 영상의 픽셀값과 각 TO의 픽셀값을 비교하여
   // 입력 영상과 가장 가까운 픽셀값을 갖는 TO를 찾아 그것의 스코어를 증가시킨다.
   IArray1D ov_array(to_array.Length); // 각 TO마다 겹치는 픽셀 수가 저장된다.
   IArray1D oc_array(to_array.Length); // 각 TO마다 입력 영상과 가장 가까운 픽셀 수가 저정된다.
   ov_array.Clear (   );
   oc_array.Clear (   );
   int sx1 = min_p.X, sy1 = min_p.Y;
   int ex1 = max_p.X, ey1 = max_p.Y;
   if (sx1 < 0) sx1 = 0;
   if (sy1 < 0) sy1 = 0;
   if (ex1 > s_image.Width ) ex1 = s_image.Width;
   if (ey1 > s_image.Height) ey1 = s_image.Height;
   int sx2 = sx1 - min_p.X;
   int sy2 = sy1 - min_p.Y;
   for (y1 = sy1,y2 = sy2; y1 < ey1; y1++,y2++) {
      byte* l_s_image = s_image[y1];
      int*  l_ov_map  = ov_map[y2];
      for (x1 = sx1,x2 = sx2; x1 < ex1; x1++,x2++) {
         if (l_ov_map[x2] > 1) {
            int min_i = 0,min_d = 256;
            for (i = 0; i < t_images.Length; i++) {
               byte** dt_image   = t_images[i];
               byte*  l_dt_image = dt_image[y2];
               if (l_dt_image[x2]) {
                  int d = abs ((int)l_s_image[x1] - l_dt_image[x2]);
                  if (d < min_d) min_d = d, min_i = i;
                  ov_array[i] += l_ov_map[x2];
               }
            }
            oc_array[min_i] += l_ov_map[x2];
         }
      }
   }
   OSArray1D os_array(to_array.Length);
   // 객체 별로 Depth 값을 구한다.
   // 객체가 카메라로부터 가까울수록 작은 Depth 값이 할당된다.
   if (FocalLength > 0.0f) {
      // Camera Calibration이 되어 있는 경우,
      // TO_i가 카메라로부터 가까울수록 Top-Center의 Y좌표 값은 큰 값을 갖는다.
      for (i = 0; i < os_array.Length; i++) {
         os_array[i].IndexNo = i;
         FPoint2D p = to_array[i]->GetPosition (0.5f,0.0f);
         os_array[i].Score = (double)p.Y;
      }
   }
   else {
      // Camera Calibration이 되어 있지 않은 경우,
      // TO_i가 카메라로부터 가까울수록 oc_array[i]/ov_array[i] 값은 큰 값을 갖는다.
      for (i = 0; i < os_array.Length; i++) {
         os_array[i].IndexNo = i;
         if (ov_array[i]) os_array[i].Score = (double)oc_array[i] / ov_array[i];
         else os_array[i].Score = 1.0;
      }
   }
   qsort (os_array,os_array.Length,sizeof(ObjectScore),Compare_ObjectScore_Descending);
   for (i = 0; i < os_array.Length; i++) to_array[os_array[i].IndexNo]->Depth = i;
   // 큰 Depth 값을 갖는 TO부터 or_map의 해당 영역에 레이블 값을 복사한다.
   IArray2D or_map(width,height);
   or_map.Clear (   );
   for (i = os_array.Length - 1; i >= 0; i--) {
      int    n = os_array[i].IndexNo;
      byte** t_image = t_images[n++];
      for (y = 0; y < height; y++) {
         byte* l_t_image = t_image[y];
         int*  l_or_map  = or_map[y];
         for (x = 0; x < width; x++)
            if (l_t_image[x]) l_or_map[x] = n;
      }   
   }
   // FR의 영역에서 TO들에 의해 겹치지 않은 곳들을 찾아내어 rm_image에 기록한다.
   GImage rm_image(or_map.Width,or_map.Height);
   rm_image.Clear (   );
   for (y1 = sy1,y2 = sy2; y1 < ey1; y1++,y2++) {
      byte* l_rm_image = rm_image[y2];
      int*  l_fr_map   = fr_map[y1];
      int*  l_or_map   = or_map[y2];
      for (x1 = sx1,x2 = sx2; x1 < ex1; x1++,x2++) {
         if (l_fr_map[x1] == fb_no) {
            if (!l_or_map[x2]) l_rm_image[x2] = PG_WHITE;
         }
         else l_or_map[x2] = PG_BLACK;
      }
   }
   // rm_image의 각각의 CC(Connected Component)에 대하여, 그 CC에 접하는 TO들의 픽셀의 수를 센다.
   IArray2D  rm_map(rm_image.Width,rm_image.Height);
   int n_crs = LabelCRs (rm_image,rm_map);
   IArray2D cn_matrix(to_array.Length + 1,n_crs + 1);
   cn_matrix.Clear (   );
   int ex2 = rm_map.Width  - 1;
   int ey2 = rm_map.Height - 1;
   for (y = 1; y < ey2; y++) {
      y1 = y - 1, y2 = y + 1;
      byte* l_rm_image0 = rm_image[y1];
      byte* l_rm_image1 = rm_image[y];
      byte* l_rm_image2 = rm_image[y2];
      int*  l_or_map0   = or_map[y1];
      int*  l_or_map1   = or_map[y];
      int*  l_or_map2   = or_map[y2];
      int*  l_rm_map    = rm_map[y];
      for (x = 1; x < ex2; x++) {
         x1 = x - 1, x2 = x + 1;
         if (l_rm_image1[x] && !(l_rm_image0[x] && l_rm_image2[x] && l_rm_image1[x1] && l_rm_image1[x2])) {
            int* l_cn_mat = cn_matrix[l_rm_map[x]];
            l_cn_mat[l_or_map0[x] ]++;
            l_cn_mat[l_or_map2[x] ]++;
            l_cn_mat[l_or_map1[x1]]++;
            l_cn_mat[l_or_map1[x2]]++;
         }
      }
   }
   // 각각의 CC는 접하는 픽셀 수가 가장 많은 TO에 속하는 것으로 결정한다.
   IArray1D cv_array1(cn_matrix.Width);
   IArray1D cv_array2(cn_matrix.Height);
   cv_array1.Clear (   );
   cv_array2.Clear (   );
   cv_array1.Copy (to_nos,0,to_nos.Length,1);
   for (i = 1; i < cn_matrix.Height; i++) {
      int  max_j  = 0;
      int  max_cn = 0;
      int* l_cn_matrix = cn_matrix[i];
      for (j = 1; j < cn_matrix.Width; j++)
         if (l_cn_matrix[j] > max_cn) max_cn = l_cn_matrix[j], max_j = j;
      if (max_cn) cv_array2[i] = cv_array1[max_j];
   }
   for (y1 = sy1,y2 = sy2; y1 < ey1; y1++,y2++) {
      int* l_or_map = or_map[y2];
      int* l_rm_map = rm_map[y2];
      int* l_d_map  = d_map[y1];
      for (x1 = sx1,x2 = sx2; x1 < ex1; x1++,x2++) {
         if      (l_or_map[x2]) l_d_map[x1] = cv_array1[l_or_map[x2]];
         else if (l_rm_map[x2]) l_d_map[x1] = cv_array2[l_rm_map[x2]];
      }
   }
}

 void ObjectTracking::PerformPostProcessing (   )

{
   TrackedObject* t_object = TrackedObjects.First (   );
   while (t_object != NULL) {
      t_object->AspectRatio2 = t_object->AspectRatio;
      t_object->Persons.Create (1);
      TrackedObject::PersonInfo& pi = t_object->Persons[0];
      pi.X      = t_object->Width / 2;
      pi.Y      = 0;
      pi.Height = t_object->Height;
      t_object  = TrackedObjects.Next (t_object);
   }
}

 int ObjectTracking::ReadFile (FileIO& file)

{
   if (file.Read ((byte*)&Options          ,1,sizeof(int  ))) return (1);
   if (file.Read ((byte*)&MinTime_Tracked  ,1,sizeof(float))) return (2);
   if (file.Read ((byte*)&MaxTime_Lost     ,1,sizeof(float))) return (3);
   if (file.Read ((byte*)&MaxTime_Static   ,1,sizeof(float))) return (4);
   if (file.Read ((byte*)&MinMovingDistance,1,sizeof(float))) return (5);
   if (TrackingAreas.ReadFile      (file)) return (6);
   if (NontrackingAreas.ReadFile   (file)) return (7);
   if (DynBkgAreas.ReadFile        (file)) return (8);
   if (NonHandoffAreas.ReadFile    (file)) return (9);
   if (FMODetector.ReadFile (file)) return (10);
   return (DONE);
}

 int ObjectTracking::ReadFile (CXMLIO* pIO)

{
   static ObjectTracking dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("ObjectTracking")) {
      xmlNode.Attribute (TYPE_ID(xint) ,"Options"          ,&Options          ,&dv.Options);
      xmlNode.Attribute (TYPE_ID(float),"MinTime_Tracked"  ,&MinTime_Tracked  ,&dv.MinTime_Tracked);
      xmlNode.Attribute (TYPE_ID(float),"MaxTime_Lost"     ,&MaxTime_Lost     ,&dv.MaxTime_Lost);
      xmlNode.Attribute (TYPE_ID(float),"MaxTime_Static"   ,&MaxTime_Static   ,&dv.MaxTime_Static);
      xmlNode.Attribute (TYPE_ID(float),"MinMovingDistance",&MinMovingDistance,&dv.MinMovingDistance);
      TrackingAreas.ReadFile    (pIO,"TrackingAreas");
      NontrackingAreas.ReadFile (pIO,"NontrackingAreas");
      DynBkgAreas.ReadFile      (pIO,"DynBkgAreas");
      NonHandoffAreas.ReadFile  (pIO,"NonHandoffAreas");
      FMODetector.ReadFile (pIO);
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}

 TrackedObject* ObjectTracking::RegisterNewObject (   )

{
   TrackedObject* t_object  = CreateInstance_TrackedObject (   );
   t_object->ID             = GetNewIntObjectID (   );
   t_object->InitFrameCount = FrameCount;
   t_object->StartTime.GetLocalTime (   );
   TrackedObjects.Add  (t_object);
   return (t_object);
}

 TrackedObject* ObjectTracking::RegisterNewObject (GImage& s_image,IBox2D& s_rgn,int s_no)

{
   TrackedObject* t_object = CreateInstance_TrackedObject (   );
   t_object->ID             = GetNewIntObjectID (   );
   t_object->InitFrameCount = FrameCount;
   t_object->Count_Tracked  = 1;
   t_object->Status         = TO_STATUS_MATCHED_1TO1;
   t_object->Time_Tracked   = FrameDuration;
   t_object->StartTime.GetLocalTime (   );
   t_object->InitTemplate (s_image,ObjRegionMap,s_rgn,s_no,ForegroundDetector->SrcColorImage);
   TrackedObjects.Add (t_object);
   return (t_object);
}

 void ObjectTracking::Reset (   )

{
   FrameCount = 0;
   TrackedObjects.Move (LostObjects);
   InvalidateAllTrkObjects (TrackedObjects);
}

 void ObjectTracking::SetObjectEndTime (   )

{
   TrackedObject* t_object = TrackedObjects.First (   );
   while (t_object != NULL) {
      if ((t_object->Status & TO_STATUS_VERIFIED) && (t_object->Status & TO_STATUS_TO_BE_REMOVED)) t_object->EndTime.GetLocalTime (   );
      t_object = TrackedObjects.Next (t_object);
   }
}

 void ObjectTracking::SetObjectVerified (TrackedObject *t_object)
 
{
   t_object->Status |= TO_STATUS_VERIFIED;
   t_object->Status |= TO_STATUS_VERIFIED_START;
   t_object->Status |= TO_STATUS_BEST_SHOT;
   t_object->MaxScore_BestShot = t_object->GetBestShotScore (SrcImgSize);
   t_object->Time_BestShot = 0.0f;
   int new_id = GetNewExtObjectID (   );
   #if defined(__DEBUG_OBT) && defined(__CONSOLE)
   logd ("Tracked Object #%05d:\n",t_object->ID);
   logd ("     Verified: New ID [%d] assigned.\n",new_id);
   #endif
   t_object->ID = new_id;
}

 void ObjectTracking::SyncBlobTrackingParams (   )

{
   BlobTracker.MinCount_Tracked    = MinCount_Tracked;
   BlobTracker.MaxTime_Undetected1 = MaxTime_Undetected;
   BlobTracker.MaxTime_Undetected2 = MaxTime_Undetected;
   BlobTracker.MaxTime_Verified    = MC_VLV;
   BlobTracker.MinMovingDistance1  = MinMovingDistance;
   BlobTracker.MinMovingDistance2  = 0.0f;
   BlobTracker.RegionUpdateRate    = 1.0f;
}

 void ObjectTracking::SyncBoxTrackingParams (   )

{
   BoxTracker.MinCount_Tracked   = MinCount_Tracked;
   BoxTracker.MaxTime_Undetected = MaxTime_Undetected;
   BoxTracker.MinMovingDistance  = MinMovingDistance;
}

 void ObjectTracking::TrackFeaturePoints (   )
#if defined(__LIB_OPENCV)
{
   int i,j,n;
   
   int max_fpt_len = (int)(MaxTime_FPT * FrameRate);
   // 특징점들을 검출하거나 추적할 필요가 있는지 알아본다.
   int flag_detect_feature_points = FALSE;
   int flag_track_feature_points  = FALSE;
   std::vector<TrackedObject*> to_list;
   TrackedObject* t_object = TrackedObjects.First (   );
   while (t_object != NULL) {
      if (!(t_object->Status & TB_STATUS_TO_BE_REMOVED)) {
         int flag_init_track = FALSE;
         if (t_object->IsFPTrackingEnabled (   )) {
            if (t_object->Status & TO_STATUS_FEATURE_POINT_TRACKING) {
               if (t_object->FPTrackLength > max_fpt_len) flag_init_track = TRUE;
               else flag_track_feature_points = TRUE;
            }
            else {
               flag_init_track = TRUE;
               t_object->ClearFPMotionPropertyValues (   );
            }
         }
         else t_object->Status &= ~TO_STATUS_FEATURE_POINT_TRACKING;
         if (flag_init_track) {
            flag_detect_feature_points = TRUE;
            t_object->FPTrackLength = 1;
            t_object->FPTracks.clear (   );
            t_object->Status &= ~TO_STATUS_FEATURE_POINT_TRACKING;
            to_list.push_back (t_object);
         }
      }
      t_object = TrackedObjects.Next (t_object);
   }
   GImage& s_image = ForegroundDetector->SrcImage;
   cv::Mat m_s_image;
   OpenCV_InitMat (s_image,m_s_image);
   // 특징점들을 검출할 필요가 있을 경우, 특징점들을 검출한다.
   if (flag_detect_feature_points) {
      n = (int)to_list.size (   );
      if (n > 254) n = 254;
      // 마스크 영상을 생성한다.
      GImage m_image(s_image.Width,s_image.Height);
      m_image.Clear (   );
      FPoint2D scale(1.2f,1.2f); // PARAMETERS
      ISize2D c_size(s_image.Width,s_image.Height);
      for (i = 0; i < n; i++) {
         IBox2D m_rgn = to_list[i]->Inflate (scale);
         CropRegion (c_size,m_rgn);
         m_image.Set (m_rgn,(byte)(i + 1));
      }
      // 특징점들을 검출한다.
      cv::Mat m_m_image;
      OpenCV_InitMat (m_image,m_m_image);
      std::vector<cv::Point2f> fps;
      int    max_n_points  = 254;   // PARAMETERS
      double quality_level = 0.001; // PARAMETERS
      double min_distance  = 5.0;   // PARAMETERS
      int    block_size    = 3;     // PARAMETERS
      goodFeaturesToTrack (m_s_image,fps,max_n_points,quality_level,min_distance,m_m_image,block_size,true);
      // 검출된 특징점들을 해당 TO에 할당한다.
      n = (int)fps.size (   );
      for (i = 0; i < n; i++) {
         cv::Point2f& fp = fps[i];
         j = (int)m_image[(int)fp.y][(int)fp.x];
         if (j) {
            std::vector<cv::Point2f> fp_track;
            fp_track.push_back (fp);
            to_list[j - 1]->FPTracks.push_back (fp_track);
         }
      }
   }
   // 특징점들을 추적할 필요가 있을 경우, 특징점들을 추적한다.
   int max_level = 3;        // PARAMETERS
   cv::Size win_size(21,21); // PARAMETERS
   if (flag_track_feature_points) {
      Swap (PrvImgPyramid,CurImgPyramid);
      buildOpticalFlowPyramid (m_s_image,*CurImgPyramid,win_size,max_level,true);
      TrackedObject* t_object = TrackedObjects.First (   );
      while (t_object != NULL) {
         if (!(t_object->Status & TB_STATUS_TO_BE_REMOVED)) {
            if (t_object->IsFPTrackingEnabled (   )) {
               if (t_object->Status & TO_STATUS_FEATURE_POINT_TRACKING) {
                  int fpt_length = t_object->FPTrackLength;
                  int lp = fpt_length - 1;
                  std::vector<std::vector<cv::Point2f> >& fp_tracks = t_object->FPTracks;
                  int n_tracks = (int)fp_tracks.size (   );
                  std::vector<cv::Point2f> p_fps;
                  for (i = 0; i < n_tracks; i++) {
                     if (fpt_length == (int)fp_tracks[i].size()) p_fps.push_back (fp_tracks[i][lp]);
                  }
                  if (p_fps.size (   )) {
                     std::vector<cv::Point2f> c_fps;
                     std::vector<uchar> status;
                     std::vector<float> error;
                     calcOpticalFlowPyrLK (*PrvImgPyramid,*CurImgPyramid,p_fps,c_fps,status,error,win_size,max_level);
                     float d_thrsld = 0.3f * GetMaximum (t_object->Width,t_object->Height); // PARAMETERS
                     for (i = j = 0; i < n_tracks; i++) {
                        if (fpt_length == (int)fp_tracks[i].size()) {
                           if (status[j]) {
                              Point2f dp = c_fps[j] - p_fps[j];
                              float d = (float)sqrt (dp.x * dp.x + dp.y * dp.y);
                              if (d < d_thrsld) fp_tracks[i].push_back (c_fps[j]);
                           }
                           j++;
                        }
                     }
                  }
                  t_object->FPTrackLength++; 
               }
            }
         }
         t_object = TrackedObjects.Next (t_object);
      }
   }
   else {
      if (flag_detect_feature_points) {
         buildOpticalFlowPyramid (m_s_image,*CurImgPyramid,win_size,max_level,true);
      }
      else {
         PrvImgPyramid->clear (   );
         CurImgPyramid->clear (   );
      }
   }
   // TO 별로 특정점 추적 정보를 이용하여 각종 속성 값을 계산한다.
   t_object = TrackedObjects.First (   );
   while (t_object != NULL) {
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED) && (t_object->Status & TO_STATUS_FEATURE_POINT_TRACKING)) {
         t_object->GetFPMotionProperties1 (FrameDuration);
         if (t_object->FPTrackLength > max_fpt_len) t_object->GetFPMotionProperties2 (FrameDuration);
      }
      t_object = TrackedObjects.Next (t_object);
   }
   // 특징점 추적을 시작한 TO들을 TO_STATUS_FEATURE_POINT_TRACKING 상태로 설정한다.
   n = (int)to_list.size (   );
   for (i = 0; i < n; i++) {
      to_list[i]->Status |= TO_STATUS_FEATURE_POINT_TRACKING;
   }
   #if defined(__DEBUG_FPT)
   _DVX_FPT = _DVY_FPT = 0;
   _DebugView_FPT->Clear (   );
   _DebugView_FPT->DrawImage (ForegroundDetector->SrcImage,_DVX_FPT,_DVY_FPT);
   t_object = TrackedObjects.First (   );
   while (t_object != NULL) {
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED) && (t_object->Status & TO_STATUS_FEATURE_POINT_TRACKING)) {
         std::vector<std::vector<cv::Point2f> >& fp_tracks = t_object->FPTracks;
         int n_tracks = (int)fp_tracks.size (   );
         for (i = 0; i < n_tracks; i++) {
            std::vector<cv::Point2f>& fp_track = fp_tracks[i];
            n = (int)fp_track.size (   );
            for (j = 1; j < n; j++) {
               int j1 = j - 1;
               _DebugView_FPT->DrawLine (
                  (int)fp_track[j1].x + _DVX_FPT,
                  (int)fp_track[j1].y + _DVY_FPT,
                  (int)fp_track[j ].x + _DVX_FPT,
                  (int)fp_track[j ].y + _DVY_FPT,
                  PC_RED
               );
            }
         }
         _DebugView_FPT->DrawRectangle (t_object->X + _DVX_FPT,t_object->Y + _DVY_FPT,t_object->Width,t_object->Height,PC_GREEN,2);
         CString text;
         text.Format (_T("FV: %.2f"),t_object->FPFlowVariance);
         int px = _DVX_FPT + t_object->X + t_object->Width + 5;
         int py = _DVY_FPT + t_object->Y;
         _DebugView_FPT->DrawText (text,px,py,PC_YELLOW,TRANSPARENT,FT_ARIAL,14);
         py += 20;
         text.Format (_T("FA: %.2f"),t_object->FPFlowActivity);
         _DebugView_FPT->DrawText (text,px,py,PC_YELLOW,TRANSPARENT,FT_ARIAL,14);
      }
      t_object = TrackedObjects.Next (t_object);
   }
   _DebugView_FPT->Invalidate (   );
   #endif
}
#else
{

}
#endif

 int ObjectTracking::UpdateAreaMaps (ISize2D& ri_size)
 
{
   if (!IsEnabled (   )) return (1);
   // Obtaining TrkAreaMap
   TrackingAreas.SetRefImageSize (ri_size);
   if (TrackingAreas.GetNumItems (   )) {
      TrkAreaMap.Clear (   );
      TrackingAreas.RenderAreaMap (TrkAreaMap,PG_WHITE);
   }
   else TrkAreaMap.Set (0,0,TrkAreaMap.Width,TrkAreaMap.Height,PG_WHITE);
   NontrackingAreas.SetRefImageSize (ri_size);
   if (NontrackingAreas.GetNumItems (   )) NontrackingAreas.RenderAreaMap (TrkAreaMap,128);
   // Obtaining DynBkgAreaMap
   DynBkgAreaMap.Clear (   );
   DynBkgAreas.SetRefImageSize (ri_size);
   if (DynBkgAreas.GetNumItems (   )) DynBkgAreas.RenderAreaMap (DynBkgAreaMap,PG_WHITE);
   // Obtaining NonHandoffAreaMap
   NonHandoffAreaMap.Clear (   );
   NonHandoffAreas.SetRefImageSize (ri_size);
   if (NonHandoffAreas.GetNumItems (   )) NonHandoffAreas.RenderAreaMap (NonHandoffAreaMap,PG_WHITE);
   return (DONE);
}
 
 void ObjectTracking::UpdateFaceRegions (   )

{
   TrackedObject* t_object = TrackedObjects.First (   );
   while (t_object != NULL) {
      t_object->Face(*t_object);
      t_object = TrackedObjects.Next (t_object);
   }
}

 int ObjectTracking::UpdateObjectThumbnails (byte *si_buffer,ISize2D &si_size,int pix_fmt,float cbs_ratio,ISize2D *tn_size)
 
{
   if (!IsEnabled (   )) return (1);
   FPoint2D scale;
   scale.X = (float)si_size.Width  / SrcImgSize.Width;
   scale.Y = (float)si_size.Height / SrcImgSize.Height;
   TrackedObject* t_object = TrackedObjects.First (   );
   while (t_object != NULL) {
      if (t_object->Status & TO_STATUS_BEST_SHOT) {
         IBox2D so_box = t_object->Scale (scale);
         t_object->ThumbPixFmt = pix_fmt;
         CaptureThumbnail (si_buffer,si_size,pix_fmt,so_box,t_object->ThumbBuffer,t_object->ThumbSize,cbs_ratio,tn_size);
      }
      t_object = TrackedObjects.Next (t_object);
   }
   return (DONE);
}

 int ObjectTracking::WriteFile (FileIO &file)

{
   if (file.Write ((byte *)&Options          ,1,sizeof(int  ))) return (1);
   if (file.Write ((byte *)&MinTime_Tracked  ,1,sizeof(float))) return (2);
   if (file.Write ((byte *)&MaxTime_Lost     ,1,sizeof(float))) return (3);
   if (file.Write ((byte *)&MaxTime_Static   ,1,sizeof(float))) return (4);
   if (file.Write ((byte *)&MinMovingDistance,1,sizeof(float))) return (5);
   if (TrackingAreas.WriteFile      (file)) return (6);
   if (NontrackingAreas.WriteFile   (file)) return (7);
   if (DynBkgAreas.WriteFile        (file)) return (8);
   if (NonHandoffAreas.WriteFile    (file)) return (9);
   if (FMODetector.WriteFile (file)) return (10);
   return (DONE);
}

 int ObjectTracking::WriteFile (CXMLIO* pIO)

{
   static ObjectTracking dv; // jun : default value;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("ObjectTracking"))
   {
      xmlNode.Attribute (TYPE_ID (xint) ,"Options"          ,&Options          ,&dv.Options);
      xmlNode.Attribute (TYPE_ID (float),"MinTime_Tracked"  ,&MinTime_Tracked  ,&dv.MinTime_Tracked);
      xmlNode.Attribute (TYPE_ID (float),"MaxTime_Lost"     ,&MaxTime_Lost     ,&dv.MaxTime_Lost);
      xmlNode.Attribute (TYPE_ID (float),"MaxTime_Static"   ,&MaxTime_Static   ,&dv.MaxTime_Static);
      xmlNode.Attribute (TYPE_ID (float),"MinMovingDistance",&MinMovingDistance,&dv.MinMovingDistance);
      TrackingAreas.WriteFile      (pIO,"TrackingAreas");
      NontrackingAreas.WriteFile   (pIO,"NontrackingAreas");
      DynBkgAreas.WriteFile        (pIO,"DynBkgAreas");
      NonHandoffAreas.WriteFile    (pIO,"NonHandoffAreas");
      FMODetector.WriteFile (pIO);
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}

}

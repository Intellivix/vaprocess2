#include "vacl_conrgn.h"
#include "vacl_edge.h"
#include "vacl_foregnd.h"
 
 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: BoundaryEdgeInfoExtraction
//
///////////////////////////////////////////////////////////////////////////////

 BoundaryEdgeInfoExtraction::BoundaryEdgeInfoExtraction (   )
 
{
   ForegroundDetector = NULL;
}

 int BoundaryEdgeInfoExtraction::Initialize (ForegroundDetection& frg_detector,IArray2D& cr_map)

{
   ForegroundDetector = &frg_detector;
   return (BoundaryPixelTracking::Initialize (cr_map));
}

 void BoundaryEdgeInfoExtraction::ProcessBoundaryPixel (   )
 
{
   byte** be_image  = ForegroundDetector->BkgEdgeImage;
   byte** fe_image1 = ForegroundDetector->FrgEdgeImage1;
   byte** fe_image2 = ForegroundDetector->FrgEdgeImage2;
   byte** fe_image3 = ForegroundDetector->FrgEdgeImage3;
   if (be_image  != NULL && (be_image [Y][X] || be_image [Y][X - 1] || be_image [Y][X + 1] || be_image [Y - 1][X] || be_image [Y + 1][X])) NumBkgEdgePixels++;
   if (fe_image1 != NULL && (fe_image1[Y][X] || fe_image1[Y][X - 1] || fe_image1[Y][X + 1] || fe_image1[Y - 1][X] || fe_image1[Y + 1][X])) NumFrgEdgePixels1++;
   if (fe_image2 != NULL && (fe_image2[Y][X] || fe_image2[Y][X - 1] || fe_image2[Y][X + 1] || fe_image2[Y - 1][X] || fe_image2[Y + 1][X])) NumFrgEdgePixels2++;
   if (fe_image3 != NULL && (fe_image3[Y][X] || fe_image3[Y][X - 1] || fe_image3[Y][X + 1] || fe_image3[Y - 1][X] || fe_image3[Y + 1][X])) NumFrgEdgePixels3++;
}

 int BoundaryEdgeInfoExtraction::StartTracking (   )
 
{
   if (ForegroundDetector == NULL) return (1);
   NumBkgEdgePixels  = 0;
   NumFrgEdgePixels1 = 0;
   NumFrgEdgePixels2 = 0;
   NumFrgEdgePixels3 = 0;
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 void DetectEdges_Sobel (GImage &s_image,short es_thrsld,GImage &d_image)
 
{
   int x,y;
   
   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   for (y = 1; y < ey; y++) {
      int y1 = y - 1;
      int y2 = y + 1;
      byte* l_s_image  = s_image[y];
      byte* l_s_image1 = s_image[y1];
      byte* l_s_image2 = s_image[y2];
      byte* l_d_image  = d_image[y];
      for (x = 1; x < ex; x++) {
         int x1 = x - 1;
         int x2 = x + 1;
         short gx = (l_s_image2[x2] + 2 * (short)l_s_image[x2] + l_s_image1[x2]) -
                    (l_s_image2[x1] + 2 * (short)l_s_image[x1] + l_s_image1[x1]);
         short gy = (l_s_image2[x2] + 2 * (short)l_s_image2[x] + l_s_image2[x1]) -
                    (l_s_image1[x1] + 2 * (short)l_s_image1[x] + l_s_image1[x2]);
         short es = (short)(abs (gx) + abs (gy));
         if (es >= es_thrsld) l_d_image[x] = PG_WHITE;
         else l_d_image[x] = PG_BLACK;
      }
   }
   d_image.SetBoundary (1,PG_BLACK);
}

 void GetGradientMagnitudes (SArray2D &dx_array,SArray2D &dy_array,SArray2D &gm_array)
 
{
   int x,y;
   
   for (y = 0; y < gm_array.Height; y++) {
      short* l_dx_array = dx_array[y];
      short* l_dy_array = dy_array[y];
      short* l_gm_array = gm_array[y];
      for (x = 0; x < gm_array.Width; x++)
         l_gm_array[x] = (short)(abs(l_dx_array[x]) + abs(l_dy_array[x]));
   }
}

 void GetGradientMagnitudes_Sobel (GImage &s_image,FArray2D &d_array)

{
   int x,y;

   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   for (y = 1; y < ey; y++) {
      int y1 = y - 1;
      int y2 = y + 1;
      byte*  l_s_image  = s_image[y];
      byte*  l_s_image1 = s_image[y1];
      byte*  l_s_image2 = s_image[y2];
      float* l_d_array  = d_array[y];
      for (x = 1; x < ex; x++) {
         int x1 = x - 1;
         int x2 = x + 1;
         short gx = (l_s_image2[x2] + 2 * (short)l_s_image[x2] + l_s_image1[x2]) -
                    (l_s_image2[x1] + 2 * (short)l_s_image[x1] + l_s_image1[x1]);
         short gy = (l_s_image2[x2] + 2 * (short)l_s_image2[x] + l_s_image2[x1]) -
                    (l_s_image1[x1] + 2 * (short)l_s_image1[x] + l_s_image1[x2]);
         float gm = (float)(abs (gx) + abs (gy));
         l_d_array[x] = gm;
      }
   }
   d_array.FillBoundary (1);
}

 void GetGradientMagnitudes_Sobel (GImage &s_image,SArray2D &d_array)

{
   int x,y;

   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   for (y = 1; y < ey; y++) {
      int y1 = y - 1;
      int y2 = y + 1;
      byte*  l_s_image  = s_image[y];
      byte*  l_s_image1 = s_image[y1];
      byte*  l_s_image2 = s_image[y2];
      short* l_d_array  = d_array[y];
      for (x = 1; x < ex; x++) {
         int x1 = x - 1;
         int x2 = x + 1;
         short gx = (l_s_image2[x2] + 2 * (short)l_s_image[x2] + l_s_image1[x2]) -
                    (l_s_image2[x1] + 2 * (short)l_s_image[x1] + l_s_image1[x1]);
         short gy = (l_s_image2[x2] + 2 * (short)l_s_image2[x] + l_s_image2[x1]) -
                    (l_s_image1[x1] + 2 * (short)l_s_image1[x] + l_s_image1[x2]);
         short gm = (short)(abs (gx) + abs (gy));
         l_d_array[x] = gm;
      }
   }
   d_array.FillBoundary (1);
}

 short GetGradientMagnitudeThreshold (SArray2D &s_array,short min_gm,float a)
 
{
   int x,y;
   
   int n = 0, sum = 0;
   for (y = 0; y < s_array.Height; y++) {
      short* l_s_array = s_array[y];
      for (x = 0; x < s_array.Width; x++) {
         if (l_s_array[x] >= min_gm) {
            sum += l_s_array[x];
            n++;
         }
      }
   }
   if (!n) return (0);
   short threshold = (short)(a * sum / n);
   return (threshold);
}

 void GetGradients (GImage &s_image,FArray2D &dx_array,FArray2D &dy_array)

{
   int x,y;

   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   for (y = 1; y < ey; y++) {
      byte*  l_s_image  = s_image[y];
      byte*  l_s_image1 = s_image[y - 1];
      byte*  l_s_image2 = s_image[y + 1];
      float* l_dx_array = dx_array[y];
      float* l_dy_array = dy_array[y];
      for (x = 1; x < ex; x++) {
         l_dx_array[x] = 0.5f * ((short)l_s_image[x + 1] - l_s_image[x - 1]);
         l_dy_array[x] = 0.5f * ((short)l_s_image2[x]    - l_s_image1[x]   );
      }
   }
   dx_array.FillBoundary (1);
   dy_array.FillBoundary (1);
}

 void GetGradients (GImage &s_image,SArray2D &dx_array,SArray2D &dy_array)

{
   int x,y;

   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   for (y = 1; y < ey; y++) {
      byte*  l_s_image  = s_image[y];
      byte*  l_s_image1 = s_image[y - 1];
      byte*  l_s_image2 = s_image[y + 1];
      short* l_dx_array = dx_array[y];
      short* l_dy_array = dy_array[y];
      for (x = 1; x < ex; x++) {
         l_dx_array[x] = ((short)l_s_image[x + 1] - l_s_image[x - 1]) / 2;
         l_dy_array[x] = ((short)l_s_image2[x]    - l_s_image1[x]   ) / 2;
      }
   }
   dx_array.FillBoundary (1);
   dy_array.FillBoundary (1);
}

 void GetRegionInteriorEdgeInfo (ForegroundDetection& frg_detector,IArray2D& cr_map,IBox2D& s_rgn,int s_no,int& n_feps,int& n_beps)

{
   int x,y;
   
   n_feps = n_beps = 0;
   int sx = s_rgn.X;
   int sy = s_rgn.Y;
   int ex = sx + s_rgn.Width;
   int ey = sy + s_rgn.Height;
   if (sx < 2) sx = 2;
   if (sy < 2) sy = 2;
   if (ex > cr_map.Width  - 2) ex = cr_map.Width  - 2;
   if (ey > cr_map.Height - 2) ey = cr_map.Height - 2;
   for (y = sy; y < ey; y++) {
      int*  l_cr_map     = cr_map[y];
      int*  l_cr_map_ym2 = cr_map[y - 2];
      int*  l_cr_map_ym1 = cr_map[y - 1];
      int*  l_cr_map_yp1 = cr_map[y + 1];
      int*  l_cr_map_yp2 = cr_map[y + 2];
      byte* l_fe_image = frg_detector.FrgEdgeImage2[y];
      byte* l_be_image = frg_detector.BkgEdgeImage[y];
      for (x = sx; x < ex; x++) {
         if (l_cr_map[x] == s_no &&
             l_cr_map[x - 2] == s_no && l_cr_map[x - 1] == s_no && l_cr_map[x + 1] == s_no && l_cr_map[x + 2] == s_no &&
             l_cr_map_ym2[x] == s_no && l_cr_map_ym1[x] == s_no && l_cr_map_yp1[x] == s_no && l_cr_map_yp2[x] == s_no) { // 영역 바운더리 픽셀들은 제외시킨다.
            if (l_fe_image[x]) n_feps++;
            if (l_be_image[x]) n_beps++;
         }
      }
   }
}

}

#include "ML/File.h"
#include "ML/OutputDataStream.h"

 namespace ML

{

OutputDataStream::OutputDataStream()
{
    
}

OutputDataStream::~OutputDataStream()
{
    
}

int OutputDataStream::SaveToFile(const String &Filename)
{
    FILE *File = CheckedFOpen(Filename.CString(), "wb");
    if (File == NULL) return (1);
    UINT MyLength = _Data.Length();
    CheckedFWrite(&MyLength, sizeof(UINT), 1, File);
    CheckedFWrite(_Data.CArray(), sizeof(BYTE), MyLength, File);
    fclose(File);
    return (DONE);
}

void OutputDataStream::SaveToFileNoHeader(const String &Filename)
{
    FILE *File = CheckedFOpen(Filename.CString(), "wb");
    CheckedFWrite(_Data.CArray(), sizeof(BYTE), _Data.Length(), File);
    fclose(File);
}

void OutputDataStream::WriteData(const BYTE *Data, UINT BytesToWrite)
{
    if(BytesToWrite > 0)
    {
        UINT StartLength = _Data.Length();
        _Data.ReSize(StartLength + BytesToWrite);
        memcpy(_Data.CArray() + StartLength, Data, BytesToWrite);
    }
}

OutputDataStream& operator << (OutputDataStream &S, const String &V)
{
    UINT Length = V.Length();
    S << Length;
    S.WriteData((const BYTE *)V.CString(), Length);
    return S;
}

}

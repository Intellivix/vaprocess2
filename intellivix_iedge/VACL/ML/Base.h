#pragma once

#include "bccl_define.h"
#include <algorithm>
#include <iostream>
#include <sstream>
#include <fstream>

using namespace std;

#if defined(__linux)
#if !defined(__forceinline)
#define __forceinline inline
#endif
#endif

#include "ML/Console.h"

#define SUPPRESS_CONSOLE

 namespace ML

{

namespace Console
{
#ifdef SUPPRESS_CONSOLE
    ofstream ConsoleFile;
#else
    ofstream ConsoleFile("Console.txt");
#endif

    ofstream& File()
    {
        return ConsoleFile;
    }

    void AdvanceLine()
    {
        WriteLine("");
    }

    void WriteLine(const String &S)
    {
#if defined(__WIN32)
        HANDLE StdHandle = GetStdHandle(STD_OUTPUT_HANDLE);
        DWORD BytesWritten;
        WriteConsole(StdHandle, S.CString(), S.Length(), &BytesWritten, NULL);
        WriteConsole(StdHandle, "\n", 1, &BytesWritten, NULL);
        ConsoleFile << S << endl;
        ConsoleFile.flush();
#endif
    }

    void OverwriteLine(const String &S)
    {
#if defined(__WIN32)
        const UINT ConsoleWidth = 79;
        HANDLE StdHandle = GetStdHandle(STD_OUTPUT_HANDLE);
        CONSOLE_SCREEN_BUFFER_INFO CursorInfo;
        GetConsoleScreenBufferInfo(StdHandle, &CursorInfo);
        CursorInfo.dwCursorPosition.X = 0;
        CursorInfo.dwCursorPosition.Y = Math::Max(CursorInfo.dwCursorPosition.Y - 1, 0);
        SetConsoleCursorPosition(StdHandle, CursorInfo.dwCursorPosition);
        DWORD BytesWritten;
        String FinalString = S;
        while(FinalString.Length() < ConsoleWidth)
        {
            FinalString.PushEnd(' ');
        }
        FinalString.PushEnd('\n');
        WriteConsole(StdHandle, FinalString.CString(), FinalString.Length(), &BytesWritten, NULL);
        ConsoleFile << S << endl;
        ConsoleFile.flush();
#endif
    }

    void WriteString(const String &S)
    {
#if defined(__WIN32)
        HANDLE StdHandle = GetStdHandle(STD_OUTPUT_HANDLE);
        DWORD BytesWritten;
        WriteConsole(StdHandle, S.CString(), S.Length(), &BytesWritten, NULL);
        ConsoleFile << S;
        ConsoleFile.flush();
#endif
    }

    void WriteLine(const char *S)
    {
#if defined(__WIN32)
        HANDLE StdHandle = GetStdHandle(STD_OUTPUT_HANDLE);
        DWORD BytesWritten;
        WriteConsole(StdHandle, S, strlen(S), &BytesWritten, NULL);
        WriteConsole(StdHandle, "\n", 1, &BytesWritten, NULL);
        ConsoleFile << S << endl;
        ConsoleFile.flush();
#endif
    }

    void OverwriteLine(const char *S)
    {
        OverwriteLine(String(S));
    }

    void WriteString(const char *S)
    {
#if defined(__WIN32)
        HANDLE StdHandle = GetStdHandle(STD_OUTPUT_HANDLE);
        DWORD BytesWritten;
        WriteConsole(StdHandle, S, strlen(S), &BytesWritten, NULL);
        ConsoleFile << S;
        ConsoleFile.flush();
#endif
    }
}

}

#pragma once

#include "ML/String.h"

 namespace ML

{

void Assert(bool Statement, const char *Description);
void Assert(bool Statement, const String &Description);
void PersistentAssert(bool Statement, const char *Description);
void PersistentAssert(bool Statement, const String &Description);
void SignalError(const char *Description);
void SignalError(const String &Description);
void PersistentSignalError(const char *Description);
void PersistentSignalError(const String &Description);

}

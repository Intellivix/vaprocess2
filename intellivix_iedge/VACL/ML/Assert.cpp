#include "ML/Assert.h"
#include "bccl_console.h"

 namespace ML

{

 void Assert(bool Statement,const char *Description)

{
   if (!Statement) {
      logd ("[ERROR] %s\n",Description);
   }
}

 void Assert(bool Statement,const String &Description)

{
   if (!Statement) {
      logd ("[ERROR] %s\n",Description.CString());
   }
}

 void SignalError(const char *Description)

{
   logd ("[ERROR] %s\n",Description);
}

 void SignalError(const String &Description)

{
   logd ("[ERROR] %s\n",Description.CString());
}

 void PersistentAssert(bool Statement,const char *Description)

{
   if (!Statement) {
      logd ("[ERROR] %s\n",Description);
   }
}

 void PersistentAssert(bool Statement,const String &Description)

{
   if (!Statement) {
      logd ("[ERROR] %s\n",Description.CString());
   }
}

 void PersistentSignalError(const char *Description)

{
   logd ("[ERROR] %s\n",Description);
}

 void PersistentSignalError(const String &Description)

{
   logd ("[ERROR] %s\n",Description.CString());
}

}

#pragma once

#include "ML/Base.h"

 namespace ML

{

 namespace Math

{
    const double PI = 3.1415926535897932384626433832795028842;
    const float PIf = 3.14159265358979323846f;

    //
    // rnd() returns a number between 0.0 and 1.0
    //
    #define rnd()            (((FLOAT)rand() ) / RAND_MAX)

    //
    // pmrnd() returns a number between -1.0 and 1.0
    //
    #define pmrnd()            ((rnd() - 0.5f) * 2.0f)

    __forceinline float DegreesToRadians(float x)
    {
        return x * PIf / 180.0f;
    }

    __forceinline float RadiansToDegrees(float x)
    {
        return x * 180.0f / PIf;
    }

    __forceinline float Sign(float x)
    {
        if(x < 0.0f)
        {
            return -1.0f;
        }
        else
        {
            return 1.0f;
        }
    }

    __forceinline double Sigmoid(double X)
    {
        return 1.0 / (1.0 + exp(-X));
    }
    
    //Math::LinearMap is a very useful function.  Given a source interval (s1, e1),
    //a target interval (s2, e2), and a source value start, returns the mapping
    //of the source value's position on the source interval to the same relative
    //position in the target interval.  For example, Math::LinearMap(-1, 1, 0, ScreenWidth, X)
    //would map from perspective space to screen space; in other words
    //LinearMap(-1, 1, 0, ScreenWidth, -1)        = 0
    //LinearMap(-1, 1, 0, ScreenWidth, -0.5)    = ScreenWidth*0.25
    //LinearMap(-1, 1, 0, ScreenWidth, 0)        = ScreenWidth*0.5
    //LinearMap(-1, 1, 0, ScreenWidth, 0.5)        = ScreenWidth*0.75
    //LinearMap(-1, 1, 0, ScreenWidth, 1)        = ScreenWidth
    __forceinline float LinearMap(float s1, float e1, float s2, float e2, float start)
    {
        return ((start-s1)*(e2-s2)/(e1-s1)+s2);
    }

    __forceinline double LinearMap(double s1, double e1, double s2, double e2, double start)
    {
        return ((start-s1)*(e2-s2)/(e1-s1)+s2);
    }

    __forceinline float Lerp(float Left, float Right, float s)
    {
        return (Left + s * (Right - Left));
    }

    __forceinline double Lerp(double Left, double Right, double s)
    {
        return (Left + s * (Right - Left));
    }

    __forceinline long double Lerp(long double Left, long double Right, long double s)
    {
        return (Left + s * (Right - Left));
    }

    template <class type> __forceinline type Abs(type x)
    {
        if(x < 0)
        {
            return -x;
        }
        else
        {
            return x;
        }
    }

    __forceinline int Mod(int x, UINT M)
    {
        if(x >= 0)
        {
            return (x % M);
        }
        else
        {
            return ((x + (x / int(M) + 2) * int(M)) % M);
        }
    }

    __forceinline int Floor(float x)
    {
        if(x >= 0.0f)
        {
            return int(x);
        }
        else
        {
            return int(x) - 1;
        }
    }

    __forceinline int Floor(double x)
    {
        if(x >= 0.0)
        {
            return int(x);
        }
        else
        {
            return int(x) - 1;
        }
    }

    __forceinline int Ceiling(float x)
    {
        int FloorX = Floor(x);
        if(x == float(FloorX))
        {
            return FloorX;
        }
        else
        {
            return FloorX + 1;
        }
    }

    __forceinline int Round(float x)
    {
        return int(x + 0.5f);
    }

    __forceinline int Round(double x)
    {
        return int(x + 0.5);
    }

    template <class type> __forceinline type Square(type T)
    {
        return T * T;
    }

    template <class type> __forceinline type Min(type A, type B)
    {
        if(A < B)
        {
            return A;
        }
        else
        {
            return B;
        }
    }

    template <class type> __forceinline type Min(type A, type B, type C)
    {
        if(A < B && A < C)
        {
            return A;
        }
        else if(B < C)
        {
            return B;
        }
        else
        {
            return C;
        }
    }

    template <class type> __forceinline type Max(type A, type B)
    {
        if(A > B)
        {
            return A;
        }
        else
        {
            return B;
        }
    }

    template <class type> __forceinline type Max(type A, type B, type C)
    {
        if(A > B && A > C)
        {
            return A;
        }
        else if(B > C)
        {
            return B;
        }
        else
        {
            return C;
        }
    }

}

}

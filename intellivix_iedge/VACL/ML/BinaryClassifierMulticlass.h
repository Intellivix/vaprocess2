#pragma once

 namespace ML

{

template<class LearnerInput> class MulticlassClassifier;
template<class LearnerInput> class MulticlassClassifierFactory;

template<class LearnerInput>
class BinaryClassifierMulticlass : public BinaryClassifier<LearnerInput>
{
public:
	typedef ClassifierDataset<LearnerInput> Dataset;
	typedef ClassifierExample<LearnerInput> Example;

    BinaryClassifierMulticlass(MulticlassClassifier<LearnerInput> &Learner)
    {
        _Learner = &Learner;
    }

    void Train(const Dataset &Examples, UINT Class0Index, UINT Class1Index)
    {
        _Class0Index = Class0Index;
        _Class1Index = Class1Index;
        _Learner->Train(Examples);
    }

    void TrainEx(const Dataset &Examples, Vector<double>& SampleWeights, double WeightTrimRate, UINT Class0Index, UINT Class1Index)
    {
        _Class0Index = Class0Index;
        _Class1Index = Class1Index;
        _Learner->TrainEx(Examples, SampleWeights, WeightTrimRate);
    }

    void Evaluate(const LearnerInput &Input, UINT &Class, double &ProbabilityClass0) const
    {
        _Learner->Evaluate(Input, Class, _ClassProbabilitiesStorage);
        ProbabilityClass0 = _ClassProbabilitiesStorage[_Class0Index];
    }

    UINT Class0Index() const
    {
        return _Class0Index;
    }

    UINT Class1Index() const
    {
        return _Class1Index;
    }

    int LoadFromBinaryStream (InputDataStream &Stream)
    {
        Stream >> _Class0Index;
        Stream >> _Class1Index;
        _Learner->LoadFromBinaryStream (Stream);
        return (DONE);
    }

    int SaveToBinaryStream (OutputDataStream &Stream)
    {
        Stream << _Class0Index;
        Stream << _Class1Index;
        _Learner->SaveToBinaryStream (Stream);
        return (DONE);
    }

private:
    UINT _Class0Index, _Class1Index;
    mutable Vector<double> _ClassProbabilitiesStorage;
    MulticlassClassifier<LearnerInput> *_Learner;
};

template<class LearnerInput>
class BinaryClassifierFactoryMulticlass : public BinaryClassifierFactory<LearnerInput>
{
public:
    BinaryClassifierFactoryMulticlass() {}
    BinaryClassifierFactoryMulticlass(const MulticlassClassifierFactory<LearnerInput> *MulticlassFactory)
    {
        _MulticlassFactory = MulticlassFactory;
    }
    void Configure(const MulticlassClassifierFactory<LearnerInput> *MulticlassFactory)
    {
        _MulticlassFactory =  MulticlassFactory;
    }

    BinaryClassifier<LearnerInput>* MakeClassifier() const
    {
        MulticlassClassifier<LearnerInput> *MulticlassLearner = _MulticlassFactory->MakeClassifier();
        BinaryClassifierMulticlass<LearnerInput> *BinaryLearner = new BinaryClassifierMulticlass<LearnerInput>(*MulticlassLearner);
        return BinaryLearner;
    }

private:
    const MulticlassClassifierFactory<LearnerInput> *_MulticlassFactory;
};

}

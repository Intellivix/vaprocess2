#pragma once


#include "ML/String.h"

 namespace ML

{

namespace Console
{
    ofstream& File();
    void AdvanceLine();
    
    void WriteLine(const String &S);
    void WriteLine(const char *S);
    
    void OverwriteLine(const String &S);
    void OverwriteLine(const char *S);
    
    void WriteString(const String &S);
    void WriteString(const char *S);
}

}



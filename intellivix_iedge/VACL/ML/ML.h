#pragma once

#include "bccl_define.h"

#if defined(__WIN32)
#pragma warning(disable : 4003)
#pragma warning(disable : 4700)
#endif

#include "ML/String.h"
#include "ML/Vector.h"
#include "ML/File.h"
#include "ML/Console.h"
#include "ML/InputDataStream.h"
#include "ML/OutputDataStream.h"
#include "ML/BinaryClassifierAdaBoost.h"
#include "ML/BinaryClassifierMulticlass.h"
#include "ML/BinaryClassifierDecisionTree.h"

/*
#include "ML/Grid.h"
#include "ML/BinaryClassifier.h"
#include "ML/BinaryClassifierBagged.h"
#include "ML/BinaryClassifierLogisticRegression.h"
#include "ML/BinaryClassifierNaiveBayes.h"
#include "ML/BinaryClassifierSVM.h"
#include "ML/MulticlassClassifier.h"
#include "ML/MulticlassClassifierAdaBoostM1.h"
#include "ML/MulticlassClassifierDecisionTree.h"
#include "ML/MulticlassClassifierGenerator.h"
#include "ML/MulticlassClassifierNearestNeighborBruteForce.h"
#include "ML/MulticlassClassifierOneVsAll.h"
#include "ML/MulticlassClassifierPairwiseCoupling.h"
#include "ML/RegressionLearner.h"
#include "ML/RegressionLearnerBagged.h"
#include "ML/RegressionLearnerDecisionTree.h"
#include "ML/RegressionLearnerNearestNeighbor.h"
*/

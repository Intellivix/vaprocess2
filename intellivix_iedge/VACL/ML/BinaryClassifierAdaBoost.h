#pragma once
#include "BinaryClassifier.h"

 namespace ML

{

template<class LearnerInput>
struct AdaBoostConfiguration
{
    AdaBoostConfiguration() {}
    AdaBoostConfiguration(UINT _BoostCount, UINT _BagSize, bool _UseWeightedClassifiers, double _WeightTrimRate, BinaryClassifierFactory<LearnerInput> *_Factory)
    {
        BoostCount = _BoostCount;
        BagSize = _BagSize;
        Factory = _Factory;
        UseWeightedClassifiers = _UseWeightedClassifiers;
        WeightTrimRate = _WeightTrimRate;
    }

    UINT   BoostCount, BagSize;
    bool   UseWeightedClassifiers;
    double WeightTrimRate;
    BinaryClassifierFactory<LearnerInput> *Factory;
};

#define SIG_BINARY_CLASSIFIER_ADABOOST   MAKEFOURCC('B','C','A','B')

template<class LearnerInput>
class BinaryClassifierAdaBoost : public BinaryClassifier<LearnerInput>
{
public:
	typedef ClassifierDataset<LearnerInput> Dataset;
	typedef ClassifierExample<LearnerInput> Example;

    BinaryClassifierAdaBoost()
    {
        _Configured = false;
    }

    BinaryClassifierAdaBoost(const AdaBoostConfiguration<LearnerInput> &Config)
    {
        _Configured = true;
        _Config = Config;
    }

    void Configure(const AdaBoostConfiguration<LearnerInput> &Config)
    {
        _Configured = true;
        _Config = Config;
    }

    int GetNumWCs(   )
    {
       return (_BaseClassifiers.Length());
    }

    void Train(const Dataset &Examples, UINT Class0Index, UINT Class1Index)
    {
        logd ("   # of weak classifiers added: ");

        if(_Config.BagSize > Examples.Entries().Length())
        {
          _Config.BagSize = Examples.Entries().Length();
        }
        
        _Class0Index = Class0Index;
        _Class1Index = Class1Index;
        
        Dataset NewDataset;
        _BaseClassifiers.Allocate(_Config.BoostCount);
        const UINT SampleCount = Examples.Entries().Length();

        Vector<double> SampleWeights(SampleCount);
        Vector<UINT> ClassificationError(SampleCount);

        //  SampleWeights.Clear(1.0 / double(SampleCount));
        // 샘플 수가 적은 클래스에 불리하지 않도록 초기 가중치를 설정한다.
        UINT NumPosSamples = 0;
        for (UINT i = 0; i < SampleCount; i++)
        {
            if (Examples.Entries()[i].ZeroOneClass (Class0Index) == 0) NumPosSamples++;
        }
        double InitPosWeight = 1.0 / (2.0 * NumPosSamples);
        double InitNegWeight = 1.0 / (2.0 * (SampleCount - NumPosSamples));
        for (UINT i = 0; i < SampleCount; i++)
        {
            if (Examples.Entries()[i].ZeroOneClass (Class0Index) == 0) SampleWeights[i] = InitPosWeight;
            else SampleWeights[i] = InitNegWeight;
        }

        if(_Config.UseWeightedClassifiers)
        {
            NewDataset = Examples;
        }

        double WeightTrimRate = _Config.WeightTrimRate;
        double HypothesisWeightSum = 0.0;
        bool Error = false;
        bool Done  = false;
        UINT ClassifierIndex;
        for(ClassifierIndex = 0; ClassifierIndex < _Config.BoostCount && !Done; ClassifierIndex++)
        {
            logd ("[%d]",ClassifierIndex + 1);

            AdaBoostClassifierInfo &CurInfo = _BaseClassifiers[ClassifierIndex];

            if(_Config.UseWeightedClassifiers)
            {
                for(UINT SampleIndex = 0; SampleIndex < SampleCount; SampleIndex++)
                {
                    NewDataset.Entries()[SampleIndex].Weight = SampleWeights[SampleIndex];
                }
            }
            else
            {
                NewDataset.SampleFromDataset(Examples, _Config.BagSize, SampleWeights);
            }

            CurInfo.Classifier = _Config.Factory->MakeClassifier();
            CurInfo.Classifier->TrainEx(NewDataset, SampleWeights, WeightTrimRate ,Class0Index, Class1Index);

            int    NumMisclassified = 0;
            double Epsilon = 0.0;
            for(UINT SampleIndex = 0; SampleIndex < SampleCount; SampleIndex++)
            {
                const Example &CurExample = Examples.Entries()[SampleIndex];
                UINT PredictedClass;
                double PredictedClass0Probability;
                CurInfo.Classifier->Evaluate(CurExample.Input, PredictedClass, PredictedClass0Probability);
                if(PredictedClass == CurExample.Class)
                {
                    ClassificationError[SampleIndex] = 0;
                }
                else
                {
                    ClassificationError[SampleIndex] = 1;
                    Epsilon += SampleWeights[SampleIndex];
                    NumMisclassified++;
                }
            }
            logd ("<MSC:%d><E:%.2f>",NumMisclassified,Epsilon);

            double Alpha = 0.5 * log((1.0 - Epsilon) / Epsilon);
            double CorrectFactor = exp(-Alpha);
            double IncorrectFactor = exp(Alpha);

            if(Epsilon <= 1e-20)
            {
                Done = true;
                continue;
            }
            if(Epsilon >= 0.5)
            {
               if (Error) Done = true;
               else
               {
                   delete CurInfo.Classifier;
                   Error = true;
                   WeightTrimRate = 1.0;
                   ClassifierIndex--;
               }
               continue;
            }
            if(Error) {
                Error = false;
                WeightTrimRate = _Config.WeightTrimRate;
            }

            CurInfo.Weight = Alpha;
            HypothesisWeightSum += Alpha;

            Vector<double> NewSampleWeights(SampleCount);
            for(UINT SampleIndex = 0; SampleIndex < SampleCount; SampleIndex++)
            {
                if(ClassificationError[SampleIndex] == 0)
                {
                    NewSampleWeights[SampleIndex] = SampleWeights[SampleIndex] * CorrectFactor;
                }
                else
                {
                    NewSampleWeights[SampleIndex] = SampleWeights[SampleIndex] * IncorrectFactor;
                }
            }
            double NewSampleWeightsSum = NewSampleWeights.Sum();
            for(UINT SampleIndex = 0; SampleIndex < SampleCount; SampleIndex++)
            {
                SampleWeights[SampleIndex] = NewSampleWeights[SampleIndex] / NewSampleWeightsSum;
            }
        }

        logd ("\n");

        if(HypothesisWeightSum == 0.0)
        {
            HypothesisWeightSum = 1.0;
            _BaseClassifiers[0].Weight = 1.0;
        }

        for(UINT ClassifierIndex = 0; ClassifierIndex < _Config.BoostCount; ClassifierIndex++)
        {
            _BaseClassifiers[ClassifierIndex].Weight /= HypothesisWeightSum;
        }
    }

    void Evaluate(const LearnerInput &Input, UINT &Class, double &ProbabilityClass0) const
    {
        double BoostedHypothesis = 0.0;
        for(UINT ClassifierIndex = 0; ClassifierIndex < _Config.BoostCount; ClassifierIndex++)
        {
            const AdaBoostClassifierInfo &CurInfo = _BaseClassifiers[ClassifierIndex];
            UINT LocalClass;
            double LocalProbabilityClass0;
            if(CurInfo.Classifier != NULL)
            {
                CurInfo.Classifier->Evaluate(Input, LocalClass, LocalProbabilityClass0);
                if(LocalClass == _Class0Index)
                {
                    BoostedHypothesis += CurInfo.Weight;
                }
            }
        }
        ProbabilityClass0 = BoostedHypothesis;
        if(ProbabilityClass0 >= 0.5)
        {
            Class = _Class0Index;
        }
        else
        {
            Class = _Class1Index;
        }
    }

    double Evaluate(const LearnerInput &Input) const
    {
        double BoostedHypothesis = 0.0;
        for(UINT ClassifierIndex = 0; ClassifierIndex < _Config.BoostCount; ClassifierIndex++)
        {
            const AdaBoostClassifierInfo &CurInfo = _BaseClassifiers[ClassifierIndex];
            UINT LocalClass;
            double LocalProbabilityClass0;
            if(CurInfo.Classifier != NULL)
            {
                CurInfo.Classifier->Evaluate(Input, LocalClass, LocalProbabilityClass0);
                if(LocalClass == _Class0Index)
                {
                    BoostedHypothesis += CurInfo.Weight;
                }
            }
        }
        return (BoostedHypothesis);
    }

    double Evaluate(const LearnerInput &Input, UINT WCIndex1, UINT WCIndex2) const
    {
        double BoostedHypothesis = 0.0;
        for(UINT ClassifierIndex = WCIndex1; ClassifierIndex <= WCIndex2; ClassifierIndex++)
        {
            const AdaBoostClassifierInfo &CurInfo = _BaseClassifiers[ClassifierIndex];
            UINT LocalClass;
            double LocalProbabilityClass0;
            if(CurInfo.Classifier != NULL)
            {
                CurInfo.Classifier->Evaluate(Input, LocalClass, LocalProbabilityClass0);
                if(LocalClass == _Class0Index)
                {
                    BoostedHypothesis += CurInfo.Weight;
                }
            }
        }
        return (BoostedHypothesis);
    }

    void Evaluate_SC(const LearnerInput &Input, double* SCRThresholds, double RTOffset, UINT &Class, double &ProbabilityClass0) const
    {
        double BoostedHypothesis = 0.0;
        for(UINT ClassifierIndex = 0; ClassifierIndex < _Config.BoostCount; ClassifierIndex++)
        {
            const AdaBoostClassifierInfo &CurInfo = _BaseClassifiers[ClassifierIndex];
            UINT LocalClass;
            double LocalProbabilityClass0;
            if(CurInfo.Classifier != NULL)
            {
                CurInfo.Classifier->Evaluate(Input, LocalClass, LocalProbabilityClass0);
                if(LocalClass == _Class0Index)
                {
                    BoostedHypothesis += CurInfo.Weight;
                }
            }
            if (BoostedHypothesis < (RTOffset + SCRThresholds[ClassifierIndex])) {
                ProbabilityClass0 = BoostedHypothesis;
                Class = _Class1Index;
                return;
            }
        }
        ProbabilityClass0 = BoostedHypothesis;
        Class = _Class0Index;
    }

    UINT Class0Index() const
    {
        return _Class0Index;
    }

    UINT Class1Index() const
    {
        return _Class1Index;
    }

    int LoadFromBinaryStream (InputDataStream &Stream)
    {
        UINT Signature;
        Stream >> Signature;
        if (Signature != SIG_BINARY_CLASSIFIER_ADABOOST) return (1);
        Stream >> _Class0Index;
        Stream >> _Class1Index;
        _BaseClassifiers.Allocate (_Config.BoostCount);
        for (UINT i = 0; i < _Config.BoostCount; i++)
        {
            Stream >> _BaseClassifiers[i].Weight;
            _BaseClassifiers[i].Classifier = _Config.Factory->MakeClassifier (   );
            _BaseClassifiers[i].Classifier->LoadFromBinaryStream (Stream);
        }
        return (DONE);
    }

    int SaveToBinaryStream (OutputDataStream &Stream)
    {
        Stream << (UINT)SIG_BINARY_CLASSIFIER_ADABOOST;
        Stream << _Class0Index;
        Stream << _Class1Index;
        UINT NumWeakClassifiers = _BaseClassifiers.Length (   );
        for (UINT i = 0; i < NumWeakClassifiers; i++)
        {
            Stream << _BaseClassifiers[i].Weight;
            _BaseClassifiers[i].Classifier->SaveToBinaryStream (Stream);
        }
        return (DONE);
    }

private:
    UINT _Class0Index, _Class1Index;
    
    struct AdaBoostClassifierInfo
    {
        AdaBoostClassifierInfo()
        {
            Classifier = NULL;
            Weight = 0.0;
        }
        BinaryClassifier<LearnerInput> *Classifier;
        double Weight;
    };
    Vector<AdaBoostClassifierInfo> _BaseClassifiers;

    bool _Configured;
    AdaBoostConfiguration<LearnerInput> _Config;
};

template<class LearnerInput>
class BinaryClassifierFactoryAdaBoost : public BinaryClassifierFactory<LearnerInput>
{
public:
    BinaryClassifierFactoryAdaBoost(const AdaBoostConfiguration<LearnerInput> &Config)
    {
        _Config = Config;
    }

    BinaryClassifier<LearnerInput>* MakeClassifier() const
    {
        BinaryClassifierAdaBoost<LearnerInput> *Result = new BinaryClassifierAdaBoost<LearnerInput>(_Config);
        return Result;
    }

private:
    AdaBoostConfiguration<LearnerInput> _Config;
};

}

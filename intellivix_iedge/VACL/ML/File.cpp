#include "ML/File.h"

 namespace ML

{

FILE* CheckedFOpen(const char *Filename, const char *Mode)
{
   FILE *File = fopen(Filename, Mode);
   PersistentAssert(File != NULL && !ferror(File), String("Failed to open file: ") + String(Filename));
   return File;
}

void CheckedFRead(void *Dest, UINT ElementSize, UINT ElementCount, FILE *File)
{
   size_t ElementsRead = fread(Dest, ElementSize, ElementCount, File);
   PersistentAssert(!ferror(File) && ElementsRead == ElementCount, "fread failed");
}

void CheckedFWrite(const void *Src, UINT ElementSize, UINT ElementCount, FILE *File)
{
   size_t ElementsWritten = fwrite(Src, ElementSize, ElementCount, File);
   PersistentAssert(!ferror(File) && ElementsWritten == ElementCount, "fwrite failed");
}

void CheckedFSeek(UINT Offset, FILE *File)
{
   int Result = fseek(File, Offset, SEEK_SET);
    PersistentAssert(!ferror(File) && Result == 0, "fseek failed");
}

}

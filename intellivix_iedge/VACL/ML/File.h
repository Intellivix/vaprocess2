#pragma once

#include "ML/Assert.h"

 namespace ML

{

FILE* CheckedFOpen(const char *Filename, const char *Mode);
void CheckedFRead(void *Dest, UINT ElementSize, UINT ElementCount, FILE *File);
void CheckedFWrite(const void *Src, UINT ElementSize, UINT ElementCount, FILE *File);
void CheckedFSeek(UINT Offset, FILE *File);

}

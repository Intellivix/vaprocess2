#include "ML/File.h"
#include "ML/InputDataStream.h"

 namespace ML

{

InputDataStream::InputDataStream()
{
    _data = NULL;
}

InputDataStream::~InputDataStream()
{
    
}

int InputDataStream::LoadFromFile(const String &filename)
{
    FILE *file = CheckedFOpen(filename.CString(), "rb");
    if (file == NULL) return (1);
    UINT length;
    CheckedFRead(&length, sizeof(UINT), 1, file);
    _storage.Allocate(length);
    CheckedFRead(_storage.CArray(), sizeof(BYTE), length, file);
    fclose(file);
    
    _data = _storage.CArray();
    _dataLength = _storage.Length();
    _readPtr = 0;

    return (DONE);
}

void InputDataStream::WrapMemory(const Vector<BYTE> &stream)
{
    _data = stream.CArray();
    _dataLength = stream.Length();
    _readPtr = 0;
}

void InputDataStream::ReadData(BYTE *result, UINT bytesToRead)
{
    Assert(_dataLength >= _readPtr + bytesToRead, "Read past end of stream");
    if(bytesToRead > 0)
    {
        memcpy(result, _data + _readPtr, bytesToRead);
        _readPtr += bytesToRead;
    }
}

InputDataStream& operator >> (InputDataStream &S, String &V)
{
    UINT Length;
    S >> Length;
    V.AllocateLength(Length);
    S.ReadData((BYTE *)V.CString(), Length);
    return S;
}

}

#include "vacl_misc.h"
#include "vacl_event.h"
#include "License/KeyDecryption.h"

#if defined(__OS_WINDOWS)
#include <Iphlpapi.h>
#pragma comment(lib,"iphlpapi.lib")
#endif
 
 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Hidden Global Variables
//
///////////////////////////////////////////////////////////////////////////////

extern EventZoneListManager *__EZLM;
extern EventRuleListManager *__ERLM;

int     __Flag_VACL_Initialized = FALSE;
StringA __LicenseKey  = "";
StringA __SerialNo    = "";
StringA __ProductKey  = "VAEINTVX";
StringA __ProductCode = "IVX";
StringA __ProductVer  = "10";

///////////////////////////////////////////////////////////////////////////////
//
// Hidden Functions
//
///////////////////////////////////////////////////////////////////////////////

 void __RegisterLicenseKey (const char* license_key,const char* serial_no)

{
   __LicenseKey  = license_key;
   __SerialNo    = serial_no;
   #if defined(__SDK_FOR_IDIS)
   __ProductKey  = "VAEIDIS1";
   __ProductCode = "IDS";
   __ProductVer  = "10";
   #endif
}

 int __GetMACAddress (StringA& mac_addr)
#if defined(__OS_WINDOWS)
{
   ULONG buf_size = sizeof(IP_ADAPTER_INFO);
   BArray1D adapter_info(buf_size);
   if (GetAdaptersInfo ((IP_ADAPTER_INFO*)(byte*)adapter_info,&buf_size) == ERROR_BUFFER_OVERFLOW) adapter_info.Create (buf_size);
   if (GetAdaptersInfo ((IP_ADAPTER_INFO*)(byte*)adapter_info,&buf_size) == ERROR_SUCCESS) {
      BYTE* addr = ((IP_ADAPTER_INFO*)(byte*)adapter_info)->Address;
      mac_addr.Format ("%02X-%02X-%02X-%02X-%02X-%02X",addr[0],addr[1],addr[2],addr[3],addr[4],addr[5]);
      return (DONE);
   }
   return (1);
}
#elif defined(__linux)
{
   mac_addr.Delete (   );

   int sock = socket (AF_INET,SOCK_DGRAM,IPPROTO_IP);
   if (sock == -1) return (1);

   /*char buf[1024];
   struct ifconf ifc;
   memset (&ifc,0,sizeof(ifc));
   ifc.ifc_len = sizeof(buf);
   ifc.ifc_buf = buf;
   if (ioctl (sock,SIOCGIFCONF,&ifc) == -1) {
      close (sock);
      return (2);
   }

   struct ifreq* it = ifc.ifc_req;
   const struct ifreq* const end = it + (ifc.ifc_len / sizeof(struct ifreq));

   struct ifreq ifr;
   memset (&ifr,0,sizeof(ifr));
   int flag_success = FALSE;
   for (   ; it != end; ++it) {
      strcpy (ifr.ifr_name,it->ifr_name);
      if (ioctl (sock,SIOCGIFFLAGS,&ifr) == 0) {
         if (!(ifr.ifr_flags & IFF_LOOPBACK)) {
            if (ioctl (sock,SIOCGIFHWADDR,&ifr) == 0) {
               flag_success = TRUE;
               break;
            }
         }
      }
      else {
         close (sock);
         return (3);
      }
   }
   close (sock);

   if (flag_success) {
      mac_addr.Format ("%02X-%02X-%02X-%02X-%02X-%02X",
         (int)(ifr.ifr_hwaddr.sa_data[0] & 0xFF),
         (int)(ifr.ifr_hwaddr.sa_data[1] & 0xFF),
         (int)(ifr.ifr_hwaddr.sa_data[2] & 0xFF),
         (int)(ifr.ifr_hwaddr.sa_data[3] & 0xFF),
         (int)(ifr.ifr_hwaddr.sa_data[4] & 0xFF),
         (int)(ifr.ifr_hwaddr.sa_data[5] & 0xFF));
      return (DONE);
   }
   else return (4);
   */
   return (DONE);
}
#else
{
   return (-1);
}
#endif

#if defined(__LICENSE_MAC)

 int __CheckLicenseKey (   )

{
   StringA mac_addr;
   int e_code = __GetMACAddress (mac_addr);
   if (e_code) {
      logd ("[ERROR] Cannot read the MAC address. (Error Code: %d)\n",e_code);
      return (1);
   }
   int fc = 0;
   int n_channels = CertifyKey (__LicenseKey,__SerialNo,mac_addr,__ProductKey,__ProductCode,__ProductVer,fc);
   if (n_channels == 1 && fc == FCLASS_VIDEOANALYTICS) return (DONE);
   else return (2);
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

void (*__ClockFunction)(int *year,int *month,int *day,int *weekday,int *hour,int *minute,int *second) = NULL;
int  (*__TimerFunction)(   ) = NULL;

 void CloseVACL (   )

{
   delete __EZLM;
   __EZLM = NULL;
   delete __ERLM;
   __ERLM = NULL;
   __Flag_VACL_Initialized = FALSE;
}

 int InitVACL (   )

{
   #if defined(__LICENSE_MAC)
   if (__CheckLicenseKey (   )) return (-1);
   #endif
   if (__Flag_VACL_Initialized) return (DONE);
   __EZLM = new EventZoneListManager;
   __ERLM = new EventRuleListManager;
   __Flag_VACL_Initialized = TRUE;
   return (DONE);
}

 int GetLicenseKeyAndSerialNumber (const char* file_name,StringA& license_key,StringA& serial_number)

{
   // NOTE(yhpark): removed. rapidxml. need to test.
   /*
   BArray1D s_buffer;
   if (ReadFileIntoBuffer (file_name,s_buffer,TRUE)) return (1);
   RapidXML_Doc xml_doc;
   xml_doc.parse<0>((char*)(byte*)s_buffer);
   RapidXML_Node* node_VAEngineLicense = xml_doc.first_node ("VAEngineLicense");
   if (node_VAEngineLicense == NULL) return (2);
   RapidXML_Node* node_LicenseKey = node_VAEngineLicense->first_node ("LicenseKey");
   if (node_LicenseKey == NULL) return (3);
   license_key = node_LicenseKey->value (   );
   RapidXML_Node* node_SerialNumber = node_VAEngineLicense->first_node ("SerialNumber");
   if (node_SerialNumber == NULL) return (4);
   serial_number = node_SerialNumber->value (   );
   */
   
   CXMLIO xmlio( file_name, XMLIO_Read );
   CXMLNode node_root( &xmlio );
   if( node_root.Start( "VAEngineLicense" ) ) {
      // TODO(yhpark)
   } else {
      return ( 1 );
   }
   
   return (DONE);
}

 void SetProductInfo (const char* key,const char* code,const char* version)

{
   __ProductKey  = key;
   __ProductCode = code;
   __ProductVer  = version;
}

}

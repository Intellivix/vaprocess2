#if !defined(__VACL_CONRGN_H)
#define __VACL_CONRGN_H

#include "vacl_define.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Definitions
//
///////////////////////////////////////////////////////////////////////////////

#define CNB_FOUR    0
#define CNB_EIGHT   1

///////////////////////////////////////////////////////////////////////////////
//
// Class: BoundaryPixelTracking
//
///////////////////////////////////////////////////////////////////////////////
 
 class BoundaryPixelTracking
 
{
   protected:
      int       X,Y;
      int       SX,SY;
      int       Margin;
      int       Direction;
      IArray2D* CRMap;
      IArray2D  CRBMap;
      
   public:
      int NumBoundaryPixels;
      int NumMarginalPixels;

   public:
      BoundaryPixelTracking (   );
      virtual ~BoundaryPixelTracking (   );

   protected:
      void _Init (   );
   
   public:
      virtual void EndTracking          (   ) {   };
      virtual int  StartTracking        (   ) { return (DONE); };
      virtual void ProcessBoundaryPixel (   ) {   };
      virtual void ProcessMarginalPixel (   ) {   };

   public:
      virtual void Close (   );
      
   public:
      // 주의: cr_map의 가장자리 픽셀 값이 0이어야 한다.
      int Initialize (IArray2D& cr_map,int margin = 3);
      int Perform    (int sx,int sy);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: ConnectedRegion
//
///////////////////////////////////////////////////////////////////////////////

 class ConnectedRegion : public IBox2D
 
{
   public:
      int   Area;           // Area
      int   Correspondence; // Correspondence
      int   SX,SY;          // Start Position
      float CX,CY;          // Centroid
      
   public:
      ConnectedRegion (   );
   
   public:
      void Clear (   );
};

typedef Array1D<ConnectedRegion> CRArray1D;

///////////////////////////////////////////////////////////////////////////////
//
// Struct: ETElement
//
///////////////////////////////////////////////////////////////////////////////
//
// ETElement: Equivalence Table Element
//

 struct ETElement

{
   int MinLabelNo;
   int Next;
};

void  InitEquivalenceTable   (ETElement* eq_table,int length);
void  UpdateEquivalenceTable (ETElement* eq_table,int a,int b);

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

// s_cr_map의 CR_i의 레이블 값을 cv_table[i]의 값으로 변경하여 d_cr_map에 저장한다.
void  ConvertCRMap        (IArray2D& s_cr_map,IArray1D &cv_table,IArray2D &d_cr_map);
void  CopyCR              (IArray2D& s_cr_map,IBox2D& s_rgn,int s_cr_no,IArray2D& d_cr_map,int d_cr_no);
void  CopyCR              (IArray2D& s_cr_map,IBox2D& s_rgn,int s_cr_no,GImage& d_image,byte d_pv);
void  CopyCR              (IArray2D& s_cr_map,IBox2D& s_rgn,int s_cr_no,IArray2D& d_cr_map,IPoint2D& d_pos,int d_cr_no);
void  CopyCR              (IArray2D& s_cr_map,IBox2D& s_rgn,int s_cr_no,GImage& d_image,IPoint2D& d_pos,byte d_pv);
void  FindMaxCRArea       (CRArray1D& cr_array,int& d_cr_no,int& d_cr_area);
int   FindMaxOverlappedCR (IArray2D& cr_map,IBox2D& s_rgn,int n_crs,int& d_ov_area);
int   FindMaxOverlappedCR (IArray2D& s_cr_map,IBox2D& s_rgn,int s_cr_no,IArray2D& t_cr_map,int n_tcrs,int& d_ov_area);
// s_rgn 내에서 cr_no의 레이블 값을 갖는 픽셀들의 수를 리턴한다.
int   GetCRArea           (IArray2D& cr_map,IBox2D& s_rgn,int cr_no);
float GetCRDensity        (IArray2D& cr_map,IBox2D& s_rgn,int cr_no);
// s_rgn 내의 CR의 ID(레이블 번호)를 찾아 리턴한다.
int   GetCRID             (IArray2D& cr_map,IBox2D& s_rgn);
void  GetCRInfo           (IArray2D& cr_map,CRArray1D &d_array);
void  GetCRMap            (GImage& s_image,int min_area,int max_area,IArray2D& cr_map,CRArray1D& cr_array,int option = CNB_FOUR);
float GetCRMean           (GImage& s_image,IArray2D& cr_map,IBox2D& s_rgn,int cr_no,int& d_num,int64& d_sum);
int   GetCROverlapArea    (IArray2D& cr_map,IBox2D& s_rgn,int cr_no,GImage& b_image);
int   GetCRPerimeter      (IArray2D& cr_map,IBox2D& s_rgn,int cr_no);
void  GetCRPrincipalAxes  (IArray2D& cr_map,IBox2D& s_rgn,int cr_no,FPoint2D& mj_axis,FPoint2D& mn_axis,float& mja_length,float& mna_length);
int   LabelCRs            (GImage& s_image,IArray2D& d_array,int option = CNB_FOUR);
void  PutCR               (IArray2D& cr_map,IBox2D& s_rgn,int cr_no,int flag_overwrite = FALSE);
void  RemoveCR            (GImage& s_image,IArray2D& cr_map,IBox2D& s_rgn,int cr_no);
// mk_array[i] == TRUE인 CR_i들을 모두 제거하고, 남은 CR들의 레이블 값을 재배정한다. (cr_map이 변경됨)
// 남은 CR들의 개수를 리턴한다.
int   RemoveCRs           (IArray2D& cr_map,BArray1D& mk_array);
// mk_array[i] == TRUE인 CR_i들이 모두 제거된 이진 영상을 d_image에 저장한다. (cr_map은 변경되지 않음)
void  RemoveCRs           (IArray2D& cr_map,BArray1D& mk_array,GImage& d_image);
// 면적이 min_area와 max_area 사이에 해당하는 CR들을 모두 제거한 이진 영상을 d_image에 저장한다.
void  RemoveCRs           (GImage& s_image,int min_area,int max_area,GImage& d_image);
// s_rgn 내에서 cr_no의 레이블 값을 갖는 CR에 대해 수평/수직 투영 알고리즘을 통해 트리밍(trimming)을 수행한다.
int   TrimCR              (IArray2D& cr_map,IBox2D& s_rgn,int cr_no,float h_thrsld,float v_thrsld,IBox2D& d_rgn);

}

#endif

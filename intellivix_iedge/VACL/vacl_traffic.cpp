#include "vacl_event.h"
#include "vacl_traffic.h"

 namespace VACL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Class: TrafficZone
//
///////////////////////////////////////////////////////////////////////////////

#define TZ_PARAM_BONUS_TIME   10.0f

 TrafficZone::TrafficZone (   )
 
{
   Flag_Processed = FALSE;   
   AvgRealSpeed   = 0.0f;
   CongestionTime = 0.0f;
   NumVehicles    = 0.0f;
   EvtZone        = NULL;
}

 int TrafficZone::DetectEvent (   )
 
{
   if (!Flag_Processed) return (0);
   if (EvtZone == NULL) return (0);
   EventRule* evt_rule = EvtZone->EvtRule;
   if (evt_rule == NULL) return (0);
   EventRuleConfig_TrafficCongestion& erc = evt_rule->ERC_TrafficCongestion;
   EventDetection* evt_detector = evt_rule->EventDetector;
   if (evt_detector == NULL) return (0);
   int   n_objects      = 0;
   int   flag_detection = FALSE;
   float max_time       = erc.DetectionTime + TZ_PARAM_BONUS_TIME;
   if (NumVehicles >= 1.0f && AvgRealSpeed <= erc.MaxAvgRealSpeed) {
      CongestionTime += evt_detector->FrameDuration;
      if (CongestionTime >= erc.DetectionTime) {
         if (CongestionTime > max_time) CongestionTime = max_time;
         flag_detection = TRUE;
      }
   }
   else {
      CongestionTime -= evt_detector->FrameDuration;
      if (CongestionTime < 0.0f) CongestionTime = 0.0f;
   }
   Event*         m_event   = NULL;
   TrackedObject* eg_object = NULL;
   TrackedObject* t_object  = evt_detector->EvtGenObjects.First (   );
   while (t_object != NULL) {
      Event* event = t_object->FindEvent (EvtZone);
      if (event != NULL) {
         m_event   = event;
         eg_object = t_object;
         break;
      }
      t_object = evt_detector->EvtGenObjects.Next (t_object);
   }
   if (m_event == NULL) {
      if (flag_detection) {
         eg_object         = CreateInstance_TrackedObject (   );
         eg_object->ID     = evt_detector->GetNewObjectID (   );
         eg_object->Status = TO_STATUS_VALIDATED | TO_STATUS_VERIFIED;
         eg_object->Type   = TO_TYPE_VEHICLE;
         m_event = CreateInstance_Event  (EvtZone,evt_rule);
         m_event->StartTime.GetLocalTime (   );
         eg_object->Events.Add (m_event);
         evt_detector->EvtGenObjects.Add (eg_object);
         n_objects = 1;
      }
   }
   else if (m_event->EvtRule == evt_rule) {
      m_event->ElapsedTime += evt_detector->FrameDuration;
      if (flag_detection) {
         switch (m_event->Status) {
            case EV_STATUS_BEGUN:
               m_event->Status = EV_STATUS_ONGOING;
               break;
            case EV_STATUS_ONGOING:
               if (m_event->ElapsedTime > evt_rule->AlarmPeriod) {
                  m_event->EndTime.GetLocalTime (   );
                  m_event->Status = EV_STATUS_ENDED;
               }
               break;
            case EV_STATUS_ENDED:
               m_event->Status = EV_STATUS_AFTER_ENDED;
               break;
            default:
               break;
         }
      }
      else if (CongestionTime < erc.DetectionTime) {
         if (m_event->Status == EV_STATUS_AFTER_ENDED) m_event->Status = EV_STATUS_TO_BE_REMOVED;
         else {
            m_event->Status = EV_STATUS_ENDED | EV_STATUS_TO_BE_REMOVED;
            m_event->EndTime.GetLocalTime (   );
         }
         eg_object->Status |= TO_STATUS_TO_BE_REMOVED;
      }
   }
   if (flag_detection) eg_object->SetRegion (VehicleRegion);
   evt_rule->ERC_Counting.Count += n_objects;
   return (n_objects);
}

 void TrafficZone::GetTrafficState (ObjectTracking& obj_tracker,float learn_rate)
 
{
   int i;
   
   Flag_Processed = FALSE;
   if (!EvtZone->IsEnabled (   )) return;
   // 차량 객체들을 저장할 배열을 생성한다.
   TrackedObjectList& to_list = obj_tracker.TrackedObjects;
   Array1D<TrackedObject*> vehicles;
   int n_objects = to_list.GetNumItems (   );
   if (n_objects) vehicles.Create (n_objects);
   // 이벤트 존 내에 있는 차량들의 평균 속력 avg_speed를 구한다.
   int   n_vehicles = 0;
   float avg_speed  = 0.0f;
   EventRule* evt_rule = EvtZone->EvtRule;
   TrackedObject* t_object = to_list.First (   );
   while (t_object != NULL) {
      if ((t_object->Status & TO_STATUS_VERIFIED) && (t_object->Type == TO_TYPE_VEHICLE) && (t_object->RealHeight > 0.0f)) {
         if (EvtZone->CheckRegionInsideZone (*t_object) == DONE && !evt_rule->ObjectFilter->Filter (t_object)) {
            avg_speed += t_object->RealSpeed;
            vehicles[n_vehicles] = t_object;
            n_vehicles++;
         }
      }
      t_object = to_list.Next (t_object);
   }
   if (n_vehicles) {
      avg_speed /= n_vehicles;
      IPoint2D min_p( 100000, 100000);
      IPoint2D max_p(-100000,-100000);
      for (i = 0; i < n_vehicles; i++) {
         int sx = vehicles[i]->X;
         int sy = vehicles[i]->Y;
         int ex = sx + vehicles[i]->Width;
         int ey = sy + vehicles[i]->Height;
         if (sx < min_p.X) min_p.X = sx;
         if (sy < min_p.Y) min_p.Y = sy;
         if (ex > max_p.X) max_p.X = ex;
         if (ey > max_p.Y) max_p.Y = ey;
      }
      VehicleRegion.X      = min_p.X - 5;
      VehicleRegion.Y      = min_p.Y - 5;
      VehicleRegion.Width  = max_p.X - min_p.X + 10;
      VehicleRegion.Height = max_p.Y - min_p.Y + 10;
      CropRegion (obj_tracker.SrcImgSize,VehicleRegion);
   }
   else VehicleRegion.Clear (   );
   float b = 1.0f - learn_rate;
   AvgRealSpeed = b * AvgRealSpeed + learn_rate * avg_speed;
   NumVehicles  = b * NumVehicles  + learn_rate * n_vehicles;
   #if defined(__DEBUG_TCD)
   logd ("[%s] AvgRealSpeed: %5.1f km/h, NumVehicles: %4.1f, CongestionTime: %5.2f seconds\n",EvtZone->Name,AvgRealSpeed,NumVehicles,CongestionTime);
   #endif
   Flag_Processed = TRUE;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: TrafficCongestionDetection
//
///////////////////////////////////////////////////////////////////////////////

 TrafficCongestionDetection::TrafficCongestionDetection (   )
 
{
   Flag_Enabled = TRUE;
   LearningRate = 0.05f;
   FrameRate    = 0.0f;
}

 TrafficCongestionDetection::~TrafficCongestionDetection (   )
 
{
   Close (   );
}

 void TrafficCongestionDetection::Close (   )
 
{
   TrafficZones.Delete (   );
}

 int TrafficCongestionDetection::Initialize (EventDetection& evt_detector)
 
{
   Close (   );
   if (!IsEnabled (   )) return (1);
   SrcImgSize = evt_detector.SrcImgSize;
   FrameRate  = evt_detector.FrameRate;
   EventZoneList& ez_list = evt_detector.EventZones;
   // 이벤트 종류가 "교통 정체"인 이벤트 존들을 찾아 등록한다.
   EventZone* evt_zone = ez_list.First (   );
   while (evt_zone != NULL) {
      if (evt_zone->EvtRule->EventType == ER_EVENT_TRAFFIC_CONGESTION) {
         TrafficZone* zone = new TrafficZone;
         zone->EvtZone = evt_zone;
         TrafficZones.Add (zone);
      }
      evt_zone = ez_list.Next (evt_zone);
   }
   if (!TrafficZones.GetNumItems (   )) return (2);
   return (DONE);
}

 int TrafficCongestionDetection::Perform (ObjectTracking &obj_tracker)
 
{
   if (!IsEnabled (   )) return (1);
   if (!TrafficZones.GetNumItems (   )) return (2);
   TrafficZone* zone = TrafficZones.First (   );
   while (zone != NULL) {
      zone->GetTrafficState (obj_tracker,LearningRate);
      zone = TrafficZones.Next (zone);
   }
   return (DONE);
}

}

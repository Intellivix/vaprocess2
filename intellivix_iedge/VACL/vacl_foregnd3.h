#if !defined(__VACL_FOREGND4_H)
#define __VACL_FOREGND4_H

#include <vector>
#include "vacl_foregnd.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: ForegroundDetection_HMD_OHV_MBS
//
///////////////////////////////////////////////////////////////////////////////
//
// HMD: Human Detection
// OHV: Overhead View
// MBS: Model-Based Segmentation
//

 class ForegroundDetection_HMD_OHV_MBS : public ForegroundDetection_SGBM

{
   public:
      float   HeadRadius;
      ISize2D RefImgSize;
   
   public:
      ForegroundDetection_HMD_OHV_MBS (   );

   protected:
      virtual int  GetFrgImage     (   );
      virtual void GetFrgRegionMap (   ) {   };

   protected:
      int  GetHumanCandidateRegions     (IArray2D& fr_map,IP2DArray1D& hp_array,IB2DArray1D& d_array);
      void GetHumanRegionLikelihoodMap  (GImage& fg_image,GImage& fe_image,GImage& d_image);
      void GetIntegrals                 (GImage& s_image,FArray2D& d_array,FArray2D& dx_array,FArray2D& dy_array);
      void GetRegionScores              (IB2DArray1D& hr_array,FArray2D& it_array,FArray1D& d_array);
      void PerformMeanShift             (FArray2D& it_array,FArray2D& ix_array,FArray2D& iy_array,IBox2D& rgn);
      void PerformNonmaximumSuppression (IB2DArray1D& s_array,FArray2D& it_array,IB2DArray1D& d_array);
      void SetFrgRegions                (GImage& fg_image,IArray2D& fr_map,IB2DArray1D& hr_array);

   public:
      float GetBodySize   (   );
      float GetHeadRadius (   );
   
   public:
      void GetHeadRadius (float& hd_radius,ISize2D& ri_size);
      void SetHeadRadius (float  hd_radius,ISize2D& ri_size);

   public:
      int ReadFileEx  (FileIO& file);
      int ReadFileEx  (CXMLIO* pIO);
      int WriteFileEx (FileIO& file);
      int WriteFileEx (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: ForegroundDetection_HMD_OHV_3DM
//
///////////////////////////////////////////////////////////////////////////////
//
// HMD: Human Detection
// OHV: Overhead View
// 3DM: 3D Depth Map
//

 class ForegroundDetection_HMD_OHV_3DM : public ForegroundDetection

{
   protected:
     struct Cell {
        bool is_valid;
        byte max_val;
        int  max_pos_x;
        int  max_pos_y;
     };
     GImage InvDepthMap;

   protected:
     int   Margin;
     float GFSize;
     float FXD,FYD;
     float FXO,FYO;
     float RealHeadSizeH;
     float RealHeadSizeV;

   protected:
     int  CameraHeight;    // 카메라의 설치 높이(mm)
     int  HeadSize;        // Depth Image 상에서 사람의 머리 크기
     int  MinObjHeight;    // 유효 물체 높이의 하한(mm)
     int  MaxObjHeight;    // 유효 물체 높이의 상한(mm)
     byte DepthLowerLimit; // 유효 뎁스 범위의 하한(mm)
     byte DepthUpperLimit; // 유효 뎁스 범위의 상한(mm)
   
   public:
      ForegroundDetection_HMD_OHV_3DM (   );

   protected:
      virtual int  GetFrgImage        (   );
      virtual void GetFrgRegionMap    (   ) {   };
      virtual void GetPreprocSrcImage (GImage& s_image);

   protected:
      void FindHeadCandidates            (GImage& s_image,int cell_size,int head_size,std::vector<Cell>& head_candidates);
      void FindLocalMaximum              (GImage& s_image,int sx,int sy,int cell_size,Cell& cell);
      void GetFixedHeadSize              (int cam_height);
      void GetHeadCentroid               (GImage& s_image,int s_cx,int s_cy,int diameter,int& d_cx,int& d_cy);
      void GetHorizontalIntegral         (GImage &s_image,int x1,int x2,int y,int& cx,int& cy,int& area);
      void GetPreprocDepthMap            (GImage& s_image,GImage& d_image);
      void SelectMaximumInNeighborPixels (Array2D<Cell>& table,GImage& s_image,int curr_x,int curr_y,int cell_size,int head_size,Cell& cell);
      bool SeparatorBasedBresenham       (GImage& s_image,int x1,int y1,int x2,int y2);
 
   public:
      virtual void Close      (   );
      virtual int  Initialize (ISize2D& si_size,float frm_rate);
   
   public:
      // 기 설정되어 있는 카메라 설치 높이 값과 검출할 객체의 높이의 유효 범위 값을 얻는다. (단위: mm)
      void GetParams (int& cam_height,int& min_obj_height,int& max_obj_height);
      // 카메라 설치 높이 값과 검출할 객체의 높이(즉 사람의 경우 키를 의미함)의 유효 범위 값을 설정한다. (단위: mm)
      // 유효하지 않은 파라미터 값을 설정하면 0이 아닌 값을 리턴한다.
      int  SetParams (int cam_height,int min_obj_height,int max_obj_height);

   public:
      int ReadFileEx  (FileIO& file);
      int ReadFileEx  (CXMLIO* pIO);
      int WriteFileEx (FileIO& file);
      int WriteFileEx (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

// 두 필드로 나뉘어져 있는 Depth Image(640x320)로부터 단일 필드의 Depth Image(320x240)를 생성한다.
int ConvertDepthImage_HDPro (GImage& s_image,GImage& d_image);
// Depth Image 상의 픽셀 좌표를 Optical Image 상의 픽셀 좌표로 변환한다.
// xd,yd    [IN] : Depth Image 상의 픽셀 좌표
// oi_width [IN] : Optical Image의 너비
// xo,yo    [OUT]: (xd,yd)에 대응하는 Optical Image 상의 픽셀 좌표
void ConvertDepthToOpticalImageCoord_HDPro (int xd,int yd,int oi_width,int& xo,int& yo);

}

#endif

#include "vacl_foregnd4.h"
#include "vacl_ipp.h"
#include "vacl_objdetec.h"
#include "vacl_warping.h"

#if defined(__DEBUG_FGD)
#include "Win32CL/Win32CL.h"
extern int _DVX_FGD,_DVY_FGD;
extern CImageView* _DebugView_FGD;
#endif

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: ForegroundDetection_FCD_Seeta
//
///////////////////////////////////////////////////////////////////////////////

// [주의] Close ( ) 함수를 만들어서 그 안에서 FaceDetector.Close() 함수를 호출하면 안된다!
//        ForegroundDetection::Initialize() 함수 내에서 Close() 함수를 호출하기 때문에
//        ForegroundDetection::Initialize() 호출 시 FaceDetector의 메모리가 해제되어 버린다!

#if defined(__LIB_SEETA)

 ForegroundDetection_FCD_Seeta::ForegroundDetection_FCD_Seeta (   )

{
   ClassID = CLSID_ForegroundDetection_FCD_Seeta;
}

 int ForegroundDetection_FCD_Seeta::GetFrgImage (   )

{
   IB2DArray1D d_rects;
   FaceDetector.Perform (SrcImage,d_rects);
   #if defined(__DEBUG_FGD)
   _DebugView_FGD->DrawImage (SrcImage,_DVX_FGD,_DVY_FGD);
   for (int i = 0; i < d_rects.Length; i++) {
      IBox2D& rect = d_rects[i];
      _DebugView_FGD->DrawRectangle (rect.X + _DVX_FGD,rect.Y + _DVY_FGD,rect.Width,rect.Height,PC_GREEN,2);
   }
   _DVY_FGD += SrcImgSize.Height;
   _DebugView_FGD->DrawText (_T("Face Detection Result"),_DVX_FGD,_DVY_FGD,PC_GREEN);
   _DVY_FGD += 20;
   #endif
   SetFrgRegions (d_rects);
   return (FGD_RCODE_NORMAL);
}

 int ForegroundDetection_FCD_Seeta::InitFaceDetector (const char* dir_name)

{
   return (FaceDetector.Initialize (dir_name));
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: ForegroundDetection_FCD_OpenVINO
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_OPENVINO)

 ForegroundDetection_FCD_OpenVINO::ForegroundDetection_FCD_OpenVINO (   )

{
   ClassID = CLSID_ForegroundDetection_FCD_OpenVINO;
}

 int ForegroundDetection_FCD_OpenVINO::GetFrgImage (   )

{
   int i;

   if (SrcColorImage == NULL) return (FGD_RCODE_ERROR);
   IB2DArray1D d_rects;
   if (FaceDetector.Perform (*SrcColorImage,d_rects)) return (FGD_RCODE_ERROR);
   if (!d_rects) ObjBndBoxes.Delete (   );
   else {
      ObjBndBoxes.Create (d_rects.Length);
      for (i = 0; i < d_rects.Length; i++) {
         ObjBndBox& obb = ObjBndBoxes[i];
         obb.ClassIdx  = 0;
         obb.ObjType   = OBJ_TYPE_FACE;
         obb.ClassName = "Face";
         obb.Set (d_rects[i]);
         // [주의] 본 객체가 "얼굴" 객체라 할지라도 obb.SubRgn_Face에는 영역 정보를 기록하면 안된다. (Why?)
      }
   }
   DrawObjBndBoxes (   );
   return (FGD_RCODE_NORMAL);
}

 int ForegroundDetection_FCD_OpenVINO::InitFaceDetector (const char* dir_name,int flag_advanced,int flag_use_gpu)

{
   FaceDetector.Flag_Advanced = flag_advanced;
   FaceDetector.Flag_UseGPU   = flag_use_gpu;
   return (FaceDetector.Initialize (dir_name));
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: ForegroundDetection_OBD_YOLO
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_DARKNET)

 ForegroundDetection_OBD_YOLO::ForegroundDetection_OBD_YOLO (   )

{
   ClassID = CLSID_ForegroundDetection_OBD_YOLO;
}

 int ForegroundDetection_OBD_YOLO::GetFrgImage (   )

{
   if (SrcColorImage == NULL) return (FGD_RCODE_ERROR);
   if (ObjectDetector.Perform (*SrcColorImage,ObjBndBoxes)) return (FGD_RCODE_ERROR);
   DrawObjBndBoxes (   );
   return (FGD_RCODE_NORMAL);
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: ForegroundDetectionClient_OBD_YOLO
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_DARKNET)

 ForegroundDetectionClient_OBD_YOLO::ForegroundDetectionClient_OBD_YOLO (   )

{
   ClassID = CLSID_ForegroundDetectionClient_OBD_YOLO;      
}

 int ForegroundDetectionClient_OBD_YOLO::GetFrgImage (   )

{
   if (Server == NULL) return (FGD_RCODE_ERROR);
   if (!Server->IsRunning (    )) return (FGD_RCODE_ERROR);
   ISize2D si_size = Server->GetSrcImageSize (   );
   if (!si_size.Width || !si_size.Height) return (FGD_RCODE_ERROR);
   if (SrcColorImage == NULL && SrcFrmBuffer == NULL) return (FGD_RCODE_ERROR);
   if (!DNNInputImage) DNNInputImage.Create (si_size.Width,si_size.Height);
   if (SrcColorImage != NULL) Resize_2 (*SrcColorImage,DNNInputImage);
   else {
      BArray1D si_buffer(si_size.Width * si_size.Height * 2);
      Resize_YUY2_2 (SrcFrmBuffer,SrcFrmSize,si_buffer,si_size);
      ConvertYUY2ToBGR_2 (si_buffer,DNNInputImage);
   }
   if (Server->AddRequest (this)) return (FGD_RCODE_ERROR);
   WaitForSingleEvent (Event_Processed);
   DrawObjBndBoxes (   );
   return (FGD_RCODE_NORMAL);
}

#endif

}

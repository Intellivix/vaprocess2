#if !defined(__KEY_DECRYPTION_H)
#define __KEY_DECRYPTION_H

#include "License/BFCrypt.h"

#define FCLASS_DISABLED        0
#define FCLASS_MONITORING      1
#define FCLASS_VIDEOANALYTICS  2

///////////////////////////////////////////////////////////////////////////////
//
// Class: CBFDecrypt
//
///////////////////////////////////////////////////////////////////////////////

 class CBFDecrypt : public CBFCrypt

{
   public:
      CBFDecrypt(unsigned char* ucKey, int n, const SBlock& roChain = SBlock(0UL,0UL));
      // Decrypt Buffer in Place
      void Decrypt(unsigned char* buf, int n, int iMode=ECB);
      // Decrypt from Input Buffer to Output Buffer
      void Decrypt(const unsigned char* in, unsigned char* out, int n, int iMode=ECB);
};

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

int DecodeKey  (const char* license_key,unsigned char* auth_key,int& fc);
int CertifyKey (const char* license_key,const char* serial_no,const char* mac_addr,const char* prod_key,const char* prod_code,const char* version,int& fc);

#endif

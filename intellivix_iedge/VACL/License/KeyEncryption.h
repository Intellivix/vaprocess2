#if !defined(__KEY_ENCRYPTION_H)
#define __KEY_ENCRYPTION_H

#include "License/BFCrypt.h"

///////////////////////////////////////////////////////////////////////////////
//
// Class: CBFEncrypt
//
///////////////////////////////////////////////////////////////////////////////

 class CBFEncrypt : public CBFCrypt

{
   public:
      CBFEncrypt(unsigned char* ucKey, int n, const SBlock& roChain = SBlock(0UL,0UL));
      void Encrypt(unsigned char* buf, int n, int iMode=ECB);
      void Encrypt(const unsigned char* in, unsigned char* out, int n, int iMode=ECB);
};

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

int EncodeKey     (char* license_key,byte* auth_key,byte ch_num);
int EncodeKey_V2  (char* license_key,byte* auth_key,byte ch_num,byte fc);
int EncryptKey    (unsigned char* auth_key,const char* serial_no,const char* mac_addr,const char* prod_key,const char* prod_code,const char*version);

#endif

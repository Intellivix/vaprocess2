#include "bccl.h"
#include "License/KeyDecryption.h"
#include "License/KeyEncryption.h"

extern byte EncryptedSecret[256];

///////////////////////////////////////////////////////////////////////////////
//
// Class: CBFDecrypt
//
///////////////////////////////////////////////////////////////////////////////

CBFDecrypt::CBFDecrypt(unsigned char* ucKey, int keysize, const SBlock& roChain) 
: CBFCrypt(ucKey, keysize, roChain)
{
}

void CBFDecrypt::Decrypt(unsigned char* buf, int n, int iMode)
{
   //Check the buffer's length - should be > 0 and multiple of 8
   if((n==0)||(n%8!=0)) logd ("CBFDecrypt::Decrypt(): Incorrect buffer length!\n");
   SBlock work;
   if(iMode == CBC) //CBC mode, using the Chain
   {
      SBlock crypt, chain(m_oChain);
      for(; n >= 8; n -= 8)
      {
         _BytesToBlock(buf, work);
         crypt = work;
         CBFCrypt::Decrypt(work);
         work ^= chain;
         chain = crypt;
         BlockTo_Bytes(work, buf+=8);
      }
   }
   else if(iMode == CFB) //CFB mode, using the Chain, not using Decrypt()
   {
      SBlock crypt, chain(m_oChain);
      for(; n >= 8; n -= 8)
      {
         _BytesToBlock(buf, work);
         Encrypt(chain);
         crypt = work;
         work ^= chain;
         chain = crypt;
         BlockTo_Bytes(work, buf+=8);
      }
   }
   else //ECB mode, not using the Chain
   {
      for(; n >= 8; n -= 8)
      {
         _BytesToBlock(buf, work);
         CBFCrypt::Decrypt(work);
         BlockTo_Bytes(work, buf+=8);
      }
   }
}

void CBFDecrypt::Decrypt(const unsigned char* in, unsigned char* out, int n, int iMode)
{
   //Check the buffer's length - should be > 0 and multiple of 8
   if((n==0)||(n%8!=0))	logd ("CBFDecrypt::Decrypt(): Incorrect buffer length!\n");
   SBlock work;
   if(iMode == CBC) //CBC mode, using the Chain
   {
      SBlock crypt, chain(m_oChain);
      for(; n >= 8; n -= 8, in += 8)
      {
         _BytesToBlock(in, work);
         crypt = work;
         CBFCrypt::Decrypt(work);
         work ^= chain;
         chain = crypt;
         BlockTo_Bytes(work, out+=8);
      }
   }
   else if(iMode == CFB) //CFB mode, using the Chain, not using Decrypt()
   {
      SBlock crypt, chain(m_oChain);
      for(; n >= 8; n -= 8, in += 8)
      {
         _BytesToBlock(in, work);
         Encrypt(chain);
         crypt = work;
         work ^= chain;
         chain = crypt;
         BlockTo_Bytes(work, out+=8);
      }
   }
   else //ECB mode, not using the Chain
   {
      for(; n >= 8; n -= 8, in += 8)
      {
         _BytesToBlock(in, work);
         CBFCrypt::Decrypt(work);
         BlockTo_Bytes(work, out+=8);
      }
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 int GetValidString (const char* s_string,char *d_string,int d_str_len)

{
   int i,n;

   memset (d_string,0,d_str_len + 1);
   for (i = n = 0; s_string[i] && n < d_str_len; i++) {
      char c = (char)toupper (s_string[i]);
      if (('0' <= c && c <= '9') || ('A' <= c && c <= 'Z')) {
         d_string[n++] = c;
      }
   }
   d_string[n] = 0;
   if (n == d_str_len) return (0);
   else return (1);
}

 int DecodeKey (const char* license_key,unsigned char* auth_key,int& fc)

{
   int i;

   char license_key2[LICENSE_KEY_LENGTH + 1];
   if (GetValidString (license_key,license_key2,LICENSE_KEY_LENGTH)) return (0);

   // [0-9][A-Z] 의 문자열을 바이너리로 치환한다. 
   // [0-9][A-Z]의 문자 수는 36이며 이 중 혼돈 가능성이 있는 문자를 제외한
   // 32개 (2의 5승) 만 사용하여 표현을 한다. 

   const int text_len = 16;
   byte plain_text[text_len];
   byte cyper_text[text_len];

   int key_length = int(ceil(float(LICENSE_KEY_LENGTH) * 5 / 8));
   byte* key_buff = new byte[key_length];
   memset (key_buff,0,key_length);

   // 바이너리로 전환
   for (i = 0; i < LICENSE_KEY_LENGTH; i++) {
      byte c = license_key2[i];
      byte value = 0;
      if (c == 'Z') c = 'G';
      if (c == 'Y') c = 'I';
      if ('A' <= c && c <= 'Z') 
         value = c - 65 + 8;
      if ('0' <= c && c <= '9')
         value = c - 50;

      int bit_pos  = (i*5)%8;
      int byte_pos = (i*5)/8;
      int remain_bit_size = bit_pos - 3;

      byte mask_a = 0x1F;
      byte mask_b = 0x00;

      if (remain_bit_size > 0) {
         mask_a >>= remain_bit_size;
         mask_b = 0x1F & (~mask_a);
      }

      byte value_a = (value & mask_a) << bit_pos;
      byte value_b = (value & mask_b) >> (5-remain_bit_size);
      key_buff[byte_pos] |= value_a;
      if (value_b) key_buff[byte_pos+1] |= value_b;
   }
   // 첫번째 디코딩
   CBFDecrypt crypter (EncryptedSecret, 8);
   crypter.Decrypt (key_buff + 2, plain_text, text_len);
   memcpy (key_buff + 2, plain_text, text_len);

   // 두번째 디코딩
   memcpy (cyper_text, key_buff, text_len);
   crypter.Decrypt (cyper_text, plain_text, text_len);

   int ch_num = plain_text[0];
   fc = (int)plain_text[1];

   // 채널수 반영
   for (i = 2; i < text_len; i++) {
      int value = int (plain_text[i]) - (ch_num + fc);
      if (value < 0) value += 256;
      plain_text[i] = 0x000000FF & value;
   }

   memcpy (key_buff, plain_text, text_len);
   key_buff[key_length-1] &= ~(0x80 | 0x40);

   // 마지막 8비트 디코딩
   int value = key_buff[key_length-1] | (0xc0 & key_buff[key_length-2]);
   value -= key_buff[(ch_num + fc) % 16];
   if (value < 0) value += 256;

   key_buff[key_length-1] = (~0xc0) & value;
   key_buff[key_length-2] &= ~0xc0;
   key_buff[key_length-2] |=  0xc0 & value;

   memcpy (auth_key, key_buff+2, key_length-2);

   delete [] key_buff;

   return (ch_num);
}

 int CertifyKey (const char* license_key,const char* serial_no,const char* mac_addr,const char* prod_key,const char* prod_code,const char* version,int& fc)

{
   int i;

   fc = FCLASS_DISABLED;
   char serial_no2[26];
   if (GetValidString (serial_no,serial_no2,sizeof(serial_no2) - 1)) return (0);
   char mac_addr2[13];
   if (GetValidString (mac_addr ,mac_addr2 ,sizeof(mac_addr2)  - 1)) return (0);
   char prod_key2[9];
   if (GetValidString (prod_key ,prod_key2 ,sizeof(prod_key2)  - 1)) return (0);
   char prod_code2[4];
   if (GetValidString (prod_code,prod_code2,sizeof(prod_code2) - 1)) return (0);
   char version2[3];
   if (GetValidString (version,version2,sizeof(version2) - 1)) return (0);
   byte auth_key1[20];
   if (EncryptKey (auth_key1,serial_no2,mac_addr2,prod_key2,prod_code2,version2)) return (0);
   byte auth_key2[20];
   int n_channels = DecodeKey (license_key,auth_key2,fc);
   int key_length = int(ceil(float(LICENSE_KEY_LENGTH) * 5 / 8)) - 3;
   for (i = 0; i < key_length; i++) {
      if (auth_key1[i] != auth_key2[i]) {
         fc = FCLASS_DISABLED;
         return (0);
      }
   }
   if (fc < FCLASS_MONITORING || fc > FCLASS_VIDEOANALYTICS) {
      fc = FCLASS_DISABLED;
      return (0);
   }
   return (n_channels);
}

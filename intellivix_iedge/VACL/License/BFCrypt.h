#if !defined(__BFCRYPT_H)
#define __BFCRYPT_H

#define LICENSE_KEY_LENGTH   30

struct SBlock
{
   SBlock(unsigned int l=0, unsigned int r=0) : m_uil(l), m_uir(r) {}
   SBlock(const SBlock& roBlock) : m_uil(roBlock.m_uil), m_uir(roBlock.m_uir) {}
   SBlock& operator^=(SBlock& b) { m_uil ^= b.m_uil; m_uir ^= b.m_uir; return *this; }
   unsigned int m_uil, m_uir;
};

class CBFCrypt
{
public:
   enum { ECB=0, CBC=1, CFB=2 };

protected:
   SBlock m_oChain0;
   SBlock m_oChain;
   unsigned int m_auiP[18];
   unsigned int m_auiS[4][256];
   static const unsigned int scm_auiInitP[18];
   static const unsigned int scm_auiInitS[4][256];

public:
   CBFCrypt(unsigned char* ucKey, int n, const SBlock& roChain = SBlock(0UL,0UL));

   void ResetChain() { m_oChain = m_oChain0; }

protected:
   unsigned int F(unsigned int ui);
   void Encrypt(SBlock&);
   void Decrypt(SBlock&);
};

//Extract low order byte
inline unsigned char _Byte(unsigned int ui)
{
   return (unsigned char)(ui & 0xff);
}

//Function F
inline unsigned int CBFCrypt::F(unsigned int ui)
{
   return ((m_auiS[0][_Byte(ui>>24)] + m_auiS[1][_Byte(ui>>16)]) ^ m_auiS[2][_Byte(ui>>8)]) + m_auiS[3][_Byte(ui)];
}

//Semi-Portable _Byte Shuffling
inline void _BytesToBlock(unsigned char const* p, SBlock& b)
{
   unsigned int y;
   //Left
   b.m_uil = 0;
   y = *p++;
   y <<= 24;
   b.m_uil |= y;
   y = *p++;
   y <<= 16;
   b.m_uil |= y;
   y = *p++;
   y <<= 8;
   b.m_uil |= y;
   y = *p++;
   b.m_uil |= y;
   //Right
   b.m_uir = 0;
   y = *p++;
   y <<= 24;
   b.m_uir |= y;
   y = *p++;
   y <<= 16;
   b.m_uir |= y;
   y = *p++;
   y <<= 8;
   b.m_uir |= y;
   y = *p++;
   b.m_uir |= y;
}

inline void BlockTo_Bytes(SBlock const& b, unsigned char* p)
{
   unsigned int y;
   //Right
   y = b.m_uir;
   *--p = _Byte(y);
   y = b.m_uir >> 8;
   *--p = _Byte(y);
   y = b.m_uir >> 16;
   *--p = _Byte(y);
   y = b.m_uir >> 24;
   *--p = _Byte(y);
   //Left
   y = b.m_uil;
   *--p = _Byte(y);
   y = b.m_uil >> 8;
   *--p = _Byte(y);
   y = b.m_uil >> 16;
   *--p = _Byte(y);
   y = b.m_uil >> 24;
   *--p = _Byte(y);
}

#endif

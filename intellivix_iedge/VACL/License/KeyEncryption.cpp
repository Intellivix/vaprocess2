#include "bccl.h"
#include "License/KeyEncryption.h"

#define ROL32(_val32,_nBits) (((_val32)<<(_nBits))|((_val32)>>(32-(_nBits))))
#define SHABLK0(i) (SHA_Block->l[i])
#define SHABLK(i) (SHA_Block->l[i&15] = ROL32(SHA_Block->l[(i+13)&15] ^ SHA_Block->l[(i+8)&15] \
   ^ SHA_Block->l[(i+2)&15] ^ SHA_Block->l[i&15],1))

// SHA-1 rounds
#define _R0(v,w,x,y,z,i) { z+=((w&(x^y))^y)+SHABLK0(i)+0x5A827999+ROL32(v,5); w=ROL32(w,30); }
#define _R1(v,w,x,y,z,i) { z+=((w&(x^y))^y)+SHABLK(i)+0x5A827999+ROL32(v,5); w=ROL32(w,30); }
#define _R2(v,w,x,y,z,i) { z+=(w^x^y)+SHABLK(i)+0x6ED9EBA1+ROL32(v,5); w=ROL32(w,30); }
#define _R3(v,w,x,y,z,i) { z+=(((w|x)&y)|(w&x))+SHABLK(i)+0x8F1BBCDC+ROL32(v,5); w=ROL32(w,30); }
#define _R4(v,w,x,y,z,i) { z+=(w^x^y)+SHABLK(i)+0xCA62C1D6+ROL32(v,5); w=ROL32(w,30); }

unsigned char  EncryptedSecret[256] = {
   0x5E,0xC9,0x24,0xEF,0xE8,0x21,0x78,0x1D,0x1C,0x8A,0x71,0xD5,0x56,0xFD,0xF9,0xE1,
   0x82,0xC6,0xF4,0x29,0x3C,0x22,0x81,0xE1,0x36,0x0B,0x7D,0x01,0xDA,0x53,0x4A,0xF5,
   0x31,0xF0,0xCE,0x96,0x04,0x97,0xBE,0x5A,0x41,0x8C,0x1C,0xC7,0x9F,0xB8,0xFE,0x2E,
   0x85,0xFC,0x92,0x95,0x29,0xC5,0x5F,0x4F,0xF5,0xE0,0xCD,0xA6,0x2E,0x31,0xE8,0x17,
   0xD7,0xDE,0x61,0xC2,0xD3,0x31,0xD5,0x5F,0x4B,0x1E,0x41,0x58,0x4F,0x81,0x18,0x7E,
   0xC0,0xCD,0x9C,0xFA,0x6D,0xA0,0xD3,0x86,0x7C,0x9B,0xAC,0xDD,0x0C,0x70,0xDF,0x6F,
   0x18,0x3C,0xE5,0x5A,0x9E,0x18,0x47,0x96,0x01,0xEA,0x1C,0x6F,0xAD,0x82,0xCD,0x39,
   0xF9,0xE2,0x1C,0x3F,0x9F,0xDC,0x65,0xFC,0x94,0xE3,0x23,0x8E,0xBB,0x1B,0xB5,0x67,
   0xBC,0xB2,0x63,0x46,0xAD,0x73,0xEE,0x65,0x2D,0x98,0x81,0xF4,0x00,0xE0,0xA8,0xC7,
   0xF9,0xE2,0x19,0x4C,0x1D,0xA1,0x9D,0xBF,0x05,0x60,0x39,0xA1,0x84,0xB8,0xF5,0x66,
   0x8A,0xE7,0xE1,0x6E,0x48,0x6B,0x5A,0x36,0x95,0xD0,0xB4,0xD0,0x91,0xC6,0x2E,0x90,
   0x88,0x76,0x9C,0x09,0x19,0x16,0x04,0x37,0x97,0xBB,0xF6,0xFE,0xAF,0x6F,0x24,0xD3,
   0x4B,0x84,0x6A,0xBA,0xB7,0x28,0x0B,0x70,0x02,0x39,0x3E,0xE9,0xA8,0x5A,0xF5,0xFD,
   0x6D,0x46,0xAC,0x5E,0x8C,0x64,0x21,0xCD,0x11,0x9C,0x62,0x8D,0x84,0x6A,0xCC,0x18,
   0xC7,0x30,0x03,0x12,0x41,0xD1,0x36,0x9E,0x3D,0x7B,0xA5,0x28,0x8C,0xC4,0x60,0x74,
   0x72,0xF9,0x51,0x33,0xC0,0xB2,0x7C,0xE7,0x3D,0xAA,0x86,0x37,0x4A,0xCE,0x74,0x08
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: CBFEncrypt
//
///////////////////////////////////////////////////////////////////////////////

//Constructor - Initialize the P and S boxes for a given Key
CBFEncrypt::CBFEncrypt(unsigned char* ucKey, int keysize, const SBlock& roChain) 
: CBFCrypt(ucKey, keysize, roChain)
{
}

//Encrypt Buffer in Place
//Returns false if n is multiple of 8
void CBFEncrypt::Encrypt(unsigned char* buf, int n, int iMode)
{
   //Check the buffer's length - should be > 0 and multiple of 8
   if((n==0)||(n%8!=0)) logd ("CBFEncrypt::Encrypt(): Incorrect buffer length!\n");
   SBlock work;
   if(iMode == CBC) //CBC mode, using the Chain
   {
      SBlock chain(m_oChain);
      for(; n >= 8; n -= 8)
      {
         _BytesToBlock(buf, work);
         work ^= chain;
         CBFCrypt::Encrypt(work);
         chain = work;
         BlockTo_Bytes(work, buf+=8);
      }
   }
   else if(iMode == CFB) //CFB mode, using the Chain
   {
      SBlock chain(m_oChain);
      for(; n >= 8; n -= 8)
      {
         CBFCrypt::Encrypt(chain);
         _BytesToBlock(buf, work);
         work ^= chain;
         chain = work;
         BlockTo_Bytes(work, buf+=8);
      }
   }
   else //ECB mode, not using the Chain
   {
      for(; n >= 8; n -= 8)
      {
         _BytesToBlock(buf, work);
         CBFCrypt::Encrypt(work);
         BlockTo_Bytes(work, buf+=8);
      }
   }
}

//Encrypt from Input Buffer to Output Buffer
//Returns false if n is multiple of 8
void CBFEncrypt::Encrypt(const unsigned char* in, unsigned char* out, int n, int iMode)
{
   //Check the buffer's length - should be > 0 and multiple of 8
   if((n==0)||(n%8!=0)) logd ("CBFEncrypt::Incorrect buffer length!\n");
   SBlock work;
   if(iMode == CBC) //CBC mode, using the Chain
   {
      SBlock chain(m_oChain);
      for(; n >= 8; n -= 8, in += 8)
      {
         _BytesToBlock(in, work);
         work ^= chain;
         CBFCrypt::Encrypt(work);
         chain = work;
         BlockTo_Bytes(work, out+=8);
      }
   }
   else if(iMode == CFB) //CFB mode, using the Chain
   {
      SBlock chain(m_oChain);
      for(; n >= 8; n -= 8, in += 8)
      {
         CBFCrypt::Encrypt(chain);
         _BytesToBlock(in, work);
         work ^= chain;
         chain = work;
         BlockTo_Bytes(work, out+=8);
      }
   }
   else //ECB mode, not using the Chain
   {
      for(; n >= 8; n -= 8, in += 8)
      {
         _BytesToBlock(in, work);
         CBFCrypt::Encrypt(work);
         BlockTo_Bytes(work, out+=8);
      }
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 int EncodeKey (char* license_key,byte* auth_key,byte ch_num)

{
   int i;

   int key_length = int(ceil(float(LICENSE_KEY_LENGTH) * 5 / 8));
   byte* license_key_buff = new byte[key_length];
   memset (license_key_buff,0, key_length);
   memcpy (license_key_buff+1, auth_key, key_length-1);
   memcpy (license_key_buff  , &ch_num,  1);

   // 마지막 8비트 인코딩
   int value = license_key_buff[key_length-1] | (0xc0 & license_key_buff[key_length-2]);
   value += license_key_buff[ch_num % 16];
   if (value >= 256) 
      value -= 256;

   license_key_buff[key_length-1] = (~0xc0) & value;
   license_key_buff[key_length-2] &= ~0xc0;
   license_key_buff[key_length-2] |=  0xc0 & value;

   const int  text_len = 16;
   byte plain_text[text_len];
   byte cyper_text[text_len];

   memcpy (plain_text, license_key_buff, text_len);

   // 첫번째 인코딩전 채널 수 반영
   for (i = 1; i < text_len; i++) {
      int value = int (plain_text[i]) + ch_num;
      if (value >= 256) value -= 256;
      plain_text[i] = 0x000000FF & value;
   }
   // 첫번째 인코딩
   CBFEncrypt crypter (EncryptedSecret, 8);
   crypter.Encrypt ((byte*)plain_text, (byte*)cyper_text, text_len);
   memcpy (license_key_buff, cyper_text,  text_len);

   /// 두번째 인코딩
   memcpy (plain_text, license_key_buff+2, text_len);
   crypter.Encrypt ((byte*)plain_text, (byte*)cyper_text, text_len);
   memcpy (license_key_buff+2, cyper_text,  text_len);

   // 스트링으로 전환
   // [0-9][A-Z]의 문자 수는 36이며 이 중 혼돈 가능성이 있는 문자를 제외한
   // 32개 (2의 5승) 만 사용하여 표현을 한다. 
   for (i = 0; i < LICENSE_KEY_LENGTH; i++) {
      int bit_pos, byte_pos;

      bit_pos  = (i*5)%8;
      byte_pos = (i*5)/8;
      byte value = (license_key_buff[byte_pos] >> bit_pos) & 0x1F ;

      int remain_bit_size = bit_pos - 3;
      if (remain_bit_size > 0) {
         byte remain = license_key_buff[byte_pos+1];
         remain <<= 8-remain_bit_size;
         remain >>= 3;
         value |= remain;
      }

      if (0 <= value && value <=7) 
         value += 50;
      else if (8 <= value && value < 32) 
         value += 65 - 8;
      else
         return (1);

      if (value == 'G') value = 'Z';
      if (value == 'I') value = 'Y';

      license_key[i] = value;
   }
   license_key[i] = '\0';

   delete [] license_key_buff;

   return (0);
}

 int EncodeKey_V2 (char* license_key,byte* auth_key,byte ch_num,byte fc)

{
   int i;

   int key_length = int(ceil(float(LICENSE_KEY_LENGTH) * 5 / 8));
   byte* license_key_buff = new byte[key_length];
   memset (license_key_buff,0, key_length);
   memcpy (license_key_buff+2, auth_key, key_length-2);
   memcpy (license_key_buff+1, &fc,      1);
   memcpy (license_key_buff  , &ch_num,  1);

   // 마지막 8비트 인코딩
   int value = license_key_buff[key_length-1] | (0xc0 & license_key_buff[key_length-2]);
   value += license_key_buff[(ch_num + fc ) % 16];
   if (value >= 256) value -= 256;

   license_key_buff[key_length-1] = (~0xc0) & value;
   license_key_buff[key_length-2] &= ~0xc0;
   license_key_buff[key_length-2] |=  0xc0 & value;

   const int  text_len = 16;
   byte plain_text[text_len];
   byte cyper_text[text_len];

   memcpy (plain_text, license_key_buff, text_len);

   // 첫번째 인코딩전 채널 수 반영
   for (i = 2; i < text_len; i++) {
      int value = int (plain_text[i]) + (ch_num + fc);
      if (value >= 256) value -= 256;
      plain_text[i] = 0x000000FF & value;
   }

   // 첫번째 인코딩
   CBFEncrypt crypter (EncryptedSecret, 8);
   crypter.Encrypt ((byte*)plain_text, (byte*)cyper_text, text_len);
   memcpy (license_key_buff, cyper_text,  text_len);

   /// 두번째 인코딩
   memcpy (plain_text, license_key_buff + 2, text_len);
   crypter.Encrypt ((byte*)plain_text, (byte*)cyper_text, text_len);
   memcpy (license_key_buff+2, cyper_text,  text_len);

   // 스트링으로 전환
   // [0-9][A-Z]의 문자 수는 36이며 이 중 혼돈 가능성이 있는 문자를 제외한
   // 32개 (2의 5승) 만 사용하여 표현을 한다. 
   for (i = 0; i < LICENSE_KEY_LENGTH; i++) {
      int bit_pos, byte_pos;

      bit_pos  = (i*5)%8;
      byte_pos = (i*5)/8;
      byte value = (license_key_buff[byte_pos] >> bit_pos) & 0x1F ;

      int remain_bit_size = bit_pos - 3;
      if (remain_bit_size > 0) {
         byte remain = license_key_buff[byte_pos+1];
         remain <<= 8-remain_bit_size;
         remain >>= 3;
         value |= remain;
      }

      if (0 <= value && value <=7) 
         value += 50;
      else if (8 <= value && value < 32) 
         value += 65 - 8;
      else
         return (1);

      if (value == 'G') value = 'Z';
      if (value == 'I') value = 'Y';

      license_key[i] = value;
   }
   license_key[i] = '\0';

   delete [] license_key_buff;

   return (0);
}

 int EncryptKey (byte* auth_key,const char* serial_no,const char* mac_addr,const char* prod_key,const char* prod_code,const char*version)

{
   int i;

   byte mt[64];
   memset (mt,0,64);

   byte buf_serial_no[32];
   byte buf_mac_addr [32];
   byte buf_prod_key [32];
   byte buf_prod_code[32];
   byte buf_version  [32];
   byte buf_secret   [32];

   const int serial_no_size   = 25;
   const int mac_addr_size    =  6;
   const int prod_key_size    =  4;
   const int prod_code_size   =  2;
   const int version_size     =  1;
   const int secret_size      =  8;

   // 스트링을 바이너리로 전환한다. 

   // Serial No를 얻는다. 
   for (i = 0; i < serial_no_size; i++)
      buf_serial_no[i] = serial_no[i];

   char text[8];
   int  i_value;
   for (i = 0; i < mac_addr_size; i++) {
      strncpy (text, mac_addr + i * 2, 2);
      sscanf  (text, "%02x", &i_value);
      buf_mac_addr[i] = byte (0x000000FF & i_value);
   }

   for (i = 0; i < prod_key_size; i++) {
      strncpy (text, prod_key + i * 2, 2);
      sscanf  (text, "%02x", &i_value);
      buf_prod_key[i] = byte (0x000000FF & i_value);
   }

   for (i = 0; i < prod_code_size; i++) {
      strncpy (text, prod_code + i * 2, 2);
      sscanf  (text, "%02x", &i_value);
      buf_prod_code[i] = byte (0x000000FF & i_value);
   }

   for (i = 0; i < version_size; i++) {
      strncpy (text, version + i * 2, 2);
      sscanf  (text, "%02x", &i_value);
      buf_version[i] = byte (0x000000FF & i_value);
   }

   for (i = 0; i < 8; i++) {
      int nAddress = EncryptedSecret[255 - (i+1)];
      buf_secret[i] = EncryptedSecret[nAddress];
   }

   int mt_idx_mac_addr  [mac_addr_size ]  = {1,2,3,4,5,6};
   int mt_idx_serial_no [serial_no_size]  = {7,8,9,10,11, 12,13,14,15,16, 17,18,19,20,21, 22,23,24,25,26, 27,28,29,30,31};
   int mt_idx_secret    [secret_size   ]  = {38,39,40,41,42, 43,44,45};
   int mt_idx_prod_key  [prod_key_size ]  = {46,47,48,49};
   int mt_idx_version   [version_size  ]  = {52};
   int mt_idx_prod_code [prod_code_size]  = {56,63};

   // Message Table을 채운다. 
   for (i = 0; i < mac_addr_size; i++) 
      mt[mt_idx_mac_addr[i]] = buf_mac_addr [i];
   for (i = 0; i < serial_no_size; i++) 
      mt[mt_idx_serial_no[i]] = buf_serial_no [i];
   for (i = 0; i < secret_size; i++) 
      mt[mt_idx_secret[i]] = buf_secret [i];
   for (i = 0; i < prod_key_size; i++) 
      mt[mt_idx_prod_key[i]] = buf_prod_key [i];
   for (i = 0; i < version_size; i++) 
      mt[mt_idx_version[i]] = buf_version [i];
   for (i = 0; i < prod_code_size; i++) 
      mt[mt_idx_prod_code[i]] = buf_prod_code [i];

   // Message Table 중 채워지지 않는 부분을 특정 값으로 채운다. 
   int mt_use [64];

   for (i = 0; i < 64; i++) 
      mt_use [i] = FALSE;

   for (i = 0; i < mac_addr_size; i++) 
      mt_use [mt_idx_mac_addr[i]] = TRUE;
   for (i = 0; i < serial_no_size; i++) 
      mt_use [mt_idx_serial_no[i]] = TRUE;
   for (i = 0; i < secret_size; i++) 
      mt_use [mt_idx_secret[i]] = TRUE;
   for (i = 0; i < prod_key_size; i++) 
      mt_use [mt_idx_prod_key[i]] = TRUE;
   for (i = 0; i < version_size; i++) 
      mt_use [mt_idx_version[i]] = TRUE;
   for (i = 0; i < prod_code_size; i++) 
      mt_use [mt_idx_prod_code[i]] = TRUE;

   int not_use_size = 64 - (mac_addr_size + serial_no_size + secret_size + prod_key_size + version_size + prod_code_size);
   int* mt_idx_not_use = new int [not_use_size];
   int idx = 0;
   for (i = 0; i < 64; i++) {
      if (mt_use[i] == FALSE) {
         mt_idx_not_use[idx++] = i;
      }
   }
   for (i = 0; i < not_use_size; i++) {
      mt[mt_idx_not_use[i]] = EncryptedSecret[i];
   }
   memset (mt_use, 0, 64);
   memset (mt_idx_not_use, 0, not_use_size);
   delete [] mt_idx_not_use;

   // MAC (massage authenticate code)을 초기화한다. 
   uint A,B,C,D,E;

   typedef union 
   { 
      uint l[16]; 
   } SHA1_WORKSPACE_BLOCK; 
   byte SHA_WorkSpace[64]; 
   SHA1_WORKSPACE_BLOCK *SHA_Block = (SHA1_WORKSPACE_BLOCK *)SHA_WorkSpace;

   A=0x67452301;
   B=0xefcdab89;
   C=0x98badcfe;
   D=0x10325476;
   E=0xc3d2e1f0;

   uint a = A, b = B, c = C, d = D, e = E; 

   memcpy(SHA_Block, mt, 64); 

   _R0(a,b,c,d,e, 0); _R0(e,a,b,c,d, 1); _R0(d,e,a,b,c, 2); _R0(c,d,e,a,b, 3); 
   _R0(b,c,d,e,a, 4); _R0(a,b,c,d,e, 5); _R0(e,a,b,c,d, 6); _R0(d,e,a,b,c, 7); 
   _R0(c,d,e,a,b, 8); _R0(b,c,d,e,a, 9); _R0(a,b,c,d,e,10); _R0(e,a,b,c,d,11); 
   _R0(d,e,a,b,c,12); _R0(c,d,e,a,b,13); _R0(b,c,d,e,a,14); _R0(a,b,c,d,e,15); 
   _R1(e,a,b,c,d,16); _R1(d,e,a,b,c,17); _R1(c,d,e,a,b,18); _R1(b,c,d,e,a,19); 
   _R2(a,b,c,d,e,20); _R2(e,a,b,c,d,21); _R2(d,e,a,b,c,22); _R2(c,d,e,a,b,23); 
   _R2(b,c,d,e,a,24); _R2(a,b,c,d,e,25); _R2(e,a,b,c,d,26); _R2(d,e,a,b,c,27); 
   _R2(c,d,e,a,b,28); _R2(b,c,d,e,a,29); _R2(a,b,c,d,e,30); _R2(e,a,b,c,d,31); 
   _R2(d,e,a,b,c,32); _R2(c,d,e,a,b,33); _R2(b,c,d,e,a,34); _R2(a,b,c,d,e,35); 
   _R2(e,a,b,c,d,36); _R2(d,e,a,b,c,37); _R2(c,d,e,a,b,38); _R2(b,c,d,e,a,39); 
   _R3(a,b,c,d,e,40); _R3(e,a,b,c,d,41); _R3(d,e,a,b,c,42); _R3(c,d,e,a,b,43); 
   _R3(b,c,d,e,a,44); _R3(a,b,c,d,e,45); _R3(e,a,b,c,d,46); _R3(d,e,a,b,c,47); 
   _R3(c,d,e,a,b,48); _R3(b,c,d,e,a,49); _R3(a,b,c,d,e,50); _R3(e,a,b,c,d,51); 
   _R3(d,e,a,b,c,52); _R3(c,d,e,a,b,53); _R3(b,c,d,e,a,54); _R3(a,b,c,d,e,55); 
   _R3(e,a,b,c,d,56); _R3(d,e,a,b,c,57); _R3(c,d,e,a,b,58); _R3(b,c,d,e,a,59); 
   _R4(a,b,c,d,e,60); _R4(e,a,b,c,d,61); _R4(d,e,a,b,c,62); _R4(c,d,e,a,b,63); 
   _R4(b,c,d,e,a,64); _R4(a,b,c,d,e,65); _R4(e,a,b,c,d,66); _R4(d,e,a,b,c,67); 
   _R4(c,d,e,a,b,68); _R4(b,c,d,e,a,69); _R4(a,b,c,d,e,70); _R4(e,a,b,c,d,71); 
   _R4(d,e,a,b,c,72); _R4(c,d,e,a,b,73); _R4(b,c,d,e,a,74); _R4(a,b,c,d,e,75); 
   _R4(e,a,b,c,d,76); _R4(d,e,a,b,c,77); _R4(c,d,e,a,b,78); _R4(b,c,d,e,a,79); 
 
   A += a; 
   B += b; 
   C += c; 
   D += d; 
   E += e; 
 
   a = b = c = d = e = 0;  

   memcpy (auth_key   , &A, 4);
   memcpy (auth_key+ 4, &E, 4);
   memcpy (auth_key+ 8, &C, 4);
   memcpy (auth_key+12, &D, 4);
   memcpy (auth_key+16, &B, 4);

   int key_length = int(ceil(float(LICENSE_KEY_LENGTH) * 5 / 8)) - 1;
   auth_key[key_length-1] &= ~(0x80 | 0x40);
   auth_key[key_length] = 0;

   return (0);
}

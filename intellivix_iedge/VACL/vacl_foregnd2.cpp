#include "vacl_event.h"
#include "vacl_filter.h"
#include "vacl_foregnd2.h"
#include "vacl_morph.h"
#include "vacl_objtrack.h"

#if defined(__DEBUG_FGD) || defined(__DEBUG_MBS)
#include "Win32CL/Win32CL.h"
#endif

#if defined(__DEBUG_FGD)
extern int _DVX_FGD,_DVY_FGD;
extern CImageView* _DebugView_FGD;
#endif

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: CThread_FGD_SGBM
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_WIN32CL)

 CThread_FGD_SGBM::CThread_FGD_SGBM (   )

{
   ThreadIdx = -1;
   SrcImage  = NULL;
}

 void CThread_FGD_SGBM::ThreadProc (   )

{
   if (ThreadIdx == -1) return;
   HANDLE events[2];
   events[0] = (HANDLE)Event_Stop;
   events[1] = (HANDLE)Event_Start;
   Event_Start.ResetEvent (   );
   Event_End.ResetEvent   (   );
   while (TRUE) {
      DWORD result = WaitForMultipleObjects (2,events,FALSE,INFINITE);
      if (result == WAIT_OBJECT_0) break;       // Stop  이벤트인 경우
      else if (result == (WAIT_OBJECT_0 + 1)) { // Start 이벤트인 경우
         if (SrcImage != NULL) {
            GImage s_image = SrcImage->Extract (ROI);
            ForegroundDetector.Perform (s_image);
         }
         Event_End.SetEvent (   );
      }
   }
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: ForegroundDetection_SGBM_MT
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_WIN32CL)

 ForegroundDetection_SGBM_MT::ForegroundDetection_SGBM_MT (   )

{
   ClassID = CLSID_ForegroundDetection_SGBM_MT;

   NumThreads = 2;
}

 ForegroundDetection_SGBM_MT::~ForegroundDetection_SGBM_MT (   )

{
   Close (   );
}

 void ForegroundDetection_SGBM_MT::Close (   )

{
   Threads.Delete   (   );
   EndEvents.Delete (   );
}

 int ForegroundDetection_SGBM_MT::GetFrgImage (   )

{
   int i,x1,y1,x2,y2;

   for (i = 0; i < Threads.Length; i++) {
      CThread_FGD_SGBM& thread = Threads[i];
      thread.SrcImage = &SrcImage;
      thread.Event_End.ResetEvent (   );
      thread.Event_Start.SetEvent (   );
   }
   WaitForMultipleObjects (EndEvents.Length,EndEvents,TRUE,INFINITE);
   if (GetBkgModelUpdateMode (   ) == FGD_BMU_MODE_INITIAL) return (FGD_RCODE_INITIAL_BACKGROUND_LEARNING);
   // 각 쓰레드가 처리한 결과를 병합한다.
   FrgImage.Clear (   );
   FrgEdgeImage3.Clear (   );
   for (i = 0; i < Threads.Length; i++) {
      CThread_FGD_SGBM& thread = Threads[i];
      IBox2D& roi = thread.ROI;
      GImage& s_fg_image = thread.ForegroundDetector.FrgImage;
      GImage& s_fe_image = thread.ForegroundDetector.FrgEdgeImage3;
      for (y1 = 0,y2 = roi.Y; y1 < s_fg_image.Height; y1++,y2++) {
         byte* l_s_fg_image = s_fg_image[y1];
         byte* l_s_fe_image = s_fe_image[y1];
         byte* l_d_fg_image = FrgImage[y2];
         byte* l_d_fe_image = FrgEdgeImage3[y2];
         for (x1 = 0,x2 = roi.X; x1 < s_fg_image.Width; x1++,x2++) {
            l_d_fg_image[x2] |= l_s_fg_image[x1];
            l_d_fe_image[x2] |= l_s_fe_image[x1];
         }
      }
   }
   return (FGD_RCODE_NORMAL);
}

 int ForegroundDetection_SGBM_MT::Initialize (ISize2D& si_size,float frm_rate)

{
   int i;
   
   if (ForegroundDetection::Initialize (si_size,frm_rate)) return (1);
   Status |= FGD_STATUS_FOREGROUND_EDGE_IMAGE;
   // FrgEdgeImage1,FrgEdgeImage2,BkgEdgeImage는 제외하고, FrgEdgeImage3만 구하는 것으로 한다.
   FrgEdgeImage3.Create (si_size.Width,si_size.Height);
   // 쓰레드 구성을 한다.
   EndEvents.Create (NumThreads);
   Threads.Create   (NumThreads);
   // 인접하는 처리 영역이 약간씩 겹치게 만든다.
   int sx = 0, sy = 0;
   int width1 = (si_size.Width / NumThreads) / 2 * 2;
   int margin = (int)(0.05f * width1) / 2 * 2; // PARAMETERS
   int width2 = width1 + margin;
   int width3 = width2 + margin;
   int height = si_size.Height;
   for (i = 0; i < NumThreads; i++) {
      CThread_FGD_SGBM& thread = Threads[i];
      thread.Event_Start.ResetEvent (   );
      thread.Event_End.ResetEvent   (   );
      EndEvents[i] = (HANDLE)thread.Event_End;
      thread.ThreadIdx = i;
      if (NumThreads <= 1) {
         thread.ROI(0,0,si_size.Width,si_size.Height);
      }
      else {
         if (i == 0) thread.ROI(sx,sy,width2,height);
         else if (i == NumThreads - 1) thread.ROI(sx - margin,sy,width2,height);
         else thread.ROI(sx - margin,sy,width3,height);
         sx += width1;
      }
      CropRegion (si_size,thread.ROI);
      thread.ForegroundDetector.CopyParams2 (*this);
      ISize2D pi_size(thread.ROI.Width,thread.ROI.Height);
      thread.ForegroundDetector.Initialize (pi_size,frm_rate);
      thread.StartThread (   );
   }
   // 모든 쓰레드가 시작했는지 체크한다.
   while (TRUE) {
      int n = 0;
      for (i = 0; i < Threads.Length; i++)
         if (Threads[i].IsThreadRunning (   )) n++;
      if (n == Threads.Length) break;
   }
   return (DONE);
}

 int ForegroundDetection_SGBM_MT::UpdateBkgModel (GImage& m_image)

{
   int i;

   if (!IsEnabled (   )) return (1);
   for (i = 0; i < Threads.Length; i++) {
      CThread_FGD_SGBM& thread = Threads[i];
      GImage m_image2 = m_image.Extract (thread.ROI);
      thread.ForegroundDetector.UpdateBkgModel (m_image2);
   }
   return (DONE);
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: ForegroundDetection_FAD
//
///////////////////////////////////////////////////////////////////////////////

 ForegroundDetection_FAD::ForegroundDetection_FAD (   )
 
{
   ClassID = CLSID_ForegroundDetection_FAD;
}

 ForegroundDetection_FAD::~ForegroundDetection_FAD (   )

{
   Close (   );
}

 void ForegroundDetection_FAD::Close (   )
 
{
   ForegroundDetection::Close (   );
   MotionDetector.Close  (   );
}

 int ForegroundDetection_FAD::GetFrgImage (   )
 
{
   MotionDetector.CFRadius = CFRadius;
   if (FrameCount <= 1) {
      MotionDetector.Initialize (SrcImage,FrameRate);
      FrgImage.Clear (   );
   }
   else MotionDetector.Perform (SrcImage,FrgImage);
   return (FGD_RCODE_NORMAL);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: ForegroundDetection_Underwater
//
///////////////////////////////////////////////////////////////////////////////

 ForegroundDetection_Underwater::ForegroundDetection_Underwater (   )

{
   ClassID         = CLSID_ForegroundDetection_Underwater;
   DThreshold      = 2.5f;
   EDThreshold     = 30;
   CFRadius        = 2;
   VGFFSize        = 0;
   BkgUpdatePeriod = 1.0f;
   SNRThreshold    = 10;
 }

 void ForegroundDetection_Underwater::GetFrgEdgeImages (GImage& fg_image)

{
   int x,y;

   float a = 1.0f;
   if (Status & FGD_STATUS_LOW_EDGE_THRESHOLD ) a = 0.4f; // PARAMETERS
   if (Status & FGD_STATUS_HIGH_EDGE_THRESHOLD) a = 1.3f; // PARAMETERS
   int ed_thrsld  = (int)(a * EDThreshold + 0.5f);
   int ed_thrsld2 = (int)(0.75f * ed_thrsld);             // PARAMETERS
   BkgEdgeImage.Clear  (   );
   FrgEdgeImage1.Clear (   );
   FrgEdgeImage2.Clear (   );
   FrgEdgeImage3.Clear (   );
   int ex = SrcImage.Width  - 1;
   int ey = SrcImage.Height - 1;
   for (y = 1; y < ey; y++) {
      byte*  l_s_image   = SrcImage[y];
      byte*  l_s_image1  = SrcImage[y - 1];
      byte*  l_s_image2  = SrcImage[y + 1];
      byte*  l_fg_image  = fg_image[y];
      byte*  l_fe_image1 = FrgEdgeImage1[y];
      byte*  l_fe_image2 = FrgEdgeImage2[y];
      byte*  l_be_image  = BkgEdgeImage[y];
      short* l_be_map    = BkgEdgeMap[y];
      for (x = 1; x < ex; x++) {
         if (l_fg_image[x]) {
            int x1 = x - 1;
            int x2 = x + 1;
            short gx = (l_s_image2[x2] + 2 * (short)l_s_image[x2] + l_s_image1[x2]) -
                       (l_s_image2[x1] + 2 * (short)l_s_image[x1] + l_s_image1[x1]);
            short gy = (l_s_image2[x2] + 2 * (short)l_s_image2[x] + l_s_image2[x1]) -
                       (l_s_image1[x1] + 2 * (short)l_s_image1[x] + l_s_image1[x2]);
            int   gm = abs (gx) + abs (gy);
            if (abs(gm - l_be_map[x]) >= ed_thrsld) l_fe_image1[x] = PG_WHITE;
            if (gm >= ed_thrsld) l_fe_image2[x] = PG_WHITE;
            if (l_be_map[x] >= ed_thrsld2) l_be_image[x] = PG_WHITE;
         }
      }
   }
   GImage be_image = BkgEdgeImage;
   int sy = BkgEdgeImage.Height / 2;
   GImage t_image1 = BkgEdgeImage.Extract (0,sy,BkgEdgeImage.Width,BkgEdgeImage.Height - sy);
   GImage s_element(1,7);
   s_element.Set (0,0,s_element.Width,s_element.Height,PG_WHITE);
   GImage t_image2(t_image1.Width,t_image1.Height);
   BinDilation (t_image1,s_element,t_image2);
   be_image.Copy (t_image2,0,0,t_image2.Width,t_image2.Height,0,sy);
   for (y = 1; y < ey; y++) {
      byte* l_fe_image2 = FrgEdgeImage2[y];
      byte* l_fe_image3 = FrgEdgeImage3[y];
      byte* l_be_image  = be_image[y];
      for (x = 1; x < ex; x++) {
         if (l_fe_image2[x] && l_fe_image2[x] && !l_be_image[x]) l_fe_image3[x] = PG_WHITE;
      }
   }
   IArray2D cr_map(FrgEdgeImage3.Width,FrgEdgeImage3.Height);
   int n_crs = LabelCRs (FrgEdgeImage3,cr_map);
   if (!n_crs) return;
   CRArray1D cr_array(n_crs + 1);
   GetCRInfo (cr_map,cr_array);
   BArray1D mk_array(cr_array.Length);
   mk_array.Clear (   );
   for (int i = 1; i < cr_array.Length; i++) {
      ConnectedRegion& cr = cr_array[i];
      if (cr.Y >= sy && cr.Area < 10) mk_array[i] = TRUE;
   }
   RemoveCRs (cr_map,mk_array,FrgEdgeImage3);
   if (ObjectTracker != NULL) {
      FPoint2D scale(1.0f,1.0f);
      TrackedObject* t_object = ObjectTracker->TrackedObjects.First (   );
      while (t_object != NULL) {
         if ((t_object->Status & TO_STATUS_VERIFIED) && !(t_object->Status & TO_STATUS_STATIC2)) {
            IPoint2D p_pos;
            t_object->GetPredictedPosition (p_pos,ObjectTracker->FrameDuration);
            ISize2D  p_size;
            t_object->GetPredictedSize (p_size,ObjectTracker->FrameDuration);
            IBox2D p_rgn(p_pos.X,p_pos.Y,p_size.Width,p_size.Height);
            p_rgn = p_rgn.Inflate (scale);
            CropRegion (ObjectTracker->SrcImgSize,p_rgn);
            p_pos(p_rgn.X,p_rgn.Y);
            FrgEdgeImage3.Copy (FrgEdgeImage2,p_rgn,p_pos);
         }
         t_object = ObjectTracker->TrackedObjects.Next (t_object);
      }
   }   
}

}

#if !defined(__VACL_FILTER_H)
#define __VACL_FILTER_H

#include "vacl_define.h"

 namespace VACL

{

#define GAUSSIAN_KERNEL_SIZE(sigma)   ((int)(sigma * 4.0f) / 2 * 2 + 1)

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

void BoxFiltering          (GImage &s_image,int kernel_size,GImage &d_image);
void BoxFiltering          (IArray2D &s_array,int kernel_size,IArray2D &d_array);
void Convolve              (FArray1D &s_array,FArray1D &kernel,FArray1D &d_array);
void ConvolveHorizontally  (GImage &s_image,FArray1D &kernel,FArray2D &d_array);
void ConvolveVertically    (FArray2D &s_array,FArray1D &kernel,GImage &d_image);
void FillGaps              (GImage& s_image,int win_width,int win_height,GImage& d_image);
void GaussianFiltering     (FArray1D &s_array,float sigma,FArray1D &d_array);
void GaussianFiltering     (GImage &s_image,float sigma,GImage &d_image);
void GaussianFiltering     (GImage &s_image,float sigma_x,float sigma_y,GImage &d_image);
void GaussianFiltering_3x3 (GImage &s_image,GImage &d_image);
void GetGaussianKernel     (float sigma,FArray1D &d_array);
void GetHorizontalSums     (GImage &s_image,int kernel_size,IArray2D &d_array);
void GetHorizontalSums     (IArray2D &s_array,int kernel_size,IArray2D &d_array);
void GetVerticalSums       (IArray2D &s_array,int kernel_size,IArray2D &d_array);
void HistogramStretching   (GImage &s_image,float a,float b,GImage &d_image);
void MirrorKernel          (FArray1D &s_array,FArray1D &d_array);
void SigmaFiltering        (GImage &s_image,int win_size,int sigma,int n,GImage &d_image);

}

#endif

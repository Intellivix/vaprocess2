#include "vacl_geometry.h"
#include "vacl_warping.h"

#if defined(__LIB_IPP)
#include "vacl_ipp.h"
#endif

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
//  Class: WarpingTable
//
///////////////////////////////////////////////////////////////////////////////

 int WarpingTable::Create (int si_width,int si_height,int di_width,int di_height)

{
   SrcImgWidth  = si_width;
   SrcImgHeight = si_height;
   return (Array2D<WTElement>::Create (di_width,di_height));
}

 void WarpingTable::Crop (int sx,int sy,int width,int height)

{
   Array2D<WTElement> t_array(width,height);
   t_array.Copy (*this,sx,sy,width,height,0,0);
   Array2D<WTElement>::Create (width,height);
   Copy (t_array,0,0,width,height,0,0);
}

 void WarpingTable::Crop (IBox2D &c_rgn)

{
   Crop (c_rgn.X,c_rgn.Y,c_rgn.Width,c_rgn.Height);
}

 void WarpingTable::GetInterpolationWeights (   )

{
   int x,y;
   
   int ex = SrcImgWidth  - 1;
   int ey = SrcImgHeight - 1;
   for (y = 0; y < Height; y++) {
      WTElement *l_Array = Array[y];
      for (x = 0; x < Width; x++) {
         FPoint2D *p = &l_Array[x].Coord;
         int ix = (int)p->X;
         int iy = (int)p->Y;
         if (0 <= ix && ix < ex && 0 <= iy && iy < ey) {
            float rx2 = p->X - ix;
            float ry2 = p->Y - iy;
            float rx1 = 1.0f - rx2;
            float ry1 = 1.0f - ry2;
            float *w = Array[y][x].Weights;
            w[0] = rx1 * ry1;
            w[1] = rx1 * ry2;
            w[2] = rx2 * ry1;
            w[3] = rx2 * ry2; 
         }
         else p->X = p->Y = -1.0f;
      }
   }
}

 int WarpingTable::ReadFile (const char *file_name)

{
   FileIO file(file_name,FIO_BINARY,FIO_READ);
   if (!file) return (1);
   if (ReadFile (file)) return (2);
   return (DONE);
}

 int WarpingTable::ReadFile (FileIO &file)

{
   int si_width,si_height,di_width,di_height;

   if (file.Read ((byte *)&di_width ,1,sizeof(int))) return (1);
   if (file.Read ((byte *)&di_height,1,sizeof(int))) return (1);
   if (di_width == 0 || di_height == 0) return (2);
   if (file.Read ((byte *)&si_width ,1,sizeof(int))) return (1);
   if (file.Read ((byte *)&si_height,1,sizeof(int))) return (1);
   Create (si_width,si_height,di_width,di_height);
   if (file.Read ((byte *)Array[0],Width * Height,sizeof(WTElement))) return (1);
   return (DONE);
}

 int WarpingTable::Warp (GImage &s_image,GImage &d_image)

{
   int x,y;

   if (SrcImgWidth != s_image.Width || SrcImgHeight != s_image.Height) return (1);
   if (d_image.Width != Width || d_image.Height != Height) d_image.Create (Width,Height);
   for (y = 0; y < Height; y++) {
      WTElement* l_Array   = Array[y];
      byte*      l_d_image = d_image[y];
      for (x = 0; x < Width; x++) {
         int x1 = (int)l_Array[x].Coord.X;
         if (x1 != -1) {
            int y1 = (int)l_Array[x].Coord.Y;
            int x2 = x1 + 1;
            int y2 = y1 + 1;
            float *w = l_Array[x].Weights;
            l_d_image[x] = (byte)(w[0] * s_image[y1][x1] + w[1] * s_image[y2][x1] + w[2] * s_image[y1][x2] + w[3] * s_image[y2][x2]);
         }
      }
   }
   return (DONE);
}

 int WarpingTable::Warp (BGRImage &s_image,BGRImage &d_image,int flag_sse2)

{
   int x,y;
   BGRPixel  *dp;
   WTElement *wte;
   #if defined(__SIMD_SSE2)
   int i,j;
   BGRPixel *sp1,*sp2;
   #endif
   
   if (SrcImgWidth != s_image.Width || SrcImgHeight != s_image.Height) return (1);
   if (d_image.Width != Width || d_image.Height != Height) d_image.Create (Width,Height);
   if (flag_sse2) {
      #if defined(__SIMD_SSE2)
      __asm {
         pxor xmm7, xmm7                    // xmm7_w = [ 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 ]
      }
      for (i = 0; i < Height; i++) {
         wte = Array[i];
         dp  = d_image[i];
         for (j = 0; j < Width; j++) {
            x = (int)wte->Coord.X;
            if (x != -1) {
               y = (int)wte->Coord.Y;
               sp1 = s_image[y] + x;
               sp2 = s_image[y + 1] + x;
               __asm {
                  mov      eax , wte
                  movupd   xmm0, [eax]      // xmm0_d = [ w3 | w2 | w1 | w0 ]
                  movapd   xmm1, xmm0      
                  shufps   xmm1, xmm1, 00h  // xmm1_d = [ w0 | w0 | w0 | w0 ]
                  movapd   xmm2, xmm0
                  shufps   xmm2, xmm2, 55h  // xmm2_d = [ w1 | w1 | w1 | w1 ]
                  movapd   xmm3, xmm0
                  shufps   xmm3, xmm3, 0AAh // xmm3_d = [ w2 | w2 | w2 | w2 ]
                  movapd   xmm4, xmm0
                  shufps   xmm4, xmm4, 0FFh // xmm4_d = [ w3 | w3 | w3 | w3 ]

                  mov       eax , sp1
                  movss     xmm5, [eax]     // xmm5_b = [ 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | ? | R0 | G0 | B0 ]
                  punpcklbw xmm5, xmm7      // xmm5_w = [ 0 | 0 | 0 | 0 | ? | R0 | G0 | B0 ]
                  punpcklwd xmm5, xmm7      // xmm5_d = [ ? | R0 | G0 | B0 ]
                  cvtdq2ps  xmm5, xmm5
                  mulps     xmm1, xmm5      // xmm1_d = [ ? | w0 * R0 | w0 * G0 | w0 * B0 ]
                                            //        = [ ? | wR0 | wB0 | wB0 ]

                  movss     xmm5, [eax + 3] // xmm5_b = [ 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | ? | R2 | G2 | B2 ]
                  punpcklbw xmm5, xmm7      // xmm5_w = [ 0 | 0 | 0 | 0 | ? | R2 | G2 | B2 ]
                  punpcklwd xmm5, xmm7      // xmm5_d = [ ? | R2 | G2 | B2 ]
                  cvtdq2ps  xmm5, xmm5
                  mulps     xmm3, xmm5      // xmm3_d = [ ? | w2 * R2 | w2 * G2 | w2 * B2 ]
                                            //        = [ ? | wR2 | wB2 | wB2 ]

                  mov       eax , sp2
                  movss     xmm5, [eax]     // xmm5_b = [ 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | ? | R1 | G1 | B1 ]
                  punpcklbw xmm5, xmm7      // xmm5_w = [ 0 | 0 | 0 | 0 | ? | R1 | G1 | B1 ]
                  punpcklwd xmm5, xmm7      // xmm5_d = [ ? | R1 | G1 | B1 ]
                  cvtdq2ps  xmm5, xmm5
                  mulps     xmm2, xmm5      // xmm2_d = [ ? | w1 * R1 | w1 * G1 | w1 * B1 ]
                                            //        = [ ? | wR1 | wB1 | wB1 ]

                  movss     xmm5, [eax + 3] // xmm5_b = [ 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | ? | R3 | G3 | B3 ]
                  punpcklbw xmm5, xmm7      // xmm5_w = [ 0 | 0 | 0 | 0 | ? | R3 | G3 | B3 ]
                  punpcklwd xmm5, xmm7      // xmm5_d = [ ? | R3 | G3 | B3 ]
                  cvtdq2ps  xmm5, xmm5
                  mulps     xmm4, xmm5      // xmm4_d = [ ? | w3 * R3 | w3 * G3 | w3 * B3 ]
                                            //        = [ ? | wR3 | wB3 | wB3 ]

                  addps     xmm1, xmm2
                  addps     xmm1, xmm3
                  addps     xmm1, xmm4      // xmm1_d = xmm1 + xmm2 + xmm3 + xmm4
                                            //        = [ ? | dR | dG | dB ]

                  cvtps2dq  xmm1, xmm1
                  packssdw  xmm1, xmm1      // xmm1_w = [ ? | ? | ? | ? | ? | dR | dG | dB ] 
                  packuswb  xmm1, xmm1      // xmm1_b = [ ? | ? | ? | ? | ? | ? | ? | ? | ? | ? | ? | ? | ? | dR | dG | dB ]
               
                  mov       eax , dp
                  movd      [eax], xmm1
               }
            }
            else {
               __asm {
                  mov  eax, dp
                  movd [eax], xmm7
               }
            }
            wte++;
            dp++;
         }
      }
      #else
      return (-1);
      #endif
   }
   else {
      for (y = 0; y < Height; y++) {
       wte = Array[y];
       dp  = d_image[y];
         for (x = 0; x < Width; x++) {
            int x1 = (int)(wte->Coord.X);
            if (x1 != -1) {
               int y1 = (int)(wte->Coord.Y);
               int x2 = x1 + 1;
               int y2 = y1 + 1;
               BGRPixel *sp11 = &s_image[y1][x1];
               BGRPixel *sp21 = &s_image[y2][x1];
               BGRPixel *sp12 = &s_image[y1][x2];
               BGRPixel *sp22 = &s_image[y2][x2];
               float *w = wte->Weights;
               dp->R = (byte)(w[0] * sp11->R + w[1] * sp21->R + w[2] * sp12->R + w[3] * sp22->R);
               dp->G = (byte)(w[0] * sp11->G + w[1] * sp21->G + w[2] * sp12->G + w[3] * sp22->G);
               dp->B = (byte)(w[0] * sp11->B + w[1] * sp21->B + w[2] * sp12->B + w[3] * sp22->B);
            }
            wte++;
            dp++;
         }
      }
      return (DONE);
   }
}

 int WarpingTable::Warp_YUY2 (byte* si_buffer,byte* di_buffer)

{
   int x,y;

   int si_width2 = SrcImgWidth * 2;
   int di_width2 = Width * 2;
   for (y = 0; y < Height; y++) {
      WTElement* l_Array = Array[y];
      for (x = 0; x < Width; x++) {
         byte* dp = di_buffer + 2 * x;
         int x1 = (int)l_Array[x].Coord.X;
         if (x1 != -1) {
            int y1 = (int)l_Array[x].Coord.Y;
            byte* sp1 = si_buffer + y1 * si_width2 + 2 * x1;
            byte* sp2 = sp1 + si_width2;
            float *w  = l_Array[x].Weights;
            dp[0] = (byte)(w[0] * sp1[0] + w[1] * sp2[0] + w[2] * sp1[2] + w[3] * sp2[2]);
            if (!(x & 0x01)) {  // 타겟 픽셀의 X 좌표 값이 짝수면 소스 영상으로부터 U,V 값을 가져온다.
               if (x1 & 0x01) { // 소스 픽셀의 X 좌표 값이 홀수인 경우
                  dp[1] = sp1[-1];
                  dp[3] = sp1[1];
               }
               else { // 소스 픽셀의 X 좌표 값이 짝수인 경우
                  dp[1] = sp1[1];
                  dp[3] = sp1[3];
               }
            }
         }
         else {
            dp[0] = 0;
            if (!(x & 0x01)) dp[1] = dp[3] = 128;
         }
      }
      di_buffer += di_width2;
   }
   return (DONE);
}

 int WarpingTable::WriteFile (const char *file_name)

{
   FileIO file(file_name,FIO_BINARY,FIO_CREATE);
   if (!file) return (1);
   if (WriteFile (file)) return (2);
   return (DONE);
}

 int WarpingTable::WriteFile (FileIO &file)

{
   if (file.Write ((byte *)&Width,1,sizeof(int))) return (1);
   if (file.Write ((byte *)&Height,1,sizeof(int))) return (1);
   if (file.Write ((byte *)&SrcImgWidth,1,sizeof(int))) return (1);
   if (file.Write ((byte *)&SrcImgHeight,1,sizeof(int))) return (1);
   if (file.Write ((byte *)Array[0],Width * Height,sizeof(WTElement))) return (1);
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
//  Class: ForwardWarpingTable
//
///////////////////////////////////////////////////////////////////////////////

 FPoint2D ForwardWarpingTable::operator () (float fx,float fy) const

{
   int ix  = (int)fx;
   int iy  = (int)fy;
   if (Array[iy][ix].Coord.X == -1.0f) return (FPoint2D(-1.0f,-1.0f));
   int ix1 = ix + 1;
   int iy1 = iy + 1;
   if (ix1 >= Width || iy1 >= Height) return (Array[iy][ix].Coord);
   if (Array[iy1][ix].Coord.X == -1.0f || Array[iy][ix1].Coord.X == -1.0f || Array[iy1][ix1].Coord.X == -1.0f) return (FPoint2D(-1.0f,-1.0f));
   float rx = fx - ix;
   float ry = fy - iy;
   FPoint2D p1 = Array[iy ][ix ].Coord + (Array[iy ][ix1].Coord - Array[iy ][ix ].Coord) * rx;
   FPoint2D p2 = Array[iy1][ix ].Coord + (Array[iy1][ix1].Coord - Array[iy1][ix ].Coord) * rx;
   FPoint2D p3 = Array[iy ][ix ].Coord + (Array[iy1][ix ].Coord - Array[iy ][ix ].Coord) * ry;
   FPoint2D p4 = Array[iy ][ix1].Coord + (Array[iy1][ix1].Coord - Array[iy ][ix1].Coord) * ry;
   FLine2D line1(p1,p2);
   FLine2D line2(p3,p4);
   FPoint2D d_point;
   if (GetIntersectionPoint (line1,line2,d_point)) return (Array[iy][ix].Coord);
   else return (d_point);
}

 int ForwardWarpingTable::Create (int si_width,int si_height,int step_size)

{
   int x1,y1;

   GetForwardWarpingRange (si_width,si_height);
   int di_width  = (int)MaxPos.X - (int)MinPos.X + 1;
   int d_height = (int)MaxPos.Y - (int)MinPos.Y + 1;
   if (WarpingTable::Create (si_width,si_height,di_width,d_height) != DONE) return (1);
   WTElement wte;
   wte.Coord (-1.0f,-1.0f);
   Set (0,0,Width,Height,wte);
   int ex = si_width  - 2;
   int ey = si_height - 2;
   for (y1 = 0; y1 < ey; y1 += step_size) {
      int y2 = y1 + step_size;
      if (y2 > ey) y2 = ey;
      if (y1 >= y2) break;
      for (x1 = 0; x1 < ex; x1 += step_size) {
         int x2 = x1 + step_size;
         if (x2 > ex) x2 = ex;
         if (x1 >= x2) break;
         WarpRectangularPatch (x1,y1,x2,y2);
      }
   }
   return (DONE);
}

 void ForwardWarpingTable::CropNonimageArea (   )

{
   Crop (ValidRegion);
}

 void ForwardWarpingTable::GetForwardWarpingRange (int si_width,int si_height)

{
   int n,x,y;

   int ey = si_height - 1;
   float y1 = -MC_VLV, y2 = MC_VLV;
   FP2DArray1D m_points(si_width * 2 + si_height * 2);
   for (x = n = 0; x < si_width; x++) {
      m_points[n] = ForwardWarpingFunction (x,0);
      if (m_points[n].Y > y1) y1 = m_points[n].Y;
      n++;
      m_points[n] = ForwardWarpingFunction (x,ey);
      if (m_points[n].Y < y2) y2 = m_points[n].Y;
      n++;
   }
   int ex = si_width - 1;
   float x1 = -MC_VLV, x2 = MC_VLV;
   for (y = 0; y < si_height; y++) {
      m_points[n] = ForwardWarpingFunction (0,y);
      if (m_points[n].X > x1) x1 = m_points[n].X;
      n++;
      m_points[n] = ForwardWarpingFunction (ex,y);
      if (m_points[n].X < x2) x2 = m_points[n].X;
      n++;
   }
   IPoint2D min_i,max_i;
   FindMinMax (m_points,MinPos,MaxPos,min_i,max_i);
   ValidRegion.X      = (int)ceil (x1 - MinPos.X);
   ValidRegion.Y      = (int)ceil (y1 - MinPos.Y);
   ValidRegion.Width  = (int)(x2 - MinPos.X) - ValidRegion.X + 1;
   ValidRegion.Height = (int)(y2 - MinPos.Y) - ValidRegion.Y + 1;
}
 
 int ForwardWarpingTable::ReadFile (FileIO &file)

{
   if (file.Read ((byte *)&ValidRegion,1,sizeof(IBox2D))) return (1);
   if (file.Read ((byte *)&MinPos,1,sizeof(FPoint2D))) return (1);
   if (file.Read ((byte *)&MaxPos,1,sizeof(FPoint2D))) return (1);
   if (WarpingTable::ReadFile (file)) return (1);
   return (DONE);
}

 void ForwardWarpingTable::WarpRectangularPatch (int sx1,int sy1,int sx2,int sy2)

{
   int i,x,y;
  
   int ex = SrcImgWidth  - 2;
   int ey = SrcImgHeight - 2;
   FP2DArray1D m_points(4);
   m_points[0]((float)sx1,(float)sy1);
   m_points[1]((float)sx2,(float)sy1);
   m_points[2]((float)sx2,(float)sy2);
   m_points[3]((float)sx1,(float)sy2);
   FP2DArray1D d_points(4);
   for (i = 0; i < m_points.Length; i++)
      d_points[i] = ForwardWarpingFunction ((int)m_points[i].X,(int)m_points[i].Y) - MinPos;
   FPoint2D min_p,max_p;
   IPoint2D min_i,max_i;
   FindMinMax (d_points,min_p,max_p,min_i,max_i);
   int dx1 = (int)min_p.X;
   int dy1 = (int)min_p.Y;
   int dx2 = (int)max_p.X;
   int dy2 = (int)max_p.Y;
   if (dx2 >= Width ) dx2 = Width  - 1;
   if (dy2 >= Height) dy2 = Height - 1;
   Matrix mat_IH(3,3);
   GetHomography (d_points,m_points,mat_IH);
   double tx1 = mat_IH[0][0] * dx1 + mat_IH[0][1] * dy1 + mat_IH[0][2];
   double ty1 = mat_IH[1][0] * dx1 + mat_IH[1][1] * dy1 + mat_IH[1][2];
   double tz1 = mat_IH[2][0] * dx1 + mat_IH[2][1] * dy1 + mat_IH[2][2];
   for (y = dy1; y <= dy2; y++) {
      double tx2 = tx1, ty2 = ty1, tz2 = tz1;
      for (x = dx1; x <= dx2; x++) {
         float fx = (float)(tx2 / tz2);
         float fy = (float)(ty2 / tz2);
         int   ix = (int)fx;
         int   iy = (int)fy;
         if (0.0f <= fx && ix <= ex && 0.0f <= fy && iy <= ey) {
            float rx2 = fx - ix;
            float ry2 = fy - iy;
            float rx1 = 1.0f - rx2;
            float ry1 = 1.0f - ry2;
            Array[y][x].Coord(fx,fy);
            float *w = Array[y][x].Weights;
            w[0] = rx1 * ry1;
            w[1] = rx1 * ry2;
            w[2] = rx2 * ry1;
            w[3] = rx2 * ry2; 
         }
         else Array[y][x].Coord.X = -1.0f;
         tx2 += mat_IH[0][0];
         ty2 += mat_IH[1][0];
         tz2 += mat_IH[2][0];
      }
      tx1 += mat_IH[0][1];
      ty1 += mat_IH[1][1];
      tz1 += mat_IH[2][1];
   }
}

 int ForwardWarpingTable::WriteFile (FileIO &file)

{
   if (file.Write ((byte *)&ValidRegion,1,sizeof(IBox2D))) return (1);
   if (file.Write ((byte *)&MinPos,1,sizeof(FPoint2D))) return (1);
   if (file.Write ((byte *)&MaxPos,1,sizeof(FPoint2D))) return (1);
   if (WarpingTable::WriteFile (file)) return (1);
   return (DONE);
}

////////////////////////////////////////////////////////////////////////////////
//
//  Class: LensDistortionCorrectionTable
//
////////////////////////////////////////////////////////////////////////////////

 int LensDistortionCorrectionTable::Create (int si_width,int si_height,int step_size,FPoint2D &c_pos,double kappa1,double kappa2)

{
   Kappa1      = kappa1;
   Kappa2      = kappa2;
   ImageCenter = c_pos;
   return (ForwardWarpingTable::Create (si_width,si_height,step_size));
}

 FPoint2D LensDistortionCorrectionTable::ForwardWarpingFunction (int x,int y)

{
    FPoint2D pt((float)x,(float)y);
   return (GetUndistortedPixelPosition (pt,ImageCenter,Kappa1,Kappa2));
}

 int LensDistortionCorrectionTable::ReadFile (const char *file_name)

{
   FileIO file(file_name,FIO_BINARY,FIO_READ);
   if (!file) return (1);
   if (file.Read ((byte *)&Kappa1,1,sizeof(double))) return (2);
   if (file.Read ((byte *)&Kappa2,1,sizeof(double))) return (2);
   if (file.Read ((byte *)&ImageCenter,1,sizeof(FPoint2D))) return (2);
   if (ForwardWarpingTable::ReadFile (file)) return (2);
   return (DONE);
}

 int LensDistortionCorrectionTable::WriteFile (const char *file_name)

{
   FileIO file(file_name,FIO_BINARY,FIO_CREATE);
   if (!file) return (1);
   if (file.Write ((byte *)&Kappa1,1,sizeof(double))) return (2);
   if (file.Write ((byte *)&Kappa2,1,sizeof(double))) return (2);
   if (file.Write ((byte *)&ImageCenter,1,sizeof(FPoint2D))) return (2);
   if (ForwardWarpingTable::WriteFile (file)) return (2);
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
//  Functions
//
///////////////////////////////////////////////////////////////////////////////

 void Rotate (GImage& s_image,float cx,float cy,float angle,GImage& d_image,int flag_normalize)
 // (cx,cy): Rotation center.
 // angle  : Rotation angle in degree.
 // d_image: Created inside the function if flag_normalize = TRUE.
{
   Matrix mat_H(3,3);
   GetHomographyForRotation (cx,cy,angle,mat_H);
   Warp (s_image,mat_H,d_image,flag_normalize);
}

 void Shift (GImage& s_image,FPoint2D& shift,GImage& d_image)

{
   int x2,y2;
   
   int dx = (int)ceil(shift.X);
   int dy = (int)ceil(shift.Y);
   int si_width1  = s_image.Width  - 1;
   int si_height1 = s_image.Height - 1;
   int si_width2  = s_image.GetLineLength (   );
   int di_width2  = s_image.GetLineLength (   );
   int sx2 = dx;
   int sy2 = dy;
   int ex2 = si_width1  + dx;
   int ey2 = si_height1 + dy;
   if (sx2 < 0) sx2 = 0;
   if (sy2 < 0) sy2 = 0;
   if (ex2 > si_width1 ) ex2 = si_width1;
   if (ey2 > si_height1) ey2 = si_height1;
   int sx12 = sx2 - dx;
   int sx22 = sx2;
   ushort rx1 = (ushort)(256 * (1.0f - (dx - shift.X)));
   ushort rx2 = 256 - rx1;
   ushort ry1 = (ushort)(256 * (1.0f - (dy - shift.Y)));
   ushort ry2 = 256 - ry1;
   byte* si_buffer = s_image;
   byte* di_buffer = d_image;
   for (y2 = sy2; y2 < ey2; y2++) {
      int y1 = y2 - dy;
      byte *sp1 = si_buffer + si_width2 * y1 + sx12;
      byte *sp2 = sp1 + si_width2;
      byte *dp  = di_buffer + di_width2 * y2 + sx22;
      for (x2 = sx2; x2 < ex2; x2++) {
         ushort p1 = (rx1 * sp1[0] + rx2 * sp1[1]) >> 8;
         ushort p2 = (rx1 * sp2[0] + rx2 * sp2[1]) >> 8;
         *dp       = (ry1 * p1 + ry2 * p2) >> 8;
         sp1++, sp2++, dp++;
      }
   }
}

 void Shift_YUY2 (byte *si_buffer,int si_width,int si_height,FPoint2D &shift,byte *di_buffer)
 
{
   int x2,y2;
   
   int dx = (int)ceil(shift.X);
   int dy = (int)ceil(shift.Y);
   int si_width1  = si_width  - 1;
   int si_height1 = si_height - 1;
   int si_width2  = si_width * 2;
   int sx2 = dx;
   int sy2 = dy;
   int ex2 = si_width1  + dx;
   int ey2 = si_height1 + dy;
   if (sx2 < 0) sx2 = 0;
   if (sy2 < 0) sy2 = 0;
   if (ex2 > si_width1 ) ex2 = si_width1;
   if (ey2 > si_height1) ey2 = si_height1;
   int sx12 = 2 * (sx2 - dx);
   int sx22 = 2 * sx2;
   ushort rx1 = (ushort)(256 * (1.0f - (dx - shift.X)));
   ushort rx2 = 256 - rx1;
   ushort ry1 = (ushort)(256 * (1.0f - (dy - shift.Y)));
   ushort ry2 = 256 - ry1;
   for (y2 = sy2; y2 < ey2; y2++) {
      int y1 = y2 - dy;
      byte *sp1 = si_buffer + si_width2 * y1 + sx12;
      byte *sp2 = sp1 + si_width2;
      byte *dp  = di_buffer + si_width2 * y2 + sx22;
      for (x2 = sx2; x2 < ex2; x2++) {
         int x1 = x2 - dx;
         ushort p1 = (rx1 * sp1[0] + rx2 * sp1[2]) >> 8;
         ushort p2 = (rx1 * sp2[0] + rx2 * sp2[2]) >> 8;
         dp[0]     = (ry1 * p1 + ry2 * p2) >> 8;
         if (x2 & 0x0001) {
            if (x1 & 0x0001) dp[1] = sp1[1];
            else dp[1] = sp1[-1];
         }
         else {
            if (x1 & 0x0001) dp[1] = sp1[-1];
            else dp[1] = sp1[1];
         }
         sp1 += 2, sp2 += 2, dp += 2;
      }
   }
}

 void Warp (GImage &s_image,Matrix &mat_IH,FP2DArray1D &m_points,GImage &d_image)
// mat_IH  : Inverse of a 3 x 3 projective transformation matrix (homography).
// m_points: Transformed positions of arbitrary four points on s_image.
{
   int i,j,n,y;
   int cx1,cy1,ix[4];
   byte** a_channel;
   PolygonEdge  edges[4];
   PolygonEdge* l_edge = NULL;
   PolygonEdge* e_table[4] = { NULL,NULL,NULL,NULL };

   IP2DArray1D m_points2(5);
   for (i = 0; i < 4; i++) {
      m_points2[i].X = (int)m_points[i].X;
      m_points2[i].Y = (int)m_points[i].Y;
   }
   m_points2[4] = m_points2[0];
   Matrix vec_p1(3),vec_p2(3);
   vec_p1(2) = 1.0;
   int sex = s_image.Width  - 1;
   int sey = s_image.Height - 1;
   int dex = d_image.Width  - 1;
   int dey = d_image.Height - 1;
   int min_y = m_points2[0].Y;
   int max_y = min_y;
   for (i = 0; i < 4; i++) {
      int x1 = m_points2[i].X;
      int y1 = m_points2[i].Y;
      int x2 = m_points2[i + 1].X;
      int y2 = m_points2[i + 1].Y;
      if (min_y > y1) min_y = y1;
      else if (max_y < y1) max_y = y1;
      if (y1 == y2) {
         edges[i].MinY = y1;
         edges[i].D    = 0;
      }
      else {
         if (y1 < y2) {
            edges[i].MinY = y1;
            edges[i].MaxY = y2;
            edges[i].X    = x1;
            edges[i].D    = y2 - y1;
            n             = x2 - x1;
         }
         else {
            edges[i].MinY = y2;
            edges[i].MaxY = y1;
            edges[i].X    = x2;
            edges[i].D    = y1 - y2;
            n             = x1 - x2;
         }
         edges[i].A = n / edges[i].D;
         edges[i].B = n - edges[i].A * edges[i].D;
         edges[i].I = 0;
         if (n < 0) {
            edges[i].B = -edges[i].B;
            edges[i].F = -1;
         }
         else if (n > 0) edges[i].F = 1;
         else edges[i].F = 0;
      }
   }
   qsort (edges,4,sizeof(PolygonEdge),Compare_PolygonEdge_Ascending);
   for (i = j = 0; i < 4;   ) {
      if (edges[i].D) {
         if (e_table[j] == NULL) {
            e_table[j] = edges + i;
            l_edge = edges + i;
            l_edge->Prev = l_edge->Next = NULL;
         }
         else {
            if (l_edge->MinY == edges[i].MinY) {
               edges[i].Prev = l_edge;
               edges[i].Next = l_edge->Next;
               l_edge->Next  = edges + i;
               l_edge        = edges + i;
            }
            else {
               j++;
               continue;
            }
         }
      }
      i++;
   }
   PolygonEdge* ae_table = e_table[0];
   ae_table->Prev = NULL;
   float dx = (float)mat_IH[0][0];
   float dy = (float)mat_IH[1][0];
   float dz = (float)mat_IH[2][0];
   if (s_image.AlphaChannel == NULL) a_channel = NULL;
   else a_channel = *s_image.AlphaChannel;
   for (y = min_y,j = 1; y <= max_y; y++) {
      PolygonEdge *c_edge = ae_table;
      byte* l_d_image = d_image[y];
      for (n = 0;  ;  ) {
         ix[n++] = c_edge->X;
         if (c_edge->Next == NULL) break;
         else c_edge = c_edge->Next;
      }
      qsort (ix,n,sizeof(int),Compare_Int_Ascending);
      n--;
      if (0 <= y && y <= dey) {
         int flag_fill = 0;
         for (i = 0; i < n; i++) {
            flag_fill = 1 - flag_fill;
            int x1 = ix[i];
            int x2 = ix[i + 1];
            if (flag_fill) {
               if (x1 <= dex && x2 >= 0) {
                  if (x1 <   0) x1 =   0;
                  if (x2 > dex) x2 = dex;
                  vec_p1(0) = x1;
                  vec_p1(1) = y;
                  vec_p2 = mat_IH * vec_p1;
                  float tx = (float)vec_p2(0);
                  float ty = (float)vec_p2(1);
                  float tz = (float)vec_p2(2);
                  for (   ; x1 <= x2; x1++) {
                     float fx = tx / tz;
                     float fy = ty / tz;
                     int cx = (int)fx;
                     int cy = (int)fy;
                     if (fx >= 0.0f && fy >= 0.0f && cx <= sex && cy <= sey) {
                        if (cx == sex) cx1 = cx;
                        else cx1 = cx + 1;
                        if (cy == sey) cy1 = cy;
                        else cy1 = cy + 1;
                        float rx2 = fx - cx;
                        float ry2 = fy - cy;
                        float rx1 = 1.0f - rx2;
                        float ry1 = 1.0f - ry2;
                        float b1  = ry1 * s_image[cy][cx ] + ry2 * s_image[cy1][cx ];
                        float b2  = ry1 * s_image[cy][cx1] + ry2 * s_image[cy1][cx1];
                        byte p = (byte)(rx1 * b1 + rx2 * b2);
                        if (a_channel != NULL) {
                           b1 = ry1 * a_channel[cy][cx ] + ry2 * a_channel[cy1][cx ];
                           b2 = ry1 * a_channel[cy][cx1] + ry2 * a_channel[cy1][cx1];
                           float a = (rx1 * b1 + rx2 * b2) / 255.0f;
                           l_d_image[x1] = (byte)((1.0f - a) * l_d_image[x1] + a * p);
                        }
                        else l_d_image[x1] = p;
                     }
                     tx += dx, ty += dy, tz += dz;
                  }
               }
            }
         }
      }
      if (e_table[j] != NULL) {
         if (y == e_table[j]->MinY) {
            l_edge = ae_table;
            while (TRUE) {
               if (l_edge->Next == NULL) break;
               else l_edge = l_edge->Next;
            }
            e_table[j]->Prev = l_edge;
            l_edge->Next = e_table[j];
            j++;
         }
      }
      c_edge = ae_table;
      while (TRUE) {
         if (c_edge->MaxY == y) {
            if (c_edge->Next == NULL) {
               if (c_edge->Prev == NULL) {
                  ae_table = NULL;
                  break;
               }
               else c_edge->Prev->Next = NULL;
            }
            else if (c_edge->Prev == NULL) {
               ae_table = c_edge->Next;
               ae_table->Prev = NULL;
            }
            else {
               c_edge->Prev->Next = c_edge->Next;
               c_edge->Next->Prev = c_edge->Prev;
            }
         }
         if (c_edge->Next == NULL) break;
         else c_edge = c_edge->Next;
      }
      c_edge = ae_table;
      if (c_edge == NULL) break;
      while (TRUE) {
         c_edge->X += c_edge->A;
         c_edge->I += c_edge->B;
         if (c_edge->I > c_edge->D) {
            c_edge->X += c_edge->F;
            c_edge->I -= c_edge->D;
         }
         if (c_edge->Next == NULL) break;
         else c_edge = c_edge->Next;
      }
   }
}

 void Warp (BGRImage &s_image,Matrix &mat_IH,FP2DArray1D &m_points,BGRImage &d_image)

{
   int i,j,n,y;
   int cx1,cy1,ix[4];
   byte** a_channel;
   PolygonEdge  edges[4];
   PolygonEdge* l_edge = NULL;
   PolygonEdge* e_table[4] = { NULL,NULL,NULL,NULL };

   IP2DArray1D m_points2(5);
   for (i = 0; i < 4; i++) {
      m_points2[i].X = (int)m_points[i].X;
      m_points2[i].Y = (int)m_points[i].Y;
   }
   m_points2[4] = m_points2[0];
   Matrix vec_p1(3),vec_p2(3);
   vec_p1(2) = 1.0;
   int sex = s_image.Width  - 1;
   int sey = s_image.Height - 1;
   int dex = d_image.Width  - 1;
   int dey = d_image.Height - 1;
   int min_y = m_points2[0].Y;
   int max_y = min_y;
   for (i = 0; i < 4; i++) {
      int x1 = m_points2[i].X;
      int y1 = m_points2[i].Y;
      int x2 = m_points2[i + 1].X;
      int y2 = m_points2[i + 1].Y;
      if (min_y > y1) min_y = y1;
      else if (max_y < y1) max_y = y1;
      if (y1 == y2) {
         edges[i].MinY = y1;
         edges[i].D     = 0;
      }
      else {
         if (y1 < y2) {
            edges[i].MinY = y1;
            edges[i].MaxY = y2;
            edges[i].X    = x1;
            edges[i].D    = y2 - y1;
            n             = x2 - x1;
         }
         else {
            edges[i].MinY = y2;
            edges[i].MaxY = y1;
            edges[i].X    = x2;
            edges[i].D    = y1 - y2;
            n             = x1 - x2;
         }
         edges[i].A = n / edges[i].D;
         edges[i].B = n - edges[i].A * edges[i].D;
         edges[i].I = 0;
         if (n < 0) {
            edges[i].B = -edges[i].B;
            edges[i].F = -1;
         }
         else if (n > 0) edges[i].F = 1;
         else edges[i].F = 0;
      }
   }
   qsort (edges,4,sizeof(PolygonEdge),Compare_PolygonEdge_Ascending);
   for (i = j = 0; i < 4;   ) {
      if (edges[i].D) {
         if (e_table[j] == NULL) {
            e_table[j] = edges + i;
            l_edge = edges + i;
            l_edge->Prev = l_edge->Next = NULL;
         }
         else {
            if (l_edge->MinY == edges[i].MinY) {
               edges[i].Prev = l_edge;
               edges[i].Next = l_edge->Next;
               l_edge->Next  = edges + i;
               l_edge        = edges + i;
            }
            else {
               j++;
               continue;
            }
         }
      }
      i++;
   }
   PolygonEdge *ae_table = e_table[0];
   ae_table->Prev = NULL;
   float dx = (float)mat_IH[0][0];
   float dy = (float)mat_IH[1][0];
   float dz = (float)mat_IH[2][0];
   if (s_image.AlphaChannel == NULL) a_channel = NULL;
   else a_channel = *s_image.AlphaChannel;
   for (y = min_y,j = 1; y <= max_y; y++) {
      PolygonEdge *c_edge = ae_table;
      BGRPixel * l_d_image = d_image[y];
      for (n = 0;   ;   ) {
         ix[n++] = c_edge->X;
         if (c_edge->Next == NULL) break;
         else c_edge = c_edge->Next;
      }
      qsort (ix,n,sizeof(int),Compare_Int_Ascending);
      n--;
      if (0 <= y && y <= dey) {
         int flag_fill = 0;
         for (i = 0; i < n; i++) {
            flag_fill = 1 - flag_fill;
            int x1 = ix[i];
            int x2 = ix[i + 1];
            if (flag_fill) {
               if (x1 <= dex && x2 >= 0) {
                  if (x1 <   0) x1 =   0;
                  if (x2 > dex) x2 = dex;
                  vec_p1(0) = x1;
                  vec_p1(1) = y;
                  vec_p2 = mat_IH * vec_p1;
                  float tx = (float)vec_p2(0);
                  float ty = (float)vec_p2(1);
                  float tz = (float)vec_p2(2);
                  for (   ; x1 <= x2; x1++) {
                     float fx = tx / tz;
                     float fy = ty / tz;
                     int cx = (int)fx;
                     int cy = (int)fy;
                     if (fx >= 0.0f && fy >= 0.0f && cx <= sex && cy <= sey) {
                        if (cx == sex) cx1 = cx;
                        else cx1 = cx + 1;
                        if (cy == sey) cy1 = cy;
                        else cy1 = cy + 1;
                        float rx2 = fx - cx;
                        float ry2 = fy - cy;
                        float rx1 = 1.0f - rx2;
                        float ry1 = 1.0f - ry2;
                        BGRPixel *sp00 = &s_image[cy ][cx ];
                        BGRPixel *sp10 = &s_image[cy1][cx ];
                        BGRPixel *sp01 = &s_image[cy ][cx1];
                        BGRPixel *sp11 = &s_image[cy1][cx1];
                        BGRPixel *dp   = l_d_image + x1;
                        float b1 = ry1 * sp00->R + ry2 * sp10->R;
                        float b2 = ry1 * sp01->R + ry2 * sp11->R;
                        byte  pr = (byte)(rx1 * b1 + rx2 * b2);
                        b1 = ry1 * sp00->G + ry2 * sp10->G;
                        b2 = ry1 * sp01->G + ry2 * sp11->G;
                        byte  pg = (byte)(rx1 * b1 + rx2 * b2);
                        b1 = ry1 * sp00->B + ry2 * sp10->B;
                        b2 = ry1 * sp01->B + ry2 * sp11->B;
                        byte  pb = (byte)(rx1 * b1 + rx2 * b2);
                        if (a_channel != NULL) {
                           b1 = ry1 * a_channel[cy][cx ] + ry2 * a_channel[cy1][cx ];
                           b2 = ry1 * a_channel[cy][cx1] + ry2 * a_channel[cy1][cx1];
                           float a1 = (rx1 * b1 + rx2 * b2) / 255.0f;
                           float a2 = 1.0f - a1;
                           dp->R = (byte)(a2 * d_image[y][x1].R + a1 * pr);
                           dp->G = (byte)(a2 * d_image[y][x1].R + a1 * pg);
                           dp->B = (byte)(a2 * d_image[y][x1].R + a1 * pb);
                        }
                        else {
                           dp->R = pr;
                           dp->G = pg;
                           dp->B = pb;
                        }
                     }
                     tx += dx, ty += dy, tz += dz;
                  }
               }
            }
         }
      }
      if (e_table[j] != NULL) {
         if (y == e_table[j]->MinY) {
            l_edge = ae_table;
            while (TRUE) {
               if (l_edge->Next == NULL) break;
               else l_edge = l_edge->Next;
            }
            e_table[j]->Prev = l_edge;
            l_edge->Next = e_table[j];
            j++;
         }
      }
      c_edge = ae_table;
      while (TRUE) {
         if (c_edge->MaxY == y) {
            if (c_edge->Next == NULL) {
               if (c_edge->Prev == NULL) {
                  ae_table = NULL;
                  break;
               }
               else c_edge->Prev->Next = NULL;
            }
            else if (c_edge->Prev == NULL) {
               ae_table = c_edge->Next;
               ae_table->Prev = NULL;
            }
            else {
               c_edge->Prev->Next = c_edge->Next;
               c_edge->Next->Prev = c_edge->Prev;
            }
         }
         if (c_edge->Next == NULL) break;
         else c_edge = c_edge->Next;
      }
      c_edge = ae_table;
      if (c_edge == NULL) break;
      while (TRUE) {
         c_edge->X += c_edge->A;
         c_edge->I += c_edge->B;
         if (c_edge->I > c_edge->D) {
            c_edge->X += c_edge->F;
            c_edge->I -= c_edge->D;
         }
         if (c_edge->Next == NULL) break;
         else c_edge = c_edge->Next;
      }
   }
}

 void Warp_YUY2 (byte *si_buffer,int si_width,int si_height,Matrix &mat_IH,FP2DArray1D &m_points,byte *di_buffer,int di_width,int di_height)

{
   int i,j,n,y;
   int cx2,cy2,ix[4];
   PolygonEdge  edges[4];
   PolygonEdge* l_edge = NULL;
   PolygonEdge* e_table[4] = { NULL,NULL,NULL,NULL };

   IP2DArray1D m_points2(5);
   for (i = 0; i < 4; i++) {
      m_points2[i].X = (int)m_points[i].X;
      m_points2[i].Y = (int)m_points[i].Y;
   }
   m_points2[4] = m_points2[0];
   Matrix vec_p1(3),vec_p2(3);
   vec_p1(2) = 1.0;
   int sex = si_width  - 1;
   int sey = si_height - 1;
   int dex = di_width  - 1;
   int dey = di_height - 1;
   int min_y = m_points2[0].Y;
   int max_y = min_y;
   for (i = 0; i < 4; i++) {
      int x1 = m_points2[i].X;
      int y1 = m_points2[i].Y;
      int x2 = m_points2[i + 1].X;
      int y2 = m_points2[i + 1].Y;
      if (min_y > y1) min_y = y1;
      else if (max_y < y1) max_y = y1;
      if (y1 == y2) {
         edges[i].MinY = y1;
         edges[i].D    = 0;
      }
      else {
         if (y1 < y2) {
            edges[i].MinY = y1;
            edges[i].MaxY = y2;
            edges[i].X    = x1;
            edges[i].D    = y2 - y1;
            n             = x2 - x1;
         }
         else {
            edges[i].MinY = y2;
            edges[i].MaxY = y1;
            edges[i].X    = x2;
            edges[i].D    = y1 - y2;
            n             = x1 - x2;
         }
         edges[i].A = n / edges[i].D;
         edges[i].B = n - edges[i].A * edges[i].D;
         edges[i].I = 0;
         if (n < 0) {
            edges[i].B = -edges[i].B;
            edges[i].F = -1;
         }
         else if (n > 0) edges[i].F = 1;
         else edges[i].F = 0;
      }
   }
   qsort (edges,4,sizeof(PolygonEdge),Compare_PolygonEdge_Ascending);
   for (i = j = 0; i < 4;   ) {
      if (edges[i].D) {
         if (e_table[j] == NULL) {
            e_table[j] = edges + i;
            l_edge = edges + i;
            l_edge->Prev = l_edge->Next = NULL;
         }
         else {
            if (l_edge->MinY == edges[i].MinY) {
               edges[i].Prev = l_edge;
               edges[i].Next = l_edge->Next;
               l_edge->Next  = edges + i;
               l_edge        = edges + i;
            }
            else {
               j++;
               continue;
            }
         }
      }
      i++;
   }
   PolygonEdge *ae_table = e_table[0];
   ae_table->Prev = NULL;
   float dx = (float)mat_IH[0][0];
   float dy = (float)mat_IH[1][0];
   float dz = (float)mat_IH[2][0];
   int si_width2 = si_width * 2;
   int di_width2 = di_width * 2;
   for (y = min_y,j = 1; y <= max_y; y++) {
      PolygonEdge *c_edge = ae_table;
      byte *l_di_buffer = di_buffer + di_width2 * y;
      for (n = 0;   ;   ) {
         ix[n++] = c_edge->X;
         if (c_edge->Next == NULL) break;
         else c_edge = c_edge->Next;
      }
      qsort (ix,n,sizeof(int),Compare_Int_Ascending);
      n--;
      if (0 <= y && y <= dey) {
         int flag_fill = 0;
         for (i = 0; i < n; i++) {
            flag_fill = 1 - flag_fill;
            int x1 = ix[i];
            int x2 = ix[i + 1];
            if (flag_fill) {
               if (x1 <= dex && x2 >= 0) {
                  if (x1 <   0) x1 =   0;
                  if (x2 > dex) x2 = dex;
                  vec_p1(0) = x1;
                  vec_p1(1) = y;
                  vec_p2 = mat_IH * vec_p1;
                  float tx = (float)vec_p2(0);
                  float ty = (float)vec_p2(1);
                  float tz = (float)vec_p2(2);
                  for (   ; x1 <= x2; x1++) {
                     float fx = tx / tz;
                     float fy = ty / tz;
                     int cx1 = (int)fx;
                     int cy1 = (int)fy;
                     if (fx >= 0.0f && fy >= 0.0f && cx1 <= sex && cy1 <= sey) {
                        if (cx1 == sex) cx2 = cx1;
                        else cx2 = cx1 + 1;
                        if (cy1 == sey) cy2 = cy1;
                        else cy2 = cy1 + 1;
                        int cx12 = cx1 * 2;
                        int cx22 = cx2 * 2;
                        float rx2 = fx - cx1;
                        float ry2 = fy - cy1;
                        float rx1 = 1.0f - rx2;
                        float ry1 = 1.0f - ry2;
                        byte *l_si_buffer1 = si_buffer + si_width2 * cy1;
                        byte *l_si_buffer2 = si_buffer + si_width2 * cy2;
                        float b1 = ry1 * l_si_buffer1[cx12] + ry2 * l_si_buffer2[cx12];
                        float b2 = ry1 * l_si_buffer1[cx22] + ry2 * l_si_buffer2[cx22];
                        int x12 = x1 * 2;
                        l_di_buffer[x12] = (byte)(rx1 * b1 + rx2 * b2);
                        if (x1 & 0x0001) { // 타겟 위치의 X 좌표가 홀수인 경우: V
                           if (cx1 & 0x0001) { // 소스 위치의 X 좌표가 홀수인 경우
                              l_di_buffer[x12 + 1] = l_si_buffer1[cx12 + 1];
                           }
                           else { //소스 위치의 X 좌표가 짝수인 경우
                              l_di_buffer[x12 + 1] = l_si_buffer1[cx12 - 1];
                           }
                        }
                        else { // 타겟 위치의 X 좌표가 짝수인 경우: U
                           if (cx1 & 0x0001) { // 소스 위치의 X 좌표가 홀수인 경우
                              l_di_buffer[x12 + 1] = l_si_buffer1[cx12 - 1];
                           }
                           else {
                              l_di_buffer[x12 + 1] = l_si_buffer1[cx12 + 1];
                           }
                        }
                     }
                     tx += dx, ty += dy, tz += dz;
                  }
               }
            }
         }
      }
      if (e_table[j] != NULL) {
         if (y == e_table[j]->MinY) {
            l_edge = ae_table;
            while (TRUE) {
               if (l_edge->Next == NULL) break;
               else l_edge = l_edge->Next;
            }
            e_table[j]->Prev = l_edge;
            l_edge->Next = e_table[j];
            j++;
         }
      }
      c_edge = ae_table;
      while (TRUE) {
         if (c_edge->MaxY == y) {
            if (c_edge->Next == NULL) {
               if (c_edge->Prev == NULL) {
                  ae_table = NULL;
                  break;
               }
               else c_edge->Prev->Next = NULL;
            }
            else if (c_edge->Prev == NULL) {
               ae_table = c_edge->Next;
               ae_table->Prev = NULL;
            }
            else {
               c_edge->Prev->Next = c_edge->Next;
               c_edge->Next->Prev = c_edge->Prev;
            }
         }
         if (c_edge->Next == NULL) break;
         else c_edge = c_edge->Next;
      }
      c_edge = ae_table;
      if (c_edge == NULL) break;
      while (TRUE) {
         c_edge->X += c_edge->A;
         c_edge->I += c_edge->B;
         if (c_edge->I > c_edge->D) {
            c_edge->X += c_edge->F;
            c_edge->I -= c_edge->D;
         }
         if (c_edge->Next == NULL) break;
         else c_edge = c_edge->Next;
      }
   }
}

 void Warp (GImage &s_image,Matrix &mat_H,GImage &d_image,int flag_normalize)
// mat_H  : 3 x 3 projective transformation matrix (homography).
// d_image: Created inside the function if flag_normalize == TRUE.
{
   int i;

   Matrix mat_IH(3,3);
   FP2DArray1D m_points(4);
   GetTransformedPositions (s_image.Width,s_image.Height,mat_H,m_points);
   if (flag_normalize) {
      FPoint2D min,max;
      IPoint2D min_i,max_i;
      FindMinMax (m_points,min,max,min_i,max_i);
      for (i = 0; i < 4; i++) {
         m_points[i].X -= min.X;
         m_points[i].Y -= min.Y;
      }
      d_image.Create ((int)(max.X - min.X + 1.5f),(int)(max.Y - min.Y + 1.5f));
      Matrix mat_T = IM(3);
      mat_T[0][2] = -min.X;
      mat_T[1][2] = -min.Y;
      mat_IH = Inv(mat_T * mat_H);
   }
   else mat_IH = Inv(mat_H);
   Warp (s_image,mat_IH,m_points,d_image);
}

 void Warp (BGRImage &s_image,Matrix &mat_H,BGRImage &d_image,int flag_normalize)

{
   int i;

   Matrix mat_IH(3,3);
   FP2DArray1D m_points(4);
   GetTransformedPositions (s_image.Width,s_image.Height,mat_H,m_points);
   if (flag_normalize) {
      FPoint2D min,max;
      IPoint2D min_i,max_i;
      FindMinMax (m_points,min,max,min_i,max_i);
      for (i = 0; i < 4; i++) {
         m_points[i].X -= min.X;
         m_points[i].Y -= min.Y;
      }
      d_image.Create ((int)(max.X - min.X + 1.5f),(int)(max.Y - min.Y + 1.5f));
      Matrix mat_T = IM(3);
      mat_T[0][2] = -min.X;
      mat_T[1][2] = -min.Y;
      mat_IH = Inv(mat_T * mat_H);
   }
   else mat_IH = Inv(mat_H);
   Warp (s_image,mat_IH,m_points,d_image);
}

 void Warp_YUY2 (byte *si_buffer,int si_width,int si_height,Matrix &mat_H,byte *di_buffer,int di_width,int di_height)
 
{
   FP2DArray1D m_points(4);
   GetTransformedPositions (si_width,si_height,mat_H,m_points);
   Matrix mat_IH = Inv(mat_H);
   Warp_YUY2 (si_buffer,si_width,si_height,mat_IH,m_points,di_buffer,di_width,di_height);
} 

 void Warp (GImage &s_image,FP2DArray1D &m_points,GImage &d_image,int flag_normalize)

{
   FP2DArray1D m_points1(4);
   m_points1[0](0.0f,0.0f);
   m_points1[1]((float)(s_image.Width - 1),0.0f);
   m_points1[2](m_points1[1].X,(float)(s_image.Height - 1));
   m_points1[3](0.0f,m_points1[2].Y);
   Matrix mat_H(3,3);
   GetHomography (m_points1,m_points,mat_H);
   Warp (s_image,mat_H,d_image,flag_normalize);
}

 void Warp (BGRImage &s_image,FP2DArray1D &m_points,BGRImage &d_image,int flag_normalize)

{
   FP2DArray1D m_points1(4);
   m_points1[0](0.0f,0.0f);
   m_points1[1]((float)(s_image.Width - 1),0.0f);
   m_points1[2](m_points1[1].X,(float)(s_image.Height - 1));
   m_points1[3](0.0f,m_points1[2].Y);
   Matrix mat_H(3,3);
   GetHomography (m_points1,m_points,mat_H);
   Warp (s_image,mat_H,d_image,flag_normalize);
}

 void Warp (GImage &s_image,FP2DArray1D &m_points1,FP2DArray1D &m_points2,GImage &d_image,int flag_normalize)

{
   Matrix mat_H(3,3);
   GetHomography (m_points1,m_points2,mat_H);
   Warp (s_image,mat_H,d_image,flag_normalize);
}

 void Warp (BGRImage &s_image,FP2DArray1D &m_points1,FP2DArray1D &m_points2,BGRImage &d_image,int flag_normalize)

{
   Matrix mat_H(3,3);
   GetHomography (m_points1,m_points2,mat_H);
   Warp (s_image,mat_H,d_image,flag_normalize);
}

}

#include "vacl_binimg.h"
#include "vacl_event.h"
#include "vacl_filter.h"
#include "vacl_fire.h"
#include "vacl_geometry.h"
#include "vacl_morph.h"

#if defined(__DEBUG_FLD)
#include "Win32CL/Win32CL.h"
extern int _DVX_FLD,_DVY_FLD;
extern CImageView* _DebugView_FLD;
#endif
#if defined(__DEBUG_SMD)
#include "Win32CL/Win32CL.h"
extern int _DVX_SMD,_DVY_SMD;
extern CImageView* _DebugView_SMD;
#endif

 namespace VACL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Class: FlameDetection
//
///////////////////////////////////////////////////////////////////////////////

#define TB_STATUS_NONFLAME   0x10000000

 FlameDetection::FlameDetection (   )
 
{
   BlobTracker.Options             = 0;
   BlobTracker.MinBlobArea         = 25;
   BlobTracker.MinCount_Tracked    = 3;
   BlobTracker.MaxTime_Undetected1 = 0.5f;
   BlobTracker.MaxTime_Undetected2 = 0.5f;
   BlobTracker.MinMovingDistance1  = 2.0f;
   BlobTracker.MinMovingDistance2  = 0.0f;
   BlobTracker.RegionUpdateRate    = 1.0f;

   MinBlobArea         = 49;
   MinCount_Tracked    = 3;
   MaxTime_Undetected1 = 0.5f;
   MaxTime_Undetected2 = 3.0f;
   MaxTime_Verified    = 10.0f;
   MinMovingDistance1  = 0.0f;
   MinMovingDistance2  = 0.0f;
   RegionUpdateRate    = 1.0f;

   FDPeriod = 2.0f;
}

 FlameDetection::~FlameDetection (   )
 
{
   Close (   );
}

 void FlameDetection::Close (   )
 
{
   BlobTracking::Close  (   );
   VFQueue.Delete       (   );
   BlobTracker.Close    (   );
   CntArray.Delete      (   );
   SumArray.Delete      (   );
   Frames.Delete        (   );
   FlickerImage.Delete  (   );
   FlameImage.Delete    (   );
}

 void FlameDetection::DetectFlickering (GImage& s_image,GImage& fg_image,GImage& d_image)
 
{
   int i,x,y;
   
   int n = FrameCount % Frames.Depth;
   float dc = 0.5f,fd = 0.4f;                // PARAMETERS
   if (Flag_NightMode) dc = 0.3f,fd = 0.25f; // PARAMETERS
   int dc_thrsld = (int)(dc * Frames.Depth);
   int fd_thrsld = (int)(fd * Frames.Depth);
   if (FrameCount) {
      if (!n) {
         d_image.Clear (   );
         for (y = 0; y < d_image.Height; y++) {
            byte* l_CntArray = CntArray[y];
            int*  l_SumArray = SumArray[y];
            byte* l_d_image  = d_image[y];
            for (x = 0; x < d_image.Width; x++) {
               if (l_CntArray[x] > dc_thrsld) {
                  int  thrsld = l_SumArray[x] / Frames.Depth;
                  int  zcc    = 0;
                  int  flag   = FALSE;
                  if (Frames[0][y][x] >= thrsld) flag = TRUE;
                  for (i = 1; i < Frames.Depth; i++) {
                     int p = (int)Frames[i][y][x];
                     if (flag && p < thrsld) {
                        zcc++;
                        flag = FALSE;
                     }
                     else if (!flag && p > thrsld) {
                        zcc++;
                        flag = TRUE;
                     }
                  }
                  if (zcc >= fd_thrsld) l_d_image[x] = PG_WHITE;
               }
            }
         }
         CntArray.Clear (   );
         SumArray.Clear (   );
         if (!Flag_NightMode) d_image &= fg_image;
      }
   }
   else {
      CntArray.Clear (   );
      SumArray.Clear (   );
      d_image.Clear  (   );
   }
   byte** frame = Frames[n];
   for (y = 0; y < fg_image.Height; y++) {
      byte* l_s_image  = s_image[y];
      byte* l_fg_image = fg_image[y];
      byte* l_frame    = frame[y];
      byte* l_CntArray = CntArray[y];
      int*  l_SumArray = SumArray[y];
      for (x = 0; x < fg_image.Width; x++) {
         l_frame[x]     = l_s_image[x];
         l_SumArray[x] += l_s_image[x];
         if (l_fg_image[x]) l_CntArray[x]++;
      }
   }
}

 void FlameDetection::GetInitFlameImage (ForegroundDetection& frg_detector,GImage &d_image)
 
{
   d_image = frg_detector.IntFrgImage2 & frg_detector.DetAreaMap;
   d_image.SetBoundary (1,PG_BLACK);
}

 int FlameDetection::Initialize (ISize2D& si_size,float frm_rate)
 
{
   if (BlobTracking::Initialize (si_size,frm_rate)) return (1);
   BlobTracker.Initialize (si_size,frm_rate);
   CntArray.Create (si_size.Width,si_size.Height);
   SumArray.Create (si_size.Width,si_size.Height);
   int n_frames = (int)(FDPeriod * frm_rate);
   Frames.Create (si_size.Width,si_size.Height,n_frames);
   FlickerImage.Create (si_size.Width,si_size.Height);
   FlameImage.Create (si_size.Width,si_size.Height);
   Reset (   );
   return (DONE);
}

 void FlameDetection::MarkMotionPixels (GImage& s_image,int d_thrsld,GImage& d_image)

{
   int x,y;

   int max_len = 2;
   VFData* vf_data = VFQueue.Get (1);
   if (vf_data != NULL) {
      GImage& p_image = vf_data->Image;
      for (y = 0; y < s_image.Height; y++) {
         byte* l_s_image = s_image[y];
         byte* l_p_image = p_image[y];
         byte* l_d_image = d_image[y];
         for (x = 0; x < s_image.Width; x++) {
            if (l_d_image[x]) {
               int d = abs ((int)l_s_image[x] - l_p_image[x]);
               if (d > d_thrsld) l_d_image[x] = 128;
            }
         }
      }
   }
   vf_data = new VFData;
   vf_data->Image = s_image;
   VFQueue.Add (vf_data,LL_FIRST);
   if (VFQueue.GetNumItems (   ) > max_len) {
      vf_data = VFQueue.Last (   );
      if (vf_data) {
         VFQueue.Remove (vf_data);
         delete vf_data;
      }
   }
}

 void FlameDetection::MarkNonFlameBlobs (GImage& m_image)

{
   int i,x,y;
   
   TrackedBlob* t_blob = BlobTracker.TrackedBlobs.First (   );
   while (t_blob != NULL) {
      if (t_blob->Status & TB_STATUS_NONFLAME) {
         t_blob->Time_Triggered += FrameDuration;
         int flag_reset = FALSE;
         if (t_blob->Time_Triggered >= 3.0f) flag_reset = TRUE; // PARAMETERS
         if (!flag_reset) {
            TrkBlobRegion* tb_rgn = t_blob->TrkBlobRegions.Get (2); // PARAMETERS
            if ((tb_rgn != NULL) && !(tb_rgn->Status & TB_STATUS_UNDETECTED)) {
               float av = (float)tb_rgn->Area / t_blob->Area;
               av = GetMaximum (av,1.0f / av);
               if (av >= 2.0f) flag_reset = TRUE; // PARAMETERS
            }
         }
         if (flag_reset) {
            t_blob->InitRegion.Set (*t_blob);
            t_blob->Status &= ~TB_STATUS_NONFLAME;
            t_blob->Time_Triggered = 0.0f;
         }
      }
      else if (!(t_blob->Status & TB_STATUS_UNDETECTED)) {
         if (!(t_blob->Status & TB_STATUS_NONFLAME)) {
            float mdt1 = 0.2f * (t_blob->InitRegion.Width + t_blob->InitRegion.Height); // PARAMETERS
            float mdt2 = 0.2f * (t_blob->Width + t_blob->Height);
            float md_thrsld = GetMaximum (mdt1,mdt2);
            float md1  = Mag(t_blob->GetCenterPosition (   ) - t_blob->InitRegion.GetCenterPosition (   ));
            float md2  = t_blob->MovingDistance2;
            if (md1 > md_thrsld && md2 > md_thrsld) t_blob->Status |= TB_STATUS_NONFLAME;
         }
         if (!(t_blob->Status & TB_STATUS_NONFLAME) && !Flag_NightMode) {
            IArray2D& br_map = BlobTracker.BlobRegionMap;
            int sx = t_blob->X;
            int sy = t_blob->Y;
            int ex = sx + t_blob->Width;
            int ey = sy + t_blob->Height;
            int id = t_blob->RgnID;
            int n  = 0;
            for (y = sy; y < ey; y++) {
               int*  l_br_map  = br_map[y];
               byte* l_m_image = m_image[y];
               for (x = sx; x < ex; x++) {
                  if (l_br_map[x] == id) {
                     if (l_m_image[x] == 128) n++;
                  }
               }
            }
            float r = (float)n / t_blob->Area;
            if (r < 0.05f) t_blob->Status |= TB_STATUS_NONFLAME; // PARAMETERS
         }
         if (!(t_blob->Status & TB_STATUS_NONFLAME)) {
            int n    = 5; // PARAMETERS
            int n_av = 0;
            TrkBlobRegion* tb_rgn = t_blob->TrkBlobRegions.First (   );
            for (i = 0; i < n; i++) {
               if (tb_rgn == NULL) break;
               if (!(tb_rgn->Status & TB_STATUS_UNDETECTED)) {
                  float av = (float)tb_rgn->UPArea / t_blob->UPArea;
                  av = GetMaximum (av,1.0f / av) - 1.0f;
                  if (av <= 0.08f) n_av++; // PARAMETERS
               }
               tb_rgn = t_blob->TrkBlobRegions.Next (tb_rgn);
            }
            if (n_av == n) t_blob->Status |= TB_STATUS_NONFLAME;
         }
      }
      t_blob = BlobTracker.TrackedBlobs.Next (t_blob);
   }
}

 int FlameDetection::Perform (ObjectTracking& obj_tracker)
 
{
   #if defined(__DEBUG_FLD)
   _DVX_FLD = _DVY_FLD = 0;
   _DebugView_FLD->Clear (   );
   #endif
   if (!IsEnabled (   )) return (1);
   if (!IsInitialized (   )) return (2);
   ForegroundDetection* frg_detector = obj_tracker.ForegroundDetector;
   if (frg_detector == NULL) return (3);
   if (frg_detector->SrcColorImage == NULL) return (4);
   Flag_NightMode = FALSE;
   if ((frg_detector->Status & FGD_STATUS_LOW_ILLUMINATION) ||
       (frg_detector->Status & FGD_STATUS_IR_MODE)) Flag_NightMode = TRUE;
   GImage& s_image    = frg_detector->SrcImage;
   BGRImage& s_cimage = *frg_detector->SrcColorImage;
   GImage t_image(s_image.Width,s_image.Height);
   GetInitFlameImage (*frg_detector,t_image);
   #if defined(__DEBUG_FLD)
   _DebugView_FLD->DrawImage (t_image,_DVX_FLD,_DVY_FLD);
   _DVY_FLD += SrcImgSize.Height;
   _DebugView_FLD->DrawText (_T("Initial Flame Image"),_DVX_FLD,_DVY_FLD,PC_GREEN);
   _DVY_FLD += 20;
   #endif
   RemoveNonFireColoredPixels (s_cimage,t_image);
   PerformBinClosing (t_image,3,t_image); // PARAMETERS
   #if defined(__DEBUG_FLD)
   _DebugView_FLD->DrawImage (t_image,_DVX_FLD,_DVY_FLD);
   _DVY_FLD += SrcImgSize.Height;
   _DebugView_FLD->DrawText (_T("Non-fire-colored Pixel Removal Result"),_DVX_FLD,_DVY_FLD,PC_GREEN);
   _DVY_FLD += 20;
   #endif
   BlobTracker.Perform (t_image);
   MarkMotionPixels  (s_image,10,t_image); // PARAMETERS
   MarkNonFlameBlobs (t_image);
   #if defined(__DEBUG_FLD)
   _DebugView_FLD->DrawImage (t_image,_DVX_FLD,_DVY_FLD);
   CString text;
   TrackedBlob* t_blob = BlobTracker.TrackedBlobs.First (   );
   while (t_blob != NULL) {
      if (!(t_blob->Status & TB_STATUS_TO_BE_REMOVED) && (t_blob->Status & TB_STATUS_VERIFIED)) {
         int x = _DVX_FLD + t_blob->X;
         int y = _DVY_FLD + t_blob->Y;
         COLORREF color = PC_GREEN;
         if (t_blob->Status & TB_STATUS_NONFLAME) color = PC_BLUE;
         _DebugView_FLD->DrawRectangle (x,y,t_blob->Width,t_blob->Height,color);
         x += t_blob->Width + 2;
         text.Format (_T("ID:%d"),t_blob->ID);
         _DebugView_FLD->DrawText (text,x,y,PC_YELLOW,TRANSPARENT,FT_ARIAL,14);
         y += 15;
      }
      t_blob = BlobTracker.TrackedBlobs.Next (t_blob);
   }
   _DVY_FLD += SrcImgSize.Height;
   _DebugView_FLD->DrawText (_T("Blob Tracking Result"),_DVX_FLD,_DVY_FLD,PC_GREEN);
   _DVY_FLD += 20;
   #endif
   RemoveNonFlameRegions (t_image);
   #if defined(__DEBUG_FLD)
   _DebugView_FLD->DrawImage (t_image,_DVX_FLD,_DVY_FLD);
   _DVY_FLD += SrcImgSize.Height;
   _DebugView_FLD->DrawText (_T("Non-flame Region Removal Result"),_DVX_FLD,_DVY_FLD,PC_GREEN);
   _DVY_FLD += 20;
   #endif
   DetectFlickering (s_image,t_image,FlickerImage);
   #if defined(__DEBUG_FLD)
   _DebugView_FLD->DrawImage (FlickerImage,_DVX_FLD,_DVY_FLD);
   _DVY_FLD += SrcImgSize.Height;
   _DebugView_FLD->DrawText (_T("Flickering Detection Result"),_DVX_FLD,_DVY_FLD,PC_GREEN);
   _DVY_FLD += 20;
   #endif
   RemoveCRs (FlickerImage,0,3,FlameImage); // PARAMETERS
   int se_radius = (int)(6.0f * s_cimage.Height / 240.0f + 0.5f); // PARAMETERS
   if (se_radius < 1) se_radius = 1;
   PerformBinClosing (FlameImage,se_radius,FlameImage);
   #if defined(__DEBUG_FLD)
   _DebugView_FLD->DrawImage (FlameImage,_DVX_FLD,_DVY_FLD);
   _DVY_FLD += SrcImgSize.Height;
   _DebugView_FLD->DrawText (_T("Flame Detection Result"),_DVX_FLD,_DVY_FLD,PC_GREEN);
   _DVY_FLD += 20;
   #endif
   BlobTracking::Perform (FlameImage);
   #if defined(__DEBUG_FLD)
   _DebugView_FLD->Invalidate (   );
   #endif
   return (DONE);
}

 void FlameDetection::RemoveNonFireColoredPixels (BGRImage& s_cimage,GImage& d_image)
 
{
   int x,y;

   byte i_thrsld = 255;
   if (Flag_NightMode) i_thrsld = 200; // PARAMETERS
   for (y = 0; y < s_cimage.Height; y++) {
      BGRPixel* sp    = s_cimage[y];
      byte* l_d_image = d_image[y];
      for (x = 0; x < s_cimage.Width; x++) {
         if (l_d_image[x]) {
            byte m = (byte)(((short)sp->R + sp->G + sp->B) / 3);
            if (m <= i_thrsld) {
               if (!( (sp->R > 150)               &&  // PARAMETERS
                      ((short)sp->R - sp->G > 15) &&  // PARAMETERS
                      (sp->G >= sp->B)
                    )
                  ) l_d_image[x] = PG_BLACK;
            }
         }
         sp++;
      }
   }
}

 void FlameDetection::RemoveNonFlameRegions (GImage& d_image)

{
   TrackedBlob* t_blob = BlobTracker.TrackedBlobs.First (   );
   while (t_blob != NULL) {
      if (t_blob->Status & TB_STATUS_NONFLAME) {
         CopyCR (BlobTracker.BlobRegionMap,*t_blob,t_blob->RgnID,d_image,PG_BLACK);
      }
      t_blob = TrackedBlobs.Next (t_blob);
   }
}

 void FlameDetection::Reset (   )
 
{
   BlobTracking::Reset (   );
   FlameImage.Clear (   );
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: BlobTracking_Smoke
//
///////////////////////////////////////////////////////////////////////////////

 BlobTracking_Smoke::BlobTracking_Smoke (   )

{
   MaxNorSpeed         = 200.0f;
   MinSpeed            = 1.0f;
   MinTime_Tracked     = 0.2f;

   MinBlobArea         = 0;
   MinCount_Tracked    = 2;
   MaxBRMLLength       = MinTime_Tracked + 0.2f;
   MaxTime_Undetected1 = 0.5f;
   MaxTime_Undetected2 = 0.5f;
   MaxTime_Verified    = MC_VLV;
   MinMovingDistance1  = 3.0f;
   MinMovingDistance2  = 0.0f;
}

 int BlobTracking_Smoke::FilterTrackedBlob (TrackedBlob* t_blob)

{
   if (!(t_blob->Status & TB_STATUS_VALIDATED)) return (FALSE);
   if (t_blob->Speed   <= MinSpeed   ) return (TRUE);
   if (t_blob->NorSpeed > MaxNorSpeed) return (TRUE);
   return (FALSE);
}

 int BlobTracking_Smoke::VerifyTrackedBlob (TrackedBlob* t_blob)

{
   if (t_blob->Time_Tracked < MinTime_Tracked) return (1);
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: SmokeDetection
//
///////////////////////////////////////////////////////////////////////////////

#define SMD_PARAM_MIN_BLOB_AREA            64
#define SMD_PARAM_CLOSING_FILTER_RADIUS    3
#define SMD_PARAM_DILATION_FILTER_RADIUS   3

 SmokeDetection::SmokeDetection (   )
 
{
   // Input Parameters (BlobTracking)
   MinBlobArea         = 100;
   MinCount_Tracked    = 2;
   MaxTime_Undetected1 = 0.5f;
   MaxTime_Undetected2 = 1.0f;
   MaxTime_Verified    = MC_VLV;
   MinMovingDistance1  = 0.0f;
   MinMovingDistance2  = 0.0f;

   // Input Parameters (SmokeDetection)
   SmokeColor       = SMD_COLOR_BLACK_OR_WHITE;
   MinArea          = 100;
   MDThreshold      = 10;
   MinWSI           = 180;
   MaxBndStrength   = 0.45f;
   MaxSaturation    = 0.35f;
   MinMotion        = 0.25f;
   TSThreshold      = 0.5f;
   MaxTransparency1 = 0.8f;
   MaxTransparency2 = 0.6f;
   SRDThreshold     = 0.5f;
   MinHRT           = 0.25f;
   HRDThreshold     = 0.6f;
   SDCThreshold     = 0.5f;
}

 SmokeDetection::~SmokeDetection (   )
 
{
   Close (   );
}

 void SmokeDetection::Close (   )
 
{
   BlobTracking::Close  (   );
   VFQueue.Delete        (   );
   SDCArray.Delete      (   );
   BlobTracker.Close    (   );
   IntSmokeImage.Delete (   );
   SmokeImage.Delete    (   );
}

 int SmokeDetection::CheckSetup (SmokeDetection* sd)

{
   int ret_flag = 0;

   FileIO buffThis,buffFrom;   
   CXMLIO xmlIOThis,xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis,XMLIO_Write);
   xmlIOFrom.SetFileIO (&buffFrom,XMLIO_Write);
   WriteFile (&xmlIOThis);
   sd->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom))
      ret_flag |= CHECK_SETUP_RESULT_CHANGED | CHECK_SETUP_RESULT_RESTART;
   buffThis.SetLength (0);
   buffFrom.SetLength (0);
   return (ret_flag);
};

 void SmokeDetection::DetectSmokeColoredPixels (BGRImage& s_cimage,GImage& sm_image,GImage& mc_image)
 
{
   int x,y;
   
   mc_image.Clear (   );
   float st_thrsld = 1.0f - MaxSaturation;
   for (y = 0; y < s_cimage.Height; y++) {
      BGRPixel* l_s_cimage = s_cimage[y];
      byte*     l_sm_image = sm_image[y];
      byte*     l_mc_image = mc_image[y];
      for (x = 0; x < s_cimage.Width; x++) {
         if (l_sm_image[x]) {
            BGRPixel& sp = l_s_cimage[x];
            byte min_c = GetMinimum (GetMinimum (sp.R,sp.G),sp.B);
            byte max_c = GetMaximum (GetMaximum (sp.R,sp.G),sp.B);
            float st = (float)min_c / max_c;
            if (st < st_thrsld) l_sm_image[x] = PG_BLACK;
            l_mc_image[x] = max_c;
         }
      }
   }
}

 void SmokeDetection::GetFinalSmokeImage (GImage& d_image)

{
   int x,y;
   
   GImage m_image1(d_image.Width,d_image.Height);
   m_image1.Clear (   );
   TrackedBlob* t_blob = BlobTracker.TrackedBlobs.First (   );
   while (t_blob != NULL) {
      if (!(t_blob->Status & TB_STATUS_TO_BE_REMOVED)) {
         if (t_blob->Status & TB_STATUS_VERIFIED_START) SetSDCArray (t_blob);
         if (t_blob->Status & TB_STATUS_VERIFIED)
            CopyCR (BlobTracker.BlobRegionMap,*(IBox2D*)t_blob,t_blob->RgnID,m_image1,PG_WHITE);
      }
      t_blob = BlobTracker.TrackedBlobs.Next (t_blob);
   }
   GImage m_image2(d_image.Width,d_image.Height);
   PerformBinDilation (m_image1,SMD_PARAM_DILATION_FILTER_RADIUS,m_image2);
   int sdc_thrsld = (int)(SDCThreshold * FrameRate + 0.5f);
   int max_sdc    = 3 * sdc_thrsld; // PARAMETERS
   for (y = 0; y < m_image2.Height; y++) {
      byte* l_m_image   = m_image2[y];
      byte* l_d_image   = d_image[y];
      int*  l_sdc_array = SDCArray[y];
      for (x = 0; x < m_image2.Width; x++) {
         int& sdc = l_sdc_array[x];
         if (l_m_image[x]) {
            sdc++;
            if (sdc > max_sdc) sdc = max_sdc;
         }
         else {
            sdc--;
            if (sdc < 0) sdc = 0;
         }
         if (sdc >= sdc_thrsld) l_d_image[x] = PG_WHITE;
         else l_d_image[x] = PG_BLACK;
      }
   }
}

 int SmokeDetection::GetInitSmokeImage (ForegroundDetection& frg_detector,GImage& d_image)
 
{
   int i,x,y;
   
   d_image.Clear (   );
   IArray2D& rg_map              = frg_detector.LDR.RegionMap;
   Array1D<LDR_Region>& rg_array = frg_detector.LDR.Regions;
   if (!rg_map || !rg_array) return (1);
   for (i = 1; i < rg_array.Length; i++) {
      LDR_Region& rgn = rg_array[i];
      if (rgn.Type == LDR_RGNTYPE_LIGHT || rgn.Type == LDR_RGNTYPE_SHADOW) {
         if (rgn.Area > SMD_PARAM_MIN_BLOB_AREA) {
            if (rgn.Flag_Remove && rgn.Score1 < MaxBndStrength) {
               CopyCR (rg_map,*(IBox2D*)&rgn,i,d_image,PG_WHITE);
            }
            else if (rgn.Type == LDR_RGNTYPE_LIGHT && rgn.Score2 < MaxBndStrength) {
               CopyCR (rg_map,*(IBox2D*)&rgn,i,d_image,PG_WHITE);
            }
         }
      }
   }
   GImage& da_map = frg_detector.DetAreaMap;
   for (y = 0; y < da_map.Height; y++) {
      byte* l_da_map  = da_map[y];
      byte* l_d_image = d_image[y];
      for (x = 0; x < da_map.Width; x++)
         if (!l_da_map[x]) l_d_image[x] = PG_BLACK;
   }
   return (DONE);
}

 void SmokeDetection::GetRegionInfo (GImage& s_image,GImage& g_image,GImage& bg_image,IArray2D& cr_map,CRArray1D& cr_array,BArray1D& rc_array,BArray1D& rt_array)

{
   int i,x,y;

   rc_array.Clear (   );
   rt_array.Clear (   );
   for (i = 1; i < cr_array.Length; i++) {
      int n     = 0;
      int sum_s = 0;
      int sum_g = 0;
      int sum_b = 0;
      int sx = cr_array[i].X;
      int sy = cr_array[i].Y;
      int ex = sx + cr_array[i].Width;
      int ey = sy + cr_array[i].Height;
      for (y = sy; y < ey; y++) {
         int*  l_cr_map   = cr_map[y];
         byte* l_s_image  = s_image[y];
         byte* l_g_image  = g_image[y];
         byte* l_bg_image = bg_image[y];
         for (x = sx; x < ex; x++) {
            if (l_cr_map[x] == i) {
               sum_s += l_s_image[x];
               sum_b += l_bg_image[x];
               if (l_g_image[x]) {
                  sum_g += l_g_image[x];
                  n++;
               }
            }
         }
      }
      if (sum_s > sum_b) rt_array[i] = TRUE;
      if (n) rc_array[i] = (byte)(sum_g / n);
   }
}

 int SmokeDetection::Initialize (ISize2D& si_size,float frm_rate)
 
{
   if (BlobTracking::Initialize (si_size,frm_rate)) return (1);
   SDCArray.Create        (si_size.Width,si_size.Height);
   IntSmokeImage.Create   (si_size.Width,si_size.Height);
   SmokeImage.Create      (si_size.Width,si_size.Height);
   BlobTracker.Initialize (si_size,frm_rate);
   Reset (   );
   return (DONE);
}

 void SmokeDetection::MarkNonsmokeRegions_Area (CRArray1D& cr_array,BArray1D& mk_array)

{
   int i;
   
   for (i = 1; i < cr_array.Length; i++) {
      if (!mk_array[i]) {
         if (cr_array[i].Area < MinArea) mk_array[i] = TRUE;
      }
   }   
}

 void SmokeDetection::MarkNonsmokeRegions_Color (BArray1D& rc_array,BArray1D& mk_array)

{
   int i;

   for (i = 1; i < rc_array.Length; i++) {
      if (!mk_array[i]) {
         switch (SmokeColor) {
            case SMD_COLOR_BLACK_ONLY:
               if ((int)rc_array[i] >= MinWSI) mk_array[i] = TRUE;
               break;
            case SMD_COLOR_WHITE_ONLY:
               if ((int)rc_array[i] < MinWSI ) mk_array[i] = TRUE;
               break;
            case SMD_COLOR_BLACK_OR_WHITE:
            default:
               break;
         }
      }
   }
}

 void SmokeDetection::MarkNonsmokeRegions_Headlight (GImage& s_image,GImage& bg_image,IArray2D& cr_map,CRArray1D& cr_array,BArray1D& rt_array,FArray1D& tp_array,BArray1D& mk_array,FArray1D& sc_array)
 
{
   int i,x,y;
   
   sc_array.Clear (   );
   #if defined(__DEBUG_SMD)
   GImage t_image = SmokeImage;
   #endif
   GImage s_image1(s_image.Width,s_image.Height);
   GaussianFiltering_3x3 (s_image,s_image1);
   for (i = 1; i < cr_array.Length; i++) {
      if (!mk_array[i] && rt_array[i] && tp_array[i] > MinHRT) {
         int n  = 0;
         int sx = cr_array[i].X;
         int sy = cr_array[i].Y;
         int ex = sx + cr_array[i].Width;
         int ey = sy + cr_array[i].Height;
         for (y = sy; y < ey; y++) {
            int*  l_cr_map    = cr_map[y];
            byte* l_s_image   = s_image1[y];
            byte* l_s_image1  = s_image1[y - 1];
            byte* l_s_image2  = s_image1[y + 1];
            byte* l_bg_image  = bg_image[y];
            byte* l_bg_image1 = bg_image[y - 1];
            byte* l_bg_image2 = bg_image[y + 1];
            #if defined(__DEBUG_SMD)
            byte* l_t_image   = t_image[y];
            #endif
            for (x = sx; x < ex; x++) {
               if (l_cr_map[x] == i) {
                  int sp = (int)l_s_image[x];
                  int bp = (int)l_bg_image[x];
                  int ds = abs(sp - l_s_image[x - 1] ) + abs(sp - l_s_image[x + 1] ) + abs(sp - l_s_image1[x] ) + abs(sp - l_s_image2[x] );
                  int db = abs(bp - l_bg_image[x - 1]) + abs(bp - l_bg_image[x + 1]) + abs(bp - l_bg_image1[x]) + abs(bp - l_bg_image2[x]);
                  if (ds >= db) {
                     // 배경에 비해 고주파 성분이 강화된 픽셀들의 수를 센다.
                     // 일반적으로 백색 연기 영역에서는 고주파 성분이 약화되는 경향이 있는 반면 헤드라이트 영역에서는 그렇지 않다.
                     n++; 
                     #if defined(__DEBUG_SMD)
                     l_t_image[x] = 128;
                     #endif
                  }
               }
            }
         }
         sc_array[i] = (float)n / cr_array[i].Area;
      }
   }
   #if defined(__DEBUG_SMD)
   _DebugView_SMD->DrawImage (t_image,_DVX_SMD,_DVY_SMD);
   #endif
   for (i = 1; i < cr_array.Length; i++) {
      if (!mk_array[i] && rt_array[i] && tp_array[i] > MinHRT) {
         if (sc_array[i] >= HRDThreshold) mk_array[i] = TRUE;
         #if defined(__DEBUG_SMD)
         CString text;
         text.Format (_T("%4.2f"),sc_array[i]);
         FPoint2D cp = cr_array[i].GetCenterPosition (   );
         _DebugView_SMD->DrawText (text,_DVX_SMD + (int)cp.X - 10,_DVY_SMD + (int)cp.Y - 10,PC_RED,TRANSPARENT,FT_ARIAL,14);
         #endif
      }
   }
   #if defined(__DEBUG_SMD)
   _DVY_SMD += t_image.Height;
   _DebugView_SMD->DrawText (_T("Headlight Region Detection Result"),_DVX_SMD,_DVY_SMD,PC_GREEN);
   _DVY_SMD += 20;
   #endif
}

 void SmokeDetection::MarkNonsmokeRegions_Motion (GImage& s_image,IArray2D& cr_map,CRArray1D& cr_array,BArray1D& mk_array,FArray1D& sc_array)

{
   int i,x,y;

   sc_array.Clear (   );
   #if defined(__DEBUG_SMD)
   GImage t_image = SmokeImage;
   #endif
   int max_len = 2;
   if (VFQueue.GetNumItems (   ) < max_len) {
      #if defined(__DEBUG_SMD)
      _DVY_SMD += s_image.Height;
      #endif
   }
   else {
      GImage& p_image1 = VFQueue.Get(0)->Image;
      GImage& p_image2 = VFQueue.Get(1)->Image;
      for (i = 1; i < cr_array.Length; i++) {
         if (!mk_array[i]) {
            int n = 0;
            int sx = cr_array[i].X;
            int sy = cr_array[i].Y;
            int ex = sx + cr_array[i].Width;
            int ey = sy + cr_array[i].Height;
            for (y = sy; y < ey; y++) {
               int*  l_cr_map   = cr_map[y];
               byte* l_s_image  = s_image[y];
               byte* l_p_image1 = p_image1[y];
               byte* l_p_image2 = p_image2[y];
               #if defined(__DEBUG_SMD)
               byte* l_t_image = t_image[y];
               #endif
               for (x = sx; x < ex; x++) {
                  if (l_cr_map[x] == i) {
                     int d1 = abs ((int)l_s_image[x] - l_p_image1[x]);
                     int d2 = abs ((int)l_s_image[x] - l_p_image2[x]);
                     int d3 = abs ((int)l_s_image[x] - l_p_image2[x]);
                     int sum_d = d1 + d2 + d3;
                     if (sum_d >= MDThreshold) {
                        n++;
                        #if defined(__DEBUG_SMD)
                        l_t_image[x] = 128;
                        #endif
                     }
                  }
               }
            }
            sc_array[i] = (float)n / cr_array[i].Area;
         }
      }
      #if defined(__DEBUG_SMD)
      _DebugView_SMD->DrawImage (t_image,_DVX_SMD,_DVY_SMD);
      #endif
      for (i = 1; i < cr_array.Length; i++) {
         if (!mk_array[i]) {
            if (sc_array[i] < MinMotion) mk_array[i] = TRUE;
            #if defined(__DEBUG_SMD)
            CString text;
            text.Format (_T("%4.2f"),sc_array[i]);
            FPoint2D cp = cr_array[i].GetCenterPosition (   );
            _DebugView_SMD->DrawText (text,_DVX_SMD + (int)cp.X - 10,_DVY_SMD + (int)cp.Y - 10,PC_RED,TRANSPARENT,FT_ARIAL,14);
            #endif
         }
      }
      #if defined(__DEBUG_SMD)
      _DVY_SMD += t_image.Height;
      #endif
   }
   VFData* vf_data = new VFData;
   vf_data->Image  = s_image;
   VFQueue.Add (vf_data,LL_FIRST);
   if (VFQueue.GetNumItems (   ) > max_len) {
      vf_data = VFQueue.Last (   );
      if (vf_data) {
         VFQueue.Remove (vf_data);
         delete vf_data;
      }
   }
   #if defined(__DEBUG_SMD)
   _DebugView_SMD->DrawText (_T("Motion Detection Result"),_DVX_SMD,_DVY_SMD,PC_GREEN);
   _DVY_SMD += 20;
   #endif
}

 void SmokeDetection::MarkNonsmokeRegions_Shadow (ObjectTracking& obj_tracker,IArray2D& cr_map,CRArray1D& cr_array,BArray1D& rt_array,BArray1D& mk_array)

{
   int i,x,y;

   GImage m_image(obj_tracker.SrcImgSize.Width,obj_tracker.SrcImgSize.Height);
   m_image.Clear (   );
   // 이동 객체들의 그림자 예상 영역에 마킹한다.
   TrackedObject* t_object = obj_tracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED) && (t_object->Status & TO_STATUS_VERIFIED)) {
         IBox2D s_rgn;
         s_rgn.X      = t_object->X;
         s_rgn.Y      = t_object->Y;
         s_rgn.Width  = t_object->Width;
         s_rgn.Height = t_object->Height;
         CropRegion  (obj_tracker.SrcImgSize,s_rgn);
         m_image.Set (s_rgn,PG_WHITE);
         int w  = (int)(0.5f * GetMinimum (t_object->Width,t_object->Height));
         int h1 = (int)(0.6f * t_object->Height);
         int h2 = (int)(0.4f * t_object->Height);
         s_rgn.X      = t_object->X - w;
         s_rgn.Y      = t_object->Y + t_object->Height - h1;
         s_rgn.Width  = t_object->Width + 2 * w;
         s_rgn.Height = h1 + h2;
         CropRegion  (obj_tracker.SrcImgSize,s_rgn);
         m_image.Set (s_rgn,PG_WHITE);
      }
      t_object = obj_tracker.TrackedObjects.Next (t_object);
   }
   // 아직 삭제가 안되었고 배경보다 어두운 영역에 대해 그림자 영역인지 판별한다.
   // 그림자 영역으로 판별되면 제거한다.
   #if defined(__DEBUG_SMD)
   FArray1D ar_array(cr_array.Length);
   ar_array.Clear (   );
   #endif
   for (i = 1; i < cr_array.Length; i++) {
      if (!mk_array[i] && !rt_array[i]) {
         int n = 0;
         ConnectedRegion& cr = cr_array[i];
         int ex = cr.X + cr.Width;
         int ey = cr.Y + cr.Height;
         for (y = cr.Y; y < ey; y++) {
            int*  l_cr_map  = cr_map[y];
            byte* l_m_image = m_image[y];
            for (x = cr.X; x < ex; x++) {
               if (l_cr_map[x] == i) {
                  if (l_m_image[x]) n++;
                  #if defined(__DEBUG_SMD)
                  l_m_image[x] = 128;
                  #endif
               }
            }
         }
         float ar = (float)n / cr.Area;
         #if defined(__DEBUG_SMD)
         ar_array[i] = ar;
         #endif
         if (ar > SRDThreshold) mk_array[i] = TRUE;
      }
   }
   #if defined(__DEBUG_SMD)
   _DebugView_SMD->DrawImage (m_image,_DVX_SMD,_DVY_SMD);
   for (i = 1; i < cr_array.Length; i++) {
      if (ar_array[i] > 0.0f) {
         CString text;
         text.Format (_T("%.2f"),ar_array[i]);
         FPoint2D cp = cr_array[i].GetCenterPosition (   );
         _DebugView_SMD->DrawText (text,_DVX_SMD + (int)cp.X - 10,_DVY_SMD + (int)cp.Y - 10,PC_RED,TRANSPARENT,FT_ARIAL,14);
      }
   }
   _DVY_SMD += m_image.Height;
   _DebugView_SMD->DrawText (_T("Shadow Region Detection Result"),_DVX_SMD,_DVY_SMD,PC_GREEN);
   _DVY_SMD += 20;
   #endif
}

 void SmokeDetection::MarkNonsmokeRegions_Transparency (GImage& s_image,GImage& bg_image,GImage& sm_image,IArray2D& cr_map,CRArray1D& cr_array,BArray1D& rt_array,BArray1D& mk_array,FArray1D& sc_array)

{
   int i,x,y;

   sc_array.Clear (   );
   #if defined(__DEBUG_SMD)
   GImage t_image = SmokeImage;
   #endif
   PerformLightDisturbanceReduction_TS (s_image,bg_image,TSThreshold,sm_image);
   #if defined(__DEBUG_SMD)
   for (y = 0; y < sm_image.Height; y++) {
      byte* l_sm_image = sm_image[y];
      byte* l_t_image  = t_image[y];
      for (x = 0; x < sm_image.Width; x++) {
         if (l_t_image[x] && !l_sm_image[x]) l_t_image[x] = 128;
      }
   }
   _DebugView_SMD->DrawImage (t_image,_DVX_SMD,_DVY_SMD);
   #endif
   for (i = 1; i < cr_array.Length; i++) {
      ConnectedRegion& cr = cr_array[i];
      int n = 0;
      int ex = cr.X + cr.Width;
      int ey = cr.Y + cr.Height;
      for (y = cr.Y; y < ey; y++) {
         int*  l_cr_map   = cr_map[y];
         byte* l_sm_image = sm_image[y];
         for (x = cr.X; x < ex; x++) {
            if (l_cr_map[x] == i && !l_sm_image[x]) n++;
         }
      }
      int area = cr_array[i].Area;
      float score = (float)n / area;
      sc_array[i] = score;
      if (rt_array[i]) {
         if (score > MaxTransparency1) mk_array[i] = TRUE;
      }
      else { // 그림자일 가능성이 높은 경우
         if (score > MaxTransparency2) mk_array[i] = TRUE;
      }
      #if defined(__DEBUG_SMD)
      CString text;
      text.Format (_T("%4.2f"),score);
      FPoint2D cp = cr_array[i].GetCenterPosition (   );
      _DebugView_SMD->DrawText (text,_DVX_SMD + (int)cp.X - 10,_DVY_SMD + (int)cp.Y - 10,PC_RED,TRANSPARENT,FT_ARIAL,14);
      #endif
   }
   #if defined(__DEBUG_SMD)
   _DVY_SMD += SmokeImage.Height;
   _DebugView_SMD->DrawText (_T("Transparency Measure Result"),_DVX_SMD,_DVY_SMD,PC_GREEN);
   _DVY_SMD += 20;
   #endif
}

 int SmokeDetection::Perform (ObjectTracking& obj_tracker)
 
{ 
   #if defined(__DEBUG_SMD)
   _DVX_SMD = _DVY_SMD = 0;
   _DebugView_SMD->Clear (   );
   #endif
   if (!IsEnabled (   )) return (1);
   if (!IsInitialized (   )) return (2);
   ForegroundDetection* frg_detector = obj_tracker.ForegroundDetector;
   if (frg_detector == NULL) return (3);
   if (frg_detector->SrcColorImage == NULL) return (4);
   GImage& s_image    = frg_detector->SrcImage;
   GImage& bg_image   = frg_detector->BkgImage;
   BGRImage& s_cimage = *frg_detector->SrcColorImage;
   // 초기 연기 픽셀들을 검출한다.
   if (GetInitSmokeImage (*frg_detector,IntSmokeImage)) return (5);
   #if defined(__DEBUG_SMD)
   if (_DebugView_SMD) {
      _DebugView_SMD->DrawImage (IntSmokeImage,_DVX_SMD,_DVY_SMD);
      _DVY_SMD += SrcImgSize.Height;
      _DebugView_SMD->DrawText (_T("Initial Smoke Image"),_DVX_SMD,_DVY_SMD,PC_GREEN);
      _DVY_SMD += 20;
   }
   #endif
   // 전경 픽셀들을 제거한다.
   RemoveForegroundPixels (frg_detector->FrgEdgeImage3,0.015f,IntSmokeImage); // PARAMETERS
   #if defined(__DEBUG_SMD)
   if (_DebugView_SMD) {
      _DebugView_SMD->DrawImage (IntSmokeImage,_DVX_SMD,_DVY_SMD);
      _DVY_SMD += SrcImgSize.Height;
      _DebugView_SMD->DrawText (_T("Foreground Pixel Removal Result"),_DVX_SMD,_DVY_SMD,PC_GREEN);
      _DVY_SMD += 20;
   }
   #endif
   // 검출된 연기 픽셀의 채도 값을 체크하여 연기에 해당하지 않는 픽셀들을 제거한다.
   GImage g_image(s_cimage.Width,s_cimage.Height);
   DetectSmokeColoredPixels (s_cimage,IntSmokeImage,g_image);
   #if defined(__DEBUG_SMD)
   if (_DebugView_SMD) {
      _DebugView_SMD->DrawImage (IntSmokeImage,_DVX_SMD,_DVY_SMD);
      _DVY_SMD += SrcImgSize.Height;
      _DebugView_SMD->DrawText (_T("Nonsmoke Pixel Removal Result (Color)"),_DVX_SMD,_DVY_SMD,PC_GREEN);
      _DVY_SMD += 20;
   }
   #endif
   PerformBinClosing (IntSmokeImage,SMD_PARAM_CLOSING_FILTER_RADIUS,SmokeImage);
   // 영역 기반 프로세싱을 수행한다.
   IArray2D cr_map(SmokeImage.Width,SmokeImage.Height);
   int n_crs = LabelCRs (SmokeImage,cr_map);
   CRArray1D cr_array(n_crs + 1);
   GetCRInfo (cr_map,cr_array);
   BArray1D rc_array(cr_array.Length);
   BArray1D rt_array(cr_array.Length);
   GetRegionInfo (s_image,g_image,bg_image,cr_map,cr_array,rc_array,rt_array);
   BArray1D mk_array(cr_array.Length);
   mk_array.Clear (   );
   FArray1D mt_sc_array(cr_array.Length);
   FArray1D tp_sc_array(cr_array.Length);
   FArray1D hr_sc_array(cr_array.Length);
   IntSmokeImage = SmokeImage;
   MarkNonsmokeRegions_Area         (cr_array,mk_array);
   MarkNonsmokeRegions_Color        (rc_array,mk_array);
   MarkNonsmokeRegions_Motion       (s_image,cr_map,cr_array,mk_array,mt_sc_array);
   MarkNonsmokeRegions_Transparency (s_image,bg_image,IntSmokeImage,cr_map,cr_array,rt_array,mk_array,tp_sc_array);
   MarkNonsmokeRegions_Shadow       (obj_tracker,cr_map,cr_array,rt_array,mk_array);
   MarkNonsmokeRegions_Headlight    (s_image,bg_image,cr_map,cr_array,rt_array,tp_sc_array,mk_array,hr_sc_array);
   // 마킹한 영역들을 제거한다.
   n_crs = RemoveCRs (cr_map,mk_array);
   Binarize (cr_map,0,IntSmokeImage);
   BlobTracker.Perform (cr_map,n_crs);
   #if defined(__DEBUG_SMD)
   _DebugView_SMD->DrawImage (IntSmokeImage,_DVX_SMD,_DVY_SMD);
   TrackedBlob* t_blob = BlobTracker.TrackedBlobs.First (   );
   while (t_blob != NULL) {
      COLORREF color = PC_GREEN;
      if (!(t_blob->Status & TB_STATUS_VERIFIED))   color = PC_BLUE;
      if (t_blob->Status & TB_STATUS_TO_BE_REMOVED) color = PC_RED;
      CString text;
      int x = _DVX_SMD + t_blob->X;
      int y = _DVY_SMD + t_blob->Y;
      _DebugView_SMD->DrawRectangle (x,y,t_blob->Width,t_blob->Height,color);
      x += t_blob->Width  + 2;
      y += t_blob->Height - 20;
      text.Format (_T("ID:%d"),t_blob->ID);
      _DebugView_SMD->DrawText (text,x,y,PC_YELLOW,TRANSPARENT,FT_ARIAL,14);
      y += 11;
      text.Format (_T("SP:%4.1f"),t_blob->Speed);
      _DebugView_SMD->DrawText (text,x,y,PC_YELLOW,TRANSPARENT,FT_ARIAL,14);
      y += 11;
      text.Format (_T("NS:%5.1f"),t_blob->NorSpeed);
      _DebugView_SMD->DrawText (text,x,y,PC_YELLOW,TRANSPARENT,FT_ARIAL,14);
      y += 11;
      text.Format (_T("DO:%4.2f"),t_blob->Disorder);
      _DebugView_SMD->DrawText (text,x,y,PC_YELLOW,TRANSPARENT,FT_ARIAL,14);
      t_blob = BlobTracker.TrackedBlobs.Next (t_blob);
   }
   _DVY_SMD += SrcImgSize.Height;
   _DebugView_SMD->DrawText (_T("Blob Tracking Result"),_DVX_SMD,_DVY_SMD,PC_GREEN);
   _DVY_SMD += 20;
   #endif
   GetFinalSmokeImage (SmokeImage);
   BlobTracking::Perform (SmokeImage);
   #if defined(__DEBUG_SMD)
   _DebugView_SMD->DrawImage (SmokeImage,_DVX_SMD,_DVY_SMD);
   _DVY_SMD += SrcImgSize.Height;
   _DebugView_SMD->DrawText  (_T("Final Smoke Image"),_DVX_SMD,_DVY_SMD,PC_GREEN);
   _DVY_SMD += 20;
   #endif
   #if defined(__DEBUG_SMD)
   _DebugView_SMD->Invalidate (   );
   #endif
   return (DONE);
}

 int SmokeDetection::ReadFile (FileIO& file)
 
{
   if (file.Read ((byte*)&SmokeColor,1,sizeof(int))) return (1);
   if (file.Read ((byte*)&MinWSI    ,1,sizeof(int))) return (2);
   return (DONE);
}

 int SmokeDetection::ReadFile (CXMLIO* pIO)
 
{
   static SmokeDetection dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("SmokeDetection")) {
      xmlNode.Attribute (TYPE_ID(int),"SmokeColor",&SmokeColor,&dv.SmokeColor);
      xmlNode.Attribute (TYPE_ID(int),"MinWSI"    ,&MinWSI    ,&dv.MinWSI);
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}

 void SmokeDetection::RemoveForegroundPixels (GImage& fg_image,float ser_factor,GImage& d_image)

{
   int x,y;
   
   GImage t_image1(fg_image.Width,fg_image.Height);
   RemoveImpulsiveNoise (fg_image,0,t_image1);
   int se_radius = (int)(fg_image.Height * ser_factor + 0.5f);
   GImage t_image2(fg_image.Width,fg_image.Height);
   PerformBinDilation (t_image1,se_radius,t_image2);
   for (y = 0; y < d_image.Height; y++) {
      byte* l_t_image = t_image2[y];
      byte* l_d_image = d_image[y];
      for (x = 0; x < d_image.Width; x++) {
         if (l_t_image[x]) l_d_image[x] = PG_BLACK;
      }
   }
}

 void SmokeDetection::Reset (   )
 
{
   BlobTracking::Reset (   );
   BlobTracker.Reset   (   );
   VFQueue.Delete       (   );
   SDCArray.Clear      (   );
   IntSmokeImage.Clear (   );
   SmokeImage.Clear    (   );
}

 void SmokeDetection::SetSDCArray (TrackedBlob* t_blob)

{
   int x,y;

   TrkBlobRegion* tb_rgn = t_blob->TrkBlobRegions.First (   );
   BRMData* brm_data     = BRMList.First (   );
   while (tb_rgn != NULL && brm_data != NULL) {
      IArray2D& br_map = brm_data->BlobRegionMap;
      int lb = tb_rgn->RgnID;
      int sx = tb_rgn->X;
      int sy = tb_rgn->Y;
      int ex = sx + tb_rgn->Width;
      int ey = sy + tb_rgn->Height;
      for (y = sy; y < ey; y++) {
         int* l_br_map    = br_map[y];
         int* l_sdc_array = SDCArray[y];
         for (x = sx; x < ex; x++) {
            if (l_br_map[x] == lb) l_sdc_array[x]++;
         }
      }
      tb_rgn   = t_blob->TrkBlobRegions.Next (tb_rgn);
      brm_data = BRMList.Next (brm_data);
   }
}

 int SmokeDetection::VerifyTrackedBlob (TrackedBlob* t_blob)
 
{
   if (BlobTracking::VerifyTrackedBlob (t_blob) != DONE) return (1);
   return (DONE);
}

 int SmokeDetection::WriteFile (FileIO& file)
 
{
   if (file.Write ((byte*)&SmokeColor,1,sizeof(int))) return (1);
   if (file.Write ((byte*)&MinWSI    ,1,sizeof(int))) return (2);
   return (DONE);
}

 int SmokeDetection::WriteFile (CXMLIO* pIO)
 
{
   static SmokeDetection dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("SmokeDetection")) {
      xmlNode.Attribute (TYPE_ID(int),"SmokeColor",&SmokeColor,&dv.SmokeColor);
      xmlNode.Attribute (TYPE_ID(int),"MinWSI"    ,&MinWSI    ,&dv.MinWSI);
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}

}

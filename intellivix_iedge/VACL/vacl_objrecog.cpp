﻿#include "vacl_objrecog.h"
#include "vacl_event.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Definitions
//
///////////////////////////////////////////////////////////////////////////////

#define OR_PARAM_NUM_TRIES               7
#define OR_PARAM_PROC_PERIOD             0.5f
#define OR_PARAM_MAX_TIME_UNRECOGNIZED   3.0f
#define OR_PARAM_OUTER_ZONE_SIZE1        0.03f
#define OR_PARAM_OUTER_ZONE_SIZE2        0.15f
#define OR_PARAM_INFLATION_X             1.0f
#define OR_PARAM_INFLATION_Y             1.0f

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectRecognition
//
///////////////////////////////////////////////////////////////////////////////

 ObjectRecognition::ObjectRecognition (   )

{
   Flag_Enabled         = TRUE;
   Flag_Initialized     = FALSE;
   SrcFrmBuffer         = NULL;
   ObjectTracker        = NULL;

   NumTries             = OR_PARAM_NUM_TRIES;
   ProcPeriod           = OR_PARAM_PROC_PERIOD;
   MaxTime_Unrecognized = OR_PARAM_MAX_TIME_UNRECOGNIZED;
   OuterZoneSize1       = OR_PARAM_OUTER_ZONE_SIZE1;
   OuterZoneSize2       = OR_PARAM_OUTER_ZONE_SIZE2;
   Inflation(OR_PARAM_INFLATION_X,OR_PARAM_INFLATION_Y);
}

 ObjectRecognition::~ObjectRecognition (   )

{
   Close (   );
}

 void ObjectRecognition::Close (   )

{
   Flag_Initialized = FALSE;
}

 int ObjectRecognition::Initialize (   )

{
   if (!IsEnabled (   )) return (1);
   Close (   );
   return (DONE);
}

 int ObjectRecognition::Perform (ObjectTracking& obj_tracker)

{
   if (!IsEnabled     (   )) return (1);
   if (!IsInitialized (   )) return (2);
   ObjectTracker = &obj_tracker;
   ForegroundDetection* fgd = ObjectTracker->ForegroundDetector;
   if (fgd == NULL) return (3);
   SrcImgSize    = fgd->SrcImgSize;
   SrcFrmSize    = fgd->SrcFrmSize;
   FrameRate     = fgd->FrameRate;
   SrcFrmBuffer  = fgd->SrcFrmBuffer;
   FrameDuration = 1.0f / FrameRate;
   Scale.X       = (float)SrcFrmSize.Width  / SrcImgSize.Width;
   Scale.Y       = (float)SrcFrmSize.Height / SrcImgSize.Height;
   int e_code = DoPreprocessing (obj_tracker);
   if      (e_code > 0) return (3);
   else if (e_code < 0) return (DONE);
   TrackedObject* t_object = ObjectTracker->TrackedObjects.First (   );
   while (t_object != NULL) {
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED)) {
         if (UpdateLikelihood (t_object) == DONE) UpdateRecognitionResult (t_object);
      }
      t_object = ObjectTracker->TrackedObjects.Next (t_object);
   }
   return (DONE);
}

 int ObjectRecognition::UpdateLikelihood (TrackedObject* t_object)

{
   if (CheckObject (t_object) != DONE) return (1);
   int*   p_count = GetObjectCountVariable (t_object);
   float* p_time  = GetObjectTimeVariable  (t_object);
   if ((*p_count) >= NumTries) return (2);
   if (CheckObjectInOuterZone (SrcImgSize,OuterZoneSize1,OuterZoneSize2,t_object) == DONE &&
       t_object->Time_Tracked < MaxTime_Unrecognized) return (3);
   int r_code = DONE;
   if (!(*p_count) || (*p_time) >= ProcPeriod) {
      (*p_count)++;
      (*p_time) = 0.0f;
      RecognizeObject (t_object);
   }
   else r_code = 4;
   (*p_time) += FrameDuration;
   return (r_code);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectRecognitionServer
//
///////////////////////////////////////////////////////////////////////////////

 ObjectRecognitionServer::ObjectRecognitionServer (   )

{
   Flag_Initialized = FALSE;
   Events.Create (2);
}

 ObjectRecognitionServer::~ObjectRecognitionServer (   )

{
   Close (   );
}

 int ObjectRecognitionServer::AddRequest (ObjectTracking* obj_tracker)

{
   if (!IsThreadRunning (   )) return (1);
   Request* request = new Request;
   request->ObjectTracker = obj_tracker;
   CS_RequestQueue.Lock   (   );
   int e_code = RequestQueue.Add (request);
   CS_RequestQueue.Unlock (   );
   if (e_code) {
      delete request;
      return (2);
   }
   Events.SetEvent (1);
   return (DONE);
}

 void ObjectRecognitionServer::Close (   )

{
   Stop (   );
   ScopedLock lock(CS_RequestQueue);
   Request* request = RequestQueue.First (   );
   while (request != NULL) {
      request->ObjectTracker->Event_Processed.SetEvent (   );
      request = RequestQueue.Next (request);
   }
   RequestQueue.Delete (   );
   Flag_Initialized = FALSE;
}

 int ObjectRecognitionServer::Initialize (   )

{
   Close (   );
   return (DONE);
}

 void ObjectRecognitionServer::RemoveAllRequests (ObjectTracking* obj_tracker)

{
   ScopedLock lock(CS_RequestQueue);
   Request* c_request = RequestQueue.First (   );
   while (c_request != NULL) {
      Request* n_request = RequestQueue.Next (c_request);
      if (c_request->ObjectTracker == obj_tracker) {
         RequestQueue.Remove (c_request);
         delete c_request;
      }
      c_request = n_request;
   }
}

 int ObjectRecognitionServer::Start (   )

{
   if (!IsInitialized (   )) return (1);
   // 쓰레드를 구동하기 전에 이벤트들을 모두 리셋해야 한다.
   Events.ResetAllEvents (   );
   // 쓰레드를 구동시킨다.
   if (StartThread (   )) return (2);
   return (DONE);
}

 void ObjectRecognitionServer::Stop (   )

{
   // 쓰레드 내 메인 루프를 종료하기 위한 이벤트를 발생시킨다.
   Events.SetEvent (0);
   // 쓰레드가 종료할 때까지 기다려야 한다.
   StopThread (   );
}

 void ObjectRecognitionServer::ThreadProc (   )

{
   if (OnThreadStarted (   )) return;
   while (TRUE) {
      int evt_idx;
      int e_code = WaitForMultiEvents (Events,FALSE,evt_idx);
      if (e_code == DONE) {
         if (evt_idx == 0) break;
         while (TRUE) {
            CS_RequestQueue.Lock   (   );
            Request* request = RequestQueue.Retrieve (   );
            CS_RequestQueue.Unlock (   );
            if (request != NULL) {
               ProcessRequest (request->ObjectTracker);
               request->ObjectTracker->Event_Processed.SetEvent (   );
               delete request;
            }
            else break;
         }
      }
   }
   OnThreadEnded (   );
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectRecognitionClient
//
///////////////////////////////////////////////////////////////////////////////

 ObjectRecognitionClient::ObjectRecognitionClient (   )

{
   Flag_Enabled         = TRUE;
   Flag_Initialized     = FALSE;
   SrcFrmBuffer         = NULL;
   ObjectTracker        = NULL;
   Server               = NULL;

   NumTries             = OR_PARAM_NUM_TRIES;
   ProcPeriod           = OR_PARAM_PROC_PERIOD;
   MaxTime_Unrecognized = OR_PARAM_MAX_TIME_UNRECOGNIZED;
   OuterZoneSize1       = OR_PARAM_OUTER_ZONE_SIZE1;
   OuterZoneSize2       = OR_PARAM_OUTER_ZONE_SIZE2;
   Inflation(OR_PARAM_INFLATION_X,OR_PARAM_INFLATION_Y);
}

 ObjectRecognitionClient::~ObjectRecognitionClient (   )

{
   Close (   );
}

 void ObjectRecognitionClient::Close (   )

{
   Flag_Initialized = FALSE;
   ObjectTracker    = NULL;
   Server           = NULL;
}

 void ObjectRecognitionClient::Connect (ObjectRecognitionServer& server)

{
   Server = &server;
}

 void ObjectRecognitionClient::Disconnect (   )

{
   if (Server != NULL && ObjectTracker != NULL) Server->RemoveAllRequests (ObjectTracker);
}

 int ObjectRecognitionClient::Initialize (   )

{
   Close (   );
   return (DONE);
}

 int ObjectRecognitionClient::IsConnected (   )

{
   if (Server != NULL) return (TRUE);
   else return (FALSE);
}

 int ObjectRecognitionClient::Perform (ObjectTracking& obj_tracker)

{
   if (!IsEnabled     (   )) return (1);
   if (!IsInitialized (   )) return (2);
   ObjectTracker = &obj_tracker;
   ForegroundDetection* fgd = ObjectTracker->ForegroundDetector;
   if (fgd == NULL) return (3);
   SrcImgSize    = fgd->SrcImgSize;
   SrcFrmSize    = fgd->SrcFrmSize;
   FrameRate     = fgd->FrameRate;
   SrcFrmBuffer  = fgd->SrcFrmBuffer;
   FrameDuration = 1.0f / FrameRate;
   Scale.X       = (float)SrcFrmSize.Width  / SrcImgSize.Width;
   Scale.Y       = (float)SrcFrmSize.Height / SrcImgSize.Height;
   int e_code = DoPreprocessing (obj_tracker);
   if      (e_code > 0) return (4);
   else if (e_code < 0) return (DONE);
   if (!IsConnected (   )) return (5);
   ISize2D di_size = Server->GetSrcImageSize (   );
   int n_objects = 0;
   TrackedObject* t_object = ObjectTracker->TrackedObjects.First (   );
   while (t_object != NULL) {
      t_object->DNNInputImage.Delete (   );
      t_object->Status &= ~TO_STATUS_RECOGNIZE;
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED) && CheckObject (t_object) == DONE) {
         int*   p_count = GetObjectCountVariable (t_object);
         float* p_time  = GetObjectTimeVariable  (t_object);
         if ((*p_count) < NumTries) {
            if (CheckObjectInOuterZone (SrcImgSize,OuterZoneSize1,OuterZoneSize2,t_object) != DONE || t_object->Time_Tracked >= MaxTime_Unrecognized) {
               if (!(*p_count) || (*p_time) >= ProcPeriod) {
                  (*p_count)++;
                  (*p_time) = 0.0f;
                  t_object->Status |= TO_STATUS_RECOGNIZE;
                  t_object->DNNInputImage.Create (di_size.Width,di_size.Height);
                  t_object->GetImagePatch_YUY2 (SrcFrmBuffer,SrcFrmSize,Scale,Inflation,t_object->DNNInputImage);
                  n_objects++;
               }
               (*p_time) += FrameDuration;
            }
         }
      }
      t_object = ObjectTracker->TrackedObjects.Next (t_object);
   }
   if (n_objects) {
      if (Server->AddRequest (ObjectTracker)) return (5);
      WaitForSingleEvent (ObjectTracker->Event_Processed);
   }
   t_object = ObjectTracker->TrackedObjects.First (   );
   while (t_object != NULL) {
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED)) UpdateRecognitionResult (t_object);
      t_object = ObjectTracker->TrackedObjects.Next (t_object);
   }
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: PersonAttrRecognition_DNN
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_CAFFE)

 void PersonAttrRecognition_DNN_Common::_Close (   )

{
   DNN.Close (   );
   CI2AI.Delete (   );
}

 int PersonAttrRecognition_DNN_Common::_Initialize (void* dll_handle,const char* dir_name,int flag_use_gpu,int gpu_idx)

{
   struct _ANAIDT {
      char  AttrName[64];
      int   AttrIdx;
      float DThreshold;
   };
   static _ANAIDT anaidt[] = {
      { "male"         , PAR_ATTR_MALE         , 0.6f },
      { "bag"          , PAR_ATTR_BAG          , 0.6f },
      { "child"        , PAR_ATTR_CHILD        , 0.6f },
      { "stripe"       , PAR_ATTR_STRIPE       , 0.6f },
      { "long_trousers", PAR_ATTR_LONG_TROUSERS, 0.6f },
      { "jeans"        , PAR_ATTR_JEANS        , 0.6f },
      { "skirt"        , PAR_ATTR_SKIRT        , 0.6f },
      { "t-shirt"      , PAR_ATTR_TSHIRT       , 0.6f },
      { "short_sleeves", PAR_ATTR_SHORT_SLEEVES, 0.6f },
      { "suit"         , PAR_ATTR_SUIT         , 0.6f },
   };
   const char cfg_file_name[] = "IVXPACCFN2.dat"; // deploy.prototxt
   const char wgt_file_name[] = "IVXPAWCFN2.dat"; // deploy.caffemodel
   const char cls_file_name[] = "IVXPANCFN2.dat"; // labels.txt
   const char mns_file_name[] = "IVXPAMCFN2.dat"; // mean.binaryproto

   int i,j;

   StringA cfg_path_name,wgt_path_name,cls_path_name,mns_path_name;
   if (dir_name == NULL) {
      cfg_path_name = cfg_file_name;
      wgt_path_name = wgt_file_name;
      cls_path_name = cls_file_name;
      mns_path_name = mns_file_name;
   }
   else {
      #if defined(__OS_WINDOWS)
      cfg_path_name.Format ("%s\\%s",dir_name,cfg_file_name);
      wgt_path_name.Format ("%s\\%s",dir_name,wgt_file_name);
      cls_path_name.Format ("%s\\%s",dir_name,cls_file_name);
      mns_path_name.Format ("%s\\%s",dir_name,mns_file_name);
      #else
      cfg_path_name.Format ("%s/%s",dir_name,cfg_file_name);
      wgt_path_name.Format ("%s/%s",dir_name,wgt_file_name);
      cls_path_name.Format ("%s/%s",dir_name,cls_file_name);
      mns_path_name.Format ("%s/%s",dir_name,mns_file_name);
      #endif
   }
   if (DNN.Initialize (dll_handle,cfg_path_name,wgt_path_name,cls_path_name,mns_path_name,flag_use_gpu,gpu_idx)) return (1);
   if (!DNN.ClassNames) return (2);
   CI2AI.Create (DNN.ClassNames.Length);
   CI2AI.Clear  (   );
   int n = sizeof(anaidt) / sizeof(_ANAIDT);
   for (i = 0; i < DNN.ClassNames.Length; i++) {
      char* cls_name = DNN.ClassNames[i];
      for (j = 0; j < n; j++) {
        if (!strcmp (cls_name,anaidt[j].AttrName)) {
            CI2AI[i].AttrIdx    = anaidt[j].AttrIdx;
            CI2AI[i].DThreshold = anaidt[j].DThreshold;
            break;
         }
      }
      if (j == n) {
         CI2AI[i].AttrIdx    = 0;
         CI2AI[i].DThreshold = MC_VLV;
      }
   }
   return (DONE);
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: PersonAttrRecognition_DNN
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_CAFFE)

 PersonAttrRecognition_DNN::PersonAttrRecognition_DNN (   )

{
   NumTries   = 100;
   ProcPeriod = 0.7f;
   Inflation(1.1f,1.0f);
}

 int PersonAttrRecognition_DNN::CheckObject (TrackedObject* t_object)

{
   if (!(t_object->Status & TO_STATUS_VERIFIED)) return (1);
   if (t_object->Type != TO_TYPE_HUMAN) return (2);
   return (DONE);
}

 void PersonAttrRecognition_DNN::Close (   )

{
   ObjectRecognition::Close (   );
   PersonAttrRecognition_DNN_Common::_Close (   );
}

 int PersonAttrRecognition_DNN::Initialize (void* dll_handle,const char* dir_name,int flag_use_gpu,int gpu_idx)

{
   if (ObjectRecognition::Initialize (   )) return (1);
   if (PersonAttrRecognition_DNN_Common::_Initialize (dll_handle,dir_name,flag_use_gpu,gpu_idx)) return (2);
   Flag_Initialized = TRUE;
   return (DONE);
}

 void PersonAttrRecognition_DNN::RecognizeObject (TrackedObject* t_object)

{
   int i;

   BGRImage s_cimage(DNN.SrcImgSize.Width,DNN.SrcImgSize.Height);
   t_object->GetImagePatch_YUY2 (SrcFrmBuffer,SrcFrmSize,Scale,Inflation,s_cimage);
   cv::Mat m_s_cimage;
   OpenCV_InitMat (s_cimage,m_s_cimage);
   std::vector<float> prediction;
   DNN.Predict (m_s_cimage,prediction);
   int n = GetMinimum ((int)prediction.size(),DNN.ClassNames.Length);
   for (i = 0; i < n; i++) {
      AIDT& c = CI2AI[i];
      if (prediction[i] >= c.DThreshold) t_object->PALikelihood[c.AttrIdx] += 1.0f;
   }
}

 void PersonAttrRecognition_DNN::UpdateRecognitionResult (TrackedObject* t_object)

{
   t_object->UpdatePersonAttributes (   );
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: PersonAttrRecognitionServer_DNN
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_CAFFE)

 PersonAttrRecognitionServer_DNN::PersonAttrRecognitionServer_DNN (   )

{
   DLLHandle   = NULL;
   GpuNo       = 0;
   Flag_UseGPU = TRUE;
}

 void PersonAttrRecognitionServer_DNN::Close (   )

{
   ObjectRecognitionServer::Close (   );
}

 int PersonAttrRecognitionServer_DNN::Initialize (void* dll_handle,const char* dir_name,int flag_use_gpu,int gpu_idx)

{
   if (ObjectRecognitionServer::Initialize (   )) return (1);
   DLLHandle        = dll_handle;
   DirName          = dir_name;
   Flag_UseGPU      = flag_use_gpu;
   GpuNo            = gpu_idx;
   Flag_Initialized = TRUE;
   return (DONE);
}

 void PersonAttrRecognitionServer_DNN::ProcessRequest (ObjectTracking* obj_tracker)

{
   int i,j;

   std::vector<TrackedObject*> t_objects;
   TrackedObject* t_object = obj_tracker->TrackedObjects.First (   );
   while (t_object != NULL) {
      if (t_object->Status & TO_STATUS_RECOGNIZE) t_objects.push_back (t_object);
      t_object = obj_tracker->TrackedObjects.Next (t_object);
   }
   int n1 = (int)t_objects.size (   );
   std::vector<cv::Mat> m_s_cimages(n1);
   for (i = 0; i < n1; i++) {
      OpenCV_InitMat (t_objects[i]->DNNInputImage,m_s_cimages[i]);
   }
   std::vector<std::vector<float> > predictions;
   DNN.Predict (m_s_cimages,predictions);
   for (i = 0; i < n1; i++) {
      int n2 = GetMinimum ((int)predictions[i].size(),DNN.ClassNames.Length);
      for (j = 0; j < n2; j++) {
         AIDT& c = CI2AI[j];
         if (predictions[i][j] >= c.DThreshold) t_objects[i]->PALikelihood[c.AttrIdx] += 1.0f;
      }
   }
}

 void PersonAttrRecognitionServer_DNN::OnThreadEnded (   )

{
   PersonAttrRecognition_DNN_Common::_Close (   );
}

 int PersonAttrRecognitionServer_DNN::OnThreadStarted (   )

{
   if (!IsInitialized (   )) return (1);
   logd (">> Initializing a PersonAttrRecognition DNN...\n");
   int e_code = PersonAttrRecognition_DNN_Common::_Initialize (DLLHandle,DirName,Flag_UseGPU,GpuNo);
   if (e_code) {
      logd (">> Initialization of the PersonAttrRecognition DNN failed. (Error Code: %d)\n",e_code);
      return (2);
   }
   return (DONE);
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: PersonAttrRecognitionClient
//
///////////////////////////////////////////////////////////////////////////////

 PersonAttrRecognitionClient::PersonAttrRecognitionClient (   )

{
   NumTries   = 100;
   ProcPeriod = 0.7f;
   Inflation(1.1f,1.0f);
}

 int PersonAttrRecognitionClient::CheckObject (TrackedObject* t_object)

{
   if (!(t_object->Status & TO_STATUS_VERIFIED)) return (1);
   if (t_object->Type != TO_TYPE_HUMAN) return (2);
   return (DONE);
}

 int PersonAttrRecognitionClient::Initialize (   )

{
   if (ObjectRecognitionClient::Initialize (   )) return (1);
   Flag_Initialized = TRUE;
   return (DONE);
}

 void PersonAttrRecognitionClient::UpdateRecognitionResult (TrackedObject* t_object)

{
   if (t_object->Status & TO_STATUS_RECOGNIZE) t_object->UpdatePersonAttributes (   );
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: VehicleTypeRecognition_DNN_Common
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_CAFFE)

 void VehicleTypeRecognition_DNN_Common::_Close (   )

{
   DNN.Close (   );
   CI2TI.Delete (   );
}

 int VehicleTypeRecognition_DNN_Common::_Initialize (void* dll_handle,const char* dir_name,int flag_use_gpu,int gpu_idx)

{
   struct _TNTI {
      char  TypeName[64];
      int   TypeIdx;
   };
   static _TNTI tnti[] = {
      { "sedan"      , VTR_TYPE_SEDAN      },
      { "SUV"        , VTR_TYPE_SUV        },
      { "van"        , VTR_TYPE_VAN        },
      { "truck"      , VTR_TYPE_TRUCK      },
      { "bus"        , VTR_TYPE_BUS        },
      { "two-wheeler", VTR_TYPE_TWOWHEELER }
   };
   const char cfg_file_name[] = "IVXVTCCFN1.dat"; // deploy.prototxt
   const char wgt_file_name[] = "IVXVTWCFN1.dat"; // deploy.caffemodel
   const char cls_file_name[] = "IVXVTNCFN1.dat"; // labels.txt
   const char mns_file_name[] = "IVXVTMCFN1.dat"; // mean.binaryproto

   int i,j;

   StringA cfg_path_name,wgt_path_name,cls_path_name,mns_path_name;
   if (dir_name == NULL) {
      cfg_path_name = cfg_file_name;
      wgt_path_name = wgt_file_name;
      cls_path_name = cls_file_name;
      mns_path_name = mns_file_name;
   }
   else {
      #if defined(__OS_WINDOWS)
      cfg_path_name.Format ("%s\\%s",dir_name,cfg_file_name);
      wgt_path_name.Format ("%s\\%s",dir_name,wgt_file_name);
      cls_path_name.Format ("%s\\%s",dir_name,cls_file_name);
      mns_path_name.Format ("%s\\%s",dir_name,mns_file_name);
      #else
      cfg_path_name.Format ("%s/%s",dir_name,cfg_file_name);
      wgt_path_name.Format ("%s/%s",dir_name,wgt_file_name);
      cls_path_name.Format ("%s/%s",dir_name,cls_file_name);
      mns_path_name.Format ("%s/%s",dir_name,mns_file_name);
      #endif
   }
   if (DNN.Initialize (dll_handle,cfg_path_name,wgt_path_name,cls_path_name,mns_path_name,flag_use_gpu,gpu_idx)) return (1);
   if (!DNN.ClassNames) return (2);
   CI2TI.Create (DNN.ClassNames.Length);
   CI2TI.Clear  (   );
   int n = sizeof(tnti) / sizeof(_TNTI);
   for (i = 0; i < DNN.ClassNames.Length; i++) {
      char* cls_name = DNN.ClassNames[i];
      for (j = 0; j < n; j++) {
         if (!strcmp (cls_name,tnti[j].TypeName)) {
            CI2TI[i] = tnti[j].TypeIdx;
            break;
         }
      }
      if (j == n) return (3);
   }
   return (DONE);
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: VehicleTypeRecognition
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_CAFFE)

 VehicleTypeRecognition_DNN::VehicleTypeRecognition_DNN (   )

{

}

 int VehicleTypeRecognition_DNN::CheckObject (TrackedObject* t_object)

{
   if (!(t_object->Status & TO_STATUS_VERIFIED)) return (1);
   if (t_object->Type != TO_TYPE_VEHICLE) return (2);
   return (DONE);
}

 void VehicleTypeRecognition_DNN::Close (   )

{
   ObjectRecognition::Close (   );
   VehicleTypeRecognition_DNN_Common::_Close (   );
}

 int VehicleTypeRecognition_DNN::Initialize (void* dll_handle,const char* dir_name,int flag_use_gpu,int gpu_idx)

{
   if (ObjectRecognition::Initialize (   )) return (1);
   if (VehicleTypeRecognition_DNN_Common::_Initialize (dll_handle,dir_name,flag_use_gpu,gpu_idx)) return (2);
   Flag_Initialized = TRUE;
   return (DONE);
}

 void VehicleTypeRecognition_DNN::RecognizeObject (TrackedObject* t_object)

{
   int i;

   BGRImage s_cimage(DNN.SrcImgSize.Width,DNN.SrcImgSize.Height);
   t_object->GetImagePatch_YUY2 (SrcFrmBuffer,SrcFrmSize,Scale,Inflation,s_cimage);
   cv::Mat m_s_cimage;
   OpenCV_InitMat (s_cimage,m_s_cimage);
   vector<float> prediction;
   DNN.Predict (m_s_cimage,prediction);
   int n = GetMinimum ((int)prediction.size(),DNN.ClassNames.Length);
   int   max_i = 0;
   float max_v = -MC_VLV;
   for (i = 0; i < n; i++) {
      if (prediction[i] > max_v) {
         max_v = prediction[i];
         max_i = i;
      }
   }
   t_object->VTLikelihood[CI2TI[max_i]] += 1.0f;
}

 void VehicleTypeRecognition_DNN::UpdateRecognitionResult (TrackedObject* t_object)

{
   t_object->UpdateVehicleType (   );
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: VehicleTypeRecognitionServer_DNN
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_CAFFE)

 VehicleTypeRecognitionServer_DNN::VehicleTypeRecognitionServer_DNN (   )

{
   DLLHandle   = NULL;
   GpuNo       = 0;
   Flag_UseGPU = TRUE;
}

 void VehicleTypeRecognitionServer_DNN::Close (   )

{
   ObjectRecognitionServer::Close (   );
}

 int VehicleTypeRecognitionServer_DNN::Initialize (void* dll_handle,const char* dir_name,int flag_use_gpu,int gpu_idx)

{

   if (ObjectRecognitionServer::Initialize (   )) return (1);
   DLLHandle        = dll_handle;
   DirName          = dir_name;
   Flag_UseGPU      = flag_use_gpu;
   GpuNo            = gpu_idx;
   Flag_Initialized = TRUE;
   return (DONE);
}

 void VehicleTypeRecognitionServer_DNN::ProcessRequest (ObjectTracking* obj_tracker)

{
   int i,j;

   std::vector<TrackedObject*> t_objects;
   TrackedObject* t_object = obj_tracker->TrackedObjects.First (   );
   while (t_object != NULL) {
      if (t_object->Status & TO_STATUS_RECOGNIZE) t_objects.push_back (t_object);
      t_object = obj_tracker->TrackedObjects.Next (t_object);
   }
   int n1 = (int)t_objects.size (   );
   std::vector<cv::Mat> m_s_cimages(n1);
   for (i = 0; i < n1; i++) {
      OpenCV_InitMat (t_objects[i]->DNNInputImage,m_s_cimages[i]);
   }
   std::vector<std::vector<float> > predictions;
   DNN.Predict (m_s_cimages,predictions);
   for (i = 0; i < n1; i++) {
      int   n2    = GetMinimum ((int)predictions[i].size(),DNN.ClassNames.Length);
      int   max_j = 0;
      float max_v = -MC_VLV;
      for (j = 0; j < n2; j++) {
         if (predictions[i][j] > max_v) {
            max_v = predictions[i][j];
            max_j = j;
         }
      }
      t_objects[i]->VTLikelihood[CI2TI[max_j]] += 1.0f;
   }
}

 void VehicleTypeRecognitionServer_DNN::OnThreadEnded (   )

{
   VehicleTypeRecognition_DNN_Common::_Close (   );
}

 int VehicleTypeRecognitionServer_DNN::OnThreadStarted (   )

{
   if (!IsInitialized (   )) return (1);
   logd (">> Initializing a VehicleTypeRecognition DNN...\n");
   int e_code = VehicleTypeRecognition_DNN_Common::_Initialize (DLLHandle,DirName,Flag_UseGPU,GpuNo);
   if (e_code) {
      logd (">> Initialization of the VehicleTypeRecognition DNN failed. (Error Code: %d)\n",e_code);
      return (2);
   }
   return (DONE);
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: VehicleTypeRecognitionClient
//
///////////////////////////////////////////////////////////////////////////////

 VehicleTypeRecognitionClient::VehicleTypeRecognitionClient (   )

{

}

 int VehicleTypeRecognitionClient::CheckObject (TrackedObject* t_object)

{
   if (!(t_object->Status & TO_STATUS_VERIFIED)) return (1);
   if (t_object->Type != TO_TYPE_VEHICLE) return (2);
   return (DONE);
}

 int VehicleTypeRecognitionClient::Initialize (   )

{
   if (ObjectRecognitionClient::Initialize (   )) return (1);
   Flag_Initialized = TRUE;
   return (DONE);
}

 void VehicleTypeRecognitionClient::UpdateRecognitionResult (TrackedObject* t_object)

{
   if (t_object->Status & TO_STATUS_RECOGNIZE) t_object->UpdateVehicleType (   );
}

}

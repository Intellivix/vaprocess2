#include "vacl_color.h"
#include "vacl_event.h"
#include "vacl_morph.h"

#if defined(__DEBUG_CCD)
#include "Win32CL/Win32CL.h"
extern int _DVX_CCD,_DVY_CCD;
extern CImageView* _DebugView_CCD;
#endif

 namespace VACL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Class: ColorChangeDetection
//
///////////////////////////////////////////////////////////////////////////////

 ColorChangeDetection::ColorChangeDetection (   )
 
{
   MinBlobArea         = 25;
   MinCount_Tracked    = 3;
   MaxTime_Undetected1 = 0.0f;
   MaxTime_Undetected2 = 0.0f;
   MaxTime_Verified    = 1.0f;
   MinMovingDistance1  = 0.0f;
   MinMovingDistance2  = 0.0f;
   
   Flag_Initialized    = FALSE;
   CFRadius            = 3;
   MDThreshold         = 20;
   Flag_RgnColor       = TRUE;
   MinTime_Static      = 3.0f;

   FrameRate           = 0.0f;
}

 ColorChangeDetection::~ColorChangeDetection (   )
 
{
   Close (   );
}

 void ColorChangeDetection::Close (   )
 
{
   BlobTracking::Close     (   );
   VotingScoreMap.Delete   (   );
   ColorChangeImage.Delete (   );
   Flag_Initialized = FALSE;
}

 int ColorChangeDetection::GetBkgModelUpdateScheme (GImage &d_image)

{
   int x,y;

   if (!IsEnabled (   )) return (1);
   TrackedBlob* t_blob = TrackedBlobs.First (   );
   while (t_blob != NULL) {
      if (!(t_blob->Status & TB_STATUS_TO_BE_REMOVED) && (t_blob->Status & TB_STATUS_IN_EVENT_ZONE)) {
         int sx = t_blob->X;
         int sy = t_blob->Y;
         int ex = sx + t_blob->Width;
         int ey = sy + t_blob->Height;
         int lb = t_blob->Correspondence;
         for (y = sy; y < ey; y++) {
            int  *l_br_map  = BlobRegionMap[y];
            byte *l_d_image = d_image[y];
            for (x = sx; x < ex; x++) {
               if (l_br_map[x] == lb) l_d_image[x] = FGD_BMU_SPEED_ZERO;
            }
         }
      }
      t_blob = TrackedBlobs.Next (t_blob);
   }
   return (DONE);
}

 void ColorChangeDetection::GetExcludedRegionMap (ObjectTracking& obj_tracker,GImage &d_map)
 
{
   d_map.Clear (   );
   TrackedObject* t_object = obj_tracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED) && (t_object->Status & TO_STATUS_VERIFIED) && (t_object->Type != TO_TYPE_UNKNOWN))
         t_object->PutMask (t_object->X,t_object->Y,PG_WHITE,d_map);
      t_object = obj_tracker.TrackedObjects.Next (t_object);
   }
}

 int ColorChangeDetection::Initialize (ISize2D& si_size,float frm_rate)
 
{
   if (BlobTracking::Initialize (si_size,frm_rate)) return (1);
   VotingScoreMap.Create   (si_size.Width,si_size.Height);
   ColorChangeImage.Create (si_size.Width,si_size.Height);
   Reset (   );
   return (DONE);
}

 int ColorChangeDetection::Perform (ObjectTracking& obj_tracker)
 
{
   if (!IsEnabled (    )) return (1);
   if (!Flag_Initialized) return (2);
   ForegroundDetection* frg_detector = obj_tracker.ForegroundDetector;
   if (frg_detector == NULL) return (3);
   if (frg_detector->SrcColorImage == NULL) return (4);
   if (frg_detector->SrcImgSize.Width != SrcImgSize.Width || frg_detector->SrcImgSize.Height != SrcImgSize.Height) return (5);
   #if defined(__DEBUG_CCD)
   _DVX_CCD = _DVY_CCD = 0;
   _DebugView_CCD->Clear (   );
   #endif
   if (!FrameCount) VotingScoreMap.Clear (   );
   GImage er_map(SrcImgSize.Width,SrcImgSize.Height);
   GetExcludedRegionMap (obj_tracker,er_map);
   VoteForColorChange   (*frg_detector->SrcColorImage,frg_detector->BkgColorImage,er_map);
   #if defined(__DEBUG_CCD)
   _DebugView_CCD->DrawImage (VotingScoreMap,_DVX_CCD,_DVY_CCD);
   _DVY_CCD += SrcImgSize.Height;
   _DebugView_CCD->DrawText ("Voting Score Map",_DVX_CCD,_DVY_CCD,PC_GREEN);
   _DVY_CCD += 20;
   #endif
   GImage d_image(SrcImgSize.Width,SrcImgSize.Height);
   int vs_thrsld = (int)(MinTime_Static * FrameRate + 0.5f);
   Binarize (VotingScoreMap,vs_thrsld,d_image);
   PerformBinClosing (d_image,CFRadius,ColorChangeImage);
   #if defined(__DEBUG_CCD)
   _DebugView_CCD->DrawImage (ColorChangeImage,_DVX_CCD,_DVY_CCD);
   _DVY_CCD += SrcImgSize.Height;
   _DebugView_CCD->DrawText ("Color Change Image",_DVX_CCD,_DVY_CCD,PC_GREEN);
   _DVY_CCD += 20;
   #endif
   BlobTracking::Perform  (ColorChangeImage);
   #if defined(__DEBUG_CCD)
   _DebugView_CCD->Invalidate (   );
   #endif
   UpdateBlobColors (*frg_detector->SrcColorImage,frg_detector->BkgColorImage);
   return (DONE);
}

 void ColorChangeDetection::Reset (   )
 
{
   BlobTracking::Reset (   );
   ColorChangeImage.Clear (   );
}

 void ColorChangeDetection::UpdateBlobColors (BGRImage& s_cimage,BGRImage& b_cimage)
 
{
   BGRColor f_color,b_color;
   TrackedBlob* t_blob = TrackedBlobs.First (   );
   while (t_blob != NULL) {
      if (!(t_blob->Status & TB_STATUS_TO_BE_REMOVED)) {
         t_blob->GetCenterColors (s_cimage,b_cimage,f_color,b_color);
         t_blob->UpdateCenterColors (f_color,b_color);
         if (Flag_RgnColor) {
            t_blob->GetRegionColors (s_cimage,b_cimage,BlobRegionMap,f_color,b_color);
            t_blob->UpdateRegionColors (f_color,b_color);
         }
      }
      t_blob = TrackedBlobs.Next (t_blob);
   }
}

 int ColorChangeDetection::VoteForColorChange (BGRImage& s_cimage,BGRImage& b_cimage,GImage& er_map)
 
{
   int x,y;

   int max_vs   = (int)((MinTime_Static + 1.0f) * FrameRate + 0.5f);
   int d_thrsld = 3 * MDThreshold;
   if (!b_cimage) return (1);
   for (y = 0; y < s_cimage.Height; y++) {
      BGRPixel* sp = s_cimage[y];
      BGRPixel* bp = b_cimage[y];
      byte* _RESTRICT l_er_map = er_map[y];
      int*  _RESTRICT l_vs_map = VotingScoreMap[y];
      for (x = 0; x < s_cimage.Width; x++) {
         if (!l_er_map[x]) {
            int vs = l_vs_map[x];
            int d = abs(sp->B - bp->B) + abs(sp->G - bp->G) + abs(sp->R - bp->R);
            if (d >= d_thrsld) {
               vs++;
               if (vs > max_vs) vs = max_vs;
            }
            else {
               vs -= 2;
               if (vs < 0) vs = 0;
            }
            l_vs_map[x] = vs;
         }
         sp++; bp++;
      }
   }
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: ColorZone
//
///////////////////////////////////////////////////////////////////////////////

 ColorZone::ColorZone (   )

{
   DetFailCount   = 0;
   Flag_Processed = FALSE;
   DetectionTime  = 0.0f;
   EvtZone        = NULL;
}

 int ColorZone::DetectEvent (   )

{
   if (!Flag_Processed) return (0);
   if (EvtZone == NULL) return (0);
   EventRule* evt_rule = EvtZone->EvtRule;
   if (evt_rule == NULL) return (0);
   EventRuleConfig_ZoneColor& erc = evt_rule->ERC_ZoneColor;
   EventDetection* evt_detector = evt_rule->EventDetector;
   if (evt_detector == NULL) return (0);
   int n_objects = 0;
   int flag_detection = FALSE;
   BGRColor& tc = erc.TgtColor;
   int dr = (int)tc.R - ZoneColor.R;
   int dg = (int)tc.G - ZoneColor.G;
   int db = (int)tc.B - ZoneColor.B;
   int d  = (int)sqrt ((float)(dr * dr + dg * dg + db * db));
   #if defined(__DEBUG_ZCD)
   logd ("Event Zone [%s]:\n",EvtZone->Name);
   logd ("   TgtColor = (%03d,%03d,%03d), ZoneColor = (%03d,%03d,%03d), Distance = %d\n",
      tc.R,tc.G,tc.B,ZoneColor.R,ZoneColor.G,ZoneColor.B,d);
   #endif
   if (d <= erc.CDS) {
      DetFailCount = 0;
      DetectionTime += evt_detector->FrameDuration;
      if (DetectionTime >= erc.DetectionTime) flag_detection = TRUE;
   }
   else {
      DetFailCount++;
      if (DetFailCount >= 2) { // PARAMETERS
         DetFailCount  = 0;
         DetectionTime = 0.0f;
      }
   }
   // 이벤트 존에 기 등록된 이벤트 및 해당 객체를 찾는다.
   Event* m_event = NULL;
   TrackedObject* eg_object = NULL;
   TrackedObject* t_object  = evt_detector->EvtGenObjects.First (   );
   while (t_object != NULL) {
      Event* s_event = t_object->FindEvent (EvtZone);
      if (s_event != NULL) {
         m_event   = s_event;
         eg_object = t_object;
         break;
      }
      t_object = evt_detector->EvtGenObjects.Next (t_object);
   }
   if (m_event == NULL) {
      if (flag_detection) {
         eg_object         = CreateInstance_TrackedObject (   );
         eg_object->ID     = evt_detector->GetNewObjectID (   );
         eg_object->Status = TO_STATUS_VALIDATED | TO_STATUS_VERIFIED;
         eg_object->Type   = TO_TYPE_UNKNOWN;
         IBox2D s_rgn;
         EvtZone->GetBoundingBox (s_rgn);
         FPoint2D scale(1.0f / EvtZone->Scale.X,1.0f / EvtZone->Scale.Y);
         s_rgn = s_rgn.Scale (scale);
         eg_object->SetRegion (s_rgn);
         m_event = CreateInstance_Event  (EvtZone,evt_rule);
         m_event->StartTime.GetLocalTime (   );
         eg_object->Events.Add (m_event);
         evt_detector->EvtGenObjects.Add (eg_object);
         n_objects = 1;
      }
   }
   else if (m_event->EvtRule == evt_rule) {
      if (flag_detection) {
         m_event->ElapsedTime += evt_detector->FrameDuration;
         m_event->BonusTime = 0.0f;
         if (m_event->Status == EV_STATUS_BEGUN) m_event->Status = EV_STATUS_ONGOING;
      }
      else {
         // 타겟 색상 검출에 실패한 경우, LastingTime 만큼 기회를 준다.
         m_event->BonusTime += evt_detector->FrameDuration;
         if (m_event->BonusTime > erc.LastingTime) {
            // LastingTime 이상 지나도록 색상 검출에 실패한 경우 해당 이벤트를 종료한다.
            m_event->Status = EV_STATUS_ENDED | EV_STATUS_TO_BE_REMOVED;
            m_event->EndTime.GetLocalTime (   );
            eg_object->Status |= TO_STATUS_TO_BE_REMOVED;
         }
      }
   }
   evt_rule->ERC_Counting.Count += n_objects;
   return (n_objects);
}

 void ColorZone::GetZoneColor (BGRImage& s_cimage)
 
{
   int x,y;
   
   Flag_Processed = FALSE;
   if (!EvtZone->IsEnabled (   )) return;
   ZoneColor.Clear (   );
   // 이벤트 존에 해당하는 마스크 영역을 얻는다.
   GImage m_image(s_cimage.Width,s_cimage.Height);
   m_image.Clear (   );
   FPoint2D scale;
   scale.X = (float)s_cimage.Width  / EvtZone->RefImgSize.Width;
   scale.Y = (float)s_cimage.Height / EvtZone->RefImgSize.Height;
   EvtZone->Fill (PG_WHITE,scale,m_image);
   // 이벤트 존의 바운딩 박스를 얻는다.
   IBox2D b_box;
   EvtZone->GetBoundingBox (b_box);
   b_box = b_box.Scale (scale);
   int sx = b_box.X;
   int sy = b_box.Y;
   int ex = sx + b_box.Width;
   int ey = sy + b_box.Height;
   CropRegion (s_cimage.Width,s_cimage.Height,sx,sy,ex,ey);
   // s_image의 이벤트 존 내의 픽셀들의 평균 색상 값을 얻는다.
   int b = 0, g = 0, r = 0, n = 0;
   for (y = sy; y < ey; y++) {
      byte* l_m_image = m_image[y];
      BGRPixel* sp = &s_cimage[y][sx];
      for (x = sx; x < ex; x++,sp++) {
         if (l_m_image[x]) {
            b += sp->B;
            g += sp->G;
            r += sp->R;
            n++;
         }
      }
   }
   if (!n) return;
   ZoneColor.B = (byte)(b / n);
   ZoneColor.G = (byte)(g / n);
   ZoneColor.R = (byte)(r / n);
   Flag_Processed = TRUE;
} 

///////////////////////////////////////////////////////////////////////////////
//
// Class: ZoneColorDetection
//
///////////////////////////////////////////////////////////////////////////////

 ZoneColorDetection::ZoneColorDetection (   )
 
{
   Flag_Enabled = TRUE;
   FrameRate    = 0.0f;
}

 ZoneColorDetection::~ZoneColorDetection (   )
 
{
   Close (   );
}

 void ZoneColorDetection::Close (   )
 
{
   ColorZones.Delete (   );
}

 int ZoneColorDetection::Initialize (EventDetection& evt_detector)
 
{
   Close (   );
   if (!IsEnabled (   )) return (1);
   SrcImgSize = evt_detector.SrcImgSize;
   FrameRate  = evt_detector.FrameRate;
   EventZoneList& ez_list = evt_detector.EventZones;
   // 이벤트 종류가 "Zone Color"인 이벤트 존들을 찾아 등록한다.
   EventZone* evt_zone = ez_list.First (   );
   while (evt_zone != NULL) {
      if (evt_zone->EvtRule->EventType == ER_EVENT_ZONE_COLOR) {
         ColorZone* cl_zone = new ColorZone;
         cl_zone->EvtZone = evt_zone;
         ColorZones.Add (cl_zone);
      }
      evt_zone = ez_list.Next (evt_zone);
   }
   if (!ColorZones.GetNumItems (   )) return (2);
   return (DONE);
}

 int ZoneColorDetection::Perform (BGRImage& s_cimage)
 
{
   if (!IsEnabled (   )) return (1);
   if (!ColorZones.GetNumItems (   )) return (2);
   ColorZone* zone = ColorZones.First (   );
   while (zone != NULL) {
      zone->GetZoneColor (s_cimage);
      zone = ColorZones.Next (zone);
   }
   return (DONE);
}

}

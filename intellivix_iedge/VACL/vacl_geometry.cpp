#include "vacl_geometry.h"
#include "vacl_optimize.h"

 namespace VACL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 int CheckHomography (Matrix& mat_H,float min_sc,float max_sc)

{
   if (fabs (mat_H[2][2]) < MC_VSV) return (1);
   double scale_x = mat_H[0][0] / mat_H[2][2];
   double scale_y = mat_H[1][1] / mat_H[2][2];
   if (scale_x < min_sc || scale_x > max_sc || scale_y < min_sc || scale_y > max_sc) return (2);
   return (DONE);
}

 float GetAngle (FPoint2D &s_vec1,FPoint2D &s_vec2)
 
{
   float m1 = Mag(s_vec1);
   float m2 = Mag(s_vec2);
   if (m1 < MC_VSV || m2 < MC_VSV) return (0.0f);
   float angle = MC_RAD2DEG * acos (IP(s_vec1,s_vec2) / (m1 * m2));
   return (angle);
}

////////////////////////////////////////////////////////////////////////////////
//
//  World of Mathematics: Euler Angles
//  Eric Weisstein
//  http://mathworld.wolfram.com/EulerAngles.html
//
////////////////////////////////////////////////////////////////////////////////
 
 void GetEulerAngles_XYZ (Matrix &mat_R,double &psi,double &theta,double &phi)
 // mat_R [IN] : 3 x 3 Rotation matrix
 //              mat_R = mat_Rx(psi) * mat_Ry(theta) * mat_Rz(phi)
 // psi   [OUT]: Angle about the x-axis
 // theta [OUT]: Angle about the y-axis
 // phi   [OUT]: Angle about the z-axis
{
   Matrix vec_r = mat_R.GetRowVector (0);
   Matrix mat_Rn = mat_R / Mag(vec_r);
   double sin_theta = -mat_Rn[0][2];
   theta = asin (sin_theta);
   if (mat_Rn[0][0] < 0.0) {
      if (sin_theta >= 0.0) theta = MC_PI - theta;
      else theta = -MC_PI - theta;
   }
   double cos_theta = cos (theta);
   if (fabs (cos_theta) >= 1E-10) {
      double cos_psi = mat_Rn[2][2] / cos_theta;
      double sin_psi = mat_Rn[1][2] / cos_theta;
      double cos_phi = mat_Rn[0][0] / cos_theta;
      double sin_phi = mat_Rn[0][1] / cos_theta;
      psi = atan2 (sin_psi,cos_psi);
      phi = atan2 (sin_phi,cos_phi);
   }
   else {
      if (mat_Rn[0][2] > 0.0) {
         double cos_psi_phi =  mat_Rn[1][1];
         double sin_psi_phi = -mat_Rn[1][0];
         double psi_phi = atan2 (sin_psi_phi,cos_psi_phi);
         psi = phi = psi_phi / 2.0;
      }
      else {
         double cos_psi_phi = mat_Rn[1][1];
         double sin_psi_phi = mat_Rn[1][0];
         double psi_phi = atan2 (sin_psi_phi,cos_psi_phi);
         phi = 0.0;
         psi = psi_phi;
      }
   }
}
 
 void GetEulerAngles_ZXZ (Matrix &mat_R,double &psi,double &theta,double &phi)
 // mat_R [IN] : 3 x 3 Rotation matrix
 //              mat_R = mat_Rz(psi) * mat_Rx(theta) * mat_Rz(phi)
 // psi   [OUT]: Angle about the z-axis
 // theta [OUT]: Angle about the x-axis
 // phi   [OUT]: Angle about the z-axis
{
   Matrix vec_r  = mat_R.GetRowVector (2);
   Matrix mat_Rn = mat_R / Mag(vec_r);
   theta = acos (mat_Rn[2][2]);
   if (mat_Rn[2][1] > 0.0) theta = -theta;
   double sin_theta = sin (theta);
   if (fabs (sin_theta) >= 1E-10) {
      double cos_psi =  mat_Rn[1][2] / sin_theta;
      double sin_psi =  mat_Rn[0][2] / sin_theta;
      double cos_phi = -mat_Rn[2][1] / sin_theta;
      double sin_phi =  mat_Rn[2][0] / sin_theta;
      psi = atan2 (sin_psi,cos_psi);
      phi = atan2 (sin_phi,cos_phi);
   }
   else {
      if (mat_Rn[2][2] > 0.0) {
         double cos_psi_phi = mat_Rn[0][0];
         double sin_psi_phi = mat_Rn[0][1];
         double psi_phi = atan2 (sin_psi_phi,cos_psi_phi);
         psi = phi = psi_phi / 2.0;
      }
      else {
         double cos_psi_phi =  mat_Rn[0][0];
         double sin_psi_phi = -mat_Rn[0][1];
         double psi_phi = atan2 (sin_psi_phi,cos_psi_phi);
         phi = 0.0;
         psi = psi_phi;
      }
   }
}

 void GetHomography (FP2DArray1D &s_points1,FP2DArray1D &s_points2,Matrix &mat_H,int flag_normalize)

{
   int i,j;

   Matrix mat_T1,mat_T2;
   FP2DArray1D n_points1;
   FP2DArray1D n_points2;
   FPoint2D *sp1 = (FPoint2D*)s_points1;
   FPoint2D *sp2 = (FPoint2D*)s_points2;
   if (flag_normalize) {
      n_points1.Create (s_points1.Length);
      n_points2.Create (s_points2.Length);
      mat_T1 = NormalizeImageCoordinates (s_points1,n_points1);
      mat_T2 = NormalizeImageCoordinates (s_points2,n_points2);
      sp1 = (FPoint2D*)n_points1;
      sp2 = (FPoint2D*)n_points2;
   }
   Matrix mat_A(2 * s_points1.Length,9);
   mat_A.Clear (   );
   for (i = j = 0; i < s_points1.Length; i++) {
      mat_A[j][3] = -sp1[i].X;
      mat_A[j][4] = -sp1[i].Y;
      mat_A[j][5] = -1.0;
      mat_A[j][6] = sp2[i].Y * sp1[i].X;
      mat_A[j][7] = sp2[i].Y * sp1[i].Y;
      mat_A[j][8] = sp2[i].Y;
      j++;
      mat_A[j][0] = sp1[i].X;
      mat_A[j][1] = sp1[i].Y;
      mat_A[j][2] = 1.0;
      mat_A[j][6] = -sp2[i].X * sp1[i].X;
      mat_A[j][7] = -sp2[i].X * sp1[i].Y;
      mat_A[j][8] = -sp2[i].X;
      j++;
   }
   Matrix vec_e(9),mat_E(9,9);
   EigenDecomp (Tr(mat_A) * mat_A,vec_e,mat_E);
   for (i = 0; i < 9; i++) mat_H(i) = mat_E[i][8];
   if (flag_normalize) mat_H = Inv(mat_T2) * mat_H * mat_T1;
}

 void GetHomography (IP2DArray1D &s_points1,IP2DArray1D &s_points2,Matrix &mat_H,int flag_normalize)
 
{
   int i;
   
   FP2DArray1D t_points1(s_points1.Length);
   FP2DArray1D t_points2(s_points2.Length);
   for (i = 0; i < s_points1.Length; i++) {
      t_points1[i].X = (float)s_points1[i].X;
      t_points1[i].Y = (float)s_points1[i].Y;
      t_points2[i].X = (float)s_points2[i].X;
      t_points2[i].Y = (float)s_points2[i].Y;
   }
   GetHomography (t_points1,t_points2,mat_H,flag_normalize);
}

 void GetHomography (FP3DArray1D &s_points1,FP3DArray1D &s_points2,Matrix &mat_H)
 
{
   int i,j;
   
   Matrix mat_A(2 * s_points1.Length,9);
   mat_A.Clear (   );
   for (i = j = 0; i < s_points1.Length; i++) {
      mat_A[j][3] = -s_points2[i].Z * s_points1[i].X;
      mat_A[j][4] = -s_points2[i].Z * s_points1[i].Y;
      mat_A[j][5] = -s_points2[i].Z * s_points1[i].Z;
      mat_A[j][6] =  s_points2[i].Y * s_points1[i].X;
      mat_A[j][7] =  s_points2[i].Y * s_points1[i].Y;
      mat_A[j][8] =  s_points2[i].Y * s_points1[i].Z;
      j++;
      mat_A[j][0] =  s_points2[i].Z * s_points1[i].X;
      mat_A[j][1] =  s_points2[i].Z * s_points1[i].Y;
      mat_A[j][2] =  s_points2[i].Z * s_points1[i].Z;
      mat_A[j][6] = -s_points2[i].X * s_points1[i].X;
      mat_A[j][7] = -s_points2[i].X * s_points1[i].Y;
      mat_A[j][8] = -s_points2[i].X * s_points1[i].Z;
      j++;
   }
   Matrix vec_e(9),mat_E(9,9);
   EigenDecomp (Tr(mat_A) * mat_A,vec_e,mat_E);
   for (i = 0; i < 9; i++) mat_H(i) = mat_E[i][8];
}

 void GetHomography_2DEuclidean (double theta,double tx,double ty,Matrix &mat_H)

{
   mat_H[0][0] =  cos (theta);
   mat_H[0][1] = -sin (theta);
   mat_H[0][2] =  tx;
   mat_H[1][0] = -mat_H[0][1];
   mat_H[1][1] =  mat_H[0][0];
   mat_H[1][2] =  ty;
   mat_H[2][0] =  0.0;
   mat_H[2][1] =  0.0;
   mat_H[2][2] =  1.0;
}

 void GetHomographyForRotation (float cx,float cy,float angle,Matrix& mat_H)
 // (cx,cy): Input         : Rotation center.
 // angle  : Input         : Rotation angle in degree.
 // mat_H  : Output (3 x 3): Planar homography matrix.
{
   angle *= MC_DEG2RAD;
   Matrix mat_R(2,2);
   mat_R[0][0] =  cos (angle);
   mat_R[0][1] = -sin (angle);
   mat_R[1][0] = -mat_R[0][1];
   mat_R[1][1] =  mat_R[0][0];
   Matrix vec_x0(2);
   vec_x0(0) = cx, vec_x0(1) = cy;
   Matrix vec_t = -mat_R * vec_x0 + vec_x0;
   mat_H.Copy (mat_R,0,0,mat_R.NRows,mat_R.NCols,0,0);
   mat_H.Copy (vec_t,0,0,vec_t.NRows,vec_t.NCols,0,2);
   mat_H[2][0] = mat_H[2][1] = 0.0;
   mat_H[2][2] = 1.0;
}

 void GetTransformedPosition (FPoint2D& s_point,Matrix& mat_H,FPoint2D& d_point)

{
   Matrix vec_p1(3);
   vec_p1(0) = s_point.X;
   vec_p1(1) = s_point.Y;
   vec_p1(2) = 1.0;
   Matrix vec_p2 = mat_H * vec_p1;
   d_point.X = (float)(vec_p2(0) / vec_p2(2));
   d_point.Y = (float)(vec_p2(1) / vec_p2(2));
}

 void GetTransformedPositions (int si_width,int si_height,Matrix& mat_H,FP2DArray1D& d_points)

{
   Matrix vec_p1(3),vec_p2(3);
   vec_p1(2) = 1.0;
   vec_p1(0) = vec_p1(1) = 0.0;
   vec_p2 = mat_H * vec_p1;
   d_points[0]((float)(vec_p2(0) / vec_p2(2)),(float)(vec_p2(1) / vec_p2(2)));
   vec_p1(0) = si_width - 1;
   vec_p1(1) = 0.0;
   vec_p2 = mat_H * vec_p1;
   d_points[1]((float)(vec_p2(0) / vec_p2(2)),(float)(vec_p2(1) / vec_p2(2)));
   vec_p1(0) = si_width  - 1;
   vec_p1(1) = si_height - 1;
   vec_p2 = mat_H * vec_p1;
   d_points[2]((float)(vec_p2(0) / vec_p2(2)),(float)(vec_p2(1) / vec_p2(2)));
   vec_p1(0) = 0.0;
   vec_p1(1) = si_height - 1;
   vec_p2 = mat_H * vec_p1;
   d_points[3]((float)(vec_p2(0) / vec_p2(2)),(float)(vec_p2(1) / vec_p2(2)));
}

 int GetIntersectionPoint (FPoint3D &vn,float d,FLine3D &s_line,FPoint3D &d_point)
 // Plane equation: vn.X * x + vn.Y * y + vn.Z * z = d
{
   FPoint3D dp = s_line[1] - s_line[0];
   float m = IP(vn,dp);
   if (fabs (m) < MC_VSV) return (1);
   float t = (d - IP(vn,s_line[0])) / m;
   d_point = s_line[0] + dp * t;
   return (DONE);
}

 int GetIntersectionPoint (FLine2D &s_line1,FLine2D &s_line2,FPoint2D &d_point)

{
   Matrix mat_A(2,2),vec_b(2);
   mat_A[0][0] = s_line1[1].X - s_line1[0].X;
   mat_A[1][0] = s_line1[1].Y - s_line1[0].Y;
   mat_A[0][1] = s_line2[0].X - s_line2[1].X;
   mat_A[1][1] = s_line2[0].Y - s_line2[1].Y;
   vec_b(0)    = s_line2[0].X - s_line1[0].X;
   vec_b(1)    = s_line2[0].Y - s_line1[0].Y;
   if (fabs(Det(mat_A)) <= MC_VSV) return (1);
   Matrix vec_t = Inv(mat_A) * vec_b;
   d_point = s_line1[0] + (s_line1[1] - s_line1[0]) * (float)vec_t(0);
   return (DONE);
}

 void GetRotationAngles_Cylinder (double phi,double tx,double ty,FPoint2D &lt_pos,FPoint2D &rb_pos,int img_width,int img_height,double &psi,double &theta)

{
   double cos_phi = cos (phi);
   double sin_phi = sin (phi);
   double cx1 =  0.5 * img_width;
   double cy1 =  0.5 * img_height;
   double cx2 =  cx1 * cos_phi + cy1 * sin_phi + tx;
   double cy2 = -cx1 * sin_phi + cy1 * cos_phi + ty;
   psi   = atan ((rb_pos.Y - lt_pos.Y) / (img_height - 1) * (cy2 - cy1));
   theta = (rb_pos.X - lt_pos.X) / (img_width - 1) * (cx1 - cx2);
}

 void GetRotationMatrices (Array1D<Matrix> &mats_Rr,Array1D<Matrix> &mats_R,int option)

{
   int i,j;
   
   mats_R.Create (mats_Rr.Length + 1);
   mats_R[0] = IM(3);
   for (i = 0; i < mats_Rr.Length; i++) mats_R[i + 1] = mats_Rr[i] * mats_R[i];
   if (option & RM_UNIFORM_TILT) {
      Matrix mat_T(3,3),mat_T1(3,3),mat_T2(3,3);
      double min_error = MC_VLV;
      for (i = 0; i < mats_R.Length; i++) {
         mat_T = Inv(mats_R[i]);
         Array1D<Matrix> mats_R2(mats_R.Length);
         for (j = 0; j < mats_R.Length; j++) mats_R2[j] = mats_R[j] * mat_T;
         Minimizaiton_UniformTilt min_ut;
         min_ut.Initialize (mats_R2);
         double error = min_ut.Perform (   );
         if (error < min_error) {
            min_error = error;
            mat_T1 = mat_T;
            min_ut.GetTransformationMatrix (mat_T2);
         }
      }
      mat_T = mat_T1 * mat_T2;
      for (i = 0; i < mats_R.Length; i++) mats_R[i] *= mat_T;
      double psi,theta,phi;
      GetEulerAngles_XYZ (mats_R[0],psi,theta,phi);
      GetRotationMatrix_Y (-theta,mat_T);
      for (i = 0; i < mats_R.Length; i++) mats_R[i] *= mat_T;
   }
}

 void GetRotationMatrix_PT (double pan_angle,double tilt_angle,Matrix &mat_R)
 
{
   Matrix mat_Rp(3,3);
   GetRotationMatrix_Y (pan_angle,mat_Rp);
   Matrix mat_Rt(3,3);
   GetRotationMatrix_X (-tilt_angle,mat_Rt);
   mat_R = mat_Rt * mat_Rp;
}

 void GetRotationMatrix_X (double angle,Matrix &mat_R)

{
   mat_R.Clear (   );
   mat_R[0][0] =  1.0;
   mat_R[1][1] =  cos (angle);
   mat_R[1][2] =  sin (angle);
   mat_R[2][1] = -mat_R[1][2];
   mat_R[2][2] =  mat_R[1][1];
}

 void GetRotationMatrix_Y (double angle,Matrix &mat_R)

{
   mat_R.Clear (   );
   mat_R[0][0] =  cos (angle);
   mat_R[0][2] = -sin (angle);
   mat_R[1][1] =  1.0;
   mat_R[2][0] = -mat_R[0][2];
   mat_R[2][2] =  mat_R[0][0];
}

 void GetRotationMatrix_Z (double angle,Matrix &mat_R)

{
   mat_R.Clear (   );
   mat_R[0][0] =  cos (angle);
   mat_R[0][1] =  sin (angle);
   mat_R[1][0] = -mat_R[0][1];
   mat_R[1][1] =  mat_R[0][0];
   mat_R[2][2] =  1.0;
}

 void GetRotationMatrix_XYZ (double psi,double theta,double phi,Matrix &mat_R)
 // mat_R = mat_Rx(psi) * mat_Ry(theta) * mat_Rz(phi)
{
   double cos_psi = cos (psi);
   double sin_psi = sin (psi);
   Matrix mat_R1(3,3);
   mat_R1.Clear (   );
   mat_R1[1][1] =  cos_psi;
   mat_R1[1][2] =  sin_psi;
   mat_R1[2][1] = -sin_psi;
   mat_R1[2][2] =  cos_psi;
   mat_R1[0][0] =  1.0;
   double cos_theta = cos (theta);
   double sin_theta = sin (theta);
   Matrix mat_R2(3,3);
   mat_R2.Clear (   );
   mat_R2[0][0] =  cos_theta;
   mat_R2[0][2] = -sin_theta;
   mat_R2[2][0] =  sin_theta;
   mat_R2[2][2] =  cos_theta;
   mat_R2[1][1] =  1.0;
   double cos_phi = cos (phi);
   double sin_phi = sin (phi);
   Matrix mat_R3(3,3);
   mat_R3.Clear (   );
   mat_R3[0][0] =  cos_phi;
   mat_R3[0][1] =  sin_phi;
   mat_R3[1][0] = -sin_phi;
   mat_R3[1][1] =  cos_phi;
   mat_R3[2][2] =  1.0;
   mat_R = mat_R1 * mat_R2 * mat_R3;
}

 void GetRotationMatrix_ZXZ (double psi,double theta,double phi,Matrix &mat_R)
 // mat_R = mat_Rz(psi) * mat_Rx(theta) * mat_Rz(phi)
{
   double cos_psi = cos (psi);
   double sin_psi = sin (psi);
   Matrix mat_R1(3,3);
   mat_R1.Clear (   );
   mat_R1[0][0] =  cos_psi;
   mat_R1[0][1] =  sin_psi;
   mat_R1[1][0] = -sin_psi;
   mat_R1[1][1] =  cos_psi;
   mat_R1[2][2] =  1.0;
   double cos_theta = cos (theta);
   double sin_theta = sin (theta);
   Matrix mat_R2(3,3);
   mat_R2.Clear (   );
   mat_R2[0][0] =  1.0;
   mat_R2[1][1] =  cos_theta;
   mat_R2[1][2] =  sin_theta;
   mat_R2[2][1] = -sin_theta;
   mat_R2[2][2] =  cos_theta;
   double cos_phi = cos (phi);
   double sin_phi = sin (phi);
   Matrix mat_R3(3,3);
   mat_R3.Clear (   );
   mat_R3[0][0] =  cos_phi;
   mat_R3[0][1] =  sin_phi;
   mat_R3[1][0] = -sin_phi;
   mat_R3[1][1] =  cos_phi;
   mat_R3[2][2] =  1.0;
   mat_R = mat_R1 * mat_R2 * mat_R3;
}

 Matrix NormalizeImageCoordinates (FP2DArray1D &s_points,FP2DArray1D &d_points)

{
   int i;

   float v = 0.0f;
   FPoint2D m(0.0f,0.0f);
   for (i = 0; i < s_points.Length; i++) {
      m.X += s_points[i].X;
      m.Y += s_points[i].Y;
      v   += s_points[i].X * s_points[i].X + s_points[i].Y * s_points[i].Y;
   }
   m /= (float)s_points.Length;
   v /= (float)s_points.Length;
   float s = (float)sqrt (2.0f / (v - IP(m,m)));
   for (i = 0; i < s_points.Length; i++) {
      d_points[i].X = s * (s_points[i].X - m.X);
      d_points[i].Y = s * (s_points[i].Y - m.Y);
   }
   Matrix mat_T(3,3);
   mat_T.Clear (   );
   mat_T[0][0] = mat_T[1][1] = (double)s;
   mat_T[0][2] = (double)(-s * m.X);
   mat_T[1][2] = (double)(-s * m.Y);
   mat_T[2][2] = 1.0;
   return (Matrix(&mat_T));
}
 
}

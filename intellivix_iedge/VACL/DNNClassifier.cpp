﻿#include "DNNClassifier.h"

#include <iostream>
#include <fstream>
#include <memory>
#include <algorithm>
#include <chrono>

#include <caffe/caffe.hpp>

#if defined(_WIN32)
#ifdef _DEBUG
#pragma comment(lib, "caffed.lib")
#else
#pragma comment(lib, "caffe.lib")
#endif
#endif


//////////////////////////////////////////////////////////////////////////////
// OpenCV
#define CV_VERSION_ID CVAUX_STR(CV_MAJOR_VERSION) CVAUX_STR(CV_MINOR_VERSION) \
  CVAUX_STR(CV_SUBMINOR_VERSION)

#ifdef _DEBUG
#define cvLIB(name) "opencv_" name CV_VERSION_ID "d"
#define otherLIB(name) name "d"
#else
#define cvLIB(name) "opencv_" name CV_VERSION_ID
#define otherLIB(name) name

#endif //_DEBUG

#if defined(_WIN32)
#pragma comment(lib, cvLIB("world"))
#endif

///////////////////////////////////////////////////////////////////////////////
//
// CDNNClassifier
//
///////////////////////////////////////////////////////////////////////////////

#if 1
class CDNNClassifier
{
public:
   CDNNClassifier() {}
   virtual ~CDNNClassifier() {};

   int Initialize(const std::string& model_file,
                  const std::string& trained_file,
                  const std::string& mean_file,
                  const std::string& label_file,
                  const int flag_use_gpu = 1,
                  const int gpu_no = 0);

   int Predict(const cv::Mat& image, std::vector<float>& result);
   int Predict(const std::vector<cv::Mat>& images, std::vector<std::vector<float> >& result);

   int Classify(const cv::Mat& image, int N, std::vector<Prediction>& pred);
   int Classify(const std::vector<cv::Mat>& images, int N, std::vector<std::vector<Prediction> >& pred);

   void GetInputSize(int& width, int& height);
   bool IsInitialized() { return is_init_; }

private:
   void SetMean(const std::string& mean_file);
   void SetLabel(const std::string& label_file);
   void WrapInputLayer(std::vector<cv::Mat>* input_channels, int n);
   void Preprocess(const cv::Mat& img,
                   std::vector<cv::Mat>* input_channels);

private:
   std::shared_ptr<caffe::Net> net_ = nullptr;
   cv::Size input_geometry_;
   int num_channels_ = 0;
   cv::Mat mean_;
   std::vector<std::string> labels_;
   bool is_init_ = false;
};

static bool PairCompare(const std::pair<float, int>& lhs,
                        const std::pair<float, int>& rhs)
{
   return lhs.first > rhs.first;
}

/* Return the indices of the top N values of vector v. */
static std::vector<int> Argmax(const std::vector<float>& v, int N)
{
   std::vector<std::pair<float, int> > pairs;
   for (size_t i = 0; i < v.size(); ++i)
      pairs.push_back(std::make_pair(v[i], i));
   std::partial_sort(pairs.begin(), pairs.begin() + N, pairs.end(), PairCompare);
   std::vector<int> result;
   for (int i = 0; i < N; ++i)
      result.push_back(pairs[i].second);

   return result;
}

int CDNNClassifier::Initialize(const std::string& model_file,
                               const std::string& trained_file,
                               const std::string& mean_file,
                               const std::string& label_file,
                               const int flag_use_gpu,
                               const int gpu_no)
{
/*
 * #ifdef CPU_ONLY
   caffe::Caffe::set_mode(caffe::Caffe::CPU);
#else
   if (flag_use_gpu)
   {
      caffe::SetMode(caffe::GPU, gpu_no);
   }
   else
   {
      caffe::SetMode(caffe::CPU, -1);
   }
#endif
*/
   
   // only cpu : ljh
   caffe::SetMode(caffe::CPU, -1);
   
   net_.reset(new caffe::Net(model_file));
   net_->CopyTrainedLayersFrom(trained_file);

   std::shared_ptr<caffe::Blob> input_layer = net_->blob_by_name(net_->blob_names().front());
   num_channels_ = input_layer->channels();
   input_geometry_ = cv::Size(input_layer->width(), input_layer->height());

   SetMean(mean_file);
   SetLabel(label_file);
   
   is_init_ = true;

   return 0;
}

void CDNNClassifier::SetMean(const std::string& mean_file)
{
   if (mean_file.empty())
   {
      cv::Scalar channel_mean = {0, 0, 0, 0};
      mean_ = cv::Mat(input_geometry_, 21, channel_mean);
      return;
   }
   else
   {
      std::shared_ptr<caffe::Blob> mean_blob = caffe::ReadBlobFromFile(mean_file);

      /* The format of the mean file is planar 32-bit float BGR or grayscale. */
      std::vector<cv::Mat> channels;
      float* data = mean_blob->mutable_cpu_data();
      for (int i = 0; i < num_channels_; ++i) {
         /* Extract an individual channel. */
         cv::Mat channel(mean_blob->height(), mean_blob->width(), CV_32FC1, data);
         channels.push_back(channel);
         data += mean_blob->height() * mean_blob->width();
      }

      /* Merge the separate channels into a single image. */
      cv::Mat mean;
      cv::merge(channels, mean);

      /* Compute the global mean pixel value and create a mean image
      * filled with this value. */
      cv::Scalar channel_mean = cv::mean(mean);
      mean_ = cv::Mat(input_geometry_, mean.type(), channel_mean);
   }
}

void CDNNClassifier::SetLabel(const std::string& label_file)
{
   std::ifstream labels(label_file);
   if (labels)
   {
      std::string line;
      while (std::getline(labels, line))
         labels_.push_back(std::string(line));
   }
}

void CDNNClassifier::WrapInputLayer(std::vector<cv::Mat>* input_channels, int n)
{
   std::shared_ptr<caffe::Blob> input_layer = net_->blob_by_name(net_->blob_names().front());
   int width = input_layer->width();
   int height = input_layer->height();
   int channels = input_layer->channels();
   float* input_data = input_layer->mutable_cpu_data() + n * width * height * channels;
   for (int i = 0; i < channels; ++i)
   {
      cv::Mat channel(height, width, CV_32FC1, input_data);
      input_channels->push_back(channel);
      input_data += width * height;
   }
}

void CDNNClassifier::Preprocess(const cv::Mat& img, std::vector<cv::Mat>* input_channels)
{
   cv::Mat sample;
   if (img.channels() == 3 && num_channels_ == 1)
      cv::cvtColor(img, sample, cv::COLOR_BGR2GRAY);
   else if (img.channels() == 4 && num_channels_ == 1)
      cv::cvtColor(img, sample, cv::COLOR_BGRA2GRAY);
   else if (img.channels() == 4 && num_channels_ == 3)
      cv::cvtColor(img, sample, cv::COLOR_BGRA2BGR);
   else if (img.channels() == 1 && num_channels_ == 3)
      cv::cvtColor(img, sample, cv::COLOR_GRAY2BGR);
   else
      sample = img;

   cv::Mat sample_resized;
   if (sample.size() != input_geometry_)
      cv::resize(sample, sample_resized, input_geometry_);
   else
      sample_resized = sample;

   cv::Mat sample_float;
   if (num_channels_ == 3)
      sample_resized.convertTo(sample_float, CV_32FC3);
   else
      sample_resized.convertTo(sample_float, CV_32FC1);

   cv::Mat sample_normalized;
   cv::subtract(sample_float, mean_, sample_normalized);
   cv::split(sample_normalized, *input_channels);
   
   //CHECK(reinterpret_cast<float*>(input_channels->at(0).data)
   //      == net_->input_blobs()[0]->cpu_data())
   //   << "Input channels are not wrapping the input layer of the network.";
}

int CDNNClassifier::Predict(const cv::Mat& image, std::vector<float>& result)
{
   if (image.empty())
      return -1;

   std::shared_ptr<caffe::Blob> input_layer = net_->blob_by_name(net_->blob_names().front());
   input_layer->Reshape(1,
                        num_channels_,
                        input_geometry_.height,
                        input_geometry_.width);

   net_->Reshape();

   std::vector<cv::Mat> input_channels;
   WrapInputLayer(&input_channels, 0);
   Preprocess(image, &input_channels);

   net_->Forward();

   std::shared_ptr<caffe::Blob> output_layer = net_->blob_by_name(net_->blob_names().back());
   const float* begin = output_layer->cpu_data();
   const float* end = begin + output_layer->channels();
   result.insert(result.begin(), begin, end);
   
   return 0;
}

int CDNNClassifier::Predict(const std::vector<cv::Mat>& images, std::vector<std::vector<float> >& result)
{
   for (auto& image : images)
   {
      if (image.empty())
         return -1;
   }

   int nImages = (int)images.size();
   std::shared_ptr<caffe::Blob> input_layer = net_->blob_by_name(net_->blob_names().front());
   input_layer->Reshape(nImages,
                        num_channels_,
                        input_geometry_.height,
                        input_geometry_.width);

   net_->Reshape();

   for (int i = 0; i < nImages; ++i)
   {
      std::vector<cv::Mat> input_channels;
      WrapInputLayer(&input_channels, i);
      Preprocess(images[i], &input_channels);
   }
   net_->Forward();

   std::shared_ptr<caffe::Blob> output_layer = net_->blob_by_name(net_->blob_names().back());
   std::vector <std::vector<float> > ret;
   for (int i = 0; i < nImages; i++)
   {
      const float* begin = output_layer->cpu_data() + i*output_layer->channels();
      const float* end = begin + output_layer->channels();
      result.push_back(std::vector<float>(begin, end));
   }

   return 0;
}

int CDNNClassifier::Classify(const cv::Mat& image, int N, std::vector<Prediction>& pred)
{
   if (image.empty())
      return -1;

   std::vector<float> output;
   if (Predict(image, output))
      return 1;

   std::vector<Prediction> all_predictions;

   N = std::min<int>((int)labels_.size(), N);
   std::vector<int> maxN = Argmax(output, N);
   std::vector<Prediction> predictions;
   for (int i = 0; i < N; ++i)
   {
      int idx = maxN[i];
      pred.push_back(std::make_pair(labels_[idx], output[idx]));
   }

   return 0;
}

int CDNNClassifier::Classify(const std::vector<cv::Mat>& images, int N, std::vector<std::vector<Prediction> >& pred)
{
   for (auto& image : images)
   {
      if (image.empty())
         return -1;
   }

   std::vector<std::vector<float> > outputs;
   if (Predict(images, outputs))
      return 1;

   std::vector<std::vector<Prediction> > all_predictions;
   for (int j = 0; j < outputs.size(); ++j)
   {
      std::vector<float> output = outputs[j];

      N = std::min<int>((int)labels_.size(), N);
      std::vector<int> maxN = Argmax(output, N);
      std::vector<Prediction> predictions;
      for (int i = 0; i < N; ++i)
      {
         int idx = maxN[i];
         predictions.push_back(std::make_pair(labels_[idx], output[idx]));
      }
      pred.push_back(predictions);
   }

   return 0;
}

void CDNNClassifier::GetInputSize(int& width, int& height)
{
   width = input_geometry_.width;
   height = input_geometry_.height;
}
#endif

///////////////////////////////////////////////////////////////////////////////
//
// CaffeClassifier
//
///////////////////////////////////////////////////////////////////////////////

CLASSIFIER_API void* Initialize(const std::string& model_file,
                                const std::string& trained_file,
                                const std::string& mean_file,
                                const std::string& label_file,
                                const int flag_use_gpu,
                                const int gpu_no)
{
   CDNNClassifier* classifier = new CDNNClassifier;
   if (classifier->Initialize(model_file,
                              trained_file,
                              mean_file,
                              label_file,
                              flag_use_gpu,
                              gpu_no))
      return nullptr;

   return (void*)classifier;
}

CLASSIFIER_API void Uninitialize(void* hClassifier)
{
   CDNNClassifier* classifier = (CDNNClassifier*)hClassifier;
   if (classifier)
   {
      delete classifier;
      classifier = nullptr;
   }
}

CLASSIFIER_API void Predict(void* hClassifier,
             const cv::Mat& image,
             std::vector<float>& output)
{
   CDNNClassifier* classifier = (CDNNClassifier*)hClassifier;
   if (classifier == nullptr)
      return;

   if (!classifier->IsInitialized())
      return;

   if (image.empty())
      return;

   classifier->Predict(image, output);
}

CLASSIFIER_API void PredictBatch(void* hClassifier,
                  const std::vector<cv::Mat>& images,
                  std::vector<std::vector<float> >& outputs)
{
   CDNNClassifier* classifier = (CDNNClassifier*)hClassifier;
   if (classifier == nullptr)
      return;

   if (!classifier->IsInitialized())
      return;

   if (images.size() == 0)
      return;

   classifier->Predict(images, outputs);
}

CLASSIFIER_API int Classify(void* hClassifier,
             const cv::Mat& image,
             std::vector<Prediction>& prediction)
{
   CDNNClassifier* classifier = (CDNNClassifier*)hClassifier;
   if (classifier == nullptr)
      return -1;

   if (!classifier->IsInitialized())
      return 1;

   if (image.empty())
      return -2;

   classifier->Classify(image, 1, prediction);

   return 0;
}

CLASSIFIER_API int ClassifyBatch(void* hClassifier,
                  const std::vector<cv::Mat>& images,
                  std::vector<std::vector<Prediction> >& predictions)
{
   CDNNClassifier* classifier = (CDNNClassifier*)hClassifier;
   if (classifier == nullptr)
      return -1;

   if (!classifier->IsInitialized())
      return 1;

   if (images.size() == 0)
      return -2;

   for (auto& image : images)
   {
      if (image.empty())
         return -3;
   }

   classifier->Classify(images, (int)images.size(), predictions);

   return 0;
}

CLASSIFIER_API void GetInputSize(void* hClassifier, int& width, int& height)
{
   CDNNClassifier* classifier = (CDNNClassifier*)hClassifier;
   if (classifier == nullptr)
      return;

   if (!classifier->IsInitialized())
      return;

   classifier->GetInputSize(width, height);
}

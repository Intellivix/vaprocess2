#if !defined(__VACL_EVENT_H)
#define __VACL_EVENT_H
 
#include "vacl_abdobj.h"
#include "vacl_crowd.h"
#include "vacl_dwell.h"
#include "vacl_evtzone.h"
#include "vacl_geometry.h"
#include "vacl_pathpass.h"
#include "vacl_schedule.h"
#include "vacl_thumbcap.h"
#include "vacl_traffic.h"
#include "vacl_water.h"

 namespace VACL

{

class EventDetection;

///////////////////////////////////////////////////////////////////////////////
//
// Class: Event
//
///////////////////////////////////////////////////////////////////////////////

#define EV_STATUS_BEGUN           0x00000001
#define EV_STATUS_ONGOING         0x00000002
#define EV_STATUS_ENDED           0x00000004
#define EV_STATUS_AFTER_ENDED     0x00000008
#define EV_STATUS_TO_BE_REMOVED   0x00000010

 class Event

{
   public:
      int   ID;             // �̺�Ʈ ID
      int   Type;           // �̺�Ʈ ���� (ER_EVENT_XXX ���ǹ� ���� )
      int   Status;         // �̺�Ʈ ���� (EV_STATUS_XXX ���ǹ� ����)
      int   NumObjects;     // �̺�Ʈ�� �߻���Ų ��ü ��
      float BonusTime;      // �̺�Ʈ ���� �� ���ʽ��� �־��� �ð�
      float ElapsedTime;    // �̺�Ʈ ��� �ð�

   public:
      Time StartTime;       // �̺�Ʈ ���� �ð�
      Time EndTime;         // �̺�Ʈ ���� �ð�

   public:
      int      ThumbPixFmt; // ����� �̹��� �ȼ� ����
      ISize2D  ThumbSize;   // ����� �̹��� ũ��
      BArray1D ThumbBuffer; // ����� �̹��� ����
      
   public:
      EventRule* EvtRule;   // ���� �̺�Ʈ ��Ģ
                            // WARNING: ��쿡 ���� Type ���� EvtRule->EventType ���� �ٸ� �� ����.
                            //          �̺�Ʈ�� ������ Ȯ���� ���� Type ���� ����� ��!
      EventZone* EvtZone;   // ���� �̺�Ʈ ��

   public:
      Event* Prev;
      Event* Next;
      
   public:
      Event (EventZone *evt_zone,EventRule *evt_rule,int evt_type = -1);
      virtual ~Event (   ) {   };
};

typedef LinkedList<Event> EventList;

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventDetection
//
///////////////////////////////////////////////////////////////////////////////

 class EventDetection

{
   protected:
      int EZ_Abandoned;
      int EZ_CarAccident;
      int EZ_CarParking;
      int EZ_ColorChange;
      int EZ_Removed;

   protected:
      float               CurrentTime;
      ObjectIDGeneration* ObjIDGenerator;

   protected:
      USArray2D           BkgMaskMap;
      AbandonedObjectList AbandonedObjects;
   
   // Input (Set directly)
   public:
      int   Flag_CheckSchedule; // ������ �Ͻ� ������ ���� ����
      float BkgPeriod;          // ���ȭ ���� �ð�
 
   // Input (Set indirectly)
   public:
      float    FrameRate;
      float    FrameDuration;
      ISize2D  SrcImgSize;
      FPoint2D Shift;
   
   // Input (Set directly)
   public:
      EventRuleList EventRules;
      EventZoneList EventZones;

   // Output
   public:
      TrackedObjectList EvtGenObjects;
      
   public:
      EventDetection (   );
      virtual ~EventDetection (   );
      
   protected:
      void _Init                 (   );
      void FinalizeEvents        (ObjectTracking& obj_tracker);
      void FinalizeEvents        (TrackedObjectList& to_list);
      void UpdateEventThumbnails (TrackedObjectList& to_list,byte* si_buffer,ISize2D& si_size,int pix_fmt,float cbs_ratio,ISize2D* tn_size);
   
   public:
      virtual int  CheckSetup (EventDetection* ed);
      virtual void Close      (   );
      virtual int  ReadFile   (FileIO& file);
      virtual int  ReadFile   (CXMLIO* pIO);
      virtual int  WriteFile  (FileIO& file);
      virtual int  WriteFile  (CXMLIO* pIO);

   public:
      int  CheckObjectsAbandonedStart (ObjectTracking& obj_tracker,EventZone* evt_zone);
      void ManageAbandonedObjects     (ObjectTracking& obj_tracker,TrackedObject* t_object,float det_time,int to_evt_start,int to_event);
      int  GetNewObjectID             (   );
      int  IsInsideBkgMask            (IBox2D& s_box,int range = 5);
      void PutBkgMask                 (TrackedObject* t_object,int shift = 10);
      void PutBkgMask                 (TrackedBlob* t_blob,BlobTracking& blb_tracker);
      void PutRectBkgMask             (TrackedBlob* t_blob);

   public:
      void CleanEvtGenObjectList      (   );
      void ClearEventRuleList         (   );
      void ClearEventZoneList         (   );
      void GetBkgModelUpdateScheme    (GImage& d_image);
      int  Initialize                 (ISize2D& si_size,float frm_rate,ObjectIDGeneration& id_generator);
      int  IsEventZoneAvailable       (int evt_type,int flag_check_enabled = FALSE,int flag_only_one_type = FALSE);
      void LinkEvtZonesToEvtRules     (   );
      int  Perform                    (ObjectTracking& obj_tracker);
      int  Perform                    (ColorChangeDetection& clc_detector);
      int  Perform                    (CrowdDensityEstimation& crd_estimator);
      int  Perform                    (DwellAnalysis& dwl_analyzer);
      int  Perform                    (FlameDetection& flm_detector);
      int  Perform                    (GroupTracking&  grp_tracker);
      int  Perform                    (SmokeDetection& smk_detector);
      int  Perform                    (TrafficCongestionDetection& trc_detector);
      int  Perform                    (WaterLevelDetection& wtl_detector);
      int  Perform                    (ZoneColorDetection& znc_detector);
      void Reset                      (   );
      void ResetObjectCounters        (   );
      void UpdateAreaMaps             (ISize2D& ri_size);
      void UpdateEventThumbnails      (byte* si_buffer,ISize2D& si_size,int pix_fmt = VAE_PIXFMT_YUY2,float cbs_ratio = 1.5f,ISize2D* tn_size = NULL);
      void UpdateEventThumbnails      (ObjectTracking& obj_tracker,byte* si_buffer,ISize2D& si_size,int pix_fmt = VAE_PIXFMT_YUY2,float cbs_ratio = 1.5f,ISize2D* tn_size = NULL);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: AlarmTriggerHandling
//
///////////////////////////////////////////////////////////////////////////////

 class AlarmTriggerHandling

{
   protected:
      int   Flag_Alarm;
      int   Flag_Event;
      float CurrentTime;
      float LstEvtTime;
      float AlmElpTime;

   public:
      float AlarmDuration;
      float EvtIgnPeriod;
   
   public:
      AlarmTriggerHandling (   );

   public:
      void Initialize (   );
      int  Perform    (ObjectTracking& obj_tracker,TrackedObject** p_t_object);
};

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

Event* CreateInstance_Event      (EventZone *evt_zone,EventRule *evt_rule,int evt_type = -1);
void   SetCreationFunction_Event (Event*(*func)(EventZone *evt_zone,EventRule *evt_rule,int evt_type));
void   SetDisabledEventType      (int event_type);
   
}

#endif

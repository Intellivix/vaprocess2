#if !defined(__VACL_CAM2CAM_H)
#define __VACL_CAM2CAM_H

#include "vacl_geometry.h"
#include "vacl_optimize.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: PTZPosition
//
///////////////////////////////////////////////////////////////////////////////

 class PTZPosition
 
{
   public:
      float PanAngle;
      float TiltAngle;
      float ZoomFactor;
      
   public:
      PTZPosition (   ) { PanAngle = TiltAngle = 0.0f; ZoomFactor = 1.0f; };
      PTZPosition (float pa,float ta,float zf) { PanAngle = pa, TiltAngle = ta, ZoomFactor = zf; };

   public:
      int  operator != (PTZPosition& pos) const;
      void operator () (float pa,float ta,float zf) { PanAngle = pa, TiltAngle = ta, ZoomFactor = zf; };

   public:
      int ReadFile  (CXMLIO* pIO);
      int WriteFile (CXMLIO* pIO);
};

typedef Array1D<PTZPosition> PTZPArray1D;

///////////////////////////////////////////////////////////////////////////////
//
// Class: MasterToSlaveCamera
//
///////////////////////////////////////////////////////////////////////////////

 class MasterToSlaveCamera : public Minimization_Powell
 
{
   public: // Input Parameters
           // Calibrate() 함수 호출 전에 반드시 적당한 값으로 설정되어 있어야 한다.
      ISize2D     ImageSize;    // 마스터 카메라 영상의 크기
      FP2DArray1D CalibPoints;  // 마스터 카메라 영상에서 캘리브레이션 포인트들의 좌표
                                // 최소 6개의 캘리브레이션 포인트가 주어져야 한다.
                                // CalibPoints[0]은 가급적이면 마스터 카메라 영상의 중심과 가까운 포인트로 설정한다.
      PTZPArray1D PTZPositions; // 캘리브레이션 포인트들에 대응하는 슬래이브 PTZ 카메라의 PTZ 좌표
      
   public: // Input Parameters (Optional)
      float MCTiltPos;          // 파노라마 카메라를 구성하는 컴포넌트 카메라의 틸트 위치 (단위: 도, 기본값: 0)
      float MinPanAngle;        // 슬래이브 PTZ 카메라의 팬 각의 최소치   (단위: 도, 기본값:   0)
      float MaxPanAngle;        // 슬래이브 PTZ 카메라의 팬 각의 최대치   (단위: 도, 기본값: 360)
      float MinTiltAngle;       // 슬래이브 PTZ 카메라의 틸트 각의 최소치 (단위: 도, 기본값: -90)
      float MaxTiltAngle;       // 슬래이브 PTZ 카메라의 틸트 각의 최대치 (단위: 도, 기본값:  90)

   public: // Input/Output Parameters
           // Calibrate() 함수 호출 전에 적당한 초기값으로 설정될 필요가 있다.
           // Calibrate() 함수 호출 후에 값이 변경된다.
      float    FocalLength;     // 마스터 카메라의 초점 거리 (단위: 픽셀, 기본값: 500)
      float    HFOV;            // 마스터 카메라의 수평 화각 (단위: 도)
                                // 마스터 카메라가 일반 카메라인 경우에는 0으로 설정하고,
                                // 파노라마 카메라인 경우에는 파노라마 카메라의 수평 화각 값으로 설정한다.
      float    PanAngleOffset;  // 슬래이브 PTZ 카메라의 팬 각의 오프셋   (단위: 도, 기본값: 0)
      float    TiltAngleOffset; // 슬래이브 PTZ 카메라의 틸트 각의 오프셋 (단위: 도, 기본값: 0)
      FPoint3D RotationAngles;  // 참조 카메라 좌표계와 PTZ 카메라 좌표계 사이의 회전 변환 관계를 나타내는 회전각들 (단위: 도, 기본값: 0)
                                // X: Rolling Angle, Y: Panning Angle, Z: Tilting Angle
                                // Y 값은 Calibrate() 함수 호출 후에도 변경되지 않는다.
   public:
      MasterToSlaveCamera *Prev;
      MasterToSlaveCamera *Next;
   
   public:
      MasterToSlaveCamera (   );
      
   protected:
      virtual double CostFunction (Matrix &vec_p);
      
   protected:
      PTZPosition GetPTPosition (FPoint2D &s_point,Matrix &vec_p);
      
   public:
      int Calibrate (   );
         // 카메라 보정을 수행한다.
         // 본 함수를 호출하기 전에 Input Parameter들이 적당한 값으로 설정되어 있어야 한다.
         // 리턴 값: 정상적으로 수행되면 0을 리턴한다.
      void Close (   );
         // 캘리브레이션 포인트와 PTZ 좌표 데이터 저장을 위한 버퍼를 해제한다.
      FPoint2D GetPixelPosition (PTZPosition &s_pos);
         // 주어진 슬래이브 PTZ 카메라의 PTZ 좌표로부터 그것에 대응하는 마스터 카메라 영상에서의 픽셀 좌표를 구한다.
         // 본 함수가 호출되기 전에 Input Parameter들이 적당한 값으로 설정되어 있어야 한다.
         // s_pos[IN]: 슬래이브 PTZ 카메라의 PTZ 좌표
         // 리턴 값  : 대응하는 마스터 카메라 영상에서의 픽셀 좌표
      PTZPosition GetPTPosition (FPoint2D &s_point);
         // 주어진 마스터 카메라 영상의 픽셀 좌표로부터 그것에 대응하는 슬래이브 PTZ 카메라의 PT 좌표를 구한다.
         // 본 함수가 호출되기 전에 Input Parameter들이 적당한 값으로 설정되어 있어야 한다.
         // s_point[IN]: 마스터 카메라 영상에서의 픽셀 좌표
         // 리턴 값    : 대응하는 슬래이브 PTZ 카메라의 PTZ 좌표
      void Initialize (int mci_width,int mci_height,int n_calib_points);
         // 마스터 카메라 영상의 크기 설정 및 카메라 보정을 위한 캘리브레이션 포인트 & PTZ 포지션 데이터를 저장하기 위한 버퍼를 할당한다.
         // ReadFile() 함수를 호출하였을 경우에는 본 함수를 호출할 필요가 없다.
         // mci_width     [IN]: 마스터 카메라 영상의 너비
         // mci_height    [IN]: 마스터 카메라 영상의 높이
         // n_calib_points[IN]: 캘리브레이션 포인트 수
      int ReadFile  (FileIO &file);
      int ReadFile  (CXMLIO* pIO);
      int WriteFile (FileIO &file);
      int WriteFile (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: MasterToSlaveCamera2
//
///////////////////////////////////////////////////////////////////////////////

 class MasterToSlaveCamera2 : public Minimization_Powell
 
{
   public: // Input Parameters
           // Calibrate() 함수 호출 전에 반드시 적당한 값으로 설정되어 있어야 한다.
      ISize2D     ImageSize;    // 마스터 카메라 영상의 크기
      FP2DArray1D CalibPoints;  // 마스터 카메라 영상에서 캘리브레이션 포인트들의 좌표
                                // 최소 6개의 캘리브레이션 포인트가 주어져야 한다.
                                // CalibPoints[0]은 가급적이면 마스터 카메라 영상의 중심과 가까운 포인트로 설정한다.
      PTZPArray1D PTZPositions; // 캘리브레이션 포인트들에 대응하는 슬래이브 PTZ 카메라의 PTZ 좌표
      
   public: // Input Parameters (Optional)
      float MCTiltPos;          // 파노라마 카메라를 구성하는 컴포넌트 카메라의 틸트 위치 (단위: 도, 기본값: 0)
      float MinPanAngle;        // 슬래이브 PTZ 카메라의 팬 각의 최소치   (단위: 도, 기본값:   0)
      float MaxPanAngle;        // 슬래이브 PTZ 카메라의 팬 각의 최대치   (단위: 도, 기본값: 360)
      float MinTiltAngle;       // 슬래이브 PTZ 카메라의 틸트 각의 최소치 (단위: 도, 기본값: -90)
      float MaxTiltAngle;       // 슬래이브 PTZ 카메라의 틸트 각의 최대치 (단위: 도, 기본값:  90)

   public: // Input/Output Parameters
           // Calibrate() 함수 호출 전에 적당한 초기값으로 설정될 필요가 있다.
           // Calibrate() 함수 호출 후에 값이 변경된다.
      float FocalLength;        // 마스터 카메라의 초점 거리 (단위: 픽셀, 기본값: 500)
      float HFOV;               // 마스터 카메라의 수평 화각 (단위: 도)
                                // 마스터 카메라가 일반 카메라인 경우에는 0으로 설정하고,
                                // 파노라마 카메라인 경우에는 파노라마 카메라의 수평 화각 값으로 설정한다.
      FPoint3D RotationAngles;  // 참조 카메라 좌표계와 PTZ 카메라 좌표계 사이의 회전 변환 관계를 나타내는 회전각들 (단위: 도, 기본값: 0)
                                // X: Rolling Angle, Y: Panning Angle, Z: Tilting Angle
   public:
      MasterToSlaveCamera2 *Prev;
      MasterToSlaveCamera2 *Next;
   
   public:
      MasterToSlaveCamera2 (   );
      
   protected:
      virtual double CostFunction (Matrix &vec_p);
      
   protected:
      PTZPosition GetPTPosition (FPoint2D &s_point,Matrix &vec_p);
      
   public:
      int Calibrate (   );
         // 카메라 보정을 수행한다.
         // 본 함수를 호출하기 전에 Input Parameter들이 적당한 값으로 설정되어 있어야 한다.
         // 리턴 값: 정상적으로 수행되면 0을 리턴한다.
      void Close (   );
         // 캘리브레이션 포인트와 PTZ 좌표 데이터 저장을 위한 버퍼를 해제한다.
      FPoint2D GetPixelPosition (PTZPosition &s_pos);
         // 주어진 슬래이브 PTZ 카메라의 PTZ 좌표로부터 그것에 대응하는 마스터 카메라 영상에서의 픽셀 좌표를 구한다.
         // 본 함수가 호출되기 전에 Input Parameter들이 적당한 값으로 설정되어 있어야 한다.
         // s_pos[IN]: 슬래이브 PTZ 카메라의 PTZ 좌표
         // 리턴 값  : 대응하는 마스터 카메라 영상에서의 픽셀 좌표
      PTZPosition GetPTPosition (FPoint2D &s_point);
         // 주어진 마스터 카메라 영상의 픽셀 좌표로부터 그것에 대응하는 슬래이브 PTZ 카메라의 PT 좌표를 구한다.
         // 본 함수가 호출되기 전에 Input Parameter들이 적당한 값으로 설정되어 있어야 한다.
         // s_point[IN]: 마스터 카메라 영상에서의 픽셀 좌표
         // 리턴 값    : 대응하는 슬래이브 PTZ 카메라의 PTZ 좌표
      void Initialize (int mci_width,int mci_height,int n_calib_points);
         // 마스터 카메라 영상의 크기 설정 및 카메라 보정을 위한 캘리브레이션 포인트 & PTZ 포지션 데이터를 저장하기 위한 버퍼를 할당한다.
         // ReadFile() 함수를 호출하였을 경우에는 본 함수를 호출할 필요가 없다.
         // mci_width     [IN]: 마스터 카메라 영상의 너비
         // mci_height    [IN]: 마스터 카메라 영상의 높이
         // n_calib_points[IN]: 캘리브레이션 포인트 수
      int ReadFile  (FileIO &file);
      int ReadFile  (CXMLIO* pIO);
      int WriteFile (FileIO &file);
      int WriteFile (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: CameraToCamera
//
///////////////////////////////////////////////////////////////////////////////

 class CameraToCamera
 
{
   public:
      char Name[128];
      // 카메라 간의 좌표 변환 관계를 나타내는 호모그래피 행렬
      Matrix Mat_H;
      
   public:
      CameraToCamera (   );
      virtual ~CameraToCamera (   ) {   };
      
   public:
      // 캘리브레이션을 수행한다.
      virtual int Calibrate (   ) = 0;
      
   public:
      // 관련 데이터를 파일로부터 읽어 온다.
      virtual int ReadFile  (FileIO &file);
      virtual int ReadFile  (CXMLIO* pIO);
      // 관련 데이터를 파일에 저장한다.
      virtual int WriteFile (FileIO &file);
      virtual int WriteFile (CXMLIO* pIO);
      
   public:
      // 주어진 P/T 포지션에 해당하는 방향 벡터를 구한다.
      FPoint3D GetDirectionVector   (PTZPosition &s_pos);
      Matrix   GetPolygon           (PTZPArray1D &sp_array,BCCL::Polygon &d_polygon);
      IPoint2D GetProjectedPosition (PTZPosition &s_pos,Matrix &mat_R,float focal_length = 1000.0f);
      // 주어진 방향 벡터에 해당하는 P/T 포지션을 구한다.
      PTZPosition GetPTPosition     (FPoint3D &vec_s);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: StaticToStaticCamera
//
///////////////////////////////////////////////////////////////////////////////

 class StaticToStaticCamera : public CameraToCamera
 
{
   public:
      FP2DArray1D CalibPoints1;
      FP2DArray1D CalibPoints2;
      
   public:
      StaticToStaticCamera *Prev;
      StaticToStaticCamera *Next;
   
   public:
      virtual ~StaticToStaticCamera (   ) {   };
   
   public:
      virtual int Calibrate (   );
      virtual int ReadFile  (FileIO &file);
      virtual int ReadFile  (CXMLIO* pIO);
      virtual int WriteFile (FileIO &file);
      virtual int WriteFile (CXMLIO* pIO);
      
   public:
      void     Close                (   );
      FPoint2D GetHandoffPosition12 (FPoint2D &s_point);
      FPoint2D GetHandoffPosition21 (FPoint2D &s_point);
      void     Initialize           (int n_calib_points);
};

typedef LinkedList<StaticToStaticCamera> StaticToStaticCamList;

///////////////////////////////////////////////////////////////////////////////
//
// Class: StaticToPTZCamera
//
///////////////////////////////////////////////////////////////////////////////

 class StaticToPTZCamera : public CameraToCamera
 
{
   public:
      FP2DArray1D CalibPoints;
      PTZPArray1D PTZPositions;

   public:
      StaticToPTZCamera *Prev;
      StaticToPTZCamera *Next;

   public:
      virtual ~StaticToPTZCamera (   ) {   };

   public:
      virtual int Calibrate (   );
      virtual int ReadFile  (FileIO &file);
      virtual int ReadFile  (CXMLIO* pIO);
      virtual int WriteFile (FileIO &file);
      virtual int WriteFile (CXMLIO* pIO);
      
   public:
      void        Close                (   );
      PTZPosition GetHandoffPosition12 (FPoint2D &s_point);
      FPoint2D    GetHandoffPosition21 (PTZPosition &s_pos);
      void        Initialize           (int n_calib_points);
};

typedef LinkedList<StaticToPTZCamera> StaticToPTZCamList;

///////////////////////////////////////////////////////////////////////////////
//
// Class: PTZToPTZCamera
//
///////////////////////////////////////////////////////////////////////////////

 class PC_PTZToPTZCam

{
   // 특정 Ground Point에 대응하는 두 PTZ 카메라의 PTZ 포지션들
   public:
      // 첫 번째 PTZ 카메라의 PTZ 포지션
      PTZPosition PTZPos1;
      // 두 번째 PTZ 카메라의 PTZ 포지션
      PTZPosition PTZPos2;
      
   public:
      PC_PTZToPTZCam *Prev;
      PC_PTZToPTZCam *Next;

   public:
      // 관련 데이터를 파일로부터 읽어 온다.
      int ReadFile (CXMLIO* pIO);
      // 관련 데이터를 파일로 저장한다.
      int WriteFile (CXMLIO* pIO);
};

 class PTZToPTZCamera : public CameraToCamera 
 // Handoff Area에 해당
{
   protected:
      Matrix  Mat_R;
      BCCL::Polygon HandoffArea;
      
   public:
      // GPC 목록
      LinkedList<PC_PTZToPTZCam> PCList;

   public:
      float HandoffZoomFactor;
      
   public:
      PTZToPTZCamera *Prev;
      PTZToPTZCamera *Next;
      
   public:
      PTZToPTZCamera (   );
      virtual ~PTZToPTZCamera (   ) {   };

   public:
      // 주어진 GPC 목록으로부터 캘리브레이션을 수행한다.
      virtual int Calibrate (   );
      // 관련 데이터를 파일로부터 읽어 온다.
      virtual int ReadFile  (FileIO &file);
      virtual int ReadFile  (CXMLIO* pIO);
      // 관련 데이터를 파일에 저장한다.
      virtual int WriteFile (FileIO &file);
      virtual int WriteFile (CXMLIO* pIO);
      
   public:
      void Close (   );
      // 첫 번째 PTZ 카메라의 특정 PTZ 좌표에 대응하는 두 번째 PTZ 카메라의 PTZ 좌표 값을 구한다.
      PTZPosition GetHandoffPosition12 (PTZPosition &s_pos);
      // 두 번째 PTZ 카메라의 특정 PTZ 좌표에 대응하는 첫 번째 PTZ 카메라의 PTZ 좌표 값을 구한다.
      PTZPosition GetHandoffPosition21 (PTZPosition &s_pos);
      int IsPositionInsideHandoffArea (PTZPosition &s_pos);
      int SetHandoffArea (   );
};

typedef LinkedList<PTZToPTZCamera> PTZToPTZCamList;

///////////////////////////////////////////////////////////////////////////////
//
// Class: HandoffInfo_PTZToPTZCam
//
///////////////////////////////////////////////////////////////////////////////

 class HandoffInfo_PTZToPTZCam : public PTZToPTZCamList
 // VPU to VPU 공조감시 정보
 // 여러 개의 Handoff Area를 갖는다.
{
   public:
      char Name[128];
      byte IPAddress[4];
      
   public:
      HandoffInfo_PTZToPTZCam *Prev;
      HandoffInfo_PTZToPTZCam *Next;
      
   public:
      HandoffInfo_PTZToPTZCam (   );
      virtual ~HandoffInfo_PTZToPTZCam (   ) {   };
   
   public:
      PTZToPTZCamera *GetFirstItem (   );
      PTZToPTZCamera *GetNextItem  (PTZToPTZCamera *item);
   
   public:
      int Calibrate            (   );
      int GetHandoffPosition12 (PTZPosition &s_pos,PTZPosition &d_pos);
      int ReadFile             (FileIO &file);
      int ReadFile             (CXMLIO* pIO);
      int SetHandoffAreas      (   );
      int WriteFile            (FileIO &file);
      int WriteFile            (CXMLIO* pIO);
};

 inline PTZToPTZCamera* HandoffInfo_PTZToPTZCam::GetFirstItem (   )

{
   return (PTZToPTZCamList::First (   ));
}
 
 inline PTZToPTZCamera* HandoffInfo_PTZToPTZCam::GetNextItem (PTZToPTZCamera *item)

{
   return (PTZToPTZCamList::Next (item));
}

}

#endif

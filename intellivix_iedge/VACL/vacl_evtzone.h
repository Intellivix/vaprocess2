#if !defined(__VACL_EVTZONE_H)
#define __VACL_EVTZONE_H

#include "vacl_color.h"
#include "vacl_evtrule.h"
#include "vacl_fire.h"

 namespace VACL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventZone
//
///////////////////////////////////////////////////////////////////////////////

 class EventZone : public Polygon

{
   public:
      int        EvtRuleID;
      char       Name[128];
      ISize2D    RefImgSize;
      FPoint2D   BasePos;
      FPoint2D   Scale; // Scale = RefImgSize / SrcImgSize
      EventRule* EvtRule;

   // CrowdDensityEstimation_FPD 수행 시 유효한 값을 갖음
   public:
      int CurCrowdCount; // 현재 존 내의 군중 수

   // DwellAnalysis 수행 시 유효한 값을 갖음
   public:
      int   CurDwellObjectCount;   // 현재 존에 체류 중인 객체 수
      int   TotalDwellObjectCount; // 현재까지 존을 방문한 총 객체 수
      float AverageDwellTime;      // 현재까지 존을 떠난 객체들의 평균 체류 시간
   
   public:
      EventZone* Prev;
      EventZone* Next;
      
   public:
      EventZone (   );
      virtual ~EventZone (   );

   public:
      virtual int CheckSetup (EventZone* ez);
      virtual int ReadFile   (FileIO& file);
      virtual int ReadFile   (CXMLIO* pIO);
      virtual int WriteFile  (FileIO& file);
      virtual int WriteFile  (CXMLIO* pIO);

   public:
      int  CheckObjectInsideZone   (TrackedObject* t_object); // [주의] 분석 영상 해상도 기준
      int  CheckPositionInsideZone (FPoint2D& s_pos);         // [주의] 분석 영상 해상도 기준
      int  CheckPositionInsideZone (IPoint2D& s_pos);         // [주의] 분석 영상 해상도 기준
      int  CheckRegionInsideZone   (IBox2D& s_rgn);           // [주의] 분석 영상 해상도 기준
      int  DetectEvents            (ObjectTracking& obj_tracker);
      int  DetectEvents            (GroupTracking& grp_tracker);
      int  DetectEvents            (FlameDetection& flm_detector);
      int  DetectEvents            (SmokeDetection& smk_detector);
      int  DetectEvents            (ColorChangeDetection& clc_detector);
      int  Enable                  (int flag_enable);
      int  IsEnabled               (   );
      void UpdateAreaMap           (FPoint2D &scale);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventZoneList
//
///////////////////////////////////////////////////////////////////////////////

 class EventZoneList : public PolygonList<EventZone>

{
   public:
      virtual int ReadFile (FileIO& file);
      virtual int ReadFile (CXMLIO* pIO, const char* szName);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EventZoneListManager
//
///////////////////////////////////////////////////////////////////////////////

 class EventZoneListManager
 
{
   protected:
      CCriticalSection CS_List;

   public:
      struct Item {
         EventZone *ItemPtr;
         Item *Prev,*Next;
      };
      LinkedList<Item> Items;

   public:
      virtual ~EventZoneListManager (   );
      
   protected:
      void Lock   (   );
      void Unlock (   );
      
   public:
      void Add      (EventZone *s_item);
      int  GetNewID (   );
      void Remove   (EventZone *s_item);
};

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

EventZone* CreateInstance_EventZone      (   );
void       SetCreationFunction_EventZone (EventZone*(*func)(   ));

}

#endif

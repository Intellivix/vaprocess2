#include "vacl_schedule.h"
 
 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Global Variables
//
///////////////////////////////////////////////////////////////////////////////

Period_HM Period_SunRiseSet;

///////////////////////////////////////////////////////////////////////////////
//
// Class: Period_HM
//
///////////////////////////////////////////////////////////////////////////////

 Period_HM::Period_HM (   )

{
   CheckMode = PERIOD_CHECK_MODE_START_END_TIME;
   Set (0,0,0);
   Set (1,23,59);
}
 
 Period_HM::~Period_HM (   )

{

}

 int Period_HM::ConvertTimeInMinute (int hour,int minute)

{
   int time = hour * 60 + minute;
   return (time);
}

 int Period_HM::IsSame (Period_HM& period_hm)

{
   FileIO fioThis, fioFrom;
   CXMLIO xmlThis (&fioThis, XMLIO_Write);
   CXMLIO xmlFrom (&fioFrom, XMLIO_Write);
   WriteFile (&xmlThis);
   period_hm.WriteFile (&xmlFrom);
   return !fioThis.IsDiff (fioFrom);
 }

 int Period_HM::IsTimeInsidePeriod (int hour,int minute)
 
{
   if (hour == -1 || minute == -1) return (FALSE);
   int time = ConvertTimeInMinute (hour,minute);
   int tm_in_min0, tm_in_min1;
   if (CheckMode == PERIOD_CHECK_MODE_DAY_TIME) {
      tm_in_min0 = Period_SunRiseSet.TimeInMinute[0];
      tm_in_min1 = Period_SunRiseSet.TimeInMinute[1];
   }
   else if (CheckMode == PERIOD_CHECK_MODE_NIGHT_TIME) {
      tm_in_min0 = Period_SunRiseSet.TimeInMinute[1];
      tm_in_min1 = Period_SunRiseSet.TimeInMinute[0];
   }
   else {
      tm_in_min0 = TimeInMinute[0];
      tm_in_min1 = TimeInMinute[1];
   }
   if (tm_in_min0 < tm_in_min1) {
      if (tm_in_min0 <= time && time <= tm_in_min1) return (TRUE);
      else return (FALSE);
   }
   else if (tm_in_min0 > tm_in_min1) {
      if (tm_in_min0 <= time || time <= tm_in_min1) return (TRUE);
      else return (FALSE);
   }
   else return (TRUE);
}

 int Period_HM::ReadFile (FileIO &file)

{
   if (file.Read ((byte *)Hour        ,1,sizeof(Hour        ))) return (1);
   if (file.Read ((byte *)Minute      ,1,sizeof(Minute      ))) return (1);
   if (file.Read ((byte *)TimeInMinute,1,sizeof(TimeInMinute))) return (1);
   return (DONE);
}

 void Period_HM::Set (int n,int hour,int minute)

{
   Hour[n]         = hour;
   Minute[n]       = minute;
   TimeInMinute[n] = ConvertTimeInMinute (hour,minute);
}

 int Period_HM::WriteFile (FileIO &file)
 
{
   if (file.Write ((byte *)Hour        ,1,sizeof(Hour        ))) return (1);
   if (file.Write ((byte *)Minute      ,1,sizeof(Minute      ))) return (1);
   if (file.Write ((byte *)TimeInMinute,1,sizeof(TimeInMinute))) return (1);
   return (DONE);
}

 int Period_HM::ReadFile (CXMLIO* pIO, const char* szNodeName)

{
   static Period_HM dv;

   CXMLNode xmlNode (pIO);
   if (!szNodeName) szNodeName = "Period_HM";
   if (xmlNode.Start (szNodeName)) {
      xmlNode.Attribute (TYPE_ID (int), "CheckMode"     , &CheckMode       , &dv.CheckMode);
      xmlNode.Attribute (TYPE_ID (int), "SHour"         , &Hour[0]         , &dv.Hour[0]);
      xmlNode.Attribute (TYPE_ID (int), "SMinute"       , &Minute[0]       , &dv.Minute[0]);
      xmlNode.Attribute (TYPE_ID (int), "STimeInMinute" , &TimeInMinute[0] , &dv.TimeInMinute[0]);
      xmlNode.Attribute (TYPE_ID (int), "EHour"         , &Hour[1]         , &dv.Hour[1]);
      xmlNode.Attribute (TYPE_ID (int), "EMinute"       , &Minute[1]       , &dv.Minute[1]);
      xmlNode.Attribute (TYPE_ID (int), "ETimeInMinute" , &TimeInMinute[1] , &dv.TimeInMinute[1]);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

 int Period_HM::WriteFile (CXMLIO* pIO, const char* szNodeName)

{
   static Period_HM dv;

   CXMLNode xmlNode (pIO);
   if (!szNodeName) szNodeName = "Period_HM";
   if (xmlNode.Start (szNodeName)) {
      xmlNode.Attribute (TYPE_ID (int), "CheckMode"     , &CheckMode       , &dv.CheckMode);
      xmlNode.Attribute (TYPE_ID (int), "SHour"         , &Hour[0]         , &dv.Hour[0]);
      xmlNode.Attribute (TYPE_ID (int), "SMinute"       , &Minute[0]       , &dv.Minute[0]);
      xmlNode.Attribute (TYPE_ID (int), "STimeInMinute" , &TimeInMinute[0] , &dv.TimeInMinute[0]);
      xmlNode.Attribute (TYPE_ID (int), "EHour"         , &Hour[1]         , &dv.Hour[1]);
      xmlNode.Attribute (TYPE_ID (int), "EMinute"       , &Minute[1]       , &dv.Minute[1]);
      xmlNode.Attribute (TYPE_ID (int), "ETimeInMinute" , &TimeInMinute[1] , &dv.TimeInMinute[1]);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: Period_YMD
//
///////////////////////////////////////////////////////////////////////////////

 Period_YMD::Period_YMD (   )

{
   ClearArray1D ((byte*)Year     ,1,sizeof(Year     ));
   ClearArray1D ((byte*)Month    ,1,sizeof(Month    ));
   ClearArray1D ((byte*)Day      ,1,sizeof(Day      ));
   ClearArray1D ((byte*)TimeInDay,1,sizeof(TimeInDay));
}

 Period_YMD::~Period_YMD (   )

{

}

 int Period_YMD::ConvertTimeInDay (int year,int month,int day)

{
   int time = year * 372 + month * 31 + day;
   return (time);
}

 int Period_YMD::IsTimeInsidePeriod (int year,int month,int day)
 
{
   if (year == -1 || month == -1 || day == -1) return (FALSE);
   int time = ConvertTimeInDay (year,month,day);
   if (TimeInDay[0] <= time && time <= TimeInDay[1]) return (TRUE);
   else return (FALSE);
}

 int Period_YMD::ReadFile (FileIO &file)

{
   if (file.Read ((byte *)Year     ,1,sizeof(Year     ))) return (1);
   if (file.Read ((byte *)Month    ,1,sizeof(Month    ))) return (1);
   if (file.Read ((byte *)Day      ,1,sizeof(Day      ))) return (1);
   if (file.Read ((byte *)TimeInDay,1,sizeof(TimeInDay))) return (1);
   // jun : 예외 추가
   #if defined(__OS_WINDOWS)
   int i;
   for (i = 0; i < 2; i++) {
      if (Year[i] > 0 && Year[i] < 200) Year[i] += 1900;
   }
   #endif
   return (DONE);
}

 void Period_YMD::Set (int n,int year,int month,int day)

{
   Year[n]      = year;
   Month[n]     = month;
   Day[n]       = day;
   TimeInDay[n] = ConvertTimeInDay (year,month,day);
}

 int Period_YMD::WriteFile (FileIO &file)
 
{
   if (file.Write ((byte *)Year     ,1,sizeof(Year     ))) return (1);
   if (file.Write ((byte *)Month    ,1,sizeof(Month    ))) return (1);
   if (file.Write ((byte *)Day      ,1,sizeof(Day      ))) return (1);
   if (file.Write ((byte *)TimeInDay,1,sizeof(TimeInDay))) return (1);
   return (DONE);
}

 int Period_YMD::ReadFile (CXMLIO* pIO, const char* szNodeName)

{
   static Period_YMD dv; // jun: default value

   CXMLNode xmlNode (pIO);
   if (!szNodeName) szNodeName = "Period_YMD";
   if (xmlNode.Start (szNodeName)) {
      xmlNode.Attribute (TYPE_ID (int), "SYear"     , &Year[0]     , &dv.Year[0]);
      xmlNode.Attribute (TYPE_ID (int), "SMonth"    , &Month[0]    , &dv.Month[0]);
      xmlNode.Attribute (TYPE_ID (int), "SDay"      , &Day[0]      , &dv.Day[0]);
      xmlNode.Attribute (TYPE_ID (int), "STimeInDay", &TimeInDay[0], &dv.TimeInDay[0]);
      xmlNode.Attribute (TYPE_ID (int), "EYear"     , &Year[1]     , &dv.Year[1]);
      xmlNode.Attribute (TYPE_ID (int), "EMonth"    , &Month[1]    , &dv.Month[1]);
      xmlNode.Attribute (TYPE_ID (int), "EDay"      , &Day[1]      , &dv.Day[1]);
      xmlNode.Attribute (TYPE_ID (int), "ETimeInDay", &TimeInDay[1], &dv.TimeInDay[1]);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

 int Period_YMD::WriteFile (CXMLIO* pIO, const char* szNodeName)

{
   static Period_YMD dv; // jun: default value

   CXMLNode xmlNode (pIO);
   if (!szNodeName) szNodeName = "Period_YMD";
   if (xmlNode.Start (szNodeName)) {
      xmlNode.Attribute (TYPE_ID (int), "SYear"     , &Year[0]     , &dv.Year[0]);
      xmlNode.Attribute (TYPE_ID (int), "SMonth"    , &Month[0]    , &dv.Month[0]);
      xmlNode.Attribute (TYPE_ID (int), "SDay"      , &Day[0]      , &dv.Day[0]);
      xmlNode.Attribute (TYPE_ID (int), "STimeInDay", &TimeInDay[0], &dv.TimeInDay[0]);
      xmlNode.Attribute (TYPE_ID (int), "EYear"     , &Year[1]     , &dv.Year[1]);
      xmlNode.Attribute (TYPE_ID (int), "EMonth"    , &Month[1]    , &dv.Month[1]);
      xmlNode.Attribute (TYPE_ID (int), "EDay"      , &Day[1]      , &dv.Day[1]);
      xmlNode.Attribute (TYPE_ID (int), "ETimeInDay", &TimeInDay[1], &dv.TimeInDay[1]);
      xmlNode.End ();
      return (DONE);
   }
   return (1);
}

}

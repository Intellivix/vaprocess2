#if !defined(__VACL_OBJTRACK_H)
#define __VACL_OBJTRACK_H

#include "vacl_blbtrack.h"
#include "vacl_boxtrack.h"
#include "vacl_edge.h"
#include "vacl_face.h"
#include "vacl_fmod.h"
#include "vacl_foregnd.h"
#include "vacl_histo.h"
#include "vacl_opencv.h"
#include "vacl_pathpass.h"
#include "vacl_thumbcap.h"
#include "VAEngine.h"

 namespace VACL

{

class Event;
class EventZone;
typedef LinkedList<Event> EventList;

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectIDGeneration
//
///////////////////////////////////////////////////////////////////////////////

 class ObjectIDGeneration

{
   protected:
      int Count;
      
   public:
      ObjectIDGeneration (   );

   public:
      int  Generate (   );
      void Reset    (   );
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: FrgRegion
//
///////////////////////////////////////////////////////////////////////////////

 class FrgRegion : public ConnectedRegion

{
   public:
      IArray1D MatTrkObjects;
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: FRArray1D
//
///////////////////////////////////////////////////////////////////////////////

 class FRArray1D : public Array1D<FrgRegion>

{
   public:
      void Initialize (CRArray1D& s_array);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: TrkObjRegion
//
///////////////////////////////////////////////////////////////////////////////

 class TrkObjRegion : public IBox2D

{
   public:
      int      FrameCount;
      int      NumPersons;
      int64    Status;
      float    Area;
      float    AspectRatio;
      float    MajorAxisAngle;
      float    NorSpeed;
      float    Speed;
      FPoint2D Centroid;
      FPoint2D MeanVelocity;
      FPoint2D Velocity;
   
   public:
      TrkObjRegion* Prev;
      TrkObjRegion* Next;
};

typedef LinkedList<TrkObjRegion> TrkObjRegionList;
 
///////////////////////////////////////////////////////////////////////////////
//
// Class: TrackedObject
//
///////////////////////////////////////////////////////////////////////////////

#define TO_EVENT_ABANDONED                 0x00000001
#define TO_EVENT_ABANDONED_START           0x00000002
#define TO_EVENT_ABANDONED_END             0x00000004
#define TO_EVENT_CAR_ACCIDENT              0x00000008
#define TO_EVENT_CAR_ACCIDENT_START        0x00000010
#define TO_EVENT_CAR_PARKING               0x00000020
#define TO_EVENT_CAR_PARKING_START         0x00000040
#define TO_EVENT_CAR_STOPPING              0x00000080
#define TO_EVENT_DIRECTIONAL_MOTION        0x00000100
#define TO_EVENT_DWELL                     0x00000200
#define TO_EVENT_ENTERING                  0x00000400
#define TO_EVENT_FALLING                   0x00000800
#define TO_EVENT_GATHERING                 0x00001000
#define TO_EVENT_LEAVING                   0x00002000
#define TO_EVENT_LINE_CROSSING             0x00004000
#define TO_EVENT_LOITERING                 0x00008000
#define TO_EVENT_PATH_PASSING              0x00010000
#define TO_EVENT_REMOVED                   0x00020000
#define TO_EVENT_REMOVED_START             0x00040000
#define TO_EVENT_STOPPING                  0x00080000
#define TO_EVENT_VIOLENCE                  0x00100000
                                           
#define TO_EVENTZONE_ABANDONED             0x00000001
#define TO_EVENTZONE_CAR_ACCIDENT          0x00000002
#define TO_EVENTZONE_CAR_PARKING           0x00000004
#define TO_EVENTZONE_CAR_STOPPING          0x00000008
#define TO_EVENTZONE_DIRECTIONAL_MOTION    0x00000010
#define TO_EVENTZONE_DWELL                 0x00000020
#define TO_EVENTZONE_ENTERING              0x00000040
#define TO_EVENTZONE_FALLING               0x00000080
#define TO_EVENTZONE_GATHERING             0x00000100
#define TO_EVENTZONE_LEAVING               0x00000200
#define TO_EVENTZONE_LINE_CROSSING         0x00000400
#define TO_EVENTZONE_LOITERING             0x00000800
#define TO_EVENTZONE_PATH_PASSING          0x00001000
#define TO_EVENTZONE_REMOVED               0x00002000
#define TO_EVENTZONE_STOPPING              0x00004000
#define TO_EVENTZONE_VIOLENCE              0x00008000
                                          
#define TO_STATUS_ABANDONED_OBJECT         0x0000000000000001
#define TO_STATUS_BEST_SHOT                0x0000000000000002
#define TO_STATUS_CHANGED_ABRUPTLY         0x0000000000000004
#define TO_STATUS_FAST_MOVING_OBJECT       0x0000000000000008
#define TO_STATUS_FEATURE_POINT_TRACKING   0x0000000000000010
#define TO_STATUS_FMO_ENDED                0x0000000000000020
#define TO_STATUS_FRONTAL_FACE1            0x0000000000000040
#define TO_STATUS_FRONTAL_FACE2            0x0000000000000080
#define TO_STATUS_GHOST_OBJECT             0x0000000000000100
#define TO_STATUS_GROUP_MEMBER             0x0000000000000200
#define TO_STATUS_HANDOFF                  0x0000000000000400
#define TO_STATUS_HUMAN                    0x0000000000000800
#define TO_STATUS_IN_OUTER_ZONE            0x0000000000001000
#define TO_STATUS_LOCKED                   0x0000000000002000
#define TO_STATUS_LOST                     0x0000000000004000
#define TO_STATUS_MASK_PUT                 0x0000000000008000
#define TO_STATUS_MATCHED_1TO1             0x0000000000010000
#define TO_STATUS_MAX_DWELL_TIME           0x0000000000020000
#define TO_STATUS_MERGED                   0x0000000000040000
#define TO_STATUS_OUT_OF_TRACKING_AREA     0x0000000000080000
#define TO_STATUS_RECOGNIZE                0x0000000000100000
#define TO_STATUS_REMOVED_OBJECT           0x0000000000200000
#define TO_STATUS_REPRESENTATIVE           0x0000000000400000
#define TO_STATUS_SPLIT                    0x0000000000800000
#define TO_STATUS_STATIC1                  0x0000000001000000
#define TO_STATUS_STATIC2                  0x0000000002000000
#define TO_STATUS_TO_BE_REMOVED            0x0000000004000000
#define TO_STATUS_UNDETECTED               0x0000000008000000
#define TO_STATUS_VALID_SPEED              0x0000000010000000
#define TO_STATUS_VALIDATED                0x0000000020000000
#define TO_STATUS_VEHICLE                  0x0000000040000000
#define TO_STATUS_VERIFIED                 0x0000000080000000
#define TO_STATUS_VERIFIED_START           0x0000000100000000
                                          
#define TO_TYPE_HUMAN                      0x00000001
#define TO_TYPE_VEHICLE                    0x00000002
#define TO_TYPE_UNKNOWN                    0x00000004
#define TO_TYPE_FACE                       0x00000008
#define TO_TYPE_FLAME                      0x00000010
#define TO_TYPE_SMOKE                      0x00000020
#define TO_TYPE_WATER_LEVEL                0x00000040
#define TO_TYPE_CROWD                      0x00000080
#define TO_TYPE_HEAD                       0x00000100
#define TO_TYPE_ANIMAL                     0x00000200
#define TO_TYPE_WHEELCHAIR                 0x00000400

#define TO_PERSON_ATTR_MALE                0x00000001
#define TO_PERSON_ATTR_CHILD               0x00000002
#define TO_PERSON_ATTR_BAG                 0x00000004
#define TO_PERSON_ATTR_STRIPE              0x00000008
#define TO_PERSON_ATTR_SUIT                0x00000010
#define TO_PERSON_ATTR_TSHIRT              0x00000020
#define TO_PERSON_ATTR_SHORT_SLEEVES       0x00000040
#define TO_PERSON_ATTR_LONG_TROUSERS       0x00000080
#define TO_PERSON_ATTR_JEANS               0x00000100
#define TO_PERSON_ATTR_SKIRT               0x00000200
#define TO_PERSON_ATTR_STICK               0x10000000   // Special Attribute

#define TO_VEHICLE_TYPE_UNKNOWN            0x00000001
#define TO_VEHICLE_TYPE_SEDAN              0x00000002
#define TO_VEHICLE_TYPE_SUV                0x00000004
#define TO_VEHICLE_TYPE_VAN                0x00000008
#define TO_VEHICLE_TYPE_TRUCK              0x00000010
#define TO_VEHICLE_TYPE_BUS                0x00000020
#define TO_VEHICLE_TYPE_TWOWHEELER         0x00000040

#define NUM_OBJECT_TYPES                   32
#define NUM_PERSON_ATTRS                   24
#define NUM_VEHICLE_TYPES                  24

 class TrackedObject : public IBox2D
// [����] �Ӽ� ���� �߰��� ������ Merge() �Լ��� ������Ʈ �ʿ�
{
   protected:
      int      Flag_FPTEnabled; // Feature Point Tracking ���� ����
      IBox2D   Rgn_Inactive;    //
      IBox2D   Rgn_Static;      // TO_STATUS_STATIC1 ���� �Ǻ��� ����
      FPoint2D Pos_Static;      // TO_STATUS_STATIC2 ���� �Ǻ��� ����
   
   public:
      int   ID;          // ��ü ID (TO_STATUS_VERIFIED ���°� �Ǳ� ������ ���� ���� ����)
      int   Type;        // ��ü ���� (TO_TYPE_XXX ���ǹ� ����)
      int64 Status;      // ��ü ���� (TO_STATUS_XXX ���ǹ� ����)
      int   PersonAttrs; // ��� �Ӽ��� (TO_PERSON_ATTR_XXX ���ǹ� ����)
      int   VehicleType; // ���� ���� (TO_VEHICLE_TYPE_XXX ���ǹ� ����)
      int   EventStatus; // �� ��ü�� �߻���Ų �̺�Ʈ�� (TO_EVENT_XXX ���ǹ� ����)
      int   EventZones;  // �� ��ü�� �����ϴ� �̺�Ʈ ������ (TO_EVENTZONE_XXX ���ǹ� ����)

   public:
      int   Depth;                           // �� ��ü�� ��ġ�ϴ� ���̾��� ��ȣ
      int   NumBndPixels;                    // # of Region Boundary Pixels
      int   NumRgnPixels;                    // # of Region Pixels
      int   NumFrgBndEdgePixels;             // # of Foreground Boundary Edge Pixels
      int   NumBkgBndEdgePixels;             // # of Background Boundary Edge Pixels
      int   TrjLength;                       // ��ü�� ���� ���� ������ ����
      int   TypeHistory;                     //
      float AreaVariation;                   // Area Variation
      float BndEdgeStrength;                 // Boundary Edge Strength
      float Dispersedness;                   // ��ü �ٿ������ �ұ��ϵ� 
      float Exposure;                        // ��ü�� ���⵵ (0.0 ~ 1.0)
      float MajorAxisAngle1;                 // ����� Y�� ������ ȸ����(Degree). ������ Y��� ��ġ�ϸ� 0�� �ȴ�.
      float MajorAxisAngle2;                 // ����� ��� �ӵ� ���� ������ ȸ����(Degree).
      float MatchingScore;                   // ���� �����ӿ��� ã�� ��ü ��ġ���� ��ü ���ø��� ��Ī ����
      float MaxMovingDistance;               // ���� ���� ��ġ�κ��� �ִ�� �ָ� �̵��� �Ÿ�
      float MaxScore_BestShot;               // BestShot ������ �ִ�ġ
      float MeanArea;                        // �ֱ� ��� ����
      float Noisiness;                       // ��ü�� �������� ���ɼ��� ���� ����
      float PathLength;                      // ������� ������ �� �Ÿ�
      float OTLikelihood[NUM_OBJECT_TYPES ]; // ��ü ���� �ν� ���
      float PALikelihood[NUM_PERSON_ATTRS ]; // ��� �Ӽ� �ν� ���
      float VTLikelihood[NUM_VEHICLE_TYPES]; // ���� ���� �ν� ���

   public:
      int FrameCount;      // ���� ������ ī��Ʈ ��
      int InitFrameCount;  // �� ��ü�� ���� �� ������ ī��Ʈ ��
      int Count_Tracked;   // ������� ������ �̿�� ������ ��
      int Count_Abandoned; //
      int Count_Removed;   //
    
   public:
      int Count_PAR;
      int Count_POC;
      int Count_VTR;
      int Result_POC;

   // Ư�� ������ ���� �ð��� �����ϴ� ������
   public:
      float Time_BestShot;
      float Time_ChangedAbruptly;
      float Time_Lost;
      float Time_Merged;
      float Time_Static;
      float Time_Tracked;
      float Time_TypeUnchanged;
      float Time_Undetected;
      float MaxTime_Static;

   public:
      float Time_PAR;
      float Time_POC;
      float Time_VTR;
   
   // Ư�� �̺�Ʈ�� ��� �ð��� �����ϴ� ������
   public:
      float Time_CarAccident;
      float Time_CarStopping;
      float Time_DirecMotion;
      float Time_FrontalFace1;
      float Time_FrontalFace2;
      float Time_Dwell;        // [Obsolete]
      float Time_Falling;   
      float Time_Ghost;
      float Time_Loitering;    // [Obsolete]
      float Time_Stopping;

   // ��ü ���͸� �� ���� �� �ִ� �Ӽ� ����
   public:
      float Area;
      float AspectRatio;
      float AspectRatio2;
      float AxisLengthRatio;
      float MajorAxisLength;
      float MinorAxisLength;
      float NorSpeed;         // 100 * Speed / Height
      float Speed;            // Mag(MeanVelocity) * FrameRate
      float RealArea;
      float RealDistance;
      float RealWidth;
      float RealHeight;
      float RealMajorAxisLength;
      float RealMinorAxisLength;
      float RealSpeed;
   
   public:
      float AvgArea;
      float AvgWidth;
      float AvgHeight;
      float AvgAspectRatio;
      float AvgAxisLengthRatio;
      float AvgMajorAxisLength;
      float AvgMinorAxisLength;
      float AvgNorSpeed;
      float AvgSpeed;
      float AvgRealArea;
      float AvgRealDistance;
      float AvgRealWidth;
      float AvgRealHeight;
      float AvgRealMajorAxisLength;
      float AvgRealMinorAxisLength;
      float AvgRealSpeed;
      
   public:
      int   MaxWidth;
      int   MaxHeight;
      float MaxArea;
      float MaxAspectRatio;
      float MaxAxisLengthRatio;
      float MaxMajorAxisLength;
      float MaxMinorAxisLength;
      float MaxNorSpeed;
      float MaxSpeed;
      float MaxRealDistance;
      float MaxRealSpeed;
      
   public:
      int   MinWidth;
      int   MinHeight;
      float MinArea;
      float MinAspectRatio;
      float MinAxisLengthRatio;
      float MinMajorAxisLength;
      float MinMinorAxisLength;
      float MinNorSpeed;
      float MinSpeed;
      float MinRealDistance;
      float MinRealSpeed;

   public:
      IBox2D   InitRegion;       // ��ü�� ���� ���� ����
      IBox2D   InitDMRegion;     // ���⼺ �̵� �� ���� ����
      FPoint2D CenterPos;        // ��ü �ٿ�� �ڽ��� �߽� ��ǥ
      FPoint2D Centroid;         // ��ü�� ��Ʈ���̵�
      FPoint2D MajorAxis;        // ��ü�� ����
      FPoint2D MinorAxis;        // ��ü�� ����
      FPoint2D MeanVelocity;     // �ֱ� 1�ʰ� Velocity  ������ ���
      FPoint2D MeanSizeVelocity; // �ֱ� 1�ʰ��� ��� ũ�� ��ȭ
      FPoint2D RealPos;          // 
      FPoint2D Velocity;         // ��ü �߽��� ������ �� �̵� ����
      IPoint2D MatchingPos;      // 

   public:
      int      GTOID;
      IBox2D   HumanRect;
      FLine2D  HumanStick;
      FPoint2D GndPos;

   public:
      BGRColor CntBkgColor;
      BGRColor CntFrgColor;
      BGRColor RgnBkgColor;
      BGRColor RgnFrgColor;
   
   public:
      int      ThumbPixFmt;
      ISize2D  ThumbSize;
      BArray1D ThumbBuffer;

   public:
      GImage      MaskImage;
      GImage      TemplateImage;
      GImage      WeightImage;
      BGRImage    DNNInputImage;
      Histogram1D GrayHistogram;
      HVHistogram ColorHistogram;

   public:
      FP2DArray1D      Trajectory;
      TrkObjRegionList TrkObjRegions;

   public:
      EventList Events;
      PathList  Paths;
   
   public:
      int      ObjRgnID;
      int      ObjRgnArea;
      IBox2D   ObjRgnBndBox;
      IArray1D MatFrgRegions;

   public:
      IBox2D       Head;
      FaceInfo     Face;
      TrackedBox*  TrkBox;
      TrackedBlob* TrkBlob;
      // ��ü �Ƿ翧�� ���� ������ ���(�Ӹ�) ��ġ
      struct PersonInfo {
         int X,Y;    // ���� ��ü ������ ��� �Ӹ��� ��ġ
         int Height; // ����� ����
      };
      Array1D<PersonInfo> Persons;

   // Ư¡�� ���� ����
   public:
      int   FPTrackLength;
      float FPFlowVariance;
      float FPFlowActivity;
      #if defined(__LIB_OPENCV)
      vector<vector<cv::Point2f> > FPTracks;
      #endif
   
   public:
      BCCL::Time StartTime; // ��ü ���� �ð�
      BCCL::Time EndTime;   // ��ü �Ҹ� �ð�
   
   public:
      TrackedObject* Prev;
      TrackedObject* Next;

   public:
      TrackedObject (   );
      virtual ~TrackedObject (   ) {   };
   
   protected:
      float GetMatchingScore (GImage &s_image,GImage &m_image,int ox,int oy,int dx,int dy);
   
   public:
      void   AddToTrkObjRegionList         (int trj_length);
      int    CheckTypeAndAttrs             (int type,int attrs);
      void   ClearFPMotionPropertyValues   (   );
      void   ClearStatus                   (   );
      void   EnableFPTracking              (int flag_enable);
      Event* FindEvent                     (EventZone *evt_zone,int evt_type = -1);
      Path*  FindPath                      (EventZone *evt_zone);
      void   GetArea                       (   );
      int    GetAreaOfSupportedRegion      (SArray2D &s_array,short thrsld);
      void   GetAreaVariation              (   );
      void   GetAspectRatio                (   );
      void   GetAxisLengthRatio            (   );
      void   GetClippedBoundingBox         (int si_width,int si_height,IPoint2D& lt_pos,IPoint2D& rb_pos,IPoint2D& c_pos);
      float  GetBestShotScore              (ISize2D &si_size);
      void   GetColorHistogram             (BGRImage &s_image,IArray2D &or_map,GImage &fg_image);
      void   GetDispersedness              (   );
      FPoint2D GetDisplacement             (int n_frames);
      int    GetFPMotionProperties1        (float frm_duration);
      int    GetFPMotionProperties2        (float frm_duration);
      float  GetHumanRectAreaRatio         (   );
      void   GetImagePatch_YUY2            (byte* si_buffer,ISize2D& si_size,FPoint2D& scale,FPoint2D& inflation,BGRImage& d_image);
      void   GetMajorAxisAngle1            (   );
      void   GetMajorAxisAngle2            (   );
      void   GetMatchingPosition           (FrgRegion& fg_rgn);
      int    GetMatchingPosition           (FrgRegion& fg_rgn,float min_sc,float max_sc);
      void   GetMatchingPosition           (GImage &s_image,GImage &m_image,IPoint2D &s_pos,int s_range);
      void   GetMaxMovingDistance          (   );
      void   GetMeanArea                   (int n_frames);
      void   GetMeanSizeVelocity           (int n_frames,FPoint2D& d_velocity);
      void   GetMeanVelocity               (int n_frames,FPoint2D& d_velocity);
      float  GetMovingDistance1            (   );
      float  GetMovingDistance2            (   );
      int    GetNumEvents                  (int pos_evt_type = -1,int neg_evt_type = -1);
      void   GetPathLength                 (   );
      void   GetPredictedPosition          (FPoint2D &p_pos,float p_time = 1.0f,int flag_smooth = FALSE);
      void   GetPredictedPosition          (IPoint2D &p_pos,float p_time = 1.0f,int flag_smooth = FALSE);
      void   GetPredictedSize              (ISize2D  &p_size,float p_time);
      void   GetPrincipalAxes              (   );
      void   GetSpeeds                     (float frm_rate); 
      void   InitTemplate                  (GImage& s_image,IArray2D& or_map,IBox2D& s_rgn,int s_rgn_id,BGRImage* s_cimage = NULL);
      int    IsFPTrackingEnabled           (   );
      int    IsInactive                    (float a);
      int    IsInMarginalArea              (ISize2D& si_size);
      int    IsStatic1                     (   );
      int    IsStatic2                     (   );
      int    IsTypeFixed                   (   );
      void   Merge                         (TrackedObject *s_object);
      void   PutMask                       (int sx,int sy,byte   m_value,GImage&    d_image,int flag_overwrite = TRUE);
      void   PutMask                       (int sx,int sy,int    m_value,IArray2D&  d_array,int flag_overwrite = TRUE);
      void   PutMask                       (int sx,int sy,ushort m_value,USArray2D& d_array,int flag_overwrite = TRUE);
      void   PutRectMask                   (int sx,int sy,byte   m_value,GImage&    d_image,int margin = 5);
      void   PutRectMask                   (int sx,int sy,int    m_value,IArray2D&  d_array,int margin = 5);
      void   PutRectMask                   (int sx,int sy,ushort m_value,USArray2D& d_array,int margin = 5);
      void   SetRegion                     (IBox2D &s_rgn);
      void   SetPos_Static                 (   );
      void   SetRegion_Inactive            (   );
      void   SetRegion_Static              (   );
      void   ResetPos_Static               (   );
      void   ResetRegion_Inactive          (   );
      void   ResetRegion_Static            (   );
      void   UpdateAvgMinMaxPropertyValues (   );
      void   UpdateCountsAndTimes          (float frm_duration);
      void   UpdateGrayHistogram           (GImage &s_image,IArray2D &or_map);
      void   UpdateNoisiness               (ISize2D& si_size);
      void   UpdateObjectType              (float frm_duration);
      void   UpdatePersonAttributes        (   );
      void   UpdateRegion                  (IBox2D &s_rgn,float ru_rate = 1.0f);
      void   UpdateRegionAndVelocity       (IBox2D &s_rgn,float vu_rate = 1.0f);
      void   UpdateTemplateImage           (GImage &s_image,IArray2D &or_map,float tu_rate,IBox2D &d_rgn,float& vu_rate);
      void   UpdateVehicleType             (   );

   public:
      struct EvtZonePathPassInfo
      {
         float      Time_PathPassing;
         EventZone* EvtZone;
         EvtZonePathPassInfo* Next;
         EvtZonePathPassInfo* Prev;
      };
      LinkedList<EvtZonePathPassInfo> EventZonePathPassingInfoList;
      EvtZonePathPassInfo* FindEventZonePathPassingInfo (EventZone* evt_zone);

   public:
      class EvtPrgTime
      {
         public:
            int        Flag_InZone;
            float      PrgTime;
            EventZone* EvtZone;

         public:
            EvtPrgTime* Prev;
            EvtPrgTime* Next;
         
         public:
            EvtPrgTime (   ) {
               Flag_InZone = TRUE;
               PrgTime     = 0.0f;
               EvtZone     = NULL;
            };

      };
      LinkedList<EvtPrgTime> EvtPrgTimeList;
      EvtPrgTime* GetEventProgressTime (EventZone* evt_zone);
};

typedef Array1D<TrackedObject*>   TOArray1D;
typedef LinkedList<TrackedObject> TrackedObjectList;

 inline void TrackedObject::EnableFPTracking (int flag_enable)

{
   Flag_FPTEnabled = flag_enable;
}

 inline int TrackedObject::IsFPTrackingEnabled (   )

{
   return (Flag_FPTEnabled);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectTracking
//
///////////////////////////////////////////////////////////////////////////////

#define OBT_OPTION_PERFORM_FAST_MOVING_OBJECT_DETECTION   0x00000001
#define OBT_OPTION_PERFORM_MEAN_SHIFT_TRACKING            0x00000002

 class ObjectTracking

{
   protected:
      int Flag_Enabled;
      int Flag_Calibrated;
      int NumObjLayers;
      int FrameCount;
      int ObjectCount;

   protected:
      FPoint2D ScaleRS;
      FPoint2D ScaleSR;
      GImage   MaskImage;
      Matrix   Mat_Hf;
      Matrix   Mat_IHf;
      Matrix   Mat_Hh;
      Matrix   Mat_IHh;

   protected:
      ObjectIDGeneration* ObjIDGenerator;

   protected:
      BoundaryEdgeInfoExtraction BndEdgeInfoExtractor;

   public:
      int   Options;
      int   SearchRange;
      float MaxTrjLength;
      float ObjUpdateRate;
      float MinBndEdgeStrength; // Ghost ��ü�� �Ǻ��Ǳ� ���� BndEdgeStrength�� �Ӱ�ġ
      float MinMovingDistance;  // ��ȿ ��ü�� �Ǻ��Ǳ� ���� �ʱ� ��ü �̵� �Ÿ��� �ּҰ� (���밪)
      float MinMovingDistance2; // ��ȿ ��ü�� �Ǻ��Ǳ� ���� �ʱ� ��ü �̵� �Ÿ��� �ּҰ� (��밪: ��ü ũ�⿡ ���)
      float OuterZoneSize1;
      float OuterZoneSize2;
      
   public:
      float    FocalLength;
      float    CamTiltAngle;
      float    CamHeight;
      ISize2D  RefImgSize;
      FSize2D  HumanSize;

   public:
      int   MinCount_Tracked;    // ��ü�� ��ȿ ��ü�� �Ǹ�Ǳ� ���� �������� ����Ǿ�� �ϴ� �ּ� Ƚ��
      float MaxTime_FPT;         //
      float MaxTime_Lost;        // ��ü�� Lost ���·� ������ �� �ִ� �ִ� �ð�
      float MaxTime_Static;      // ����(static) ��ü�� ���ȭ�κ��� ��ȣ�ϴ� �ִ� �ð�
      float MaxTime_Undetected;  // ��ü�� �̰��� ���·� ������ �� �ִ� �ִ� �ð� (��ü�� TO_STATUS_VERIFIED ������ ���)
      float MaxTime_Undetected2; // ��ü�� �̰��� ���·� ������ �� �ִ� �ִ� �ð� (��ü�� ���� TO_STATUS_VERIFIED ���°� �ƴ� ���)
      float MinTime_Tracked;     // ��ü�� �����ϴµ� �ҿ�Ǵ� �ּ� �ð�
   
   public:
      PolygonalAreaList DynBkgAreas;
      PolygonalAreaList NonHandoffAreas;
      PolygonalAreaList NontrackingAreas;
      PolygonalAreaList TrackingAreas;

   public:
      SingleEvent Event_Processed;

   public:
      float    FrameDuration;
      float    FrameRate;
      ISize2D  SrcImgSize;
      FPoint2D Shift;

   public:
      ForegroundDetection* ForegroundDetector;
  
   public:
      IArray2D          ObjRegionMap;
      TrackedObjectList LostObjects;
      TrackedObjectList TrackedObjects;

  public:
      GImage DynBkgAreaMap;
      GImage NonHandoffAreaMap;
      GImage TrkAreaMap;

   public:
      #if defined(__LIB_OPENCV)
      std::vector<cv::Mat>  ImagePyramid1;
      std::vector<cv::Mat>  ImagePyramid2;
      std::vector<cv::Mat>* PrvImgPyramid;
      std::vector<cv::Mat>* CurImgPyramid;
      #endif

   public:
      BlobTracking              BlobTracker;
      BoxTracking               BoxTracker;
      FastMovingObjectDetection FMODetector;

   public:
      ObjectTracking (   );
      virtual ~ObjectTracking (   );

   protected:
      void  _Init                                       (   );
      void  ChangeObjectRegionLabel                     (IArray2D &s_map,IBox2D &s_rgn,int s_no,int d_no);
      int   CheckBlobOverlapWithTrackingArea            (IArray2D &s_map,IBox2D &s_rgn,int s_no);
      int   CheckObjectInDynamicBackgroundArea          (TrackedObject *t_object);
      int   CheckObjectInitMotion                       (TrackedObject *t_object);
      int   CheckObjectInTrackingArea                   (TrackedObject *t_object);
      int   CheckObjectStatic                           (TrackedObject *t_object);
      int   CheckRegionInMarginalArea                   (IBox2D& s_rgn);
      int   CheckStaticObjectConditions                 (TrackedObject* t_object);
      void  CopyBlobRegion                              (IArray2D &s_map,IBox2D &s_rgn,int s_no,int d_no,IArray2D &d_map);
      void  CopyResidualBlobRegion                      (IArray2D &s_map,IBox2D &s_rgn,int s_no,TrackedObject* t_object,int d_no,IArray2D &d_map);
      TrackedObject* FindMatchingObjectInLostObjectList (TrackedObject *t_object);
      void  GetFMOCorrespondenceMatrix                  (TOArray1D &t_objects,IArray2D &fr_map,IArray2D &oc_matrix);
      void  GetFMOCorrespondences                       (IArray2D &oc_matrix,TOArray1D &t_objects,FRArray1D &fr_array);
      void  GetFrgRegionMaskImage                       (IArray2D &fr_map,GImage &d_image);
      int   GetHumanRect                                (IArray2D &or_map,TrackedObject* t_object);
      int   GetNewExtObjectID                           (   );
      int   GetNewIntObjectID                           (   );
      void  GetObjectCorrespondenceMatrix               (TOArray1D &t_objects,IArray2D &fr_map,IArray2D &oc_matrix);
      void  GetObjectCorrespondences                    (IArray2D &oc_matrix,TOArray1D &t_objects,FRArray1D &fr_array);
      void  GetObjectMatchingPosition                   (GImage& s_image,TrackedObject* t_object);
      void  GetObjectProperties                         (TrackedObject* t_object);
      void  GetObjectRegionBoundaryEdgeInfo             (TrackedObject* t_object);
      int   InitHomographies                            (   );
      void  ManageLostObjects                           (   );
      void  MergeObjects                                (TOArray1D &to_array,IArray1D &to_nos,IBox2D &fb_region,int fb_no,IArray2D &oc_matrix,IArray2D &d_map);
      void  MoveToLostObjectList                        (TrackedObject *t_object);
      void  PerformMeanShift                            (IArray2D& or_map,IBox2D& s_rgn,int or_no,float ub_a,float ub_w,int n_iterations,IBox2D& d_rgn);
      void  PerformOcclusionReasoning                   (GImage &s_image,TOArray1D &t_objects,IArray1D &to_nos,IArray2D &fr_map,IBox2D &fb_region,int fb_no,IArray2D &d_map);
      void  PerformPostProcessing                       (   );
      TrackedObject* RegisterNewObject                  (   );
      TrackedObject* RegisterNewObject                  (GImage& s_image,IBox2D& s_rgn,int s_no);
      void  SetObjectEndTime                            (   );
      void  SyncBlobTrackingParams                      (   );
      void  SyncBoxTrackingParams                       (   );
      void  TrackFeaturePoints                          (   );

   protected:
      int   PerformBlobTracking   (   );
      int   PerformBoxTracking    (   );
      int   PerformObjectTracking (   );
   
   public:
      virtual int  CheckSetup (ObjectTracking* ot);
      virtual void Close      (   );
      virtual int  ReadFile   (FileIO &file);
      virtual int  ReadFile   (CXMLIO* pIO);
      virtual int  WriteFile  (FileIO &file);  
      virtual int  WriteFile  (CXMLIO* pIO);

   public:
      void CleanTrackedObjectList      (   );
      void ClearAllAreaLists           (   );
      int  CheckObjectInNonHandoffArea (TrackedObject *t_object);
      int  CheckObjectStatus           (int status);
      void Enable                      (int flag_enable);
      void GetBkgModelUpdateScheme     (int bmu_mode,GImage &d_image);
      void GetObjectArray              (TOArray1D &d_array);
      int  Initialize                  (ISize2D& si_size,float frm_rate,ObjectIDGeneration& id_generator);
      void InitParams                  (EventDetection& evt_detector);
      int  IsEnabled                   (   );
      int  Perform                     (ForegroundDetection& frg_detector);
      void Reset                       (   );
      void SetObjectVerified           (TrackedObject *t_object);
      int  UpdateAreaMaps              (ISize2D& ri_size);
      void UpdateFaceRegions           (   ); // jun
      int  UpdateObjectThumbnails      (byte *si_buffer,ISize2D &si_size,int pix_fmt = VAE_PIXFMT_YUY2,float cbs_ratio = 1.5f,ISize2D *tn_size = NULL);
};

 inline int ObjectTracking::IsEnabled (   )

{
   return (Flag_Enabled);
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

int            CheckObjectInOuterZone            (ISize2D& si_size,float oz_size1,float oz_size2,TrackedObject* t_object);
void           CleanTrackedObjectList            (TrackedObjectList& to_list);
int            ConvertObjTypeToTrkObjType        (int64 obj_type);
TrackedObject* CreateInstance_TrackedObject      (   );
int            GetTrkObjectTypeName              (int to_type,char* d_name);
int            GetVehicleTypeName                (int vc_type,char* d_name);
void           InvalidateAllTrkObjects           (TrackedObjectList& to_list);
void           SetCreationFunction_TrackedObject (TrackedObject*(*func)(   ));

}

#endif

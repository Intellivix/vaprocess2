#include "vacl_event.h"
#include "vacl_imgsynth.h"
#include "vacl_morph.h"
#include "vacl_objtrack.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 void GetSynthesizedImage (BGRImage &s_image,BGRImage &bg_image,GImage &m_image,BGRImage &d_image)

{
   int x,y;

   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   for (y = 1; y < ey; y++) {
      byte *l_m_image  = m_image[y];
      byte *l_m_image1 = m_image[y - 1];
      byte *l_m_image2 = m_image[y + 1];
      BGRPixel *sp = s_image [y];
      BGRPixel *bp = bg_image[y];
      BGRPixel *dp = d_image [y];
      for (x = 1; x < ex; x++) {
         if (l_m_image[x]) {
            if (!(l_m_image[x - 1] && l_m_image[x + 1] && l_m_image1[x] && l_m_image2[x]))
               dp->R = 255, dp->G = 0, dp->B = 0;
            else *dp = *sp;
         }
         else *dp = *bp;
         sp++,bp++,dp++;
      }
   }
}

 void GetSynthesizedImage (BGRImage &s_image,BGRImage &bg_image,ObjectTracking &obj_tracker,BGRImage &d_image)
 
{
   GImage t_image(s_image.Width,s_image.Height);
   t_image.Clear (   );
   TrackedObject *t_object = obj_tracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED) && (t_object->Status & TO_STATUS_VERIFIED)) {
         t_object->PutMask (t_object->X,t_object->Y,PG_WHITE,t_image,TRUE);
      }
      t_object = obj_tracker.TrackedObjects.Next (t_object);
   }
   GImage s_element(5,5);
   GetBinStrElement (s_element);
   GImage m_image (s_image.Width,s_image.Height);
   BinDilation (t_image,s_element,m_image);
   GetSynthesizedImage (s_image,bg_image,m_image,d_image);
}

}

#if !defined(__VACL_OBJDETEC_H)
#define __VACL_OBJDETEC_H

#include "vacl_dnn.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//
///////////////////////////////////////////////////////////////////////////////

class ForegroundDetection;

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectDetection_DNN
//
///////////////////////////////////////////////////////////////////////////////

 class ObjectDetection_DNN

{
   protected:
      int Flag_Initialized;

   public:
      int64 ObjTypeFilter;
   
   public:
      ObjectDetection_DNN (   );
      virtual ~ObjectDetection_DNN (   );

   protected:
      void FindCorrespondingObject (OBBArray1D& s_array,int64 obj_type,IBox2D& s_rgn,float min_tar,float max_tar,float est_tar,int& max_i,float& max_s);
      void GetHeadCandidateRegion  (IBox2D& s_rgn,IBox2D& d_rgn);
      void ScaleObjBndBoxes        (ISize2D& si_size,ISize2D& oi_size,OBBArray1D& d_array);
   
   protected:
      virtual int DetectObjects (BGRImage& s_cimage,OBBArray1D& d_array) = 0;
   
   public:
      virtual void    Close           (   );
      virtual ISize2D GetSrcImageSize (   ) = 0;
      virtual int     Initialize      (const char* dir_name);
      virtual int     Perform         (BGRImage& s_cimage,OBBArray1D& d_array,ISize2D* oi_size = NULL);

   public:
      int IsInitialized (   );
};

 inline int ObjectDetection_DNN::IsInitialized (   )

{
   return (Flag_Initialized);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectDetectionServer
//
///////////////////////////////////////////////////////////////////////////////

 class ObjectDetectionServer : public Thread

{
   protected:
      int Flag_Initialized;

   protected:
      struct Request {
         ForegroundDetection* ForegroundDetector;
         Request* Prev;
         Request* Next;
      };
      Queue<Request>  RequestQueue;
      CriticalSection CS_RequestQueue;

   public:
      MultiEvents Events;
   
   public:
      ObjectDetectionServer (   );
      virtual ~ObjectDetectionServer (   );

   protected:
      virtual void ThreadProc     (   );
      virtual void ProcessRequest (ForegroundDetection* frg_detector) = 0;
   
   public:
      virtual void    Close           (   );
      virtual ISize2D GetSrcImageSize (   ) = 0;
   
   public:
      int  AddRequest        (ForegroundDetection* frg_detector);
      int  IsInitialized     (   );
      int  IsRunning         (   );
      void RemoveAllRequests (ForegroundDetection* frg_detector);
      int  Start             (   );
      void Stop              (   );
};

 inline int ObjectDetectionServer::IsInitialized (   )

{
   return (Flag_Initialized);
}

 inline int ObjectDetectionServer::IsRunning (   )

{
   return (IsThreadRunning (   ));
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectDetection_YOLO
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_DARKNET)

 class ObjectDetection_YOLO : public ObjectDetection_DNN

{
   protected:
      DNN_Darknet_YOLO   DNN;
      I64Array1D CI2OT; // Class Index => Object Type

   public:
      ObjectDetection_YOLO (   );
   
   protected:
      virtual int DetectObjects (BGRImage& s_cimage,OBBArray1D& d_array);
   
   public:
      virtual ISize2D GetSrcImageSize (   );
      virtual int     Initialize      (void* dll_handle,const char* dir_name,int gpu_idx);
      virtual void    Close           (   );
};

 inline ISize2D ObjectDetection_YOLO::GetSrcImageSize (   )

{
   return (DNN.SrcImgSize);
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectDetectionServer_YOLO
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_DARKNET)

 class ObjectDetectionServer_YOLO : public ObjectDetectionServer

{
   protected:
      ObjectDetection_YOLO ObjectDetector;
      
   protected:
      virtual void ProcessRequest (ForegroundDetection* frg_detector);
   
   public:
      virtual void    Close           (   );
      virtual ISize2D GetSrcImageSize (   );
   
   public:
      int Initialize (void* dll_handle,const char* dir_name,int gpu_idx);
};

 inline ISize2D ObjectDetectionServer_YOLO::GetSrcImageSize (   )

{
   return (ObjectDetector.GetSrcImageSize (   ));
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

int64 ConvertClsNameToObjType (const char* cls_name);

#endif

}

#endif

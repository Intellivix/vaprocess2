#if !defined(__VACL_OPTFLOW_H)
#define __VACL_OPTFLOW_H

#include "vacl_opencv.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: FlowField
//
///////////////////////////////////////////////////////////////////////////////

 class FlowField

{
   public:
      FP2DArray2D Field;

   public:
      FlowField* Prev;
      FlowField* Next;
};

typedef LinkedList<FlowField> FlowFieldList;

///////////////////////////////////////////////////////////////////////////////
//
// Class: DenseOpticalFlow
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_OPENCV)

 class DenseOpticalFlow

{
   public:
      float FlowLength; // 단위: 초
   
   public:
      int   NumLevels;
      int   WinSize;
      float PyramidScale;

   public:
      int   Width;
      int   Height;
      float FrameRate;
   
   public:
      GImage PrvImage;
      FlowFieldList FlowHistory;

   public:
      DenseOpticalFlow (   );
      virtual ~DenseOpticalFlow (   );
   
   protected:
      void _Init (   );

   public:
      virtual void Close (   );
   
   public:
      FlowField* GetFlowField (GImage& s_iamge);
      void Initialize         (int width,int height,float frm_rate);
};

#endif

}

#endif



#if !defined(__VACL_CAMERA_H)
#define __VACL_CAMERA_H

#include "vacl_define.h"

 namespace VACL
 
{

#define LDC_ORIGINAL_SIZE   0
#define LDC_CIRCUMSCRIBED   1
#define LDC_INSCRIBED       2

////////////////////////////////////////////////////////////////////////////////
// 
// Functions
//
////////////////////////////////////////////////////////////////////////////////

void     GetCalibrationMatrix          (double f_length,int si_width,int si_height,Matrix &mat_K);
FPoint2D GetDistortedPixelPosition     (FPoint2D &s_point,FPoint2D &c_point,double kappa);
double   GetFocalLength                (Matrix &mat_H,int si_width,int si_height);
void     GetHomography_3DRotational    (double f_length1,double f_length2,double psi,double theta,double phi,int si_width,int si_height,Matrix &mat_H);
void     GetHomography_3DRotational    (double f_length1,double f_length2,Matrix &mat_Rr,int si_width,int si_height,Matrix &mat_H);
void     GetInverseOfCalibrationMatrix (double f_length,int si_width,int si_height,Matrix &mat_IK);
void     GetRotationMatrix             (Matrix &mat_H,double f_length1,double f_length2,int si_width,int si_height,Matrix &mat_R);
void     GetUndistortedImage           (GImage &s_image,double kappa,int option,GImage &d_image);
void     GetUndistortedImage           (BGRImage &s_image,double kappa,int option,BGRImage &d_image);
FPoint2D GetUndistortedPixelPosition   (FPoint2D &s_point,FPoint2D &c_point,double kappa);
FPoint2D GetUndistortedPixelPosition   (FPoint2D &s_point,FPoint2D &c_point,double kappa1,double kappa2);

}

#endif

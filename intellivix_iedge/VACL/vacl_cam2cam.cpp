#include "vacl_cam2cam.h"

 namespace VACL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Class: PTZPosition
//
///////////////////////////////////////////////////////////////////////////////

 BOOL PTZPosition::operator != (PTZPosition& pos) const

{
   if (pos.PanAngle != PanAngle) return TRUE;
   if (pos.TiltAngle != TiltAngle) return TRUE;
   if (pos.ZoomFactor != ZoomFactor) return TRUE;
   return FALSE;
}

 int PTZPosition::ReadFile (CXMLIO* pIO)

{
   static PTZPosition dv;
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PTZPos"))
   {
      xmlNode.Attribute (TYPE_ID (float), "P", &PanAngle  , &dv.PanAngle);
      xmlNode.Attribute (TYPE_ID (float), "T", &TiltAngle , &dv.TiltAngle);
      xmlNode.Attribute (TYPE_ID (float), "Z", &ZoomFactor, &dv.ZoomFactor);
      xmlNode.End ();
   }
   return (DONE);
}

 int PTZPosition::WriteFile (CXMLIO* pIO)

{
   static PTZPosition dv;
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PTZPos"))
   {
      xmlNode.Attribute (TYPE_ID (float), "P", &PanAngle  , &dv.PanAngle);
      xmlNode.Attribute (TYPE_ID (float), "T", &TiltAngle , &dv.TiltAngle);
      xmlNode.Attribute (TYPE_ID (float), "Z", &ZoomFactor, &dv.ZoomFactor);
      xmlNode.End ();
   }   
   return (DONE);
}


///////////////////////////////////////////////////////////////////////////////
//
// Class: MasterToSlaveCamera
//
///////////////////////////////////////////////////////////////////////////////

 MasterToSlaveCamera::MasterToSlaveCamera (   )
 
{
   HFOV            = 0.0f;
   FocalLength     = 500.0f;
   PanAngleOffset  = 0.0f;
   TiltAngleOffset = 0.0f;
   MCTiltPos       = 0.0f;
   MinPanAngle     = 0.0f;
   MaxPanAngle     = 360.0f;
   MinTiltAngle    = -90.0f;
   MaxTiltAngle    =  90.0f;
}

 int MasterToSlaveCamera::Calibrate (   )
 
{
   int i;
   
   if (!ImageSize.Width || !ImageSize.Height) return (1);
   if (CalibPoints.Length < 6) return (2);
   if (CalibPoints.Length != PTZPositions.Length) return (3);
   for (i = 0; i < CalibPoints.Length; i++)
      if (CalibPoints[i].X == MC_VLV || PTZPositions[i].PanAngle == MC_VLV) break;
   if (i != CalibPoints.Length) return (4);
   RotationAngles.Z = PTZPositions[0].TiltAngle;
   PanAngleOffset   = PTZPositions[0].PanAngle;
   Minimization_Powell::Initialize (6);
   ParamVector(0) = FocalLength;
   ParamVector(1) = RotationAngles.X;
   ParamVector(2) = RotationAngles.Z;
   ParamVector(3) = PanAngleOffset;
   ParamVector(4) = TiltAngleOffset;
   ParamVector(5) = HFOV;
   Perform (   );
   FocalLength      = (float)ParamVector(0);
   RotationAngles.X = (float)ParamVector(1);
   RotationAngles.Z = (float)ParamVector(2);
   PanAngleOffset   = (float)ParamVector(3);
   TiltAngleOffset  = (float)ParamVector(4);
   HFOV             = (float)ParamVector(5);
   return (DONE);
}

 void MasterToSlaveCamera::Close (   )
 
{
   Minimization_Powell::Close (   );
   CalibPoints.Delete  (   );
   PTZPositions.Delete (   );
}

 double MasterToSlaveCamera::CostFunction (Matrix &vec_p)

{
   int i;

   double sum = 0.0;
   for (i = 0; i < CalibPoints.Length; i++) {
      PTZPosition s_pos = GetPTPosition (CalibPoints[i],vec_p);
      float d_pa = s_pos.PanAngle  - PTZPositions[i].PanAngle;
      float d_ta = s_pos.TiltAngle - PTZPositions[i].TiltAngle;
      sum += d_pa * d_pa + d_ta * d_ta;
   }
   double rms_error = sqrt (sum / CalibPoints.Length);
   return (rms_error);
}

 FPoint2D MasterToSlaveCamera::GetPixelPosition (PTZPosition &s_pos)
 
{
   float x,y;
   
   double pa = s_pos.PanAngle  - PanAngleOffset;
   double ta = s_pos.TiltAngle - TiltAngleOffset;
   if      (pa < -180.0) pa += 360.0;
   else if (pa >  180.0) pa -= 360.0;
   Matrix vec_x2(3);
   double a  = tan (-pa / MC_RAD2DEG);
   vec_x2(1) = sin (ta  / MC_RAD2DEG);
   vec_x2(0) = sqrt((1.0 - vec_x2(1) * vec_x2(1)) / (1.0 + a * a));
   if (pa < -90.0 || pa > 90.0) vec_x2(0) = -vec_x2(0);
   vec_x2(2) = a * vec_x2(0);
   Matrix mat_R(3,3);
   GetRotationMatrix_XYZ ((double)MC_DEG2RAD * RotationAngles.X,(double)MC_DEG2RAD * RotationAngles.Y,(double)-MC_DEG2RAD * RotationAngles.Z,mat_R);
   Matrix vec_x1 = Inv(mat_R) * vec_x2;
   vec_x1 = Nor(vec_x1);
   if (HFOV == 0.0f) {
      x = (float)(FocalLength * vec_x1(0) / vec_x1(2));
      y = (float)(FocalLength * vec_x1(1) / vec_x1(2));
   }
   else {
      double phi_x = atan2 (vec_x1(2),vec_x1(0));
      x = (float)(ImageSize.Width / HFOV * phi_x / MC_DEG2RAD);
      y = (float)(FocalLength * tan (asin (vec_x1(1)) - MC_DEG2RAD * MCTiltPos));
   }
   FPoint2D d_point;
   d_point.X = x + 0.5f * ImageSize.Width;
   d_point.Y = y + 0.5f * ImageSize.Height;
   return (d_point);
}

 PTZPosition MasterToSlaveCamera::GetPTPosition (FPoint2D &s_point,Matrix &vec_p)

{
   double phi_x,phi_y;

   float x = s_point.X - 0.5f * ImageSize.Width;
   float y = s_point.Y - 0.5f * ImageSize.Height;
   if (HFOV == 0.0f) {
      phi_x = atan (x / vec_p(0));
      phi_y = atan (y / sqrt (x * x + vec_p(0) * vec_p(0)));
   }
   else {
      phi_x = MC_DEG2RAD * x * vec_p(5) / ImageSize.Width;
      phi_y = atan (y / vec_p(0)) + MC_DEG2RAD * MCTiltPos;
   }
   Matrix vec_x1(3);
   vec_x1(0) = cos (phi_x) * cos (phi_y);
   vec_x1(1) = sin (phi_y);
   vec_x1(2) = sin (phi_x) * cos (phi_y);
   Matrix mat_R(3,3);
   GetRotationMatrix_XYZ ((double)MC_DEG2RAD * vec_p(1),(double)MC_DEG2RAD * RotationAngles.Y,(double)-MC_DEG2RAD * vec_p(2),mat_R);
   Matrix vec_x2 = mat_R * vec_x1;
   vec_x2 = Nor(vec_x2);
   float d_pa = (float)(-MC_RAD2DEG * atan2 (vec_x2(2),vec_x2(0)) + vec_p(3));
   float d_ta = (float)(MC_RAD2DEG  * asin (vec_x2(1)) + vec_p(4));
   if (d_pa < 0.0f        ) d_pa += 360.0f;
   if (d_pa >= 360.0f     ) d_pa -= 360.0f;
   if (d_pa < MinPanAngle ) d_pa = MinPanAngle;
   if (d_pa > MaxPanAngle ) d_pa = MaxPanAngle;
   if (d_ta < MinTiltAngle) d_ta = MinTiltAngle;
   if (d_ta > MaxTiltAngle) d_ta = MaxTiltAngle;
   PTZPosition d_pos(d_pa,d_ta,1.0f);
   return (d_pos);
}

 PTZPosition MasterToSlaveCamera::GetPTPosition (FPoint2D &s_point)
 
{
   double phi_x,phi_y;

   float x = s_point.X - 0.5f * ImageSize.Width;
   float y = s_point.Y - 0.5f * ImageSize.Height;
   if (HFOV == 0.0f) {
      phi_x = atan (x / FocalLength);
      phi_y = atan (y / sqrt (x * x + FocalLength * FocalLength));
   }
   else {
      phi_x = MC_DEG2RAD * x * HFOV / ImageSize.Width;
      phi_y = atan (y / FocalLength) + MC_DEG2RAD * MCTiltPos;
   }
   Matrix vec_x1(3);
   vec_x1(0) = cos (phi_x) * cos (phi_y);
   vec_x1(1) = sin (phi_y);
   vec_x1(2) = sin (phi_x) * cos (phi_y);
   Matrix mat_R(3,3);
   GetRotationMatrix_XYZ ((double)MC_DEG2RAD * RotationAngles.X,(double)MC_DEG2RAD * RotationAngles.Y,(double)-MC_DEG2RAD * RotationAngles.Z,mat_R);
   Matrix vec_x2 = mat_R * vec_x1;
   vec_x2 = Nor(vec_x2);
   float d_pa = (float)(-MC_RAD2DEG * atan2 (vec_x2(2),vec_x2(0)) + PanAngleOffset);
   float d_ta = (float)(MC_RAD2DEG  * asin (vec_x2(1)) + TiltAngleOffset);
   if (d_pa < 0.0f        ) d_pa += 360.0f;
   if (d_pa >= 360.0f     ) d_pa -= 360.0f;
   if (d_pa < MinPanAngle ) d_pa = MinPanAngle;
   if (d_pa > MaxPanAngle ) d_pa = MaxPanAngle;
   if (d_ta < MinTiltAngle) d_ta = MinTiltAngle;
   if (d_ta > MaxTiltAngle) d_ta = MaxTiltAngle;
   PTZPosition d_pos(d_pa,d_ta,0.0f);
   return (d_pos);
}

 void MasterToSlaveCamera::Initialize (int mci_width,int mci_height,int n_calib_points)
 
{
   Close (   );
   ImageSize(mci_width,mci_height);
   CalibPoints.Create  (n_calib_points);
   PTZPositions.Create (n_calib_points);
   CalibPoints.Set     (0,CalibPoints.Length,FPoint2D(MC_VLV,MC_VLV));
   PTZPositions.Set    (0,PTZPositions.Length,PTZPosition(MC_VLV,MC_VLV,0.0f));
}

 int MasterToSlaveCamera::ReadFile (FileIO &file)
 
{
   int n;
   
   if (file.Read ((byte*)&ImageSize      ,1,sizeof(ISize2D)))  return (1);
   if (file.Read ((byte*)&FocalLength    ,1,sizeof(float)))    return (2);
   if (file.Read ((byte*)&HFOV           ,1,sizeof(float)))    return (3);
   if (file.Read ((byte*)&PanAngleOffset ,1,sizeof(float)))    return (4);
   if (file.Read ((byte*)&TiltAngleOffset,1,sizeof(float)))    return (5);
   if (file.Read ((byte*)&RotationAngles ,1,sizeof(FPoint3D))) return (6);
   if (file.Read ((byte*)&MCTiltPos      ,1,sizeof(float)))    return (7); 
   if (file.Read ((byte*)&MinPanAngle    ,1,sizeof(float)))    return (8);
   if (file.Read ((byte*)&MaxPanAngle    ,1,sizeof(float)))    return (9);
   if (file.Read ((byte*)&MinTiltAngle   ,1,sizeof(float)))    return (10);
   if (file.Read ((byte*)&MaxTiltAngle   ,1,sizeof(float)))    return (11);
   if (file.Read ((byte*)&n              ,1,sizeof(int)))      return (12);
   CalibPoints.Create (n);
   if (file.Read ((byte*)(FPoint2D*)CalibPoints,n,sizeof(FPoint2D))) return (13);
   if (file.Read ((byte*)&n              ,1,sizeof(int)))      return (14);
   PTZPositions.Create (n);
   if (file.Read ((byte*)(PTZPosition*)PTZPositions,n,sizeof(PTZPosition))) return (15);
   return (DONE);
}

 int MasterToSlaveCamera::WriteFile (FileIO &file)
 
{
   if (file.Write ((byte*)&ImageSize         ,1,sizeof(ISize2D)))  return (1);
   if (file.Write ((byte*)&FocalLength       ,1,sizeof(float)))    return (2);
   if (file.Write ((byte*)&HFOV              ,1,sizeof(float)))    return (3);
   if (file.Write ((byte*)&PanAngleOffset    ,1,sizeof(float)))    return (4);
   if (file.Write ((byte*)&TiltAngleOffset   ,1,sizeof(float)))    return (5);
   if (file.Write ((byte*)&MCTiltPos         ,1,sizeof(float)))    return (6);
   if (file.Write ((byte*)&RotationAngles    ,1,sizeof(FPoint3D))) return (7);
   if (file.Write ((byte*)&MinPanAngle       ,1,sizeof(float)))    return (8);
   if (file.Write ((byte*)&MaxPanAngle       ,1,sizeof(float)))    return (9);
   if (file.Write ((byte*)&MinTiltAngle      ,1,sizeof(float)))    return (10);
   if (file.Write ((byte*)&MaxTiltAngle      ,1,sizeof(float)))    return (11);
   if (file.Write ((byte*)&CalibPoints.Length,1,sizeof(int)))      return (12);
   if (file.Write ((byte*)(FPoint2D*)CalibPoints,CalibPoints.Length,sizeof(FPoint2D))) return (13);
   if (file.Write ((byte*)&PTZPositions.Length,1,sizeof(int))) return (14);
   if (file.Write ((byte*)(PTZPosition*)PTZPositions,PTZPositions.Length,sizeof(PTZPosition))) return (15);
   return (DONE);
}

 // jun: xml 입출력 추가
 int MasterToSlaveCamera::WriteFile (CXMLIO *xmlio)
 
{
   int i;
   CXMLNode xmlNode (xmlio);
   if (xmlNode.Start ("MasterToSlaveCamera"))
   {
      xmlNode.Attribute (TYPE_ID (ISize2D)  , "ImageSize"      , &ImageSize);
      xmlNode.Attribute (TYPE_ID (float)    , "FocalLength"    , &FocalLength);
      xmlNode.Attribute (TYPE_ID (float)    , "HFOV"           , &HFOV);
      xmlNode.Attribute (TYPE_ID (float)    , "PanAngleOffset" , &PanAngleOffset);
      xmlNode.Attribute (TYPE_ID (float)    , "TiltAngleOffset", &TiltAngleOffset);
      xmlNode.Attribute (TYPE_ID (FPoint3D) , "RotationAngles" , &RotationAngles);
      xmlNode.Attribute (TYPE_ID (float)    , "MCTiltPos"      , &MCTiltPos);
      xmlNode.Attribute (TYPE_ID (float)    , "MinPanAngle"    , &MinPanAngle);
      xmlNode.Attribute (TYPE_ID (float)    , "MaxPanAngle"    , &MaxPanAngle);
      xmlNode.Attribute (TYPE_ID (float)    , "MinTiltAngle"   , &MinTiltAngle);
      xmlNode.Attribute (TYPE_ID (float)    , "MaxTiltAngle"   , &MaxTiltAngle);
      BOOL flag_write_calibpoints = FALSE;
      for (i = 0; i < CalibPoints.Length; i++) {
         FPoint2D& point = CalibPoints[i];
         if ((point.X != MC_VLV) || point.Y != MC_VLV) {
            flag_write_calibpoints = TRUE;
            break;
         }
      }
      if (flag_write_calibpoints)
         xmlNode.Attribute (TYPE_ID (FPoint2D), "CalibPoints" , (FPoint2D *)CalibPoints, CalibPoints.Length);

      BOOL flag_write_ptzpos = FALSE;
      for (i = 0; i < PTZPositions.Length; i++) {
         if ((PTZPositions[i].PanAngle != MC_VLV) || PTZPositions[i].TiltAngle != MC_VLV) {
            flag_write_ptzpos = TRUE;
            break;
         }
      }
      if (flag_write_ptzpos) {
         for (i = 0; i < PTZPositions.Length; i++) 
            PTZPositions[i].WriteFile (xmlio);
      }      
      xmlNode.End ();
   }
   return (DONE);
}

  int MasterToSlaveCamera::ReadFile (CXMLIO *xmlio)
 
{
   int i;
   CXMLNode xmlNode (xmlio);
   if (xmlNode.Start ("MasterToSlaveCamera"))
   {
      xmlNode.Attribute (TYPE_ID (ISize2D)  , "ImageSize"      , &ImageSize);
      xmlNode.Attribute (TYPE_ID (float)    , "FocalLength"    , &FocalLength);
      xmlNode.Attribute (TYPE_ID (float)    , "HFOV"           , &HFOV);
      xmlNode.Attribute (TYPE_ID (float)    , "PanAngleOffset" , &PanAngleOffset);
      xmlNode.Attribute (TYPE_ID (float)    , "TiltAngleOffset", &TiltAngleOffset);
      xmlNode.Attribute (TYPE_ID (FPoint3D) , "RotationAngles" , &RotationAngles);
      xmlNode.Attribute (TYPE_ID (float)    , "MCTiltPos"      , &MCTiltPos);
      xmlNode.Attribute (TYPE_ID (float)    , "MinPanAngle"    , &MinPanAngle);
      xmlNode.Attribute (TYPE_ID (float)    , "MaxPanAngle"    , &MaxPanAngle);
      xmlNode.Attribute (TYPE_ID (float)    , "MinTiltAngle"   , &MinTiltAngle);
      xmlNode.Attribute (TYPE_ID (float)    , "MaxTiltAngle"   , &MaxTiltAngle);
      int n_points = 0;
      xmlNode.Attribute (TYPE_ID (int)       , "PTZPositionsNum"     , &n_points);
      xmlNode.Attribute (TYPE_ID (FPoint2D)  , "CalibPoints"         , (FPoint2D *)CalibPoints, n_points);
      for (i = 0; i < n_points; i++) 
         PTZPositions[i].ReadFile (xmlio);
      xmlNode.End ();
   }
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: MasterToSlaveCamera2
//
///////////////////////////////////////////////////////////////////////////////

 MasterToSlaveCamera2::MasterToSlaveCamera2 (   )
 
{
   HFOV         = 0.0f;
   FocalLength  = 500.0f;
   MCTiltPos    = 0.0f;
   MinPanAngle  = 0.0f;
   MaxPanAngle  = 360.0f;
   MinTiltAngle = -90.0f;
   MaxTiltAngle =  90.0f;
}

 int MasterToSlaveCamera2::Calibrate (   )
 
{
   int i;
   
   if (!ImageSize.Width || !ImageSize.Height) return (1);
   if (CalibPoints.Length < 6) return (2);
   if (CalibPoints.Length != PTZPositions.Length) return (3);
   for (i = 0; i < CalibPoints.Length; i++)
      if (CalibPoints[i].X == MC_VLV || PTZPositions[i].PanAngle == MC_VLV) break;
   if (i != CalibPoints.Length) return (4);
   Minimization_Powell::Initialize (5);
   ParamVector(0) = FocalLength;
   ParamVector(1) = RotationAngles.X;
   ParamVector(2) = RotationAngles.Y;
   ParamVector(3) = RotationAngles.Z;
   ParamVector(4) = HFOV;
   Perform (   );
   FocalLength      = (float)ParamVector(0);
   RotationAngles.X = (float)ParamVector(1);
   RotationAngles.Y = (float)ParamVector(2);
   RotationAngles.Z = (float)ParamVector(3);
   HFOV             = (float)ParamVector(4);
   return (DONE);
}

 void MasterToSlaveCamera2::Close (   )
 
{
   Minimization_Powell::Close (   );
   CalibPoints.Delete  (   );
   PTZPositions.Delete (   );
}

 double MasterToSlaveCamera2::CostFunction (Matrix &vec_p)

{
   int i;

   double sum = 0.0;
   for (i = 0; i < CalibPoints.Length; i++) {
      PTZPosition s_pos = GetPTPosition (CalibPoints[i],vec_p);
      float d_pa = s_pos.PanAngle  - PTZPositions[i].PanAngle;
      float d_ta = s_pos.TiltAngle - PTZPositions[i].TiltAngle;
      sum += d_pa * d_pa + d_ta * d_ta;
   }
   double rms_error = sqrt (sum / CalibPoints.Length);
   return (rms_error);
}

 FPoint2D MasterToSlaveCamera2::GetPixelPosition (PTZPosition &s_pos)
 
{
   float x,y;
   
   double pa = s_pos.PanAngle;
   double ta = s_pos.TiltAngle;
   if      (pa < -180.0) pa += 360.0;
   else if (pa >  180.0) pa -= 360.0;
   Matrix vec_x2(3);
   double a  = tan (pa * MC_DEG2RAD);
   vec_x2(1) = sin (ta * MC_DEG2RAD);
   vec_x2(0) = sqrt((1.0 - vec_x2(1) * vec_x2(1)) / (1.0 + a * a));
   if (pa < -90.0 || pa > 90.0) vec_x2(0) = -vec_x2(0);
   vec_x2(2) = a * vec_x2(0);
   Matrix mat_R(3,3);
   GetRotationMatrix_XYZ ((double)(MC_DEG2RAD * RotationAngles.X),(double)(MC_DEG2RAD * RotationAngles.Y),(double)(-MC_DEG2RAD * RotationAngles.Z),mat_R);
   Matrix vec_x1 = Inv(mat_R) * vec_x2;
   vec_x1 = Nor(vec_x1);
   if (HFOV == 0.0f) { // 참조 카메라가 일반 카메라인 경우
      x = (float)(FocalLength * vec_x1(0) / vec_x1(2));
      y = (float)(FocalLength * vec_x1(1) / vec_x1(2));
   }
   else { // 참조 카메라가 파노라마 카메라인 경우
      double phi_x = atan2 (vec_x1(2),vec_x1(0));
      x = (float)(ImageSize.Width / HFOV * phi_x * MC_RAD2DEG);
      y = (float)(FocalLength * tan (asin (vec_x1(1)) - MC_DEG2RAD * MCTiltPos));
   }
   FPoint2D d_point;
   d_point.X = x + 0.5f * ImageSize.Width;
   d_point.Y = y + 0.5f * ImageSize.Height;
   return (d_point);
}

 PTZPosition MasterToSlaveCamera2::GetPTPosition (FPoint2D &s_point,Matrix &vec_p)

{
   double phi_x,phi_y;

   float x = s_point.X - 0.5f * ImageSize.Width;
   float y = s_point.Y - 0.5f * ImageSize.Height;
   if (HFOV == 0.0f) { // 참조 카메라가 일반 카메라인 경우
      phi_x = atan (x / vec_p(0));
      phi_y = atan (y / sqrt (x * x + vec_p(0) * vec_p(0)));
   }
   else { // 참조 카메라가 파노라마 카메라인 경우
      phi_x = MC_DEG2RAD * x * vec_p(4) / ImageSize.Width;
      phi_y = atan (y / vec_p(0)) + MC_DEG2RAD * MCTiltPos;
   }
   Matrix vec_x1(3);
   vec_x1(0) = cos (phi_x) * cos (phi_y);
   vec_x1(1) = sin (phi_y);
   vec_x1(2) = sin (phi_x) * cos (phi_y);
   Matrix mat_R(3,3);
   GetRotationMatrix_XYZ (MC_DEG2RAD * vec_p(1),MC_DEG2RAD * vec_p(2),-MC_DEG2RAD * vec_p(3),mat_R);
   Matrix vec_x2 = mat_R * vec_x1;
   vec_x2 = Nor(vec_x2);
   float d_pa = (float)(MC_RAD2DEG * atan2 (vec_x2(2),vec_x2(0)));
   float d_ta = (float)(MC_RAD2DEG * asin  (vec_x2(1)));
   if (d_pa < 0.0f        ) d_pa += 360.0f;
   if (d_pa >= 360.0f     ) d_pa -= 360.0f;
   if (d_pa < MinPanAngle ) d_pa = MinPanAngle;
   if (d_pa > MaxPanAngle ) d_pa = MaxPanAngle;
   if (d_ta < MinTiltAngle) d_ta = MinTiltAngle;
   if (d_ta > MaxTiltAngle) d_ta = MaxTiltAngle;
   PTZPosition d_pos(d_pa,d_ta,1.0f);
   return (d_pos);
}

 PTZPosition MasterToSlaveCamera2::GetPTPosition (FPoint2D &s_point)
 
{
   double phi_x,phi_y;

   float x = s_point.X - 0.5f * ImageSize.Width;
   float y = s_point.Y - 0.5f * ImageSize.Height;
   if (HFOV == 0.0f) { // 참조 카메라가 일반 카메라인 경우
      phi_x = atan (x / FocalLength);
      phi_y = atan (y / sqrt (x * x + FocalLength * FocalLength));
   }
   else { // 참조 카메라가 파노라마 카메라인 경우
      phi_x = MC_DEG2RAD * x * HFOV / ImageSize.Width;
      phi_y = atan (y / FocalLength) + MC_DEG2RAD * MCTiltPos;
   }
   Matrix vec_x1(3);
   vec_x1(0) = cos (phi_x) * cos (phi_y);
   vec_x1(1) = sin (phi_y);
   vec_x1(2) = sin (phi_x) * cos (phi_y);
   Matrix mat_R(3,3);
   GetRotationMatrix_XYZ ((double)(MC_DEG2RAD * RotationAngles.X),(double)(MC_DEG2RAD * RotationAngles.Y),(double)(-MC_DEG2RAD * RotationAngles.Z),mat_R);
   Matrix vec_x2 = mat_R * vec_x1;
   vec_x2 = Nor(vec_x2);
   float d_pa = (float)(MC_RAD2DEG * atan2 (vec_x2(2),vec_x2(0)));
   float d_ta = (float)(MC_RAD2DEG * asin  (vec_x2(1)));
   if (d_pa < 0.0f        ) d_pa += 360.0f;
   if (d_pa >= 360.0f     ) d_pa -= 360.0f;
   if (d_pa < MinPanAngle ) d_pa = MinPanAngle;
   if (d_pa > MaxPanAngle ) d_pa = MaxPanAngle;
   if (d_ta < MinTiltAngle) d_ta = MinTiltAngle;
   if (d_ta > MaxTiltAngle) d_ta = MaxTiltAngle;
   PTZPosition d_pos(d_pa,d_ta,0.0f);
   return (d_pos);
}

 void MasterToSlaveCamera2::Initialize (int mci_width,int mci_height,int n_calib_points)
 
{
   Close (   );
   ImageSize(mci_width,mci_height);
   CalibPoints.Create  (n_calib_points);
   PTZPositions.Create (n_calib_points);
   CalibPoints.Set     (0,CalibPoints.Length,FPoint2D(MC_VLV,MC_VLV));
   PTZPositions.Set    (0,PTZPositions.Length,PTZPosition(MC_VLV,MC_VLV,0.0f));
}

 int MasterToSlaveCamera2::ReadFile (FileIO &file)
 
{
   int n;
   
   if (file.Read ((byte*)&ImageSize      ,1,sizeof(ISize2D)))  return (1);
   if (file.Read ((byte*)&FocalLength    ,1,sizeof(float)))    return (2);
   if (file.Read ((byte*)&HFOV           ,1,sizeof(float)))    return (3);
   if (file.Read ((byte*)&RotationAngles ,1,sizeof(FPoint3D))) return (4);
   if (file.Read ((byte*)&MCTiltPos      ,1,sizeof(float)))    return (5); 
   if (file.Read ((byte*)&MinPanAngle    ,1,sizeof(float)))    return (6);
   if (file.Read ((byte*)&MaxPanAngle    ,1,sizeof(float)))    return (7);
   if (file.Read ((byte*)&MinTiltAngle   ,1,sizeof(float)))    return (8);
   if (file.Read ((byte*)&MaxTiltAngle   ,1,sizeof(float)))    return (9);
   if (file.Read ((byte*)&n              ,1,sizeof(int)))      return (10);
   CalibPoints.Create (n);
   if (file.Read ((byte*)(FPoint2D*)CalibPoints,n,sizeof(FPoint2D))) return (11);
   if (file.Read ((byte*)&n              ,1,sizeof(int)))      return (12);
   PTZPositions.Create (n);
   if (file.Read ((byte*)(PTZPosition*)PTZPositions,n,sizeof(PTZPosition))) return (13);
   return (DONE);
}

 int MasterToSlaveCamera2::WriteFile (FileIO &file)
 
{
   if (file.Write ((byte*)&ImageSize         ,1,sizeof(ISize2D)))  return (1);
   if (file.Write ((byte*)&FocalLength       ,1,sizeof(float)))    return (2);
   if (file.Write ((byte*)&HFOV              ,1,sizeof(float)))    return (3);
   if (file.Write ((byte*)&RotationAngles    ,1,sizeof(FPoint3D))) return (4);
   if (file.Write ((byte*)&MCTiltPos         ,1,sizeof(float)))    return (5);
   if (file.Write ((byte*)&MinPanAngle       ,1,sizeof(float)))    return (6);
   if (file.Write ((byte*)&MaxPanAngle       ,1,sizeof(float)))    return (7);
   if (file.Write ((byte*)&MinTiltAngle      ,1,sizeof(float)))    return (8);
   if (file.Write ((byte*)&MaxTiltAngle      ,1,sizeof(float)))    return (9);
   if (file.Write ((byte*)&CalibPoints.Length,1,sizeof(int)))      return (10);
   if (file.Write ((byte*)(FPoint2D*)CalibPoints,CalibPoints.Length,sizeof(FPoint2D))) return (11);
   if (file.Write ((byte*)&PTZPositions.Length,1,sizeof(int))) return (12);
   if (file.Write ((byte*)(PTZPosition*)PTZPositions,PTZPositions.Length,sizeof(PTZPosition))) return (13);
   return (DONE);
}

 // jun: xml 입출력 추가
 int MasterToSlaveCamera2::WriteFile (CXMLIO *xmlio)
 
{
   int i;
   static MasterToSlaveCamera2 dv;
   CXMLNode xmlNode (xmlio);
   if (xmlNode.Start ("MasterToSlaveCamera2"))
   {
      xmlNode.Attribute (TYPE_ID (ISize2D)  , "ImageSize"      , &ImageSize      , &dv.ImageSize     );
      xmlNode.Attribute (TYPE_ID (float)    , "FocalLength"    , &FocalLength    , &dv.FocalLength   );
      xmlNode.Attribute (TYPE_ID (float)    , "HFOV"           , &HFOV           , &dv.HFOV          );
      xmlNode.Attribute (TYPE_ID (FPoint3D) , "RotationAngles" , &RotationAngles , &dv.RotationAngles);
      xmlNode.Attribute (TYPE_ID (float)    , "MCTiltPos"      , &MCTiltPos      , &dv.MCTiltPos     );
      xmlNode.Attribute (TYPE_ID (float)    , "MinPanAngle"    , &MinPanAngle    , &dv.MinPanAngle   );
      xmlNode.Attribute (TYPE_ID (float)    , "MaxPanAngle"    , &MaxPanAngle    , &dv.MaxPanAngle   );
      xmlNode.Attribute (TYPE_ID (float)    , "MinTiltAngle"   , &MinTiltAngle   , &dv.MinTiltAngle  );
      xmlNode.Attribute (TYPE_ID (float)    , "MaxTiltAngle"   , &MaxTiltAngle   , &dv.MaxTiltAngle  );
      BOOL flag_write_calibpoints = FALSE;
      for (i = 0; i < CalibPoints.Length; i++) {
         FPoint2D& point = CalibPoints[i];
         if ((point.X != MC_VLV) || point.Y != MC_VLV) {
            flag_write_calibpoints = TRUE;
            break;
         }
      }
      if (flag_write_calibpoints)
         xmlNode.Attribute (TYPE_ID (FPoint2D), "CalibPoints" , (FPoint2D *)CalibPoints, CalibPoints.Length);

      BOOL flag_write_ptzpos = FALSE;
      for (i = 0; i < PTZPositions.Length; i++) {
         if ((PTZPositions[i].PanAngle != MC_VLV) || PTZPositions[i].TiltAngle != MC_VLV) {
            flag_write_ptzpos = TRUE;
            break;
         }
      }
      if (flag_write_ptzpos) {
         for (i = 0; i < PTZPositions.Length; i++) 
            PTZPositions[i].WriteFile (xmlio);
      }      
      xmlNode.End ();
   }
   return (DONE);
}

  int MasterToSlaveCamera2::ReadFile (CXMLIO *xmlio)
 
{
   int i;
   CXMLNode xmlNode (xmlio);
   if (xmlNode.Start ("MasterToSlaveCamera2"))
   {
      xmlNode.Attribute (TYPE_ID (ISize2D)  , "ImageSize"       , &ImageSize);
      xmlNode.Attribute (TYPE_ID (float)    , "FocalLength"     , &FocalLength);
      xmlNode.Attribute (TYPE_ID (float)    , "HFOV"            , &HFOV);
      xmlNode.Attribute (TYPE_ID (FPoint3D) , "RotationAngles"  , &RotationAngles);
      xmlNode.Attribute (TYPE_ID (float)    , "MCTiltPos"       , &MCTiltPos);
      xmlNode.Attribute (TYPE_ID (float)    , "MinPanAngle"     , &MinPanAngle);
      xmlNode.Attribute (TYPE_ID (float)    , "MaxPanAngle"     , &MaxPanAngle);
      xmlNode.Attribute (TYPE_ID (float)    , "MinTiltAngle"    , &MinTiltAngle);
      xmlNode.Attribute (TYPE_ID (float)    , "MaxTiltAngle"    , &MaxTiltAngle);
      int n_points = 0;
      xmlNode.Attribute (TYPE_ID (int)       , "PTZPositionsNum", &n_points);
      xmlNode.Attribute (TYPE_ID (FPoint2D)  , "CalibPoints"    , (FPoint2D *)CalibPoints, n_points);
      for (i = 0; i < n_points; i++) 
         PTZPositions[i].ReadFile (xmlio);
      xmlNode.End ();
   }
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CameraToCamera
//
///////////////////////////////////////////////////////////////////////////////
 
 CameraToCamera::CameraToCamera (   )
 
{
   Name[0] = 0;
   Mat_H   = IM(3);
}

 FPoint3D CameraToCamera::GetDirectionVector (PTZPosition &s_pos)
 
{
   double phi_x = MC_DEG2RAD * s_pos.PanAngle;
   double phi_y = MC_DEG2RAD * s_pos.TiltAngle;
   double cpy   = cos (phi_y);
   FPoint3D vec_d;
   vec_d.X = (float)(cos (phi_x) * cpy);
   vec_d.Y = (float)sin (phi_y);
   vec_d.Z = (float)(sin (phi_x) * cpy);
   return (vec_d);
}

 Matrix CameraToCamera::GetPolygon (PTZPArray1D &sp_array,BCCL::Polygon &d_polygon)
 
{
   int i;
   
   FPoint3D az(0.0f,0.0f,0.0f);
   for (i = 0; i < sp_array.Length; i++)
      az += GetDirectionVector (sp_array[i]);
   Matrix vec_p1 = GetDirectionVector (sp_array[0]);
   Matrix vec_az = Nor(az);
   Matrix vec_ax = Nor(CP(vec_az,vec_p1));
   Matrix vec_ay = Nor(CP(vec_ax,vec_az));
   Matrix mat_Rt(3,3);
   mat_Rt.Copy (vec_ax,0,0,3,1,0,0);
   mat_Rt.Copy (vec_ay,0,0,3,1,0,1);
   mat_Rt.Copy (vec_az,0,0,3,1,0,2);
   Matrix mat_R = Tr(mat_Rt);
   d_polygon.Create (sp_array.Length);
   for (i = 0; i < sp_array.Length; i++)
      d_polygon.Vertices[i] = GetProjectedPosition (sp_array[i],mat_R);
   return (Matrix(&mat_R));
}

 IPoint2D CameraToCamera::GetProjectedPosition (PTZPosition &s_pos,Matrix &mat_R,float focal_length)
 
{
   Matrix vec_x1 = GetDirectionVector (s_pos);
   Matrix vec_x2 = mat_R * vec_x1;
   IPoint2D d_point;
   d_point.X = (int)(focal_length * vec_x2(0) / vec_x2(2));
   d_point.Y = (int)(focal_length * vec_x2(1) / vec_x2(2));
   return (d_point);
}

 PTZPosition CameraToCamera::GetPTPosition (FPoint3D &vec_s)

{
   PTZPosition d_pos;
   FPoint3D vec_x = Nor(vec_s);
   if (vec_x.Y < 0.0f) vec_x = -vec_x;
   d_pos.PanAngle  = (float)(MC_RAD2DEG * atan2 (vec_x.Z,vec_x.X));
   d_pos.TiltAngle = (float)(MC_RAD2DEG * asin  (vec_x.Y));
   return (d_pos);
}

 int CameraToCamera::ReadFile (FileIO &file)
 
{
   int i;
  
   if (file.Read ((byte*)Name,1,sizeof(Name))) return (1);
   for (i = 0; i < 9; i++)
      if (file.Read ((byte*)&Mat_H(i),1,sizeof(double))) return (1);
   return (DONE);
}

 int CameraToCamera::ReadFile (CXMLIO* pIO)

{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("CameraToCamera"))
   {
      xmlNode.Attribute (TYPE_ID (char), "Name", Name, sizeof(Name));
      Mat_H.ReadFile (pIO);
      xmlNode.End ();
   }
   return (DONE);
}

 int CameraToCamera::WriteFile (FileIO &file)

{
   int i;

   if (file.Write ((byte*)Name,1,sizeof(Name))) return (1);
   for (i = 0; i < 9; i++)
      if (file.Write ((byte*)&Mat_H(i),1,sizeof(double))) return (1);
   return (DONE);
}

 int CameraToCamera::WriteFile (CXMLIO* pIO)

{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("CameraToCamera"))
   {
      xmlNode.Attribute (TYPE_ID (char), "Name", Name, sizeof(Name));
      Mat_H.WriteFile (pIO);
      xmlNode.End ();
   }   
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: StaticToStaticCamera
//
///////////////////////////////////////////////////////////////////////////////

 int StaticToStaticCamera::Calibrate (   )
 
{
   int i;
   
   if (CalibPoints1.Length < 4) return (1);
   if (CalibPoints1.Length != CalibPoints2.Length) return (2);
   for (i = 0; i < CalibPoints1.Length; i++)
      if (CalibPoints1[i].X == MC_VLV || CalibPoints2[i].X == MC_VLV) break;
   if (i != CalibPoints1.Length) return (3);
   GetHomography (CalibPoints1,CalibPoints2,Mat_H);
   return (DONE);
}

 void StaticToStaticCamera::Close (   )
 
{
   CalibPoints1.Delete (   );
   CalibPoints2.Delete (   );
}

 FPoint2D StaticToStaticCamera::GetHandoffPosition12 (FPoint2D &s_point)
 
{
   Matrix vec_x1 = GetHomogeneousVector (s_point);
   Matrix vec_x2 = Mat_H * vec_x1;
   FPoint2D d_point;
   d_point.X = (float)(vec_x2(0) / vec_x2(2));
   d_point.Y = (float)(vec_x2(1) / vec_x2(2));
   return (d_point);
}

 FPoint2D StaticToStaticCamera::GetHandoffPosition21 (FPoint2D &s_point)

{
   Matrix vec_x2 = GetHomogeneousVector (s_point);
   Matrix vec_x1 = Inv(Mat_H) * vec_x2;
   FPoint2D d_point;
   d_point.X = (float)(vec_x1(0) / vec_x1(2));
   d_point.Y = (float)(vec_x1(1) / vec_x1(2));
   return (d_point);
}

 void StaticToStaticCamera::Initialize (int n_calib_points)
 
{
   Close (   );
   Mat_H = IM(3);
   CalibPoints1.Create (n_calib_points);
   CalibPoints2.Create (n_calib_points);
   CalibPoints1.Set    (0,n_calib_points,FPoint2D(MC_VLV,MC_VLV));
   CalibPoints2.Set    (0,n_calib_points,FPoint2D(MC_VLV,MC_VLV));
}

 int StaticToStaticCamera::ReadFile (FileIO &file)
 
{
   int n;

   if (CameraToCamera::ReadFile (file)) return (1);
   if (file.Read ((byte*)&n,1,sizeof(int))) return (2);
   CalibPoints1.Create (n);
   if (file.Read ((byte*)(FPoint2D*)CalibPoints1,n,sizeof(FPoint2D))) return (3);
   if (file.Read ((byte*)&n,1,sizeof(int))) return (4);
   CalibPoints2.Create (n);
   if (file.Read ((byte*)(FPoint2D*)CalibPoints2,n,sizeof(FPoint2D))) return (5);
   return (DONE);
}

 int StaticToStaticCamera::ReadFile (CXMLIO* pIO)

{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("StaticToStaticCamera"))
   {
      if (CameraToCamera::ReadFile (pIO)) return (1);
      xmlNode.Attribute (TYPE_ID (int),      "CalibPntsLen1", &CalibPoints1.Length);
      xmlNode.Attribute (TYPE_ID (FPoint2D), "CalibPnts1"   , CalibPoints1, CalibPoints1.Length);
      xmlNode.Attribute (TYPE_ID (int),      "CalibPntsLen2", &CalibPoints2.Length);
      xmlNode.Attribute (TYPE_ID (FPoint2D), "CalibPnts2"   , CalibPoints2, CalibPoints2.Length);
      xmlNode.End ();
   }
   return (DONE);
}

 int StaticToStaticCamera::WriteFile (FileIO &file)

{
   if (CameraToCamera::WriteFile (file)) return (1);
   if (file.Write ((byte*)&CalibPoints1.Length,1,sizeof(int))) return (2);
   if (file.Write ((byte*)(FPoint2D*)CalibPoints1,CalibPoints1.Length,sizeof(FPoint2D))) return (3);
   if (file.Write ((byte*)&CalibPoints2.Length,1,sizeof(int))) return (4);
   if (file.Write ((byte*)(FPoint2D*)CalibPoints2,CalibPoints2.Length,sizeof(FPoint2D))) return (5);
   return (DONE);
}

 int StaticToStaticCamera::WriteFile (CXMLIO* pIO)

{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("StaticToStaticCamera"))
   {
      if (CameraToCamera::WriteFile (pIO)) return (1);
      xmlNode.Attribute (TYPE_ID (int),      "CalibPntsLen1", &CalibPoints1.Length);
      xmlNode.Attribute (TYPE_ID (FPoint2D), "CalibPnts1"   , CalibPoints1, CalibPoints1.Length);
      xmlNode.Attribute (TYPE_ID (int),      "CalibPntsLen2", &CalibPoints2.Length);
      xmlNode.Attribute (TYPE_ID (FPoint2D), "CalibPnts2"   , CalibPoints2, CalibPoints2.Length);
      xmlNode.End ();
   }   
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: StaticToPTZCamera
//
///////////////////////////////////////////////////////////////////////////////

 int StaticToPTZCamera::Calibrate (   )
 
{
   int i;
   
   if (CalibPoints.Length < 4) return (1);
   if (CalibPoints.Length != PTZPositions.Length) return (2);
   for (i = 0; i < CalibPoints.Length; i++)
      if (CalibPoints[i].X == MC_VLV || PTZPositions[i].PanAngle == MC_VLV) break;
   if (i != CalibPoints.Length) return (3);
   FP3DArray1D s_points1(CalibPoints.Length);
   FP3DArray1D s_points2(PTZPositions.Length);
   for (i = 0; i < CalibPoints.Length; i++) {
      s_points1[i].X = CalibPoints[i].X;
      s_points1[i].Y = CalibPoints[i].Y;
      s_points1[i].Z = 1.0f;
      s_points2[i] = GetDirectionVector (PTZPositions[i]);
   }
   GetHomography (s_points1,s_points2,Mat_H);
   return (DONE);
}

 void StaticToPTZCamera::Close (   )
 
{
   CalibPoints.Delete  (   );
   PTZPositions.Delete (   );
}

 PTZPosition StaticToPTZCamera::GetHandoffPosition12 (FPoint2D &s_point)

{
   Matrix vec_x1 = GetHomogeneousVector (s_point);
   Matrix vec_x2 = Mat_H * vec_x1;
   FPoint3D vec_d((float)vec_x2(0),(float)vec_x2(1),(float)vec_x2(2));
   PTZPosition d_pos = CameraToCamera::GetPTPosition (vec_d);
   return (d_pos);
}

 FPoint2D StaticToPTZCamera::GetHandoffPosition21 (PTZPosition &s_pos)
 
{
   FPoint3D vec_s = GetDirectionVector (s_pos);
   Matrix vec_x2  = vec_s;
   Matrix vec_x1  = Inv(Mat_H) * vec_x2;
   FPoint2D d_point;
   d_point.X = (float)(vec_x1(0) / vec_x1(2));
   d_point.Y = (float)(vec_x1(1) / vec_x1(2));
   return (d_point);
}

 void StaticToPTZCamera::Initialize (int n_calib_points)
 
{
   Close (   );
   Mat_H = IM(3);
   CalibPoints.Create  (n_calib_points);
   PTZPositions.Create (n_calib_points);
   CalibPoints.Set     (0,n_calib_points,FPoint2D(MC_VLV,MC_VLV));
   PTZPositions.Set    (0,n_calib_points,PTZPosition(MC_VLV,MC_VLV,0.0f));
}

 int StaticToPTZCamera::ReadFile (FileIO &file)
 
{
   int n;

   if (CameraToCamera::ReadFile (file)) return (1);
   if (file.Read ((byte*)&n,1,sizeof(int))) return (2);
   CalibPoints.Create (n);
   if (file.Read ((byte*)(FPoint2D*)CalibPoints,n,sizeof(FPoint2D))) return (3);
   if (file.Read ((byte*)&n,1,sizeof(int))) return (4);
   PTZPositions.Create (n);
   if (file.Read ((byte*)(PTZPosition*)PTZPositions,n,sizeof(PTZPosition))) return (5);
   return (DONE);
}

 int StaticToPTZCamera::ReadFile (CXMLIO* pIO)

{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("StaticToPTZCamera"))
   {
      if (CameraToCamera::ReadFile (pIO)) return (1);
      xmlNode.Attribute (TYPE_ID (int),      "CalibPntsLen", &CalibPoints.Length);
      xmlNode.Attribute (TYPE_ID (FPoint2D), "CalibPnts"   , CalibPoints, CalibPoints.Length);
      xmlNode.Attribute (TYPE_ID (int),      "PTZPosLen"   , &PTZPositions.Length);
      xmlNode.Attribute (TYPE_ID (FPoint3D), "PTZPos"      , (FPoint3D*)(PTZPosition*)PTZPositions, PTZPositions.Length);
      xmlNode.End ();
   }
   return (DONE);
}

 int StaticToPTZCamera::WriteFile (FileIO &file)

{
   if (CameraToCamera::WriteFile (file)) return (1);
   if (file.Write ((byte*)&CalibPoints.Length,1,sizeof(int))) return (2);
   if (file.Write ((byte*)(FPoint2D*)CalibPoints,CalibPoints.Length,sizeof(FPoint2D))) return (3);
   if (file.Write ((byte*)&PTZPositions.Length,1,sizeof(int))) return (4);
   if (file.Write ((byte*)(PTZPosition*)PTZPositions,PTZPositions.Length,sizeof(PTZPosition))) return (5);
   return (DONE);
}

 int StaticToPTZCamera::WriteFile (CXMLIO* pIO)

{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("StaticToPTZCamera"))
   {
      if (CameraToCamera::WriteFile (pIO)) return (1);
      xmlNode.Attribute (TYPE_ID (int),      "CalibPntsLen", &CalibPoints.Length);
      xmlNode.Attribute (TYPE_ID (FPoint2D), "CalibPnts"   , CalibPoints, CalibPoints.Length);
      xmlNode.Attribute (TYPE_ID (int),      "PTZPosLen"   , &PTZPositions.Length);
      xmlNode.Attribute (TYPE_ID (FPoint3D), "PTZPos"      , (FPoint3D*)(PTZPosition*)PTZPositions, PTZPositions.Length);
      xmlNode.End ();
   }   
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: PC_PTZToPTZCam
//
///////////////////////////////////////////////////////////////////////////////

 int PC_PTZToPTZCam::ReadFile (CXMLIO* pIO)

{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PC_PTZToPTZCam"))
   {
      xmlNode.Attribute (TYPE_ID (FPoint3D), "PTZPos1", &PTZPos1);
      xmlNode.Attribute (TYPE_ID (FPoint3D), "PTZPos2", &PTZPos2);
      xmlNode.End ();
   }
   return (DONE);
}

 int PC_PTZToPTZCam::WriteFile (CXMLIO* pIO)

{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PC_PTZToPTZCam"))
   {
      xmlNode.Attribute (TYPE_ID (FPoint3D), "PTZPos1", &PTZPos1);
      xmlNode.Attribute (TYPE_ID (FPoint3D), "PTZPos2", &PTZPos2);
      xmlNode.End ();
   }
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: PTZToPTZCamera
//
///////////////////////////////////////////////////////////////////////////////

 PTZToPTZCamera::PTZToPTZCamera (   )

{
   HandoffZoomFactor = 2.0f;
}

 int PTZToPTZCamera::Calibrate (   )
 
{
   int i;
   
   int n_items = PCList.GetNumItems (   );
   if (n_items < 4) return (1);
   FP3DArray1D s_points1(n_items);
   FP3DArray1D s_points2(n_items);
   PC_PTZToPTZCam *pc = PCList.First (   );
   for (i = 0; pc != NULL; i++) {
      s_points1[i] = GetDirectionVector (pc->PTZPos1);
      s_points2[i] = GetDirectionVector (pc->PTZPos2);
      pc = PCList.Next (pc);
   }
   GetHomography (s_points1,s_points2,Mat_H);
   return (DONE);
}

 void PTZToPTZCamera::Close (   )
 
{
   Mat_R.Delete       (   );
   HandoffArea.Delete (   );
   PCList.Delete      (   );
}

 PTZPosition PTZToPTZCamera::GetHandoffPosition12 (PTZPosition &s_pos)
 
{
   FPoint3D vec_s1 = GetDirectionVector (s_pos);
   Matrix vec_x1   = vec_s1;
   Matrix vec_x2   = Mat_H * vec_x1;
   FPoint3D vec_s2((float)vec_x2(0),(float)vec_x2(1),(float)vec_x2(2));
   PTZPosition d_pos = GetPTPosition (vec_s2);
   return (d_pos);
}

 PTZPosition PTZToPTZCamera::GetHandoffPosition21 (PTZPosition &s_pos)
 
{
   FPoint3D vec_s2 = GetDirectionVector (s_pos);
   Matrix vec_x2   = vec_s2;
   Matrix vec_x1   = Inv(Mat_H) * vec_x2;
   FPoint3D vec_s1((float)vec_x1(0),(float)vec_x1(1),(float)vec_x1(2));
   PTZPosition d_pos = GetPTPosition (vec_s1);
   return (d_pos);
}

 int PTZToPTZCamera::IsPositionInsideHandoffArea (PTZPosition &s_pos)
 
{
   if (!Mat_R) return (FALSE);
   IPoint2D p_pos = GetProjectedPosition (s_pos,Mat_R);
   return (HandoffArea.IsPointInsidePolygon (p_pos));
}

 int PTZToPTZCamera::ReadFile (FileIO &file)
 
{
   int i,n_items;

   if (CameraToCamera::ReadFile (file)) return (1);
   if (file.Read ((byte*)&n_items,1,sizeof(int))) return (2);
   for (i = 0; i < n_items; i++) {
      PC_PTZToPTZCam *pc = new PC_PTZToPTZCam;
      if (file.Read ((byte*)&pc->PTZPos1,1,sizeof(PTZPosition))) return (3);
      if (file.Read ((byte*)&pc->PTZPos2,1,sizeof(PTZPosition))) return (4);
   }
   SetHandoffArea (   );
   return (DONE);
}

 int PTZToPTZCamera::ReadFile (CXMLIO* pIO)

{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PTZToPTZCamera"))
   {
      xmlNode.Attribute (TYPE_ID (float), "HandoffZoomFactor", &HandoffZoomFactor);
      if (CameraToCamera::ReadFile (pIO)) return (1);
      BCCL::ReadFile (PCList, pIO, "PCList");
      xmlNode.End ();
   }
   SetHandoffArea (   );
   return (DONE);
}

 int PTZToPTZCamera::SetHandoffArea (   )

{
   int i;
   
   int n_items = PCList.GetNumItems (   );
   if (n_items < 4) return (1);
   PTZPArray1D p_array(n_items);
   PC_PTZToPTZCam *pc = PCList.First (   );
   for (i = 0; pc != NULL; i++) {
      p_array[i] = pc->PTZPos1;
      pc = PCList.Next (pc);
   }
   Mat_R = GetPolygon (p_array,HandoffArea);
   return (DONE);
}

 int PTZToPTZCamera::WriteFile (FileIO &file)

{
   if (CameraToCamera::WriteFile (file)) return (1);
   int n_items = PCList.GetNumItems (   );
   if (file.Write ((byte*)&n_items,1,sizeof(int))) return (2);
   PC_PTZToPTZCam *pc = PCList.First (   );
   while (pc != NULL) {
      if (file.Write ((byte*)&pc->PTZPos1,1,sizeof(PTZPosition))) return (3);
      if (file.Write ((byte*)&pc->PTZPos1,2,sizeof(PTZPosition))) return (4);
      pc = PCList.Next (pc);
   }
   return (DONE);
}

 int PTZToPTZCamera::WriteFile (CXMLIO* pIO)

{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("PTZToPTZCamera"))
   {
      xmlNode.Attribute (TYPE_ID (float), "HandoffZoomFactor", &HandoffZoomFactor);
      if (CameraToCamera::WriteFile (pIO)) return (1);
      BCCL::WriteFile (PCList, pIO, "PCList");
      xmlNode.End ();
   }   
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: HandoffInfo_PTZToPTZCam
//
///////////////////////////////////////////////////////////////////////////////

 HandoffInfo_PTZToPTZCam::HandoffInfo_PTZToPTZCam (   )
 
{
   Name[0] = 0;
   memset (IPAddress,0,sizeof(IPAddress));
}

 int HandoffInfo_PTZToPTZCam::Calibrate (   )
 
{
   int r_code = DONE;
   PTZToPTZCamera *camera = GetFirstItem (   );
   while (camera != NULL) {
      if (camera->Calibrate (   )) r_code = 1;
      camera = GetNextItem (camera);
   }
   return (r_code);
}

 int HandoffInfo_PTZToPTZCam::GetHandoffPosition12 (PTZPosition &s_pos,PTZPosition &d_pos)
 
{
   int flag_inside = FALSE;
   PTZToPTZCamera *camera = GetFirstItem (   );
   while (camera != NULL) {
      if (camera->IsPositionInsideHandoffArea (s_pos)) {
         d_pos = camera->GetHandoffPosition12 (s_pos);
         flag_inside = TRUE;
         break;
      }
      camera = GetNextItem (camera);
   }
   return (flag_inside);
}

 int HandoffInfo_PTZToPTZCam::ReadFile (FileIO &file)
 
{
   int i,n_areas;
   
   Delete (   );
   if (file.Read ((byte*)Name,1,sizeof(Name))) return (1);
   if (file.Read ((byte*)IPAddress,1,sizeof(IPAddress))) return (2);
   if (file.Read ((byte*)&n_areas,1,sizeof(int))) return (3);
   for (i = 0; i < n_areas; i++) {
      PTZToPTZCamera *ho_area = new PTZToPTZCamera;
      if (ho_area->ReadFile (file)) {
         delete ho_area;
         return (4);
      }
      else Add (ho_area);
   }
   return (DONE);
}

 int HandoffInfo_PTZToPTZCam::ReadFile (CXMLIO* pIO)

{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("HandoffInfo_PTZToPTZCam"))
   {
      xmlNode.Attribute (TYPE_ID (char), "Name"     , Name,      sizeof(Name));
      xmlNode.Attribute (TYPE_ID (byte), "IPAddress", IPAddress, sizeof(IPAddress));
      BCCL::ReadFile (*this, pIO, "PCList");
      xmlNode.End ();
   }
   return (DONE);
}

 int HandoffInfo_PTZToPTZCam::SetHandoffAreas (   )
 
{
   int r_code = DONE;
   PTZToPTZCamera *camera = GetFirstItem (   );
   while (camera != NULL) {
      if (camera->SetHandoffArea (   )) r_code = 1;
      camera = GetNextItem (camera);
   }
   return (r_code);
}

 int HandoffInfo_PTZToPTZCam::WriteFile (FileIO &file)
 
{
   int n_areas = GetNumItems (   );
   if (file.Write ((byte*)Name,1,sizeof(Name))) return (1);
   if (file.Write ((byte*)IPAddress,1,sizeof(IPAddress))) return (2);
   if (file.Write ((byte*)&n_areas,1,sizeof(int))) return (3);
   PTZToPTZCamera *ho_area = GetFirstItem (   );
   while (ho_area != NULL) {
      if (ho_area->WriteFile (file)) return (4);
      ho_area = GetNextItem (ho_area);
   }
   return (DONE);
}

 int HandoffInfo_PTZToPTZCam::WriteFile (CXMLIO* pIO)

{
   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("HandoffInfo_PTZToPTZCam"))
   {
      xmlNode.Attribute (TYPE_ID (char), "Name"     , Name,      sizeof(Name));
      xmlNode.Attribute (TYPE_ID (byte), "IPAddress", IPAddress, sizeof(IPAddress));
      BCCL::WriteFile (*this, pIO, "PCList");
      xmlNode.End ();
   }   
   return (DONE);
}

}

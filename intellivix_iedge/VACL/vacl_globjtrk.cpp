#include "vacl_globjtrk.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: LocalView
//
///////////////////////////////////////////////////////////////////////////////

 LocalView::LocalView (   )
 
{
   ID            = 0;
   ObjectTracker = NULL;
   Mat_H         = IM(3);
   Mat_IH        = IM(3);
}

 FPoint2D LocalView::GetGlobalViewPos (FPoint2D& s_pos)

 {
   Matrix vec_lp = GetHomogeneousVector (s_pos);
   Matrix vec_gp = Mat_H * vec_lp;
   FPoint2D d_pos;
   GetInhomogeneousCoordinates (vec_gp,d_pos);
   return (d_pos);
 }

 FPoint2D LocalView::GetGlobalViewPos (TrackedObject* t_object)

{
   FPoint2D scale;
   scale.X = (float)VidFrmSize.Width  / ObjectTracker->SrcImgSize.Width;
   scale.Y = (float)VidFrmSize.Height / ObjectTracker->SrcImgSize.Height;
   FPoint2D d_pos = t_object->GetPosition (0.5f,1.0f);
   d_pos *= scale;
   return (GetGlobalViewPos (d_pos));
}

 void LocalView::Initialize (ISize2D& vf_size,Matrix &mat_H,ObjectTracking& obj_tracker)
 
{
   VidFrmSize    = vf_size;
   Mat_H         = mat_H;
   Mat_IH        = Inv(mat_H);
   ObjectTracker = &obj_tracker;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: LocalTrackedObject
//
///////////////////////////////////////////////////////////////////////////////

 LocalTrackedObject::LocalTrackedObject (   )
 
{
   BirthCount    = 0;
   Time_Unstable = 0.0f;
   View          = NULL;
   TrkObject     = NULL;
}

 float LocalTrackedObject::GetWeight (   )

{
   float weight = TrkObject->GetHumanRectAreaRatio (   );
   if (weight > 1.0f) weight = 1.0f;
   return (weight);
}

 void LocalTrackedObject::Initialize (LocalView* l_view,TrackedObject* t_object,int b_count)
 
{
   View          = l_view;
   TrkObject     = t_object;
   BirthCount    = b_count;
   Time_Unstable = 0.0f;
   UpdateGlobalViewPos (   );
}

 void LocalTrackedObject::UpdateGlobalViewPos (   )
 
{
   if (View == NULL || TrkObject == NULL) return;
   GlobalPos = View->GetGlobalViewPos (TrkObject);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: GlobalTrackedObject
//
///////////////////////////////////////////////////////////////////////////////

 GlobalTrackedObject::GlobalTrackedObject (   )
 
{
   ID                = -1;
   PlayerID          = -1;
   Status            = 0;
   BirthCount        = 0;
   Count_Tracked     = 0;
   Radius            = 0.0f;
   MaxMovingDistance = 0.0f;
   Time_Lost         = 0.0f;
   Time_Tracked      = 0.0f;
   Time_Unstable     = 0.0f;
   Velocity (MC_VLV,MC_VLV);
   Velocity2(MC_VLV,MC_VLV);
}

 LocalTrackedObject* GlobalTrackedObject::FindLTO (LocalView* l_view)

{
   LocalTrackedObject* lt_object = LTOs.First (   );
   while (lt_object != NULL) {
      if (lt_object->View == l_view) break;
      lt_object = LTOs.Next (lt_object);
   }
   return (lt_object);
}

 LocalTrackedObject* GlobalTrackedObject::FindLTO (TrackedObject* t_object)

{
   LocalTrackedObject* lt_object = LTOs.First (   );
   while (lt_object != NULL) {
      if (lt_object->TrkObject == t_object) break;
      lt_object = LTOs.Next (lt_object);
   }
   return (lt_object);
}

 float GlobalTrackedObject::GetDistance (LocalTrackedObject* lt_object)

{
   return (Mag(PredPos - lt_object->GlobalPos));
}

 void GlobalTrackedObject::GetPredictedPos (   )

{
   if (Velocity.X == MC_VLV) PredPos = Pos;
   else PredPos = Pos + Velocity;
   if (Velocity2.X == MC_VLV) PredPos2 = Pos;
   else PredPos2 = Pos2 + Velocity2;
}

 void GlobalTrackedObject::Initialize (LocalView* l_view,TrackedObject* t_object,float radius,int b_count)

{
   Radius     = radius;
   BirthCount = b_count;
   LocalTrackedObject* lt_object = new LocalTrackedObject;
   lt_object->Initialize (l_view,t_object,b_count);
   LTOs.Add (lt_object);
   InitPos = lt_object->GlobalPos;
   Pos  = PredPos  = InitPos;
   Pos2 = PredPos2 = InitPos;
}

 void GlobalTrackedObject::UpdatePos (   )

{
   float pu_rate   = 0.8f; // PARAMETERS
   float vu_rate   = 0.4f; // PARAMETERS
   float pu_rate2  = 0.2f; // PARAMETERS
   float vu_rate2  = 0.1f; // PARAMETERS
   FPoint2D p_pos  = Pos;
   FPoint2D p_pos2 = Pos2;
   FPoint2D c_pos(0.0f,0.0f);
   float w_sum = 0.0f;
   LocalTrackedObject* lt_object = LTOs.First (   );
   while (lt_object != NULL) {
      lt_object->TrkObject->GTOID = ID;
      float w = lt_object->GetWeight (   );
      c_pos += w * lt_object->GlobalPos;
      w_sum += w;
      lt_object = LTOs.Next (lt_object);
   }
   if (!w_sum) c_pos = PredPos;
   else c_pos /= w_sum;
   Pos  = (1.0f - pu_rate ) * PredPos  + pu_rate  * c_pos;
   Pos2 = (1.0f - pu_rate2) * PredPos2 + pu_rate2 * c_pos;
   FPoint2D v  = Pos - p_pos;
   if (Velocity.X == MC_VLV) Velocity = v;
   else Velocity = (1.0f - vu_rate) * Velocity + vu_rate * v;
   FPoint2D v2 = Pos2 - p_pos2;
   if (Velocity2.X == MC_VLV) Velocity2 = v2;
   else Velocity2 = (1.0f - vu_rate2) * Velocity2 + vu_rate2 * v2;
   float md = Mag (Pos - InitPos);
   if (md > MaxMovingDistance) MaxMovingDistance = md;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: GlobalObjectTracking
//
///////////////////////////////////////////////////////////////////////////////

 GlobalObjectTracking::GlobalObjectTracking (   )
 
{
   MinNumLTOs          = 3;
   ObjectRadius        = 0.7f;
   MDThreshold         = 3.5f * ObjectRadius;
   NRThreshold         = 0.4f;
   MaxRcvDistance      = 10.0f;
   MaxTime_Lost        = 2.0f;
   MinTime_Tracked     = 0.8f;
   MaxTime_Unstable    = 1.0f;
   MaxTime_LTOUnstable = 0.5f;
   _Init (   );
}

 GlobalObjectTracking::~GlobalObjectTracking (   )

{
   Close (   );
}

 void GlobalObjectTracking::_Init (   )

{
   ObjectCount    = 1;
   LocalViewCount = 0;
   FrameCount     = 0;
   FrameRate      = 0.0f;
   FrameDuration  = 0.0f;
}

 void GlobalObjectTracking::AddLocalView (ISize2D& vf_size,Matrix &mat_H,ObjectTracking& obj_tracker)
 
{
   LocalView *l_view = new LocalView;
   l_view->Initialize (vf_size,mat_H,obj_tracker);
   l_view->ID = LocalViewCount++;
   LocalViews.Add (l_view);
}

 void GlobalObjectTracking::Close (   )

{
   LocalViews.Delete     (   );
   TrackedObjects.Delete (   );
   LostObjects.Delete    (   );
   _Init (   );
}

 GlobalTrackedObject* GlobalObjectTracking::FindClosestGTO (LocalView* l_view,TrackedObject* t_object,float& distance)

{
   distance = MC_VLV;
   GlobalTrackedObject* cgto = NULL;
   FPoint2D to_gp = l_view->GetGlobalViewPos (t_object);
   GlobalTrackedObject* gt_object = TrackedObjects.First (   );
   while (gt_object != NULL) {
      LocalTrackedObject* lto = gt_object->FindLTO (l_view);
      if (lto == NULL) {
         float d = Mag(gt_object->PredPos - to_gp);
         if (d < distance) {
            distance = d;
            cgto = gt_object;
         }
      }
      gt_object = TrackedObjects.Next (gt_object);
   }
   return (cgto);
}

 GlobalTrackedObject* GlobalObjectTracking::FindClosestGTO (LocalTrackedObject* lt_object,float& distance)

{
   distance = MC_VLV;
   GlobalTrackedObject* cgto = NULL;
   GlobalTrackedObject* gt_object = TrackedObjects.First (   );
   while (gt_object != NULL) {
      LocalTrackedObject* lto = gt_object->FindLTO (lt_object->View);
      if (lto == NULL) {
         float d = Mag(gt_object->PredPos - lt_object->GlobalPos);
         if (d < distance) {
            distance = d;
            cgto = gt_object;
         }
      }
      gt_object = TrackedObjects.Next (gt_object);
   }
   return (cgto);
}

 GlobalTrackedObject* GlobalObjectTracking::FindClosestLGTO (GlobalTrackedObject* s_gt_object,float& distance)

{
   distance = MC_VLV;
   GlobalTrackedObject* cgto = NULL;
   GlobalTrackedObject* gt_object = LostObjects.First (   );
   while (gt_object != NULL) {
      float d = Mag(s_gt_object->InitPos - gt_object->Pos);
      if (d < distance) {
         distance = d;
         cgto     = gt_object;
      }
      gt_object = LostObjects.Next (gt_object);
   }
   return (cgto);
}

 GlobalTrackedObject* GlobalObjectTracking::FindMatchingLGTO (GlobalTrackedObject* s_gt_object)

{
   float distance;
   GlobalTrackedObject* c_gt_object = FindClosestLGTO (s_gt_object,distance);
   if (c_gt_object != NULL && distance < MaxRcvDistance) return (c_gt_object);
   else return (NULL);
}

 GlobalTrackedObject* GlobalObjectTracking::FindOwningGTO (TrackedObject* t_object)

{
   GlobalTrackedObject* gt_object = TrackedObjects.First (   );
   while (gt_object != NULL) {
      LocalTrackedObject* lt_object = gt_object->FindLTO (t_object);
      if (lt_object != NULL) break;
      gt_object = TrackedObjects.Next (gt_object);
   }
   return (gt_object);
}

 void GlobalObjectTracking::Initialize (float frm_rate)

{
   Close (   );
   FrameRate     = frm_rate;
   FrameDuration = 1.0f / FrameRate;
}

 void GlobalObjectTracking::Perform (   )
 
{
   Flag_Break = FALSE;
   // GTO_STATUS_TO_BE_REMOVED 상태의 GTO들을 제거한다.
   // 남은 GTO들의 Predicted Position을 구한다.
   GlobalTrackedObject* c_gt_object = TrackedObjects.First (   );
   while (c_gt_object != NULL) {
      GlobalTrackedObject* n_gt_object = TrackedObjects.Next (c_gt_object);
      if (c_gt_object->Status & GTO_STATUS_TO_BE_REMOVED) {
         TrackedObjects.Remove (c_gt_object);
         delete c_gt_object;
      }
      else c_gt_object->GetPredictedPos (   );
      c_gt_object = n_gt_object;
   }
   // TO_STATUS_TO_BE_REMOVED 상태의 LTO들을 제거한다.
   // 남은 LTO들의 Global View Position을 업데이트한다.
   GlobalTrackedObject* gt_object = TrackedObjects.First (   );
   while (gt_object != NULL) {
      int n_ltos = gt_object->LTOs.GetNumItems (   );
      LocalTrackedObject* c_lt_object = gt_object->LTOs.First (   );
      while (c_lt_object != NULL) {
         LocalTrackedObject* n_lt_object = gt_object->LTOs.Next (c_lt_object);
         if (c_lt_object->TrkObject->Status & TO_STATUS_TO_BE_REMOVED) {
            gt_object->LTOs.Remove (c_lt_object);
            delete c_lt_object;
         }
         else c_lt_object->UpdateGlobalViewPos (   );
         c_lt_object = n_lt_object;
      }
      gt_object = TrackedObjects.Next (gt_object);
   }
   // 현재 어느 GTO에도 속하지 않은 LTO들에 대해 기존의 GTO들과의 대응 여부를 체크한다.
   // 대응되는 GTO가 있으면, 그 GTO의 LTO 목록에 그 LTO를 추가한다.
   // 대응되는 GTO가 없으면, 기존 GTO에 속한 적이 없는 LTO의 경우 => 신규 GTO를 생성한다.
   //                        기존 GTO에 속한 적이 있는 LTO의 경우 => LTO의 모양이 이상하면 제거한다.
   LocalView* l_view = LocalViews.First (   );
   while (l_view != NULL) {
      TrackedObjectList& to_list = l_view->ObjectTracker->TrackedObjects;
      TrackedObject* t_object = to_list.First (   );
      while (t_object != NULL) {
         if ((t_object->Status & TO_STATUS_VERIFIED) && !(t_object->Status & TO_STATUS_TO_BE_REMOVED)) {
            int flag_inside_ta = TRUE;
            if (TrackingArea.Width) {
               FPoint2D to_gp = l_view->GetGlobalViewPos (t_object);
               if (!TrackingArea.IsPointInsideBox (to_gp)) flag_inside_ta = FALSE;
            }
            if (flag_inside_ta) {
               int flag_matched = FALSE;
               // t_object를 소유한 GTO를 찾는다.
               GlobalTrackedObject* m_gt_object = FindOwningGTO (t_object);
               // t_object를 소유한 GTO가 없는 경우
               if (m_gt_object == NULL) {
                  // t_object와 공간적으로 가장 가까운 GTO를 찾는다.
                  float distance;
                  m_gt_object = FindClosestGTO (l_view,t_object,distance);
                  // 찾은 GTO와의 거리가 MDThreshold보다 작으면 t_object를 GTO의 멤버로 추가한다.
                  if (m_gt_object != NULL && distance < MDThreshold) {
                     LocalTrackedObject* lt_object = new LocalTrackedObject;
                     lt_object->Initialize (l_view,t_object,FrameCount);
                     m_gt_object->LTOs.Add (lt_object);
                     flag_matched = TRUE;
                     #if defined(__DEBUG_GOT)
                     logd ("[New LTO(LV=%d,ID=%d): Belongs to GTO(ID=%d)]\n",
                        lt_object->View->ID,lt_object->TrkObject->ID,m_gt_object->ID);
                     #endif
                     Flag_Break = TRUE;

                  }
               }
               else flag_matched = TRUE;
               // t_object에 매칭이 되는 GTO가 없는 경우
               if (!flag_matched) {
                  // t_object가 지금까지 어떤 GTO에도 속한 적이 없는 경우 완전 신규 객체임을 의미하므로 신규 GTO를 생성한다.
                  if (!t_object->GTOID) {
                     m_gt_object = new GlobalTrackedObject;
                     m_gt_object->Initialize (l_view,t_object,ObjectRadius,FrameCount);
                     TrackedObjects.Add (m_gt_object);
                     #if defined(__DEBUG_GOT)
                     logd ("[New LTO(LV=%d,ID=%d): Belongs to New GTO]\n",l_view->ID,t_object->ID);
                     #endif
                     Flag_Break = TRUE;
                  }
                  else {
                     // t_object의 형태가 이상하면 버린다.
                     float ar = t_object->GetHumanRectAreaRatio (   );
                     if (0.0f < ar && ar <= NRThreshold) t_object->Status |= TO_STATUS_TO_BE_REMOVED;
                  }
               }
            }
         }
         t_object = to_list.Next (t_object);
      }
      l_view = LocalViews.Next (l_view);
   }
   // 2개 이상의 LTO 멤버를 가지고 있는 GTO에 대해, GTO와 LTO 멤버 간의 거리가 MDThreshold 이상인 LTO 멤버들 중,
   // 가장 거리가 큰 LTO를 버릴지 판단한다.
   gt_object = TrackedObjects.First (   );
   while (gt_object != NULL) {
      if (!(gt_object->Status & GTO_STATUS_TO_BE_REMOVED)) {
         int n_ltos = gt_object->LTOs.GetNumItems (   );
         if (n_ltos >= 2) {
            float m_distance = 0.0f;
            LocalTrackedObject* m_lt_object = NULL;
            LocalTrackedObject* lt_object = gt_object->LTOs.First (   );
            while (lt_object != NULL) {
               float distance = gt_object->GetDistance (lt_object);
               if (distance > MDThreshold) {
                  if (distance > m_distance) {
                     m_lt_object = lt_object;
                     m_distance  = distance;
                  }
               }
               else {
                  lt_object->Time_Unstable -= FrameDuration;
                  if (lt_object->Time_Unstable < 0.0f) lt_object->Time_Unstable = 0.0f;
               }
               lt_object = gt_object->LTOs.Next (lt_object);
            }
            if (m_lt_object != NULL) {
               m_lt_object->Time_Unstable += FrameDuration;
               if (m_lt_object->Time_Unstable > MaxTime_LTOUnstable) {
                  m_lt_object->TrkObject->GTOID = 0;
                  gt_object->LTOs.Remove (m_lt_object);
                  #if defined(__DEBUG_GOT)
                  logd ("[LTO(LV=%d,ID=%d) removed from GTO%d, distance = %.2f]\n",
                     m_lt_object->View->ID,m_lt_object->TrkObject->ID,gt_object->ID,m_distance);
                  #endif
                  delete m_lt_object;
                  Flag_Break = TRUE;
               }
            }
         }
      }
      gt_object = TrackedObjects.Next (gt_object);
   }
   // LTO 멤버가 1개 뿐인 GTO들의 병합 여부를 판단한다.
   gt_object = TrackedObjects.First (   );
   while (gt_object != NULL) {
      if (!(gt_object->Status & GTO_STATUS_TO_BE_REMOVED) && (gt_object->Status & GTO_STATUS_VERIFIED)) {
         int flag_unstable = FALSE;
         int n_ltos1 = gt_object->LTOs.GetNumItems (   );
         if (n_ltos1 == 1) {
            LocalTrackedObject* lt_object = gt_object->LTOs.First (   );
            float distance;
            GlobalTrackedObject* m_gt_object = FindClosestGTO (lt_object,distance);
            if (m_gt_object != NULL && distance < MDThreshold) {
               int n_ltos2 = m_gt_object->LTOs.GetNumItems (   );
               if ((n_ltos2 == 1 && m_gt_object->BirthCount < gt_object->BirthCount) || (n_ltos2 > 1)) {
                  if (gt_object->Status & GTO_STATUS_VERIFIED) {
                     flag_unstable = TRUE;
                     gt_object->Time_Unstable += FrameDuration;
                     if (gt_object->Time_Unstable > MaxTime_Unstable) {
                        gt_object->LTOs.Remove (lt_object);
                        gt_object->Status |= GTO_STATUS_TO_BE_REMOVED;
                        m_gt_object->LTOs.Add (lt_object);
                        #if defined(__DEBUG_GOT)
                        logd ("[Moved LTO(LV=%d,ID=%d): GTO%d => GTO%d, distance = %.2f]\n",
                        lt_object->View->ID,lt_object->TrkObject->ID,gt_object->ID,m_gt_object->ID,distance);
                        #endif
                        Flag_Break = TRUE;
                     }
                  }
               }
            }
         }
         if (!flag_unstable) gt_object->Time_Unstable = 0.0f;
      }
      gt_object = TrackedObjects.Next (gt_object);
   }
   // LTO 멤버를 하나도 가지고 있지 않은 GTO가 TrackedArea 내에 있으면 LGTO 목록으로 이동시킨다.
   c_gt_object = TrackedObjects.First (   );
   while (c_gt_object != NULL) {
      GlobalTrackedObject* n_gt_object = TrackedObjects.Next (c_gt_object);
      if (!(c_gt_object->Status & GTO_STATUS_TO_BE_REMOVED)) {
         int flag_lost = FALSE;
         if (!c_gt_object->LTOs.GetNumItems (   )) {
            if (c_gt_object->Status & GTO_STATUS_VERIFIED) {
               if (TrackingArea.Width) {
                  if (TrackingArea.IsPointInsideBox (c_gt_object->PredPos)) {
                     TrackedObjects.Remove (c_gt_object);
                     LostObjects.Add (c_gt_object);
                     c_gt_object->Time_Lost = 0.0f;
                     flag_lost = TRUE;
                  }
               }
            }
            if (!flag_lost) c_gt_object->Status |= GTO_STATUS_TO_BE_REMOVED;
         }
      }
      c_gt_object = n_gt_object;
   }
   // GTO들을 업데이트한다.
   gt_object = TrackedObjects.First (   );
   while (gt_object != NULL) {
      gt_object->Count_Tracked++;
      if (!(gt_object->Status & GTO_STATUS_TO_BE_REMOVED)) {
         int n_ltos = gt_object->LTOs.GetNumItems (   );
         LocalTrackedObject* lt_object = gt_object->LTOs.First (   );
         gt_object->UpdatePos (   );
         gt_object->Time_Tracked += FrameDuration;
         // 특정 GTO가 아직 GTO_STATUS_VERIFIED 상태가 아닌 경우, LGTO 목록에서 그 GTO와 매칭이
         // 될만한 LGTO를 찾아본다. 만약 매칭이 되는 LGTO가 존재하면 그 GTO와 LGTO를 연결시킨다.
         // GTO의 Time_Tracked 값이 MinTime_Tracked보다 커질 때까지 매칭이 되는 LGTO가 없으면
         // 그 GTO를 GTO_STATUS_VERIFIED 상태로 만들고 신규 ID 값을 부여한다.
         if (!(gt_object->Status & GTO_STATUS_VERIFIED)) {
            GlobalTrackedObject* l_gt_object = FindMatchingLGTO (gt_object);
            if (l_gt_object != NULL) {
               LostObjects.Remove (l_gt_object);
               gt_object->ID = l_gt_object->ID;
               delete l_gt_object;
               gt_object->Status |= GTO_STATUS_VERIFIED;
               #if defined(__DEBUG_GOT)
               logd ("[GTO(ID=%d) Recovered]\n",gt_object->ID);
               #endif
               Flag_Break = TRUE;
            }
            if (!(gt_object->Status & GTO_STATUS_VERIFIED) && (gt_object->Time_Tracked > MinTime_Tracked)) {
               if (n_ltos >= MinNumLTOs && gt_object->MaxMovingDistance > ObjectRadius) {
                  gt_object->Status |= GTO_STATUS_VERIFIED;
                  gt_object->ID = ObjectCount++;
                  #if defined(__DEBUG_GOT)
                  logd ("[GTO(ID=%d) Verified]\n",gt_object->ID);
                  #endif
                  Flag_Break = TRUE;
               }
               else gt_object->Status |= GTO_STATUS_TO_BE_REMOVED;
            }
         }
      }
      gt_object = TrackedObjects.Next (gt_object);
   }
   // LGTO들의 Time_Lost 값을 증가시킨다.
   // 특정 LGTO의 Time_Lost 값이 MaxTime_Lost 보다 크면 제거한다.
   c_gt_object = LostObjects.First (   );
   while (c_gt_object != NULL) {
      GlobalTrackedObject* n_gt_object = LostObjects.Next (c_gt_object);
      c_gt_object->Time_Lost += FrameDuration;
      if (c_gt_object->Time_Lost > MaxTime_Lost) {
         LostObjects.Remove (c_gt_object);
         TrackedObjects.Add (c_gt_object);
         c_gt_object->Status |= GTO_STATUS_TO_BE_REMOVED;
      }
      c_gt_object = n_gt_object;
   }
   FrameCount++;
}

}

#if !defined(__VACL_CORNER_H)
#define __VACL_CORNER_H

#include "vacl_define.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

int DetectCorners_Tomasi (GImage &s_image,int win_size,int e_thrsld,float c_thrsld,int nms_win_size,GImage &d_image);

}

#endif

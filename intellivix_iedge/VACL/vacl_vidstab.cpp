#include "vacl_vidstab.h"
#include "vacl_warping.h"

#if defined(__DEBUG_VST)
#include "Win32CL/Win32CL.h"
#endif

#if defined(__DEBUG_VST)
extern int _DVX_VST,_DVY_VST;
extern CImageView* _DebugView_VST;
#endif

#if defined(__LIB_OPENCV)

 namespace VACL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Class: VideoStabilization
//
///////////////////////////////////////////////////////////////////////////////

 VideoStabilization::VideoStabilization (   )

{
   MaxSrcImgHeight = 320;
   _Init (   );
}

 VideoStabilization::~VideoStabilization (   )

{
   Close (   );
}

 void VideoStabilization::_Init (   )

{
   FrameCount = 0;
   SubsmpSize = 1;
   VidFrmSize.Clear (   );
   SrcImgSize.Clear (   );
}

 void VideoStabilization::ClearFrameBuffer_YUY2 (byte *si_buffer)
 
{
   int i;
   
   ushort *s_buffer = (ushort*)si_buffer;
   int s_buf_size = VidFrmSize.Width * VidFrmSize.Height;
   for (i = 0; i < s_buf_size; i++) s_buffer[i] = 0x8000;
}

 void VideoStabilization::Close (   )

{
   CurImage.Delete (   );
   TmpImage.Delete (   );
   PhaseCorrelator.Close (   );
   _Init (   );
}

 void VideoStabilization::GetSubsampledGrayImage_YUY2 (byte *si_buffer,FArray2D &d_array)
 
{
   int x1,y1,x2,y2;
   
   int dx1 = SubsmpSize * 2;
   int dy1 = SubsmpSize;
   int ex1 = VidFrmSize.Width * 2;
   int ey1 = VidFrmSize.Height;
   int nsb = ex1 * SubsmpSize;
   for (y1 = y2 = 0; y1 < ey1; y1 += dy1,y2++) {
      float *l_d_array = d_array[y2];
      for (x1 = x2 = 0; x1 < ex1; x1 += dx1,x2++) l_d_array[x2] = (float)si_buffer[x1];
      si_buffer += nsb;
   }
}

 void VideoStabilization::Initialize (ISize2D& frm_size)

{
   int n;

   Close (   );
   VidFrmSize = SrcImgSize = frm_size;
   for (n = 0;   ; n++) {
      if (SrcImgSize.Height > MaxSrcImgHeight) {
         SrcImgSize.Width  /= 2;
         SrcImgSize.Height /= 2;
      }
      else break;
   }
   SubsmpSize = 1 << n;
   CurImage.Create (SrcImgSize.Width,SrcImgSize.Height);
   TmpImage.Create (SrcImgSize.Width,SrcImgSize.Height);
   PhaseCorrelator.Initialize (SrcImgSize);
}

 void VideoStabilization::Reset (   )

{
   FrameCount = 0;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: VideoStabilization_MovingCam
//
///////////////////////////////////////////////////////////////////////////////

 VideoStabilization_MovingCam::VideoStabilization_MovingCam (   )
 
{
   MSFKRadius   = 15;
   MaxShiftSize = 0.15f;
   _Init (   );
}

 VideoStabilization_MovingCam::~VideoStabilization_MovingCam (   )

{
   Close (   );
}

 void VideoStabilization_MovingCam::_Init (   )
 
{
   FPQIndex = 0;
}

 void VideoStabilization_MovingCam::Close (   )
 
{
   VideoStabilization::Close (   );
   MSFKernel.Delete   (   );
   PrvImage.Delete    (   );
   FrmPosQueue.Delete (   );
   FrameQueue.Delete  (   );
   _Init (   );
}

 void VideoStabilization_MovingCam::CreateMSFKernel (   )
 
{
   int i;
   
   MSFKernel.Create (2 * MSFKRadius + 1);
   Matrix vec_c(MSFKernel.Length);
   savgol (vec_c,MSFKernel.Length,MSFKRadius,MSFKRadius,0,2);
   MSFKernel[MSFKRadius] = (float)vec_c(0);
   for (i = 0; i <  MSFKRadius; i++) MSFKernel[i] = (float)vec_c(MSFKRadius - i);
   for (i = 1; i <= MSFKRadius; i++) MSFKernel[MSFKRadius + i] = (float)vec_c(MSFKernel.Length - i);
}

 FPoint2D VideoStabilization_MovingCam::GetFilteredFrmPos (   )
 
{
   int i;
   
   FPoint2D ffp(0.0f,0.0f);
   for (i = 0; i < MSFKernel.Length; i++)
      ffp += MSFKernel[i] * FrmPosQueue[i];
   return (ffp);
}

 void VideoStabilization_MovingCam::Initialize (ISize2D& frm_size,int msfk_radius)
 
{
   VideoStabilization::Initialize (frm_size);
   MSFKRadius = msfk_radius;
   CreateMSFKernel    (   );
   FrmPosQueue.Create (MSFKernel.Length);
   PrvImage.Create    (SrcImgSize.Width,SrcImgSize.Height);
}

 int VideoStabilization_MovingCam::Perform_YUY2 (byte* si_buffer,byte* di_buffer)
 
{
   int i;
   FPoint2D dp;

   if (!VidFrmSize.Width || !VidFrmSize.Height || !SrcImgSize.Width || !SrcImgSize.Height) return (1);
   float mss = VidFrmSize.Height * MaxShiftSize;
   Matrix mat_H = IM(3);
   if (FrameCount) {
      GetSubsampledGrayImage_YUY2 (si_buffer,CurImage);
      // CurImage를 직접 PhaseCorrelator.Perform( ) 함수에 입력하면 CurImage의 내용이 바뀌는 문제가 발생한다.
      // CurImage의 내용이 바뀌는 것을 방지하기 위해 TmpImage에 복사한 후 TmpImage를 입력한다.
      TmpImage = CurImage; 
      FPoint2D shift;
      PhaseCorrelator.Perform (PrvImage,TmpImage,shift);
      dp.X = -(shift.X * SubsmpSize);
      dp.Y = -(shift.Y * SubsmpSize);
      if (fabs (dp.X) > mss) dp.X = dp.X / fabs (dp.X) * mss;
      if (fabs (dp.Y) > mss) dp.Y = dp.Y / fabs (dp.Y) * mss;
      if (FrameCount >= FrmPosQueue.Length) {
         for (i = 1; i < FrmPosQueue.Length; i++)
            FrmPosQueue[i - 1] = FrmPosQueue[i];
      }
      FrmPosQueue[FPQIndex] = FrmPosQueue[FPQIndex - 1] + dp;
      PrvImage = CurImage;
   }
   else {
      GetSubsampledGrayImage_YUY2 (si_buffer,PrvImage);
      FrmPosQueue[FPQIndex].Clear (   );
   }
   if (FrameCount < FrmPosQueue.Length - 1) FPQIndex++;
   if (FrameCount >= MSFKRadius) {
      Frame* frame = new Frame;
      frame->Buffer.Create (VidFrmSize.Width * VidFrmSize.Height * 2);
      memcpy (frame->Buffer,si_buffer,frame->Buffer.Length);
      FrameQueue.Add (frame);
   }
   if (FrameCount >= FrmPosQueue.Length - 1) {
      FPoint2D ffp = GetFilteredFrmPos (   );
      dp = FrmPosQueue[MSFKRadius] - ffp;
      Frame* frame = FrameQueue.Retrieve (   );
      Shift_YUY2 (frame->Buffer,VidFrmSize.Width,VidFrmSize.Height,dp,di_buffer);
      delete frame;
   }
   else ClearFrameBuffer_YUY2 (di_buffer);
   FrameCount++;
   return (DONE);
}

 void VideoStabilization_MovingCam::Reset (   )
 
{
   VideoStabilization::Reset (   );
   FPQIndex = 0;
   FrameQueue.Delete (   );
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: VideoStabilization_StaticCam
//
///////////////////////////////////////////////////////////////////////////////

 VideoStabilization_StaticCam::VideoStabilization_StaticCam (   )

{
   BkgUpdateRate   = 0.025f;
   BkgUpdatePeriod = 1.0f;
}

 VideoStabilization_StaticCam::~VideoStabilization_StaticCam (   )

{
   Close (   );
}

 void VideoStabilization_StaticCam::Close (   )

{
   VideoStabilization::Close (   );
   BkgImage.Delete (   );
}

 void VideoStabilization_StaticCam::Initialize (ISize2D& frm_size,float frm_rate)

{
   VideoStabilization::Initialize (frm_size);
   FrameRate = frm_rate;
   BkgImage.Create (SrcImgSize.Width,SrcImgSize.Height);
}

 int VideoStabilization_StaticCam::Perform_YUY2 (byte* si_buffer,byte* di_buffer)

{
   #if defined(__DEBUG_VST)
   _DVX_VST = _DVY_VST = 0;
   _DebugView_VST->Clear (   );
   #endif
   if (!VidFrmSize.Width || !VidFrmSize.Height || !SrcImgSize.Width || !SrcImgSize.Height) return (1);
   if (FrameCount) {
      GetSubsampledGrayImage_YUY2 (si_buffer,CurImage);
      // BkgImage를 직접 PhaseCorrelator.Perform( ) 함수에 입력하면 BkgImage의 내용이 바뀌는 문제가 발생한다.
      // BkgImage의 내용이 바뀌는 것을 방지하기 위해 TmpImage에 복사한 후 TmpImage를 입력한다.
      TmpImage = BkgImage;
      FPoint2D shift;
      PhaseCorrelator.Perform (TmpImage,CurImage,shift);
      shift.X = -(shift.X * SubsmpSize);
      shift.Y = -(shift.Y * SubsmpSize);
      Shift_YUY2 (si_buffer,VidFrmSize.Width,VidFrmSize.Height,shift,di_buffer);
      int bup = (int)(BkgUpdatePeriod * FrameRate);
      if (FrameCount <= (int)FrameRate || !(FrameCount % bup)) UpdateBkgImage_YUY2 (di_buffer);
   }
   else {
      GetSubsampledGrayImage_YUY2 (si_buffer,BkgImage);
      memcpy (di_buffer,si_buffer,VidFrmSize.Width * VidFrmSize.Height * 2);
   }
   #if defined(__DEBUG_VST)
   GImage bg_image(BkgImage.Width,BkgImage.Height);
   Convert (BkgImage,bg_image);
   _DebugView_VST->DrawImage (bg_image,_DVX_VST,_DVY_VST);
   _DebugView_VST->Invalidate (   );
   #endif
   FrameCount++;
   return (DONE);
}

 void VideoStabilization_StaticCam::UpdateBkgImage_YUY2 (byte* si_buffer)

{
   int x,y;
   
   float a = 1.0f - BkgUpdateRate;
   FArray2D s_image(SrcImgSize.Width,SrcImgSize.Height);
   GetSubsampledGrayImage_YUY2 (si_buffer,s_image);
   for (y = 0; y < BkgImage.Height; y++) {
      float* l_BkgImage = BkgImage[y];
      float* l_s_image  = s_image[y];
      for (x = 0; x < BkgImage.Width; x++) {
         l_BkgImage[x] = a * l_BkgImage[x] + BkgUpdateRate * l_s_image[x];
      }
   }
}

}

#endif

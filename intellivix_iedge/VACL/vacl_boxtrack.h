#if !defined(__VACL_BOXTRACK_H)
#define __VACL_BOXTRACK_H

#include "vacl_define.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjBndBox
//
///////////////////////////////////////////////////////////////////////////////

#define OBJ_TYPE_UNKNOWN      0x0000000000000000
#define OBJ_TYPE_HUMAN        0x0000000000000001
#define OBJ_TYPE_VEHICLE      0x0000000000000002
#define OBJ_TYPE_ANIMAL       0x0000000000000004
#define OBJ_TYPE_HEAD         0x0000000000000008
#define OBJ_TYPE_FACE         0x0000000000000010
#define OBJ_TYPE_WHEELCHAIR   0x0000000000000020
#define OBJ_TYPE_STICK        0x0000000000000040

 class ObjBndBox : public IBox2D

{
   public:
      int     ClassIdx;
      int     MatTrkBoxIdx;
      int64   ObjType;
      int64   Properties;
      float   Score;
      StringA ClassName;

   public:
      IBox2D SubRgn_Head;
      IBox2D SubRgn_Face;
      IBox2D SubRgn_Stick;
   
   public:
      ObjBndBox (   );
};

typedef Array1D<ObjBndBox> OBBArray1D;

///////////////////////////////////////////////////////////////////////////////
//
// Class: TrackedBox
//
///////////////////////////////////////////////////////////////////////////////

#define TBX_STATUS_CREATED                0x00000001
#define TBX_STATUS_FACE_INITIALIZED       0x00000002
#define TBX_STATUS_HEAD_INITIALIZED       0x00000004
#define TBX_STATUS_OUT_OF_TRACKING_AREA   0x00000008
#define TBX_STATUS_TO_BE_REMOVED          0x00000010
#define TBX_STATUS_UNDETECTED             0x00000020
#define TBX_STATUS_VALIDATED              0x00000040
#define TBX_STATUS_VERIFIED               0x00000080
#define TBX_STATUS_VERIFIED_START         0x00000100

 class TrackedBox : public IBox2D

{
   public:
      int   ID;
      int   FaceID;
      int   Status;
      int   ClassIdx;
      int   MatDetBoxIdx;
      int64 ObjType;

   public:
      int   Count_Tracked;
      float MovingDistance;
      float MaxMovingDistance;
      float Time_Stick;
      float Time_Tracked;
      float Time_Undetected;
   
   public:
      IBox2D   InitRegion;
      IBox2D   PredRegion;
      FPoint2D InitPos;
      FPoint2D CenterPos;
      FPoint2D Velocity;

   public:
      IBox2D Head;
      IBox2D Face;

   public:
      TrackedBox* Prev;
      TrackedBox* Next;

   public:
      TrackedBox (   );
      virtual ~TrackedBox (   ) {   };

   public:
      void Initialize (ObjBndBox& s_obb);
      void Predict    (   );
      void Update     (ObjBndBox& s_obb,float bu_rate = 1.0f,float vu_rate = 1.0f);
};

typedef Array1D<TrackedBox*>   TBXArray1D;
typedef LinkedList<TrackedBox> TrackedBoxList;

///////////////////////////////////////////////////////////////////////////////
//
// Class: BoxTracking
//
///////////////////////////////////////////////////////////////////////////////
 
#define BXT_OPTION_DONT_CARE_OBJECT_CLASS   0x00000001

 class BoxTracking

{
   protected:
      int   Flag_Enabled;
      int   BoxCount;
      int   FrameCount;

   // Input (Set directly)
   public:
      int Options;

   // Input (Set directly)
   public:
      float BMThreshold;        // Box Matching Threshold
      float RegionUpdateRate;   //
      float VelocityUpdateRate; //

   // Input (Set directly)
   public:
      int   MinCount_Tracked;
      float MinMovingDistance;
      float MaxTime_Undetected;

   // Input (Set indirectly)
   public:
      float   FrameRate;
      float   FrameDuration;
      ISize2D SrcImgSize;

   // Output
   public:
      TrackedBoxList TrackedBoxes;
   
   public:
      BoxTracking (   );
      virtual ~BoxTracking (   );

   protected:
      void  _Init                     (   );
      int   CheckBoxInTrackingArea    (TrackedBox* t_box);
      void  CleanBoxList              (   );
      void  GetBoxArray               (TBXArray1D& d_array);
      void  GetBoxMatchingScoreMatrix (TBXArray1D& tb_array,OBBArray1D& db_array,FArray2D& d_array);
      int   GetNewBoxID               (   );
      void  GetPredictedBoxRegions    (   );
      void  PerformBoxMatching        (TBXArray1D& tb_array,OBBArray1D& db_array,FArray2D& bor_matrix);
      void  UpdateBoxList             (OBBArray1D& db_array);

   public:
      virtual void Close (   );

   public:
      void Enable     (int flag_enable);
      int  Initialize (ISize2D& si_size,float frm_rate);
      int  IsEnabled  (   );
      int  Perform    (OBBArray1D& db_array,BGRImage* s_cimage = NULL);
      void Reset      (   );
};

 inline void BoxTracking::Enable (int flag_enable)

{
   Flag_Enabled = flag_enable;
}

 inline int BoxTracking::IsEnabled (   )

{
   return (Flag_Enabled);
}

}

#endif

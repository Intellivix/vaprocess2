﻿#include "vacl_objclass.h"
#include "vacl_event.h"
#include "vacl_filter.h"
#include "vacl_ipp.h"
#include "vacl_opencv.h"
#include "vacl_profile.h"

// #define __DEBUG_OBC

#if defined(__DEBUG_OBC)
#include "Win32CL/Win32CL.h"
extern int _DVX_OBC,_DVY_OBC;
extern CImageView* _DebugView_OBC;
#endif

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Definitions
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__KISA_CERT)
#define OC_PARAM_NUM_TRIES     1000
#else
#define OC_PARAM_NUM_TRIES     1000
#endif
#define OC_PARAM_INFLATION_X   1.4f
#define OC_PARAM_INFLATION_Y   1.3f

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectClassification_Common
//
///////////////////////////////////////////////////////////////////////////////

 ObjectClassification_Common::ObjectClassification_Common (   )

{
   EnfObjType        = 0;
   MaxTime_Unchanged = 4.0f;
}

 int ObjectClassification_Common::GetHumanHeadPositions (TrackedObject* t_object)

{
   int i,x,y;
   
   Array1D<TrackedObject::PersonInfo> &hp_array = t_object->Persons;
   hp_array.Delete (   );
   int width  = t_object->MaskImage.Width;
   int height = t_object->MaskImage.Height;
   byte **s_image = t_object->MaskImage;
   // Vertical Projection을 수행하여 Projection Histogram을 얻는다.
   FArray1D vp_array(width);
   for (x = 0; x < width; x++) {
      for (y = 0; y < height; y++) {
         if (s_image[y][x]) break;
      }
      vp_array[x] = (float)(height - y);
   }
   // 얻은 Projection Histogram에 대해 Gaussian Smoothing을 수행한다.
   // Gaussian Kernel의 크기는 객체의 높이에 비례한다.
   FArray1D fp_array(vp_array.Length);
   float sigma = 0.05f * height; // PARAMETERS
   if (sigma < 1.5f) sigma = 1.5f;
   GaussianFiltering (vp_array,sigma,fp_array);
   // Smoothed Projection Histogram에서 Local Minima/Maxima를 찾는다.
   IArray1D min_array,max_array;
   FindLocalMinMax (fp_array,min_array,max_array);
   if (!max_array) return (0);
   hp_array.Create (max_array.Length);
   for (i = 0; i < max_array.Length; i++) {
      x = max_array[i];
      hp_array[i].X = x;
      int h = 0;
      for (y = 0; y < height; y++)
         if (s_image[y][x]) break;
      if (y < height) {
         int sy = y;
         hp_array[i].Y = sy;
         for (y = height - 1; y >= 0; y--)
            if (s_image[y][x]) break;
         h = y - sy + 1;
      }
      else hp_array[i].Y = 0;
      hp_array[i].Height = h;
   }
   #if defined(__DEBUG_OBC)
   _DebugView_OBC->DrawImage (t_object->TemplateImage,_DVX_OBC,_DVY_OBC);
   _DVX_OBC += t_object->Width  + 10;
   _DebugView_OBC->DrawImage (t_object->MaskImage,_DVX_OBC,_DVY_OBC);
   for (i = 0; i < hp_array.Length; i++) {
      int sx = _DVX_OBC + hp_array[i].X;
      int sy = _DVY_OBC + hp_array[i].Y;
      int ex = sx;
      int ey = sy + hp_array[i].Height;
      _DebugView_OBC->DrawLine (sx,sy,ex,ey,PC_RED,2);
   }
   _DVY_OBC += t_object->Height + 10;
   DrawGraph (_DebugView_OBC,fp_array,_DVX_OBC,_DVY_OBC,t_object->Height,PC_GREEN,TRUE,FALSE,0);
   _DVX_OBC = 0;
   _DVY_OBC += t_object->Height + 10;
   #endif
   return (hp_array.Length);
}

 int ObjectClassification_Common::Preprocess (ObjectTracking& obj_tracker)

{
   ForegroundDetection* fgd = obj_tracker.ForegroundDetector;
   if (fgd == NULL) return (1);
   int flag_hhp = TRUE;
   int r_code   = 0;
   switch (fgd->GetClassID()) {
      case CLSID_ForegroundDetection_HMD_OHV_MBS:
      case CLSID_ForegroundDetection_HMD_OHV_3DM:
         SetEnforcedObjectType (TO_TYPE_HUMAN);
         flag_hhp = FALSE;
         r_code   = -1;
         break;
      case CLSID_ForegroundDetection_FCD_Seeta:
      case CLSID_ForegroundDetection_FCD_OpenVINO:
         SetEnforcedObjectType (TO_TYPE_FACE);
         flag_hhp = FALSE;
         r_code   = -1;
         break;
      case CLSID_ForegroundDetection_OBD_YOLO:
      case CLSID_ForegroundDetectionClient_OBD_YOLO:
         flag_hhp = FALSE;
         r_code   = -1;
         break;
      default:
         break;
   }
   TrackedObject* t_object = obj_tracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED)) {
         if (EnfObjType > 0) t_object->Type = EnfObjType;
         if (flag_hhp) GetHumanHeadPositions (t_object);
      }
      t_object = obj_tracker.TrackedObjects.Next (t_object);
   }
   return (r_code);
}

 int ObjectClassification_Common::ReadFile (FileIO& file)

{
   if (file.Read ((byte*)&EnfObjType,1,sizeof(int))) return (1);
   return (DONE);
}

 int ObjectClassification_Common::ReadFile (CXMLIO* pIO)

{
   static ObjectClassification_Common dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("ObjectClassification"))
   {
      xmlNode.Attribute (TYPE_ID(int),"EnfObjType",&EnfObjType,&dv.EnfObjType);
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}

 int ObjectClassification_Common::WriteFile (FileIO& file)

{
   if (file.Write ((byte*)&EnfObjType,1,sizeof(int))) return (1);
   return (DONE);
}

 int ObjectClassification_Common::WriteFile (CXMLIO* pIO)

{
   static ObjectClassification_Common dv;

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("ObjectClassification"))
   {
      xmlNode.Attribute (TYPE_ID(int),"EnfObjType",&EnfObjType,&dv.EnfObjType);
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}
 
 int ObjectClassification_Common::CheckSetup(ObjectClassification_Common* fd)

{
   int ret_flag = 0;

   FileIO buffThis, buffFrom;
   CXMLIO xmlIOThis, xmlIOFrom;
   // 변경된 아이템이 있는지 체크.
   xmlIOThis.SetFileIO(&buffThis, XMLIO_Write);
   xmlIOFrom.SetFileIO(&buffFrom, XMLIO_Write);
   WriteFile(&xmlIOThis);
   fd->WriteFile(&xmlIOFrom);
   if (buffThis.IsDiff(buffFrom)) ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength(0);
   buffFrom.SetLength(0);
   if (ret_flag & CHECK_SETUP_RESULT_CHANGED) {
   }
   return (ret_flag);
}


///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectClassification_Basic
//
///////////////////////////////////////////////////////////////////////////////

 ObjectClassification_Basic::ObjectClassification_Basic (   )
 
{
   Flag_CheckHumanGroup = TRUE;
}

int ObjectClassification_Basic::CheckHumanGroup (TrackedObject* t_object)
 
{
   int i,n;

   if (!Flag_CheckHumanGroup) return (-1);
   Array1D<TrackedObject::PersonInfo> &hp_array = t_object->Persons;
   if (hp_array.Length < 2) return (1); // 그룹 내의 사람 수가 2보다 작으면 리턴한다.
   IArray1D w_array(hp_array.Length + 1);
   // 그룹 내 사람의 평균 너비를 구한다.
   for (i = 1,n = 0; i < hp_array.Length; i++)
      w_array[n++]= hp_array[i].X - hp_array[i - 1].X + 1;
   w_array[n++] = 2 * hp_array[0].X;
   w_array[n++] = 2 * (t_object->Width - hp_array[hp_array.Length - 1].X);
   float mw = GetMean (w_array);
   // 그룹 내 사람의 평균 높이를 구한다.
   float mh = 0.0f;
   for (i = 0; i < hp_array.Length; i++) mh += hp_array[i].Height;
   mh /= hp_array.Length;
   // 그룹 내 사람의 평균 AspectRatio를 구하여 적정 범위 내에 들어오는지 체크한다.
   float ar = mh / mw;
   t_object->AspectRatio2 = ar;
   if (1.4f <= ar && ar <= 6.0f) return (DONE); // PARAMETERS
   return (2);
}
 
 int ObjectClassification_Basic::DoPreprocessing (ObjectTracking& obj_tracker)

{
   return (ObjectClassification_Common::Preprocess (obj_tracker));
}

 int ObjectClassification_Basic::Initialize (   )

{
   if (ObjectRecognition::Initialize (   )) return (1);
   Flag_Initialized = TRUE;
   return (DONE);
}

 void ObjectClassification_Basic::InitParams (EventDetection& evt_detector)

{
   if (evt_detector.IsEventZoneAvailable (ER_EVENT_CAR_STOPPING)) Flag_CheckHumanGroup = FALSE;
   else Flag_CheckHumanGroup = TRUE;
}

 int ObjectClassification_Basic::Perform (ObjectTracking& obj_tracker)

{
   #if defined(__DEBUG_OBC)
   _DVX_OBC = _DVY_OBC = 0;
   _DebugView_OBC->Clear (   );
   #endif
   int r_code = ObjectRecognition::Perform (obj_tracker);
   #if defined(__DEBUG_OBC)
   if (_DebugView_OBC) _DebugView_OBC->Invalidate (   );
   #endif
   return (r_code);
}

// #define __DEBUG_OBC2

 int ObjectClassification_Basic::UpdateLikelihood (TrackedObject* t_object)
 
{
   int i;
   #if defined(__DEBUG_OBC) || defined(__DEBUG_OBC2)
   int tgt_id = 1;
   #endif
   GetHumanHeadPositions (t_object);
   if (t_object->IsTypeFixed (   )) return (1);
   float weight = 1.0f;
   if (t_object->Status & TO_STATUS_STATIC2) {
      // 충분한 시간 동안 추적을 한 객체에 대해서는 "정적" 상태일 때 객체의 타입을 업데이트하지 않는다.
      if (t_object->Time_Tracked > MaxTime_Unchanged) return (2);
      // 그렇지 않으면 객체의 타입을 서서히 업데이트한다.
      else weight = 0.05f;
   }
   float likelihood[3];
   memset (likelihood,0,sizeof(likelihood));
   float ma_angle   = fabs (t_object->MajorAxisAngle1);
   float mv_dist1   = t_object->GetMovingDistance1 (   );
   float mv_dist2   = t_object->GetMovingDistance2 (   );
   float md_thrsld1 = 0.5f * GetMinimum (t_object->Width,t_object->Height); // PARAMETERS
   float md_thrsld2 = 0.1f * SrcImgSize.Height; // PARAMETERS
   float md_thrsld  = GetMinimum (md_thrsld1,md_thrsld2);
   #if defined(__DEBUG_OBC) || defined(__DEBUG_OBC2)
   if (t_object->ID == tgt_id) {
      logd ("[Tracked Object #%04d]\n",t_object->ID);
      logd ("- ma_angle = %.2f, mv_dist1 = %.2f, mv_dist2 = %.2f\n",ma_angle,mv_dist1,mv_dist2);
      logd ("- md_thrsld1 = %.2f, md_thrsld2 = %.2f, md_thrsld = %.2f\n",md_thrsld1,md_thrsld2,md_thrsld);
      logd ("- AspectRatio = %.2f, NorSpeed = %.2f, AxisLengthRatio = %.2f. MajorAxisAngle2 = %.2f\n",t_object->AspectRatio,t_object->NorSpeed,t_object->AxisLengthRatio,t_object->MajorAxisAngle2);
   }
   #endif
   if (mv_dist1 <= md_thrsld) {
      #if defined(__DEBUG_OBC) || defined(__DEBUG_OBC2)
      if (t_object->ID == tgt_id) logd ("- Object Classification Condition 01: Unknown\n");
      #endif
      likelihood[OC_TYPE_UNKNOWN] += 0.1f;
   }
   else {
      if (ma_angle <= 35.0f && 1.4f <= t_object->AspectRatio && t_object->AspectRatio <= 6.0f && t_object->NorSpeed <= 175.0f) {
         #if defined(__DEBUG_OBC) || defined(__DEBUG_OBC2)
         if (t_object->ID == tgt_id) logd ("- Object Classification Condition 02: Human\n");
         #endif
         likelihood[OC_TYPE_HUMAN] += 2.0f;
      }
      else {
         if (t_object->NorSpeed > 60.0f) {
            #if defined(__DEBUG_OBC) || defined(__DEBUG_OBC2)
            if (t_object->ID == tgt_id) logd ("- Object Classification Condition 03: Vehicle\n");
            #endif
            likelihood[OC_TYPE_VEHICLE] += 1.0f;
         }
         else {
            if (CheckHumanGroup (t_object) == DONE) {
               #if defined(__DEBUG_OBC) || defined(__DEBUG_OBC2)
               if (t_object->ID == tgt_id) logd ("- Object Classification Condition 04: Human\n");
               #endif
               likelihood[OC_TYPE_HUMAN] += 0.5f;
            }
            else {
               if (40.0f <= ma_angle && ma_angle <= 75.0f && t_object->MajorAxisAngle2 < 25.0f){
                  #if defined(__DEBUG_OBC) || defined(__DEBUG_OBC2)
                  if (t_object->ID == tgt_id) logd ("- Object Classification Condition 05: Vehicle\n");
                  #endif
                  likelihood[OC_TYPE_VEHICLE] += 0.5f;
               }
               else {
                  if (mv_dist2 > 0.05f * SrcImgSize.Height) {
                     #if defined(__DEBUG_OBC) || defined(__DEBUG_OBC2)
                     if (t_object->ID == tgt_id) logd ("- Object Classification Condition 06: Vehicle\n");
                     #endif
                     likelihood[OC_TYPE_VEHICLE] += 0.5f;
                  }
                  else {
                     #if defined(__DEBUG_OBC) || defined(__DEBUG_OBC2)
                     if (t_object->ID == tgt_id) logd ("- Object Classification Condition 07: ?\n");
                     #endif
                  }
               }
            }  
         }
      }
   }
   if (t_object->RealHeight) {
      if (t_object->RealSpeed > 15.0f) {
         if (t_object->RealWidth >= 1.4f && t_object->RealHeight >= 1.4f) likelihood[OC_TYPE_VEHICLE] += 2.0f;
         else likelihood[OC_TYPE_UNKNOWN] += 1.0f;
      }
      else {
         if (t_object->RealWidth >= 1.4f && t_object->RealHeight >= 1.4f) {
            if      (t_object->Persons.Length == 1) likelihood[OC_TYPE_VEHICLE] += 2.0f;
            else if ((t_object->Status & TO_STATUS_STATIC2) && (t_object->EventStatus & TO_EVENT_CAR_PARKING_START)) likelihood[OC_TYPE_VEHICLE] += 1.0f;
         }
         else if (t_object->RealWidth >= 1.4f && t_object->RealHeight >= 2.3f) likelihood[OC_TYPE_VEHICLE] += 2.0f;
         else if (t_object->RealHeight < 1.0f) likelihood[OC_TYPE_UNKNOWN] += 1.5f;
      }
   }
   for (i = 0; i < 3; i++) t_object->OTLikelihood[i] += weight * likelihood[i];
   #if defined(__DEBUG_OBC) || defined(__DEBUG_OBC2)
   if (t_object->ID == tgt_id) logd ("- Type Likelihood: Unknown = %.2f, Human = %.2f, Vehicle = %.2f\n",
      t_object->OTLikelihood[OC_TYPE_UNKNOWN],t_object->OTLikelihood[OC_TYPE_HUMAN],t_object->OTLikelihood[OC_TYPE_VEHICLE]);
   #endif
   return (DONE);
}

 void ObjectClassification_Basic::UpdateRecognitionResult (TrackedObject* t_object)

{
   t_object->UpdateObjectType (FrameDuration);
   if (t_object->Time_TypeUnchanged > MaxTime_Unchanged) t_object->TypeHistory |= t_object->Type;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectClassification_DNN_Common
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_CAFFE)

 void ObjectClassification_DNN_Common::_Close (   )

{
   DNN.Close (   );
}

 int ObjectClassification_DNN_Common::_Initialize (void* dll_handle,const char* dir_name,int flag_use_gpu,int gpu_idx)

{
   const char cfg_file_name[] = "IVXOCCCFN2.dat"; // deploy.prototxt
   const char wgt_file_name[] = "IVXOCWCFN2.dat"; // deploy.caffemodel
   const char cls_file_name[] = "IVXOCNCFN2.dat"; // labels.txt
   const char mns_file_name[] = "IVXOCMCFN2.dat"; // mean.binaryproto
   
   StringA cfg_path_name,wgt_path_name,cls_path_name,mns_path_name;
   if (dir_name == NULL) {
      cfg_path_name = cfg_file_name;
      wgt_path_name = wgt_file_name;
      cls_path_name = cls_file_name;
      mns_path_name = mns_file_name;
   }
   else {
      #if defined(__OS_WINDOWS)
      cfg_path_name.Format ("%s\\%s",dir_name,cfg_file_name);
      wgt_path_name.Format ("%s\\%s",dir_name,wgt_file_name);
      cls_path_name.Format ("%s\\%s",dir_name,cls_file_name);
      mns_path_name.Format ("%s\\%s",dir_name,mns_file_name);
      #else
      cfg_path_name.Format ("%s/%s",dir_name,cfg_file_name);
      wgt_path_name.Format ("%s/%s",dir_name,wgt_file_name);
      cls_path_name.Format ("%s/%s",dir_name,cls_file_name);
      mns_path_name.Format ("%s/%s",dir_name,mns_file_name);
      #endif
   }
   
   int nerror = DNN.Initialize (dll_handle,cfg_path_name,wgt_path_name,cls_path_name,mns_path_name,flag_use_gpu,gpu_idx);
   if (nerror != DONE)   {
      LOGE << "dnn common_init error : " <<nerror;
      return (1);
   }
   if (!DNN.ClassNames) return (2);
   
   
   LOGI << "ObjectClassification DNN Initialize Done !!! ";
   return (DONE);
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectClassification_DNN
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_CAFFE)

 ObjectClassification_DNN::ObjectClassification_DNN (   ) 

{
   NumTries = OC_PARAM_NUM_TRIES;
   Inflation(OC_PARAM_INFLATION_X,OC_PARAM_INFLATION_Y);
}

 int ObjectClassification_DNN::CheckObject (TrackedObject* t_object)

{
   if (!(t_object->Status & TO_STATUS_VERIFIED)) return (1);
   return (DONE);
}

 void ObjectClassification_DNN::Close (   )

{
   ObjectRecognition::Close (   );
   ObjectClassification_DNN_Common::_Close (   );
}

 int ObjectClassification_DNN::DoPreprocessing (ObjectTracking& obj_tracker)

{
   return (ObjectClassification_Common::Preprocess (obj_tracker));
}

 int ObjectClassification_DNN::Initialize (void* dll_handle,const char* dir_name,int flag_use_gpu,int gpu_idx)

{
   if (FALSE == Flag_Initialized) {
      if (ObjectRecognition::Initialize (   )) return (1);
      if (ObjectClassification_DNN_Common::_Initialize (dll_handle,dir_name,flag_use_gpu,gpu_idx)) return (2);
      Flag_Initialized = TRUE;
   }

   return (DONE);
}

 int ObjectClassification_DNN::Perform (ObjectTracking& obj_tracker)

{
   #if defined(__DEBUG_OBC)
   _DVX_OBC = _DVY_OBC = 0;
   _DebugView_OBC->Clear (   );
   #endif
   int r_code = ObjectRecognition::Perform (obj_tracker);
   #if defined(__DEBUG_OBC)
   if (_DebugView_OBC) _DebugView_OBC->Invalidate (   );
   #endif
   return (r_code);
}

 void ObjectClassification_DNN::RecognizeObject (TrackedObject* t_object)

{
   if (NULL == t_object)   return;

   t_object->DNNInputImage.Delete (   );
   t_object->DNNInputImage.Create (DNN.SrcImgSize.Width,DNN.SrcImgSize.Height);
   t_object->GetImagePatch_YUY2 (SrcFrmBuffer,SrcFrmSize,Scale,Inflation,t_object->DNNInputImage);

#if 0
   static int count = 0;
   std::string str = "object/object_" + std::to_string(count++) + ".jpg";
   t_object->DNNInputImage.WriteJPGFile(str.c_str(), 100);
   return;
#endif

   if (0 == t_object->DNNInputImage.Width || 0 == t_object->DNNInputImage.Height)
      return;

   cv::Mat m_s_cimage;
   OpenCV_InitMat (t_object->DNNInputImage,m_s_cimage);
   std::vector<Prediction> result = DNN.Classify (m_s_cimage);
   if (result.size()) {
      std::string& label = result[0].first;
      int obj_type = OC_TYPE_UNKNOWN;
      if      (std::string::npos != label.find("human"))   obj_type = OC_TYPE_HUMAN;
      else if (std::string::npos != label.find("vehicle")) obj_type = OC_TYPE_VEHICLE;
      t_object->Result_POC = obj_type;
      t_object->OTLikelihood[obj_type] += 1.0f;

      //logi("Object     ID [%d]  Type [%s]", t_object->ID, label.c_str());
   }

   #if defined(__DEBUG_OBC)
   _DebugView_OBC->DrawImage (s_cimage,_DVX_OBC,_DVY_OBC);
   _DVY_OBC += s_cimage.Height;
   CString type_name;
   ConvertString (label.c_str(),type_name);
   CString text;

   text.Format (_T("Object #%04d: %s (%d tries)"),t_object->ID,type_name,t_object->Count_POC);
   _DebugView_OBC->DrawText (text,_DVX_OBC,_DVY_OBC,PC_GREEN);
   _DVY_OBC += 20;
   #endif
}

 int ObjectClassification_DNN::UpdateLikelihood (TrackedObject* t_object)

{
   if (t_object->IsTypeFixed (   )) return (1);
   if (ObjectRecognition::UpdateLikelihood (t_object)) return (2);
   return (DONE);
}

 void ObjectClassification_DNN::UpdateRecognitionResult (TrackedObject* t_object)

{
   t_object->UpdateObjectType (FrameDuration);
   if (t_object->Time_TypeUnchanged > MaxTime_Unchanged) t_object->TypeHistory |= t_object->Type;
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectClassificationServer_DNN
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_CAFFE)

 ObjectClassificationServer_DNN::ObjectClassificationServer_DNN (   )

{
   DLLHandle   = NULL;
   GpuNo       = 0;
   Flag_UseGPU = TRUE;
}

 void ObjectClassificationServer_DNN::Close (   )

{
   ObjectRecognitionServer::Close (   );
}

 int ObjectClassificationServer_DNN::Initialize (void* dll_handle,const char* dir_name,int flag_use_gpu,int gpu_idx)

{

   if (ObjectRecognitionServer::Initialize (   )) return (1);
   DLLHandle        = dll_handle;
   DirName          = dir_name;
   Flag_UseGPU      = flag_use_gpu;
   GpuNo            = gpu_idx;
   Flag_Initialized = TRUE;
   return (DONE);
}

 void ObjectClassificationServer_DNN::ProcessRequest (ObjectTracking* obj_tracker)

{
   int i;

   std::vector<TrackedObject*> t_objects;
   TrackedObject* t_object = obj_tracker->TrackedObjects.First (   );
   while (t_object != NULL) {
      if ((t_object->Status & TO_STATUS_RECOGNIZE) && !t_object->IsTypeFixed (   )) t_objects.push_back (t_object);
      t_object = obj_tracker->TrackedObjects.Next (t_object);
   }
   int n = (int)t_objects.size (   );
   std::vector<cv::Mat> m_s_cimages(n);
   for (i = 0; i < n; i++) {
      OpenCV_InitMat (t_objects[i]->DNNInputImage,m_s_cimages[i]);
   }
   std::vector<std::vector<Prediction> > results = DNN.Classify (m_s_cimages);
   for (i = 0; i < n; i++) {
      std::string& label = results[i][0].first;
      int obj_type = OC_TYPE_UNKNOWN;
      if      (std::string::npos != label.find("human"))   obj_type = OC_TYPE_HUMAN;
      else if (std::string::npos != label.find("vehicle")) obj_type = OC_TYPE_VEHICLE;
      t_objects[i]->Result_POC = obj_type;
      t_objects[i]->OTLikelihood[obj_type] += 1.0f;
   }
}

 void ObjectClassificationServer_DNN::OnThreadEnded (   )

{
   ObjectClassification_DNN_Common::_Close (   );
}

 int ObjectClassificationServer_DNN::OnThreadStarted (   )

{
   if (!IsInitialized (   )) return (1);
   logd (">> Initializing a ObjectClassification DNN...\n");
   int e_code = ObjectClassification_DNN_Common::_Initialize (DLLHandle,DirName,Flag_UseGPU,GpuNo);
   if (e_code) {
      logd ("Initialization of the ObjectClassification DNN failed. (Error Code: %d)\n",e_code);
      return (2);
   }
   return (DONE);
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectClassificationClient
//
///////////////////////////////////////////////////////////////////////////////

 ObjectClassificationClient::ObjectClassificationClient (   )

{
   NumTries = OC_PARAM_NUM_TRIES;
   Inflation(OC_PARAM_INFLATION_X,OC_PARAM_INFLATION_Y);
}

 int ObjectClassificationClient::CheckObject (TrackedObject* t_object)

{
   if (!(t_object->Status & TO_STATUS_VERIFIED)) return (1);
   return (DONE);
}

 int ObjectClassificationClient::DoPreprocessing (ObjectTracking& obj_tracker)

{
   return (ObjectClassification_Common::Preprocess (obj_tracker));
}

 int ObjectClassificationClient::Initialize (   )

{
   if (ObjectRecognitionClient::Initialize (   )) return (1);
   Flag_Initialized = TRUE;
   return (DONE);
}

 void ObjectClassificationClient::UpdateRecognitionResult (TrackedObject* t_object)

{
   if (!(t_object->Status & TO_STATUS_VERIFIED)) return;
   t_object->UpdateObjectType (FrameDuration);
   if (t_object->Time_TypeUnchanged > MaxTime_Unchanged) t_object->TypeHistory |= t_object->Type;
}

}

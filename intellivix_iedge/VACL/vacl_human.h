#if !defined(__VACL_HUMAN_H)
#define __VACL_HUMAN_H

#include "vacl_dnn.h"
//#include "vacl_face.h"
#include "vacl_environ.h"

#if defined(__LIB_OPENCV)
#include "vacl_opencv.h"
#endif

 namespace VACL

{

#if defined(__LIB_DARKNET)

///////////////////////////////////////////////////////////////////////////////
// 
// Class: HumanDetection_YOLO
//
///////////////////////////////////////////////////////////////////////////////

 class HumanDetection_YOLO

{
   public:
      DNN_YOLO DNN;

   public:
      float MinAspectRatio;
      float MaxAspectRatio;

   public:
      HumanDetection_YOLO (   );
      virtual ~HumanDetection_YOLO (   );

   public:
      virtual void Close (   );

   public:
      int Initialize    (const char* dir_name);
      int IsInitialized (   );
      int Perform       (GImage& s_image,IB2DArray1D& d_rects);

};

 inline int HumanDetection_YOLO::IsInitialized (   )

{
   return (DNN.IsInitialized (   ));
}

#endif
}

#endif

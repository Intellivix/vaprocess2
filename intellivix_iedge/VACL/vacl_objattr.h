#if !defined(__VACL_OBJATTR_H)
#define __VACL_OBJATTR_H

#include "vacl_dnn.h"
#include "vacl_event.h"
#include "vacl_objtrack.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: PersonAttrRecognition_DNN
//
///////////////////////////////////////////////////////////////////////////////

#define PAR_ATTR_MALE            0
#define PAR_ATTR_CHILD           1
#define PAR_ATTR_BAG             2
#define PAR_ATTR_STRIPE          3
#define PAR_ATTR_SUIT            4
#define PAR_ATTR_TSHIRT          5
#define PAR_ATTR_SHORT_SLEEVES   6
#define PAR_ATTR_LONG_TROUSERS   7
#define PAR_ATTR_JEANS           8
#define PAR_ATTR_SKIRT           9

#if defined(__LIB_CAFFE)

 class PersonAttrRecognition_DNN

{
   protected:
      int Flag_Enabled;
      int Flag_Initialized;
   
   protected:
      float           FrameDuration;
      float           FrameRate;
      ISize2D         SrcImgSize;
      ISize2D         SrcFrmSize;
      FPoint2D        Scale;
      byte*           SrcFrmBuffer;
      ObjectTracking* ObjectTracker;
   
   protected:
      struct AIDT {
         int   AttrIdx;
         float DThreshold;
      };
      Array1D<AIDT> CI2AI;
   
   protected:
      DNN_Caffe_Classifier DNN;

   public:
      int   NumTries;
      float ProcPeriod;
      float OuterZoneSize;

   public:
      PersonAttrRecognition_DNN (   );
      virtual ~PersonAttrRecognition_DNN (   );

   protected:
      int  CheckObjectInOuterZone (TrackedObject* t_object);
      void RecognizeAttributes    (TrackedObject* t_object,float* d_attrs);
      int  UpdateAttrLikelihood   (TrackedObject* t_object);
   
   public:
      virtual void Close (   );

   public:
      void Enable        (int flag_enable);
      int  Initialize    (const char* dir_name,int flag_use_gpu = TRUE);
      int  IsEnabled     (   );
      int  IsInitialized (   );
      int  Perform       (ObjectTracking& obj_tracker);
};

 inline void PersonAttrRecognition_DNN::Enable (int flag_enable)

{
   Flag_Enabled = flag_enable;
}

 inline int PersonAttrRecognition_DNN::IsEnabled (   )

{
   return (Flag_Enabled);
}

 inline int PersonAttrRecognition_DNN::IsInitialized (   )

{
   return (Flag_Initialized);
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: VehicleTypeRecognition_DNN
//
///////////////////////////////////////////////////////////////////////////////

#define VTR_TYPE_UNKNOWN      0
#define VTR_TYPE_SEDAN        1
#define VTR_TYPE_SUV          2
#define VTR_TYPE_VAN          3
#define VTR_TYPE_TRUCK        4
#define VTR_TYPE_BUS          5
#define VTR_TYPE_TWOWHEELER   6

#if defined(__LIB_CAFFE)

 class VehicleTypeRecognition_DNN

{
   protected:
      int Flag_Enabled;
      int Flag_Initialized;
   
   protected:
      float           FrameDuration;
      float           FrameRate;
      ISize2D         SrcImgSize;
      ISize2D         SrcFrmSize;
      FPoint2D        Scale;
      byte*           SrcFrmBuffer;
      ObjectTracking* ObjectTracker;
   
   protected:
      IArray1D CI2TI;
      DNN_Caffe_Classifier DNN;

   public:
      int   NumTries;
      float ProcPeriod;
      float OuterZoneSize;

   public:
      VehicleTypeRecognition_DNN (   );
      virtual ~VehicleTypeRecognition_DNN (   );

   protected:
      int CheckObjectInOuterZone (TrackedObject* t_object);
      int RecognizeType          (TrackedObject* t_object);
      int UpdateTypeLikelihood   (TrackedObject* t_object);
   
   public:
      virtual void Close (   );

   public:
      void Enable        (int flag_enable);
      int  Initialize    (const char* dir_name,int flag_use_gpu = TRUE);
      int  IsEnabled     (   );
      int  IsInitialized (   );
      int  Perform       (ObjectTracking& obj_tracker);
};

 inline void VehicleTypeRecognition_DNN::Enable (int flag_enable)

{
   Flag_Enabled = flag_enable;
}

 inline int VehicleTypeRecognition_DNN::IsEnabled (   )

{
   return (Flag_Enabled);
}

 inline int VehicleTypeRecognition_DNN::IsInitialized (   )

{
   return (Flag_Initialized);
}

#endif

}

#endif

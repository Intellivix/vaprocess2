#include "vacl_svm.h"

#define LINEAR_SVM_DATA_FILE_HEADER   "LINEAR_SVM_DATA"

 namespace VACL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Class: LinearSVM
//
///////////////////////////////////////////////////////////////////////////////

 LinearSVM::LinearSVM (   )
 
{
   _Init (   );
}

 void LinearSVM::_Init (   )
 
{
   Dimension = 0;
   Bias      = 0.0;
}

 void LinearSVM::Close (   )
 
{
   Weights.Delete (   );
   _Init (   );
}

 void LinearSVM::Initialize (int dimension)
 
{
   Dimension = dimension;
   Weights.Create (dimension);
}

 int LinearSVM::IsInitialized (   )
 
{
   if (Dimension && (float*)Weights != NULL) return (TRUE);
   else return (FALSE);
}

 float LinearSVM::Predict (float* s_array)
 
{
   int i;
   
   if (!Dimension || !Weights) return (1);
   float result = 0.0;
   for (i = 0; i < Dimension; i++) {
      result += s_array[i] * Weights[i];
   }
   result += Bias;
   return (result);
}

 int LinearSVM::ReadFile (const char* file_name)
 
{
   Close (   );
   CArray1D header((int)strlen(LINEAR_SVM_DATA_FILE_HEADER) + 1);
   FileIO file(file_name,FIO_BINARY,FIO_READ);
   if (!file) return (1);
   if (file.Read ((byte*)(char*)header,header.Length,sizeof(char))) return (2);
   header[header.Length - 1] = 0;
   if (strcmp (header,LINEAR_SVM_DATA_FILE_HEADER)) return (3);
   if (file.Read ((byte*)&Dimension,1,sizeof(int))) return (4);
   Weights.Create (Dimension);
   if (file.Read ((byte*)(float*)Weights,Dimension,sizeof(float))) return (5);
   if (file.Read ((byte*)&Bias,1,sizeof(float))) return (6);
   return (DONE);
}

 int LinearSVM::WriteFile (const char* file_name)
 
{
   FileIO file(file_name,FIO_BINARY,FIO_CREATE);
   if (!file) return (1);
   int header_size = (int)strlen (LINEAR_SVM_DATA_FILE_HEADER) + 1;
   if (file.Write ((byte*)LINEAR_SVM_DATA_FILE_HEADER,header_size,1)) return (2);
   if (file.Write ((byte*)&Dimension,1,sizeof(int))) return (3);
   if (file.Write ((byte*)(float*)Weights,Dimension,sizeof(float))) return (4);
   if (file.Write ((byte*)&Bias,1,sizeof(float))) return (5);
   return (DONE);
}

}

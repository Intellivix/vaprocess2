﻿#include "vacl_dnn.h"
#include "vacl_ipp.h"
#include "DNNClassifier.h"
#include <fstream>

#if defined(__OS_WINDOWS)
#include <windows.h>
#endif
#if defined(__linux)
#include <dlfcn.h>
#endif
#if defined(__LIB_OPENVINO)
   #if defined(__OS_WINDOWS)
   #pragma warning(disable: 4275)
   #endif
   #include "ext_list.hpp"
   #if defined(__OS_WINDOWS)
      #if defined(_DEBUG)
         #pragma comment(lib,"Debug/inference_engine.lib")
         #pragma comment(lib,"Debug/cpu_extension.lib")
      #else
         #pragma comment(lib,"Release/inference_engine.lib")
         #pragma comment(lib,"Release/cpu_extension.lib")
      #endif
   #endif
#endif

#if defined(__DEBUG_FPE)
#include "Win32CL/Win32CL.h"
extern int _DVX_FPE,_DVY_FPE;
extern CImageView* _DebugView_FPE;
#endif

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: DNN_Caffe_ObjectClassifier
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_CAFFE)

 static bool PairCompare (const std::pair<float,int>& lhs,const std::pair<float,int>& rhs)

{
   return (lhs.first > rhs.first);
}

 static std::vector<int> Argmax (const std::vector<float>& v,int N)
// Return the indices of the top N values of std::vector v.
{
   std::vector<std::pair<float,int> > pairs;
   for (int i = 0; i < (int)v.size(); ++i)
      pairs.push_back (std::make_pair (v[i],i));
   std::partial_sort (pairs.begin(),pairs.begin() + N,pairs.end(),PairCompare);
   std::vector<int> result;
   for (int i = 0; i < N; ++i)
      result.push_back (pairs[i].second);
   return (result);
}

 DNN_Caffe_ObjectClassifier::DNN_Caffe_ObjectClassifier (   )

{
   Caffe_Handle        = NULL;
   Caffe_Initialize    = NULL;
   Caffe_Uninitialize  = NULL;
   Caffe_Predict       = NULL;
   Caffe_PredictBatch  = NULL;
   Caffe_Classify      = NULL;
   Caffe_ClassifyBatch = NULL;
   Caffe_GetInputSize  = NULL;
}

 DNN_Caffe_ObjectClassifier::~DNN_Caffe_ObjectClassifier (   )

{
   Close (   );
}

 std::vector<Prediction> DNN_Caffe_ObjectClassifier::Classify (cv::Mat& img,int N)

{
   std::vector<Prediction> predictions;
   if (Caffe_Classify)
      Caffe_Classify(Caffe_Handle, img, predictions);

   return (predictions);
}

 void DNN_Caffe_ObjectClassifier::Close (   )

{
    if (Caffe_Uninitialize) {
       if (Caffe_Handle) {
          Caffe_Uninitialize(Caffe_Handle);
          Caffe_Handle = nullptr;
       }
    }

   ClassNames.Delete (   );
   Flag_Initialized = FALSE;
}

 std::vector<std::vector<Prediction> > DNN_Caffe_ObjectClassifier::Classify (const std::vector<cv::Mat>& imgs,int N)

{
   std::vector<std::vector<Prediction> > all_predictions;
   if (Caffe_ClassifyBatch)
      Caffe_ClassifyBatch(Caffe_Handle, imgs, all_predictions);
   
   return (all_predictions);
}

 int DNN_Caffe_ObjectClassifier::Initialize (void* dll_handle,const char* cfg_file_name,const char* wgt_file_name,const char* cls_file_name,const char* mns_file_name,int flag_use_gpu, int gpu_idx)

{
   int i;

   Close (   );
   
   // Check whether the files exist or not.
   if (AccessFile (cfg_file_name)) return (1);
   if (AccessFile (wgt_file_name)) return (2);
   if (AccessFile (cls_file_name)) return (3);
   if (AccessFile (mns_file_name)) return (4);
   if (SetProcAddresses(dll_handle)) return (5);

   if (Caffe_Initialize)
      Caffe_Handle = Caffe_Initialize(cfg_file_name,
                                      wgt_file_name,
                                      mns_file_name,
                                      cls_file_name,
                                      flag_use_gpu,
                                      gpu_idx);
   
   if (nullptr == Caffe_Handle)
      return (6);

   if (Caffe_GetInputSize)
      Caffe_GetInputSize(Caffe_Handle, SrcImgSize.Width, SrcImgSize.Height);
   
   // Load the label file.
   std::string cls_name = cls_file_name;
   std::vector<std::string> labels;
   std::ifstream ifs_labels(cls_name);
   if (ifs_labels) {
      std::string line;
      while (std::getline (ifs_labels,line)) 
         labels.push_back (std::string(line));
   }
   // Copy the labels to ClassNames.
   int n = int(labels.size ( ));
   if (n) {
      ClassNames.Create (n);
      for (i = 0; i < n; i++) 
         ClassNames[i] = labels[i].c_str (   );
   }
   Flag_Initialized = TRUE;
   return (DONE);
}

 void DNN_Caffe_ObjectClassifier::Predict (const cv::Mat& img,std::vector<float>& output)

{
   if (Caffe_Predict)
      Caffe_Predict(Caffe_Handle, img, output);
}

 void DNN_Caffe_ObjectClassifier::Predict (const std::vector<cv::Mat>& imgs,std::vector<std::vector<float> >& outputs)

{
   if (Caffe_PredictBatch)
      Caffe_PredictBatch(Caffe_Handle, imgs, outputs);
}
 
  int DNN_Caffe_ObjectClassifier::SetProcAddresses(void* dll_handle)

{
#if defined(_WIN32)
   if (dll_handle == NULL) return (-1);

   Caffe_Initialize = (Caffe_InitializeFunc)GetProcAddress((HMODULE)dll_handle, "Initialize");
   if (!Caffe_Initialize) return (-2);

   Caffe_Uninitialize = (Caffe_UninitializeFunc)GetProcAddress((HMODULE)dll_handle, "Uninitialize");
   if (!Caffe_Initialize) return (-3);

   Caffe_Predict = (Caffe_PredictFunc)GetProcAddress((HMODULE)dll_handle, "Predict");
   if (!Caffe_Predict) return (-4);

   Caffe_PredictBatch = (Caffe_PredictBatchFunc)GetProcAddress((HMODULE)dll_handle, "PredictBatch");
   if (!Caffe_PredictBatch) return (-5);

   Caffe_Classify = (Caffe_ClassifyFunc)GetProcAddress((HMODULE)dll_handle, "Classify");
   if (!Caffe_Classify) return (-6);

   Caffe_ClassifyBatch = (Caffe_ClassifyBatchFunc)GetProcAddress((HMODULE)dll_handle, "ClassifyBatch");
   if (!Caffe_ClassifyBatch) return (-7);

   Caffe_GetInputSize = (Caffe_GetInputSizeFunc)GetProcAddress((HMODULE)dll_handle, "GetInputSize");
   if (!Caffe_GetInputSize) return (-8);

   return (DONE);
#else
#if 0
   if (dll_handle == NULL) return (-1);

   Caffe_Initialize = (Caffe_InitializeFunc)dlsym(dll_handle, "Initialize");
   if (!Caffe_Initialize) return (-2);

   Caffe_Uninitialize = (Caffe_UninitializeFunc)dlsym(dll_handle, "Uninitialize");
   if (!Caffe_Initialize) return (-3);

   Caffe_Predict = (Caffe_PredictFunc)dlsym(dll_handle, "Predict");
   if (!Caffe_Predict) return (-4);

   Caffe_PredictBatch = (Caffe_PredictBatchFunc)dlsym(dll_handle, "PredictBatch");
   if (!Caffe_PredictBatch) return (-5);

   Caffe_Classify = (Caffe_ClassifyFunc)dlsym(dll_handle, "Classify");
   if (!Caffe_Classify) return (-6);

   Caffe_ClassifyBatch = (Caffe_ClassifyBatchFunc)dlsym(dll_handle, "ClassifyBatch");
   if (!Caffe_ClassifyBatch) return (-7);

   Caffe_GetInputSize = (Caffe_GetInputSizeFunc)dlsym(dll_handle, "GetInputSize");
   if (!Caffe_GetInputSize) return (-8);

   return (DONE);
#else
   Caffe_Initialize    = ::Initialize;
   Caffe_Uninitialize  = ::Uninitialize;
   Caffe_Predict       = ::Predict;
   Caffe_PredictBatch  = ::PredictBatch;
   Caffe_Classify      = ::Classify;
   Caffe_ClassifyBatch = ::ClassifyBatch;
   Caffe_GetInputSize  = ::GetInputSize;

   return (DONE);
#endif

#endif
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: DNN_Darknet_YOLO
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_DARKNET)
 
 DNN_Darknet_YOLO::DNN_Darknet_YOLO (   )

{
   Flag_Initialized    = FALSE;
   Network             = NULL;
   DThreshold          = 0.3f;
   NMSThreshold        = 0.4f;

   Yolo_Initialize     = NULL;
   Yolo_Uninitialize   = NULL;
   Yolo_Predict        = NULL;
   Yolo_FreeDetections = NULL;
}

 DNN_Darknet_YOLO::~DNN_Darknet_YOLO (   )

{
   Close (   );
}

 void DNN_Darknet_YOLO::Close (   )

{
    if (Yolo_Uninitialize) {
       if (Network) {
          Yolo_Uninitialize(Network);
          Network = nullptr;
       }
    }

   ClassNames.Delete (   );
   SrcImgSize.Clear  (   );
   Flag_Initialized = FALSE;
}

 void DNN_Darknet_YOLO::Convert (BGRImage& s_image,FArray1D& d_array)

{
   int x,y;
   
   int area = s_image.Width * s_image.Height;
   d_array.Create (area * 3);
   float* dpr = (float*)d_array;
   float* dpg = dpr + area;
   float* dpb = dpg + area;
   for (y = 0; y < s_image.Height; y++) {
      BGRPixel* sp = s_image[y];
      for (x = 0; x < s_image.Width; x++) {
         *dpr++ = sp->R / 255.0f;
         *dpg++ = sp->G / 255.0f;
         *dpb++ = sp->B / 255.0f;
         sp++;
      }
   }
}

 void DNN_Darknet_YOLO::GetObjectBoundingBoxes (int si_width,int si_height,detection* detections,int n_detections,int n_classes,OBBArray1D& d_array)

{
   int i,j,n;
   
   OBBArray1D t_array(n_detections);
   for (i = n = 0; i < n_detections; i++) {
      float* probs = detections[i].prob;
      for (j = 0; j < n_classes; j++) {
         if (probs[j] > DThreshold) break;
      }
      if (j < n_classes) {
         box& s_box = detections[i].bbox;
         float hw = 0.5f * s_box.w;
         float hh = 0.5f * s_box.h;
         int sx = (int)((s_box.x - hw) * si_width);
         int ex = (int)((s_box.x + hw) * si_width);
         int sy = (int)((s_box.y - hh) * si_height);
         int ey = (int)((s_box.y + hh) * si_height);
         CropRegion (si_width,si_height,sx,sy,ex,ey);
         t_array[n](sx,sy,ex - sx,ey - sy);
         t_array[n].Score    = probs[j];
         t_array[n].ClassIdx = j;
         n++;
      }
   }
   if (n) d_array = t_array.Extract (0,n);
   else d_array.Delete (   );
}

 int DNN_Darknet_YOLO::Initialize (void* dll_handle,const char* cfg_file_name,const char* wgt_file_name,const char* cls_file_name, int gpu_idx)

{
   if (SetProcAddresses(dll_handle))
      return (-1);
   
   Close (   );

   #if defined(__DEBUG)
   logd (">> Loading data files for DNNs... ");
   #endif
   if (Yolo_Initialize)
      Network = Yolo_Initialize(cfg_file_name, wgt_file_name, gpu_idx);

   if (Network == NULL) {
      #if defined(__DEBUG)
      logd (" Failed. (Error Code: 1)\n");
      #endif
      return (1);
   }

   if (LoadClassNames (cls_file_name)) {
      Close (   );
      #if defined(__DEBUG)
      logd (" Failed. (Error Code: 2)\n");
      #endif
      return (2);
   }

   SrcImgSize(Network->w,Network->h);
   Flag_Initialized = TRUE;
   #if defined(__DEBUG)
   logd (" OK.\n");
   #endif

   return (DONE);
}

 int DNN_Darknet_YOLO::LoadClassNames (const char* cls_file_name)

{
   ClassNames.Delete  (   );
   FileIO file(cls_file_name,FIO_TEXT,FIO_READ);
   if (!file) return (1);
   int n = 0;
   int flag_cont = TRUE;
   StringA text(MAX_BUFFER_SIZE);
   STRArray1DA t_array(100000);
   while (flag_cont && n < t_array.Length) {
      if (file.ReadLine (text,text.Length)) flag_cont = FALSE;
      if (strlen (text)) t_array[n++] = text;
   }
   if (!n) return (2);
   ClassNames = t_array.Extract (0,n);

   return (DONE);
}

 int DNN_Darknet_YOLO::Predict (BGRImage& s_cimage,OBBArray1D& d_array)

{
   if (!IsInitialized (   )) return (1);
   BGRImage  t_cimage;
   BGRImage* p_s_cimage = &s_cimage;
   if (s_cimage.Width != SrcImgSize.Width || s_cimage.Height != SrcImgSize.Height) {
      t_cimage.Create (SrcImgSize.Width,SrcImgSize.Height);
      Resize_2 (s_cimage,t_cimage);
      p_s_cimage = &t_cimage;
   }
   FArray1D si_array;
   Convert (*p_s_cimage,si_array);
   int n_dets = 0;
   int n_classes = 0;
   detection* dets = nullptr;
   if (Yolo_Predict)
      dets = Yolo_Predict(Network, si_array, p_s_cimage->Width, p_s_cimage->Height, DThreshold, NMSThreshold, n_dets, n_classes);
   GetObjectBoundingBoxes (s_cimage.Width,s_cimage.Height,dets,n_dets,n_classes,d_array);
   if (Yolo_FreeDetections && dets)
      Yolo_FreeDetections(dets, n_dets);

   return (DONE);
}

 int DNN_Darknet_YOLO::SetProcAddresses(void* dll_handle)

{
   if (dll_handle == NULL) return (-1);

   Yolo_Initialize = (Yolo_InitializeFunc)GetProcAddress((HMODULE)dll_handle, "Initialize");
   if (!Yolo_Initialize) return (-2);

   Yolo_Uninitialize = (Yolo_UninitializeFunc)GetProcAddress((HMODULE)dll_handle, "Uninitialize");
   if (!Yolo_Uninitialize) return (-3);

   Yolo_Predict = (Yolo_PredictFunc)GetProcAddress((HMODULE)dll_handle, "Predict");
   if (!Yolo_Predict) return (-4);

   Yolo_FreeDetections = (Yolo_FreeDetectionsFunc)GetProcAddress((HMODULE)dll_handle, "FreeDetections");
   if (!Yolo_FreeDetections) return (-5);

   return (DONE);
}
 
#endif

}

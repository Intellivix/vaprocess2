#if !defined(__VACL_FIRE_H)
#define __VACL_FIRE_H

#include "vacl_objtrack.h"

 namespace VACL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Class: FlameDetection
//
///////////////////////////////////////////////////////////////////////////////

 class FlameDetection : public BlobTracking
 
{
   protected:
      int Flag_NightMode;
   
   protected:
      struct VFData {
         GImage Image;
         VFData* Prev;
         VFData* Next;
      };
      LinkedList<VFData> VFQueue;

   protected:
      BArray2D CntArray;
      IArray2D SumArray;
      BArray3D Frames;
      GImage   FlickerImage;
   
   protected:
      BlobTracking BlobTracker;
  
   public:
      float FDPeriod;
   
   public:
      GImage FlameImage;

   public:
      FlameDetection (   );
      virtual ~FlameDetection (   );

   protected:
      void DetectFlickering           (GImage& s_image,GImage& fg_image,GImage& d_image);
      void GetInitFlameImage          (ForegroundDetection& frg_detector,GImage& d_image);
      void MarkMotionPixels           (GImage& s_image,int d_thrsld,GImage& d_image);
      void MarkNonFlameBlobs          (GImage& m_image);
      void RemoveNonFireColoredPixels (BGRImage& s_cimage,GImage& d_image);
      void RemoveNonFlameRegions      (GImage& d_image);

   public:
      virtual void Close (   );
   
   public:
      int  Initialize (ISize2D& si_size,float frm_rate);
      int  Perform    (ObjectTracking& obj_tracker);
      void Reset      (   );
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: BlobTracking_Smoke
//
///////////////////////////////////////////////////////////////////////////////

 class BlobTracking_Smoke : public BlobTracking

{
   public:
      float MaxNorSpeed;
      float MinSpeed;
      float MinTime_Tracked;

   public:
      BlobTracking_Smoke (   );

   protected:
      virtual int FilterTrackedBlob (TrackedBlob* t_blob);
      virtual int VerifyTrackedBlob (TrackedBlob* t_blob);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: SmokeDetection
//
///////////////////////////////////////////////////////////////////////////////

#define SMD_COLOR_BLACK_OR_WHITE   0
#define SMD_COLOR_BLACK_ONLY       1
#define SMD_COLOR_WHITE_ONLY       2

 class SmokeDetection : public BlobTracking
 
{
   protected:
      struct VFData {
         GImage Image;
         VFData* Prev;
         VFData* Next;
      };
      LinkedList<VFData> VFQueue;
      IArray2D SDCArray; // Smoke detection count array
      
   protected:
      BlobTracking_Smoke BlobTracker;
   
   // Input
   public:
      int SmokeColor; // Smoke color
      int MinWSI;     // Minimum white smoke intensity

   // Input
   public:
      int   MinArea;          // Minimum smoke region area
      int   MDThreshold;      // Motion detection threshold
      float MaxBndStrength;   // Maximum smoke region bounary strength
      float MaxSaturation;    // Maximum smoke color saturation
      float MinMotion;        // Minimum amount of motion
      float TSThreshold;      // Texture similarity threshold
      float MaxTransparency1; // Maximum transparency (for regions brighter than background)
      float MaxTransparency2; // Maximum transparency (for regions darker than background)
      float SRDThreshold;     // Shadow region detection threshold
      float MinHRT;           // Minimum headlight region tranasparency
      float HRDThreshold;     // Headlight region detection threshold
      float SDCThreshold;     // Smoke detection count threshold
      
   // Output
   public:
      GImage IntSmokeImage;
      GImage SmokeImage;

   public:
      SmokeDetection (   );
      virtual ~SmokeDetection (   );

   protected:
      void DetectSmokeColoredPixels         (BGRImage& s_cimage,GImage& d_image,GImage& g_image);
      void GetFinalSmokeImage               (GImage& d_image);
      int  GetInitSmokeImage                (ForegroundDetection& frg_detector,GImage& d_image);
      void GetRegionInfo                    (GImage& s_image,GImage& g_image,GImage& bg_image,IArray2D& cr_map,CRArray1D& cr_array,BArray1D& rc_array,BArray1D& rt_array);
      void MarkNonsmokeRegions_Area         (CRArray1D& cr_array,BArray1D& mk_array);
      void MarkNonsmokeRegions_Color        (BArray1D& rc_array,BArray1D& mk_array);
      void MarkNonsmokeRegions_Headlight    (GImage& s_image,GImage& bg_image,IArray2D& cr_map,CRArray1D& cr_array,BArray1D& rt_array,FArray1D& tp_array,BArray1D& mk_array,FArray1D& sc_array);
      void MarkNonsmokeRegions_Motion       (GImage& s_image,IArray2D& cr_map,CRArray1D& cr_array,BArray1D& mk_array,FArray1D& sc_array);
      void MarkNonsmokeRegions_Shadow       (ObjectTracking& obj_tracker,IArray2D& cr_map,CRArray1D& cr_array,BArray1D& rt_array,BArray1D& mk_array);
      void MarkNonsmokeRegions_Transparency (GImage& s_image,GImage& bg_image,GImage& sm_image,IArray2D& cr_map,CRArray1D& cr_array,BArray1D& rt_array,BArray1D& mk_array,FArray1D& sc_array);
      void SetSDCArray                      (TrackedBlob* t_blob);
      void RemoveForegroundPixels           (GImage& fg_image,float ser_factor,GImage& d_image);
      
   public:
      virtual void Close             (   );
      virtual int  VerifyTrackedBlob (TrackedBlob* t_blob);
   
   public:
      virtual int CheckSetup (SmokeDetection* sd);
      virtual int ReadFile   (FileIO &file);
      virtual int ReadFile   (CXMLIO *pIO);
      virtual int WriteFile  (FileIO &file);
      virtual int WriteFile  (CXMLIO *pIO);

   public:
      int  Initialize (ISize2D& si_size,float frm_rate);
      int  Perform    (ObjectTracking& obj_tracker);
      void Reset      (   );
};

}

#endif

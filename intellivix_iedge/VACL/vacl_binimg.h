#if !defined(__VACL_BINIMG_H)
#define __VACL_BINIMG_H

#include "vacl_define.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

void  Binarize                (GImage& s_image,int min_pv,int max_pv,GImage& d_image);
int   GetNumEntirePixels      (GImage& s_image);
int   GetNumRegionPixels      (GImage& s_image,IBox2D& s_rgn);
void  GetRegionBoundaryPixels (GImage& s_image,GImage &d_image);
int   GetRegionPerimeter      (GImage& s_image);
float GetRegionPixelDensity   (GImage& s_image,IBox2D& s_rgn);
float MeasureNoisiness        (GImage& s_image);
void  RemoveImpulsiveNoise    (GImage& s_image,int n_nps,GImage& d_image);

}

#endif

#if !defined(__IVCP_PKSTRUCT_H)
#define __IVCP_PKSTRUCT_H

#include "ivcp_packet.h"

 namespace IVCP
 
{

///////////////////////////////////////////////////////////////////////////////
//
//  IVCP 3.0
//
///////////////////////////////////////////////////////////////////////////////

typedef UINT64 CHFLAG;   // 채널 별 녹화정보를 담는 타입 

///////////////////////////////////////////////////////////////////////////////
//
// Enumerations (PacketType)
//
///////////////////////////////////////////////////////////////////////////////

enum IVCP30_PacketType
{
   IVCP_TYPE_UNKNOWN = 0,
   IVCP_TYPE_CONN,      // 연결 
   IVCP_TYPE_SYSTEM,    // 시스템
   IVCP_TYPE_CAMERA,    // 카메라 
   IVCP_TYPE_PTZ,       // PTZ 제어
   IVCP_TYPE_LIVE,      // 실시간 데이터
   IVCP_TYPE_PLAY,      // 재생 데이터
   IVCP_TYPE_SEARCH,    // 검색 관련
   IVCP_TYPE_EXPORT     // 내보내기 관련
};
   
enum IVCP30_BodyType
{
   IVCP_TYPE_XML = 0,   // XML 텍스트
   IVCP_TYPE_TXT,       // Text 형태
   IVCP_TYPE_BIN,       // Binary 형태
   IVCP_TYPE_XMZ        // XML 압축 (with Zip)
};

///////////////////////////////////////////////////////////////////////////////
//
// Enumerations (PacketID)
//
///////////////////////////////////////////////////////////////////////////////

enum IVCP30_PacketID
{
   IVCP_ID_Unknown = 0,
   IVCP_ID_Conn_OptionsReq = 1,           // 시스템의 종류,버전과 카메라 개수를 읽어옵니다.
   IVCP_ID_Conn_OptionsRes,
   IVCP_ID_Conn_LoginReq,                 // 로그인을 요청합니다. 옵션으로 시스템 정보를 요청합니다.
   IVCP_ID_Conn_LoginRes,              
   IVCP_ID_Conn_LogoutReq,                // 로그아웃을 요청합니다.
   IVCP_ID_Conn_LogoutRes,    
   IVCP_ID_Conn_CameraListReq,            // 카메라 목록 정보를 요청합니다.
   IVCP_ID_Conn_CameraListRes,
   IVCP_ID_Conn_StartUpReq,               // 비디오/메타데이터 전송을 전체 시작한다.
   IVCP_ID_Conn_StartUpRes,
   IVCP_ID_Conn_ShutDownReq,              // 비디오/메타데이터 전송을 전체 중지한다.
   IVCP_ID_Conn_ShutDownRes,
   IVCP_ID_Conn_SnapshotReq,              // 초기 접속시 카메라 스냅 화면 요청
   IVCP_ID_Conn_SnapshotRes,

   IVCP_ID_Camera_LiveMetadataReq,        // 카메라 메타데이터 전송을 요청합니다.
   IVCP_ID_Camera_LiveMetadataRes,
   IVCP_ID_Camera_LiveVideoReq,           // 카메라 비디오 전송을 요청합니다.
   IVCP_ID_Camera_LiveVideoRes,
   
   IVCP_ID_Camera_PlayStreamStartReq,     // 재생 영상 전송을 요청합니다.
   IVCP_ID_Camera_PlayStreamStartRes,
   IVCP_ID_Camera_PlayStreamStopReq,      // 재생 영상 전송을 중지합니다.
   IVCP_ID_Camera_PlayStreamStopRes,
   IVCP_ID_Camera_PlayStreamControlReq,   // 재생 영상 전송을 제어합니다.
   IVCP_ID_Camera_PlayStreamControlRes,

   IVCP_ID_Camera_SnapshotReq,            // 카메라 스냅 화면 요청
   IVCP_ID_Camera_SnapshotRes,
   IVCP_ID_Camera_SettingReq,             // 카메라 관련 세팅을 조절합니다.
   IVCP_ID_Camera_SettingRes,
   IVCP_ID_Camera_CommandReq,             // 카메라 관련 명령을 요청합니다.
   IVCP_ID_Camera_CommandRes,
   IVCP_ID_Camera_TalkAudioReq,           // 오디오 방송을 설정합니다.
   IVCP_ID_Camera_TalkAudioRes,
   IVCP_ID_Camera_TalkStatus,             // 오디오 방송 상태 전달
   IVCP_ID_Camera_SendAudioData,          // 오디오 방송 업로드 데이터
   IVCP_ID_Camera_RecvAudioData,          // 오디오 방송 다운로드 데이터

   IVCP_ID_Ptz_ContMoveReq,               // PTZ 연속 이동 제어
   IVCP_ID_Ptz_ContMoveRes,
   IVCP_ID_Ptz_AbsMoveReq,                // PTZ 절대각 제어
   IVCP_ID_Ptz_AbsMoveRes,
   IVCP_ID_Ptz_BoxMoveReq,                // PTZ 박스 이동 제어 (화면에서의 포인트)
   IVCP_ID_Ptz_BoxMoveRes,
   IVCP_ID_Ptz_GetAbsPosReq,              // PTZ 절대각 읽기
   IVCP_ID_Ptz_GetAbsPosRes,

   IVCP_ID_Live_FrameInfo,                // 프레임 메타데이터 (실시간)
   IVCP_ID_Live_EventInfo,                // 이벤트 메타데이터
   IVCP_ID_Live_ObjectInfo,               // 객체 메타데이터
   IVCP_ID_Live_PlateInfo,                // 번호판 메타데이터
   IVCP_ID_Live_FaceInfo,                 // 얼굴 메타데이터
   IVCP_ID_Live_LogInfo,                  // 로그 정보
   IVCP_ID_Live_PtzDataInfo,              // PTZ 정보 메타데이터
   IVCP_ID_Live_PresetIdInfo,             // PresetID 메타데이터
   IVCP_ID_Live_EventZoneInfo,            // 이벤트존 메타데이터
   IVCP_ID_Live_EventZoneCount,           // 이벤트존 카운트 메타데이터
   IVCP_ID_Live_PresetMapInfo,            // 프리셋 맵 정보
   IVCP_ID_Live_SettingInfo,              // 카메라 설정 정보
   IVCP_ID_Live_VideoData,                // 영상 수신 데이터
   IVCP_ID_Live_AudioData,                // 오디오 수신 데이터
   IVCP_ID_Live_FrmObjInfo,               // 프레임 객제 메터데이터 (SDS 지원용 - 빅데이터용)
   IVCP_ID_Live_Status,                   // 실시간 상태 정보
   
   IVCP_ID_Play_FrameInfo,
   IVCP_ID_Play_EventInfo,
   IVCP_ID_Play_ObjectInfo,
   IVCP_ID_Play_PlateInfo,
   IVCP_ID_Play_FaceInfo,
   IVCP_ID_Play_PtzDataInfo,
   IVCP_ID_Play_PresetIdInfo,
   IVCP_ID_Play_EventZoneInfo,
   IVCP_ID_Play_EventZoneCount,
   IVCP_ID_Play_VideoData,
   IVCP_ID_Play_AudioData,
   IVCP_ID_Play_Progress,

   IVCP_ID_Search_FrameReq,               // 프레임 검색을 요청합니다.
   IVCP_ID_Search_FrameRes,
   IVCP_ID_Search_EventReq,               // 이벤트 검색을 요청합니다.
   IVCP_ID_Search_EventRes,
   IVCP_ID_Search_ObjectReq,              // 객체 검색을 요청합니다.
   IVCP_ID_Search_ObjectRes,
   IVCP_ID_Search_PlateReq,               // 번호판 검색을 요청합니다.
   IVCP_ID_Search_PlateRes,
   IVCP_ID_Search_FaceReq,                // 얼굴 검색을 요청합니다.
   IVCP_ID_Search_FaceRes,
   IVCP_ID_Search_LogReq,                 // 로그 검색을 요청합니다.
   IVCP_ID_Search_LogRes,
   IVCP_ID_Search_StopReq,                // 검색을 중지합니다. (통합)
   IVCP_ID_Search_StopRes,

   IVCP_ID_Search_Progress,               // 검색 진행 과정
   IVCP_ID_Search_FrameInfo,              // 프레임 검색 결과
   IVCP_ID_Search_EventInfo,              // 이벤트 검색 결과
   IVCP_ID_Search_ObjectInfo,             // 객체 검색 결과
   IVCP_ID_Search_PlateInfo,              // 번호판 검색 결과
   IVCP_ID_Search_FaceInfo,               // 얼굴 검색 결과
   IVCP_ID_Search_LogInfo,                // 로그 검색 결과

   IVCP_ID_System_SettingReq,
   IVCP_ID_System_SettingRes,
   IVCP_ID_System_CommandReq,
   IVCP_ID_System_CommandRes,
   IVCP_ID_System_MonitorReq,
   IVCP_ID_System_MonitorRes,
   IVCP_ID_System_MonitorInfo,            //
   IVCP_ID_System_AudioInOutReq,          // 오디오 방송을 설정합니다.
   IVCP_ID_System_AudioInOutRes,
   IVCP_ID_System_SendAudioData,
   IVCP_ID_System_RecvAudioData,

   IVCP_ID_Camera_ExportStartReq,
   IVCP_ID_Camera_ExportStartRes,
   IVCP_ID_Camera_ExportStopReq,
   IVCP_ID_Camera_ExportStopRes,

   IVCP_ID_Export_Progress,
   IVCP_ID_Export_FrameInfo,
   IVCP_ID_Export_EventInfo,
   IVCP_ID_Export_ObjectInfo,
   IVCP_ID_Export_PlateInfo,
   IVCP_ID_Export_FaceInfo,
   IVCP_ID_Export_PtzDataInfo,
   IVCP_ID_Export_PresetIdInfo,
   IVCP_ID_Export_EventZoneInfo,
   IVCP_ID_Export_EventZoneCount,
   IVCP_ID_Export_VideoData,
   IVCP_ID_Export_AudioData
};

///////////////////////////////////////////////////////////////////////////////

enum IVCP30_MetadataFlag
{
   IVCP30_META_EVENT_BEGUN   = 0x00000001,
   IVCP30_META_EVENT_ENDED   = 0x00000002,
   IVCP30_META_OBJECT_BEGUN  = 0x00000004,
   IVCP30_META_OBJECT_ENDED  = 0x00000008,
   IVCP30_META_FRAME_INFO    = 0x00000010,
   IVCP30_META_PLATE_BEGUN   = 0x00000020,
   IVCP30_META_PLATE_ENDED   = 0x00000040,
   IVCP30_META_FACE_BEGUN    = 0x00000080,
   IVCP30_META_FACE_ENDED    = 0x00000100,
   IVCP30_META_PTZ_DATA      = 0x00000200,
   IVCP30_META_PRESET_ID     = 0x00000400,
   IVCP30_META_EVTZONE_INFO  = 0x00000800,
   IVCP30_META_EVTZONE_COUNT = 0x00001000,
   IVCP30_META_PRESET_MAP    = 0x00002000,
   IVCP30_META_SETTING       = 0x00004000,
   IVCP30_META_LOG_INFO      = 0x00008000,
   IVCP30_META_FRAME_OBJ     = 0x00010000
};

// 카메라 특성 값

enum IVCP30_CameraInfoParam
{
   IVCP30_CAM_PTZ_CAMERA        = 0x00000001,  // PTZ 카메라
   IVCP30_CAM_USE_MOBILE        = 0x00000002,  // 모바일용으로 권장하는 카메라
   IVCP30_CAM_USE_AUDIO_OUT     = 0x00000004,  // 오디오 아웃 지원
   IVCP30_CAM_USE_AUDIO_IN      = 0x00000008   // 오디오 인 지원
};

// 카메라 설정 값

#ifndef IVCP_CAMERA_SETTING_FLAG
#define IVCP_CAMERA_SETTING_FLAG

enum IVCP30_CameraSettingFlag
{
   IVCP30_ENABLE_AUTO_PTZ                = 0x0000000000000001, // 자동 추적 기능 (PTZ)
   IVCP30_ENABLE_AUTO_FOCUS              = 0x0000000000000002, // 자동 초점 기능 (Focus)
   IVCP30_ENABLE_PRESET_TOURING          = 0x0000000000000004, // 프리셋 튜어링 기능
   IVCP30_ENABLE_VIDEO_ANALYTICS         = 0x0000000000000008, // 영상분석 On/Off
   IVCP30_PAUSE_VIDEO_ANALYTICS          = 0x0000000000000010, // 영상분석 일시중지 On/Off
   IVCP30_PAUSE_VIDEO_ANALYTICS_SCHEDULE = 0x0000000000000020, // 영상분석 스케쥴 일시중지 On/Off
   IVCP30_ENABLE_VIDEO_DEFOG             = 0x0000000000000040, // 영상 안개 개선 기능 On/Off
   IVCP30_ENABLE_VIDEO_STABILIZATION     = 0x0000000000000080, // 영상 안정화 기능 On/Off
   IVCP30_START_HOME_POS_VIDEO_ANALYTICS = 0X0000000000000100, // 홈 위치 비디오 분석 시작 (홈 위치와 상관없이)
   IVCP30_GOTO_HOME_POSITION             = 0X0000000000000200,

   IVCP30_ENABLE_WIPER                   = 0x0000000000001000,
   IVCP30_ENABLE_HEATER                  = 0x0000000000002000,
   IVCP30_ENABLE_BLC_MODE                = 0x0000000000004000,
   IVCP30_ENABLE_ONE_SHOT_AUTO_FOCUS     = 0x0000000000008000,
   IVCP30_ENABLE_THERMAL                 = 0x0000000000010000,
   IVCP30_ENABLE_FAN                     = 0x0000000000020000,
   IVCP30_ENABLE_PTZ_LOCK                = 0x0000000000040000,
   
   IVCP30_ENABLE_PLATE_INSTANT           = 0x0000000000100000, // 번호판 인식 (고정)
   IVCP30_ENABLE_PLATE_LOCKEDOBJ         = 0x0000000000200000, // 번호판 인식 (마스터/슬래이브)
   IVCP30_ENABLE_FACE_INSTANT            = 0x0000000000400000, // 얼굴 감지 (고정)
   IVCP30_ENABLE_FACE_LOCKEDOBJ          = 0x0000000000800000  // 얼굴 감지 (마스터/슬래이브)
};

#endif

// 카메라 설정 값
#ifndef IVCP_CAMERA_COMMAND_FLAG
#define IVCP_CAMERA_COMMAND_FLAG

enum IVCP30_CameraCommandFlag
{
   IVCP30_CAMERA_RESTART          = 1,  // 카메라 재시작
   IVCP30_PTZ_POSITION_INIT       = 2,  // PTZ 위치 초기화
   IVCP30_PTZ_GOTO_HOME_POSITION  = 3,  // PTZ 홈 위치 이동
   IVCP30_SET_DEFOG_LEVEL         = 4,  // 안개 개선 레벨
   IVCP30_SET_STABILIZATION_LEVEL = 5,  // 영상 안정화 레벨
   IVCP30_SENSOR_INPUT            = 6,  // 센서 입력
   IVCP30_GOTO_PRESET_ID          = 7,  // 프리셋 이동
   IVCP30_GOTO_PRESET_TOURING     = 8,  // 프리셋 투어링 시작
   IVCP30_SET_IR_MODE             = 9,  // IR 제어 - 0:Off, 1:On, 2:Schedule
   IVCP30_SET_DAY_NIGHT_MODE      = 10, // Day Night Mode - 0:Day, 1:Night, 2:Auto
   IVCP30_SET_AUTO_WHITE_BALANCE  = 11  // Auto White Balance - 0:Auto, 1:Manual, 2:Indoor, 3:Outdoor
};

#endif

///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// Functions (PacketType, PacketID)
//
///////////////////////////////////////////////////////////////////////////////

const char* IvcpGetPacketTypeString  (int nPacketID);
const char* IvcpGetPacketDescDString (int nPacketID);
void        IvcpGetPacketString      (char *strMessage,int nPacketID,int nBodyType = IVCP_TYPE_XML);

BOOL IvcpIsBinaryPacket    (const char *strMessage);
int  IvcpGetPacketType     (const char *strMessage);
int  IvcpGetPacketIDforReq (const char *strMessage);
int  IvcpGetPacketIDforRes (const char *strMessage);

void ivcpGetFileTimeFromString (FILETIME &ftTime, const char *strTime);
void ivcpGetStringFromFileTime (StringA &strTime, FILETIME &ftTime);

///////////////////////////////////////////////////////////////////////////////
//
//  CIVCPPacketExt
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPPacketExt
{
public:
   int m_nPacketID;

public:
   virtual int ReadXML         (CXMLIO* pIO) { return (DONE); }
   virtual int WriteXML        (CXMLIO* pIO) { return (DONE); }
   virtual int ReadPacketBody  (FileIO& buff);
   virtual int WritePacketBody (FileIO& buff);

public:
   virtual int ReadFromPacket (CIVCPPacket& aPacket);
   virtual int WriteToPacket  (CIVCPPacket& aPacket);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnOptionReq : 서버 기본 정보를 읽어온다.
// 
///////////////////////////////////////////////////////////////////////////////

class CIVCPConnOptionsReq : public CIVCPPacketExt
{
public:
   float m_fNetworkVer;

public:
   CIVCPConnOptionsReq (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnOptionsRes : 서버 기본 정보
// 
///////////////////////////////////////////////////////////////////////////////

class CIVCPConnOptionsRes : public CIVCPPacketExt
{
public:
   char  m_strDeviceName[128];  // G400 V4.0
   int   m_nDeviceType;         // VPU, RAS, SRS, VMS
   int   m_nDeviceVer;          // 1332
   float m_fNetworkVer;         // IVCP 3.0
   char  m_strSystemName[128];  // 시스템 이름
   int   m_nCameraNum;          // 카메라 개수
   int   m_nSystemUID;          // 시스템 UID

public:
   enum IVCP_DeviceType
   {
      IVX = 0,
      RAS = 1,
      SRS = 2,
      VMS = 3,
      ADM = 4,
      OTH = 5
   };

public:
   CIVCPConnOptionsRes (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnLoginReq : 로그인을 요청한다.
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPConnLoginReq : public CIVCPPacketExt
{
public:
   int  m_nLoginType;         // 인증방식 (자동, 로컬, 인증)
   char m_strUserID[128];     // 사용자 이름
   char m_strPasswd[128];     // 암호
   char m_strDeviceName[128]; // 디바이스 이름
   int  m_nDeviceType;        // 디바이스 종류
   int  m_nServiceType;       // 연결 서비스 형태 (일반, Push, Ctrl)
   int  m_nServiceCaps;       // 연결 서비스에 사용 가능한 옵션

public:
   enum IVCP_LoginType
   {
      Auto   = 0,
      Local  = 1,
      Server = 2
   };

public:
   CIVCPConnLoginReq (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnLoginRes : 로그인 응답
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPConnLoginRes : public CIVCPPacketExt
{
public:
   int m_nLoginResult;

public:
   enum IVCP_LoginResult
   {
      LoginSuccess    = 0,
      InvalidAccount  = 1,
      IncorrectPasswd = 2,
      AlreadyLogin    = 3,
      NotSupported    = 4
   };

public:
   CIVCPConnLoginRes (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnLogoutReq : 로그아웃을 요청한다.
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPConnLogoutReq : public CIVCPPacketExt
{
public:
   int m_nLogoutType;

public:
   CIVCPConnLogoutReq (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnLogoutRes : 로그아웃 응답
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPConnLogoutRes : public CIVCPPacketExt
{
public:
   int m_nLogoutResult;

public:
   enum IVCP_Error
   {
      LogoutSuccess = 0,
      ForcedLogout  = 1
   };

public:
   CIVCPConnLogoutRes ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnCameraListReq : 카메라 목록 정보 요청
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPConnCameraListReq : public CIVCPPacketExt
{
public:
   int m_nStart; // 읽어올 카메라 인덱스 (0: 전부)
   int m_nCount; // 읽어올 카메라 개수

public:
   CIVCPConnCameraListReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// class CIVCPCameraInfo
//
///////////////////////////////////////////////////////////////////////////////

#define MAX_VSTREAMS                3
#define MAX_ASTREAMS                1

class CIVCPCameraInfo
{
public:
   // 카메라 기본 정보
   int   m_nCameraUID;                   // 카메라 고유 번호 (UID)
   char  m_strGuid[128];                 // 카메라 Guid (고유 키값)
   int   m_nCameraNo;                    // 로컬 채널 번호
   char  m_strCameraName[128];           // 카메라 이름
   int   m_nCameraType;                  // 카메라 종류 (아날로그/IP카메라/파일)
   int   m_nCameraParam;                 // 카메라 특성 (PTZ 카메라 여부 등)
   int   m_nWidth;                       // 원본 영상 너비
   int   m_nHeight;                      // 원본 영상 높이
   float m_fFrameRate;                   // 원본 프레임 레이트
   int   m_nVStreamNum;                  // 비디오 스트리밍 갯수
   int   m_nAStreamNum;                  // 오디오 스트리밍 갯수

   // 비디오 스트리밍 정보
   int   m_nVideoCodec[MAX_VSTREAMS];    // 압축 코덱
   int   m_nQuality[MAX_VSTREAMS];       // 영상 품질
   int   m_nVideoWidth[MAX_VSTREAMS];    // 영상 너비
   int   m_nVideoHeight[MAX_VSTREAMS];   // 영상 높이
   float m_fFrmRate[MAX_VSTREAMS];       // 영상 전송 프레임 레이트
   int   m_nSendType[MAX_VSTREAMS];      // 전송 타입 (이벤트-적응형, 일반)

   // 오디오 스트리밍 정보
   int   m_nAudioCodec[MAX_ASTREAMS];    // 오디오 코덱
   int   m_nChannels[MAX_ASTREAMS];      // 채널수
   int   m_nSamplingRate[MAX_ASTREAMS];  // 영상 너비
   int   m_nBitsPerSample[MAX_ASTREAMS]; // 영상 높이

public:
   CIVCPCameraInfo (   );
   virtual ~CIVCPCameraInfo (   ) {   };

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);

public:
   void ParseVideoStreamDesc (int nIdx, StringA &strDesc);
   void ParseAudioStreamDesc (int nIdx, StringA &strDesc);
   void MakeVideoStreamDesc  (int nIdx, StringA &strDesc);
   void MakeAudioStreamDesc  (int nIdx, StringA &strDesc);
};

typedef std::deque<CIVCPCameraInfo*> IVCPCameraInfoList;

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnCameraListRes : 카메라 목록 정보 응답
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPConnCameraListRes : public CIVCPPacketExt
{
public:
   // 카메라 정보
   int m_nStart; // 읽어온 카메라 시작 (0: 전부, 1-based index)
   int m_nCount; // 읽어온 카메라 개수
   IVCPCameraInfoList m_CameraList;

public:
   CIVCPConnCameraListRes (   );
   virtual ~CIVCPConnCameraListRes (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);

public:
   void ClearCameras (   );
};

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnStartUpReq : 비디오/메타데이터 전송을 전체 시작한다.
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPConnStartUpReq : public CIVCPPacketExt
{
public:
   int m_nStreamFlag;
   int m_nMetaFlag;
   int m_nOptions;

public:
   CIVCPConnStartUpReq (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnStartUpRes
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPConnStartUpRes : public CIVCPPacketExt
{
public:
   int m_nResult;

public:
   CIVCPConnStartUpRes (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnShutDownReq : 비디오/메타데이터 전송을 전체 중지한다.
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPConnShutDownReq : public CIVCPPacketExt
{
public:
   int m_nStreamFlag;
   int m_nMetaFlag;
   int m_nOptions;

public:
   CIVCPConnShutDownReq (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnShutDownRes
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPConnShutDownRes : public CIVCPPacketExt
{
public:
   int m_nResult;

public:
   CIVCPConnShutDownRes (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnSnapshotReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPConnSnapshotReq : public CIVCPPacketExt
{
public:
   CHFLAG m_nChannelFlag;
   int    m_nResizeFlag;
   int    m_nWidth;
   int    m_nHeight;

public:
   CIVCPConnSnapshotReq (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPConnSnapshotRes - 카메라 개수대로 날라옴
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPConnSnapshotRes : public CIVCPPacketExt
{
public:
   int   m_nCameraUID;
   int   m_nCodecID;
   int   m_nWidth;
   int   m_nHeight;

public:
   int   m_nDataLen;
   int   m_nBodyLen;
   char* m_pBodyBuffer; // 전달용 버퍼

public:
   CIVCPConnSnapshotRes (   );
  ~CIVCPConnSnapshotRes (   );

public:
   byte *GetSnapshotBuffer (   ) { return (byte*)(m_pBodyBuffer + m_nDataLen); }
   int   GetSnapshotLen    (   ) { return (m_nBodyLen - m_nDataLen); }

public:
   virtual int ReadXML        (CXMLIO* pIO);
   virtual int WriteXML       (CXMLIO* pIO);
   virtual int ReadFromPacket (CIVCPPacket& aPacket);
   virtual int WriteToPacket  (CIVCPPacket& aPacket);

   void ParseBodyBuffer (   );
   void MakeBodyBuffer  (byte *pEncodedBuff, int nEncodedLen);
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// (CONN) CIVCPSystemResultRes
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPSystemResultRes : public CIVCPPacketExt
{
public:
   int m_nResponseID; // 응답 ID
   int m_nResult;     // 결과값  

public:
   CIVCPSystemResultRes (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraResultRes
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPCameraResultRes : public CIVCPPacketExt
{
public:
   int m_nCameraUID;  // 카메라 UID
   int m_nResponseID; // 응답 ID
   int m_nResult;     // 결과값  

public:
   CIVCPCameraResultRes (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraLiveMetadataReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPCameraLiveMetadataReq : public CIVCPPacketExt
{
public:
   int m_nCameraUID;  // 카메라 UID
   int m_nResponseID; // 응답 ID
   int m_nMetaFlag;   // 전송 여부
   int m_nOptionFlag; // 썸메일 전송 여부

public:
   CIVCPCameraLiveMetadataReq (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraLiveVideoReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPCameraLiveVideoReq : public CIVCPPacketExt
{
public:
   int m_nCameraUID;  // 카메라 UID
   int m_nResponseID; // 응답 ID
   int m_nStreamFlag; // 스트리밍 인덱스 (우측부터 0 채널: 1 << StreamIndex)

public:
   CIVCPCameraLiveVideoReq (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraPlayStreamStartReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPCameraPlayStreamStartReq : public CIVCPPacketExt
{
public:
   int      m_nCameraUID;    // 카메라 UID
   int      m_nResponseID;   // 응답 ID
   int      m_nStreamID;     // 스트림 콜백 ID
   int      m_nStreamFlag;   // 전송받을 스트림 플래그
   int      m_nMetaFlag;     // 전송받을 메타 플래그
   FILETIME m_ftStart;       // 재생 시작 시간
   FILETIME m_ftEnd;         // 재생 종료 시간
   BOOL     m_bReverse;      // 재생 방향
   float    m_fPlaySpeed;    // 재생 속도 (1/8 ~ 32)
   int      m_nPlayMode;     // 이벤트 재생 여부 (현재는 미사용)

   BOOL     m_bTranscode;    // 트랜스코딩 여부
   int      m_nCodecID;      // 코덱 ID
   int      m_nResizeOption; // 축소 방법 (0:비율, 1:너비기준크기, 2:고정크기)
   float    m_fResizeRate;   // 축소 비율
   int      m_nWidth;        // 크기
   int      m_nHeight;       // 높이
   int      m_nQuality;      // 품질

public:
   CIVCPCameraPlayStreamStartReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraPlayStreamStopReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPCameraPlayStreamStopReq : public CIVCPPacketExt
{
public:
   int m_nCameraUID;  // 카메라 UID
   int m_nResponseID; // 응답 ID
   int m_nStreamID;   // 스트림 콜백 ID

public:
   CIVCPCameraPlayStreamStopReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraPlayStreamControlReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPCameraPlayStreamControlReq : public CIVCPPacketExt
{
public:
   int      m_nCameraUID;  // 카메라 UID
   int      m_nResponseID; // 응답 ID
   int      m_nStreamID;   // 스트림 콜백 ID
   int      m_nCtrlFlag;   // 플래그 (1: 플레이시간, 2:재생속도. 4:재생방향)
   FILETIME m_ftPlayTime;  // 현재 재생 시간
   float    m_fPlaySpeed;  // 재생 속도 (1/8 ~ 32)
   BOOL     m_bReverse;    // 재생 방향

public:
   CIVCPCameraPlayStreamControlReq ();

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraSnapshotReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPCameraSnapshotReq : public CIVCPPacketExt
{
public:
   int m_nCameraUID;
   int m_nResponseID; // 응답 ID
   int m_nResizeFlag;
   int m_nWidth;
   int m_nHeight;

public:
   CIVCPCameraSnapshotReq (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraSnapshotRes
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPCameraSnapshotRes : public CIVCPPacketExt
{
public:
   int   m_nCameraUID;
   int   m_nResponseID;  // 응답 ID
   int   m_nCodecID;
   int   m_nWidth;
   int   m_nHeight;

public:
   int   m_nDataLen;
   int   m_nBodyLen;
   char* m_pBodyBuffer; // 전달용 버퍼

public:
   CIVCPCameraSnapshotRes (   );
  ~CIVCPCameraSnapshotRes (   );

   byte *GetSnapshotBuffer (   ) { return (byte*)(m_pBodyBuffer + m_nDataLen); }
   int GetSnapshotLen      (   ) { return (m_nBodyLen - m_nDataLen); }

public:
   virtual int ReadXML        (CXMLIO* pIO);
   virtual int WriteXML       (CXMLIO* pIO);
   virtual int ReadFromPacket (CIVCPPacket& aPacket);
   virtual int WriteToPacket  (CIVCPPacket& aPacket);

   void ParseBodyBuffer (   );
   void MakeBodyBuffer  (byte *pEncodedBuff,int nEncodedLen);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CAMERA) CIVCPCameraSettingReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPCameraSettingReq : public CIVCPPacketExt
{
public:
   int    m_nCameraUID;
   int    m_nResponseID;  // 응답 ID
   UINT64 m_nSettingMask;
   UINT64 m_nSettingFlag;

public:
   CIVCPCameraSettingReq (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CAME) CIVCPCameraCommandReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPCameraCommandReq : public CIVCPPacketExt
{
public:
   int m_nCameraUID;
   int m_nResponseID; // 응답 ID
   int m_nCommand;
   int m_nParam1;
   int m_nParam2;

public:
   CIVCPCameraCommandReq (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CAME) CIVCPCameraTalkAudioReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPCameraTalkAudioReq : public CIVCPPacketExt
{
public:
   int m_nCameraUID;
   int m_nResponseID; // 응답 ID
   int m_nTalkID;
   int m_nAudioFlag;

public:
   CIVCPCameraTalkAudioReq (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (CAME) CIVCPCameraTalkStatus
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPCameraTalkStatus : public CIVCPPacketExt
{
public:
   int m_nCameraUID;
   int m_nTalkID;
   int m_nStatus;

public:
   CIVCPCameraTalkStatus (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (DATA) CIVCPCameraSendAudioData
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPCameraSendAudioData : public CIVCPPacketExt
{
public:
   int   m_nCameraUID;
   int   m_nTalkID;
   int   m_nCodecID;
   int   m_nChannel;
   int   m_nBitPerSamples;
   int   m_nSamplingRate;
   
public:
   int   m_nDataLen;
   int   m_nBodyLen;
   char* m_pBodyBuffer; // 전달용 버퍼

public:
   CIVCPCameraSendAudioData (   );
  ~CIVCPCameraSendAudioData (   );

   byte* GetAudioDataBuffer (   ) { return (byte*)(m_pBodyBuffer + m_nDataLen); }
   int   GetAudioDataLen    (   ) { return (m_nBodyLen - m_nDataLen); }

public:
   virtual int ReadFromPacket (CIVCPPacket& aPacket);
   virtual int WriteToPacket  (CIVCPPacket& aPacket);

   void ParseBodyBuffer (   );
   void MakeBodyBuffer  (byte *pEncodedBuff,int nEncodedLen);
};

///////////////////////////////////////////////////////////////////////////////
//
// (PTZ) CIVCPCameraPtzContMoveReq
//
///////////////////////////////////////////////////////////////////////////////

const float IVCP_PTZ_NO_CMD = (float)1E-20;   

class CIVCPCameraPtzContMoveReq : public CIVCPPacketExt
{
public:
   int   m_nCameraUID;  // 카메라 UID
   int   m_nResponseID; // 응답 ID
   float m_fPan;
   float m_fTilt;
   float m_fZoom;
   float m_fFocus;

public:
   CIVCPCameraPtzContMoveReq (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (PTZ) CIVCPCameraPtzAbsMoveReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPCameraPtzAbsMoveReq : public CIVCPPacketExt
{
public:
   int   m_nCameraUID;  // 카메라 UID
   int   m_nResponseID; // 응답 ID
   float m_fPan;
   float m_fTilt;
   float m_fZoom;
   float m_fFocus;

public:
   CIVCPCameraPtzAbsMoveReq (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (PTZ) CIVCPCameraPtzBoxMoveReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPCameraPtzBoxMoveReq : public CIVCPPacketExt
{
public:
   int   m_nCameraUID;  // 카메라 UID
   int   m_nResponseID; // 응답 ID
   float m_fLeft;
   float m_fTop;
   float m_fWidth;
   float m_fHeight;   

public:
   CIVCPCameraPtzBoxMoveReq (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (PTZ) CIVCPCameraPtzGetAbsPosReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPCameraPtzGetAbsPosReq : public CIVCPPacketExt
{
public:
   int m_nCameraUID;  // 카메라 UID
   int m_nResponseID; // 응답 ID

public:
   CIVCPCameraPtzGetAbsPosReq (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (PTZ) CIVCPCameraPtzGetAbsPosRes
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPCameraPtzGetAbsPosRes : public CIVCPPacketExt
{
public:
   int      m_nCameraUID;  // 카메라 UID
   int      m_nResponseID; // 응답 ID
   FILETIME m_ftFrameTime; // 메타 발생 시간
   float    m_fPanAngle;   // Pan 각도 : 반시계 방향이 증가하는 방향이다. 단위(degree, 0~360도)
   float    m_fTiltAngle;  // Tilt 각도 : 아래로 향하는 방향이 증가하는 방향임. 단위(degree, 0~360도)
   float    m_fZoomFactor; // Zoom 값 : 현재 줌 위치를 나타냄. 18배줌 카메라이면 1~18사이의 값을 가짐.
   float    m_fFOV;        // FOV (Field Of View) : Pan 축으로의 화각을 나타냄. 단위(degree, 0~360도)

public:
   CIVCPCameraPtzGetAbsPosRes (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLiveFrameInfo
//
///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// CIVCPFrameEvent

class CIVCPFrameEvent
{
public:
   int m_nEventID;
   int m_nEventType;

public:
   CIVCPFrameEvent (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

/////////////////////////////////////////////////////////////////////
// CIVCPFrameObject

class CIVCPFrameObject
{
public:
   int    m_nObjectID;
   int    m_nObjectType;
   IBox2D m_ObjectBox;
   int    m_nEventNum;
   CIVCPFrameEvent* m_pEvents;

public:
   CIVCPFrameObject (   );
  ~CIVCPFrameObject (   );

   void ClearEvents (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

/////////////////////////////////////////////////////////////////////
// CIVCPLiveFrameInfo

class CIVCPLiveFrameInfo : public CIVCPPacketExt
{
public:
   int      m_nCameraUID;
   FILETIME m_ftFrameTime;
   int      m_nObjectNum;
   CIVCPFrameObject* m_pObjects;

public:
   int      m_nCameraState;
   int      m_nPTZState;
   int      m_nPTZStateEx;
   float    m_fFrameRate;
   int      m_nProcWidth;
   int      m_nProcHeight;

public:
   CIVCPLiveFrameInfo (   );
  ~CIVCPLiveFrameInfo (   );

   void ClearObjects (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLiveFrameObectInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPFrameObjectEx
{
public:
   int    m_nObjectID;
   int    m_nObjectType;
   IBox2D m_ObjectBox;
   int    m_nObjectStatus;
   float  m_fCalcArea;
   float  m_fCalcWidth;
   float  m_fCalcHeight;
   float  m_fCalcMajorAxisLen;
   float  m_fCalcMinorAxisLen;
   float  m_fCalcSpeed;
   float  m_fRealArea;
   float  m_fRealWidth;
   float  m_fRealHeight;
   float  m_fRealMajorAxisLen;
   float  m_fRealMinorAxisLen;
   float  m_fRealSpeed;
   float  m_fNorSpeed;
   int    m_nEventNum;
   CIVCPFrameEvent* m_pEvents;

public:
   CIVCPFrameObjectEx (   );
  ~CIVCPFrameObjectEx (   );

   void ClearEvents (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

/////////////////////////////////////////////////////////////////////
class CIVCPLiveFrameObjectInfo : public CIVCPPacketExt
{
public:
   int      m_nCameraUID;
   char     m_strCameraGuid[128];
   FILETIME m_ftFrameTime;
   float    m_fFrameRate;
   ISize2D  m_szFrameSize;
   int      m_nObjectNum;
   CIVCPFrameObjectEx* m_pObjects;

public:
   CIVCPLiveFrameObjectInfo (   );
  ~CIVCPLiveFrameObjectInfo (   );

   void ClearObjects (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLiveEventInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPLiveEventInfo : public CIVCPPacketExt
{
public:
   int      m_nCameraUID;            // 카메라 UID
   FILETIME m_ftStartTime;           // 이벤트 시작 시간
   FILETIME m_ftEndTime;             // 이벤트 종료 시간
   int      m_nEventStatus;          // 이벤트 상태 (시작, 종료)
   int      m_nEventID;              // 이벤트 ID (시작-종료을 매칭시킬때 사용)
   int      m_nEventType;            // 이벤트 종류
   int      m_nEventRuleID;          // 이벤트 룰 ID
   int      m_nObjectID;             // 객체 ID (출력,저장용)
   int      m_nObjectType;           // 객체 종류 (사람, 차량, 미확인)
   IBox2D   m_ObjectBox;             // 객체 위치 (아래 Proc 좌표 기준)
   int      m_nEventZoneID;          // 발생한 이벤트 존 ID
   char     m_strEventZoneName[128]; // 발생한 이벤트 존 이름
 
   int      m_nProcWidth;            // 분석 영상 크기
   int      m_nProcHeight;
   int      m_nThumbCodecID;         // 썸네일 코덱 ID
   int      m_nThumbWidth;           // 썸네일 이미지 크기
   int      m_nThumbHeight;
   BOOL     m_bThumbnail;            // 썸네일 첨부 여부

public:
   int      m_nDataLen;
   int      m_nBodyLen;
   char*    m_pBodyBuffer;           // 전달용 버퍼

public:
   enum IVCP_LiveEventInfoStatus
   {
      EVENT_STATUS_BEGUN  =  0, // Start of event.
      EVENT_STATUS_ENDED  =  1  // End of event.
   };

public:
   CIVCPLiveEventInfo (   );
  ~CIVCPLiveEventInfo (   );

   byte* GetThumbnailBuffer (   ) { return (byte*)(m_pBodyBuffer + m_nDataLen); }
   int   GetThumbnailLen    (   ) { return (m_nBodyLen - m_nDataLen); }

public:
   virtual int ReadXML        (CXMLIO* pIO);
   virtual int WriteXML       (CXMLIO* pIO);
   virtual int ReadFromPacket (CIVCPPacket& aPacket);
   virtual int WriteToPacket  (CIVCPPacket& aPacket);

   void ParseBodyBuffer (   );
   void MakeBodyBuffer  (byte *pEncodedBuff,int nEncodedLen);
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLiveObjectInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPLiveObjectInfo : public CIVCPPacketExt
{
public:
   int      m_nCameraUID;      // 카메라 UID
   FILETIME m_ftStartTime;
   FILETIME m_ftEndTime;
   int      m_nObjectStatus;
   int      m_nObjectID;
   int      m_nObjectType;
   IBox2D   m_ObjectBox;
   // Average
   float    m_fAvgArea;
   float    m_fAvgWidth;
   float    m_fAvgHeight;
   float    m_fAvgAspectRatio;
   float    m_fAvgAxisLengthRatio;
   float    m_fAvgMajorAxisLength;
   float    m_fAvgMinorAxisLength;
   float    m_fAvgNorSpeed;
   float    m_fAvgSpeed;
   float    m_fAvgRealArea;
   float    m_fAvgRealDistance;
   float    m_fAvgRealWidth;
   float    m_fAvgRealHeight;
   float    m_fAvgRealMajorAxisLength;
   float    m_fAvgRealMinorAxisLength;
   float    m_fAvgRealSpeed;
   // Max
   int      m_nMaxWidth;
   int      m_nMaxHeight;
   float    m_fMaxArea;
   float    m_fMaxAspectRatio;
   float    m_fMaxAxisLengthRatio;
   float    m_fMaxMajorAxisLength;
   float    m_fMaxMinorAxisLength;
   float    m_fMaxNorSpeed;
   float    m_fMaxSpeed;
   float    m_fMaxRealDistance;
   float    m_fMaxRealSpeed;
   // Min
   int      m_nMinWidth;
   int      m_nMinHeight;
   float    m_fMinArea;
   float    m_fMinAspectRatio;
   float    m_fMinAxisLengthRatio;
   float    m_fMinMajorAxisLength;
   float    m_fMinMinorAxisLength;
   float    m_fMinNorSpeed;
   float    m_fMinSpeed;
   float    m_fMinRealDistance;
   float    m_fMinRealSpeed;
   // Color Histogram
   float    m_fHistogram[10];

   int      m_nProcWidth;       // 분석 영상 크기
   int      m_nProcHeight;
   int      m_nThumbCodecID;    // 썸네일 코덱 ID
   int      m_nThumbWidth;      // 썸네일 이미지 크기
   int      m_nThumbHeight;
   BOOL     m_bThumbnail;       // 썸네일 첨부 여부

public:
   int      m_nDataLen;
   int      m_nBodyLen;
   char*    m_pBodyBuffer; // 전달용 버퍼

public:
   CIVCPLiveObjectInfo (   );
  ~CIVCPLiveObjectInfo (   );

   byte* GetThumbnailBuffer (   ) { return (byte*)(m_pBodyBuffer + m_nDataLen); }
   int   GetThumbnailLen    (   ) { return (m_nBodyLen - m_nDataLen); }

public:
   virtual int ReadXML        (CXMLIO* pIO);
   virtual int WriteXML       (CXMLIO* pIO);
   virtual int ReadFromPacket (CIVCPPacket& aPacket);
   virtual int WriteToPacket  (CIVCPPacket& aPacket);

   void ParseBodyBuffer (   );
   void MakeBodyBuffer  (byte *pEncodedBuff, int nEncodedLen);
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLivePlateInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPLivePlateInfo : public CIVCPPacketExt
{
public:
   int      m_nCameraUID;      // 카메라 UID
   FILETIME m_ftStartTime;     // 시작 시간
   FILETIME m_ftEndTime;       // 종료 시간
   int      m_nStatus;         // 상태 (시작, 종료)
   char     m_strPlateName[20];// 번호판 문자열
   int      m_nPlateID;        // ID (시작-종료을 매칭시킬때 사용)
   int      m_nPlateType;      // 번호판 종류
   IBox2D   m_PlateBox;        // 객체 위치 (아래 Proc 좌표 기준)
   int      m_nProcWidth;      // 분석 영상 크기
   int      m_nProcHeight;
   int      m_nThumbCodecID;   // 썸네일 코덱 ID
   int      m_nThumbWidth;     // 썸네일 이미지 크기
   int      m_nThumbHeight;
   BOOL     m_bThumbnail;      // 썸네일 첨부 여부
   BOOL     m_bPicture;        // 이미지 첨부 여부 (JPG 파일 형태)

   int      m_nMainThumbLen;   // 사이즈
   int      m_nSubThumbLen;
   int      m_nMainImageLen;

public:
   int      m_nDataLen;
   int      m_nBodyLen;
   char*    m_pBodyBuffer;      // 전달용 버퍼

public:
   enum IVCP_LivePlateInfoStatus
   {
      PLATE_STATUS_BEGUN  =  0, // Start
      PLATE_STATUS_ENDED  =  1  // End.
   };

public:
   CIVCPLivePlateInfo (   );
  ~CIVCPLivePlateInfo (   );

   byte *GetMainThumbBuffer (   ) { return (byte*)(m_pBodyBuffer + m_nDataLen); }
   byte *GetSubThumbBuffer  (   ) { return (byte*)(m_pBodyBuffer + m_nDataLen + m_nMainThumbLen); }
   byte *GetMainImageBuffer (   ) { return (byte*)(m_pBodyBuffer + m_nDataLen + m_nMainThumbLen + m_nSubThumbLen); }

public:
   virtual int ReadXML        (CXMLIO* pIO);
   virtual int WriteXML       (CXMLIO* pIO);
   virtual int ReadFromPacket (CIVCPPacket& aPacket);
   virtual int WriteToPacket  (CIVCPPacket& aPacket);

   void ParseBodyBuffer (   );
   void MakeBodyBuffer  (byte *pMainThumbBuff,int nMainThumbLen,byte *pSubThumbBuff,int nSubThumbLen,byte *pMainImageBuff,int nMainImageLen);
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLiveFaceInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPLiveFaceInfo : public CIVCPPacketExt      // not defined
{
public:
   int m_nCameraUID;
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLiveLogInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPLiveLogInfo : public CIVCPPacketExt      // not defined
{
public:
   int m_nCameraUID;
};

///////////////////////////////////////////////////////////////////////////////
//
// (LIVE) CIVCPLivePTZInfoData
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPLivePTZDataInfo : public CIVCPPacketExt
{
public:
   int      m_nCameraUID;  // 카메라 UID
   FILETIME m_ftFrameTime; // 메타 발생 시간
   float    m_fPanAngle;   // Pan 각도 : 반시계 방향이 증가하는 방향이다. 단위(degree, 0~360도)
   float    m_fTiltAngle;  // Tilt 각도 : 아래로 향하는 방향이 증가하는 방향임. 단위(degree, 0~360도)
   float    m_fZoomFactor; // Zoom 값 : 현재 줌 위치를 나타냄. 18배줌 카메라이면 1~18사이의 값을 가짐.
   float    m_fFOV;        // FOV (Field Of View) : Pan 축으로의 화각을 나타냄. 단위(degree, 0~360도)

public:
   CIVCPLivePTZDataInfo (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLivePresetIdInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPLivePresetIdInfo : public CIVCPPacketExt
{
public:
   int      m_nCameraUID;
   FILETIME m_ftFrameTime;
   int      m_nCurPresetID;

public:
   CIVCPLivePresetIdInfo (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (LIVE) CIVCPLiveEventZoneInfo
//
///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// CIVCPPlayEventRule

class CIVCPEventRule_DetectionTime
{
public:
   float m_fDetectionTime;

public:
   CIVCPEventRule_DetectionTime (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};
class CIVCPEventRule_Counting
{
public:
   int     m_nCount;
   float   m_fAspectRatio;
   ILine2D m_aMajorAxis;

public:
   CIVCPEventRule_Counting (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};
class CIVCPEventRule_DirectionalMotion
{
public:
   float m_fDetectionTime;
   float m_fDirection;
   float m_fRange;

public:
   CIVCPEventRule_DirectionalMotion (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};
class CIVCPEventRule_PathPassingItem
{
public:
   int       m_nID;
   IPoint2D  m_ptCenter;
   int       m_nVerticesNum;
   IPoint2D *m_pVertices;

public:
   CIVCPEventRule_PathPassingItem (   );
  ~CIVCPEventRule_PathPassingItem (   );

public:
   void ClearVertices (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};
class CIVCPEventRule_PathPassingArrow
{
public:
   int     m_nSplitZoneID;
   ILine2D m_lnArrow;

public:
   CIVCPEventRule_PathPassingArrow (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};
class CIVCPEventRule_PathPassing
{
public:
   int   m_nDirection;
   float m_fDetectionTime;
   float m_fPassingTime;
   int   m_nItemNum;
   CIVCPEventRule_PathPassingItem *m_pItems;
   int   m_nArrowNum;
   CIVCPEventRule_PathPassingArrow *m_pArrows;

public:
   CIVCPEventRule_PathPassing (   );
  ~CIVCPEventRule_PathPassing (   );

public:
   void ClearItems (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

/////////////////////////////////////////////////////////////////////
// CIVCPEventZone

class CIVCPEventZone
{
public:
   int       m_nZoneID;
   int       m_nPresetID;
   int       m_nPriority;
   int       m_nOptions;
   int       m_nWidth;
   int       m_nHeight;
   char      m_strName[128];
   IPoint2D  m_ptCenter;
   int       m_nVerticesNum;
   IPoint2D *m_pVertices;

   // 룰 정보
   int       m_nRuleOptions;
   int       m_nRuleEventType;
   int       m_nRuleObjectType;
   char      m_strRuleName[128];

   CIVCPEventRule_DetectionTime     m_aAbandoned; // 버려짐
   CIVCPEventRule_Counting          m_aCounting; // 카운트
   CIVCPEventRule_DirectionalMotion m_aDirectionalMotion; // 금지된 방향으로 이동
   CIVCPEventRule_PathPassing       m_aPathPassing; // 경로 통과

public:
   CIVCPEventZone (   );
  ~CIVCPEventZone (   );
   
public:
   void ClearVertices (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

class CIVCPLiveEventZoneInfo : public CIVCPPacketExt
{
public:
   int      m_nCameraUID;
   FILETIME m_ftFrameTime;
   int      m_nWidth;
   int      m_nHeight;
   int      m_nZoneNum;
   CIVCPEventZone *m_pEventZones;

public:
   CIVCPLiveEventZoneInfo (   );
  ~CIVCPLiveEventZoneInfo (   );

public:
   void ClearEventZones (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLiveEventZoneCountInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPEventZoneCount
{
public:
   int m_nZoneID;
   int m_nZoneCount;

public:
   CIVCPEventZoneCount (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////

class CIVCPLiveEventZoneCountInfo : public CIVCPPacketExt
{
public:
   int      m_nCameraUID;
   FILETIME m_ftFrameTime;
   int      m_nZoneNum;
   CIVCPEventZoneCount *m_pZoneCounts;

public:
   CIVCPLiveEventZoneCountInfo (   );
  ~CIVCPLiveEventZoneCountInfo (   );

public:   
   void ClearZoneCounts (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

//////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLivePresetMapInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPPresetItemInfo
{
public:
   int  m_nID;
   char m_strName[256];

public:
   CIVCPPresetItemInfo (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

class CIVCPPresetGroupInfo
{
public:
   int  m_nID;
   char m_strName[256];
   int  m_nCycleType;
   int  m_nPresetNum;
   CIVCPPresetItemInfo *m_pPresetItems;

public:
   CIVCPPresetGroupInfo (   );
  ~CIVCPPresetGroupInfo (   );

public:
   void ClearPresetItems (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////

class CIVCPLivePresetMapInfo : public CIVCPPacketExt
{
public:
   int                   m_nCameraUID;
   FILETIME              m_ftFrameTime;
   int                   m_nGroupNum;
   CIVCPPresetGroupInfo *m_pGroupItems;

public:
   CIVCPLivePresetMapInfo (   );
  ~CIVCPLivePresetMapInfo (   );
   
public:
   void ClearGroupItems (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

//////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPLiveSettingInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPLiveSettingInfo : public CIVCPPacketExt
{
public:
   int      m_nCameraUID;
   FILETIME m_ftFrameTime;
   UINT64   m_nSettingMask;      // 세팅 마스크
   UINT64   m_nSettingFlag;      // 세팅 플래그
   int      m_nStabilNum;
   int      m_nDefogLevel;
   int      m_nWhiteBalanceMode;
   int      m_nDayNightMode;

public:
   CIVCPLiveSettingInfo (   );
  ~CIVCPLiveSettingInfo (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

//////////////////////////////////////////////////////////////////////////////
//
// (DATA) CIVCPLiveVideoStreamData
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPLiveVideoStreamData : public CIVCPPacketExt
{
public:
   int      m_nCameraUID;
   int      m_nStreamIdx;
   int      m_nCodecID;
   int      m_nWidth;
   int      m_nHeight;
   float    m_fFrameRate;
   int      m_nFrameFlag;
   FILETIME m_ftFrameTime;
   
public:
   int      m_nDataLen;
   int      m_nBodyLen;
   char*    m_pBodyBuffer; // 전달용 버퍼

public:
   CIVCPLiveVideoStreamData (   );
  ~CIVCPLiveVideoStreamData (   );

public:
   byte* GetVideoFrameBuffer (  ) { return (byte*)(m_pBodyBuffer + m_nDataLen); }
   int   GetVideoFrameLen    (   ) { return (m_nBodyLen - m_nDataLen); }

public:
   virtual int ReadFromPacket (CIVCPPacket& aPacket);
   virtual int WriteToPacket  (CIVCPPacket& aPacket);

public:   
   void ParseBodyBuffer (   );
   void MakeBodyBuffer  (byte *pEncodedBuff,int nEncodedLen);
};

//////////////////////////////////////////////////////////////////////////////
//
// (DATA) CIVCPLiveAudioStreamData
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPLiveAudioStreamData : public CIVCPPacketExt
{
public:
   int      m_nCameraUID;
   int      m_nStreamIdx;
   int      m_nCodecID;
   int      m_nChannel;
   int      m_nBitPerSamples;
   int      m_nSamplingRate;
   FILETIME m_ftFrameTime;
   
public:
   int      m_nDataLen;
   int      m_nBodyLen;
   char*    m_pBodyBuffer; // 전달용 버퍼

public:
   CIVCPLiveAudioStreamData (   );
  ~CIVCPLiveAudioStreamData (   );

public:
   byte* GetAudioDataBuffer (   ) { return (byte*)(m_pBodyBuffer + m_nDataLen); }
   int   GetAudioDataLen    (   ) { return (m_nBodyLen - m_nDataLen); }

public:
   virtual int ReadFromPacket (CIVCPPacket& aPacket);
   virtual int WriteToPacket  (CIVCPPacket& aPacket);

public:
   void ParseBodyBuffer (   );
   void MakeBodyBuffer  (byte *pEncodedBuff,int nEncodedLen);
};

//////////////////////////////////////////////////////////////////////////////
//
// (INFO) CIVCPLiveStatus
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPLiveStatus : public CIVCPPacketExt
{
public:
   int      m_nCameraUID;  // 카메라 UID
   FILETIME m_ftFrameTime; // 현재 시간
   int      m_nType;       // 상태 타입
   int      m_nParam;      // 상태 정보

public:
   CIVCPLiveStatus (   );
  ~CIVCPLiveStatus (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPPlayFrameMetaInfo
//
///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// CIVCPPlayFrameObject

class CIVCPPlayFrameObject
{
public:
   int    m_nObjectID;
   int    m_nObjectType;
   int    m_nEventFlag;
   int    m_nFlagLocked;
   IBox2D m_ObjectBox;

public:
   CIVCPPlayFrameObject (   );
  ~CIVCPPlayFrameObject (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

/////////////////////////////////////////////////////////////////////
// CIVCPPlayFrameInfo

class CIVCPPlayFrameInfo : public CIVCPPacketExt
{
public:
   int      m_nCameraUID;
   int      m_nStreamID;
   FILETIME m_ftFrameTime;
   int      m_nObjectNum;
   CIVCPPlayFrameObject* m_pObjects;

public:
   int      m_nCameraState;
   int      m_nPTZState;
   int      m_nPTZStateEx;
   float    m_fFrameRate;
   int      m_nProcWidth;
   int      m_nProcHeight;

public:
   CIVCPPlayFrameInfo (   );
  ~CIVCPPlayFrameInfo (   );

public:
   void ClearObjects (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPPlayEventInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPPlayEventInfo : public CIVCPLiveEventInfo // not defined
{
public:
   int m_nStreamID;
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPPlayEventInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPPlayObjectInfo : public CIVCPLiveObjectInfo  // not defined
{
public:
   int m_nStreamID;
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPPlayEventInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPPlayPlateInfo : public CIVCPLivePlateInfo // not defined
{
public:
   int m_nStreamID;
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPPlayEventInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPPlayFaceInfo : public CIVCPLiveFaceInfo  // not defined
{
public:
   int m_nStreamID;
};

///////////////////////////////////////////////////////////////////////////////
//
// (LIVE) CIVCPPlayPTZDataInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPPlayPTZDataInfo : public CIVCPPacketExt
{
public:
   int      m_nCameraUID;
   int      m_nStreamID;
   FILETIME m_ftFrameTime;
   float    m_fPanAngle;   // Pan 각도 : 반시계 방향이 증가하는 방향이다. 단위(degree, 0~360도)
   float    m_fTiltAngle;  // Tilt 각도 : 아래로 향하는 방향이 증가하는 방향임. 단위(degree, 0~360도)
   float    m_fZoomFactor; // Zoom 값 : 현재 줌 위치를 나타냄. 18배줌 카메라이면 1~18사이의 값을 가짐.
   float    m_fFOV;        // FOV (Field Of View) : Pan 축으로의 화각을 나타냄. 단위(degree, 0~360도)

public:
   CIVCPPlayPTZDataInfo (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPPlayPresetIdInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPPlayPresetIdInfo : public CIVCPPacketExt
{
public:
   int      m_nCameraUID;
   int      m_nStreamID;
   FILETIME m_ftFrameTime;
   int      m_nCurPresetID;

public:
   CIVCPPlayPresetIdInfo (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPPlayEventZoneInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPPlayEventZoneInfo : public CIVCPPacketExt
{
public:
   int      m_nCameraUID;
   int      m_nStreamID;
   FILETIME m_ftFrameTime;
   int      m_nWidth;
   int      m_nHeight;
   int      m_nZoneNum;
   CIVCPEventZone *m_pEventZones;

public:
   CIVCPPlayEventZoneInfo (   );
  ~CIVCPPlayEventZoneInfo (   );

public:
   void ClearEventZones (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (PLAY) CIVCPPlayEventZoneCountInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPPlayEventZoneCountInfo : public CIVCPPacketExt
{
public:
   int      m_nCameraUID;
   int      m_nStreamID;
   FILETIME m_ftFrameTime;
   int      m_nZoneNum;
   CIVCPEventZoneCount *m_pZoneCounts;

public:
   CIVCPPlayEventZoneCountInfo (   );
  ~CIVCPPlayEventZoneCountInfo (   );
   
public:
   void ClearZoneCounts (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (DATA) CIVCPPlayVideoStreamData
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPPlayVideoStreamData : public CIVCPPacketExt
{
public:
   int      m_nCameraUID;
   int      m_nStreamID;
   int      m_nCodecID;
   int      m_nWidth;
   int      m_nHeight;
   float    m_fFrameRate;
   int      m_nFrameFlag;
   FILETIME m_ftFrameTime;
   FILETIME m_ftPlayTime;  // 재생 타임 (플레이어 싱크용)
   
public:
   int      m_nDataLen;
   int      m_nBodyLen;
   char*    m_pBodyBuffer; // 전달용 버퍼

public:
   CIVCPPlayVideoStreamData (   );
  ~CIVCPPlayVideoStreamData (   );

public:
   byte* GetVideoFrameBuffer (   ) { return (byte*)(m_pBodyBuffer + m_nDataLen); }
   int   GetVideoFrameLen    (   ) { return (m_nBodyLen - m_nDataLen); }

public:
   virtual int ReadFromPacket (CIVCPPacket& aPacket);
   virtual int WriteToPacket  (CIVCPPacket& aPacket);

public:
   void ParseBodyBuffer (   );
   void MakeBodyBuffer  (byte *pEncodedBuff,int nEncodedLen);
};

///////////////////////////////////////////////////////////////////////////////
//
// (DATA) CIVCPPlayAudioStreamData
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPPlayAudioStreamData : public CIVCPLiveAudioStreamData
{

};

///////////////////////////////////////////////////////////////////////////////
//
// (DATA) CIVCPPlayProgress
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPPlayProgress : public CIVCPPacketExt
{
public:
   int      m_nCameraUID;
   int      m_nStreamID;
   int      m_nStep;
   int      m_nParam;
   FILETIME m_ftTime;
   FILETIME m_ftPlayTime;

public:
   enum IVCP_PlayProgressStep
   {
      WAITING = 1,
      READY   = 2,
      BEGIN   = 3,
      END     = 4
   };

public:
   CIVCPPlayProgress (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchFremeReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPSearchFrameReq : public CIVCPPacketExt
{
public:
   int      m_nSearchID;
   CHFLAG   m_nChannelFlag;
   FILETIME m_ftStart;
   FILETIME m_ftEnd;

public:
   CIVCPSearchFrameReq (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchEventReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPSearchEventReq : public CIVCPPacketExt
{
public:
   int      m_nSearchID;
   CHFLAG   m_nChannelFlag;
   FILETIME m_ftStart;
   FILETIME m_ftEnd;
   int      m_nEvtFlag;
   int      m_nObjFlag;
   int      m_nEvtZoneNum;
   int     *m_pEvtZoneIDs;
   BOOL     m_bThumbnail;  // 섬네일 전송 여부

public:
   CIVCPSearchEventReq (   );
  ~CIVCPSearchEventReq (   );

public:
   void ClearEvtZoneIDs (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchObjectReq
//
///////////////////////////////////////////////////////////////////////////////

#define SEARCH_SELECT_COLOR_MAX   6  // 대표 색상 최대 선택 수

class CIVCPSearchColorFilter
{
public:
   int   m_nColorType;        // 대표 색상 or 히스토그램
   int   m_nColorMode;        // 조건 AND, OR
   int   m_nColorSensibility; // 색상 매칭 민감도

   int   m_nColorNum;                         // 대표 색상 개수
   DWORD m_crColor [SEARCH_SELECT_COLOR_MAX]; // 대표 색상
   int   m_nPercent[SEARCH_SELECT_COLOR_MAX]; // 점유률

   int   m_nWidth;
   int   m_nHeight;
   int   m_nDepth;
   int   m_nHistogramLen;  // 히스토그램 데이터 길이
   void* m_pHistogramData; // 히스토그램 버퍼

public:
   CIVCPSearchColorFilter (   );
  ~CIVCPSearchColorFilter (   );
   
public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);

public:
   void SetHistogramData (void *pBuffer, int nLen = 0, int nWidth = 0, int nHeight = 0, int nDepth = 0);
   BOOL GetHistogramData (void **pBuffer, int &nLen, int &nWidth, int &nHeight, int &nDepth);
};

class CIVCPSearchObjectFilter
{
public:
   int   m_nOptions;
   float m_fMinWidth;
   float m_fMaxWidth;
   float m_fMinHeight;
   float m_fMaxHeight;
   float m_fMinArea;
   float m_fMaxArea;
   float m_fMinAspectRatio;
   float m_fMaxAspectRatio;
   float m_fMinAxisLengthRatio;
   float m_fMaxAxisLengthRatio;
   float m_fMinMajorAxisLength;
   float m_fMaxMajorAxisLength;
   float m_fMinMinorAxisLength;
   float m_fMaxMinorAxisLength;
   float m_fMinNorSpeed;
   float m_fMaxNorSpeed;
   float m_fMinSpeed;
   float m_fMaxSpeed;
   float m_fMinRealArea;
   float m_fMaxRealArea;
   float m_fMinRealDistance;
   float m_fMaxRealDistance;
   float m_fMinRealWidth;
   float m_fMaxRealWidth;
   float m_fMinRealHeight;
   float m_fMaxRealHeight;
   float m_fMinRealMajorAxisLength;
   float m_fMaxRealMajorAxisLength;
   float m_fMinRealMinorAxisLength;
   float m_fMaxRealMinorAxisLength;
   float m_fMinRealSpeed;
   float m_fMaxRealSpeed;

public:
   enum IVCP_SearchObjectFilterOption
   {
      FILTER_ENABLE         = 0x00000001,
      USE_REAL_SIZE_VALUES  = 0x00000002,
      USE_WIDTH             = 0x00000004,
      USE_HEIGHT            = 0x00000008,
      USE_AREA              = 0x00000010,
      USE_ASPECT_RATIO      = 0x00000020,
      USE_AXIS_LENGTH_RATIO = 0x00000040,
      USE_MAJOR_AXIS_LENGTH = 0x00000080,
      USE_MINOR_AXIS_LENGTH = 0x00000100,
      USE_NORMALIZED_SPEED  = 0x00000200,
      USE_SPEED             = 0x00000400,
      USE_REAL_DISTANCE     = 0x00000800
   };

public:
   CIVCPSearchObjectFilter();

   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////

class CIVCPSearchObjectReq : public CIVCPPacketExt
{
public:
   int      m_nSearchID;
   CHFLAG   m_nChannelFlag;
   FILETIME m_ftStart;
   FILETIME m_ftEnd;
   int      m_nCondFlag; // 검색 조건에 대한 플래그

   int                     m_nObjFlag;      // 객체 종류
   CIVCPSearchColorFilter  m_ColorFilter;   // 색상 필터
   CIVCPSearchObjectFilter m_ObjectFilter;  // 객체 필터

   BOOL     m_bThumbnail;  // 섬네일 전송 여부

public:
   enum IVCP_SearchObjectCondition
   {
      USE_COLOR     = 0x00000001,
      USE_OBJTYPE   = 0x00000002,
      USE_OBJFILTER = 0x00000004
   };

public:
   CIVCPSearchObjectReq (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchPlateReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPSearchPlateReq : public CIVCPPacketExt
{
public:
   int      m_nSearchID;
   CHFLAG   m_nChannelFlag;
   FILETIME m_ftStart;
   FILETIME m_ftEnd;
   char     m_strPlateNumber[32];
   BOOL     m_bUseColor;  // 색상 검색 사용
   DWORD    m_crColor;    // 검색 색상
   BOOL     m_bThumbnail; // 섬네일 전송 여부

public:
   CIVCPSearchPlateReq (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchFaceReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPSearchFaceReq : public CIVCPPacketExt
{
public:
   int      m_nSearchID;
   CHFLAG   m_nChannelFlag;
   FILETIME m_ftStart;
   FILETIME m_ftEnd;
   char     m_strName[64];
   BOOL     m_bThumbnail; // 섬네일 전송 여부

public:
   CIVCPSearchFaceReq (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchLogReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPSearchLogReq : public CIVCPPacketExt
{
public:
   int      m_nSearchID;
   CHFLAG   m_nChannelFlag;
   FILETIME m_ftStart;
   FILETIME m_ftEnd;
   char     m_strKeyword[128];
   int      m_nOption;    // 검색 옵션 플래그
   int      m_nEvtFlag;   // 이벤트 플래그
   int      m_nObjFlag;   // 객체 플래그
   BOOL     m_bThumbnail; // 섬네일 전송 여부

public:
   CIVCPSearchLogReq (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchStopReq
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPSearchStopReq : public CIVCPPacketExt
{
public:
   int m_nSearchID;

public:
   CIVCPSearchStopReq (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchCommonRes
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPSearchResultRes : public CIVCPPacketExt
{
public:
   int m_nSearchID;
   int m_nResult;

public:
   CIVCPSearchResultRes (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (FIND) CIVCPSearchProgress
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPSearchProgress : public CIVCPPacketExt
{
public:
   int m_nSearchID;
   int m_nStep;
   int m_nParam;

public:
   enum IVCP_SearchProgressStep
   {
      BEGIN     = 0,
      END       = 1,
      NOT_FOUND = 2
   };

public:
   CIVCPSearchProgress (   );

public:
   virtual int ReadXML  (CXMLIO* pIO);
   virtual int WriteXML (CXMLIO* pIO);
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPSearchFrameInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPSearchFrameInfo : public CIVCPPlayFrameInfo  // not defined
{
public:
   int m_nSearchID;
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPSearchEventInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPSearchEventInfo : public CIVCPLiveEventInfo  // same
{
public:
   int m_nSearchID;
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPSearchObjectInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPSearchObjectInfo : public CIVCPLiveObjectInfo // same
{
public:
   int m_nSearchID;
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPSearchPlateInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPSearchPlateInfo : public CIVCPLivePlateInfo  // same
{
public:
   int m_nSearchID;
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPSearchFaceInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPSearchFaceInfo : public CIVCPLiveFaceInfo  // not defined
{
public:
   int m_nSearchID;
};

///////////////////////////////////////////////////////////////////////////////
//
// (META) CIVCPSearchFaceInfo
//
///////////////////////////////////////////////////////////////////////////////

class CIVCPSearchLogInfo : public CIVCPLiveLogInfo  // not defined
{
public:
   int m_nSearchID;
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// (CAME) CIVCPSystemSettingReq
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// (CAME) CIVCPSystemCammandReq
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// (CAME) CIVCPSystemAudioInOutReq
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// (DATA) CIVCPSystemAudioDataReq
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// (DATA) CIVCPSystemAudioData
//
///////////////////////////////////////////////////////////////////////////////

}

#endif

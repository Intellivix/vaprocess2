#if !defined(__VACL_FMOD_H)
#define __VACL_FMOD_H

#include "vacl_foregnd.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: FastMovingObjectDetection
//
///////////////////////////////////////////////////////////////////////////////

 class FastMovingObjectDetection

{
   protected:
      int Flag_Initialized;

   protected:
      class VFData
     {
        public:
           IArray2D FRMap;
           IArray1D FRAArray;
           BArray1D FMODArray1;
           BArray1D FMODArray2;
        
        public:
           VFData* Prev;
           VFData* Next;
     };
     Queue<VFData> VFQueue;

   // Input
   public:
      int GroupingRange;

   // Input
   public:
      int   VFQLength;
      float MinAreaRatio;
      float MaxOverlap;

   // Input
   public:
      ISize2D SrcImgSize;

   // Output
   public:
      IArray2D AFMOMap;
   
   public:
      FastMovingObjectDetection (   );
      virtual ~FastMovingObjectDetection (   );

   protected:
      void _Init                     (   );
      void AccumulateFMOs            (VFData* vf_data,int fmo_type,GImage& d_image);
      void AccumulateFMOs            (VFData* vf_data,int fmo_type,int fr_no,int base_lb_no,IArray2D& d_map);
      void AddToVidFrmQueue          (ForegroundDetection& frg_detector);
      void CheckFMOValidity          (Array1D<ETElement>& eq_table,IArray1D& nfra_array);
      void GetAccumulatedFMOImage    (int fmo_type,GImage& d_image);
      void GetAccumulatedFMOMap      (int fmo_type,IArray2D& d_map);
      void GetFinalAccumulatedFMOMap (Array1D<ETElement>& eq_table);
      int  GetNetFRAArray            (IArray1D& nfra_array);
      void GetRegionBoundaryMap      (IArray2D& s_map,IArray2D& d_map);
      void GroupAdjacentRegions      (IArray2D& rb_map,IArray1D& nfra_array,Array1D<ETElement>& eq_table);
      void UnmarkFalseFMO            (VFData* vfd1,VFData* vfd2);
   
   public:
      virtual void Close     (   );
      virtual int  ReadFile  (FileIO &file);
      virtual int  ReadFile  (CXMLIO* pIO);
      virtual int  WriteFile (FileIO &file);  
      virtual int  WriteFile (CXMLIO* pIO);
   
   public:
      void Initialize    (ISize2D& si_size);
      int  IsInitialized (   );
      int  Perform       (ForegroundDetection& frg_detector);
};

 inline int FastMovingObjectDetection::IsInitialized (   )

{
   return (Flag_Initialized);
}

}

#endif

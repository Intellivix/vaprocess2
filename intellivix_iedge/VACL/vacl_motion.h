#if !defined(__VACL_MOTION_H)
#define __VACL_MOTION_H

#include "vacl_light.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: MotionDetection_FAD
//
///////////////////////////////////////////////////////////////////////////////
//
// FAD: Frame Alignment and Differencing
//

 class MotionDetection_FAD
 
{
   protected:
      struct Frame {
         GImage Image;
         Frame* Prev;
         Frame* Next;
      };

   protected:
      int          FrameCount;
      Queue<Frame> FrameQueue;
   
   // Input (Set directly)
   public:
      int   MDThreshold;   // Motion Detection Threshold
      int   NBWSize;       // Neighborhood Window Size
      int   MinMDRArea;    // Minimum MD Region Area
      int   CFRadius;      // Closing Filter Radius
      float FrameInterval; // Frame Interval in Second
      float MaxShift;      // Maximum Shift
      
   // Input (Set indirectly)
   public:
      float FrameRate;
   
   public:
      MotionDetection_FAD (   );
      virtual ~MotionDetection_FAD (   );
      
   protected:
      void AddToFrameQueue (GImage& s_image);
   
   public:
      virtual void Close (   );
   
   public:
      int Initialize (GImage& s_image,float frm_rate);
      int Perform    (GImage& s_image,GImage& d_image);
};

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

void GetFrameDifference     (GImage& s_image1,GImage& s_image2,int md_thrsld,GImage& d_image);
void GetFrameDifference     (GImage& s_image1,GImage& s_image2,int md_thrsld,int nbw_size,GImage& d_image);
void RemoveBoundaryMDPixels (FPoint2D& shift,GImage& d_image);

}

#endif

#if !defined(__VACL_CLCHANGE_H)
#define __VACL_CLCHANGE_H

#include "vacl_objtrack.h"

 namespace VACL
 
{

class EventZone;
class EventDetection;

///////////////////////////////////////////////////////////////////////////////
//
// Class: ColorChangeDetection
//
///////////////////////////////////////////////////////////////////////////////

 class ColorChangeDetection : public BlobTracking
 
{
   protected:
      IArray2D VotingScoreMap;
   
   public:
      int   CFRadius;       // Closing Filter Radius
      int   MDThreshold;    // Motion Detection Threshold
      int   Flag_RgnColor;  // 본 값이 FALSE이면 추적 중인 블럽의 바운딩 박스 중심의 컬러 값만을 계산한다.
                            // (TrackedBlob.CntBkgColor,TrackedBlob.CntFrgColor)
                            // 본 값이 TRUE이면 추적 중인 블럽의 영역 픽셀들의 평균 컬러 값을 아울러 계산한다.
                            // (TrackedBlob.RgnBkgColor,TrackedBlob.RgnFrgColor)
      float MinTime_Static; // 색상 변화 검출이 이루어지기 위한 검출 영역의 최소한의 정적 시간
   
   public:
      GImage ColorChangeImage;

   public:
      ColorChangeDetection (   );
      virtual ~ColorChangeDetection (   );

   protected:
      void GetExcludedRegionMap (ObjectTracking& obj_tracker,GImage& d_map);
      void UpdateBlobColors     (BGRImage& s_cimage,BGRImage& b_cimage);
      int  VoteForColorChange   (BGRImage& s_cimage,BGRImage& b_cimage,GImage& er_map);

   public:
      virtual void Close                   (   );
      virtual int  GetBkgModelUpdateScheme (GImage& d_image);
   
   public:
      int  Initialize (ISize2D& si_size,float frm_rate);
      int  Perform    (ObjectTracking& obj_tracker);
      void Reset      (   );
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: ColorZone
//
///////////////////////////////////////////////////////////////////////////////

 class ColorZone

{
   protected:
      int   DetFailCount;
      int   Flag_Processed;
      float DetectionTime;

   public:
      BGRColor   ZoneColor;
      EventZone* EvtZone;

   public:
      ColorZone* Prev;
      ColorZone* Next;

   public:
      ColorZone (   );

   public:
      int  DetectEvent  (   );
      void GetZoneColor (BGRImage& s_image);
};

typedef LinkedList<ColorZone> ColorZoneList;

///////////////////////////////////////////////////////////////////////////////
//
// Class: ZoneColorDetection
//
///////////////////////////////////////////////////////////////////////////////

 class ZoneColorDetection

{
   protected:
      int Flag_Enabled;

   public:
      float   FrameRate;
      ISize2D SrcImgSize;

   public:
      ColorZoneList ColorZones;

   public:
      ZoneColorDetection (   );
      virtual ~ZoneColorDetection (   );
   
   public:
      virtual void Close (   );

   public:
      void Enable     (int flag_enable);
      int  Initialize (EventDetection& evt_detector);
      int  IsEnabled  (   );
      int  Perform    (BGRImage& s_cimage);
};

 inline void ZoneColorDetection::Enable (int flag_enable)
 
{
   Flag_Enabled = flag_enable;
}

 inline int ZoneColorDetection::IsEnabled (  )
 
{
   return (Flag_Enabled);
} 

}

#endif

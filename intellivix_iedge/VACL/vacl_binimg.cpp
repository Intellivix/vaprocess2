#include "vacl_binimg.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 void Binarize (GImage& s_image,int min_pv,int max_pv,GImage& d_image)

{
   int x,y;

   for (y = 0; y < s_image.Height; y++) {
      byte* l_s_image = s_image[y];
      byte* l_d_image = d_image[y];
      for (x = 0; x < s_image.Width; x++) {
         int pv = (int)l_s_image[x];
         if (min_pv <= pv && pv <= max_pv) l_d_image[x] = PG_WHITE;
         else l_d_image[x] = PG_BLACK;
      }
   }
}

 int GetNumEntirePixels (GImage& s_image)

{
   IBox2D s_rgn(0,0,s_image.Width,s_image.Height);
   return (GetNumRegionPixels (s_image,s_rgn));
}

 int GetNumRegionPixels (GImage& s_image,IBox2D& s_rgn)

{
   int x,y;
   
   int n  = 0;
   int sx = s_rgn.X;
   int sy = s_rgn.Y;
   int ex = s_rgn.Width;
   int ey = s_rgn.Height;
   for (y = sy; y < ey; y++) {
      byte* l_s_image = s_image[y];
      for (x = sx; x < ex; x++)
         if (l_s_image[x]) n++;
   }
   return (n);
}

 void GetRegionBoundaryPixels (GImage& s_image,GImage& d_image)

{
   int x,y;

   d_image.Clear (   );
   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   for (y = 1; y < ey; y++) {
      byte* l_s_image  = s_image[y];
      byte* l_s_image1 = s_image[y - 1];
      byte* l_s_image2 = s_image[y + 1];
      byte* l_d_image  = d_image[y];
      for (x = 1; x < ex; x++) {
         if (l_s_image[x]) {
            if (!(l_s_image[x - 1] && l_s_image[x + 1] && l_s_image1[x] && l_s_image2[x])) l_d_image[x] = PG_WHITE;
         }
      }
   }
}

 int GetRegionPerimeter (GImage& s_image)

{
   int x,y;
   
   // CR 바운더리 추적 시작 위치를 찾는다.
   int sx = -1, sy = -1;
   for (y = 0; y < s_image.Height; y++) {
      byte* l_s_image = s_image[y];
      for (x = 0; x < s_image.Width; x++) {
         if (l_s_image[x]) {
            sx = x;
            sy = y;
            break;
         }
      }
      if (sx != -1) break;
   }
   if (sx == -1) return (0);
   // CR 바운더리 추적을 수행한다.
   x = sx, y = sy;
   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   int n_bps     = 0;
   int count     = 0;
   int direction = 0;
   GImage b_image(s_image.Width,s_image.Height);
   b_image.Clear (   );
   while (TRUE) {
      if (x < 0 || x > ex || y < 0 || y > ey || !s_image[y][x]) {
         direction--;
         if (direction < 0) direction = 3;
      }
      else {
         if (!b_image[y][x]) { // 이미 지나간 바운더리 픽셀은 제외
            if (x == 0 || y == 0 || x == ex || y == ey || !s_image[y][x - 1] || !s_image[y][x + 1] || !s_image[y - 1][x] || !s_image[y + 1][x]) { // 바운더리 픽셀인 경우
               b_image[y][x] = PG_WHITE;
               n_bps++;
            }
         }
         direction = (direction + 1) % 4;
      }
      switch (direction) {
         case 0:
            x++;
            break;
         case 1:
            y--;
            break;
         case 2:
            x--;
            break;
         case 3:
            y++;
            break;
         default:
            break;
      }
      if (x == sx && y == sy) {
         if (count == 2) break;
         else count++;
      }
   }
   return (n_bps);
}

 float GetRegionPixelDensity (GImage& s_image,IBox2D& s_rgn)

{
   int n_pixels  = GetNumRegionPixels (s_image,s_rgn);
   float density = (float)n_pixels / (s_image.Width * s_image.Height);
   return (density);
}


 float MeasureNoisiness (GImage& s_image)

{
   int x,y;

   int n = 0;
   int flag_on = FALSE;
   for (y = 0; y < s_image.Height; y++) {
      flag_on = FALSE;
      byte* l_s_image = s_image[y];
      for (x = 0; x < s_image.Width; x++) {
         if (flag_on) {
            if (!l_s_image[x]) {
               flag_on = FALSE;
               n++;
            }
         }
         else {
            if (l_s_image[x]) {
               flag_on = TRUE;
               n++;
            }
         }
      }
   }
   for (x = 0; x < s_image.Width; x++) {
      flag_on = FALSE;
      for (y = 0; y < s_image.Height; y++) {
         if (flag_on) {
            if (!s_image[y][x]) {
               flag_on = FALSE;
               n++;
            }
         }
         else {
            if (s_image[y][x]) {
               flag_on = TRUE;
               n++;
            }
         }
      }
   }
   float noisiness = (float)n / (s_image.Width * s_image.Height);
   return (noisiness);
}

}

#if !defined(__VACL_FOREGND2_H)
#define __VACL_FOREGND2_H

#include "vacl_foregnd.h"
#include "vacl_imgreg.h"
#include "vacl_motion.h"

#if defined(__LIB_WIN32CL)
#include "Win32CL/Win32CL.h"
#endif

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: ForegroundDetection_SGBM2
//
///////////////////////////////////////////////////////////////////////////////

 class ForegroundDetection_SGBM2 : public ForegroundDetection_SGBM

{
   protected:
      virtual void GetFrgRegionMap (   ) {   };
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: CThread_FGD_SGBM
//
///////////////////////////////////////////////////////////////////////////////
//
// FGD : Foreground Detection
// SGBM: Single Gaussian Background Model
//

#if defined(__LIB_WIN32CL)

 class CThread_FGD_SGBM : public CThread

{
   public:
      int    ThreadIdx;
      IBox2D ROI;
   
   public:
      GImage* SrcImage;
      ForegroundDetection_SGBM2 ForegroundDetector;

   public:
      CEvent Event_Start;
      CEvent Event_End;

   public:
      CThread_FGD_SGBM (   );

   protected:
      virtual void ThreadProc (   );
};

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: ForegroundDetection_SGBM_MT
//
///////////////////////////////////////////////////////////////////////////////
//
// SGBM: Single Gaussian Background Model
// MT  : Multithreaded
//

#if defined(__LIB_WIN32CL)

 class ForegroundDetection_SGBM_MT : public ForegroundDetection

{
   protected:
      Array1D<HANDLE>           EndEvents;
      Array1D<CThread_FGD_SGBM> Threads;

   public:
      int NumThreads;

   public:
      ForegroundDetection_SGBM_MT (   );
      virtual ~ForegroundDetection_SGBM_MT (   );
   
   protected:
      virtual int GetFrgImage (   );
   
   public:
      virtual void Close          (   );
      virtual int  Initialize     (ISize2D& si_size,float frm_rate);
      virtual int  UpdateBkgModel (GImage& m_image);
};

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: ForegroundDetection_FAD
//
///////////////////////////////////////////////////////////////////////////////
//
// FAD: Frame Aligning & Differencing
//

 class ForegroundDetection_FAD : public ForegroundDetection
 
{
   public:
      MotionDetection_FAD MotionDetector;
      
   public:
      ForegroundDetection_FAD (   );
      virtual ~ForegroundDetection_FAD (   );
      
   protected:
      virtual int  GetFrgImage (   );
   
   public:
      virtual void Close (   );
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: ForegroundDetection_Underwater
//
///////////////////////////////////////////////////////////////////////////////

 class ForegroundDetection_Underwater : public ForegroundDetection_SGBM

{
   public:
      ForegroundDetection_Underwater (   );
   
   public:
      virtual void GetFrgEdgeImages (GImage& fg_image);
};

}

#endif

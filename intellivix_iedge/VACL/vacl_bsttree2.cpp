#include "vacl_bsttree.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: OpenCV_BoostedTree
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_OPENCV)

 OpenCV_BoostedTree::OpenCV_BoostedTree (   )

{
   SCDThreshold = 0.0f;
   HCRThreshold = 0.0f;
}

 OpenCV_BoostedTree::~OpenCV_BoostedTree (   )

{
   Close (   );
}

 void OpenCV_BoostedTree::Close (   )

{
   clear (   );
   SCRThresholds1.Delete (   );
   SCRThresholds2.Delete (   );
}

 int OpenCV_BoostedTree::Classify (float* s_array,int length)

{
   float score = Evaluate (s_array,length);
   if (score > SCDThreshold) return (1);
   else return (-1);
}

 int OpenCV_BoostedTree::Classify_HC (float* s_array,int length,float rt_offset,float& score)

{
   score = Evaluate (s_array,length);
   if (score > (HCRThreshold + rt_offset)) return (1);
   else return (-1);
}

 int OpenCV_BoostedTree::Classify_SC1 (float* s_array,int length,float rt_offset,float& score)

{
   int i;
 
   if (!SCRThresholds1) return (0);
   FArray1D t_array;
   float* s_data = s_array;
   if (active_vars->cols != length) {
      int var_count = active_vars->cols;
      int* vidx_abs = active_vars_abs->data.i;
      t_array.Create (var_count);
      for (i = 0; i < var_count; i++)
         t_array[i] = s_array[vidx_abs[i]];
      s_data = t_array;
   }
   CvSeqReader reader;
   cvStartReadSeq (weak,&reader);
   cvSetSeqReaderPos (&reader,0);
   score = 0.0f;
   for (i = 0; i < weak->total; i++ ) {
      CvBoostTree* wtree;
      const CvDTreeNode* node;
      CV_READ_SEQ_ELEM (wtree,reader);
      node = wtree->get_root (   );
      while (node->left) {
         CvDTreeSplit* split = node->split;
         float val = s_data[split->condensed_idx];
         int dir = val <= split->ord.c ? -1 : 1;
         if (split->inversed) dir = -dir;
         node = dir < 0 ? node->left : node->right;
      }
      score += (float)node->value;
      if (score < (SCRThresholds1[i] + rt_offset)) return (-1);
   }
   return (1);
}

 int OpenCV_BoostedTree::Classify_SC2 (float* s_array,int length,float rt_offset,float& score)

{
   int i;
 
   if (!SCRThresholds2) return (0);
   FArray1D t_array;
   float* s_data = s_array;
   if (active_vars->cols != length) {
      int var_count = active_vars->cols;
      int* vidx_abs = active_vars_abs->data.i;
      t_array.Create (var_count);
      for (i = 0; i < var_count; i++)
         t_array[i] = s_array[vidx_abs[i]];
      s_data = t_array;
   }
   CvSeqReader reader;
   cvStartReadSeq (weak,&reader);
   cvSetSeqReaderPos (&reader,0);
   score = 0.0f;
   for (i = 0; i < weak->total; i++ ) {
      CvBoostTree* wtree;
      const CvDTreeNode* node;
      CV_READ_SEQ_ELEM (wtree,reader);
      node = wtree->get_root (   );
      while (node->left) {
         CvDTreeSplit* split = node->split;
         float val = s_data[split->condensed_idx];
         int dir = val <= split->ord.c ? -1 : 1;
         if (split->inversed) dir = -dir;
         node = dir < 0 ? node->left : node->right;
      }
      score += (float)node->value;
      if (score < (SCRThresholds2[i] + rt_offset)) return (-1);
   }
   return (1);
}

 float OpenCV_BoostedTree::Evaluate (float* s_array,int length)

{
   int i;
 
   FArray1D t_array;
   float* s_data = s_array;
   if (active_vars->cols != length) {
      int var_count = active_vars->cols;
      int* vidx_abs = active_vars_abs->data.i;
      t_array.Create (var_count);
      for (i = 0; i < var_count; i++)
         t_array[i] = s_array[vidx_abs[i]];
      s_data = t_array;
   }
   CvSeqReader reader;
   cvStartReadSeq (weak,&reader);
   cvSetSeqReaderPos (&reader,0);
   float sum = 0.0f;
   for (i = 0; i < weak->total; i++ ) {
      CvBoostTree* wtree;
      const CvDTreeNode* node;
      CV_READ_SEQ_ELEM (wtree,reader);
      node = wtree->get_root (   );
      while (node->left) {
         CvDTreeSplit* split = node->split;
         float val = s_data[split->condensed_idx];
         int dir = val <= split->ord.c ? -1 : 1;
         if (split->inversed) dir = -dir;
         node = dir < 0 ? node->left : node->right;
      }
      sum += (float)node->value;
   }
   return (sum);
}

 float OpenCV_BoostedTree::Evaluate (float* s_array,int length,int s_wc_idx,int e_wc_idx)

{
   CvMat m_s_array;
   cvInitMatHeader (&m_s_array,1,length,CV_32FC1,(void*)s_array,length * sizeof(float));
   return (predict (&m_s_array,NULL,NULL,cvSlice(s_wc_idx,e_wc_idx + 1),false,true));
}


 int OpenCV_BoostedTree::GetNumWCs (   )

{
   CvSeq* wcs = get_weak_predictors (   );
   if (wcs == NULL) return (0);
   else return (wcs->total);
}

 int OpenCV_BoostedTree::GetHCRThreshold (FArray2D& fv_array,FArray1D& cl_array,int n_samples)

{
   int i;
   
   HCRThreshold = 0.0f;
   if (!IsTrained (   )) return (1);
   float min_sc = MC_VLV;
   for (i = 0; i < n_samples; i++) {
      if (cl_array[i] > 0.0f) {
         float score = Evaluate (fv_array[i],fv_array.Width);
         if (score < min_sc) min_sc = score;
      }
   }
   if (min_sc == MC_VLV) return (2);
   HCRThreshold = min_sc;
   return (DONE);
}

 int OpenCV_BoostedTree::GetSCRThresholds1 (FArray2D& fv_array,FArray1D& cl_array,int n_samples)

{
   int i,j;

   logd ("   Calculating the stage rejection thresholds (1)...\n");
   int n_wcs = GetNumWCs (   );
   if (!n_wcs) return (1);
   SCRThresholds1.Create (n_wcs);
   SCRThresholds1.Clear  (   );
   FArray1D sc_array(n_samples);
   sc_array.Clear (   );
   for (i = 0; i < n_wcs; i++) {
      float min_sc = MC_VLV;
      for (j = 0; j < n_samples; j++) {
         if (cl_array[j] > 0.0f) {
            sc_array[j] += Evaluate (fv_array[j],fv_array.Width,i,i);
            if (sc_array[j] < min_sc) min_sc = sc_array[j];
         }
      }
      SCRThresholds1[i] = min_sc;
   }
   return (DONE);
}

 int OpenCV_BoostedTree::GetSCRThresholds2 (FArray2D& fv_array,FArray1D& cl_array,int n_samples)

{
   int   i,j;
   float score;
   
   logd ("   Calculating the stage rejection thresholds (2)...\n");
   int n_wcs = GetNumWCs (   );
   if (!n_wcs) return (1);
   SCRThresholds2.Create (n_wcs);
   SCRThresholds2.Clear  (   );
   BArray1D ps_array(n_samples);
   ps_array.Clear (   );
   // Pos 샘플들 중에 Strong Classifier를 통해 Positive로 분류되는 샘플들만 사용한다.
   for (i = 0; i < n_samples; i++) {
      score = Evaluate (fv_array[i],fv_array.Width);
      if (cl_array[i] > 0.0f && score > SCDThreshold) {
         ps_array[i] = TRUE;
      }
   }
   FArray1D sc_array(n_samples);
   sc_array.Clear (   );
   for (i = 0; i < n_wcs; i++) {
       // Pos 샘플들에 대해 i 번째 WC의 Prediction 값을 누적시킨다.
      for (j = 0; j < n_samples; j++)
         if (ps_array[j]) sc_array[j] += Evaluate (fv_array[j],fv_array.Width,i,i);
      // Pos 샘플들에 대해 최소 누적 스코어 값을 찾아, i 번째 스테이지에 대한 Rejection Threshold 값으로 설정한다. 
      float min_sc = MC_VLV;
      for (j = 0; j < n_samples; j++) {
         if (ps_array[j]) {
            if (sc_array[j] < min_sc) min_sc = sc_array[j];
         }
      }
      if (min_sc == MC_VLV) return (2);
      SCRThresholds2[i] = min_sc;
   }
   return (DONE);
}

 int OpenCV_BoostedTree::IsTrained (   )

{
   CvSeq* wcs = get_weak_predictors (   );
   if (wcs == NULL || !wcs->total) return (FALSE);
   else return (TRUE);
}

 int OpenCV_BoostedTree::Load (const char* base_file_name)

{
   Close (   );
   StringA file_name;
   file_name.Format ("%s.bt1",base_file_name);
   if (LoadConfig (file_name)) return (1);
   file_name.Format ("%s.bt2",base_file_name);
   if (GetFileLength (file_name) < 0) return (2);
   CvBoost::load (file_name);
   return (DONE);
}
 
 int OpenCV_BoostedTree::LoadConfig( const char* file_name )
 
 {
    BArray1D s_buffer;
    if( LoadFileToBuffer( file_name, s_buffer, TRUE ) ) return ( 1 );
    
    // TODO(yhpark) : rapidxml. need to test. (deprecated.)
    /*
        int i;
        
        BArray1D s_buffer;
        if (ReadFileIntoBuffer (file_name,s_buffer,TRUE)) return (1);
        RapidXML_Doc xml_doc;
        xml_doc.parse<0>((char*)(byte*)s_buffer);
        // Node: BoostedTree
        RapidXML_Node* node_BoostedTree = xml_doc.first_node ("BoostedTree");
        if (node_BoostedTree == NULL) return (2);
        // Node: HCRThreshold
        RapidXML_Node* node_HCRThreshold = node_BoostedTree->first_node ("HCRThreshold");
        if (node_HCRThreshold == NULL) return (3);
        HCRThreshold = (float)atof (node_HCRThreshold->value (   ));
        // Node: SCRThresholds1
        RapidXML_Node* node_SCRThresholds1 = node_BoostedTree->first_node ("SCRThresholds1");
        if (node_SCRThresholds1 == NULL) return (5);
        // Attr: NumStages
        RapidXML_Attr* attr_NumStages = node_SCRThresholds1->first_attribute ("NumStages");
        if (attr_NumStages == NULL) return (6);
        int n_stages = atoi (attr_NumStages->value (   ));
        if (n_stages) {
           SCRThresholds1.Create (n_stages);
           SCRThresholds1.Clear  (   );
           // Node: Threshold
           RapidXML_Node* node_SCRThreshold = node_SCRThresholds1->first_node ("Threshold");
           for (i = 0; i < n_stages && node_SCRThreshold != NULL; i++) {
              SCRThresholds1[i]  = (float)atof (node_SCRThreshold->value (   ));
              node_SCRThreshold = node_SCRThreshold->next_sibling ("Threshold");
           }
           if (i != n_stages) return (7);
        }
        else SCRThresholds1.Delete (   );
        // Node: SCRThresholds2
        RapidXML_Node* node_SCRThresholds2 = node_BoostedTree->first_node ("SCRThresholds2");
        if (node_SCRThresholds2 == NULL) return (8);
        // Attr: NumStages
        attr_NumStages = node_SCRThresholds2->first_attribute ("NumStages");
        if (attr_NumStages == NULL) return (9);
        n_stages = atoi (attr_NumStages->value (   ));
        if (n_stages) {
           SCRThresholds2.Create (n_stages);
           SCRThresholds2.Clear  (   );
           // Node: Threshold
           RapidXML_Node* node_SCRThreshold = node_SCRThresholds2->first_node ("Threshold");
           for (i = 0; i < n_stages && node_SCRThreshold != NULL; i++) {
              SCRThresholds2[i]  = (float)atof (node_SCRThreshold->value (   ));
              node_SCRThreshold = node_SCRThreshold->next_sibling ("Threshold");
           }
           if (i != n_stages) return (10);
        }
        else SCRThresholds2.Delete (   );
    */
    
    CXMLIO pIO( file_name, XMLIO_Read );
    CXMLNode node( &pIO );
    if( node.Start( "BoostedTree" ) ) {
       if( node.Start( "HCRThreshold" ) ) {
          node.TextValue( TYPE_ID( float ), &HCRThreshold );
       }
       if( node.Start( "SCRThresholds1" ) ) {
          node.Attribute( TYPE_ID( int ), "NumStages", &SCRThresholds1.Length );
          if( !!SCRThresholds1 ) {
             for( int i = 0; i < SCRThresholds1.Length; i++ ) {
                if( node.Start( "Threshold" ) ) {
                   node.TextValue( TYPE_ID( float ), &SCRThresholds1[i] );
                }
             }
          }
       }
       if( node.Start( "SCRThresholds2" ) ) {
          node.Attribute( TYPE_ID( int ), "NumStages", &SCRThresholds2.Length );
          if( !!SCRThresholds2 ) {
             for( int i = 0; i < SCRThresholds2.Length; i++ ) {
                if( node.Start( "Threshold" ) ) {
                   node.TextValue( TYPE_ID( float ), &SCRThresholds2[i] );
                }
             }
          }
       }
    }

    return ( DONE );
 }
 
 int OpenCV_BoostedTree::Save (const char* base_file_name)

{
   StringA file_name;
   file_name.Format ("%s.bt1",base_file_name);
   if (SaveConfig (file_name)) return (1);
   file_name.Format ("%s.bt2",base_file_name);
   CvBoost::save (file_name);
   return (DONE);
}
 
 int OpenCV_BoostedTree::SaveConfig( const char* file_name )
 
 {
    // TODO(yhpark) : rapidxml. need to test. (deprecated.)
    /*
    int i;
 
    XMLFile file;
    file.WriteHeader ("BoostedTree");
    file.IncreaseIndentLevel (   );
    file.WriteNode      ("HCRThreshold",HCRThreshold);
    file.WriteTagStart  ("SCRThresholds1");
    file.WriteAttribute ("NumStages",SCRThresholds1.Length);
    file.WriteTagEnd    (   );
    file.IncreaseIndentLevel (   );
    if (!!SCRThresholds1) {
       for (i = 0; i < SCRThresholds1.Length; i++) {
          file.WriteNode ("Threshold",SCRThresholds1[i]);
       }
    }
    file.DecreaseIndentLevel (   );
    file.WriteNodeEnd   ("SCThresholds1");
    file.WriteTagStart  ("SCRThresholds2");
    file.WriteAttribute ("NumStages",SCRThresholds2.Length);
    file.WriteTagEnd    (   );
    file.IncreaseIndentLevel (   );
    if (!!SCRThresholds2) {
       for (i = 0; i < SCRThresholds2.Length; i++) {
          file.WriteNode ("Threshold",SCRThresholds2[i]);
       }
    }
    file.DecreaseIndentLevel (   );
    file.WriteNodeEnd ("SCThresholds2");
    file.DecreaseIndentLevel (   );
    file.WriteFooter ("BoostedTree");
    if (WriteBufferIntoFile (file_name,file.GetBuffer(),file.GetLength())) return (1);
    */
    
    CXMLIO pIO( file_name, XMLIO_Write );
    CXMLNode node( &pIO );
    if( node.Start( "BoostedTree" ) ) {
       if( node.Start( "HCRThreshold" ) ) {
          node.TextValue( TYPE_ID( float ), &HCRThreshold );
       }
       if( node.Start( "SCRThresholds1" ) ) {
          node.Attribute( TYPE_ID( int ), "NumStages", &SCRThresholds1.Length );
          if( !!SCRThresholds1 ) {
             for( int i = 0; i < SCRThresholds1.Length; i++ ) {
                if( node.Start( "Threshold" ) ) {
                   node.TextValue( TYPE_ID( float ), &SCRThresholds1[i] );
                }
             }
          }
       }
       if( node.Start( "SCRThresholds2" ) ) {
          node.Attribute( TYPE_ID( int ), "NumStages", &SCRThresholds2.Length );
          if( !!SCRThresholds2 ) {
             for( int i = 0; i < SCRThresholds2.Length; i++ ) {
                if( node.Start( "Threshold" ) ) {
                   node.TextValue( TYPE_ID( float ), &SCRThresholds2[i] );
                }
             }
          }
       }
    }
 
    return ( DONE );
 }

 int OpenCV_BoostedTree::Test_Org (FArray2D& fv_array,FArray1D& cl_array,int n_samples,float rt_offset,int& n_pos,int& n_neg,int& n_tps,int& n_fps)

{
   int i;

   logd ("   Testing the classifier...\n");
   n_pos = 0, n_neg = 0;
   n_tps = 0, n_fps = 0;
   int n_wcs = GetNumWCs (   );
   if (!n_wcs) return (1);
   for (i = 0; i < n_samples; i++) {
      int result = Classify (fv_array[i],fv_array.Width);
      if (cl_array[i] > 0.0f) {
         n_pos++;
         if (result > 0) n_tps++;
      }
      if (cl_array[i] < 0.0f) {
         n_neg++;
         if (result > 0) n_fps++;
      }
   }
   float tp_rate = (float)n_tps / n_pos * 100.0f;
   float fp_rate = (float)n_fps / n_neg * 100.0f;
   logd ("   True Positive Rate  = %5.1f%% (%d/%d)\n",tp_rate,n_tps,n_pos);
   logd ("   False Positive Rate = %5.1f%% (%d/%d)\n",fp_rate,n_fps,n_neg);
   return (DONE);
}

 int OpenCV_BoostedTree::Test_SC1 (FArray2D& fv_array,FArray1D& cl_array,int n_samples,float rt_offset,int& n_pos,int& n_neg,int& n_tps,int& n_fps)

{
   int   i;
   float score;

   logd ("   Testing the classifier with the local stage rejection thresholds (1)...\n");
   n_pos = 0, n_neg = 0;
   n_tps = 0, n_fps = 0;
   int n_wcs = GetNumWCs (   );
   if (!n_wcs) return (1);
   for (i = 0; i < n_samples; i++) {
      int result = Classify_SC1 (fv_array[i],fv_array.Width,rt_offset,score);
      if (cl_array[i] > 0.0f) {
         n_pos++;
         if (result > 0) n_tps++;
      }
      if (cl_array[i] < 0.0f) {
         n_neg++;
         if (result > 0) n_fps++;
      }
   }
   float tp_rate = (float)n_tps / n_pos * 100.0f;
   float fp_rate = (float)n_fps / n_neg * 100.0f;
   logd ("   True Positive Rate  = %5.1f%% (%d/%d)\n",tp_rate,n_tps,n_pos);
   logd ("   False Positive Rate = %5.1f%% (%d/%d)\n",fp_rate,n_fps,n_neg);
   return (DONE);
}

 int OpenCV_BoostedTree::Test_SC2 (FArray2D& fv_array,FArray1D& cl_array,int n_samples,float rt_offset,int& n_pos,int& n_neg,int& n_tps,int& n_fps)

{
   int   i;
   float score;

   logd ("   Testing the classifier with the local stage rejection thresholds (2)...\n");
   n_pos = 0, n_neg = 0;
   n_tps = 0, n_fps = 0;
   int n_wcs = GetNumWCs (   );
   if (!n_wcs) return (1);
   for (i = 0; i < n_samples; i++) {
      int result = Classify_SC2 (fv_array[i],fv_array.Width,rt_offset,score);
      if (cl_array[i] > 0.0f) {
         n_pos++;
         if (result > 0) n_tps++;
      }
      if (cl_array[i] < 0.0f) {
         n_neg++;
         if (result > 0) n_fps++;
      }
   }
   float tp_rate = (float)n_tps / n_pos * 100.0f;
   float fp_rate = (float)n_fps / n_neg * 100.0f;
   logd ("   True Positive Rate  = %5.1f%% (%d/%d)\n",tp_rate,n_tps,n_pos);
   logd ("   False Positive Rate = %5.1f%% (%d/%d)\n",fp_rate,n_fps,n_neg);
   return (DONE);
}

 int OpenCV_BoostedTree::Train (FArray2D& fv_array,FArray1D& cl_array,int n_samples,int max_td,int max_wcc)

{
   logd ("   # of weak classifiers added: ");

   Close (   );

   CvMat m_fv_array;
   cvInitMatHeader (&m_fv_array,n_samples,fv_array.Width,CV_32FC1,(void*)fv_array,fv_array.Width * sizeof(float));
   CvMat m_cl_array;
   cvInitMatHeader (&m_cl_array,1,n_samples,CV_32FC1,(void*)cl_array,n_samples * sizeof(float));

   CvBoostParams _params;
   CvMat* _train_data     = &m_fv_array;
   int    _tflag          = CV_ROW_SAMPLE;
   CvMat* _responses      = &m_cl_array;
   CvMat* _var_idx        = NULL;
   CvMat* _sample_idx     = NULL;
   CvMat* _var_type       = NULL;
   CvMat* _missing_mask   = NULL;
   _params.boost_type     = CvBoost::GENTLE;
   _params.max_depth      = max_td;
   _params.weak_count     = max_wcc;

   bool _update           = false;

   int i;
   int r_code = 1;
   CvMemStorage* storage = 0;

   CV_FUNCNAME( "OpenCV_BoostedTree::Train" );

   __BEGIN__;

   set_params( _params );

   cvReleaseMat( &active_vars );
   cvReleaseMat( &active_vars_abs );

   if( !_update || !data )
   {
       clear();
       data = new CvDTreeTrainData( _train_data, _tflag, _responses, _var_idx,
           _sample_idx, _var_type, _missing_mask, _params, true, true );

       if( data->get_num_classes() != 2 )
           CV_ERROR( CV_StsNotImplemented,
           "Boosted trees can only be used for 2-class classification." );
       CV_CALL( storage = cvCreateMemStorage() );
       weak = cvCreateSeq( 0, sizeof(CvSeq), sizeof(CvBoostTree*), storage );
       storage = 0;
   }
   else
   {
       data->set_data( _train_data, _tflag, _responses, _var_idx,
           _sample_idx, _var_type, _missing_mask, _params, true, true, true );
   }

   if ( (_params.boost_type == LOGIT) || (_params.boost_type == GENTLE) )
       data->do_responses_copy();

   update_weights( 0 );

   for( i = 0; i < params.weak_count; i++ )
   {
       CvBoostTree* tree = new CvBoostTree;
       if( !tree->train( data, subsample_mask, this ) )
       {
           delete tree;
           break;
       }
       cvSeqPush( weak, &tree );
       update_weights( tree );
       trim_weights();
       if( cvCountNonZero(subsample_mask) == 0 )
           break;

       if (weak->total && !(weak->total % 5)) logd ("[%d]",weak->total);
   }
   logd ("[%d]\n",weak->total);

   if(weak->total > 0)
   {
       get_active_vars();
       data->is_classifier = true;
       data->free_train_data();
       r_code = DONE;
   }
   else
       clear();

   __END__;

   return (r_code);
}

#endif

}

#if !defined(__VACL_SCHEDULE_H)
#define __VACL_SCHEDULE_H

#include "vacl_define.h"
 
 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: Period_HM
//
///////////////////////////////////////////////////////////////////////////////

 enum PERIOD_CHECK_MODE

{
   PERIOD_CHECK_MODE_START_END_TIME = 0,
   PERIOD_CHECK_MODE_DAY_TIME       = 1,
   PERIOD_CHECK_MODE_NIGHT_TIME     = 2
};

 class Period_HM

{
   protected:
      int TimeInMinute[2];

   public:
      int CheckMode;
      int Hour[2];
      int Minute[2];

   public:
      Period_HM (   );
      virtual ~Period_HM (   );

   protected:
      int ConvertTimeInMinute (int hour,int minute);
   
   public:
      int  IsTimeInsidePeriod (int hour,int minute);
      int  IsSame             (Period_HM& period_hm);
      int  ReadFile           (FileIO &file);
      int  ReadFile           (CXMLIO* pIO, const char* szNodeName = NULL);
      void Set                (int n,int hour,int minute);
      int  WriteFile          (FileIO &file);
      int  WriteFile          (CXMLIO* pIO, const char* szNodeName = NULL);

   public:
      Period_HM *Prev,*Next;
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: Period_YMD
//
///////////////////////////////////////////////////////////////////////////////

 class Period_YMD

{
   protected:
      int TimeInDay[2];
   
   public:
      int Year[2];
      int Month[2];
      int Day[2];

   public:
      Period_YMD (   );
      virtual ~Period_YMD (   );

   protected:
      int ConvertTimeInDay (int year,int month,int day);
   
   public:
      int  IsTimeInsidePeriod (int year,int month,int day);
      int  ReadFile           (FileIO &file);
      int  ReadFile           (CXMLIO* pIO, const char* szNodeName = NULL);
      void Set                (int n,int year,int month,int day);
      int  WriteFile          (FileIO &file);
      int  WriteFile          (CXMLIO* pIO, const char* szNodeName = NULL);

   public:
      Period_YMD *Prev,*Next;
};

///////////////////////////////////////////////////////////////////////////////
//
// Global Variables
//
///////////////////////////////////////////////////////////////////////////////

extern Period_HM Period_SunRiseSet;
 
}
 
#endif


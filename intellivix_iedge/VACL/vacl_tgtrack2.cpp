#include "vacl_event.h"
#include "vacl_filter.h"
#include "vacl_ipp.h"
#include "vacl_morph.h"
#include "vacl_tgtrack.h"
#include "vacl_warping.h"

#if defined(__DEBUG_FGD) || defined(__DEBUG_TGT)
#include "Win32CL/Win32CL.h"
#endif

#if defined(__DEBUG_FGD)
extern int _DVX_FGD,_DVY_FGD;
extern CImageView* _DebugView_FGD;
#endif

#if defined(__DEBUG_TGT)
extern int _DVX_TGT,_DVY_TGT;
extern CImageView* _DebugView_TGT;
#endif

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: TargetTracking_MD_NCC
//
///////////////////////////////////////////////////////////////////////////////

 TargetTracking_MD_NCC::TargetTracking_MD_NCC (   )
 
{
   MinMatchingScore     = 0.6f;
   ModelUpdateRate      = 0.1f;
   VelocityUpdateRate   = 0.1f;

   MatchingRange        = 9;
   MinTgtSizeSimilarity = 0.4f;
}

 void TargetTracking_MD_NCC::Close (   )
 
{
   TargetTracking::Close (   );
   MotionDetector.Close  (   );
   FrgImage.Delete       (   );
   TargetMask.Delete     (   );
   TargetModel.Delete    (   );
}

 void TargetTracking_MD_NCC::AdjustMatchingFrgRegion (IBox2D& m_rgn)

{
   float a = ModelUpdateRate; // PARAMETERS
   float b = 1.0f - a;
   m_rgn.X      = (int)(a * m_rgn.X      + b * MatchingPos.X    );
   m_rgn.Y      = (int)(a * m_rgn.Y      + b * MatchingPos.Y    );
   m_rgn.Width  = (int)(a * m_rgn.Width  + b * TargetMask.Width );
   m_rgn.Height = (int)(a * m_rgn.Height + b * TargetMask.Height);
   CropRegion (SrcImgSize,m_rgn);
}

 int TargetTracking_MD_NCC::GetMatchingFrgRegion (IArray2D& fr_map,int n_frs,IBox2D& s_rgn,IBox2D& d_m_rgn,int& d_mr_no)
 
{
   int x,y;
   
   d_m_rgn.Clear (   );
   d_mr_no = 0;
   if (!n_frs) return (-1);
   int sx = s_rgn.X;
   int sy = s_rgn.Y;
   int ex = sx + s_rgn.Width;
   int ey = sy + s_rgn.Height;
   CropRegion (fr_map.Width,fr_map.Height,sx,sy,ex,ey);
   IArray1D n_array(n_frs + 1);
   n_array.Clear (   );
   for (y = sy; y < ey; y++) {
      int *l_fr_map = fr_map[y];
      for (x = sx; x < ex; x++)
         if (l_fr_map[x]) n_array[l_fr_map[x]]++;
   }
   int max_i,max_n;
   FindMaximum (n_array,max_n,max_i);
   float ar = (float)max_n / (s_rgn.Width * s_rgn.Height);
   if (max_n < 25 || ar < 0.1f) return (-1); // PARAMETERS
   sx = fr_map.Width;
   sy = fr_map.Height;
   ex = ey = -1;
   int   n = 0;
   int   sum_x  = 0,sum_y  = 0;
   int64 sum_xx = 0,sum_yy = 0;
   for (y = 0; y < fr_map.Height; y++) {
      int *l_fr_map = fr_map[y];
      for (x = 0; x < fr_map.Width; x++) {
         if (l_fr_map[x] == max_i) {
            if (x < sx) sx = x;
            if (y < sy) sy = y;
            if (x > ex) ex = x;
            if (y > ey) ey = y;
            sum_x  += x;
            sum_y  += y;
            sum_xx += x * x;
            sum_yy += y * y;
            n++;
         }
      }
   }
   const double a = 1.9; // PARAMETERS
   double mx = (double)sum_x / n;
   double my = (double)sum_y / n;
   double hw = a * sqrt (sum_xx / n - mx * mx);
   double hh = a * sqrt (sum_yy / n - my * my);
   int sx2 = (int)(mx - hw + 0.5);
   int sy2 = (int)(my - hh + 0.5);
   int ex2 = (int)(mx + hw + 0.5);
   int ey2 = (int)(my + hh + 0.5);
   if (sx < sx2) sx = sx2;
   if (sy < sy2) sy = sy2;
   if (ex > ex2) ex = ex2;
   if (ey > ey2) ey = ey2;
   d_mr_no = max_i;
   d_m_rgn(sx,sy,ex - sx + 1,ey - sy + 1);
   CropRegion (SrcImgSize,d_m_rgn);
   FPoint2D cp_mr = d_m_rgn.GetCenterPosition (   );
   FPoint2D cp_sr = s_rgn.GetCenterPosition   (   );
   if (d_m_rgn.IsPointInsideBox (cp_sr) || s_rgn.IsPointInsideBox (cp_mr)) {
      if (GetSizeRatio (d_m_rgn) >= MinTgtSizeSimilarity) return (DONE);
   }
   return (1);
}

 float TargetTracking_MD_NCC::GetNCC (GImage& s_image,int ox,int oy,int dx,int dy)
 
{
   int x1,y1;

   int sx = ox;
   int sy = oy;
   int ex = sx + TargetModel.Width;
   int ey = sy + TargetModel.Height;
   if (sx < 0) sx = 0;
   if (sy < 0) sy = 0;
   if (ex > s_image.Width ) ex = s_image.Width;
   if (ey > s_image.Height) ey = s_image.Height;
   uint  sum_w   = 0;
   int64 sum_wx  = 0, sum_wy  = 0;
   int64 sum_wxx = 0, sum_wyy = 0, sum_wxy = 0;
   for (y1 = sy; y1 < ey; y1 += dy) {
      int y2 = y1 - oy;
      byte *l_s_image = s_image[y1];
      byte *l_r_model = TargetModel[y2];
      byte *l_w_image = TargetMask[y2];
      for (x1 = sx; x1 < ex; x1 += dx) {
         int x2  = x1 - ox;
         uint w  = l_w_image[x2];
         uint wx = w * l_s_image[x1];
         uint wy = w * l_r_model[x2];
         sum_w   += w;
         sum_wx  += wx;
         sum_wy  += wy;
         sum_wxx += wx * l_s_image[x1];
         sum_wyy += wy * l_r_model[x2];
         sum_wxy += wx * l_r_model[x2];
      }
   }
   if (!sum_w) return (0.0f);
   else {
      double mx  = (double)sum_wx  / sum_w;
      double my  = (double)sum_wy  / sum_w;
      double mxx = (double)sum_wxx / sum_w;
      double myy = (double)sum_wyy / sum_w;
      double mxy = (double)sum_wxy / sum_w;
      double ncc = (mxy - mx * my) / sqrt ((mxx - mx * mx) * (myy - my * my));
      return ((float)ncc);
   }
}

 void TargetTracking_MD_NCC::GetPreprocessedImage (GImage& s_image,GImage& d_image)

{
   GaussianFiltering_3x3 (s_image,d_image);
}

 float TargetTracking_MD_NCC::GetSizeRatio (IBox2D& s_rgn)
 
{
   float rw = GetMinimum ((float)TargetRegion.Width  / s_rgn.Width ,(float)s_rgn.Width  / TargetRegion.Width);
   float rh = GetMinimum ((float)TargetRegion.Height / s_rgn.Height,(float)s_rgn.Height / TargetRegion.Height);
   return (GetMinimum (rw,rh));
}

 int TargetTracking_MD_NCC::GetTargetArea (   )

{
   int x,y;

   int n = 0;
   for (y = 0; y < TargetMask.Height; y++) {
      byte* l_t_mask = TargetMask[y];
      for (x = 0; x < TargetMask.Width; x++) {
         if (l_t_mask[x] >= 128) n++;
      }
   }
   return (n);
}

 void TargetTracking_MD_NCC::Initialize (GImage& s_image,IBox2D& tg_rgn,float frm_rate,GImage* m_image)
 
{
   ISize2D si_size(s_image.Width,s_image.Height);
   TargetTracking::Initialize (si_size,tg_rgn,frm_rate);

   MatchingPos(TargetRegion.X,TargetRegion.Y);
   TargetVelocity(MC_VLV,MC_VLV);

   GImage ps_image(s_image.Width,s_image.Height);
   GetPreprocessedImage (s_image,ps_image);
   TargetModel = ps_image.Extract (TargetRegion);
   TargetMask.Create (TargetRegion.Width,TargetRegion.Height);
   if (m_image == NULL) TargetMask.Set (0,0,TargetRegion.Width,TargetRegion.Height,PG_WHITE);
   else TargetMask.Copy (*m_image,TargetRegion.X,TargetRegion.Y,TargetRegion.Width,TargetRegion.Height,0,0);

   FrgImage.Create (SrcImgSize.Width,SrcImgSize.Height);
   FrgImage.Clear  (   );

   MotionDetector.Initialize (ps_image,FrameRate);
}

 float TargetTracking_MD_NCC::Perform (GImage &s_image)
 
{
   #if defined(__DEBUG_FGD)
   _DVX_FGD = _DVY_FGD = 0;
   _DebugView_FGD->Clear (   );
   #endif
   #if defined(__DEBUG_TGT)
   _DVX_TGT = _DVY_TGT = 0;
   _DebugView_TGT->Clear (   );
   #endif
   #if defined(__DEBUG_TGT)
   logd ("[TargetTracking_MD_NCC]\n");
   #endif
   FrameCount++;
   Status             = 0;
   MatchingScore      = 0.0f;
   Flag_SetTargetSize = FALSE;
   if (!IsInitialized (   )) {
      Status |= TT_STATUS_NOT_INITIALIZED;
      return (0.0f);
   }
   MatchingPos(TargetRegion.X,TargetRegion.Y);
  if (CheckTargetOutOfRange (   )) {
      FrgImage.Clear (   );
      #if defined(__DEBUG_TGT)
      logd ("Target out of range.\n");
      #endif
      return (0.0f);
   }
   GImage ps_image(s_image.Width,s_image.Height);
   GetPreprocessedImage (s_image,ps_image);
   MotionDetector.Perform (ps_image,FrgImage);
   #if defined(__DEBUG_TGT)
   _DebugView_TGT->DrawImage (ps_image,_DVX_TGT,_DVY_TGT);
   #endif
   int    mr_no = 0;
   IBox2D m_rgn;
   IArray2D fr_map(FrgImage.Width,FrgImage.Height);
   int n_frs = LabelCRs (FrgImage,fr_map);
   IPoint2D p_pos = GetPredictedPosition (   );
   MatchingScore  = PerformMatching (ps_image,p_pos,MatchingRange,MatchingPos);
   IBox2D s_rgn(p_pos.X,p_pos.Y,TargetRegion.Width,TargetRegion.Height);
   // s_rgn 내에서 가장 큰 면적을 차지하는 FR을 찾는다.
   int r_code = GetMatchingFrgRegion (fr_map,n_frs,s_rgn,m_rgn,mr_no);
   switch (r_code) {
      case 0: {
         // 찾은 FR이 타겟과 크기 유사도가 높은 경우
         Status |= TT_STATUS_EXACT_FR_MATCH;
         #if defined(__DEBUG_TGT)
         logd ("Exact FR match.\n");
         #endif
      } break;
      case 1: {
         // 찾은 FR이 타겟과 크기 유사도가 낮은 경우
         AdjustMatchingFrgRegion (m_rgn);
         Status |= TT_STATUS_ROUGH_FR_MATCH;
         #if defined(__DEBUG_TGT)
         logd ("Rough FR match.\n");
         #endif
      } break;
      default: {
         // FR이 없는 경우
         Status |= TT_STATUS_NO_FR_MATCH;
         #if defined(__DEBUG_TGT)
         logd ("No FR match.\n");
         #endif
      } break;
   }
   #if defined(__DEBUG_TGT)
   logd ("Matching Position: X = %d, Y = %d\n",MatchingPos.X,MatchingPos.Y);
   logd ("Matching Score   : %.2f\n",MatchingScore);
   _DebugView_TGT->DrawRectangle (_DVX_TGT + p_pos.X      ,_DVY_TGT + p_pos.Y      ,TargetRegion.Width,TargetRegion.Height,PC_BLUE,2);
   _DebugView_TGT->DrawRectangle (_DVX_TGT + MatchingPos.X,_DVY_TGT + MatchingPos.Y,TargetRegion.Width,TargetRegion.Height,PC_GREEN,2);
   #endif
   if (MatchingScore < MinMatchingScore || !mr_no) {
      Status |= TT_STATUS_TARGET_LOST;
      TargetLostTime += FrameDuration;
      #if defined(__DEBUG_TGT)
      logd ("Target lost. (TargetLostTime: %.2f)\n",TargetLostTime);
      #endif
   }
   else {
      TargetLostTime -= FrameDuration;
      if (TargetLostTime < 0.0f) TargetLostTime = 0.0f;
   }
   if (mr_no) {
      UpdateFrgImageAndRgnMap (fr_map,m_rgn,mr_no);
      UpdateTargetModel       (ps_image,fr_map,m_rgn,mr_no);
   }
   else UpdateTargetModel (ps_image);
   UpdateTargetRegion (   );
   #if defined(__DEBUG_TGT)
   _DebugView_TGT->DrawRectangle (_DVX_TGT + TargetRegion.X,_DVY_TGT + TargetRegion.Y,TargetRegion.Width,TargetRegion.Height,PC_RED,2);
   _DVY_TGT += s_image.Height;
   _DebugView_TGT->DrawText (_T("Target Tracking Result"),_DVX_TGT,_DVY_TGT,PC_GREEN);
   _DVY_TGT += 20;
   _DebugView_TGT->DrawImage (FrgImage,_DVX_TGT,_DVY_TGT);
   _DVY_TGT += FrgImage.Height;
   _DebugView_TGT->DrawText (_T("Foreground Image"),_DVX_TGT,_DVY_TGT,PC_GREEN);
   _DVY_TGT += 30;
   _DebugView_TGT->DrawImage (TargetModel,_DVX_TGT,_DVY_TGT);
   _DVX_TGT += TargetModel.Width + 10;
   _DebugView_TGT->DrawImage (TargetMask,_DVX_TGT,_DVY_TGT);
   _DVX_TGT  = 0;
   _DVY_TGT += TargetMask.Height + 10;
   _DebugView_TGT->DrawText (_T("Target Model"),_DVX_TGT,_DVY_TGT,PC_GREEN);
   _DVY_TGT += 20;
   #endif
   #if defined(__DEBUG_FGD)
   _DebugView_FGD->Invalidate (   );
   #endif
   #if defined(__DEBUG_TGT)
   _DebugView_TGT->Invalidate (   );
   #endif
   return (MatchingScore);
}

 float TargetTracking_MD_NCC::PerformMatching (GImage &s_image,IPoint2D &s_pos,int m_range,IPoint2D &m_pos)

{
   int x,y;

   int b_size = s_image.Height / 5; // PARAMETERS
   int dx = TargetModel.Width  / b_size + 1;
   int dy = TargetModel.Height / b_size + 1;
   int mr = (int)(0.5f * (dx + dy) * m_range);
   if (mr > m_range) mr = (int)(m_range + 0.4f * (mr - m_range)); // PARAMETERS
   int sx = s_pos.X - mr;
   int sy = s_pos.Y - mr;
   int ex = s_pos.X + mr;
   int ey = s_pos.Y + mr;
   float m_score = 0.0f;
   m_pos = s_pos;
   for (y = sy; y <= ey; y++) {
      for (x = sx; x <= ex; x++) {
         float score = GetNCC (s_image,x,y,dx,dy);
         if (score > m_score) {
            m_score = score;
            m_pos(x,y);
         }
      }
   }
   return (m_score);
}

 void TargetTracking_MD_NCC::SetTargetSize (ISize2D &size)
 
{
   TargetTracking::SetTargetSize (size);
   GImage t_image(size.Width,size.Height);
   Resize_2 (TargetModel,t_image);
   TargetModel = t_image;
   GImage w_image(size.Width,size.Height);
   Resize_2 (TargetMask,w_image);
   TargetMask = w_image;
}

 void TargetTracking_MD_NCC::UpdateFrgImageAndRgnMap (IArray2D& fr_map,IBox2D& m_rgn,int mr_no)
 
{
   int x,y;
   
   GImage t_image1(m_rgn.Width,m_rgn.Height);
   int sx = m_rgn.X;
   int sy = m_rgn.Y;
   int ex = m_rgn.X + m_rgn.Width;
   int ey = m_rgn.Y + m_rgn.Height;
   for (y = sy; y < ey; y++) {
      int*  l_fr_map   = fr_map[y];
      byte* l_t_image1 = t_image1[y - sy];
      for (x = sx; x < ex; x++) {
         if (l_fr_map[x] == mr_no) l_t_image1[x - sx] = PG_WHITE;
         else l_t_image1[x - sx] = PG_BLACK;
      }
   }
   GImage t_image2(t_image1.Width,t_image1.Height);
   BinHRLFiltering (t_image1,t_image1.Width,t_image2);
   BinVRLFiltering (t_image2,t_image1.Height / 2,t_image1);
   for (y = sy; y < ey; y++) {
      int*  l_fr_map   = fr_map[y];
      byte* l_fg_image = FrgImage[y];
      byte* l_t_image1 = t_image1[y - sy];
      for (x = sx; x < ex; x++) {
         if (l_t_image1[x - sx]) {
            l_fr_map[x]   = mr_no;
            l_fg_image[x] = PG_WHITE;
         }
      }
   }
}

 void TargetTracking_MD_NCC::UpdateTargetModel (GImage& s_image,IArray2D& fr_map,IBox2D& m_rgn,int mr_no)
 
{
   int x1,y1,x2,y2;

   int sx1 = MatchingPos.X;
   int sy1 = MatchingPos.Y;
   int ex1 = sx1 + TargetModel.Width;
   int ey1 = sy1 + TargetModel.Height;
   int sx2 = m_rgn.X;
   int sy2 = m_rgn.Y;
   int ex2 = sx2 + m_rgn.Width;
   int ey2 = sy2 + m_rgn.Height;
   if (sx1 < sx2) sx2 = sx1;
   if (sy1 < sy2) sy2 = sy1;
   if (ex1 > ex2) ex2 = ex1;
   if (ey1 > ey2) ey2 = ey1;
   GImage t_image(ex2 - sx2,ey2 - sy2);
   t_image.Clear (   );
   GImage w_image(t_image.Width,t_image.Height);
   w_image.Clear (   );
   int dx = sx1 - sx2;
   int dy = sy1 - sy2;
   t_image.Copy (TargetModel,0,0,TargetModel.Width,TargetModel.Height,dx,dy);
   w_image.Copy (TargetMask,0,0,TargetMask.Width,TargetMask.Height,dx,dy);
   int sx3 = sx2, sy3 = sy2, ex3 = ex2, ey3 = ey2;
   CropRegion (SrcImgSize.Width,SrcImgSize.Height,sx3,sy3,ex3,ey3);
   int sx4 = sx3 - sx2;
   int sy4 = sy3 - sy2;
   float a = ModelUpdateRate;
   float b = 1.0f - a;
   float c = a * PG_WHITE;
   byte  w0 = (byte)c;
   for (y1 = sy3,y2 = sy4; y1 < ey3; y1++,y2++) {
      byte* l_s_image = s_image[y1];
      byte* l_t_image = t_image[y2];
      byte* l_w_image = w_image[y2];
      int*  l_fr_map  = fr_map[y1];
      for (x1 = sx3,x2 = sx4; x1 < ex3; x1++,x2++) {
         if (l_w_image[x2]) {
            l_t_image[x2] = (byte)(b * l_t_image[x2] + a * l_s_image[x1]);
            if (l_fr_map[x1] == mr_no) l_w_image[x2] = (byte)(b * l_w_image[x2] + c);
            else l_w_image[x2] = (byte)(b * l_w_image[x2]);
         }
         else {
            l_t_image[x2] = l_s_image[x1];
            if (l_fr_map[x1] == mr_no) l_w_image[x2] = w0;
         }
      }
   }
   if (!w0) w0 = 1;
   uint  sum_w = 0;
   uint  mx  = 0, my  = 0;
   int64 sdx = 0, sdy = 0;
   for (y1 = 0; y1 < w_image.Height; y1++) {
      byte *l_w_image = w_image[y1];
      for (x1 = 0; x1 < w_image.Width; x1++) {
         byte w = l_w_image[x1];
         if (w) {
            uint wx1 = w * x1;
            uint wy1 = w * y1;
            sum_w += w;
            mx    += wx1;
            my    += wy1;
            sdx   += wx1 * x1;
            sdy   += wy1 * y1;
         }
      }
   }
   float mx2  = (float)mx  / sum_w;
   float my2  = (float)my  / sum_w;
   float sdx2 = (float)sdx / sum_w;
   float sdy2 = (float)sdy / sum_w;
   sdx2 = sqrt (sdx2 - mx2 * mx2);
   sdy2 = sqrt (sdy2 - my2 * my2);
   if (sdx2 < 1.0f) sdx2 = 1.0f;
   if (sdy2 < 1.0f) sdy2 = 1.0f;
   float sigma = 1.9f; // PARAMETERS
   float rx    = sigma * sdx2;
   float ry    = sigma * sdy2;
   sx1 = (int)(mx2 - rx);
   sy1 = (int)(my2 - ry);
   ex1 = (int)(mx2 + rx);
   ey1 = (int)(my2 + ry);
   CropRegion (t_image.Width,t_image.Height,sx1,sy1,ex1,ey1);
   int width   = ex1 - sx1;
   int height  = ey1 - sy1;
   TargetModel = t_image.Extract (sx1,sy1,width,height);
   TargetMask  = w_image.Extract (sx1,sy1,width,height);
   MatchingPos.X = sx1 + sx2;
   MatchingPos.Y = sy1 + sy2;
}

 void TargetTracking_MD_NCC::UpdateTargetModel (GImage& s_image)
 
{
   int x1,y1,x2,y2;

   int sx1 = MatchingPos.X;
   int sy1 = MatchingPos.Y;
   int ex1 = sx1 + TargetModel.Width;
   int ey1 = sy1 + TargetModel.Height;
   int sx2 = sx1, sy2 = sy1;
   CropRegion (SrcImgSize.Width,SrcImgSize.Height,sx1,sy1,ex1,ey1);
   sx2 = sx1 - sx2;
   sy2 = sy1 - sy2;
   float a = ModelUpdateRate; // PARAMETERS
   float b = 1.0f - a;
   for (y1 = sy1,y2 = sy2; y1 < ey1; y1++,y2++) {
      byte *l_s_image = s_image[y1];
      byte *l_t_image = TargetModel[y2];
      for (x1 = sx1,x2 = sx2; x1 < ex1; x1++,x2++)
         l_t_image[x2] = (byte)(a * l_s_image[x1] + b * l_t_image[x2]);
   }
}

 void TargetTracking_MD_NCC::UpdateTargetRegion (   )
 
{
   FPoint2D r_pos,t_pos;
   r_pos.X = (float)(TargetRegion.X + TargetRegion.Width  / 2);
   r_pos.Y = (float)(TargetRegion.Y + TargetRegion.Height / 2);
   t_pos.X = (float)(MatchingPos.X  + TargetModel.Width   / 2);
   t_pos.Y = (float)(MatchingPos.Y  + TargetModel.Height  / 2);
   if (VelocityUpdateRate) {
      if (TargetVelocity.X == MC_VLV) TargetVelocity = t_pos - r_pos;
      else TargetVelocity = (1.0f - VelocityUpdateRate) * TargetVelocity + VelocityUpdateRate * (t_pos - r_pos);
   }
   else TargetVelocity(0.0f,0.0f);
   TargetRegion.X      = MatchingPos.X;
   TargetRegion.Y      = MatchingPos.Y;
   TargetRegion.Width  = TargetModel.Width;
   TargetRegion.Height = TargetModel.Height;
}

}

#include "vacl_camera.h"
#include "vacl_filter.h"
#include "vacl_geometry.h"
#include "vacl_imgreg.h"
#include "vacl_panorama.h"
#include "vacl_profile.h"

 namespace VACL

{

#define IDENTIFIER_PICT   "PVFCT"
#define IDENTIFIER_CPT    "CPT"

///////////////////////////////////////////////////////////////////////////////
//
// Class: CylindricalProjection
//
///////////////////////////////////////////////////////////////////////////////

 CylindricalProjection::CylindricalProjection (   )
 
{
   Flag_Closed  = FALSE;
   SrcImgWidth  = 0;
   SrcImgHeight = 0;
   PanImage     = NULL;
   Mats_K       = NULL;
   Mats_R       = NULL;

   OverlapWidth = PAN_OVERLAP_WIDTH;
   PIWidthScale = PAN_WIDTH_SCALE;
   Kappa        = 0.0;

   HFOV         = 0.0f;
   VFOV         = 0.0f;
}

 void CylindricalProjection::Close (   )

{
   SrcDistMap.Delete (   );
   PanDistMap.Delete (   );
   Mat_H.Delete      (   );
}

 void CylindricalProjection::ClosePanorama (BGRImage &s_cimage,float l_pos,float r_pos)

{
   HFOV = 360.0f;
   Flag_Closed = TRUE;
   SrcDistMap.Delete (   );
   Array1D<Matrix> &mats_K = *Mats_K;
   // 마지막 소스 영상이 실린더에 맵핑된 영역을 알아낸 후, 그 영역을 PanImage로부터 얻는다.
   IPoint2D p1,p2,p3;
   GetSrcImageMappingRange (mats_K.Length - 1,p1,p2);
   BGRImage l_cimage = PanImage->Extract (p1.X,p1.Y,p2.X - p1.X + 1,p2.Y - p1.Y + 1);
   GImage   l_image(l_cimage.Width,l_cimage.Height);
   Convert (l_cimage,l_image);
   // 첫 번째 소스 영상이 실린더에 맵핑된 영역을 알아낸 후, 그 영역을 PanImage로부터 얻는다.
   IPoint2D sp1,ep1;
   GetSrcImageMappingRange (0,sp1,ep1);
   BGRImage t_cimage = PanImage->Extract (sp1.X,sp1.Y,ep1.X - sp1.X + 1,ep1.Y - sp1.Y + 1);
   FArray2D td_map   = PanDistMap.Extract (sp1.X,sp1.Y,t_cimage.Width,t_cimage.Height);
   GImage t_image(t_cimage.Width,t_cimage.Height);
   Convert (t_cimage,t_image);
   PanDistMap.Delete (   );
   // 마지막 소스 영상과 첫번째 소스 영상을 템플릿 매칭한다.
   HTM_NCC2 htm;
   FPoint2D lt_overlap(l_pos + 1.0f,0.8f);
   FPoint2D rb_overlap(r_pos,0.8f);
   htm.Initialize (l_image,t_image,170,lt_overlap,rb_overlap); // PARAMETERS
   htm.Perform (2.0f,10); // PARAMETERS
   FPoint2D m_pos = htm.GetMatchingPosition (   );
   LFMPos(p1.X + (int)m_pos.X,p1.Y + (int)m_pos.Y);
   l_image.Delete (   );
   t_image.Delete (   );
   // 마지막 소스 영상에서 불필요한 부분을 잘라낸다.
   int width = (int)(m_pos.X + t_cimage.Width / 2);
   if (width > l_cimage.Width) width = l_cimage.Width;
   BGRImage r_cimage = l_cimage.Extract (0,0,width,l_cimage.Height);
   l_cimage.Delete (   );
   FArray2D rd_map(r_cimage.Width,r_cimage.Height);
   GetDistanceMapForBlending (rd_map);
   // 마지막 소스 영상과 첫번째 소스 영상을 합친다.
   Array1D<Matrix> mats_H(2);
   mats_H[0] = IM(3);
   mats_H[1] = IM(3);
   mats_H[1][0][2] = m_pos.X;
   mats_H[1][1][2] = m_pos.Y;
   IS2DArray1D img_sizes(2);
   img_sizes[0](r_cimage.Width,r_cimage.Height);
   img_sizes[1](t_cimage.Width,t_cimage.Height);
   DArray1D rd_coeffs(2);
   BGRPixel black(0,0,0);
   r_cimage.SetMask (black);
   t_cimage.SetMask (black);
   BGRImage d_cimage;
   PlanarProjection p_prj;
   p_prj.Initialize (mats_H,img_sizes,rd_coeffs,d_cimage);
   p_prj.Perform (r_cimage,rd_map,0);
   p_prj.Perform (t_cimage,td_map,1);
   p_prj.GetSrcImageMappingRange (0,p2,p3);
   p_prj.Close  (   );
   r_cimage.Delete (   );
   rd_map.Delete (   );
   td_map.Delete (   );
   // 파노라마 영상과 위에서 구한 영상을 합친다.
   IPoint2D sp2,ep2;
   sp2.X = (int)m_pos.X + p1.X;
   sp2.Y = (int)m_pos.Y + p1.Y;
   ep2.X = sp2.X + t_cimage.Width  - 1;
   ep2.Y = sp2.Y + t_cimage.Height - 1;
   t_cimage.Delete (   );
   BGRImage p_cimage(p1.X + d_cimage.Width,PanImage->Height);
   p_cimage.Copy (*PanImage,0,0,PanImage->Width,PanImage->Height,0,0);
   PanImage->Delete (   );
   p3 = p1 - p2;
   FPoint2D p4((float)(p3.X + d_cimage.Width - 1),(float)(p3.Y + d_cimage.Height - 1));
   FPoint2D p5,p6;
   p5.X = (float)p3.X;
   if (p3.Y < 0) p5.Y = 0.0f;
   else p5.Y = (float)p3.Y;
   p6.X = (float)p4.X;
   if (p4.Y >= p_cimage.Height) p6.Y = (float)(p_cimage.Height - 1);
   else p6.Y = (float)p4.Y;
   IBox2D c_box;
   c_box.X = (int)(p5.X - p3.X);
   c_box.Y = (int)(p5.Y - p3.Y);
   c_box.Width  = (int)(p6.X - p5.X + 1);
   c_box.Height = (int)(p6.Y - p5.Y + 1);
   p_cimage.Copy  (d_cimage,c_box.X,c_box.Y,c_box.Width,c_box.Height,(int)p5.X,(int)p5.Y);
   d_cimage.Delete (   );
   // 파노라마 영상의 양 끝을 맞춘다.
   FP2DArray1D m_points1(4),m_points2(4);
   m_points1[0].X = (float)((sp1.X + ep1.X) / 2);
   m_points1[0].Y = (float)sp1.Y;
   m_points1[1].X = (float)((sp2.X + ep2.X) / 2);
   m_points1[1].Y = (float)sp2.Y;
   m_points1[2].X = m_points1[1].X;
   m_points1[2].Y = (float)ep2.Y;
   m_points1[3].X = m_points1[0].X;
   m_points1[3].Y = (float)ep1.Y;
   m_points2[0]   = m_points1[0];
   m_points2[3]   = m_points1[3];
   width = (int)m_points1[1].X - (int)m_points1[0].X + 1;
   m_points2[1].X = m_points1[0].X + (width / 24 * 24) - 1;
   m_points2[1].Y = m_points2[0].Y;
   m_points2[2].X = m_points2[1].X;
   m_points2[2].Y = m_points2[3].Y;
   Mat_H.Create (3,3);
   GetHomography (m_points1,m_points2,Mat_H);
   d_cimage.Create (p_cimage.Width,p_cimage.Height);
   Warp (p_cimage,Mat_H,d_cimage,FALSE);
   p_cimage.Delete (   );
   // 파노라마 영상의 불필요한 부분을 잘라낸다.
   ValidRegion.X      = (int)m_points2[0].X;
   ValidRegion.Y      = 0;
   ValidRegion.Width  = (int)(m_points2[2].X - m_points2[0].X) + 1;
   ValidRegion.Height = d_cimage.Height;
   *PanImage = d_cimage.Extract (ValidRegion);
}

 void CylindricalProjection::CropNonimageArea (   )

{
   int sx,sy,ex,ey;

   float a1 = (MaxPos.X - MinPos.X) / (PanImage->Width - 1);
   float b1 = MinPos.X;
   float a2 = (MinPos.Y - MaxPos.Y) / (PanImage->Height - 1);
   float b2 = MaxPos.Y;
   if (Flag_Closed) {
      BGRImage t_image;
      CropNonimageArea_Horizontal (*PanImage,t_image,sy,ey);
      ValidRegion.Y      = sy;
      ValidRegion.Height = ey - sy + 1;
      *PanImage = t_image;
   }
   else {
      BGRImage t_image;
      CropNonimageArea_Vertical2 (*PanImage,t_image,sx,ex);
      PanImage->Delete (   );
      CropNonimageArea_Horizontal (t_image,*PanImage,sy,ey);
      ValidRegion.X = sx;
      ValidRegion.Y = sy;
      ValidRegion.Width  = ex - sx + 1;
      ValidRegion.Height = ey - sy + 1;
      float theta1 = a1 * sx + b1;
      float theta2 = a1 * ex + b1;
      HFOV = (theta2 - theta1) * MC_RAD2DEG;
   }
   float y1 = a2 * ey + b2;
   float y2 = a2 * sy + b2;
   VFOV = (atan (y2) - atan (y1)) * MC_RAD2DEG;
   ValidRegion.Y++;      
   ValidRegion.Height--;
}

 void CylindricalProjection::GetPanImageMappingRange (   )

{
   int i;
    
   MinPos.X =  MC_VLV;
   MinPos.Y =  MC_VLV;
   MaxPos.X = -MC_VLV;
   MaxPos.Y = -MC_VLV;
   FPoint2D p1,p2;
   for (i = 0; i < Mats_K->Length; i++) {
      GetSrcImageMappingRange (i,p1,p2);
      if (p1.X < MinPos.X) MinPos.X = p1.X;
      if (p2.X > MaxPos.X) MaxPos.X = p2.X;
      if (p1.Y < MinPos.Y) MinPos.Y = p1.Y;
      if (p2.Y > MaxPos.Y) MaxPos.Y = p2.Y;
      #if defined(__DEBUG_PAN)
      logd ("Source Image #%03d: p1.X = %7.2f, p1.Y =  %7.2f, p2.X = %7.2f, p2.Y = %7.2f\n",i,p1.X * MC_RAD2DEG,p1.Y,p2.X * MC_RAD2DEG,p2.Y);
      #endif
   }
   HFOV = (MaxPos.X - MinPos.X) * MC_RAD2DEG;
   VFOV = (atan (MaxPos.Y) - atan (MinPos.Y)) * MC_RAD2DEG;
}

 ISize2D CylindricalProjection::GetPanImageSize (   )

{
   FPoint2D p1,p2;
   int ci = Mats_K->Length / 2;
   GetSrcImageMappingRange (ci,p1,p2);
   float a = PIWidthScale * SrcImgWidth / (p2.X - p1.X);
   float b = SrcImgHeight / (p2.Y - p1.Y);
   GetPanImageMappingRange (   );
   ISize2D size((int)((MaxPos.X - MinPos.X) * a + 0.5f) + 1,(int)((MaxPos.Y - MinPos.Y) * b + 0.5f) + 1);
   #if defined(__DEBUG_PAN)
   logd ("Panoramic Image Size: Width = %d, Height = %d\n",size.Width,size.Height);
   #endif
   return (size);
}

 void CylindricalProjection::GetSrcImageMappingRange (int img_no,FPoint2D &min_pos,FPoint2D &max_pos)

{
   int x,y;
   int flag_add_2pi;
   FPoint2D sp,cp;
   FPoint2D p1,p2;

   min_pos.X =  MC_VLV;
   min_pos.Y =  MC_VLV;
   max_pos.X = -MC_VLV;
   max_pos.Y = -MC_VLV;
   float ex = (float)(SrcImgWidth  - 1);
   float ey = (float)(SrcImgHeight - 1);
   Matrix mat_T = Inv((*Mats_K)[img_no] * (*Mats_R)[img_no]);
   // 영상의 중심점에 대한 실린더 투영 위치를 구한다.
   sp(0.5f * SrcImgWidth,0.5f * SrcImgHeight);
   PlaneToCylinder (sp,mat_T,cp,FALSE);
   if (-0.1f * MC_PI_2 <= cp.X && cp.X < MC_PI_2) flag_add_2pi = FALSE;
   else flag_add_2pi = TRUE;
   #if defined(__DEBUG_PAN)
   logd ("img_no = %d, flag_add_2pi = %d\n",img_no,flag_add_2pi);
   #endif
   for (x = 0; x < SrcImgWidth; x++) {
     sp((float)x,0.0f);
      PlaneToCylinder (sp,mat_T,p1,flag_add_2pi);
      sp((float)x,ey);
      PlaneToCylinder (sp,mat_T,p2,flag_add_2pi);
      if (p1.X > p2.X) Swap (p1.X,p2.X);
      if (p1.Y > p2.Y) Swap (p1.Y,p2.Y);
      if (p1.X < min_pos.X) min_pos.X = p1.X;
      if (p2.X > max_pos.X) max_pos.X = p2.X;
      if (p1.Y < min_pos.Y) min_pos.Y = p1.Y;
      if (p2.Y > max_pos.Y) max_pos.Y = p2.Y;
   }
   for (y = 0; y < SrcImgHeight; y++) {
     sp(0.0f,(float)y);
      PlaneToCylinder (sp,mat_T,p1,flag_add_2pi);
      sp(ex  ,(float)y);
      PlaneToCylinder (sp,mat_T,p2,flag_add_2pi);
      if (p1.X > p2.X) Swap (p1.X,p2.X);
      if (p1.Y > p2.Y) Swap (p1.Y,p2.Y);
      if (p1.X < min_pos.X) min_pos.X = p1.X;
      if (p2.X > max_pos.X) max_pos.X = p2.X;
      if (p1.Y < min_pos.Y) min_pos.Y = p1.Y;
      if (p2.Y > max_pos.Y) max_pos.Y = p2.Y;
   }
}

 void CylindricalProjection::GetSrcImageMappingRange (int img_no,IPoint2D &min_pos,IPoint2D &max_pos)

{
    float a = (MaxPos.X - MinPos.X) / (PanImage->Width  - 1);
    float b = (MaxPos.Y - MinPos.Y) / (PanImage->Height - 1);
    FPoint2D min_p,max_p;
    GetSrcImageMappingRange (img_no,min_p,max_p);
    min_pos.X = (int)((min_p.X - MinPos.X) / a + 0.5f);
    min_pos.Y = (int)((min_p.Y - MinPos.Y) / b + 0.5f);
    max_pos.X = (int)((max_p.X - MinPos.X) / a + 0.5f);
    max_pos.Y = (int)((max_p.Y - MinPos.Y) / b + 0.5f);
}
 
 void CylindricalProjection::Initialize (Array1D<Matrix> &mats_K,Array1D<Matrix> &mats_R,int si_width,int si_height,BGRImage &p_image)

{
   Flag_Closed  = FALSE;
   Mats_K       = &mats_K;
   Mats_R       = &mats_R;
   SrcImgWidth  = si_width;
   SrcImgHeight = si_height;
   PanImage     = &p_image;
   SrcImgCenter(0.5f * SrcImgWidth,0.5f * SrcImgHeight);
   ISize2D size = GetPanImageSize (   );
   PanImage->Create  (size.Width,size.Height);
   PanImage->Clear   (   );
   PanDistMap.Create (size.Width,size.Height);
   SrcDistMap.Create (SrcImgWidth,SrcImgHeight);
   GetDistanceMapForBlending (SrcDistMap);
   PanDistMap.Set (0,0,PanDistMap.Width,PanDistMap.Height,-MC_VLV);
}

 void CylindricalProjection::Perform (BGRImage &s_image,int img_no)

{
   int    x,y,ix1,iy1;
   byte   **mask;
   double a1 = 0.0,a2 = 0.0,a3 = 0.0,a4 = 0.0, a5 = 0.0;
    
   if (Kappa != 0.0) {
      a1 = Kappa * Kappa;
      a2 = 1.0 / (27.0 * Kappa * a1);
      a3 = 1.0 / (2.0 * a1);
      a4 = 1.0 / (729.0 * a1 * a1 * a1);
      a5 = 2.0 / (3.0 * Kappa);
   }
   int sex = s_image.Width  - 1;
   int sey = s_image.Height - 1;
   IPoint2D min_p,max_p;
   GetSrcImageMappingRange (img_no,min_p,max_p);
   float a = (MaxPos.X - MinPos.X) / (PanImage->Width  - 1);
   float b = (MaxPos.Y - MinPos.Y) / (PanImage->Height - 1);
   Matrix vec_x(3);
   Matrix mat_T = (*Mats_K)[img_no] * (*Mats_R)[img_no];
   if (s_image.AlphaChannel == NULL) mask = NULL;
   else mask = *s_image.AlphaChannel;
   for (y = min_p.Y; y <= max_p.Y; y++) {
      float v = b * y + MinPos.Y;
      vec_x(1) = v / sqrt (1.0 + v * v);
      double r = sqrt (1.0 - vec_x(1) * vec_x(1));
      float *l_PanDistMap  = PanDistMap[y];
      BGRPixel *l_PanImage = (*PanImage)[y];
      for (x = min_p.X; x <= max_p.X; x++) {
         double theta = a * x + MinPos.X;
         vec_x(0) = r * sin (theta);
         vec_x(2) = r * cos (theta);
         Matrix vec_p = mat_T * vec_x;
         float fx = (float)(vec_p(0) / vec_p(2));
         float fy = (float)(vec_p(1) / vec_p(2));
         if (Kappa != 0.0) {
            double xu = fx - SrcImgCenter.X;
            double yu = fy - SrcImgCenter.Y;
            double sru = xu * xu + yu * yu;
            double c1  = a2 + sru * a3;
            double c2  = pow (sqrt (c1 * c1 - a4) + c1,1.0 / 3.0);
            double srd = c2 + 1.0 / (9.0 * c2 * a1) - a5;
            double c3  = 1.0 + Kappa * srd;
            fx = (float)(xu / c3 + SrcImgCenter.X);
            fy = (float)(yu / c3 + SrcImgCenter.Y);
         }
         int ix = (int)fx;
         int iy = (int)fy;
         if (fx >= 0.0f && fy >= 0.0f && ix <= sex && iy <= sey) {
            if (ix == sex) ix1 = ix;
            else ix1 = ix + 1;
            if (iy == sey) iy1 = iy;
            else iy1 = iy + 1;
            if ((mask == NULL) || (mask != NULL && mask[iy][ix] && mask[iy1][ix] && mask[iy][ix1] && mask[iy1][ix1])) {
               float d = l_PanDistMap[x] - SrcDistMap[iy][ix];
               if ((-OverlapWidth <= d && d <= OverlapWidth) || d < 0.0f) {
                  float rx2 = fx - ix;
                  float ry2 = fy - iy;
                  float rx1 = 1.0f - rx2;
                  float ry1 = 1.0f - ry2;
                  float b1  = ry1 * s_image[iy][ix ].R + ry2 * s_image[iy1][ix ].R;
                  float b2  = ry1 * s_image[iy][ix1].R + ry2 * s_image[iy1][ix1].R;
                  float pr  = rx1 * b1 + rx2 * b2;
                        b1  = ry1 * s_image[iy][ix ].G + ry2 * s_image[iy1][ix ].G;
                        b2  = ry1 * s_image[iy][ix1].G + ry2 * s_image[iy1][ix1].G;
                  float pg  = rx1 * b1 + rx2 * b2;
                        b1  = ry1 * s_image[iy][ix ].B + ry2 * s_image[iy1][ix ].B;
                        b2  = ry1 * s_image[iy][ix1].B + ry2 * s_image[iy1][ix1].B;
                  float pb  = rx1 * b1 + rx2 * b2;
                  BGRPixel &p = l_PanImage[x];
                  if (-OverlapWidth <= d && d <= OverlapWidth) {
                     float w1 = 0.5f * (d / OverlapWidth + 1.0f);
                     float w2 = 1.0f - w1;
                     p.R = (byte)(w1 * p.R + w2 * pr);
                     p.G = (byte)(w1 * p.G + w2 * pg);
                     p.B = (byte)(w1 * p.B + w2 * pb);
                     if (d < 0.0f) l_PanDistMap[x] = SrcDistMap[iy][ix];
                  }
                  else {
                     p.R  = (byte)pr;
                     p.G  = (byte)pg;
                     p.B  = (byte)pb;
                     l_PanDistMap[x] = SrcDistMap[iy][ix];
                  }
                  if (!(p.R || p.G || p.B)) p.R = p.G = p.B = 1;
               }
            }
         }
      }
   }
}

 void CylindricalProjection::PlaneToCylinder (FPoint2D &s_point,Matrix &mat_T,FPoint2D &d_point,int flag_add_2pi)

{
   FPoint2D pu = GetUndistortedPixelPosition (s_point,SrcImgCenter,Kappa);
   Matrix vec_p = mat_T * GetHomogeneousVector (pu);
   d_point.X = (float)atan2 (vec_p(0),vec_p(2));
   d_point.Y = (float)(vec_p(1) / sqrt (vec_p(0) * vec_p(0) + vec_p(2) * vec_p(2)));
   if (d_point.X < 0.0f && flag_add_2pi) d_point.X += MC_2_PI;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: PlanarProjection
//
///////////////////////////////////////////////////////////////////////////////

 PlanarProjection::PlanarProjection (   )
 
{
   PanImage = NULL;
   Mats_K   = NULL;
   Mats_R   = NULL;
   
   OverlapWidth = PAN_OVERLAP_WIDTH;
}

 void PlanarProjection::Close (   )

{
   RDCoeffs.Delete    (   );
   SrcDistMap.Delete  (   );
   PanDistMap.Delete  (   );
   SrcImgSizes.Delete (   );
   Mats_H.Delete      (   );
}

 void PlanarProjection::CropNonimageArea (   )

{
   int sx,sy,ex,ey;
   
   BGRImage t_image;
   CropNonimageArea_Vertical1  (*PanImage,t_image,sx,ex);
   PanImage->Delete (   );
   CropNonimageArea_Horizontal (t_image,*PanImage,sy,ey);
}

 void PlanarProjection::GetHomographies (   )

{
   int i;
   
   double fl = 0.0;
   Array1D<Matrix> &mats_K = *Mats_K;
   Array1D<Matrix> &mats_R = *Mats_R;
   for (i = 0; i < mats_K.Length; i++) fl += mats_K[i][0][0];
   fl /= mats_K.Length;
   Matrix mat_K(3,3);
   mat_K[0][0] = mat_K[1][1] = fl;
   mat_K[2][2] = 1.0;
   Mats_H.Create (mats_K.Length);
   for (i = 0; i < Mats_H.Length; i++)
      Mats_H[i] = mat_K * Inv(mats_K[i] * mats_R[i]);
}

 void PlanarProjection::GetPanImageMappingRange (   )

{
   int i;
   
   MinPos(MC_VLV,MC_VLV);
   MaxPos(-MC_VLV,-MC_VLV);
   FPoint2D min_p,max_p;
   for (i = 0; i < Mats_H.Length; i++) {
      GetSrcImageMappingRange (i,min_p,max_p);
      if (min_p.X < MinPos.X) MinPos.X = min_p.X;
      if (min_p.Y < MinPos.Y) MinPos.Y = min_p.Y;
      if (max_p.X > MaxPos.X) MaxPos.X = max_p.X;
      if (max_p.Y > MaxPos.Y) MaxPos.Y = max_p.Y;
   }
}

 ISize2D PlanarProjection::GetPanImageSize (   )

{
   GetPanImageMappingRange (   );
   ISize2D size((int)(MaxPos.X - MinPos.X + 0.5f) + 1,(int)(MaxPos.Y - MinPos.Y + 0.5f) + 1);
   return (size);
}

 void PlanarProjection::GetSrcImageMappingRange (int img_no,FPoint2D &min_pos,FPoint2D &max_pos)

{
   int i;
   
   float ex = (float)(SrcImgSizes[img_no].Width  - 1);
   float ey = (float)(SrcImgSizes[img_no].Height - 1);
   FP2DArray1D s_points(4);
   s_points[0](0.0f,0.0f);
   s_points[1](ex,0.0f);
   s_points[2](ex,ey);
   s_points[3](0.0f,ey);
   FP2DArray1D d_points(4);
   FPoint2D pc(0.5f * SrcImgSizes[img_no].Width,0.5f * SrcImgSizes[img_no].Height);
   for (i = 0; i < s_points.Length; i++) {
      FPoint2D pu = GetUndistortedPixelPosition (s_points[i],pc,RDCoeffs[img_no]);
      GetInhomogeneousCoordinates (Mats_H[img_no] * GetHomogeneousVector (pu),d_points[i]);
   }
   IPoint2D min_i,max_i;
   FindMinMax (d_points,min_pos,max_pos,min_i,max_i);
}

 void PlanarProjection::GetSrcImageMappingRange (int img_no,IPoint2D &min_pos,IPoint2D &max_pos)

{
   FPoint2D min_p,max_p;
   GetSrcImageMappingRange (img_no,min_p,max_p);
   min_pos.X = (int)(min_p.X - MinPos.X);
   min_pos.Y = (int)(min_p.Y - MinPos.Y);
   max_pos.X = (int)(max_p.X - MinPos.X + 0.5f);
   max_pos.Y = (int)(max_p.Y - MinPos.Y + 0.5f);
}

 void PlanarProjection::Initialize (BGRImage &p_image)

{
   PanImage = &p_image;
   ISize2D size = GetPanImageSize (   );
   PanImage->Create  (size.Width,size.Height);
   PanImage->Clear   (   );
   PanDistMap.Create (size.Width,size.Height);
   PanDistMap.Set    (0,0,PanDistMap.Width,PanDistMap.Height,-MC_VLV);
}

 void PlanarProjection::Initialize (Array1D<Matrix> &mats_H,int si_width,int si_height,BGRImage &p_image,double kappa)

{
   Mats_H = mats_H;
   SrcImgSizes.Create (mats_H.Length);
   SrcImgSizes.Set    (0,SrcImgSizes.Length,ISize2D(si_width,si_height));
   RDCoeffs.Create    (mats_H.Length);
   RDCoeffs.Set       (0,RDCoeffs.Length,kappa);
   SrcDistMap.Create  (si_width,si_height);
   GetDistanceMapForBlending (SrcDistMap);
   Initialize (p_image);
}

 void PlanarProjection::Initialize (Array1D<Matrix> &mats_H,IS2DArray1D &si_sizes,DArray1D &rd_coeffs,BGRImage &p_image)

{
   Mats_H      = mats_H;
   SrcImgSizes = si_sizes;
   RDCoeffs    = rd_coeffs;
   Initialize (p_image);
}

 void PlanarProjection::Initialize (Array1D<Matrix> &mats_K,Array1D<Matrix> &mats_R,int si_width,int si_height,BGRImage &p_image,double kappa)

{
   Mats_K = &mats_K;
   Mats_R = &mats_R;
   SrcImgSizes.Create (mats_K.Length);
   SrcImgSizes.Set (0,SrcImgSizes.Length,ISize2D(si_width,si_height));
   RDCoeffs.Create (mats_K.Length);
   RDCoeffs.Set    (0,RDCoeffs.Length,kappa);
   GetHomographies (   );
   SrcDistMap.Create (si_width,si_height);
   GetDistanceMapForBlending (SrcDistMap);
   Initialize (p_image);
}

 void PlanarProjection::Perform (BGRImage &s_image,int img_no)

{
   Perform (s_image,SrcDistMap,img_no);
}

 void PlanarProjection::Perform (BGRImage &s_image,FArray2D &sd_map,int img_no)

{
   int    x,y,ix1,iy1;
   byte   **mask;
   double a1,a2,a3,a4,a5;

   int sex = s_image.Width  - 1;
   int sey = s_image.Height - 1;
   double kappa = RDCoeffs[img_no];
   if (kappa != 0.0) {
      a1 = kappa * kappa;
      a2 = 1.0 / (27.0 * kappa * a1);
      a3 = 1.0 / (2.0 * a1);
      a4 = 1.0 / (729.0 * a1 * a1 * a1);
      a5 = 2.0 / (3.0 * kappa);
   }
   IPoint2D min_p,max_p;
   GetSrcImageMappingRange (img_no,min_p,max_p);
   Matrix vec_x(3);
   Matrix mat_T = Inv(Mats_H[img_no]);
   vec_x(2) = 1.0;
   FPoint2D pc(0.5f * s_image.Width,0.5f * s_image.Height);
   if (s_image.AlphaChannel == NULL) mask = NULL;
   else mask = *s_image.AlphaChannel;
   for (y = min_p.Y; y <= max_p.Y; y++) {
      vec_x(1) = y + MinPos.Y;
      float *l_PanDistMap  = PanDistMap[y];
      BGRPixel *l_PanImage = (*PanImage)[y];
      for (x = min_p.X; x <= max_p.X; x++) {
         vec_x(0) = x + MinPos.X;
         Matrix vec_p = mat_T * vec_x;
         float fx = (float)(vec_p(0) / vec_p(2));
         float fy = (float)(vec_p(1) / vec_p(2));
         if (kappa != 0.0) {
            double xu = fx - pc.X;
            double yu = fy - pc.Y;
            double sru = xu * xu + yu * yu;
            double c1  = a2 + sru * a3;
            double c2  = pow (sqrt (c1 * c1 - a4) + c1,1.0 / 3.0);
            double srd = c2 + 1.0 / (9.0 * c2 * a1) - a5;
            double c3  = 1.0 + kappa * srd;
            fx = (float)(xu / c3 + pc.X);
            fy = (float)(yu / c3 + pc.Y);
         }
         int ix = (int)fx;
         int iy = (int)fy;
         if (fx >= 0.0f && fy >= 0.0f && ix <= sex && iy <= sey) {
            if (ix == sex) ix1 = ix;
            else ix1 = ix + 1;
            if (iy == sey) iy1 = iy;
            else iy1 = iy + 1;
            if ((mask == NULL) || (mask != NULL && mask[iy][ix] && mask[iy1][ix] && mask[iy][ix1] && mask[iy1][ix1])) {
               float d = l_PanDistMap[x] - sd_map[iy][ix];
               if ((-OverlapWidth <= d && d <= OverlapWidth) || d < 0.0f) {
                  float rx2 = fx - ix;
                  float ry2 = fy - iy;
                  float rx1 = 1.0f - rx2;
                  float ry1 = 1.0f - ry2;
                  float b1  = ry1 * s_image[iy][ix ].R + ry2 * s_image[iy1][ix ].R;
                  float b2  = ry1 * s_image[iy][ix1].R + ry2 * s_image[iy1][ix1].R;
                  float pr  = rx1 * b1 + rx2 * b2;
                        b1  = ry1 * s_image[iy][ix ].G + ry2 * s_image[iy1][ix ].G;
                        b2  = ry1 * s_image[iy][ix1].G + ry2 * s_image[iy1][ix1].G;
                  float pg  = rx1 * b1 + rx2 * b2;
                        b1  = ry1 * s_image[iy][ix ].B + ry2 * s_image[iy1][ix ].B;
                        b2  = ry1 * s_image[iy][ix1].B + ry2 * s_image[iy1][ix1].B;
                  float pb  = rx1 * b1 + rx2 * b2;
                  BGRPixel &p = l_PanImage[x];
                  if (-OverlapWidth <= d && d <= OverlapWidth) {
                     float w1 = 0.5f * (d / OverlapWidth + 1.0f);
                     float w2 = 1.0f - w1;
                     p.R = (byte)(w1 * p.R + w2 * pr);
                     p.G = (byte)(w1 * p.G + w2 * pg);
                     p.B = (byte)(w1 * p.B + w2 * pb);
                     if (d < 0.0f) l_PanDistMap[x] = sd_map[iy][ix];
                  }
                  else {
                     p.R = (byte)pr;
                     p.G = (byte)pg;
                     p.B = (byte)pb;
                     l_PanDistMap[x] = sd_map[iy][ix];
                  }
                  if (!(p.R || p.G || p.B)) p.R = p.G = p.B = 1;
               }
            }
         }
      }
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: SphericalProjection
//
///////////////////////////////////////////////////////////////////////////////

 SphericalProjection::SphericalProjection (   )
 
{
   SrcImgWidth  = 0;
   SrcImgHeight = 0;
   PanImage     = NULL;
   Mats_K       = NULL;
   Mats_R       = NULL;
   
   OverlapWidth = PAN_OVERLAP_WIDTH;
   PIWidthScale = PAN_WIDTH_SCALE;
   Kappa        = 0.0;
}

 void SphericalProjection::Close (   )

{
   SrcDistMap.Delete (   );
   PanDistMap.Delete (   );
}

 void SphericalProjection::CropNonimageArea (   )

{
   int sx,sy,ex,ey;

   BGRImage t_image;
   CropNonimageArea_Vertical1 (*PanImage,t_image,sx,ex);
   PanImage->Delete (   );
   CropNonimageArea_Horizontal (t_image,*PanImage,sy,ey);
}

 void SphericalProjection::GetPanImageMappingRange (   )

{
   int i;
   
   MinPos.X =  MC_VLV;
   MinPos.Y =  MC_VLV;
   MaxPos.X = -MC_VLV;
   MaxPos.Y = -MC_VLV;
   FPoint2D p1,p2;
   for (i = 0; i < Mats_K->Length; i++) {
      GetSrcImageMappingRange (i,p1,p2);
      if (p1.X < MinPos.X) MinPos.X = p1.X;
      if (p2.X > MaxPos.X) MaxPos.X = p2.X;
      if (p1.Y < MinPos.Y) MinPos.Y = p1.Y;
      if (p2.Y > MaxPos.Y) MaxPos.Y = p2.Y;
   }
}

 ISize2D SphericalProjection::GetPanImageSize (   )

{
   FPoint2D p1,p2;
   int ci = Mats_K->Length / 2;
   GetSrcImageMappingRange (ci,p1,p2);
   float a = PIWidthScale * SrcImgWidth / (p2.X - p1.X);
   float b = SrcImgHeight / (p2.Y - p1.Y);
   GetPanImageMappingRange (   );
   ISize2D size((int)((MaxPos.X - MinPos.X) * a + 0.5f) + 1,(int)((MaxPos.Y - MinPos.Y) * b + 0.5f) + 1);
   return (size);
}

 void SphericalProjection::GetSrcImageMappingRange (int img_no,FPoint2D &min_pos,FPoint2D &max_pos)

{
   int x,y,sign;
   FPoint2D sp,p1,p2;

   min_pos.X =  MC_VLV;
   min_pos.Y =  MC_VLV;
   max_pos.X = -MC_VLV;
   max_pos.Y = -MC_VLV;
   float ex = (float)(SrcImgWidth  - 1);
   float ey = (float)(SrcImgHeight - 1);
   if (Mats_K->Length <= 2) sign = 0;
   else {
      #if defined(__RANGE1)
      if      (img_no < (int)(0.25f * Mats_K->Length + 0.5f)) sign = -1;
      else if (img_no > (int)(0.75f * Mats_K->Length + 0.5f)) sign = +1;
      else sign = 0;
      #elif defined(__RANGE2)
      if      (img_no <= (int)(0.33f * Mats_K->Length)) sign = -1;
      else if (img_no >= (int)(0.66f * Mats_K->Length)) sign = +1;
      else sign = 0;
      #elif defined(__RANGE3)
      if (img_no <= 1) sign = -1;
      else sign = 0;
      #else
      sign = 0;
      #endif
   }
   Matrix mat_T = Inv((*Mats_K)[img_no] * (*Mats_R)[img_no]);
   for (x = 0; x < SrcImgWidth; x++) {
     sp((float)x,0.0f);
      PlaneToSphere (sp,mat_T,sign,p1);
      sp((float)x,ey  );
      PlaneToSphere (sp,mat_T,sign,p2);
      if (p1.X > p2.X) Swap (p1.X,p2.X);
      if (p1.Y > p2.Y) Swap (p1.Y,p2.Y);
      if (p1.X < min_pos.X) min_pos.X = p1.X;
      if (p2.X > max_pos.X) max_pos.X = p2.X;
      if (p1.Y < min_pos.Y) min_pos.Y = p1.Y;
      if (p2.Y > max_pos.Y) max_pos.Y = p2.Y;
   }
   for (y = 0; y < SrcImgHeight; y++) {
      sp(0.0f,(float)y);
      PlaneToSphere (sp,mat_T,sign,p1);
      sp(ex  ,(float)y);
      PlaneToSphere (sp,mat_T,sign,p2);
      if (p1.X > p2.X) Swap (p1.X,p2.X);
      if (p1.Y > p2.Y) Swap (p1.Y,p2.Y);
      if (p1.X < min_pos.X) min_pos.X = p1.X;
      if (p2.X > max_pos.X) max_pos.X = p2.X;
      if (p1.Y < min_pos.Y) min_pos.Y = p1.Y;
      if (p2.Y > max_pos.Y) max_pos.Y = p2.Y;
   }
}

 void SphericalProjection::GetSrcImageMappingRange (int img_no,IPoint2D &min_pos,IPoint2D &max_pos)

{
   float a = (MaxPos.X - MinPos.X) / (PanImage->Width  - 1);
   float b = (MaxPos.Y - MinPos.Y) / (PanImage->Height - 1);
   FPoint2D min_p,max_p;
   GetSrcImageMappingRange (img_no,min_p,max_p);
   min_pos.X = (int)((min_p.X - MinPos.X) / a + 0.5f);
   min_pos.Y = (int)((min_p.Y - MinPos.Y) / b + 0.5f);
   max_pos.X = (int)((max_p.X - MinPos.X) / a + 0.5f);
   max_pos.Y = (int)((max_p.Y - MinPos.Y) / b + 0.5f);
}

 void SphericalProjection::Initialize (Array1D<Matrix> &mats_K,Array1D<Matrix> &mats_R,int si_width,int si_height,BGRImage &p_image)

{
   Mats_K       = &mats_K;
   Mats_R       = &mats_R;
   SrcImgWidth  = si_width;
   SrcImgHeight = si_height;
   PanImage     = &p_image;
   SrcImgCenter(0.5f * SrcImgWidth,0.5f * SrcImgHeight);
   ISize2D size = GetPanImageSize (   );
   PanImage->Create  (size.Width,size.Height);
   PanImage->Clear   (   );
   PanDistMap.Create (size.Width,size.Height);
   SrcDistMap.Create (SrcImgWidth,SrcImgHeight);
   GetDistanceMapForBlending (SrcDistMap);
   PanDistMap.Set (0,0,PanDistMap.Width,PanDistMap.Height,-MC_VLV);
}

 void SphericalProjection::Perform (BGRImage &s_image,int img_no)

{
   int    x,y,ix1,iy1;
   byte   **mask;
   double a1 = 0.0,a2 = 0.0,a3 = 0.0,a4 = 0.0,a5 = 0.0;
   
   if (Kappa != 0.0) {
      a1 = Kappa * Kappa;
      a2 = 1.0 / (27.0 * Kappa * a1);
      a3 = 1.0 / (2.0 * a1);
      a4 = 1.0 / (729.0 * a1 * a1 * a1);
      a5 = 2.0 / (3.0 * Kappa);
   }
   int sex = s_image.Width  - 1;
   int sey = s_image.Height - 1;
   IPoint2D min_p,max_p;
   GetSrcImageMappingRange (img_no,min_p,max_p);
   float a = (MaxPos.X - MinPos.X) / (PanImage->Width  - 1);
   float b = (MaxPos.Y - MinPos.Y) / (PanImage->Height - 1);
   Matrix vec_x(3);
   Matrix mat_T = (*Mats_K)[img_no] * (*Mats_R)[img_no];
   if (s_image.AlphaChannel == NULL) mask = NULL;
   else mask = *s_image.AlphaChannel;
   for (y = min_p.Y; y <= max_p.Y; y++) {
      double phi = b * y + MinPos.Y;
      double tan_phi = tan (phi);
      vec_x(1) = tan_phi / sqrt (1.0 + tan_phi * tan_phi);
      double r = sqrt (1.0 - vec_x(1) * vec_x(1));
      float *l_PanDistMap  = PanDistMap[y];
      BGRPixel *l_PanImage = (*PanImage)[y];
      for (x = min_p.X; x <= max_p.X; x++) {
         double theta = a * x + MinPos.X;
         vec_x(0) = r * sin (theta);
         vec_x(2) = r * cos (theta);
         Matrix vec_p = mat_T * vec_x;
         float fx = (float)(vec_p(0) / vec_p(2));
         float fy = (float)(vec_p(1) / vec_p(2));
         if (Kappa != 0.0) {
            double xu = fx - SrcImgCenter.X;
            double yu = fy - SrcImgCenter.Y;
            double sru = xu * xu + yu * yu;
            double c1  = a2 + sru * a3;
            double c2  = pow (sqrt (c1 * c1 - a4) + c1,1.0 / 3.0);
            double srd = c2 + 1.0 / (9.0 * c2 * a1) - a5;
            double c3  = 1.0 + Kappa * srd;
            fx = (float)(xu / c3 + SrcImgCenter.X);
            fy = (float)(yu / c3 + SrcImgCenter.Y);
         }
         int ix = (int)fx;
         int iy = (int)fy;
         if (fx >= 0.0f && fy >= 0.0f && ix <= sex && iy <= sey) {
            if (ix == sex) ix1 = ix;
            else ix1 = ix + 1;
            if (iy == sey) iy1 = iy;
            else iy1 = iy + 1;
            if ((mask == NULL) || (mask != NULL && mask[iy][ix] && mask[iy1][ix] && mask[iy][ix1] && mask[iy1][ix1])) {
               float d = l_PanDistMap[x] - SrcDistMap[iy][ix];
               if ((-OverlapWidth <= d && d <= OverlapWidth) || d < 0.0f) {
                  float rx2 = fx - ix;
                  float ry2 = fy - iy;
                  float rx1 = 1.0f - rx2;
                  float ry1 = 1.0f - ry2;
                  float b1  = ry1 * s_image[iy][ix ].R + ry2 * s_image[iy1][ix ].R;
                  float b2  = ry1 * s_image[iy][ix1].R + ry2 * s_image[iy1][ix1].R;
                  float pr  = rx1 * b1 + rx2 * b2;
                        b1  = ry1 * s_image[iy][ix ].G + ry2 * s_image[iy1][ix ].G;
                        b2  = ry1 * s_image[iy][ix1].G + ry2 * s_image[iy1][ix1].G;
                  float pg  = rx1 * b1 + rx2 * b2;
                        b1  = ry1 * s_image[iy][ix ].B + ry2 * s_image[iy1][ix ].B;
                        b2  = ry1 * s_image[iy][ix1].B + ry2 * s_image[iy1][ix1].B;
                  float pb  = rx1 * b1 + rx2 * b2;
                  BGRPixel &p = l_PanImage[x];
                  if (-OverlapWidth <= d && d <= OverlapWidth) {
                     float w1 = 0.5f * (d / OverlapWidth + 1.0f);
                     float w2 = 1.0f - w1;
                     p.R = (byte)(w1 * p.R + w2 * pr);
                     p.G = (byte)(w1 * p.G + w2 * pg);
                     p.B = (byte)(w1 * p.B + w2 * pb);
                     if (d < 0.0f) l_PanDistMap[x] = SrcDistMap[iy][ix];
                  }
                  else {
                     p.R = (byte)pr;
                     p.G = (byte)pg;
                     p.B = (byte)pb;
                     l_PanDistMap[x] = SrcDistMap[iy][ix];
                  }
                  if (!(p.R || p.G || p.B)) p.R = p.G = p.B = 1;
               }
            }
         }
      }
   }
}

 void SphericalProjection::PlaneToSphere (FPoint2D &s_point,Matrix &mat_T,int sign,FPoint2D &d_point)

{
   FPoint2D pu = GetUndistortedPixelPosition (s_point,SrcImgCenter,Kappa);
   Matrix vec_p = mat_T * GetHomogeneousVector (pu);
   d_point.X = (float)atan2 (vec_p(0),vec_p(2));
   d_point.Y = (float)atan2 (vec_p(1),sqrt (vec_p(0) * vec_p(0) + vec_p(2) * vec_p(2)));
   if      (sign < 0 && d_point.X >  MC_PI / 3.0f) d_point.X -= MC_2_PI;
   else if (sign > 0 && d_point.X < -MC_PI / 3.0f) d_point.X += MC_2_PI;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CylindricalProjectionTable
//
///////////////////////////////////////////////////////////////////////////////

 CylindricalProjectionTable::CylindricalProjectionTable (   )
 
{
   Mats_K       = NULL;
   Mats_R       = NULL;
   LDCTable     = NULL;
   NumSrcImages = 0;
   SrcImgWidth  = 0;
   SrcImgHeight = 0;
   OverlapWidth = (int)PAN_OVERLAP_WIDTH;
   PIWidthScale = PAN_WIDTH_SCALE;
   HFOV         = 0.0f;
}

 void CylindricalProjectionTable::ClosePanorama (IPoint2D &lfm_pos,Matrix &mat_H)

{
   Array2D<CPTEP> s_table;
   JoinTwoEndImages (lfm_pos,s_table);
   Array2D<CPTEP>::Delete (   );
   Array2D<CPTEP>::Create (s_table.Width,s_table.Height);
   Warp (s_table,mat_H);
   HFOV = 360.0f;
}

 void CylindricalProjectionTable::CopyCameraInfo (CylindricalProjectionTable &s_table)

{
   NumSrcImages = s_table.NumSrcImages;
   SrcImgWidth  = s_table.SrcImgWidth;
   SrcImgHeight = s_table.SrcImgHeight;
   HFOV         = s_table.HFOV;
}

 void CylindricalProjectionTable::CropNonimageArea (IBox2D &v_rgn)

{
   float hfov = HFOV * v_rgn.Width / Width;
   Array2D<CPTEP> s_table(v_rgn.Width,v_rgn.Height);
   IPoint2D pt(0,0);
   s_table.Copy (*this,v_rgn,pt);
   Array2D<CPTEP>::Delete (   );
   Array2D<CPTEP>::Create (s_table.Width,s_table.Height);
   Array2D<CPTEP>::Copy   (s_table,0,0,s_table.Width,s_table.Height,0,0);
   HFOV = hfov;
}

 void CylindricalProjectionTable::Delete (   )

{
   Array2D<CPTEP>::Delete (   );
   SrcDistMap.Delete      (   );
}

 void CylindricalProjectionTable::GetBlendingWeights (   )

{
   int x,y;

   for (y = 0; y < Height; y++) {
      CPTEP *l_Array = Array[y];
      for (x = 0; x < Width; x++) {
         CPTE *cpte1 = &l_Array[x].Pair[0];
         CPTE *cpte2 = &l_Array[x].Pair[1];
         if (cpte1->SrcImgNo != -1 && cpte2->SrcImgNo != -1) {
            int d = (int)cpte1->BWeight - cpte2->BWeight;
            if (-OverlapWidth <= d && d <= OverlapWidth) {
               float w = 0.5f * ((float)d / OverlapWidth + 1.0f);
               cpte1->BWeight = (ushort)(w * 256.0f);
               cpte2->BWeight = (ushort)((1.0f - w) * 256.0f);
            }
            else if (d > 0) cpte2->SrcImgNo = -1;
            else if (d < 0) cpte1->SrcImgNo = -1;
         }
      }
   }
}

 void CylindricalProjectionTable::GetPanImageMappingRange (   )

{
   int i;
   
   MinPos.X =  MC_VLV;
   MinPos.Y =  MC_VLV;
   MaxPos.X = -MC_VLV;
   MaxPos.Y = -MC_VLV;
   FPoint2D p1,p2;
   for (i = 0; i < Mats_K->Length; i++) {
      GetSrcImageMappingRange (i,p1,p2);
      if (p1.X < MinPos.X) MinPos.X = p1.X;
      if (p2.X > MaxPos.X) MaxPos.X = p2.X;
      if (p1.Y < MinPos.Y) MinPos.Y = p1.Y;
      if (p2.Y > MaxPos.Y) MaxPos.Y = p2.Y;
   }
   HFOV = (MaxPos.X - MinPos.X) * MC_RAD2DEG;
}

 ISize2D CylindricalProjectionTable::GetPanoramicImageSize (   )

{
   FPoint2D p1,p2;
   int ci = Mats_K->Length / 2;
   GetSrcImageMappingRange (ci,p1,p2);
   int si_width,si_height;
   if (LDCTable == NULL) {
      si_width  = SrcImgWidth;
      si_height = SrcImgHeight;
   }
   else {
      si_width  = LDCTable->Width;
      si_height = LDCTable->Height;
   }
   float a = PIWidthScale * si_width / (p2.X - p1.X);
   float b = si_height / (p2.Y - p1.Y);
   GetPanImageMappingRange (   );
   ISize2D size((int)((MaxPos.X - MinPos.X) * a + 0.5f) + 1,(int)((MaxPos.Y - MinPos.Y) * b + 0.5f) + 1);
   return (size);
}

 void CylindricalProjectionTable::GetSrcImageMappingRange (int img_no,FPoint2D &min_pos,FPoint2D &max_pos)

{
   int x,y;
   int flag_add_2pi;
   FPoint2D sp,cp;
   FPoint2D p1,p2;

   min_pos.X =  MC_VLV;
   min_pos.Y =  MC_VLV;
   max_pos.X = -MC_VLV;
   max_pos.Y = -MC_VLV;
   int si_width,si_height;
   if (LDCTable == NULL) {
      si_width  = SrcImgWidth;
      si_height = SrcImgHeight;
   }
   else {
      si_width  = LDCTable->Width;
      si_height = LDCTable->Height;
   }
   float ex = (float)(si_width  - 1);
   float ey = (float)(si_height - 1);
   Matrix mat_T = Inv((*Mats_K)[img_no] * (*Mats_R)[img_no]);
   sp(0.5f * SrcImgWidth,0.5f * SrcImgHeight);
   PlaneToCylinder (sp,mat_T,cp,FALSE);
   if (-0.1f * MC_PI_2 <= cp.X && cp.X < MC_PI_2) flag_add_2pi = FALSE;
   else flag_add_2pi = TRUE;
   for (x = 0; x < si_width; x++) {
     sp((float)x,0.0f);
      PlaneToCylinder (sp,mat_T,p1,flag_add_2pi);
      sp((float)x,ey  );
      PlaneToCylinder (sp,mat_T,p2,flag_add_2pi);
      if (p1.X > p2.X) Swap (p1.X,p2.X);
      if (p1.Y > p2.Y) Swap (p1.Y,p2.Y);
      if (p1.X < min_pos.X) min_pos.X = p1.X;
      if (p2.X > max_pos.X) max_pos.X = p2.X;
      if (p1.Y < min_pos.Y) min_pos.Y = p1.Y;
      if (p2.Y > max_pos.Y) max_pos.Y = p2.Y;
   }
   for (y = 0; y < si_height; y++) {
     sp(0.0f,(float)y);
      PlaneToCylinder (sp,mat_T,p1,flag_add_2pi);
      sp(ex  ,(float)y);
      PlaneToCylinder (sp,mat_T,p2,flag_add_2pi);
      if (p1.X > p2.X) Swap (p1.X,p2.X);
      if (p1.Y > p2.Y) Swap (p1.Y,p2.Y);
      if (p1.X < min_pos.X) min_pos.X = p1.X;
      if (p2.X > max_pos.X) max_pos.X = p2.X;
      if (p1.Y < min_pos.Y) min_pos.Y = p1.Y;
      if (p2.Y > max_pos.Y) max_pos.Y = p2.Y;
   }
}

 void CylindricalProjectionTable::GetSrcImageMappingRange (int img_no,IPoint2D &min_pos,IPoint2D &max_pos)

{
   float a = (MaxPos.X - MinPos.X) / (Width  - 1);
   float b = (MaxPos.Y - MinPos.Y) / (Height - 1);
   FPoint2D min_p,max_p;
   GetSrcImageMappingRange (img_no,min_p,max_p);
   min_pos.X = (int)((min_p.X - MinPos.X) / a + 0.5f);
   min_pos.Y = (int)((min_p.Y - MinPos.Y) / b + 0.5f);
   max_pos.X = (int)((max_p.X - MinPos.X) / a + 0.5f);
   max_pos.Y = (int)((max_p.Y - MinPos.Y) / b + 0.5f);
}

 void CylindricalProjectionTable::Initialize (Array1D<Matrix> &mats_K,Array1D<Matrix> &mats_R,int si_width,int si_height,LensDistortionCorrectionTable *ldc_table)

{
   int x,y;

   Mats_K       = &mats_K;
   Mats_R       = &mats_R;
   LDCTable     = ldc_table;
   NumSrcImages = mats_K.Length;
   SrcImgWidth  = si_width;
   SrcImgHeight = si_height;
   if (LDCTable == NULL) SrcImgCenter(0.5f * si_width,0.5f * si_height);
   else SrcImgCenter(0.5f * LDCTable->Width,0.5f * LDCTable->Height);
   ISize2D size = GetPanoramicImageSize (   );
   Create (size.Width,size.Height);
   for (y = 0; y < Height; y++) {
      CPTEP *l_Array = Array[y];
      for (x = 0; x < Width; x++) {
         CPTE *cpte = l_Array[x].Pair;
         cpte[0].SrcImgNo = cpte[1].SrcImgNo = -1;
      }
   }
   SrcDistMap.Create (si_width,si_height);
   GetDistanceMapForBlending (SrcDistMap);
}

 void CylindricalProjectionTable::JoinTwoEndImages (IPoint2D &lfm_pos,Array2D<CPTEP> &d_table)

{
   int x,y;

   IPoint2D sp1,ep1;
   GetSrcImageMappingRange (0,sp1,ep1);
   IPoint2D dp = ep1 - sp1;
   d_table.Create (lfm_pos.X + dp.X + 1,Height);
   for (y = 0; y < d_table.Height; y++) {
      CPTEP *l_d_table = d_table[y];
      for (x = lfm_pos.X; x < d_table.Width; x++) {
         CPTE *cpte = l_d_table[x].Pair;
         cpte[0].SrcImgNo = cpte[1].SrcImgNo = -1;
      }
   }
   d_table.Copy (*this,0,0,Width,Height,0,0);
   IPoint2D sp2 = lfm_pos;
   IPoint2D ep2 = lfm_pos + dp;
   if (sp2.Y < 0) sp2.Y = 0;
   if (ep2.Y >= d_table.Height) ep2.Y = d_table.Height - 1;
   dp   = sp1 - lfm_pos;
   sp2 += dp, ep2 += dp;
   int n  = Mats_K->Length % 2;
   int y2 = lfm_pos.Y;
   if (y2 < 0) y2 = 0;
   for (y = sp2.Y; y <= ep2.Y; y++,y2++) {
      int x2 = lfm_pos.X;
      CPTEP *l_d_table1 = d_table[y];
      CPTEP *l_d_table2 = d_table[y2];
      for (x = sp2.X; x <= ep2.X; x++,x2++) {
         if (l_d_table2[x2].Pair[n].SrcImgNo == -1) l_d_table2[x2].Pair[n] = l_d_table1[x].Pair[0];
      }
   }
}

 void CylindricalProjectionTable::Perform (int img_no,byte **mask)

{
   int x,y;

   int n  = img_no % 2;
   int si_width,si_height;
   if (LDCTable == NULL) {
      si_width  = SrcImgWidth;
      si_height = SrcImgHeight;
   }
   else {
      si_width  = LDCTable->Width;
      si_height = LDCTable->Height;
   }
   int exu = si_width  - 2;
   int eyu = si_height - 2;
   double a = (MaxPos.X - MinPos.X) / (Width  - 1);
   double b = (MaxPos.Y - MinPos.Y) / (Height - 1);
   IPoint2D min_p,max_p;
   GetSrcImageMappingRange (img_no,min_p,max_p);
   Matrix vec_x(3);
   Matrix mat_T = (*Mats_K)[img_no] * (*Mats_R)[img_no];
   for (y = min_p.Y; y <= max_p.Y; y++) {
      double v = b * y + MinPos.Y;
      vec_x(1) = v / sqrt (1.0 + v * v);
      double r = sqrt (1.0 - vec_x(1) * vec_x(1));
      for (x = min_p.X; x <= max_p.X; x++) {
         double theta = a * x + MinPos.X;
         vec_x(0) = r * sin (theta);
         vec_x(2) = r * cos (theta);
         Matrix vec_p = mat_T * vec_x;
         float xu = (float)(vec_p(0) / vec_p(2));
         float yu = (float)(vec_p(1) / vec_p(2));
         if (2 <= (int)xu && (int)xu <= exu && 1 <= (int)yu && (int)yu <= eyu) {
            FPoint2D pd;
            if (LDCTable == NULL) pd(xu,yu);
            else pd = (*LDCTable)(xu,yu);
            if (pd.X != -1.0f) {
               if (mask == NULL || mask[(int)pd.Y][(int)pd.X]) {
                  CPTE *cpte = &Array[y][x].Pair[n];
                  cpte->SrcImgNo = (char)img_no;
                  cpte->BWeight  = (ushort)SrcDistMap[(int)pd.Y][(int)pd.X];
                  cpte->Coord   = pd;
               }
            }
         }
      }
   }
}

 void CylindricalProjectionTable::PerformPostprocessing (   )

{
   int n,x,y;

   int ex = SrcImgWidth  - 2;
   int ey = SrcImgHeight - 2;
   for (y = 0; y < Height; y++) {
      CPTEP *l_Array = Array[y];
      for (x = 0; x < Width; x++) {
         CPTE *cpte = l_Array[x].Pair;
         for (n = 0; n < 2; n++) {
            if (cpte->SrcImgNo != -1) {
               int cx = (int)cpte->Coord.X;
               int cy = (int)cpte->Coord.Y;
               if (cx < 0 || cx > ex || cy < 0 || cy > ey) cpte->SrcImgNo = -1;
            }
            cpte++;
         }
      }
   }
   for (y = 0; y < Height; y++) {
      CPTEP *l_Array = Array[y];
      for (x = 0; x < Width; x += 2) {
         CPTE *cpte = l_Array[x].Pair;
         for (n = 0; n < 2; n++) {
            if (cpte[0].SrcImgNo != cpte[2].SrcImgNo)
               cpte[0].SrcImgNo = cpte[2].SrcImgNo = -1;
            cpte++;
         }
      }
   }
}

 void CylindricalProjectionTable::PlaneToCylinder (FPoint2D &s_point,Matrix &mat_T,FPoint2D &d_point,int flag_add_2pi)

{
   Matrix vec_p = mat_T * GetHomogeneousVector (s_point);
   d_point.X = (float)atan2 (vec_p(0),vec_p(2));
   d_point.Y = (float)(vec_p(1) / sqrt (vec_p(0) * vec_p(0) + vec_p(2) * vec_p(2)));
   if (d_point.X < 0.0f && flag_add_2pi) d_point.X += MC_2_PI;
}

 int CylindricalProjectionTable::ReadFile (const char *file_name)
 
{
   int  width,height;
   char identifier[] = IDENTIFIER_CPT;

   FileIO file(file_name,FIO_BINARY,FIO_READ);
   if (!file) return (1);
   CArray1D buffer(sizeof(identifier));
   if (file.Read ((byte *)(char *)buffer,buffer.Length,sizeof(char))) return (2);
   buffer[buffer.Length - 1] = 0;
   if (strcmp (buffer,identifier)) return (3);
   if (file.Read ((byte *)&width ,1,sizeof(int))) return (5);
   if (file.Read ((byte *)&height,1,sizeof(int))) return (6);
   if (file.Read ((byte *)&NumSrcImages,1,sizeof(int))) return (7);
   if (file.Read ((byte *)&SrcImgWidth,1,sizeof(int))) return (8);
   if (file.Read ((byte *)&SrcImgHeight,1,sizeof(int))) return (9);
   if (file.Read ((byte *)&HFOV,1,sizeof(float))) return (10);
   Create (width,height);
   if (file.Read ((byte *)Array[0],Width * Height,sizeof(CPTEP))) return (11);
   return (DONE);
}

 void CylindricalProjectionTable::Resize (CylindricalProjectionTable &s_table)

{
   int n,x,y;

   CopyCameraInfo (s_table);
   Matrix mat_H(3,3);
   mat_H[0][0] = (double)Width  / s_table.Width;
   mat_H[1][1] = (double)Height / s_table.Height;
   mat_H[2][2] = 1.0;
   Warp (s_table,mat_H);
   float zf_x = (float)SrcImgWidth  / s_table.SrcImgWidth;
   float zf_y = (float)SrcImgHeight / s_table.SrcImgHeight;
   for (y = 0; y < Height; y++) {
      CPTEP *l_Array = Array[y];
      for (x = 0; x < Width; x++) {
         CPTE *cpte = l_Array[x].Pair;
         for (n = 0; n < 2; n++) {
            if (cpte->SrcImgNo != -1) {
               cpte->Coord.X *= zf_x;
               cpte->Coord.Y *= zf_y;
            }
            cpte++;
         }
      }
   }
   PerformPostprocessing (   );
}

 void CylindricalProjectionTable::Rotate180Degrees (CylindricalProjectionTable &s_table)

{
   int x,y;
   
   CopyCameraInfo (s_table);
   int ex = s_table.Width  - 1;
   int ey = s_table.Height - 1;
   for (y = 0; y < s_table.Height; y++) {
      CPTEP *l_s_table1 = s_table[y];
      CPTEP *l_s_table2 = s_table[ey - y];
      for (x = 0; x < s_table.Width; x++)
         l_s_table2[ex - x] = l_s_table1[x];
   }
   PerformPostprocessing (   );
}

 void CylindricalProjectionTable::Warp (Array2D<CPTEP> &s_table,Matrix &mat_H)

{
   int n,x,y,ix1,iy1;

   Matrix mat_IH = Inv(mat_H);
   double dx = mat_IH[0][0];
   double dy = mat_IH[1][0];
   double dz = mat_IH[2][0];
   int ex = s_table.Width  - 1;
   int ey = s_table.Height - 1;
   for (y = 0; y < Height; y++) {
      double tx = mat_IH[0][1] * y + mat_IH[0][2];
      double ty = mat_IH[1][1] * y + mat_IH[1][2];
      double tz = mat_IH[2][1] * y + mat_IH[2][2];
      for (x = 0; x < Width; x++) {
         float fx = (float)(tx / tz);
         float fy = (float)(ty / tz);
         int   ix = (int)fx;
         int   iy = (int)fy;
         if (fx >= 0.0f && fy >= 0.0f && ix <= ex && iy <= ey) {
            if (ix == ex) ix1 = ix;
            else ix1 = ix + 1;
            if (iy == ey) iy1 = iy;
            else iy1 = iy + 1;
            for (n = 0; n < 2; n++) {
               CPTE *s_cpte = &s_table[iy][ix].Pair[n];
               CPTE *d_cpte = &Array[y][x].Pair[n];
               int no = s_cpte->SrcImgNo;
               if (no != -1 && no == s_table[iy1][ix].Pair[n].SrcImgNo && no == s_table[iy][ix1].Pair[n].SrcImgNo && no == s_table[iy1][ix1].Pair[n].SrcImgNo) {
                  float rx = fx - ix;
                  float ry = fy - iy;
                  FPoint2D p1 = s_table[iy ][ix ].Pair[n].Coord + (s_table[iy ][ix1].Pair[n].Coord - s_table[iy ][ix ].Pair[n].Coord) * rx;
                  FPoint2D p2 = s_table[iy1][ix ].Pair[n].Coord + (s_table[iy1][ix1].Pair[n].Coord - s_table[iy1][ix ].Pair[n].Coord) * rx;
                  FPoint2D p3 = s_table[iy ][ix ].Pair[n].Coord + (s_table[iy1][ix ].Pair[n].Coord - s_table[iy ][ix ].Pair[n].Coord) * ry;
                  FPoint2D p4 = s_table[iy ][ix1].Pair[n].Coord + (s_table[iy1][ix1].Pair[n].Coord - s_table[iy ][ix1].Pair[n].Coord) * ry;
                  FLine2D line1(p1,p2);
                  FLine2D line2(p3,p4);
                  if      (ix == ix1) d_cpte->Coord = p1 + (p2 - p1) * ry; // p3 is equal to p4.
                  else if (iy == iy1) d_cpte->Coord = p3 + (p4 - p3) * rx; // p1 is eqaul to p2.
                  else if (GetIntersectionPoint (line1,line2,d_cpte->Coord)) d_cpte->Coord = s_cpte->Coord;
                  d_cpte->SrcImgNo = s_cpte->SrcImgNo;
                  d_cpte->BWeight  = s_cpte->BWeight;
               }
               else d_cpte->SrcImgNo = -1;
            }
         }
         else Array[y][x].Pair[0].SrcImgNo = Array[y][x].Pair[1].SrcImgNo = -1;
         tx += dx, ty += dy, tz += dz;
      }
   }
}

 int CylindricalProjectionTable::WriteFile (const char *file_name)
 
{
   char identifier[] = IDENTIFIER_CPT;

   FileIO file(file_name,FIO_BINARY,FIO_CREATE);
   if (!file) return (1);
   if (file.Write ((byte *)identifier,1,sizeof(identifier))) return (2);
   if (file.Write ((byte *)&Width,1,sizeof(int))) return (3);
   if (file.Write ((byte *)&Height,1,sizeof(int))) return (4);
   if (file.Write ((byte *)&NumSrcImages,1,sizeof(int))) return (5);
   if (file.Write ((byte *)&SrcImgWidth,1,sizeof(int))) return (6);
   if (file.Write ((byte *)&SrcImgHeight,1,sizeof(int))) return (7);
   if (file.Write ((byte *)&HFOV,1,sizeof(float))) return (8);
   if (file.Write ((byte *)Array[0],Width * Height,sizeof(CPTEP))) return (9);
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: PanImageCompositionTable
//
///////////////////////////////////////////////////////////////////////////////

 PanImageCompositionTable::PanImageCompositionTable (   )
 
{
   Flag_IBCEnabled = FALSE;
   IBCBaseImgIdx  = 0;
   NumSrcImages   = 0;
   ProjectionType = 0;
   SrcImgWidth    = 0;
   SrcImgHeight   = 0;
   HFOV           = 0.0f;
}

 int PanImageCompositionTable::Create (CylindricalProjectionTable &cp_table)

{
   int i;
   
   if (Array2D<PICTEP>::Create (cp_table.Width,cp_table.Height) != DONE) return (1);
   NumSrcImages   = cp_table.NumSrcImages;
   ProjectionType = PM_CYLINDER;
   SrcImgWidth    = cp_table.SrcImgWidth;
   SrcImgHeight   = cp_table.SrcImgHeight;
   HFOV           = cp_table.HFOV;
   CPTE*  cpte    = cp_table[0][0].Pair;
   PICTE* picte   = Array[0][0].Pair;
   int length = Width * Height * 2;
   for (i = 0; i < length; i++) {
      picte->SrcImgNo = cpte->SrcImgNo;
      if (cpte->SrcImgNo != -1) {
         int ix = (int)cpte->Coord.X;
         int iy = (int)cpte->Coord.Y;
         float rx2 = (cpte->Coord.X - ix);
         float ry2 = (cpte->Coord.Y - iy);
         float rx1 = 1.0f - rx2;
         float ry1 = 1.0f - ry2;
         picte->IWeights[0] = (ushort)(rx1 * ry1 * 256.0f);
         picte->IWeights[1] = (ushort)(rx1 * ry2 * 256.0f);
         picte->IWeights[2] = (ushort)(rx2 * ry1 * 256.0f);
         picte->IWeights[3] = (ushort)(rx2 * ry2 * 256.0f);
         picte->BWeight = cpte->BWeight;
         picte->X = (ushort)cpte->Coord.X;
         picte->Y = (ushort)cpte->Coord.Y;
      }
      cpte++,picte++;
   }
   return (DONE);
}

 int PanImageCompositionTable::EstimateIBCParams_YUY2 (byte **s_buffers)

{
   int i,x,y;

   int n_pairs = NumSrcImages - 1;
   if (Array == NULL) return (1);
   int si_line_len = SrcImgWidth * 2;
   IArray2D x_array(NumSrcImages,NumSrcImages);
      // 인접하는 컴포넌트 영상들 사이의 겹치는 영역에서의 픽셀 밝기의 합
      // x_array[i][j]: i번째 컴포넌트 영상과 j번째 컴포넌트 영상 사이의 겹치는 영역에서
      //                i번째 컴포넌트 영상의 픽셀 밝기의 합
   x_array.Clear (   );
   I64Array2D xx_array(NumSrcImages,NumSrcImages);
   xx_array.Clear (   );
      // 인접하는 컴포넌트 영상들 사이의 겹치는 영역에서의 픽셀 밝기의 자승의 합
      // xx_array[i][j]: i번째 컴포넌트 영상과 j번째 컴포넌트 영상 사이의 겹치는 영역에서
      //                 i번째 컴포넌트 영상의 픽셀 밝기의 자승의 합
   IArray1D n_array(n_pairs);
      // 인접하는 컴포넌트 영상들 사이의 겹치는 영역에서의 픽셀의 수
      // n_array[i]: i번째 컴포넌트 영상과 (i+1)번째 컴포넌트 영상 사이의
      //             겹치는 영역에서의 픽셀 수
   n_array.Clear (   );
   for (y = 0; y < Height; y++) {
      PICTE* picte = (PICTE*)Array[y];
      for (x = 0; x < Width; x++) {
         int si1 = picte[0].SrcImgNo;
         int si2 = picte[1].SrcImgNo;
         if (si1 != -1 && si2 != -1) {
            int p1 = *(s_buffers[si1] + si_line_len * picte[0].Y + picte[0].X * 2);
            int p2 = *(s_buffers[si2] + si_line_len * picte[1].Y + picte[1].X * 2);
            x_array [si1][si2] += p1;
            x_array [si2][si1] += p2;
            xx_array[si1][si2] += p1 * p1;
            xx_array[si2][si1] += p2 * p2;
            n_array[GetMinimum(si1,si2)]++;
         }
         picte += 2;
      }
   }
   if (IBCParams1.Length != NumSrcImages) IBCParams1.Create (NumSrcImages);
   if (IBCParams2.Length != NumSrcImages) IBCParams2.Create (NumSrcImages);
   IBCParams1.Clear  (   );
   IBCParams2.Clear  (   );
   IBCParams1[0] = 1.0f;
   for (i = 0; i < n_pairs; i++) {
      int    i1 = i + 1;
      int    n  = n_array[i];
      double m1 = (double)x_array[i ][i1] / n;
      double m2 = (double)x_array[i1][i ] / n;
      double s1 = sqrt ((double)xx_array[i ][i1] / n - m1 * m1);
      double s2 = sqrt ((double)xx_array[i1][i ] / n - m2 * m2);
      IBCParams1[i1] = (float)(s1 / s2);
      IBCParams2[i1] = (float)(m1 - IBCParams1[i1] * m2);
   }
   for (i = 2; i < NumSrcImages; i++) {
      int  i1 = i - 1;
      float a = IBCParams1[i];
      float b = IBCParams2[i];
      IBCParams1[i] = a * IBCParams1[i1];
      IBCParams2[i] = a * IBCParams2[i1] + b;
   }
   float a = IBCParams1[IBCBaseImgIdx];
   float b = IBCParams2[IBCBaseImgIdx];
   for (i = 0; i < NumSrcImages; i++) {
      IBCParams1[i] = IBCParams1[i] / a;
      IBCParams2[i] = (IBCParams2[i] - b) / a;
   }
   return (DONE);
}

 int PanImageCompositionTable::Perform (Array1D<BGRImage> &s_images,BGRImage &d_image)
#if defined(__SIMD_SSE2)
{
   int i,j,no1,no2;
   PICTE *picte;
   BGRPixel *sp01,*sp11,*sp02,*sp12,*dp,**s_image;

   if (d_image.Width != Width || d_image.Height != Height) d_image.Create (Width,Height);
   __asm {
      pxor xmm7, xmm7 // xmm7_w = [ 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 ]
   }
   for (i = 0; i < Height; i++) {
      picte = Array[i][0].Pair;
      dp   = d_image[i];
      for (j = 0; j < Width; j += 2) {
         no1 = picte->SrcImgNo;
         if (no1 >= 0) {
            s_image = (BGRPixel**)s_images[no1];
            __asm {
               mov      eax,  picte

               pxor     xmm0, xmm0
               movlpd   xmm0, [eax + 1]  // xmm0_w = [ 0 | 0 | 0 | 0 | w04 | w03 | w02 | w01 ]
               pshuflw  xmm1, xmm0,  00h // xmm1_w = [ 0 | 0 | 0 | 0 | w01 | w01 | w01 | w01 ]
               pshuflw  xmm2, xmm0,  55h // xmm2_w = [ 0 | 0 | 0 | 0 | w02 | w02 | w02 | w02 ]
               pshuflw  xmm3, xmm0, 0AAh // xmm3_w = [ 0 | 0 | 0 | 0 | w03 | w03 | w03 | w03 ]
               pshuflw  xmm4, xmm0, 0FFh // xmm4_w = [ 0 | 0 | 0 | 0 | w04 | w04 | w04 | w04 ]

               pxor     xmm0, xmm0
               movhpd   xmm0, [eax + 31] // xmm0_w = [ w14 | w13 | w12 | w11 | 0 | 0 | 0 | 0 ]
               pshufhw  xmm5, xmm0,  00h // xmm5_w = [ w11 | w11 | w11 | w11 | 0 | 0 | 0 | 0 ]
               por      xmm1, xmm5       // xmm1_w = [ w11 | w11 | w11 | w11 | w01 | w01 | w01 | w01 ]
               pshufhw  xmm5, xmm0,  55h // xmm5_w = [ w12 | w12 | w12 | w12 | 0 | 0 | 0 | 0 ]
               por      xmm2, xmm5       // xmm2_w = [ w12 | w12 | w12 | w12 | w02 | w02 | w02 | w02 ]
               pshufhw  xmm5, xmm0, 0AAh // xmm5_w = [ w13 | w13 | w13 | w13 | 0 | 0 | 0 | 0 ]
               por      xmm3, xmm5       // xmm3_w = [ w13 | w13 | w13 | w13 | w03 | w03 | w03 | w03 ]
               pshufhw  xmm5, xmm0, 0FFh // xmm5_w = [ w14 | w14 | w14 | w14 | 0 | 0 | 0 | 0 ]
               por      xmm4, xmm5       // xmm4_w = [ w14 | w14 | w14 | w14 | w04 | w04 | w04 | w04 ]
            }
            sp01 = s_image[picte[0].Y] + picte[0].X;
            sp02 = s_image[picte[0].Y + 1] + picte[0].X;
            sp11 = s_image[picte[2].Y] + picte[2].X;
            sp12 = s_image[picte[2].Y + 1] + picte[2].X;
            __asm {
               mov       eax , sp01

               movd      mm0 , [eax]     // mm0_b = [ 0 | 0 | 0 | 0 | B03 | R01 | G01 | B01 ]
               movd      mm1 , [eax + 3] // mm1_b = [ 0 | 0 | 0 | 0 | B05 | R03 | G03 | B03 ]

               mov       eax , sp11 
               movd      mm2 , [eax]     // mm2_b = [ 0 | 0 | 0 | 0 | B13 | R11 | G11 | B11 ]
               movd      mm3 , [eax + 3] // mm3_b = [ 0 | 0 | 0 | 0 | B15 | R13 | G13 | B13 ]

               pshufw    mm4 , mm2, 4Eh  // mm4_b = [ B13 | R11 | G11 | B11 | 0 | 0 | 0 | 0 ]
               por       mm0 , mm4       // mm0_b = [ B13 | R11 | G11 | B11 | B03 | R01 | G01 | B01 ]

               pshufw    mm4 , mm3, 4Eh  // mm4_b = [ B15 | R13 | G13 | B13 | 0 | 0 | 0 | 0 ]
               por       mm1 , mm4       // mm1_b = [ B15 | R13 | G13 | B13 | B05 | R03 | G03 | B03 ]

               movq2dq   xmm0, mm0
               punpcklbw xmm0, xmm7      // xmm0_w = [ B13 | R11 | G11 | B11 | B03 | R01 | G01 | B01 ]
               pmullw    xmm1, xmm0      // xmm1_w = [ w11 * B13 | w11 * R11 | w11 * G11 | w11 * B11 | w01 * B03 | w01 * R01 | w01 * G01 | w01 * B01 ]
                                         //        = [ wB13 | wR11 | wG11 | wB11 | wB03 | wR01 | wG01 | wB01 ]

               movq2dq   xmm0, mm1
               punpcklbw xmm0, xmm7      // xmm0_w = [ B15 | R13 | G13 | B13 | B05 | R03 | G03 | B03 ]
               pmullw    xmm3, xmm0      // xmm3_w = [ w13 * B15 | w13 * R13 | w13 * G13 | w13 * B13 | w03 * B05 | w03 * R03 | w03 * G03 | w03 * B03 ]
                                         //        = [ wB15 | wR13 | wG13 | wB13 | wB05 | wR03 | wG03 | wB03 ]

               mov       eax , sp02
               movd      mm0 , [eax]     // mm0_b = [ 0 | 0 | 0 | 0 | B04 | R02 | G02 | B02 ]
               movd      mm1 , [eax + 3] // mm1_b = [ 0 | 0 | 0 | 0 | B06 | R04 | G04 | B04 ]

               mov       eax , sp12
               movd      mm2 , [eax]     // mm2_b = [ 0 | 0 | 0 | 0 | B14 | R12 | G12 | B12 ]
               movd      mm3 , [eax + 3] // mm3_b = [ 0 | 0 | 0 | 0 | B16 | R14 | G14 | B14 ]

               pshufw    mm4 , mm2, 4Eh  // mm4_b = [ B14 | R12 | G12 | B12 | 0 | 0 | 0 | 0 ]
               por       mm0 , mm4       // mm0_b = [ B14 | R12 | G12 | B12 | B04 | R02 | G02 | B02 ]

               pshufw    mm4 , mm3, 4Eh  // mm4_b = [ B16 | R14 | G14 | B14 | 0 | 0 | 0 | 0 ]
               por       mm1 , mm4       // mm1_b = [ B16 | R14 | G14 | B14 | B06 | R04 | G04 | B04 ]

               movq2dq   xmm0, mm0
               punpcklbw xmm0, xmm7      // xmm0_w = [ B14 | R12 | G12 | B12 | B04 | R02 | G02 | B02 ]
               pmullw    xmm2, xmm0      // xmm2_w = [ w12 * B14 | w12 * R12 | w12 * G12 | w12 * B12 | w02 * B04 | w02 * R02 | w02 * G02 | w02 * B02 ]
                                         //        = [ wB14 | wR12 | wG12 | wB12 | wB04 | wR02 | wG02 | wB02 ]

               movq2dq   xmm0, mm1
               punpcklbw xmm0, xmm7      // xmm0_w = [ B16 | R14 | G14 | B14 | B06 | R04 | G04 | B04 ]
               pmullw    xmm4, xmm0      // xmm4_w = [ w14 * B16 | w14 * R14 | w14 * G14 | w14 * B14 | w04 * B06 | w04 * R04 | w04 * G04 | w04 * B04 ]
                                         //        = [ wB16 | wR14 | wG14 | wB14 | wB06 | wR04 | wG04 | wB04 ]

               paddusw   xmm1, xmm2      
               paddusw   xmm1, xmm3
               paddusw   xmm1, xmm4      // xmm1_w = [ wB13 | wR11 | wG11 | wB11 | wB03 | wR01 | wG01 | wB01 ]
                                         //        + [ wB14 | wR12 | wG12 | wB12 | wB04 | wR02 | wG02 | wB02 ]
                                         //        + [ wB15 | wR13 | wG13 | wB13 | wB05 | wR03 | wG03 | wB03 ]
                                         //			+ [ wB16 | wR14 | wG14 | wB14 | wB06 | wR04 | wG04 | wB04 ]
               psrlw     xmm1, 08h       // xmm1_w /= 256 -> xmm1_w = [ iB11 | iR11 | iG11 | iB11 | iB01 | iR01 | iG01 | iB01 ]
               movdqa    xmm5, xmm1      // xmm5_w = xmm1_w

               emms
            }
         }
         picte++;
         no2 = picte->SrcImgNo;
         if (no2 >= 0) {
            s_image = (BGRPixel**)s_images[no2];
            __asm {
               mov      eax, picte

               pxor     xmm0, xmm0
               movlpd   xmm0, [eax + 1]  // xmm0_w = [ 0 | 0 | 0 | 0 | w04 | w03 | w02 | w01 ]
               pshuflw  xmm1, xmm0,  00h // xmm1_w = [ 0 | 0 | 0 | 0 | w01 | w01 | w01 | w01 ]
               pshuflw  xmm2, xmm0,  55h // xmm2_w = [ 0 | 0 | 0 | 0 | w02 | w02 | w02 | w02 ]
               pshuflw  xmm3, xmm0, 0AAh // xmm3_w = [ 0 | 0 | 0 | 0 | w03 | w03 | w03 | w03 ]
               pshuflw  xmm4, xmm0, 0FFh // xmm4_w = [ 0 | 0 | 0 | 0 | w04 | w04 | w04 | w04 ]

               pxor     xmm0, xmm0
               movhpd   xmm0, [eax + 31] // xmm0_w = [ w14 | w13 | w12 | w11 | 0 | 0 | 0 | 0 ]
               pshufhw  xmm6, xmm0,  00h // xmm6_w = [ w11 | w11 | w11 | w11 | 0 | 0 | 0 | 0 ]
               por      xmm1, xmm6       // xmm1_w = [ w11 | w11 | w11 | w11 | w01 | w01 | w01 | w01 ]
               pshufhw  xmm6, xmm0,  55h // xmm6_w = [ w12 | w12 | w12 | w12 | 0 | 0 | 0 | 0 ]
               por      xmm2, xmm6       // xmm2_w = [ w12 | w12 | w12 | w12 | w02 | w02 | w02 | w02 ]
               pshufhw  xmm6, xmm0, 0AAh // xmm6_w = [ w13 | w13 | w13 | w13 | 0 | 0 | 0 | 0 ]
               por      xmm3, xmm6       // xmm3_w = [ w13 | w13 | w13 | w13 | w03 | w03 | w03 | w03 ]
               pshufhw  xmm6, xmm0, 0FFh // xmm6_w = [ w14 | w14 | w14 | w14 | 0 | 0 | 0 | 0 ]
               por      xmm4, xmm6       // xmm4_w = [ w14 | w14 | w14 | w14 | w04 | w04 | w04 | w04 ]
            }
            sp01 = s_image[picte[0].Y] + picte[0].X;
            sp02 = s_image[picte[0].Y + 1] + picte[0].X;
            sp11 = s_image[picte[2].Y] + picte[2].X;
            sp12 = s_image[picte[2].Y + 1] + picte[2].X;
            __asm {

               mov       eax , sp01
               movd      mm0 , [eax]     // mm0_b = [ 0 | 0 | 0 | 0 | B03 | R01 | G01 | B01 ]
               movd      mm1 , [eax + 3] // mm1_b = [ 0 | 0 | 0 | 0 | B05 | R03 | G03 | B03 ]

               mov       eax , sp11 
               movd      mm2 , [eax]     // mm2_b = [ 0 | 0 | 0 | 0 | B13 | R11 | G11 | B11 ]
               movd      mm3 , [eax + 3] // mm3_b = [ 0 | 0 | 0 | 0 | B15 | R13 | G13 | B13 ]

               pshufw    mm4 , mm2, 4Eh  // mm4_b = [ B13 | R11 | G11 | B11 | 0 | 0 | 0 | 0 ]
               por       mm0 , mm4       // mm0_b = [ B13 | R11 | G11 | B11 | B03 | R01 | G01 | B01 ]

               pshufw    mm4 , mm3, 4Eh  // mm4_b = [ B15 | R13 | G13 | B13 | 0 | 0 | 0 | 0 ]
               por       mm1 , mm4       // mm1_b = [ B15 | R13 | G13 | B13 | B05 | R03 | G03 | B03 ]

               movq2dq   xmm0, mm0
               punpcklbw xmm0, xmm7      // xmm0_w = [ B13 | R11 | G11 | B11 | B03 | R01 | G01 | B01 ]
               pmullw    xmm1, xmm0      // xmm1_w = [ w11 * B13 | w11 * R11 | w11 * G11 | w11 * B11 | w01 * B03 | w01 * R01 | w01 * G01 | w01 * B01 ]
                                         //        = [ wB13 | wR11 | wG11 | wB11 | wB03 | wR01 | wG01 | wB01 ]

               movq2dq   xmm0, mm1
               punpcklbw xmm0, xmm7      // xmm0_w = [ B15 | R13 | G13 | B13 | B05 | R03 | G03 | B03 ]
               pmullw    xmm3, xmm0      // xmm3_w = [ w13 * B15 | w13 * R13 | w13 * G13 | w13 * B13 | w03 * B05 | w03 * R03 | w03 * G03 | w03 * B03 ]
                                         //        = [ wB15 | wR13 | wG13 | wB13 | wB05 | wR03 | wG03 | wB03 ]

               mov       eax , sp02
               movd      mm0 , [eax]     // mm0_b = [ 0 | 0 | 0 | 0 | B04 | R02 | G02 | B02 ]
               movd      mm1 , [eax + 3] // mm1_b = [ 0 | 0 | 0 | 0 | B06 | R04 | G04 | B04 ]

               mov       eax , sp12
               movd      mm2 , [eax]     // mm2_b = [ 0 | 0 | 0 | 0 | B14 | R12 | G12 | B12 ]
               movd      mm3 , [eax + 3] // mm3_b = [ 0 | 0 | 0 | 0 | B16 | R14 | G14 | B14 ]

               pshufw    mm4 , mm2, 4Eh  // mm4_b = [ B14 | R12 | G12 | B12 | 0 | 0 | 0 | 0 ]
               por       mm0 , mm4       // mm0_b = [ B14 | R12 | G12 | B12 | B04 | R02 | G02 | B02 ]

               pshufw    mm4 , mm3, 4Eh  // mm4_b = [ B16 | R14 | G14 | B14 | 0 | 0 | 0 | 0 ]
               por       mm1 , mm4       // mm1_b = [ B16 | R14 | G14 | B14 | B06 | R04 | G04 | B04 ]

               movq2dq   xmm0, mm0
               punpcklbw xmm0, xmm7      // xmm0_w = [ B14 | R12 | G12 | B12 | B04 | R02 | G02 | B02 ]
               pmullw    xmm2, xmm0      // xmm2_w = [ w12 * B14 | w12 * R12 | w12 * G12 | w12 * B12 | w02 * B04 | w02 * R02 | w02 * G02 | w02 * B02 ]
                                         //        = [ wB14 | wR12 | wG12 | wB12 | wB04 | wR02 | wG02 | wB02 ]

               movq2dq   xmm0, mm1
               punpcklbw xmm0, xmm7      // xmm0_w = [ B16 | R14 | G14 | B14 | B06 | R04 | G04 | B04 ]
               pmullw    xmm4, xmm0      // xmm4_w = [ w14 * B16 | w14 * R14 | w14 * G14 | w14 * B14 | w04 * B06 | w04 * R04 | w04 * G04 | w04 * B04 ]
                                         //        = [ wB16 | wR14 | wG14 | wB14 | wB06 | wR04 | wG04 | wB04 ]

               paddusw   xmm1, xmm2      
               paddusw   xmm1, xmm3
               paddusw   xmm1, xmm4      // xmm1_w = [ wB13 | wR11 | wG11 | wB11 | wB03 | wR01 | wG01 | wB01 ]
                                         //        + [ wB14 | wR12 | wG12 | wB12 | wB04 | wR02 | wG02 | wB02 ]
                                         //        + [ wB15 | wR13 | wG13 | wB13 | wB05 | wR03 | wG03 | wB03 ]
                                         //			+ [ wB16 | wR14 | wG14 | wB14 | wB06 | wR04 | wG04 | wB04 ]
               psrlw     xmm1, 08h       // xmm1_w /= 256 -> xmm1_w = [ iB12 | iR12 | iG12 | iB12 | iB02 | iR02 | iG02 | iB02 ]
               movdqa    xmm6, xmm1      // xmm6_w = xmm1_w
             
               emms
            }
         }
         if (no1 >= 0 && no2 >= 0) {
            __asm {
               mov       eax, picte

               pinsrw    xmm0, [eax - 6], 00h  // xmm0_w = [ ? | ? | ? | ? | ? | ? | ? | w01 ]
               pshuflw   xmm1, xmm0, 00h       // xmm1_w = [ ? | ? | ? | ? | w01 | w01 | w01 | w01 ]

               pinsrw    xmm1, [eax + 24], 04h // xmm1_w = [ ? | ? | ? | w11 | w01 | w01 | w01 | w01 ]
               pshufhw   xmm1, xmm1, 00h       // xmm1_w = [ w11 | w11 | w11 | w11 | w01 | w01 | w01 | w01 ]
               
               pinsrw    xmm0, [eax +  9], 00h // xmm0_w = [ ? | ? | ? | ? | ? | ? | ? | w02 ]
               pshuflw   xmm2, xmm0, 00h       // xmm2_w = [ ? | ? | ? | ? | w02 | w02 | w02 | w02 ]

               pinsrw    xmm2, [eax + 39], 04h // xmm2_w = [ ? | ? | ? | w12 | w02 | w02 | w02 | w02 ]
               pshufhw   xmm2, xmm2, 00h       // xmm2_w = [ w12 | w12 | w12 | w12 | w02 | w02 | w02 | w02 ]

               pmullw    xmm1, xmm5            // xmm1_w = [ w11 * iB11 | w11 * iR11 | w11 * iG11 | w11 * iB11 | w01 * iB01 | w01 * iR01 | w01 * iG01 | w01 * iB01 ]
                                               //        = [ wB11 | wR11 | wG11 | wB11 | wB01 | wR01 | wG01 | wB01 ]
               pmullw    xmm2, xmm6            // xmm2_w = [ w12 * iB12 | w12 * iR12 | w12 * iG12 | w12 * iB12 | w02 * iB02 | w02 * iR02 | w02 * iG02 | w02 * iB02 ]
                                               //        = [ wB12 | wR12 | wG12 | wB12 | wB02 | wR02 | wG02 | wB02 ]
               paddusw   xmm1, xmm2            // xmm1_w = [ wB11 | wR11 | wG11 | wB11 | wB01 | wR01 | wG01 | wB01 ]
                                               //        + [ wB12 | wR12 | wG12 | wB12 | wB02 | wR02 | wG02 | wB02 ]

               psrlw     xmm1, 08h             // xmm1_w /= 256 -> xmm1_w = [ iB1 | iR1 | iG1 | iB1 | iB0 | iR0 | iG0 | iB0 ]
               packuswb  xmm1, xmm1            // xmm1_b = [ 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | iB1 | iR1 | iG1 | iB1 | iB0 | iR0 | iG0 | iB0 ]

               mov       eax, dp
               movd      [eax], xmm1
               psrldq    xmm1, 04h             // xmm1_b = [ 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | iB1 | iR1 | iG1 | iB1 ]
               movd      [eax + 3], xmm1
            }
         }
         else if (no1 >= 0) {
            __asm {
               packuswb  xmm1,xmm5
               mov       eax, dp
               movd      [eax], xmm1
               psrldq    xmm1, 04h
               movd      [eax + 3], xmm1
            }
         }
         else if (no2 >= 0) {
            __asm {
               packuswb  xmm1,xmm6
               mov       eax, dp
               movd      [eax], xmm1
               psrldq    xmm1, 04h
               movd      [eax + 3], xmm1
            }
         }
         else {
            __asm {
               mov  eax, dp
               movd [eax], xmm7
               movd [eax + 3], xmm7
            }
         } 
         picte += 3;
         dp += 2;
      }
   }
   return (DONE);
}
#else
{
   return (-1);
}
#endif

 int PanImageCompositionTable::Perform_YUY2 (byte** s_buffers,byte* d_buffer)

{ 
   int    x,y;
   int    no1,no2;
   byte   u0,v0,u1,v1;
   byte   *sp1,*sp2,*s_buffer;
   short  ip01,ip02,ip11,ip12;
   ushort *iw;

   if (Array == NULL || !NumSrcImages) return (1);
   FArray1D ibc_params1(NumSrcImages);
   FArray1D ibc_params2(NumSrcImages);
   if (Flag_IBCEnabled && !!IBCParams1 && !!IBCParams2) {
      ibc_params1 = IBCParams1;
      ibc_params2 = IBCParams2;
   }
   else {
      ibc_params1.Set   (0,NumSrcImages,1.0f);
      ibc_params2.Clear (   );
   }
   int si_sll = SrcImgWidth * 2;
   PICTE *picte = Array[0][0].Pair;
   for (y = 0; y < Height; y++) {
      for (x = 0; x < Width; x += 2) {
         no1 = (int)picte[0].SrcImgNo;
         if (no1 >= 0) {
            float a  = ibc_params1[no1];
            float b  = ibc_params2[no1];
            s_buffer = s_buffers[no1];
            iw   = picte[0].IWeights;
            sp1  = s_buffer + picte[0].Y * si_sll + (picte[0].X << 1);
            sp2  = sp1 + si_sll;
            ip01 = (iw[0] * sp1[0] + iw[1] * sp2[0] + iw[2] * sp1[2] + iw[3] * sp2[2]) >> 8;
            ip01 = (short)(a * ip01 + b);
            if (ip01 < 0) ip01 = 0;
            else if (ip01 > 255) ip01 = 255;
            if (picte[0].X & 0x0001) u0 = sp1[-1], v0 = sp1[1];
            else u0 = sp1[1], v0 = sp1[3];
            iw   = picte[2].IWeights;
            sp1  = s_buffer + picte[2].Y * si_sll + (picte[2].X << 1);
            sp2  = sp1 + si_sll;
            ip02 = (iw[0] * sp1[0] + iw[1] * sp2[0] + iw[2] * sp1[2] + iw[3] * sp2[2]) >> 8;
            ip02 = (short)(a * ip02 + b);
            if (ip02 < 0) ip02 = 0;
            else if (ip02 > 255) ip02 = 255;
         }
         no2 = (int)picte[1].SrcImgNo;
         if (no2 >= 0) {
            float a  = ibc_params1[no2];
            float b  = ibc_params2[no2];
            s_buffer = s_buffers[no2];
            iw   = picte[1].IWeights;
            sp1  = s_buffer + picte[1].Y * si_sll + (picte[1].X << 1);
            sp2  = sp1 + si_sll;
            ip11 = (iw[0] * sp1[0] + iw[1] * sp2[0] + iw[2] * sp1[2] + iw[3] * sp2[2]) >> 8;
            ip11 = (short)(a * ip11 + b);
            if (ip11 < 0) ip11 = 0;
            else if (ip11 > 255) ip11 = 255;
            if (picte[1].X & 0x0001) u1 = sp1[-1], v1 = sp1[1];
            else u1 = sp1[1], v1 = sp1[3];
            iw   = picte[3].IWeights;
            sp1  = s_buffer + picte[3].Y * si_sll + (picte[3].X << 1);
            sp2  = sp1 + si_sll;
            ip12 = (iw[0] * sp1[0] + iw[1] * sp2[0] + iw[2] * sp1[2] + iw[3] * sp2[2]) >> 8;
            ip12 = (short)(a * ip12 + b);
            if (ip12 < 0) ip12 = 0;
            else if (ip12 > 255) ip12 = 255;
         }
         if (no1 >= 0 && no2 >= 0) {
            d_buffer[0] = (byte)((picte[0].BWeight * ip01 + picte[1].BWeight * ip11) >> 8);
            d_buffer[2] = (byte)((picte[2].BWeight * ip02 + picte[3].BWeight * ip12) >> 8);
            d_buffer[1] = (byte)(((ushort)u0 + u1) >> 1);
            d_buffer[3] = (byte)(((ushort)v0 + v1) >> 1);
         }
         else if (no1 >= 0) {
            d_buffer[0] = (byte)ip01;
            d_buffer[2] = (byte)ip02;
            d_buffer[1] = u0;
            d_buffer[3] = v0;
         }
         else if (no2 >= 0) {
            d_buffer[0] = (byte)ip11;
            d_buffer[2] = (byte)ip12;
            d_buffer[1] = u1;
            d_buffer[3] = v1;
         }
         else {
            d_buffer[0] = d_buffer[2] = 0;
            d_buffer[1] = d_buffer[3] = 128;
         }
         d_buffer += 4;
         picte += 4;
      }
   }
   return (DONE);
}

 int PanImageCompositionTable::Perform_YUY2 (byte** s_buffers,IBox2D& s_rect,byte* d_buffer)

{
   int    i,x,y;
   byte   u0,v0,u1,v1;
   byte   *sp1,*sp2,*s_buffer;
   short  no1,no2;
   short  ip01,ip02,ip11,ip12;
   ushort *iw;

   if (Array == NULL || !NumSrcImages) return (1);
   FArray1D ibc_params1(NumSrcImages);
   FArray1D ibc_params2(NumSrcImages);
   if (Flag_IBCEnabled && !!IBCParams1 && !!IBCParams2) {
      ibc_params1 = IBCParams1;
      ibc_params2 = IBCParams2;
   }
   else {
      ibc_params1.Set   (0,NumSrcImages,1.0f);
      ibc_params2.Clear (   );
   }
   int sr_x = s_rect.X;
   if (sr_x < 0) {
      if (abs (sr_x) % 2) sr_x++;
   }
   int s_sx = sr_x;
   int s_sy = s_rect.Y;
   int s_ex = s_sx + s_rect.Width;
   int s_ey = s_sy + s_rect.Height;
   int flag_cropped = FALSE;
   if (s_sx < 0) s_sx = 0, flag_cropped = TRUE;
   if (s_sy < 0) s_sy = 0, flag_cropped = TRUE;
   if (s_ex > Width ) s_ex = Width , flag_cropped = TRUE;
   if (s_ey > Height) s_ey = Height, flag_cropped = TRUE;
   int d_sx = s_sx - sr_x;
   int d_sy = s_sy - s_rect.Y;
   if (flag_cropped) {
      // d_buffer 영역 전체를 클리어한다.
      int ei = s_rect.Width * s_rect.Height;
      ushort* t_buffer = (ushort*)d_buffer;
      for (i = 0; i < ei; i++) t_buffer[i] = 0x8000;
   }
   int si_sll = SrcImgWidth  * 2;
   int di_sll = s_rect.Width * 2;
   int pi_sll = Width * 2;
   PICTE* picte0    = Array[s_sy][s_sx].Pair;
   byte*  d_buffer0 = d_buffer + d_sy * di_sll + d_sx * 2;
   for (y = s_sy; y < s_ey; y++) {
      PICTE* picte    = picte0;
      byte*  d_buffer = d_buffer0;
      for (x = s_sx; x < s_ex; x += 2) {
         no1 = picte[0].SrcImgNo;
         if (no1 >= 0) {
            float a = ibc_params1[no1];
            float b = ibc_params2[no1];
            s_buffer = s_buffers[no1];
            iw   = picte[0].IWeights;
            sp1  = s_buffer + picte[0].Y * si_sll + (picte[0].X << 1);
            sp2  = sp1 + si_sll;
            ip01 = (iw[0] * sp1[0] + iw[1] * sp2[0] + iw[2] * sp1[2] + iw[3] * sp2[2]) >> 8;
            ip01 = (short)(a * ip01 + b);
            if (ip01 < 0) ip01 = 0;
            else if (ip01 > 255) ip01 = 255;
            if (picte[0].X & 0x0001) u0 = sp1[-1], v0 = sp1[1];
            else u0 = sp1[1], v0 = sp1[3];
            iw   = picte[2].IWeights;
            sp1  = s_buffer + picte[2].Y * si_sll + (picte[2].X << 1);
            sp2  = sp1 + si_sll;
            ip02 = (iw[0] * sp1[0] + iw[1] * sp2[0] + iw[2] * sp1[2] + iw[3] * sp2[2]) >> 8;
            ip02 = (short)(a * ip02 + b);
            if (ip02 < 0) ip02 = 0;
            else if (ip02 > 255) ip02 = 255;
         }
         no2 = picte[1].SrcImgNo;
         if (no2 >= 0) {
            float a = ibc_params1[no2];
            float b = ibc_params2[no2];
            s_buffer = s_buffers[no2];
            iw   = picte[1].IWeights;
            sp1  = s_buffer + picte[1].Y * si_sll + (picte[1].X << 1);
            sp2  = sp1 + si_sll;
            ip11 = (iw[0] * sp1[0] + iw[1] * sp2[0] + iw[2] * sp1[2] + iw[3] * sp2[2]) >> 8;
            ip11 = (short)(a * ip11 + b);
            if (ip11 < 0) ip11 = 0;
            else if (ip11 > 255) ip11 = 255;
            if (picte[1].X & 0x0001) u1 = sp1[-1], v1 = sp1[1];
            else u1 = sp1[1], v1 = sp1[3];
            iw   = picte[3].IWeights;
            sp1  = s_buffer + picte[3].Y * si_sll + (picte[3].X << 1);
            sp2  = sp1 + si_sll;
            ip12 = (iw[0] * sp1[0] + iw[1] * sp2[0] + iw[2] * sp1[2] + iw[3] * sp2[2]) >> 8;
            ip12 = (short)(a * ip12 + b);
            if (ip12 < 0) ip12 = 0;
            else if (ip12 > 255) ip12 = 255;
         }
         if (no1 >= 0 && no2 >= 0) {
            d_buffer[0] = (byte)((picte[0].BWeight * ip01 + picte[1].BWeight * ip11) >> 8);
            d_buffer[2] = (byte)((picte[2].BWeight * ip02 + picte[3].BWeight * ip12) >> 8);
            d_buffer[1] = (byte)(((ushort)u0 + u1) >> 1);
            d_buffer[3] = (byte)(((ushort)v0 + v1) >> 1);
         }
         else if (no1 >= 0) {
            d_buffer[0] = (byte)ip01;
            d_buffer[2] = (byte)ip02;
            d_buffer[1] = u0;
            d_buffer[3] = v0;
         }
         else if (no2 >= 0) {
            d_buffer[0] = (byte)ip11;
            d_buffer[2] = (byte)ip12;
            d_buffer[1] = u1;
            d_buffer[3] = v1;
         }
         else {
            d_buffer[0] = d_buffer[2] = 0;
            d_buffer[1] = d_buffer[3] = 128;
         }
         d_buffer += 4;
         picte    += 4;
      }
      d_buffer0 += di_sll;
      picte0    += pi_sll;
   }
   return (DONE);
}

 int PanImageCompositionTable::Perform_YUY2 (byte** s_buffers,IBox2D& s_rect,IPoint2D& d_pos,byte* d_buffer,ISize2D& di_size)

{
   int    x,y;
   byte   u0,v0,u1,v1;
   byte   *sp1,*sp2,*s_buffer;
   short  no1,no2;
   short  ip01,ip02,ip11,ip12;
   ushort *iw;

   if (Array == NULL || !NumSrcImages) return (1);
   FArray1D ibc_params1(NumSrcImages);
   FArray1D ibc_params2(NumSrcImages);
   if (Flag_IBCEnabled && !!IBCParams1 && !!IBCParams2) {
      ibc_params1 = IBCParams1;
      ibc_params2 = IBCParams2;
   }
   else {
      ibc_params1.Set   (0,NumSrcImages,1.0f);
      ibc_params2.Clear (   );
   }
   int sr_x = s_rect.X;
   if (sr_x < 0) {
      if (abs (sr_x) % 2) sr_x++;
   }
   int s_sx = sr_x;
   int s_sy = s_rect.Y;
   int s_ex = s_sx + s_rect.Width;
   int s_ey = s_sy + s_rect.Height;
   if (s_sx < 0) s_sx = 0;
   if (s_sy < 0) s_sy = 0;
   if (s_ex > Width ) s_ex = Width;
   if (s_ey > Height) s_ey = Height;
   int d_sx = s_sx - sr_x     + d_pos.X;
   int d_sy = s_sy - s_rect.Y + d_pos.Y;
   int si_sll = SrcImgWidth   * 2;
   int di_sll = di_size.Width * 2;
   int pi_sll = Width * 2;
   PICTE* picte0  = Array[s_sy][s_sx].Pair;
   byte*  d_buffer0 = d_buffer + d_sy * di_sll + d_sx * 2;
   for (y = s_sy; y < s_ey; y++) {
      PICTE* picte   = picte0;
      byte* d_buffer = d_buffer0;
      for (x = s_sx; x < s_ex; x += 2) {
         no1 = picte[0].SrcImgNo;
         if (no1 >= 0) {
            float a  = ibc_params1[no1];
            float b  = ibc_params2[no1];
            s_buffer = s_buffers[no1];
            iw   = picte[0].IWeights;
            sp1  = s_buffer + picte[0].Y * si_sll + (picte[0].X << 1);
            sp2  = sp1 + si_sll;
            ip01 = (iw[0] * sp1[0] + iw[1] * sp2[0] + iw[2] * sp1[2] + iw[3] * sp2[2]) >> 8;
            ip01 = (short)(a * ip01 + b);
            if (ip01 < 0) ip01 = 0;
            else if (ip01 > 255) ip01 = 255;
            if (picte[0].X & 0x0001) u0 = sp1[-1], v0 = sp1[1];
            else u0 = sp1[1], v0 = sp1[3];
            iw   = picte[2].IWeights;
            sp1  = s_buffer + picte[2].Y * si_sll + (picte[2].X << 1);
            sp2  = sp1 + si_sll;
            ip02 = (iw[0] * sp1[0] + iw[1] * sp2[0] + iw[2] * sp1[2] + iw[3] * sp2[2]) >> 8;
            ip02 = (short)(a * ip02 + b);
            if (ip02 < 0) ip02 = 0;
            else if (ip02 > 255) ip02 = 255;
         }
         no2 = picte[1].SrcImgNo;
         if (no2 >= 0) {
            float a = ibc_params1[no2];
            float b = ibc_params2[no2];
            s_buffer = s_buffers[no2];
            iw   = picte[1].IWeights;
            sp1  = s_buffer + picte[1].Y * si_sll + (picte[1].X << 1);
            sp2  = sp1 + si_sll;
            ip11 = (iw[0] * sp1[0] + iw[1] * sp2[0] + iw[2] * sp1[2] + iw[3] * sp2[2]) >> 8;
            ip11 = (short)(a * ip11 + b);
            if (ip11 < 0) ip11 = 0;
            else if (ip11 > 255) ip11 = 255;
            if (picte[1].X & 0x0001) u1 = sp1[-1], v1 = sp1[1];
            else u1 = sp1[1], v1 = sp1[3];
            iw   = picte[3].IWeights;
            sp1  = s_buffer + picte[3].Y * si_sll + (picte[3].X << 1);
            sp2  = sp1 + si_sll;
            ip12 = (iw[0] * sp1[0] + iw[1] * sp2[0] + iw[2] * sp1[2] + iw[3] * sp2[2]) >> 8;
            ip12 = (short)(a * ip12 + b);
            if (ip12 < 0) ip12 = 0;
            else if (ip12 > 255) ip12 = 255;
         }
         if (no1 >= 0 && no2 >= 0) {
            d_buffer[0] = (byte)((picte[0].BWeight * ip01 + picte[1].BWeight * ip11) >> 8);
            d_buffer[2] = (byte)((picte[2].BWeight * ip02 + picte[3].BWeight * ip12) >> 8);
            d_buffer[1] = (byte)(((ushort)u0 + u1) >> 1);
            d_buffer[3] = (byte)(((ushort)v0 + v1) >> 1);
         }
         else if (no1 >= 0) {
            d_buffer[0] = (byte)ip01;
            d_buffer[2] = (byte)ip02;
            d_buffer[1] = u0;
            d_buffer[3] = v0;
         }
         else if (no2 >= 0) {
            d_buffer[0] = (byte)ip11;
            d_buffer[2] = (byte)ip12;
            d_buffer[1] = u1;
            d_buffer[3] = v1;
         }
         else {
            d_buffer[0] = d_buffer[2] = 0;
            d_buffer[1] = d_buffer[3] = 128;
         }
         d_buffer += 4;
         picte    += 4;
      }
      d_buffer0 += di_sll;
      picte0    += pi_sll;
   }
   return (DONE);
}

 int PanImageCompositionTable::ReadFile (const char *file_name)
 
{
   int  width,height;
   char identifier[] = IDENTIFIER_PICT;

   FileIO file(file_name,FIO_BINARY,FIO_READ);
   if (!file) return (1);
   CArray1D buffer(sizeof(identifier));
   if (file.Read ((byte *)(char *)buffer,1,sizeof(identifier))) return (2);
   buffer[buffer.Length - 1] = 0;
   if (strcmp (buffer,identifier)) return (3);
   if (file.Read ((byte *)&ProjectionType,1,sizeof(int))) return (4);
   if (file.Read ((byte *)&width ,1,sizeof(int))) return (5);
   if (file.Read ((byte *)&height,1,sizeof(int))) return (6);
   if (file.Read ((byte *)&NumSrcImages,1,sizeof(int))) return (7);
   if (file.Read ((byte *)&SrcImgWidth,1,sizeof(int))) return (8);
   if (file.Read ((byte *)&SrcImgHeight,1,sizeof(int))) return (9);
   if (file.Read ((byte *)&HFOV,1,sizeof(float))) return (10);
   if (Array2D<PICTEP>::Create (width,height) != DONE) return (11);
   if (file.Read ((byte *)Array[0],Width * Height,sizeof(PICTEP))) return (12);
   return (DONE);
}

 int PanImageCompositionTable::WriteFile (const char *file_name)

{
   char identifier[] = IDENTIFIER_PICT;

   FileIO file(file_name,FIO_BINARY,FIO_CREATE);
   if (!file) return (1);
   if (file.Write ((byte *)identifier,1,sizeof(identifier))) return (2);
   if (file.Write ((byte *)&ProjectionType,1,sizeof(int))) return (3);
   if (file.Write ((byte *)&Width ,1,sizeof(int))) return (4);
   if (file.Write ((byte *)&Height,1,sizeof(int))) return (5);
   if (file.Write ((byte *)&NumSrcImages,1,sizeof(int))) return (6);
   if (file.Write ((byte *)&SrcImgWidth,1,sizeof(int))) return (7);
   if (file.Write ((byte *)&SrcImgHeight,1,sizeof(int))) return (8);
   if (file.Write ((byte *)&HFOV,1,sizeof(float))) return (9);
   if (file.Write ((byte *)Array[0],Width * Height,sizeof(PICTEP))) return (10);
   return (DONE);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: CThread_PIC
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_WIN32CL)

 CThread_PIC::CThread_PIC (   )

{
   ThreadIdx     = -1;
   ParImgWidth   = 0;
   SrcImgBuffers = NULL;
   DstImgBuffer  = NULL;
   PICT          = NULL;
}

 void CThread_PIC::ThreadProc (   )

{
   if (ThreadIdx == -1 || ParImgWidth == 0 || PICT == NULL) return;
   IBox2D   s_rect(ThreadIdx * ParImgWidth,0,ParImgWidth,PICT->Height);
   IPoint2D d_pos (s_rect.X,s_rect.Y);
   ISize2D  di_size(PICT->Width,PICT->Height);
   HANDLE events[2];
   events[0] = (HANDLE)Event_Stop;
   events[1] = (HANDLE)Event_Start;
   Event_Start.ResetEvent (   );
   Event_End.ResetEvent   (   );
   while (TRUE) {
      DWORD result = WaitForMultipleObjects (2,events,FALSE,INFINITE);
      if (result == WAIT_OBJECT_0) break;       // Stop  이벤트인 경우
      else if (result == (WAIT_OBJECT_0 + 1)) { // Start 이벤트인 경우
         if (SrcImgBuffers != NULL && DstImgBuffer != NULL) {
            PICT->Perform_YUY2 (SrcImgBuffers,s_rect,d_pos,DstImgBuffer,di_size);
         }
         Event_End.SetEvent (   );
      }
   }
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: PanImageComposition_MT
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_WIN32CL)

 PanImageComposition_MT::PanImageComposition_MT (   )

{
   _Init (   );
}

 PanImageComposition_MT::~PanImageComposition_MT (   )

{
   Close (   );
}

 void PanImageComposition_MT::_Init (   )

{
   NumSrcImages = 0;
   SrcImgSize.Clear (   );
   PanImgSize.Clear (   );
}

 void PanImageComposition_MT::Close (   )

{
   Threads.Delete (   );
   EndEvents.Delete (   );
   PICT.Delete (   );
   _Init (   );
}

 int PanImageComposition_MT::Initialize (int n_threads)

{
   int i;
   
   if (!PICT) return (1);
   Threads.Delete   (   );
   EndEvents.Delete (   );
   EndEvents.Create (n_threads);
   Threads.Create   (n_threads);
   int pi_width = PICT.Width / n_threads;
   for (i = 0; i < n_threads; i++) {
      EndEvents[i]           = (HANDLE)Threads[i].Event_End;
      Threads[i].ThreadIdx   = i;
      Threads[i].ParImgWidth = pi_width;
      Threads[i].PICT        = &PICT;
      Threads[i].StartThread (   );
   }
   return (DONE);
}

 int PanImageComposition_MT::LoadPICT (const char* file_name)

{
   Close (   );
   int e_code = PICT.ReadFile (file_name);
   if (e_code) return (e_code);
   NumSrcImages = PICT.NumSrcImages;
   PanImgSize(PICT.Width,PICT.Height);
   SrcImgSize(PICT.SrcImgWidth,PICT.SrcImgHeight);
   return (DONE);
}

 int PanImageComposition_MT::Perform_YUY2 (byte** s_buffers,byte* d_buffer)

{
   int i;
   
   if (!PICT   ) return (1);
   if (!Threads) return (2);
   for (i = 0; i < Threads.Length; i++) {
      Threads[i].SrcImgBuffers = s_buffers;
      Threads[i].DstImgBuffer  = d_buffer;
      Threads[i].Event_End.ResetEvent (   );
      Threads[i].Event_Start.SetEvent (   );
   }
   WaitForMultipleObjects (EndEvents.Length,EndEvents,TRUE,INFINITE);
   return (DONE);
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: CylindricalPanorama
//
///////////////////////////////////////////////////////////////////////////////

 CylindricalPanorama::CylindricalPanorama (   )
 
{
   Flag_Closed         = FALSE;
   Flag_FixFocalLength = TRUE;
   OverlapWidth        = PAN_OVERLAP_WIDTH;
   PIWidthScale        = PAN_WIDTH_SCALE;
   _Init (   );
}

 void CylindricalPanorama::_Init (   )
 
{
   NumCameras    = 0;
   CamImgWidth   = 0;
   CamImgHeight  = 0;
   FocalLength   = 0.0;
   Kappa1        = 0.0;
   Kappa2        = 0.0;
   SrcImgWidth1  = 0;
   SrcImgHeight1 = 0;
   SrcImgWidth2  = 0;
   SrcImgHeight2 = 0;
   FocalLength1  = 0.0;
   ValidRegion_SI.Clear (   );
   ValidRegion_PI.Clear (   );
   CamImgCenter.Clear   (   );
   LFMPos.Clear         (   );
}

 void CylindricalPanorama::Close (   )

{
   Mat_H.Delete    (   );
   Mats_H1.Delete  (   );
   Mats_H2.Delete  (   );
   Mats_K.Delete   (   );
   Mats_R.Delete   (   );
   CPTable.Delete  (   );
   LDCTable.Delete (   );
   PanImage.Delete (   );
   _Init (   );
}

 void CylindricalPanorama::CorrectCameraParameters (   )
 
{
   int i;

   Matrix mat_H1 = IM(3);
   mat_H1[0][2]  = -ValidRegion_SI.X;
   mat_H1[1][2]  = -ValidRegion_SI.Y;
   Matrix mat_H2 = IM(3);
   mat_H2[0][2]  =  ValidRegion_SI.X;
   mat_H2[1][2]  =  ValidRegion_SI.Y;
   for (i = 0; i < Mats_H2.Length; i++) Mats_H1[i] = mat_H2 * Mats_H2[i] * mat_H1;
   FocalLength1 = GetFocalLength (Mats_H1[0],SrcImgWidth1,SrcImgHeight1);
}

 void CylindricalPanorama::CreateCylindricalProjectionTable (Array1D<GImage> *m_images)

{
   int i;

   #if defined(__DEBUG_PAN)
   logd (">> Creating a cylindrical projection table: ");
   #endif
   CPTable.OverlapWidth = (int)OverlapWidth;
   CPTable.PIWidthScale = PIWidthScale;
   LensDistortionCorrectionTable *ldc_table;
   if (!LDCTable) ldc_table = NULL;
   else ldc_table = &LDCTable;
   CPTable.Initialize (Mats_K,Mats_R,CamImgWidth,CamImgHeight,ldc_table);
   for (i = 0; i < Mats_K.Length; i++) {
      #if defined(__DEBUG_PAN)
      logd ("[%d]",i + 1);
      #endif
      byte **mask;
      if (m_images == NULL) mask = NULL;
      else mask = (*m_images)[i];
      CPTable.Perform (i,mask);
   }
   if (Flag_Closed) {
      #if defined(__DEBUG_PAN)
      logd ("[Closing]");
      #endif
      CPTable.ClosePanorama (LFMPos,Mat_H);
   }
   #if defined(__DEBUG_PAN)
   logd ("[Blending]");
   #endif
   CPTable.GetBlendingWeights (   );   
   #if defined(__DEBUG_PAN)
   logd ("[Cropping]");
   #endif
   CPTable.CropNonimageArea (ValidRegion_PI);
   #if defined(__DEBUG_PAN)
   logd ("[Postprocessing]");
   #endif
   CPTable.PerformPostprocessing (   );
   #if defined(__DEBUG_PAN)
   logd ("... OK.\n");
   #endif
}

 void CylindricalPanorama::CreateLensDistortionCorrectionTable (   )

{
   LDCTable.Create (CamImgWidth,CamImgHeight,4,CamImgCenter,Kappa1,Kappa2);
   ValidRegion_SI = LDCTable.ValidRegion;
   SrcImgWidth1   = LDCTable.Width;
   SrcImgHeight1  = LDCTable.Height;
   SrcImgWidth2   = ValidRegion_SI.Width;
   SrcImgHeight2  = ValidRegion_SI.Height;
}

 void CylindricalPanorama::CreateMaskImages (Array1D<GImage> &s_images)

{
   int i;
   
   #if defined(__DEBUG_PAN)
   logd (">> Creating mask images: ");
   #endif
   for (i = 0; i < s_images.Length; i++) {
      #if defined(__DEBUG_PAN)
      logd ("[%d]",i + 1);
      #endif
      GImage gf_image(s_images[i].Width,s_images[i].Height);
      GaussianFiltering (s_images[i],1.5f,gf_image); // PARAMETERS
      Binarize (gf_image,(byte)50,s_images[i]);      // PARAMETERS
   }
   #if defined(__DEBUG_PAN)
   logd ("... Done.\n");
   #endif
}

 void CylindricalPanorama::CreatePanoramicImage (Array1D<BGRImage> &s_images)
 // s_images: 렌즈 왜곡이 제거되고 크롭핑 되기 전인 영상
{
   int i;
   
   #if defined(__DEBUG_PAN)
   logd (">> Creating a panoramic image: ");
   #endif
   CylindricalProjection c_prj;
   c_prj.OverlapWidth = OverlapWidth;
   c_prj.PIWidthScale = PIWidthScale;
   c_prj.Initialize (Mats_K,Mats_R,SrcImgWidth1,SrcImgHeight1,PanImage);
   for (i = 0; i < s_images.Length; i++) {
      #if defined(__DEBUG_PAN)
      logd ("[%d]",i + 1);
      #endif
      c_prj.Perform (s_images[i],i);
   }
   if (Flag_Closed) {
      #if defined(__DEBUG_PAN)
      logd ("[Closing]");
      #endif
      c_prj.ClosePanorama (s_images[0],0.3f,0.2f); // PARAMETERS
      Mat_H  = c_prj.Mat_H;
      LFMPos = c_prj.LFMPos;
   }
   #if defined(__DEBUG_PAN)
   logd ("[Cropping]");
   #endif
   c_prj.CropNonimageArea (   );
   ValidRegion_PI = c_prj.ValidRegion;
   #if defined(__DEBUG_PAN)
   logd ("... OK.\n");
   logd ("   HFOV = %f, VFOV = %f\n",c_prj.HFOV,c_prj.VFOV);
   #endif
}

 void CylindricalPanorama::EstimateFocalLength (GImage &r_image,GImage &s_image)
 // r_image,s_image: 렌즈 왜곡이 제거되고 크롭핑이 된 영상
{
   #if defined(__DEBUG_PAN) 
   logd (">> Estimating a focal length...\n");
   _DVX_PAN = _DVY_PAN = 0;
   _DebugView_PAN->Clear (   );
   float scale = GetResolutionReductionFactor (   );
   GImage t_image;
   t_image.Create ((int)(r_image.Width * scale),(int)(r_image.Height * scale));
   Reduce (r_image,t_image);
   _DebugView_PAN->DrawImage (t_image,_DVX_PAN,_DVY_PAN);
   _DVX_PAN += t_image.Width + 10;
   t_image.Create ((int)(s_image.Width * scale),(int)(s_image.Height * scale));
   Reduce (s_image,t_image);
   _DebugView_PAN->DrawImage (t_image,_DVX_PAN,_DVY_PAN);
   _DVX_PAN = 0;
   _DVY_PAN += t_image.Height + 10;
   #endif
   HTM_NCC2 htm;
   FPoint2D lt_overlap(1.05f,0.9f);
   FPoint2D rb_overlap(0.5f,0.9f);
   htm.Initialize (r_image,s_image,170,lt_overlap,rb_overlap); // PARAMETERS
   htm.Perform (2.0f,15,2); // PARAMETERS
   FPoint2D m_pos = htm.GetMatchingPosition (   );
   Matrix mat_H = IM(3);
   mat_H[0][2] = -m_pos.X;
   mat_H[1][2] = -m_pos.Y;
   HLIR_MSSD_2DProjective hlir_2dp;
   hlir_2dp.Initialize (r_image,s_image,mat_H,150); // PARAMETERS
   hlir_2dp.Perform (1.75f); // PARAMETERS
   hlir_2dp.GetHomography (mat_H);
   #if defined(__DEBUG_PAN)
   CombineTwoImages ((BGRImage)r_image,(BGRImage)s_image,mat_H,t_cimage,PM_CYLINDER,scale);
   _DebugView_PAN->DrawImage (t_cimage,_DVX_PAN,_DVY_PAN);
   _DVY_PAN += t_cimage.Height + 10;
   #endif
   FocalLength = GetFocalLength (mat_H,SrcImgWidth2,SrcImgHeight2);
   #if defined(__DEBUG_PAN)
   float f2    = 2.0f * FocalLength;
   float h_fov = 2.0 * (float)atan (SrcImgWidth2  / f2) * MC_RAD2DEG;
   float v_fov = 2.0 * (float)atan (SrcImgHeight2 / f2) * MC_RAD2DEG;
   logd ("   FocalLength = %f, HFOV = %f, VFOV = %f\n",FocalLength,h_fov,v_fov);
   #endif
}

 int CylindricalPanorama::GetLeftMargin (BGRImage &s_image)

{
   int x,y;
   
   int l_margin = 0;
   int n = s_image.Height * 3;
   for (x = 0; x < s_image.Width; x++) {
      int sum = 0;
      for (y = 0; y < s_image.Height; y++) {
         BGRPixel &p = s_image[y][x];
         sum += (int)p.R + p.G + p.B;
      }
      if ((sum / n) > 20) break; // PARAMETERS
      else l_margin++;
   }
   return (l_margin);
}

 void CylindricalPanorama::GetProjectionMatrices (   )

{
   int i;
   
   Array1D<Matrix> mats_Rr(Mats_H1.Length);
   for (i = 0; i < mats_Rr.Length; i++)
      GetRotationMatrix (Mats_H1[i],FocalLength1,FocalLength1,SrcImgWidth1,SrcImgHeight1,mats_Rr[i]);
// GetRotationMatrices (mats_Rr,Mats_R,RM_CENTER | RM_UNIFORM_TILT);
   GetRotationMatrices (mats_Rr,Mats_R,RM_UNIFORM_TILT);
   Mats_K.Create (Mats_R.Length);
   for (i = 0; i < Mats_K.Length; i++) {
      Mats_K[i].Create (3,3);
      GetCalibrationMatrix (FocalLength1,SrcImgWidth1,SrcImgHeight1,Mats_K[i]);
   }
}

 float CylindricalPanorama::GetResolutionReductionFactor (   )

{
   int d_size = 500; // PARAMETERS
   int size = GetMaximum (SrcImgWidth2,SrcImgHeight2);
   if (size < d_size) return (1.0f);
   else return ((float)d_size / size);
}

 int CylindricalPanorama::GetRightMargin (BGRImage &s_image)

{
   int x,y;
   
   int r_margin = 0;
   int n = s_image.Height * 3;
   for (x = s_image.Width - 1; x >= 0; x--) {
      int sum = 0;
      for (y = 0; y < s_image.Height; y++) {
         BGRPixel &p = s_image[y][x];
         sum += (int)p.R + p.G + p.B;
      }
      if ((sum / n) > 20) break; // PARAMETERS
      else r_margin++;
   }
   return (r_margin);
}

 void CylindricalPanorama::Initialize (int n_cameras,int si_width,int si_height,double f_length,double kappa1,double kappa2)

{
   Close (   );
   NumCameras    = n_cameras;
   CamImgWidth   = si_width;
   CamImgHeight  = si_height;
   FocalLength   = f_length;
   Kappa1        = kappa1;
   Kappa2        = kappa2;
   SrcImgWidth1  = si_width;
   SrcImgHeight1 = si_height;
   SrcImgWidth2  = si_width;
   SrcImgHeight2 = si_height;
   CamImgCenter(0.5f * si_width,0.5f * si_height);
   ValidRegion_SI(0,0,si_width,si_height);
   if (Kappa1 || Kappa2) CreateLensDistortionCorrectionTable (   );
   Mats_H1.Create (n_cameras - 1);
   Mats_H2.Create (Mats_H1.Length);
}

 int CylindricalPanorama::LoadCylindricalProjectionTable (const char *file_name)

{
   #if defined(__DEBUG_PAN)
   logd (">> Loading a cylindrical projection table... \n");
   #endif
   if (CPTable.ReadFile (file_name)) {
      #if defined(__DEBUG_PAN)
      logd ("Failed.\n");
      #endif
      return (1);
   }
   NumCameras   = CPTable.NumSrcImages;
   CamImgWidth  = CPTable.SrcImgWidth;
   CamImgHeight = CPTable.SrcImgHeight;
   ValidRegion_PI(0,0,CPTable.Width,CPTable.Height);
   #if defined(__DEBUG_PAN)
   logd ("OK.\n");
   logd ("   # of Cameras: %d\n",CPTable.NumSrcImages);
   logd ("   Source Image: %d x %d\n",CPTable.SrcImgWidth,CPTable.SrcImgHeight);
   logd ("   Panoramic Image: %d x %d\n",CPTable.Width,CPTable.Height);
   #endif
   return (DONE);
}

 int CylindricalPanorama::LoadLensDistortionCorrectionTable (const char *file_name)

{
   #if defined(__DEBUG_PAN)
   logd (">> Loading a lens distortion correction table... \n");
   #endif
   if (LDCTable.ReadFile (file_name)) {
      #if defined(__DEBUG_PAN)
      logd ("Failed.\n");
      #endif
      return (1);
   }
   ValidRegion_SI = LDCTable.ValidRegion;
   CamImgWidth    = LDCTable.SrcImgWidth;
   CamImgHeight   = LDCTable.SrcImgHeight;
   SrcImgWidth1   = LDCTable.Width;
   SrcImgHeight1  = LDCTable.Height;
   SrcImgWidth2   = ValidRegion_SI.Width;
   SrcImgHeight2  = ValidRegion_SI.Height;
   CamImgCenter   = LDCTable.ImageCenter;
   Kappa1         = LDCTable.Kappa1;
   Kappa2         = LDCTable.Kappa2;
   #if defined(__DEBUG_PAN)
   logd ("OK.\n");
   logd ("   Source Image: %d x %d\n",OrgImgWidth,OrgImgHeight);
   logd ("   Distortion-Free Image: %d x %d\n",SrcImgWidth1,SrcImgHeight1);
   logd ("   Cropped Distortion-Free Image: %d x %d\n",SrcImgWidth2,SrcImgHeight2);
   logd ("   Radial Distortion Parameters: Kappa1 = %e, Kappa2 = %e\n",Kappa1,Kappa2);
   #endif
   return (DONE);
}

 int CylindricalPanorama::LoadSourceImage (const char *file_name,BGRImage &d_image,int flag_crop)

{
   #if defined(__DEBUG_PAN)
   logd (">> Loading a source image [%s]... \n",file_name);
   #endif
   int e_code = DONE;
   if (!LDCTable) {
      if (d_image.ReadFile (file_name)) {
         e_code = 1;
         goto end;
      }
      if (d_image.Width != CamImgWidth || d_image.Height != CamImgHeight) {
         e_code = 2;
         goto end;
      }
   }
   else {
      BGRImage s_image;
      if (s_image.ReadFile (file_name)) {
         e_code = 1;
         goto end;
      }
      if (s_image.Width != CamImgWidth || s_image.Height != CamImgHeight) {
         e_code = 2;
         goto end;
      }
      int l_margin = GetLeftMargin  (s_image);
      int r_margin = GetRightMargin (s_image);
      if (l_margin) s_image.Set (0,0,l_margin,s_image.Height,BGRPixel(0,0,0));
      if (r_margin) s_image.Set (s_image.Width - r_margin,0,r_margin,s_image.Height,BGRPixel(0,0,0));
      if (flag_crop) {
         BGRImage t_image;
         LDCTable.Warp (s_image,t_image);
         d_image = t_image.Extract (ValidRegion_SI);
      }
      else {
         LDCTable.Warp (s_image,d_image);
         // 왜곡이 제거된 영상은 상하좌우로 검정 영역이 있으므로 검정 영역이 파노라마 영상 생성 시
         // 개입되지 않도록 알파 채널을 설정한다.
         GImage t_image(s_image.Width,s_image.Height);
         t_image.Set (0,0,t_image.Width,t_image.Height,PG_WHITE);
         d_image.CreateAlphaChannel (   );
         LDCTable.Warp (t_image,*d_image.AlphaChannel);
      }
   }
   end:;
   #if defined(__DEBUG_PAN)
   if (e_code == DONE) logd ("OK.\n");
   else logd ("Failed.\n");
   #endif
   return (e_code);
}

 int CylindricalPanorama::LoadSourceImage (const char *file_name,GImage &d_image,int flag_crop)

{
   BGRImage d_cimage;
   int e_code = LoadSourceImage (file_name,d_cimage,flag_crop);
   if (e_code) return (e_code);
   d_image = d_cimage;
   return (DONE);
}

 int CylindricalPanorama::PerformLocalImageRegistration (GImage &r_image,GImage &s_image,int img_no)
 // r_image, s_image: cropped images.
{
   double psi,theta,phi;
   
   #if defined(__DEBUG_PAN)
   logd (">> Performing local image registration [%d]...\n",img_no);
   #endif
   if (!FocalLength) {
      #if defined(__DEBUG_PAN)
      logd ("   No initial focal length.\n");
      #endif
      return (1);   
   }
   GImage r_image2,s_image2;
   FPoint2D lt_pos,rb_pos;
   PerformCylindricalProjection (r_image,FocalLength,lt_pos,rb_pos,r_image2);
   PerformCylindricalProjection (s_image,FocalLength,lt_pos,rb_pos,s_image2);
   #if defined(__DEBUG_PAN)
   X = Y = 0;
   _DebugView_PAN->Clear (   );
   float scale = GetResolutionReductionFactor (   );
   GImage t_image;
   t_image.Create ((int)(r_image2.Width * scale),(int)(r_image2.Height * scale));
   Reduce (r_image2,t_image);
   _DebugView_PAN->DrawImage (t_image,_DVX_PAN,_DVY_PAN);
   _DVX_PAN += t_image.Width + 10;
   t_image.Create ((int)(s_image2.Width * scale),(int)(s_image2.Height * scale));
   Reduce (s_image2,t_image);
   _DebugView_PAN->DrawImage (t_image,_DVX_PAN,_DVY_PAN);
   _DVX_PAN = 0;
   _DVX_PAN += t_image.Height + 10;
   t_image.Delete (   );
   #endif
   HTM_NCC2 htm;
   FPoint2D lt_overlap(1.5f,0.8f);
   FPoint2D rb_overlap(0.2f,0.8f);
   htm.Initialize (r_image2,s_image2,200,lt_overlap,rb_overlap); // PARAMETERS
   htm.Perform (2.0f,15,2); // PARAMETERS
   FPoint2D m_pos = htm.GetMatchingPosition (   );
   Matrix mat_H = IM(3);
   #if defined(__DEBUG_PAN)
   BGRImage t_cimage;
   mat_H[0][2] = -m_pos.X;
   mat_H[1][2] = -m_pos.Y;
   CombineTwoImages ((BGRImage)r_image2,(BGRImage)s_image2,mat_H,t_cimage,PM_PLANE,scale);
   _DebugView_PAN->DrawImage (t_cimage,_DVX_PAN,_DVY_PAN);
   _DVY_PAN += t_cimage.Height + 10;
   t_cimage.Delete (   );
   #endif
   phi = 0.0;
   GetRotationAngles_Cylinder (0.0,-m_pos.X,-m_pos.Y,lt_pos,rb_pos,r_image2.Width,r_image2.Height,psi,theta);
   #if defined(__DEBUG_PAN)
   logd ("   Initial Parameter Values (3D Rotational Transformation:)\n");
   logd ("   * Rotation Angles: %f,%f,%f\n",psi * MC_RAD2DEG,theta * MC_RAD2DEG,phi * MC_RAD2DEG);
   #endif
   if (Flag_FixFocalLength) {
      HLIR_MSSD_3DRotational_XYZ hlir_3dr;
      hlir_3dr.Initialize (r_image,s_image,FocalLength,psi,theta,phi,150); // PARAMETERS
      hlir_3dr.Perform (1.75f);
      hlir_3dr.GetParameters (psi,theta,phi);
      hlir_3dr.GetHomography (mat_H);
   }
   else {
      HLIR_MSSD_3DRotational_FXYZ hlir_3dr;
      hlir_3dr.Initialize (r_image,s_image,FocalLength,psi,theta,phi,150); // PARAMETERS
      hlir_3dr.Perform (1.75f);
      double f_length;
      hlir_3dr.GetParameters (f_length,psi,theta,phi);
      FocalLength = f_length;
      hlir_3dr.GetHomography (mat_H);
   }
   #if defined(__DEBUG_PAN)
   logd ("   Parameter Values after Optimization (3D Rotational Transformation):\n");
   logd ("   * Focal Length: %f\n",FocalLength);
   logd ("   * Rotation Angles: %f,%f,%f\n",psi * MC_RAD2DEG,theta * MC_RAD2DEG,phi * MC_RAD2DEG);
   CombineTwoImages ((BGRImage)r_image,(BGRImage)s_image,mat_H,t_cimage,PM_CYLINDER,scale);
   _DebugView_PAN->DrawImage (t_cimage,_DVX_PAN,_DVY_PAN);
   _DVY_PAN += t_cimage.Height;
   #endif
   Mats_H2[img_no] = mat_H;
   return (DONE);
}

 void CylindricalPanorama::RenderPanoramicImage (Array1D<BGRImage> &s_images)

{
   PanImageCompositionTable pict;
   pict.Create  (CPTable);
   pict.Perform (s_images,PanImage);
}

 int CylindricalPanorama::SaveCylindricalProjectionTable (const char *file_name,int si_width,int si_height,int cpt_width,int cpt_height,int flag_rotation)

{
   int r_code;

   CylindricalProjectionTable cpt1;
   cpt1.Create (cpt_width,cpt_height);
   cpt1.Resize (CPTable);
   if (flag_rotation) {
      CylindricalProjectionTable cpt2;
      cpt2.Create (cpt1.Width,cpt1.Height);
      cpt2.Rotate180Degrees (cpt1);
      r_code = cpt2.WriteFile (file_name);
   }
   else r_code = cpt1.WriteFile (file_name);
   #if defined(__DEBUG_PAN)
   logd (">> Saved Cylindrical Projeciton Table: %s\n",file_name);
   logd ("   Source Image: %d x %d\n",si_width,si_height);
   logd ("   Panoramic Image: %d x %d\n",cpt_width,cpt_height);
   #endif
   return (r_code);
}

 int CylindricalPanorama::SaveLensDistortionCorrectionTable (const char *file_name)

{
   return (LDCTable.WriteFile (file_name));
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: PanoramaToPTZCamera
//
///////////////////////////////////////////////////////////////////////////////

 PanoramaToPTZCamera::PanoramaToPTZCamera (   )
 
{
   _Init (   );
}

 void PanoramaToPTZCamera::_Init (   )
 
{
   SrcImgWidth       = 0;
   SrcImgHeight      = 0;
   PanImgWidth       = 0;
   PanImgHeight      = 0;
   SrcImgFocalLength = 0.0f;
   PanImgHFOV        = 0.0f;
}

 int PanoramaToPTZCamera::Calibrate (   )
 
{
   int   x,y;
   int   nx = 8,ny = 4;
   float pan_pos,tilt_pos;
   
   if (PICT == NULL) return (1);
   M2SC.Initialize (PICT->Width,PICT->Height,nx * ny + 1);
   int dx = PICT->Width  / (nx + 1);
   int dy = PICT->Height / (ny + 1);
   int ex = dx * nx;
   int ey = dy * ny;
   int n  = 1;
   for (y = dy; y <= ey; y += dy) {
      for (x = dx; x <= ex; x += dx) {
         M2SC.CalibPoints[n]((float)x,(float)y);
         ConvertPixelPosToPTPos (x,y,pan_pos,tilt_pos);
         M2SC.PTZPositions[n](pan_pos,tilt_pos,1.0f);
         n++;
      } 
   }
   int cx = PICT->Width  / 2;
   int cy = PICT->Height / 2;
   M2SC.CalibPoints[0]((float)cx,(float)cy);
   ConvertPixelPosToPTPos (cx,cy,pan_pos,tilt_pos);
   M2SC.PTZPositions[0](pan_pos,tilt_pos,1.0f);
   M2SC.FocalLength      = SrcImgFocalLength;
   M2SC.HFOV             = PanImgHFOV;
   M2SC.MCTiltPos        = PanScanTiltPos;
   M2SC.RotationAngles.X = 0.0f;
   M2SC.RotationAngles.Y = pan_pos;
   M2SC.RotationAngles.Z = 0.0f;
   logd (">> Before Calibration\n");
   logd ("   * Focal Length     : %6.1f\n",M2SC.FocalLength);
   logd ("   * HFOV             : %6.1f\n",M2SC.HFOV);
   logd ("   * Rolling Angle    : %6.1f\n",M2SC.RotationAngles.X);
   logd ("   * Panning Angle    : %6.1f\n",M2SC.RotationAngles.Y);
   logd ("   * Tilting Angle    : %6.1f\n",M2SC.RotationAngles.Z);
   if (M2SC.Calibrate (   )) return (2);
   PanImgFocalLength = M2SC.FocalLength;
   PanImgHFOV        = M2SC.HFOV;
   logd (">> After Calibration\n");
   logd ("   * Focal Length     : %6.1f\n",M2SC.FocalLength);
   logd ("   * HFOV             : %6.1f\n",M2SC.HFOV);
   logd ("   * Rolling Angle    : %6.1f\n",M2SC.RotationAngles.X);
   logd ("   * Panning Angle    : %6.1f\n",M2SC.RotationAngles.Y);
   logd ("   * Tilting Angle    : %6.1f\n",M2SC.RotationAngles.Z);
   return (DONE);
}

 void PanoramaToPTZCamera::Close (   )
 
{
   Mats_IP.Delete (   );
   M2SC.Close (   );
   _Init (   );
}

 int PanoramaToPTZCamera::ConvertPixelPosToPTPos (int px,int py,float &pan_pos,float &tilt_pos)
 
{
   pan_pos = tilt_pos = 0.0f;
   if (PICT == NULL) return (1);
   if (px < 0 || px >= PICT->Width ) return (2);
   if (py < 0 || py >= PICT->Height) return (3);
   int si_no,si_x,si_y;
   PICTE *picte = (*PICT)[py][px].Pair;
   if (picte[0].SrcImgNo != -1) {
      si_no = picte[0].SrcImgNo;
      si_x  = picte[0].X;
      si_y  = picte[0].Y;
   }
   else if (picte[1].SrcImgNo != -1) {
      si_no = picte[1].SrcImgNo;
      si_x  = picte[1].X;
      si_y  = picte[1].Y;
   }
   else return (4);
   Matrix vec_p(3);
   vec_p(0) = (double)si_x;
   vec_p(1) = (double)si_y;
   vec_p(2) = 1.0;
   Matrix vec_X = Nor(Mats_IP[si_no] * vec_p);
   pan_pos  = MC_RAD2DEG * (float)atan2 (vec_X(0),vec_X(2));
   tilt_pos = MC_RAD2DEG * (float)asin  (vec_X(1));
   if (pan_pos < 0.0f  ) pan_pos += 360.0f;
   if (pan_pos > 360.0f) pan_pos -= 360.0f;
   return (DONE);
}

 void PanoramaToPTZCamera::GetPixelPos (float pan_pos,float tilt_pos,int &px,int &py)
 
{
   PTZPosition ptz_pos(pan_pos,tilt_pos,1.0f);
   FPoint2D px_pos = M2SC.GetPixelPosition (ptz_pos);
   px = (int)(px_pos.X + 0.5f);
   py = (int)(px_pos.Y + 0.5f);
}

 void PanoramaToPTZCamera::GetPTPos (int px,int py,float &pan_pos,float &tilt_pos)
 
{
   FPoint2D px_pos((float)px,(float)py);
   PTZPosition pt_pos = M2SC.GetPTPosition (px_pos);
   pan_pos  = pt_pos.PanAngle;
   tilt_pos = pt_pos.TiltAngle;
}

 void PanoramaToPTZCamera::GetPTZPos (IBox2D &s_rect,float &pan_pos,float &tilt_pos,float &zoom_pos)
 
{
   float f_length;
   
   int cx = s_rect.X + s_rect.Width  / 2;
   int cy = s_rect.Y + s_rect.Height / 2;
   GetPTPos (cx,cy,pan_pos,tilt_pos);
   if (s_rect.Width >= s_rect.Height) {
      float hfov = MC_DEG2RAD * (float)s_rect.Width / PanImgWidth * PanImgHFOV;
      f_length = SrcImgWidth / (2.0f * tan (0.5f * hfov));
   }
   else f_length = (float)PanImgFocalLength * SrcImgHeight / s_rect.Height;
   zoom_pos = f_length / (SrcImgFocalLength / PanScanZoomPos);
}

 void PanoramaToPTZCamera::GetViewRect (float pan_pos,float tilt_pos,float zoom_pos,IBox2D &d_rect)
 
{
   int cx,cy;

   float f_length = (SrcImgFocalLength / PanScanZoomPos) * zoom_pos;
   float hfov     = 2.0f * MC_RAD2DEG * atan (SrcImgWidth / (2.0f * f_length));
   d_rect.Width   = (int)(hfov / PanImgHFOV * PanImgWidth + 0.5f);
   d_rect.Height  = (int)(SrcImgHeight * PanImgFocalLength / f_length + 0.5f);
   GetPixelPos (pan_pos,tilt_pos,cx,cy);
   d_rect.X       = cx - d_rect.Width  / 2;
   d_rect.Y       = cy - d_rect.Height / 2;
}

 void PanoramaToPTZCamera::Initialize (PanImageCompositionTable &pict,PTZPArray1D &cp_array,float f_length,float tilt_pos,float zoom_pos)
 
{
   int i;
   
   Close (   );
   PICT             = &pict;
   SrcImgWidth       = pict.SrcImgWidth;
   SrcImgHeight      = pict.SrcImgHeight;
   PanImgWidth       = pict.Width;
   PanImgHeight      = pict.Height;
   SrcImgFocalLength = f_length;
   PanScanTiltPos    = tilt_pos;
   PanScanZoomPos    = zoom_pos;
   PanImgFocalLength = f_length;
   PanImgHFOV        = pict.HFOV;
   Matrix mat_K(3,3),mat_R(3,3);
   Mats_IP.Create (cp_array.Length);
   for (i = 0; i < cp_array.Length; i++) {
      GetCalibrationMatrix (f_length,pict.SrcImgWidth,pict.SrcImgHeight,mat_K);
      GetRotationMatrix_PT (MC_DEG2RAD * cp_array[i].PanAngle,MC_DEG2RAD * cp_array[i].TiltAngle,mat_R);
      Mats_IP[i] = Inv(mat_K * mat_R);
   }
}

 int PanoramaToPTZCamera::ReadFile (FileIO &file)
 
{
   Close (   );
   if (M2SC.ReadFile (file)) return (1);
   if (file.Read ((byte*)&SrcImgWidth      ,1,sizeof(SrcImgWidth      ))) return (2);
   if (file.Read ((byte*)&SrcImgHeight     ,1,sizeof(SrcImgHeight     ))) return (3);
   if (file.Read ((byte*)&PanImgWidth      ,1,sizeof(PanImgWidth      ))) return (4);
   if (file.Read ((byte*)&PanImgHeight     ,1,sizeof(PanImgHeight     ))) return (5);
   if (file.Read ((byte*)&SrcImgFocalLength,1,sizeof(SrcImgFocalLength))) return (6);
   if (file.Read ((byte*)&PanScanTiltPos   ,1,sizeof(PanScanTiltPos   ))) return (7);
   if (file.Read ((byte*)&PanScanZoomPos   ,1,sizeof(PanScanZoomPos   ))) return (8);
   if (file.Read ((byte*)&PanImgFocalLength,1,sizeof(PanImgFocalLength))) return (9);
   if (file.Read ((byte*)&PanImgHFOV       ,1,sizeof(PanImgHFOV       ))) return (10);
   return (DONE);
}

 int PanoramaToPTZCamera::ReadFile (CXMLIO* xmlio)

{
   CXMLNode xmlNode (xmlio);
   if (xmlNode.Start ("PanoramaToPTZCamera"))
   {
      M2SC.ReadFile (xmlio);
      xmlNode.Attribute (TYPE_ID (int),   "SrcImgWidth"      , &SrcImgWidth);
      xmlNode.Attribute (TYPE_ID (int),   "SrcImgHeight"     , &SrcImgHeight);
      xmlNode.Attribute (TYPE_ID (int),   "PanImgWidth"      , &PanImgWidth);
      xmlNode.Attribute (TYPE_ID (int),   "PanImgHeight"     , &PanImgHeight);
      xmlNode.Attribute (TYPE_ID (float), "SrcImgFocalLength", &SrcImgFocalLength);
      xmlNode.Attribute (TYPE_ID (float), "PanScanTiltPos"   , &PanScanTiltPos);
      xmlNode.Attribute (TYPE_ID (float), "PanScanZoomPos"   , &PanScanZoomPos);
      xmlNode.Attribute (TYPE_ID (float), "PanImgFocalLength", &PanImgFocalLength);
      xmlNode.Attribute (TYPE_ID (float), "PanImgHFOV"       , &PanImgHFOV);
      xmlNode.End ();
   }
   return (DONE);
}

 int PanoramaToPTZCamera::ReadFile (const char *file_name)
 
{
   FileIO file(file_name,FIO_BINARY,FIO_READ);
   if (!file) return (-1);
   return (ReadFile (file));
}

 int PanoramaToPTZCamera::WriteFile (FileIO &file)
 
{
   if (M2SC.WriteFile (file)) return (1);
   if (file.Write ((byte*)&SrcImgWidth      ,1,sizeof(SrcImgWidth      ))) return (2);
   if (file.Write ((byte*)&SrcImgHeight     ,1,sizeof(SrcImgHeight     ))) return (3);
   if (file.Write ((byte*)&PanImgWidth      ,1,sizeof(PanImgWidth      ))) return (4);
   if (file.Write ((byte*)&PanImgHeight     ,1,sizeof(PanImgHeight     ))) return (5);
   if (file.Write ((byte*)&SrcImgFocalLength,1,sizeof(SrcImgFocalLength))) return (6);
   if (file.Write ((byte*)&PanScanTiltPos   ,1,sizeof(PanScanTiltPos   ))) return (7);
   if (file.Write ((byte*)&PanScanZoomPos   ,1,sizeof(PanScanZoomPos   ))) return (8);
   if (file.Write ((byte*)&PanImgFocalLength,1,sizeof(PanImgFocalLength))) return (9);
   if (file.Write ((byte*)&PanImgHFOV       ,1,sizeof(PanImgHFOV       ))) return (10);
   return (DONE);
}

 int PanoramaToPTZCamera::WriteFile (CXMLIO* xmlio)

{
   static PanoramaToPTZCamera dv;

   CXMLNode xmlNode (xmlio);
   if (xmlNode.Start ("PanoramaToPTZCamera"))
   {
      M2SC.WriteFile (xmlio);
      xmlNode.Attribute (TYPE_ID (int),   "SrcImgWidth"      , &SrcImgWidth      , &dv.SrcImgWidth         );
      xmlNode.Attribute (TYPE_ID (int),   "SrcImgHeight"     , &SrcImgHeight     , &dv.SrcImgHeight        );
      xmlNode.Attribute (TYPE_ID (int),   "PanImgWidth"      , &PanImgWidth      , &dv.PanImgWidth         );
      xmlNode.Attribute (TYPE_ID (int),   "PanImgHeight"     , &PanImgHeight     , &dv.PanImgHeight        );
      xmlNode.Attribute (TYPE_ID (float), "SrcImgFocalLength", &SrcImgFocalLength, &dv.SrcImgFocalLength   );
      xmlNode.Attribute (TYPE_ID (float), "PanScanTiltPos"   , &PanScanTiltPos   , &dv.PanScanTiltPos      );
      xmlNode.Attribute (TYPE_ID (float), "PanScanZoomPos"   , &PanScanZoomPos   , &dv.PanScanZoomPos      );
      xmlNode.Attribute (TYPE_ID (float), "PanImgFocalLength", &PanImgFocalLength, &dv.PanImgFocalLength   );
      xmlNode.Attribute (TYPE_ID (float), "PanImgHFOV"       , &PanImgHFOV       , &dv.PanImgHFOV          );
      xmlNode.End ();
   }
   return (DONE);
}

 int PanoramaToPTZCamera::WriteFile (const char *file_name)
 
{
   FileIO file(file_name,FIO_BINARY,FIO_CREATE);
   if (!file) return (-1);
   return (WriteFile (file));
}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 void CombineTwoImages (BGRImage &r_image,BGRImage &s_image,Matrix &mat_H,BGRImage &d_image,int prj_method,float scale,double kappa,float ovl_width)

{
   Matrix mat_S(3,3);
   mat_S.Clear  (   );
   Matrix mat_IS(3,3);
   mat_IS.Clear (   );
   mat_S [0][0] = mat_S [1][1] = scale;
   mat_IS[0][0] = mat_IS[1][1] = 1.0 / scale;
   mat_S [2][2] = mat_IS[2][2] = 1.0;
   Matrix mat_Hs = mat_S * mat_H * mat_IS;
   kappa /= scale * scale;
   IS2DArray1D img_sizes(2);
   img_sizes[0]((int)(r_image.Width * scale),(int)(r_image.Height * scale));
   img_sizes[1]((int)(s_image.Width * scale),(int)(s_image.Height * scale));
   BGRImage rr_image,rs_image;
   BGRImage *r_image2,*s_image2;
   if (scale == 1.0f) {
      r_image2 = &r_image;
      s_image2 = &s_image;
   }
   else {
      rr_image.Create (img_sizes[0].Width,img_sizes[0].Height);
      rs_image.Create (img_sizes[1].Width,img_sizes[1].Height);
      Reduce (r_image,rr_image);
      Reduce (s_image,rs_image);
      if (r_image.AlphaChannel != NULL) {
         rr_image.CreateAlphaChannel (   );
         Reduce (*r_image.AlphaChannel,*rr_image.AlphaChannel);
      }
      if (s_image.AlphaChannel != NULL) {
         rs_image.CreateAlphaChannel (   );
         Reduce (*s_image.AlphaChannel,*rs_image.AlphaChannel);
      }
      r_image2 = &rr_image;
      s_image2 = &rs_image;
   }
   if (prj_method == PM_PLANE) {
      Array1D<Matrix> mats_H(2);
      mats_H[0] = IM(3);
      mats_H[1] = Inv(mat_Hs);
      DArray1D rd_coeffs(2);
      rd_coeffs[0] = rd_coeffs[1] = kappa;
      PlanarProjection p_prj;
      p_prj.OverlapWidth = ovl_width;
      p_prj.Initialize (mats_H,img_sizes,rd_coeffs,d_image);
      FArray2D d_map(r_image2->Width,r_image2->Height);
      GetDistanceMapForBlending (d_map);
      p_prj.Perform (*r_image2,d_map,0);
      d_map.Create (s_image2->Width,s_image2->Height);
      GetDistanceMapForBlending (d_map);
      p_prj.Perform (*s_image2,d_map,1);
   }
   else {
      double f_length = GetFocalLength (mat_Hs,r_image2->Width,r_image2->Height);
      Array1D<Matrix> mats_K(2);
      mats_K[0].Create (3,3);
      GetCalibrationMatrix (f_length,r_image2->Width,r_image2->Height,mats_K[0]);
      mats_K[1] = mats_K[0];
      Array1D<Matrix> mats_R(2);
      mats_R[0] = IM(3);
      mats_R[1].Create (3,3);
      GetRotationMatrix (mat_Hs,f_length,f_length,r_image2->Width,r_image2->Height,mats_R[1]);
      if (prj_method == PM_CYLINDER) {
         CylindricalProjection c_prj;
         c_prj.Kappa        = kappa;
         c_prj.OverlapWidth = ovl_width;
         c_prj.Initialize (mats_K,mats_R,r_image2->Width,r_image2->Height,d_image);
         c_prj.Perform (*r_image2,0);
         c_prj.Perform (*s_image2,1);
      }
      else {
         SphericalProjection s_prj;
         s_prj.Initialize (mats_K,mats_R,r_image2->Width,r_image2->Height,d_image);
         s_prj.Perform (*r_image2,0);
         s_prj.Perform (*s_image2,1);
      }
   }
}

 void CropNonimageArea_Horizontal (BGRImage &s_image,BGRImage &d_image,int &sy,int &ey)

{
   int y,x;

   sy = 0;
   for (y = sy; y < s_image.Height; y++) {
      BGRPixel *l_s_image = s_image[y];
      for (x = 0; x < s_image.Width; x++) {
         BGRPixel &p = l_s_image[x];
         if (!p.R && !p.G && !p.B) break;
      }
      if (x == s_image.Width) {
         sy = y;
         break;
      }
   }
   ey = s_image.Height - 1;
   for (y = ey; y >= sy; y--) {
      BGRPixel *l_s_image = s_image[y];
      for (x = 0; x < s_image.Width; x++) {
         BGRPixel &p = l_s_image[x];
         if (!p.R && !p.G && !p.B) break;
      }
      if (x == s_image.Width) {
         ey = y;
         break;
      }
   }
   d_image = s_image.Extract (0,sy,s_image.Width,ey - sy + 1);
}

 void CropNonimageArea_Vertical1 (BGRImage &s_image,BGRImage &d_image,int &sx,int &ex)

{
   int i,j,x,y;
   
   FArray1D s_array(s_image.Width);
   s_array.Clear (   );
   for (x = 0; x < s_image.Width; x++) {
      for (y = 0; y < s_image.Height; y++) {
         BGRPixel &p = s_image[y][x];
         if (p.R || p.G || p.B) s_array[x]++;
      }
   }
   FArray1D f_array(s_array.Length);
   GaussianFiltering (s_array,5.0f,f_array);
   FArray1D sd_array = Abs(Dxx(f_array));
   int ei = sd_array.Length - 1;
   for (i = 0; i <= 7; i++) sd_array[i] = sd_array[ei - i] = 0.0f;
   int ei2 = sd_array.Length / 2;
   float max_v1 = 0.0f;
   float max_v2 = 0.0f;
   sx = 0, ex = ei;
   for (i = 0; i < ei2; i++) {
      j = ei - i;
      if (sd_array[i] > max_v1) max_v1 = sd_array[i], sx = i;
      if (sd_array[j] > max_v2) max_v2 = sd_array[j], ex = j;
   }
   int width = (ex - sx + 1) / 4 * 4;
   ex = sx + width - 1;
   d_image = s_image.Extract (sx,0,ex - sx + 1,s_image.Height);
}

 void CropNonimageArea_Vertical2 (BGRImage &s_image,BGRImage &d_image,int &sx,int &ex)

{
   int x,y;

   FArray1D s_array(s_image.Width);
   s_array.Clear (   );
   for (x = 0; x < s_image.Width; x++) {
      for (y = 0; y < s_image.Height; y++) {
         BGRPixel &p = s_image[y][x];
         if (p.R || p.G || p.B) s_array[x]++;
      }
   }
   FArray1D f_array(s_array.Length);
   GaussianFiltering (s_array,5.0f,f_array);
   float thrsld = (float)(GetMean (f_array) * 1.0);
   RPArray rp_array;
   if (GetRectPulses (f_array,thrsld,rp_array)) {
      sx = 0;
      ex = s_image.Width - 1;
   }
   else {
      sx = rp_array[0].StartPos;
      ex = rp_array[rp_array.Length - 1].EndPos;
   }
   int width = (ex - sx + 1) / 4 * 4;
   ex = sx + width - 1;
   d_image = s_image.Extract (sx,0,ex - sx + 1,s_image.Height);
}

 void GetDistanceMapForBlending (FArray2D &d_array)

{
   int x,y;
   
   float  cx = 0.5f * d_array.Width;
   float  cy = 0.5f * d_array.Height;
   double max_d = sqrt (cx * cx + cy * cy);
   for (y = 0; y < d_array.Height; y++) {
      float dy  = y - cy;
      float sdy = dy * dy;
      float *l_d_array = d_array[y];
      for (x = 0; x < d_array.Width; x++) {
         float dx = x - cx;
         l_d_array[x] = (float)(max_d - sqrt (dx * dx + sdy));
      }
   }
}

 void PerformCylindricalProjection (GImage &s_image,double f_length,FPoint2D &lt_pos,FPoint2D &rb_pos,GImage &d_image)

{
   int    x,y;
   double v,theta;

   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   FPoint2D pc(0.5f * s_image.Width,0.5f * s_image.Height);
   d_image.Create (s_image.Width,s_image.Height);
   lt_pos.X = (float)atan (-pc.X / f_length);
   rb_pos.X = (float)atan ((pc.X - 1.0f) / f_length);
   lt_pos.Y = (float)(-pc.Y / f_length);
   rb_pos.Y = (float)((pc.Y - 1.0f) / f_length);
   double d_theta = (rb_pos.X - lt_pos.X) / (d_image.Width  - 1);
   double d_v     = (rb_pos.Y - lt_pos.Y) / (d_image.Height - 1);
   for (x = 0,theta = lt_pos.X; x < d_image.Width; x++,theta += d_theta) {
      double tan_theta = tan (theta);
      double a = f_length * sqrt (1.0 + tan_theta * tan_theta);
      float  fx = (float)(f_length * tan_theta + pc.X);
      int    ix = (int)fx;
      if (fx >= 0.0f && ix <= ex) {
         int ix1 = ix + 1;
         if (ix1 > ex) ix1 = ex;
         float rx  = fx - ix;
         float rx1 = 1.0f - rx;
         for (y = 0,v = lt_pos.Y; y < d_image.Height; y++,v += d_v) {
            float fy = (float)(a * v + pc.Y);
            int   iy = (int)fy;
            if (fy >= 0.0f && iy <= ey) {
               int iy1 = iy + 1;
               if (iy1 > ey) iy1 = ey;
               float ry  = fy - iy;
               float ry1 = 1.0f - ry;
               float b1  = ry1 * s_image[iy][ix ] + ry * s_image[iy1][ix ];
               float b2  = ry1 * s_image[iy][ix1] + ry * s_image[iy1][ix1];
               d_image[y][x] = (byte)(rx1 * b1 + rx * b2);
            }
            else d_image[y][x] = 0;
         }
      }
      else for (y = 0; y < d_image.Height; y++) d_image[y][x] = 0;
   }
}

}

#include "vacl_ipp.h"
#include "vacl_vaengine.h"
#include "ivcp.h"
#include "VAEngine.h"

 namespace VACL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Global Variables/Functions
//
///////////////////////////////////////////////////////////////////////////////

extern int  __Flag_VACL_Initialized;
extern void __RegisterLicenseKey (const char* license_key,const char* serial_no);

///////////////////////////////////////////////////////////////////////////////
//
// Class: VAEngine
//
///////////////////////////////////////////////////////////////////////////////

 VAEngine::VAEngine (   )
 
{
   Flag_Initialized = FALSE;
   FrameCount       = 0;
   ChannelID        = 0;
   Mode             = VAE_MODE_GENERAL;
   PVFPixFmt        = VAE_PIXFMT_GRAY8;
   PVFRate          = 7.0f;
   PVFSize(352,240);
   RefImgSize(REF_IMAGE_WIDTH,REF_IMAGE_HEIGHT);
} 

 VAEngine::~VAEngine (   )
 
{
   Close (   );
}

 void VAEngine::Close (   )
 
{
   Flag_Initialized = FALSE;
   FrameCount       = 0;
   BMUSImage.Delete (   );
   SrcImage.Delete  (   );
   SrcColorImage.Delete (   );
   ForegroundDetector_General.Close  (   );
   ForegroundDetector_Overhead.Close (   );
   ObjectTracker.Close    (   );
   ObjectClassifier.Close (   );
   EventDetector.Close    (   );
   DwellAnalyzer.Close    (   );
   VACfgDataBuffer.Close  (   );
}

 byte* VAEngine::GetConfiguration (int& cfg_buf_len)
 
{
   VACfgDataBuffer.Close (   );
   CXMLIO xmlIO(&VACfgDataBuffer,XMLIO_Write);
   CXMLNode xmlNode_VAConfig (&xmlIO);
   if (xmlNode_VAConfig.Start ("VAConfig")) {
      xmlNode_VAConfig.Attribute (TYPE_ID(int),"VAMode",&Mode);
      ForegroundDetector_General.WriteFile (&xmlIO);
      ForegroundDetector_Overhead.WriteFileEx (&xmlIO);
      ObjectTracker.WriteFile (&xmlIO);
      RealObjectSizeEstimator.WriteFile (&xmlIO);
      ObjectFilter.WriteFile  (&xmlIO);
      EventDetector.WriteFile (&xmlIO);
      xmlNode_VAConfig.End (   );
   }
   cfg_buf_len = VACfgDataBuffer.GetLength (   );
   return (VACfgDataBuffer.GetBuffer (   ));
}

 ForegroundDetection* VAEngine::GetForegroundDetector (   )
 
{
   ForegroundDetection* frg_detector = NULL;
   switch (Mode) {
      case VAE_MODE_OVERHEAD:
         frg_detector = &ForegroundDetector_Overhead;
         break;
      default:
         frg_detector = &ForegroundDetector_General;
         break;
   }
   return (frg_detector);
}

 int VAEngine::Initialize (int va_mode,int pvf_width,int pvf_height,float pvf_rate,int pvf_pix_fmt)
 
{
   // 일단 엔진을 닫는다. (관련 메모리 모두 해제)
   Close (   );
   // 내부 파리미터들을 초기화한다.
   FrameCount = 0;
   // 입력 파라미터들을 설정한다.
   Mode = va_mode;
   PVFSize(pvf_width,pvf_height);
   if (pvf_rate > 0.0f) PVFRate = pvf_rate;
   PVFPixFmt = pvf_pix_fmt;
   // 비디오 분석에 필요한 버퍼들을 생성한다.
   switch (pvf_pix_fmt) {
      case VAE_PIXFMT_BGR24:
         SrcImage.Create (pvf_width,pvf_height);
         break;
      case VAE_PIXFMT_YUY2:
         SrcImage.Create (pvf_width,pvf_height);
         SrcColorImage.Create (pvf_width,pvf_height);
         break;
      default:
         break;
   }
   BMUSImage.Create (PVFSize.Width,PVFSize.Height);
   BMUSImage.Clear  (   );
   // 기본 비디오 분석 모듈들을 초기화한다.
   ObjIDGenerator.Reset (   );
   ForegroundDetection* frg_detector = GetForegroundDetector (   );
   if (frg_detector->Initialize (PVFSize,PVFRate)) {
      Close (   );
      return (1);
   }
   if (ObjectTracker.Initialize (PVFSize,PVFRate,ObjIDGenerator)) {
      Close (   );
      return (2);
   }
   if (RealObjectSizeEstimator.Initialize (RefImgSize)) {
      Close (   );
      return (3);
   }
   if (ObjectClassifier.Initialize (   )) {
      Close (   );
      return (4);
   }
   if (EventDetector.Initialize (PVFSize,PVFRate,ObjIDGenerator)) {
      Close (   );
      return (5);
   }
   // 객체 카운터를 초기화한다.
   EventDetector.ResetObjectCounters (   );
   // 각종 Area Map을 초기화한다.
   ObjectTracker.UpdateAreaMaps      (RefImgSize);
   frg_detector->SetDetectionAreaMap (ObjectTracker.TrkAreaMap);
   EventDetector.UpdateAreaMaps      (RefImgSize);
   // 일단 확장 VA 모듈들을 모두 Disabled 상태로 만든다.
   DwellAnalyzer.Enable (FALSE);
   // 이벤트 존 설정 상태에 따라 필요한 확장 VA 모듈을 Enabled 상태로 만든다.
   if (EventDetector.IsEventZoneAvailable (ER_EVENT_DWELL)) DwellAnalyzer.Enable (TRUE);
   // 확장 VA 모듈들을 초기화한다. (Enabled 상태의 모듈들만 초기화를 수행함)
   StringA path_name;
   path_name.Format ("DwellAnalysis-CH%02d.xml",ChannelID);
   DwellAnalyzer.Initialize (EventDetector,path_name);
   // 이벤트 존의 설정 상태에 따라 모듈들의 파라미터 값들을 조절한다.
   frg_detector->InitParams    (EventDetector);
   ObjectTracker.InitParams    (EventDetector);
   ObjectClassifier.InitParams (EventDetector);
   ObjectFilter.InitParams     (EventDetector);
   Flag_Initialized = TRUE;
   return (DONE);
}

 int VAEngine::ProcessVideoFrame (byte* s_pvf_buf,char* time_stamp,byte* d_pkt_buf,int d_buf_len,int flag_finalize,IBox2D* s_boxes,int n_boxes)
 
{
   if (!Flag_Initialized) return (0);
   // 타임 스탬프 값을 얻는다.
   if (time_stamp != NULL) TimeStamp.SetTimeInString (time_stamp);
   else TimeStamp.GetLocalTime (   );
   // 입력 영상 데이터로부터 비디오 분석에 필요한 픽셀 포맷의 영상 데이터를 얻는다.
   switch (PVFPixFmt) {
      case VAE_PIXFMT_BGR24:
         SrcColorImage.Import ((BGRPixel*)s_pvf_buf,PVFSize.Width,PVFSize.Height);
         Convert (SrcColorImage,SrcImage);
         break;
      case VAE_PIXFMT_YUY2:
         ConvertYUY2ToGray  (s_pvf_buf,SrcImage);
         ConvertYUY2ToBGR_2 (s_pvf_buf,SrcColorImage);
         break;
      default:
         SrcImage.Import (s_pvf_buf,PVFSize.Width,PVFSize.Height);
         SrcColorImage.Import ((BGRPixel**)NULL,0,0);
         break;
   }
   // ObjectTracker.TrackedObjects 목록에 속한 객체들 중에 TO_STATUS_TO_BE_REMOVED 상태인 객체들을 제거한다.
   // 여기에서 호출해야 리셋 후 배경 재학습 시 이벤트 알람 종료가 재발생하지 않는다.
   ObjectTracker.CleanTrackedObjectList (   );
   // EventDetector.EvtGenObjects  목록에 속한 객체들 중에 TO_STATUS_TO_BE_REMOVED 상태인 객체들을 제거한다.
   EventDetector.CleanEvtGenObjectList  (   );
   // 현재 모드에 맞는 전경 검출기를 얻는다.
   ForegroundDetection* frg_detector = GetForegroundDetector (   );
   // 전경 검출을 수행한다.
   int fgd_rcode;
   if (!SrcColorImage) fgd_rcode = frg_detector->Perform (SrcImage,NULL,NULL,NULL,&ObjectTracker,&RealObjectSizeEstimator);
   else fgd_rcode = frg_detector->Perform (SrcImage,&SrcColorImage,NULL,NULL,&ObjectTracker,&RealObjectSizeEstimator);
   // 객체 추적 및 이벤트 검출을 수행한다.
   switch (fgd_rcode) {
      case FGD_RCODE_NORMAL: {
         // 기본 비디오 분석을 수행한다.
         ObjectTracker.Perform           (*frg_detector);
         RealObjectSizeEstimator.Perform (ObjectTracker);
         ObjectClassifier.Perform        (ObjectTracker);
         ObjectFilter.Perform            (ObjectTracker);
         // 배경 모델 업데이트 계획을 획득한다.
         ObjectTracker.GetBkgModelUpdateScheme (frg_detector->GetBkgModelUpdateMode(),BMUSImage);
            // [주의] 처음에 호출해야 한다.
         EventDetector.GetBkgModelUpdateScheme (BMUSImage);
            // [주의] 마지막에 호출해야 한다.
         // 배경 모델을 업데이트한다.
         frg_detector->UpdateBkgModel (BMUSImage);
      }  break;
      case FGD_RCODE_INITIAL_BACKGROUND_LEARNING: {
         // 배경 모델을 업데이트한다.
         BMUSImage.Clear (    );
         frg_detector->UpdateBkgModel (BMUSImage);
      }  break;
      case FGD_RCODE_SUDDEN_SCENE_CHANGE: {

      }  break;
      case FGD_RCODE_RESET: {
         // VA 모듈들을 리셋한다.
         frg_detector->Reset (   );
         ObjectTracker.Reset (   );
         EventDetector.Reset (   );
      }  break;
      default:
         break;
   }
   if (flag_finalize) {
      ObjectTracker.Reset (   );
      EventDetector.Reset (   );
   }
   // 이벤트 검출을 수행한다.
   EventDetector.Perform (ObjectTracker);
   DwellAnalyzer.Perform (ObjectTracker);
   // [주의] DwellAnalyzer.Perform(ObjectTracker)은 반드시 EventDetector.Perform(ObjectTracker) 보다 뒤에 호출되어야 한다.
   //        DwellAnalyzer.Perform(ObjectTracker)가 EventDetector.Perform(ObjectTracker)의 실행 결과를 이용하기 때문이다.
   EventDetector.Perform(DwellAnalyzer);
   int pkt_buf_len = 0;
   if (d_pkt_buf != NULL && d_buf_len) {
      // 객체/이벤트 썸네일을 획득한다.
      ObjectTracker.UpdateObjectThumbnails (s_pvf_buf,PVFSize,PVFPixFmt);
      EventDetector.UpdateEventThumbnails  (s_pvf_buf,PVFSize,PVFPixFmt);
      EventDetector.UpdateEventThumbnails  (ObjectTracker,s_pvf_buf,PVFSize,PVFPixFmt);
      // 비디오 분석 결과에 대한 IVCP 패킷을 생성한다.
      FileIO pkt_buf;
      WriteIVCPPacket_Frame   (pkt_buf);
      WriteIVCPPacket_Events  (pkt_buf);
      WriteIVCPPacket_Objects (pkt_buf);
      WriteIVCPPacket_EventZoneCounts (pkt_buf);
      pkt_buf_len = pkt_buf.GetLength (   );
      if (pkt_buf_len > d_buf_len) pkt_buf_len = d_buf_len;
      memcpy (d_pkt_buf,pkt_buf.GetBuffer(),pkt_buf_len);
   }
   FrameCount++;
   // Import 설정을 해제한다.
   switch (PVFPixFmt) {
      case VAE_PIXFMT_BGR24:
         SrcColorImage.Import ((BGRPixel*)NULL,0,0);
         break;
      case VAE_PIXFMT_YUY2:
         break;
      default:
         SrcImage.Import ((byte*)NULL,0,0);
         break;
   }   
   return (pkt_buf_len);
}

 int VAEngine::SetConfiguration (byte* s_cfg_buf,int s_buf_len)
 
{
   int r_code = DONE;

   FileIO file;
   file.Attach (s_cfg_buf,s_buf_len);
   CXMLIO xmlIO(&file,XMLIO_Read);
   CXMLNode xmlNode_VAConfig (&xmlIO);
   if (xmlNode_VAConfig.Start ("VAConfig")) {
      int va_mode;
      xmlNode_VAConfig.Attribute (TYPE_ID(int),"VAMode",&va_mode);
      ForegroundDetector_General.ReadFile (&xmlIO);
      ForegroundDetector_Overhead.CopyParams1 (ForegroundDetector_General);
      ForegroundDetector_Overhead.ReadFileEx (&xmlIO);
      ObjectTracker.ReadFile (&xmlIO);
      RealObjectSizeEstimator.ReadFile (&xmlIO);
      ObjectFilter.ReadFile  (&xmlIO);
      EventDetector.ReadFile (&xmlIO);
      xmlNode_VAConfig.End (   );
      if (Initialize (va_mode,PVFSize.Width,PVFSize.Height,PVFRate,PVFPixFmt)) r_code = 2;
   }
   else r_code = 1;
   file.Detach (   );
   return (r_code);
}

 void VAEngine::WriteIVCPPacket (CIVCPPacket* s_packet,FileIO& d_pkt_buf)
 
{
   d_pkt_buf.Print ("%s %d %d %d\n",s_packet->m_Header.m_strMessage,s_packet->m_Header.m_nOptions,s_packet->m_Header.m_nDataLen,s_packet->m_Header.m_nBodyLen);
   d_pkt_buf.Write ((byte*)s_packet->m_pBody,s_packet->m_Header.m_nBodyLen,1);
}

 void VAEngine::WriteIVCPPacket_Event (TrackedObject* t_object,Event* event,FileIO& d_pkt_buf)
 
{
   if (!(event->Status & (EV_STATUS_BEGUN|EV_STATUS_ENDED))) return;
   CIVCPLiveEventInfo eventInfo;
   eventInfo.m_nCameraUID = 0;
   if (event->Status & EV_STATUS_BEGUN) {
      eventInfo.m_nEventStatus = CIVCPLiveEventInfo::EVENT_STATUS_BEGUN;
      event->StartTime = TimeStamp;
      Convert (event->StartTime,eventInfo.m_ftStartTime);
      Convert (event->StartTime,eventInfo.m_ftEndTime);
   }
   if (event->Status & EV_STATUS_ENDED) {
      event->EndTime = TimeStamp;
      eventInfo.m_nEventStatus = CIVCPLiveEventInfo::EVENT_STATUS_ENDED;
      Convert (event->StartTime,eventInfo.m_ftStartTime);
      Convert (event->EndTime  ,eventInfo.m_ftEndTime);
   }
   eventInfo.m_nEventID     = event->ID;
   eventInfo.m_nEventType   = event->EvtRule->EventType;
   eventInfo.m_nEventRuleID = event->EvtRule->ID;
   eventInfo.m_nObjectID    = t_object->ID;
   eventInfo.m_nObjectType  = t_object->Type;
   eventInfo.m_ObjectBox    = (IBox2D)(*t_object);
   eventInfo.m_nEventZoneID = event->EvtZone->ID;
   strcpy(eventInfo.m_strEventZoneName,event->EvtZone->Name);
   eventInfo.m_nProcWidth   = PVFSize.Width;
   eventInfo.m_nProcHeight  = PVFSize.Height;
   eventInfo.m_bThumbnail   = FALSE;
   eventInfo.MakeBodyBuffer (NULL,0);
   CIVCPPacket* packet = new CIVCPPacket;
   IvcpGetPacketString (packet->m_Header.m_strMessage,IVCP_ID_Live_EventInfo);
   eventInfo.WriteToPacket (*packet);
   WriteIVCPPacket (packet,d_pkt_buf);
   delete packet;
   //
   // 썸네일 있는 버전 보내기 기능 추가
   //
}

 void VAEngine::WriteIVCPPacket_Events (FileIO& d_pkt_buf)
 
{
   // 기본 이벤트 처리
   TrackedObject *t_object = ObjectTracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      if (t_object->Status & TO_STATUS_VERIFIED) {
         Event *event = t_object->Events.First (   );
         while (event != NULL) {
            if (event->Status & (EV_STATUS_BEGUN|EV_STATUS_ENDED)) WriteIVCPPacket_Event (t_object,event,d_pkt_buf);
            event = t_object->Events.Next (event);
         }
      }
      t_object = ObjectTracker.TrackedObjects.Next (t_object);
   }
   TrackedObject *l_object = ObjectTracker.LostObjects.First (   );
   while (l_object != NULL) {
      Event *event = l_object->Events.First (   );
      while (event != NULL) {
         if (event->EvtRule->EventType == ER_EVENT_LEAVING) {
            if (event->Status & (EV_STATUS_BEGUN|EV_STATUS_ENDED)) WriteIVCPPacket_Event (l_object,event,d_pkt_buf);
         }
         event = l_object->Events.Next (event);
      }
      l_object = ObjectTracker.LostObjects.Next (l_object);
   }
}

 void VAEngine::WriteIVCPPacket_EventZoneCounts (FileIO& d_pkt_buf)
 
{
   CIVCPLiveEventZoneCountInfo zoneCountInfo;
   zoneCountInfo.m_nCameraUID = 0;
   Convert (TimeStamp,zoneCountInfo.m_ftFrameTime);
   EventZoneList& evtZoneList = EventDetector.EventZones;
   int nZoneNum = evtZoneList.GetNumItems (   );
   zoneCountInfo.m_pZoneCounts = new CIVCPEventZoneCount[nZoneNum];
   EventZone* pEvtZone = evtZoneList.First (   );
   while (pEvtZone != NULL) 
   {
      if (pEvtZone->EvtRule != NULL)
      {
         zoneCountInfo.m_pZoneCounts[zoneCountInfo.m_nZoneNum].m_nZoneID    = pEvtZone->ID;
         zoneCountInfo.m_pZoneCounts[zoneCountInfo.m_nZoneNum].m_nZoneCount = pEvtZone->EvtRule->ERC_Counting.Count;
         zoneCountInfo.m_nZoneNum++;
      }
      pEvtZone = evtZoneList.Next (pEvtZone);
   }
   CIVCPPacket* packet = new CIVCPPacket;
   IvcpGetPacketString (packet->m_Header.m_strMessage,IVCP_ID_Live_EventZoneCount);
   zoneCountInfo.WriteToPacket (*packet);
   WriteIVCPPacket (packet,d_pkt_buf);
   delete packet;
}

 void VAEngine::WriteIVCPPacket_EventZones (FileIO& d_pkt_buf)
 
{
   CIVCPLiveEventZoneInfo evtZoneInfo;
   evtZoneInfo.m_nCameraUID = 0;
   Convert (TimeStamp,evtZoneInfo.m_ftFrameTime);
   EventZoneList& evtZoneList = EventDetector.EventZones;
   evtZoneInfo.m_nWidth  = evtZoneList.Width;
   evtZoneInfo.m_nHeight = evtZoneList.Height;
   int nZoneNum = evtZoneList.GetNumItems (   );
   evtZoneInfo.m_pEventZones = new CIVCPEventZone[nZoneNum];
   EventZone* pEvtZone = evtZoneList.First (   );
   while (pEvtZone != NULL) {
      if (pEvtZone->EvtRule != NULL) {
         CIVCPEventZone& evtZoneItem = evtZoneInfo.m_pEventZones[evtZoneInfo.m_nZoneNum];
         evtZoneItem.m_nZoneID       = pEvtZone->ID;
         evtZoneItem.m_nWidth        = pEvtZone->RefImgSize.Width;
         evtZoneItem.m_nHeight       = pEvtZone->RefImgSize.Height;
         strcpy (evtZoneItem.m_strName,pEvtZone->Name);
         evtZoneItem.m_ptCenter      = pEvtZone->CenterPos;    
         evtZoneItem.m_nVerticesNum  = pEvtZone->Vertices.Length;
         if (evtZoneItem.m_nVerticesNum > 0)
         {
            evtZoneItem.m_pVertices = new IPoint2D[evtZoneItem.m_nVerticesNum];
            for (int i=0; i<evtZoneItem.m_nVerticesNum; i++)
            {
               evtZoneItem.m_pVertices[i] = pEvtZone->Vertices[i];
            }
         }
         evtZoneItem.m_nRuleOptions    = pEvtZone->EvtRule->Options;
         evtZoneItem.m_nRuleEventType  = pEvtZone->EvtRule->EventType;
         evtZoneItem.m_nRuleObjectType = pEvtZone->EvtRule->ObjectType;
         strcpy(evtZoneItem.m_strRuleName,pEvtZone->EvtRule->Name);
         evtZoneItem.m_aAbandoned.m_fDetectionTime         = pEvtZone->EvtRule->ERC_Abandoned.DetectionTime;
         evtZoneItem.m_aCounting.m_nCount                  = pEvtZone->EvtRule->ERC_Counting.Count;
         evtZoneItem.m_aCounting.m_fAspectRatio            = pEvtZone->EvtRule->ERC_Counting.AspectRatio;
         evtZoneItem.m_aCounting.m_aMajorAxis              = pEvtZone->EvtRule->ERC_Counting.MajorAxis;
         evtZoneItem.m_aDirectionalMotion.m_fRange         = pEvtZone->EvtRule->ERC_DirectionalMotion.Range;
         evtZoneItem.m_aDirectionalMotion.m_fDirection     = pEvtZone->EvtRule->ERC_DirectionalMotion.Direction;
         evtZoneItem.m_aDirectionalMotion.m_fDetectionTime = pEvtZone->EvtRule->ERC_DirectionalMotion.DetectionTime;
         EventRuleConfig_PathPassing &ppFrom = pEvtZone->EvtRule->ERC_PathPassing;
         evtZoneItem.m_aPathPassing.m_nDirection = ppFrom.Direction;
         evtZoneItem.m_aPathPassing.m_nItemNum   = ppFrom.ZoneTree.Children.Length;
         if (evtZoneItem.m_aPathPassing.m_nItemNum > 0)
         {
            evtZoneItem.m_aPathPassing.m_pItems = new CIVCPEventRule_PathPassingItem[evtZoneItem.m_aPathPassing.m_nItemNum];
            for (int i = 0; i<evtZoneItem.m_aPathPassing.m_nItemNum; i++)
            {
               CIVCPEventRule_PathPassingItem& ppItem = evtZoneItem.m_aPathPassing.m_pItems[i];
               ppItem.m_nID           = ppFrom.ZoneTree.Children[i].ID;
               ppItem.m_ptCenter      = ppFrom.ZoneTree.Children[i].CenterPos;
               ppItem.m_nVerticesNum  = ppFrom.ZoneTree.Children[i].Vertices.Length;
               if (ppItem.m_nVerticesNum > 0)
               {
                  ppItem.m_pVertices = new IPoint2D[ppItem.m_nVerticesNum];
                  for (int j=0; j<ppItem.m_nVerticesNum; j++)
                  {
                     ppItem.m_pVertices[j] = ppFrom.ZoneTree.Children[i].Vertices[j];
                  }
               }
            }
         }
         evtZoneItem.m_aPathPassing.m_nArrowNum = ppFrom.Arrows.GetNumItems();
         if (evtZoneItem.m_aPathPassing.m_nArrowNum > 0)
         {
            evtZoneItem.m_aPathPassing.m_pArrows = new CIVCPEventRule_PathPassingArrow[evtZoneItem.m_aPathPassing.m_nArrowNum];
            for (int i=0; i<evtZoneItem.m_aPathPassing.m_nArrowNum; i++)
            {
               Arrow *pArrow = ppFrom.Arrows.Get(i);
               evtZoneItem.m_aPathPassing.m_pArrows[i].m_nSplitZoneID = pArrow->SplitZoneID;
               evtZoneItem.m_aPathPassing.m_pArrows[i].m_lnArrow      = *pArrow;
            }
         }
         evtZoneInfo.m_nZoneNum++;
      }
      pEvtZone = evtZoneList.Next (pEvtZone);
   }
   CIVCPPacket* packet = new CIVCPPacket;
   IvcpGetPacketString (packet->m_Header.m_strMessage,IVCP_ID_Live_EventZoneInfo);
   evtZoneInfo.WriteToPacket (*packet);
   WriteIVCPPacket (packet,d_pkt_buf);
   delete packet;
}

 void VAEngine::WriteIVCPPacket_Frame (FileIO& d_pkt_buf)
 
{
   CIVCPLiveFrameInfo frameInfo;
   frameInfo.m_nCameraUID   = 0;
   frameInfo.m_nCameraState = 0; // 주의
   frameInfo.m_nObjectNum   = 0;
   frameInfo.m_nPTZState    = 0;
   frameInfo.m_nPTZStateEx  = 0;
   frameInfo.m_fFrameRate   = PVFRate;
   frameInfo.m_nProcWidth   = PVFSize.Width;
   frameInfo.m_nProcHeight  = PVFSize.Height;
   Convert (TimeStamp,frameInfo.m_ftFrameTime);
   int nObjCount = 0;
   TrackedObject* t_object = ObjectTracker.TrackedObjects.First (   );
   while (t_object) {
      if (t_object->Status & TO_STATUS_VERIFIED) nObjCount++;
      t_object = ObjectTracker.TrackedObjects.Next (t_object);
   }
   frameInfo.m_pObjects = new CIVCPFrameObject[nObjCount];
   t_object = ObjectTracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      if (frameInfo.m_nObjectNum >= nObjCount) break;
      if (t_object->Status & TO_STATUS_VERIFIED) { 
         CIVCPFrameObject& frameObject  = frameInfo.m_pObjects[frameInfo.m_nObjectNum];
         frameObject.m_nObjectID        = t_object->ID;
         frameObject.m_nObjectType      = t_object->Type;
         frameObject.m_ObjectBox.X      = t_object->X;
         frameObject.m_ObjectBox.Y      = t_object->Y;
         frameObject.m_ObjectBox.Width  = t_object->Width;
         frameObject.m_ObjectBox.Height = t_object->Height;
         int nEventNum = t_object->Events.GetNumItems (   );
         frameObject.m_pEvents = new CIVCPFrameEvent[nEventNum];
         frameObject.m_nEventNum = 0;
         Event* pEvent = t_object->Events.First (   );
         while (pEvent != NULL) {
            if (frameObject.m_nEventNum >= nEventNum) break;
            CIVCPFrameEvent& frameEvent = frameObject.m_pEvents[frameObject.m_nEventNum];
            frameEvent.m_nEventID   = pEvent->ID;
            frameEvent.m_nEventType = pEvent->EvtRule->EventType;
            frameObject.m_nEventNum++;
            pEvent = t_object->Events.Next (pEvent);
         }
         frameInfo.m_nObjectNum++;
      }
      t_object = ObjectTracker.TrackedObjects.Next (t_object);
   }
   CIVCPPacket* packet = new CIVCPPacket;
   IvcpGetPacketString (packet->m_Header.m_strMessage,IVCP_ID_Live_FrameInfo);
   frameInfo.WriteToPacket (*packet);
   WriteIVCPPacket (packet,d_pkt_buf);
   delete packet;
}

 void VAEngine::WriteIVCPPacket_FrameEx (FileIO& d_pkt_buf)
 
{
   CIVCPLiveFrameObjectInfo frameInfo;
   frameInfo.m_nCameraUID       = 0;
   frameInfo.m_strCameraGuid[0] = 0; // 주의
   frameInfo.m_nObjectNum       = 0;
   frameInfo.m_fFrameRate       = PVFRate;
   frameInfo.m_szFrameSize      = PVFSize;
   Convert (TimeStamp,frameInfo.m_ftFrameTime);
   int nObjCount = 0;
   TrackedObject* t_object = ObjectTracker.TrackedObjects.First (   );
   while (t_object) {
      if (t_object->Status & TO_STATUS_VERIFIED) nObjCount++;
      t_object = ObjectTracker.TrackedObjects.Next (t_object);
   }
   frameInfo.m_pObjects = new CIVCPFrameObjectEx[nObjCount];
   t_object = ObjectTracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      if (frameInfo.m_nObjectNum >= nObjCount) break;
      if (t_object->Status & TO_STATUS_VERIFIED) { 
         CIVCPFrameObjectEx& frameObject = frameInfo.m_pObjects[frameInfo.m_nObjectNum];
         frameObject.m_nObjectID         = t_object->ID;
         frameObject.m_nObjectType       = t_object->Type;
         frameObject.m_ObjectBox.X       = t_object->X;
         frameObject.m_ObjectBox.Y       = t_object->Y;
         frameObject.m_ObjectBox.Width   = t_object->Width;
         frameObject.m_ObjectBox.Height  = t_object->Height;
         frameObject.m_nObjectStatus     = t_object->Status;
         frameObject.m_fCalcArea         = t_object->Area;
         frameObject.m_fCalcWidth        = (float)t_object->Width;
         frameObject.m_fCalcHeight       = (float)t_object->Height;
         frameObject.m_fCalcMajorAxisLen = t_object->MajorAxisLength;
         frameObject.m_fCalcMinorAxisLen = t_object->MinorAxisLength;
         frameObject.m_fCalcSpeed        = t_object->Speed;
         frameObject.m_fRealArea         = t_object->RealArea;
         frameObject.m_fRealHeight       = t_object->RealHeight;
         frameObject.m_fRealWidth        = t_object->RealWidth;
         frameObject.m_fRealMajorAxisLen = t_object->RealMajorAxisLength;
         frameObject.m_fRealMinorAxisLen = t_object->RealMinorAxisLength;
         frameObject.m_fRealSpeed        = t_object->RealSpeed;
         frameObject.m_fNorSpeed         = t_object->NorSpeed;
         int nEventNum = t_object->Events.GetNumItems (   );
         frameObject.m_pEvents   = new CIVCPFrameEvent[nEventNum];
         frameObject.m_nEventNum = 0;
         Event* pEvent = t_object->Events.First (   );
         while (pEvent != NULL) {
            if (frameObject.m_nEventNum >= nEventNum) break;
            CIVCPFrameEvent& frameEvent = frameObject.m_pEvents[frameObject.m_nEventNum];
            frameEvent.m_nEventID   = pEvent->ID;
            frameEvent.m_nEventType = pEvent->EvtRule->EventType;
            frameObject.m_nEventNum++;
            pEvent = t_object->Events.Next (pEvent);
         }
         frameInfo.m_nObjectNum++;
      }
      t_object = ObjectTracker.TrackedObjects.Next (t_object);
   }
   CIVCPPacket* packet = new CIVCPPacket;
   IvcpGetPacketString (packet->m_Header.m_strMessage,IVCP_ID_Live_FrmObjInfo);
   frameInfo.WriteToPacket (*packet);
   WriteIVCPPacket (packet,d_pkt_buf);
   delete packet;
}

 void VAEngine::WriteIVCPPacket_Object (TrackedObject* t_object,FileIO& d_pkt_buf)
 
{
   if (!(t_object->Status & (TO_STATUS_VERIFIED_START|TO_STATUS_TO_BE_REMOVED))) return;
   CIVCPLiveObjectInfo objectInfo;
   objectInfo.m_nCameraUID = 0;
   if (t_object->Status & TO_STATUS_VERIFIED_START) {
      t_object->StartTime = TimeStamp;
      objectInfo.m_nObjectStatus = CIVCPLiveEventInfo::EVENT_STATUS_BEGUN;
      Convert (t_object->StartTime,objectInfo.m_ftStartTime);
      Convert (t_object->StartTime,objectInfo.m_ftEndTime);
   }
   if (t_object->Status & TO_STATUS_TO_BE_REMOVED) {
      t_object->EndTime = TimeStamp;
      objectInfo.m_nObjectStatus = CIVCPLiveEventInfo::EVENT_STATUS_ENDED;
      Convert (t_object->StartTime,objectInfo.m_ftStartTime);
      Convert (t_object->EndTime  ,objectInfo.m_ftEndTime);
   }
   objectInfo.m_nObjectID               = t_object->ID;
   objectInfo.m_nObjectType             = t_object->Type;
   objectInfo.m_fAvgArea                = t_object->AvgArea;
   objectInfo.m_fAvgWidth               = t_object->AvgWidth;
   objectInfo.m_fAvgHeight              = t_object->AvgHeight;
   objectInfo.m_fAvgAspectRatio         = t_object->AvgAspectRatio;
   objectInfo.m_fAvgAxisLengthRatio     = t_object->AvgAxisLengthRatio;
   objectInfo.m_fAvgMajorAxisLength     = t_object->AvgMajorAxisLength;
   objectInfo.m_fAvgMinorAxisLength     = t_object->AvgMinorAxisLength;
   objectInfo.m_fAvgNorSpeed            = t_object->AvgNorSpeed;
   objectInfo.m_fAvgSpeed               = t_object->AvgSpeed;
   objectInfo.m_fAvgRealArea            = t_object->AvgRealArea;
   objectInfo.m_fAvgRealDistance        = t_object->AvgRealDistance;
   objectInfo.m_fAvgRealWidth           = t_object->AvgRealWidth;
   objectInfo.m_fAvgRealHeight          = t_object->AvgRealHeight;
   objectInfo.m_fAvgRealMajorAxisLength = t_object->AvgRealMajorAxisLength;
   objectInfo.m_fAvgRealMinorAxisLength = t_object->AvgRealMinorAxisLength;
   objectInfo.m_fAvgRealSpeed           = t_object->AvgRealSpeed;
   objectInfo.m_nMaxWidth               = t_object->MaxWidth;
   objectInfo.m_nMaxHeight              = t_object->MaxHeight;
   objectInfo.m_fMaxArea                = t_object->MaxArea;
   objectInfo.m_fMaxAspectRatio         = t_object->MaxAspectRatio;
   objectInfo.m_fMaxAxisLengthRatio     = t_object->MaxAxisLengthRatio;
   objectInfo.m_fMaxMajorAxisLength     = t_object->MaxMajorAxisLength;
   objectInfo.m_fMaxMinorAxisLength     = t_object->MaxMinorAxisLength;
   objectInfo.m_fMaxNorSpeed            = t_object->MaxNorSpeed;
   objectInfo.m_fMaxSpeed               = t_object->MaxSpeed;
   objectInfo.m_fMaxRealDistance        = t_object->MaxRealDistance;
   objectInfo.m_fMaxRealSpeed           = t_object->MaxRealSpeed;
   objectInfo.m_nMinWidth               = t_object->MinWidth;
   objectInfo.m_nMinHeight              = t_object->MinHeight;
   objectInfo.m_fMinArea                = t_object->MinArea;
   objectInfo.m_fMinAspectRatio         = t_object->MinAspectRatio;
   objectInfo.m_fMinAxisLengthRatio     = t_object->MinAxisLengthRatio;
   objectInfo.m_fMinMajorAxisLength     = t_object->MinMajorAxisLength;
   objectInfo.m_fMinMinorAxisLength     = t_object->MinMinorAxisLength;
   objectInfo.m_fMinNorSpeed            = t_object->MinNorSpeed;
   objectInfo.m_fMinSpeed               = t_object->MinSpeed;
   objectInfo.m_fMinRealDistance        = t_object->MinRealDistance;
   objectInfo.m_fMinRealSpeed           = t_object->MinRealSpeed;
   t_object->ColorHistogram.Finalize (objectInfo.m_fHistogram);
   objectInfo.m_nProcWidth              = PVFSize.Width;
   objectInfo.m_nProcHeight             = PVFSize.Height;
   objectInfo.m_bThumbnail              = FALSE;
   objectInfo.MakeBodyBuffer (NULL,0);
   CIVCPPacket* packet = new CIVCPPacket;
   IvcpGetPacketString (packet->m_Header.m_strMessage,IVCP_ID_Live_ObjectInfo);
   objectInfo.WriteToPacket (*packet);
   WriteIVCPPacket (packet,d_pkt_buf);
   delete packet;
   //
   // 썸네일 있는 버전 보내기 기능 추가
   //
}

 void VAEngine::WriteIVCPPacket_Objects (FileIO& d_pkt_buf)
 
{
   TrackedObject* t_object = ObjectTracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      if (t_object->Status & TO_STATUS_VERIFIED) WriteIVCPPacket_Object (t_object,d_pkt_buf);
      t_object = ObjectTracker.TrackedObjects.Next (t_object);
   }
}

}

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

extern "C" {

 void VAEngine_CalcPVFSize (int svf_width,int svf_height,float vfsr_factor,int* pvf_width,int* pvf_height)
 
{
   int w = (int)(vfsr_factor * svf_width );
   int h = (int)(vfsr_factor * svf_height);
   *pvf_width  = (w + 3) / 4 * 4;
   *pvf_height = (h + 3) / 4 * 4;
}

 int VAEngine_Close (HVAENGINE h_va_engine)
 
{
   if (h_va_engine == NULL) return (-1);
   VACL::VAEngine* va_engine = (VACL::VAEngine*)h_va_engine;
   va_engine->Close (   );
   return (DONE);
}

 void* VAEngine_GetConfiguration (HVAENGINE h_va_engine,int *cfg_buf_len)
 
{
   if (h_va_engine == NULL) return (NULL);
   VACL::VAEngine* va_engine = (VACL::VAEngine*)h_va_engine;
   int d_buf_len;
   byte* cfg_buf = va_engine->GetConfiguration (d_buf_len);
   *cfg_buf_len = d_buf_len;
   return (cfg_buf);
}

 int VAEngine_GetMode (HVAENGINE h_va_engine)
 
{
   if (h_va_engine == NULL) return (-1);
   VACL::VAEngine* va_engine = (VACL::VAEngine*)h_va_engine;
   return (va_engine->Mode);
}

 int VAEngine_GetPVFPixFmt (HVAENGINE h_va_engine)
 
{
   if (h_va_engine == NULL) return (VAE_PIXFMT_NONE);
   VACL::VAEngine* va_engine = (VACL::VAEngine*)h_va_engine;
   return (va_engine->PVFPixFmt);
}

 float VAEngine_GetPVFRate (HVAENGINE h_va_engine)
 
{
   if (h_va_engine == NULL) return (-1.0f);
   VACL::VAEngine* va_engine = (VACL::VAEngine*)h_va_engine;
   return (va_engine->PVFRate);
}

 int VAEngine_GetPVFSize (HVAENGINE h_va_engine,int* pvf_width,int* pvf_height)
 
{
   if (h_va_engine == NULL) {
      *pvf_width  = 0;
      *pvf_height = 0;
      return (-1);
   }
   VACL::VAEngine* va_engine = (VACL::VAEngine*)h_va_engine;
   *pvf_width  = va_engine->PVFSize.Width;
   *pvf_height = va_engine->PVFSize.Height;
   return (DONE);
}

 int VAEngine_Initialize (HVAENGINE h_va_engine,int va_mode,int pvf_width,int pvf_height,float pvf_rate,int pvf_pix_fmt)
 
{
   if (h_va_engine == NULL) return (-1);
   VACL::VAEngine* va_engine = (VACL::VAEngine*)h_va_engine;
   return (va_engine->Initialize (va_mode,pvf_width,pvf_height,pvf_rate,pvf_pix_fmt));
}

 int VAEngine_IsInitialized (HVAENGINE h_va_engine)
 
{
   if (h_va_engine == NULL) return (FALSE);
   VACL::VAEngine* va_engine = (VACL::VAEngine*)h_va_engine;
   return (va_engine->IsInitialized (   ));
}

 int VAEngine_ProcessVideoFrame (HVAENGINE h_va_engine,void* s_pvf_buf,char* time_stamp,int flag_finalize,BOX* s_boxes,int n_boxes,void* d_pkt_buf,int d_buf_len)
 
{
   int i;

   if (h_va_engine == NULL) return (-1);
   VACL::VAEngine* va_engine = (VACL::VAEngine*)h_va_engine;
   IBox2D* s_bbs = NULL;
   IB2DArray1D bb_array;
   if (n_boxes > 0) {
      bb_array.Create (n_boxes);
      s_bbs = bb_array;
      for (i = 0;i < n_boxes; i++) {
         bb_array[i].X      = s_boxes[i].X;
         bb_array[i].Y      = s_boxes[i].Y;
         bb_array[i].Width  = s_boxes[i].Width;
         bb_array[i].Height = s_boxes[i].Height;
      }
   }
   return (va_engine->ProcessVideoFrame ((byte*)s_pvf_buf,time_stamp,(byte*)d_pkt_buf,d_buf_len,flag_finalize,s_bbs,n_boxes));
}

 int VAEngine_SetConfiguration (HVAENGINE h_va_engine,void* s_cfg_buf,int s_buf_len)
 
{
   if (h_va_engine == NULL) return (-1);
   VACL::VAEngine* va_engine = (VACL::VAEngine*)h_va_engine;
   return (va_engine->SetConfiguration ((byte*)s_cfg_buf,s_buf_len));
}

///////////////////////////////////////////////////////////////////////////////

 int VAEngine_InitLibrary (const char* license_key,const char* serial_no)
 
{
   if (license_key != NULL && serial_no != NULL) VACL::__RegisterLicenseKey (license_key,serial_no);
   return (VACL::InitVACL (   ));
}

 void VAEngine_CloseLibrary (   )
 
{
   VACL::CloseVACL (   );
}

 HVAENGINE VAEngine_Create (int channel_id)
 
{
   if (!VACL::__Flag_VACL_Initialized) return (NULL);
   VACL::VAEngine* va_engine = new VACL::VAEngine;
   va_engine->ChannelID = channel_id;
   return ((HVAENGINE)va_engine);
}
 
 int VAEngine_Delete (HVAENGINE h_va_engine)
 
{
   if (h_va_engine == NULL) return (1);
   VACL::VAEngine* va_engine = (VACL::VAEngine*)h_va_engine;
   delete va_engine;
   return (DONE);
}

 int VAEngine_Run (HVAENGINE h_va_engine,int va_mode,void* pvf_buf,int pvf_width,int pvf_height,float pvf_rate,int pvf_pix_fmt,char* time_stamp,int flag_finalize,void* cfg_buf,int cfg_buf_len,void* d_pkt_buf,int d_buf_len)

{
   return (VAEngine_RunEx (h_va_engine,va_mode,pvf_buf,pvf_width,pvf_height,pvf_rate,pvf_pix_fmt,time_stamp,flag_finalize,cfg_buf,cfg_buf_len,NULL,0,d_pkt_buf,d_buf_len));
}

 int VAEngine_RunEx (HVAENGINE h_va_engine,int va_mode,void* pvf_buf,int pvf_width,int pvf_height,float pvf_rate,int pvf_pix_fmt,char* time_stamp,int flag_finalize,void* cfg_buf,int cfg_buf_len,BOX* s_boxes,int n_boxes,void* d_pkt_buf,int d_buf_len)

{
   int   pkt_len;
   int   c_va_mode;
   int   c_pvf_width;
   int   c_pvf_height;
   int   c_pvf_pix_fmt;
   float c_pvf_rate;
   
   if (!VACL::__Flag_VACL_Initialized) return (-1);
   pkt_len = 0;
   if (h_va_engine == NULL || pvf_buf == NULL) return (0);
   if (flag_finalize && VAEngine_IsInitialized (h_va_engine)) {
      // 추적 객체 및 이벤트들을 모두 종료시킨다.
      pkt_len = VAEngine_ProcessVideoFrame (h_va_engine,pvf_buf,time_stamp,TRUE,s_boxes,n_boxes,d_pkt_buf,d_buf_len);
      // VAEngine을 닫는다.
      VAEngine_Close (h_va_engine);
   }
   else {
      if (cfg_buf != NULL && cfg_buf_len) {
         if (VAEngine_IsInitialized (h_va_engine)) {
            // 추적 객체 및 이벤트들을 모두 종료시킨다.
            pkt_len = VAEngine_ProcessVideoFrame (h_va_engine,pvf_buf,time_stamp,TRUE,s_boxes,n_boxes,d_pkt_buf,d_buf_len);
            // VAEngine을 닫는다.
            VAEngine_Close (h_va_engine);
         }
         // 주어진 설정 XML 데이터를 이용하여 VAEngine의 파라미터 값들을 설정한다.
         VAEngine_SetConfiguration (h_va_engine,cfg_buf,cfg_buf_len);
      }
      else {
         c_va_mode     = VAEngine_GetMode      (h_va_engine);
         c_pvf_rate    = VAEngine_GetPVFRate   (h_va_engine);
         c_pvf_pix_fmt = VAEngine_GetPVFPixFmt (h_va_engine);
         VAEngine_GetPVFSize (h_va_engine,&c_pvf_width,&c_pvf_height);
         // VAEngine의 설정이 바뀌었거나 아직 초기화되지 않았으면 VAEngine의 초기화를 수행한다.
         if (!VAEngine_IsInitialized (h_va_engine)                 ||
             (va_mode != VAE_MODE_DEFAULT && va_mode != c_va_mode) ||
             pvf_width   != c_pvf_width                            ||
             pvf_height  != c_pvf_height                           ||
             pvf_rate    != c_pvf_rate                             ||
             pvf_pix_fmt != c_pvf_pix_fmt) {
                if (va_mode == VAE_MODE_DEFAULT) va_mode = c_va_mode;
                VAEngine_Initialize (h_va_engine,va_mode,pvf_width,pvf_height,pvf_rate,pvf_pix_fmt);
         }
         // 비디오 분석을 수행한다.
         pkt_len = VAEngine_ProcessVideoFrame (h_va_engine,pvf_buf,time_stamp,FALSE,s_boxes,n_boxes,d_pkt_buf,d_buf_len);
      }
   }
   return (pkt_len);
}

 int VAEngine_Reset (HVAENGINE h_va_engine)

{
   return (VAEngine_Close (h_va_engine));
}

}

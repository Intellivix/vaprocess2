#include "vacl_camera.h"
#include "vacl_edge.h"
#include "vacl_geometry.h"
#include "vacl_imgreg.h"

 namespace VACL

{
 
///////////////////////////////////////////////////////////////////////////////
//
// Class: LIR_MSSD
//
///////////////////////////////////////////////////////////////////////////////

 LIR_MSSD::LIR_MSSD (   )
 
{
   // Internal Variables
   RefImage = NULL;
   SrcImage = NULL;
}

 LIR_MSSD::~LIR_MSSD (   )

{
   Close (   );
}

 void LIR_MSSD::Close (   )

{
   RefImage = NULL;
   SrcImage = NULL;
   Mat_A.Delete       (   );
   Vec_b.Delete       (   );
   Vec_p.Delete       (   );
   GxArray.Delete     (   );
   GyArray.Delete     (   );
   ErrArray.Delete    (   );
   ParamVector.Delete (   );
}

 double LIR_MSSD::CostFunction (Matrix &vec_p)

{
   int   x,y;
   float fx,fy;

   double ssd = 0.0;
   NMeasurements = 0;
   GImage &r_image = *RefImage;
   GImage &s_image = *SrcImage;
   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   for (y = RI_Min.Y; y <= RI_Max.Y; y++) {
      for (x = RI_Min.X; x <= RI_Max.X; x++) {
         if (r_image[y][x]) {
            Transformation (vec_p,x,y,fx,fy);
            int ix = (int)fx;
            int iy = (int)fy;
            if (ix > 0 && iy > 0 && ix < ex && iy < ey) {
               int ix1 = ix + 1;
               int iy1 = iy + 1;
               if (s_image[iy][ix] && s_image[iy1][ix] && s_image[iy][ix1] && s_image[iy1][ix1]) {
                  float rx  = fx - ix;
                  float ry  = fy - iy;
                  float rx1 = 1.0f - rx;
                  float ry1 = 1.0f - ry;
                  float b1  = ry1 * s_image[iy][ix ] + ry * s_image[iy1][ix ];
                  float b2  = ry1 * s_image[iy][ix1] + ry * s_image[iy1][ix1];
                  float er  = rx1 * b1 + rx * b2 - r_image[y][x];
                  ErrArray[NMeasurements++] = er;
                  ssd += er * er;
               }
            }
         }
      }
   }
   if (NMeasurements) return (sqrt (ssd / NMeasurements));
   else return (0.0);
}

 void LIR_MSSD::Derivative_Transformation (Matrix &vec_p,int n,int sx,int sy,float fx,float fy,float &dx,float &dy)

{
   float fx2,fy2;
   
   double tp = vec_p(n);
   double dp = fabs (1E-4 * tp);
   if (dp < 1E-6) dp = 1E-6;
   vec_p(n) += dp;
   Transformation (vec_p,sx,sy,fx2,fy2);
   dx = (float)((fx2 - fx) / dp);
   dy = (float)((fy2 - fy) / dp);
   vec_p(n) = tp;
}

 void LIR_MSSD::GetHomography (Matrix &mat_H)

{
   GetHomographySub (ParamVector,mat_H);
}

 void LIR_MSSD::GetJacobianMatrix (Matrix &vec_p,Matrix &mat_J)

{
   int   i,x,y;
   float fx,fy,dx,dy;

   GImage &r_image = *RefImage;
   GImage &s_image = *SrcImage;
   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   int n_measurements = 0;
   for (y = RI_Min.Y; y <= RI_Max.Y; y++) {
      for (x = RI_Min.X; x <= RI_Max.X; x++) {
         if (r_image[y][x]) {
            Transformation (vec_p,x,y,fx,fy);
            int ix = (int)fx;
            int iy = (int)fy;
            if (ix > 0 && iy > 0 && ix < ex && iy < ey) {
               int ix1 = ix + 1;
               int iy1 = iy + 1;
               if (s_image[iy][ix] && s_image[iy1][ix] && s_image[iy][ix1] && s_image[iy1][ix1]) {
                  float rx   = fx - ix;
                  float ry   = fy - iy;
                  float rx1  = 1.0f - rx;
                  float ry1  = 1.0f - ry;
                  float b1   = ry1 * GxArray[iy][ix ] + ry * GxArray[iy1][ix ];
                  float b2   = ry1 * GxArray[iy][ix1] + ry * GxArray[iy1][ix1];
                  float gx   = rx1 * b1 + rx * b2;
                        b1   = ry1 * GyArray[iy][ix ] + ry * GyArray[iy1][ix ];
                        b2   = ry1 * GyArray[iy][ix1] + ry * GyArray[iy1][ix1];
                  float gy   = rx1 * b1 + rx * b2;
                  for (i = 0; i < vec_p.NRows; i++) {
                     Derivative_Transformation (vec_p,i,x,y,fx,fy,dx,dy);
                     mat_J[n_measurements][i] = gx * dx + gy * dy;
                  }
                  n_measurements++;
               }
            }
         }
      }
   }
}

 void LIR_MSSD::Initialize (GImage &r_image,GImage &s_image,int n_params)

{
   Close (   );
   Error          = MC_VLV;
   Lambda         = 1E-3;
   RefImage       = &r_image;
   SrcImage       = &s_image;
   IterationCount = 0;
   ParamVector.Create (n_params);
   ErrArray.Create (r_image.Width * r_image.Height);
   GxArray.Create  (s_image.Width,s_image.Height);
   GyArray.Create  (s_image.Width,s_image.Height);
   GetGradients    (s_image,GxArray,GyArray);
}

 double LIR_MSSD::Perform (int n_iterations,double d_thrsld,double e_thrsld)

{
   int    i;
   double p_error,c_error;

   if (RefImage == NULL || SrcImage == NULL) return (-1.0);
   p_error = MC_VLV;
   for (i = 0; n_iterations <= 0 || i < n_iterations; i++) {
      c_error = Update (   );
      if (fabs (c_error - p_error) < d_thrsld) break;
      else p_error = c_error;
      if (c_error <= e_thrsld) break;
   }
   return (c_error);
}

 void LIR_MSSD::SetRefImageRange (Matrix &vec_p)

{
   int i;
   
   int ex = SrcImage->Width  - 1;
   int ey = SrcImage->Height - 1;
   Matrix mat_H(3,3);
   GetHomographySub (vec_p,mat_H);
   Matrix mat_IH = Inv(mat_H);
   Array1D<Matrix> vecs_x(4);
   for (i = 0; i < 4; i++) {
      vecs_x[i].Create (3);
      vecs_x[i](2) = 1.0;
   }
   vecs_x[1](0) = (double)ex;
   vecs_x[2](1) = (double)ey;
   vecs_x[3](0) = vecs_x[1](0);
   vecs_x[3](1) = vecs_x[2](1);
   FP2DArray1D s_points(4);
   for (i = 0; i < 4; i++) GetInhomogeneousCoordinates (mat_IH * vecs_x[i],s_points[i]);
   IPoint2D min_x,max_x;
   FPoint2D min_p,max_p;
   FindMinMax (s_points,min_p,max_p,min_x,max_x);
   RI_Min.X = (int)min_p.X, RI_Min.Y = (int)min_p.Y;
   RI_Max.X = (int)max_p.X, RI_Max.Y = (int)max_p.Y;
   ex = RefImage->Width  - 1;
   ey = RefImage->Height - 1;
   if      (RI_Min.X <  0) RI_Min.X = 0;
   else if (RI_Min.X > ex) RI_Min.X = ex;
   if      (RI_Min.Y <  0) RI_Min.Y = 0;
   else if (RI_Min.Y > ey) RI_Min.Y = ey;
   if      (RI_Max.X <  0) RI_Max.X = 0;
   else if (RI_Max.X > ex) RI_Max.X = ex;
   if      (RI_Max.Y <  0) RI_Max.Y = 0;
   else if (RI_Max.Y > ey) RI_Max.Y = ey;
}

 double LIR_MSSD::Update (   )

{
   int i;
   
   if (IterationCount == 0) Vec_p = ParamVector;
   Prepare_Transformation (Vec_p);
   SetRefImageRange (Vec_p);
   double error = CostFunction (Vec_p);
   if (error == 0.0) return (0.0);
   if (error >= Error && IterationCount != 0) {
      if (Lambda > 1E+8) Lambda = 1e+8;
      else Lambda *= 5.0f;
   }
   else {
      Error = error;
      Lambda /= 5.0f;
      ParamVector = Vec_p;
      Prepare_Derivative_Transformation (ParamVector);
      Matrix mat_J(NMeasurements,ParamVector.NRows);
      GetJacobianMatrix (ParamVector,mat_J);
      Matrix vec_e(NMeasurements);
      for (i = 0; i < NMeasurements; i++) vec_e(i) = ErrArray[i];
      Matrix mat_Jt = Tr(mat_J);
      Mat_A = mat_Jt * mat_J;
      Vec_b = mat_Jt * vec_e;
   }
   Matrix mat_A  = Mat_A;
   double lambda = 1.0 + Lambda;
   for (i = 0; i < mat_A.NRows; i++) mat_A[i][i] *= lambda;
   Vec_p = ParamVector - Inv(mat_A) * Vec_b;
   IterationCount++;
   return (error);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: LIR_MSSD_2DAffine
//
///////////////////////////////////////////////////////////////////////////////

 double LIR_MSSD_2DAffine::CostFunction (Matrix &vec_p)

{
   int x,y;

   double ssd = 0.0;
   NMeasurements = 0;
   GImage &r_image = *RefImage;
   GImage &s_image = *SrcImage;
   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   Matrix mat_H(3,3);
   GetHomographySub (vec_p,mat_H);
   Matrix vec_x = mat_H * GetHomogeneousVector (RI_Min);
   double tx1 = vec_x(0);
   double ty1 = vec_x(1);
   for (y = RI_Min.Y; y <= RI_Max.Y; y++) {
      double tx2 = tx1;
      double ty2 = ty1;
      for (x = RI_Min.X; x <= RI_Max.X; x++) {
         if (r_image[y][x]) {
            float fx = (float)tx2;
            float fy = (float)ty2;
            int ix = (int)fx;
            int iy = (int)fy;
            if (ix > 0 && iy > 0 && ix < ex && iy < ey) {
               int ix1 = ix + 1;
               int iy1 = iy + 1;
               if (s_image[iy][ix] && s_image[iy1][ix] && s_image[iy][ix1] && s_image[iy1][ix1]) {
                  float rx  = fx - ix;
                  float ry  = fy - iy;
                  float ry1 = 1.0f - ry;
                  float b1  = ry1 * s_image[iy][ix ] + ry * s_image[iy1][ix ];
                  float b2  = ry1 * s_image[iy][ix1] + ry * s_image[iy1][ix1];
                  float er  = (1.0f - rx) * b1 + rx * b2 - r_image[y][x];
                  ErrArray[NMeasurements++] = er;
                  ssd += er * er;
               }
            }
         }
         tx2 += vec_p(0);
         ty2 += vec_p(3);
      }
      tx1 += vec_p(1);
      ty1 += vec_p(4);
   }
   if (NMeasurements) return (sqrt (ssd / NMeasurements));
   else return (0.0);
}

 void LIR_MSSD_2DAffine::GetHomographySub (Matrix &vec_p,Matrix &mat_H)

{
   int i;

   for (i = 0; i < 6; i++) mat_H(i) = vec_p(i);
   mat_H[2][0] = mat_H[2][1] = 0.0;
   mat_H[2][2] = 1.0;
}

 void LIR_MSSD_2DAffine::GetJacobianMatrix (Matrix &vec_p,Matrix &mat_J)

{
   int x,y;

   GImage &r_image = *RefImage;
   GImage &s_image = *SrcImage;
   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   int n_measurements = 0;
   Matrix mat_H(3,3);
   GetHomographySub (vec_p,mat_H);
   Matrix vec_x = mat_H * GetHomogeneousVector (RI_Min);
   double tx1 = vec_x(0);
   double ty1 = vec_x(1);
   for (y = RI_Min.Y; y <= RI_Max.Y; y++) {
      double tx2 = tx1;
      double ty2 = ty1;
      for (x = RI_Min.X; x <= RI_Max.X; x++) {
         if (r_image[y][x]) {
            float fx = (float)tx2;
            float fy = (float)ty2;
            int   ix = (int)fx;
            int   iy = (int)fy;
            if (ix > 0 && iy > 0 && ix < ex && iy < ey) {
               int ix1 = ix + 1;
               int iy1 = iy + 1;
               if (s_image[iy][ix] && s_image[iy1][ix] && s_image[iy][ix1] && s_image[iy1][ix1]) {
                  float rx   = fx - ix;
                  float ry   = fy - iy;
                  float rx1  = 1.0f - rx;
                  float ry1  = 1.0f - ry;
                  float b1   = ry1 * GxArray[iy][ix ] + ry * GxArray[iy1][ix ];
                  float b2   = ry1 * GxArray[iy][ix1] + ry * GxArray[iy1][ix1];
                  float gx   = rx1 * b1 + rx * b2;
                        b1   = ry1 * GyArray[iy][ix ] + ry * GyArray[iy1][ix ];
                        b2   = ry1 * GyArray[iy][ix1] + ry * GyArray[iy1][ix1];
                  float gy   = rx1 * b1 + rx * b2;
                  mat_J[n_measurements][0] = gx * x;
                  mat_J[n_measurements][1] = gx * y;
                  mat_J[n_measurements][2] = gx;
                  mat_J[n_measurements][3] = gy * x;
                  mat_J[n_measurements][4] = gy * y;
                  mat_J[n_measurements][5] = gy;
                  n_measurements++;
               }
            }
         }
         tx2 += vec_p(0);
         ty2 += vec_p(3);
      }
      tx1 += vec_p(1);
      ty1 += vec_p(4);
   }
}

 void LIR_MSSD_2DAffine::Initialize (GImage &r_image,GImage &s_image,Matrix &mat_H)

{
   int i;
   
   LIR_MSSD::Initialize (r_image,s_image,6);
   for (i = 0; i < 6; i++) ParamVector(i) = mat_H(i) / mat_H[2][2];
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: LIR_MSSD_2DEuclidean
//
///////////////////////////////////////////////////////////////////////////////

 double LIR_MSSD_2DEuclidean::CostFunction (Matrix &vec_p)

{
   int x,y;

   double ssd = 0.0;
   NMeasurements = 0;
   GImage &r_image = *RefImage;
   GImage &s_image = *SrcImage;
   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   double cos_theta = cos (vec_p(0));
   double sin_theta = sin (vec_p(0));
   for (y = RI_Min.Y; y <= RI_Max.Y; y++) {
      double a1 = -y * sin_theta + vec_p(1);
      double a2 =  y * cos_theta + vec_p(2);
      for (x = RI_Min.X; x <= RI_Max.X; x++) {
         if (r_image[y][x]) {
            float fx = (float)(x * cos_theta + a1);
            float fy = (float)(x * sin_theta + a2);
            int ix = (int)fx;
            int iy = (int)fy;
            if (ix > 0 && iy > 0 && ix < ex && iy < ey) {
               int ix1 = ix + 1;
               int iy1 = iy + 1;
               if (s_image[iy][ix] && s_image[iy1][ix] && s_image[iy][ix1] && s_image[iy1][ix1]) {
                  float rx  = fx - ix;
                  float ry  = fy - iy;
                  float rx1 = 1.0f - rx;
                  float ry1 = 1.0f - ry;
                  float b1  = ry1 * s_image[iy][ix ] + ry * s_image[iy1][ix ];
                  float b2  = ry1 * s_image[iy][ix1] + ry * s_image[iy1][ix1];
                  float er  = rx1 * b1 + rx * b2 - r_image[y][x];
                  ErrArray[NMeasurements++] = er;
                  ssd += er * er;
               }
            }
         }
      }
   }
   if (NMeasurements) return (sqrt (ssd / NMeasurements));
   else return (0.0);
}

 void LIR_MSSD_2DEuclidean::GetHomographySub (Matrix &vec_p,Matrix &mat_H)

{
   mat_H[0][0] =  cos (vec_p(0));
   mat_H[0][1] = -sin (vec_p(0));
   mat_H[0][2] =  vec_p(1);
   mat_H[1][0] = -mat_H[0][1];
   mat_H[1][1] =  mat_H[0][0];
   mat_H[1][2] =  vec_p(2);
   mat_H[2][0] =  0.0;
   mat_H[2][1] =  0.0;
   mat_H[2][2] =  1.0;
}

 void LIR_MSSD_2DEuclidean::GetJacobianMatrix (Matrix &vec_p,Matrix &mat_J)

{
   int x,y;

   GImage &r_image = *RefImage;
   GImage &s_image = *SrcImage;
   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   int n_measurements = 0;
   double cos_theta = cos (vec_p(0));
   double sin_theta = sin (vec_p(0));
   for (y = RI_Min.Y; y <= RI_Max.Y; y++) {
      double a1 = -y * sin_theta + vec_p(1);
      double a2 =  y * cos_theta + vec_p(2);
      double a3 = -y * cos_theta;
      double a4 = -y * sin_theta;
      for (x = RI_Min.X; x <= RI_Max.X; x++) {
         if (r_image[y][x]) {
            float fx = (float)(x * cos_theta + a1);
            float fy = (float)(x * sin_theta + a2);
            int   ix = (int)fx;
            int   iy = (int)fy;
            if (ix > 0 && iy > 0 && ix < ex && iy < ey) {
               int ix1 = ix + 1;
               int iy1 = iy + 1;
               if (s_image[iy][ix] && s_image[iy1][ix] && s_image[iy][ix1] && s_image[iy1][ix1]) {
                  float rx  = fx - ix;
                  float ry  = fy - iy;
                  float rx1 = 1.0f - rx;
                  float ry1 = 1.0f - ry;
                  float b1  = ry1 * GxArray[iy][ix ] + ry * GxArray[iy1][ix ];
                  float b2  = ry1 * GxArray[iy][ix1] + ry * GxArray[iy1][ix1];
                  float gx  = rx1 * b1 + rx * b2;
                        b1  = ry1 * GyArray[iy][ix ] + ry * GyArray[iy1][ix ];
                        b2  = ry1 * GyArray[iy][ix1] + ry * GyArray[iy1][ix1];
                  float gy  = rx1 * b1 + rx * b2;
                  float dx  = (float)(-x * sin_theta + a3);
                  float dy  = (float)( x * cos_theta + a4);
                  mat_J[n_measurements][0] = gx * dx + gy * dy;
                  mat_J[n_measurements][1] = gx;
                  mat_J[n_measurements][2] = gy;
                  n_measurements++;
               }
            }
         }
      }
   }
}

 void LIR_MSSD_2DEuclidean::GetParameters (double &theta,double &tx,double &ty)

{
   theta = ParamVector(0);
   tx    = ParamVector(1);
   ty    = ParamVector(2);
}	

 void LIR_MSSD_2DEuclidean::Initialize (GImage &r_image,GImage &s_image,double theta,double tx,double ty)

{
   LIR_MSSD::Initialize (r_image,s_image,3);
   ParamVector(0) = theta;
   ParamVector(1) = tx;
   ParamVector(2) = ty;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: LIR_MSSD_2DIsoScale
//
///////////////////////////////////////////////////////////////////////////////

 double LIR_MSSD_2DIsoScale::CostFunction (Matrix &vec_p)

{
   int x,y;

   double ssd = 0.0;
   NMeasurements = 0;
   GImage &r_image = *RefImage;
   GImage &s_image = *SrcImage;
   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   float scale   = (float)vec_p(0);
   float trans_x = (float)vec_p(1);
   float trans_y = (float)vec_p(2);
   float fy = scale * RI_Min.Y + trans_y;
   for (y = RI_Min.Y; y <= RI_Max.Y; y++) {
      float fx = scale * RI_Min.X + trans_x;
      for (x = RI_Min.X; x <= RI_Max.X; x++) {
         if (r_image[y][x]) {
            int ix = (int)fx;
            int iy = (int)fy;
            if (ix > 0 && iy > 0 && ix < ex && iy < ey) {
               int ix1 = ix + 1;
               int iy1 = iy + 1;
               if (s_image[iy][ix] && s_image[iy1][ix] && s_image[iy][ix1] && s_image[iy1][ix1]) {
                  float rx  = fx - ix;
                  float ry  = fy - iy;
                  float rx1 = 1.0f - rx;
                  float ry1 = 1.0f - ry;
                  float b1  = ry1 * s_image[iy][ix ] + ry * s_image[iy1][ix ];
                  float b2  = ry1 * s_image[iy][ix1] + ry * s_image[iy1][ix1];
                  float er  = rx1 * b1 + rx * b2 - r_image[y][x];
                  ErrArray[NMeasurements++] = er;
                  ssd += er * er;
               }
            }
         }
         fx += scale;
      }
      fy += scale;
   }
   if (NMeasurements) return (sqrt (ssd / NMeasurements));
   else return (0.0);
}

 void LIR_MSSD_2DIsoScale::GetHomographySub (Matrix &vec_p,Matrix &mat_H)

{
   mat_H.Clear (   );
   mat_H[0][0] = vec_p(0);
   mat_H[1][1] = vec_p(0);
   mat_H[0][2] = vec_p(1);
   mat_H[1][2] = vec_p(2);
   mat_H[2][2] = 1.0;
}

 void LIR_MSSD_2DIsoScale::GetJacobianMatrix (Matrix &vec_p,Matrix &mat_J)

{
   int x,y;

   GImage &r_image = *RefImage;
   GImage &s_image = *SrcImage;
   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   int n_measurements = 0;
   float scale   = (float)vec_p(0);
   float trans_x = (float)vec_p(1);
   float trans_y = (float)vec_p(2);
   float fy = scale * RI_Min.Y + trans_y;
   for (y = RI_Min.Y; y <= RI_Max.Y; y++) {
      float fx = scale * RI_Min.X + trans_x;
      for (x = RI_Min.X; x <= RI_Max.X; x++) {
         if (r_image[y][x]) {
            int ix = (int)fx;
            int iy = (int)fy;
            if (ix > 0 && iy > 0 && ix < ex && iy < ey) {
               int ix1 = ix + 1;
               int iy1 = iy + 1;
               if (s_image[iy][ix] && s_image[iy1][ix] && s_image[iy][ix1] && s_image[iy1][ix1]) {
                  float rx  = fx - ix;
                  float ry  = fy - iy;
                  float rx1 = 1.0f - rx;
                  float ry1 = 1.0f - ry;
                  float b1  = ry1 * GxArray[iy][ix ] + ry * GxArray[iy1][ix ];
                  float b2  = ry1 * GxArray[iy][ix1] + ry * GxArray[iy1][ix1];
                  float gx  = rx1 * b1 + rx * b2;
                        b1  = ry1 * GyArray[iy][ix ] + ry * GyArray[iy1][ix ];
                        b2  = ry1 * GyArray[iy][ix1] + ry * GyArray[iy1][ix1];
                  float gy  = rx1 * b1 + rx * b2;
                  mat_J[n_measurements][0] = gx * x + gy * y;
                  mat_J[n_measurements][1] = gx;
                  mat_J[n_measurements][2] = gy;
                  n_measurements++;
               }
            }
         }
         fx += scale;
      }
      fy += scale;
   }
}

 void LIR_MSSD_2DIsoScale::GetParameters (double &scale,double &trans_x,double &trans_y)

{
   scale   = ParamVector(0);
   trans_x = ParamVector(1);
   trans_y = ParamVector(2);
}	

 void LIR_MSSD_2DIsoScale::Initialize (GImage &r_image,GImage &s_image,double scale,double trans_x,double trans_y)

{
   LIR_MSSD::Initialize (r_image,s_image,3);
   ParamVector(0) = scale;
   ParamVector(1) = trans_x;
   ParamVector(2) = trans_y;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: LIR_MSSD_2DProjective
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
//  Image Mosaicing for Tele-Reality Applications
//  R. Szeliski
//  Technical Report CRL 94/2
//  DEC Cambridge Research Lab, 1994
//
///////////////////////////////////////////////////////////////////////////////

 double LIR_MSSD_2DProjective::CostFunction (Matrix &vec_p)

{
   int x,y;

   double ssd = 0.0;
   NMeasurements = 0;
   GImage &r_image = *RefImage;
   GImage &s_image = *SrcImage;
   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   Matrix mat_H(3,3);
   GetHomographySub (vec_p,mat_H);
   Matrix vec_x = mat_H * GetHomogeneousVector (RI_Min);
   double tx1 = vec_x(0);
   double ty1 = vec_x(1);
   double tz1 = vec_x(2);
   for (y = RI_Min.Y; y <= RI_Max.Y; y++) {
      double tx2 = tx1;
      double ty2 = ty1;
      double tz2 = tz1;
      for (x = RI_Min.X; x <= RI_Max.X; x++) {
         if (r_image[y][x]) {
            float fx = (float)(tx2 / tz2);
            float fy = (float)(ty2 / tz2);
            int ix = (int)fx;
            int iy = (int)fy;
            if (ix > 0 && iy > 0 && ix < ex && iy < ey) {
               int ix1 = ix + 1;
               int iy1 = iy + 1;
               if (s_image[iy][ix] && s_image[iy1][ix] && s_image[iy][ix1] && s_image[iy1][ix1]) {
                  float rx  = fx - ix;
                  float ry  = fy - iy;
                  float ry1 = 1.0f - ry;
                  float b1  = ry1 * s_image[iy][ix ] + ry * s_image[iy1][ix ];
                  float b2  = ry1 * s_image[iy][ix1] + ry * s_image[iy1][ix1];
                  float er  = (1.0f - rx) * b1 + rx * b2 - r_image[y][x];
                  ErrArray[NMeasurements++] = er;
                  ssd += er * er;
               }
            }
         }
         tx2 += vec_p(0);
         ty2 += vec_p(3);
         tz2 += vec_p(6);
      }
      tx1 += vec_p(1);
      ty1 += vec_p(4);
      tz1 += vec_p(7);
   }
   if (NMeasurements) return (sqrt (ssd / NMeasurements));
   else return (0.0);
}

 void LIR_MSSD_2DProjective::GetHomographySub (Matrix &vec_p,Matrix &mat_H)

{
   int i;

   for (i = 0; i < 8; i++) mat_H(i) = vec_p(i);
   mat_H[2][2] = 1.0;
}

 void LIR_MSSD_2DProjective::GetJacobianMatrix (Matrix &vec_p,Matrix &mat_J)

{
   int x,y;

   GImage &r_image = *RefImage;
   GImage &s_image = *SrcImage;
   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   int n_measurements = 0;
   Matrix mat_H(3,3);
   GetHomographySub (vec_p,mat_H);
   Matrix vec_x = mat_H * GetHomogeneousVector (RI_Min);
   double tx1 = vec_x(0);
   double ty1 = vec_x(1);
   double tz1 = vec_x(2);
   for (y = RI_Min.Y; y <= RI_Max.Y; y++) {
      double tx2 = tx1;
      double ty2 = ty1;
      double tz2 = tz1;
      for (x = RI_Min.X; x <= RI_Max.X; x++) {
         if (r_image[y][x]) {
            float fx = (float)(tx2 / tz2);
            float fy = (float)(ty2 / tz2);
            int   ix = (int)fx;
            int   iy = (int)fy;
            if (ix > 0 && iy > 0 && ix < ex && iy < ey) {
               int ix1 = ix + 1;
               int iy1 = iy + 1;
               if (s_image[iy][ix] && s_image[iy1][ix] && s_image[iy][ix1] && s_image[iy1][ix1]) {
                  float rx   = fx - ix;
                  float ry   = fy - iy;
                  float rx1  = 1.0f - rx;
                  float ry1  = 1.0f - ry;
                  float b1   = ry1 * GxArray[iy][ix ] + ry * GxArray[iy1][ix ];
                  float b2   = ry1 * GxArray[iy][ix1] + ry * GxArray[iy1][ix1];
                  float gx   = rx1 * b1 + rx * b2;
                        b1   = ry1 * GyArray[iy][ix ] + ry * GyArray[iy1][ix ];
                        b2   = ry1 * GyArray[iy][ix1] + ry * GyArray[iy1][ix1];
                  float gy   = rx1 * b1 + rx * b2;
                  float gxd  = (float)(gx / tz2);
                  float gyd  = (float)(gy / tz2);
                  float gxxd = gxd * x;
                  float gyyd = gyd * y;
                  float gxyd = gxd * y;
                  float gyxd = gyd * x;
                  mat_J[n_measurements][0] = gxxd;
                  mat_J[n_measurements][1] = gxyd;
                  mat_J[n_measurements][2] = gxd;
                  mat_J[n_measurements][3] = gyxd;
                  mat_J[n_measurements][4] = gyyd;
                  mat_J[n_measurements][5] = gyd;
                  mat_J[n_measurements][6] = -gxxd * fx - gyxd * fy;
                  mat_J[n_measurements][7] = -gxyd * fx - gyyd * fy;
                  n_measurements++;
               }
            }
         }
         tx2 += vec_p(0);
         ty2 += vec_p(3);
         tz2 += vec_p(6);
      }
      tx1 += vec_p(1);
      ty1 += vec_p(4);
      tz1 += vec_p(7);
   }
}

 void LIR_MSSD_2DProjective::Initialize (GImage &r_image,GImage &s_image,Matrix &mat_H)

{
   int i;
   
   LIR_MSSD::Initialize (r_image,s_image,8);
   for (i = 0; i < 8; i++) ParamVector(i) = mat_H(i) / mat_H[2][2];
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: LIR_MSSD_2DProjective_LD
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
//  Characterization of Errors in Compositing Panoramic Images
//  S. B. Kang and R. Weiss
//  Technical Report CRL 96/2
//  DEC Cambridge Research Lab, 1996
//
///////////////////////////////////////////////////////////////////////////////

 void LIR_MSSD_2DProjective_LD::Derivative_Transformation (Matrix &vec_p,int n,int sx,int sy,float fx,float fy,float &dx,float &dy)

{
   float fx2,fy2;
   
   double tp = vec_p(n);
   double dp = fabs (1E-4 * tp);
   if (dp < 1E-6 && n != 8) dp = 1E-6;
   vec_p(n) += dp;
   Transformation (vec_p,sx,sy,fx2,fy2);
   dx = (float)((fx2 - fx) / dp);
   dy = (float)((fy2 - fy) / dp);
   vec_p(n) = tp;
}

 void LIR_MSSD_2DProjective_LD::GetHomographySub (Matrix &vec_p,Matrix &mat_H)

{
   int i;
   
   for (i = 0; i < 8; i++) mat_H(i) = vec_p(i);
   mat_H[2][2] = 1.0;
}

 double LIR_MSSD_2DProjective_LD::GetLensDistortionCoefficient (   )

{
   return (ParamVector(8));
}

 void LIR_MSSD_2DProjective_LD::Initialize (GImage &r_image,GImage &s_image,Matrix &mat_H,double kappa)

{
   int i;

   LIR_MSSD::Initialize (r_image,s_image,9);
   for (i = 0; i < 8; i++) ParamVector(i) = mat_H(i) / mat_H[2][2];
   ParamVector(8) = kappa;
   RefImageCenter(0.5f * r_image.Width,0.5f * r_image.Height);
   SrcImageCenter(0.5f * s_image.Width,0.5f * s_image.Height);
}

 void LIR_MSSD_2DProjective_LD::Transformation (Matrix &vec_p,int sx,int sy,float &dx,float &dy)

{
   double a,sxu,syu;

   int flag_nrd = FALSE;
   if (fabs (vec_p(8)) <= MC_VSV) {
      sxu = sx, syu = sy;
      flag_nrd = TRUE;
   }
   else {
      double sxd = sx - RefImageCenter.X;
      double syd = sy - RefImageCenter.Y;
      a   = 1.0 + vec_p(8) * (sxd * sxd + syd * syd);
      sxu = sxd * a + RefImageCenter.X;
      syu = syd * a + RefImageCenter.Y;
   }
   a = vec_p(6) * sxu + vec_p(7) * syu + 1.0;
   double dxu = (vec_p(0) * sxu + vec_p(1) * syu + vec_p(2)) / a;
   double dyu = (vec_p(3) * sxu + vec_p(4) * syu + vec_p(5)) / a;
   if (flag_nrd) dx = (float)dxu, dy = (float)dyu;
   else {
      dxu -= SrcImageCenter.X;
      dyu -= SrcImageCenter.Y;
      double sru = dxu * dxu + dyu * dyu;
      double a1  = vec_p(8) * vec_p(8);
      double a2  = 1.0 / (27.0 * a1 * vec_p(8)) + sru / (2.0 * a1);
      double a3  = pow (sqrt (a2 * a2 - 1.0 / (729.0 * a1 * a1 * a1)) + a2,1.0 / 3.0);
      double srd = a3 + 1.0 / (9.0 * a3 * a1) - 2.0 / (3.0 * vec_p(8));
      a = 1.0 + vec_p(8) * srd;
      dx = (float)(dxu / a + SrcImageCenter.X);
      dy = (float)(dyu / a + SrcImageCenter.Y);
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: LIR_MSSD_2DScale
//
///////////////////////////////////////////////////////////////////////////////

 double LIR_MSSD_2DScale::CostFunction (Matrix &vec_p)

{
   int x,y;

   double ssd = 0.0;
   NMeasurements = 0;
   GImage &r_image = *RefImage;
   GImage &s_image = *SrcImage;
   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   float scale_x = (float)vec_p(0);
   float scale_y = (float)vec_p(1);
   float trans_x = (float)vec_p(2);
   float trans_y = (float)vec_p(3);
   float fy = scale_y * RI_Min.Y + trans_y;
   for (y = RI_Min.Y; y <= RI_Max.Y; y++) {
      float fx = scale_x * RI_Min.X + trans_x;
      for (x = RI_Min.X; x <= RI_Max.X; x++) {
         if (r_image[y][x]) {
            int ix = (int)fx;
            int iy = (int)fy;
            if (ix > 0 && iy > 0 && ix < ex && iy < ey) {
               int ix1 = ix + 1;
               int iy1 = iy + 1;
               if (s_image[iy][ix] && s_image[iy1][ix] && s_image[iy][ix1] && s_image[iy1][ix1]) {
                  float rx  = fx - ix;
                  float ry  = fy - iy;
                  float rx1 = 1.0f - rx;
                  float ry1 = 1.0f - ry;
                  float b1  = ry1 * s_image[iy][ix ] + ry * s_image[iy1][ix ];
                  float b2  = ry1 * s_image[iy][ix1] + ry * s_image[iy1][ix1];
                  float er  = rx1 * b1 + rx * b2 - r_image[y][x];
                  ErrArray[NMeasurements++] = er;
                  ssd += er * er;
               }
            }
         }
         fx += scale_x;
      }
      fy += scale_y;
   }
   if (NMeasurements) return (sqrt (ssd / NMeasurements));
   else return (0.0);
}

 void LIR_MSSD_2DScale::GetHomographySub (Matrix &vec_p,Matrix &mat_H)

{
   mat_H.Clear (   );
   mat_H[0][0] = vec_p(0);
   mat_H[1][1] = vec_p(1);
   mat_H[0][2] = vec_p(2);
   mat_H[1][2] = vec_p(3);
   mat_H[2][2] = 1.0;
}

 void LIR_MSSD_2DScale::GetJacobianMatrix (Matrix &vec_p,Matrix &mat_J)

{
   int x,y;

   GImage &r_image = *RefImage;
   GImage &s_image = *SrcImage;
   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   int n_measurements = 0;
   float scale_x = (float)vec_p(0);
   float scale_y = (float)vec_p(1);
   float trans_x = (float)vec_p(2);
   float trans_y = (float)vec_p(3);
   float fy = scale_y * RI_Min.Y + trans_y;
   for (y = RI_Min.Y; y <= RI_Max.Y; y++) {
      float fx = scale_x * RI_Min.X + trans_x;
      for (x = RI_Min.X; x <= RI_Max.X; x++) {
         if (r_image[y][x]) {
            int ix = (int)fx;
            int iy = (int)fy;
            if (ix > 0 && iy > 0 && ix < ex && iy < ey) {
               int ix1 = ix + 1;
               int iy1 = iy + 1;
               if (s_image[iy][ix] && s_image[iy1][ix] && s_image[iy][ix1] && s_image[iy1][ix1]) {
                  float rx  = fx - ix;
                  float ry  = fy - iy;
                  float rx1 = 1.0f - rx;
                  float ry1 = 1.0f - ry;
                  float b1  = ry1 * GxArray[iy][ix ] + ry * GxArray[iy1][ix ];
                  float b2  = ry1 * GxArray[iy][ix1] + ry * GxArray[iy1][ix1];
                  float gx  = rx1 * b1 + rx * b2;
                        b1  = ry1 * GyArray[iy][ix ] + ry * GyArray[iy1][ix ];
                        b2  = ry1 * GyArray[iy][ix1] + ry * GyArray[iy1][ix1];
                  float gy  = rx1 * b1 + rx * b2;
                  mat_J[n_measurements][0] = gx * x;
                  mat_J[n_measurements][1] = gy * y;
                  mat_J[n_measurements][2] = gx;
                  mat_J[n_measurements][3] = gy;
                  n_measurements++;
               }
            }
         }
         fx += scale_x;
      }
      fy += scale_y;
   }
}

 void LIR_MSSD_2DScale::GetParameters (double &sx,double &sy,double &tx,double &ty)

{
   sx = ParamVector(0);
   sy = ParamVector(1);
   tx = ParamVector(2);
   ty = ParamVector(3);
}	

 void LIR_MSSD_2DScale::Initialize (GImage &r_image,GImage &s_image,double sx,double sy,double tx,double ty)

{
   LIR_MSSD::Initialize (r_image,s_image,4);
   ParamVector(0) = sx;
   ParamVector(1) = sy;
   ParamVector(2) = tx;
   ParamVector(3) = ty;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: LIR_MSSD_2DTranslational
//
///////////////////////////////////////////////////////////////////////////////

 double LIR_MSSD_2DTranslational::CostFunction (Matrix &vec_p)

{
   int x,y;

   double ssd = 0.0;
   NMeasurements = 0;
   GImage &r_image = *RefImage;
   GImage &s_image = *SrcImage;
   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   float trans_x = (float)vec_p(0);
   float trans_y = (float)vec_p(1);
   float fy = RI_Min.Y + trans_y;
   for (y = RI_Min.Y; y <= RI_Max.Y; y++) {
      float fx = RI_Min.X + trans_x;
      for (x = RI_Min.X; x <= RI_Max.X; x++) {
         if (r_image[y][x]) {
            int ix = (int)fx;
            int iy = (int)fy;
            if (ix > 0 && iy > 0 && ix < ex && iy < ey) {
               int ix1 = ix + 1;
               int iy1 = iy + 1;
               if (s_image[iy][ix] && s_image[iy1][ix] && s_image[iy][ix1] && s_image[iy1][ix1]) {
                  float rx  = fx - ix;
                  float ry  = fy - iy;
                  float rx1 = 1.0f - rx;
                  float ry1 = 1.0f - ry;
                  float b1  = ry1 * s_image[iy][ix ] + ry * s_image[iy1][ix ];
                  float b2  = ry1 * s_image[iy][ix1] + ry * s_image[iy1][ix1];
                  float er  = rx1 * b1 + rx * b2 - r_image[y][x];
                  ErrArray[NMeasurements++] = er;
                  ssd += er * er;
               }
            }
         }
         fx++;
      }
      fy++;
   }
   if (NMeasurements) return (sqrt (ssd / NMeasurements));
   else return (0.0);
}

 void LIR_MSSD_2DTranslational::GetHomographySub (Matrix &vec_p,Matrix &mat_H)

{
   mat_H = IM(3);
   mat_H[0][2] = vec_p(0);
   mat_H[1][2] = vec_p(1);
}

 void LIR_MSSD_2DTranslational::GetJacobianMatrix (Matrix &vec_p,Matrix &mat_J)

{
   int x,y;

   GImage &r_image = *RefImage;
   GImage &s_image = *SrcImage;
   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   int n_measurements = 0;
   float trans_x = (float)vec_p(0);
   float trans_y = (float)vec_p(1);
   float fy = RI_Min.Y + trans_y;
   for (y = RI_Min.Y; y <= RI_Max.Y; y++) {
      float fx = RI_Min.X + trans_x;
      for (x = RI_Min.X; x <= RI_Max.X; x++) {
         if (r_image[y][x]) {
            int ix = (int)fx;
            int iy = (int)fy;
            if (ix > 0 && iy > 0 && ix < ex && iy < ey) {
               int ix1 = ix + 1;
               int iy1 = iy + 1;
               if (s_image[iy][ix] && s_image[iy1][ix] && s_image[iy][ix1] && s_image[iy1][ix1]) {
                  float rx  = fx - ix;
                  float ry  = fy - iy;
                  float rx1 = 1.0f - rx;
                  float ry1 = 1.0f - ry;
                  float b1  = ry1 * GxArray[iy][ix ] + ry * GxArray[iy1][ix ];
                  float b2  = ry1 * GxArray[iy][ix1] + ry * GxArray[iy1][ix1];
                  float gx  = rx1 * b1 + rx * b2;
                        b1  = ry1 * GyArray[iy][ix ] + ry * GyArray[iy1][ix ];
                        b2  = ry1 * GyArray[iy][ix1] + ry * GyArray[iy1][ix1];
                  float gy  = rx1 * b1 + rx * b2;
                  mat_J[n_measurements][0] = gx;
                  mat_J[n_measurements][1] = gy;
                  n_measurements++;
               }
            }
         }
         fx++;
      }
      fy++;
   }
}

 void LIR_MSSD_2DTranslational::GetParameters (double &trans_x,double &trans_y)

{
   trans_x = ParamVector(0);
   trans_y = ParamVector(1);
}	

 void LIR_MSSD_2DTranslational::Initialize (GImage &r_image,GImage &s_image,double trans_x,double trans_y)

{
   LIR_MSSD::Initialize (r_image,s_image,2);
   ParamVector(0) = trans_x;
   ParamVector(1) = trans_y;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: LIR_MSSD_3DRotational_FXYZ
//
///////////////////////////////////////////////////////////////////////////////

 void LIR_MSSD_3DRotational_FXYZ::Derivative_Transformation (Matrix &,int n,int sx,int sy,float fx,float fy,float &dx,float &dy)

{
   int i;
   double m[3];

   for (i = 0; i < 3; i++) m[i] = Mats_Hd[n][i][0] * sx + Mats_Hd[n][i][1] * sy + Mats_Hd[n][i][2];
   double fx2 = m[0] / m[2];
   double fy2 = m[1] / m[2];
   dx = (float)((fx2 - fx) / Vec_dp(n));
   dy = (float)((fy2 - fy) / Vec_dp(n));
}

 void LIR_MSSD_3DRotational_FXYZ::GetHomographySub (Matrix &vec_p,Matrix &mat_H)

{
   GetHomography_3DRotational (vec_p(0),vec_p(0),vec_p(1),vec_p(2),vec_p(3),RefImage->Width,RefImage->Height,mat_H);
}

 void LIR_MSSD_3DRotational_FXYZ::GetParameters (double &f_length,double &psi,double &theta,double &phi)

{
   f_length = ParamVector(0);
   psi      = ParamVector(1);
   theta    = ParamVector(2);
   phi      = ParamVector(3);
}

 void LIR_MSSD_3DRotational_FXYZ::Initialize (GImage &r_image,GImage &s_image,double f_length,double psi,double theta,double phi)

{
   Vec_dp.Create  (4);
   Mats_Hd.Create (4);
   LIR_MSSD::Initialize (r_image,s_image,4);
   ParamVector(0) = f_length;
   ParamVector(1) = psi;
   ParamVector(2) = theta;
   ParamVector(3) = phi;
}

 void LIR_MSSD_3DRotational_FXYZ::Prepare_Derivative_Transformation (Matrix &vec_p)

{
   int i;
   
   for (i = 0; i < vec_p.NRows; i++) {
      double tp = vec_p(i);
      Vec_dp(i) = fabs (1E-4 * tp);
      if (Vec_dp(i) < 1E-6) Vec_dp(i) = 1E-6;
      vec_p(i) += Vec_dp(i);
      GetHomographySub (vec_p,Mats_Hd[i]);
      vec_p(i) = tp;
   }
}

 void LIR_MSSD_3DRotational_FXYZ::Prepare_Transformation (Matrix &vec_p)

{
   GetHomographySub (vec_p,Mat_H);
}

 void LIR_MSSD_3DRotational_FXYZ::Transformation (Matrix &,int sx,int sy,float &dx,float &dy)

{
   int i;
   double m[3];

   for (i = 0; i < 3; i++) m[i] = Mat_H[i][0] * sx + Mat_H[i][1] * sy + Mat_H[i][2];
   dx = (float)(m[0] / m[2]);
   dy = (float)(m[1] / m[2]);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: LIR_MSSD_3DRotational_FXYZ_LD
//
///////////////////////////////////////////////////////////////////////////////

 void LIR_MSSD_3DRotational_FXYZ_LD::Derivative_Transformation (Matrix &vec_p,int n,int sx,int sy,float fx,float fy,float &dx,float &dy)

{
   int i;
   float  fx2,fy2;
   double m[3],sxu,syu,kappa;

   int flag_nrd = FALSE;
   if (n == 4) kappa = vec_p(4) + Vec_dp(4);
   else kappa = vec_p(4); 
   if (kappa <= MC_VSV) {
      sxu = sx, syu = sy;
      flag_nrd = TRUE;
   }
   else {
      double sxd = sx - RefImageCenter.X;
      double syd = sy - RefImageCenter.Y;
      double a   = 1.0 + kappa * (sxd * sxd + syd * syd);
      sxu = sxd * a + RefImageCenter.X;
      syu = syd * a + RefImageCenter.Y;
   }
   for (i = 0; i < 3; i++) m[i] = Mats_Hd[n][i][0] * sxu + Mats_Hd[n][i][1] * syu + Mats_Hd[n][i][2];
   double dxu = m[0] / m[2];
   double dyu = m[1] / m[2];
   if (flag_nrd) fx2 = (float)dxu, fy2 = (float)dyu;
   else {
      dxu -= SrcImageCenter.X;
      dyu -= SrcImageCenter.Y;
      double sru = dxu * dxu + dyu * dyu;
      double a1  = kappa * kappa;
      double a2  = 1.0 / (27.0 * a1 * kappa) + sru / (2.0 * a1);
      double a3  = pow (sqrt (a2 * a2 - 1.0 / (729.0 * a1 * a1 * a1)) + a2,1.0 / 3.0);
      double srd = a3 + 1.0 / (9.0 * a3 * a1) - 2.0 / (3.0 * kappa);
      double a = 1.0 + kappa * srd;
      fx2 = (float)(dxu / a + SrcImageCenter.X);
      fy2 = (float)(dyu / a + SrcImageCenter.Y);
   }
   dx = (float)((fx2 - fx) / Vec_dp(n));
   dy = (float)((fy2 - fy) / Vec_dp(n));
}

 void LIR_MSSD_3DRotational_FXYZ_LD::GetHomographySub (Matrix &vec_p,Matrix &mat_H)

{
   GetHomography_3DRotational (vec_p(0),vec_p(0),vec_p(1),vec_p(2),vec_p(3),RefImage->Width,RefImage->Height,mat_H);
}

 void LIR_MSSD_3DRotational_FXYZ_LD::GetParameters (double &f_length,double &psi,double &theta,double &phi,double &kappa)

{
   f_length = ParamVector(0);
   psi      = ParamVector(1);
   theta    = ParamVector(2);
   phi      = ParamVector(3);
   kappa    = ParamVector(4);
}

 void LIR_MSSD_3DRotational_FXYZ_LD::Prepare_Derivative_Transformation (Matrix &vec_p)

{
   int i;
   
   for (i = 0; i < vec_p.NRows; i++) {
      double tp = vec_p(i);
      if (i != 4) {
         Vec_dp(i) = fabs (1E-4 * tp);
         if (Vec_dp(i) < 1E-6) Vec_dp(i) = 1E-6;
      }
      else Vec_dp(i) = fabs (1E-2 * tp);
      vec_p(i) += Vec_dp(i);
      GetHomographySub (vec_p,Mats_Hd[i]);
      vec_p(i) = tp;
   }
}

 void LIR_MSSD_3DRotational_FXYZ_LD::Prepare_Transformation (Matrix &vec_p)

{
   GetHomographySub (vec_p,Mat_H);
}

 void LIR_MSSD_3DRotational_FXYZ_LD::Transformation (Matrix &vec_p,int sx,int sy,float &dx,float &dy)

{
   int    i;
   double sxu,syu;
   double m[3];

   int flag_nrd = FALSE;
   if (fabs (vec_p(4)) <= MC_VSV) {
      sxu = sx, syu = sy;
      flag_nrd = TRUE;
   }
   else {
      double sxd = sx - RefImageCenter.X;
      double syd = sy - RefImageCenter.Y;
      double a   = 1.0 + vec_p(4) * (sxd * sxd + syd * syd);
      sxu = sxd * a + RefImageCenter.X;
      syu = syd * a + RefImageCenter.Y;
   }
   for (i = 0; i < 3; i++) m[i] = Mat_H[i][0] * sxu + Mat_H[i][1] * syu + Mat_H[i][2];
   double dxu = (float)(m[0] / m[2]);
   double dyu = (float)(m[1] / m[2]);
   if (flag_nrd) dx = (float)dxu, dy = (float)dyu;
   else {
      dxu -= SrcImageCenter.X;
      dyu -= SrcImageCenter.Y;
      double sru = dxu * dxu + dyu * dyu;
      double a1  = vec_p(4) * vec_p(4);
      double a2  = 1.0 / (27.0 * a1 * vec_p(4)) + sru / (2.0 * a1);
      double a3  = pow (sqrt (a2 * a2 - 1.0 / (729.0 * a1 * a1 * a1)) + a2,1.0 / 3.0);
      double srd = a3 + 1.0 / (9.0 * a3 * a1) - 2.0 / (3.0 * vec_p(4));
      double a = 1.0 + vec_p(4) * srd;
      dx = (float)(dxu / a + SrcImageCenter.X);
      dy = (float)(dyu / a + SrcImageCenter.Y);
   }
}

 void LIR_MSSD_3DRotational_FXYZ_LD::Initialize (GImage &r_image,GImage &s_image,double f_length,double psi,double theta,double phi,double kappa)

{
   Vec_dp.Create  (5);
   Mats_Hd.Create (5);
   LIR_MSSD::Initialize (r_image,s_image,5);
   ParamVector(0) = f_length;
   ParamVector(1) = psi;
   ParamVector(2) = theta;
   ParamVector(3) = phi;
   ParamVector(4) = kappa;
   RefImageCenter(0.5f * r_image.Width,0.5f * r_image.Height);
   SrcImageCenter(0.5f * s_image.Width,0.5f * s_image.Height);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: LIR_MSSD_3DRotational_XY
//
///////////////////////////////////////////////////////////////////////////////

 void LIR_MSSD_3DRotational_XY::Derivative_Transformation (Matrix &,int n,int sx,int sy,float fx,float fy,float &dx,float &dy)

{
   int i;
   double m[3];

   for (i = 0; i < 3; i++) m[i] = Mats_Hd[n][i][0] * sx + Mats_Hd[n][i][1] * sy + Mats_Hd[n][i][2];
   double fx2 = m[0] / m[2];
   double fy2 = m[1] / m[2];
   dx = (float)((fx2 - fx) / Vec_dp(n));
   dy = (float)((fy2 - fy) / Vec_dp(n));
}

 void LIR_MSSD_3DRotational_XY::GetHomographySub (Matrix &vec_p,Matrix &mat_H)

{
   GetHomography_3DRotational (FocalLength,FocalLength,vec_p(0),vec_p(1),Angle_Phi,RefImage->Width,RefImage->Height,mat_H);
}

 void LIR_MSSD_3DRotational_XY::GetParameters (double &psi,double &theta)

{
   psi   = ParamVector(0);
   theta = ParamVector(1);
}

 void LIR_MSSD_3DRotational_XY::Initialize (GImage &r_image,GImage &s_image,double f_length,double psi,double theta,double phi)

{
   Vec_dp.Create  (2);
   Mats_Hd.Create (2);
   LIR_MSSD::Initialize (r_image,s_image,2);
   FocalLength    = f_length;
   ParamVector(0) = psi;
   ParamVector(1) = theta;
   Angle_Phi      = phi;
}

 void LIR_MSSD_3DRotational_XY::Prepare_Derivative_Transformation (Matrix &vec_p)

{
   int i;
   
   for (i = 0; i < vec_p.NRows; i++) {
      double tp = vec_p(i);
      Vec_dp(i) = fabs (1E-4 * tp);
      if (Vec_dp(i) < 1E-6) Vec_dp(i) = 1E-6;
      vec_p(i) += Vec_dp(i);
      GetHomographySub (vec_p,Mats_Hd[i]);
      vec_p(i) = tp;
   }
}

 void LIR_MSSD_3DRotational_XY::Prepare_Transformation (Matrix &vec_p)

{
   GetHomographySub (vec_p,Mat_H);
}

 void LIR_MSSD_3DRotational_XY::Transformation (Matrix &,int sx,int sy,float &dx,float &dy)

{
   int i;
   double m[3];

   for (i = 0; i < 3; i++) m[i] = Mat_H[i][0] * sx + Mat_H[i][1] * sy + Mat_H[i][2];
   dx = (float)(m[0] / m[2]);
   dy = (float)(m[1] / m[2]);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: LIR_MSSD_3DRotational_XYZ
//
///////////////////////////////////////////////////////////////////////////////

 void LIR_MSSD_3DRotational_XYZ::Derivative_Transformation (Matrix &,int n,int sx,int sy,float fx,float fy,float &dx,float &dy)

{
   int i;
   double m[3];

   for (i = 0; i < 3; i++) m[i] = Mats_Hd[n][i][0] * sx + Mats_Hd[n][i][1] * sy + Mats_Hd[n][i][2];
   double fx2 = m[0] / m[2];
   double fy2 = m[1] / m[2];
   dx = (float)((fx2 - fx) / Vec_dp(n));
   dy = (float)((fy2 - fy) / Vec_dp(n));
}

 void LIR_MSSD_3DRotational_XYZ::GetHomographySub (Matrix &vec_p,Matrix &mat_H)

{
   GetHomography_3DRotational (FocalLength,FocalLength,vec_p(0),vec_p(1),vec_p(2),RefImage->Width,RefImage->Height,mat_H);
}

 void LIR_MSSD_3DRotational_XYZ::GetParameters (double &psi,double &theta,double &phi)

{
   psi   = ParamVector(0);
   theta = ParamVector(1);
   phi   = ParamVector(2);
}

 void LIR_MSSD_3DRotational_XYZ::Initialize (GImage &r_image,GImage &s_image,double f_length,double psi,double theta,double phi)

{
   Vec_dp.Create  (3);
   Mats_Hd.Create (3);
   LIR_MSSD::Initialize (r_image,s_image,3);
   FocalLength    = f_length;
   ParamVector(0) = psi;
   ParamVector(1) = theta;
   ParamVector(2) = phi;
}

 void LIR_MSSD_3DRotational_XYZ::Prepare_Derivative_Transformation (Matrix &vec_p)

{
   int i;
   
   for (i = 0; i < vec_p.NRows; i++) {
      double tp = vec_p(i);
      Vec_dp(i) = fabs (1E-4 * tp);
      if (Vec_dp(i) < 1E-6) Vec_dp(i) = 1E-6;
      vec_p(i) += Vec_dp(i);
      GetHomographySub (vec_p,Mats_Hd[i]);
      vec_p(i) = tp;
   }
}

 void LIR_MSSD_3DRotational_XYZ::Prepare_Transformation (Matrix &vec_p)

{
   GetHomographySub (vec_p,Mat_H);
}

 void LIR_MSSD_3DRotational_XYZ::Transformation (Matrix &,int sx,int sy,float &dx,float &dy)

{
   int i;
   double m[3];

   for (i = 0; i < 3; i++) m[i] = Mat_H[i][0] * sx + Mat_H[i][1] * sy + Mat_H[i][2];
   dx = (float)(m[0] / m[2]);
   dy = (float)(m[1] / m[2]);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: HierarchicalLIR_MSSD
//
///////////////////////////////////////////////////////////////////////////////

 void HierarchicalLIR_MSSD::Close (   )

{
   ParamVector.Delete (   );
}

 void HierarchicalLIR_MSSD::GetReducedImages (GImage &rr_image,GImage &rs_image)

{
   GImage &r_image = *RefImage;
   GImage &s_image = *SrcImage;
   if (Scale >= 1.0f) {
      Scale    = 1.0f;
      rr_image = r_image;
      rs_image = s_image;
   }
   else {
      rr_image.Create ((int)(r_image.Width * Scale),(int)(r_image.Height * Scale));
      rs_image.Create ((int)(s_image.Width * Scale),(int)(s_image.Height * Scale));
      Reduce (r_image,rr_image);
      Reduce (s_image,rs_image);
   }
}

 void HierarchicalLIR_MSSD::Initialize (GImage &r_image,GImage &s_image,int n_params,int coarsest_img_size)

{
   int img_size;
   
   NumIterations = 0;
   Flag_Stop     = FALSE;
   RefImage      = &r_image;
   SrcImage       = &s_image;
   ParamVector.Create (n_params);
   if (s_image.Width > s_image.Height) img_size = s_image.Width;
   else img_size = s_image.Height;
   Scale = (float)coarsest_img_size / img_size;
}

 void HierarchicalLIR_MSSD::Perform (float scale_inc,int n_iterations1,int n_iterations2,double d_thrsld,double e_thrsld)

{
   int i;
   
   for (i = 0; n_iterations1 <= 0 || i < n_iterations1; i++)
      if (Update (scale_inc,n_iterations2,d_thrsld,e_thrsld)) break;
}

 void HierarchicalLIR_MSSD::PreprocessImages (GImage &r_image,GImage &s_image)

{
   int y,x,y2,x2;

   GImage d_image(s_image.Width,s_image.Height);
   FArray2D gm_array(s_image.Width,s_image.Height);
   GetGradientMagnitudes_Sobel (s_image,gm_array);
   float gm_thrsld = (float)GetMean (gm_array);
   int hws = 4;
   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   for (y = 0; y <= ey; y++) {
      int sy2 = y - hws;
      int ey2 = y + hws;
      if (sy2 <  0) sy2 = 0;
      if (ey2 > ey) ey2 = ey;
      float* l_gm_array = gm_array;
      for (x = 0; x <= ex; x++) {
         if (l_gm_array[x] >= gm_thrsld) {
            int sx2 = x - hws;
            int ex2 = x + hws;
            if (sx2 <  0) sx2 = 0;
            if (ex2 > ex) ex2 = ex;
            for (y2 = sy2; y2 <= ey2; y2++) {
               byte* l_s_image = s_image[y2];
               byte* l_d_image = d_image[y2];
               for (x2 = sx2; x2 <= ex2; x2++) {
                  l_d_image[x2] = l_s_image[x2];
               }
            }
         }
      }
   }
   s_image = d_image;
}

 int HierarchicalLIR_MSSD::Update (float scale_inc,int n_iterations,double d_thrsld,double e_thrsld)

{
   if (Flag_Stop) return (TRUE);
   NumIterations++;
   GImage r_image,s_image;
   GetReducedImages (r_image,s_image);
   ScaleDown (   );
   PreprocessImages (r_image,s_image);
   PerformLIR (r_image,s_image,n_iterations,d_thrsld,e_thrsld);
   ScaleUp (   );
   if (Scale >= 1.0f) Flag_Stop = TRUE;
   else Scale *= scale_inc;
   return (Flag_Stop);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: HLIR_MSSD_2DAffine
//
///////////////////////////////////////////////////////////////////////////////
 
 void HLIR_MSSD_2DAffine::GetHomography (Matrix &mat_H)

{
   int i;

   for (i = 0; i < 9; i++) mat_H(i) = ParamVector(i);
}

 void HLIR_MSSD_2DAffine::Initialize (GImage &r_image,GImage &s_image,Matrix &mat_H,int coarsest_img_size)

{
   HierarchicalLIR_MSSD::Initialize (r_image,s_image,9,coarsest_img_size);
   SetHomography (mat_H);
}

 void HLIR_MSSD_2DAffine::PerformLIR (GImage &r_image,GImage &s_image,int n_iterations,double d_thrsld,double e_thrsld)

{
   Matrix mat_H(3,3);
   GetHomography (mat_H);
   LIR_MSSD_2DAffine lir;
   lir.Initialize (r_image,s_image,mat_H);
   lir.Perform (n_iterations,d_thrsld,e_thrsld);
   lir.GetHomography (mat_H);
   SetHomography (mat_H);
}

 void HLIR_MSSD_2DAffine::ScaleDown (   )

{
   Matrix mat_H(3,3);
   GetHomography (mat_H);
   Matrix mat_S  = IM(3);
   Matrix mat_IS = IM(3);
   mat_S [0][0] = mat_S [1][1] = Scale;
   mat_IS[0][0] = mat_IS[1][1] = 1.0f / Scale;
   mat_H = mat_S * mat_H * mat_IS;
   SetHomography (mat_H);
}   

 void HLIR_MSSD_2DAffine::ScaleUp (   )

{
   Matrix mat_H(3,3);
   GetHomography (mat_H);
   Matrix mat_S  = IM(3);
   Matrix mat_IS = IM(3);
   mat_S [0][0] = mat_S [1][1] = Scale;
   mat_IS[0][0] = mat_IS[1][1] = 1.0f / Scale;
   mat_H = mat_IS * mat_H * mat_S;
   SetHomography (mat_H);
}

 void HLIR_MSSD_2DAffine::SetHomography (Matrix &mat_H)

{
   int i;

   for (i = 0; i < 9; i++) ParamVector(i) = mat_H(i);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: HLIR_MSSD_2DEuclidean
//
///////////////////////////////////////////////////////////////////////////////

 void HLIR_MSSD_2DEuclidean::GetHomography (Matrix &mat_H)

{
   GetHomography_2DEuclidean (ParamVector(0),ParamVector(1),ParamVector(2),mat_H);
}

 void HLIR_MSSD_2DEuclidean::GetParameters (double &theta,double &tx,double &ty)

{
   theta = ParamVector(0);
   tx    = ParamVector(1);
   ty    = ParamVector(2);
}

 void HLIR_MSSD_2DEuclidean::Initialize (GImage &r_image,GImage &s_image,double theta,double tx,double ty,int coarsest_img_size)

{
   HierarchicalLIR_MSSD::Initialize (r_image,s_image,3,coarsest_img_size);
   SetParameters (theta,tx,ty);
}

 void HLIR_MSSD_2DEuclidean::PerformLIR (GImage &r_image,GImage &s_image,int n_iterations,double d_thrsld,double e_thrsld)

{
   LIR_MSSD_2DEuclidean lir;
   lir.Initialize (r_image,s_image,ParamVector(0),ParamVector(1),ParamVector(2));
   lir.Perform (n_iterations,d_thrsld,e_thrsld);
   lir.GetParameters (ParamVector(0),ParamVector(1),ParamVector(2));
}

 void HLIR_MSSD_2DEuclidean::ScaleDown (   )

{
   ParamVector(1) *= Scale;
   ParamVector(2) *= Scale;
}   

 void HLIR_MSSD_2DEuclidean::ScaleUp (   )

{
   ParamVector(1) /= Scale;
   ParamVector(2) /= Scale;
}

 void HLIR_MSSD_2DEuclidean::SetParameters (double theta,double tx,double ty)

{
   ParamVector(0) = theta;
   ParamVector(1) = tx;
   ParamVector(2) = ty;
}

///////////////////////////////////////////////////////////////////////////////
//
//  Class: HLIR_MSSD_2DProjective
//
///////////////////////////////////////////////////////////////////////////////

 void HLIR_MSSD_2DProjective::GetHomography (Matrix &mat_H)

{
   int i;

   for (i = 0; i < 9; i++) mat_H(i) = ParamVector(i);
}

 void HLIR_MSSD_2DProjective::Initialize (GImage &r_image,GImage &s_image,Matrix &mat_H,int coarsest_img_size)

{
   HierarchicalLIR_MSSD::Initialize (r_image,s_image,9,coarsest_img_size);
   SetHomography (mat_H);
}

 void HLIR_MSSD_2DProjective::PerformLIR (GImage &r_image,GImage &s_image,int n_iterations,double d_thrsld,double e_thrsld)

{
   Matrix mat_H(3,3);
   GetHomography (mat_H);
   LIR_MSSD_2DProjective lir;
   lir.Initialize (r_image,s_image,mat_H);
   lir.Perform (n_iterations,d_thrsld,e_thrsld);
   lir.GetHomography (mat_H);
   SetHomography (mat_H);
}

 void HLIR_MSSD_2DProjective::ScaleDown (   )

{
   Matrix mat_H(3,3);
   GetHomography (mat_H);
   Matrix mat_S  = IM(3);
   Matrix mat_IS = IM(3);
   mat_S [0][0] = mat_S [1][1] = Scale;
   mat_IS[0][0] = mat_IS[1][1] = 1.0f / Scale;
   mat_H = mat_S * mat_H * mat_IS;
   SetHomography (mat_H);
}   

 void HLIR_MSSD_2DProjective::ScaleUp (   )

{
   Matrix mat_H(3,3);
   GetHomography (mat_H);
   Matrix mat_S  = IM(3);
   Matrix mat_IS = IM(3);
   mat_S [0][0] = mat_S [1][1] = Scale;
   mat_IS[0][0] = mat_IS[1][1] = 1.0f / Scale;
   mat_H = mat_IS * mat_H * mat_S;
   SetHomography (mat_H);
}

 void HLIR_MSSD_2DProjective::SetHomography (Matrix &mat_H)

{
   int i;

   for (i = 0; i < 9; i++) ParamVector(i) = mat_H(i);
}

///////////////////////////////////////////////////////////////////////////////
//
//  Class: HLIR_MSSD_2DProjective_LD
//
///////////////////////////////////////////////////////////////////////////////

 void HLIR_MSSD_2DProjective_LD::GetHomography (Matrix &mat_H)

{
   int i;

   for (i = 0; i < 9; i++) mat_H(i) = ParamVector(i);
}

 double HLIR_MSSD_2DProjective_LD::GetLensDistortionCoefficient (   )

{
   return (ParamVector(9));
}

 void HLIR_MSSD_2DProjective_LD::Initialize (GImage &r_image,GImage &s_image,Matrix &mat_H,double kappa,int coarsest_img_size)

{
   HierarchicalLIR_MSSD::Initialize (r_image,s_image,10,coarsest_img_size);
   SetHomography (mat_H);
   SetLensDistortionCoefficient (kappa);
}

 void HLIR_MSSD_2DProjective_LD::PerformLIR (GImage &r_image,GImage &s_image,int n_iterations,double d_thrsld,double e_thrsld)

{
   Matrix mat_H(3,3);
   GetHomography (mat_H);
   double kappa = GetLensDistortionCoefficient (   );
   LIR_MSSD_2DProjective_LD lir;
   lir.Initialize (r_image,s_image,mat_H,kappa);
   lir.Perform (n_iterations,d_thrsld,e_thrsld);
   lir.GetHomography (mat_H);
   kappa = lir.GetLensDistortionCoefficient (   );
   SetHomography (mat_H);
   SetLensDistortionCoefficient (kappa);
}

 void HLIR_MSSD_2DProjective_LD::ScaleDown (   )

{
   Matrix mat_H(3,3);
   GetHomography (mat_H);
   Matrix mat_S  = IM(3);
   Matrix mat_IS = IM(3);
   mat_S [0][0] = mat_S [1][1] = Scale;
   mat_IS[0][0] = mat_IS[1][1] = 1.0f / Scale;
   mat_H = mat_S * mat_H * mat_IS;
   SetHomography (mat_H);
   double kappa = GetLensDistortionCoefficient (   );
   SetLensDistortionCoefficient (kappa / (Scale * Scale));
}   

 void HLIR_MSSD_2DProjective_LD::ScaleUp (   )

{
   Matrix mat_H(3,3);
   GetHomography (mat_H);
   Matrix mat_S  = IM(3);
   Matrix mat_IS = IM(3);
   mat_S [0][0] = mat_S [1][1] = Scale;
   mat_IS[0][0] = mat_IS[1][1] = 1.0f / Scale;
   mat_H = mat_IS * mat_H * mat_S;
   SetHomography (mat_H);
   double kappa = GetLensDistortionCoefficient (   );
   SetLensDistortionCoefficient (kappa * Scale * Scale);
}

 void HLIR_MSSD_2DProjective_LD::SetHomography (Matrix &mat_H)

{
   int i;

   for (i = 0; i < 9; i++) ParamVector(i) = mat_H(i);
}

 void HLIR_MSSD_2DProjective_LD::SetLensDistortionCoefficient (double kappa)

{
   ParamVector(9) = kappa;
}

///////////////////////////////////////////////////////////////////////////////
//
//  Class: HLIR_MSSD_2DScale
//
///////////////////////////////////////////////////////////////////////////////

 void HLIR_MSSD_2DScale::GetHomography (Matrix &mat_H)

{
   mat_H.Clear (   );
   mat_H[0][0] = ParamVector(0);
   mat_H[1][1] = ParamVector(1);
   mat_H[0][2] = ParamVector(2);
   mat_H[1][2] = ParamVector(3);
   mat_H[2][2] = 1.0;
}

 void HLIR_MSSD_2DScale::GetParameters (double &sx,double &sy,double &tx,double &ty)

{
   sx = ParamVector(0);
   sy = ParamVector(1);
   tx = ParamVector(2);
   ty = ParamVector(3);
}

 void HLIR_MSSD_2DScale::Initialize (GImage &r_image,GImage &s_image,double sx,double sy,double tx,double ty,int coarsest_img_size)

{
   HierarchicalLIR_MSSD::Initialize (r_image,s_image,4,coarsest_img_size);
   SetParameters (sx,sy,tx,ty);
}

 void HLIR_MSSD_2DScale::PerformLIR (GImage &r_image,GImage &s_image,int n_iterations,double d_thrsld,double e_thrsld)

{
   LIR_MSSD_2DScale lir;
   lir.Initialize (r_image,s_image,ParamVector(0),ParamVector(1),ParamVector(2),ParamVector(3));
   lir.Perform (n_iterations,d_thrsld,e_thrsld);
   lir.GetParameters (ParamVector(0),ParamVector(1),ParamVector(2),ParamVector(3));
}

 void HLIR_MSSD_2DScale::ScaleDown (   )

{
   ParamVector(2) *= Scale;
   ParamVector(3) *= Scale;
}   

 void HLIR_MSSD_2DScale::ScaleUp (   )

{
   ParamVector(2) /= Scale;
   ParamVector(3) /= Scale;
}

 void HLIR_MSSD_2DScale::SetParameters (double sx,double sy,double tx,double ty)

{
   ParamVector(0) = sx;
   ParamVector(1) = sy;
   ParamVector(2) = tx;
   ParamVector(3) = ty;
}

///////////////////////////////////////////////////////////////////////////////
//
//  Class: HLIR_MSSD_3DRotational_FXYZ
//
///////////////////////////////////////////////////////////////////////////////
 
 void HLIR_MSSD_3DRotational_FXYZ::GetHomography (Matrix &mat_H)

{
   GetHomography_3DRotational (ParamVector(0),ParamVector(0),ParamVector(1),ParamVector(2),ParamVector(3),RefImage->Width,RefImage->Height,mat_H);
}

 void HLIR_MSSD_3DRotational_FXYZ::GetParameters (double &f_length,double &psi,double &theta,double &phi)

{
   f_length = ParamVector(0);
   psi      = ParamVector(1);
   theta    = ParamVector(2);
   phi      = ParamVector(3);
}

 void HLIR_MSSD_3DRotational_FXYZ::Initialize (GImage &r_image,GImage &s_image,double f_length,double psi,double theta,double phi,int coarsest_img_size)

{
   HierarchicalLIR_MSSD::Initialize (r_image,s_image,4,coarsest_img_size);
   SetParameters (f_length,psi,theta,phi);
}

 void HLIR_MSSD_3DRotational_FXYZ::PerformLIR (GImage &r_image,GImage &s_image,int n_iterations,double d_thrsld,double e_thrsld)

{
   LIR_MSSD_3DRotational_FXYZ lir;
   lir.Initialize (r_image,s_image,ParamVector(0),ParamVector(1),ParamVector(2),ParamVector(3));
   lir.Perform (n_iterations,d_thrsld,e_thrsld);
   lir.GetParameters (ParamVector(0),ParamVector(1),ParamVector(2),ParamVector(3));
}

 void HLIR_MSSD_3DRotational_FXYZ::ScaleDown (   )

{
   ParamVector(0) *= Scale;   
}   

 void HLIR_MSSD_3DRotational_FXYZ::ScaleUp (   )

{
   ParamVector(0) /= Scale;
}

 void HLIR_MSSD_3DRotational_FXYZ::SetParameters (double f_length,double psi,double theta,double phi)

{
   ParamVector(0) = f_length;
   ParamVector(1) = psi;
   ParamVector(2) = theta;
   ParamVector(3) = phi;
}

///////////////////////////////////////////////////////////////////////////////
//
//  Class: HLIR_MSSD_3DRotational_FXYZ_LD
//
///////////////////////////////////////////////////////////////////////////////

 void HLIR_MSSD_3DRotational_FXYZ_LD::GetHomography (Matrix &mat_H)

{
   GetHomography_3DRotational (ParamVector(0),ParamVector(0),ParamVector(1),ParamVector(2),ParamVector(3),RefImage->Width,RefImage->Height,mat_H);
}

 void HLIR_MSSD_3DRotational_FXYZ_LD::GetParameters (double &f_length,double &psi,double &theta,double &phi,double &kappa)

{
   f_length = ParamVector(0);
   psi      = ParamVector(1);
   theta    = ParamVector(2);
   phi      = ParamVector(3);
   kappa    = ParamVector(4);
}

 void HLIR_MSSD_3DRotational_FXYZ_LD::Initialize (GImage &r_image,GImage &s_image,double f_length,double psi,double theta,double phi,double kappa,int coarsest_img_size)

{
   HierarchicalLIR_MSSD::Initialize (r_image,s_image,5,coarsest_img_size);
   SetParameters (f_length,psi,theta,phi,kappa);
}

 void HLIR_MSSD_3DRotational_FXYZ_LD::PerformLIR (GImage &r_image,GImage &s_image,int n_iterations,double d_thrsld,double e_thrsld)

{
   LIR_MSSD_3DRotational_FXYZ_LD lir;
   lir.Initialize (r_image,s_image,ParamVector(0),ParamVector(1),ParamVector(2),ParamVector(3),ParamVector(4));
   lir.Perform (n_iterations,d_thrsld,e_thrsld);
   lir.GetParameters (ParamVector(0),ParamVector(1),ParamVector(2),ParamVector(3),ParamVector(4));
}

 void HLIR_MSSD_3DRotational_FXYZ_LD::ScaleDown (   )

{
   ParamVector(0) *= Scale;
   ParamVector(4) /= (Scale * Scale);
}   

 void HLIR_MSSD_3DRotational_FXYZ_LD::ScaleUp (   )

{
   ParamVector(0) /= Scale;
   ParamVector(4) *= (Scale * Scale);
}

 void HLIR_MSSD_3DRotational_FXYZ_LD::SetParameters (double f_length,double psi,double theta,double phi,double kappa)

{
   ParamVector(0) = f_length;
   ParamVector(1) = psi;
   ParamVector(2) = theta;
   ParamVector(3) = phi;
   ParamVector(4) = kappa;
}

///////////////////////////////////////////////////////////////////////////////
//
//  Class: HLIR_MSSD_3DRotational_XY
//
///////////////////////////////////////////////////////////////////////////////

 void HLIR_MSSD_3DRotational_XY::GetHomography (Matrix &mat_H)

{
   GetHomography_3DRotational (FocalLength,FocalLength,ParamVector(0),ParamVector(1),Angle_Phi,RefImage->Width,RefImage->Height,mat_H);
}

 void HLIR_MSSD_3DRotational_XY::GetParameters (double &psi,double &theta)

{
   psi   = ParamVector(0);
   theta = ParamVector(1);
}

 void HLIR_MSSD_3DRotational_XY::Initialize (GImage &r_image,GImage &s_image,double f_length,double psi,double theta,double phi,int coarsest_img_size)

{
   HierarchicalLIR_MSSD::Initialize (r_image,s_image,2,coarsest_img_size);
   SetParameters (f_length,psi,theta,phi);
}

 void HLIR_MSSD_3DRotational_XY::PerformLIR (GImage &r_image,GImage &s_image,int n_iterations,double d_thrsld,double e_thrsld)

{
   LIR_MSSD_3DRotational_XY lir;
   lir.Initialize (r_image,s_image,FocalLength,ParamVector(0),ParamVector(1),Angle_Phi);
   lir.Perform (n_iterations,d_thrsld,e_thrsld);
   lir.GetParameters (ParamVector(0),ParamVector(1));
}

 void HLIR_MSSD_3DRotational_XY::ScaleDown (   )

{
   FocalLength *= Scale;
}   

 void HLIR_MSSD_3DRotational_XY::ScaleUp (   )

{
   FocalLength /= Scale;
}

 void HLIR_MSSD_3DRotational_XY::SetParameters (double f_length,double psi,double theta,double phi)

{
   FocalLength    = f_length;
   ParamVector(0) = psi;
   ParamVector(1) = theta;
   Angle_Phi      = phi;
}

///////////////////////////////////////////////////////////////////////////////
//
//  Class: HLIR_MSSD_3DRotational_XYZ
//
///////////////////////////////////////////////////////////////////////////////

 void HLIR_MSSD_3DRotational_XYZ::GetHomography (Matrix &mat_H)

{
   GetHomography_3DRotational (FocalLength,FocalLength,ParamVector(0),ParamVector(1),ParamVector(2),RefImage->Width,RefImage->Height,mat_H);
}

 void HLIR_MSSD_3DRotational_XYZ::GetParameters (double &psi,double &theta,double &phi)

{
   psi   = ParamVector(0);
   theta = ParamVector(1);
   phi   = ParamVector(2);
}

 void HLIR_MSSD_3DRotational_XYZ::Initialize (GImage &r_image,GImage &s_image,double f_length,double psi,double theta,double phi,int coarsest_img_size)

{
   HierarchicalLIR_MSSD::Initialize (r_image,s_image,3,coarsest_img_size);
   SetParameters (f_length,psi,theta,phi);
}

 void HLIR_MSSD_3DRotational_XYZ::PerformLIR (GImage &r_image,GImage &s_image,int n_iterations,double d_thrsld,double e_thrsld)

{
   LIR_MSSD_3DRotational_XYZ lir;
   lir.Initialize (r_image,s_image,FocalLength,ParamVector(0),ParamVector(1),ParamVector(2));
   lir.Perform (n_iterations,d_thrsld,e_thrsld);
   lir.GetParameters (ParamVector(0),ParamVector(1),ParamVector(2));
}

 void HLIR_MSSD_3DRotational_XYZ::ScaleDown (   )

{
   FocalLength *= Scale;
}   

 void HLIR_MSSD_3DRotational_XYZ::ScaleUp (   )

{
   FocalLength /= Scale;
}

 void HLIR_MSSD_3DRotational_XYZ::SetParameters (double f_length,double psi,double theta,double phi)

{
   FocalLength    = f_length;
   ParamVector(0) = psi;
   ParamVector(1) = theta;
   ParamVector(2) = phi;
}

///////////////////////////////////////////////////////////////////////////////
//
//  Class: HierarchicalTM
//
///////////////////////////////////////////////////////////////////////////////

 FPoint2D HierarchicalTM::GetMatchingPosition (   )

{
   return (MatchingPos);
}

 void HierarchicalTM::GetReducedImages (GImage &rs_image,GImage &rt_image)

{
   int rs_width,rs_height;
   int rt_width,rt_height;

   GImage &s_image = *SrcImage;
   GImage &t_image = *TplImage;
   if (Scale >= 1.0f) {
      Scale     = 1.0f;
      rs_width  = s_image.Width;
      rs_height = s_image.Height;
      rt_width  = t_image.Width;
      rt_height = t_image.Height;
   }
   else {
      rs_width  = (int)(s_image.Width  * Scale + 0.5f);
      rs_height = (int)(s_image.Height * Scale + 0.5f);
      rt_width  = (int)(t_image.Width  * Scale + 0.5f);
      rt_height = (int)(t_image.Height * Scale + 0.5f);
   }
   rs_image.Create (rs_width,rs_height);
   rt_image.Create (rt_width,rt_height);
   if (Scale == 1.0f) {
      rs_image = s_image;
      rt_image = t_image;
   }
   else {
      Reduce (s_image,rs_image);
      Reduce (t_image,rt_image);
   }
}

 void HierarchicalTM::Initialize (GImage &s_image,GImage &t_image,int coarsest_img_size,FPoint2D &lt_overlap,FPoint2D &rb_overlap)

{
   int img_size;
   
   Flag_Stop     = FALSE;
   NumIterations = 0;
   LTOverlap     = lt_overlap;
   RBOverlap     = rb_overlap;
   SrcImage      = &s_image;
   TplImage      = &t_image;
   if (s_image.Width > s_image.Height) img_size = s_image.Width;
   else img_size = s_image.Height;
   Scale = (float)coarsest_img_size / img_size;
}

 float HierarchicalTM::Perform (float scale_inc,int win_size,int n_iterations)

{
   int   i;
   float error = 0.0f;
   
   for (i = 0; n_iterations <= 0 || i < n_iterations; i++)
      if (Update (scale_inc,win_size,error)) break;
   return (error);
}

 int HierarchicalTM::Update (float scale_inc,int win_size,float &error)

{
   if (Flag_Stop) return (TRUE);
   NumIterations++;
   GImage s_image,t_image;
   GetReducedImages (s_image,t_image);
   IPoint2D d_pos;
   if (NumIterations == 1) error = PerformInitTemplateMatching (s_image,t_image,d_pos);
   else {
      IPoint2D  s_pos((int)(MatchingPos.X * Scale),(int)(MatchingPos.Y * Scale));
      error = PerformTemplateMatching (s_image,t_image,s_pos,win_size,d_pos);
   }
   MatchingPos(d_pos.X / Scale,d_pos.Y / Scale);
   if (Scale == 1.0f) Flag_Stop = TRUE;
   else Scale *= scale_inc;
   return (Flag_Stop);
}

///////////////////////////////////////////////////////////////////////////////
//
//  Class: HTM_NCC1
//
///////////////////////////////////////////////////////////////////////////////

 float HTM_NCC1::PerformInitTemplateMatching (GImage &s_image,GImage &t_image,IPoint2D &d_pos)

{
   return (PerformTemplateMatching_NCC1 (s_image,t_image,LTOverlap,RBOverlap,d_pos));
}
 
 float HTM_NCC1::PerformTemplateMatching (GImage &s_image,GImage &t_image,IPoint2D &s_pos,int win_size,IPoint2D &d_pos)

{
   int dt = win_size / 2;
   IPoint2D min_pos(s_pos.X - dt,s_pos.Y - dt);
   IPoint2D max_pos(s_pos.X + dt,s_pos.Y + dt);
   return (PerformTemplateMatching_NCC1 (s_image,t_image,min_pos,max_pos,d_pos));
}

///////////////////////////////////////////////////////////////////////////////
//
//  Class: HTM_NCC2
//
///////////////////////////////////////////////////////////////////////////////

 float HTM_NCC2::PerformInitTemplateMatching (GImage &s_image,GImage &t_image,IPoint2D &d_pos)

{
   return (PerformTemplateMatching_NCC2 (s_image,t_image,LTOverlap,RBOverlap,d_pos));
}
 
 float HTM_NCC2::PerformTemplateMatching (GImage &s_image,GImage &t_image,IPoint2D &s_pos,int win_size,IPoint2D &d_pos)

{
   int dt = win_size / 2;
   IPoint2D min_pos(s_pos.X - dt,s_pos.Y - dt);
   IPoint2D max_pos(s_pos.X + dt,s_pos.Y + dt);
   return (PerformTemplateMatching_NCC2 (s_image,t_image,min_pos,max_pos,d_pos));
}

///////////////////////////////////////////////////////////////////////////////
//
//  Functions
//
///////////////////////////////////////////////////////////////////////////////

 int PerformImageRegistration_MSSD (GImage& s_image,GImage& r_image,Matrix& d_mat_H)

{
   d_mat_H = IM(3);
   HTM_NCC1 htm;
   FPoint2D p1(0.75f,0.75f);
   FPoint2D p2(0.75f,0.75f);
   htm.Initialize (r_image,s_image,100,p1,p2);
   htm.Perform (2.0f,5,2);
   FPoint2D shift = htm.GetMatchingPosition (   );
   d_mat_H[0][2] = shift.X;
   d_mat_H[1][2] = shift.Y;
   Matrix mat_H = d_mat_H;
   HLIR_MSSD_2DProjective hlir;
   hlir.Initialize (s_image,r_image,mat_H,150);
   hlir.Perform (2.5f,2,4);
   hlir.GetHomography (mat_H);
   if (CheckHomography (mat_H)) return (1);
   d_mat_H = mat_H;
   return (DONE);
}

 float PerformTemplateMatching_NCC1 (GImage &s_image,GImage &t_image,IPoint2D &min_pos,IPoint2D &max_pos,IPoint2D &d_pos)

{
   int xs,ys,xt,yt;
   int x0,y0,x1,y1;
   int sxs,exs,sys,eys;
   int sxt,ext,syt,eyt;
   int ps_s,ps_ss,ps_t,ps_tt;
   int ts_s    = 0,ts_t    = 0;
   int old_sxs = 0,old_exs = 0;
   int old_sxt = 0,old_ext = 0;
   double ts_ss = 0.0,ts_tt = 0.0;

   double max_score = 0.0;
   for (y0 = min_pos.Y; y0 <= max_pos.Y; y0++) {
      y1 = y0 + t_image.Height;
      if (y0 < 0) sys = 0;
      else sys = y0;
      if (y1 < s_image.Height) eys = y1;
      else eys = s_image.Height;
      syt = sys - y0; 
      eyt = eys - y0;
      for (x0 = min_pos.X; x0 <= max_pos.X; x0++) {
         x1 = x0 + t_image.Width;
         if (x0 < 0) sxs = 0;
         else sxs = x0;
         if (x1 < s_image.Width) exs = x1;
         else exs = s_image.Width;
         if (x0 == min_pos.X) {
            ts_s = ts_t = 0;
            ts_ss = ts_tt = 0.0;
            for (ys = sys; ys < eys; ys++) {
               yt = ys - y0;
               for (xs = sxs; xs < exs; xs++) {
                  xt = xs - x0;
                  ts_s  += s_image[ys][xs];
                  ts_t  += t_image[yt][xt];
                  ts_ss += (int)s_image[ys][xs] * s_image[ys][xs];
                  ts_tt += (int)t_image[yt][xt] * t_image[yt][xt];
               }
            }
         }
         else {
            if (sxs != old_sxs) {
               ps_s = ps_ss = 0;
               for (ys = sys; ys < eys; ys++) {
                  ps_s  += s_image[ys][old_sxs];
                  ps_ss += (int)s_image[ys][old_sxs] * s_image[ys][old_sxs];
               }
               ts_s  -= ps_s;
               ts_ss -= ps_ss;
            }
            if (exs != old_exs) {
               xs = exs - 1;
               ps_s = ps_ss = 0;
               for (ys = sys; ys < eys; ys++) {
                  ps_s  += s_image[ys][xs];
                  ps_ss += (int)s_image[ys][xs] * s_image[ys][xs];
               }
               ts_s  += ps_s;
               ts_ss += ps_ss;
            }
            sxt = sxs - x0;
            ext = exs - x0;
            if (sxt != old_sxt) {
               ps_t = ps_tt = 0;
               for (yt = syt; yt < eyt; yt++) {
                  ps_t  += t_image[yt][sxt];
                  ps_tt += (int)t_image[yt][sxt] * t_image[yt][sxt];
               }
               ts_t  += ps_t;
               ts_tt += ps_tt;
            }
            if (ext != old_ext) {
               xt = old_ext - 1;
               ps_t = ps_tt = 0;
               for (yt = syt; yt < eyt; yt++) {
                  ps_t  += t_image[yt][xt];
                  ps_tt += (int)t_image[yt][xt] * t_image[yt][xt];
               }
               ts_t  -= ps_t;
               ts_tt -= ps_tt;
            }
         }
         old_sxs = sxs;
         old_exs = exs;
         old_sxt = sxs - x0;
         old_ext = exs - x0;
         uint   nps   = 0;
         double ts_st = 0.0;
         for (ys = sys; ys < eys; ys++) {
            yt = ys - y0;
            for (xs = sxs; xs < exs; xs++) {
               ts_st += (int)s_image[ys][xs] * t_image[yt][xs - x0];
               nps++;
            }
         }
         if (nps) {
            double m_s  = (double)ts_s / nps;
            double m_t  = (double)ts_t / nps;
            double m_ss = ts_ss / nps;
            double m_tt = ts_tt / nps;
            double m_st = ts_st / nps;
            double score  = (m_st - m_s * m_t) / (sqrt(m_ss - m_s * m_s) * sqrt(m_tt - m_t * m_t));
            if (score > max_score) {
               max_score = score;
               d_pos(x0,y0);
            }
         }
      }
   }
   return ((float)max_score);
}

 float PerformTemplateMatching_NCC1 (GImage &s_image,GImage &t_image,FPoint2D &lt_overlap,FPoint2D &rb_overlap,IPoint2D &d_pos)

{
   IPoint2D min_pos,max_pos;
   min_pos.X = (int)(t_image.Width  * lt_overlap.X) - t_image.Width;
   min_pos.Y = (int)(t_image.Height * lt_overlap.Y) - t_image.Height;
   max_pos.X = s_image.Width  - (int)(t_image.Width  * rb_overlap.X);
   max_pos.Y = s_image.Height - (int)(t_image.Height * rb_overlap.Y);
   return (PerformTemplateMatching_NCC1 (s_image,t_image,min_pos,max_pos,d_pos));
}

 float PerformTemplateMatching_NCC2 (GImage &s_image,GImage &t_image,IPoint2D &min_pos,IPoint2D &max_pos,IPoint2D &d_pos)

{
   int xs,ys,xt,yt;
   int x0,y0,x1,y1;
   int sxs,exs,sys,eys;

   double max_score = 0.0;
   for (y0 = min_pos.Y; y0 <= max_pos.Y; y0++) {
      y1 = y0 + t_image.Height;
      if (y0 < 0) sys = 0;
      else sys = y0;
      if (y1 < s_image.Height) eys = y1;
      else eys = s_image.Height;
      for (x0 = min_pos.X; x0 <= max_pos.X; x0++) {
         x1 = x0 + t_image.Width;
         if (x0 < 0) sxs = 0;
         else sxs = x0;
         if (x1 < s_image.Width) exs = x1;
         else exs = s_image.Width;
         uint   nps = 0, sum_s  = 0, sum_t = 0;
         double sum_st = 0.0, sum_ss = 0.0, sum_tt = 0.0;
         for (ys = sys; ys < eys; ys++) {
            yt = ys - y0;
            for (xs = sxs; xs < exs; xs++) {
               xt = xs - x0;
               if (s_image[ys][xs] && t_image[yt][xt]) {
                  sum_s  += s_image[ys][xs];
                  sum_t  += t_image[yt][xt];
                  sum_ss += (int)s_image[ys][xs] * s_image[ys][xs];
                  sum_tt += (int)t_image[yt][xt] * t_image[yt][xt];
                  sum_st += (int)s_image[ys][xs] * t_image[yt][xt];
                  nps++;
               }
            }
         }
         if (nps) {
            double m_s  = (double)sum_s / nps;
            double m_t  = (double)sum_t / nps;
            double m_ss = sum_ss / nps;
            double m_tt = sum_tt / nps;
            double m_st = sum_st / nps;
            double score  = (m_st - m_s * m_t) / (sqrt(m_ss - m_s * m_s) * sqrt(m_tt - m_t * m_t));
            if (score > max_score) {
               max_score = score;
               d_pos(x0,y0);
            }
         }
      }
   }
   return ((float)max_score);
}

 float PerformTemplateMatching_NCC2 (GImage &s_image,GImage &t_image,FPoint2D &lt_overlap,FPoint2D &rb_overlap,IPoint2D &d_pos)

{
   IPoint2D min_pos,max_pos;
   min_pos.X = (int)(t_image.Width  * lt_overlap.X) - t_image.Width;
   min_pos.Y = (int)(t_image.Height * lt_overlap.Y) - t_image.Height;
   max_pos.X = s_image.Width  - (int)(t_image.Width  * rb_overlap.X);
   max_pos.Y = s_image.Height - (int)(t_image.Height * rb_overlap.Y);
   return (PerformTemplateMatching_NCC2 (s_image,t_image,min_pos,max_pos,d_pos));
}

 void PrepareImageForLIR (GImage &s_image)

{
   int x,y;

   for (y = 0; y < s_image.Height; y++) {
      byte *l_s_image = s_image[y];
      for (x = 0; x < s_image.Width; x++)
         if (!l_s_image[x]) l_s_image[x] = 1;
   }
}

}

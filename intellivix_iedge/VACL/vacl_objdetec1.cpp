#include "vacl_foregnd.h"
#include "vacl_objdetec.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectDetection_DNN
//
///////////////////////////////////////////////////////////////////////////////

 ObjectDetection_DNN::ObjectDetection_DNN (   )

{
   Flag_Initialized = FALSE;
   ObjTypeFilter    = OBJ_TYPE_HUMAN|OBJ_TYPE_VEHICLE;
}

 ObjectDetection_DNN::~ObjectDetection_DNN (   )

{
   Close (   );
}

 void ObjectDetection_DNN::Close (   )

{
   Flag_Initialized = FALSE;
}

 void ObjectDetection_DNN::FindCorrespondingObject (OBBArray1D& s_array,int64 obj_type,IBox2D& s_rgn,float min_tar,float max_tar,float est_tar,int& max_i,float& max_s)

{
   int i;
   
   max_i = -1;
   max_s = 0.0f;
   int   s_area = s_rgn.Width * s_rgn.Height;
   float min_ta = min_tar * s_area;
   float max_ta = max_tar * s_area;
   float est_ta = est_tar * s_area;
   for (i = 0; i < s_array.Length; i++) {
      ObjBndBox& obb = s_array[i];
      if (obb.ObjType == obj_type) {
         int o_area = GetOverlapArea (s_rgn,obb);
         if (o_area > 0) {
            float t_area = (float)(obb.Width * obb.Height);
            if (min_ta <= t_area && t_area <= max_ta) {
               float score;
               if (est_ta > 0.0f) score = o_area / est_ta;
               else score = GetMaximum ((float)o_area / (s_rgn.Width * s_rgn.Height),(float)o_area / (obb.Width * obb.Height));
               if (score > max_s) {
                  max_s = score;
                  max_i = i;
               }
            }
         }
      }
   }
}

 void ObjectDetection_DNN::GetHeadCandidateRegion (IBox2D& s_rgn,IBox2D& d_rgn)

{
   int size     = GetMinimum (s_rgn.Width,s_rgn.Height);
   FPoint2D cp  = s_rgn.GetCenterPosition (   );
   d_rgn.X      = (int)(cp.X - 0.5f * size);
   d_rgn.Y      = s_rgn.Y;
   d_rgn.Width  = size;
   d_rgn.Height = size;
}

 int ObjectDetection_DNN::Initialize (const char* dir_name)

{
   Close (   );
   return (DONE);
}

 int ObjectDetection_DNN::Perform (BGRImage& s_cimage,OBBArray1D& d_array,ISize2D* oi_size)

{
   int i,n;

   d_array.Delete (   );
   // 객체들을 검출한다.
   OBBArray1D s_array;
   if (DetectObjects (s_cimage,s_array)) return (1);
   if (!s_array) return (DONE);
   if (oi_size != NULL) {
      ISize2D si_size(s_cimage.Width,s_cimage.Height);
      ScaleObjBndBoxes (si_size,*oi_size,s_array);
   }
   // HEAD/FACE/STICK 객체의 경우, 그것과 대응하는 HUMAN 객체를 찾아 연결시킨다.
   OBBArray1D t_array(s_array.Length);
   for (i = n = 0; i < s_array.Length; i++) {
      // ObjTypeFilter에 설정된 타입의 객체들만 뽑아낸다.
      if (s_array[i].ObjType & ObjTypeFilter) {
         t_array[n] = s_array[i];
         ObjBndBox& obb = t_array[n++];
         if (obb.ObjType == OBJ_TYPE_HUMAN) {
            int max_j; float max_s; IBox2D s_rgn;
            GetHeadCandidateRegion  (obb,s_rgn);
            FindCorrespondingObject (s_array,OBJ_TYPE_HEAD,s_rgn,0.1f,1.0f,0.25f,max_j,max_s); // PARAMETERS
            if (max_j >= 0 && max_s >= 0.5f) { // PARAMETERS
               obb.SubRgn_Head(s_array[max_j]);
               obb.Properties |= OBJ_TYPE_HEAD;
               // 찾은 HEAD 객체에 속하는 FACE 객체를 찾는다.
               FindCorrespondingObject (s_array,OBJ_TYPE_FACE,obb.SubRgn_Head,0.25f,0.9f,0.0f,max_j,max_s); // PARAMETERS
               if (max_j >= 0 && max_s >= 0.7f) { // PARAMETERS
                  obb.SubRgn_Face(s_array[max_j]);
                  obb.Properties |= OBJ_TYPE_FACE;
               }
            }
            // obb에 속하는 STICK 객체를 찾는다.
            FPoint2D scale(1.4f,1.0f); // PARAMETERS
            s_rgn = obb.Inflate (scale);
            FindCorrespondingObject (s_array,OBJ_TYPE_STICK,s_rgn,0.0f,1.0f,0.0f,max_j,max_s); // PARAMETERS
            if (max_j >= 0 && max_s >= 0.5f) { // PARAMETERS
               obb.SubRgn_Stick (s_array[max_j]);
               obb.Properties |= OBJ_TYPE_STICK;
            }
         }
      }
   }
   if (n) d_array = t_array.Extract (0,n);
   return (DONE);
}

 void ObjectDetection_DNN::ScaleObjBndBoxes (ISize2D& si_size,ISize2D& oi_size,OBBArray1D& d_array)

{
   int i;

   FPoint2D scale;
   scale.X = (float)oi_size.Width  / si_size.Width;
   scale.Y = (float)oi_size.Height / si_size.Height;
   for (i = 0; i < d_array.Length; i++) {
      ObjBndBox& obb = d_array[i];
      obb.X      = (int)(scale.X * obb.X + 0.5f);
      obb.Y      = (int)(scale.Y * obb.Y + 0.5f);
      obb.Width  = (int)(scale.X * obb.Width  + 0.5f);
      obb.Height = (int)(scale.Y * obb.Height + 0.5f);
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectDetectionServer
//
///////////////////////////////////////////////////////////////////////////////

 ObjectDetectionServer::ObjectDetectionServer (   )

{
   Flag_Initialized = FALSE;
   Events.Create (2);
}

 ObjectDetectionServer::~ObjectDetectionServer (   )

{
   Close (   );
}

 int ObjectDetectionServer::AddRequest (ForegroundDetection* frg_detector)

{
   if (!IsThreadRunning (   )) return (1);
   Request* request = new Request;
   request->ForegroundDetector = frg_detector;
   CS_RequestQueue.Lock   (   );
   int e_code = RequestQueue.Add (request);
   CS_RequestQueue.Unlock (   );
   if (e_code) {
      delete request;
      return (2);
   }
   Events.SetEvent (1);
   return (DONE);
}

 void ObjectDetectionServer::Close (   )

{
   Stop (   );
   ScopedLock lock(CS_RequestQueue);
   Request* request = RequestQueue.First (   );
   while (request != NULL) {
      request->ForegroundDetector->Event_Processed.SetEvent (   );
      request = RequestQueue.Next (request);
   }
   RequestQueue.Delete (   );
   Flag_Initialized = FALSE;
}

 void ObjectDetectionServer::RemoveAllRequests (ForegroundDetection* frg_detector)

{
   ScopedLock lock(CS_RequestQueue);
   Request* c_request = RequestQueue.First (   );
   while (c_request != NULL) {
      Request* n_request = RequestQueue.Next (c_request);
      if (c_request->ForegroundDetector == frg_detector) {
         RequestQueue.Remove (c_request);
         delete c_request;
      }
      c_request = n_request;
   }
}

 int ObjectDetectionServer::Start (   )

{
   Events.ResetAllEvents (   );
   return (StartThread (   ));
}

 void ObjectDetectionServer::Stop (   )

{
   // 쓰레드 내 메인 루프를 종료하기 위한 이벤트를 발생시킨다.
   Events.SetEvent (0);
   // 쓰레드가 종료할 때까지 기다려야 한다.
   StopThread (   );
}

 void ObjectDetectionServer::ThreadProc (   )

{
   while (TRUE) {
      int evt_idx;
      int e_code = WaitForMultiEvents (Events,FALSE,evt_idx);
      if (e_code == DONE) {
         if (evt_idx == 0) break;
         while (TRUE) {
            CS_RequestQueue.Lock   (   );
            Request* request = RequestQueue.Retrieve (   );
            CS_RequestQueue.Unlock (   );
            if (request != NULL) {
               ProcessRequest (request->ForegroundDetector);
               request->ForegroundDetector->Event_Processed.SetEvent (   );
               delete request;
            }
            else break;
         }
      }
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectDetection_YOLO
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_DARKNET)

 ObjectDetection_YOLO::ObjectDetection_YOLO (   )

{
   ObjTypeFilter = OBJ_TYPE_HUMAN|OBJ_TYPE_VEHICLE|OBJ_TYPE_WHEELCHAIR;
}

 int ObjectDetection_YOLO::DetectObjects (BGRImage& s_cimage,OBBArray1D& d_array)

{
   int i;

   if (!DNN.IsInitialized (   )) return (1);
   DNN.Predict (s_cimage,d_array);
   ISize2D cr_size(s_cimage.Width,s_cimage.Height);
   for (i = 0; i < d_array.Length; i++) {
      ObjBndBox& obb = d_array[i];
      obb.ObjType    = CI2OT[obb.ClassIdx];
      obb.ClassName  = DNN.ClassNames[obb.ClassIdx];
      // 객체 타입이 Face인 경우 영역을 보정한다. (약간 넓게 만듦)
      if (obb.ObjType == OBJ_TYPE_FACE) {
         FPoint2D cp  = obb.GetCenterPosition (   );
         IBox2D s_rgn;
         s_rgn.X      = (int)(cp.X - 0.5f  * obb.Width);
         s_rgn.Y      = (int)(cp.Y - 0.45f * obb.Height);
         s_rgn.Width  = (int)(1.0f * obb.Width);
         s_rgn.Height = (int)(1.2f * obb.Height);
         s_rgn.Height = GetMaximum (s_rgn.Width,s_rgn.Height);
         CropRegion (cr_size,s_rgn);
         obb(s_rgn);
      }
   }
   return (DONE);
}

 void ObjectDetection_YOLO::Close (   )

{
   DNN.Close (   );
   ObjectDetection_DNN::Close (   );
}


 int ObjectDetection_YOLO::Initialize (void* dll_handle,const char* dir_name,int gpu_idx)

{
   const char cfg_file_name[] = "IVXODCDKN2.dat"; // yolo.cfg
   const char wgt_file_name[] = "IVXODWDKN2.dat"; // yolo.weights
   const char cls_file_name[] = "IVXODNDKN2.dat"; // yolo.names

   int i;

   if (ObjectDetection_DNN::Initialize (dir_name)) return (1);
   CI2OT.Delete (   );
   StringA cfg_path_name;
   cfg_path_name.Format ("%s/%s",dir_name,cfg_file_name);
   StringA wgt_path_name;
   wgt_path_name.Format ("%s/%s",dir_name,wgt_file_name);
   StringA cls_path_name;
   cls_path_name.Format ("%s/%s",dir_name,cls_file_name);
   int e_code = DNN.Initialize (dll_handle,cfg_path_name,wgt_path_name,cls_path_name,gpu_idx);
   if (e_code) return (e_code);
   CI2OT.Create (DNN.ClassNames.Length);
   // Class Index 값을 Obejct Type 값으로 변환하는 테이블을 만든다.
   for (i = 0; i < DNN.ClassNames.Length; i++) {
      CI2OT[i] = ConvertClsNameToObjType (DNN.ClassNames[i]);
   }
   Flag_Initialized = TRUE;
   return (DONE);
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectDetectionServer_YOLO
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_DARKNET)

 void ObjectDetectionServer_YOLO::Close (   )

{
   ObjectDetectionServer::Close (   );
   ObjectDetector.Close (   );
}

 int ObjectDetectionServer_YOLO::Initialize (void* dll_handle, const char* dir_name,int gpu_idx)

{
   Close (   );
   int e_code = ObjectDetector.Initialize (dll_handle,dir_name,gpu_idx);
   if (e_code == DONE) Flag_Initialized = TRUE;
   return (e_code);
}

 void ObjectDetectionServer_YOLO::ProcessRequest (ForegroundDetection* frg_detector)

{
   ObjectDetector.Perform (frg_detector->DNNInputImage,frg_detector->ObjBndBoxes,&frg_detector->SrcImgSize);
}

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

 int64 ConvertClsNameToObjType (const char* cls_name)

{
   struct _CN2OT {
      char  ClassName[64];
      int64 ObjectType;
   };
   static _CN2OT cn2ot[] = {
      { "person"    , OBJ_TYPE_HUMAN      },
      { "head"      , OBJ_TYPE_HEAD       },
      { "face"      , OBJ_TYPE_FACE       },
      { "wheelchair", OBJ_TYPE_WHEELCHAIR },
      { "stick"     , OBJ_TYPE_STICK      },
      { "bicycle"   , OBJ_TYPE_VEHICLE    },
      { "car"       , OBJ_TYPE_VEHICLE    },
      { "motorbike" , OBJ_TYPE_VEHICLE    },
      { "aeroplane" , OBJ_TYPE_VEHICLE    },
      { "bus"       , OBJ_TYPE_VEHICLE    },
      { "train"     , OBJ_TYPE_VEHICLE    },
      { "truck"     , OBJ_TYPE_VEHICLE    },
      { "boat"      , OBJ_TYPE_VEHICLE    },
      { "bird"      , OBJ_TYPE_ANIMAL     },
      { "cat"       , OBJ_TYPE_ANIMAL     },
      { "dog"       , OBJ_TYPE_ANIMAL     },
      { "horse"     , OBJ_TYPE_ANIMAL     },
      { "sheep"     , OBJ_TYPE_ANIMAL     },
      { "cow"       , OBJ_TYPE_ANIMAL     },
      { "elephant"  , OBJ_TYPE_ANIMAL     },
      { "bear"      , OBJ_TYPE_ANIMAL     },
      { "zebra"     , OBJ_TYPE_ANIMAL     },
      { "giraffe"   , OBJ_TYPE_ANIMAL     }
   };

   int n = sizeof(cn2ot) / sizeof(_CN2OT);
   for (int i = 0; i < n; i ++) {
      if (!strcmp (cls_name,cn2ot[i].ClassName)) return (cn2ot[i].ObjectType);
   }
   return (OBJ_TYPE_UNKNOWN);
}

}

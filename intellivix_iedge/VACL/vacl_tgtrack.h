#if !defined(__VACL_TGTRACK_H) 
#define __VACL_TGTRACK_H

#include "vacl_histo.h"
#include "vacl_motion.h"

#if defined(__LIB_DLIB)
#include "dlib/image_processing.h"
#endif
 
 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Definitions
//
///////////////////////////////////////////////////////////////////////////////

#define TT_STATUS_NOT_INITIALIZED       0x00000001
#define TT_STATUS_TARGET_OUT_OF_RANGE   0x00000002
#define TT_STATUS_TARGET_LOST           0x00000004
#define TT_STATUS_TARGET_RECOVERED      0x00000008
#define TT_STATUS_EXACT_FR_MATCH        0x00000010
#define TT_STATUS_ROUGH_FR_MATCH        0x00000020
#define TT_STATUS_NO_FR_MATCH           0x00000040

#define TT_TYPE_PF_CH                   0x00000001
#define TT_TYPE_MS                      0x00000002
#define TT_TYPE_NCC                     0x00000003
#define TT_TYPE_MD_NCC                  0x00000004
#define TT_TYPE_DSST                    0x00000011

///////////////////////////////////////////////////////////////////////////////
//
// Class: TargetTracking
//
///////////////////////////////////////////////////////////////////////////////

 class TargetTracking
 
{
   protected:
      int Flag_Initialized;
      int Flag_SetTargetSize;

   // Input
   public:
      float    FrameRate;          // 입력 비디오 영상의 프레임 레이트
      float    FrameDuration;      // 1.0 / FrameRate
      float    MinColorSimilarity; // 동일 타겟이라고 판단하기 위한 색상 유사도의 최소값
      float    MinMatchingScore;   // 동일 타켓이라고 판단하기 위한 매칭 스코어의 최소값
      float    MinTargetLostTime;  // 타겟을 잃어버렸다고 판단하기 위한 최소 시간
      float    MinTargetOutRatio;  // 타겟이 영상 밖으로 나갔는지를 판단하기 위한 파라미터
      float    ModelUpdateRate;    // 모델 업데이트 레이트: Model(t+1) = (1 - ModelUpdateRate) * Model(t) +  ModelUpdateRate * Target(t)
      float    VelocityUpdateRate; // 속도 업데이트 레이트: V(t+1)     = (1 - VelocityUpdateRate) * V(t)  +  VelocityUpdateRate * (P(t+1) - P(t))
      ISize2D  SrcImgSize;         // 입력 영상의 크기

   // Output
   public:
      int      Status;
      int      FrameCount;
      float    MatchingScore;      // 매칭 위치에서 타겟 모델과 관측된 타겟 사이의 유사도
      float    TargetLostTime;     // 타겟을 잃어버린 후 경과 시간
      IBox2D   TargetRegion;       // 현재 타겟이 차지하는 영역
      FPoint2D TargetVelocity;     // 타겟의 이동 속도
      IPoint2D MatchingPos;        // 타겟의 매칭 위치
      
   protected:
      virtual void UpdateTargetRegion (   ) = 0;
   
   public:
      virtual int  GetTrackerType (   ) = 0;

   public:
      virtual void Close         (   );
      virtual void SetTargetSize (ISize2D& size);
   
   public:
      TargetTracking (   );
      virtual ~TargetTracking (   );
   
   protected:
      int CheckTargetOutOfRange (   );

   public:
      IPoint2D GetPredictedPosition (   );
      void     Initialize           (ISize2D& si_size,IBox2D& tg_rgn,float frm_rate);
      int      IsInitialized        (   );
};

 inline int TargetTracking::IsInitialized (   )

{
   return (Flag_Initialized);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: TargetSample
//
///////////////////////////////////////////////////////////////////////////////

 class TargetSample

{
   public:
      int   CX,CY;
      int   Width,Height;
      float Probability;
      
   public:
      void   Clear     (   );
      IBox2D GetRegion (   );
};

typedef Array1D<TargetSample> TSArray1D;

///////////////////////////////////////////////////////////////////////////////
//
// Class: TargetTracking_PF
//
///////////////////////////////////////////////////////////////////////////////
//
// PF: Particle Filter
//

 class TargetTracking_PF : public TargetTracking

{
   // Input
   public:
      int   NumSamples;         // 샘플 수를 설정하는 파라미터.
                                // 설정 가능 범위: 0 < NumSamples
                                // 본 값이 클 수록 추적 정확도는 증가하는 경향이 있으나 계산량(즉 CPU 부하)이 증가한다.

      float GNSD_Scale;         // Sample Propagation 시, 샘플의 스케일 값에 더해지는 가우시안 노이즈의 표준편차를 설정하는 파라미터.
                                // 설정 가능 범위: 0 <= GNSD_Scale
                                // 샘플의 스케일 값은 (1.0 + 가우시안 노이즈 값)으로 결정된다.
                                // 본 파라미터 값이 0이면 타겟 바운딩 박스의 크기는 더이상 변하지 않고 고정된다.
                                // 본 파리미터 값이 클수록 타겟의 크기 변화에 따른 타겟 바운딩 박스의 크기 적응이 빠르다는 장점이 있으나,
                                // 반면 타겟 바운딩 박스의 크기 변화가 심해지는 단점이 있다.

      float GNSD_Center;        // Sample Propagation 시, 샘플의 중심 좌표에 더해지는 가우시안 노이즈의 표준편차를 설정하는 파라미터.
                                // 설정 가능 범위: 0 < GNSD_Center
                                // [Real Standard Deviation Value of Gaussian Noise Added to Target Center] = GNSD_Center * 0.5 * ([Target Width] + [Target Height]).
                                // 본 파라미터 값이 클수록 샘플들이 타겟의 현재 중심을 기준으로 공간적으로 넓게 분포된다.
                                // 본 파라미터 값이 아무리 크더라도 샘플들이 분포되는 공간의 크기는 SamplingRange 값에 의해 제한된다.

      float SamplingRange;      // 샘플링 가능 영역을 정의하는 샘플링 윈도우의 가로/세로 길이를 설정하는 파라미터.
                                // 설정 가능 범위: 0 < SamplingRange
                                // [Real Sampling Window Width/Height Value] = SamplingRange * 0.5 * ([Target Width] + [Target Height]).
                                // 본 파라미터 값이 클수록 샘플들이 타겟의 현재 중심을 기준으로 공간적으로 넓게 분포된다.

   // Output
   public:
      TSArray1D    Samples;     // 샘플들
      TargetSample SampleMean;  // 샘플들의 평균값
   
   public:
      TargetTracking_PF (   );
   
   protected:
      virtual float PerformMatching       (TargetSample &smp_mean) = 0;
      virtual void  PerformPreprocessing  (   ) {   };
      virtual void  PerformPostprocessing (   ) {   };
      virtual void  UpdateTargetModel     (   ) = 0;
      virtual void  UpdateTargetRegion    (   );
   
   protected:
      TargetSample GetSampleMean        (   );
      void NormalizeSampleProbabilities (   );
      void PerformInitialSampling       (   );
      void PerformResampling            (   );
      void PropagateSamples             (   );
      
   public:
      virtual void Close         (   );
      virtual void SetTargetSize (ISize2D& size);
   
   public:
      float Perform (   );
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: TargetTracking_PF_CH
//
///////////////////////////////////////////////////////////////////////////////
//
// PF: Particle Filter
// CH: Color Histogram
//

 class TargetTracking_PF_CH : public TargetTracking_PF

{
   protected:
      BGRImage *SrcImage;
      FArray1D TargetHistogram;
      
   // Output
   public:
      FArray1D TargetModel;
      
   public:
      TargetTracking_PF_CH (   );
      
   protected:
      virtual float PerformMatching      (TargetSample &m_result);
      virtual void  PerformPreprocessing (   );
      virtual void  UpdateTargetModel    (   );

   protected:
      void  GetColorHistogram    (IBox2D &s_rgn,FArray1D &d_hist);
      float GetSampleProbability (float similarity);
   
   public:
      virtual void Close          (   );
      virtual int  GetTrackerType (   );
   
   public:
      void  Initialize (BGRImage &s_image,IBox2D &tg_rgn,float frm_rate);
      float Perform    (BGRImage &s_image);
};

 inline int TargetTracking_PF_CH::GetTrackerType (   )
 
{
   return (TT_TYPE_PF_CH);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: TargetTracking_MS
//
///////////////////////////////////////////////////////////////////////////////
//
// MS: MeanShift
//
 
 class TargetTracking_MS : public TargetTracking

{
   protected:
      FArray1D TargetHistogram;
      SArray1D PixelIndices;

   public: // Input Parameters
      int MaxNumIterations;
      
   public: // Output Parameters
      FArray1D TargetModel;

   public:
      TargetTracking_MS (   );
      
   protected:
      virtual void UpdateTargetRegion (   );
   
   protected:
      void     GetColorHistogram  (BGRImage &s_image,IBox2D &s_rgn,FArray1D &d_hist);
      FPoint2D GetMeanShiftVector (IBox2D &s_rgn,FArray1D &t_hist);
      float    PerformMatching    (BGRImage &s_image,IBox2D &s_rgn,FArray1D &t_hist,IPoint2D &m_pos);
      void     UpdateTargetModel  (   );

   public:
      virtual void  Close          (   );
      virtual int   GetTrackerType (   );
   
   public:
      void  Initialize (BGRImage &s_image,IBox2D &tg_rgn,float frm_rate);
      float Perform    (BGRImage &s_image);
};

 inline int TargetTracking_MS::GetTrackerType (   )
 
{
   return (TT_TYPE_MS);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: TargetTracking_NCC
//
///////////////////////////////////////////////////////////////////////////////

 class TargetTracking_NCC : public TargetTracking

{
   protected:
      ISize2D MatchingSize;

   public: // Input Parameters
      int   MatchingRange;
      float MSThreshold; // 현재 템플릿을 축소/확대한 템플릿 적용 여부 결정을 위한 파라미터
      float TSThreshold; // 타겟 크기에 따른 영상 축소 여부 결정을 위한 파라미터
      float ZoomStep;
      
   public: // Output Parameters
      GImage TargetModel;

   public:
      TargetTracking_NCC (   );

   protected:
      virtual void UpdateTargetRegion (   );
   
   protected:
      void  GetImagePatch     (GImage &s_image,IBox2D &s_rgn,GImage &d_image);
      float PerformMatching   (GImage &s_image,GImage &t_image,IPoint2D &b_pos,int m_range,IPoint2D &m_pos);
      void  UpdateTargetModel (GImage &s_image);

   public:
      virtual void Close          (   );
      virtual int  GetTrackerType (   );
      virtual void SetTargetSize  (ISize2D& size);

   public:
      void  Initialize (GImage& s_image,IBox2D& tg_rgn,float frm_rate);
      float Perform    (GImage& s_image);
};

 inline int TargetTracking_NCC::GetTrackerType (   )
 
{
   return (TT_TYPE_NCC);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: TargetTracking_MD_NCC
//
///////////////////////////////////////////////////////////////////////////////
//
// MD : Motion Detection
// NCC: Normalized Cross Correlation
//

 class TargetTracking_MD_NCC : public TargetTracking
 
{
   protected:
      MotionDetection_FAD MotionDetector;
   
   // Input
   public:
      int   MatchingRange;
      float MinTgtSizeSimilarity;
      
   // Output
   public:
      GImage FrgImage;
      GImage TargetMask;
      GImage TargetModel;
   
   public:
      TargetTracking_MD_NCC (   );
   
   protected:
      virtual void UpdateTargetRegion (   );

   protected:
      void  AdjustMatchingFrgRegion (IBox2D& m_rgn);
      int   GetMatchingFrgRegion    (IArray2D& fr_map,int n_frs,IBox2D& s_rgn,IBox2D& d_m_rgn,int& d_mr_no);
      float GetNCC                  (GImage& s_image,int ox,int oy,int dx,int dy);
      void  GetPreprocessedImage    (GImage& s_image,GImage& d_image);
      float GetSizeRatio            (IBox2D& s_rgn);
      int   GetTargetArea           (   );
      float PerformMatching         (GImage& s_image,IPoint2D& s_pos,int m_range,IPoint2D& m_pos);
      void  UpdateFrgImageAndRgnMap (IArray2D& fr_map,IBox2D& m_rgn,int mr_no);
      void  UpdateTargetModel       (GImage& s_image,IArray2D& fr_map,IBox2D& m_rgn,int mr_no);
      void  UpdateTargetModel       (GImage& s_image);
   
   public:
      virtual void Close          (   );
      virtual int  GetTrackerType (   );
      virtual void SetTargetSize  (ISize2D& size);

   public:
      void  Initialize (GImage& s_image,IBox2D &s_rgn,float frm_rate,GImage *m_image = NULL);
      float Perform    (GImage& s_image);
};

 inline int TargetTracking_MD_NCC::GetTrackerType (   )
 
{
   return (TT_TYPE_MD_NCC);
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: TargetTracking_DSST
//
///////////////////////////////////////////////////////////////////////////////
//
// DSST: Discriminative Scale Space Tracking
//

#if defined(__LIB_DLIB)

 class TargetTracking_DSST : public TargetTracking

{
   protected:
      dlib::correlation_tracker* TargetTracker;

   public:
      TargetTracking_DSST (   );
   
   protected:
      virtual void UpdateTargetRegion (   );
   
   public:
      virtual void Close          (   );
      virtual int  GetTrackerType (   );
   
   public:
      // [주의] s_image.Width는 4의 배수여야 한다.
      void  Initialize (GImage& s_image,IBox2D& tg_rgn,float frm_rate);
      float Perform    (GImage& s_image);
};

 inline int TargetTracking_DSST::GetTrackerType (   )

{
   return (TT_TYPE_DSST);
}

#endif

}

#endif

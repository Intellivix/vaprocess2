#if !defined(__VACL_OPTIMIZE_H)
#define __VACL_OPTIMIZE_H

#include "vacl_define.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: Minimization_LM
//
///////////////////////////////////////////////////////////////////////////////

 class Minimization_LM

{
   protected:
      int    NMsmVectors;
      int    MsmVectorSize;
      double Error,Lambda;
      Matrix Mat_A,Vec_b,Vec_p;

   public:
      int    NIterations;
      Matrix ParamVector;

   public:
      virtual ~Minimization_LM (   ) {   };

   protected:
      virtual void CostFunction (Matrix &vec_p,int n,Matrix &vec_m) = 0;

   protected:
      virtual void GetJacobianMatrix (Matrix &vec_p,Matrix &mat_J);
      virtual void PrepareForUpdate  (Matrix &vec_p) {   };

   public:
      void   Close      (   );
      void   Initialize (int n_m_vecs,int m_vec_size,int n_params);
      double Perform    (int n_iterations = -1,double d_thrlsd = 1E-7,double e_thrsld = -1.0);
      double Update     (   );
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: Minimization_Powell
//
///////////////////////////////////////////////////////////////////////////////

 class Minimization_Powell

{
   protected:
      Matrix pcom,xicom;

   public:
      Matrix ParamVector;

   public:
      virtual ~Minimization_Powell (   ) {   };

   protected:
      virtual double CostFunction (Matrix &vec_p) = 0;

   protected:
      double brent  (double ax,double bx,double cx,double tol,double &xmin);
      double f1dim  (double x);
      void   linmin (Matrix &p,Matrix &xi,double &fret);
      void   mnbrak (double &ax,double &bx,double &cx,double &fa,double &fb,double &fc);
      void   powell (Matrix &p,Matrix &xi,double ftol,int &iter,double &fret);
      void   shft3  (double &a,double &b,double &c,const double d);
      double SIGN   (const double a,const double b);
      double SQR    (const double a);
   
   public:
      void   Close      (   );
      void   Initialize (int n_params);
      double Perform    (double ftol = 1E-7);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: Minimizaiton_UniformTilt
//
///////////////////////////////////////////////////////////////////////////////

 class Minimizaiton_UniformTilt : public Minimization_LM

{
   protected:
      double          MeanY;
      Array1D<Matrix> Vecs_r;

   protected:
      virtual void CostFunction     (Matrix &vec_p,int n,Matrix &vec_m);
      virtual void PrepareForUpdate (Matrix &vec_p);
   
   protected:
      void GetInverseOfTransformationMatrix (Matrix &vec_p,Matrix &mat_IT);

   public:
      void Close                   (   );
      void Initialize              (Array1D<Matrix> &mats_R);
      void GetTransformationMatrix (Matrix &mat_T);
};

}

#endif


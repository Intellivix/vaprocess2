#include "vacl_event.h"
#include "vacl_filter.h"
#include "vacl_profile.h"
#include "vacl_water.h"

#if defined(__DEBUG_WLD)
#include "Win32CL/Win32CL.h"
extern int _DVX_WLD,_DVY_WLD;
extern CImageView* _DebugView_WLD;
#endif

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Definitions
//
///////////////////////////////////////////////////////////////////////////////

#define WLD_PARAM_MAX_GAP_SIZE            9
#define WLD_PARAM_SMOOTHING_FILTER_SIZE   6.0f
#define WLD_PARAM_THRESHOLD_FACTOR2       0.75f
#define WLD_PARAM_GAP_SIZE_FACTOR         0.7f

///////////////////////////////////////////////////////////////////////////////
//
// Class: WaterZone
//
///////////////////////////////////////////////////////////////////////////////

 WaterZone::WaterZone (   )

{
   Flag_Processed  = FALSE;
   DetTime         = 0.0f;
   EvtZone         = NULL;
   PrevLevel       = -1;
   MergeThrsld     = -1.0f;
   ThrsldLearnRate = 0.03f;
   WaterLearnRate  = 0.3f;
   WaterMean       = FPoint3D(-1.0f,-1.0f,-1.0f);
   DangerHeight    = 0;
   WaterRatio      = 0.0f;
}

 int WaterZone::DetectEvent (   )
 
{
   if (!Flag_Processed) return (0);
   if (!WaterLevel[0].X && !WaterLevel[1].X) return (0);
   if (EvtZone == NULL) return (0);
   EventRule* evt_rule = EvtZone->EvtRule;
   if (evt_rule == NULL) return (0);
   EventRuleConfig_WaterLevel& erc = evt_rule->ERC_WaterLevel;
   EventDetection* evt_detector = evt_rule->EventDetector;
   if (evt_detector == NULL) return (0);
   int n_objects = 0;
   int flag_detection = FALSE;
   float cl_y = erc.CrossingLine[0].Y / EvtZone->Scale.Y;
   if (cl_y < 0.0f) return (0);
   if (erc.Direction == 1) {
      if ((float)WaterLevel[0].Y >= cl_y) flag_detection = TRUE;
   }
   else {
      if ((float)WaterLevel[0].Y <= cl_y) flag_detection = TRUE;
   }
   if (flag_detection) DetTime += evt_detector->FrameDuration;
   else DetTime = 0.0f;
   Event* m_event = NULL;
   TrackedObject* eg_object = NULL;
   TrackedObject* t_object = evt_detector->EvtGenObjects.First (   );
   while (t_object != NULL) {
      Event* event = t_object->FindEvent (EvtZone);
      if (event != NULL) {
         m_event   = event;
         eg_object = t_object;
         break;
      }
      t_object = evt_detector->EvtGenObjects.Next (t_object);
   }
   if (m_event == NULL) {
      if (flag_detection) {
         if (DetTime >= erc.DetectionTime) {
            eg_object         = CreateInstance_TrackedObject (   );
            eg_object->ID     = evt_detector->GetNewObjectID (   );
            eg_object->Status = TO_STATUS_VALIDATED | TO_STATUS_VERIFIED;
            eg_object->Type    = TO_TYPE_WATER_LEVEL;
            m_event = CreateInstance_Event  (EvtZone,evt_rule);
            m_event->StartTime.GetLocalTime (   );
            eg_object->Events.Add (m_event);
            evt_detector->EvtGenObjects.Add (eg_object);
            n_objects = 1;
         }
      }
   }
   else if (m_event->EvtRule == evt_rule) {
      m_event->ElapsedTime += evt_detector->FrameDuration;
      switch (m_event->Status) {
         case EV_STATUS_BEGUN:
            m_event->Status = EV_STATUS_ONGOING;
            break;
         case EV_STATUS_ONGOING:
            if (!flag_detection) {
               m_event->BonusTime += evt_detector->FrameDuration;
               if (m_event->BonusTime >= 2.0f) { // PARAMETERS
                  m_event->Status = EV_STATUS_ENDED | EV_STATUS_TO_BE_REMOVED;
                  m_event->EndTime.GetLocalTime (   );
                  eg_object->Status |= TO_STATUS_TO_BE_REMOVED;
               }
            }
            else m_event->BonusTime = 0.0f;
            break;
         default:
            break;
      }
   }
   if (eg_object != NULL) {
      IBox2D s_rgn;
      s_rgn.Width  = WaterLevel[1].X - WaterLevel[0].X + 7;
      s_rgn.Height = 11;
      s_rgn.X      = WaterLevel[0].X - 3;
      s_rgn.Y      = WaterLevel[0].Y - s_rgn.Height / 2;
      eg_object->SetRegion (s_rgn);
   }
   evt_rule->ERC_Counting.Count += n_objects;
   return (n_objects);
}

 float WaterZone::GetDistance (FPoint3D& p1,FPoint3D& p2)
 
{
   // HSV cylindrical coordinate에서 Euclidean distance를 계산한다.
   int h,s,v;
   FPoint3D hsv1_vec,hsv2_vec;
   BGRPixel hsv1((byte)p1[2],(byte)p1[1],(byte)p1[0]);
   ConvertBGRToHSV (hsv1,h,s,v);
   hsv1_vec[0] = cos((float)h * MC_DEG2RAD) * (float)s;
   hsv1_vec[1] = sin((float)h * MC_DEG2RAD) * (float)s;
   hsv1_vec[2] = (float)v;
   BGRPixel hsv2((byte)p2[2],(byte)p2[1],(byte)p2[0]);
   ConvertBGRToHSV (hsv2,h,s,v);
   hsv2_vec[0] = cos((float)h * MC_DEG2RAD) * (float)s;
   hsv2_vec[1] = sin((float)h * MC_DEG2RAD) * (float)s;
   hsv2_vec[2] = (float)v;
   return Mag(hsv1_vec - hsv2_vec);
}

 void WaterZone::GetRGBMean (BGRImage& s_cimage,int sx,int sy,int width,int height,float& mean_r,float& mean_g,float& mean_b)
 
{
   if (width == 0 || height == 0 || sx < 0 || sy < 0) {
      mean_r = mean_g = mean_b = 0.0f;
      return;
   }

   int x,y;

   int ex = sx + width;
   int ey = sy + height;
   if (s_cimage.Width < ex) ex = s_cimage.Width;
   if (s_cimage.Height < ey) ey = s_cimage.Height;

   double size = (double)(width*height);
   int64 sum_r,sum_g,sum_b;
   sum_r = sum_g = sum_b = 0;
   for (y = sy; y < ey; y++) {
      BGRPixel* l_s_cimage = s_cimage[y];
      for (x = sx; x < ex; x++) {
         sum_r += l_s_cimage[x].R;
         sum_g += l_s_cimage[x].G;
         sum_b += l_s_cimage[x].B;
      }
   }
   mean_r = (float)((double)sum_r / size);
   mean_g = (float)((double)sum_g / size);
   mean_b = (float)((double)sum_b / size);
}

 void WaterZone::GetWaterLevel (GImage& s_image,BGRImage& s_cimage)

{
   Flag_Processed = FALSE;
   // ROI에 해당하는 이미지를 얻는다.
   GImage t_image1(ROI.Width,ROI.Height);
   t_image1.Copy  (s_image,ROI.X,ROI.Y,ROI.Width,ROI.Height,0,0);
   GImage t_image2(ROI.Width,ROI.Height);
   // FillGaps (t_image1,WLD_PARAM_MAX_GAP_SIZE,WLD_PARAM_MAX_GAP_SIZE,t_image2);
   t_image2 = t_image1;
   // 수평 투영 프로파일을 얻는다.
   FArray1D p_array1(ROI.Height);
   HorizontalProjection (t_image2,0,ROI.Width,p_array1,TRUE);
   FArray1D p_array2(ROI.Height);
   GaussianFiltering (p_array1,WLD_PARAM_SMOOTHING_FILTER_SIZE,p_array2);
   // 수평 투영 프로파일을 이진화하기 위한 임계치를 얻는다.
   float thrsld = EvtZone->EvtRule->ERC_WaterLevel.Threshold * GetMean (p_array2);
   #if defined(__DEBUG_WLD)
   _DebugView_WLD->DrawImage (t_image1,_DVX_WLD,_DVY_WLD);
   _DVX_WLD += t_image1.Width + 10;
   _DebugView_WLD->DrawImage (t_image2,_DVX_WLD,_DVY_WLD);
   _DVX_WLD += t_image2.Width + 10;
   DrawGraph (_DebugView_WLD,p_array1,_DVX_WLD,_DVY_WLD,100,PC_GREEN,TRUE,TRUE,thrsld);
   _DVX_WLD += 100;
   DrawGraph (_DebugView_WLD,p_array2,_DVX_WLD,_DVY_WLD,100,PC_GREEN,TRUE,TRUE,thrsld);
   _DVX_WLD += 100;
   #endif
   // 수평 투영 프로파일을 이진화한다.
   RPArray rp_array;
   if (GetRectPulses (p_array2,thrsld,rp_array)) {
      WaterLevel.Clear (   );
      return;
   }
   // 폭이 최대인 펄스(max_rp)를 찾는다.
   int max_width,max_i;
   FindMaxWidth (rp_array,max_width,max_i);
   RectPulse& max_rp = rp_array[max_i];
   // max_rp의 양 에지와 구간의 양 끝 간의 거리를 구하여,
   // 거리가 긴 쪽의 에지를 최종 수면 위치로 선택한다.
   int d1 = max_rp.StartPos;
   int d2 = ROI.Height - max_rp.EndPos;
   int py = 0;
   if (d1 > d2) py = max_rp.StartPos;
   else py = max_rp.EndPos;
   
   ///// 아래부터는 기존 level 정보로부터 추가한 알고리즘
   BGRImage t_cimage(ROI.Width,ROI.Height);
   t_cimage.Copy (s_cimage,ROI.X,ROI.Y,ROI.Width,ROI.Height,0,0);
   // 물 영역의 경계를 이전 level보다 일정 블럭 아래(수위가 감소할 수도 있으므로)로 설정한다.
   int block_size = 8;
   int water_bndry = PrevLevel < 0 ? py + block_size : PrevLevel + block_size;
   float mean_r,mean_g,mean_b;
   GetRGBMean (t_cimage,0,water_bndry,ROI.Width,ROI.Height-water_bndry,mean_r,mean_g,mean_b);
   FPoint3D water_mean(mean_r,mean_g,mean_b);

   if (WaterMean[0] < 0.0f) WaterMean = water_mean;
   else WaterMean = WaterMean * (1.0f - WaterLearnRate) + water_mean * WaterLearnRate;

   // 물이 아닌 영역의 mean을 계산하기 위해 boundary를 설정
   // 첫 분석 프레임에는 기존의 level까지 고려하고, 이후에는 이전 분석 프레임에서의 최종 level까지 고려
   int non_water_bndry = PrevLevel < 0 ? py : PrevLevel;
   FPoint3D non_water_mean(0.0f,0.0f,0.0f);
   if (MergeThrsld < 0.0f) {
      GetRGBMean (t_cimage,0,0,ROI.Width,non_water_bndry,mean_r,mean_g,mean_b);
      non_water_mean.X = mean_r;
      non_water_mean.Y = mean_g;
      non_water_mean.Z = mean_b;
   }
   else {
      int non_water_block_size = 8;
      int area_size = 0, bndry = 0;
      while (non_water_bndry > bndry) {
         int new_bndry = bndry + non_water_block_size;
         if (new_bndry > non_water_bndry) new_bndry = non_water_bndry;
         int block_height = new_bndry - bndry;
         GetRGBMean (t_cimage,0,bndry,ROI.Width,block_height,mean_r,mean_g,mean_b);
         FPoint3D non_water_block_mean(mean_r,mean_g,mean_b);
         bndry = new_bndry;
         if (GetDistance (WaterMean,non_water_block_mean) <= MergeThrsld)
            continue;
         int size = ROI.Width * block_height;
         area_size += size;
         non_water_mean += non_water_block_mean * (float)size;
      }
      if (area_size) non_water_mean /= (float)area_size;
   }

   // merge를 위한 threshold를 업데이트한다.
   float merge_thrsld = GetDistance (water_mean,non_water_mean) * 0.45f;
   if (MergeThrsld < 0.0f) MergeThrsld = merge_thrsld;
   else if (merge_thrsld >= MergeThrsld * 0.9f && merge_thrsld <= MergeThrsld * 1.5f)
      MergeThrsld = MergeThrsld * (1.0f - ThrsldLearnRate) + merge_thrsld * ThrsldLearnRate;

   // merge 가능한 블럭들을 모두 merge한다. 
   // water_bndry와 ROI의 바닥을 1:2로 내분하는 지점을 merge를 시작할 초기 level로 설정한다.
   py = (2 * water_bndry + (ROI.Height - 1)) / 3;
   while (block_size) {
      int level = py - block_size;
      if (level < 0) {
         block_size /= 2;
         continue;
      }
      GetRGBMean (t_cimage,0,level,ROI.Width,block_size,mean_r,mean_g,mean_b);
      FPoint3D block_mean(mean_r,mean_g,mean_b);
      if (GetDistance (WaterMean,block_mean) <= MergeThrsld) py = level;
      else block_size /= 2;
   }
   
   // level을 최종 업데이트한다.
   if (PrevLevel < 0) PrevLevel = py;
   py = (py + 9 * PrevLevel) / 10;
   PrevLevel = py;
   WaterRatio = 100.0f * (float)(ROI.Height - py) / DangerHeight;
   if (WaterRatio > 100.0f) WaterRatio = 100.0f;
   py += ROI.Y;

   // 검출된 수위를 기록한다.
   WaterLevel[0](ROI.X,py);
   WaterLevel[1](ROI.X + ROI.Width - 1,py);
   Flag_Processed = TRUE;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: WaterLevelDetection
//
///////////////////////////////////////////////////////////////////////////////

 WaterLevelDetection::WaterLevelDetection (   )

{
   Flag_Enabled = TRUE;
   FrameRate    = 0.0f;
}

 WaterLevelDetection::~WaterLevelDetection (   )

{
   Close (   );
}

 void WaterLevelDetection::Close (   )

{
   WaterZones.Delete (   );
}

 int WaterLevelDetection::Initialize (EventDetection& evt_detector)

{
   Close (   );
   if (!IsEnabled (   )) return (1);
   SrcImgSize = evt_detector.SrcImgSize;
   FrameRate  = evt_detector.FrameRate;
   EventZoneList& ez_list = evt_detector.EventZones;
   EventZone* evt_zone = ez_list.First (   );
   while (evt_zone != NULL) {
      if (evt_zone->EvtRule->EventType == ER_EVENT_WATER_LEVEL) {
         // 수위 감지를 수행할 ROI를 찾는다.
         IPoint2D min_v,max_v,min_x,max_x;
         FindMinMax (evt_zone->Vertices,min_v,max_v,min_x,max_x);
         float sx = (float)SrcImgSize.Width  / evt_zone->RefImgSize.Width;
         float sy = (float)SrcImgSize.Height / evt_zone->RefImgSize.Height;
         int x1 = (int)(sx * min_v.X + 0.5f);
         int y1 = (int)(sy * min_v.Y + 0.5f);
         int x2 = (int)(sx * max_v.X + 0.5f);
         int y2 = (int)(sy * max_v.Y + 0.5f);
         if (x1 < 0) x1 = 0;
         if (y1 < 0) y1 = 0;
         if (x2 >= SrcImgSize.Width ) x2 = SrcImgSize.Width  - 1;
         if (y2 >= SrcImgSize.Height) y2 = SrcImgSize.Height - 1;
         // WaterZone을 생성한다.
         WaterZone* zone = new WaterZone;
         zone->ROI(x1,y1,x2 - x1 + 1,y2 - y1 + 1);
         zone->EvtZone = evt_zone;
         zone->DangerHeight = zone->ROI.Height - (int)(sy * zone->EvtZone->EvtRule->ERC_WaterLevel.CrossingLine[0].Y + 0.5f);
         if (zone->DangerHeight > zone->ROI.Height) zone->DangerHeight = zone->ROI.Height;
         if (zone->DangerHeight < 1) zone->DangerHeight = 1;
         WaterZones.Add (zone);
      }
      evt_zone = ez_list.Next (evt_zone);
   }
   if (!WaterZones.GetNumItems (   )) return (2);
   return (DONE);
}

 int WaterLevelDetection::Perform (GImage& s_image,BGRImage& s_cimage)

{
   if (!IsEnabled (   )) return (1);
   if (!WaterZones.GetNumItems (   )) return (2);
   #if defined(__DEBUG_WLD)
   _DVX_WLD = _DVY_WLD = 0;
   _DebugView_WLD->Clear (   );
   #endif
   WaterZone* zone = WaterZones.First (   );
   while (zone != NULL) {
      zone->GetWaterLevel (s_image,s_cimage);
      zone = WaterZones.Next (zone);
   }
   #if defined(__DEBUG_WLD)
   _DebugView_WLD->Invalidate (   );
   #endif
   return (DONE);
}

}

﻿#include "vacl_event.h"
#include "vacl_foregnd3.h"
#include "vacl_objfilter.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjectFiltering
//
///////////////////////////////////////////////////////////////////////////////

 ObjectFiltering::ObjectFiltering (   )
 
{
   Flag_Enabled = TRUE;
   Options = OBF_OPTION_ENABLE                 |
             OBF_OPTION_USE_WIDTH              |
             OBF_OPTION_USE_HEIGHT             |
             OBF_OPTION_USE_AREA               |
             OBF_OPTION_USE_ASPECT_RATIO       |
             OBF_OPTION_USE_AXIS_LENGTH_RATIO  |
             OBF_OPTION_USE_MAJOR_AXIS_LENGTH  |
             OBF_OPTION_USE_MINOR_AXIS_LENGTH  |
             OBF_OPTION_USE_NORMALIZED_SPEED   |
             OBF_OPTION_USE_SPEED;

   MaxOverlap             = 1.0f;
   MaxTime_Tracked        = MC_VLV;
   MinWidth               = 0;
   MaxWidth               = 10000;
   MinHeight              = 0;
   MaxHeight              = 10000;
   MinArea                = 0.0f;
   MaxArea                = 100000000.0f;
   MinAspectRatio         = 0.1f;
   MaxAspectRatio         = 10.0f;
   MinAxisLengthRatio     = 1.0f;
   MaxAxisLengthRatio     = 10.0f;
   MinMajorAxisLength     = 0.0f;
   MaxMajorAxisLength     = 10000.0f;
   MinMinorAxisLength     = 0.0f;
   MaxMinorAxisLength     = 10000.0f;
   MinNorSpeed            = 0.0f;
   MaxNorSpeed            = 10000000.0f;
   MinSpeed               = 0.0f;
   MaxSpeed               = 10000.0f;
   MinRealArea            = 0.0f;
   MaxRealArea            = 100000000.0f;
   MinRealDistance        = 0.0f;
   MaxRealDistance        = 10000.0f;
   MinRealWidth           = 0.0f;
   MaxRealWidth           = 10000.0f;
   MinRealHeight          = 0.0f;
   MaxRealHeight          = 10000.0f;
   MinRealMajorAxisLength = 0.0f;
   MaxRealMajorAxisLength = 10000.0f;
   MinRealMinorAxisLength = 0.0f;
   MaxRealMinorAxisLength = 10000.0f;
   MinRealSpeed           = 0.0f;
   MaxRealSpeed           = 10000.0f;
   ResetCDS (   );
   TgtColor(0,0,255);

   #if defined(__CPNI_CERT_SZM)
   MaxTime_Tracked        = 4.0f;
   MinArea                = 300.0f;
   MaxArea                = 35000.0f;
   #endif
   
   #if defined(__KISA_CERT_OLD)
   MinAspectRatio         = 0.4f;
   MaxAspectRatio         = 7.5f;
   #endif
}

 int ObjectFiltering::CheckSetup (ObjectFiltering* of)

{
   int ret_flag = 0;
   FileIO buffThis,buffFrom;
   CXMLIO xmlIOThis,xmlIOFrom;
   xmlIOThis.SetFileIO (&buffThis,XMLIO_Write);
   xmlIOFrom.SetFileIO (&buffFrom,XMLIO_Write);
   WriteFile (&xmlIOThis);
   of->WriteFile (&xmlIOFrom);
   if (buffThis.IsDiff (buffFrom))
      ret_flag |= CHECK_SETUP_RESULT_CHANGED;
   buffThis.SetLength (0); buffFrom.SetLength (0);
   return (ret_flag);
}

 int ObjectFiltering::Filter (TrackedBlob* t_blob)
 
{
   if (!(Options & OBF_OPTION_ENABLE)) return (0);
   if (t_blob->Time_Tracked > MaxTime_Tracked) return (0);
   if (Options & OBF_OPTION_USE_WIDTH) {
      if (t_blob->Width < MinWidth || t_blob->Width > MaxWidth) return (1);
   }
   if (Options & OBF_OPTION_USE_HEIGHT) {
      if (t_blob->Height < MinHeight || t_blob->Height > MaxHeight) return (2);
   }
   if (Options & OBF_OPTION_USE_AREA) {
      int area = t_blob->Width * t_blob->Height;
      if (area < MinArea || area > MaxArea) return (3);
   }
   if (Options & OBF_OPTION_USE_ASPECT_RATIO) {
      if (t_blob->Width) {
         float ar = (float)t_blob->Height / t_blob->Width;
         if (ar < MinAspectRatio || ar > MaxAspectRatio) return (4);
      }
   }
   if (Options & OBF_OPTION_USE_TARGET_COLOR) {
      int rc_r = (int)t_blob->RgnFrgColor.R;
      int rc_g = (int)t_blob->RgnFrgColor.G;
      int rc_b = (int)t_blob->RgnFrgColor.B;
      int dr   = TgtColor.R - rc_r;
      int dg   = TgtColor.G - rc_g;
      int db   = TgtColor.B - rc_b;
      int d    = (int)sqrt ((float)(dr * dr + dg * dg + db * db));
      #if defined(__DEBUG_OBF)
      logd ("Blob ID: %d\n",t_blob->ID);
      logd ("   TgtColor = (%03d,%03d,%03d), RgnColor = (%03d,%03d,%03d), Distance = %d\n",TgtColor.R,TgtColor.G,TgtColor.B,rc_r,rc_g,rc_b,d);
      #endif
      if (d > CDS) return (5);
   }
   return (0);
}

 int ObjectFiltering::Filter (TrackedObject *t_object)
 
{
   if (!(Options & OBF_OPTION_ENABLE)) return (0);
   if (t_object->Time_Tracked > MaxTime_Tracked) return (0);
   if (Options & OBF_OPTION_USE_REAL_SIZE_VALUES) {
      if (t_object->RealHeight) {
         if (Options & OBF_OPTION_USE_WIDTH) {
            if (t_object->RealWidth < MinRealWidth || t_object->RealWidth > MaxRealWidth) return (1);
         }
         if (Options & OBF_OPTION_USE_HEIGHT) {
            if (t_object->RealHeight < MinRealHeight || t_object->RealHeight > MaxRealHeight) return (2);
         }
         if (Options & OBF_OPTION_USE_AREA) {
            float a = 1.0f;
            if (t_object->Status & TO_STATUS_IN_OUTER_ZONE) a *= 0.5f;
            float min_area = a * MinRealArea;
            if (t_object->RealArea < min_area || t_object->RealArea > MaxRealArea) return (3);
         }
         if (Options & OBF_OPTION_USE_MAJOR_AXIS_LENGTH) {
            if (t_object->RealMajorAxisLength < MinRealMajorAxisLength || t_object->RealMajorAxisLength > MaxRealMajorAxisLength) return (4);
         }
         if (Options & OBF_OPTION_USE_MINOR_AXIS_LENGTH) {
            if (t_object->RealMinorAxisLength < MinRealMinorAxisLength || t_object->RealMinorAxisLength > MaxRealMinorAxisLength) return (5);
         }
         if (Options & OBF_OPTION_USE_SPEED) {
            if (t_object->RealSpeed >= 0.0f) {
               if (t_object->RealSpeed < MinRealSpeed || t_object->RealSpeed > MaxRealSpeed) return (6);
            }
         }
      }
   }
   if (Options & OBF_OPTION_USE_WIDTH) {
      if (t_object->Width < MinWidth || t_object->Width > MaxWidth) return (7);
   }
   if (Options & OBF_OPTION_USE_HEIGHT) {
      if (t_object->Height < MinHeight || t_object->Height > MaxHeight) return (8);
   }
   if (Options & OBF_OPTION_USE_AREA) {
      float a = 1.0f;
      #if defined(__CPNI_CERT_SZM)
      if (t_object->Status & TO_STATUS_IN_OUTER_ZONE      ) a *= 0.5f;
      if (t_object->CenterPos.Y > 0.5f * SrcImgSize.Height) a *= 1.5f;
      #endif
      float min_area = a * MinArea;
      if (t_object->Area < min_area || t_object->Area > MaxArea) return (9);
   }
   if (Options & OBF_OPTION_USE_MAJOR_AXIS_LENGTH) {
      if (t_object->MajorAxisLength < MinMajorAxisLength || t_object->MajorAxisLength > MaxMajorAxisLength) return (10);
   }
   if (Options & OBF_OPTION_USE_MINOR_AXIS_LENGTH) {
      if (t_object->MinorAxisLength < MinMinorAxisLength || t_object->MinorAxisLength > MaxMinorAxisLength) return (11);
   }
   if (Options & OBF_OPTION_USE_SPEED) {
      if (t_object->Speed < MinSpeed || t_object->Speed > MaxSpeed) return (12);
   }
   if (Options & OBF_OPTION_USE_ASPECT_RATIO) {
      if (t_object->AspectRatio < MinAspectRatio || t_object->AspectRatio > MaxAspectRatio) return (13);
   }
   if (Options & OBF_OPTION_USE_AXIS_LENGTH_RATIO) {
      if (t_object->AxisLengthRatio < MinAxisLengthRatio || t_object->AxisLengthRatio > MaxAxisLengthRatio) return (14);
   }
   if (Options & OBF_OPTION_USE_NORMALIZED_SPEED) {
      if (t_object->NorSpeed < MinNorSpeed || t_object->NorSpeed > MaxNorSpeed) return (15);
   }
   if (Options & OBF_OPTION_USE_REAL_DISTANCE) {
      if (t_object->RealHeight) {
         if (t_object->RealDistance < MinRealDistance || t_object->RealDistance > MaxRealDistance) return (16);
      }
   }
   return (0);
}

 void ObjectFiltering::FilterObjects (ObjectTracking& obj_tracker)

{
   TrackedObject *t_object = obj_tracker.TrackedObjects.First (   );
   while (t_object != NULL) {
      if (!(t_object->Status & TO_STATUS_TO_BE_REMOVED)) {
         int r_code = Filter (t_object);
         if (r_code) {
            t_object->Status |= TO_STATUS_TO_BE_REMOVED;
         }
      }
      t_object = obj_tracker.TrackedObjects.Next (t_object);
   }
}

 void ObjectFiltering::InitParams (EventDetection& evt_detector)

{
   MaxOverlap = 1.0f;
   if (evt_detector.IsEventZoneAvailable (ER_EVENT_CAR_STOPPING)) MaxOverlap = 0.7f; // PARAMETERS
}

 int ObjectFiltering::Perform (ObjectTracking& obj_tracker)

{
   if (!IsEnabled (   )) return (1);
   SrcImgSize = obj_tracker.SrcImgSize;
   int fgd_cls_id = obj_tracker.ForegroundDetector->GetClassID (   );
   if (fgd_cls_id == CLSID_ForegroundDetection_HMD_OHV_MBS) {
      ForegroundDetection_HMD_OHV_MBS* fgd = (ForegroundDetection_HMD_OHV_MBS*)obj_tracker.ForegroundDetector;
      float bd_size = fgd->GetBodySize (   );
      MaxOverlap = 0.6f;                     // PARAMETERS
      MinArea    = 0.5f * bd_size * bd_size; // PARAMETERS
      MinWidth   = (int)(0.5f * bd_size);    // PARAMETERS
      MinHeight  = MinWidth;
   }
   FilterObjects   (obj_tracker);
   SuppressOverlap (obj_tracker);
   return (DONE);
}

 int ObjectFiltering::ReadFile (FileIO &file)
 
{
   if (file.Read ((byte*)&Options               ,1,sizeof(int  ))) return (1);
   if (file.Read ((byte*)&MinWidth              ,1,sizeof(int  ))) return (2);
   if (file.Read ((byte*)&MaxWidth              ,1,sizeof(int  ))) return (3);
   if (file.Read ((byte*)&MinHeight             ,1,sizeof(int  ))) return (4);
   if (file.Read ((byte*)&MaxHeight             ,1,sizeof(int  ))) return (5);
   if (file.Read ((byte*)&MinArea               ,1,sizeof(float))) return (6);
   if (file.Read ((byte*)&MaxArea               ,1,sizeof(float))) return (7);
   if (file.Read ((byte*)&MinAspectRatio        ,1,sizeof(float))) return (8);
   if (file.Read ((byte*)&MaxAspectRatio        ,1,sizeof(float))) return (9);
   if (file.Read ((byte*)&MinAxisLengthRatio    ,1,sizeof(float))) return (10);
   if (file.Read ((byte*)&MaxAxisLengthRatio    ,1,sizeof(float))) return (11);
   if (file.Read ((byte*)&MinMajorAxisLength    ,1,sizeof(float))) return (12);
   if (file.Read ((byte*)&MaxMajorAxisLength    ,1,sizeof(float))) return (13);
   if (file.Read ((byte*)&MinMinorAxisLength    ,1,sizeof(float))) return (14);
   if (file.Read ((byte*)&MaxMinorAxisLength    ,1,sizeof(float))) return (15);
   if (file.Read ((byte*)&MinNorSpeed           ,1,sizeof(float))) return (16);
   if (file.Read ((byte*)&MaxNorSpeed           ,1,sizeof(float))) return (17);
   if (file.Read ((byte*)&MinSpeed              ,1,sizeof(float))) return (18);
   if (file.Read ((byte*)&MaxSpeed              ,1,sizeof(float))) return (19);
   if (file.Read ((byte*)&MinRealArea           ,1,sizeof(float))) return (20);
   if (file.Read ((byte*)&MaxRealArea           ,1,sizeof(float))) return (21);
   if (file.Read ((byte*)&MinRealDistance       ,1,sizeof(float))) return (22);
   if (file.Read ((byte*)&MaxRealDistance       ,1,sizeof(float))) return (23);
   if (file.Read ((byte*)&MinRealWidth          ,1,sizeof(float))) return (24);
   if (file.Read ((byte*)&MaxRealWidth          ,1,sizeof(float))) return (25);
   if (file.Read ((byte*)&MinRealHeight         ,1,sizeof(float))) return (26);
   if (file.Read ((byte*)&MaxRealHeight         ,1,sizeof(float))) return (27);
   if (file.Read ((byte*)&MinRealMajorAxisLength,1,sizeof(float))) return (28);
   if (file.Read ((byte*)&MaxRealMajorAxisLength,1,sizeof(float))) return (29);
   if (file.Read ((byte*)&MinRealMinorAxisLength,1,sizeof(float))) return (30);
   if (file.Read ((byte*)&MaxRealMinorAxisLength,1,sizeof(float))) return (31);
   if (file.Read ((byte*)&MinRealSpeed          ,1,sizeof(float))) return (32);
   if (file.Read ((byte*)&MaxRealSpeed          ,1,sizeof(float))) return (33);
   if (file.Read ((byte*)&CDS                   ,1,sizeof(int  ))) return (34);
   if (file.Read ((byte*)&TgtColor              ,1,sizeof(BGRColor))) return (35);
   return (DONE);
}

 int ObjectFiltering::ReadFile (CXMLIO* pIO)

{
   static ObjectFiltering dv; // jun : defalut value (기본값과 같으면 xml로 부터 읽거나 저장하지 않음)

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("ObjectFiltering"))
   {
      xmlNode.Attribute (TYPE_ID(xint) ,"Options"               ,&Options               ,&dv.Options               );
      xmlNode.Attribute (TYPE_ID(float),"MinArea"               ,&MinArea               ,&dv.MinArea               );
      xmlNode.Attribute (TYPE_ID(float),"MaxArea"               ,&MaxArea               ,&dv.MaxArea               );
      xmlNode.Attribute (TYPE_ID(int)  ,"MinWidth"              ,&MinWidth              ,&dv.MinWidth              );
      xmlNode.Attribute (TYPE_ID(int)  ,"MaxWidth"              ,&MaxWidth              ,&dv.MaxWidth              );
      xmlNode.Attribute (TYPE_ID(int)  ,"MinHeight"             ,&MinHeight             ,&dv.MinHeight             );
      xmlNode.Attribute (TYPE_ID(int)  ,"MaxHeight"             ,&MaxHeight             ,&dv.MaxHeight             );
      xmlNode.Attribute (TYPE_ID(float),"MinAspectRatio"        ,&MinAspectRatio        ,&dv.MinAspectRatio        );
      xmlNode.Attribute (TYPE_ID(float),"MaxAspectRatio"        ,&MaxAspectRatio        ,&dv.MaxAspectRatio        );   
      xmlNode.Attribute (TYPE_ID(float),"MinAxisLengthRatio"    ,&MinAxisLengthRatio    ,&dv.MinAxisLengthRatio    );
      xmlNode.Attribute (TYPE_ID(float),"MaxAxisLengthRatio"    ,&MaxAxisLengthRatio    ,&dv.MaxAxisLengthRatio    );
      xmlNode.Attribute (TYPE_ID(float),"MinMajorAxisLength"    ,&MinMajorAxisLength    ,&dv.MinMajorAxisLength    );
      xmlNode.Attribute (TYPE_ID(float),"MaxMajorAxisLength"    ,&MaxMajorAxisLength    ,&dv.MaxMajorAxisLength    );
      xmlNode.Attribute (TYPE_ID(float),"MinMinorAxisLength"    ,&MinMinorAxisLength    ,&dv.MinMinorAxisLength    );
      xmlNode.Attribute (TYPE_ID(float),"MaxMinorAxisLength"    ,&MaxMinorAxisLength    ,&dv.MaxMinorAxisLength    );
      xmlNode.Attribute (TYPE_ID(float),"MinNorSpeed"           ,&MinNorSpeed           ,&dv.MinNorSpeed           );
      xmlNode.Attribute (TYPE_ID(float),"MaxNorSpeed"           ,&MaxNorSpeed           ,&dv.MaxNorSpeed           );
      xmlNode.Attribute (TYPE_ID(float),"MinSpeed"              ,&MinSpeed              ,&dv.MinSpeed              );
      xmlNode.Attribute (TYPE_ID(float),"MaxSpeed"              ,&MaxSpeed              ,&dv.MaxSpeed              );
      xmlNode.Attribute (TYPE_ID(float),"MinRealArea"           ,&MinRealArea           ,&dv.MinRealArea           );
      xmlNode.Attribute (TYPE_ID(float),"MaxRealArea"           ,&MaxRealArea           ,&dv.MaxRealArea           );
      xmlNode.Attribute (TYPE_ID(float),"MinRealDistance"       ,&MinRealDistance       ,&dv.MinRealDistance       );
      xmlNode.Attribute (TYPE_ID(float),"MaxRealDistance"       ,&MaxRealDistance       ,&dv.MaxRealDistance       );
      xmlNode.Attribute (TYPE_ID(float),"MinRealWidth"          ,&MinRealWidth          ,&dv.MinRealWidth          );
      xmlNode.Attribute (TYPE_ID(float),"MaxRealWidth"          ,&MaxRealWidth          ,&dv.MaxRealWidth          );
      xmlNode.Attribute (TYPE_ID(float),"MinRealHeight"         ,&MinRealHeight         ,&dv.MinRealHeight         );
      xmlNode.Attribute (TYPE_ID(float),"MaxRealHeight"         ,&MaxRealHeight         ,&dv.MaxRealHeight         );
      xmlNode.Attribute (TYPE_ID(float),"MinRealMajorAxisLength",&MinRealMajorAxisLength,&dv.MinRealMajorAxisLength);
      xmlNode.Attribute (TYPE_ID(float),"MaxRealMajorAxisLength",&MaxRealMajorAxisLength,&dv.MaxRealMajorAxisLength);
      xmlNode.Attribute (TYPE_ID(float),"MinRealMinorAxisLength",&MinRealMinorAxisLength,&dv.MinRealMinorAxisLength);
      xmlNode.Attribute (TYPE_ID(float),"MaxRealMinorAxisLength",&MaxRealMinorAxisLength,&dv.MaxRealMinorAxisLength);
      xmlNode.Attribute (TYPE_ID(float),"MinRealSpeed"          ,&MinRealSpeed          ,&dv.MinRealSpeed          );
      xmlNode.Attribute (TYPE_ID(float),"MaxRealSpeed"          ,&MaxRealSpeed          ,&dv.MaxRealSpeed          );
      xmlNode.Attribute (TYPE_ID(int)  ,"CDS"                   ,&CDS                   ,&dv.CDS                   );
      xmlNode.Attribute (TYPE_ID(byte) ,"TgtColor_B"            ,&TgtColor.B            ,&dv.TgtColor.B            );
      xmlNode.Attribute (TYPE_ID(byte) ,"TgtColor_G"            ,&TgtColor.G            ,&dv.TgtColor.G            );
      xmlNode.Attribute (TYPE_ID(byte) ,"TgtColor_R"            ,&TgtColor.R            ,&dv.TgtColor.R            );
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}

 void ObjectFiltering::ResetCDS (   )

{
   CDS = 70;
}

 void ObjectFiltering::SuppressOverlap (ObjectTracking& obj_tracker)

{
   int i,j;
   
   if (MaxOverlap >= 1.0f) return;
   TOArray1D to_array;
   obj_tracker.GetObjectArray (to_array);
   for (i = 1; i < to_array.Length; i++) {
      TrackedObject* t_object1 = to_array[i];
      if (!(t_object1->Status & TO_STATUS_TO_BE_REMOVED)) {
         int a1 = t_object1->Width * t_object1->Height;
         for (j = i + 1; j < to_array.Length; j++) {
            TrackedObject* t_object2 = to_array[j];
            int a2 = t_object2->Width * t_object2->Height;
            if (!(t_object2->Status & TO_STATUS_TO_BE_REMOVED)) {
               int   oa  = GetOverlapArea (*t_object1,*t_object2);
               float ov1 = (float)oa / a1;
               float ov2 = (float)oa / a2;
               float ov  = GetMaximum (ov1,ov2);
               if (ov > MaxOverlap) {
                  if (!(t_object1->Status & TO_STATUS_VERIFIED) && (t_object2->Status & TO_STATUS_VERIFIED)) {
                     t_object1->Status |= TO_STATUS_TO_BE_REMOVED;
                  }
                  else if ((t_object1->Status & TO_STATUS_VERIFIED) && !(t_object2->Status & TO_STATUS_VERIFIED)) {
                     t_object2->Status |= TO_STATUS_TO_BE_REMOVED;
                  }
                  else if (a1 < a2) {
                     t_object1->Status |= TO_STATUS_TO_BE_REMOVED;
                  }
                  else {
                     t_object2->Status |= TO_STATUS_TO_BE_REMOVED;
                  }
               }
            }
         }
      }
   }
}

 int ObjectFiltering::WriteFile (FileIO& file)
 
{
   if (file.Write ((byte*)&Options               ,1,sizeof(int  ))) return (1);
   if (file.Write ((byte*)&MinWidth              ,1,sizeof(int  ))) return (2);
   if (file.Write ((byte*)&MaxWidth              ,1,sizeof(int  ))) return (3);
   if (file.Write ((byte*)&MinHeight             ,1,sizeof(int  ))) return (4);
   if (file.Write ((byte*)&MaxHeight             ,1,sizeof(int  ))) return (5);
   if (file.Write ((byte*)&MinArea               ,1,sizeof(float))) return (6);
   if (file.Write ((byte*)&MaxArea               ,1,sizeof(float))) return (7);
   if (file.Write ((byte*)&MinAspectRatio        ,1,sizeof(float))) return (8);
   if (file.Write ((byte*)&MaxAspectRatio        ,1,sizeof(float))) return (9);
   if (file.Write ((byte*)&MinAxisLengthRatio    ,1,sizeof(float))) return (10);
   if (file.Write ((byte*)&MaxAxisLengthRatio    ,1,sizeof(float))) return (11);
   if (file.Write ((byte*)&MinMajorAxisLength    ,1,sizeof(float))) return (12);
   if (file.Write ((byte*)&MaxMajorAxisLength    ,1,sizeof(float))) return (13);
   if (file.Write ((byte*)&MinMinorAxisLength    ,1,sizeof(float))) return (14);
   if (file.Write ((byte*)&MaxMinorAxisLength    ,1,sizeof(float))) return (15);
   if (file.Write ((byte*)&MinNorSpeed           ,1,sizeof(float))) return (16);
   if (file.Write ((byte*)&MaxNorSpeed           ,1,sizeof(float))) return (17);
   if (file.Write ((byte*)&MinSpeed              ,1,sizeof(float))) return (18);
   if (file.Write ((byte*)&MaxSpeed              ,1,sizeof(float))) return (19);
   if (file.Write ((byte*)&MinRealArea           ,1,sizeof(float))) return (20);
   if (file.Write ((byte*)&MaxRealArea           ,1,sizeof(float))) return (21);
   if (file.Write ((byte*)&MinRealDistance       ,1,sizeof(float))) return (22);
   if (file.Write ((byte*)&MaxRealDistance       ,1,sizeof(float))) return (23);
   if (file.Write ((byte*)&MinRealWidth          ,1,sizeof(float))) return (24);
   if (file.Write ((byte*)&MaxRealWidth          ,1,sizeof(float))) return (25);
   if (file.Write ((byte*)&MinRealHeight         ,1,sizeof(float))) return (26);
   if (file.Write ((byte*)&MaxRealHeight         ,1,sizeof(float))) return (27);
   if (file.Write ((byte*)&MinRealMajorAxisLength,1,sizeof(float))) return (28);
   if (file.Write ((byte*)&MaxRealMajorAxisLength,1,sizeof(float))) return (29);
   if (file.Write ((byte*)&MinRealMinorAxisLength,1,sizeof(float))) return (30);
   if (file.Write ((byte*)&MaxRealMinorAxisLength,1,sizeof(float))) return (31);
   if (file.Write ((byte*)&MinRealSpeed          ,1,sizeof(float))) return (32);
   if (file.Write ((byte*)&MaxRealSpeed          ,1,sizeof(float))) return (33);
   if (file.Write ((byte*)&CDS                   ,1,sizeof(int  ))) return (34);
   if (file.Write ((byte*)&TgtColor              ,1,sizeof(BGRColor))) return (35);
   return (DONE);
}

 int ObjectFiltering::WriteFile (CXMLIO* pIO)

{
   static ObjectFiltering dv; // jun : defalut value (기본값과 같으면 xml로 부터 읽거나 저장하지 않음. 

   CXMLNode xmlNode (pIO);
   if (xmlNode.Start ("ObjectFiltering"))
   {
      xmlNode.Attribute (TYPE_ID(xint) ,"Options"               ,&Options               ,&dv.Options               );
      xmlNode.Attribute (TYPE_ID(float),"MinArea"               ,&MinArea               ,&dv.MinArea               );
      xmlNode.Attribute (TYPE_ID(float),"MaxArea"               ,&MaxArea               ,&dv.MaxArea               );
      xmlNode.Attribute (TYPE_ID(int)  ,"MinWidth"              ,&MinWidth              ,&dv.MinWidth              );
      xmlNode.Attribute (TYPE_ID(int)  ,"MaxWidth"              ,&MaxWidth              ,&dv.MaxWidth              );
      xmlNode.Attribute (TYPE_ID(int)  ,"MinHeight"             ,&MinHeight             ,&dv.MinHeight             );
      xmlNode.Attribute (TYPE_ID(int)  ,"MaxHeight"             ,&MaxHeight             ,&dv.MaxHeight             );
      xmlNode.Attribute (TYPE_ID(float),"MinAspectRatio"        ,&MinAspectRatio        ,&dv.MinAspectRatio        );
      xmlNode.Attribute (TYPE_ID(float),"MaxAspectRatio"        ,&MaxAspectRatio        ,&dv.MaxAspectRatio        );
      
      MinAspectRatio = 1.5f;
      MaxAspectRatio = 4.0f;
      
      xmlNode.Attribute (TYPE_ID(float),"MinAxisLengthRatio"    ,&MinAxisLengthRatio    ,&dv.MinAxisLengthRatio    );
      xmlNode.Attribute (TYPE_ID(float),"MaxAxisLengthRatio"    ,&MaxAxisLengthRatio    ,&dv.MaxAxisLengthRatio    );
      xmlNode.Attribute (TYPE_ID(float),"MinMajorAxisLength"    ,&MinMajorAxisLength    ,&dv.MinMajorAxisLength    );
      xmlNode.Attribute (TYPE_ID(float),"MaxMajorAxisLength"    ,&MaxMajorAxisLength    ,&dv.MaxMajorAxisLength    );
      xmlNode.Attribute (TYPE_ID(float),"MinMinorAxisLength"    ,&MinMinorAxisLength    ,&dv.MinMinorAxisLength    );
      xmlNode.Attribute (TYPE_ID(float),"MaxMinorAxisLength"    ,&MaxMinorAxisLength    ,&dv.MaxMinorAxisLength    );
      xmlNode.Attribute (TYPE_ID(float),"MinNorSpeed"           ,&MinNorSpeed           ,&dv.MinNorSpeed           );
      xmlNode.Attribute (TYPE_ID(float),"MaxNorSpeed"           ,&MaxNorSpeed           ,&dv.MaxNorSpeed           );
      xmlNode.Attribute (TYPE_ID(float),"MinSpeed"              ,&MinSpeed              ,&dv.MinSpeed              );
      xmlNode.Attribute (TYPE_ID(float),"MaxSpeed"              ,&MaxSpeed              ,&dv.MaxSpeed              );
      xmlNode.Attribute (TYPE_ID(float),"MinRealArea"           ,&MinRealArea           ,&dv.MinRealArea           );
      xmlNode.Attribute (TYPE_ID(float),"MaxRealArea"           ,&MaxRealArea           ,&dv.MaxRealArea           );
      xmlNode.Attribute (TYPE_ID(float),"MinRealDistance"       ,&MinRealDistance       ,&dv.MinRealDistance       );
      xmlNode.Attribute (TYPE_ID(float),"MaxRealDistance"       ,&MaxRealDistance       ,&dv.MaxRealDistance       );
      xmlNode.Attribute (TYPE_ID(float),"MinRealWidth"          ,&MinRealWidth          ,&dv.MinRealWidth          );
      xmlNode.Attribute (TYPE_ID(float),"MaxRealWidth"          ,&MaxRealWidth          ,&dv.MaxRealWidth          );
      xmlNode.Attribute (TYPE_ID(float),"MinRealHeight"         ,&MinRealHeight         ,&dv.MinRealHeight         );
      xmlNode.Attribute (TYPE_ID(float),"MaxRealHeight"         ,&MaxRealHeight         ,&dv.MaxRealHeight         );
      xmlNode.Attribute (TYPE_ID(float),"MinRealMajorAxisLength",&MinRealMajorAxisLength,&dv.MinRealMajorAxisLength);
      xmlNode.Attribute (TYPE_ID(float),"MaxRealMajorAxisLength",&MaxRealMajorAxisLength,&dv.MaxRealMajorAxisLength);
      xmlNode.Attribute (TYPE_ID(float),"MinRealMinorAxisLength",&MinRealMinorAxisLength,&dv.MinRealMinorAxisLength);
      xmlNode.Attribute (TYPE_ID(float),"MaxRealMinorAxisLength",&MaxRealMinorAxisLength,&dv.MaxRealMinorAxisLength);
      xmlNode.Attribute (TYPE_ID(float),"MinRealSpeed"          ,&MinRealSpeed          ,&dv.MinRealSpeed          );
      xmlNode.Attribute (TYPE_ID(float),"MaxRealSpeed"          ,&MaxRealSpeed          ,&dv.MaxRealSpeed          );
      xmlNode.Attribute (TYPE_ID(int)  ,"CDS"                   ,&CDS                   ,&dv.CDS                   );
      xmlNode.Attribute (TYPE_ID(byte) ,"TgtColor_B"            ,&TgtColor.B            ,&dv.TgtColor.B            );
      xmlNode.Attribute (TYPE_ID(byte) ,"TgtColor_G"            ,&TgtColor.G            ,&dv.TgtColor.G            );
      xmlNode.Attribute (TYPE_ID(byte) ,"TgtColor_R"            ,&TgtColor.R            ,&dv.TgtColor.R            );
      xmlNode.End (   );
      return (DONE);
   }
   return (1);
}

}

#if !defined(__VACL_VAENGINE_H)
#define __VACL_VAENGINE_H

#include "ivcp.h"
#include "vacl_objclass.h"
#include "vacl_color.h"
#include "vacl_dwell.h"
#include "vacl_event.h"
#include "vacl_fire.h"
#include "vacl_foregnd.h"
#include "vacl_foregnd2.h"
#include "vacl_foregnd3.h"
#include "vacl_grptrack.h"
#include "vacl_misc.h"
#include "vacl_objfilter.h"
#include "vacl_objtrack.h"
#include "vacl_realsize.h"
#include "vacl_traffic.h"
#include "VAEngine.h"

 namespace VACL
 
{

///////////////////////////////////////////////////////////////////////////////
//
// Class: VAEngine
//
///////////////////////////////////////////////////////////////////////////////

 class VAEngine
 
{
   protected:
      int    Flag_Initialized;
      int    FrameCount;
      Time   TimeStamp;
      FileIO VACfgDataBuffer;
      GImage BMUSImage;

   // 입력 파라미터
   public:
      int     ChannelID;  // 비디오 채널 ID
      int     Mode;       // 비디오 분석 모드 (VAE_MODE_XXX 정의문 참조)
      int     PVFPixFmt;  // 비디오 분석 프레임의 픽셀 포맷
      float   PVFRate;    // 비디오 분석 프레임 레이트
      ISize2D PVFSize;    // 비디오 분석 프레임의 해상도
      ISize2D RefImgSize; // 참조 이미지 해상도 (이벤트 존 또는 프로세싱 영역 설정 시의 기준 해상도)
   
   // 출력 파라미터
   public:
      GImage   SrcImage;      // 비디오 분석 용 입력 흑백 영상
      BGRImage SrcColorImage; // 비디오 분석 용 입력 컬러 영상
   
   // 기본 비디오 분석 모듈 (항상 동작)
   public:
      ObjectIDGeneration              ObjIDGenerator;
      ForegroundDetection_SGBM        ForegroundDetector_General;
      ForegroundDetection_HMD_OHV_MBS ForegroundDetector_Overhead;
      ObjectTracking                  ObjectTracker;
      RealObjectSizeEstimation        RealObjectSizeEstimator;
      ObjectClassification_Basic      ObjectClassifier;
      ObjectFiltering                 ObjectFilter;
      EventDetection                  EventDetector;

   // 확장 비디오 분석 모듈 (필요할 때만 동작)
   public:
      DwellAnalysis DwellAnalyzer;

   public:
      VAEngine (   );
      virtual ~VAEngine (   );
      
   protected:
      void WriteIVCPPacket        (CIVCPPacket* s_packet,FileIO& d_pkt_buf);
      void WriteIVCPPacket_Event  (TrackedObject* t_object,Event* event,FileIO& d_pkt_buf);
      void WriteIVCPPacket_Object (TrackedObject* t_object,FileIO& d_pkt_buf);
   
   protected:
      void WriteIVCPPacket_Events          (FileIO& d_pkt_buf);
      void WriteIVCPPacket_EventZoneCounts (FileIO& d_pkt_buf); 
      void WriteIVCPPacket_EventZones      (FileIO& d_pkt_buf); 
      void WriteIVCPPacket_Frame           (FileIO& d_pkt_buf);
      void WriteIVCPPacket_FrameEx         (FileIO& d_pkt_buf);
      void WriteIVCPPacket_Objects         (FileIO& d_pkt_buf);
   
   public:
      ForegroundDetection* GetForegroundDetector (   );
         // 현재 모드에 해당하는 ForegroundDetector를 리턴한다.

   public:
      virtual void Close (   );

   public:
      byte* GetConfiguration  (int& cfg_buf_len);
      int   Initialize        (int va_mode,int pvf_width,int pvf_height,float pvf_rate,int pvf_pix_fmt);
      int   IsInitialized     (   );
      int   ProcessVideoFrame (byte* s_pvf_buf,char* time_stamp = NULL,byte* d_pkt_buf = NULL,int d_buf_len = 0,int flag_finalize = FALSE,IBox2D* s_boxes = NULL,int n_boxes = 0);
      int   SetConfiguration  (byte* s_cfg_buf,int s_buf_len);
};

 inline int VAEngine::IsInitialized (   )
 
{
   return (Flag_Initialized);
}

}

#endif

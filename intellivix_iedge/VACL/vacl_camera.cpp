#include "vacl_camera.h"
#include "vacl_geometry.h"

 namespace VACL
 
{

////////////////////////////////////////////////////////////////////////////////
// 
// Functions
//
////////////////////////////////////////////////////////////////////////////////

 void GetCalibrationMatrix (double f_length,int si_width,int si_height,Matrix &mat_K)

{
   mat_K.Clear (   );
   mat_K[0][0] = mat_K[1][1] = f_length;
   mat_K[0][2] = 0.5 * si_width;
   mat_K[1][2] = 0.5 * si_height;
   mat_K[2][2] = 1.0;
}

 FPoint2D GetDistortedPixelPosition (FPoint2D &s_point,FPoint2D &c_point,double kappa)

{
   FPoint2D pd;
   if (kappa == 0.0) pd = s_point;
   else {
      FPoint2D pu = s_point - c_point;
      double sru = SquaredMag (pu);
      double a1  = kappa * kappa;
      double a2  = 1.0 / (27.0 * a1 * kappa) + sru / (2.0 * a1);
      double a3  = pow (sqrt (a2 * a2 - 1.0 / (729.0 * a1 * a1 * a1)) + a2,1.0 / 3.0);
      double srd = a3 + 1.0 / (9.0 * a3 * a1) - 2.0 / (3.0 * kappa);
      double a   = 1.0 + kappa * srd;
      pd.X = (float)(pu.X / a + c_point.X);
      pd.Y = (float)(pu.Y / a + c_point.Y);
   }
   return (pd);
}

////////////////////////////////////////////////////////////////////////////////
//
//  Panoramic Image Mosaics
//  Heung-Yeung Shum and Richard Szeliski
//  Technical Report, MSR-TR-97-23
//  Microsoft Research, 1997
//
////////////////////////////////////////////////////////////////////////////////

 double GetFocalLength (Matrix &mat_H,int si_width,int si_height)
 // [IN] mat_H    : Planar homography (3 x 3)
 //                 The homography describes the relationship between two images obtained by camera rotation.
 //                 It is assumed that the focal length of a camera is fixed during rotation.
 // [IN] si_width : Image width
 // [IN] si_height: Image height
{
   Matrix mat_K(3,3);
   GetCalibrationMatrix (1.0,si_width,si_height,mat_K);
   Matrix mat_IK(3,3);
   GetInverseOfCalibrationMatrix (1.0,si_width,si_height,mat_IK);
   Matrix mat_R = mat_IK * mat_H * mat_K;
   double a  = (mat_R[2][0] * mat_R[2][0] + mat_R[2][1] * mat_R[2][1]) * 2.0;
   double b  = (mat_R[2][2] * mat_R[2][2] - mat_R[0][0] * mat_R[0][0] - mat_R[0][1] * mat_R[0][1]);
   double c  = -mat_R[0][2] * mat_R[0][2];
   double f_length = 0.0;
   if (fabs (a) > MC_VSV) {
      double d = b * b - 2.0 * a * c;
      if (d >= 0.0) {
         d = sqrt (d);
         double f = (d - b) / a;
         if (f > 0.0) f_length = sqrt (f);
         else {
            f = -(b + d) / a;
            if (f > 0.0) f_length = sqrt (f);
         }
      }
   }
   return (f_length);
}

 void GetHomography_3DRotational (double f_length1,double f_length2,double psi,double theta,double phi,int si_width,int si_height,Matrix &mat_H)

{
   Matrix mat_IK1(3,3);
   GetInverseOfCalibrationMatrix (f_length1,si_width,si_height,mat_IK1);
   Matrix mat_K2(3,3);
   GetCalibrationMatrix (f_length2,si_width,si_height,mat_K2);
   Matrix mat_R(3,3);
   GetRotationMatrix_XYZ (psi,theta,phi,mat_R);
   mat_H = mat_K2 * mat_R * mat_IK1;
}

 void GetHomography_3DRotational (double f_length1,double f_length2,Matrix &mat_Rr,int si_width,int si_height,Matrix &mat_H)

{
   Matrix mat_IK1(3,3);
   GetInverseOfCalibrationMatrix (f_length1,si_width,si_height,mat_IK1);
   Matrix mat_K2(3,3);
   GetCalibrationMatrix (f_length2,si_width,si_height,mat_K2);
   mat_H = mat_K2 * mat_Rr * mat_IK1;
}   

 void GetInverseOfCalibrationMatrix (double f_length,int si_width,int si_height,Matrix &mat_IK)

{
   mat_IK.Clear (   );
   mat_IK[0][0] = mat_IK[1][1] = 1.0 / f_length;
   mat_IK[0][2] = -0.5 * si_width   / f_length;
   mat_IK[1][2] = -0.5 * si_height  / f_length;
   mat_IK[2][2] = 1.0;
}

 void GetRotationMatrix (Matrix &mat_H,double f_length1,double f_length2,int si_width,int si_height,Matrix &mat_R)
 // mat_H = mat_K2 * mat_R * Inv(mat_K1)
{
   Matrix mat_K1(3,3);
   GetCalibrationMatrix (f_length1,si_width,si_height,mat_K1);
   Matrix mat_IK2(3,3);
   GetInverseOfCalibrationMatrix (f_length2,si_width,si_height,mat_IK2);
   mat_R = mat_IK2 * mat_H * mat_K1;
   double m = sqrt (mat_R[0][0] * mat_R[0][0] + mat_R[0][1] * mat_R[0][1] + mat_R[0][2] * mat_R[0][2]);
   if (Det(mat_R) < 0.0) m = -m;
   mat_R /= m;
}

 void GetUndistortedImage (GImage &s_image,double kappa,int option,GImage &d_image)

{
   int i,j,ix1,iy1;
   int d_width,d_height;
   
   if (kappa == 0.0) {
      d_image = s_image;
      return;
   }
   DPoint2D cpd(0.5 * s_image.Width,0.5 * s_image.Height);
   switch (option) {
      case LDC_CIRCUMSCRIBED: {
         double srd = cpd.X * cpd.X + cpd.Y * cpd.Y;
         double a   = 1.0 + kappa * srd;
         d_width  = (int)(2.0 * cpd.X * a);
         d_height = (int)(2.0 * cpd.Y * a);
      }  break;
      case LDC_INSCRIBED: {
         double srd = cpd.X * cpd.X;
         double a   = 1.0 + kappa * srd;
         d_width  = (int)(2.0 * cpd.X * a);
         srd = cpd.Y * cpd.Y;
         a   = 1.0 + kappa * srd;
         d_height = (int)(2.0 * cpd.Y * a);
      }  break;
      default: {
         d_width  = s_image.Width;
         d_height = s_image.Height;
      }  break;
   }
   d_image.Create (d_width,d_height);
   DPoint2D cpu(0.5 * d_image.Width,0.5 * d_image.Height);
   double a1 = kappa * kappa;
   double a2 = 1.0 / (27.0 * kappa * a1);
   double a3 = 1.0 / (2.0 * a1);
   double a4 = 1.0 / (729.0 * a1 * a1 * a1);
   double a5 = 2.0 / (3.0 * kappa);
   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   for (i = 0; i < d_image.Height; i++) {
      double yu  = i - cpu.Y;
      double syu = yu * yu;
      for (j = 0; j < d_image.Width; j++) {
         double xu  = j - cpu.X;
         double sru = xu * xu + syu;
         double c1  = a2 + sru * a3;
         double c2  = pow (sqrt (c1 * c1 - a4) + c1,1.0 / 3.0);
         double srd = c2 + 1.0 / (9.0 * c2 * a1) - a5;
         double c3  = 1.0 + kappa * srd;
         float  xd  = (float)(xu / c3 + cpd.X);
         float  yd  = (float)(yu / c3 + cpd.Y);
         int    ix  = (int)xd;
         int    iy  = (int)yd;
         if (xd >= 0.0f && yd >= 0.0f && ix <= ex && iy <= ey) {
            if (ix == ex) ix1 = ix;
            else ix1  = ix + 1;
            if (iy == ey) iy1 = iy;
            else iy1  = iy + 1;
            float rx  = xd - ix;
            float ry  = yd - iy;
            float rx1 = 1.0f - rx;
            float ry1 = 1.0f - ry;
            float b1  = ry1 * s_image[iy][ix ] + ry * s_image[iy1][ix ];
            float b2  = ry1 * s_image[iy][ix1] + ry * s_image[iy1][ix1];
            d_image[i][j] = (byte)(rx1 * b1 + rx * b2);
         }
      }
   }
}

 void GetUndistortedImage (BGRImage &s_image,double kappa,int option,BGRImage &d_image)
 
{
   int i,j,ix1,iy1;
   int d_width,d_height;
   
   if (kappa == 0.0) {
      d_image = s_image;
      return;
   }
   DPoint2D cpd(0.5 * s_image.Width,0.5 * s_image.Height);
   switch (option) {
      case LDC_CIRCUMSCRIBED: {
         double srd = cpd.X * cpd.X + cpd.Y * cpd.Y;
         double a   = 1.0 + kappa * srd;
         d_width  = (int)(2.0 * cpd.X * a);
         d_height = (int)(2.0 * cpd.Y * a);
      }  break;
      case LDC_INSCRIBED: {
         double srd = cpd.X * cpd.X;
         double a   = 1.0 + kappa * srd;
         d_width  = (int)(2.0 * cpd.X * a);
         srd = cpd.Y * cpd.Y;
         a   = 1.0 + kappa * srd;
         d_height = (int)(2.0 * cpd.Y * a);
      }  break;
      default: {
         d_width  = s_image.Width;
         d_height = s_image.Height;
      }  break;
   }
   d_image.Create (d_width,d_height);
   DPoint2D cpu(0.5 * d_image.Width,0.5 * d_image.Height);
   double a1 = kappa * kappa;
   double a2 = 1.0 / (27.0 * kappa * a1);
   double a3 = 1.0 / (2.0 * a1);
   double a4 = 1.0 / (729.0 * a1 * a1 * a1);
   double a5 = 2.0 / (3.0 * kappa);
   int ex = s_image.Width  - 1;
   int ey = s_image.Height - 1;
   for (i = 0; i < d_image.Height; i++) {
      double yu  = i - cpu.Y;
      double syu = yu * yu;
      for (j = 0; j < d_image.Width; j++) {
         double xu  = j - cpu.X;
         double sru = xu * xu + syu;
         double c1  = a2 + sru * a3;
         double c2  = pow (sqrt (c1 * c1 - a4) + c1,1.0 / 3.0);
         double srd = c2 + 1.0 / (9.0 * c2 * a1) - a5;
         double c3  = 1.0 + kappa * srd;
         float  xd  = (float)(xu / c3 + cpd.X);
         float  yd  = (float)(yu / c3 + cpd.Y);
         int    ix  = (int)xd;
         int    iy  = (int)yd;
         if (xd >= 0.0f && yd >= 0.0f && ix <= ex && iy <= ey) {
            if (ix == ex) ix1 = ix;
            else ix1  = ix + 1;
            if (iy == ey) iy1 = iy;
            else iy1  = iy + 1;
            float rx  = xd - ix;
            float ry  = yd - iy;
            float rx1 = 1.0f - rx;
            float ry1 = 1.0f - ry;
            float b1  = ry1 * s_image[iy][ix ].R + ry * s_image[iy1][ix ].R;
            float b2  = ry1 * s_image[iy][ix1].R + ry * s_image[iy1][ix1].R;
            d_image[i][j].R = (byte)(rx1 * b1 + rx * b2);
                  b1  = ry1 * s_image[iy][ix ].G + ry * s_image[iy1][ix ].G;
                  b2  = ry1 * s_image[iy][ix1].G + ry * s_image[iy1][ix1].G;
            d_image[i][j].G = (byte)(rx1 * b1 + rx * b2);
                  b1  = ry1 * s_image[iy][ix ].B + ry * s_image[iy1][ix ].B;
                  b2  = ry1 * s_image[iy][ix1].B + ry * s_image[iy1][ix1].B;
            d_image[i][j].B = (byte)(rx1 * b1 + rx * b2);
         }
      }
   }
}

 FPoint2D GetUndistortedPixelPosition (FPoint2D &s_point,FPoint2D &c_point,double kappa)

{
   FPoint2D pu;
   if (kappa == 0.0) pu = s_point;
   else {
      FPoint2D pd = s_point - c_point;
      double a = (1.0 + kappa * SquaredMag (pd));
      pu.X = (float)(a * pd.X + c_point.X);
      pu.Y = (float)(a * pd.Y + c_point.Y);
   }
   return (pu);
}

 FPoint2D GetUndistortedPixelPosition (FPoint2D &s_point,FPoint2D &c_point,double kappa1,double kappa2)

{
   FPoint2D pu;
   if (kappa1 == 0.0 && kappa2 == 0.0) pu = s_point;
   else {
      FPoint2D pd = s_point - c_point;
      double r2 = SquaredMag (pd);
      double r4 = r2 * r2;
      double a = (1.0 + kappa1 * r2 + kappa2 * r4);
      pu.X = (float)(a * pd.X + c_point.X);
      pu.Y = (float)(a * pd.Y + c_point.Y);
   }
   return (pu);
}

}

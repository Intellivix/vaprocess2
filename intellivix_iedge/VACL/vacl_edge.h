#if !defined(__VACL_EDGE_H)
#define __VACL_EDGE_H

#include "vacl_conrgn.h"

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Forward Declaration
//
///////////////////////////////////////////////////////////////////////////////

class ForegroundDetection;

///////////////////////////////////////////////////////////////////////////////
//
// Class: BoundaryEdgeInfoExtraction
//
///////////////////////////////////////////////////////////////////////////////

 class BoundaryEdgeInfoExtraction : public BoundaryPixelTracking
 
{
   protected:
      ForegroundDetection* ForegroundDetector;
      
   public:
      int NumBkgEdgePixels;
      int NumFrgEdgePixels1;
      int NumFrgEdgePixels2;
      int NumFrgEdgePixels3;
   
   public:
      BoundaryEdgeInfoExtraction (   );
   
   public:
      virtual void ProcessBoundaryPixel (   );
      virtual void ProcessMarginalPixel (   ) {   };
      virtual int  StartTracking        (   );
      
   public:
      int Initialize (ForegroundDetection& frg_detector,IArray2D& cr_map);
};

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

void  DetectEdges_Sobel             (GImage &s_image,short es_thrsld,GImage &d_image);
void  GetGradientMagnitudes         (SArray2D &dx_array,SArray2D &dy_array,SArray2D &gm_array);
void  GetGradientMagnitudes_Sobel   (GImage &s_image,FArray2D &d_array);
void  GetGradientMagnitudes_Sobel   (GImage &s_image,SArray2D &d_array);
short GetGradientMagnitudeThreshold (SArray2D &s_array,short min_gm = 20,float a = 1.0f);
void  GetGradients                  (GImage &s_image,FArray2D &dx_array,FArray2D &dy_array);
void  GetGradients                  (GImage &s_image,SArray2D &dx_array,SArray2D &dy_array);
void  GetRegionInteriorEdgeInfo     (ForegroundDetection& frg_detector,IArray2D& cr_map,IBox2D& s_rgn,int s_no,int& n_feps,int& n_beps);

}

#endif

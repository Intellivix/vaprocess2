#include "vacl_boxtrack.h"

#if defined(__DEBUG_BXT)
#include "Win32CL/Win32CL.h"
extern int _DVX_BXT,_DVY_BXT;
extern CImageView* _DebugView_BXT;
#endif

#define __CONSOLE

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: ObjBndBox
//
///////////////////////////////////////////////////////////////////////////////

 ObjBndBox::ObjBndBox (   )

{
   ObjType      = OBJ_TYPE_UNKNOWN;
   ClassIdx     = -1;
   MatTrkBoxIdx = -1;
   Properties   = 0;
   Score        = 0.0f;
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: TrackedBox
//
///////////////////////////////////////////////////////////////////////////////

 TrackedBox::TrackedBox (   )

{
   ID                = 0;
   FaceID            = 0;
   Status            = 0;
   ClassIdx          = -1;
   MatDetBoxIdx      = -1;
   ObjType           = OBJ_TYPE_UNKNOWN;

   Count_Tracked     = 0;
   MovingDistance    = 0.0f;
   MaxMovingDistance = 0.0f;
   Time_Stick        = 0.0f;
   Time_Tracked      = 0.0f;
   Time_Undetected   = 0.0f;

   Velocity(MC_VLV,MC_VLV);
}

 void TrackedBox::Initialize (ObjBndBox& s_obb)

{
   ObjType  = s_obb.ObjType;
   ClassIdx = s_obb.ClassIdx;
   (*this)(s_obb);
   CenterPos = GetCenterPosition (   );
   InitRegion(s_obb);
   InitPos = CenterPos;
   Status |= TBX_STATUS_CREATED;
   if (s_obb.SubRgn_Head.Width) {
      Head = s_obb.SubRgn_Head;
      Status |= TBX_STATUS_HEAD_INITIALIZED;
   }
   if (s_obb.SubRgn_Face.Width) {
      Face = s_obb.SubRgn_Face;
      Status |= TBX_STATUS_FACE_INITIALIZED;
   }
}

 void TrackedBox::Predict (   )

{
   if (Velocity.X == MC_VLV) {
      PredRegion = *(IBox2D*)this;
   }
   else {
      PredRegion.X      = (int)(X + Velocity.X + 0.5f);
      PredRegion.Y      = (int)(Y + Velocity.Y + 0.5f);
      PredRegion.Width  = Width;
      PredRegion.Height = Height;
   }
}

 void TrackedBox::Update (ObjBndBox& s_obb,float ru_rate,float vu_rate)

{
   FPoint2D cp1 = GetCenterPosition (   );
   FPoint2D cp2 = s_obb.GetCenterPosition (   );
   FPoint2D dp  = cp2 - cp1;
   float a = 1.0f - vu_rate;
   float b = vu_rate;
   if (Velocity.X == MC_VLV) Velocity = dp;
   else Velocity = a * Velocity + b * dp;
   IBox2D p_rgn = *(IBox2D*)this;
   a      = 1.0f - ru_rate;
   b      = ru_rate;
   X      = (int)(a * PredRegion.X      + b * s_obb.X      + 0.5f);
   Y      = (int)(a * PredRegion.Y      + b * s_obb.Y      + 0.5f);
   Width  = (int)(a * PredRegion.Width  + b * s_obb.Width  + 0.5f);
   Height = (int)(a * PredRegion.Height + b * s_obb.Height + 0.5f);
   CenterPos = GetCenterPosition (   );
   MovingDistance = Mag (CenterPos - InitPos);
   if (MovingDistance > MaxMovingDistance) MaxMovingDistance = MovingDistance;
   // Head 영역에 대해 처리한다.
   if (Head.Width && s_obb.SubRgn_Head.Width) {
      // 이전 프레임과 현재 프레임에서 모두 Head가 검출된 경우
      // Head 영역에 대한 Predictor가 없는 관계로 그냥 현재 검출된 Head 영역을 기록한다.
      Head(s_obb.SubRgn_Head);
   }
   else if (!Head.Width && s_obb.SubRgn_Head.Width) {
      // 이전 프레임에서는 Head가 검출되지 않았고, 현재 프레임에서 Head가 검출된 경우
      Head(s_obb.SubRgn_Head);
      Status |= TBX_STATUS_HEAD_INITIALIZED;
   }
   else if (Head.Width  && !s_obb.SubRgn_Head.Width) {
      // 이전 프레임에서는 Head가 검출되었고, 현재Wid 프레임에서는 Head가 검출되지 않은 경우
      float sx = (float)Width  / p_rgn.Width;
      float sy = (float)Height / p_rgn.Height;
      float tx = X - sx * p_rgn.X;
      float ty = Y - sy * p_rgn.Y;
      Head.X      = (int)(sx * Head.X + tx + 0.5f);
      Head.Y      = (int)(sy * Head.Y + ty + 0.5f);
      Head.Width  = (int)(sx * Head.Width  + 0.5f);
      Head.Height = (int)(sy * Head.Height + 0.5f);
   }
   // Face 영역에 대해 처리한다.
   if (Face.Width && s_obb.SubRgn_Face.Width) {
      // 이전 프레임과 현재 프레임에서 모두 Face가 검출된 경우
      // Face 영역에 대한 Predictor가 없는 관계로 그냥 현재 검출된 Face 영역을 기록한다.
      Face(s_obb.SubRgn_Face);
   }
   else if (!Face.Width && s_obb.SubRgn_Face.Width) {
      // 이전 프레임에서는 Face가 검출되지 않았고, 현재 프레임에서는 Face가 검출된 경우
      Face(s_obb.SubRgn_Face);
      Status |= TBX_STATUS_FACE_INITIALIZED;
   }
   else if (Face.Width && !s_obb.SubRgn_Face.Width) {
      // 이전 프레임에서는 Face가 검출되었고, 현재 프레임에서는 Face가 검출되지 않은 경우
      Face.Clear (   );
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// Class: BoxTracking
//
///////////////////////////////////////////////////////////////////////////////

 BoxTracking::BoxTracking (   )

{
   Flag_Enabled       = TRUE;
   BoxCount           = 0;

   Options            = 0;

   BMThreshold        = 0.1f;
   RegionUpdateRate   = 0.5f;
   VelocityUpdateRate = 1.0f;
    
   MinCount_Tracked   = 3;
   MinMovingDistance  = 0.0f;
   MaxTime_Undetected = 1.0f;

   FrameRate          = 7.0f;
   FrameDuration      = 1.0f / FrameRate;

   _Init (   );
}

 BoxTracking::~BoxTracking (   )

{
   Close (   );
}

 void BoxTracking::_Init (   )

{
   FrameRate  = 0.0f;
   BoxCount   = 1;
   FrameCount = 0;   
   SrcImgSize.Clear (   );
}

 int BoxTracking::CheckBoxInTrackingArea (TrackedBox* t_box)

{
   int cx = t_box->X + t_box->Width  / 2;
   int cy = t_box->Y + t_box->Height / 2;
   if (0 <= cx && cx < SrcImgSize.Width && 0 <= cy && cy < SrcImgSize.Height) return (DONE);
   IBox2D s_box(0,0,SrcImgSize.Width,SrcImgSize.Height);
   float or1 = (float)GetOverlapArea (s_box,*t_box) / (t_box->Width * t_box->Height);
   if (or1 > 0.25f) return (DONE); // PARAMETERS
   return (1);
}

 void BoxTracking::CleanBoxList (   )

{
   TrackedBox* t_box = TrackedBoxes.First (   );
   while (t_box != NULL) {
      TrackedBox* n_obb = TrackedBoxes.Next (t_box);
      if (t_box->Status & TBX_STATUS_TO_BE_REMOVED) {
         TrackedBoxes.Remove (t_box);
         delete t_box;
      }
      t_box = n_obb;
   }
}

 void BoxTracking::Close (   )

{
   TrackedBoxes.Delete (   );
   _Init (   );
}

 void BoxTracking::GetBoxArray (TBXArray1D& d_array)

{
   int i;

   d_array.Delete (   );
   int n = TrackedBoxes.GetNumItems (   );
   if (!n) return;
   d_array.Create (n);
   TrackedBox* t_box = TrackedBoxes.First (   );
   for (i = 0; i < d_array.Length; i++) {
      d_array[i] = t_box;
      t_box = TrackedBoxes.Next (t_box);
   }
}

 void BoxTracking::GetBoxMatchingScoreMatrix (TBXArray1D& tb_array,OBBArray1D& db_array,FArray2D& d_array)

{
   int i,j;
   
   d_array.Clear (   );
   int flag_dcoc = FALSE;
   if (Options & BXT_OPTION_DONT_CARE_OBJECT_CLASS) flag_dcoc = TRUE;
   for (i = 0; i < d_array.Height; i++) {
      TrackedBox& t_box = *tb_array[i];
      float* l_d_array = d_array[i];
      for (j = 0; j < d_array.Width; j++) {
         ObjBndBox& d_box = db_array[j];
         if (flag_dcoc || t_box.ClassIdx == d_box.ClassIdx) {
            float score1 = GetMinOverlapRatio (t_box.PredRegion,d_box);
            float score2 = 0.0f;
            if (score1 > 0.0f) score2 = GetSimilarity (t_box.PredRegion,d_box);
            l_d_array[j] = 0.7f * score1 + 0.3f * score2; // PARAMETERS
         }
      }
   }
}

 int BoxTracking::GetNewBoxID (   )

{
   return (BoxCount++);
}

 void BoxTracking::GetPredictedBoxRegions (   )

{
   TrackedBox* t_box = TrackedBoxes.First (   );
   while (t_box != NULL) {
      t_box->Predict (   );
      t_box = TrackedBoxes.Next (t_box);
   }
}

 int BoxTracking::Initialize (ISize2D& si_size,float frm_rate)

{
   if (!IsEnabled (   )) return (1);
   Close (   );
   SrcImgSize    = si_size;
   FrameRate     = frm_rate;
   FrameDuration = 1.0f / frm_rate;
   return (DONE);
}

 int BoxTracking::Perform (OBBArray1D& db_array,BGRImage* s_cimage)

{
   int i;

   #if defined(__DEBUG_BXT)
   CString text;
   _DVX_BXT = _DVY_BXT = 0;
   _DebugView_BXT->Clear (   );
   #endif
   FrameCount++;
   CleanBoxList (   );
   GetPredictedBoxRegions (   );
   TBXArray1D tb_array;
   GetBoxArray (tb_array);
   #if defined(__DEBUG_BXT)
   if (s_cimage != NULL) {
      _DebugView_BXT->DrawImage (*s_cimage,_DVX_BXT,_DVY_BXT);
      for (i = 0; i < db_array.Length; i++) {
         ObjBndBox& d_box = db_array[i];
         int px = _DVX_BXT + d_box.X;
         int py = _DVY_BXT + d_box.Y;
         _DebugView_BXT->DrawRectangle (px,py,d_box.Width,d_box.Height,PC_GREEN,2);
         px += d_box.Width + 5; py -= 5;
         text.Format (_T("%d"),i);
         _DebugView_BXT->DrawText (text,px,py,PC_GREEN,TRANSPARENT,FT_ARIAL,14);
      }
      for (i = 0; i < tb_array.Length; i++) {
         TrackedBox* t_box = tb_array[i];
         int px = _DVX_BXT + t_box->PredRegion.X;
         int py = _DVY_BXT + t_box->PredRegion.Y;
         _DebugView_BXT->DrawRectangle (px,py,t_box->PredRegion.Width,t_box->PredRegion.Height,PC_RED,2);
         px -= 15; py -= 5;
         text.Format (_T("%d"),t_box->ID);
         _DebugView_BXT->DrawText (text,px,py,PC_RED,TRANSPARENT,FT_ARIAL,14);
      }
      _DVY_BXT += s_cimage->Height;
      _DebugView_BXT->DrawText (_T("Detected(Green) and Predicted(Red) Boxes"),_DVX_BXT,_DVY_BXT,PC_GREEN);
      _DVY_BXT += 20;
   }
   #endif
   for (i = 0; i < db_array.Length; i++) db_array[i].MatTrkBoxIdx = -1;
   for (i = 0; i < tb_array.Length; i++) {
      tb_array[i]->MatDetBoxIdx = -1;
      tb_array[i]->Status &= ~(
         TBX_STATUS_CREATED|TBX_STATUS_FACE_INITIALIZED|TBX_STATUS_HEAD_INITIALIZED|TBX_STATUS_OUT_OF_TRACKING_AREA|TBX_STATUS_UNDETECTED|TBX_STATUS_VERIFIED_START
      );
   }
   if (db_array.Length && tb_array.Length) {
      FArray2D bms_matrix(db_array.Length,tb_array.Length);
      GetBoxMatchingScoreMatrix (tb_array,db_array,bms_matrix);
      #if defined(__DEBUG_BXT) && defined(__CONSOLE)
      logd ("[Box Matching Score Matrix]\n");
      logd ("     ");
      for (i = 0; i < bms_matrix.Width; i++) {
         logd ("[%02d] ",i);
      }
      logd ("\n");
      for (i = 0; i < bms_matrix.Height; i++) {
         logd ("[%02d] ",tb_array[i]->ID);
         for (int j = 0; j < bms_matrix.Width; j++) {
            logd ("%4.2f ",bms_matrix[i][j]);
         }
         logd ("\n");
      }
      #endif
      PerformBoxMatching (tb_array,db_array,bms_matrix);
   }
   UpdateBoxList (db_array);
   #if defined(__DEBUG_BXT)
   if (s_cimage != NULL) {
      _DebugView_BXT->DrawImage (*s_cimage,_DVX_BXT,_DVY_BXT);
      for (i = 0; i < db_array.Length; i++) {
         ObjBndBox& d_box = db_array[i];
         int px = _DVX_BXT + d_box.X;
         int py = _DVY_BXT + d_box.Y;
         _DebugView_BXT->DrawRectangle (px,py,d_box.Width,d_box.Height,PC_GREEN,1);
      }
      TrackedBox* t_box = TrackedBoxes.First (   );
      while (t_box != NULL) {
         COLORREF pen_color = PC_RED;
         if (!(t_box->Status & TBX_STATUS_VERIFIED)) pen_color = RGB(255,150,150);
         int px = _DVX_BXT + t_box->X;
         int py = _DVY_BXT + t_box->Y;
         _DebugView_BXT->DrawRectangle (px,py,t_box->Width,t_box->Height,pen_color,2);
         text.Format (_T("%d"),t_box->ID);
         px += t_box->Width + 5;
         _DebugView_BXT->DrawText (text,px,py,PC_RED,TRANSPARENT,FT_ARIAL,16);
         t_box = TrackedBoxes.Next (t_box);
      }
      _DVY_BXT += s_cimage->Height;
      _DebugView_BXT->DrawText (_T("Detected(Green) and Tracked(Red) Boxes"),_DVX_BXT,_DVY_BXT,PC_GREEN);
      _DVY_BXT += 20;
   }
   #endif
   #if defined(__DEBUG_BXT)
   _DebugView_BXT->Invalidate (   );
   #endif
   return (DONE);
}

 void BoxTracking::PerformBoxMatching (TBXArray1D& tb_array,OBBArray1D& db_array,FArray2D& bms_matrix)

{
   int i,j,n;
   
   OSArray1D os_array1(bms_matrix.Width * bms_matrix.Height);
   for (i = n = 0; i < bms_matrix.Height; i++) {
      float* l_bms_matrix = bms_matrix[i];
      for (j = 0; j < bms_matrix.Width; j++) {
         if (l_bms_matrix[j] >= BMThreshold) {
            os_array1[n].IndexNo = i * bms_matrix.Width + j;
            os_array1[n].Score   = (double)l_bms_matrix[j];
            n++;
         }
      }
   }
   if (!n) return;
   OSArray1D os_array2 = os_array1.Extract (0,n);
   qsort (os_array2,os_array2.Length,sizeof(ObjectScore),Compare_ObjectScore_Descending);
   for (i = 0; i < os_array2.Length; i++) {
      ObjectScore& os = os_array2[i];
      int tb_idx = os.IndexNo / bms_matrix.Width;
      int db_idx = os.IndexNo % bms_matrix.Width;
      if (tb_array[tb_idx]->MatDetBoxIdx < 0 && db_array[db_idx].MatTrkBoxIdx < 0) {
         tb_array[tb_idx]->MatDetBoxIdx = db_idx;
         db_array[db_idx].MatTrkBoxIdx  = tb_idx;
      }
   }
   #if defined(__DEBUG_BXT) && defined(__CONSOLE)
   logd ("[Box Matching Result (TB => DB)]\n");
   for (i = 0; i < tb_array.Length; i++) {
      logd ("[%d => %d]",tb_array[i]->ID,tb_array[i]->MatDetBoxIdx);
   }
   logd ("\n");
   logd ("[Box Matching Result (DB => TB)]\n");
   for (i = 0; i < db_array.Length; i++) {
      int tb_idx = db_array[i].MatTrkBoxIdx;
      if (tb_idx >= 0) logd ("[%d => %d]",i,tb_array[tb_idx]->ID);
      else logd ("[%d => -1]",i);
   }
   logd ("\n");
   #endif
}

 void BoxTracking::Reset (   )

{
   FrameCount = 0;
   TrackedBox* t_box = TrackedBoxes.First (   );
   while (t_box != NULL) {
      t_box->Status |= TBX_STATUS_TO_BE_REMOVED;
      t_box = TrackedBoxes.Next (t_box);
   }
}

 void BoxTracking::UpdateBoxList (OBBArray1D& db_array)

{
   int i;

   // 기존 TB들을 업데이트한다.
   TrackedBox* t_box = TrackedBoxes.First (   );
   while (t_box != NULL) {
      if (t_box->MatDetBoxIdx >= 0) {
         // TB와 매칭이 되는 DB가 있는 경우
         ObjBndBox& d_obb = db_array[t_box->MatDetBoxIdx];
         t_box->Update (d_obb,RegionUpdateRate,VelocityUpdateRate);
         t_box->Time_Undetected = 0.0f;
         // DB가 STICK을 소유하고 있는 경우
         if (d_obb.Properties & OBJ_TYPE_STICK) t_box->Time_Stick += FrameDuration;
         else {
            t_box->Time_Stick -= 0.3f * FrameDuration; // PARAMETERS
            if (t_box->Time_Stick < 0.0f) t_box->Time_Stick = 0.0f;
         }
      }
      else {
         // TB와 매칭이 되는 DB가 없는 경우
         t_box->Status |= TBX_STATUS_UNDETECTED;

         // Prediction 결과를 이용한다.
         // t_box->Update (t_box->PredRegion,RegionUpdateRate,VelocityUpdateRate);
         // (Predictor보다는 Template-Based Tracker를 사용하는 것이 낫다!)
         
         t_box->Time_Undetected += FrameDuration;
         if (t_box->Time_Undetected > MaxTime_Undetected) t_box->Status |= TBX_STATUS_TO_BE_REMOVED;
      }
      // TB가 추적 영역 내에 있는지 체크한다.
      if (CheckBoxInTrackingArea (t_box) != DONE) t_box->Status |= TBX_STATUS_OUT_OF_TRACKING_AREA|TBX_STATUS_TO_BE_REMOVED;
      t_box = TrackedBoxes.Next (t_box);
   }
   // 기존 TB와 매칭이 되지 않는 DB를 신규 TB로 추가한다.
   for (i = 0; i < db_array.Length; i++) {
      if (db_array[i].MatTrkBoxIdx < 0) {
         t_box = new TrackedBox;
         t_box->ID = GetNewBoxID (   );
         t_box->Initialize (db_array[i]);
         TrackedBoxes.Add (t_box);
      }
   }
   // TB들의 카운트 값 증가 및 유효성 체크를 수행한다.
   float md_thrsld = MinMovingDistance * 240.0f;
   t_box = TrackedBoxes.First (   );
   while (t_box != NULL) {
      if (!(t_box->Status & TBX_STATUS_TO_BE_REMOVED)) {
         t_box->Count_Tracked++;
         t_box->Time_Tracked += FrameDuration;
         if (t_box->Count_Tracked > MinCount_Tracked) {
            if (!(t_box->Status & TBX_STATUS_VALIDATED)) t_box->Status |= TBX_STATUS_VALIDATED;
            if (!(t_box->Status & TBX_STATUS_VERIFIED )) {
               if (t_box->MaxMovingDistance >= md_thrsld) t_box->Status |= TBX_STATUS_VERIFIED|TBX_STATUS_VERIFIED_START;
            }
         }
         else {
            if (t_box->Status & TBX_STATUS_UNDETECTED) t_box->Status  |= TBX_STATUS_TO_BE_REMOVED;
         }
      }
      t_box = TrackedBoxes.Next (t_box);
   }
}

}

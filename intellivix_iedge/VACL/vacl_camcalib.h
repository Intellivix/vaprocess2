#if !defined(__VACL_CAMCALIB_H)
#define __VACL_CAMCALIB_H

#include <vector>
#include "vacl_objtrack.h"
#include "vacl_optimize.h"

using namespace std;

//
//  [ 카메라 파라미터의 추정 - 카메라 좌표계에서 x 및 z 축 좌표를 이용 ]
//
//  카메라의 원점은 이미지 평면의 중심에 있고, 오른쪽 방향이 카메라의 x축, 아래 방향이 카메라의 y축, 바라보는 방향이 z축이다.
//  카메라는 지면을 향하는 방향으로 0º< θ < 90º만큼 tilt할 수 있고, 아래 그림에서의 현재 카메라의 tilt angle은 θ = 0º이다.
//  (거리에 관한 수치의 단위는 모두 meter로 표기)
//
//  ****** NOTE ********************************************************************
//  *  tilt angle이 경계값, 즉 0º또는 90º에 해당할 때에, 초점 거리가 소거되는 등 *
//  *  수식이 불안정해지므로, 이는 제외한다고 가정하였다.                          *
//  ********************************************************************************
//      /
//  ┬  ㅡㅡㅡㅡㅡ> z (카메라가 바라보는 방향)
//  ： |\
//  ： |                                 ┌ f  0  ox ┐    f : 카메라의 초점거리
//  ： |              Intrinsic Matrix = │ 0  f  oy │,   ox: 영상 width의 1/2 
//  ： |                                 └ 0  0   1 ┘    oy: 영상 height의 1/2
//  ： v
//  ： y
//  ：																											
//  H (카메라의 설치 높이)                                                                        지면의 normal vector
//  ：                       사람의 top 좌표 (xt, yt, zt) -> ●┬                              Δ (0, -cosθ, -sinθ)
//	 ：                                                       │：                              | 
//	 ：                                                       │：										  |   
//	 ：                                                       │h (모델의 키)                   |
//	 ：                                                       │：										  |
//  ：                                                       │：						              |
//  ：                                                       │：	                   	        |
//  ┴ ================== 사람의 bottom 좌표 (xb, yb, zb) -> ●┴ =============================================== 지 면 =======
// 
//
//                      (ut-ox)(H-h)           (vt-oy)(H-h)              f(H-h)
// (xt, yt, zt) = ( ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ , ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ , ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ )
//                  fsinθ + (vt-oy)cosθ    fsinθ + (vt-oy)cosθ	   fsinθ + (vt-oy)cosθ
// 
//
//                        (ub-ox)H               (vb-oy)H                  fH
// (xb, yb, zb) = ( ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ , ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ , ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ )
//                  fsinθ + (vb-oy)cosθ    fsinθ + (vb-oy)cosθ	   fsinθ + (vb-oy)cosθ
//
// (ut, vt): (xt, yt, zt)가 이미지에 맺히는 픽셀 위치
// (ub, vb): (xb, yb, zb)가 이미지에 맺히는 픽셀 위치
//
//
// (xt, yt, zt)와 (xb, yb, zb)는 tilt가 0º인 카메라의 좌표계로 변환될 수 있으며, 다음의 회전 행렬 R을 통해 변환될 수 있다.
// 이때, tilt가 0º인 카메라의 좌표계에서의 사람의 top 좌표와 bottom 좌표는 x 및 z 값이 서로 같아야 한다. (xtw = xbw, ztw = zbw)
//
//     ┌xt┐ ┌ 1    0     0   ┐┌xt┐ ┌        xt        ┐ ┌xtw┐
//	   R│yt│=│ 0   cosθ  sinθ ││yt│=│  ytcosθ + ztsinθ │=│ytw│
//	    └zt┘ └ 0  -sinθ  cosθ ┘└zt┘ └ -ytsinθ + ztcosθ ┘ └ztw┘
//	  
//     ┌xb┐ ┌ 1    0     0   ┐┌xb┐ ┌        xb        ┐ ┌xbw┐
//    R│yb│=│ 0   cosθ  sinθ ││yb│=│  ybcosθ + zbsinθ │=│ybw│
//     └zb┘ └ 0  -sinθ  cosθ ┘└zb┘ └ -ybsinθ + zbcosθ ┘ └zbw┘                Cost Function1  
//                                                                                           |
//  ztw = zbw  <=>  -ytsinθ + ztcosθ = -ybsinθ + zbcosθ  <=>                                 v
//                                                                  *******************************************************
//   {fcosθ - (vt-oy)sinθ}(H-h)     {fcosθ - (vb-oy)sinθ}H          *   {f - (vt-oy)tanθ}(H-h)      {f - (vb-oy)tanθ}H    *
//  ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ = ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ  <=>  * ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ = ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ * 
//      fsinθ + (vt-oy)cosθ           fsinθ + (vb-oy)cosθ           *      ftanθ + (vt-oy)            ftanθ + (vb-oy)     *
//                                                                  *******************************************************
//   
//
// 한편, (xtw, ytw, ztw)와 (xbw, ybw, zbw)는 R의 역행렬에 의해 다시 (xt, yt, zt) 및 (xb, yb, zb)가 된다.
// 
//       ┌xtw┐ ┌ 1   0      0   ┐┌xtw┐ ┌        xtw        ┐ ┌xt┐            
// inv(R)│ytw│=│ 0  cosθ  -sinθ ││ytw│=│ ytwcosθ - ztwsinθ │=│yt│ =>  ut-ox = fxt/zt = fxtw/(ytwsinθ + ztwcosθ)
//       └ztw┘ └ 0  sinθ   cosθ ┘└ztw┘ └ ytwsinθ + ztwcosθ ┘ └zt┘
//   
//       ┌xbw┐ ┌ 1   0      0   ┐┌xbw┐ ┌        xbw        ┐ ┌xb┐
// inv(R)│ybw│=│ 0  cosθ  -sinθ ││ybw│=│ ybwcosθ - zbwsinθ │=│yb│ =>  ub-ox = fxb/zb = fxbw/(ybwsinθ + zbwcosθ)
//       └zbw┘ └ 0  sinθ   cosθ ┘└zbw┘ └ ybwsinθ + zbwcosθ ┘ └zb┘
//
// 또한, xtw = xbw, ztw = zbw 이므로 fxtw/ztw = fxbw/zbw 이어야 한다.
//
//   fxtw/ztw = fxbw/zbw  <=>  (ut-ox)(ytwsinθ + ztwcosθ)/ztw = (ub-ox)(ybwsinθ + zbwcosθ)/zbw  <=>
//
//   ****************************************************************
//   *   (ut-ox)(ytwsinθ + ztwcosθ)     (ub-ox)(ybwsinθ + zbwcosθ)  *                    (fxtw/ztw = fxbw/zbw로 정의했을 때 보다
//   *  ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ = ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ *  <- Cost Function2  xtw/fztw = xbw/fzbw로 정의했을 때 
//   *            fztw                            fzbw				  *                     정확도가 더 높았음)
//   ****************************************************************
//
//
// Focal Length, Tilt Angle, Camera Height 중 하나의 미지수만 모를 때에는 "추정"이 아니라 식을 전개하여 "계산"할 수 있다.
// 이때, ztw = zbw의 식으로부터 다음을 얻을 수 있다.
//
//      H(vb-vt)(secθ)^2 + h{(vt-oy)(tanθ)^2-(vb-oy)} ±sqrt(4(vt-oy)(vb-oy)(htanθ)^2 + {H(vb-vt)(secθ)^2 + h{(vt-oy)(tanθ)^2-(vb-oy)}}^2) 
// f = ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ
//                                                                2htanθ
//
//      (ub-ox)(vt-oy)H - (ut-ox)(vb-oy)(H-h)
// f = ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ  (이 식은 보다 간단하지만 model의 분포에 민감)
//          tanθ{(ut-ox)(h-H) + (ub-ox)H}
//
//
//         h(f^2-(vt-oy)(vb-oy)) ±sqrt({h(f^2-(vt-oy)(vb-oy))}^2 + 4f^2{(vt-oy)(h-H)+(vb-oy)H}{(vt-oy)H+(vb-oy)(h-H)})
// tanθ = ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ
//                                                 2f{(vt-oy)(h-H)+(vb-oy)H}
//
//         (ub-ox)(vt-oy)H - (ut-ox)(vb-oy)(H-h)
// tanθ = ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ  (이 식은 보다 간단하지만 model의 분포에 민감)
//             f{(ut-ox)(h-H) + (ub-ox)H}
//
//
//      h{(vt-oy)tanθ - f}{(vb-oy) + ftanθ}(cosθ)^2
// H = ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ
//                       f(vt-vb)

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: Calibrate_WithModelDepth
//
///////////////////////////////////////////////////////////////////////////////

class Calibrate_WithModelDepth : public Minimization_Powell
{

protected:
   double HalfWidth;
   double HalfHeight;
   double Kappa;
   vector<DPoint2D> ModelTops;
   vector<DPoint2D> ModelBottoms;
   vector<double> ModelHeights;
   vector<double> ModelDistances; // 현재 사용하지 않음

   Calibrate_WithModelDepth(int width,                  // 영상의 width
                            int height,                 // 영상의 height
                            vector<DPoint2D>& model_ts, // 영상에 투영된 모델의 top(head) 좌표
                            vector<DPoint2D>& model_bs, // 영상에 투영된 모델의 bottom(foot) 좌표
                            vector<double>& model_hs,   // 모델의 실제 키 (in meter)
                            double k1 = 0.0);           // distortion coefficient k1

   double getCost(double focal, double tan_tilt, double cam_height, double k1);
   double getSubCost(double focal, double tan_tilt, double cam_height, double k1);
   DPoint2D getUndistortedPixelPosition(DPoint2D& uv_d, double k1);
   
public:
   double MinCost;

   double getCameraHeight(double f, double tan_tilt, double k1);
   double getFocalLength(double tan_tilt, double cam_height, double k1);
   double getTiltAngle(double f, double cam_height, double k1);

   void setModelDistances(vector<double>& model_ds); // 현재 사용하지 않음
};

// =========================================================================================== //
//                                                                                             //
// Focal Length, Tilt Angle, Camera Height + (Distortion Coefficient) 추정하는 클래스          //
//                                                                                             //
// =========================================================================================== //

///////////////////////////////////////////////////////////////////////////////
//
// Class: EstimateFTH_WithModelDepth
//
///////////////////////////////////////////////////////////////////////////////

class EstimateFTH_WithModelDepth : public Calibrate_WithModelDepth
{
protected:
   virtual double CostFunction(Matrix &vec_p);

public:
   EstimateFTH_WithModelDepth(int width, 
                              int height, 
                              vector<DPoint2D>& model_ts, 
                              vector<DPoint2D>& model_bs, 
                              vector<double>& model_hs,
                              double k1 = 0.0);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EstimateFTHK_WithModelDepth
//
///////////////////////////////////////////////////////////////////////////////

class EstimateFTHK_WithModelDepth : public Calibrate_WithModelDepth
{
protected:
   virtual double CostFunction(Matrix &vec_p);

public:
   EstimateFTHK_WithModelDepth(int width,
                               int height,
                               vector<DPoint2D>& model_ts, 
                               vector<DPoint2D>& model_bs, 
                               vector<double>& model_hs);
};

// =========================================================================================== //
//                                                                                             //
// Focal Length, Tilt Angle + (Distortion Coefficient) 추정하는 클래스                         //
//                                                                                             //
// =========================================================================================== //

///////////////////////////////////////////////////////////////////////////////
//
// Class: EstimateFT_WithModelDepth
//
///////////////////////////////////////////////////////////////////////////////

class EstimateFT_WithModelDepth : public Calibrate_WithModelDepth
{
protected:
   double CamHeight;

   virtual double CostFunction(Matrix &vec_p);

public:
   EstimateFT_WithModelDepth(int width,
                             int height,
                             double cam_h,  // 설치된 카메라의 지면으로부터의 높이 (in meter)
                             vector<DPoint2D>& model_ts, 
                             vector<DPoint2D>& model_bs, 
                             vector<double>& model_hs,
                             double k1 = 0.0);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EstimateFTK_WithModelDepth
//
///////////////////////////////////////////////////////////////////////////////

class EstimateFTK_WithModelDepth : public Calibrate_WithModelDepth
{
protected:
   double CamHeight;

   virtual double CostFunction(Matrix &vec_p);

public:
   EstimateFTK_WithModelDepth(int width,
                              int height,
                              double cam_h,
                              vector<DPoint2D>& model_ts, 
                              vector<DPoint2D>& model_bs, 
                              vector<double>& model_hs);
};

// =========================================================================================== //
//                                                                                             //
// Focal Length, Camera Height + (Distortion Coefficient) 추정하는 클래스                      //
//                                                                                             //
// =========================================================================================== //

///////////////////////////////////////////////////////////////////////////////
//
// Class: EstimateFH_WithModelDepth
//
///////////////////////////////////////////////////////////////////////////////

class EstimateFH_WithModelDepth : public Calibrate_WithModelDepth
{
protected:
   double TanTilt;

   virtual double CostFunction(Matrix &vec_p);

public:
   EstimateFH_WithModelDepth(int width,
                             int height,
                             double tilt,  // 카메라의 tilt angle (in degree)
                             vector<DPoint2D>& model_ts, 
                             vector<DPoint2D>& model_bs, 
                             vector<double>& model_hs,
                             double k1 = 0.0);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EstimateFHK_WithModelDepth
//
///////////////////////////////////////////////////////////////////////////////

class EstimateFHK_WithModelDepth : public Calibrate_WithModelDepth
{
protected:
   double TanTilt;

   virtual double CostFunction(Matrix &vec_p);

public:
   EstimateFHK_WithModelDepth(int width,
                              int height,
                              double tilt,
                              vector<DPoint2D>& model_ts, 
                              vector<DPoint2D>& model_bs, 
                              vector<double>& model_hs);
};


// =========================================================================================== //
//                                                                                             //
// Tilt Angle, Camera Height + (Distortion Coefficient) 추정하는 클래스                        //
//                                                                                             //
// =========================================================================================== //

///////////////////////////////////////////////////////////////////////////////
//
// Class: EstimateTH_WithModelDepth
//
///////////////////////////////////////////////////////////////////////////////

class EstimateTH_WithModelDepth : public Calibrate_WithModelDepth
{
protected:
   double F;

   virtual double CostFunction(Matrix &vec_p);

public:
   EstimateTH_WithModelDepth(int width,
                             int height,
                             double focal,  // 카메라의 focal length
                             vector<DPoint2D>& model_ts, 
                             vector<DPoint2D>& model_bs, 
                             vector<double>& model_hs,
                             double k1 = 0.0);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EstimateTHK_WithModelDepth
//
///////////////////////////////////////////////////////////////////////////////

class EstimateTHK_WithModelDepth : public Calibrate_WithModelDepth
{
protected:
   double F;

   virtual double CostFunction(Matrix &vec_p);

public:
   EstimateTHK_WithModelDepth(int width,
                              int height,
                              double focal,
                              vector<DPoint2D>& model_ts, 
                              vector<DPoint2D>& model_bs, 
                              vector<double>& model_hs);
};

// =========================================================================================== //
//                                                                                             //
// Focal Length + (Distortion Coefficient) 추정하는 클래스                                     //
//                                                                                             //
// =========================================================================================== //

///////////////////////////////////////////////////////////////////////////////
//
// Class: EstimateF_WithModelDepth
//
///////////////////////////////////////////////////////////////////////////////

class EstimateF_WithModelDepth : public Calibrate_WithModelDepth
{
protected:
   virtual double CostFunction(Matrix &vec_p);

public:
   double TanTilt;
   double CamHeight;

   EstimateF_WithModelDepth(int width,
                            int height,
                            double tilt,   // 카메라의 tilt angle (in degree)
                            double cam_h,  // 설치된 카메라의 지면으로부터의 높이 (in meter)
                            vector<DPoint2D>& model_ts, 
                            vector<DPoint2D>& model_bs, 
                            vector<double>& model_hs,
                            double k1 = 0.0);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EstimateFK_WithModelDepth
//
///////////////////////////////////////////////////////////////////////////////

class EstimateFK_WithModelDepth : public EstimateF_WithModelDepth
{
protected:
   virtual double CostFunction(Matrix &vec_p);

public:
   EstimateFK_WithModelDepth(int width,
                             int height,
                             double tilt,
                             double cam_h,
                             vector<DPoint2D>& model_ts, 
                             vector<DPoint2D>& model_bs, 
                             vector<double>& model_hs);
};


// =========================================================================================== //
//                                                                                             //
// Tilt Angle + (Distortion Coefficient) 추정하는 클래스                                       //
//                                                                                             //
// =========================================================================================== //

///////////////////////////////////////////////////////////////////////////////
//
// Class: EstimateT_WithModelDepth
//
///////////////////////////////////////////////////////////////////////////////

class EstimateT_WithModelDepth : public Calibrate_WithModelDepth
{
protected:
   virtual double CostFunction(Matrix &vec_p);

public:
   double F;
   double CamHeight;

   EstimateT_WithModelDepth(int width,
                            int height,
                            double focal,  // 카메라의 focal length
                            double cam_h,  // 설치된 카메라의 지면으로부터의 높이 (in meter)
                            vector<DPoint2D>& model_ts, 
                            vector<DPoint2D>& model_bs, 
                            vector<double>& model_hs,
                            double k1 = 0.0);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EstimateTK_WithModelDepth
//
///////////////////////////////////////////////////////////////////////////////

class EstimateTK_WithModelDepth : public EstimateT_WithModelDepth
{
protected:
   virtual double CostFunction(Matrix &vec_p);

public:
   EstimateTK_WithModelDepth(int width,
                             int height,
                             double focal,
                             double cam_h,
                             vector<DPoint2D>& model_ts, 
                             vector<DPoint2D>& model_bs, 
                             vector<double>& model_hs);
};


// =========================================================================================== //
//                                                                                             //
// Camera Height + (Distortion Coefficient) 추정하는 클래스                                       //
//                                                                                             //
// =========================================================================================== //

///////////////////////////////////////////////////////////////////////////////
//
// Class: EstimateH_WithModelDepth
//
///////////////////////////////////////////////////////////////////////////////

class EstimateH_WithModelDepth : public Calibrate_WithModelDepth
{
protected:
   virtual double CostFunction(Matrix &vec_p);

public:
   double F;
   double TanTilt;

   EstimateH_WithModelDepth(int width,
                            int height,
                            double focal,  // 카메라의 focal length
                            double tilt,   // 카메라의 tilt angle (in degree)
                            vector<DPoint2D>& model_ts, 
                            vector<DPoint2D>& model_bs, 
                            vector<double>& model_hs,
                            double k1 = 0.0);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: EstimateHK_WithModelDepth
//
///////////////////////////////////////////////////////////////////////////////

class EstimateHK_WithModelDepth : public EstimateH_WithModelDepth
{
protected:
   virtual double CostFunction(Matrix &vec_p);

public:
   EstimateHK_WithModelDepth(int width,
                             int height,
                             double focal,
                             double tilt,
                             vector<DPoint2D>& model_ts, 
                             vector<DPoint2D>& model_bs, 
                             vector<double>& model_hs);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: CamAutoCalibByRefObjs
//
///////////////////////////////////////////////////////////////////////////////

class CamAutoCalibByRefObjs
{
private:
   int Width, Height;
   vector<double>   ModelHeights;
   vector<DPoint2D> ModelTops;
   vector<DPoint2D> ModelBottoms;

public:
   CamAutoCalibByRefObjs(int width,            // 영상의 width
                   int height,                 // 영상의 height
                   vector<DPoint2D>& model_ts, // 영상에 투영된 모델의 top(head) 좌표
                   vector<DPoint2D>& model_bs, // 영상에 투영된 모델의 bottom(foot) 좌표
                   vector<double>& model_hs);  // 모델의 실제 키 (in meter)
   ~CamAutoCalibByRefObjs();


   ///// 함수 호출 및 반환 시 사용되는 주요 파라미터들의 단위: 
   ///// Tilt (in degree), Camera Height (in meter)

   // Focal Length + (Distortion Coefficient) 추정하는 함수
   void calibrateF(double& est_focal, double tilt, double cam_h, double k1 = 0.0);
   void calibrateFK(double& est_focal, double& est_k1, double tilt, double cam_h);

   // Tilt Angle + (Distortion Coefficient) 추정하는 함수
   void calibrateT(double& est_tilt, double focal, double cam_h, double k1 = 0.0);
   void calibrateTK(double& est_tilt, double& est_k1, double focal, double cam_h);

   // Camera Height + (Distortion Coefficient) 추정하는 함수
   void calibrateH(double& est_camh, double focal, double tilt, double k1 = 0.0);
   void calibrateHK(double& est_camh, double& est_k1, double focal, double tilt);

   // Focal Length, Tilt Angle + (Distortion Coefficient) 추정하는 함수
   void calibrateFT(double& est_focal, double& est_tilt, double cam_h, double k1 = 0.0);
   void calibrateFTK(double& est_focal, double& est_tilt, double& est_k1, double cam_h);

   // Focal Length, Camera Height + (Distortion Coefficient) 추정하는 함수
   void calibrateFH(double& est_focal, double& est_camh, double tilt, double k1 = 0.0);
   void calibrateFHK(double& est_focal, double& est_camh, double& est_k1, double tilt);

   // Tilt Angle, Camera Height + (Distortion Coefficient) 추정하는 함수
   void calibrateTH(double& est_tilt, double& est_camh, double focal, double k1 = 0.0);
   void calibrateTHK(double& est_tilt, double& est_camh, double& est_k1, double focal);

   // Focal Length, Tilt Angle, Camera Height + (Distortion Coefficient) 추정하는 함수
   void calibrateFTH(double& est_focal, double& est_tilt, double& est_camh, double k1 = 0.0);
   void calibrateFTHK(double& est_focal, double& est_tilt, double& est_camh, double& est_k1);

   // 모델 정보를 변경할 수 있도록 만든 함수
   void setModels(vector<DPoint2D>& model_ts, vector<DPoint2D>& model_bs, vector<double>& model_hs);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: CalibObject
//
///////////////////////////////////////////////////////////////////////////////

 class CalibObject

{
   public:
      float    Length;     // Height in Pixel
      float    RealHeight; // Real Height in Meter
      FPoint2D TopPos;     // Top(Head) Position in Pixel
      FPoint2D CenterPos;  // Center Position in Pixel
      FPoint2D BottomPos;  // Bottom(Foot) Position in Pixel

   public:
      CalibObject (   );
};

typedef Array1D<CalibObject> COArray1D;
typedef Array2D<CalibObject> COArray2D;

///////////////////////////////////////////////////////////////////////////////
//
// Class: CameraAutoCalibration_ObjTrk
//
///////////////////////////////////////////////////////////////////////////////

#define CAC_RCODE_ERROR                 1
#define CAC_RCODE_TIMEOUT               2
#define CAC_RCODE_CALIBRATION_DONE      0
#define CAC_RCODE_LEARNING             -1
#define CAC_RCODE_ALREADY_CALIBRATED   -2

 class CameraAutoCalibration_ObjTrk
 
{
   protected:
      int   Flag_Initialized;
      int   Flag_Timeout;
      float LearningTime;
      float SamplingTime;

   protected:
      IArray1D  NumObjects;
      COArray2D Objects;
   
   // Input
   public:
      int   MinNumObjects;
      float RealObjHeight;
      float SamplingRate;
      float MinLearningTime;
      float MaxLearningTime;

   // Input
   public:
      float MinObjHeight;      // 참조 이미지 해상도 기준
      float MaxObjHeight;      // 참조 이미지 해상도 기준
      float MinObjNorSpeed;    //
      float MaxObjNorSpeed;    //
      float MinObjAspectRatio; //
      float MaxObjAspectRatio; //

   // Input
   public:
      ISize2D RefImgSize;

   // Output
   public:
      float FocalLength;
      float TiltAngle;
      float CamHeight;
   
   public:
      CameraAutoCalibration_ObjTrk (   );
      virtual ~CameraAutoCalibration_ObjTrk (   );

   protected:
      void _Init              (   );
      int  CalibrateCamera    (   );
      void RemoveNoisyObjects (   );
   
   public:
      virtual void Close (   );
   
   public:
      int Initialize    (ISize2D& ri_size);
      int IsInitialized (   );
      int Perform       (ObjectTracking& obj_tracker);
};

 inline int CameraAutoCalibration_ObjTrk::IsInitialized (   )

{
   return (Flag_Initialized);
}

}

#endif

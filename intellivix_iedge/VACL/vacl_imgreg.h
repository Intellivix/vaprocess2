#if !defined(__VACL_IMGREG_H)
#define __VACL_IMGREG_H
 
#include "vacl_opencv.h"

 namespace VACL

{

#define IR_NUM_ITERATIONS      30
#define IR_CONVERGENCE_ERROR   1E-7

///////////////////////////////////////////////////////////////////////////////
//
// Class: LIR_MSSD
//
///////////////////////////////////////////////////////////////////////////////
// 
// MSSD: Minimization of SSD(Sum of Squared Difference)
//

 class LIR_MSSD

{
   protected:
      int      IterationCount;
      int      NMeasurements;
      double   Error,Lambda;
      GImage   *RefImage,*SrcImage;
      Matrix   Mat_A,Vec_b,Vec_p;
      IPoint2D RI_Min,RI_Max;
      FArray1D ErrArray;
      FArray2D GxArray,GyArray;

   public:
      int    NumIterations;
      Matrix ParamVector;

   public:
      LIR_MSSD (   );
      virtual ~LIR_MSSD (   );

   protected:
      virtual void GetHomographySub (Matrix &vec_p,Matrix &mat_H) = 0;

   protected:
      virtual double CostFunction                      (Matrix &vec_p);
      virtual void   Derivative_Transformation         (Matrix &vec_p,int n,int sx,int sy,float fx,float fy,float &dx,float &dy);
      virtual void   GetJacobianMatrix                 (Matrix &vec_p,Matrix &mat_J);
      virtual void   Prepare_Derivative_Transformation (Matrix &vec_p) {   };
      virtual void   Prepare_Transformation            (Matrix &vec_p) {   };
      virtual void   SetRefImageRange                  (Matrix &vec_p);
      virtual void   Transformation                    (Matrix &vec_p,int sx,int sy,float &dx,float &dy) {   };

   public:
      virtual void Close (   );

   public:
      void   GetHomography (Matrix &mat_H);
      void   Initialize    (GImage &r_image,GImage &s_image,int n_params);
      double Perform       (int n_iterations = -1,double d_thrlsd = 1E-7,double e_thrsld = -1.0);
      double Update        (   );
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: LIR_MSSD_2DAffine
//
///////////////////////////////////////////////////////////////////////////////

 class LIR_MSSD_2DAffine : public LIR_MSSD

{
   protected:
      virtual double CostFunction      (Matrix &vec_p);
      virtual void   GetHomographySub  (Matrix &vec_p,Matrix &mat_H);
      virtual void   GetJacobianMatrix (Matrix &vec_p,Matrix &mat_J);

   public:
      void Initialize (GImage &r_image,GImage &s_image,Matrix &mat_H);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: LIR_MSSD_2DEuclidean
//
///////////////////////////////////////////////////////////////////////////////

 class LIR_MSSD_2DEuclidean : public LIR_MSSD
 
{
   protected:
      virtual double CostFunction      (Matrix &vec_p);
      virtual void   GetHomographySub  (Matrix &vec_p,Matrix &mat_H);
      virtual void   GetJacobianMatrix (Matrix &vec_p,Matrix &mat_J);
   
   public:
      void GetParameters (double &theta,double &tx,double &ty);
      void Initialize    (GImage &r_image,GImage &s_image,double theta,double tx,double ty);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: LIR_MSSD_2DIsoScale
//
///////////////////////////////////////////////////////////////////////////////

 class LIR_MSSD_2DIsoScale : public LIR_MSSD
 
{
   protected:
      virtual double CostFunction      (Matrix &vec_p);
      virtual void   GetHomographySub  (Matrix &vec_p,Matrix &mat_H);
      virtual void   GetJacobianMatrix (Matrix &vec_p,Matrix &mat_J);
      
   public:
      void GetParameters (double &scale,double &trans_x,double &trans_y);
      void Initialize    (GImage &r_image,GImage &s_image,double scale,double trans_x,double trans_y);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: LIR_MSSD_2DProjective
//
///////////////////////////////////////////////////////////////////////////////

 class LIR_MSSD_2DProjective : public LIR_MSSD
 
{
   protected:
      virtual double CostFunction      (Matrix &vec_p);
      virtual void   GetHomographySub  (Matrix &vec_p,Matrix &mat_H);
      virtual void   GetJacobianMatrix (Matrix &vec_p,Matrix &mat_J);
      
   public:
      void Initialize (GImage &r_image,GImage &s_image,Matrix &mat_H);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: LIR_MSSD_2DProjective_LD
//
///////////////////////////////////////////////////////////////////////////////
 
 class LIR_MSSD_2DProjective_LD : public LIR_MSSD

{
   protected:
      FPoint2D RefImageCenter;
      FPoint2D SrcImageCenter;
   
   protected:
      virtual void Derivative_Transformation (Matrix &vec_p,int n,int sx,int sy,float fx,float fy,float &dx,float &dy);
      virtual void GetHomographySub          (Matrix &vec_p,Matrix &mat_H);
      virtual void Transformation            (Matrix &vec_p,int sx,int sy,float &dx,float &dy);

   public:
      double GetLensDistortionCoefficient (   );
      void   Initialize                   (GImage &r_image,GImage &s_image,Matrix &mat_H,double kappa);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: LIR_MSSD_2DScale
//
///////////////////////////////////////////////////////////////////////////////

 class LIR_MSSD_2DScale : public LIR_MSSD
 
{
   protected:
      virtual double CostFunction      (Matrix &vec_p);
      virtual void   GetHomographySub  (Matrix &vec_p,Matrix &mat_H);
      virtual void   GetJacobianMatrix (Matrix &vec_p,Matrix &mat_J);

   public:
      void GetParameters (double &sx,double &sy,double &tx,double &ty);
      void Initialize    (GImage &r_image,GImage &s_image,double sx,double sy,double tx,double ty);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: LIR_MSSD_2DTranslational
//
///////////////////////////////////////////////////////////////////////////////

 class LIR_MSSD_2DTranslational : public LIR_MSSD
 
{
   protected:
      virtual double CostFunction      (Matrix &vec_p);
      virtual void   GetHomographySub  (Matrix &vec_p,Matrix &mat_H);
      virtual void   GetJacobianMatrix (Matrix &vec_p,Matrix &mat_J);
   
   public:
      void GetParameters (double &trans_x,double &trans_y);
      void Initialize    (GImage &r_image,GImage &s_image,double trans_x,double trans_y);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: LIR_MSSD_3DRotational_FXYZ
//
///////////////////////////////////////////////////////////////////////////////

 class LIR_MSSD_3DRotational_FXYZ : public LIR_MSSD

{
   protected:
      Matrix          Vec_dp;
      Matrix          Mat_H;
      Array1D<Matrix> Mats_Hd;
   
   protected:
      virtual void Derivative_Transformation         (Matrix &,int n,int sx,int sy,float fx,float fy,float &dx,float &dy);
      virtual void GetHomographySub                  (Matrix &vec_p,Matrix &mat_H);
      virtual void Prepare_Derivative_Transformation (Matrix &);
      virtual void Prepare_Transformation            (Matrix &);
      virtual void Transformation                    (Matrix &,int,int,float &,float &);

   public:
      void GetParameters (double &f_length,double &psi,double &theta,double &phi);
      void Initialize    (GImage &r_image,GImage &s_image,double f_length,double psi,double theta,double phi);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: LIR_MSSD_3DRotational_FXYZ_LD
//
///////////////////////////////////////////////////////////////////////////////

 class LIR_MSSD_3DRotational_FXYZ_LD : public LIR_MSSD

{
   protected:
      Matrix          Vec_dp;
      Matrix          Mat_H;
      FPoint2D        RefImageCenter;
      FPoint2D        SrcImageCenter;
      Array1D<Matrix> Mats_Hd;
   
   protected:
      virtual void Derivative_Transformation         (Matrix &vec_p,int n,int sx,int sy,float fx,float fy,float &dx,float &dy);
      virtual void GetHomographySub                  (Matrix &vec_p,Matrix &mat_H);
      virtual void Prepare_Derivative_Transformation (Matrix &vec_p);
      virtual void Prepare_Transformation            (Matrix &vec_p);
      virtual void Transformation                    (Matrix &vec_p,int sx,int sy,float &dx,float &dy);

   public:
      void GetParameters (double &f_length,double &psi,double &theta,double &phi,double &kappa);
      void Initialize    (GImage &r_image,GImage &s_image,double f_length,double psi,double theta,double phi,double kappa);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: LIR_MSSD_3DRotational_XY
//
///////////////////////////////////////////////////////////////////////////////

 class LIR_MSSD_3DRotational_XY : public LIR_MSSD

{
   protected:
      double          Angle_Phi;
      double          FocalLength;
      Matrix          Vec_dp;
      Matrix          Mat_H;
      Array1D<Matrix> Mats_Hd;
   
   protected:
      virtual void Derivative_Transformation         (Matrix &,int n,int sx,int sy,float fx,float fy,float &dx,float &dy);
      virtual void GetHomographySub                  (Matrix &vec_p,Matrix &mat_H);
      virtual void Prepare_Derivative_Transformation (Matrix &vec_p);
      virtual void Prepare_Transformation            (Matrix &vec_p);
      virtual void Transformation                    (Matrix &,int sx,int sy,float &dx,float &dy);

   public:
      void GetParameters (double &psi,double &theta);
      void Initialize    (GImage &r_image,GImage &s_image,double f_length,double psi,double theta,double phi);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: LIR_MSSD_3DRotational_XYZ
//
///////////////////////////////////////////////////////////////////////////////

 class LIR_MSSD_3DRotational_XYZ : public LIR_MSSD

{
   protected:
      double          FocalLength;
      Matrix          Vec_dp;
      Matrix          Mat_H;
      Array1D<Matrix> Mats_Hd;
   
   protected:
      virtual void Derivative_Transformation         (Matrix &,int n,int sx,int sy,float fx,float fy,float &dx,float &dy);
      virtual void GetHomographySub                  (Matrix &vec_p,Matrix &mat_H);
      virtual void Prepare_Derivative_Transformation (Matrix &vec_p);
      virtual void Prepare_Transformation            (Matrix &vec_p);
      virtual void Transformation                    (Matrix &,int sx,int sy,float &dx,float &dy);

   public:
      void GetParameters (double &psi,double &theta,double &phi);
      void Initialize    (GImage &r_image,GImage &s_image,double f_length,double psi,double theta,double phi);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: HierarchicalLIR_MSSD
//
///////////////////////////////////////////////////////////////////////////////

 class HierarchicalLIR_MSSD

{
   protected:
      int     NumIterations;
      int     Flag_Stop;
      float   Scale;
      GImage *RefImage;
      GImage *SrcImage;

   public:
      Matrix ParamVector;

   public:
      virtual ~HierarchicalLIR_MSSD (   ) {   };
   
   protected:
      void GetReducedImages (GImage &rr_image,GImage &rs_image);

   protected:
      virtual void PerformLIR (GImage &r_image,GImage &s_image,int n_iterations,double d_thrsld,double e_thrsld) = 0;
      virtual void ScaleDown  (   )                                                                              = 0;
      virtual void ScaleUp    (   )                                                                              = 0;

   protected:
      virtual void PreprocessImages (GImage &r_image,GImage &s_image);

   public:
      void Close      (   );
      void Initialize (GImage &r_image,GImage &s_image,int n_params,int coarsest_img_size);
      void Perform    (float scale_inc,int n_iterations1 = -1,int n_iterations2 = IR_NUM_ITERATIONS,double d_thrsld = IR_CONVERGENCE_ERROR,double e_thrsld = -1.0);
      int  Update     (float scale_inc,int n_iterations,double d_thrsld,double e_thrsld);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: HLIR_MSSD_2DAffine
//
///////////////////////////////////////////////////////////////////////////////

 class HLIR_MSSD_2DAffine : public HierarchicalLIR_MSSD

{
   protected:
      virtual void PerformLIR (GImage &r_image,GImage &s_image,int n_iterations,double d_thrsld,double e_thrsld);
      virtual void ScaleDown  (   ); 
      virtual void ScaleUp    (   );

   protected:
      void SetHomography (Matrix &mat_H);

   public:
      void GetHomography (Matrix &mat_H);
      void Initialize    (GImage &r_image,GImage &s_image,Matrix &mat_H,int coarsest_img_size);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: HLIR_MSSD_2DEuclidean
//
///////////////////////////////////////////////////////////////////////////////

 class HLIR_MSSD_2DEuclidean : public HierarchicalLIR_MSSD

{
   protected:
      virtual void PerformLIR (GImage &r_image,GImage &s_image,int n_iterations,double d_thrsld,double e_thrsld);
      virtual void ScaleDown  (   ); 
      virtual void ScaleUp    (   );

   protected:
      void SetParameters (double theta,double tx,double ty);
   
   public:
      void GetHomography (Matrix &mat_H);
      void GetParameters (double &theta,double &tx,double &ty);
      void Initialize    (GImage &r_image,GImage &s_image,double theta,double tx,double ty,int coarsest_img_size);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: HLIR_MSSD_2DProjective
//
///////////////////////////////////////////////////////////////////////////////

 class HLIR_MSSD_2DProjective : public HierarchicalLIR_MSSD

{
   protected:
      virtual void PerformLIR (GImage &r_image,GImage &s_image,int n_iterations,double d_thrsld,double e_thrsld);
      virtual void ScaleDown  (   ); 
      virtual void ScaleUp    (   );

   protected:
      void SetHomography (Matrix &mat_H);

   public:
      void GetHomography (Matrix &mat_H);
      void Initialize    (GImage &r_image,GImage &s_image,Matrix &mat_H,int coarsest_img_size);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: HLIR_MSSD_2DProjective_LD
//
///////////////////////////////////////////////////////////////////////////////

 class HLIR_MSSD_2DProjective_LD : public HierarchicalLIR_MSSD

{
   protected:
      virtual void PerformLIR       (GImage &r_image,GImage &s_image,int n_iterations,double d_thrsld,double e_thrsld);
      virtual void PreprocessImages (GImage &r_image,GImage &s_image) {   };
      virtual void ScaleDown        (   );
      virtual void ScaleUp          (   );

   protected:
      void SetHomography                (Matrix &mat_h);
      void SetLensDistortionCoefficient (double kappa);

   public:
      void   GetHomography                (Matrix &mat_H);
      double GetLensDistortionCoefficient (   );
      void   Initialize                   (GImage &r_image,GImage &s_image,Matrix &mat_H,double kappa,int coarsest_img_size);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: HLIR_MSSD_2DScale
//
///////////////////////////////////////////////////////////////////////////////

 class HLIR_MSSD_2DScale : public HierarchicalLIR_MSSD

{
   protected:
      virtual void PerformLIR (GImage &r_image,GImage &s_image,int n_iterations,double d_thrsld,double e_thrsld);
      virtual void ScaleDown  (   ); 
      virtual void ScaleUp    (   );

   protected:
      void SetParameters (double sx,double sy,double tx,double ty);
   
   public:
      void GetHomography (Matrix &mat_H);
      void GetParameters (double &sx,double &sy,double &tx,double &ty);
      void Initialize    (GImage &r_image,GImage &s_image,double sx,double sy,double tx,double ty,int coarsest_img_size);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: HLIR_MSSD_3DRotational_FXYZ
//
///////////////////////////////////////////////////////////////////////////////

 class HLIR_MSSD_3DRotational_FXYZ : public HierarchicalLIR_MSSD

{
   protected:
      virtual void PerformLIR (GImage &r_image,GImage &s_image,int n_iterations,double d_thrsld,double e_thrsld);
      virtual void ScaleDown  (   ); 
      virtual void ScaleUp    (   );

   protected:
      void SetParameters (double f_length,double psi,double theta,double phi);
   
   public:
      void GetHomography (Matrix &mat_H);
      void GetParameters (double &f_length,double &psi,double &theta,double &phi);
      void Initialize    (GImage &r_image,GImage &s_image,double f_length,double psi,double theta,double phi,int coarsest_img_size);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: HLIR_MSSD_3DRotational_FXYZ_LD
//
///////////////////////////////////////////////////////////////////////////////

 class HLIR_MSSD_3DRotational_FXYZ_LD : public HierarchicalLIR_MSSD

{
   protected:
      virtual void PerformLIR       (GImage &r_image,GImage &s_image,int n_iterations,double d_thrsld,double e_thrsld);
      virtual void PreprocessImages (GImage &r_image,GImage &s_image) {   };
      virtual void ScaleDown        (   ); 
      virtual void ScaleUp          (   );

   protected:
      void SetParameters (double f_length,double psi,double theta,double phi,double kappa);
   
   public:
      void GetHomography (Matrix &mat_H);
      void GetParameters (double &f_length,double &psi,double &theta,double &phi,double &kappa);
      void Initialize    (GImage &r_image,GImage &s_image,double f_length,double psi,double theta,double phi,double kappa,int coarsest_img_size);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: HLIR_MSSD_3DRotational_XY
//
///////////////////////////////////////////////////////////////////////////////

 class HLIR_MSSD_3DRotational_XY : public HierarchicalLIR_MSSD

{
   protected:
      double Angle_Phi;
      double FocalLength;
   
   protected:
      virtual void PerformLIR       (GImage &r_image,GImage &s_image,int n_iterations,double d_thrsld,double e_thrsld); 
      virtual void PreprocessImages (GImage &,GImage &) {   };
      virtual void ScaleDown        (   ); 
      virtual void ScaleUp          (   );

   protected:
      void SetParameters (double f_length,double psi,double theta,double phi);
   
   public:
      void GetHomography (Matrix &mat_H);
      void GetParameters (double &psi,double &theta);
      void Initialize    (GImage &r_image,GImage &s_image,double f_length,double psi,double theta,double phi,int coarsest_img_size);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: HLIR_MSSD_3DRotational_XYZ
//
///////////////////////////////////////////////////////////////////////////////

 class HLIR_MSSD_3DRotational_XYZ : public HierarchicalLIR_MSSD

{
   protected:
      double FocalLength;
   
   protected:
      virtual void PerformLIR (GImage &r_image,GImage &s_image,int n_iterations,double d_thrsld,double e_thrsld);
      virtual void ScaleDown  (   ); 
      virtual void ScaleUp    (   );

   protected:
      void SetParameters (double f_length,double psi,double theta,double phi);
   
   public:
      void GetHomography (Matrix &mat_H);
      void GetParameters (double &psi,double &theta,double &phi);
      void Initialize    (GImage &r_image,GImage &s_image,double f_length,double psi,double theta,double phi,int coarsest_img_size);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: HierarchicalTM
//
///////////////////////////////////////////////////////////////////////////////

 class HierarchicalTM

{
   protected:
      int      Flag_Stop;
      int      NumIterations;
      float    Scale;
      GImage*  SrcImage;
      GImage*  TplImage;
      FPoint2D LTOverlap;
      FPoint2D RBOverlap;
      FPoint2D MatchingPos;

   public:
      virtual ~HierarchicalTM (   ) {   };
   
   protected:
      void GetReducedImages (GImage &rs_image,GImage &rt_image);

   protected:
      virtual float PerformInitTemplateMatching (GImage &s_image,GImage &t_image,IPoint2D &d_pos)                              = 0;
      virtual float PerformTemplateMatching     (GImage &s_image,GImage &t_image,IPoint2D &s_pos,int win_size,IPoint2D &d_pos) = 0;

   public:
      void     Initialize          (GImage &s_image,GImage &t_image,int coarsest_img_size,FPoint2D &lt_overlap,FPoint2D &rb_overlap);
      FPoint2D GetMatchingPosition (   );
      float    Perform             (float scale_inc,int win_size,int n_iterations = -1);
      int      Update              (float scale_inc,int win_size,float &error);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: HTM_NCC1
//
///////////////////////////////////////////////////////////////////////////////

 class HTM_NCC1 : public HierarchicalTM

{
   protected:
      virtual float PerformInitTemplateMatching (GImage &s_image,GImage &t_image,IPoint2D &d_pos);
      virtual float PerformTemplateMatching     (GImage &s_image,GImage &t_image,IPoint2D &s_pos,int win_size,IPoint2D &d_pos);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: HTM_NCC2
//
///////////////////////////////////////////////////////////////////////////////

 class HTM_NCC2 : public HierarchicalTM

{
   protected:
      virtual float PerformInitTemplateMatching (GImage &s_image,GImage &t_image,IPoint2D &d_pos);
      virtual float PerformTemplateMatching     (GImage &s_image,GImage &t_image,IPoint2D &s_pos,int win_size,IPoint2D &d_pos);
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: PointPair
//
///////////////////////////////////////////////////////////////////////////////

 class IPoint2DPair
 
{
   public:
      IPoint2D Points[2];
      
   public:
      IPoint2D& operator [] (int i);
};

 inline IPoint2D& IPoint2DPair::operator [] (int i)
 
{
   return (Points[i]);
}

 typedef Array1D<IPoint2DPair> IP2DPArray1D;
 
///////////////////////////////////////////////////////////////////////////////
//
// Class: LocalImageRegistration_FPM
//
///////////////////////////////////////////////////////////////////////////////
//
// LocalImageRegistration_FPM computes the homography mat_H that satisfies
// s_image = mat_H * p_image using the feature point matching and RANSAC methods.
//
// p_image: Previously given image
// s_image: Currently given image
// FPM    : Feature Point Matching
//

 class LocalImageRegistration_FPM
 
{
   // Input Parameters
   public:
      int   NumSamples;
      int   CNMSWinSize;
      int   SearchRange;
      int   CornerWinSize;
      int   CornerPatchSize;
      int   MinEdgeStrength;
      float MinCornerStrength;
      float MatchingThreshold;
      float InlierSupportRange;
      
   // Output Parameters
   public:
      GImage       RefImage;
      GImage       SrcImage;
      IArray1D     Inliers;
      IP2DArray1D  RefPoints;
      IP2DArray1D  SrcPoints;
      IP2DPArray1D Correspondences;

   public:
      LocalImageRegistration_FPM (   );
      virtual ~LocalImageRegistration_FPM (   );

   protected:
      Matrix ComputeHomography           (IArray1D &pi_array,int flag_normalize);
      Matrix EstimateHomography          (   );
      int    FindMatchingPoint12         (int idx_p1,Matrix &mat_H ,IArray2D &s_map2,FArray2D &ms_array);
      int    FindMatchingPoint21         (int idx_p2,Matrix &mat_IH,IArray2D &s_map1,FArray2D &ms_array);
      int    GetInliers                  (Matrix &mat_H,IArray1D &inliers);
      float  GetNCC                      (GImage &s_image1,IPoint2D &p1,GImage &s_image2,IPoint2D &p2);
      void   GetPointList                (GImage &s_image,IP2DArray1D &p_array);
      void   GetPointMap                 (IP2DArray1D &s_array,IArray2D &d_map);
      float  GetRMSD                     (GImage &s_image1,IPoint2D &p1,GImage &s_image2,IPoint2D &p2);
      Matrix NormalizeImageCoordinates   (IP2DArray1D &s_points,FP2DArray1D &d_points);
      int    PerformFeaturePointMatching (Matrix &mat_H);
   
   public:
      virtual void Close (   );
   
   public:
      int Initialize (GImage &s_image);
      int GetInliers (IP2DPArray1D &inliers);
      int Perform    (GImage &s_image,Matrix &mat_H);
         // NOTICE: The matrix mat_H must be initialized with a proper value before call to the fuction Perform().
         //         If you don't have any prior information about geometric transformation between the previous frame
         //         and the current one, set mat_H to the 3x3 identity matrix. If you can estimate an approximated
         //         geometric transformation matrix (homography) from the panning/tilting angles of a PTZ camera, try
         //         to use the estimated matrix as an initial value of mat_H.
};

///////////////////////////////////////////////////////////////////////////////
//
// Class: OpenCV_PhaseCorrelation
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_OPENCV)

 class OpenCV_PhaseCorrelation
 
{
   protected:
      cv::Mat HanningWindow;
      
   public:
      ISize2D SrcImgSize;
   
   public:
      ~OpenCV_PhaseCorrelation (  );

   public:
      virtual void Close (   );
   
   public:
      void Initialize (ISize2D& si_size);
      // 주의: Perform() 함수 수행 후 s_array1와 s_array2의 내용이 바뀐다!
      int  Perform    (FArray2D &s_array1,FArray2D &s_array2,FPoint2D& d_shift);
};

#endif

///////////////////////////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////////////////////////

FPoint2D GetShift                      (Matrix& mat_H);
int      PerformImageRegistration_KLT  (GImage& s_image,GImage& r_image,Matrix& d_mat_H);
int      PerformImageRegistration_MSSD (GImage& s_image,GImage& r_image,Matrix& d_mat_H);
float    PerformTemplateMatching_NCC1  (GImage& s_image,GImage& t_image,IPoint2D& min_pos,IPoint2D& max_pos,IPoint2D& d_pos);
float    PerformTemplateMatching_NCC1  (GImage& s_image,GImage& t_image,FPoint2D& lt_overlap,FPoint2D& rb_overlap,IPoint2D& d_pos);
float    PerformTemplateMatching_NCC2  (GImage& s_image,GImage& t_image,IPoint2D& min_pos,IPoint2D& max_pos,IPoint2D& d_pos);
float    PerformTemplateMatching_NCC2  (GImage& s_image,GImage& t_image,FPoint2D& lt_overlap,FPoint2D& rb_overlap,IPoint2D& d_pos);
void     PrepareImageForLIR            (GImage& s_image);

}

#endif

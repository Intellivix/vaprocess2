#if !defined(__VACL_PROFILE_H)
#define __VACL_PROFILE_H

#include "vacl_define.h"

 namespace VACL

{

#define LOCAL_MINIMUM   1
#define LOCAL_MAXIMUM   2

///////////////////////////////////////////////////////////////////////////////
//
// Class: RectPulse
//
///////////////////////////////////////////////////////////////////////////////

 class RectPulse
 
{
   public:
      int Width;
      int StartPos;
      int EndPos;
      float Area;
};

typedef Array1D<RectPulse> RPArray;

void FindLocalMinMax      (FArray1D &s_array,BArray1D &d_array);
void FindLocalMinMax      (FArray1D &s_array,IArray1D &min_array,IArray1D &max_array);
void FindMaxArea          (RPArray &s_array,float &max_area,int &max_i);
void FindMaxHeight        (RPArray &s_array,float &max_height,int &max_i);
void FindMaxWidth         (RPArray &s_array,int &max_width,int &max_i);
void FindMinArea          (RPArray &s_array,float &min_area,int &min_i);
void FindMinHeight        (RPArray &s_array,float &min_height,int &min_i);
void FindMinMaxAreas      (RPArray &s_array,float &min_area,float &max_area,int &min_i,int &max_i);
void FindMinMaxHeights    (RPArray &s_array,float &min_height,float &max_height,int &min_i,int &max_i);
void FindMinMaxWidths     (RPArray &s_array,int &min_width,int &max_width,int &min_i,int &max_i);
void FindMinWidth         (RPArray &s_array,int &min_width,int &min_i);
int  GetRectPulses        (FArray1D &s_array,float thrsld,RPArray &d_array);
void HorizontalProjection (FArray2D &s_array,int sx,int length,FArray1D &d_array,int flag_clear = TRUE);
void HorizontalProjection (GImage &s_image,int sx,int length,FArray1D &d_array,int flag_clear = TRUE);
void HorizontalProjection (IArray2D& cr_map,IBox2D& s_rgn,int cr_no,FArray1D& d_array);
int  InvertRectPulses     (RPArray &s_array,int length,RPArray &d_array);
void SortByArea           (RPArray &s_array,int flag_ascending);
void SortByWidth          (RPArray &s_array,int flag_ascending);
void VerticalProjection   (FArray2D &s_array,int sy,int length,FArray1D &d_array,int flag_clear = TRUE);
void VerticalProjection   (GImage &s_image,int sy,int length,FArray1D &d_array,int flag_clear = TRUE);
void VerticalProjection   (IArray2D& cr_map,IBox2D& s_rgn,int cr_no,FArray1D& d_array);

}

#endif

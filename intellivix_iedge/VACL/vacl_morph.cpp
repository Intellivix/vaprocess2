#include "vacl_conrgn.h"
#include "vacl_morph.h"

#if defined(__LIB_OPENCV)
#include "vacl_opencv.h"
#endif

 namespace VACL

{


 void BinClosing (GImage& s_image,GImage& s_element,GImage& d_image)

{
   GImage t_image(s_image.Width,s_image.Height);
   BinDilation (s_image,s_element,t_image);
   BinErosion  (t_image,s_element,d_image);
}

 void BinDilation (GImage& s_image,GImage& s_element,GImage& d_image)

{
   int x1,y1,x2,y2,x3,y3;

   int ew  = s_element.Width;
   int eh  = s_element.Height;
   int ew2 = ew / 2;
   int eh2 = eh / 2;
   int sx1 = ew2;
   int sy1 = eh2;
   int ex1 = s_image.Width  - ew2;
   int ey1 = s_image.Height - eh2;
   if (eh2 < 1) sy1 = 1, ey1 = s_image.Height - 1;
   if (ew2 < 1) sx1 = 1, ex1 = s_image.Width  - 1;
   d_image = s_image;
   for (y1 = sy1; y1 < ey1; y1++) {
      int sy2 = y1 - eh2;
      byte *l_s_image  = s_image[y1];
      byte *l_s_image1 = s_image[y1 - 1];
      byte *l_s_image2 = s_image[y1 + 1];
      for (x1 = sx1; x1 < ex1; x1++) {
         if (l_s_image[x1]) {
            if (!(l_s_image[x1 - 1] && l_s_image[x1 + 1] && l_s_image1[x1] && l_s_image2[x1])) {
               int sx2 = x1 - ew2;
               for (y2 = sy2,y3 = 0; y3 < eh; y2++,y3++) {
                  byte *l_d_image   = d_image[y2];
                  byte *l_s_element = s_element[y3];
                  for (x2 = sx2,x3 = 0; x3 < ew; x2++,x3++)
                     l_d_image[x2] |= l_s_element[x3];
               }
            }
         }
      }
   }
}

 void BinErosion (GImage& s_image,GImage& s_element,GImage& d_image)

{
   int x1,y1,x2,y2,x3,y3;

   int ew  = s_element.Width;
   int eh  = s_element.Height;
   int ew2 = ew / 2;
   int eh2 = eh / 2;
   int sx1 = ew2;
   int sy1 = eh2;
   int ex1 = s_image.Width  - ew2;
   int ey1 = s_image.Height - eh2;
   if (eh2 < 1) sy1 = 1, ey1 = s_image.Height - 1;
   if (ew2 < 1) sx1 = 1, ex1 = s_image.Width  - 1;
   d_image = s_image;
   GImage ns_element(s_element.Width,s_element.Height);
   for (y1 = 0; y1 < s_element.Height; y1++) {
      byte *l_s_element  = s_element[y1];
      byte *l_ns_element = ns_element[y1];
      for (x1 = 0; x1 < s_element.Width; x1++) { 
         l_ns_element[x1] = ~l_s_element[x1];
      }
   }
   for (y1 = sy1; y1 < ey1; y1++) {
      int sy2 = y1 - eh2;
      byte *l_s_image  = s_image[y1];
      byte *l_s_image1 = s_image[y1 - 1];
      byte *l_s_image2 = s_image[y1 + 1];
      for (x1 = sx1; x1 < ex1; x1++) {
         if (l_s_image[x1]) {
            if (!(l_s_image[x1 - 1] && l_s_image[x1 + 1] && l_s_image1[x1] && l_s_image2[x1])) {
               int sx2 = x1 - ew2;
               for (y2 = sy2,y3 = 0; y3 < eh; y2++,y3++) {
                  byte *l_d_image    = d_image[y2];
                  byte *l_ns_element = ns_element[y3];
                  for (x2 = sx2,x3 = 0; x3 < ew; x2++,x3++)
                     l_d_image[x2] &= l_ns_element[x3];
               }
            }
         }
      }
   }
   d_image.Set (0,0,ew2,d_image.Height,PG_BLACK);
   d_image.Set (d_image.Width - ew2,0,ew2,d_image.Height,PG_BLACK);
   d_image.Set (0,0,d_image.Width,eh2,PG_BLACK);
   d_image.Set (0,d_image.Height - eh2,d_image.Width,eh2,PG_BLACK);
}

 void BinHoleFilling (int min_area,int max_area,GImage& d_image)

{
   int i,x,y;
   
   GImage t_image = ~d_image;
   IArray2D cr_map(t_image.Width,t_image.Height);
   int n_crs = LabelCRs (t_image,cr_map);
   if (!n_crs) return;
   CRArray1D cr_array(n_crs + 1);
   GetCRInfo (cr_map,cr_array);
   for (i = 1; i < cr_array.Length; i++) {
      ConnectedRegion& cr = cr_array[i];
      if (min_area <= cr.Area && cr.Area <= max_area) {
         int ex = cr.X + cr.Width;
         int ey = cr.Y + cr.Height;
         for (y = cr.Y; y < ey; y++) {
            int*  l_cr_map  = cr_map[y];
            byte* l_d_image = d_image[y];
            for (x = cr.X; x < ex; x++) {
               if (l_cr_map[x] == i) l_d_image[x] = PG_WHITE;
            }
         }
      }
   }
}

 void BinHRLFiltering (GImage& s_image,int gap_size,GImage& d_image)

{
   int x,x2,y;

   d_image = s_image;
   if (!gap_size) return;
   int ex = s_image.Width - 1;
   for (y = 0; y < s_image.Height; y++) {
      byte* l_s_image = s_image[y];
      byte* l_d_image = d_image[y];
      for (x = 0; x < ex; x++) {
         int x1 = x + 1;
         if (l_s_image[x] && !l_s_image[x1]) {
            int ex2 = x1 + gap_size;
            if (ex2 > ex) ex2 = ex;
            int last = -1;
            for (x2 = x1; x2 <= ex2; x2++) if (l_s_image[x2]) last = x2;
            if (last != -1) {
               for (x2 = x1; x2 < last; x2++) l_d_image[x2] = PG_WHITE;
               x = last - 1;
            }
         }
      }
   }
}

 void BinOpening (GImage& s_image,GImage& s_element,GImage& d_image)

{
   GImage t_image(s_image.Width,s_image.Height);
   BinErosion  (s_image,s_element,t_image);
   BinDilation (t_image,s_element,d_image);
}

 void BinVRLFiltering (GImage& s_image,int gap_size,GImage& d_image)

{
   int x,y,y2;
   
   d_image = s_image;
   if (!gap_size) return;
   int ei = s_image.Height - 1;
   for (x = 0; x < s_image.Width; x++) {
      for (y = 0; y < ei; y++) {
         int y1 = y + 1;
         if (s_image[y][x] && !s_image[y1][x]) {
            int ey2 = y1 + gap_size;
            if (ey2 > ei) ey2 = ei;
            int last = -1;
            for (y2 = y1; y2 <= ey2; y2++) if (s_image[y2][x]) last = y2;
            if (last != -1) {
               for (y2 = y1; y2 < last; y2++) d_image[y2][x] = PG_WHITE;
               y = last - 1;
            }
         }
      }
   }
}

 void CRDilation (IArray2D& s_cr_map,GImage &s_element,IArray2D& d_cr_map)

{
   int x1,y1,x2,y2,x3,y3;

   int ew  = s_element.Width;
   int eh  = s_element.Height;
   int ew2 = ew / 2;
   int eh2 = eh / 2;
   int sx1 = ew2;
   int sy1 = eh2;
   int ex1 = s_cr_map.Width  - ew2;
   int ey1 = s_cr_map.Height - eh2;
   if (eh2 < 1) sy1 = 1, ey1 = s_cr_map.Height - 1;
   if (ew2 < 1) sx1 = 1, ex1 = s_cr_map.Width  - 1;
   d_cr_map = s_cr_map;
   for (y1 = sy1; y1 < ey1; y1++) {
      int sy2 = y1 - eh2;
      int *l_s_cr_map  = s_cr_map[y1];
      int *l_s_cr_map1 = s_cr_map[y1 - 1];
      int *l_s_cr_map2 = s_cr_map[y1 + 1];
      for (x1 = sx1; x1 < ex1; x1++) {
         if (l_s_cr_map[x1]) {
            int cr_no = l_s_cr_map[x1];
            if (!(l_s_cr_map[x1 - 1] == cr_no && l_s_cr_map[x1 + 1] == cr_no && l_s_cr_map1[x1] && l_s_cr_map2[x1] == cr_no)) {
               int sx2 = x1 - ew2;
               for (y2 = sy2,y3 = 0; y3 < eh; y2++,y3++) {
                  int*  l_d_cr_map  = d_cr_map[y2];
                  byte* l_s_element = s_element[y3];
                  for (x2 = sx2,x3 = 0; x3 < ew; x2++,x3++) {
                     if (l_s_element[x3] && !l_d_cr_map[x2]) l_d_cr_map[x2] = cr_no;
                  }
               }
            }
         }
      }
   }
}

 void CRErosion (IArray2D& s_cr_map,GImage &s_element,IArray2D& d_cr_map)

{
   int x1,y1,x2,y2,x3,y3;

   int ew  = s_element.Width;
   int eh  = s_element.Height;
   int ew2 = ew / 2;
   int eh2 = eh / 2;
   int sx1 = ew2;
   int sy1 = eh2;
   int ex1 = s_cr_map.Width  - ew2;
   int ey1 = s_cr_map.Height - eh2;
   if (eh2 < 1) sy1 = 1, ey1 = s_cr_map.Height - 1;
   if (ew2 < 1) sx1 = 1, ex1 = s_cr_map.Width  - 1;
   d_cr_map = s_cr_map;
   for (y1 = sy1; y1 < ey1; y1++) {
      int sy2 = y1 - eh2;
      int* l_s_cr_map  = s_cr_map[y1];
      int* l_s_cr_map1 = s_cr_map[y1 - 1];
      int* l_s_cr_map2 = s_cr_map[y1 + 1];
      for (x1 = sx1; x1 < ex1; x1++) {
         if (l_s_cr_map[x1]) {
            int cr_no = l_s_cr_map[x1];
            if (!(l_s_cr_map[x1 - 1] == cr_no && l_s_cr_map[x1 + 1] == cr_no && l_s_cr_map1[x1] == cr_no && l_s_cr_map2[x1] == cr_no)) {
               int sx2 = x1 - ew2;
               for (y2 = sy2,y3 = 0; y3 < eh; y2++,y3++) {
                  int*  l_s_cr_map  = s_cr_map[y2];
                  int*  l_d_cr_map  = d_cr_map[y2];
                  byte* l_s_element = s_element[y3];
                  for (x2 = sx2,x3 = 0; x3 < ew; x2++,x3++) {
                     if (l_s_element[x3] && l_s_cr_map[x2] == cr_no) l_d_cr_map[x2] = 0;
                  }
               }
            }
         }
      }
   }
   d_cr_map.Set (0,0,ew2,d_cr_map.Height,0);
   d_cr_map.Set (d_cr_map.Width - ew2,0,ew2,d_cr_map.Height,0);
   d_cr_map.Set (0,0,d_cr_map.Width,eh2,0);
   d_cr_map.Set (0,d_cr_map.Height - eh2,d_cr_map.Width,eh2,0);
}

 void CRHoleFilling (int min_area,int max_area,IArray2D& d_cr_map)

{
   int i,x,y;
   
   // d_cr_map의 반전 영상을 얻는다.
   GImage t_image(d_cr_map.Width,d_cr_map.Height);
   t_image.Clear (   );
   for (y = 0; y < d_cr_map.Height; y++) {
      int*  l_d_cr_map = d_cr_map[y];
      byte* l_t_image  = t_image[y];
      for (x = 0; x < d_cr_map.Width; x++)
         if (!l_d_cr_map[x]) l_t_image[x] = PG_WHITE;
   }
   // 반전 영상의 CRL을 수행한다.
   IArray2D cr_map(t_image.Width,t_image.Height);
   int n_crs = LabelCRs (t_image,cr_map);
   if (!n_crs) return;
   CRArray1D cr_array(n_crs + 1);
   GetCRInfo (cr_map,cr_array);
   // 반전 영상의 CR은 d_cr_map의 Hole에 해당한다.
   // 면적 조건을 만족하는 Hole들을 채운다.
   for (i = 1; i < cr_array.Length; i++) {
      ConnectedRegion& cr = cr_array[i];
      if (min_area <= cr.Area && cr.Area <= max_area) {
         // 현재 Hole을 포함하는 d_cr_map 상의 CR의 번호를 얻는다.
         int cr_no = 0;
         int cx = (int)(cr.CX + 0.5f);
         int cy = (int)(cr.CY + 0.5f);
         int* l_d_cr_map = d_cr_map[cy];
         // Hole의 무게 중심에서 오른쪽 방향으로 스캔한다.
         for (x = cx; x < d_cr_map.Width; x++) {
            if (l_d_cr_map[x]) {
               cr_no = l_d_cr_map[x];
               break;
            }
         }
         // Hole의 무게 중심에서 왼쪽 방향으로 스캔한다.
         if (!cr_no) {
            for (x = cx; x > 0; x--) {
               if (l_d_cr_map[x]) {
                  cr_no = l_d_cr_map[x];
                  break;
               }
            }
         }
         // Hole의 무게 중심에서 아래쪽 방향으로 스캔한다.
         if (!cr_no) {
            for (y = cy; y < d_cr_map.Height; y++) {
               if (d_cr_map[y][cx]) {
                  cr_no = d_cr_map[y][cx];
                  break;
               }
            }
         }
         // Hole의 무게 중심에서 위쪽 방향으로 스캔한다.
         if (!cr_no) {
            for (y = cy; y > 0; y--) {
               if (d_cr_map[y][cx]) {
                  cr_no = d_cr_map[y][cx];
                  break;
               }
            }
         }
         // d_cr_map의 Hole을 채운다.
         if (cr_no) {
            int ex = cr.X + cr.Width;
            int ey = cr.Y + cr.Height;
            for (y = cr.Y; y < ey; y++) {
               int* l_cr_map   = cr_map[y];
               int* l_d_cr_map = d_cr_map[y];
               for (x = cr.X; x < ex; x++)
                  if (l_cr_map[x] == i) l_d_cr_map[x] = cr_no;
            }
         }
      }
   }
}

 void CRHRLFiltering (IArray2D& s_cr_map,int gap_size,IArray2D& d_cr_map)

{
   int x,x2,y;

   d_cr_map = s_cr_map;
   if (!gap_size) return;
   int ex = s_cr_map.Width - 1;
   for (y = 0; y < s_cr_map.Height; y++) {
      int* l_s_cr_map = s_cr_map[y];
      int* l_d_cr_map = d_cr_map[y];
      for (x = 0; x < ex; x++) {
         int x1 = x + 1;
         if (l_s_cr_map[x] && !l_s_cr_map[x1]) {
            int cr_no = l_s_cr_map[x];
            int ex2 = x1 + gap_size;
            if (ex2 > ex) ex2 = ex;
            int last = -1;
            for (x2 = x1; x2 <= ex2; x2++) if (l_s_cr_map[x2] == cr_no) last = x2;
            if (last != -1) {
               for (x2 = x1; x2 < last; x2++) l_d_cr_map[x2] = cr_no;
               x = last - 1;
            }
         }
      }
   }
}

 void CRVRLFiltering (IArray2D& s_cr_map,int gap_size,IArray2D& d_cr_map)

{
   int x,y,y2;
   
   d_cr_map = s_cr_map;
   if (!gap_size) return;
   int ei = s_cr_map.Height - 1;
   for (x = 0; x < s_cr_map.Width; x++) {
      for (y = 0; y < ei; y++) {
         int y1 = y + 1;
         if (s_cr_map[y][x] && !s_cr_map[y1][x]) {
            int cr_no = s_cr_map[y][x];
            int ey2 = y1 + gap_size;
            if (ey2 > ei) ey2 = ei;
            int last = -1;
            for (y2 = y1; y2 <= ey2; y2++) if (s_cr_map[y2][x] == cr_no) last = y2;
            if (last != -1) {
               for (y2 = y1; y2 < last; y2++) d_cr_map[y2][x] = cr_no;
               y = last - 1;
            }
         }
      }
   }
}

 void GetBinStrElement (GImage& s_element)

{
   s_element.Set (0,0,s_element.Width,s_element.Height,PG_WHITE);
   int ex = s_element.Width  - 1;
   int ey = s_element.Height - 1;
   if (s_element.Width <= 1 || s_element.Height <= 1) return;
   s_element[0 ][0 ] = PG_BLACK;
   s_element[0 ][ex] = PG_BLACK;
   s_element[ey][ex] = PG_BLACK;
   s_element[ey][0 ] = PG_BLACK;
}

 void GetBinStrElement (int se_radius,GImage& s_element)

{
   int se_size = 2 * se_radius + 1;
   s_element.Create (se_size,se_size);
   s_element.Clear  (   );
   DrawFilledCircle (se_radius,se_radius,se_size,(byte)PG_WHITE,s_element);
}

 void PerformBinClosing (GImage& s_image,int se_radius,GImage& d_image,int rd)
 
{
   GImage s_element1;
   GetBinStrElement (se_radius,s_element1);
   se_radius -= rd;
   if (se_radius < 0) se_radius = 0;
   GImage s_element2;
   GetBinStrElement (se_radius,s_element2);
   GImage t_image(s_image.Width,s_image.Height);
   BinDilation (s_image,s_element1,t_image);
   BinErosion  (t_image,s_element2,d_image);
}

 void PerformBinClosing2 (GImage& s_image,int se_radius,GImage& d_image,int rd)

{
   se_radius = (int)(se_radius * d_image.Height / 240.0f + 0.5f);
   if (se_radius < 1) se_radius = 1;
   PerformBinClosing (s_image,se_radius,d_image,rd);
}

 void PerformBinDilation (GImage& s_image,int se_radius,GImage& d_image)
 
{
   GImage s_element;
   GetBinStrElement (se_radius,s_element);
   BinDilation (s_image,s_element,d_image);
}

 void PerformBinErosion (GImage& s_image,int se_radius,GImage& d_image)
 
{
   GImage s_element;
   GetBinStrElement (se_radius,s_element);
   BinErosion (s_image,s_element,d_image);
}

 void PerformBinOpening (GImage& s_image,int se_radius,GImage& d_image)
 
{
   GImage s_element;
   GetBinStrElement (se_radius,s_element);
   GImage t_image(s_image.Width,s_image.Height);
   BinErosion  (s_image,s_element,t_image);
   BinDilation (t_image,s_element,d_image);
}

 void PerformCRClosing (IArray2D& s_cr_map,int se_radius,IArray2D& d_cr_map,int rd)
 
{
   GImage s_element1;
   GetBinStrElement (se_radius,s_element1);
   se_radius -= rd;
   if (se_radius < 0) se_radius = 0;
   GImage s_element2;
   GetBinStrElement (se_radius,s_element2);
   IArray2D t_cr_map(s_cr_map.Width,s_cr_map.Height);
   CRDilation (s_cr_map,s_element1,t_cr_map);
   CRErosion  (t_cr_map,s_element2,d_cr_map);
}

 void PerformCRDilation (IArray2D& s_cr_map,int se_radius,IArray2D& d_cr_map)
 
{
   GImage s_element;
   GetBinStrElement (se_radius,s_element);
   CRDilation (s_cr_map,s_element,d_cr_map);
}

 void PerformCRErosion (IArray2D& s_cr_map,int se_radius,IArray2D& d_cr_map)
 
{
   GImage s_element;
   GetBinStrElement (se_radius,s_element);
   CRErosion (s_cr_map,s_element,d_cr_map);
}

 void PerformCROpening (IArray2D& s_cr_map,int se_radius,IArray2D& d_cr_map)
 
{
   GImage s_element;
   GetBinStrElement (se_radius,s_element);
   IArray2D t_cr_map(s_cr_map.Width,s_cr_map.Height);
   CRErosion  (s_cr_map,s_element,t_cr_map);
   CRDilation (t_cr_map,s_element,d_cr_map);
}

}

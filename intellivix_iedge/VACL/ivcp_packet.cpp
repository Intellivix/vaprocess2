#include "ivcp_packet.h"

 namespace IVCP 
 
{

////////////////////////////////////////////////////////////////////////////////
//
// CIVCPPacketHeader
//
////////////////////////////////////////////////////////////////////////////////

CIVCPPacket::CIVCPPacketHeader::CIVCPPacketHeader ()
{
   memset(m_strMessage, 0, sizeof(m_strMessage));
   m_nOptions = 0;
   m_nDataLen = 0;
	m_nBodyLen = 0;
}

////////////////////////////////////////////////////////////////////////////////
//
// CIVCPPacket
//
////////////////////////////////////////////////////////////////////////////////

CIVCPPacket::CIVCPPacket ()
{
	m_pBody = NULL;
}

CIVCPPacket::CIVCPPacket (const char *szMessage)
{
	strcpy(m_Header.m_strMessage, szMessage);
	m_pBody = NULL;
}

CIVCPPacket::~CIVCPPacket ()
{
   FreeBody();
}

CIVCPPacket& CIVCPPacket::operator= (CIVCPPacket& from)
{
   strcpy(m_Header.m_strMessage, from.m_Header.m_strMessage);
   m_Header.m_nOptions = from.m_Header.m_nOptions; // Packet Options
   m_Header.m_nDataLen = from.m_Header.m_nDataLen; // Data length of IVCP.
   m_Header.m_nBodyLen = from.m_Header.m_nBodyLen; // Body length of IVCP.
   AllocBody();
   if (m_pBody && from.m_pBody)
   {
      memcpy(m_pBody, from.m_pBody, m_Header.m_nBodyLen);
   }
   else
   {
      m_Header.m_nDataLen = 0;
      m_Header.m_nBodyLen = 0;
   }
   return *this;
}

BOOL CIVCPPacket::AllocBody ()
{
   FreeBody();
   if (m_Header.m_nBodyLen > 0)
   {
      m_pBody = (char*)malloc(m_Header.m_nBodyLen);
      if (!m_pBody) return FALSE;
   }
   return TRUE;
}

void CIVCPPacket::FreeBody ()
{
	if (m_pBody) 
   {
		free(m_pBody);
		m_pBody = NULL;
	}
}

inline BOOL IsDigit (char ch)
{
   if ((ch >= '0') && (ch <= '9')) return TRUE;
   return FALSE;
}

BOOL CIVCPPacket::ParseHeader (const char* resString, int nLen)
{
   if (nLen < 8) return FALSE; // 최소 크기

   BOOL bDataOK = FALSE;
   int i = 0;

   // 헤더 타입: "C_SYSINFO 0 0 1024\n" (패킷ID, 옵션, 데이터크기, 바디크기)

   while ((i < nLen) && (resString[i] != ' ')) i++;
   if (resString[i] == ' ')
   {
      // Read Message
      int nMsgLen = i;
      if (i >= 20) nMsgLen = 20-1;
      strncpy(m_Header.m_strMessage, resString, nMsgLen);
      m_Header.m_strMessage[nMsgLen] = '\0';

      // Read Option
      while ((i < nLen) && (resString[i] == ' ')) i++;
      if (i < nLen)
      {
         if (IsDigit(resString[i])) // isdigit((int)resString[i]))
         {
            m_Header.m_nOptions = atoi(&(resString[i]));
         }
      }

      while ((i < nLen) && (resString[i] != ' ')) i++;
      if (resString[i] == ' ')
      {
         // Read Data Length
         while ((i < nLen) && (resString[i] == ' ')) i++;
         if (i < nLen)
         {
            if (IsDigit(resString[i])) // (isdigit((int)resString[i]))
            {
               m_Header.m_nDataLen = atoi(&(resString[i]));
            }
         }

         while ((i < nLen) && (resString[i] != ' ')) i++;
         if (resString[i] == ' ')
         {
            // Read Body Length
            while ((i < nLen) && (resString[i] == ' ')) i++;
            if (i < nLen)
            {
               if (IsDigit(resString[i])) // (isdigit((int)resString[i]))
               {
                  m_Header.m_nBodyLen = atoi(&(resString[i]));
                  bDataOK = TRUE;
               }
            }
         }
      }
   }
   return bDataOK;
}

}

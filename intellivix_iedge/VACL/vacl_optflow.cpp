#include "vacl_ipp.h"
#include "vacl_optflow.h"
#include "vacl_warping.h"

#if defined(__DEBUG_OFF)
#include "Win32CL/Win32CL.h"
extern int _DVX_OFF,_DVY_OFF;
extern CImageView* _DebugView_OFF;
#endif

 namespace VACL

{

///////////////////////////////////////////////////////////////////////////////
//
// Class: DenseOpticalFlow
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__LIB_OPENCV)

 DenseOpticalFlow::DenseOpticalFlow (   )

{
   FlowLength   = 2.0f;
   NumLevels    = 2;
   WinSize      = 7;
   PyramidScale = 0.5f;
   _Init (   );
}

 DenseOpticalFlow::~DenseOpticalFlow (   )

{
   Close (   );
}

 void DenseOpticalFlow::_Init (   )

{
   Width  = 0;
   Height = 0;
}

 void DenseOpticalFlow::Close (   )

{
   PrvImage.Delete (   );
   FlowHistory.Delete (   );
   _Init (   );
}

 FlowField* DenseOpticalFlow::GetFlowField (GImage& s_image)

{
   if (!Width || !Height) return (NULL);
   if (!PrvImage) {
      PrvImage.Create (Width,Height);
      Resize_2 (s_image,PrvImage);
      return (NULL);
   }
   GImage* p_s_image = &s_image;
   GImage  s_image2;
   if (Width != s_image.Width || Height != s_image.Height) {
      s_image2.Create (Width,Height);
      Resize_2 (s_image,s_image2);
      p_s_image = &s_image2;
   }
   FlowField* flow_field = new FlowField;
   flow_field->Field.Create (Width,Height);
   OpenCV_GetDenseOpticalFlow (*p_s_image,PrvImage,flow_field->Field,PyramidScale,NumLevels,WinSize,1,5,1.1f);
   FlowHistory.Add (flow_field,LL_FIRST);
   int n = FlowHistory.GetNumItems (   );
   int n_thrsld = (int)(FlowLength * FrameRate);
   if (n > n_thrsld) {
      flow_field = FlowHistory.Last (   );
      FlowHistory.Remove (flow_field);
      delete flow_field;
   }
   PrvImage = *p_s_image;
   #if defined(__DEBUG_OFF)
   int x,y;
   int step = 10;
   _DVX_OFF = _DVY_OFF = 0;
   _DebugView_OFF->Clear (   );
   _DebugView_OFF->DrawImage (*p_s_image,_DVX_OFF,_DVY_OFF);
   flow_field = FlowHistory.First (   );
   while (flow_field != NULL) {
      FP2DArray2D& field = flow_field->Field;
      for (y = 0; y < field.Height; y += step) {
         FPoint2D* l_field = field[y];
         for (x = 0; x < field.Width; x += step) {
            FPoint2D& p = l_field[x];
            _DebugView_OFF->DrawLine (x,y,x + p.X,y + p.Y,PC_RED);
         }
      }
      flow_field = FlowHistory.Next (flow_field);
   }
   _DebugView_OFF->Invalidate (   );
   #endif
   return (flow_field);
}

 void DenseOpticalFlow::Initialize (int width,int height,float frm_rate)

{
   Close (   );
   Width     = width;
   Height    = height;
   FrameRate = frm_rate;
}

#endif

}

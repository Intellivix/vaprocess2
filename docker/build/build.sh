#!/bin/bash

sudo cp -ar /usr/local/lib .
cp ../../intellivix_iedge/build-release-linux-x86_64/Intellivix .
cp ../../etc/Configurations/SystemConfig.xml .

docker build -t intellivix:latest .

rm -f SystemConfig.xml
rm -f Intellivix
sudo rm -rf lib



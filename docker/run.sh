#!/bin/bash

CAM_ID=camid
if [ ! -d $CAM_ID ]
then
	echo " #error: no $CAM_ID dir."
	exit 1
fi


docker run -v $PWD/camid:/root/$CAM_ID -it --rm --name it intellivix:latest Intellivix -i $CAM_ID $@


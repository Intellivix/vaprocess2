#!/bin/sh -v

# need root.
sudo -v

# TODO: yum install pre-required-libs.
sudo yum -y install cmake3

cd casablanca
git reset --hard
git clean -fdx

cd Release
mkdir build
cd build
cmake3 -DCMAKE_BUILD_TYPE=Release -DWERROR=OFF ..
make
sudo make install

git clean -fdx


# Oracle linux 7.4 package install

sudo rpm -ivh keyutils-libs-devel-1.5.8-3.el7.x86_64.rpm
sudo rpm -ivh libcom_err-devel-1.42.9-10.0.1.el7.x86_64.rpm
sudo rpm -ivh libkadm5-1.15.1-8.el7.x86_64.rpm
sudo rpm -ivh libsepol-devel-2.5-6.el7.x86_64.rpm
sudo rpm -ivh pkgconfig-0.27.1-4.el7.x86_64.rpm
sudo rpm -ivh pcre-devel-8.32-17.el7.x86_64.rpm
sudo rpm -ivh libselinux-devel-2.5-11.el7.x86_64.rpm
sudo rpm -ivh libverto-devel-0.2.5-4.el7.x86_64.rpm
sudo rpm -ivh krb5-devel-1.15.1-8.el7.x86_64.rpm
sudo rpm -ivh zlib-devel-1.2.7-17.el7.x86_64.rpm
sudo rpm -ivh openssl-devel-1.0.2k-8.0.1.el7.x86_64.rpm
sudo rpm -ivh openssl-1.0.2k-8.0.1.el7.x86_64.rpm

sudo rpm -ivh libjpeg-turbo-devel-1.2.90-5.el7.x86_64.rpm
sudo rpm -ivh libpng-devel-1.5.13-7.el7_2.x86_64.rpm

sudo rpm -ivh pugixml-1.8-1.el7.x86_64.rpm
sudo rpm -ivh pugixml-devel-1.8-1.el7.x86_64.rpm

sudo tar -xzvf lib.tar.gz -C /usr/local/lib
#!/bin/sh -v

sudo apt-get install g++ git make zlib1g-dev libboost-all-dev libssl-dev cmake

if [ $? -ne 0 ]
then
  echo "# Error..."
  echo "# Are you using Debian OS?"
  exit 1
fi

cd casablanca/Release
git checkout -b v2.10.2
git clean -fdx

mkdir build
cd build
cmake ..
make clean
make
sudo make install

git clean -fdx


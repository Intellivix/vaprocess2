#!/bin/sh -v

# need root.
sudo -v

sudo yum -y install libicu-devel zlib-devel python-devel bzip2-devel texinfo

cd boost_1_58_0

git clean -fdx

./bootstrap.sh
./b2 -j$(nproc) variant=release define=_GLIBCXX_USE_CXX11_ABI=1
sudo ./b2 install

git clean -fdx



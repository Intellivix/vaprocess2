# openblas install (for mini-caffe 3rdparty)
cd openblas_rpm
./openblas_install_rhel.sh
cd ..

# mini-caffe submoudle download
cd mini-caffe
git reset --hard
git clean -fdx
git submodule update --init
git checkout -b v0.4.0 v0.4.0
cd ..

# mini-caffe 3rdparty build
cd mini-caffe/3rdparty/src/protobuf
./autogen.sh
./configure
make -j4
sudo make install
cd ../../../../

# mini-caffe build
cd mini-caffe
./generatepb.sh
rm -rf build
mkdir build
cd build
CXXFLAGS=-isystem\ /usr/include/openblas cmake .. -DCMAKE_BUILD_TYPE=Release
make -j4
sudo cp libcaffe.so /usr/local/lib
cd ../
sudo cp -r include/caffe /usr/local/include

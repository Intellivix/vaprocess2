#!/bin/bash -v

# need root
sudo -v

# TODO: yum install pre-required-libs.
sudo yum -y install nasm

if [ $? -ne 0 ]
then
  echo "# Error... Are you using RHEL OS?"
  exit 1
fi

cd FFmpeg

git reset --hard
git clean -fdx

./configure --enable-shared --disable-static

make

sudo make install

git clean -fdx


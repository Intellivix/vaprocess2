#!/bin/bash -v

C=*
C=$C,-android*
C=$C,-hicpp*
C=$C,-cppcoreguidelines*
C=$C,-clang-analyzer-alpha*
C=$C,-llvm-include-order
C=$C,-llvm-header-guard
C=$C,-modernize-replace-auto-ptr
C=$C,-modernize-use-auto
C=$C,-modernize-use-bool-literals

clang-tidy \
 -checks=$C \
 -header-filter='*' \
 -fix -fix-errors $1 -- \
 -std=c++11 \
 -I. -I../../third_party/Plog -I../../third_party/SimpleWeb -I../../third_party/RapidJSON \
 -I../BCCL -I../VACL -I../Common -I../VCM -I../IVCP -I../Live555 \
 -I/usr/include/c++/5 \
 -D__DEV__ -D__CPP11 -D__LIB_FFMPEG -D__LIB_OPENCV -D__LIB_JPEG -D__LIB_PNG -D__LIB_PLOG


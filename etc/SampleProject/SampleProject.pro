TARGET = SampleProject
TEMPLATE = app

##########################################################################
message("qmake === [$$TEMPLATE] $$TARGET ===")

# remove qt's dependencies.
QT -= gui core
CONFIG -= app_bundle qt qpa import_plugins import_qpa_plugin

# choose lib static or shared.
equals(TEMPLATE, lib):CONFIG += staticlib # to enable static lib.

# In shared lib case, plugin makes *.so instead of soname, *.so.1.2.3
equals(TEMPLATE, lib):CONFIG(shared, shared|staticlib):CONFIG += unversioned_libname unversioned_soname

# add compile support from QT.
CONFIG += c++11 link_pkgconfig thread

# smart build location. 'Shaddow build' option in QtCreator's project MUST BE TURNED OFF.
CONFIG(debug, debug|release):  DESTDIR = "$$PWD/build-debug-linux-x86_64"
else:CONFIG(force_debug_info): DESTDIR = "$$PWD/build-profile-linux-x86_64"
else:                          DESTDIR = "$$PWD/build-release-linux-x86_64"

# smart build location. 'Shaddow build' option in QtCreator's project MUST BE TURNED OFF.
OBJECTS_DIR     = $$DESTDIR/.output/$$TARGET
MOC_DIR         = $$DESTDIR/.output/$$TARGET
UI_DIR          = $$DESTDIR/.output/$$TARGET
RCC_DIR         = $$DESTDIR/.output/$$TARGET

########################################################################


#DEFINES += YOURS_DEFINES

#INCLUDEPATH += YOURS_INC_PATH

#LIBS += -LYOURS_LIB_PATH -lyourslib.

HEADERS += \

SOURCES += \
        main.cpp \


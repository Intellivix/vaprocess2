﻿#include <iostream>

using namespace std;

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>

/* Flag set by ‘--verbose’. */
static int verbose_flag;

int main( int argc, char** argv )
{
   int c;

   while( 1 ) {
      static struct option long_options[] = {
         // --verbose 옵션을 만나면 "verbose_flag = 1"이 세팅된다.
         { "verbose", no_argument, &verbose_flag, 1 },
         { "brief", no_argument, &verbose_flag, 0 },
         // 이 아래로는 짧은 옵션과 동일한 옵션들
         // 예를 들어 --add 는 -a와 동일한 동작을 한다.
         { "add", no_argument, 0, 'a' },
         // 널포인터의 val이 'a'이면 'a'를 리턴할 것이다.
         { "append", no_argument, 0, 'b' },
         { "delete", required_argument, 0, 'd' },
         { "create", required_argument, 0, 'c' },
         { "file", required_argument, 0, 'f' },
         { 0, 0, 0, 0 } // 옵션 배열은 {0,0,0,0} 센티넬에 의해 만료된다.
      };

      // !!! 옵션 인덱스를 저장하는 변수
      int option_index = 0;

      // 옵션 파싱
      c = getopt_long( argc, argv, "abc:d:f:",
                       long_options, &option_index );

      // getopt()와 동일하게 모든 옵션 파싱을 마치면 -1을 리턴합니다.
      if( c == -1 )
         break;

      switch( c ) {
      case 0:
         // 긴 이름 옵션을 만났을 때
         if( long_options[option_index].flag != 0 )
            break;
         printf( "option %s", long_options[option_index].name );
         if( optarg )
            printf( " with arg %s", optarg );
         printf( "\n" );
         break;

      // 이하는 짧은 옵션과 동일한 긴 이름 옵션을 만났을 때
      case 'a':
         // `-a`를 만났거나 `--add`를 만났다.
         puts( "option -a\n" );
         break;

      case 'b':
         puts( "option -b\n" );
         break;

      case 'c':
         printf( "option -c with value `%s'\n", optarg );
         break;

      case 'd':
         // `--delete`, `-d`를 만난 경우, 추가 옵션이 필요하며 이는 `optarg`에 저장되어 있다.
         printf( "using short option.\n" );
         printf( "option -d with value `%s'\n", optarg );
         break;

      case 'f':
         printf( "option -f with value `%s'\n", optarg );
         break;

      case '?':
         /* getopt_long already printed an error message. */
         break;

      default:
         abort();
      }
   }

   // verbose_flag가 1로 설정되었을 때(--verbose)
   if( verbose_flag )
      puts( "verbose flag is set" );

   /* Print any remaining command line arguments (not options). */
   if( optind < argc ) {
      printf( "non-option ARGV-elements: " );
      while( optind < argc )
         printf( "%s ", argv[optind++] );
      putchar( '\n' );
   }

   cout << "Hello World!" << endl;

   exit( 0 );
}


// ./SampleProject --verbose

// ./SamplePooject --add
// ./SampleProject -a

// ./SampleProject -c "path_dir/file_name"
// ./SampleProject --create "path_dir/file_name"
// ./SampleProject --create="path_dir/file_name

// ./SampleProject -d tmp
// ./SampleProject --delete=tmp
// ./SampleProject --delete tmp
// ./SampleProject --delete "tmp"

